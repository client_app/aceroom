package kr.co.xcolo.android.fw.mvvm.retrofit;

public interface GenericRetrofitModel {
    boolean isError();
    String getErrorMessage();
}
