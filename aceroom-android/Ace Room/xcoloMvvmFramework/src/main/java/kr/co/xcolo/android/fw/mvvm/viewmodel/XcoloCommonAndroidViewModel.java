package kr.co.xcolo.android.fw.mvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.status.Status;
import kr.co.xcolo.android.fw.mvvm.status.StatusResource;
import kr.co.xcolo.android.fw.mvvm.status.XcoloCommonStatusHandleInterface;
import kr.co.xcolo.android.fw.mvvm.status.XcoloStatusCallback;
import kr.co.xcolo.android.fw.mvvm.status.XcoloStatusHandleHelper;

public abstract class XcoloCommonAndroidViewModel extends AndroidViewModel implements XcoloCommonStatusHandleInterface, XcoloStatusCallback {
    final XcoloStatusHandleHelper mStatusHandleViewModelHelper = new XcoloStatusHandleHelper();

    public XcoloCommonAndroidViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public LiveData<StatusResource> getStatusResource() {
        return mStatusHandleViewModelHelper.getStatusResource();
    }

    @Override
    public void setStatus(StatusResource statusResource) {
        mStatusHandleViewModelHelper.setStatusResource(statusResource);
    }

    @Override
    public void onLoading(Object who) {
        XcoloLog.d("onLoading: " + who);
        mStatusHandleViewModelHelper.setStatusResource(new StatusResource(who, Status.LOADING));
    }

    @Override
    public void onSuccess(Object who, Object data) {
        XcoloLog.d("onSuccess: " + who);
        mStatusHandleViewModelHelper.setStatusResource(new StatusResource(who, Status.SUCCESS));
    }

    @Override
    public void onError(Object who, Exception e) {
        XcoloLog.d("[" + who.toString() + "] onException: " + e.toString());
        mStatusHandleViewModelHelper.setStatusResource(new StatusResource(who, Status.ERROR, e));
    }
}
