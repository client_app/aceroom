package kr.co.xcolo.android.fw.mvvm.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;

import java.util.List;

@Dao
public interface GenericRoomDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(T... entity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertList(List<T> entity);

    @Update
    void update(T entity);

//    @Query("SELECT * FROM #{T} WHERE id = :id")
//    void findAll(int id);

    @Delete
    void delete(T entity);
}
