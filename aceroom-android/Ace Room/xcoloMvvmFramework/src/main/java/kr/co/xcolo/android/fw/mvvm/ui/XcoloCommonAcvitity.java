package kr.co.xcolo.android.fw.mvvm.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;

import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.status.XcoloCommonStatusHandleInterface;

public abstract class XcoloCommonAcvitity extends AppCompatActivity implements XcoloCommonViewInterface {
    @Override
    public void setViewModel(ViewModel viewModel) {
        subscribeToModel((XcoloCommonStatusHandleInterface) viewModel);
    }

    private void subscribeToModel(XcoloCommonStatusHandleInterface viewModel) {
        // 공통 오류 처리
        viewModel.getStatusResource().observe(this, statusResource -> {
            XcoloLog.d("Common Status Process: " + statusResource);
            switch (statusResource.getStatus()) {
                case LOADING:
                    onLoading();
                    break;
                case SUCCESS:
                case ERROR:
                    onLoadingComplete();
                    break;
            }
        });
    }

    @Override
    public void onLoading() {

    }

    @Override
    public void onLoadingComplete() {

    }
}
