package kr.co.xcolo.android.fw.mvvm.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.status.Status;
import kr.co.xcolo.android.fw.mvvm.status.StatusResource;
import kr.co.xcolo.android.fw.mvvm.status.XcoloCommonStatusHandleInterface;
import kr.co.xcolo.android.fw.mvvm.status.XcoloStatusCallback;
import kr.co.xcolo.android.fw.mvvm.status.XcoloStatusHandleHelper;

public abstract class XcoloCommonViewModel extends ViewModel implements XcoloCommonStatusHandleInterface, XcoloStatusCallback {
    final XcoloStatusHandleHelper mStatusHandleViewModelHelper = new XcoloStatusHandleHelper();

    @Override
    public LiveData<StatusResource> getStatusResource() {
        return mStatusHandleViewModelHelper.getStatusResource();
    }

    @Override
    public void setStatus(StatusResource statusResource) {
        mStatusHandleViewModelHelper.setStatusResource(statusResource);
    }

    @Override
    public void onLoading(Object who) {
        XcoloLog.d("onLoading: " + who);
        mStatusHandleViewModelHelper.setStatusResource(new StatusResource(who, Status.LOADING));
    }

    @Override
    public void onSuccess(Object who, Object data) {
        XcoloLog.d("onSuccess: " + who);
        mStatusHandleViewModelHelper.setStatusResource(new StatusResource(who, Status.SUCCESS));
    }

    @Override
    public void onError(Object who, Exception e) {
        XcoloLog.d("[" + who.toString() + "] onException: " + e.toString());
        mStatusHandleViewModelHelper.setStatusResource(new StatusResource(who, Status.ERROR, e));
    }
}
