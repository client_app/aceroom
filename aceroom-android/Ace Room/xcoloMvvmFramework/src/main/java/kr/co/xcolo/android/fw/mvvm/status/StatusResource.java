package kr.co.xcolo.android.fw.mvvm.status;

import lombok.Data;

@Data
public class StatusResource {
    private Object who;
    private Exception exception;
    private Status status;

    public StatusResource(Object who, Status status, Exception exception) {
        this.who = who;
        this.status = status;
        this.exception = exception;
    }

    public StatusResource(Object who, Status status) {
        this.who = who;
        this.status = status;
    }
}
