package kr.co.xcolo.android.fw.mvvm.status;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class XcoloException extends RuntimeException {
    public XcoloException() {
        super();
    }

    public XcoloException(String message) {
        super(message);
    }

    public XcoloException(String message, Throwable cause) {
        super(message, cause);
    }

    public XcoloException(Throwable cause) {
        super(cause);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    protected XcoloException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
