package kr.co.xcolo.android.fw.mvvm.retrofit;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    /*
    private static ShortenUrlApi urlApi;
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://openapi.naver.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static <T> T createService(Class<T> serviceClass) {
        return retrofit.create(serviceClass);
    }
    */

    private static String mApiBaseUrl = "http://abc.com";

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(mApiBaseUrl)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = null;

    private static HttpLoggingInterceptor logging =
            new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

//    private static HostSelectionInterceptor hostSelectInterceptor =
//            new HostSelectionInterceptor();

    private static OkHttpClient.Builder httpClient =
            new OkHttpClient.Builder();

    public static void changeApiBaseUrl(String newApiBaseUrl) {
        mApiBaseUrl = newApiBaseUrl;

        builder = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(mApiBaseUrl);
    }

    public static <S> S createService(Class<S> serviceClass) {
        httpClient.addInterceptor(new Interceptor() {

            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
//                String userId = NuguController.requestNuguData(NuguConst.USER_ID);
//                String authKey = NuguController.requestNuguData(NuguConst.AUTH_TOKEN);

                Request request = original.newBuilder()
                        .header("X-Naver-Client-Id", "QFveq6ch7evc8i1P20Eb")
                        .header("X-Naver-Client-Secret", "7cH82h4gbW")
                        .header("X-Api-Key", "LvF9CNv14a2gDhDvBXra29mNQIWVSSeBanxwNKhh")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        if (!httpClient.interceptors().contains(logging)) {
            httpClient.addInterceptor(logging);
        }

        builder.client(httpClient.build());
        retrofit = builder.build();

        return retrofit.create(serviceClass);
    }


}
