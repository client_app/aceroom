package kr.co.xcolo.android.fw.mvvm.ui;

import androidx.lifecycle.ViewModel;

public interface XcoloCommonViewInterface {
    void setViewModel(ViewModel viewModel);
    void refresh();
    void onLoading();
    void onLoadingComplete();
}
