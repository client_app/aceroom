package kr.co.xcolo.android.fw;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class XcoloLog {
//    private final static String AG_COMMON_TAG = "[XCOLO_" + BuildConfig.VERSION_NAME + "]";
    private final static String AG_COMMON_TAG = "[XCOLO]";
    private final static String AG_ERROR_TAG = AG_COMMON_TAG;

    private static OnLogErrorHandler mLogErrorHandler = null;
    private static Throwable mExp = null;

    public static void setOnLogErrorHandler(OnLogErrorHandler handler) {
        mLogErrorHandler = handler;
    }

    public static void d(String tag, String msg) {
        Log.d(tag, msg);
    }

    public static void i(String tag, String msg) {
        Log.i(tag, msg);
    }

    public static void w(String tag, String msg) {
        Log.w(tag, msg);
    }

    public static void e(String tag, String msg) {
        Log.e(tag, msg);
        if (mLogErrorHandler != null) {
            mLogErrorHandler.onLogError(msg, new Throwable(mExp));
        }
    }

    private static long mStartM = System.currentTimeMillis();

    public static void resetP() {
        mStartM = System.currentTimeMillis();
    }

    public static void trace(String message) {
        d(AG_COMMON_TAG, getExceptionMessage(new RuntimeException(message)));
    }

    public static void p(String msg) {
        long currentT = System.currentTimeMillis();
        d(AG_COMMON_TAG + "_PERF", (currentT - mStartM) + ":" + msg);
        mStartM = currentT;
    }

    public static void d(String msg) {
        XcoloLog.d(AG_COMMON_TAG, "[" + getCallClassName() + "] " + msg);
    }

    public static void d(Object clz, String text) {
        String head = null;
        if (clz instanceof Class) {
            head = ((Class<?>) clz).getSimpleName();
        } else {
            head = clz.getClass().getSimpleName();
        }
        d(AG_COMMON_TAG, "[" + head + "] " + text);
    }

    public static void i(String msg) {
        i(AG_COMMON_TAG, "[" + getCallClassName() + "] " + msg);
    }

    public static void i(Object clz, String text) {
        mExp = null;
        String head = null;
        if (clz instanceof Class) {
            head = ((Class<?>) clz).getSimpleName();
        } else {
            head = clz.getClass().getSimpleName();
        }
        i(AG_COMMON_TAG, "[" + head + "] " + text);
    }

    public static void w(String msg) {
        w(AG_COMMON_TAG, "[" + getCallClassName() + "] " + msg);
    }

    public static void w(Throwable cause) {
        w(getExceptionMessage(cause));
    }

    public static void w(Object clz, String text) {
        mExp = null;
        w(AG_ERROR_TAG, "[" + clz.getClass().getSimpleName() + "] " + text);
    }

    public static void e(String msg) {
        mExp = null;
        e(AG_ERROR_TAG, "[" + getCallClassName() + "] " + msg);
    }

    public static void e(String tag, Throwable cause) {
        mExp = cause;
        e(tag, getExceptionMessage(cause));
    }

    public static void e(Throwable cause) {
        mExp = cause;
        e(AG_ERROR_TAG, getExceptionMessage(cause));
    }

    public static void e(Object clz, String text) {
        mExp = null;
        String head = null;
        if (clz instanceof Class) {
            head = ((Class<?>) clz).getSimpleName();
        } else {
            head = clz.getClass().getSimpleName();
        }
        e(AG_ERROR_TAG, "[" + head + "] " + text);
    }

    public static void e(Object clz, Throwable cause) {
        mExp = cause;
        e(AG_ERROR_TAG, "[" + clz.getClass().getSimpleName() + "] " + getExceptionMessage(cause));
    }

    private static String getExceptionMessage(Throwable cause) {
        String ret_message = "";

        if (cause != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            cause.printStackTrace(ps);

            ret_message = baos.toString();
            //		ret_message = baos.toString().replaceAll("\n", " ");
        }

        return ret_message;
    }

    private static String getCallClassName() {
        StackTraceElement[] ste = new Throwable().getStackTrace();
        List<String> elementList = new ArrayList<>();
        int i = 0;
        for (StackTraceElement element : ste) {
//            NUGULog.d(NUGULog.class, "element: [" + i++ + "]" + element.getClassName());
            elementList.add(element.getClassName());
        }
        if (elementList.size() > 2) {
            String classFullName = elementList.get(2);
            return classFullName.substring(classFullName.lastIndexOf(".")+1);
        } else {
            return null;
        }
    }

    public static interface OnLogErrorHandler {
        public void onLogError(String msg, Throwable cause);
    }
}
