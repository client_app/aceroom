package kr.co.xcolo.android.fw.mvvm.retrofit;

import kr.co.xcolo.android.fw.mvvm.status.XcoloException;
import retrofit2.Response;

public class ApiErrorHandler {
    public static Exception getException(Response response) {
//        return new RuntimeException(response.errorBody(), response.message(), response.code())
        return new XcoloException("[" + response.code() + "]: " + response.message() );
    }

    public static Exception getException(String message) {
//        return new RuntimeException(response.errorBody(), response.message(), response.code())
        return new XcoloException(message);
    }
}
