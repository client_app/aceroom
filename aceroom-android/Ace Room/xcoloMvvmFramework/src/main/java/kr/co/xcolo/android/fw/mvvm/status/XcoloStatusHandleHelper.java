package kr.co.xcolo.android.fw.mvvm.status;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class XcoloStatusHandleHelper {
    private MutableLiveData<StatusResource> mStatusResource = new MutableLiveData<>();

    public LiveData<StatusResource> getStatusResource() {
        return mStatusResource;
    }

    public void setStatusResource(StatusResource statusResource) {
        mStatusResource.postValue(statusResource);
    }

//    public void setSuccessOccur(Object who) {
//        StatusResource statusTarget = new StatusResource();
//        statusTarget.setWho(who);
//        statusTarget.setStatusResource(Status.SUCCESS);
//        mStatusResource.postValue(statusTarget);
//    }
//
//    public void setErrorOccur(Object who, Exception exception) {
//        StatusResource statusTarget = new StatusResource();
//        statusTarget.setWho(who);
//        statusTarget.setException(exception);
//        statusTarget.setStatusResource(Status.ERROR);
//        mStatusResource.postValue(statusTarget);
//    }
//
//    public void setLoadingOccur(Object who) {
//        StatusResource statusTarget = new StatusResource();
//        statusTarget.setWho(who);
//        statusTarget.setStatusResource(Status.LOADING);
//        mStatusResource.postValue(statusTarget);
//    }
}
