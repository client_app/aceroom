package kr.co.xcolo.android.fw.mvvm.retrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class ApiCallback<T> implements Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.body() != null) {
            handleResponseData(response.body());
        } else {
            handleError(response);
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        handleException((Exception) t);
        /*
        if (t instanceof Exception) {
            handleException((Exception) t);
        } else {
            // ...
        }
        */
    }

    abstract protected void handleResponseData(T data);
    abstract protected void handleError(Response<T> response);
    abstract protected void handleException(Exception t);
}
