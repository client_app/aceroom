package kr.co.xcolo.android.fw.mvvm.retrofit;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.status.XcoloStatusCallback;
import retrofit2.Call;
import retrofit2.Response;

public abstract class GenericRetrofitRepository<R extends GenericRetrofitModel> {
    XcoloStatusCallback mStatusCallback;

    abstract protected Call<R> makeRequest();

    public final void setStatusCallback(XcoloStatusCallback statusCallback) {
        mStatusCallback = statusCallback;
    }

    public final LiveData<R> doRequest() {
        final MutableLiveData<R> liveData = new MutableLiveData<>();
        if (mStatusCallback != null)
            mStatusCallback.onLoading(GenericRetrofitRepository.this);

        makeRequest().enqueue(new ApiCallback<R>() {
            @Override
            protected void handleResponseData(R data) {
                XcoloLog.d("handleResponseData: " + data.toString());

                if (data.isError() == true) {
                    if (mStatusCallback != null) {
                        mStatusCallback.onError(GenericRetrofitRepository.this, ApiErrorHandler.getException(data.getErrorMessage()));
                    }
                } else {
                    if (mStatusCallback != null)
                        mStatusCallback.onSuccess(GenericRetrofitRepository.this, data);
                    liveData.setValue(data);
                }
            }

            @Override
            protected void handleError(Response<R> response) {
                XcoloLog.d("handleError: " + mStatusCallback);

                if (mStatusCallback != null) {
                    mStatusCallback.onError(GenericRetrofitRepository.this, ApiErrorHandler.getException(response));
                }
            }

            @Override
            protected void handleException(Exception t) {
                XcoloLog.d("handleException: " + mStatusCallback);

                if (mStatusCallback != null) {
                    mStatusCallback.onError(GenericRetrofitRepository.this, t);
                }
            }
        });
        return liveData;
    }
}
