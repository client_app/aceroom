package kr.co.xcolo.android.fw.mvvm.status;

public interface XcoloStatusCallback {
    void onLoading(Object who);
    void onSuccess(Object who, Object data);
    /**
     * 오류 발생 시 구동되는 공통 Callback
     * @param who 오류를 발생시킨 대상
     * @param e 오류 내용
     */
    void onError(Object who, Exception e);
}
