package kr.co.xcolo.android.fw.mvvm.status;

import androidx.lifecycle.LiveData;

public interface XcoloCommonStatusHandleInterface {

    /**
     * 상태 변경 시 구동되는 Callback
     * @return
     */
    LiveData<StatusResource> getStatusResource();

    /**
     * 발생한 상태를 등록한다.
     *      * @param StatusResource 상태 정보
     */
    void setStatus(StatusResource statusResource);
}
