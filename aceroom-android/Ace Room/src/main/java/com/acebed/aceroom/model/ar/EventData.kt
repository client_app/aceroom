package com.acebed.aceroom.model.ar

data class EventData (
    val id: String = "",
    val name: String = "",
    val createdAt: String = "",
    val image: String = "",
    val title: String = "",
    val targetUrl: String = "",
    val type: String = "product"    // Event 데이터 RoomSet 와 비교하기 위해 강제 지정 ( 추후 데이터 json 구조 변경 될 경우 해당 값 초기화 처리 추가 )
)