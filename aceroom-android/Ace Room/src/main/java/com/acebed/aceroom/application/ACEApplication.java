package com.acebed.aceroom.application;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.igaworks.v2.core.AdBrixRm;
import com.igaworks.v2.core.application.AbxActivityHelper;
import com.igaworks.v2.core.application.AbxActivityLifecycleCallbacks;

import kr.co.xcolo.android.fw.AppExecutors;
import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.retrofit.ApiService;
/**
 * ACEApplication
 * Application
 * @Author Xcolo DK
 */
public class ACEApplication extends Application implements AdBrixRm.DeferredDeeplinkListener {
    private static ACEApplication mAppInstance;
    private AppExecutors mAppExecutors;
    private String mCurrentBaseUrl = "";
    private LatLng selectedLatLng = null;
    private Activity mActivity = null;

    public static ACEApplication getInstance() {
        return mAppInstance;
    }

    int mCurrentMenu = -1;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppInstance = this;
        mAppExecutors = new AppExecutors();
        ApiService.changeApiBaseUrl("https://apis.urbanbase.com/");

        // adbrix
        AbxActivityHelper.initializeSdk(ACEApplication.this, "fHo66gcr5kmK5IaXsgUAeQ", "ygiU06Xq50eXyvMUs1NHSg");
        if (Build.VERSION.SDK_INT >= 14) {
            registerActivityLifecycleCallbacks(new AbxActivityLifecycleCallbacks());
        }
        AdBrixRm.setDeferredDeeplinkListener(this);
    }

    // Main Menu 현재 선택 버튼
    public int getmCurrentMenu() {
        return mCurrentMenu;
    }

    public void setmCurrentMenu(int mCurrentMenu) {
        this.mCurrentMenu = mCurrentMenu;
    }

    public AppExecutors getAppExecutors() {
        return mAppExecutors;
    }

    public void changeBaseUrl(String url) {
        setmCurrentBaseUrl(url);
        ApiService.changeApiBaseUrl(url);
    }

    public String getmCurrentBaseUrl() {
        return mCurrentBaseUrl;
    }

    public void setmCurrentBaseUrl(String mCurrentBaseUrl) {
        this.mCurrentBaseUrl = mCurrentBaseUrl;
    }

    // 지도 선택했던 마커
    public LatLng getSelectedLatLng() {
        return selectedLatLng;
    }

    public void setSelectedLatLng(LatLng selectedLatLng) {
        this.selectedLatLng = selectedLatLng;
    }

    @Override
    public void onReceiveDeferredDeeplink(String s) {
        // AdbrixRm SDk returns deferred deeplink url as string.
        // Use this string value and send your user to certin acivity.
        XcoloLog.d("Application onReceiveDeferredDeeplink : " + s);

    }

    // 값 불러오기
    public boolean getPreferences(String key) {
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        return pref.getBoolean(key, false);
    }

    // 값 저장하기
    public void savePreferences(String key){
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, true);
        editor.commit();
    }

    public void leavedActivity(Activity activity) {
        mActivity = activity;
    }
    public Activity getLeavedActivity() {
        return mActivity;
    }

}
