package com.acebed.aceroom.view.catalog.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.acebed.aceroom.R;
import com.acebed.aceroom.adapter.CatalogWallFloorAdapter;
import com.acebed.aceroom.api.wallfloor.WallFloorResponse;
import com.acebed.aceroom.databinding.FragmentWallpaperBinding;
import com.acebed.aceroom.environment.ACEEnvironment;
import com.acebed.aceroom.ifaces.UserClickCallback;
import com.acebed.aceroom.model.catalog.WallFloorItem;
import com.acebed.aceroom.view.catalog.product.ProductDetailActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import kr.co.xcolo.android.fw.mvvm.ui.XcoloCommonFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link WallpaperFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

/**
 * WallpaperFragment
 * 벽지 카탈로그 리스트
 * @Author Xcolo DK
 */
public class WallpaperFragment extends XcoloCommonFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private List<WallFloorResponse.WallFloorListData> mWallPaperList;
    private ArrayList<WallFloorItem> mWallpaperArrayList;
    private CatalogWallFloorAdapter mAdapter;
    ProductViewModel mViewModel;

    private FragmentWallpaperBinding mBinding;
    // TODO: Rename and change types of parameters
    private String mFilter; // 필터값
    private String mParam2;

    public WallpaperFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WallpaperFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WallpaperFragment newInstance(String param1, String param2) {
        WallpaperFragment fragment = new WallpaperFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mFilter = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_wallpaper, container, false);
//        initViews(userInfoView);
        mBinding.setLifecycleOwner(getViewLifecycleOwner());

        return mBinding.getRoot();
    }

    @Override
    public void refresh() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        mBinding.setProductViewModel(mViewModel);

        mBinding.wallpaperImageButtonToScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.wallpaperRecyclerViewList.scrollTop();
                mBinding.wallpaperImageButtonToScrollTop.setVisibility(View.INVISIBLE);
            }
        });
        mBinding.wallpaperRecyclerViewList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(-1)) {
                    mBinding.wallpaperImageButtonToScrollTop.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && mBinding.wallpaperImageButtonToScrollTop.getVisibility() == View.INVISIBLE)
                    mBinding.wallpaperImageButtonToScrollTop.setVisibility(View.VISIBLE);
            }
        });

        setWallPaperItems(mFilter);
        setViewModel(mViewModel);

    }

    private void setWallPaperItems(String filter) {
        mViewModel.requestWallFloor().observe(this, wallFloorResponse -> {
            mWallPaperList = new ArrayList<>();
            mWallPaperList = wallFloorResponse.getWallpaperList();
            mWallpaperArrayList = new ArrayList<>();
            for (WallFloorResponse.WallFloorListData data : mWallPaperList) {
                mWallpaperArrayList.add(new WallFloorItem(data.getTitle(), data.getDescription(), data.getThumbnailPath(), data.getFilter(), data));
            }

            if (filter.equals(ACEEnvironment.CATEGORY_TOTAL)) {
                mAdapter = new CatalogWallFloorAdapter(mWallpaperArrayList, mUserClickCallback);
            } else {
                ArrayList<WallFloorItem> selectedArrayList = new ArrayList<>();
                for (WallFloorItem wallFloorItem : mWallpaperArrayList) {
                    if (wallFloorItem.getFilter().equals(filter)) {
                        selectedArrayList.add(wallFloorItem);
                    }
                }
                mAdapter = new CatalogWallFloorAdapter(selectedArrayList, mUserClickCallback);
            }

            mBinding.wallpaperRecyclerViewList.setAdapter(mAdapter);
        });
    }

    private final UserClickCallback mUserClickCallback = new UserClickCallback() {
        @Override
        public void onClick(Object wallpaperItem) {
            WallFloorItem item = (WallFloorItem) wallpaperItem;
            String wallpaperInfo = new Gson().toJson(item.getWallFloorListData());
            String returnMsg = "{\"material\":" + wallpaperInfo + "}";
            Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
            intent.putExtra("isPageMode", ACEEnvironment.PAGE_MODE_WALLFLOOR);
            intent.putExtra("wallFloorName", item.getName());
            intent.putExtra("wallFloorDescription", item.getDescription());
            intent.putExtra("wallFloorImageUrl", item.getCatalogThumbnailImageUrl());
            intent.putExtra("wallFloorResultMsg", returnMsg);
            getActivity().startActivity(intent);
        }
    };
}
