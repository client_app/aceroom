package com.acebed.aceroom.exception

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast

/**
 * UnityPlayerActivity에서 발생하는 모든 Exception 에러 처리 Handler
 * 작성자: Urbanbase Android 팀
 */
class AppExceptionHandler(val application: Application) : Thread.UncaughtExceptionHandler {
    
    init {
        application.registerActivityLifecycleCallbacks(
            object : Application.ActivityLifecycleCallbacks {
                override fun onActivityPaused(activity: Activity?) {
                }
                
                override fun onActivityResumed(activity: Activity?) {
                }
                
                override fun onActivityStarted(activity: Activity?) {
               
                }
                
                override fun onActivityDestroyed(activity: Activity?) {
                }
                
                override fun onActivitySaveInstanceState(activity: Activity?,
                                                         outState: Bundle?) {
                }
                
                override fun onActivityStopped(activity: Activity?) {
                }
                
                override fun onActivityCreated(activity: Activity?,
                                               savedInstanceState: Bundle?) {
                }
            })
    }
    
    
    override fun uncaughtException(t: Thread?, e: Throwable) {
        killThisProcess {
            Toast.makeText(application.applicationContext, "error", Toast.LENGTH_SHORT).show()
        }
    }
    
    private fun killThisProcess(action: () -> Unit = {}) {
        action()
        
        android.os.Process.killProcess(android.os.Process.myPid())
        System.exit(0)
    }
}
