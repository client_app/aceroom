package com.acebed.aceroom.model.catalog;

import lombok.Data;

@Data
public class CatalogOptionItem {
    String name;
    String filter;

    public CatalogOptionItem(String name, String filter) {
        this.name = name;
        this.filter = filter;
    }
}
