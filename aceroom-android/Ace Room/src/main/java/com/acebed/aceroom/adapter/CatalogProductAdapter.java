package com.acebed.aceroom.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.acebed.aceroom.R;
import com.acebed.aceroom.ifaces.UserClickCallback;
import com.acebed.aceroom.model.catalog.ProductItem;
import com.acebed.aceroom.util.ACEUtil;

import java.util.ArrayList;

import kr.co.xcolo.android.fw.XcoloLog;

public class CatalogProductAdapter extends RecyclerView.Adapter<CatalogProductAdapter.CatalogProductViewHolder> {

private ArrayList<ProductItem> mProductItemList;

    @Nullable
    private final UserClickCallback mUserClickCallback;
    public CatalogProductAdapter(ArrayList<ProductItem> list, @Nullable UserClickCallback clickCallback) {
        this.mProductItemList = list;
        this.mUserClickCallback = clickCallback;
    }


    @Override
    public CatalogProductViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_product, viewGroup, false);
        CatalogProductViewHolder viewHolder = new CatalogProductViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull CatalogProductViewHolder viewholder, int position) {
        ACEUtil.setImageFromUrl(mProductItemList.get(position).getCatalogThumbnailImageUrl(), viewholder.imageViewThumbnail);

        viewholder.imageViewThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                XcoloLog.d("Item Click : " + mProductItemList.get(position).getName());
                mUserClickCallback.onClick(mProductItemList.get(position));

            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != mProductItemList ? mProductItemList.size() : 0);
    }


    public class CatalogProductViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewThumbnail;

        public CatalogProductViewHolder(View view) {
            super(view);
            imageViewThumbnail = view.findViewById(R.id.item_imageView_thumbnail);
        }
    }

}
