package com.acebed.aceroom.model;

import lombok.Data;

@Data
public class TutorialItem {
    int imageId;
    String text;

    public TutorialItem(int imageId, String text) {
        this.imageId = imageId;
        this.text = text;
    }
}
