package com.acebed.aceroom.model.home;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.maps.android.clustering.ClusterItem;

import lombok.Data;

@Data
public class Hangjung implements ClusterItem {
    String name;
    double lat;
    double lng;
    @Expose
    String snippet;

    @Override
    public LatLng getPosition() {
        LatLng latLng = new LatLng(this.lat, this.lng);
        return latLng;
    }

    @Override
    public String getTitle() {
        return this.name;
    }
}
