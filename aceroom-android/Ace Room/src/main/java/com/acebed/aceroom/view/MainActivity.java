package com.acebed.aceroom.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.acebed.aceroom.R;
import com.acebed.aceroom.adapter.WheelMenuAdapter;
import com.acebed.aceroom.application.ACEApplication;
import com.acebed.aceroom.lib.CursorWheelLayout;
import com.acebed.aceroom.model.MainMenuData;
import com.igaworks.v2.core.AdBrixRm;

import java.util.ArrayList;
import java.util.List;

import kr.co.xcolo.android.fw.XcoloLog;

/**
 * MainActivity
 * @Author Xcolo DK
 */

public class MainActivity extends Activity {
    ViewMapper mViewMapper;
    List<MainMenuData> mMainMenuImage;
    WheelMenuAdapter mMenuAdapter;
    boolean mMenuStatus = false;

    AdBrixRm.AttrModel eventAttr = new AdBrixRm.AttrModel();

    @Override
    public void onBackPressed() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("result", "MAIN_BACK");
        ACEApplication.getInstance().setmCurrentMenu(-1);
        setResult(RESULT_OK, resultIntent);
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewMapper = new ViewMapper(this);

        loadData();

        mMenuAdapter = new WheelMenuAdapter(getBaseContext(), mMainMenuImage);
        mViewMapper.wheel_image.setAdapter(mMenuAdapter);

        int currentMenuNo = ACEApplication.getInstance().getmCurrentMenu();
        XcoloLog.d("currentMenuNo : " + currentMenuNo);
        if (currentMenuNo != -1) {
            mViewMapper.imageButtonMenu.setBackgroundResource(R.drawable.main_02_button_click);
            mViewMapper.layoutWheel.setVisibility(View.VISIBLE);
            mMenuStatus = true;
            mViewMapper.wheel_image.setSelection(currentMenuNo);
        }

        //        wheel_text.setOnMenuSelectedListener(this);
        mViewMapper.wheel_image.setOnMenuSelectedListener(new CursorWheelLayout.OnMenuSelectedListener() {
            @Override
            public void onItemSelected(CursorWheelLayout parent, View view, final int pos) {
                XcoloLog.d("onItemSelected pos : " + pos);
                mViewMapper.textViewTitleKo.setText(mMainMenuImage.get(pos).imageDescriptionKo);
            }
        });

        // AdBrix 이벤트 추가 정보 설정


        // "친구초대" 버튼 클릭
        AdBrixRm.event("Main_Menu_open_menu_btn",eventAttr);

        mViewMapper.wheel_image.setOnMenuItemClickListener(new CursorWheelLayout.OnMenuItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                XcoloLog.d("onItemClick pos : " + pos);
                if (mViewMapper.wheel_image.getSelectedPosition() == pos) {
                    XcoloLog.d("moveToPage pos : " + pos);
                    ACEApplication.getInstance().setmCurrentMenu(pos);
                    Intent resultIntent = new Intent();
                    String isBtnName = "";
                    switch (pos % 3) {
                        case 0:
                            resultIntent.putExtra("result", "TARGET");
                            isBtnName = "Main_Menu_AR3_btn";
                            break;
                        case 1:
                            resultIntent.putExtra("result", "LENS");
                            isBtnName = "Main_Menu_AR2_btn";
                            break;
                        case 2:
                            resultIntent.putExtra("result", "ROOM");
                            isBtnName = "Main_Menu_AR1_btn";
                            break;
                    }
                    setResult(RESULT_OK, resultIntent);
                    AdBrixRm.event(isBtnName,eventAttr);
                    finish();
                }
            }
        });

        mViewMapper.imageButtonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMenuStatus) {
                    mViewMapper.imageButtonMenu.setBackgroundResource(R.drawable.main_01_button_unclick);
                    mViewMapper.layoutWheel.setVisibility(View.INVISIBLE);
                    mMenuStatus = false;
                    //AdBrixRm.event("Main_Menu_close_menu_btn",eventAttr);
                } else {
                    mViewMapper.imageButtonMenu.setBackgroundResource(R.drawable.main_02_button_click);
                    mViewMapper.layoutWheel.setVisibility(View.VISIBLE);
                    mMenuStatus = true;
                    mMenuAdapter.notifyDataSetChanged();
//                    AdBrixRm.event("Main_Menu_open_menu_btn",eventAttr);
                }
            }
        });
    }

    // Device Home Btn Event 예외처리
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        ACEApplication.getInstance().leavedActivity(this);
    }

    /**
     * Button Data Setting
     */
    private void loadData() {
        XcoloLog.d("loadData()");
        mMainMenuImage = new ArrayList<>();
        mMainMenuImage.add(new MainMenuData(R.drawable.main_02_button_mattress_unclick, R.drawable.main_03_button_mattress_click, "매트리스 분석", "Mattress Analysis"));
        mMainMenuImage.add(new MainMenuData(R.drawable.main_03_button_arbed_unclick, R.drawable.main_02_button_acebed_click, "AR침대배치", "AR Mattress Position"));
        mMainMenuImage.add(new MainMenuData(R.drawable.main_02_button_home_unclick, R.drawable.main_04_button_home_click, "3D홈디자인", "3D Home Design"));
        mMainMenuImage.add(new MainMenuData(R.drawable.main_02_button_mattress_unclick, R.drawable.main_03_button_mattress_click, "매트리스 분석", "Mattress Analysis"));
        mMainMenuImage.add(new MainMenuData(R.drawable.main_03_button_arbed_unclick, R.drawable.main_02_button_acebed_click, "AR침대배치", "AR Mattress Position"));
        mMainMenuImage.add(new MainMenuData(R.drawable.main_02_button_home_unclick, R.drawable.main_04_button_home_click, "3D홈디자인", "3D Home Design"));
        mMainMenuImage.add(new MainMenuData(R.drawable.main_02_button_mattress_unclick, R.drawable.main_03_button_mattress_click, "매트리스 분석", "Mattress Analysis"));
        mMainMenuImage.add(new MainMenuData(R.drawable.main_03_button_arbed_unclick, R.drawable.main_02_button_acebed_click, "AR침대배치", "AR Mattress Position"));
        mMainMenuImage.add(new MainMenuData(R.drawable.main_02_button_home_unclick, R.drawable.main_04_button_home_click, "3D홈디자인", "3D Home Design"));
    }

    protected class ViewMapper {
        public ViewGroup layoutWheel;
        public ViewGroup layoutTitle;
        public CursorWheelLayout wheel_image;
        public ImageButton imageButtonMenu;
        public TextView textViewTitleKo;

        public ViewMapper(Activity activity) {
            layoutWheel = activity.findViewById(R.id.main_layout_wheel);
            layoutTitle = activity.findViewById(R.id.main_layout_menuTitle);
            wheel_image = activity.findViewById(R.id.wheel_image);
            imageButtonMenu = activity.findViewById(R.id.main_imageButton_menu);
            textViewTitleKo = activity.findViewById(R.id.main_textView_titleKo);
        }
    }
}
