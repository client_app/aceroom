package com.acebed.aceroom.model.ar

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AceProductData(
        val brand: String? = null,
        val basePath: String? = null,
        val products: List<ProductObjectData>? = null,
        val mattressList: List<MattressObjectData>? = null,
        val roomsetList: List<ProductObjectData>? = null,
        val createdAt: String? = null,
        val version: String? = null
) : Parcelable

@Parcelize
data class ProductObjectData(
        val name: String? = null,
        val catalogThumbnail: String? = null,
        val descList: List<DescriptionData>? = null,
        val catalogName: String? = null,
        val sizeList: List<String>? = null,
        val colorList: List<ColorObjectData>? = null,
        val baseOpt: BaseOptionData?= null,
        val filter: String? = null,
        var objectType: Int = -1,    // Type 0 : Frame&Mattress, 1 : Room Set
        val frameList: List<FrameObjectData>? = null,
        val furnitureList: List<FurnitureObjectData>? = null,
        val landingTarget: String = ""
) : Parcelable

@Parcelize
data class DescriptionData(
        val subject: String? = null,
        val desc: List<String>? = null
) : Parcelable

@Parcelize
data class ColorObjectData(
        val name: String? = null,
        val path: String? = null,
        val title: String? = null,
        val rgb: String? = null
) : Parcelable

@Parcelize
data class BaseOptionData(
        val size: String? = null,
        val color: String? = null,
        val mattress: String? = null
) : Parcelable

@Parcelize
data class FrameObjectData(
        val frameId: String? = null,
        val size: String? = null,
        val color: String? = null,
        val thumbnail: String? = null,
        val path: String? = null,
        val sfbPath: String? = null,
        val daeiPath: String? = null,
        val isDisplay: Boolean,
        val mattressOffset: MattressOffsetData? = null,
        val mattressScale: Double
) : Parcelable

@Parcelize
data class MattressOffsetData(
        val x: Double,
        val y: Double,
        val z: Double
) : Parcelable

@Parcelize
data class MattressObjectData(
        val mattressid: String? = null,
        val path: String? = null,
        val sfbPath: String? = null,
        val daeiPath: String? = null,
        val name: String? = null,
        val size: String? = null
) : Parcelable

@Parcelize
data class FurnitureObjectData(
        val furnitureId: String? = null,
        val color: String? = null,
        val thumbnail: String? = null,
        val path: String? = null,
        val sfbPath: String? = null,
        val daeiPath: String? = null,
        val isDisplay: Boolean
) : Parcelable