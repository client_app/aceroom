package com.acebed.aceroom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.acebed.aceroom.R;
import com.acebed.aceroom.lib.CursorWheelLayout;
import com.acebed.aceroom.model.MainMenuData;

import java.util.List;

public class WheelMenuAdapter extends CursorWheelLayout.CycleWheelAdapter {

    private Context mContext;
    private List<MainMenuData> menuItems;
    private LayoutInflater inflater;
    private int gravity;

    public WheelMenuAdapter(Context mContext, List<MainMenuData> menuItems) {
        this.mContext = mContext;
        this.menuItems = menuItems;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return menuItems.size();
    }

    @Override
    public View getView(View parent, int position) {
        MainMenuData data = getItem(position);
        View root = inflater.inflate(R.layout.wheel_menu_layout, null, false);
        ImageView imageView = (ImageView) root.findViewById(R.id.wheel_menu_item_iv);
        imageView.setImageResource(data.imageResource);
        root.setTag(data);
        return root;
    }

    @Override
    public MainMenuData getItem(int position) {
        return menuItems.get(position);
    }
}
