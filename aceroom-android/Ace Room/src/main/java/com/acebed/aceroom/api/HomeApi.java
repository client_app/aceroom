package com.acebed.aceroom.api;

import com.acebed.aceroom.api.buildings.BuildingDetailResponse;
import com.acebed.aceroom.api.buildings.BuildingsResponse;
import com.acebed.aceroom.api.productlist.ProductListResponse;
import com.acebed.aceroom.api.wallfloor.WallFloorResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface HomeApi {

    // 건물 조회
    @GET("v3/buildings")
    Call<BuildingsResponse> buildings(@Query(value = "bounds") String boundsValue, @Query(value = "attributes") String attributesValue, @Query(value = "include") String includeValue);

    // 건물 상세정보
    @GET("v3/buildings/{building_uuid}")
    Call<BuildingDetailResponse> buildingDetail(@Path ("building_uuid") String buildingUuid, @Query(value = "attributes") String attributesValue, @Query(value = "include") String includeValue);



    // Product Bed
//    @GET("json/productList_200724.json")
    @GET("json/productList_addLanding.json")
//    @GET("json/productList_twin.json")
    Call<ProductListResponse> products();

    // WallFloor Json
    @GET("json/wallfloorList.json")
    Call<WallFloorResponse> wallFloor();




}