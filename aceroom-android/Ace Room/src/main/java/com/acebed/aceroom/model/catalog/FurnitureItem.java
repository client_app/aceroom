package com.acebed.aceroom.model.catalog;

import com.acebed.aceroom.api.productlist.ProductListResponse;

import java.io.Serializable;

import lombok.Data;

@Data
public class FurnitureItem implements Serializable {
    String name;
    String catalogThumbnailImageUrl;
    String filter;

    ProductListResponse.RoomsetListData roomsetListData;

    public FurnitureItem(String name, String catalogThumbnailImageUrl, String filter, ProductListResponse.RoomsetListData roomsetListData) {
        this.name = name;
        this.catalogThumbnailImageUrl = catalogThumbnailImageUrl;
        this.filter = filter;
        this.roomsetListData = roomsetListData;
    }
}
