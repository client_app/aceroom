package com.acebed.aceroom.plugin;

import android.util.Log;

import com.acebed.aceroom.view.UnityPlayerActivity;
import com.unity3d.player.UnityPlayer;

public class Plugin {
    private static Plugin _plugin = null;
    private Plugin() {

    }
    public static Plugin getInstance() {
        if (_plugin == null) {
            _plugin = new Plugin();
        }
        return _plugin;
    }

    private UnityPlayerActivity mTargetActivity;

    public void setTargetActivity(UnityPlayerActivity activity) {
        mTargetActivity = activity;
    }

    public void CallAndroid(String msg)
    {
        Log.d("MYTEST", "CallAndroid");
        String message = msg + "(by CallAndroid)";
        UnityPlayer.UnitySendMessage("AndroidManager", "SetLog", message);
    }

    public void SetSDKMode(String mode)
    {
        UnityPlayer.UnitySendMessage("GeneralSetting", "SetSDKMode", mode); //"productview" homedesign
    }

    public void SetProductViewerMode()
    {
        UnityPlayer.UnitySendMessage("UIController", "ProductViewerMode", "");
    }

    public void LoadFrameAndMattress()
    {
        String json = "{\"frameName\":\"VENATO\", \"size\":\"K\", \"color\":\"OakNatural\", \"mattress\":\"ROYALACE380\"}";
        UnityPlayer.UnitySendMessage("UIController", "SetFrameAndMattressOption", json);
    }

    public void RequestCallback(String value)
    {
        Log.e("MYTEST", "RequestCallback. value : " + value);
        if (mTargetActivity != null) {
            mTargetActivity.hideProgressDlg();
        }else{
            Log.e("MYTEST", "RequestCallback. mTrargetAcity is null");
        }
    }

    public void LoadProductCallback(String value)
    {
        Log.e("MYTEST", "LoadProductCallback. value : " + value);
        if (mTargetActivity != null) {
            mTargetActivity.hideProgressDlg();
        }else{
            Log.e("MYTEST", "LoadProductCallback. mTrargetAcity is null");
        }
    }
}