package com.acebed.aceroom.model.catalog;

import com.acebed.aceroom.api.wallfloor.WallFloorResponse;

import java.io.Serializable;

import lombok.Data;

@Data
public class WallFloorItem implements Serializable {
    String name;
    String description;
    String catalogThumbnailImageUrl;
    String filter;
    WallFloorResponse.WallFloorListData wallFloorListData;

    public WallFloorItem(String name, String description, String catalogThumbnailImageUrl, String filter, WallFloorResponse.WallFloorListData wallFloorListData) {
        this.name = name;
        this.description = description;
        this.catalogThumbnailImageUrl = catalogThumbnailImageUrl;
        this.filter = filter;
        this.wallFloorListData = wallFloorListData;
    }
}
