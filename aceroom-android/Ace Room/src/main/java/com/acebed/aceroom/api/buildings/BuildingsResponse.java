package com.acebed.aceroom.api.buildings;

import com.acebed.aceroom.environment.ACEEnvironment;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import kr.co.xcolo.android.fw.mvvm.retrofit.GenericRetrofitModel;
import lombok.Data;

/**
 * BuildingsResponse
 * 빌딩정보 Response
 * @Author Xcolo DK
 * 수정 : 2020.05.14 - 불필요한 값들 제거
 */
@Data
public class BuildingsResponse implements GenericRetrofitModel {
    private String code;
    private String message;
    private BuildingsData data;
    @Override
    public boolean isError() {
        if(getCode().equals("00000"))
            return false;
        else
            return true;
    }

    @Override
    public String getErrorMessage() {
        return getMessage();
    }

    @Data
    public static class BuildingsData {
        private List<BuildingsListData> buildings;
    }

    @Data
    public static class BuildingsListData {
        private String id;
//        @SerializedName("building_name") private String buildingName;
//        @SerializedName("building_no") private String buildingNo;
//        @SerializedName("road_address") private String roadAddress;
//        private String address;
        private double latitude;
        private double longitude;
//        @SerializedName("construction_company") private String constructionCompany;
//        @SerializedName("building_completion_ym") private String buildingCompletionYm;
//        @SerializedName("total_household_count") private int totalHouseholdCount;
//        @SerializedName("total_dong_count") private int totalDongCount;
//        @SerializedName("m_building_type_id") private String mBuildingTypeId;
//        @SerializedName("batl_ratio") private int batlRatio;
//        @SerializedName("btl_ratio") private int btlRatio;
//        @SerializedName("household_parking") private int householdParking;
//        @SerializedName("low_floor") private int lowFloor;
//        @SerializedName("high_floor") private int highFloor;
//        @SerializedName("heat_type") private String heatType;
//        @SerializedName("building_area") private String buildingArea;
//        @SerializedName("country_id") private String countryId;
//        private Boolean publish;
//        private Boolean enabled;
//        @SerializedName("object_id") private String objectId;
//        @SerializedName("created_date") private String createdDate;
//        @SerializedName("updated_date") private Date updatedDate;
//        @SerializedName("deleted_date") private Date deletedDate;
        @SerializedName("building_uuid") private String buildingUuid;
        private CountryData country;
    }

    @Data
    public static class CountryData {
        private String id;
        private String name;
        private String code;
        private String callingCode;
    }


    @Data
    public static class EngineStatusData {
        private String id;
        private String name;
        private String code;
        private String category;
        private Boolean enabled;
        private Object sort;
        private String description;
    }
}
