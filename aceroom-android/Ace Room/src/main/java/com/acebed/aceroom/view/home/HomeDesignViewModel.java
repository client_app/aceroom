package com.acebed.aceroom.view.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.acebed.aceroom.api.buildings.BuildingDetailRepository;
import com.acebed.aceroom.api.buildings.BuildingDetailResponse;
import com.acebed.aceroom.application.ACEApplication;
import com.acebed.aceroom.api.buildings.BuildingsRepository;
import com.acebed.aceroom.api.buildings.BuildingsResponse;
import com.google.android.gms.maps.model.LatLng;

import kr.co.xcolo.android.fw.AppExecutors;
import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.viewmodel.XcoloCommonAndroidViewModel;

public class HomeDesignViewModel extends XcoloCommonAndroidViewModel {
    private boolean isError;
    private boolean isLoading;
    private AppExecutors mAppExecutors;

    public MutableLiveData<BuildingsResponse> mBuildings;
    public MutableLiveData<BuildingDetailResponse> mBuildingDetail;

    public HomeDesignViewModel(@NonNull Application application) {
        super(application);
        mAppExecutors = ((ACEApplication) application).getAppExecutors();
        mBuildings = new MutableLiveData<>();
    }

    // 도면정보 가져오기
    public LiveData<BuildingsResponse> requestBuildings(LatLng left, LatLng right) {
        String boundString = "[" + left.latitude + "," + left.longitude + "," + right.latitude + "," + right.longitude + "]";
        XcoloLog.d("BoundString: " + boundString);
        mBuildings = (MutableLiveData<BuildingsResponse>) BuildingsRepository.createInstance(boundString, this).onBuildingsRequest();
        return mBuildings;
    }

    // 건물정보 가져오기
    public LiveData<BuildingDetailResponse> requestBuildingDetail(String buildingUuid) {
        XcoloLog.d("BuildingUuid: " + buildingUuid);
        mBuildingDetail = (MutableLiveData<BuildingDetailResponse>) BuildingDetailRepository.createInstance(buildingUuid, this).onBuildingsRequest();
        return mBuildingDetail;
    }

    /*
     * View Model에서 오류에 대한 업무 로직 처리가 필요할때 해당 메소드를 상속받아서 처리한다.
     */
    @Override
    public void onError(Object who, Exception e) {
        super.onError(who, e);
        XcoloLog.d("Exception: " + e.getMessage());
        if (who instanceof BuildingsRepository) {
            XcoloLog.d("onError: UnitsRepository");
        } else {
            XcoloLog.d("onError.who: " + who);
        }
    }
}
