package com.acebed.aceroom.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.acebed.aceroom.R;
import com.acebed.aceroom.util.ACEUtil;
import com.acebed.aceroom.view.catalog.product.ProductDetailActivity;

import kr.co.xcolo.android.fw.XcoloLog;
/**
 * ColorButton
 * 침대 색상 선택 버튼
 * @Author Xcolo DK
 */
public class ColorButton extends LinearLayout {

    ViewMapper mViewMapper;

    View.OnClickListener mUserOnClickListener = null;
    String mColorName;
    String mColorValue;
    int mColorRgb;
    String mColorPath;

    public ColorButton(Context context) {
        super(context);
        initView();
    }

    public ColorButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PRODUCT);
        mColorName = typedArray.getString(R.styleable.PRODUCT_colorName);
        mColorValue = typedArray.getString(R.styleable.PRODUCT_colorValue);
        mColorRgb = typedArray.getInt(R.styleable.PRODUCT_colorId, 0);
        mColorPath = typedArray.getString(R.styleable.PRODUCT_colorValue);
        initView();
    }

    public ColorButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PRODUCT);
        mColorName = typedArray.getString(R.styleable.PRODUCT_colorName);
        mColorValue = typedArray.getString(R.styleable.PRODUCT_colorValue);
        mColorRgb = typedArray.getInt(R.styleable.PRODUCT_colorId, 0);
        mColorPath = typedArray.getString(R.styleable.PRODUCT_colorValue);
        initView();
    }

    public ColorButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PRODUCT);
        mColorName = typedArray.getString(R.styleable.PRODUCT_colorName);
        mColorValue = typedArray.getString(R.styleable.PRODUCT_colorValue);
        mColorRgb = typedArray.getInt(R.styleable.PRODUCT_colorId, 0);
        mColorPath = typedArray.getString(R.styleable.PRODUCT_colorValue);
        initView();
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        final View v = li.inflate(R.layout.layout_color_button, this, false);
        addView(v);

        super.setOnClickListener(mOnClickListener);

        mViewMapper = new ViewMapper(v);

        if (mColorRgb != 0) {
            mViewMapper.viewColor.setBackgroundColor(mColorRgb);
        }
        if (mColorName != null) {
            mViewMapper.textViewColorName.setText(mColorName);
        }

    }

    // 터치 이벤트 잡았을때 수신 코드
    View.OnClickListener mOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            v.findViewById(R.id.colorButton_layout_colorArea).setSelected(true);
            XcoloLog.d("Color Button mOnClickListener onClick");
        }
    };

    public void setColorButton(ProductDetailActivity.ColorItem item) {
        mViewMapper.textViewColorName.setText(item.getTitle());
        if(!item.getPath().isEmpty()) {
            ACEUtil.setImageFromUrl(item.getPath(), mViewMapper.viewColor);
        } else {
            mViewMapper.viewColor.setBackgroundColor(item.getColor());
            if (item.getColor() == Color.parseColor("#ffffff")) {
                mViewMapper.viewColorBack.setVisibility(View.VISIBLE);
            } else {
                mViewMapper.viewColorBack.setVisibility(View.GONE);
            }
        }

        mColorName = item.getTitle();
        mColorValue = item.getName();
        mColorRgb = item.getColor();
        mColorPath = item.getPath();
    }

    protected class ViewMapper {
        ViewGroup layoutColorArea;
        ImageView viewColor;
        View viewColorBack;
        TextView textViewColorName;

        public ViewMapper(View view) {
            layoutColorArea = view.findViewById(R.id.colorButton_layout_colorArea);
            viewColor = view.findViewById(R.id.colorButton_view_color);
            viewColorBack = view.findViewById(R.id.colorButton_view_colorBack);
            textViewColorName = view.findViewById(R.id.colorButton_textView_colorName);
        }
    }
}
