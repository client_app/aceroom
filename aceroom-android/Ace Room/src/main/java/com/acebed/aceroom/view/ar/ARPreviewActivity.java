package com.acebed.aceroom.view.ar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;

import com.acebed.aceroom.R;
import com.acebed.aceroom.model.ar.ColorObjectData;
import com.acebed.aceroom.model.ar.Const;
import com.acebed.aceroom.model.ar.DescriptionData;
import com.acebed.aceroom.model.ar.FrameObjectData;
import com.acebed.aceroom.model.ar.FurnitureObjectData;
import com.acebed.aceroom.model.ar.MattressObjectData;
import com.acebed.aceroom.model.ar.ProductObjectData;
import com.acebed.aceroom.plugin.AcePlugin;
import com.acebed.aceroom.view.UnityPlayerActivity;
import com.unity3d.player.UnityPlayer;
import com.urbanbase.sdk.arviewer.helper.Utils;
import com.urbanbase.sdk.arviewer.ui.UBARActivity;
import com.urbanbase.sdk.arviewer.ui.view.AceColorSelectionView;
import com.urbanbase.sdk.arviewer.ui.view.AceSpinner;
import com.urbanbase.sdk.arviewer.ui.view.UBGroupTextView;

import java.util.ArrayList;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;

public class ARPreviewActivity extends UnityPlayerActivity implements View.OnClickListener {
    
    private ProgressBar mProgressBar;
    private NestedScrollView mScrollView;
    private ArrayList<AceColorSelectionView> colorViewList = new ArrayList<>();
    private AceSpinner mSizeSpinner;
    private AceSpinner mMattressSpinner;
    private View mProgressDimView;
    private FrameLayout mContainer;
    
    private String mSelectedProductName = "";
    private String mSelectedSize = "";
    private String mSelectedColor = "";
    private String mSelectedMattressName = "";
    private String mBasePath = "";
    private int mProductType = -1;

    private ProductObjectData mSelectedProductObjectData;
    private ArrayList<MattressObjectData> mMattressObjectDatas;
    
    private static ARPreviewActivity mActivity;
    
    private View.OnTouchListener onDisallowInterceptTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            ((ViewGroup) v).requestDisallowInterceptTouchEvent(true);
            return false;
        }
    };
    
    @Override
    protected void onCreate(Bundle bundle) {
        isProductMode = true;
        
        super.onCreate(bundle);
        
        setContentView(R.layout.unity_activity);
        
        mProgressBar = findViewById(R.id.loading_progress);

        mContainer = findViewById(R.id.container_layout);

        mUnityPlayer = new UnityPlayer(this);
        mContainer.addView(mUnityPlayer.getView(),
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.MATCH_PARENT);
        mUnityPlayer.setOnTouchListener(onDisallowInterceptTouchListener);
        mUnityPlayer.requestFocus();

        mMattressObjectDatas = getIntent().getParcelableArrayListExtra(Const.EXTRA_KEY_MATTRESS_LIST_INFO);

        mBasePath = getIntent().getStringExtra(Const.EXTRA_KEY_BASE_PATH);
        mProductType = getIntent().getIntExtra(Const.EXTRA_KEY_PRODUCT_TYPE, -1);

        if (mProductType == ARActivity.ItemTypeProductCode) {
            mSelectedProductObjectData = getIntent().getParcelableExtra(Const.EXTRA_KEY_PRODUCT_INFO);

            mSelectedProductName = mSelectedProductObjectData.getName();
            mSelectedSize = mSelectedProductObjectData.getBaseOpt().getSize();
            mSelectedColor = mSelectedProductObjectData.getBaseOpt().getColor();
            mSelectedMattressName = mSelectedProductObjectData.getBaseOpt().getMattress();
        } else if (mProductType == ARActivity.ItemTypeRoomSetCode) {
            mSelectedProductObjectData = getIntent().getParcelableExtra(Const.EXTRA_KEY_ROOM_SET_INFO);

            mSelectedProductName = mSelectedProductObjectData.getName();
            mSelectedColor = mSelectedProductObjectData.getBaseOpt().getColor();
        }

        initWithControls();

//        showProgressDlg();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        if(mActivity == null) {
//        }
        mActivity = this;
        AcePlugin.getInstance().CallChangeSceneUrbanProductViewer("");
    }

    @Override
    protected void onDestroy() {
//        mActivity = null;
        isProductMode = false;
        super.onDestroy();

    }
    
    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.e("ARPreviewActivity", "onKeyUp is Back Key");
            return super.onKeyUp(keyCode, event);
        } else {
            Log.e("ARPreviewActivity", "onKeyUp is Any Key");
            return mUnityPlayer.injectEvent(event);
        }
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.e("ARPreviewActivity", "onKeyDown is Back Key");
            return super.onKeyDown(keyCode, event);
        } else {
            Log.e("ARPreviewActivity", "onKeyDown is Any Key");
            return mUnityPlayer.injectEvent(event);
        }
    }
    
    @Override
    public void onBackPressed() {
        Log.e("ARPreviewActivity", "onBackPressed");

        goBackIntent();
//        super.onBackPressed();
    }

    private void showProgressDlg() {
        if (!mProgressBar.isShown()) {
            Log.e("ARPreviewActivity", "ProgressBar Not Show. Set Visible");
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressDimView.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgressDlg() {
        runOnUiThread(() -> {
            Log.e("ARPreviewActivity", "hideProgressDlg()");
            mProgressBar.setVisibility(View.GONE);
            mProgressDimView.setVisibility(View.GONE);
        });
    }

    private void goBackIntent() {
        if (!isCheckUnityRunning()) {
            Intent data = new Intent(this, ARActivity.class);
            data.putExtra(UBARActivity.EXTRA_KEY_PRODUCT_TYPE, "");
            data.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(data);
        }
    }

    private void initWithControls() {
        // 놓아보기 버튼 선택 시 처리
        findViewById(R.id.ib_back).setOnClickListener(this);
        findViewById(R.id.btn_select).setOnClickListener(this);
        
        TextView titleNameTextView = findViewById(R.id.tv_title_name);
        String titleName = mSelectedProductObjectData.getCatalogName();
        titleNameTextView.setText(titleName);

        mSizeSpinner = findViewById(R.id.ace_spinner_size);
        mMattressSpinner = findViewById(R.id.ace_spinner_mattress);
        mProgressDimView = findViewById(R.id.dim_view);

        TextView sizeTextView = findViewById(R.id.tv_size);
        TextView mattressTextView = findViewById(R.id.tv_mattress);
        if (mProductType == ARActivity.ItemTypeProductCode) {
            sizeTextView.setVisibility(View.VISIBLE);
            mattressTextView.setVisibility(View.VISIBLE);

            mSizeSpinner.setVisibility(View.VISIBLE);
            mMattressSpinner.setVisibility(View.VISIBLE);

            mSizeSpinner.setData(mSelectedProductObjectData.getSizeList().toArray(new String[mSelectedProductObjectData.getSizeList().size()]), mSelectedSize);
            mSizeSpinner.setOnAceSpinnerClickListener(new Function0<Unit>() {
                @Override
                public Unit invoke() {
                    if (mMattressSpinner.isActive()) mMattressSpinner.hideList();
                    return null;
                }
            });
            mSizeSpinner.setOnAceSpinnetItemClickListener(new Function1<String, Unit>() {
                @Override
                public Unit invoke(String s) {
                    if (s != null && !s.isEmpty()) {
                        mSelectedSize = s;
                        showSelectedProduct();
                        setMattressSpinnerItems(mSelectedSize);
                    }
                    return null;
                }
            });

            setMattressSpinnerItems(mSelectedSize);
        } else {
            sizeTextView.setVisibility(View.GONE);
            mattressTextView.setVisibility(View.GONE);

            mSizeSpinner.setVisibility(View.GONE);
            mMattressSpinner.setVisibility(View.GONE);
        }

        findViewById(R.id.fab_scroll_up).setOnClickListener(this);
        mScrollView = findViewById(R.id.sv_preview);
        
        // 뷰 리스트 등록
        for (ColorObjectData colorObjectData : mSelectedProductObjectData.getColorList()) {
            if (colorObjectData.getPath() != null && !colorObjectData.getPath().isEmpty()) {
                colorViewList.add(new AceColorSelectionView(
                        this,
                        -1, colorObjectData.getName(), colorObjectData.getTitle(), colorObjectData.getPath()
                ));
            } else {
                colorViewList.add(new AceColorSelectionView(
                        this,
                        Color.parseColor(colorObjectData.getRgb()), colorObjectData.getName(), colorObjectData.getTitle(), colorObjectData.getPath()
                ));
            }
        }
        
        // 리스너 등록 및 addView
        LinearLayout colorListLayout = findViewById(R.id.ll_color_list);
        for (AceColorSelectionView colorSelectionView : colorViewList) {
            if (colorSelectionView.getName() != null && colorSelectionView.getName().equals(mSelectedColor)) {
                colorSelectionView.select();
            }
            colorSelectionView.setOnColorSelectionClickListener(new Function1<AceColorSelectionView, Unit>() {
                @Override
                public Unit invoke(AceColorSelectionView aceColorSelectionView) {
                    for (AceColorSelectionView child : colorViewList) {
                        child.unSelect();
                    }
                    aceColorSelectionView.select();
                    mSelectedColor = aceColorSelectionView.getName();
                    showSelectedProduct();
                    return null;
                }
            });

            colorListLayout.addView(colorSelectionView);
        }
        
        for (DescriptionData descriptionData : mSelectedProductObjectData.getDescList()) {
            LinearLayout container = findViewById(R.id.tv_container);
            UBGroupTextView gtv = new UBGroupTextView(this);
            
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.topMargin = Utils.INSTANCE.dpToPixel(this, 33);
            gtv.setLayoutParams(params);
            
            gtv.setSubectText(descriptionData.getSubject());
            gtv.setDescText(descriptionData.getDesc());
            
            container.addView(gtv);
        }
    }

    private void setMattressSpinnerItems(String selectedSize) {
        ArrayList<String> mattressNameList = new ArrayList<>();
        for (MattressObjectData mattressObjectData : mMattressObjectDatas) {
            if (mattressObjectData.getSize().equals(selectedSize)) {
                mattressNameList.add(mattressObjectData.getName());
            }
        }

        mMattressSpinner.setData(mattressNameList.toArray(new String[mattressNameList.size()]), mSelectedMattressName);
        mMattressSpinner.setOnAceSpinnerClickListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {
                if (mSizeSpinner.isActive()) mSizeSpinner.hideList();
                return null;
            }
        });
        mMattressSpinner.setOnAceSpinnetItemClickListener(new Function1<String, Unit>() {
            @Override
            public Unit invoke(String s) {
                if (s != null && !s.isEmpty()) {
                    mSelectedMattressName = s;
                    showSelectedProduct();
                }
                return null;
            }
        });
    }

    private boolean isCheckUnityRunning() {
//        return mProgressBar.isShown();
        return false;
    }

    private void showSelectedProduct() {
        String setFrameMattressJson = "{\n" +
                "            \"url\": \"" + getProductUrl() + "\",\n" +
                "                \"format\": \".zip\",\n" +
                "                \"planeType\": \"floor\",\n" +
                "                \"mattressUrl\": \"" + getMattressUrl() + "\",\n" +
                "                \"mattressFormat\": \".zip\",\n" +
                "                \"productType\": \"" + getProductTypeString() + "\"\n" +
                "        }";

        Log.e("MYTEST", setFrameMattressJson);
        AcePlugin.getInstance().CallSetFrameAndMattress(setFrameMattressJson);
//        showProgressDlg();
    }
    
    private void setProductResultAndFinish() {
        Intent data = new Intent(this, ARActivity.class);
        data.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        if (mProductType == ARActivity.ItemTypeProductCode) {
            data.putExtra(UBARActivity.EXTRA_FRAME_NAME, mSelectedProductName);
            data.putExtra(UBARActivity.EXTRA_MATTRESS_NAME, mSelectedMattressName);
            data.putExtra(UBARActivity.EXTRA_KEY_SIZE, mSelectedSize);
            data.putExtra(UBARActivity.EXTRA_KEY_PRODUCT_TYPE, ARActivity.ItemTypeProduct);
        } else {
            data.putExtra(UBARActivity.EXTRA_ROOM_SET_NAME, mSelectedProductName);
            data.putExtra(UBARActivity.EXTRA_KEY_PRODUCT_TYPE, ARActivity.ItemTypeRoomSet);
        }

        data.putExtra(UBARActivity.EXTRA_KEY_COLOR, mSelectedColor);

        setResult(RESULT_OK, data);
//        mUnityPlayer.destroy();
//        finish();

        startActivity(data);
    }

    private void scrollTop() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mScrollView.smoothScrollTo(0, 0);
            }
        });
    }

    private String getProductUrl() {
        String frameUrl = "";
        if (mProductType == ARActivity.ItemTypeProductCode) {
            for (FrameObjectData frameObjectData : mSelectedProductObjectData.getFrameList()) {
                if (frameObjectData.getColor() != null && frameObjectData.getColor().equals(mSelectedColor) &&
                        frameObjectData.getSize() != null && frameObjectData.getSize().equals(mSelectedSize)) {
                    frameUrl = frameObjectData.getPath();
                    break;
                }
            }
        } else if (mProductType == ARActivity.ItemTypeRoomSetCode) {
            for (FurnitureObjectData furnitureObjectData : mSelectedProductObjectData.getFurnitureList()) {
                if (furnitureObjectData.getColor() != null && furnitureObjectData.getColor().equals(mSelectedColor)) {
                    frameUrl = furnitureObjectData.getPath();
                    break;
                }
            }
        }

        return frameUrl;
    }

    private String getMattressUrl() {
        String mattressUrl = "";
        if (mProductType == ARActivity.ItemTypeProductCode) {
            for (MattressObjectData mattressObjectData : mMattressObjectDatas) {
                if (mattressObjectData.getName() != null && mattressObjectData.getName().equals(mSelectedMattressName) &&
                        mattressObjectData.getSize() != null && mattressObjectData.getSize().equals(mSelectedSize)) {
                    mattressUrl = mattressObjectData.getPath();
                    break;
                }
            }
        }

        return mattressUrl;
    }

    private String getProductTypeString() {
        if (mProductType == ARActivity.ItemTypeProductCode) {
            return "frame";
        } else if (mProductType == ARActivity.ItemTypeRoomSetCode) {
            return "roomset";
        } else {
            return "";
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select:
                if (!isCheckUnityRunning()) {
                    setProductResultAndFinish();
                }
                break;
            
            case R.id.fab_scroll_up:
                scrollTop();
                break;
            
            case R.id.ib_back:
                goBackIntent();
                break;
        }
    }
    
    
    /**
     * CallChangeSceneUrbanProductViewer() 호출 후 콜백 받기
     * 작성자: Urbanbase Android 팀
     *
     * @param value
     */
    public static void LoadProductViewerSceneCallback(Activity activity, String value) {
        Log.e("MYTEST", "RequestCallback. value : " + value);
//        mActivity.hideProgressDlg();

        mActivity.showSelectedProduct();
    }
    
    /**
     * UnitySendMessage() 호출 후 콜백 받기
     * 작성자: Urbanbase Android 팀
     *
     * @param value
     */
    public static void LoadProductCallback(Activity activity, String value) {
        Log.e("MYTEST", "LoadProductCallback. value : " + value);
        mActivity.hideProgressDlg();
    }
    
}
