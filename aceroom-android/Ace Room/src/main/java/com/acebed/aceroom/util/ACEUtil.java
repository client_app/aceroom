package com.acebed.aceroom.util;

import android.app.Activity;
import android.content.res.Resources;
import android.net.Uri;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.acebed.aceroom.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.text.SimpleDateFormat;
import java.util.Date;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * ACEUtil
 * 각종 Util
 * @Author Xcolo DK
 */
public class ACEUtil {
    public static int dpToPx(double dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(float px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static String dateName(long dateTaken) {
        Date date = new Date(dateTaken);
        SimpleDateFormat dateFormat =
                new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        return dateFormat.format(date);
    }

    public static void setImageFromUrl(String imageUrl, ImageView view) {
        setImageFromUrl(imageUrl, view, false);
    }

    public static void setImageFromUrl(String imageUrl, ImageView view, @Nullable boolean errorCheck) {
        Uri imageUri = Uri.parse(imageUrl);
        final Transformation transformation = new RoundedCornersTransformation(6, 0);
        if(errorCheck) {
            Picasso.get().load(imageUri).error(R.drawable.sub_05_background_03_3d_text_img).fit().transform(transformation).into(view);
        } else {
            Picasso.get().load(imageUri).fit().transform(transformation).into(view);
        }

    }

    public static void slideActivityFromLeftToRight(Activity activity) {
        activity.overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_nothing);
//        activity.overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);
    }

    public static void slideActivityFromRightToLeft(Activity activity) {
        activity.overridePendingTransition(R.anim.anim_nothing, R.anim.anim_slide_out_left);
//        activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    public static void fadeInActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out);
    }

    public static void fadeOutActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.anim_fade_out, R.anim.anim_nothing);
    }
}
