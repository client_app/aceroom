package com.acebed.aceroom.view.catalog;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.acebed.aceroom.application.ACEApplication;
import com.acebed.aceroom.api.productlist.ProductListRepository;
import com.acebed.aceroom.api.productlist.ProductListResponse;

import kr.co.xcolo.android.fw.AppExecutors;
import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.viewmodel.XcoloCommonAndroidViewModel;

public class HomeCatalogViewModel extends XcoloCommonAndroidViewModel {
    private boolean isError;
    private boolean isLoading;
    private AppExecutors mAppExecutors;

    public MutableLiveData<ProductListResponse> products;

    public HomeCatalogViewModel(@NonNull Application application) {
        super(application);
        mAppExecutors = ((ACEApplication) application).getAppExecutors();
        products = new MutableLiveData<>();
    }

    public LiveData<ProductListResponse> requestProducts() {
        products = (MutableLiveData<ProductListResponse>) ProductListRepository.createInstance(this).onProductsRequest();
        ;
        return products;
    }

    /*
     * View Model에서 오류에 대한 업무 로직 처리가 필요할때 해당 메소드를 상속받아서 처리한다.
     */
    @Override
    public void onError(Object who, Exception e) {
        super.onError(who, e);
        XcoloLog.d("Exception: " + e.getMessage());
    }
}
