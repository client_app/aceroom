package com.acebed.aceroom.view.catalog.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.acebed.aceroom.R;
import com.acebed.aceroom.adapter.CatalogWallFloorAdapter;
import com.acebed.aceroom.api.wallfloor.WallFloorResponse;
import com.acebed.aceroom.databinding.FragmentFlooringBinding;
import com.acebed.aceroom.environment.ACEEnvironment;
import com.acebed.aceroom.ifaces.UserClickCallback;
import com.acebed.aceroom.model.catalog.WallFloorItem;
import com.acebed.aceroom.view.catalog.product.ProductDetailActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import kr.co.xcolo.android.fw.mvvm.ui.XcoloCommonFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link FlooringFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
/**
 * FlooringFragment
 * 바닥재 카탈로그 리스트
 * @Author Xcolo DK
 */
public class FlooringFragment extends XcoloCommonFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private List<WallFloorResponse.WallFloorListData> mFloorList;
    private ArrayList<WallFloorItem> mFloorArrayList;
    private CatalogWallFloorAdapter mAdapter;
    ProductViewModel mViewModel;

    private FragmentFlooringBinding mBinding;
    // TODO: Rename and change types of parameters
    private String mFilter; // 필터값
    private String mParam2;

    public FlooringFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FlooringFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FlooringFragment newInstance(String param1, String param2) {
        FlooringFragment fragment = new FlooringFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mFilter = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_flooring, container, false);
//        initViews(userInfoView);
        mBinding.setLifecycleOwner(getViewLifecycleOwner());

        return mBinding.getRoot();
    }

    @Override
    public void refresh() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        mBinding.setProductViewModel(mViewModel);
        mBinding.flooringImageButtonToScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.flooringRecyclerViewList.scrollTop();
                mBinding.flooringImageButtonToScrollTop.setVisibility(View.INVISIBLE);
            }
        });
        mBinding.flooringRecyclerViewList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(-1)) {
                    mBinding.flooringImageButtonToScrollTop.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && mBinding.flooringImageButtonToScrollTop.getVisibility() == View.INVISIBLE)
                    mBinding.flooringImageButtonToScrollTop.setVisibility(View.VISIBLE);
            }
        });

        setFloorItems(mFilter);
        setViewModel(mViewModel);

    }

    private void setFloorItems(String filter) {
        mViewModel.requestWallFloor().observe(this, wallFloorResponse -> {
            mFloorList = new ArrayList<>();
            mFloorList = wallFloorResponse.getFloorList();
            mFloorArrayList = new ArrayList<>();
            for (WallFloorResponse.WallFloorListData data : mFloorList) {
                mFloorArrayList.add(new WallFloorItem(data.getTitle(), data.getDescription(), data.getThumbnailPath(), data.getFilter(), data));
            }

            if (filter.equals(ACEEnvironment.CATEGORY_TOTAL)) {
                mAdapter = new CatalogWallFloorAdapter(mFloorArrayList, mUserClickCallback);
            } else {
                ArrayList<WallFloorItem> selectedArrayList = new ArrayList<>();
                for (WallFloorItem wallFloorItem : mFloorArrayList) {
                    if (wallFloorItem.getFilter().equals(filter)) {
                        selectedArrayList.add(wallFloorItem);
                    }
                }
                mAdapter = new CatalogWallFloorAdapter(selectedArrayList, mUserClickCallback);
            }

            mBinding.flooringRecyclerViewList.setAdapter(mAdapter);

        });
    }

    private final UserClickCallback mUserClickCallback = new UserClickCallback() {
        @Override
        public void onClick(Object wallpaperItem) {
            WallFloorItem item = (WallFloorItem) wallpaperItem;
            String flooringInfo = new Gson().toJson(item.getWallFloorListData());
            String returnMsg = "{\"material\":" + flooringInfo + "}";
            Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
            intent.putExtra("isPageMode", ACEEnvironment.PAGE_MODE_WALLFLOOR);
            intent.putExtra("wallFloorName", item.getName());
            intent.putExtra("wallFloorDescription", item.getDescription());
            intent.putExtra("wallFloorImageUrl", item.getCatalogThumbnailImageUrl());
            intent.putExtra("wallFloorResultMsg", returnMsg);
            getActivity().startActivity(intent);
        }
    };

}
