package com.acebed.aceroom.view;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.Toast;

import com.acebed.aceroom.application.ACEApplication;
import com.acebed.aceroom.dialog.ACETutorialDialog;
import com.acebed.aceroom.dialog.HomeEditorSettingDialog;
import com.acebed.aceroom.exception.AppExceptionHandler;
import com.acebed.aceroom.plugin.AcePlugin;
import com.acebed.aceroom.util.ACEImageManager;
import com.acebed.aceroom.view.ar.ARActivity;
import com.acebed.aceroom.view.catalog.HomeCatalogActivity;
import com.acebed.aceroom.view.home.HomeDesignActivity;
import com.igaworks.v2.core.AdBrixRm;
import com.unity3d.player.UnityPlayer;

import kr.co.xcolo.android.fw.XcoloLog;

import static com.acebed.aceroom.application.ACEApplication.getInstance;

//import com.acebed.aceroom.view.ArLensActivity;

public class UnityPlayerActivity extends Activity {
    protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code
    private static final String TAG = "UnityPlayerActivity";

    // ARPreviewActivity에서 상품보기뷰를 구성하기 위한 플래그
    // ARPreviewActivity에서만 해당 플래그 true로 설정하여 사용.
    // 기존 UnityPlayer 미 동작 처리시에도 해당 플래그 사용
    // by. [urbanbase mobile team] - 2019.09.23
    public static boolean isProductMode = false;

    public static ACEImageManager mAceImageManager;
    public static String isFunction = "";

    private static boolean mIsCallMain = true;

    // Override this in your custom UnityPlayerActivity to tweak the command line arguments passed to the Unity Android Player
    // The command line arguments are passed as a string, separated by spaces
    // UnityPlayerActivity calls this from 'onCreate'
    // Supported: -force-gles20, -force-gles30, -force-gles31, -force-gles31aep, -force-gles32, -force-gles, -force-vulkan
    // See https://docs.unity3d.com/Manual/CommandLineArguments.html
    // @param cmdLine the current command line arguments, may be null
    // @return the modified command line string or null
    protected String updateUnityCommandLineArguments(String cmdLine) {
        return cmdLine;
    }

    // Setup activity layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        XcoloLog.e(TAG, "===========================onCreate()");
        String cmdLine = updateUnityCommandLineArguments(getIntent().getStringExtra("unity"));
        getIntent().putExtra("unity", cmdLine);

        if (!isProductMode) {
            mUnityPlayer = new UnityPlayer(this);
            setContentView(mUnityPlayer);
            mUnityPlayer.requestFocus();
        }

        setUpCrashHandler();

        // 이벤트 업로드 주기 설정 : 누적 이벤트가 10건일 때, AdBrix 서버로 이벤트 업로드
        AdBrixRm.setEventUploadCountInterval(AdBrixRm.AdBrixEventUploadCountInterval.MIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (isProductMode) return;

        Log.d(TAG, "onActivityResult requestCode : " + requestCode);
        Log.d(TAG, "onActivityResult resultCode : " + resultCode);
        Intent intent = null;

        //AcePlugin.getInstance().SendMessage("","");

        boolean isIntent = true;
        if (resultCode == RESULT_OK) {
            Log.d(TAG, "onActivityResult data.getStringExtra(result) : " + data.getStringExtra("result"));
            switch (requestCode) {
                case 3000:
                    if (data.getStringExtra("result").equals("TARGET")) {
                        Log.d(TAG, "onActivityResult TARGET!!");
                        isIntent = false;
//                        UnityPlayer.UnitySendMessage("DefaultSceneUIController", "onClickLoadARTargetScene","");
                        AcePlugin.getInstance().CallUnityLotationPortrait();
                        AcePlugin.getInstance().ChangeSceneARMattress();

                        if(!getInstance().getPreferences("TARGET")) {
                            ACETutorialDialog.newInstance(this).setMattressTutorialData().show();
                            getInstance().savePreferences("TARGET");
                        }
                    } else if (data.getStringExtra("result").equals("LENS")) {
//                        UnityPlayer.UnitySendMessage("DefaultSceneUIController", "onClickLoadARLensScene","");
//                        intent = new Intent(this, ArLensActivity.class);
                        intent = new Intent(this, ARActivity.class);
                    } else if (data.getStringExtra("result").equals("LENS_BACK")) {
                        intent = new Intent(this, MainActivity.class);
                    } else if (data.getStringExtra("result").equals("ROOM")) {
                        AcePlugin.getInstance().CallUnityLotationLandscape();
                        isIntent = false;
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(UnityPlayerActivity.this, HomeDesignActivity.class);
                                        UnityPlayerActivity.this.startActivityForResult(intent, 3000);
                                    }
                                });
                            }
                        }, 1000);

//                        isIntent = true;
//                        intent = new Intent(UnityPlayerActivity.this, HomeDesignActivity.class);

                    } else if (data.getStringExtra("result").equals("FLOOR_PLAN")) {
                        String floorPlanInfo = data.getStringExtra("floorPlanInfo");
                        Log.d(TAG, "floorPlanInfo : " + floorPlanInfo);
                        isIntent = false;
//                        UnityPlayer.UnitySendMessage("DefaultSceneUIController", "onClickRoomScene","");
//                        AcePlugin.getInstance().ChangeSceneHomeEditor("");
//                        UnityPlayerActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        AcePlugin.getInstance().ChangeSceneHomeEditor(floorPlanInfo);
                        //TODO:TEST

//                        AcePlugin.getInstance().ChangeSceneHomeEditor("{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1c271da53ab8ad712e9.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}");
                    } else if (data.getStringExtra("result").equals("HOME_BACK")) {
                        AcePlugin.getInstance().CallUnityLotationPortrait();
                        intent = new Intent(this, MainActivity.class);
                    } else if (data.getStringExtra("result").equals("MAIN_BACK")) {
                        isIntent = false;
                        finish();
                    }
                    if (isIntent) this.startActivityForResult(intent, 3000);
                    break;
            }
        } else {
            // Device Home Btn Event 예외처리
            if(ACEApplication.getInstance().getLeavedActivity() != null) {
                intent = new Intent(this, ACEApplication.getInstance().getLeavedActivity().getClass());
                this.startActivityForResult(intent, 3000);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // To support deep linking, we need to make sure that the client can get access to
        // the last sent intent. The clients access this through a JNI api that allows them
        // to get the intent set on launch. To update that after launch we have to manually
        // replace the intent with the one caught here.
        setIntent(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mAceImageManager != null) {
            mAceImageManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    // Quit Unity
    @Override
    protected void onDestroy() {
        XcoloLog.e(TAG, "===========================onDestroy()");
        mUnityPlayer.destroy();
        super.onDestroy();
    }

    // Pause Unity
    @Override
    protected void onPause() {
        XcoloLog.e(TAG, "===========================onPause()");
        mUnityPlayer.pause();
        super.onPause();
    }

    // Resume Unity
    @Override
    protected void onResume() {
        XcoloLog.e(TAG, "===========================onResume()");
        super.onResume();
        mUnityPlayer.resume();
    }

    @Override
    protected void onStart() {
        XcoloLog.e(TAG, "===========================onStart()");
        super.onStart();
        mUnityPlayer.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mUnityPlayer.stop();
    }

    // Low Memory Unity
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mUnityPlayer.lowMemory();
    }

    // Trim Memory Unity
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level == TRIM_MEMORY_RUNNING_CRITICAL) {
            mUnityPlayer.lowMemory();
        }
    }

    // This ensures the layout will be correct.
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.e("UnityPlayerActivity", "onKeyUp is Back Key");
            return super.onKeyUp(keyCode, event);
        } else {
            Log.e("UnityPlayerActivity", "onKeyUp is Any Key");
            return mUnityPlayer.injectEvent(event);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.e("UnityPlayerActivity", "onKeyDown is Back Key");
            return super.onKeyDown(keyCode, event);
        } else {
            Log.e("UnityPlayerActivity", "onKeyDown is Any Key");
            return mUnityPlayer.injectEvent(event);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    /*API12*/
    public boolean onGenericMotionEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    @Override
    public void onBackPressed() {
        Log.e("UnityPlayerActivity", "onBackPressed");
        if (isProductMode)
            super.onBackPressed();
    }

    public static void ShowToast(Activity activity, String msg) {
        Log.d(activity.getClass().getName(), "ShowToast : " + msg);
        String message = msg + "(by ShowToast)";
        Log.d(activity.getClass().getName(), "CallEcco : " + message);
        AcePlugin.getInstance().SendMessage("CallEcco", "From " + activity.getClass().getName() + " - " + message);
        Log.d(activity.getClass().getName(), "ShowToast end");
    }

    // Unity -> Android 호출

    /**
     * 메인 액티비티 호출
     *
     * @param activity
     * @param mode
     */
    public static void CallMainActivity(Activity activity, String mode) {

        if (isProductMode) return;
        Log.v("From Unity", "메인 메뉴 호출");
        XcoloLog.d("CallMainActivity scene name : " + mode);
        Intent intent;
        if (mode.equals("HomeEditorScene")) {
            intent = new Intent(activity, HomeDesignActivity.class);
        } else {
            intent = new Intent(activity, MainActivity.class);
        }
        activity.startActivityForResult(intent, 3000);
    }

    /**
     * 튜토리얼 화면 호출
     *
     * @param sceneName
     */
    public static void CallTutorial(Activity activity, String sceneName) {
        if ("ARMattress".equals(sceneName)) {
            //매트리스 분석 튜토리얼
            Toast.makeText(activity.getApplicationContext(), "매트리스 분석 튜토리얼", Toast.LENGTH_LONG).show();
            ACETutorialDialog.newInstance(activity).setMattressTutorialData().show();
        } else if ("HomeEditor".equals(sceneName)) {
            //홈에디터 튜토리얼
            Log.v("From Unity", "홈에디터 튜토리얼");
            Toast.makeText(activity.getApplicationContext(), "홈에디터 튜토리얼", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 홈에디터 - 도면 불러오기 완료
     */
    public static void CallbackLoadUnitComplete(Activity activity, String msg) {
        Log.v("From Unity", "도면 로딩 완료");
        Toast.makeText(activity.getApplicationContext(), "도면 로딩 완료", Toast.LENGTH_LONG).show();
    }

    /**
     * 홈에디터 - 카탈로그 팝업
     */
    public static void CallCatalog(Activity activity, String msg) {
        Log.v("From Unity", "카탈로그 열기");
//        Toast.makeText(activity.getApplicationContext(), "카탈로그 열기", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(activity, HomeCatalogActivity.class);
        activity.startActivity(intent);

    }

    /**
     * 홈에디터 - 제품 불러오기 완료
     */
    public static void CallbackLoadProductComplete(Activity activity, String msg) {
        Log.v("From Unity", "제품 로딩 완료");
        Toast.makeText(activity.getApplicationContext(), "제품 로딩 완료", Toast.LENGTH_LONG).show();
    }

    /**
     * 홈에디터 - 제품(가구) 불러오기 완료
     */
    public static void CallbackLoadFurnitureComplete(Activity activity, String msg) {
        Log.v("From Unity", "가구 로딩 완료");
        Toast.makeText(activity.getApplicationContext(), "가구 로딩 완료", Toast.LENGTH_LONG).show();
    }

    /**
     * 홈에디터 - 옵션화면 열기
     *
     * @param optionJson
     */
    public static void CallHomeOption(Activity activity, String optionJson) {
        Log.v("From Unity", "옵션 열기");
//        Toast.makeText(activity.getApplicationContext(), "개발 진행중입니다.", Toast.LENGTH_LONG).show();
        HomeEditorSettingDialog.newInstance(activity).setSettingValue(optionJson).show();
    }

    /**
     * 홈에디터 - 스크린샷 리턴
     *
     * @param imgPath
     */
    public static void CallScreenShot(Activity activity, String imgPath) {
        Log.v("From Unity", "스크린샷");
        mAceImageManager = new ACEImageManager(activity, imgPath);
        mAceImageManager.saveImageToGallery();
    }

    /**
     * 홈에디터 - 공유하기
     *
     * @param imgPath
     */
    public static void CallShare(Activity activity, String imgPath) {
        Log.v("From Unity", "홈에디터 공유하기");
        mAceImageManager = new ACEImageManager(activity, imgPath);
        mAceImageManager.shareImage();
    }

    /**
     * adbrix custom action용 유니티 이벤트 콜백
     */
    public static void CallBackStatic(Activity activity, String msg) {
        Log.v("From Unity", "adbrix event : "+msg);
    }

    public void hideProgressDlg() {

    }

    /**
     * ARActivity - UnityPlayerActivity 이동 과정에서 Unity Process 종료 에러가 날 때, Exception 가로채기
     * 작성자: Urbanbase Android 팀
     */
    private void setUpCrashHandler() {
        Thread.setDefaultUncaughtExceptionHandler(new AppExceptionHandler(getApplication()));
    }
}
