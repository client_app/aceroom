package com.acebed.aceroom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.acebed.aceroom.R;
import com.acebed.aceroom.ifaces.UserClickCallback;
import com.acebed.aceroom.model.catalog.WallFloorItem;
import com.acebed.aceroom.util.ACEUtil;

import java.util.ArrayList;

import kr.co.xcolo.android.fw.XcoloLog;

public class CatalogWallFloorAdapter extends RecyclerView.Adapter<CatalogWallFloorAdapter.CatalogWallFloorViewHolder> {

    private ArrayList<WallFloorItem> mWallFloorItems;
    private Context mContext;
    @Nullable
    private final UserClickCallback mUserClickCallback;

    public CatalogWallFloorAdapter(ArrayList<WallFloorItem> list, @Nullable UserClickCallback clickCallback) {
        this.mWallFloorItems = list;
        this.mUserClickCallback = clickCallback;
    }


    @Override
    public CatalogWallFloorViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_wallfloor, viewGroup, false);
        mContext = viewGroup.getContext();
        CatalogWallFloorViewHolder viewHolder = new CatalogWallFloorViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull CatalogWallFloorViewHolder viewholder, int position) {

        if(mWallFloorItems.get(position).getCatalogThumbnailImageUrl() != null) {
            XcoloLog.d("getCatalogThumbnailImageUrl : " + mWallFloorItems.get(position).getCatalogThumbnailImageUrl());
            ACEUtil.setImageFromUrl(mWallFloorItems.get(position).getCatalogThumbnailImageUrl(), viewholder.imageViewThumbnail);
        }


        viewholder.imageViewThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                XcoloLog.d("Item Click : " + mWallFloorItems.get(position).getName());
                mUserClickCallback.onClick(mWallFloorItems.get(position));

            }
        });
        viewholder.textViewName.setText(mWallFloorItems.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return (null != mWallFloorItems ? mWallFloorItems.size() : 0);
    }


    public class CatalogWallFloorViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewThumbnail;
        TextView textViewName;
        public CatalogWallFloorViewHolder(View view) {
            super(view);
            imageViewThumbnail = view.findViewById(R.id.item_imageView_thumbnail);
            textViewName = view.findViewById(R.id.item_textView_name);
        }
    }

}
