package com.acebed.aceroom.model.home;

import lombok.Data;

@Data
public class SettingValue {
    int effect;
    boolean collision;
}
