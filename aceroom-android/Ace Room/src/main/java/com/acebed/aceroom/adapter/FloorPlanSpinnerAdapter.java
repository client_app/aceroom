package com.acebed.aceroom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.acebed.aceroom.R;
import com.acebed.aceroom.model.home.DrawerItem;

import java.util.List;

public class FloorPlanSpinnerAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    List<DrawerItem> mDrawerItemList;

    public FloorPlanSpinnerAdapter(@NonNull Context context, List<DrawerItem>  drawerItems) {
        this.mDrawerItemList = drawerItems;
        this.mContext = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if(mDrawerItemList!=null) return mDrawerItemList.size();
        else return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.item_home_floor_plan, parent, false);
        }

        if(mDrawerItemList!=null){
            //데이터세팅
            String info = mDrawerItemList.get(position).getDisplaySupplyAreaPyeong() + "형 "
                    + mDrawerItemList.get(position).getExclusiveArea() + "㎡(전용)";
            ((TextView)convertView.findViewById(R.id.floorPlan_textView_info)).setText(info);
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflater.inflate(R.layout.item_home_floor_plan_dropdown, parent, false);
        }

        //데이터세팅
        String info = mDrawerItemList.get(position).getDisplaySupplyAreaPyeong() + "형 "
                + mDrawerItemList.get(position).getExclusiveArea() + "㎡(전용)";
        ((TextView)convertView.findViewById(R.id.floorPlan_textView_info)).setText(info);

        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return mDrawerItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}