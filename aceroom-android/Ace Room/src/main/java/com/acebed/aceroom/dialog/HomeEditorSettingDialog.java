package com.acebed.aceroom.dialog;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.acebed.aceroom.BuildConfig;
import com.acebed.aceroom.R;
import com.acebed.aceroom.model.home.SettingValue;
import com.acebed.aceroom.plugin.AcePlugin;
import com.acebed.aceroom.widget.ACECommonDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import kr.co.xcolo.android.fw.XcoloLog;

public class HomeEditorSettingDialog extends ACECommonDialog {
    private static HomeEditorSettingDialog mHomeEditorSettingDialog = null;

    public synchronized static HomeEditorSettingDialog newInstance(Activity activity) {
        mHomeEditorSettingDialog = new HomeEditorSettingDialog(activity);
        return mHomeEditorSettingDialog;
    }

    ViewMapper mMapper;

    private HomeEditorSettingDialog(Activity activity) {
        super(activity);
    }

    @Override
    protected View inflateContentsView() {
        View settingView = getActivity().getLayoutInflater().inflate(R.layout.layout_home_editor_setting, null, false);
        mMapper = new ViewMapper(settingView);
        mMapper.imageButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingValue settingValue = new SettingValue();
                if (mMapper.radioButtonLow.isChecked()) {
                    settingValue.setEffect(0);
                } else if (mMapper.radioButtonMedium.isChecked()) {
                    settingValue.setEffect(1);
                } else if (mMapper.radioButtonHigh.isChecked()) {
                    settingValue.setEffect(2);
                }
                settingValue.setCollision(mMapper.checkBoxWallCrush.isChecked());
                String settingValueString = new GsonBuilder().serializeNulls().create().toJson(settingValue);
                hide();
            }
        });
        mMapper.radioGroupQualityArea.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.setting_radioButton_high:
                        AcePlugin.getInstance().LoadHomeOptionEffect("2");
                        break;
                    case R.id.setting_radioButton_medium:
                        AcePlugin.getInstance().LoadHomeOptionEffect("1");
                        break;
                    case R.id.setting_radioButton_low:
                        AcePlugin.getInstance().LoadHomeOptionEffect("0");
                        break;
                }
            }
        });

        mMapper.checkBoxWallCrush.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                AcePlugin.getInstance().LoadHomeOptionCollision(b);
            }
        });

        mMapper.imageButtonGoToAce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://acebedmall.co.kr/front/viewMain.do";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                getActivity().startActivity(intent);
            }
        });

        mMapper.textViewAppVersion.setText(BuildConfig.VERSION_NAME);
        return settingView;
    }

    @Override
    protected void showDialog() {
        if (getActivity() instanceof Activity && ((Activity) getActivity()).isFinishing() == false) {
            getDialog().show();
        }
    }

    public HomeEditorSettingDialog setSettingValue(String postValue) {
        XcoloLog.d("setSettingValue postValue : " + postValue);
        SettingValue settingValue = new Gson().fromJson(postValue, SettingValue.class);
        XcoloLog.d("setSettingValue settingValue : " + settingValue);
        switch (settingValue.getEffect()) {
            case 0:
                mMapper.radioButtonLow.setChecked(true);
                break;
            case 1:
                mMapper.radioButtonMedium.setChecked(true);
                break;
            case 2:
                mMapper.radioButtonHigh.setChecked(true);
                break;
        }
        mMapper.checkBoxWallCrush.setChecked(settingValue.isCollision());
        return this;

    }

    protected class ViewMapper {
        // ImageButton
        ImageButton imageButtonCancel;
        ImageButton imageButtonGoToAce;

        //CheckBox
        RadioGroup radioGroupQualityArea;
        RadioButton radioButtonHigh;
        RadioButton radioButtonMedium;
        RadioButton radioButtonLow;
        CheckBox checkBoxWallCrush;

        //TextView
        TextView textViewAppVersion;

        public ViewMapper(View view) {
            // ImageButton
            imageButtonCancel = view.findViewById(R.id.setting_imageButton_cancel);
            imageButtonGoToAce = view.findViewById(R.id.setting_imageButton_goToAce);

            //radioButton
            radioGroupQualityArea = view.findViewById(R.id.setting_layout_qualityArea);
            radioButtonHigh = view.findViewById(R.id.setting_radioButton_high);
            radioButtonMedium = view.findViewById(R.id.setting_radioButton_medium);
            radioButtonLow = view.findViewById(R.id.setting_radioButton_low);
            checkBoxWallCrush = view.findViewById(R.id.setting_checkBox_wallCrush);

            //TextView
            textViewAppVersion = view.findViewById(R.id.setting_textView_appVersion);

        }
    }
}
