package com.acebed.aceroom.view.catalog;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.acebed.aceroom.R;
import com.acebed.aceroom.adapter.CatalogSpinnerAdapter;
import com.acebed.aceroom.application.ACEApplication;
import com.acebed.aceroom.databinding.ActivityHomeCatalogBinding;
import com.acebed.aceroom.environment.ACEEnvironment;
import com.acebed.aceroom.view.catalog.fragment.FurnitureFragment;
import com.acebed.aceroom.view.catalog.fragment.ProductFragment;
import com.acebed.aceroom.view.catalog.fragment.FlooringFragment;
import com.acebed.aceroom.view.catalog.fragment.WallpaperFragment;
import com.acebed.aceroom.model.catalog.CatalogOptionItem;
import com.acebed.aceroom.util.ACEUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.ui.XcoloCommonAcvitity;
/**
 * HomeCatalogActivity
 * 카탈로그 화면
 * @Author Xcolo DK
 */
public class HomeCatalogActivity extends XcoloCommonAcvitity {

    private ActivityHomeCatalogBinding mBinding;
    private String mBaseUrlOfCatalog = ACEEnvironment.BASE_URL_OF_CATALOG;
    CatalogSpinnerAdapter mCatalogSpinnerAdapter;
    HashMap<Integer, List<CatalogOptionItem>> mOptionMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ACEUtil.slideActivityFromLeftToRight(this);
        ACEApplication.getInstance().changeBaseUrl(mBaseUrlOfCatalog);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home_catalog);

        final HomeCatalogViewModel model = ViewModelProviders.of(this).get(HomeCatalogViewModel.class);
        mBinding.setHomeCatalogViewModel(model);
        mBinding.setLifecycleOwner(this);

        setOptionMap();
        setCategorySpinner(mOptionMap.get(mBinding.catalogRadioGroupCategory.getCheckedRadioButtonId()));
        // 최초 화면 세팅
        setFragment(mBinding.catalogRadioGroupCategory.getCheckedRadioButtonId(), ACEEnvironment.CATEGORY_TOTAL);

        mBinding.catalogImageButtonExitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                ACEUtil.slideActivityFromRightToLeft(HomeCatalogActivity.this);
            }
        });

        mBinding.catalogRadioGroupCategory.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                setCategorySpinner(mOptionMap.get(radioGroup.getCheckedRadioButtonId()));
                setFragment(radioGroup.getCheckedRadioButtonId(), ACEEnvironment.CATEGORY_TOTAL);
            }
        });
    }

    private void setFragment(int buttonId, String filter) {
        if (mBinding.catalogRadioButtonBed.getId() == buttonId) {
            XcoloLog.d("setFragment Bed");
            getSupportFragmentManager().beginTransaction().replace(R.id.catalog_layout_itemList, ProductFragment.newInstance(filter, "")).commit();
        } else if (mBinding.catalogRadioButtonWall.getId() == buttonId) {
            XcoloLog.d("setFragment Wall");
            getSupportFragmentManager().beginTransaction().replace(R.id.catalog_layout_itemList, WallpaperFragment.newInstance(filter, "")).commit();
        } else if (mBinding.catalogRadioButtonFloor.getId() == buttonId) {
            XcoloLog.d("setFragment Floor");
            getSupportFragmentManager().beginTransaction().replace(R.id.catalog_layout_itemList, FlooringFragment.newInstance(filter, "")).commit();
        } else if (mBinding.catalogRadioButtonFurniture.getId() == buttonId) {
            XcoloLog.d("setFragment Floor");
            getSupportFragmentManager().beginTransaction().replace(R.id.catalog_layout_itemList, FurnitureFragment.newInstance(filter, "")).commit();
        }
    }

    // 카테고리 구분 스피너 세팅
    private void setCategorySpinner(List<CatalogOptionItem> optionList) {
        mCatalogSpinnerAdapter = new CatalogSpinnerAdapter(HomeCatalogActivity.this.getBaseContext(), optionList);
        mBinding.catalogSpinnerCategory.setAdapter(mCatalogSpinnerAdapter);
        mBinding.catalogSpinnerCategory.setDropDownVerticalOffset(ACEUtil.dpToPx(35.7));
        mBinding.catalogSpinnerCategory.setArrowViewId(R.id.productSize_imageView_arrow);
        mBinding.catalogSpinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                XcoloLog.d("onItemSelected");
                setFragment(mBinding.catalogRadioGroupCategory.getCheckedRadioButtonId(), optionList.get(adapterView.getSelectedItemPosition()).getFilter());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    // 침대 구분값 세팅
    private void setOptionMap() {
        mOptionMap = new HashMap<Integer, List<CatalogOptionItem>>();
        //test Case
        List<CatalogOptionItem> productOptionList = new ArrayList<>();
        productOptionList.add(new CatalogOptionItem("전체", "TOTAL"));
        productOptionList.add(new CatalogOptionItem("패밀리침대", "Family"));
        productOptionList.add(new CatalogOptionItem("신혼침대", "Wedding"));
        productOptionList.add(new CatalogOptionItem("슈퍼싱글침대", "SuperSingle"));
        mOptionMap.put(mBinding.catalogRadioButtonBed.getId(), productOptionList);

        List<CatalogOptionItem> wallpaperOptionList = new ArrayList<>();
        wallpaperOptionList.add(new CatalogOptionItem("전체", "TOTAL"));
        wallpaperOptionList.add(new CatalogOptionItem("Plain Paper", "Plain paper"));
        wallpaperOptionList.add(new CatalogOptionItem("Fabric", "Fabric"));
        wallpaperOptionList.add(new CatalogOptionItem("Tile", "Tile"));
        mOptionMap.put(mBinding.catalogRadioButtonWall.getId(), wallpaperOptionList);

        List<CatalogOptionItem> flooringOptionList = new ArrayList<>();
        flooringOptionList.add(new CatalogOptionItem("전체", "TOTAL"));
        flooringOptionList.add(new CatalogOptionItem("Wood", "Wood"));
        flooringOptionList.add(new CatalogOptionItem("Tile", "Tile"));
        flooringOptionList.add(new CatalogOptionItem("Concrete", "Concrete"));
        mOptionMap.put(mBinding.catalogRadioButtonFloor.getId(), flooringOptionList);

        List<CatalogOptionItem> furnitureOptionList = new ArrayList<>();
        furnitureOptionList.add(new CatalogOptionItem("전체", "TOTAL"));
        furnitureOptionList.add(new CatalogOptionItem("룸세트", "roomset"));
//        flooringOptionList.add(new CatalogOptionItem("리빙가구", "living"));
        mOptionMap.put(mBinding.catalogRadioButtonFurniture.getId(), furnitureOptionList);

    }

    @Override
    public void refresh() {

    }
}
