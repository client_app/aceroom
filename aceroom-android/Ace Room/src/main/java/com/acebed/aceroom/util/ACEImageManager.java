package com.acebed.aceroom.util;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.acebed.aceroom.environment.ACEEnvironment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import kr.co.xcolo.android.fw.XcoloLog;

import static androidx.core.content.FileProvider.getUriForFile;
/**
 * ACEImageManager
 * 이미지 캡쳐 Util
 * @Author Xcolo DK
 */
public class ACEImageManager {
    private Activity mActivity = null;
    private Bitmap mImageBitmap = null;
    private String mCurrentCalledFunction = "";
    private String FUNCTION_NAME_SAVE = "SAVE";
    private String FUNCTION_NAME_SHARE = "SHARE";

    public ACEImageManager(Activity activity, Bitmap bitmap) {
        this.mActivity = activity;
        this.mImageBitmap = bitmap;
    }

    public ACEImageManager(Activity activity, String imagePath) {
        this.mActivity = activity;
        File imageFile = new File(imagePath);

        Bitmap bitmap = null;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);

        try {
            FileOutputStream stream = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.mImageBitmap = bitmap;
    }

    // Image 갤러리 저장
    public void saveImageToGallery() {

        if (mImageBitmap == null || mActivity == null) {
            Toast.makeText(mActivity, "스크린샷 오류.", Toast.LENGTH_LONG).show();
            return;
        }
        mCurrentCalledFunction = FUNCTION_NAME_SAVE;
        if (!checkPermission(mActivity)) {
            return;
        }
        String fileName = ACEUtil.dateName(System.currentTimeMillis());
        MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), mImageBitmap, fileName, null);
        Toast.makeText(mActivity, "저장", Toast.LENGTH_LONG).show();
    }

    // Image 공유
    public void shareImage() {
        if (mImageBitmap == null || mActivity == null) {
            Toast.makeText(mActivity, "스크린샷 오류.", Toast.LENGTH_LONG).show();
            return;
        }
        mCurrentCalledFunction = FUNCTION_NAME_SHARE;
        if (!checkPermission(mActivity)) {
            return;
        }
        String fileName = ACEUtil.dateName(System.currentTimeMillis());
        Uri uri = null;
        File file = new File(mActivity.getBaseContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName + ".jpg");
        uri = getUriForFile(mActivity, "com.acebed.aceroom.fileprovider", file);
        try {
            FileOutputStream stream = new FileOutputStream(file);
            mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.close();
            uri = getUriForFile(mActivity, "com.acebed.aceroom.fileprovider", file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), mImageBitmap, fileName, null);
        Toast.makeText(mActivity, "저장", Toast.LENGTH_LONG).show();

        XcoloLog.d("#############Uri : " + uri);
        //공유하기 개발중
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        mActivity.startActivityForResult(Intent.createChooser(shareIntent, "이미지 공유하기"), 1);
    }

    public boolean checkPermission(Activity activity) {
        //현재 안드로이드 버전이 6.0미만이면 메서드를 종료한다.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            activity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    ACEEnvironment.STORAGE_REQUEST_CODE);
            return false;
        } else {
            // 다음 부분은 항상 허용일 경우에 해당이 됩니다.
            XcoloLog.d("success");
            return true;
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ACEEnvironment.STORAGE_REQUEST_CODE) {
            for (int i = 0; i < grantResults.length; i++) {
                //허용됐다면
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    XcoloLog.d("권한 허용");
                } else {
                    Toast.makeText(mActivity.getApplicationContext(), "앱 권한 설정이 필요합니다.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
            if (mCurrentCalledFunction.equals(FUNCTION_NAME_SHARE)) {
                XcoloLog.d("FUNCTION_NAME_SHARE");
                shareImage();
            } else if (mCurrentCalledFunction.equals(FUNCTION_NAME_SAVE)) {
                XcoloLog.d("FUNCTION_NAME_SAVE");
                saveImageToGallery();
            } else {
                Toast.makeText(mActivity.getApplicationContext(), "오류.", Toast.LENGTH_LONG).show();
                return;
            }
        }
    }
}
