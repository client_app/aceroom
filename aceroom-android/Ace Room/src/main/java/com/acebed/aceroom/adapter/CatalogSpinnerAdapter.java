package com.acebed.aceroom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.acebed.aceroom.R;
import com.acebed.aceroom.model.catalog.CatalogOptionItem;

import java.util.List;

public class CatalogSpinnerAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    List<CatalogOptionItem> mSizeList;
    List<String> mOptionDataSizeList;
    boolean isOption = false;

    public CatalogSpinnerAdapter(@NonNull Context context, List<CatalogOptionItem> sizeList) {
        this.mSizeList = sizeList;
        this.mContext = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isOption = false;
    }

    public CatalogSpinnerAdapter(@NonNull Context context, List<String> optionSizeList, boolean option) {
        this.mOptionDataSizeList = optionSizeList;
        this.mContext = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isOption = option;
    }

    @Override
    public int getCount() {
        if(isOption) {
            if(mOptionDataSizeList!=null) return mOptionDataSizeList.size();
            else return 0;
        } else {
            if(mSizeList!=null) return mSizeList.size();
            else return 0;
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.item_product_size, parent, false);
        }

        if(isOption) {
            if(mOptionDataSizeList!=null){
                //데이터세팅
                String name = mOptionDataSizeList.get(position);
                ((TextView)convertView.findViewById(R.id.productSize_textView_size)).setText(name);
            }
        } else {
            if(mSizeList!=null){
                //데이터세팅
                String name = mSizeList.get(position).getName();
                ((TextView)convertView.findViewById(R.id.productSize_textView_size)).setText(name);
            }
        }

        convertView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                view.findViewById(R.id.productSize_imageView_arrow).setBackgroundResource(R.drawable.up_icon);
            }
        });
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflater.inflate(R.layout.item_product_size_dropdown, parent, false);
        }

        if(isOption) {
            if(mOptionDataSizeList!=null){
                //데이터세팅
                String name = mOptionDataSizeList.get(position);
                ((TextView)convertView.findViewById(R.id.productSize_textView_size)).setText(name);
            }
        } else {
            if(mSizeList!=null){
                //데이터세팅
                String name = mSizeList.get(position).getName();
                ((TextView)convertView.findViewById(R.id.productSize_textView_size)).setText(name);
            }
        }
        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return mSizeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}