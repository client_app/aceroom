package com.acebed.aceroom.view.catalog.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.acebed.aceroom.R;
import com.acebed.aceroom.adapter.CatalogProductAdapter;
import com.acebed.aceroom.api.productlist.ProductListResponse;
import com.acebed.aceroom.databinding.FragmentBedBinding;
import com.acebed.aceroom.environment.ACEEnvironment;
import com.acebed.aceroom.ifaces.UserClickCallback;
import com.acebed.aceroom.model.catalog.ProductItem;
import com.acebed.aceroom.view.catalog.product.ProductDetailActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kr.co.xcolo.android.fw.mvvm.ui.XcoloCommonFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
/**
 * ProductFragment
 * 제품 카탈로그 리스트
 * @Author Xcolo DK
 */
public class ProductFragment extends XcoloCommonFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private FragmentBedBinding mBinding;
    private ArrayList<ProductItem> mProductArrayList;
    private CatalogProductAdapter mAdapter;
    ProductViewModel mViewModel;
    List<ProductListResponse.ProductsData> mProductsListData;
    List<ProductListResponse.MattressListData> mMattressListData = new ArrayList<>();

    // TODO: Rename and change types of parameters
    private String mFilter = ""; // 필터값
    private String mParam2 = "";

    public ProductFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProductFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductFragment newInstance(String param1, String param2) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mFilter = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_bed, container, false);
//        initViews(userInfoView);
        mBinding.setLifecycleOwner(getViewLifecycleOwner());
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        mBinding.setProductViewModel(mViewModel);

        mBinding.bedImageButtonToScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mGridLayoutManager.scrollToPositionWithOffset(0,0);
                mBinding.bedRecyclerViewList.scrollTop();
                mBinding.bedImageButtonToScrollTop.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.bedRecyclerViewList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(-1)) {
                    mBinding.bedImageButtonToScrollTop.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && mBinding.bedImageButtonToScrollTop.getVisibility() == View.INVISIBLE)
                    mBinding.bedImageButtonToScrollTop.setVisibility(View.VISIBLE);
            }
        });

        setProductItems(mFilter);
        setViewModel(mViewModel);
    }


    private void setProductItems(String filter) {
        mProductArrayList = new ArrayList<>();
        mViewModel.requestProducts().observe(this, productListBasePathResponse -> {
            mMattressListData = productListBasePathResponse.getMattressList();
            for (ProductListResponse.ProductsData item : productListBasePathResponse.getProducts()) {
                mProductArrayList.add(new ProductItem(item.getCatalogName(),item.getCatalogThumbnail(), item.getFilter(), item));
            }
            if (filter.equals(ACEEnvironment.CATEGORY_TOTAL)) {
                mAdapter = new CatalogProductAdapter(mProductArrayList, mUserClickCallback);
            } else {
                ArrayList<ProductItem> selectedArrayList = new ArrayList<>();
                for (ProductItem productItem : mProductArrayList) {
                    if (productItem.getSize().equals(filter)) {
                        selectedArrayList.add(productItem);
                    }
                }
                mAdapter = new CatalogProductAdapter(selectedArrayList, mUserClickCallback);
            }
            mBinding.bedRecyclerViewList.setAdapter(mAdapter);
        });
    }

    private final UserClickCallback mUserClickCallback = new UserClickCallback() {
        @Override
        public void onClick(Object productItem) {
            ProductItem item = (ProductItem) productItem;
            Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
            intent.putExtra("isPageMode", ACEEnvironment.PAGE_MODE_PRODUCT);
            intent.putExtra("productName", item.getName());
            intent.putExtra("productsData", (Serializable) item.getProductsData());
            intent.putExtra("mattressList", (Serializable) mMattressListData);
            getActivity().startActivity(intent);
        }
    };

    @Override
    public void refresh() {

    }
}
