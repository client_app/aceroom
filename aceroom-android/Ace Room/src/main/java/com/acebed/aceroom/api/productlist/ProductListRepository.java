package com.acebed.aceroom.api.productlist;

import androidx.lifecycle.LiveData;

import com.acebed.aceroom.api.HomeApi;

import kr.co.xcolo.android.fw.mvvm.retrofit.ApiService;
import kr.co.xcolo.android.fw.mvvm.retrofit.GenericRetrofitRepository;
import kr.co.xcolo.android.fw.mvvm.status.XcoloStatusCallback;
import retrofit2.Call;

public class ProductListRepository extends GenericRetrofitRepository<ProductListResponse> {
    private HomeApi mHomeService = ApiService.createService(HomeApi.class);

    public static ProductListRepository createInstance(XcoloStatusCallback statusCallback) {
        ProductListRepository repository = new ProductListRepository();

        repository.setStatusCallback(statusCallback);

        return repository;
    }

    public LiveData<ProductListResponse> onProductsRequest() {
        return doRequest();
    }

    @Override
    protected Call<ProductListResponse> makeRequest() {
        return mHomeService.products();
    }
}
