package com.acebed.aceroom.api.wallfloor;

import androidx.lifecycle.LiveData;

import com.acebed.aceroom.api.HomeApi;

import kr.co.xcolo.android.fw.mvvm.retrofit.ApiService;
import kr.co.xcolo.android.fw.mvvm.retrofit.GenericRetrofitRepository;
import kr.co.xcolo.android.fw.mvvm.status.XcoloStatusCallback;
import retrofit2.Call;

public class WallFloorRepository extends GenericRetrofitRepository<WallFloorResponse> {
    private HomeApi mHomeService = ApiService.createService(HomeApi.class);

    public static WallFloorRepository createInstance(XcoloStatusCallback statusCallback) {
        WallFloorRepository repository = new WallFloorRepository();

        repository.setStatusCallback(statusCallback);

        return repository;
    }

    public LiveData<WallFloorResponse> onWallFloorRequest() {
        return doRequest();
    }

    @Override
    protected Call<WallFloorResponse> makeRequest() {
        return mHomeService.wallFloor();
    }
}
