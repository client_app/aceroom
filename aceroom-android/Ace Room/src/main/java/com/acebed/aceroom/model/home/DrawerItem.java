package com.acebed.aceroom.model.home;

import com.acebed.aceroom.api.buildings.BuildingDetailResponse;
import com.acebed.aceroom.api.buildings.BuildingsResponse;

import lombok.Data;

/*
 마커 선택시 나오는 Drawer Item
 */
@Data
public class DrawerItem {
    private String displaySupplyAreaPyeong;
    private Double exclusiveArea;
    private String id;
    private int bathroomCount;
    private int roomCount;
    private String topViewImageFullPath;
    private String floorPlanImagePath;
    private String floorPlanMetadataPath;
    private BuildingDetailResponse.UnitAttachesData unitAttaches;

    public DrawerItem(String displaySupplyAreaPyeong, Double exclusiveArea, String id, int bathroomCount, int roomCount, String topViewImageFullPath, BuildingDetailResponse.UnitAttachesData unitAttaches) {
        this.displaySupplyAreaPyeong = displaySupplyAreaPyeong;
        this.exclusiveArea = exclusiveArea;
        this.id = id;
        this.bathroomCount = bathroomCount;
        this.roomCount = roomCount;
        this.topViewImageFullPath = topViewImageFullPath;
        this.unitAttaches = unitAttaches;
    }
}
