package com.acebed.aceroom.widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import kr.co.xcolo.android.fw.XcoloLog;


/**
 * ACECommonDialog
 * @Author Xcolo DK
 */

public abstract class ACECommonDialog {
    protected abstract View inflateContentsView();

    protected abstract void showDialog();

    private Activity mActivity = null;
    private Dialog mDialog = null;
    private View mContentsView = null;

    protected ACECommonDialog(Activity activity) {
        mActivity = activity;
        mContentsView = inflateContentsView();
    }

    protected View getContentsView() {
        return mContentsView;
    }

    protected Activity getActivity() {
        return mActivity;
    }

    protected Dialog getDialog() {
        return mDialog;
    }

    public synchronized void show() {

        if (mDialog == null) {
            mDialog = new Dialog(mActivity);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mDialog.setCancelable(false);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.addContentView(
                    mContentsView,
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        if (mActivity instanceof Activity && ((Activity) mActivity).isFinishing() == false) {
            mDialog.show();
        }
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.dimAmount = (float) 0.8;
        mDialog.getWindow().setAttributes(lp);

        showDialog();
    }

    protected boolean isAliveDialog() {
        return mActivity != null;
    }

    final synchronized public void hide() {
        XcoloLog.d("hide");
        if (mDialog != null) {
            if (mDialog.isShowing() == true) {
                mDialog.dismiss();
            }
        }
        mDialog = null;
    }

    final synchronized public void cancel() {
        XcoloLog.d("cancel");
        if (mDialog != null) {
            if (mDialog.isShowing() == true) {
                mDialog.dismiss();
            }
        }
        mDialog = null;
    }

}
