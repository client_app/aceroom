package com.acebed.aceroom.api.buildings;

import androidx.lifecycle.LiveData;

import com.acebed.aceroom.api.HomeApi;

import kr.co.xcolo.android.fw.mvvm.retrofit.ApiService;
import kr.co.xcolo.android.fw.mvvm.retrofit.GenericRetrofitRepository;
import kr.co.xcolo.android.fw.mvvm.status.XcoloStatusCallback;
import retrofit2.Call;
// 건물 상세정보 조회
public class BuildingDetailRepository extends GenericRetrofitRepository<BuildingDetailResponse> {
    private HomeApi mHomeService = ApiService.createService(HomeApi.class);
    private String mBuildingUuid;

    public static BuildingDetailRepository createInstance(String buildingUuid, XcoloStatusCallback statusCallback) {
        BuildingDetailRepository repository = new BuildingDetailRepository();

        repository.mBuildingUuid = buildingUuid;
        repository.setStatusCallback(statusCallback);

        return repository;
    }

    public LiveData<BuildingDetailResponse> onBuildingsRequest() {
        return doRequest();
    }

    @Override
    protected Call<BuildingDetailResponse> makeRequest() {
        return mHomeService.buildingDetail(mBuildingUuid, "[\"id\",\"building_name\",\"address\",\"building_uuid\"]" ,"{\"country\":{},\"building_areas\":{\"include\":{\"units\":{\"include\":{\"unit_attaches\":{}}}}}}");
    }
}
