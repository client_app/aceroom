package com.acebed.aceroom.view.ar;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.acebed.aceroom.R;
import com.acebed.aceroom.application.ACEApplication;
import com.acebed.aceroom.databinding.ActivityArBinding;
import com.acebed.aceroom.dialog.ACETutorialDialog;
import com.acebed.aceroom.model.ar.AceProductData;
import com.acebed.aceroom.model.ar.Const;
import com.acebed.aceroom.model.ar.EventData;
import com.acebed.aceroom.model.ar.FrameObjectData;
import com.acebed.aceroom.model.ar.FurnitureObjectData;
import com.acebed.aceroom.model.ar.MattressObjectData;
import com.acebed.aceroom.model.ar.ProductObjectData;
import com.acebed.aceroom.plugin.AcePlugin;
import com.acebed.aceroom.util.ACEImageManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.urbanbase.sdk.arviewer.api.Api;
import com.urbanbase.sdk.arviewer.helper.UBEnums;
import com.urbanbase.sdk.arviewer.model.ARViewer;
import com.urbanbase.sdk.arviewer.model.Figure;
import com.urbanbase.sdk.arviewer.ui.UBARActivity;

import com.unity3d.player.UnityPlayer;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ARActivity extends UBARActivity implements ARViewer.ARViewerListener, Figure.onFigureStateListener {

    public static String ItemTypeProduct = "product";
    public static int ItemTypeProductCode = 0;    // Frame&Mattress 인 경우 0
    public static String ItemTypeRoomSet = "roomSet";
    public static int ItemTypeRoomSetCode = 1;    // Room Set 인 경우 1

    private AceProductData mAceProductData = null;
    private boolean mIsFloating = false;
    private boolean mIsInfinityMode = false;
    ACEImageManager mAceImageManager = null;

    private FrameLayout mContainer;
    private UnityPlayer unityPlayer;



    private ActivityArBinding binding;
    private ProductObjectData selectedProduct = null;


    private View.OnTouchListener onDisallowInterceptTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            ((ViewGroup) v).requestDisallowInterceptTouchEvent(true);
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("ARActivity", "onCreate");

        binding = DataBindingUtil.setContentView(this, R.layout.activity_ar);
//        setContentView(R.layout.activity_ar);

        // 권한 체크

        // ARViewer 초기화
        // aar 빌드 별 초기화 수정 사항
        // infinity mode ARViewer 적용 시 하기 플래그를 true로 설정
        // floating mode ARViewer 적용 시 하기 플래그 false로 설정
        mIsInfinityMode = true; // 초기 값은 infinity mode로 적용
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Log.e("ARViewer", "Permission is Granted");
            initARViewer();
        }
        initGoToExternalWebListener();
    }

    private void initGoToExternalWebListener(){
        binding.goToExternalWeb.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri landingUri = Uri.parse(selectedProduct.getLandingTarget());
            intent.setData(landingUri);

            startActivity(intent);
        });
    }

    private void visibleGoToExternalWeb(){
        binding.goToExternalWeb.setVisibility(View.VISIBLE);
    }

    private void goneGoToExternalWeb(){
        binding.goToExternalWeb.setVisibility(View.GONE);
    }

    private void updateSelectedProduct(ProductObjectData selectedProduct){
        this.selectedProduct = selectedProduct;

        if (selectedProduct == null){
            goneGoToExternalWeb();
            return;
        }

        if (selectedProduct.getLandingTarget().isEmpty())
            goneGoToExternalWeb();
        else
            visibleGoToExternalWeb();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String frameName = intent.getStringExtra(EXTRA_FRAME_NAME);
        String size = intent.getStringExtra(EXTRA_KEY_SIZE);
        String color = intent.getStringExtra(EXTRA_KEY_COLOR);
        String mattressName = intent.getStringExtra(EXTRA_MATTRESS_NAME);

        if (frameName == null || frameName.isEmpty())
            updateSelectedProduct(null);

        Log.e("ARActivity", "onNewIntent() frameName : " + frameName + ", size : " + size + ", color : " + color + ", mattressName : " + mattressName);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("ARActivity", "onResume");
        if(getArViewer() == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Log.e("ARViewer", "Permission is Granted");
                initARViewer();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.e("ARActivity", "onStart");
    }

    // Device Home Btn Event 예외처리
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        ACEApplication.getInstance().leavedActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.e("ARActivity", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.e("ARActivity", "onStop");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.e("ARActivity", "onConfigurationChanged . orientation : " + newConfig.orientation);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("ARActivity", "onRequestPermissionsResult. requestCode : " + requestCode + ", permissions : " + Arrays.toString(permissions) + ", grantResults : " + Arrays.toString(grantResults));

        if (requestCode == REQUEST_CODE_PREVIEW) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            if (getArViewer() == null) {
                initARViewer();
            }
        } else {
            Intent resultIntent = new Intent();
            resultIntent.putExtra("result", "LENS_BACK");
            setResult(RESULT_OK, resultIntent);

            finish();
        }
        if (mAceImageManager != null) {
            mAceImageManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onCloseClick() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("result", "LENS_BACK");
        setResult(RESULT_OK, resultIntent);

        finish();
    }

    @Override
    public void onInfoClick(Api.Item item) {

    }

    @Override
    public void onCaptureClick(Bitmap bitmap) {
        mAceImageManager = new ACEImageManager(this, bitmap);
        mAceImageManager.shareImage();
    }

    @Override
    public void onFiguresClick() {

    }

    @Override
    public void onFindPlane() {
        Log.e("ARActivity", "onFindPlane");
    }

    @Override
    public void onCloseARViewer() {

    }

    @Override
    public void onTutorialClick() {
        ACETutorialDialog.newInstance(this).setArTutorialData().show();
    }

    @Override
    public void onAddItemClick(String s, String t) {
        if (t != null && t.equals(ItemTypeProduct)) {   // Frame & Mattress 인 경우
            if (s != null && !s.isEmpty() && mAceProductData.getProducts() != null) {
                for (ProductObjectData productObjectData : mAceProductData.getProducts()) {
                    if (productObjectData.getName() != null && productObjectData.getName().equals(s)) {
                        updateSelectedProduct(productObjectData);
                        showProductPreviewActivity(productObjectData);
                        break;
                    }
                }
            }
        } else if (t != null && t.equals(ItemTypeRoomSet)) {    // Room Set 인 경우
            if (s != null && !s.isEmpty() && mAceProductData.getRoomsetList() != null) {
                for (ProductObjectData roomSetObjectData : mAceProductData.getRoomsetList()) {
                    if (roomSetObjectData.getName() != null && roomSetObjectData.getName().equals(s)) {
                        updateSelectedProduct(roomSetObjectData);
                        showRoomSetPreviewActivity(roomSetObjectData);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("result", "LENS_BACK");
        setResult(RESULT_OK, resultIntent);
        super.onBackPressed();
    }

    private void initARViewer() {
        ARViewer arViewer = new ARViewer(this, this, this, true, false,
                ARViewer.FIND_PLANE_MODE_TYPE.MODE_HORIZONTAL,
                UBEnums.VIEWER_MODE.MODE_ASSET);

        arViewer.createScene(R.id.ar_fragment);
        arViewer.changeVertexColor(255f, 255f, 255f);
        arViewer.setGuideMessage("바닥을 터치 해\n상품을 위치 시켜 주세요.");
        arViewer.setPlaneInfinity(true);
//        arViewer.getUiManager().changeGuideImage(null); // 가이드 이미지 비 노출

        // UBARActivity 에 객체 선언
        setArViewer(arViewer);

        // 아이템 팝업에 노출 될 정보 로드
        FetchItemsTask fetchItemsTask = new FetchItemsTask();
        fetchItemsTask.execute(productUrl);

        FetchEventsTask fetchEventsTask = new FetchEventsTask();
        fetchEventsTask.execute(eventUrl);

        if(!ACEApplication.getInstance().getPreferences("LENS")) {
            ACETutorialDialog.newInstance(this).setArTutorialData().show();
            ACEApplication.getInstance().savePreferences("LENS");
         }
    }

    private void showRoomSetPreviewActivity(ProductObjectData roomSetObjectData) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ARActivity.this, ARPreviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(Const.EXTRA_KEY_BASE_PATH, mAceProductData.getBasePath());
                intent.putExtra(Const.EXTRA_KEY_PREVIEW_MODE, true);
                intent.putExtra(Const.EXTRA_KEY_PRODUCT_TYPE, ItemTypeRoomSetCode);
                intent.putExtra(Const.EXTRA_KEY_ROOM_SET_INFO, roomSetObjectData);

                startActivity(intent);
//                showProductActivity(intent);
            }
        });
    }

    private void showProductPreviewActivity(ProductObjectData productObjectData) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ARActivity.this, ARPreviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(Const.EXTRA_KEY_BASE_PATH, mAceProductData.getBasePath());
                intent.putExtra(Const.EXTRA_KEY_PREVIEW_MODE, true);
                intent.putExtra(Const.EXTRA_KEY_PRODUCT_TYPE, ItemTypeProductCode);
                intent.putExtra(Const.EXTRA_KEY_PRODUCT_INFO, productObjectData);
                intent.putParcelableArrayListExtra(Const.EXTRA_KEY_MATTRESS_LIST_INFO, getMattressSizeGroup(productObjectData.getSizeList()));

                startActivity(intent);
//                showProductActivity(intent);
            }
        });
    }

    private ArrayList<MattressObjectData> getMattressSizeGroup(List<String> sizeList) {
        ArrayList<MattressObjectData> mattressObjectDataList = new ArrayList<>();
        if (mAceProductData.getMattressList() != null) {
            for (MattressObjectData mattressObjectData : mAceProductData.getMattressList()) {
                if (mattressObjectData.getSize() != null && sizeList.contains(mattressObjectData.getSize())) {
                    mattressObjectDataList.add(mattressObjectData);
                }
            }
        }

        return mattressObjectDataList;
    }

//    private String productUrl = "https://acebed-3d-assets-dev.s3.ap-northeast-2.amazonaws.com/json/productList_200214.json";
//    private String productUrl = "https://acebed-3d-assets-dev.s3.ap-northeast-2.amazonaws.com/json/productList_twin.json";
//    private String productUrl = "http://dtribe.youyoung.net/json/productList_200724.json"; // 테스트 url
//    private String productUrl = "http://dtribe.youyoung.net/json/productList.json"; // 운영
    private String productUrl = "http://dtribe.youyoung.net/json/productList_addLanding.json"; // 랜딩 테스트
    private String eventUrl = "http://dtribe.youyoung.net/json/event_ace.json";

    @Override
    public void onFigureSingleClick(Figure figure) {

    }

    @Override
    public void onFigureLongClick(Figure figure) {
        // infinity mode 가 아닌 경우에만 플로팅 모드 설정을 처리.
        if (!mIsInfinityMode) {
            mIsFloating = !mIsFloating;
            figure.setFloatingMode(mIsFloating);
        }
    }

    @Override
    public void onFigureDidPlaned(Figure figure) {

    }

    @Override
    public void onFigureRotateChanged(Figure figure) {

    }

    @Override
    public void onFigureScaleChanged(Figure figure) {

    }

    // Sub Tasks
    class FetchItemsTask extends AsyncTask<String, Void, AceProductData> {

        @Override
        protected AceProductData doInBackground(String... strings) {
            String url = strings[0];    //Api.GET_POST 를 저장
            OkHttpClient client = new OkHttpClient();

            // 2.1.0 버전부터는 obj 파일이 아닌 sfb 파일을 내려받음, 이를 위해 해더에 버전정보 입력 (버전정보 자동 파싱 구현 필요)
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Gson gson = new Gson();
                return gson.fromJson(response.body().string(), AceProductData.class);
            } catch (IOException e) {
                Log.d("FetchItemsTask", e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(AceProductData productData) {
            super.onPostExecute(productData);
            mAceProductData = productData;

            try {
                String basePath = productData.getBasePath();

                ArrayList<Api.Item> arItemList = new ArrayList<>();

                String name = "";
                String thumbPath = "";
                Api.ModelSFB modelSfb = null;
                for (ProductObjectData productObjData : productData.getProducts()) {
                    productObjData.setObjectType(ItemTypeProductCode);
                    name = productObjData.getCatalogName();
                    thumbPath = productObjData.getCatalogThumbnail();
                    modelSfb = new Api.ModelSFB();
                    for (FrameObjectData frameObjectData : productObjData.getFrameList()) {
                        modelSfb.addModelSFB(new Api.ModelSFB.SFBObject(
                                frameObjectData.getColor(),
                                frameObjectData.getSize(),
                                basePath + frameObjectData.getSfbPath(),
                                new Api.ModelSFB.MattressOffset(
                                        frameObjectData.getMattressOffset().getX(),
                                        frameObjectData.getMattressOffset().getY(),
                                        frameObjectData.getMattressOffset().getZ()
                                ),
                                frameObjectData.getMattressScale()
                        ));
                    }

                    arItemList.add(new Api.Item().makeItem(
                            name,
                            productObjData.getName(),
                            thumbPath,
                            ItemTypeProduct,
                            modelSfb)
                    );
                }

                ArrayList<Api.MattressItem> arMattressItemList = new ArrayList<>();
                for (MattressObjectData mattressObjectData : productData.getMattressList()) {
                    arMattressItemList.add(new Api.MattressItem(mattressObjectData.getMattressid(),
                            basePath + mattressObjectData.getSfbPath(),
                            mattressObjectData.getName(),
                            mattressObjectData.getSize()));
                }

                ArrayList<Api.Item> arRoomSetItemList = new ArrayList<>();
                for (ProductObjectData roomSetObjectData : productData.getRoomsetList()) {
                    roomSetObjectData.setObjectType(ItemTypeRoomSetCode);
                    name = roomSetObjectData.getCatalogName();
                    thumbPath = roomSetObjectData.getCatalogThumbnail();
                    modelSfb = new Api.ModelSFB();
                    for (FurnitureObjectData furnitureObjectData : roomSetObjectData.getFurnitureList()) {
                        modelSfb.addModelSFB(new Api.ModelSFB.SFBObject(
                                furnitureObjectData.getColor(),
                                null,
                                basePath + furnitureObjectData.getSfbPath(),
                                null,
                                1.0
                        ));
                    }

                    arRoomSetItemList.add(new Api.Item().makeItem(
                            name,
                            roomSetObjectData.getName(),
                            thumbPath,
                            ItemTypeRoomSet,
                            modelSfb
                    ));
                }

                if (getArViewer() != null) {
                    getArViewer().addItems(arItemList, arRoomSetItemList);
                    getArViewer().addMattressItems(arMattressItemList);
                }
            } catch (Exception e) {
                Log.d("test", e + "");
            }
        }
    }

    class FetchEventsTask extends AsyncTask<String, Void, List<EventData>> {

        @Override
        protected List<EventData> doInBackground(String... strings) {
            String url = strings[0];    //Api.GET_POST 를 저장
            OkHttpClient client = new OkHttpClient();

            // 2.1.0 버전부터는 obj 파일이 아닌 sfb 파일을 내려받음, 이를 위해 해더에 버전정보 입력 (버전정보 자동 파싱 구현 필요)
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                Gson gson = new Gson();
                Type type = new TypeToken<List<EventData>>() {
                }.getType();
                return gson.fromJson(response.body().string(), type);
            } catch (IOException e) {
                Log.d("FetchItemsTask", e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<EventData> items) {
            super.onPostExecute(items);
            try {
                ArrayList<Api.Event> arEventList = new ArrayList<>();
                for (EventData event : items) {
                    arEventList.add(new Api.Event(event.getName(), event.getTitle(), event.getImage(), event.getTargetUrl(), event.getType()));
                }

                if (getArViewer() != null) {
                    getArViewer().addEvents(arEventList);
                }
            } catch (Exception e) {
                Log.d("test", e + "");
            }
        }
    }

    public void RequestCallback(String value) {
        Log.e("MYTEST", "RequestCallback. value : " + value);

    }

    public void LoadProductCallback(String value) {
        Log.e("MYTEST", "LoadProductCallback. value : " + value);

    }
}
