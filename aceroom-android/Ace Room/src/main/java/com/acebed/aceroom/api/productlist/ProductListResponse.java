package com.acebed.aceroom.api.productlist;

import java.io.Serializable;
import java.util.List;

import kr.co.xcolo.android.fw.mvvm.retrofit.GenericRetrofitModel;
import lombok.Data;
/**
 * ProductListResponse
 * 제품정보 Response
 * @Author Xcolo DK
 */
@Data
public class ProductListResponse implements GenericRetrofitModel {
    private String brand;
    private String basePath;
    private List<ProductsData> products;
    private List<MattressListData> mattressList;
    private List<RoomsetListData> roomsetList;
    private String createdAt;
    private String version;
    @Override
    public boolean isError() {
        return false;
    }

    @Override
    public String getErrorMessage() {
        return null;
    }

    @Data
    public static class ProductsData implements Serializable{
        private String name;
        private String catalogThumbnail;
//        @SerializedName("descList") private List<DescList> descList ;
        private String catalogName;
        private List<String> sizeList ;
        private List<ColorListData> colorList ;
        private BaseOptData baseOpt;
        private String filter;
        private List<FrameListData> frameList ;
    }

    @Data
    public static class ColorListData implements Serializable {
        private String title;
        private String name;
        private String path;
        private String rgb;
    }

    @Data
    public static class BaseOptData implements Serializable {
        private String size;
        private String color;
        private String mattress;
        private String thumbnail;
    }

    @Data
    public static class FrameListData implements Serializable {
        private String frameid;
        private String size;
        private String color;
        private String thumbnail;
        private String path;
        private String sfbPath;
        private String daeiPath;
        private MattressOffsetData mattressOffset;
        private double mattressScale;
    }

    @Data
    public static class MattressOffsetData implements Serializable {
        double x;
        double y;
        double z;
    }

    @Data
    public static class MattressListData implements Serializable {
        private String mattressid;
        private String path;
        private String sfbPath;
        private String daeiPath;
        private String name;
        private String size;
    }

    @Data
    public static class RoomsetListData implements Serializable {
        private String name;
        private String catalogThumbnail;
        //@SerializedName("descList") private List<DescList> descList ;
        private List<ColorListData> colorList ;
        private BaseOptData baseOpt;
        private String filter;
        private List<FurnitureListData> furnitureList ;
    }
    @Data
    public static class FurnitureListData implements Serializable {
        private String furnitureId;
        private String color;
        private String thumbnail;
        private String path;
        private String sfbPath;
        private String daeiPath;
        private String isDisplay;
    }
}
