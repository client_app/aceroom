package com.acebed.aceroom.model;

public class MainMenuData {
    public int imageResource;
    public int selectedImageResource;
    public String imageDescriptionKo;
    public String imageDescriptionEN;

    public MainMenuData(int imageResource, int selectedImageResource, String imageDescriptionKo, String imageDescriptionEN) {
        this.imageResource = imageResource;
        this.selectedImageResource = selectedImageResource;
        this.imageDescriptionKo = imageDescriptionKo;
        this.imageDescriptionEN = imageDescriptionEN;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public int getSelectedImageResource() {
        return selectedImageResource;
    }

    public void setSelectedImageResource(int selectedImageResource) {
        this.selectedImageResource = selectedImageResource;
    }

    public String getImageDescriptionKo() {
        return imageDescriptionKo;
    }

    public void setImageDescriptionKo(String imageDescriptionKo) {
        this.imageDescriptionKo = imageDescriptionKo;
    }

    public String getImageDescriptionEN() {
        return imageDescriptionEN;
    }

    public void setImageDescriptionEN(String imageDescriptionEN) {
        this.imageDescriptionEN = imageDescriptionEN;
    }
}
