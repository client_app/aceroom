package com.acebed.aceroom.model.ar;

public class Const {
    public static final String EXTRA_KEY_PRODUCT_INFO = "productInfo";
    public static final String EXTRA_KEY_ROOM_SET_INFO = "roomSetInfo";
    public static final String EXTRA_KEY_MATTRESS_LIST_INFO = "mattressListInfo";
    public static final String EXTRA_KEY_PREVIEW_MODE = "previewMode";
    public static final String EXTRA_KEY_PRODUCT_TYPE = "productType";
    public static final String EXTRA_KEY_BASE_PATH = "basePath";
}
