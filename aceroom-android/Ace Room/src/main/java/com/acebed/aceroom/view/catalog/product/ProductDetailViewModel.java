package com.acebed.aceroom.view.catalog.product;

import android.app.Application;

import androidx.annotation.NonNull;

import com.acebed.aceroom.application.ACEApplication;

import kr.co.xcolo.android.fw.AppExecutors;
import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.viewmodel.XcoloCommonAndroidViewModel;

public class ProductDetailViewModel extends XcoloCommonAndroidViewModel {
    private boolean isError;
    private boolean isLoading;
    private AppExecutors mAppExecutors;


    public ProductDetailViewModel(@NonNull Application application) {
        super(application);
        mAppExecutors = ((ACEApplication) application).getAppExecutors();
    }

    /*
     * View Model에서 오류에 대한 업무 로직 처리가 필요할때 해당 메소드를 상속받아서 처리한다.
     */
    @Override
    public void onError(Object who, Exception e) {
        super.onError(who, e);
        XcoloLog.d("Exception: " + e.getMessage());
    }
}
