package com.acebed.aceroom.view.catalog.product;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.acebed.aceroom.R;
import com.acebed.aceroom.adapter.CatalogSpinnerAdapter;
import com.acebed.aceroom.api.productlist.ProductListResponse;
import com.acebed.aceroom.application.ACEApplication;
import com.acebed.aceroom.databinding.ActivityProductDetailBinding;
import com.acebed.aceroom.environment.ACEEnvironment;
import com.acebed.aceroom.plugin.AcePlugin;
import com.acebed.aceroom.util.ACEUtil;
import com.acebed.aceroom.view.UnityPlayerActivity;
import com.acebed.aceroom.widget.ColorButton;
import com.acebed.aceroom.widget.CustomSpinner;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.ui.XcoloCommonAcvitity;
import lombok.Data;
/**
 * ProductDetailActivity
 * 제품 상세보기
 * @Author Xcolo DK
 */
public class ProductDetailActivity extends XcoloCommonAcvitity {
    private ActivityProductDetailBinding mBinding;
    private ProductDetailViewModel mViewModel;
    private String mBaseUrlOfDetail = ACEEnvironment.BASE_URL_OF_DETAIL;
    int mIsCurrentMode = -1;

    String mWallFloorResultMsg = "";

    CatalogSpinnerAdapter mCatalogSpinnerAdapter;
    List<ColorItem> mColorItemList;
    ProductByOption mProductByOption = new ProductByOption();
    ProductListResponse.ProductsData mProductsData;
    List<ProductListResponse.MattressListData> mMattressListData = new ArrayList<>();
    ProductListResponse.RoomsetListData mRoomsetListData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ACEApplication.getInstance().changeBaseUrl(mBaseUrlOfDetail);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail);
        mViewModel = ViewModelProviders.of(this).get(ProductDetailViewModel.class);

        mIsCurrentMode = getIntent().getIntExtra("isPageMode", -1);

        String productName = "";
        String wallFloorName = "";
        String furnitureName = "";
        // 선택 제품별로 분기
        switch(mIsCurrentMode) {
            case ACEEnvironment.PAGE_MODE_PRODUCT:
                productName = getIntent().getStringExtra("productName");
                break;
            case ACEEnvironment.PAGE_MODE_WALLFLOOR:
                wallFloorName = getIntent().getStringExtra("wallFloorName");
                break;
            case ACEEnvironment.PAGE_MODE_FURNITURE:
                furnitureName = getIntent().getStringExtra("furnitureName");
                break;
            default:
                Toast.makeText(ProductDetailActivity.this, "데이터 에러", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
        setViewVisibleForMode(mIsCurrentMode);
        String imageUrl = "";
        switch(mIsCurrentMode) {
            case ACEEnvironment.PAGE_MODE_PRODUCT:
                mProductsData = (ProductListResponse.ProductsData) getIntent().getSerializableExtra("productsData");
                mMattressListData = (List<ProductListResponse.MattressListData>) getIntent().getSerializableExtra("mattressList");

                // 최초 UI 세팅
                mBinding.productDetailTextViewProductTitle.setText(productName);
                imageUrl = mProductsData.getBaseOpt().getThumbnail();

                // 최초값 설정
                setBaseOption(productName, mProductsData.getBaseOpt());

                setColorItem(mProductsData.getColorList());
                setSizeSpinner(mProductsData.getSizeList());
                break;
            case ACEEnvironment.PAGE_MODE_WALLFLOOR:
                mBinding.productDetailTextViewProductTitle.setText(wallFloorName);
                String wallFloorDescription = getIntent().getStringExtra("wallFloorDescription");
                mBinding.productDetailTextViewWallFloorDescription.setText(wallFloorDescription);
                String wallFloorImageUrl = getIntent().getStringExtra("wallFloorImageUrl");
                imageUrl = wallFloorImageUrl;

                mWallFloorResultMsg = getIntent().getStringExtra("wallFloorResultMsg");
                break;
            case ACEEnvironment.PAGE_MODE_FURNITURE:
                mRoomsetListData = (ProductListResponse.RoomsetListData) getIntent().getSerializableExtra("roomsetListData");

                // 최초 UI 세팅
                mBinding.productDetailTextViewProductTitle.setText(furnitureName);
                imageUrl = mRoomsetListData.getBaseOpt().getThumbnail();

                // 최초값 설정
                setBaseOption(productName, mRoomsetListData.getBaseOpt());

                setColorItem(mRoomsetListData.getColorList());
                break;
        }
        ACEUtil.setImageFromUrl(imageUrl, mBinding.productDetailImageViewProductThumbnail);

        mBinding.productDetailImageButtonExitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // 색상버튼 세팅
        mBinding.productDetailImageButtonPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductDetailActivity.this, UnityPlayerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                switch(mIsCurrentMode) {
                    case ACEEnvironment.PAGE_MODE_PRODUCT:
                        String selectedColor = mProductByOption.getColor();
                        String selectedSize = mProductByOption.getSize();
                        String selectedMattress = mProductByOption.getMattress();
                        ProductListResponse.FrameListData searchedFrame = null;
                        ProductListResponse.MattressListData searchedMattress = null;

                        for (ProductListResponse.FrameListData data : mProductsData.getFrameList()) {
                            if (data.getSize().equals(selectedSize) && data.getColor().equals(selectedColor)) {
                                searchedFrame = data;
                                break;
                            }
                        }
                        if (searchedFrame == null) {
                            Toast.makeText(ProductDetailActivity.this, "프레임 데이터 에러", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        for (ProductListResponse.MattressListData data : mMattressListData) {
                            if (data.getSize().equals(selectedSize) && data.getName().equals(selectedMattress)) {
                                searchedMattress = data;
                                break;
                            }
                        }
                        if (searchedMattress == null) {
                            Toast.makeText(ProductDetailActivity.this, "매트리스 데이터 에러", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        String searchedFrameString = new Gson().toJson(searchedFrame);
                        String searchedMattressString = new Gson().toJson(searchedMattress);
                        String product = "{\"frame\":" + searchedFrameString + ",\"mattress\":" + searchedMattressString + "}";

                        AcePlugin.getInstance().LoadHomeProduct(product);
                        break;
                    case ACEEnvironment.PAGE_MODE_WALLFLOOR:
                        AcePlugin.getInstance().ChangeWallPaperStyle(mWallFloorResultMsg);
                        break;
                    case ACEEnvironment.PAGE_MODE_FURNITURE:
                        String selectedFurnitureColor = mProductByOption.getColor();
                        ProductListResponse.FurnitureListData searchedFurniture = null;


                        for (ProductListResponse.FurnitureListData data : mRoomsetListData.getFurnitureList()) {
                            if (data.getColor().equals(selectedFurnitureColor)) {
                                searchedFurniture = data;
                                break;
                            }
                        }
                        if (searchedFurniture == null) {
                            Toast.makeText(ProductDetailActivity.this, "가구 데이터 에러", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        String searchedFurnitureString = new Gson().toJson(searchedFurniture);

                        String furniture = "{\"furniture\":" + searchedFurnitureString + "}";
                        XcoloLog.d("furniture : " + furniture);
                        AcePlugin.getInstance().LoadHomeFurniture(furniture);
                        break;
                }

                startActivity(intent);
                ACEUtil.slideActivityFromRightToLeft(ProductDetailActivity.this);
            }
        });
        mBinding.setProductDetailViewModel(mViewModel);
        mBinding.setLifecycleOwner(this);
    }

    // 모드별로 View 분기처리
    private void setViewVisibleForMode(int mode) {
        switch(mode) {
            case ACEEnvironment.PAGE_MODE_PRODUCT:
                mBinding.productDetailTextViewWallFloorDescription.setVisibility(View.GONE);
                mBinding.productDetailLayoutColorArea.setVisibility(View.VISIBLE);
                mBinding.productDetailLayoutSizeArea.setVisibility(View.VISIBLE);
                break;
            case ACEEnvironment.PAGE_MODE_WALLFLOOR:
                mBinding.productDetailTextViewWallFloorDescription.setVisibility(View.VISIBLE);
                mBinding.productDetailLayoutColorArea.setVisibility(View.GONE);
                mBinding.productDetailLayoutSizeArea.setVisibility(View.GONE);
                break;
            case ACEEnvironment.PAGE_MODE_FURNITURE:
                mBinding.productDetailTextViewWallFloorDescription.setVisibility(View.GONE);
                mBinding.productDetailLayoutColorArea.setVisibility(View.VISIBLE);
                mBinding.productDetailLayoutSizeArea.setVisibility(View.GONE);
                break;
        }
    }

    // 사이즈 스피너 세팅
    private void setSizeSpinner(List<String> sizeList) {
        mCatalogSpinnerAdapter = new CatalogSpinnerAdapter(ProductDetailActivity.this.getBaseContext(), sizeList, true);
        mBinding.productDetailSpinnerSize.setAdapter(mCatalogSpinnerAdapter);

        int firstIndex = 0;
        mColorItemList = new ArrayList<>();
        for (int i = 0; i < sizeList.size(); i++) {
            if (mProductByOption.getSize().equals(sizeList.get(i))) {
                firstIndex = i;
            }
        }
        mBinding.productDetailSpinnerSize.setSelection(firstIndex);


        mBinding.productDetailSpinnerSize.setDropDownVerticalOffset(ACEUtil.dpToPx(37.7));
        mBinding.productDetailSpinnerSize.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
            @Override
            public void onSpinnerOpened(Spinner spinner) {
                spinner.getRootView().findViewById(R.id.productSize_imageView_arrow).setBackgroundResource(R.drawable.up_icon);
            }

            @Override
            public void onSpinnerClosed(Spinner spinner) {
                spinner.getRootView().findViewById(R.id.productSize_imageView_arrow).setBackgroundResource(R.drawable.sub_03_drop_down_list_btn_arrow_down);
            }
        });
        mBinding.productDetailSpinnerSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedSize = sizeList.get(adapterView.getSelectedItemPosition());
                mProductByOption.setSize(selectedSize);
                changeThumbnailImage(mProductByOption);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setColorItem(List<ProductListResponse.ColorListData> colorListData) {
        int firstIndex = 0;
        mColorItemList = new ArrayList<>();
        for (int i = 0; i < colorListData.size(); i++) {
            if (colorListData.get(i).getRgb().isEmpty() && !colorListData.get(i).getPath().isEmpty()) {
                mColorItemList.add(new ColorItem(colorListData.get(i).getTitle(), colorListData.get(i).getName(), colorListData.get(i).getPath()));
            } else if (!colorListData.get(i).getRgb().isEmpty() && colorListData.get(i).getPath().isEmpty()) {
                mColorItemList.add(new ColorItem(colorListData.get(i).getTitle(), colorListData.get(i).getName(), Color.parseColor(colorListData.get(i).getRgb())));
            } else if (!colorListData.get(i).getRgb().isEmpty() && !colorListData.get(i).getPath().isEmpty()) {
                mColorItemList.add(new ColorItem(colorListData.get(i).getTitle(), colorListData.get(i).getName(), colorListData.get(i).getPath()));
            } else {
                Toast.makeText(ProductDetailActivity.this, "데이터 에러", Toast.LENGTH_SHORT).show();
                finish();
            }

            if (mProductByOption.getColor().equals(colorListData.get(i).getName())) {
                firstIndex = i;
            }
        }

        for (int i = 0; i < mColorItemList.size(); i++) {
            ColorButton colorButton = new ColorButton(ProductDetailActivity.this);
            colorButton.setColorButton(mColorItemList.get(i));
            colorButton.setTag(mColorItemList.get(i).getName());
            mBinding.productDetailLayoutColorButton.addView(colorButton);
        }

        // 최초 색상값 select
        mBinding.productDetailLayoutColorButton.getChildAt(firstIndex).setSelected(true);
        for (int i = 0; i < mBinding.productDetailLayoutColorButton.getChildCount(); i++) {
            mBinding.productDetailLayoutColorButton.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (int j = 0; j < mBinding.productDetailLayoutColorButton.getChildCount(); j++) {
                        mBinding.productDetailLayoutColorButton.getChildAt(j).setSelected(false);
                    }
                    view.setSelected(true);
                    String selectedColorValue = (String) view.getTag();
                    mProductByOption.setColor(selectedColorValue);
                    changeThumbnailImage(mProductByOption);
                }
            });
        }
    }

    private void setBaseOption(String name, ProductListResponse.BaseOptData baseOption) {
        // 제품명
        mProductByOption.setName(name);

        switch(mIsCurrentMode) {
            case ACEEnvironment.PAGE_MODE_PRODUCT:
                // 색상
                mProductByOption.setColor(baseOption.getColor());
                // 사이즈
                mProductByOption.setSize(baseOption.getSize());
                // 매트리스
                mProductByOption.setMattress(baseOption.getMattress());
                break;

            case ACEEnvironment.PAGE_MODE_FURNITURE:
                // 색상
                mProductByOption.setColor(baseOption.getColor());
                break;
        }
    }


    private void changeThumbnailImage(ProductByOption option) {
        String thumbnail = "";
        switch(mIsCurrentMode) {
            case ACEEnvironment.PAGE_MODE_PRODUCT:
                for (ProductListResponse.FrameListData data : mProductsData.getFrameList()) {
                    if (data.getSize().equals(option.getSize()) && data.getColor().equals(option.color)) {
                        thumbnail = data.getThumbnail();
                        break;
                    }
                }
                break;
            case ACEEnvironment.PAGE_MODE_FURNITURE:
                for (ProductListResponse.FurnitureListData data : mRoomsetListData.getFurnitureList()) {
                    if (data.getColor().equals(option.color)) {
                        thumbnail = data.getThumbnail();
                        break;
                    }
                }
                break;
        }

        Uri imageUri = Uri.parse(thumbnail);
        Picasso.get().load(imageUri).fit().centerCrop().into(mBinding.productDetailImageViewProductThumbnail);
    }

    @Override
    public void refresh() {

    }

    @Data
    private class ProductByOption {
        String name;
        String color;
        String size;
        String mattress;
    }

    @Data
    public class ColorItem {
        String title;
        String name;
        int color;
        String path;

        public ColorItem(String title, String name, int color) {
            this.title = title;
            this.name = name;
            this.color = color;
            this.path = "";
        }

        public ColorItem(String title, String name, String path) {
            this.title = title;
            this.name = name;
            this.color = 0;
            this.path = path;
        }
    }
}
