package com.acebed.aceroom.api.buildings;

import com.acebed.aceroom.environment.ACEEnvironment;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import kr.co.xcolo.android.fw.mvvm.retrofit.GenericRetrofitModel;
import lombok.Data;

/**
 * BuildingsResponse
 * 빌딩상세정보 Response
 * @Author Xcolo DK
 * 수정 : 2020.05.14 - 불필요한 값들 제거
 */
@Data
public class BuildingDetailResponse implements GenericRetrofitModel {
    private String code;
    private String message;
    private BuildingsListData data;
    @Override
    public boolean isError() {
        if(getCode().equals("00000"))
            return false;
        else
            return true;
    }

    @Override
    public String getErrorMessage() {
        return getMessage();
    }

    @Data
    public static class BuildingsData {
        private List<BuildingsListData> buildings;
    }

    @Data
    public static class BuildingsListData {
        private String id;
        @SerializedName("building_name") private String buildingName;
//        @SerializedName("building_no") private String buildingNo;
//        @SerializedName("road_address") private String roadAddress;
        private String address;
//        private double latitude;
//        private double longitude;
//        @SerializedName("construction_company") private String constructionCompany;
//        @SerializedName("building_completion_ym") private String buildingCompletionYm;
//        @SerializedName("total_household_count") private int totalHouseholdCount;
//        @SerializedName("total_dong_count") private int totalDongCount;
//        @SerializedName("m_building_type_id") private String mBuildingTypeId;
//        @SerializedName("batl_ratio") private int batlRatio;
//        @SerializedName("btl_ratio") private int btlRatio;
//        @SerializedName("household_parking") private int householdParking;
//        @SerializedName("low_floor") private int lowFloor;
//        @SerializedName("high_floor") private int highFloor;
//        @SerializedName("heat_type") private String heatType;
//        @SerializedName("building_area") private String buildingArea;
//        @SerializedName("country_id") private String countryId;
//        private Boolean publish;
//        private Boolean enabled;
//        @SerializedName("object_id") private String objectId;
//        @SerializedName("created_date") private String createdDate;
//        @SerializedName("updated_date") private Date updatedDate;
//        @SerializedName("deleted_date") private Date deletedDate;
        @SerializedName("building_uuid") private String buildingUuid;
        private CountryData country;
        @SerializedName("building_areas") private List<BuildingAreasData> buildingAreas;
    }

    @Data
    public static class CountryData {
        private String id;
        private String name;
        private String code;
        private String callingCode;
    }
    @Data
    public static class BuildingAreasData {
        private String id;
        @SerializedName("building_id")private String buildingId;
        @SerializedName("supply_area")private Double supplyArea;
        @SerializedName("exclusive_area")private Double exclusiveArea;
        @SerializedName("display_supply_area")private String displaySupplyArea;
        @SerializedName("display_supply_area_pyeong")private String displaySupplyAreaPyeong;
        @SerializedName("exclusive_ratio")private Integer exclusiveRatio;
        @SerializedName("m_entrance_type_id")private String mEntranceTypeId;
        private Boolean publish;
        private Boolean enabled;
        @SerializedName("created_date")private Date createdDate;
        @SerializedName("updated_date")private Date updatedDate;
        @SerializedName("deleted_date")private Date deletedDate;
        @SerializedName("units")private List<UnitsListData> units;
    }

    @Data
    public static class UnitsListData {
        @SerializedName("id")private String id;
        @SerializedName("building_area_id")private String buildingAreaId;
        @SerializedName("unit_uuid")private String unitUuid;
        @SerializedName("user_id")private String userId;
        @SerializedName("room_count")private int roomCount;
        @SerializedName("bathroom_count")private int bathroomCount;
        @SerializedName("top_view_image_path")private String topViewImagePath;
        @SerializedName("floor_plan_image_path")private String floorPlanImagePath;
        @SerializedName("floor_plan_metadata_path")private String floorPlanMetadataPath;
        @SerializedName("engine_version")private int engineVersion;
        @SerializedName("m_engine_status_id")private String mEngineStatusId;
        @SerializedName("approve_user_id")private Object approveUserId;
        @SerializedName("approve_date")private Date approveDate;
        @SerializedName("is_admin_confirm")private Boolean isAdminConfirm;
        @SerializedName("is_expand")private Boolean isExpand;
        private Boolean publish;
        private Boolean enabled;
        @SerializedName("object_id")private String objectId;
        @SerializedName("created_date")private Date createdDate;
        @SerializedName("updated_date")private Date updatedDate;
        @SerializedName("deleted_date")private Date deletedDate;
        @SerializedName("engine_status")private EngineStatusData engineStatus;
        @SerializedName("unit_attaches")private List<UnitAttachesData> unitAttaches;

        public String getTopViewImageFullPath() {
            if (topViewImagePath != null) {
                return ACEEnvironment.BASE_URL_OF_BUILDINGS + topViewImagePath;
            }
            return null;
        }
    }

    @Data
    public static class UnitAttachesData {
        private String id;
        @SerializedName("unit_id") private String unitId;
        @SerializedName("file_path") private String filePath;
        @SerializedName("is_default") private Boolean isDefault;
        @SerializedName("m_attach_type_id") private String mAttachTypeId;
        private String extension;
        private Boolean enabled;
        @SerializedName("created_date") private Date createdDate;
        @SerializedName("updated_date") private Date updatedDate;
        @SerializedName("deleted_date") private Date deletedDate;
        @SerializedName("signed_url") private String signedUrl;
    }

    @Data
    public static class EngineStatusData {
        private String id;
        private String name;
        private String code;
        private String category;
        private Boolean enabled;
        private Object sort;
        private String description;
    }
}
