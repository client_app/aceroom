package com.acebed.aceroom.model.catalog;

import com.acebed.aceroom.api.productlist.ProductListResponse;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProductItem implements Serializable {
    String name;
    String catalogThumbnailImageUrl;
    String size;

    ProductListResponse.ProductsData productsData;

    public ProductItem(String name, String catalogThumbnailImageUrl, String size, ProductListResponse.ProductsData productsData) {
        this.name = name;
        this.catalogThumbnailImageUrl = catalogThumbnailImageUrl;
        this.size = size;
        this.productsData = productsData;
    }
}
