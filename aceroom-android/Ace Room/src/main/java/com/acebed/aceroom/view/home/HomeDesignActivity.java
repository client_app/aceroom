package com.acebed.aceroom.view.home;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.acebed.aceroom.R;
import com.acebed.aceroom.adapter.FloorPlanSpinnerAdapter;
import com.acebed.aceroom.api.buildings.BuildingDetailResponse;
import com.acebed.aceroom.application.ACEApplication;
import com.acebed.aceroom.databinding.ActivityHomeDesignActivityBinding;
import com.acebed.aceroom.dialog.ACETutorialDialog;
import com.acebed.aceroom.api.buildings.BuildingsResponse;
import com.acebed.aceroom.environment.ACEEnvironment;
import com.acebed.aceroom.model.home.DrawerItem;
import com.acebed.aceroom.model.home.Hangjung;
import com.acebed.aceroom.util.ACEUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import kr.co.xcolo.android.fw.XcoloLog;
import kr.co.xcolo.android.fw.mvvm.ui.XcoloCommonAcvitity;


/**
 * HomeDesignActivity
 * 지도, 도면선택
 * @Author Xcolo DK
 */
public class HomeDesignActivity extends XcoloCommonAcvitity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {
    private ActivityHomeDesignActivityBinding mBinding;
    private HomeDesignViewModel mViewModel;
    private GoogleMap mGoogleMap = null;
    private ACEApplication mApplication = ACEApplication.getInstance();
    //    List<DrawerItem> mDrawerItemList;
    BuildingDetailResponse.UnitAttachesData mSelectedUnitAttachesData; //선택된 마커의 Data
    FloorPlanSpinnerAdapter mFloorPlanSpinnerAdapter;

    // Declare a variable for the cluster manager.
    private Geocoder mGeocoder;

    boolean isOpenDrawer = false;

    // 행정구역 정보
    private final String DONG_SNIPPET_JSON = "emd_kor_nm.json";
    private final String SIGUNGU_SNIPPET_JSON = "sig_kor_nm.json";
    private final String SIDO_SNIPPET_JSON = "ctp_kor_nm.json";

    @Override
    public void onBackPressed() {
        // drawer open 유무에 따라 취소키 이벤트 분기처리
        if (isOpenDrawer) {
            mBinding.homeLayoutBuildingInfo.closeDrawers();
            mBinding.homeImageViewFloorPlan.setVisibility(View.GONE);
            mBinding.homeImageViewReadyText.setVisibility(View.VISIBLE);
            isOpenDrawer = false;
        } else {
            mApplication.setSelectedLatLng(null);
            Intent resultIntent = new Intent();
            resultIntent.putExtra("result", "HOME_BACK");
            setResult(RESULT_OK, resultIntent);
            super.onBackPressed();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication.changeBaseUrl(ACEEnvironment.BASE_URL_OF_MAP);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home_design_activity);

        mViewModel = ViewModelProviders.of(this).get(HomeDesignViewModel.class);

        mBinding.setHomeDesignViewModel(mViewModel);
        mBinding.setLifecycleOwner(this);
        setViewModel(mViewModel);

        //Drawer Layout
        mBinding.homeLayoutBuildingInfo.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mBinding.homeLayoutBuildingInfo.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                XcoloLog.d("onDrawerOpened");
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                XcoloLog.d("onDrawerClosed");
                mBinding.homeImageViewFloorPlan.setVisibility(View.GONE);
                mBinding.homeImageViewReadyText.setVisibility(View.VISIBLE);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        // 백버튼
        mBinding.homeImageButtonExitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mApplication.setSelectedLatLng(null);
                Intent resultIntent = new Intent();
                resultIntent.putExtra("result", "HOME_BACK");
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });

        // 튜토리얼 버튼
        mBinding.homeImageButtonTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ACETutorialDialog.newInstance(HomeDesignActivity.this).setHomeDesignTutorialData().show();
            }
        });

        FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment = (MapFragment) fragmentManager
                .findFragmentById(R.id.home_layout_map);
        mapFragment.getMapAsync(this);

        if(!ACEApplication.getInstance().getPreferences("ROOM")) {
            ACETutorialDialog.newInstance(this).setHomeDesignTutorialData().show();
            ACEApplication.getInstance().savePreferences("ROOM");
        }
    }

    // Device Home Btn Event 예외처리
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        ACEApplication.getInstance().leavedActivity(this);
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        mGoogleMap = map;
        mGeocoder = new Geocoder(this);
        loadHanjungJson();

        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);

        // 카메라 시점에 따른 이벤트처리
        mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                float currentMapZoomLevel = mGoogleMap.getCameraPosition().zoom;
                // 왼쪽 아래, 오른쪽 위 좌표 구하기
                VisibleRegion visibleRegion = mGoogleMap.getProjection().getVisibleRegion();
                LatLng bottomLeft = visibleRegion.nearLeft;
                LatLng topRight = visibleRegion.farRight;

                XcoloLog.d("Zoom Level: " + currentMapZoomLevel);
                XcoloLog.d("center LatLng: [" + mGoogleMap.getCameraPosition().target.latitude + ", " + mGoogleMap.getCameraPosition().target.longitude + "]");

                // zoom level에 따라 행정도시 표시
                if (currentMapZoomLevel >= 14) {
                    setUpBuilding(bottomLeft, topRight);
//                } else if (currentMapZoomLevel >= 14) {
//                    setUpDong(bottomLeft, topRight);
                } else if (currentMapZoomLevel >= 11) {
                    setUpSiGunGuGeoJson(bottomLeft, topRight);
                } else {
                    setUpSiDoGeoJson(bottomLeft, topRight);
                }
            }
        });

        // 마커 이벤트
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                List<DrawerItem> drawerItemList = new ArrayList<>();
                CameraUpdate center = CameraUpdateFactory.newLatLng(marker.getPosition());
                mGoogleMap.animateCamera(center);
                mApplication.setSelectedLatLng(marker.getPosition());
                // 지역 마커 이동
                String marketSnippet = marker.getSnippet();
                int targetZoomLevel = 0;

                if (SIDO_SNIPPET_JSON.equals(marketSnippet)) {
                    targetZoomLevel = 11;
                } else if (SIGUNGU_SNIPPET_JSON.equals(marketSnippet)) {
                    targetZoomLevel = 14;
//                } else if (DONG_SNIPPET_JSON.equals(marketSnippet)) {
//                    targetZoomLevel = 16;
                }

                XcoloLog.d("targetZoomLevel: " + targetZoomLevel);
                if (targetZoomLevel > 0) {
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), targetZoomLevel), 400, null);
                    return true;
                }

                //TODO : 아래부분을 API 호출로 변경해야함
                // 건물정보
                String markerBuildingUuid = (String)marker.getTag();
                mBinding.getHomeDesignViewModel().requestBuildingDetail(markerBuildingUuid).observe(HomeDesignActivity.this, buildingDetailResponse -> {
                    synchronized (HomeDesignActivity.this) {
                        BuildingDetailResponse.BuildingsListData buildingData = buildingDetailResponse.getData();

                        if (buildingData == null) {
                            return;
                        }
                        if (buildingData.getBuildingAreas().size() == 0) {
                            Toast.makeText(HomeDesignActivity.this, "잘못된 정보", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        mBinding.homeTextViewBuildingName.setText(buildingData.getBuildingName());
                        mBinding.homeTextViewAddress.setText(buildingData.getAddress());

                        for (BuildingDetailResponse.BuildingAreasData data : buildingData.getBuildingAreas()) {
                            if (data.getUnits().size() > 0) {
                                for (BuildingDetailResponse.UnitsListData units : data.getUnits()) {
                                    BuildingDetailResponse.UnitAttachesData unitAttachesData = null;
                                    if (units.getUnitAttaches().size() > 0) {
                                        unitAttachesData = units.getUnitAttaches().get(0);
                                    }
                                    drawerItemList.add(new DrawerItem(
                                            data.getDisplaySupplyAreaPyeong()
                                            , data.getExclusiveArea()
                                            , units.getId()
                                            , units.getBathroomCount()
                                            , units.getRoomCount()
                                            , units.getTopViewImageFullPath()
                                            , unitAttachesData));

                                }
                            }
                        }

                        if (drawerItemList.size() > 0) {
                            mBinding.homeTextViewBathCount.setText(drawerItemList.get(0).getBathroomCount() + "개");
                            mBinding.homeTextViewRoomCount.setText(drawerItemList.get(0).getRoomCount() + "개");

                            XcoloLog.d("##############################Image Test : " + drawerItemList.get(0).getTopViewImageFullPath());
                            if(drawerItemList.get(0).getTopViewImageFullPath() != null) {
                                ACEUtil.setImageFromUrl(drawerItemList.get(0).getTopViewImageFullPath(), mBinding.homeImageViewFloorPlan, true);
                                mBinding.homeImageViewFloorPlan.setVisibility(View.VISIBLE);
                                mBinding.homeImageViewReadyText.setVisibility(View.GONE);
                            } else {
                                mBinding.homeImageViewFloorPlan.setVisibility(View.GONE);
                                mBinding.homeImageViewReadyText.setVisibility(View.VISIBLE);
                            }

                            // 평면도 리스트 생성
                            mFloorPlanSpinnerAdapter = new FloorPlanSpinnerAdapter(HomeDesignActivity.this.getBaseContext(), drawerItemList);
                            mBinding.homeSpinnerFloorPlanInfo.setAdapter(mFloorPlanSpinnerAdapter);
                            mBinding.homeSpinnerFloorPlanInfo.setDropDownVerticalOffset(ACEUtil.dpToPx(41.7));
                            mBinding.homeSpinnerFloorPlanInfo.setArrowViewId(R.id.floorPlan_imageView_arrow);
                            mBinding.homeSpinnerFloorPlanInfo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    int index = adapterView.getSelectedItemPosition();
                                    if (drawerItemList.get(index).getUnitAttaches() != null) {
                                        mBinding.homeTextViewBathCount.setText(drawerItemList.get(index).getBathroomCount() + "개");
                                        mBinding.homeTextViewRoomCount.setText(drawerItemList.get(index).getRoomCount() + "개");
                                        mSelectedUnitAttachesData = drawerItemList.get(index).getUnitAttaches();
                                    } else if (drawerItemList.get(index).getRoomCount() > 0 && drawerItemList.get(index).getBathroomCount() > 0) {
                                        mBinding.homeTextViewBathCount.setText(drawerItemList.get(index).getBathroomCount() + "개");
                                        mBinding.homeTextViewRoomCount.setText(drawerItemList.get(index).getRoomCount() + "개");
                                        mSelectedUnitAttachesData = null;
                                    } else {
                                        mBinding.homeTextViewBathCount.setText("0 개");
                                        mBinding.homeTextViewRoomCount.setText("0 개");
                                        mSelectedUnitAttachesData = null;
                                    }
                                    if(drawerItemList.get(index).getTopViewImageFullPath() != null) {
                                        ACEUtil.setImageFromUrl(drawerItemList.get(index).getTopViewImageFullPath(), mBinding.homeImageViewFloorPlan, true);
                                        mBinding.homeImageViewFloorPlan.setVisibility(View.VISIBLE);
                                        mBinding.homeImageViewReadyText.setVisibility(View.GONE);
                                    } else {
                                        mBinding.homeImageViewFloorPlan.setVisibility(View.GONE);
                                        mBinding.homeImageViewReadyText.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {
                                }
                            });

                            mBinding.homeLayoutBuildingInfo.openDrawer(Gravity.RIGHT);
                            if (isOpenDrawer == false) {
                                isOpenDrawer = true;
                            }
                        } else {
                            Toast.makeText(HomeDesignActivity.this, "도면정보가 없습니다.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }
                });
                return true;
            }
        });


        // 꾸미기 버튼 이벤트
        mBinding.homeButtonDecorate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedUnitAttachesData == null) {
                    Toast.makeText(HomeDesignActivity.this, "도면정보가 없습니다.", Toast.LENGTH_LONG).show();
                } else {
                    XcoloLog.e("================");
                    XcoloLog.e(mSelectedUnitAttachesData.toString());
                    XcoloLog.e("================");
//                    String floorPlanInfo = new Gson().toJson(mSelectedUnitAttachesData);
                    Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                    String floorPlanInfo = gson.toJson(mSelectedUnitAttachesData);
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("result", "FLOOR_PLAN");
                    resultIntent.putExtra("floorPlanInfo", floorPlanInfo);
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
            }
        });
        LatLng startPoint;

        // 지도 시작점 설정
        if (mApplication.getSelectedLatLng() == null) {
//            startPoint = new LatLng(37.56, 126.97); //  서울
            startPoint = new LatLng(37.553780973755636, 126.98414124548435);
            float zoomLevel = (float)10.9;
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startPoint, zoomLevel), 300, null);
            XcoloLog.d("mApplication.getSelectedLatLng() == null");
        } else {
            startPoint = mApplication.getSelectedLatLng();
            int zoomLevel = 18;
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startPoint, zoomLevel), 300, null);
            XcoloLog.d("mApplication.getSelectedLatLng() != null");
        }

        // 찾기버튼 이벤트
        mBinding.homeImageButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 버튼 이벤트
                String str = mBinding.homeEditTextSearchText.getText().toString();
                if(str.isEmpty()) {
                    Toast.makeText(HomeDesignActivity.this, "주소를 입력해주세요.", Toast.LENGTH_SHORT).show();
                } else if(str.trim().equals("")) {
                    Toast.makeText(HomeDesignActivity.this, "주소를 입력해주세요.", Toast.LENGTH_SHORT).show();
                } else {
                    List<Address> addressList = null;
                    try {
                        // editText에 입력한 텍스트(주소, 지역, 장소 등)을 지오 코딩을 이용해 변환
                        addressList = mGeocoder.getFromLocationName(
                                str, // 주소
                                10); // 최대 검색 결과 개수
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (addressList.size() > 0) {
                        if(addressList.get(0).getCountryCode().equals("KR")) {
                            String address = addressList.get(0).getAddressLine(0); // 주소
                            Double latitude = addressList.get(0).getLatitude();
                            Double longitude = addressList.get(0).getLongitude();
                            XcoloLog.d("address : " + address);
                            XcoloLog.d("latitude : " + latitude);
                            XcoloLog.d("longitude : " + longitude);

                            // 좌표(위도, 경도) 생성
                            LatLng point = new LatLng(latitude, longitude);
                            // 마커 생성
                            MarkerOptions mOptions2 = new MarkerOptions();
                            mOptions2.title("search result");
                            mOptions2.snippet(address);
                            mOptions2.position(point);
                            // 마커 추가

                            // 해당 좌표로 화면 줌
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 18));
                        } else {
                            Toast.makeText(HomeDesignActivity.this, "검색정보가 없습니다.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(HomeDesignActivity.this, "검색정보가 없습니다.", Toast.LENGTH_SHORT).show();
                    }
                }
                mBinding.homeEditTextSearchText.setText("");
//                mBinding.homeEditTextSearchText.setFocusable(false);
                mBinding.homeEditTextSearchText.clearFocus();

            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        CameraUpdate center = CameraUpdateFactory.newLatLng(marker.getPosition());
        mGoogleMap.animateCamera(center);
        return true;
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void refresh() {

    }

    private void clearAllmarker() {
        mGoogleMap.clear();
    }

    // 해당 화면에 마커정보 세팅
    private void setUpBuilding(LatLng bottomLeft, LatLng topRight) {
        mBinding.getHomeDesignViewModel().requestBuildings(bottomLeft, topRight).observe(HomeDesignActivity.this, buildingsResponse -> {
            synchronized (HomeDesignActivity.this) {
                clearAllmarker();
                loadBuilding(buildingsResponse.getData().getBuildings());
            }
        });
    }

    // 마커 추가
    private void loadBuilding(List<BuildingsResponse.BuildingsListData> buildings) {
        for (BuildingsResponse.BuildingsListData building : buildings) {
            mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(building.getLatitude(), building.getLongitude()))
                    .icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(HomeDesignActivity.this, R.drawable.sub_05_map_icon)))).setTag(building.getBuildingUuid());
        }
    }

    private static List<Hangjung> mDongHanjungs = null;
    private static List<Hangjung> mSiGunGuHanjungs = null;
    private static List<Hangjung> mSiDoHanjungs = null;

    // 행정정보 가져오기
    private void loadHanjungJson() {
        long start = System.currentTimeMillis();

        XcoloLog.d("loadHanjungJson.start: " + start);
        mDongHanjungs = getJsonFromFile(DONG_SNIPPET_JSON);
        XcoloLog.d("mDongHanjungs.size: " + mDongHanjungs.size());
        mSiGunGuHanjungs = getJsonFromFile(SIGUNGU_SNIPPET_JSON);
        XcoloLog.d("mSiGunGuHanjungs.size: " + mSiGunGuHanjungs.size());
        mSiDoHanjungs = getJsonFromFile(SIDO_SNIPPET_JSON);
        XcoloLog.d("mSiDoHanjungs.size: " + mSiDoHanjungs.size());
        XcoloLog.d("loadHanjungJson.eclipsed: " + (System.currentTimeMillis() - start) / 1000);
    }

    private List<Hangjung> getJsonFromFile(String fileName) {
        InputStream is = null;
        List<Hangjung> hanjungs = new ArrayList<>();
        try {
            is = getAssets().open(fileName);
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new InputStreamReader(is));
            Hangjung[] results = gson.fromJson(reader, Hangjung[].class);
            for (Hangjung result : results) {
                result.setSnippet(fileName);
                hanjungs.add(result);
            }
//            hanjungs = Arrays.asList(result);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return hanjungs;
    }

    private void setUpDong(LatLng bottomLeft, LatLng topRight) {
        setUpHangjung(bottomLeft, topRight, mDongHanjungs);
    }

    private void setUpSiGunGuGeoJson(LatLng bottomLeft, LatLng topRight) {
        setUpHangjung(bottomLeft, topRight, mSiGunGuHanjungs);
    }

    private void setUpSiDoGeoJson(LatLng bottomLeft, LatLng topRight) {
        setUpHangjung(bottomLeft, topRight, mSiDoHanjungs);
    }

    private void setUpHangjung(final LatLng bottomLeft, final LatLng topRight, final List<Hangjung> hangjungs) {
        Thread thread = new Thread(new Runnable() {
            @Override
//            public void run() {
//                setUpGeoJsonThread(bottomLeft, topRight, geojsonResId, nameKey);
//            }
            public void run() {
                setUpHangjungThread(bottomLeft, topRight, hangjungs);
            }
        });
        thread.run();
    }

    @SuppressLint("ResourceAsColor")
    private void setUpHangjungThread(LatLng bottomLeft, LatLng topRight, List<Hangjung> hangjuns) {
        long startTime = System.currentTimeMillis();
        clearAllmarker();

//        XcoloLog.d("bottomLeft: " + bottomLeft + ", topRight: " + topRight);
        List<LatLng> coordinates = new ArrayList<>();
        for (Hangjung hanjung : hangjuns) {
//            XcoloLog.d("[" + hanjung.getTitle() + "].position : " + hanjung.getPosition());
            if (isInCoordenate(bottomLeft, topRight, hanjung.getPosition())) {
                TextView markerTextView = new TextView(HomeDesignActivity.this);
//                markerTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                markerTextView.setPadding(30, 30, 30, 30);
                markerTextView.setBackgroundResource(R.drawable.round);
                markerTextView.setText(hanjung.getTitle());
                markerTextView.setTextColor(getResources().getColor(R.color.mapMarkerFontColor));
                mGoogleMap.addMarker(new MarkerOptions().position(hanjung.getPosition()).icon(BitmapDescriptorFactory.fromBitmap(convertViewToDrawable(markerTextView))).snippet(hanjung.getSnippet()));
//                mMap.addMarker(new MarkerOptions().position(hanjung.getPosition()).title(hanjung.getTitle()));
            }
        }
        XcoloLog.d("setUpHanjungThread: " + (System.currentTimeMillis() - startTime) / 1000);
    }

    private boolean isInCoordenate(LatLng bottomLeft, LatLng topRight, LatLng coordinate) {
        if (bottomLeft.latitude <= coordinate.latitude && coordinate.latitude <= topRight.latitude
                && bottomLeft.longitude <= coordinate.longitude && coordinate.longitude <= topRight.longitude) {
            return true;
        }
        return false;
    }

    private void addAllCoordinates(List<LatLng> result, List<? extends List<LatLng>> lists) {
        for (List<LatLng> list : lists) {
            for (LatLng latLng : list) {
                result.add(latLng);
            }
        }
    }

    private LatLng getCenter(List<LatLng> list) {
        double minLat = Double.MAX_VALUE;
        double minLng = Double.MAX_VALUE;
        double maxLat = Double.MIN_VALUE;
        double maxLng = Double.MIN_VALUE;
        for (LatLng latLng : list) {
            if (latLng.latitude < minLat) {
                minLat = latLng.latitude;
            }
            if (latLng.latitude > maxLat) {
                maxLat = latLng.latitude;
            }
            if (latLng.longitude < minLng) {
                minLng = latLng.longitude;
            }
            if (latLng.longitude > maxLng) {
                maxLng = latLng.longitude;
            }
        }
        double centerLat = (maxLat - minLat) / 2;
        double centerLng = (maxLng - minLng) / 2;

        return new LatLng(minLat + centerLat, minLng + centerLng);
    }

    private Bitmap convertViewToDrawable(View view) {
        int spec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap b = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        c.translate((-view.getScrollX()), -view.getScrollY());
        view.draw(c);
        return b;
    }

    private static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }
}
