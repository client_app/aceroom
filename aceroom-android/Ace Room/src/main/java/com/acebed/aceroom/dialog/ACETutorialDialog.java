package com.acebed.aceroom.dialog;

import android.app.Activity;
import android.content.res.Configuration;
import android.view.View;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.acebed.aceroom.R;
import com.acebed.aceroom.adapter.TutorialViewPagerAdapter;
import com.acebed.aceroom.model.TutorialItem;
import com.acebed.aceroom.widget.ACECommonDialog;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class ACETutorialDialog extends ACECommonDialog {

    private static ACETutorialDialog mAceTutorialDialog = null;
    private ArrayList<TutorialItem> mTutorialItemList;
    private boolean mIsPortMode = true;
    private ViewMapper mMapper;


    public synchronized static ACETutorialDialog newInstance(Activity activity) {
        mAceTutorialDialog = new ACETutorialDialog(activity);
        return mAceTutorialDialog;
    }


    private ACETutorialDialog(Activity activity) {
        super(activity);
    }

    @Override
    protected View inflateContentsView() {
        View settingView;
        int orientation = getActivity().getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            settingView = getActivity().getLayoutInflater().inflate(R.layout.layout_tutorial_port, null, false);
        } else {
            settingView = getActivity().getLayoutInflater().inflate(R.layout.layout_tutorial_land, null, false);
        }

        mMapper = new ViewMapper(settingView);

        mMapper.textViewSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide();
            }
        });

        mMapper.textViewNextEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = mMapper.viewPagerPage.getCurrentItem() + 1;
                mMapper.viewPagerPage.setCurrentItem(index, true);
            }
        });

        return settingView;
    }

    @Override
    protected void showDialog() {
        if (getActivity() instanceof Activity && ((Activity) getActivity()).isFinishing() == false) {

            // 버튼이벤트
            mMapper.viewPagerPage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (position == mTutorialItemList.size() - 1) {
                        mMapper.textViewNextEnd.setText("END");
                        mMapper.textViewNextEnd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                hide();
                            }
                        });
                    } else {
                        mMapper.textViewNextEnd.setText("NEXT");
                        mMapper.textViewNextEnd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                int index = mMapper.viewPagerPage.getCurrentItem() + 1;
                                mMapper.viewPagerPage.setCurrentItem(index, true);
                            }
                        });
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            TutorialViewPagerAdapter adapter = new TutorialViewPagerAdapter(getActivity(), mTutorialItemList, mIsPortMode);
            mMapper.viewPagerPage.setClipToPadding(false);
            mMapper.viewPagerPage.setAdapter(adapter);
            mMapper.circleIndicator.setViewPager(mMapper.viewPagerPage);

            adapter.registerDataSetObserver(mMapper.circleIndicator.getDataSetObserver());

            getDialog().show();
        }
    }

    public ACETutorialDialog setArTutorialData() {
        mIsPortMode = true;
        mTutorialItemList = new ArrayList<>();
        mTutorialItemList.add(new TutorialItem(R.drawable.sub_tutorial_ar_bed_img_01, "에이스 침대 카탈로그에서\n마음에 드는 제품을 찾아보세요"));
        mTutorialItemList.add(new TutorialItem(R.drawable.sub_tutorial_ar_bed_img_02, "사이즈, 색상, 매트리스 제품의\n옵션을 선택하세요"));
        mTutorialItemList.add(new TutorialItem(R.drawable.sub_tutorial_ar_bed_img_03, "원하는 위치에\n침대를 배치하세요"));

        return this;
    }

    public ACETutorialDialog setMattressTutorialData() {
        mIsPortMode = true;
        mTutorialItemList = new ArrayList<>();
        mTutorialItemList.add(new TutorialItem(R.drawable.sub_tutorial_marker_img_01, "비치된 마커에\n카메라를 올려보세요"));
        mTutorialItemList.add(new TutorialItem(R.drawable.sub_tutorial_marker_img_02, "하이브리드 Z 스프링, 올인원 공법,\n투매트리스 시스템\n세가지 컨텐츠 중 하나를 고르세요"));
        mTutorialItemList.add(new TutorialItem(R.drawable.sub_tutorial_marker_img_03, "플레이되는 AR 컨텐츠를\n즐겨보세요!"));

        return this;
    }

    public ACETutorialDialog setHomeDesignTutorialData() {
        mIsPortMode = false;
        mTutorialItemList = new ArrayList<>();
        mTutorialItemList.add(new TutorialItem(R.drawable.sub_tutorial_homedesgin_img_01, "지도에서 보고싶은 도면을 가진\n아파트를 선택하세요"));
        mTutorialItemList.add(new TutorialItem(R.drawable.sub_tutorial_homedesgin_img_02, "에디터에서 침대부터 인테리어까지\n다양한 조합을 찾아보세요"));
        mTutorialItemList.add(new TutorialItem(R.drawable.sub_tutorial_homedesgin_img_03, "배치하기를 눌러서\n원하는 공간에 올려보아요"));

        return this;
    }

    protected class ViewMapper {
        ViewPager viewPagerPage;
        TextView textViewSkip;
        TextView textViewNextEnd;
        CircleIndicator circleIndicator;

        public ViewMapper(View view) {
            viewPagerPage = view.findViewById(R.id.tutorial_viewPager_page);
            textViewSkip = view.findViewById(R.id.tutorial_textView_skip);
            textViewNextEnd = view.findViewById(R.id.tutorial_textView_nextEnd);
            circleIndicator = view.findViewById(R.id.tutorial_circleIndicator_indicator);
        }
    }
}
