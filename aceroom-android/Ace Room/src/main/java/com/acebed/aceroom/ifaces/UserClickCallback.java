package com.acebed.aceroom.ifaces;

public interface UserClickCallback {
    void onClick(Object item);
}
