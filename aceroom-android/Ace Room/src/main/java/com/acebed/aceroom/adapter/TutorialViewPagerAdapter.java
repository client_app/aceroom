package com.acebed.aceroom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.acebed.aceroom.R;
import com.acebed.aceroom.model.TutorialItem;

import java.util.ArrayList;

public class TutorialViewPagerAdapter extends PagerAdapter {
    private Context mContext;
    private ArrayList<TutorialItem> mTutorialItemList;
    private ViewMapper mViewMapper;
    private boolean mIsPortMode;

    public TutorialViewPagerAdapter(Context context, ArrayList<TutorialItem> mTutorialItemList, boolean isPortMode)
    {
        this.mContext = context;
        this.mTutorialItemList = mTutorialItemList;
        this.mIsPortMode = isPortMode;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        if(mIsPortMode) {
            view = inflater.inflate(R.layout.item_tutorial_port, null);
        } else {
            view = inflater.inflate(R.layout.item_tutorial_land, null);
        }


        mViewMapper = new ViewMapper(view);

        mViewMapper.imageViewItem.setImageResource(mTutorialItemList.get(position).getImageId());
        mViewMapper.textViewItem.setText(mTutorialItemList.get(position).getText());

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return mTutorialItemList.size();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return (view == (View)o);
    }

    protected class ViewMapper {
        ImageView imageViewItem;
        TextView textViewItem;
        public ViewMapper(View view) {
            imageViewItem = view.findViewById(R.id.tutorial_imageView_item);
            textViewItem = view.findViewById(R.id.tutorial_textView_item);
        }
    }
}