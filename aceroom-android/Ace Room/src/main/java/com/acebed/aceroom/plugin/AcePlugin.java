package com.acebed.aceroom.plugin;

import android.content.Context;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

import java.util.Random;

public class AcePlugin {
    private static AcePlugin _plugin = null;
    private static Context mContext = null;

    public AcePlugin() {

    }

    public static AcePlugin getInstance() {
        if (_plugin == null) {
            _plugin = new AcePlugin();
        }
        return _plugin;
    }

    public void setContext(Context context) {
        this.mContext = context;
    }

    public void ShowToast(String msg) {
        Log.d("AceBedAndroid", "ShowToast : " + msg);
        String message = msg + "(by ShowToast)";
        Log.d("AceBedAndroid", "CallEcco : " + message);
        SendMessage("CallEcco", message);
        Log.d("AceBedAndroid", "ShowToast end");
    }

    public void DeviceVersionCheck(String msg) {
        Log.d("AceBedAndroid", "DeviceVersionCheck : " + msg);
        String message = msg + "(by DeviceVersionCheck)";
        Log.d("AceBedAndroid", "CallEcco : " + message);
        SendMessage("CallEcco", message);
        Log.d("AceBedAndroid", "DeviceVersionCheck end");
    }


    //공통부분

    /**
     * Unity 내부 notch 디자인 세팅
     * @param isNotch
     */
    public void SetIsNotch(boolean isNotch) {
        String strIsNotch;
        if(isNotch){
            strIsNotch = "true";
        }else{
            strIsNotch = "false";
        }

        SendMessage("setIsNotch", strIsNotch);
    }

    /**
     * Unity Scene 이동
     *
     * @param sceneName
     */
    public void CallChangeScene(String sceneName) {
        SendMessage("CallChangeScene", sceneName);
    }

    /**
     * Unity 기본화면으로 이동
     */
    public void ChangeSceneDefault() {

        ChangeSceneDefault("");
    }

    /**
     * Unity 기본화면으로 이동
     */
    public void ChangeSceneDefault(String strMsg) {
        if (strMsg == null || strMsg.isEmpty()) {
            strMsg = "";
        }
        SendMessage("CallChangeSceneDefault", strMsg);
    }

    /**
     * Unity 매트리스 분석으로 이동
     */
    public void ChangeSceneARMattress() {
        ChangeSceneARMattress("");
    }

    /**
     * Unity 매트리스 분석으로 이동
     */
    public void ChangeSceneARMattress(String strMsg) {
        if (strMsg == null || strMsg.isEmpty()) {
            strMsg = "";
        }
        SendMessage("CallChangeSceneARMattress", strMsg);
    }

    /**
     * Unity 홈에디터로 이동
     */
    public void ChangeSceneHomeEditor(String strMsg) {
        if (strMsg == null || strMsg.isEmpty()) {
            strMsg = "";
        }
        SendMessage("CallChangeSceneHomeEditor", strMsg);
    }

    /**
     * Unity 어반베이스 홈에디터로 이동
     */
    public void CallChangeSceneUrbanHomeEditor(String strMsg) {
        if (strMsg == null || strMsg.isEmpty()) {
            strMsg = "";
        }
        SendMessage("CallChangeSceneUrbanHomeEditor", strMsg);
    }

    //홈에디터

    /**
     * 도면 로드
     *
     * @param jsonString
     */
    public void LoadHomeUnit(String jsonString) {
        SendMessage("LoadHomeUnit", jsonString);
    }

    /**
     * 제품 로드
     *
     * @param jsonString
     */
    public void LoadHomeProduct(String jsonString) {
        Log.v(this.getClass().getName(), "Send Product JSON - " + jsonString);
        SendMessage("LoadHomeProduct", jsonString);
    }

    /**
     * 홈에디터 - 도면 벽지 변경
     *
     * @param jsonString
     */
    public void ChangeWallPaperStyle(String jsonString) {
        Log.v(this.getClass().getName(), "Send Product JSON - " + jsonString);
        SendMessage("ChangeWallPaperStyle", jsonString);
    }

    /**
     * 제품(가구) 로드
     *
     * @param jsonString
     */
    public void LoadHomeFurniture(String jsonString) {
        Log.v(this.getClass().getName(), "Send Furniture JSON - " + jsonString);
        SendMessage("LoadHomeFurniture", jsonString);
    }

    /**
     * 홈에디터 - 도면 바닥재 변경
     *
     * @param jsonString
     */
    public void ChangeFloorStyle(String jsonString) {
        Log.v(this.getClass().getName(), "Send Product JSON - " + jsonString);
        SendMessage("ChangeFloorStyle", jsonString);
    }

    /**
     * 화면 초기화
     */
    public void ClearHomeEditor() {
        SendMessage("ClearHomeEditor", "");
    }

    /**
     * 화면 설정 적용
     * TODO:옵션 관련한 부분은 변경 될 수도 있음.
     */
    public void LoadHomeOption(String jsonString) {
        SendMessage("LoadHomeOption", jsonString);
    }

    /**
     * 화면 설정 적용 - 사양
     */
    public void LoadHomeOptionEffect(QualityOption option) {

        String sendMsg = "";
        if (option.equals(QualityOption.High)) {
            sendMsg = "2";
        } else if (option.equals(QualityOption.Middle)) {
            sendMsg = "1";
        } else if (option.equals(QualityOption.Low)) {
            sendMsg = "0";
        } else {
            sendMsg = "1";
        }

        SendMessage("LoadHomeOptionEffect", sendMsg);
    }

    /**
     * 화면 설정 적용 - 사양
     */
    public void LoadHomeOptionEffect(String option) {
        SendMessage("LoadHomeOptionEffect", option);
    }

    /**
     * 화면 설정 적용 - 벽 충돌
     *
     * @param strBool
     */
    public void LoadHomeOptionCollision(String strBool) {
        SendMessage("LoadHomeOptionCollision", strBool);
    }

    /**
     * 화면 설정 적용 - 벽 충돌
     *
     * @param isBool
     */
    public void LoadHomeOptionCollision(boolean isBool) {
        String sendMsg = "";
        if (isBool) {
            sendMsg = "true";
        } else {
            sendMsg = "false";
        }

        SendMessage("LoadHomeOptionCollision", sendMsg);
    }


    public void CallUnityLotationLandscape() {
        SendMessage("CallUnityLotationLandscape", "");
    }

    public void CallUnityLotationPortrait() {
        SendMessage("CallUnityLotationPortrait", "");
    }


    /*
    public void SetSDKMode(String mode)
    {
        UnityPlayer.UnitySendMessage("GeneralSetting", "SetSDKMode", mode); //"productview" homedesign
    }

    public void SetProductViewerMode()
    {
        UnityPlayer.UnitySendMessage("UIController", "ProductViewerMode", "");
    }

    public void LoadFrameAndMattress()
    {
        String json = "{\"frameName\":\"VENATO\", \"size\":\"K\", \"color\":\"OakNatural\", \"mattress\":\"ROYALACE380\"}";
        UnityPlayer.UnitySendMessage("UIController", "SetFrameAndMattressOption", json);
    }
    */


    /**
     * 안드로이드 -> 유니티 호출(MobileManager 대상)
     *
     * @param methodName
     * @param param
     */
    public void SendMessage(String methodName, String param) {
        SendMessage("MobileManager", methodName, param);
    }

    /**
     * 안드로이드 -> 유니티 호출
     *
     * @param objName
     * @param methodName
     * @param param
     */
    public void SendMessage(String objName, String methodName, String param) {
        if (objName == null
                && objName.isEmpty()
                && methodName == null
                && methodName.isEmpty()) {
            return;
        }

        if (param == null
                && param.isEmpty()) {
            param = "";
        }

        UnityPlayer.UnitySendMessage(objName, methodName, param);
    }


    /**
     * Unity Ecco Test용
     *
     * @param msg
     */
    public void CallTest(String msg) {
        String returnMethod = "CallEcco";
        String returnMsg = "정의 되지 않은 이벤트 : " + msg;


        if ("LoadRoom".equals(msg)) {//방 호출
            returnMethod = "LoadHomeUnit";
            returnMsg = "{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1c271da53ab8ad712e9.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}";
        } else if ("LoadProduct".equals(msg)) {//제품 호출
            returnMethod = "LoadHomeProduct";
            returnMsg = "{\"frame\":{\"frameid\": \"3d74a410-a1f3-11e9-a718-9ba302dbf24d\",\"path\": \"/product/LUNATO3_K_Gray.zip\",\"sfbPath\": \"/product/###.sfb\",\"3dModelPath\": \"\",\"thumbnail\": \"/product/LUNATO3_K_Gray_thumbnail.png\",\"catalogThumbnail\": \"/product/LUNATO3_K_Gray.png\",\"name\": \"LUNATO3\",\"size\": \"K\",\"color\": \"Gray\",\"isDisplay\": true,\"mattressOffset\": [3.817524,23.96977,331.5197],\"mattressScale\": 1},\"mattress\":{\"mattress\": \"9a113be0-c7cb-11e9-a8ed-bb36fb6c62a7\",\"path\": \"/product/HT3_K.zip\",\"sfbPath\": \"/product/###.sfb\",\"3dModelPath\": \"\",\"name\": \"HT3\",\"size\": \"K\"}}";
        } else if ("ClearAll".equals(msg)) {//전체 삭제
            returnMethod = "ClearHomeEditor";
            returnMsg = "";//값 없음
        } else if ("OptionHigh".equals(msg)) {//옵션
            returnMethod = "LoadHomeOptionEffect";
            returnMsg = "0";//하
        } else if ("OptionMiddle".equals(msg)) {//옵션
            returnMethod = "LoadHomeOptionEffect";
            returnMsg = "1";//중
        } else if ("OptionLow".equals(msg)) {//옵션
            returnMethod = "LoadHomeOptionEffect";
            returnMsg = "2";//상
        } else if ("OptionCollision".equals(msg)) {//충돌
            returnMethod = "LoadHomeOptionCollision";

            Random generator = new Random();
            int num1 = generator.nextInt(10) + 1;
            if (num1 % 2 == 0) {
                returnMsg = "true";
            } else {
                returnMsg = "false";
            }
        } else if ("WallPaper".equals(msg)) {//옵션
            returnMethod = "ChangeFloorStyle";
            returnMsg = "{\"material\":{\"id\":\"61100\",\"thumbnail_path\":\"thumbnail/material/61100/thumbnail.jpg\",\"title\":\"세라마블 라이트그레이 1 HQ\",\"description\":\"마루\",\"type\":\"wall\",\"data\":{\"colorDiffuse\":[1,1,1],\"colorSpecular\":[0,0,0],\"colorEmissive\":[0,0,0],\"shading\":\"phong\",\"specularCoef\":10,\"transparent\":false,\"opacity\":1,\"flatShading\":false,\"flipSided\":false,\"doubleSided\":false,\"DbgColor\":\"\",\"DbgIndex\":0,\"opticalDensity\":0,\"illumination\":\"\",\"blending\":1,\"depthTest\":true,\"depthWrite\":true,\"colorWrite\":true,\"reflectivity\":1,\"visible\":true,\"vertexColors\":false,\"mapDiffuse\":\"material/61100/0d3cdb251fffdbc2b30555fclightgray.jpg\",\"mapDiffuseRepeat\":[6,6],\"mapDiffuseOffset\":[0,0],\"mapDiffuseWrap\":[\"repeat\",\"repeat\"],\"mapDiffuseAnisotropy\":2,\"mapEmissive\":\"\",\"mapEmissiveRepeat\":[1,1],\"mapEmissiveOffset\":[0,0],\"mapEmissiveWrap\":[\"repeat\",\"repeat\"],\"mapEmissiveAnisotropy\":2,\"mapLight\":\"\",\"mapLightRepeat\":[1,1],\"mapLightOffset\":[0,0],\"mapLightWrap\":[\"repeat\",\"repeat\"],\"mapLightAnisotropy\":2,\"mapAO\":\"\",\"mapAORepeat\":[1,1],\"mapAOOffset\":[0,0],\"mapAOWrap\":[\"repeat\",\"repeat\"],\"mapAOAnisotropy\":2,\"mapBump\":\"material/61100/0d3cdb251fffdbc2b30555fclightgrayb.jpg\",\"mapBumpScale\":10,\"mapBumpRepeat\":[1,1],\"mapBumpOffset\":[0,0],\"mapBumpWrap\":[\"repeat\",\"repeat\"],\"mapBumpAnisotropy\":2,\"mapNormal\":\"material/61100/0d3cdb251fffdbc2b30555fclightgraybn.jpg\",\"mapNormalFactor\":1,\"mapNormalRepeat\":[1,1],\"mapNormalOffset\":[0,0],\"mapNormalWrap\":[\"repeat\",\"repeat\"],\"mapNormalAnisotropy\":2,\"mapSpecular\":\"\",\"mapSpecularRepeat\":[1,1],\"mapSpecularOffset\":[0,0],\"mapSpecularWrap\":[\"repeat\",\"repeat\"],\"mapSpecularAnisotropy\":2,\"mapRoughness\":\"\",\"mapRoughnessRepeat\":[1,1],\"mapRoughnessOffset\":[0,0],\"mapRoughnessWrap\":[\"repeat\",\"repeat\"],\"mapRoughnessAnisotropy\":2,\"mapMetalness\":\"\",\"mapMetalnessRepeat\":[1,1],\"mapMetalnessOffset\":[0,0],\"mapMetalnessWrap\":[\"repeat\",\"repeat\"],\"mapMetalnessAnisotropy\":2,\"mapAlpha\":\"\",\"mapAlphaRepeat\":[1,1],\"mapAlphaOffset\":[0,0],\"mapAlphaWrap\":[\"repeat\",\"repeat\"],\"mapAlphaAnisotropy\":2,\"wireframe\":false,\"metalness\":0,\"smoothness\":0.5}}}";
        } else if ("UnitFloor".equals(msg)) {//옵션
            returnMethod = "ChangeFloorStyle";
            returnMsg = "{\"material\":{\"id\":\"65100\",\"thumbnail_path\":\"thumbnail/material/65100/thumbnail.jpg\",\"title\":\"세라마블 라이트그레이 1 HQ\",\"description\":\"마루\",\"type\":\"floor\",\"data\":{\"colorDiffuse\":[1,1,1],\"colorSpecular\":[0,0,0],\"colorEmissive\":[0,0,0],\"shading\":\"phong\",\"specularCoef\":10,\"transparent\":false,\"opacity\":1,\"flatShading\":false,\"flipSided\":false,\"doubleSided\":false,\"DbgColor\":\"\",\"DbgIndex\":0,\"opticalDensity\":0,\"illumination\":\"\",\"blending\":1,\"depthTest\":true,\"depthWrite\":true,\"colorWrite\":true,\"reflectivity\":1,\"visible\":true,\"vertexColors\":false,\"mapDiffuse\":\"material/65100/0d3cdb251fffdbc2b30555fclightgray.jpg\",\"mapDiffuseRepeat\":[6,6],\"mapDiffuseOffset\":[0,0],\"mapDiffuseWrap\":[\"repeat\",\"repeat\"],\"mapDiffuseAnisotropy\":2,\"mapEmissive\":\"\",\"mapEmissiveRepeat\":[1,1],\"mapEmissiveOffset\":[0,0],\"mapEmissiveWrap\":[\"repeat\",\"repeat\"],\"mapEmissiveAnisotropy\":2,\"mapLight\":\"\",\"mapLightRepeat\":[1,1],\"mapLightOffset\":[0,0],\"mapLightWrap\":[\"repeat\",\"repeat\"],\"mapLightAnisotropy\":2,\"mapAO\":\"\",\"mapAORepeat\":[1,1],\"mapAOOffset\":[0,0],\"mapAOWrap\":[\"repeat\",\"repeat\"],\"mapAOAnisotropy\":2,\"mapBump\":\"material/65100/0d3cdb251fffdbc2b30555fclightgrayb.jpg\",\"mapBumpScale\":10,\"mapBumpRepeat\":[1,1],\"mapBumpOffset\":[0,0],\"mapBumpWrap\":[\"repeat\",\"repeat\"],\"mapBumpAnisotropy\":2,\"mapNormal\":\"material/65100/0d3cdb251fffdbc2b30555fclightgraybn.jpg\",\"mapNormalFactor\":1,\"mapNormalRepeat\":[1,1],\"mapNormalOffset\":[0,0],\"mapNormalWrap\":[\"repeat\",\"repeat\"],\"mapNormalAnisotropy\":2,\"mapSpecular\":\"\",\"mapSpecularRepeat\":[1,1],\"mapSpecularOffset\":[0,0],\"mapSpecularWrap\":[\"repeat\",\"repeat\"],\"mapSpecularAnisotropy\":2,\"mapRoughness\":\"\",\"mapRoughnessRepeat\":[1,1],\"mapRoughnessOffset\":[0,0],\"mapRoughnessWrap\":[\"repeat\",\"repeat\"],\"mapRoughnessAnisotropy\":2,\"mapMetalness\":\"\",\"mapMetalnessRepeat\":[1,1],\"mapMetalnessOffset\":[0,0],\"mapMetalnessWrap\":[\"repeat\",\"repeat\"],\"mapMetalnessAnisotropy\":2,\"mapAlpha\":\"\",\"mapAlphaRepeat\":[1,1],\"mapAlphaOffset\":[0,0],\"mapAlphaWrap\":[\"repeat\",\"repeat\"],\"mapAlphaAnisotropy\":2,\"wireframe\":false,\"metalness\":0,\"smoothness\":0.5}}}";//상
        }


        SendMessage(returnMethod, returnMsg);
    }


    /**
     * Unity 어반베이스 제품 미리보기 이동
     */
    public void CallChangeSceneUrbanProductViewer(String strMsg) {
        if (strMsg == null || strMsg.isEmpty()) {
            strMsg = "";
        }
        SendMessage("CallChangeSceneUrbanProductViewer", strMsg);
    }

    /**
     * Unity 어반베이스 제품 미리보기 이동
     */
    public void CallSetFrameAndMattress(String strMsg) {
        if (strMsg == null || strMsg.isEmpty()) {
            strMsg = "";
        }
        SendMessage("ProductViewController", "SetFrameAndMattress", strMsg);
    }

}