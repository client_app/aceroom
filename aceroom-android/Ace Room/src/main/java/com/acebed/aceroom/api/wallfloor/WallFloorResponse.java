package com.acebed.aceroom.api.wallfloor;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import kr.co.xcolo.android.fw.mvvm.retrofit.GenericRetrofitModel;
import lombok.Data;
/**
 * WallFloorResponse
 * 벽지 및 바닥재 Response
 * @Author Xcolo DK
 */
@Data
public class WallFloorResponse implements GenericRetrofitModel {
    private List<WallFloorListData> wallpaperList;
    private List<WallFloorListData> floorList;


    @Override
    public boolean isError() {
        return false;
    }

    @Override
    public String getErrorMessage() {
        return null;
    }

    @Data
    public static class WallFloorListData implements Serializable {
        private String id;
        @SerializedName("thumbnail_path") private String thumbnailPath;
        private String title;
        private String description;
        private String type;
        private String filter;
        private DataResponse data;

    }
    @Data
    public static class DataResponse implements Serializable {
        private List<Integer> colorDiffuse ;
        private List<Integer> colorSpecular ;
        private List<Integer> colorEmissive ;
        private String shading;
        private Integer specularCoef;
        private Boolean transparent;
        private Integer opacity;
        private Boolean flatShading;
        private Boolean flipSided;
        private Boolean doubleSided;
        private String dbgColor;
        private Integer dbgIndex;
        private Integer opticalDensity;
        private String illumination;
        private Integer blending;
        private Boolean depthTest;
        private Boolean depthWrite;
        private Boolean colorWrite;
        private Integer reflectivity;
        private Boolean visible;
        private Boolean vertexColors;
        private String mapDiffuse;
        private List<Double> mapDiffuseRepeat ;
        private List<Integer> mapDiffuseOffset ;
        private List<String> mapDiffuseWrap ;
        private Integer mapDiffuseAnisotropy;
        private String mapEmissive;
        private List<Integer> mapEmissiveRepeat ;
        private List<Integer> mapEmissiveOffset ;
        private List<String> mapEmissiveWrap ;
        private Integer mapEmissiveAnisotropy;
        private String mapLight;
        private List<Integer> mapLightRepeat ;
        private List<Integer> mapLightOffset ;
        private List<String> mapLightWrap ;
        private Integer mapLightAnisotropy;
        private String mapAO;
        private List<Integer> mapAORepeat ;
        private List<Integer> mapAOOffset ;
        private List<String> mapAOWrap ;
        private Integer mapAOAnisotropy;
        private String mapBump;
        private Integer mapBumpScale;
        private List<Integer> mapBumpRepeat ;
        private List<Integer> mapBumpOffset ;
        private List<String> mapBumpWrap ;
        private Integer mapBumpAnisotropy;
        private String mapNormal;
        private Integer mapNormalFactor;
        private List<Integer> mapNormalRepeat ;
        private List<Integer> mapNormalOffset ;
        private List<String> mapNormalWrap ;
        private Integer mapNormalAnisotropy;
        private String mapSpecular;
        private List<Integer> mapSpecularRepeat ;
        private List<Integer> mapSpecularOffset ;
        private List<String> mapSpecularWrap ;
        private Integer mapSpecularAnisotropy;
        private String mapRoughness;
        private List<Integer> mapRoughnessRepeat ;
        private List<Integer> mapRoughnessOffset ;
        private List<String> mapRoughnessWrap ;
        private Integer mapRoughnessAnisotropy;
        private String mapMetalness;
        private List<Integer> mapMetalnessRepeat ;
        private List<Integer> mapMetalnessOffset ;
        private List<String> mapMetalnessWrap ;
        private Integer mapMetalnessAnisotropy;
        private String mapAlpha;
        private List<Integer> mapAlphaRepeat ;
        private List<Integer> mapAlphaOffset ;
        private List<String> mapAlphaWrap ;
        private Integer mapAlphaAnisotropy;
        private Boolean wireframe;
        private Integer metalness;
        private Double smoothness;
    }

}
