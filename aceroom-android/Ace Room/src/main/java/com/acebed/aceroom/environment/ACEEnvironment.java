package com.acebed.aceroom.environment;

public class ACEEnvironment {

    // Url info
    public final static String BASE_URL_OF_MAP = "https://apis.urbanbase.com/";
    public final static String BASE_URL_OF_CATALOG = "http://dtribe.youyoung.net/";
    public final static String BASE_URL_OF_DETAIL = "http://dtribe.youyoung.net/";
    //public final static String BASE_URL_OF_BUILDINGS = "https://d1wjr0mdrcotdx.cloudfront.net/"; //as-is
    public final static String BASE_URL_OF_BUILDINGS = "https://d3bcljm2ejdkqk.cloudfront.net/";

    // Keyword
    public final static String CATEGORY_TOTAL = "TOTAL";

    public final static int PAGE_MODE_PRODUCT = 0;
    public final static int PAGE_MODE_WALLFLOOR = 1;
    public final static int PAGE_MODE_FURNITURE = 2;


    //Request Code
    public final static int STORAGE_REQUEST_CODE = 7777;

}
