package com.acebed.aceroom.api.buildings;

import androidx.lifecycle.LiveData;

import com.acebed.aceroom.api.HomeApi;

import kr.co.xcolo.android.fw.mvvm.retrofit.ApiService;
import kr.co.xcolo.android.fw.mvvm.retrofit.GenericRetrofitRepository;
import kr.co.xcolo.android.fw.mvvm.status.XcoloStatusCallback;
import retrofit2.Call;

public class BuildingsRepository extends GenericRetrofitRepository<BuildingsResponse> {
    private HomeApi mHomeService = ApiService.createService(HomeApi.class);
    private String boundsValue;

    public static BuildingsRepository createInstance(String boundsValue, XcoloStatusCallback statusCallback) {
        BuildingsRepository repository = new BuildingsRepository();

        repository.boundsValue = boundsValue;
        repository.setStatusCallback(statusCallback);

        return repository;
    }

    public LiveData<BuildingsResponse> onBuildingsRequest() {
        return doRequest();
    }

    @Override
    protected Call<BuildingsResponse> makeRequest() {
        return mHomeService.buildings(boundsValue,"[\"id\",\"latitude\",\"longitude\",\"building_uuid\"]", "{\"country\":{}}");
    }
}
