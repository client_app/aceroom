//
//  AcePlugin.h
//  Unity-iPhone
//
//  Created by 김태원 on 09/09/2019.
//

#import <Foundation/Foundation.h>

/* Unity Scene Name*/
#define SCENE_DEFAULT @"Default"
#define SCENE_AR_MATTRESS @"ARMattress"
#define SCENE_HOME_EDITOR @"HomeEditor"
#define NAME_AR_LENS @"ArLens"


typedef NS_ENUM(NSInteger, HomeEditorQuality)
{
    kHomeEditorQualityLow = 0,
    kHomeEditorQualityMiddle = 1,
    kHomeEditorQualityHigh = 2
};

/*!
   유니티 플러그인과 네이티브의 통신을 위한 플러그인
    ***주의***
        수정 후 Unity의 Plugin 경로에도 적용해야 소스코드가 유지된다.(Unity 빌드 시 마다 덮어 씌워진다)
    ***********
       Unity에서 호출 - UnityuPlugin에서 UnityBridge를 호출하며, UnityBridge는각 화면을 호출
       Native에서 호출 -각 화면에서 UnityBridge를 호출 후 UnityPlugin을 호출하여 전달
 
    안드로이드와 iOS의 동일 규칙을 유지하기 위해, 리턴값은 없으며 값이 필요할 경우 대응하는 클러그인 메소드를 호출함.
*/
@interface AcePlugin : NSObject

+ (instancetype)sharedInstance;
/*!
   기기에 노치가 있는지 여부를 전달
*/
-(void)unitySetIsNotch:(BOOL)isNotch;
/*!
   테스트용 플러그인
*/
-(void)sendMassageForObj:(NSString *)objName andMethod:(NSString *)methodName andParam:(NSString *)message;
/*!
   에코 테스트용 플러그인
*/
-(void)callEccoParam:(NSString *)param;
/*!
   유니티에서 실행중인 Scene이 어떤건지 요청
*/
-(void)callNowScene;
/*!
   홈에디터 - 도면 정보 전달
*/
-(void)loadHomeUnitJson:(NSString *)jsonString;
/*!
   홈에디터 - 제품 정보 전달
*/
-(void)loadHomeProduct:(NSString *)jsonString;
/*!
   홈에디터 - 제품(가구) 정보 전달
*/
-(void)LoadHomeFurniture:(NSString *)jsonString;
/*!
   [사용안함]홈에디터 - 화면에 표시되어 있는 도면과 제품을 제거
*/
-(void)clearHomeEditor;

/*!
   [사용안함]유니티 화면 회전 - 가로
*/
-(void)callUnityLotationLandscape;
/*!
   [시용안함]유니티 화면 회전 - 세로
*/
-(void)callUnityLotationPortrait;

#pragma mark - Scene 이동
/*!
   DefaultScene(메인메뉴)로 이동
*/
-(void)changeSceneDefault;
//-(void)changeSceneDefault:(NSString *)strMsg;
/*!
   ARMattressScene(매트리스 분석) 화면으로 이동
*/
-(void)changeSceneARMattress;
//-(void)changeSceneARMattress:(NSString *)strMsg;
//-(void)changeSceneHomeEditor;
/*!
   HomeEditorScene 홈에디터 화면으로 이동
*/
-(void)changeSceneHomeEditor:(NSString *)strMsg;


#pragma mark - AR Vuforia

#pragma mark - Home Editor
/*!
   홈에디터 - 벽지 정보 전달
*/
-(void)changeWallPaperStyle:(NSString *)jsonString;
/*!
   홈에디터 - 바닥재 정보 전달
*/
-(void)changeFloorStyle:(NSString *)jsonString;
/*!
   홈에디터 - 환경설정의 사양 등의 정보 전달
*/
-(void)LoadHomeOptionEffect:(HomeEditorQuality)quality;

/*!
 * 화면 설정 적용 - 벽 충돌
 * @param isBool
 */
-(void)LoadHomeOptionCollision:(BOOL)isBool;

#if __cplusplus
extern "C"
{
#endif /* __cplusplus */
    
    void ace_UnitySendMessage(const char *name, const char *method, NSString *params);
    
    //
    
#if __cplusplus
}
#endif /* __cplusplus */

@end
