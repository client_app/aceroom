//
//  ARViewer.swift
//  ARViewer
//
//  Created by 이우석 on 11/12/2018.
//  Copyright © 2018 이우석. All rights reserved.
//

import Foundation
import ARKit
import SceneKit
import AFNetworking

enum FeatureType {
    case AR, QR
}

protocol ARLifeCycleDelegate {
    func viewWillAppear()
    func viewWillDisappear()
}

@objc public protocol ARDelegate {
    @objc optional func findPlane()
    @objc optional func closeARViewer()
    @objc optional func didPlaceFigure()
    @objc optional func selectProduct(selectedFigure: Figure?)
    @objc optional func rotateChanged(selectedFigure: Figure)
    @objc optional func scaleChanged(selectedFigure: Figure)
    @objc optional func productListUrl() -> String
    @objc optional func eventListUrl() -> String
    @objc optional func productSelected(name: String, type: String)
}

open class ARViewer: UIView {
    
    public var arSceneView: ARViewerSCNView
    public var session: ARSession {
        return arSceneView.session
    }
    
    public var loadedFigures = [Figure]()
    
    var screenCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    //let updateQueue = DispatchQueue(label: "com.urbanbase.ARViewer.\(NSDate().timeIntervalSince1970.intValue).updateQueue")
    let updateQueue = DispatchQueue(label: "com.urbanbase.ARViewer.\(NSDate().timeIntervalSince1970.intValue).updateQueue", qos: .userInteractive, attributes: [], autoreleaseFrequency: .inherit, target: nil)
    
    lazy var planeDetection = PlaneDetection(arView: self, planeMode: .None, enableFocusSquare: false)
    
    let figureInteraction: FigureInteraction
    
    weak var attachedView: UIView?
    
    var toBePlacedProduct: (assetId: String?, token: String?)? //uuid
    
    @objc public weak var buttonDelegate: ButtonSelectedDelegate?
    
    var mode: FeatureType = .AR
    
    override public init(frame: CGRect) {
        arSceneView = ARViewerSCNView(frame: frame)
        figureInteraction = FigureInteraction(sceneView: arSceneView)
        super.init(frame: frame)
        backgroundColor = .clear
        addSubview(arSceneView)
        arSceneView.fitSuperview()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        arSceneView = ARViewerSCNView(frame: UIScreen.main.bounds)
        figureInteraction = FigureInteraction(sceneView: arSceneView)
        super.init(coder: aDecoder)
        backgroundColor = .clear
        addSubview(arSceneView)
        arSceneView.fitSuperview()
    }
    
    deinit {
        print("ws.lee : ARViewer \(#function)")
    }
}
