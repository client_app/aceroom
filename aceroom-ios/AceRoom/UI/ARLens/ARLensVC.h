//
//  ARLensVC.h
//  Unity-iPhone
//
//  Created by 김태원 on 04/10/2019.
//

#import "XCCommonVC.h"
#import <ARViewer.framework/Headers/ARViewer.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   AR 침대배치
    - 어반베이스 Lens 프레임워크 안에 모든 UI가 있으며, 해당 클래스는 Lens 프레임워크를 화면에 배치하기만 함.
*/
@interface ARLensVC : XCCommonVC <ButtonSelectedDelegate, ARDelegate>

@property (strong, nonatomic) ARViewer *arView;

@end

NS_ASSUME_NONNULL_END
