//
//  ARLensVC.m
//  Unity-iPhone
//
//  Created by 김태원 on 04/10/2019.
//

#import "ARLensVC.h"


@interface ARLensVC () 
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) UIButton *buyButton;
@property (strong, nonatomic) UIView *containerView; // ARViewer 내부의 가구 정보 페이지
@property (strong, nonatomic) NSString *buyProductName;
@property (strong, nonatomic) NSString *buyProductLandingTarget;

@property (strong, nonatomic) NSMutableArray *productList;
@property (strong, nonatomic) NSMutableArray *roomProductList;

@end

@implementation ARLensVC

#pragma mark - LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initViews];
    [self requestProducts];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_arView viewWillAppear];

    [self updateBuyButtonHiddenState];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
	[_arView viewWillDisappear];
//	[_arView setFrame:CGRectZero];
}

-(void)initViews
{
    //    self.arView = [[ARViewer alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    [_arView setFrame:[[UIScreen mainScreen] bounds]];
    [self.mainView addSubview:_arView];
    [self.arView setButtonDelegate:self];
    
    
    self.containerView = [self findContainerView];
}

#pragma mark networks
/*!
   어반베이스 - 제품목록 조회
 */
-(void) requestProducts
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    [manager GET:[NSString stringWithFormat:@"%@%@",URBAN_PRODUCT_API_DOMAIN, API_BED]
      parameters:nil
        progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
        //            NSLog(@"responseObject ================= %@ ================= ", responseObject);

        NSMutableDictionary *rDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
        self.productList = [[NSMutableArray alloc] init];
        self.roomProductList = [[NSMutableArray alloc] init];

        if ( [rDic objectForKey:@"brand"]!=nil )
        {
            //product 세팅
            NSArray *arr_products = [rDic objectForKey:@"products"];

            if ( arr_products && [arr_products count] > 0 )
            {
                [self.productList addObjectsFromArray:arr_products];
            }
            else
            {
                [self.view makeToast:@"products가 없습니다."];
            }

            //            //매트리스 세팅.
            //            NSArray *arr_mtList = [rDic objectForKey:@"mattressList"];
            //            [self.productList addObjectsFromArray:arr_mtList];

            NSArray *arr_roomsetList = [rDic objectForKey:@"roomsetList"];
            if ( arr_products && [arr_products count]>0 )
            {
                [self.roomProductList addObjectsFromArray:arr_roomsetList];
            }
            else
            {
                [self.view makeToast:@"가구 정보가 없습니다."];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:self.productList forKey:@"productList"];
            [[NSUserDefaults standardUserDefaults] setObject:self.roomProductList forKey:@"roomProductList"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            [self.view makeToast:@"데이터 오류입니다. 다시 시도해보세요."];
        }

    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.view makeToast:[NSString stringWithFormat:@"%@", error]];
    }];
}


#pragma mark - 구매 버튼
- (void)setBuyButton
{
    
    if (self.buyButton.superview != nil) {
        [self updateBuyButtonHiddenState];
        return;
    }
    
    CGFloat buttonSize = 56;
    CGFloat buttonMargin = -41;
    CGFloat buttonMarginX = -22;
    CGRect buttonRect = CGRectMake(0, 0, buttonSize, buttonSize);
    
    UIWindow *window = UIApplication.sharedApplication.keyWindow;
    CGFloat bottomPadding = window.safeAreaInsets.bottom;
    
    _buyButton = [[UIButton alloc] initWithFrame: buttonRect];
    [_buyButton setBackgroundImage:[UIImage imageNamed:@"sub01ButtonBuy"] forState:UIControlStateNormal];
    [_buyButton addTarget:self action:@selector(onClickBuyButton:) forControlEvents:UIControlEventTouchUpInside];
    self.mainView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.mainView addSubview:self.buyButton];
    
    _buyButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    UILayoutGuide *guide = self.view.layoutMarginsGuide;
    [_buyButton.heightAnchor constraintEqualToConstant:buttonSize].active = YES;
    [_buyButton.widthAnchor constraintEqualToConstant:buttonSize].active = YES;
    [_buyButton.leadingAnchor constraintEqualToAnchor:self.mainView.leadingAnchor constant:20].active = YES;
    
    if ( bottomPadding > 0 )
    {
//        NSLog(@"🐥 youairy - X 기종");
        [_buyButton.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor constant:buttonMarginX].active = YES;
    }
    else {
//        NSLog(@"🐥 youairy - X 기종 아님!!!!!!!!!!!!");
        [_buyButton.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor constant:buttonMargin].active = YES;
    }
    
    
//    UILayoutGuide * margins = self.view.safeAreaLayoutGuide;
    
    [self.view layoutIfNeeded];
}

- (IBAction)onClickBuyButton:(id)sender
{
    [self openWebBrowser:[NSURL URLWithString:_buyProductLandingTarget]];
}

- (void)openWebBrowser:(NSURL *)url
{
    
    NSLog(@"openWebBrowser");
    NSLog(@"%@", url);
    UIApplication *application = [UIApplication sharedApplication];
    [application openURL:url options:@{} completionHandler:nil];
}

- (void)updateBuyButtonHiddenState
{
    UIView *containerView = [self findContainerView];
    CGFloat position = containerView.layer.position.y;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    if ( position > screenHeight )
    {
//        NSLog(@"🐥 YOUAIRY - 카메라 보임");
        [_buyButton setHidden:NO];
    }
    else
    {
//        NSLog(@"🐥 YOUAIRY - 카메라 가림!");
        [_buyButton setHidden:YES];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self updateBuyButtonHiddenState];
    });
}

- (UIView *)findContainerView
{
    UICollectionView *collectionView = _arView.subviews.lastObject.subviews.lastObject.subviews.lastObject.subviews.lastObject;
    UIView *containerView = collectionView.superview;
    
    return containerView;
}

#pragma mark - AR Lens Delegate(ButtonSelectedDelegate) - 화면이동 액션(닫기버튼) 컨트롤용 델리게이트
- (void)backButtonSelectedWithSender:(UIButton * _Nonnull)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tutorialButtonSelectedWithSender:(UIButton * _Nonnull)sender
{
    [kUnityBridge OpenTutorial:NAME_AR_LENS closeHandler:^{
        
    }];
}

#pragma mark - AR Lens Delegate(ARDelegate) API 호출용 URL 델리게이트

- (NSString *)productListUrl
{
	return [NSString stringWithFormat:@"%@%@",URBAN_PRODUCT_API_DOMAIN, API_BED];
}

- (NSString *)eventListUrl
{
	return [NSString stringWithFormat:@"%@%@",URBAN_PRODUCT_API_DOMAIN, API_EVENT];
}

- (void)productSelectedWithName:(NSString *)name type:(NSString *)type
{
    NSMutableArray *temp = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"productList"]];
    _buyProductLandingTarget = nil;

    NSMutableArray *roomTemp = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"roomProductList"]];
    
    if ([type  isEqual: @"roomset"]) {
        for (NSDictionary *dic in roomTemp) {
            if (![[dic objectForKey:@"catalogName"] isEqual:name]) continue;
            _buyProductLandingTarget = [dic objectForKey:@"landingTargetiOS"];
        }
    }else{
        for (NSDictionary *dic in temp) {
            if (![[dic objectForKey:@"catalogName"] isEqual:name]) continue;
            _buyProductLandingTarget = [dic objectForKey:@"landingTargetiOS"];
        }
    }
    
    
    
    
    if (self.buyProductLandingTarget == nil || self.buyProductLandingTarget.validateEmptyText) {
        [self.buyButton removeFromSuperview];
    }else{
        [self setBuyButton];
    }
}
@end

 
