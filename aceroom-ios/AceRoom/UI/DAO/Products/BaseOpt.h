//
//  BaseOpt.h
//  Unity-iPhone
//
//  Created by SangYeonE on 14/10/2019.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   제폼목록 API 의 도메인 클래스.
    파라미터 관련 어반베이스에 문의
*/
@interface BaseOpt : NSObject
@property (nonatomic, strong) NSString *size;
@property (nonatomic, strong) NSString *color;
@property (nonatomic, strong) NSString *mattress;
@property (nonatomic, strong) NSString *thumbnail;
@end

NS_ASSUME_NONNULL_END
