//
//  roomset_entity.h
//  Unity-iPhone
//
//  Created by 김태원 on 2020/02/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   제폼목록 API 의 도메인 클래스.(가구)
    파라미터 관련 어반베이스에 문의
*/
@interface roomset_entity : JSONModel
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *catalogThumbnail;
@property (nonatomic, strong) NSMutableArray *descList;
@property (nonatomic, strong) NSString *catalogName;
@property (nonatomic, strong) NSMutableArray *colorList;
@property (nonatomic, strong) NSDictionary *baseOpt;
@property (nonatomic, strong) NSString *filter;
@property (nonatomic, strong) NSMutableArray *furnitureList;
@end

NS_ASSUME_NONNULL_END
