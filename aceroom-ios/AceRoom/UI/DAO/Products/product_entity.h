//
//  product_entity.h
//  Unity-iPhone
//
//  Created by SangYeonE on 14/10/2019.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   제폼목록 API 의 도메인 클래스.
    파라미터 관련 어반베이스에 문의
*/
@interface product_entity : JSONModel
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *catalogThumbnail;
@property (nonatomic, strong) NSMutableArray *descList;
@property (nonatomic, strong) NSString *catalogName;
@property (nonatomic, strong) NSMutableArray*sizeList;
@property (nonatomic, strong) NSMutableArray*colorList;
@property (nonatomic, strong) NSDictionary *baseOpt;
@property (nonatomic, strong) NSString*filter;
@property (nonatomic, strong) NSMutableArray *frameList;
@end

NS_ASSUME_NONNULL_END
