//
//  mattressOffset.h
//  Unity-iPhone
//
//  Created by SangYeonE on 14/10/2019.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   제폼목록 API 의 도메인 클래스.
    파라미터 관련 어반베이스에 문의
*/
@interface MattressOffset : NSObject
@property (nonatomic) 		  int 		x;
@property (nonatomic) 		  int 		y;
@property (nonatomic) 		  int 		z;
@end

NS_ASSUME_NONNULL_END
