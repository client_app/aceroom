//
//  building_entity_detail.h
//  Unity-iPhone
//
//  Created by 김태원 on 2020/05/11.
//

#import <JSONModel/JSONModel.h>
#import "building_entity.h"

NS_ASSUME_NONNULL_BEGIN

@interface building_entity_detail : JSONModel
@property (nonatomic, strong) NSString <Optional>	*id;
@property (nonatomic, strong) NSString 	*building_name;
@property (nonatomic, strong) NSString <Optional>	*building_no;
@property (nonatomic, strong) NSString 	*road_address;
@property (nonatomic, strong) NSString 	*address;
@property (nonatomic)  		  double 	latitude;
@property (nonatomic) 		  double 	longitude;
@property (nonatomic, strong) NSString 	*construction_company;
@property (nonatomic, strong) NSString 	*building_completion_ym;
@property (nonatomic) 		  int 		total_household_count;
@property (nonatomic) 		  int 		total_dong_count;
@property (nonatomic, strong) NSString 	*m_building_type_id;
@property (nonatomic, strong) NSString 	*batl_ratio;
@property (nonatomic, strong) NSString 	*btl_ratio;
@property (nonatomic, strong) NSString 	*household_parking;
@property (nonatomic, strong) NSString 	*low_floor;
@property (nonatomic, strong) NSString 	*high_floor;
@property (nonatomic, strong) NSString 	*heat_type;
@property (nonatomic, strong) NSString 	*building_area;
@property (nonatomic, strong) NSString 	*country_id;
@property (nonatomic) BOOL publish;
@property (nonatomic) BOOL enabled;

//@property (nonatomic, strong) NSDate 	*created_date;
//@property (nonatomic, strong) NSDate 	<Optional> *updated_date;
//@property (nonatomic, strong) NSDate 	<Optional> *deleted_date;
@property (nonatomic, strong) NSString 	*building_uuid;
@property (nonatomic, strong) NSMutableArray <Optional> *building_areas;


//200508 BuildingsAPI 변경으로 추가되는 파라미터
//@property (nonatomic, strong) NSString <Optional> *completion_date;
//@property (nonatomic, strong) NSString <Optional> *country_locality_id;
//@property (nonatomic, strong) NSString <Optional> *description;
//@property (nonatomic, strong) NSString <Optional> *postal_code;
//@property (nonatomic, strong) NSString <Optional> *pre_sale_count;
//@property (nonatomic, strong) NSString <Optional> *sales_company;
//@property (nonatomic, strong) NSString <Optional> *site_area;
//@property (nonatomic, strong) NSString <Optional> *structure;
//@property (nonatomic, strong) NSString <Optional> *traffic;
//@property (nonatomic, strong) NSDate <Optional> *transfer_date;
//@property (nonatomic, strong) NSString <Optional> *url;
//@property (nonatomic, strong) NSString <Optional> *user_id;


-(CLLocationCoordinate2D) getPosition;
@end

NS_ASSUME_NONNULL_END
