//
//  building_entity_detail.m
//  Unity-iPhone
//
//  Created by 김태원 on 2020/05/11.
//

#import "building_entity_detail.h"

@implementation building_entity_detail
-(CLLocationCoordinate2D)getPosition
{
	CLLocationCoordinate2D location = CLLocationCoordinate2DMake(_latitude, _longitude);
	return location;
}
@end
