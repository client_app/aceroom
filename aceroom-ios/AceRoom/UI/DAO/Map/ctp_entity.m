//
//  ctp_entity.m
//  Unity-iPhone
//
//  Created by SangYeonE on 25/09/2019.
//

#import "ctp_entity.h"

@implementation ctp_entity
-(CLLocationCoordinate2D)getPosition
{
	CLLocationCoordinate2D location = CLLocationCoordinate2DMake(_lat, _lng);
	return location;
}
@end
