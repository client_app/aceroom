//
//  building_area.h
//  Unity-iPhone
//
//  Created by SangYeonE on 26/09/2019.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   Buildings API 의 도메인 클래스.
    파라미터 관련 어반베이스에 문의
*/
@interface building_area : JSONModel
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *building_id;
@property (nonatomic)		  double	supply_area;
@property (nonatomic)		  double	exclusive_area;
@property (nonatomic, strong) NSString *display_supply_area;
@property (nonatomic, strong) NSString *display_supply_area_pyeong;
@property (nonatomic, strong) NSString *exclusive_ratio;
@property (nonatomic, strong) NSString *m_entrance_type_id;
@property (nonatomic)		  BOOL	publish;
@property (nonatomic)		  BOOL	enabled;
//@property (nonatomic, strong) NSDate <Optional> *created_date;
//@property (nonatomic, strong) NSDate <Optional> *updated_date;
//@property (nonatomic, strong) NSDate <Optional> *deleted_date;
@property (nonatomic, strong) NSMutableArray <Optional> *units;
@end

NS_ASSUME_NONNULL_END


//"id","building_id","supply_area","exclusive_area","display_supply_area","display_supply_area_pyeong","exclusive_ratio","m_entrance_type_id","publish","enabled","household_count"
