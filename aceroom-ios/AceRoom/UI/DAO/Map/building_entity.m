//
//  building.m
//  Unity-iPhone
//
//  Created by SangYeonE on 26/09/2019.
//

#import "building_entity.h"

@implementation building_entity

-(CLLocationCoordinate2D)getPosition
{
	CLLocationCoordinate2D location = CLLocationCoordinate2DMake(_latitude, _longitude);
	return location;
}
@end
