//
//  ctp_entity.h
//  Unity-iPhone
//
//  Created by SangYeonE on 25/09/2019.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   Buildings API 의 도메인 클래스.
    파라미터 관련 어반베이스에 문의
*/
@interface ctp_entity : JSONModel
//@property (nonatomic) CLLocationCoordinate2D location;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) double lat;
@property (nonatomic) double lng;
@property (nonatomic, strong) NSString <Optional> *snippet;

-(CLLocationCoordinate2D) getPosition;
@end

NS_ASSUME_NONNULL_END
