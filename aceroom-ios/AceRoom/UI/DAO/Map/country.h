//
//  country.h
//  Unity-iPhone
//
//  Created by SangYeonE on 26/09/2019.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   Buildings API 의 도메인 클래스.
    파라미터 관련 어반베이스에 문의
*/
@interface country : JSONModel
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *calling_code;
@end

NS_ASSUME_NONNULL_END
