//
//  unit_attache.h
//  Unity-iPhone
//
//  Created by SangYeonE on 26/09/2019.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   Buildings API 의 도메인 클래스.
    파라미터 관련 어반베이스에 문의
*/
@interface unit_attache : JSONModel
//@property (nonatomic, strong) NSString *id                ;
@property (nonatomic, strong) NSString *unit_id           ;
@property (nonatomic, strong) NSString *file_path         ;
@property (nonatomic) 		  BOOL		is_default        ;
@property (nonatomic, strong) NSString *m_attach_type_id  ;
@property (nonatomic, strong) NSString *extension         ;
@property (nonatomic) 		  BOOL		enabled           ;
//@property (nonatomic, strong) NSDate 	*created_date      ;
//@property (nonatomic, strong) NSDate 	<Optional> *updated_date      ;
//@property (nonatomic, strong) NSDate 	<Optional> *deleted_date      ;
@property (nonatomic, strong) NSString <Optional> *signed_url;
@end
//"unit_id","file_path","is_default","m_attach_type_id","extension","enabled"

NS_ASSUME_NONNULL_END
