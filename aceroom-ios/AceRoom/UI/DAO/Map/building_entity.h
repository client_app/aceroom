//
//  building.h
//  Unity-iPhone
//
//  Created by SangYeonE on 26/09/2019.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   Buildings API 의 도메인 클래스.
    파라미터 관련 어반베이스에 문의
*/
@interface building_entity : JSONModel

@property (nonatomic, strong) NSString <Optional>	*id;
@property (nonatomic, strong) NSString 	*building_name;
@property (nonatomic, strong) NSString <Optional>	*building_no;
@property (nonatomic, strong) NSString 	*road_address;
@property (nonatomic, strong) NSString 	*address;
@property (nonatomic)  		  double 	latitude;
@property (nonatomic) 		  double 	longitude;
@property (nonatomic, strong) NSString 	*construction_company;
@property (nonatomic, strong) NSString 	*building_completion_ym;
@property (nonatomic) 		  int 		total_household_count;
@property (nonatomic) 		  int 		total_dong_count;
@property (nonatomic, strong) NSString 	*m_building_type_id;
@property (nonatomic, strong) NSString 	*batl_ratio;
@property (nonatomic, strong) NSString 	*btl_ratio;
@property (nonatomic, strong) NSString 	*household_parking;
@property (nonatomic, strong) NSString 	*low_floor;
@property (nonatomic, strong) NSString 	*high_floor;
@property (nonatomic, strong) NSString 	*heat_type;
@property (nonatomic, strong) NSString 	*building_area;
@property (nonatomic, strong) NSString 	*country_id;
@property (nonatomic) BOOL publish;
@property (nonatomic) BOOL enabled;
@property (nonatomic, strong) NSString 	*building_uuid;
@property (nonatomic, strong) NSDictionary *country;


//200508 BuildingsAPI 변경으로 제외되는 파라미터
//@property (nonatomic, strong) NSString <Optional>	*object_id;
//@property (nonatomic, strong) NSString <Optional>	*complexes_idx;
//@property (nonatomic, strong) NSMutableArray <Optional> *building_areas;
//@property (nonatomic, strong) NSDate 	<Optional> *created_date;
//@property (nonatomic, strong) NSDate 	<Optional> *updated_date;
//@property (nonatomic, strong) NSDate 	<Optional> *deleted_date;



//200508 BuildingsAPI 변경으로 추가되는 파라미터

-(CLLocationCoordinate2D) getPosition;

@end

NS_ASSUME_NONNULL_END
