//
//  unit.h
//  Unity-iPhone
//
//  Created by SangYeonE on 26/09/2019.
//

#import <Foundation/Foundation.h>
#import "engine_status.h"
#import "unit_attache.h"

NS_ASSUME_NONNULL_BEGIN
/*!
   Buildings API 의 도메인 클래스.
    파라미터 관련 어반베이스에 문의
*/
@interface unit_entity : JSONModel
//@property (nonatomic, strong) NSString *id;
//@property (nonatomic, strong) NSString *building_area_id;
@property (nonatomic, strong) NSString *unit_uuid;
//@property (nonatomic, strong) NSString <Optional> *user_id;
@property (nonatomic)		  int		room_count;
@property (nonatomic)		  int		bathroom_count;
@property (nonatomic, strong) NSString <Optional> *top_view_image_path;
//@property (nonatomic, strong) NSString <Optional> *floor_plan_image_path;
//@property (nonatomic, strong) NSString <Optional> *floor_plan_metadata_path;
//@property (nonatomic)		  int *engine_version;
//@property (nonatomic)		  NSString <Optional> *engine_version; //null로 인해 변경
//@property (nonatomic, strong) NSString <Optional> *m_engine_status_id;
//@property (nonatomic, strong) NSString <Optional> *approve_user_id;
//@property (nonatomic, strong) NSDate <Optional> *approve_date;
//@property (nonatomic) 		  BOOL		is_admin_confirm;
//@property (nonatomic) 		  BOOL		is_expand;
//@property (nonatomic) 		  BOOL		publish;
//@property (nonatomic) 		  BOOL		enabled;
//@property (nonatomic, strong) NSString <Optional> *object_id;
//@property (nonatomic, strong) NSDate *created_date;
//@property (nonatomic, strong) NSDate <Optional> *updated_date;
//@property (nonatomic, strong) NSDate <Optional> *deleted_date;
//@property (nonatomic, strong) NSDictionary <Optional> *engine_status;
@property (nonatomic, strong) NSMutableArray <Optional> *unit_attaches;

//"unit_uuid","room_count","bathroom_count","top_view_image_path"

@end


NS_ASSUME_NONNULL_END
