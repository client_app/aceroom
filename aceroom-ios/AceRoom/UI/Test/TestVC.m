//
//  TestVC.m
//  Unity-iPhone
//
//  Created by 김태원 on 06/09/2019.
//

#import "TestVC.h"
#import "PlanMapVC.h"

@interface TestVC ()

@end

@implementation TestVC
-(void)loadView
{
	[super loadView];
	
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    [kAcePlugin callUnityLotationPortrait];
	
	NSMutableArray *ctp_kor_nm = [self loadJSONFile:[NSString stringWithFormat:@"%@",@"ctp_kor_nm"]];
	NSMutableArray *sig_kor_nm = [self loadJSONFile:@"sig_kor_nm"];
	
	[[UnityBridge sharedInstance] setCTP_NM:ctp_kor_nm];
	[[UnityBridge sharedInstance] setSIG_NM:sig_kor_nm];
	NSLog(@"ctp_kor_nm : [ %@ ]", [[UnityBridge sharedInstance] CTP_NM]);
	NSLog(@"sig_kor_nm : [ %@ ]", [[UnityBridge sharedInstance] SIG_NM]);
}

- (NSMutableArray *) loadJSONFile:(NSString *)nm
{
	NSMutableArray *returnData;
	
	NSArray *jsonArray = [[NSBundle mainBundle] pathsForResourcesOfType:@"json" inDirectory:nil];
	
	for(int i=0; i<[jsonArray count]; i++){
		NSString *path = [jsonArray objectAtIndex:i];
		if([path containsString:nm]){
			NSData *data = [NSData dataWithContentsOfFile:path];
			returnData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
			break;
		}
	}
	
	return returnData;
}

- (IBAction)onClickVuforiaAR:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
        [kAcePlugin changeSceneARMattress];
    }];
}

- (IBAction)onClickLensAR:(id)sender {
    //nothing...
    
    [CRToast showToastMessage:@"개발 중 입니다."];
    
}

- (IBAction)onClickHomeEditor:(id)sender {
//    [kAcePlugin callUnityLotationLandscape];
	
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        PlanMapVC *vc = [[PlanMapVC alloc] initWithNibName:@"PlanMapVC" bundle:nil];
//        [self presentViewController:vc animated:YES completion:^{
//            [self dismissViewControllerAnimated:NO completion:nil];
//        }];
//    });
    
    PlanMapVC *vc = [[PlanMapVC alloc] initWithNibName:@"PlanMapVC" bundle:nil];
    [self presentViewController:vc animated:YES completion:^{
//        [self dismissViewControllerAnimated:NO completion:nil];
    }];
	
//    [self dismissViewControllerAnimated:YES completion:^{
//        [kAcePlugin callUnityLotationLandscape];
//        [kAcePlugin changeSceneHomeEditor:@"{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1c271da53ab8ad712e9.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}"];
//    }];
}

@end
