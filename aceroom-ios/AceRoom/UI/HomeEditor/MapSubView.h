//
//  MapSubView.h
//  Unity-iPhone
//
//  Created by SangYeonE on 27/09/2019.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   홈에디터 - 지도화면의 좌측 상세화면 객체
*/
@interface MapSubView : UIView

@property (weak, nonatomic) IBOutlet UIView *view_DropDown;
@property (weak, nonatomic) IBOutlet UILabel *label_RightTitle;
@property (weak, nonatomic) IBOutlet UIButton *btn_SelectList;
@property (weak, nonatomic) IBOutlet UIImageView *img_ObjType;
@property (weak, nonatomic) IBOutlet UILabel *label_select;
@property (weak, nonatomic) IBOutlet UIImageView *img_Thumb;
@property (weak, nonatomic) IBOutlet UILabel *label_address;
@property (weak, nonatomic) IBOutlet UILabel *label_roomCnt;
@property (weak, nonatomic) IBOutlet UILabel *label_restRoomCnt;
@property (weak, nonatomic) IBOutlet UIButton *btn_HomeEdit;

@end

NS_ASSUME_NONNULL_END
