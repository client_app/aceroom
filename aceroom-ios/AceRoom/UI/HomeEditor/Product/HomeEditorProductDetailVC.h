//
//  HomeEditorProductDetailVC.h
//  Unity-iPhone
//
//  Created by SangYeonE on 14/10/2019.
//

#import <UIKit/UIKit.h>
#import "product_entity.h"
#import "Frame_entity.h"
#import "HomeEditorProductVC.h"
#import "roomset_entity.h"

NS_ASSUME_NONNULL_BEGIN

/*!
   카탈로그 - 제품 상세화면
    색상은 4개 이상 미지원
*/
@interface HomeEditorProductDetailVC : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>
{
	BOOL pickerVisible;
	NSString *selected_Color;
	NSString *selected_Size;
	CGFloat minFontScale;
}
@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet UIView *naviView;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *colorItemView;
@property (weak, nonatomic) IBOutlet UIView *colorSelectView1;//컬러 선택용 뷰
@property (weak, nonatomic) IBOutlet UIView *colorSelectView2;
@property (weak, nonatomic) IBOutlet UIView *colorSelectView3;
@property (weak, nonatomic) IBOutlet UIView *colorSelectView4;
@property (weak, nonatomic) IBOutlet UIView *colorView1;//색상
@property (weak, nonatomic) IBOutlet UIView *colorView2;
@property (weak, nonatomic) IBOutlet UIView *colorView3;
@property (weak, nonatomic) IBOutlet UIView *colorView4;
@property (weak, nonatomic) IBOutlet UIButton *colorButton1;//선택 버튼
@property (weak, nonatomic) IBOutlet UIButton *colorButton2;
@property (weak, nonatomic) IBOutlet UIButton *colorButton3;
@property (weak, nonatomic) IBOutlet UIButton *colorButton4;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel1;//색상 이름
@property (weak, nonatomic) IBOutlet UILabel *colorLabel2;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel3;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel4;
@property (weak, nonatomic) IBOutlet UIImageView *colorImageView1;//색상(이미지인 경우)
@property (weak, nonatomic) IBOutlet UIImageView *colorImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *colorImageView3;
@property (weak, nonatomic) IBOutlet UIImageView *colorImageView4;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIView *sizeView;
@property (weak, nonatomic) IBOutlet UIButton *sizeButton;
@property (weak, nonatomic) IBOutlet UIImageView *sizeButtonArrowImg;


@property (weak, nonatomic) IBOutlet UIView *colorMainView;


@property (strong, nonatomic) product_entity *selected_entity;	//선택된 제품 전체 데이터
@property (strong, nonatomic) NSMutableArray *mattressList;
@property (strong, nonatomic) NSMutableArray *mColorList;		//선택된 제품 컬러 목록
@property (nonatomic, strong) NSMutableArray *mSizeList;		//선택된 제품 사이즈 목록
@property (nonatomic, strong) NSMutableArray *pickerArray;		//사이즈 목록 피커뷰 데이터

@property (nonatomic, strong) NSMutableDictionary *selected_frame;	//최종 선택된 프레임.
@property (nonatomic, strong) NSMutableDictionary *selected_mattress;	//최종 선택된 메트리스

@property (nonatomic) enum displayType mDisplayType;

@property (nonatomic, strong) roomset_entity *selected_roomset;    //최종 선택된 가구.

@end

NS_ASSUME_NONNULL_END
