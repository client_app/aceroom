//
//  HomeEditorProductVC.m
//  Unity-iPhone
//
//  Created by SangYeonE on 10/10/2019.
//

#import "HomeEditorProductVC.h"
#import <UIImageView+AFNetworking.h>

#define kCellID @"ProductCell"
#define kCell_img 444
#define kCell_lbl 445

@interface HomeEditorProductVC ()

@end

@implementation HomeEditorProductVC

-(NSArray *)pickerOriginArray
{
	if(_pickerOriginArray ==nil)
	{
		_pickerOriginArray = @[
			@[
                @"전체",
				@"패밀리침대",
				@"신혼침대",
				@"슈퍼싱글침대"
			] ,
            @[
                @"전체",
                @"Plain paper",
				@"Fabric",
				@"Tile"
            ],
            @[
                @"전체",
                @"Wood",
				@"Tile",
				@"Concrete"
			],
            @[
                @"전체",
                @"룸세트",
                @"리빙가구",
            ],
		];
	}
	
	return _pickerOriginArray;
}

- (BOOL) shouldAutorotate {
    return TRUE;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
//	[self rotationScreen:UIInterfaceOrientationLandscapeLeft];
	[[UIDevice currentDevice] setValue:[NSNumber numberWithInteger: UIInterfaceOrientationLandscapeLeft] forKey:@"orientation"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
	[self Initialize];
	
	[self requestProducts];
	[self requestWallFloorList];
	
}

-(void)Initialize
{
	
	UINib* nib = [UINib nibWithNibName:kCellID bundle:nil];
	[self.collectionView registerNib:nib forCellWithReuseIdentifier:kCellID];
	
	self.collectDataArray = [[NSMutableArray alloc] init];
	
	self.pickerDispArray = [[NSMutableArray alloc] init];
	self.pickerDispArray = self.pickerOriginArray[0];
	
	[self onClickBed:nil];
	
	pickerVisible = NO;
	selPickerIdx = 0;
}

#pragma mark networks
/*!
   어반베이스 - 제품목록 조회
*/
-(void) requestProducts
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	
	
	[manager GET:[NSString stringWithFormat:@"%@%@",URBAN_PRODUCT_API_DOMAIN, API_BED]
	  parameters:nil
		progress:nil
		 success:^(NSURLSessionTask *task, id responseObject) {
//			NSLog(@"responseObject ================= %@ ================= ", responseObject);
	
		NSMutableDictionary *rDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
		
		if([rDic objectForKey:@"brand"]!=nil){
			
			//product 세팅
			NSArray *arr_products = [rDic objectForKey:@"products"];
			
			if(arr_products && [arr_products count]>0){
				
				self.mProducts = [[NSMutableArray alloc] initWithArray:arr_products];
				self.moriginProducts = [[NSMutableArray alloc] initWithArray:arr_products];
				
				if(self->mDisplayType == BED)
					self.collectDataArray = self->_mProducts;
				
				[self.collectionView reloadData];
			
			}else{
				[self.view makeToast:@"products가 없습니다."];
			}
			
			
			//매트리스 세팅.
			NSArray *arr_mtList = [rDic objectForKey:@"mattressList"];
			self.mMattressList = [[NSMutableArray alloc] initWithArray:arr_mtList];
            
            NSArray *arr_roomsetList = [rDic objectForKey:@"roomsetList"];
            if(arr_products && [arr_products count]>0){
                self.mRoomsetList = [[NSMutableArray alloc] initWithArray:arr_roomsetList];
                self.moriginRoomsetList = [[NSMutableArray alloc] initWithArray:arr_roomsetList];;
            }else{
                [self.view makeToast:@"가구 정보가 없습니다."];
            }
            
			
		}else{
			[self.view makeToast:@"데이터 오류입니다. 다시 시도해보세요."];
		}
		
		} failure:^(NSURLSessionTask *operation, NSError *error) {
			 [self.view makeToast:[NSString stringWithFormat:@"%@", error]];
		}
	 ];
	
}

/*!
   어반베이스 - 벽지, 바닥재 정보 조회
*/
-(void)requestWallFloorList
{
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
		manager.responseSerializer = [AFJSONResponseSerializer serializer];
		
		
		[manager GET:[NSString stringWithFormat:@"%@%@",URBAN_PRODUCT_API_DOMAIN, API_WALL]
		  parameters:nil
			progress:nil
			 success:^(NSURLSessionTask *task, id responseObject) {
	//			NSLog(@"responseObject ================= %@ ================= ", responseObject);
		
				NSMutableDictionary *rDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
				
				if([rDic objectForKey:@"wallpaperList"]!=nil){
					self.moriginWallPapers = [[NSMutableArray alloc] initWithArray:[rDic objectForKey:@"wallpaperList"]];
					self.mWallPaperList = [[NSMutableArray alloc] initWithArray:[rDic objectForKey:@"wallpaperList"]];
				}else{
					[self.view makeToast:@"벽지 정보가 없습니다. 다시 시도해보세요."];
				}
				
				if([rDic objectForKey:@"floorList"]!=nil){
					self.moriginFloorList = [[NSMutableArray alloc] initWithArray:[rDic objectForKey:@"floorList"]];
					self.mFloorList = [[NSMutableArray alloc] initWithArray:[rDic objectForKey:@"floorList"]];
				}else{
					[self.view makeToast:@"바닥 정보가 없습니다. 다시 시도해보세요."];
				}
			
//				if(self->mDisplayType == WALL)
//					self.collectDataArray = self->_mWallPaperList;
//				else if(self->mDisplayType == FLOOR)
//					self.collectDataArray = self->_mFloorList;
			
			} failure:^(NSURLSessionTask *operation, NSError *error) {
				 [self.view makeToast:[NSString stringWithFormat:@"%@", error]];
			}
		 ];
}

#pragma mark Actions
- (IBAction)onClickClose:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickDropDownList:(id)sender {
	[self togglePicker];
}

/*!
   카테고리 - 침대
*/
- (IBAction)onClickBed:(id)sender {
//	[kUnityBridge adbrixCustomAction:@"AR1_bed_category_btn"];
	
	mDisplayType = BED;
	self.pickerDispArray = _pickerOriginArray[0];
	[self.selectBtn setTitle:[_pickerDispArray objectAtIndex:0] forState:UIControlStateNormal];
	[_pickerView reloadAllComponents];
	pickerVisible = YES;
	[self togglePicker];
	
	[_btnBed setSelected:YES];
	[_btnWall setSelected:NO];
	[_btnCarpet setSelected:NO];
    [_btnRoomset setSelected:NO];
	
	[self refreshDataByFilter:0];
	
}
/*!
   카테고리 - 벽지
*/
- (IBAction)onClickWall:(id)sender {
//	[kUnityBridge adbrixCustomAction:@"AR1_wallpaper_category_btn"];
	 
	mDisplayType = WALL;
	
	self.pickerDispArray = _pickerOriginArray[1];
	[self.selectBtn setTitle:[_pickerDispArray objectAtIndex:0] forState:UIControlStateNormal];
	[_pickerView reloadAllComponents];
	pickerVisible = YES;
	[self togglePicker];
	
	[_btnBed setSelected:NO];
	[_btnWall setSelected:YES];
	[_btnCarpet setSelected:NO];
    [_btnRoomset setSelected:NO];
	
	[self refreshDataByFilter:0];
	
}
/*!
   카테고리 - 바닥재
*/
- (IBAction)onClickCarpet:(id)sender {
//	[kUnityBridge adbrixCustomAction:@"AR1_flooring_category_btn"];
	
	mDisplayType = FLOOR;
	
	self.pickerDispArray = _pickerOriginArray[2];
	[self.selectBtn setTitle:[_pickerDispArray objectAtIndex:0] forState:UIControlStateNormal];
	[_pickerView reloadAllComponents];
	pickerVisible = YES;
	[self togglePicker];
	
	[_btnBed setSelected:NO];
	[_btnWall setSelected:NO];
	[_btnCarpet setSelected:YES];
    [_btnRoomset setSelected:NO];
	
	[self refreshDataByFilter:0];
	
}

- (IBAction)onClickRoomset:(id)sender {
    mDisplayType = ROOMSET;
    
    self.pickerDispArray = _pickerOriginArray[3];
    [self.selectBtn setTitle:[_pickerDispArray objectAtIndex:0] forState:UIControlStateNormal];
    [_pickerView reloadAllComponents];
    pickerVisible = YES;
    [self togglePicker];
    
    [_btnBed setSelected:NO];
    [_btnWall setSelected:NO];
    [_btnCarpet setSelected:NO];
    [_btnRoomset setSelected:YES];
    
    [self refreshDataByFilter:0];
}

/*!
   제품 상세화면으로 이동
*/
- (void)onClickNext:(NSIndexPath *)indexPath
{
	if(mDisplayType == BED){
		NSMutableDictionary *tmpDic = ((NSMutableDictionary*)[_mProducts objectAtIndex:indexPath.row]);
//        product_entity *entity = [[product_entity alloc] init];
//		[entity setValuesForKeysWithDictionary:tmpDic];
        
        NSError *error;
        product_entity *entity = [[product_entity alloc] initWithDictionary:tmpDic error:&error];
        if(error){
            NSLog(@"ERROR !!! : %@", error);
        }
        
		
		HomeEditorProductDetailVC *vc = [[HomeEditorProductDetailVC alloc] initWithNibName:@"HomeEditorProductDetailVC" bundle:nil];
		vc.selected_entity = entity;
        vc.mDisplayType = mDisplayType;
		vc.mattressList = _mMattressList;
		[self.navigationController pushViewController:vc animated:YES];
		
		/////Adbrix
//		NSDictionary *attrData = @{
//			@"name" : entity.name
//
//		};
//		[kUnityBridge adbrixCustomAction:@"AR1_Product_btn" withAttrData:attrData];
	
	}else if(mDisplayType == WALL){
		
		NSMutableDictionary *sendDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[_mWallPaperList objectAtIndex:indexPath.row],@"material", nil];
		[kAcePlugin changeWallPaperStyle:[CommonUtil convertDictionaryToString:sendDic]];
	
		[self.navigationController popToRootViewControllerAnimated:YES];
        /////Adbrix
//        NSDictionary *attrData = @{
//            @"name" : entity.name
//
//        };
//        [kUnityBridge adbrixCustomAction:@"AR1_Product_btn" withAttrData:attrData];
	}else if(mDisplayType == FLOOR){
		NSMutableDictionary *sendDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[_mFloorList objectAtIndex:indexPath.row],@"material", nil];
		[kAcePlugin changeFloorStyle:[CommonUtil convertDictionaryToString:sendDic]];
		
		[self.navigationController popToRootViewControllerAnimated:YES];
        /////Adbrix
//        NSDictionary *attrData = @{
//            @"name" : entity.name
//
//        };
//        [kUnityBridge adbrixCustomAction:@"AR1_Product_btn" withAttrData:attrData];
	}else if(mDisplayType == ROOMSET){
        NSMutableDictionary *tmpDic = ((NSMutableDictionary*)[_mRoomsetList objectAtIndex:indexPath.row]);
        NSError *error;
        roomset_entity *entity = [[roomset_entity alloc] initWithDictionary:tmpDic error:&error];
        if(error){
            NSLog(@"ERROR !!! : %@", error);
        }
        
        HomeEditorProductDetailVC *vc = [[HomeEditorProductDetailVC alloc] initWithNibName:@"HomeEditorProductDetailVC" bundle:nil];
        vc.selected_roomset = entity;
        vc.mDisplayType = mDisplayType;
        [self.navigationController pushViewController:vc animated:YES];
        /////Adbrix
//        NSDictionary *attrData = @{
//            @"name" : entity.name
//
//        };
//        [kUnityBridge adbrixCustomAction:@"AR1_Product_btn" withAttrData:attrData];
    }
}

#pragma mark 필터 별 데이터 핸들링.
-(void)refreshDataByFilter:(NSInteger)row
{
	switch (mDisplayType) {
		case BED:
			mBedType = (int)row;
			[self filterBed:mBedType];
			break;
		case WALL:
			[self filterWall:(int)row];
			break;
		case FLOOR:
			[self filterFloor:(int)row];
			break;
        case ROOMSET:
            mRoomSetType = (int)row;
            [self filterRoomSet:(int)row];
            break;
			
		default:
			mBedType = (int)row;
			[self filterBed:mBedType];
			break;
	}
	
	
	
}

/*!
   카테고리 - 침대 사이즈 필터링
*/
-(void)filterBed:(bedType)selType
{
	NSString *srch_string = @"";
	[_mProducts removeAllObjects];
	NSMutableArray *cpArray = [[NSMutableArray alloc] initWithArray:_moriginProducts];
	self.mProducts = cpArray;
	
	switch (mBedType) {
		case Family:
			srch_string = @"Family";
			break;
		case Wedding:
			srch_string = @"Wedding";
			break;
		case SuperSingle:
			srch_string = @"SuperSingle";
			break;
		default:
			break;
	}
	
	if(mBedType != ALL)	{
		
		if(_mProducts && [_mProducts count] > 0){
			NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
			for(int i=0; i<[_mProducts count]; i++){
				NSDictionary *tmpDic = ((NSDictionary*)[_mProducts objectAtIndex:i]);
				if([[[tmpDic objectForKey:@"filter"] uppercaseString] isEqualToString:[srch_string uppercaseString]]){
					[tmpArray addObject:tmpDic];
				}
			}
			[_mProducts removeAllObjects];
			self.mProducts = tmpArray;
		}
		
	}
	
	self.collectDataArray = _mProducts;
	[_collectionView reloadData];
}

/*!
   카테고리 - 벽지 필터링
*/
-(void)filterWall:(int)row
{
	NSString *srch_string = _pickerOriginArray[1][row];
	[_mWallPaperList removeAllObjects];
	NSMutableArray *cpArray = [[NSMutableArray alloc] initWithArray:_moriginWallPapers];
	self.mWallPaperList  = cpArray;
	
	if(row > 0){
		if(_mWallPaperList && [_mWallPaperList count] > 0){
			NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
			
			for(int i=0; i<[_mWallPaperList count]; i++){
			
				NSDictionary *tmpDic = ((NSDictionary*)[_mWallPaperList objectAtIndex:i]);
				
				if([[[tmpDic objectForKey:@"filter"] uppercaseString] isEqualToString:[srch_string uppercaseString]]){
					[tmpArray addObject:tmpDic];
				}
			}
			
			[_mWallPaperList removeAllObjects];
			self.mWallPaperList = tmpArray;
		}
	}
	
	self.collectDataArray = _mWallPaperList;
	[_collectionView reloadData];
}

/*!
   카테고리 - 바닥재 필터링
*/
-(void)filterFloor:(int)row
{
	NSString *srch_string = _pickerOriginArray[2][row];
	[_mFloorList removeAllObjects];
	NSMutableArray *cpArray = [[NSMutableArray alloc] initWithArray:_moriginFloorList];
	self.mFloorList  = cpArray;
	
	if(row > 0){
		if(_mFloorList && [_mFloorList count] > 0){
			NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
			
			for(int i=0; i<[_mFloorList count]; i++){
			
				NSDictionary *tmpDic = ((NSDictionary*)[_mFloorList objectAtIndex:i]);
				
				if([[[tmpDic objectForKey:@"filter"] uppercaseString] isEqualToString:[srch_string uppercaseString]]){
					[tmpArray addObject:tmpDic];
				}
			}
			
			[_mFloorList removeAllObjects];
			self.mFloorList = tmpArray;
		}
	}
	
	self.collectDataArray = _mFloorList;
	[_collectionView reloadData];
}

/*!
   카테고리 - 가구 필터링
*/
-(void)filterRoomSet:(int)row
{
    NSString *srch_string = @"";
    [_mRoomsetList removeAllObjects];
    NSMutableArray *cpArray = [[NSMutableArray alloc] initWithArray:_moriginRoomsetList];
    self.mRoomsetList  = cpArray;
    
    switch (mRoomSetType) {
        case RS_RoomSet:
            srch_string = @"roomset";
            break;
        case RS_Living:
            srch_string = @"living";
            break;
        default:
            break;
    }
    
    if(row > 0){
        if(_mRoomsetList && [_mRoomsetList count] > 0){
            NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
            
            for(int i=0; i<[_mRoomsetList count]; i++){
            
                NSDictionary *tmpDic = ((NSDictionary*)[_mRoomsetList objectAtIndex:i]);
                
                if([[[tmpDic objectForKey:@"filter"] uppercaseString] isEqualToString:[srch_string uppercaseString]]){
                    [tmpArray addObject:tmpDic];
                }
            }
            
            [_mRoomsetList removeAllObjects];
            self.mRoomsetList = tmpArray;
        }
    }
    
    self.collectDataArray = _mRoomsetList;
    [_collectionView reloadData];
}

#pragma mark UICollectionView
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_collectDataArray count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenRect = [self.collectionView bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 3.5; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth, cellWidth*0.8f);

    return size;
}

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//    return 6.0;
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
//    return 6.0;
//}
//
//// Layout: Set Edges
//- (UIEdgeInsets)collectionView:
//(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//   // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
//    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	[self onClickNext:indexPath];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellID forIndexPath:indexPath];
	cell.layer.borderWidth=1.0f;
    cell.layer.borderColor=[UIColor colorWithRed:183.f/255 green:183.f/255 blue:183.f/255 alpha:1].CGColor;
	
	NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
	
	UIImageView *imageView = (UIImageView*)[cell viewWithTag:kCell_img];
	UILabel *lable = (UILabel *)[cell viewWithTag:kCell_lbl];
	
	[cell.contentView addSubview:imageView];
	[cell.contentView addSubview:lable];
	
	if(mDisplayType == BED){        //침대 셀
		
//		product_entity *entity = [[product_entity alloc] init];
		NSLog(@"_mProducts count :: %ld ", [_mProducts count]);
		tmpDic = ((NSMutableDictionary*)[_mProducts objectAtIndex:indexPath.row]);
//		[entity setValuesForKeysWithDictionary:tmpDic];
        
        NSError *error;
        product_entity *entity = [[product_entity alloc] initWithDictionary:tmpDic error:&error];
        if(error){
            NSLog(@"ERROR !!! : %@", error);
        }
		
		
		__weak UIImageView *aImageView = imageView;
		[imageView setImageWithURLRequest:[NSURLRequest requestWithURL:
										   [NSURL URLWithString:
//											[NSString stringWithFormat:@"%@%@",URBAN_PRODUCT_API_DOMAIN, entity.catalogThumbnail]
											[NSString stringWithFormat:@"%@",entity.catalogThumbnail]
											]
										   ]
						 placeholderImage:[UIImage imageNamed:@"sub02CatalogImageBestaceLoadingIcon"]
			success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
			aImageView.image = image;
		} failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
			NSLog(@"fail to load images :::: %@", [NSString stringWithFormat:@"%@", error]);
		}];
			
			
		[lable setText:entity.name];
		[lable setHidden:YES];
		
	}else if(mDisplayType == WALL ){        //벽지 셀
		
		tmpDic = ((NSMutableDictionary*)[_mWallPaperList objectAtIndex:indexPath.row]);
		
		__weak UIImageView *aImageView = imageView;
		__weak UILabel *aLabel = lable;
		[imageView setImageWithURLRequest:[NSURLRequest requestWithURL:
										   [NSURL URLWithString:
//											[NSString stringWithFormat:@"%@/%@",URBAN_PRODUCT_API_DOMAIN, [tmpDic objectForKey:@"thumbnail_path"]]
											[NSString stringWithFormat:@"%@",[tmpDic objectForKey:@"thumbnail_path"]]
											]
										   ]
						 placeholderImage:[UIImage imageNamed:@"sub02CatalogImageBestaceLoadingIcon"]
			success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
			aImageView.image = image;
		} failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
			NSLog(@"fail to load images :::: %@", [NSString stringWithFormat:@"%@", error]);
		}];
		
		[aLabel setText:[tmpDic objectForKey:@"title"]];
		[aLabel setHidden:NO];
		
	}else if(mDisplayType == FLOOR){        //바닥재 셀
		
		tmpDic = ((NSMutableDictionary*)[_mFloorList objectAtIndex:indexPath.row]);
		
		__weak UIImageView *aImageView = imageView;
		__weak UILabel *aLabel = lable;
		[imageView setImageWithURLRequest:[NSURLRequest requestWithURL:
										   [NSURL URLWithString:
//											[NSString stringWithFormat:@"%@/%@",URBAN_PRODUCT_API_DOMAIN, [tmpDic objectForKey:@"thumbnail_path"]]
											[NSString stringWithFormat:@"%@",[tmpDic objectForKey:@"thumbnail_path"]]
											]
										   ]
						 placeholderImage:[UIImage imageNamed:@"sub02CatalogImageBestaceLoadingIcon"]
			success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
			aImageView.image = image;
		} failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
			NSLog(@"fail to load images :::: %@", [NSString stringWithFormat:@"%@", error]);
		}];
		
		[aLabel setText:[tmpDic objectForKey:@"title"]];
		[aLabel setHidden:NO];
	}else if(mDisplayType == ROOMSET){        //가구 셀
            
            tmpDic = ((NSMutableDictionary*)[_mRoomsetList objectAtIndex:indexPath.row]);
            
            __weak UIImageView *aImageView = imageView;
            __weak UILabel *aLabel = lable;
            [imageView setImageWithURLRequest:[NSURLRequest requestWithURL:
                                               [NSURL URLWithString:
    //                                            [NSString stringWithFormat:@"%@/%@",URBAN_PRODUCT_API_DOMAIN, [tmpDic objectForKey:@"thumbnail_path"]]
                                                [NSString stringWithFormat:@"%@",[tmpDic objectForKey:@"catalogThumbnail"]]
                                                ]
                                               ]
                             placeholderImage:[UIImage imageNamed:@"sub02CatalogImageBestaceLoadingIcon"]
                success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                aImageView.image = image;
            } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                NSLog(@"fail to load images :::: %@", [NSString stringWithFormat:@"%@", error]);
            }];
            
            [aLabel setText:[tmpDic objectForKey:@"catalogName"]];
            [aLabel setHidden:YES];
        }
	
	
	
	return cell;
}

#pragma mark - Picker View
-(void)resetSelectedView
{
	for(int i=0; i<[_pickerDispArray count]; i++){
		XCPickerCellView *view = (XCPickerCellView *)[_pickerView viewForRow:(NSInteger)i forComponent:0];
		[view setSelected:NO];
	}
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [_pickerDispArray count];
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 46.f;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	NSLog(@"선택값 ::: %@", [_pickerDispArray objectAtIndex:row]);
	
	selPickerIdx = (int)row;
	[self refreshDataByFilter:row];
	
	[self.selectBtn setTitle:[_pickerDispArray objectAtIndex:row] forState:UIControlStateNormal];
	
	[self resetSelectedView];
	XCPickerCellView *selectedView = (XCPickerCellView *)[pickerView viewForRow:row forComponent:component];
	[selectedView setSelected:YES];
	
	[self togglePicker];
}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//	return [_pickerDispArray objectAtIndex:row];
//}
//
//- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    NSString *title = [_pickerDispArray objectAtIndex:row];
//    NSAttributedString *attString =
//        [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:110.0/255 green:110.0/255 blue:110.0/255 alpha:1]}];
//
//    return attString;
//}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	XCPickerCellView *cellView = [[[NSBundle mainBundle] loadNibNamed:@"XCPickerCellView" owner:self options:nil] objectAtIndex:0];
	[cellView setFrame:CGRectMake(0, 0, pickerView.frame.size.width, cellView.frame.size.height)];
	[cellView.lbl_type setHidden:YES];
	[cellView.lbl_title setText:[_pickerDispArray objectAtIndex:row]];
	
	return cellView;
}


-(void)togglePicker
{
	if(pickerVisible == NO)
    {
		if([_pickerDispArray count]>0){
			[_pickerView setHidden:NO];
			pickerVisible = YES;
		}else{
//			[CRToast showToastMessage:@"목록이 없습니다."];
		}
	}else{
		[_pickerView setHidden:YES];
        pickerVisible = NO;
	}
}
@end
