//
//  HomeEditorProductVC.h
//  Unity-iPhone
//
//  Created by SangYeonE on 10/10/2019.
//

#import <UIKit/UIKit.h>
#import "product_entity.h"
#import "Btn_Sel_Product.h"
#import "Frame_entity.h"
#import "HomeEditorProductDetailVC.h"
#import "roomset_entity.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum bedType {
	ALL,
	Family,
	Wedding,
	SuperSingle
} bedType;

typedef enum roomSetType {
    RS_All,
    RS_RoomSet,
    RS_Living
} roomSetType;

typedef enum displayType {
	BED,
	WALL,
	FLOOR,
    ROOMSET
} displayType;


/*!
   홈에디터 - 제품 목록
*/
@interface HomeEditorProductVC : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIPickerViewDelegate, UIPickerViewDataSource>
{
	BOOL pickerVisible;
	bedType mBedType;			//침대 피커 인덱스용인데 쓸 지는 모르겠음.
    roomSetType mRoomSetType;   //가구 피커 인덱스용인데 씀.
	displayType mDisplayType;	//좌측 상단 선택 값 침대, 벽지, 바닥재
	int selPickerIdx;			//피커 선택 인덱스
}

@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet UIView *naviView;

//상단 뷰
@property (weak, nonatomic) IBOutlet UIView *menuView;
//상단 버튼 관련
@property (weak, nonatomic) IBOutlet UIView *btnView;
@property (weak, nonatomic) IBOutlet Btn_Sel_Product *btnBed;
@property (weak, nonatomic) IBOutlet Btn_Sel_Product *btnWall;
@property (weak, nonatomic) IBOutlet Btn_Sel_Product *btnCarpet;
@property (weak, nonatomic) IBOutlet Btn_Sel_Product *btnRoomset;

//상단 선택 관련
@property (weak, nonatomic) IBOutlet UIView *selectView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

//본문 제품 목록
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


//데이터
@property (strong, nonatomic) NSMutableArray *moriginProducts;	//초기 전체 product 정보
@property (strong, nonatomic) NSMutableArray *mProducts;		//필터 적용된 product 정보 (display용)
@property (strong, nonatomic) NSMutableArray *mMattressList;	//매트리스 리스트
@property (nonatomic, strong) NSArray *pickerOriginArray;
@property (nonatomic, strong) NSMutableArray *pickerDispArray;

@property (strong, nonatomic) NSMutableArray *moriginWallPapers;
@property (nonatomic, strong) NSMutableArray *mWallPaperList;	//벽지 목록

@property (strong, nonatomic) NSMutableArray *moriginFloorList;
@property (nonatomic, strong) NSMutableArray *mFloorList;		//바닥 목록

@property (strong, nonatomic) NSMutableArray *moriginRoomsetList;
@property (strong, nonatomic) NSMutableArray *mRoomsetList;    //룸셋(가구) 리스트

@property (nonatomic, strong) NSMutableArray *collectDataArray;

@end

NS_ASSUME_NONNULL_END
