//
//  Btn_Sel_Product.m
//  Unity-iPhone
//
//  Created by SangYeonE on 10/10/2019.
//

#import "Btn_Sel_Product.h"

@implementation Btn_Sel_Product

-(instancetype)initWithCoder:(NSCoder *)coder
{
	self =[super initWithCoder:coder];
	if(self){
		self.backgroundColor = [UIColor whiteColor];
//		self.titleLabel.textColor = [UIColor colorWithRed:160 green:160 blue:160 alpha:0.98];
		
		[self setTitleColor:[UIColor colorWithRed:160.0/255 green:160.0/255 blue:160.0/255 alpha:0.98] forState:UIControlStateNormal];
//		[self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		[self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
				
		[self setBackgroundImage:[self imageFromColor:[UIColor whiteColor]] forState:UIControlStateNormal];
		[self setBackgroundImage:[self imageFromColor:[UIColor colorWithRed:116.0/255 green:204.0/255 blue:220.0/255 alpha:1]] forState:UIControlStateSelected];
//		[self setBackgroundImage:[self imageFromColor:[UIColor redColor]] forState:UIControlStateSelected];
		
		self.layer.borderColor = [UIColor colorWithRed:223.0/255 green:223.0/255 blue:223.0/255 alpha:1].CGColor;
		self.layer.borderWidth = 1;
		
		self.layer.cornerRadius = 15; // this value vary as per your desire
		self.clipsToBounds = YES;
	}
	return self;
}

/*!
   제품 선택 시 색상 변경
*/
-(void)setSelected:(BOOL)selected
{
	[super setSelected:selected];

	if(selected == YES){
		self.layer.borderColor = [UIColor clearColor].CGColor;
	}else{
		self.layer.borderColor = [UIColor colorWithRed:223.0/255 green:223.0/255 blue:223.0/255 alpha:1].CGColor;
	}
	
}

/*!
   색상값으로 이미지 생성
*/
- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
