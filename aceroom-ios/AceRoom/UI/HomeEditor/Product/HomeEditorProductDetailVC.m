//
//  HomeEditorProductDetailVC.m
//  Unity-iPhone
//
//  Created by SangYeonE on 14/10/2019.
//

#import "HomeEditorProductDetailVC.h"
#import <UIImageView+AFNetworking.h>
#import "UILabel+XC.h"

#define kCellID @"ColorCell"
#define kCell_img 444
#define kColorSelectItem_tag 2000
#define kColorItem_tag 1000
#define kColorItemBtn_tag 1100
#define kColorItemName_tag 1200

@interface HomeEditorProductDetailVC ()
@property (strong, nonatomic) NSArray *colorViewArray;
@end

@implementation HomeEditorProductDetailVC

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
	minFontScale = 10.0f;
	
	[self initialize];
	
	[self changeColorTextFontSize];
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self setBasicValue];
}
- (void)initialize
{
	__weak UIImageView *aImageView = _productImageView;
    NSString *thumbnailPath = @"";
	
    if(self.mDisplayType == BED) {
        self.mColorList = [[NSMutableArray alloc] initWithArray:_selected_entity.colorList];
        self.mSizeList = [[NSMutableArray alloc] initWithArray:_selected_entity.sizeList];
        thumbnailPath = [NSString stringWithFormat:@"%@", [_selected_entity.baseOpt objectForKey:@"thumbnail"]];
        
        [self setColorList];
        [self setSizeList];
//        [self setBasicValue];
    }else if(self.mDisplayType == ROOMSET) {
        self.mColorList = [[NSMutableArray alloc] initWithArray:_selected_roomset.colorList];
        self.mSizeList = [[NSMutableArray alloc] initWithCapacity:0];
        thumbnailPath = [NSString stringWithFormat:@"%@", [_selected_roomset.baseOpt objectForKey:@"thumbnail"]];
        
        [self setColorList];
//        [self setSizeList];
//        [self setBasicValue];
        //사이즈 화면 숨기기. 가구는 사이즈가 없다.
        self.sizeView.hidden = YES;
    }
	
    //제품 썸네일
	[_productImageView setImageWithURLRequest:
	 [NSURLRequest requestWithURL:
		   [NSURL URLWithString:
//			[NSString stringWithFormat:@"%@%@",URBAN_PRODUCT_API_DOMAIN, [_selected_entity.baseOpt objectForKey:@"thumbnail"]]
//			[NSString stringWithFormat:@"%@", [_selected_entity.baseOpt objectForKey:@"thumbnail"]]
            thumbnailPath
		   ]
	 ]
					 placeholderImage:[UIImage imageNamed:@"sub02CatalogImageBestaceLoadingIcon"]
		success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
		aImageView.image = image;
	} failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
		NSLog(@"fail to load images :::: %@", [NSString stringWithFormat:@"%@", error]);
	}];
	
//	[self setColorList];
//	[self setSizeList];
//	[self setBasicValue];
}

- (void)setBasicValue
{
    if(self.mDisplayType == BED) {
        [self.titleLabel setText:_selected_entity.name];
        for(int i=0; i<[_pickerArray count]; i++){
            NSString *baseSize = [NSString stringWithFormat:@"%@", [_selected_entity.baseOpt objectForKey:@"size"]];
            NSString *pikerStr = [_pickerArray objectAtIndex:i];
            if([baseSize isEqualToString:pikerStr]){
                
                selected_Size = [NSString stringWithFormat:@"%@", pikerStr];
                [_sizeButton setTitle:pikerStr forState:UIControlStateNormal];
                [_pickerView reloadAllComponents];
                [_pickerView selectRow:i+1 inComponent:0 animated:YES];
                
                XCPickerCellView *view = (XCPickerCellView *)[_pickerView viewForRow:(NSInteger)i+1 forComponent:0];
                [view setSelected:YES];
                break;
            }
        }
    }else if(self.mDisplayType == ROOMSET) {
        [self.titleLabel setText:_selected_roomset.catalogName];
    }
}

#define KEY_COLOR_MAIN @"MainView"
#define KEY_COLOR_COLOR @"ColorView"
#define KEY_COLOR_BUTTON @"Button"
#define KEY_COLOR_NAME @"NameLabel"
#define KEY_COLOR_IMAGE @"ImageView"

#pragma mark - getter/setter
/*!
   색상 선택 UI용 데이터구조 - 색상은 화면 구조 상 4개만 지원
*/
-(NSArray *)colorViewArray{
    if(_colorViewArray == nil){
        _colorViewArray = @[
            @{
                KEY_COLOR_MAIN : self.colorSelectView1,
                KEY_COLOR_COLOR : self.colorView1,
                KEY_COLOR_BUTTON : self.colorButton1,
                KEY_COLOR_NAME : self.colorLabel1,
                KEY_COLOR_IMAGE : self.colorImageView1
            },
            @{
                KEY_COLOR_MAIN : self.colorSelectView2,
                KEY_COLOR_COLOR : self.colorView2,
                KEY_COLOR_BUTTON : self.colorButton2,
                KEY_COLOR_NAME : self.colorLabel2,
                KEY_COLOR_IMAGE : self.colorImageView2
            },
            @{
                KEY_COLOR_MAIN : self.colorSelectView3,
                KEY_COLOR_COLOR : self.colorView3,
                KEY_COLOR_BUTTON : self.colorButton3,
                KEY_COLOR_NAME : self.colorLabel3,
                KEY_COLOR_IMAGE : self.colorImageView3
            },
            @{
                KEY_COLOR_MAIN : self.colorSelectView4,
                KEY_COLOR_COLOR : self.colorView4,
                KEY_COLOR_BUTTON : self.colorButton4,
                KEY_COLOR_NAME : self.colorLabel4,
                KEY_COLOR_IMAGE : self.colorImageView4
            },
        ];
    }
    
    return _colorViewArray;
}

/*!
   제품 색상 선택용 버튼 배치
*/
- (void)setColorList
{
    NSString *defaultColorName;
    if(self.mDisplayType == BED) {
        defaultColorName = [_selected_entity.baseOpt objectForKey:@"color"];
    }else if(self.mDisplayType == ROOMSET) {
        defaultColorName = [_selected_roomset.baseOpt objectForKey:@"color"];
    }
//    [self.colorView2 setBackgroundColor:[UIColor redColor]];
    
    for (int i = 0; i < [self.colorViewArray count]; i++) {
        NSDictionary *coloViewDic = self.colorViewArray[i];
        
        UIView *colorSelectView = [coloViewDic objectForKey:KEY_COLOR_MAIN];
        
        if([_mColorList count] > i){
            [colorSelectView setHidden:NO];
            
            NSDictionary *nowColor = [_mColorList objectAtIndex:i];
            NSLog(@"!!!COLOR : %@", nowColor);
            NSString *colorHexString = [nowColor objectForKey:@"rgb"];
            NSString *colorPathURLString = [nowColor objectForKey:@"path"];
            NSString *colorNameString = [nowColor objectForKey:@"name"];
            NSString *colorTitleString = [nowColor objectForKey:@"title"];
            
            UIView *colorView = [coloViewDic objectForKey:KEY_COLOR_COLOR];
            __weak UIImageView *colorImageView = [coloViewDic objectForKey:KEY_COLOR_IMAGE];
            
            //2020.01 디트라이브/어반베이스 - rgb 값이 없는경우는 url path로 대체(2개 이상의 색상인 경우 이미지로 대체
            if(colorPathURLString != nil && colorPathURLString.length > 1){//색상값이 없고 url path가 있는 경우
                [colorView setHidden:YES];
                
                NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:colorPathURLString]];
                
                [colorImageView setImageWithURLRequest:request
                                      placeholderImage:nil
                                               success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                                    [colorImageView setImage:image];
                                                }
                                               failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                                    NSLog(@"fail to load images (Product Color) :::: %@", [NSString stringWithFormat:@"%@", error]);
                    
                                                }
                 ];
                
            }else if(colorHexString != nil && colorHexString.length >= 6){//색상값이 있는 경우
                [colorImageView setHidden:YES];
                [colorView setBackgroundColor:[UIColor colorWithHexString:colorHexString]];
            }else { //이도 저도 없는 경우-> 빨간색(그럴리는 없겠지만...)
                [colorImageView setHidden:YES];
                [colorView setBackgroundColor:[UIColor colorWithHexString:@"#ff0092"]];
            }
            
//            [colorView setBackgroundColor:[UIColor redColor]];
            
            UIButton *selectButton = [coloViewDic objectForKey:KEY_COLOR_BUTTON];
            [selectButton setButtonBackgroundImageSettingDefaultImage:[UIImage imageWithColor:[UIColor clearColor]] select:[UIImage imageNamed:@"sub03ButtonColorClick"]];
            
            UILabel *colorLabel = [coloViewDic objectForKey:KEY_COLOR_NAME];
            [colorLabel setText:colorTitleString];
			
			NSLog(@"=== Text Size : [%f] [%@]", colorLabel.actualScaleFactor, colorTitleString);
			CGFloat labelFontScale = colorLabel.actualScaleFactor;
			if(minFontScale > labelFontScale){
				minFontScale = labelFontScale;
			}
            
            if([defaultColorName isEqualToString:colorNameString]){
                [selectButton setSelected:YES];
                selected_Color = defaultColorName;
            }
            
        }else{
            [colorSelectView setHidden:YES];
        }
        
    }
}

/*!
   폰트 사이즈 변경 - 제품 색상명이 긴 경우 폰트 사이즈 크기 변경
*/
- (void)changeColorTextFontSize {
	for (NSDictionary *coloViewDic in self.colorViewArray) {
		UILabel *colorLabel = [coloViewDic objectForKey:KEY_COLOR_NAME];
		CGFloat fontSize = colorLabel.font.pointSize * minFontScale;
		NSLog(@"=== SET Text Size : [%f] [%@]", fontSize, colorLabel.text);
		[colorLabel setFontSize:fontSize];
	}
}

/*!
   제품 사이즈 선택용 picker 세팅
*/
- (void)setSizeList
{
	[_sizeButton setTitle:@"선택" forState:UIControlStateNormal];
	
	self.pickerArray = [[NSMutableArray alloc] init];
	
	for(int i=0; i<[_mSizeList count]; i++)
	{
		[self.pickerArray addObject:[_mSizeList objectAtIndex:i]];
	}
	[self.pickerArray insertObject:@"선택" atIndex:0];
	
	[_pickerView reloadAllComponents];
}

/*!
   색상과 사이즈 변경 시 제품 썸네일 변경
*/
- (void)changeProductThumbnailImage {
    __weak UIImageView *aImageView = self.productImageView;
    [aImageView setImage:[UIImage imageNamed:@"sub02CatalogImageBestaceLoadingIcon"]];
    
    NSString *producthumbnail;
    
    if(self.mDisplayType == BED) {
        for (NSDictionary *frame in _selected_entity.frameList) {
            NSString *frameColor = [frame objectForKey:@"color"];
            NSString *frameSize = [frame objectForKey:@"size"];
            if([selected_Color isEqualToString:frameColor] && [selected_Size isEqualToString:frameSize]){
                producthumbnail = [frame objectForKey:@"thumbnail"];
                break;
            }
        }
    } else if(self.mDisplayType == ROOMSET) {
        for (NSDictionary *furniture in _selected_roomset.furnitureList) {
//            bool isDisplay = [[furniture objectForKey:@"isDisplay"] boolValue];
            NSString *color = [furniture objectForKey:@"color"];
            if([selected_Color isEqualToString:color]) {
                producthumbnail = [furniture objectForKey:@"thumbnail"];
                break;
            }
        }
    }
    
    if(producthumbnail != nil && producthumbnail.length > 0){
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:producthumbnail]];
        
        [aImageView setImageWithURLRequest:request
                                 placeholderImage:[UIImage imageNamed:@"sub02CatalogImageBestaceLoadingIcon"]
                                          success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                                        aImageView.image = image;
                                                    }
                                          failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                                        NSLog(@"fail to load images :::: %@", [NSString stringWithFormat:@"%@", error]);
            
                                                    }
         ];
    }
}

#pragma mark Actions

- (void)resetColorButtons
{
	[self.colorButton1 setSelected:NO];
	[self.colorButton2 setSelected:NO];
	[self.colorButton3 setSelected:NO];
    [self.colorButton4 setSelected:NO];
}

- (IBAction)onClickClose:(id)sender {
	
	[self.navigationController popViewControllerAnimated:YES];
	
}


- (IBAction)onClickColorBtn:(id)sender {
	
	[self resetColorButtons];
	
	UIButton *btn = (UIButton *)sender;
	NSLog(@"btn tag : %ld" , btn.tag);
	int selIdx = 0;
	switch (btn.tag) {
			
		case kColorItemBtn_tag:
			
			break;
		case kColorItemBtn_tag+1:
			selIdx = 1;
			break;
		case kColorItemBtn_tag+2:
			selIdx = 2;
			break;
        case kColorItemBtn_tag+3:
            selIdx = 3;
            break;
		default:
			break;
	}
	
	selected_Color = [[_mColorList objectAtIndex:selIdx] objectForKey:@"name"];
	UIButton *sel_btn = [_colorItemView viewWithTag:kColorItemBtn_tag+selIdx];
	[sel_btn setSelected:YES];
    
	NSLog(@"선택 칼라 값 : %@", selected_Color);
    
    
    
    [self changeProductThumbnailImage];
}

- (IBAction)onClickDropDownList:(id)sender {
	[self togglePicker];
}

/*!
   다음 화면으로 진행(유니티)
*/
- (IBAction)onClickNext:(id)sender {
	if(self.mDisplayType == BED) {
        for(int i=0; i<[_selected_entity.frameList count]; i++){
            NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
            tmpDic = (NSMutableDictionary*)[_selected_entity.frameList objectAtIndex:i];
            
            if([[tmpDic objectForKey:@"color"] isEqualToString:selected_Color] &&
               [[tmpDic objectForKey:@"size"] isEqualToString:selected_Size])
            {
                self.selected_frame = [tmpDic copy];
                break;
            }
        }
        
        for(int i=0; i<[_mattressList count]; i++){
            
            NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
            tmpDic = (NSMutableDictionary*)[_mattressList objectAtIndex:i];
            
            if([[tmpDic objectForKey:@"size"] isEqualToString:selected_Size] &&
               [[tmpDic objectForKey:@"name"] isEqualToString:[_selected_entity.baseOpt objectForKey:@"mattress"]])
            {
                self.selected_mattress = [[NSMutableDictionary alloc] initWithDictionary:tmpDic];
                
                break;
            }
        }
        
        if(_selected_frame && _selected_mattress){
            NSLog(@"_selected_Frame :: %@", _selected_frame);
            NSLog(@"_selected_mattress :: %@", _selected_mattress);
            
            NSMutableDictionary *sendDic = [[NSMutableDictionary alloc] init];
            [sendDic setObject:_selected_frame forKey:@"frame"];
            [sendDic setObject:_selected_mattress forKey:@"mattress"];
            NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray: self.navigationController.viewControllers];
            
            NSLog(@"allViewControllers :: %@", allViewControllers);
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            
            [kAcePlugin loadHomeProduct:[CommonUtil convertDictionaryToString:sendDic]];

//            NSDictionary *attrData = @{
//                @"frame" : _selected_frame,
//                @"mattress" : _selected_mattress,
//                @"color" : selected_Color,
//                @"size" : selected_Size,
//
//            };
//            [kUnityBridge adbrixCustomAction:@"AR1_placement_btn" withAttrData:attrData];
            
        } else {
            [self.view makeToast:@"제품 선택에 오류가 있습니다. 다시 시도해주세요."];
        }
    } else if(self.mDisplayType == ROOMSET) {
        NSMutableDictionary *sendDic = [[NSMutableDictionary alloc] init];
		NSMutableDictionary *furnitureDic = [[NSMutableDictionary alloc] init];
        for (NSDictionary *furniture in _selected_roomset.furnitureList) {
//            bool isDisplay = [[furniture objectForKey:@"isDisplay"] boolValue];
            NSString *color = [furniture objectForKey:@"color"];
            if([selected_Color isEqualToString:color]) {
				[furnitureDic addEntriesFromDictionary:furniture];
                break;
            }
        }
		
		[sendDic setObject:furnitureDic forKey:@"furniture"];
		
        
        if(furnitureDic != nil && furnitureDic.count > 0){
            //아직 테스트
            NSLog(@"::: 가구 전송 ::: %@", sendDic);
            [self.navigationController popToRootViewControllerAnimated:YES];
            [kAcePlugin LoadHomeFurniture:[CommonUtil convertDictionaryToString:sendDic]];
            
//            NSDictionary *attrData = @{
//                @"frame" : _selected_frame,
//                @"mattress" : _selected_mattress,
//                @"color" : selected_Color,
//                @"size" : selected_Size,
//
//            };
//            [kUnityBridge adbrixCustomAction:@"AR1_placement_btn" withAttrData:attrData];
        }else{
            [self.view makeToast:@"제품 선택에 오류가 있습니다. 다시 시도해주세요."];
        }
    }
}

#pragma mark - Picker View
-(void)resetSelectedView
{

    for(int i=0; i<[_pickerArray count]; i++){

        XCPickerCellView *view = (XCPickerCellView *)[_pickerView viewForRow:(NSInteger)i forComponent:0];

        [view setSelected:NO];

    }

}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [_pickerArray count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	NSLog(@"선택값 ::: %@", [_pickerArray objectAtIndex:row]);
	selected_Size = [_pickerArray objectAtIndex:row];
//	[self.titleLabel setText:[_pickerArray objectAtIndex:row]];
	[_sizeButton setTitle:[_pickerArray objectAtIndex:row] forState:UIControlStateNormal];
    
    //사이즈 변경으로 인한 썸네일 변경
    [self changeProductThumbnailImage];
    
    [self resetSelectedView];
    XCPickerCellView *selectedView = (XCPickerCellView *)[pickerView viewForRow:row forComponent:component];
    [selectedView setSelected:YES];
    
	[self togglePicker];
}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//	return [_pickerArray objectAtIndex:row];
//}
//
//- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    NSString *title = [_pickerArray objectAtIndex:row];
//    NSAttributedString *attString =
//        [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:110.0/255 green:110.0/255 blue:110.0/255 alpha:1]}];
//
//    return attString;
//}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{

    XCPickerCellView *cellView = [[[NSBundle mainBundle] loadNibNamed:@"XCPickerCellView" owner:self options:nil] objectAtIndex:0];

    [cellView setFrame:CGRectMake(0, 0, pickerView.frame.size.width, cellView.frame.size.height)];
    [cellView.lbl_type setHidden:YES];
    [cellView.lbl_title setText:[_pickerArray objectAtIndex:row]];

    return cellView;

}

-(void)togglePicker
{
	if(pickerVisible == NO)
    {
		if([_pickerArray count]>0){
			[_pickerView setHidden:NO];
			pickerVisible = YES;
		}else{
//			[CRToast showToastMessage:@"목록이 없습니다."];
		}
	}else{
		[_pickerView setHidden:YES];
        pickerVisible = NO;
	}
}


/**
 선택화면
 colorList
 sizeList
 기본값
  : baseOpt
  : colorList->rgb

 배치하기

 1. baseOpt -> mattress: "HT3" 값.
 2. 선택한 사이즈 : 사이즈값(String)
 3. 1.2번 값으로 mattressList의 아이템 조회
 {
		 mattressid: "9a113be0-c7cb-11e9-a8ed-bb36fb6c62a7",
		 path: "/product/HT3_K_optimized.zip",
		 sfbPath: "/product/sfb/HT3_K.sfb",
		 daeiPath: "/product/iOS/HT3_K.zip",
		 name: "HT3",
		 size: "K"
 }

 4. 배치하기

 4-1. 3번에 선택한 frameList 중 size: "K", color: "NaturalOak” 가 일치하는 아이템 선택
 {
		 frameId: "c4796110-b352-11e9-87ff-957091992448",
		 size: "K",
		 color: "NaturalOak",
		 thumbnail: "/product/BMA1147_K_NaturalOak_thumbnail.png",
		 path: "/product/BMA1147_K_NaturalOak.zip",
		 sfbPath: "/product/sfb/BMA1147_K_NaturalOak.sfb",
		 daeiPath: "/product/iOS/BMA1147_K_NaturalOak.zip",
		 isDisplay: true,
		 mattressOffset:  {
		 x: 0,
		 y: 0.306366,
		 z: 0.001026
		 },
		 mattressScale: 0.001
 }

 4-2. 3번에 선택한 mattressList의 아이템

 */
@end
