//
//  HomeEditorOptionAlertView.h
//  Unity-iPhone
//
//  Created by 김태원 on 07/10/2019.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^HomeEditorAlertHandler)(NSString * jsonString, BOOL isClose);
/*!
   홈에디터 - 설정화면
*/
@interface HomeEditorOptionAlertView : UIView
/*!
   설정 저장시 사용될 핸들러
*/
-(void)setResultHandler:(HomeEditorAlertHandler) handler;
/*!
   유니티의 설정을 표시하기 위한 메소드
*/
-(void)setOption:(NSString *)optionJson;
@end

NS_ASSUME_NONNULL_END
