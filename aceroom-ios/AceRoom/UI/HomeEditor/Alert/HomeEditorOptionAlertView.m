//
//  HomeEditorOptionAlertView.m
//  Unity-iPhone
//
//  Created by 김태원 on 07/10/2019.
//

#import "HomeEditorOptionAlertView.h"

@interface HomeEditorOptionAlertView()
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIButton *btnQualityOptHigh;
@property (weak, nonatomic) IBOutlet UIButton *btnQualityOptMiddle;
@property (weak, nonatomic) IBOutlet UIButton *btnQualityOptLow;
@property (weak, nonatomic) IBOutlet UIButton *btnOptColider;

@property (weak, nonatomic) IBOutlet UILabel *labelVersion;

@property (strong, nonatomic) HomeEditorAlertHandler returnHandler;

@end

@implementation HomeEditorOptionAlertView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    [super drawRect:rect];
    
    [self.btnQualityOptHigh setButtonImageSettingDefault:@"sub06CheckButton01" select:@"sub06CheckButtonClick01"];
    [self.btnQualityOptMiddle setButtonImageSettingDefault:@"sub06CheckButton01" select:@"sub06CheckButtonClick01"];
    [self.btnQualityOptLow setButtonImageSettingDefault:@"sub06CheckButton01" select:@"sub06CheckButtonClick01"];
    [self.btnOptColider setButtonImageSettingDefault:@"sub06CheckButton01" select:@"sub06CheckButtonClick01"];
    
    [self.labelVersion setText:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
}

//-(void)setCloseListener:(void (^)(NSString *optionJson))block
-(void)setResultHandler:(HomeEditorAlertHandler) handler
{
    self.returnHandler = handler;
}

#pragma mark - Function
-(void)setOption:(NSString *)optionJson
{
    NSData *jsonData = [optionJson dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
    
    NSString *qualityOption = [[dict objectForKey:@"effect"] stringValue];
    
    
    if([qualityOption isEqualToString:@"0"])
    {
        [self.btnQualityOptLow setSelected:YES];
    }
    else if([qualityOption isEqualToString:@"1"])
    {
        [self.btnQualityOptMiddle setSelected:YES];
    }
    else if([qualityOption isEqualToString:@"2"])
    {
        [self.btnQualityOptHigh setSelected:YES];
    }
    else
    {
        [self.btnQualityOptHigh setSelected:YES];
    }
    
    
    [self.btnOptColider setSelected:[[dict objectForKey:@"collision"] boolValue]];
}

- (NSString *)getOptionJson
{
    NSMutableDictionary *json = [[NSMutableDictionary alloc] initWithCapacity:2];
    
    NSString *qualityOption;
    
    if([self.btnQualityOptHigh isSelected])
    {
        qualityOption = @"2";
    }
    else if([self.btnQualityOptMiddle isSelected])
    {
        qualityOption = @"1";
    }
    else if([self.btnQualityOptLow isSelected])
    {
        qualityOption = @"0";
    }
    else
    {
        qualityOption = @"1";
    }
    [json setObject:qualityOption forKey:@"effect"];
    
    
    if([self.btnOptColider isSelected])
    {
        [json setObject:@"true" forKey:@"collision"];
    }
    else
    {
        [json setObject:@"false" forKey:@"collision"];
    }
    
    return  [NSString stringWithFormat:@"%@", json];
    
}

#pragma mark - Action
- (IBAction)onClickClose:(UIButton *)sender {
    if(self.returnHandler){
        self.returnHandler([self getOptionJson], YES);
    }
}

- (IBAction)onClickQuality:(UIButton *)sender {
    
    [self.btnQualityOptHigh setSelected:NO];
    [self.btnQualityOptMiddle setSelected:NO];
    [self.btnQualityOptLow setSelected:NO];
    
    [sender setSelected:YES];
    
    if ([sender.restorationIdentifier isEqualToString:@"High"]) {
        [kAcePlugin LoadHomeOptionEffect:kHomeEditorQualityHigh];
    }else if ([sender.restorationIdentifier isEqualToString:@"Middle"]) {
        [kAcePlugin LoadHomeOptionEffect:kHomeEditorQualityMiddle];
    }else if ([sender.restorationIdentifier isEqualToString:@"Low"]) {
        [kAcePlugin LoadHomeOptionEffect:kHomeEditorQualityLow];
    }else{
        [kAcePlugin LoadHomeOptionEffect:kHomeEditorQualityMiddle];
    }
    
//    if(self.returnHandler){
//        self.returnHandler([self getOptionJson], NO);
//    }
}

- (IBAction)onClickColider:(UIButton *)sender {
    [sender setSelected:![sender isSelected]];
    
    [kAcePlugin LoadHomeOptionCollision:[sender isSelected] ];
    
//    if(self.returnHandler){
//        self.returnHandler([self getOptionJson], NO);
//    }
}

- (IBAction)onClickAceMallLink:(UIButton *)sender {
    if(self.returnHandler){
        self.returnHandler([self getOptionJson], YES);
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://acebedmall.co.kr"]] options:@{} completionHandler:^(BOOL success) {
        NSLog(@"Link Open Success");
    }];
}

@end
