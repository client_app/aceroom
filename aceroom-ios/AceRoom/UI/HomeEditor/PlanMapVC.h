//
//  PlanMapVC.h
//  Unity-iPhone
//  홈에디터 - 지도 화면
//  Created by 김태원 on 16/09/2019.
//

#import "UnityAppController.h"
#import "XCCommonVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import <Toast/UIView+Toast.h>

#import "MapSubView.h"
#import "ctp_entity.h"
#import "building_entity.h"
#import "building_entity_detail.h"
#import "building_area.h"
#import "country.h"
#import "unit_entity.h"
#import "engine_status.h"
#import "unit_attache.h"

NS_ASSUME_NONNULL_BEGIN

/*!
   홈에디터 - 지도 화면
*/
@interface PlanMapVC : XCCommonVC <GMSMapViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
	BOOL pickerVisible;
	unit_attache *sel_unit_attache;
	NSMutableDictionary *sel_unit_dict;
	AFHTTPSessionManager *sessionManager;
}
@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UIImageView *ivLogo;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;

@property (nonatomic, strong) GMSMapView *mMapView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading_indicator;
@property (weak, nonatomic) IBOutlet UIView *loading_bgView;

@property (weak, nonatomic) IBOutlet MapSubView *mSubView;
@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UIView *pictureView;
@property (weak, nonatomic) IBOutlet UIView *specView;
@property (nonatomic, strong) IBOutlet UIPickerView *pickerView;

@property (nonatomic, strong) NSMutableArray *pickerArray;
//썸네일 확인용
@property (nonatomic, strong) NSMutableArray *pickerUnitArray;
@property (nonatomic, strong) NSMutableArray *buildingArray;

@property (nonatomic, strong) NSMutableDictionary *mapsObject;
@property (nonatomic, strong) NSMutableDictionary *buildingObject;

@property (nonatomic, strong) building_entity_detail *selectedBuilding;

@end

NS_ASSUME_NONNULL_END
