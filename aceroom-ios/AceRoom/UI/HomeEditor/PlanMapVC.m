//
//  PlanMapVC.m
//  Unity-iPhone
//
//  Created by 김태원 on 16/09/2019.
//

#import "PlanMapVC.h"
#import "MainViewController.h"
#import <UIImageView+AFNetworking.h>
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/AFImageDownloader.h>

#define DEFAULT_LAT 37.566535f
#define DEFAULT_LON 126.97796919000007f
#define DEFAULT_ZOOM 10
#define CTP_NM_TAG @"ctp_kor_nm.json"
#define SIG_NM_TAG @"sig_kor_nm.json"
#define MARKER_QUEUE @"MARKER_QUEUE"

@interface PlanMapVC ()
{
	CLLocationCoordinate2D lastPosition;
}

@end

@implementation PlanMapVC

#pragma mark - Life Cycle
-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	//[self rotationScreen:UIInterfaceOrientationUnknown];
}
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    //화면 진입 시 가로회전
    [self rotationScreen:UIInterfaceOrientationLandscapeLeft];
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	NSLog(@"deviceOrientation ::: %@", [self deviceOrientation:[[UIDevice currentDevice] orientation]]);
}

-(NSString*)deviceOrientation:(UIDeviceOrientation)orien
{
	NSString *orientation = @"";
	
	switch (orien) {
        case UIDeviceOrientationUnknown:             // Device oriented vertically, home button on the bottom
            {
                orientation = @"UIInterfaceOrientationUnknown";
            }
            break;
        case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
            {
                orientation = @"UIInterfaceOrientationPortrait";
            }
            break;
        case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
            {
                orientation = @"UIInterfaceOrientationPortraitUpsideDown";
            }
            break;
        case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
            {
                orientation = @"UIInterfaceOrientationLandscapeLeft";
            }
            break;
        case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
            {
                orientation = @"UIInterfaceOrientationLandscapeRight";
            }
            break;
		default:
		{
			orientation = @"UIInterfaceOrientationUnknown";
		}
			break;
    }
	
	return orientation;
}

- (void)loadView
{
    //화면 회전
    [super loadView];
//	[self.mSubView setCenter:CGPointMake(self.viewMain.frame.size.width, _mSubView.frame.origin.y)];
	
}

#define TOP_NAVI_VIEW_HEIGHT 45.f
#define S 45.f
- (void)viewDidLoad {
    [super viewDidLoad];
 
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.HTTPAdditionalHeaders = @{@"X-Api-Key":@"LvF9CNv14a2gDhDvBXra29mNQIWVSSeBanxwNKhh"};
    sessionManager = [[AFHTTPSessionManager manager] initWithSessionConfiguration:config];
    
	[self mapInitialize];
    
	self.buildingArray = [[NSMutableArray alloc] init];
	self.pickerArray = [[NSMutableArray alloc] init];
	self.pickerUnitArray = [[NSMutableArray alloc] init];
	
	[_viewMain bringSubviewToFront:_mSubView];
	
	[self toggleRightView:NO];
}

/*!
   구글맵 기본 설정
*/
-(void)mapInitialize
{
	GMSCameraPosition *camera;
	
	//홈에디터에서 지도화면으로 돌아오는 경우 기존 위치로 이동하기 위한 소스코드 - 이동
	NSDictionary *lastPosition = [[NSUserDefaults standardUserDefaults] objectForKey:LAST_MAP_POSITION];
	if(lastPosition == nil){
		camera = [GMSCameraPosition cameraWithLatitude:DEFAULT_LAT
											 longitude:DEFAULT_LON
												  zoom:DEFAULT_ZOOM];
	}else{
		camera = [GMSCameraPosition cameraWithLatitude:[[lastPosition objectForKey:@"latitude"] doubleValue]
											 longitude:[[lastPosition objectForKey:@"longitude"] doubleValue]
												  zoom:[[lastPosition objectForKey:@"zoom"] floatValue]];
	}
	
    
	float mapViewHeight = 0.0f;
    
    //지도 그릴 사이즈 계산 - 화면 사이즈
    CGFloat screenWidth = 0.0f;
    CGFloat screenHeight = 0.0f;
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    //기기나 로직에 따라 회전이 된 상태일수도 아닐수도 있다.
    if(screenSize.width > screenSize.height)
    {
        screenWidth = screenSize.width;
        screenHeight = screenSize.height;
    }else{
        screenWidth = screenSize.height;
        screenHeight = screenSize.width;
    }
    
    
    //지도 그릴 사이즈 높이 = 스크린 높이 - 상단 네비뷰 높이
    mapViewHeight = screenHeight - TOP_NAVI_VIEW_HEIGHT;
    if(IDIOM == IPAD)
    {
        //지도 그릴 사이즈 계산 - 스테이터스 바 높이
        CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
        mapViewHeight = mapViewHeight  - statusBarFrame.size.height;
    }
	
	CGRect frame = CGRectMake(0, 0, screenWidth, mapViewHeight);
    self.mMapView = [GMSMapView mapWithFrame:frame camera:camera];
	[self.mMapView setDelegate:self];
    [self.viewMain addSubview:_mMapView];
}

#pragma mark Google Maps

- (void) mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay
{
	[self toggleRightView:NO];
	[self.view endEditing:YES];
}

- (void) mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
	[self toggleRightView:NO];
	[self.view endEditing:YES];
}

- (void) mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
//	[self showSideView:NO];
	NSLog(@"didChangeCameraPosition");
	[self.view endEditing:YES];
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
	
//	[self progressStart];
	
	lastPosition = position.target;
	
	float currentMapZoomLevel = self.mMapView.camera.zoom;
	GMSVisibleRegion visibleRegion = self.mMapView.projection.visibleRegion;
	CLLocationCoordinate2D bottomLeft = visibleRegion.nearLeft;
	CLLocationCoordinate2D topRight = visibleRegion.farRight;

	NSLog(@"zoom level : %f", currentMapZoomLevel);

	[mapView clear];
	
    //카메라 위치가 변경되는 경우 시군구 정보 변경
	if (currentMapZoomLevel >= 14) {
//	  [self setUpBuilding:bottomLeft :topRight];
		[self requestBuildingsAPIMap:bottomLeft :topRight];
	} else if (currentMapZoomLevel >= 11) {
		[self setUpSiGunGuGeoJson:bottomLeft :topRight ctp_list:[[UnityBridge sharedInstance] SIG_NM] NM:SIG_NM_TAG];
	} else {
		[self setUpSiGunGuGeoJson:bottomLeft :topRight ctp_list:[[UnityBridge sharedInstance] CTP_NM] NM:CTP_NM_TAG];
	}
  
}

-(BOOL) mapView:(GMSMapView *)mapView didTapMarker:(nonnull GMSMarker *)marker
{
	NSLog(@"didTapMarker :::: %@", [marker snippet]);
	NSString *marketSnippet = marker.snippet;
	int targetZoomLevel = 0;
	
	if ([CTP_NM_TAG isEqualToString:marketSnippet]) {
		targetZoomLevel = 11;
	}else if ([SIG_NM_TAG isEqualToString:marketSnippet]) {
		targetZoomLevel = 14;
	}else{
		//마커 눌렀을때
//		[self toggleRightView:YES];
//		[self closePicker];
		NSNumber *number = [marker.userData objectForKey:@"marker_id"];
		[self onClickBuildingMarker:[number integerValue]];
	}

	if (targetZoomLevel > 0 &&
		([CTP_NM_TAG isEqualToString:marketSnippet] || [SIG_NM_TAG isEqualToString:marketSnippet])
		) {
		
		GMSCameraPosition *camera = [GMSCameraPosition
									 cameraWithLatitude:marker.position.latitude
									 longitude:marker.position.longitude
			 zoom:targetZoomLevel];
		
		[_mMapView animateToCameraPosition:camera];
		
	}
	[self.view endEditing:YES];
	
	return YES;
}
#pragma mark - networks

-(void)setUpBuilding :(CLLocationCoordinate2D) bottomLeft :(CLLocationCoordinate2D) topRight
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
	  
	  // 작업이 오래 걸리는 API를 백그라운드 스레드에서 실행한다.
	  dispatch_async(dispatch_get_main_queue(), ^{
		  // 이 블럭은 메인스레드(UI)에서 실행된다.
		  [self requestBuildingsAPIMap:bottomLeft :topRight];
	  });
	});
}

-(void)cancelRequest
{
	for (NSURLSessionTask *task in sessionManager.downloadTasks)
	{
		NSLog(@"downloadTasks task : %@", task);
		[task cancel];
	}
	
	for (NSURLSessionTask *task in sessionManager.uploadTasks)
		   {
			   NSLog(@"uploadTasks task : %@", task);
			   [task cancel];
		   }
}

/*!
   어반베이스 - Buildings API 호출 - 지도부분
*/
-(NSMutableArray*) requestBuildingsAPIMap :(CLLocationCoordinate2D) bottomLeft :(CLLocationCoordinate2D) topRight
{
	[self cancelRequest];
	
	NSMutableArray *returnArray = [[NSMutableArray alloc] init];
	
    //전송 파라미터는 어반베이스에 문의
	NSString *boundString = [NSString stringWithFormat:@"[%f,%f,%f,%f]",
							 bottomLeft.latitude, bottomLeft.longitude,
							 topRight.latitude, topRight.longitude];
	NSString *attributes = @"[\"building_name\",\"building_no\",\"road_address\",\"address\",\"latitude\",\"longitude\",\"construction_company\",\"building_completion_ym\",\"total_household_count\",\"total_dong_count\",\"m_building_type_id\",\"batl_ratio\",\"btl_ratio\",\"household_parking\",\"low_floor\",\"high_floor\",\"heat_type\",\"building_area\",\"country_id\",\"publish\",\"enabled\",\"building_uuid\"]";
	NSLog(@"bounds : %@", boundString);
//	NSString *includeValue = [NSString stringWithFormat:@"%@",
//							  @"{\"country\":{},\"building_areas\":{\"include\":{\"units\":{\"required\":true,\"include\":{\"unit_attaches\":{\"required\":true}}}}}}"];
	
	//Urbanbase 도면(지도)API 조회
	sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
	
	//200508 BuildingsAPI 변경
	NSString *url = URBAN_MAP_API_URL;
	NSString *includeValue = @"{\"country\":{}}";
//	https://apis.urbanbase.com/v2/buildings
	[sessionManager GET:url
			 parameters:@{@"bounds":boundString, @"include":includeValue, @"attributes":attributes}
		progress:nil
		 success:^(NSURLSessionTask *task, id responseObject) {
	
		NSMutableDictionary *rDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
		NSLog(@"objectForKey = %@", [rDic objectForKey:@"code"]);
		
		if([[rDic objectForKey:@"code"] isEqualToString:@"00000"] && [rDic objectForKey:@"data"]!=nil){
			self.mapsObject = [rDic objectForKey:@"data"];
			
			@synchronized (self) {
				[self loadLeftView];
			}
			
		}else{
			[self.view makeToast:[rDic objectForKey:@"message"]];
		}
		
		} failure:^(NSURLSessionTask *operation, NSError *error) {
			 NSLog(@"error ================= %@ =================", error);
			[self.view makeToast:[NSString stringWithFormat:@"%@", error]];
		}
	 ];
	
	return returnArray;
}

/*!
   어반베이스 - Buildings API 호출 - 마커 내 도면부분
*/
-(NSMutableArray*) requestBuildingsAPIInfo:(NSString *)uuid
{
	[self cancelRequest];
	
	NSMutableArray *returnArray = [[NSMutableArray alloc] init];

	//Urbanbase 도면(지도)API 조회
	sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
	
	//200508 BuildingsAPI 변경으로 인한 추가API
	//https://apis.urbanbase.com/v2/buildings/{building_uuid}?
	//include={"building_areas": {"include":{"units": {"include": {"unit_attaches": {}}}}}}
	
	NSString *url = URBAN_BUILINGS_API_URL(uuid);
//	NSString *includeValue = @"{\"building_areas\": {\"include\":{\"units\": {\"include\": {\"unit_attaches\": {}}}}}}";
	NSString *includeValue = @"{\"building_areas\":{\"attributes\":[\"id\",\"building_id\",\"supply_area\",\"exclusive_area\",\"display_supply_area\",\"display_supply_area_pyeong\",\"exclusive_ratio\",\"m_entrance_type_id\",\"publish\",\"enabled\",\"household_count\"],\"order\":[[\"supply_area\",\"asc\"],[\"exclusive_area\",\"asc\"]],\"include\":{\"units\":{\"attributes\":[\"unit_uuid\",\"room_count\",\"bathroom_count\",\"top_view_image_path\"],\"include\":{\"unit_attaches\":{\"attributes\":[\"unit_id\",\"file_path\",\"is_default\",\"m_attach_type_id\",\"extension\",\"enabled\"]}}}}}}";
	NSString *attributes = @"[\"id\",\"building_name\",\"building_no\",\"road_address\",\"address\",\"latitude\",\"longitude\",\"construction_company\",\"building_completion_ym\",\"total_household_count\",\"total_dong_count\",\"m_building_type_id\",\"batl_ratio\",\"btl_ratio\",\"household_parking\",\"low_floor\",\"high_floor\",\"heat_type\",\"building_area\",\"country_id\",\"publish\",\"enabled\",\"building_uuid\"]";
//	https://apis.urbanbase.com/v2/buildings
    
	[sessionManager GET:url
			 parameters:@{@"attributes":attributes, @"include":includeValue}
		progress:nil
		 success:^(NSURLSessionTask *task, id responseObject) {
	
		NSMutableDictionary *rDic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
		NSLog(@"objectForKey = %@", [rDic objectForKey:@"code"]);
		
		if([[rDic objectForKey:@"code"] isEqualToString:@"00000"] && [rDic objectForKey:@"data"]!=nil){
			
			
			self.buildingObject = [rDic objectForKey:@"data"];
			NSLog(@"_buildingObject = %@", self.buildingObject);
			
			@synchronized (self) {
//				[self setRightView:building];
				[self updateRightView];
			}
			
		}else{
			[self.view makeToast:[rDic objectForKey:@"message"]];
		}
		
		} failure:^(NSURLSessionTask *operation, NSError *error) {
			 NSLog(@"error ================= %@ =================", error);
			[self.view makeToast:[NSString stringWithFormat:@"%@", error]];
		}
	 ];
	
	return returnArray;
}

#pragma mark - 지도 데이터 세팅.
/*!
   좌표가 범위 안에 있는지 체크
*/
-(BOOL) isInCoordenate : (CLLocationCoordinate2D) bottomLeft topRight:(CLLocationCoordinate2D) topRight coordinate:(CLLocationCoordinate2D) coordinate
{
	if (bottomLeft.latitude <= coordinate.latitude && coordinate.latitude <= topRight.latitude
		&& bottomLeft.longitude <= coordinate.longitude && coordinate.longitude <= topRight.longitude) {
		return true;
	}
	return false;
}
/*!
    지도 화면이 어느 시군구인지 검색
*/
-(void)setUpSiGunGuGeoJson :(CLLocationCoordinate2D) bottomLeft :(CLLocationCoordinate2D) topRight ctp_list:(NSMutableArray *)ctp_list NM:(NSString*)NM
{
	[_mMapView clear];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
		
		// 작업이 오래 걸리는 API를 백그라운드 스레드에서 실행한다.
		dispatch_async(dispatch_get_main_queue(), ^{
			// 이 블럭은 메인스레드(UI)에서 실행된다.
			[self setUpHangjungThread:bottomLeft topRight:topRight ctp_list:ctp_list NM:NM];
		});
	});
}

-(void)setUpHangjungThread : (CLLocationCoordinate2D) bottomLeft topRight:(CLLocationCoordinate2D) topRight ctp_list:(NSMutableArray *)ctp_list  NM:(NSString*)NM
{
	for( int i=0; i< [ctp_list count]; i++){
//		ctp_entity *entity = [[ctp_entity alloc] init];
//		[entity setValuesForKeysWithDictionary:((NSDictionary*)[ctp_list objectAtIndex:i])];
        
        NSLog(@"INDEX!! = %d", i);
        NSLog(@"ENTITY!!! = %@", ((NSDictionary*)[ctp_list objectAtIndex:i]));
        NSError *error;
        ctp_entity *entity = [[ctp_entity alloc] initWithDictionary:((NSDictionary*)[ctp_list objectAtIndex:i]) error:&error];
        if(error){
            NSLog(@"ERROR - ctp_entity !!! : %@", error);
        }
		
		if ([self isInCoordenate:bottomLeft topRight:topRight coordinate:entity.getPosition]) {
//			NSLog(@"isInCoordenate ==== %@", entity);
			GMSMarker *marker = [[GMSMarker alloc] init];
			marker.map = _mMapView;
			marker.position = entity.getPosition;
			marker.snippet = NM;
			
			UIView *markerView = [self makeMakerView:entity];
			marker.iconView = markerView;
			
		}
	}
	
	[self progressStop];
}
/*!
   어반베이스의 Buildings API 정보를 지도화면에 표시
*/
-(void)loadBuilding
{
	[_mMapView clear];
	
	if(_mapsObject && [_mapsObject objectForKey:@"buildings"]){
		NSMutableArray *buildings = (NSMutableArray*)[_mapsObject objectForKey:@"buildings"];
		for(int i=0; i<[buildings count]; i++){
			building_entity *entity = [[building_entity alloc] init];
			
			GMSMarker *marker = [[GMSMarker alloc] init];
			marker.map = _mMapView;
			marker.position = entity.getPosition;
			marker.userData = @{@"marker_id":[NSNumber numberWithInt:i]};
			
			UIView *markerView = [self makeMakerBuildingView:entity];
			marker.iconView = markerView;
		}
	}
	
	[self progressStop];
}

/*!
   어반베이스의 Buildings API 에서 도면부분을 표시
*/
-(void)loadLeftView
{
	[_mMapView clear];
	
	if(_mapsObject && [_mapsObject objectForKey:@"buildings"]){
		NSMutableArray *buildings = (NSMutableArray*)[_mapsObject objectForKey:@"buildings"];
		for(int i=0; i<[buildings count]; i++){
//			building_entity *entity = [[building_entity alloc] init];
			NSDictionary *tmpDic = ((NSDictionary*)[(NSMutableArray*)[_mapsObject objectForKey:@"buildings"] objectAtIndex:i]);
//			[entity setValuesForKeysWithDictionary:tmpDic];
            
            NSError *error;
            building_entity *entity = [[building_entity alloc] initWithDictionary:tmpDic error:&error];
            if(error){
                NSLog(@"ERROR building_entity !!! : %@", error);
            }
            
			
			GMSMarker *marker = [[GMSMarker alloc] init];
			marker.map = _mMapView;
			marker.position = entity.getPosition;
			marker.userData = @{@"marker_id":[NSNumber numberWithInt:i]};
			
			UIView *markerView = [self makeMakerBuildingView:entity];
			marker.iconView = markerView;
		}
	}
	
	[self progressStop];
}

/*!
   어반베이스의 Buildings API 정보를 좌측 상세화면에 표시
*/
-(UIView *)makeMakerBuildingView:(building_entity *)ctp_entity
{
	UIView *markerView = [[UIView alloc] init];
	[markerView setBackgroundColor:[UIColor clearColor]];
	UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sub05MapIcon"]];
	[imgView setBackgroundColor:[UIColor clearColor]];
	[markerView setFrame:imgView.frame];
	[markerView addSubview:imgView];
	
	return markerView;
}

/*!
   어반베이스의 Buildings API 정보를 지도화면에 표시 후 마커 선택 시 화면표시
*/
-(UIView *)makeMakerView:(ctp_entity *)entity
{
	UIView *markerView = [[UIView alloc] init];
	[markerView setBackgroundColor:[UIColor whiteColor]];
	UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sub05AdressTextFrame"]];
	[imgView setBackgroundColor:[UIColor clearColor]];
	[markerView setFrame:imgView.frame];
	
	[markerView addSubview:imgView];
	
	UILabel *label = [[UILabel alloc] initWithFrame:imgView.frame];
	[label setAdjustsFontSizeToFitWidth:YES];
	[label setBackgroundColor:[UIColor clearColor]];
	[label setText:entity.name];
	[label setTextAlignment:NSTextAlignmentCenter];
	
//	[label setTextColor:[UIColor colorWithRed:2 green:163 blue:192 alpha:1]];
	
	[markerView addSubview:label];
	
	return markerView;
}

#pragma mark - Picker View
-(void)resetSelectedView
{
	for(int i=0; i<[_pickerArray count]; i++){
		XCPickerCellView *view = (XCPickerCellView *)[_pickerView viewForRow:(NSInteger)i forComponent:0];
		[view setSelected:NO];
	}
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [_pickerArray count];
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 44.f;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	NSLog(@"선택값 ::: [%ld]%@", (long)row, [_pickerArray objectAtIndex:row]);
	[self.mSubView.label_select setText:[_pickerArray objectAtIndex:row]];
	
	[self resetSelectedView];
	XCPickerCellView *selectedView = (XCPickerCellView *)[pickerView viewForRow:row forComponent:component];
	[selectedView setSelected:YES];
	if(row>0)
		[self setRightThumbnail:(int)row-1];
	[self togglePicker];
}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//	return [_pickerArray objectAtIndex:row];
//}

//- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    NSString *title = [_pickerArray objectAtIndex:row];
//    NSAttributedString *attString =
//        [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:110.0/255 green:110.0/255 blue:110.0/255 alpha:1]}];
//
//    return attString;
//}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	XCPickerCellView *cellView = [[[NSBundle mainBundle] loadNibNamed:@"XCPickerCellView" owner:self options:nil] objectAtIndex:0];
	[cellView setFrame:CGRectMake(0, 0, pickerView.frame.size.width, cellView.frame.size.height)];
	
	if((int)row==0)
		[cellView.lbl_type setHidden:YES];
	else
		[cellView.lbl_type setHidden:NO];
	
	[cellView.lbl_type setText:@"기본형"];
	[cellView.lbl_title setText:[_pickerArray objectAtIndex:row]];
	
	return cellView;
}

#pragma mark - Right View Data 세팅

-(void)updateRightView {
	if(self.buildingObject == nil){
		return;
	}
	
	NSError *error;
	building_entity_detail *building = [[building_entity_detail alloc] initWithDictionary:self.buildingObject error:&error];
	if(error){
		NSLog(@"ERROR building_entity_detail !!! : %@", error);
	}
	
	[self.pickerUnitArray removeAllObjects];
	[self.pickerArray removeAllObjects];
	
	[_mSubView.label_RightTitle setText:building.building_name];
		
	if(building && [building building_areas] && [building.building_areas count] > 0){
		building_area *firstAreaEntity;
		unit_entity *firstUnitEntity;
		NSString *firstTitle;
		
		for (int i = 0; i < building.building_areas.count; i++) {
			NSDictionary *area = building.building_areas[i];
			NSError *error;
			building_area *areaEntity = [[building_area alloc] initWithDictionary:area error:&error];
			if(error){
				NSLog(@"ERROR building_area !!! : %@", error);
				continue;
			}
			
			firstAreaEntity = areaEntity;
			
			if(areaEntity.units == nil || areaEntity.units.count == 0) {
				continue;
			}
			
			for(int j=0; j < areaEntity.units.count; j++) {
				NSDictionary *unitdata = [areaEntity.units objectAtIndex:j];
				
				[self.pickerUnitArray addObject:unitdata];
				
				NSString* formattedNumber = [NSString stringWithFormat:@"%.02f", areaEntity.exclusive_area];
				NSString *title = [NSString stringWithFormat:@"%@형 %@㎡(전용)", areaEntity.display_supply_area_pyeong, formattedNumber];
				
				[self.pickerArray addObject:title];
				
				if(i == 0 && j == 0){
					firstTitle = title;
					firstUnitEntity = [[unit_entity alloc] initWithDictionary:unitdata error:&error];
					if(error){
						NSLog(@"ERROR f unit_entity !!! : %@", error);
						continue;
					}
				}
			}
		}
		
		if(self.pickerUnitArray.count != 0 && firstUnitEntity != nil){
			//처음 값 세팅
			[self setRightThumbnail:0];
			[_mSubView.label_address setText:building.address];
			[_mSubView.label_roomCnt setText:[NSString stringWithFormat:@"%i", firstUnitEntity.room_count]];
			[_mSubView.label_restRoomCnt setText:[NSString stringWithFormat:@"%i", firstUnitEntity.bathroom_count]];
		}else if(firstAreaEntity != nil){
			[self.pickerArray addObject:firstAreaEntity.display_supply_area_pyeong];
			[self.pickerUnitArray addObject:@{}];
			
			NSLog(@"units가 없음.");
		}else{
			[_mSubView.label_address setText:@"units 정보가 없습니다."];
			[_mSubView.label_roomCnt setText:@""];
			[_mSubView.label_restRoomCnt setText:@""];
			[_mSubView.btn_HomeEdit setEnabled:NO];
		}
		
		[_mSubView.label_select setText:firstTitle];
		[self.pickerArray insertObject:@"==선택==" atIndex:0];
		if(_selectedBuilding==nil){
			self.selectedBuilding = [[building_entity_detail alloc] init];
		}
		
		self.selectedBuilding = building;
		[_pickerView reloadAllComponents];
		
		[self toggleRightView:YES];
		[self closePicker];
	}
	else {
		[_mSubView.label_select setText:@""];
		[_mSubView.label_address setText:@"building 정보가 없습니다."];
		[_mSubView.label_roomCnt setText:@""];
		[_mSubView.label_restRoomCnt setText:@""];
		[_mSubView.btn_HomeEdit setEnabled:NO];
	}
}
/*!
   건물 상세정보 - 썸네일 표시 규칙
    평수 선택에 따라 다른 썸네일을 화면에 표시
*/
-(void)setRightThumbnail:(int)idx
{
	NSDictionary *unitdata = [self.pickerUnitArray objectAtIndex:idx];
	unit_entity *unitentity = nil;
	
	if([unitdata count] != 0){
		NSError *error;
		unitentity = [[unit_entity alloc] initWithDictionary:unitdata error:&error];
		if(error){
			NSLog(@"ERROR unit_entity !!! : %@", error);
			unitentity = nil;
		}
	}
	
	
	NSLog(@"Select Picker Idx ::: %d", idx);
	if(unitentity != nil){
		sel_unit_dict = [unitentity.unit_attaches firstObject];
		
		[_mSubView.label_roomCnt setText:[NSString stringWithFormat:@"%i", unitentity.room_count]];
		[_mSubView.label_restRoomCnt setText:[NSString stringWithFormat:@"%i", unitentity.bathroom_count]];
		
		NSString *topViewImagePath = unitentity.top_view_image_path;
		NSLog(@"topViewImagePath ::: %@", topViewImagePath);
		if(topViewImagePath == nil){
			[_mSubView.img_Thumb setImage:[UIImage imageNamed:@"sub_05_background_03_3d_text_img"]];
		}else{
			NSString *str_url = [NSString stringWithFormat:@"%@/%@",URBAN_3D_API_DOMAIN, topViewImagePath];
			NSLog(@"str_url ::: %@", str_url);
			__weak UIImageView *aImageView = _mSubView.img_Thumb;
//			[_mSubView.img_Thumb setImageWithURL:[NSURL URLWithString:str_url] placeholderImage:[UIImage imageNamed:@"sub02CatalogImageBestaceLoadingIcon"]];
			
			/*
			AFImageResponseSerializer *serializer = [[AFImageResponseSerializer alloc] init];
			serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpg"];
			NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
			AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
			sessionManager.responseSerializer = serializer;

			AFImageDownloader *download = [[AFImageDownloader alloc]
						 initWithSessionManager:sessionManager
						 downloadPrioritization:AFImageDownloadPrioritizationFIFO
						 maximumActiveDownloads:2
									 imageCache:[[AFAutoPurgingImageCache alloc] init]];
			
			NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString: str_url] cachePolicy:NSURLRequestReturnCacheDataElseLoad
				timeoutInterval:60];
			aImageView.image = [UIImage imageNamed:@"sub02CatalogImageBestaceLoadingIcon"];
			[download downloadImageForURLRequest:request success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
				NSLog(@"Success download image");
				aImageView.image = image;
			} failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
				NSLog(@"Error download image");
				NSLog(@"fail to load images ::   :: %@", [NSString stringWithFormat:@"%@", error]);
				aImageView.image = [UIImage imageNamed:@"sub_05_background_03_3d_text_img"];
			}];
			 */
			
			
			[_mSubView.img_Thumb setImageWithURLRequest:[NSURLRequest requestWithURL:
											   [NSURL URLWithString:str_url]]
							 placeholderImage:[UIImage imageNamed:@"sub02CatalogImageBestaceLoadingIcon"]
				success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
				aImageView.image = image;
			} failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
				NSLog(@"fail to load images ::   :: %@", [NSString stringWithFormat:@"%@", error]);
				aImageView.image = [UIImage imageNamed:@"sub_05_background_03_3d_text_img"];
			}];
		}
		
		[self.mSubView.btn_HomeEdit setEnabled:YES];
	}else{
		[_mSubView.img_Thumb setImage:[UIImage imageNamed:@"sub_05_background_03_3d_text_img"]];
        [self.mSubView.btn_HomeEdit setEnabled:NO];
	}
}

#pragma mark - Action

-(void)togglePicker
{
	pickerVisible = !pickerVisible;
	
	if(pickerVisible == YES)
    {
		if([_pickerArray count]>0){
			[_pickerView setHidden:NO];
		}else{
//			[CRToast showToastMessage:@"목록이 없습니다."];
			
		}
	}else{
		[_pickerView setHidden:YES];
	}
}

-(void)closePicker{
	pickerVisible = NO;
	[_pickerView setHidden:YES];
}

/*!
 빌딩 마커 클릭 시
 1. buildings array => 지도 표시 마커 레벨.=> 클릭 시 현재 함수 실행.
 2. buildings.building_areas array =>  우측 상단 드랍다운 박스 배열.
 3. buildings.building_areas.units array => 화장실 수 등 표시. (얘는 배열이면 안될 것 같은데 확인 필요)
 4. buildings.building_areas.unitsunit_attaches => 도면 정보 넘김.
 */
-(void)onClickBuildingMarker:(NSInteger)idx
{
//	NSUInteger buildingCnt = 0;
//	NSUInteger unitCnt = 0;
	[_pickerArray removeAllObjects];
	[_pickerUnitArray removeAllObjects];
	
	NSDictionary *data = [[_mapsObject objectForKey:@"buildings"] objectAtIndex:idx];
	
    NSError *error;
    building_entity *building = [[building_entity alloc] initWithDictionary:data error:&error];
    if(error){
        NSLog(@"ERROR building_entity !!! : %@", error);
    }
	
	if(building && building.building_uuid != nil && building.building_uuid.length > 0){
		[self requestBuildingsAPIInfo:building.building_uuid];
	}else{
		NSLog(@"building가 유효하지 않음.");
	}
	
}

- (IBAction)onClickSelectList:(id)sender {
	
	if([_pickerArray count]==0)
		[self.view makeToast:@"목록이 없습니다."];
	
	[self togglePicker];
}


- (IBAction)onClickBackButton:(id)sender {
	//홈에디터에서 지도화면으로 돌아오는 경우 기존 위치로 이동하기 위한 소스코드 - 메뉴화면 이동 시 기존 위치 제거
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LAST_MAP_POSITION];
	
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)textFieldFinished:(id)sender {
	[self onClickSearch:nil];
}


- (IBAction)onClickSearch:(id)sender {
	
	if(_tfSearch.text && [_tfSearch.text length]>0){
		
		CLGeocoder *geocoder = [[CLGeocoder alloc] init];
		[geocoder geocodeAddressString:[NSString stringWithFormat:@"%@",_tfSearch.text] completionHandler:^(NSArray* placemarks, NSError* error){
			if (!error) {

				NSMutableArray *placemarksArray = [[NSMutableArray alloc] initWithArray:placemarks];
				
				if(placemarks && [placemarks count] > 0){
				
					if([placemarks count] == 1){
						
						CLPlacemark* aPlacemark = [placemarksArray objectAtIndex:0];
						
						GMSCameraPosition *camera = [GMSCameraPosition
													 cameraWithLatitude:aPlacemark.location.coordinate.latitude
													 longitude:aPlacemark.location.coordinate.longitude
							 zoom:18];
						
						[self.mMapView animateToCameraPosition:camera];
						
					}else{
						
						for (CLPlacemark* aPlacemark in placemarks)
						{
							NSLog(@"place--%@", [aPlacemark locality]);
							NSLog(@"lat--%f\nlong--%f",aPlacemark.location.coordinate.latitude,aPlacemark.location.coordinate.longitude);
						}
						
						[self.view makeToast:@"결과 여러 개 나옴"];
						
					}
					
					
					
				}else{
					
					[self.view makeToast:@"검색 결과가 없습니다."];
					
				}
				
			}
			else{
				[self.view makeToast:@"검색 결과가 없습니다."];
				NSLog(@"error--%@",[error localizedDescription]);
			}
		}];
		
	}else{
		[self.view makeToast:@"검색어를 입력하세요."];
	}
	
	[self.view endEditing:YES];
	
}

- (void)toggleRightView:(BOOL)doShow
{
	if([_mSubView alpha] > 0 && doShow == YES){
		return;
	}
	
	if([_mSubView alpha] == 0 && doShow == NO){
		return;
	}
	
	[UIView animateWithDuration:0.1f animations:^{
		if(doShow==YES){
//			[self.mSubView setCenter:CGPointMake(self.mSubView.center.x - moveWidth, self.mSubView.center.y)];
			[self.mSubView setAlpha:1.0f];
		}else{
//			[self.mSubView setCenter:CGPointMake(self.mSubView.center.x + moveWidth, self.mSubView.center.y)];
			[self.mSubView setAlpha:.0f];
			[self togglePicker];
		}
	}];
	
}

-(NSString*) bv_jsonStringWithPrettyPrint:(BOOL) prettyPrint dict:(NSDictionary*)dict {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:(NSJSONWritingOptions) (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];

    if (! jsonData) {
        NSLog(@"%s: error: %@", __func__, error.localizedDescription);
        return @"[]";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

/*!
   홈에디터(유니티)로 화면 전환
*/
- (IBAction)onClickHomeEdit:(id)sender {
	//TODO: 도면 정보 넘기기?
//	NSDictionary *attrData = @{
//		@"building_name" : self.selectedBuilding.building_name,
//		@"road_address" : self.selectedBuilding.road_address,
//		@"area" : _mSubView.label_select.text,
//	};
//	[kUnityBridge adbrixCustomAction:@"AR1_decorate_btn"withAttrData:attrData];
//	[kUnityBridge adbrixCustomAction:@"AR1_decorate_btn"];
	
	NSString *sendString = [CommonUtil convertDictionaryToString:sel_unit_dict];
	
//	sendString = @"{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1c271da53ab8ad712e9.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}";
	
	
	NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray: self.navigationController.viewControllers];
	
	[self rotationScreen:UIInterfaceOrientationLandscapeLeft];
	[self.navigationController popToViewController:[allViewControllers objectAtIndex:0] animated:NO];
	
	//홈에디터에서 지도화면으로 돌아오는 경우 기존 위치로 이동하기 위한 소스코드 - 홈에디터 이동 전 기존 위치 저장
	[[NSUserDefaults standardUserDefaults] setObject:@{
		@"latitude" : @(lastPosition.latitude),
		@"longitude" : @(lastPosition.longitude),
		@"zoom" : @(self.mMapView.camera.zoom),
	} forKey:LAST_MAP_POSITION];
	
	[kAcePlugin changeSceneHomeEditor:sendString];
	
}

#pragma mark 화면 관련

-(void)progressStart
{
	[self.loading_indicator setHidden:NO];
	[self.loading_bgView setHidden:NO];
	[self.loading_indicator startAnimating];
}

-(void)progressStop
{
	[self.loading_indicator setHidden:YES];
	[self.loading_bgView setHidden:YES];
	[self.loading_indicator stopAnimating];
}

@end
