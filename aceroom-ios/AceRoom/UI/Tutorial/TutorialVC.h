//
//  TutorialVC.h
//  Unity-iPhone
//
//  Created by 김태원 on 10/10/2019.
//

#import "XCCommonVC.h"
#import <TAPageControl/TAPageControl.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, TUTORIAL_TYPE) {
    TUTORIAL_TYPE_AR_VUFORIA = 0,
    TUTORIAL_TYPE_AR_LENS = 1,
    TUTORIAL_TYPE_HOME_EDITOR = 2,
};
/*!
   각 화면의 튜토리얼용 화면
    modal popup 형식으로 표시
*/
@interface TutorialVC : XCCommonVC
/*!
   어떤 튜토리얼을 표시할 지 구분값
*/
@property (nonatomic) TUTORIAL_TYPE tutorialType;
/**
    구형 기기에서 constraint가 xib device setting 기준 너비로 잡혀있는 현상이 있어 미리 받음.
 */
@property (nonatomic) float pageViewWidth;
@end

NS_ASSUME_NONNULL_END
