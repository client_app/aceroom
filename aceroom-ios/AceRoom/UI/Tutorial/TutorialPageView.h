//
//  TutorialPageView.h
//  Unity-iPhone
//
//  Created by 김태원 on 15/10/2019.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/*!
   튜토리얼 페이지뷰에 표시할 페이지 클래스
*/
@interface TutorialPageView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imgCenter;
@property (weak, nonatomic) IBOutlet UILabel *labelInfo;
@property (weak, nonatomic) IBOutlet UIView *viewCenter;

@end

NS_ASSUME_NONNULL_END
