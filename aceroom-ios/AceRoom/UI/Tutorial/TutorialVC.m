//
//  TutorialVC.m
//  Unity-iPhone
//
//  Created by 김태원 on 10/10/2019.
//

#import "TutorialVC.h"
#import "TutorialPageView.h"
#import "PlanMapVC.h"
#import "ARLensVC.h"

@interface TutorialVC ()<UIScrollViewDelegate, TAPageControlDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (weak, nonatomic) IBOutlet UIView *centerView;
@property (weak, nonatomic) IBOutlet UIScrollView *tutorialScrollView;
@property (weak, nonatomic) IBOutlet UIView *tutorialPageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollWidthConstraint;
@property (weak, nonatomic) IBOutlet TAPageControl *pageControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageControlWidthConstraint;
@property (strong, nonatomic) NSArray *arrayPageInfo;
@end

#define PAGER_DOT_WIDTH 12.0f
#define PAGER_DOT_HEIGHT 12.0f
#define PAGER_DOT_SIZE CGSizeMake(PAGER_DOT_WIDTH, PAGER_DOT_HEIGHT)

@implementation TutorialVC

/*
 typedef NS_ENUM(NSInteger, TUTORIAL_TYPE) {
     TUTORIAL_TYPE_AR_VUFORIA = 0,
     TUTORIAL_TYPE_AR_LENS = 1,
     TUTORIAL_TYPE_HOME_EDITORE = 2,
 };
 */
#pragma mark - Initialize
-(NSArray *)arrayPageInfo{
    if(_arrayPageInfo == nil){
        if(self.tutorialType == TUTORIAL_TYPE_AR_VUFORIA){
            _arrayPageInfo = @[
                                    @{
                                        @"text" : @"비치된 마커에\n카메라를 올려보세요",
                                        @"img" : @"subTutorialMarkerImg01"
                                    },
                                    @{
                                        @"text" : @"하이브리드 Z 스프링, 올인원 공법,\n투매트리스 시스템\n세가지 컨텐츠 중 하나를 고르세요",
                                        @"img" : @"subTutorialMarkerImg02"
                                    },
                                    @{
                                        @"text" : @"플레이되는 AR 컨텐츠를\n즐겨 보세요!",
                                        @"img" : @"subTutorialMarkerImg03"
                                    }
                                 ];
        }else if(self.tutorialType == TUTORIAL_TYPE_AR_LENS){
            _arrayPageInfo = @[
                                   @{
                                       @"text" : @"에이스 침대 카탈로그에서\n마음에 드는 제품을 찾아보세요",
                                       @"img" : @"subTutorialArBedImg01"
                                   },
                                   @{
                                       @"text" : @"사이즈, 색상, 매트리스 제품의\n옵션을 선택하세요",
                                       @"img" : @"subTutorialArBedImg02"
                                   },
                                   @{
                                       @"text" : @"원하는 위치에\n침대를 배치하세요",
                                       @"img" : @"subTutorialArBedImg03"
                                   }
                            ];
        }else if(self.tutorialType == TUTORIAL_TYPE_HOME_EDITOR){
            _arrayPageInfo = @[
                                   @{
                                       @"text" : @"지도에서 보고싶은 도면을 가진\n아파트를 선택하세요",
                                       @"img" : @"subTutorialHomedesginImg01"
                                   },
                                   @{
                                       @"text" : @"에디터에서 침대부터 인테리어까지\n다양한 조합을 찾아보세요",
                                       @"img" : @"subTutorialHomedesginImg02"
                                   },
                                   @{
                                       @"text" : @"배치하기를 눌러서\n원하는 공간에 올려보아요",
                                       @"img" : @"subTutorialHomedesginImg03"
                                   }
                            ];
        }else
        {
            _arrayPageInfo = @[];
        }
    }
    
    return _arrayPageInfo;
}

#pragma mark - Life Cycle
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[[UIDevice currentDevice] setValue:[NSNumber numberWithInteger: UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];
}

- (void)loadView
{
    //화면 회전
    if(self.tutorialType == TUTORIAL_TYPE_HOME_EDITOR){
        [self rotationScreen:UIInterfaceOrientationLandscapeLeft];
    }else{
        [self rotationScreen:UIInterfaceOrientationPortrait];
    }
    
    [super loadView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.tutorialScrollView.delegate = self;
    self.pageControl.delegate = self;
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self setPageView];
}


#pragma mark - Method
/*!
   튜툐리얼 페이지의 이미지와 설명문 구성
*/
-(void)setPageView
{
    if(self.pageViewWidth == 0.0f){
        self.pageViewWidth = self.centerView.frame.size.width;
    }
    
    NSLog(@"pageView width = %f", self.pageViewWidth);
    
    [self.centerView layoutIfNeeded];
    float pageViewHeight = self.centerView.frame.size.height;
    
    self.scrollWidthConstraint.constant = self.pageViewWidth * [self.arrayPageInfo count];
    NSLog(@"scrol width = %f", self.scrollWidthConstraint.constant);
    
    // 1. 인트로 이미지 배치
    for(int i = 0; i < [self.arrayPageInfo count]; i++){
        NSDictionary *pageInfo = [self.arrayPageInfo objectAtIndex:i];
        
        //TutorialPageView
        UINib* nib = [UINib nibWithNibName:@"TutorialPageView" bundle:nil];
        NSArray *customViewList = [nib instantiateWithOwner:self options:nil];
        
        TutorialPageView *copyPage = (TutorialPageView *)[customViewList firstObject];
        [copyPage setFrame:CGRectMake(i * self.pageViewWidth, 0.f, self.pageViewWidth, pageViewHeight)];
        if(self.tutorialType == TUTORIAL_TYPE_HOME_EDITOR){
            CGRect centerFrame = copyPage.viewCenter.frame;
//            [copyPage.viewCenter setFrame:CGRectMake(centerFrame.origin.x, centerFrame.origin.y, centerFrame.size.width, 246.f)];
//            [copyPage.viewCenter setFrame:CGRectMake(centerFrame.origin.x, centerFrame.origin.y, centerFrame.size.width, pageViewHeight)];
        }
        
        [copyPage.labelInfo setText:[pageInfo objectForKey:@"text"]];
        
        UIImage *img = [UIImage imageNamed:[pageInfo objectForKey:@"img"]];
        [copyPage.imgCenter setImage:img];
        
        [self.tutorialPageView addSubview:copyPage];
    }
    
    // 2. DotImage 생성
    UIImage *indecatorCurrent = [UIImage imageNamed:@"subTutorialDotClk"];
    UIImage *indecatorPoint = [UIImage imageNamed:@"subTutorialDot"];
    
    // 3. pageControl 세팅
    self.pageControl.numberOfPages = [self.arrayPageInfo count];
    self.pageControl.hidesForSinglePage = YES;
    self.pageControl.dotSize = PAGER_DOT_SIZE;
    self.pageControl.spacingBetweenDots = 10;
    self.pageControl.shouldResizeFromCenter = YES;
    self.pageControl.backgroundColor = [UIColor clearColor];
    self.pageControl.dotImage = indecatorPoint;
    self.pageControl.currentDotImage = indecatorCurrent;
    
//    CGFloat pageControlWidth = ((self.pageControl.spacingBetweenDots +  self.pageControl.dotSize.width) * self.pageControl.numberOfPages);
    
//    self.pageControlWidthConstraint.constant = pageControlWidth;
}

/**
 페이지 변경시 이벤트
 
 @param index 페이지 index
 */
-(void)pageChangPage:(NSInteger)index{
    if(index == [self.arrayPageInfo count] - 1){
        
    }
    self.pageControl.currentPage = index;
    [self.tutorialScrollView scrollRectToVisible:CGRectMake(CGRectGetWidth(self.tutorialScrollView.frame) * index, 0, CGRectGetWidth(self.tutorialScrollView.frame), CGRectGetHeight(self.tutorialScrollView.frame)) animated:YES];
}

#pragma mark - TAPageControlDelegate
- (void)TAPageControl:(TAPageControl *)pageControl didSelectPageAtIndex:(NSInteger)index
{
    if([self.arrayPageInfo count] - 1 == index){
        [self.btnNext setTitle:@"END" forState:UIControlStateNormal];
    }else{
        [self.btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
    }
    
    [self pageChangPage:index];
}

#pragma mark - UIScrollViewDelegate
/**
 @brief UIScrollViewDelegate의 콜백 함수, 이동후 스크롤뷰가 중단될때 호출, pageControl 의 페이지 상태를 변경시킨다.
 @param scrollView 해당 스크롤뷰
 */
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    CGFloat pageWidth = scrollView.frame.size.width;
//    CGFloat currentPage = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) +1;
    
    
    NSInteger pageIndex = scrollView.contentOffset.x / CGRectGetWidth(scrollView.frame);
    if([self.arrayPageInfo count] - 1 == (NSUInteger)pageIndex){
        [self.btnNext setTitle:@"END" forState:UIControlStateNormal];
    }else{
        [self.btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
    }
    NSLog(@"Move Tab Page : %ld", (long)pageIndex);
    [self pageChangPage:pageIndex];
}

-(void)endTutorial
{
//    if(
//       self.tutorialType == TUTORIAL_TYPE_AR_VUFORIA
//       && [[NSUserDefaults standardUserDefaults] boolForKey:USER_KEY_FIRST_AR_MATTRESS] != YES){
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_KEY_FIRST_AR_MATTRESS];
//        [self dismissViewControllerAnimated:YES completion:^{
//            [kAcePlugin changeSceneARMattress];
//        }];
//
//    }else if(
//             self.tutorialType == TUTORIAL_TYPE_AR_LENS
//             && [[NSUserDefaults standardUserDefaults] boolForKey:USER_KEY_FIRST_AR_LENS] != YES){
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_KEY_FIRST_AR_LENS];
//        [self dismissViewControllerAnimated:YES completion:^{
//            UINavigationController *nav = (UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController;
//
//            ARLensVC *vc = [[ARLensVC alloc] initWithNibName:@"ARLensVC" bundle:nil];
//            [nav presentViewController:vc animated:YES completion:^{
//
//            }];
//        }];
//
//    }else if(
//             self.tutorialType == TUTORIAL_TYPE_HOME_EDITOR
//             && [[NSUserDefaults standardUserDefaults] boolForKey:USER_KEY_FIRST_HOME_EDITOR] != YES){
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_KEY_FIRST_HOME_EDITOR];
//        [self dismissViewControllerAnimated:YES completion:^{
//                UINavigationController *nav = (UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController;
//                PlanMapVC *vc = [[PlanMapVC alloc] initWithNibName:@"PlanMapVC" bundle:nil];
//                [nav pushViewController:vc animated:YES];
//        }];
//
//    }else{
//        [self dismissViewControllerAnimated:YES completion:^{
//
//        }];
//    }
    
//    [self dismissViewControllerAnimated:YES completion:^{}];
	
	//네비게이션 애니메이션으로 인해 잔상같이 남는 현상 수정
	[self.centerView setClipsToBounds:YES];
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Action

- (IBAction)onClickNext:(id)sender {
    NSInteger nextPage = self.pageControl.currentPage + 1;
    if(nextPage == [self.arrayPageInfo count]){
        [self endTutorial];
        return;
    }
    
    if([self.arrayPageInfo count] - 1 == nextPage){
        [self.btnNext setTitle:@"END" forState:UIControlStateNormal];
    }else{
        [self.btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
    }
    
    [self pageChangPage:nextPage];
}

- (IBAction)onClickSkip:(id)sender {
    [self endTutorial];
}

@end
