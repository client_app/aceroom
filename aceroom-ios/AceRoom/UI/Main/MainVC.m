//
//  MainVC.m
//  Unity-iPhone
//
//  Created by 김태원 on 04/09/2019.
//

#import "MainVC.h"

@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //plugin
//    [AcePlugin callEccoParam:@""];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onClickMoveARVuforia:(id)sender {
    //Vuforia AR
    [kAcePlugin changeSceneARMattress];
}

- (IBAction)onClickARLens:(id)sender {
    //Native
    //Urbanbase AR Lens
}

- (IBAction)onClickMoveHomeEditor:(id)sender {
    //Urbanbase Crane (Home Editor)
//    [AcePlugin changeSceneHomeEditor:@""];
    //TODO: test
    [kAcePlugin changeSceneHomeEditor:@"{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1c271da53ab8ad712e9.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}"];
    
    
}

@end
