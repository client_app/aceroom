//
//  MainViewController.m
//  StrangeUI
//
//  Created by Jeonghoon Lee on 25/09/2019.
//  Copyright © 2019 codefac. All rights reserved.
//

#import "MainViewController.h"
#import "PlanMapVC.h"
#import "ARLensVC.h"
#import "TutorialVC.h"
//#import "Unity-iPhonne-Swift.h"


#define VIEW_WIDTH 80.0

@interface MainViewController ()
@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, assign) BOOL isCarouselHidden;
@end

@implementation MainViewController
@synthesize carousel;
@synthesize items;
@synthesize btnPlus;
@synthesize lbDebug;


- (void)setUp
{
    self.wrap = YES;
    self.isCarouselHidden = YES;
    
    self.items = [NSMutableArray array];
    for (int i = 0; i < 6; i++)
    {
        int v = i % 3 ;
        [self.items addObject:@(v)];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
        [self setUp];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self setUp];
    }
    return self;
}

- (void)dealloc
{
    //it's a good idea to set these to nil here to avoid
    //sending messages to a deallocated viewcontroller
    carousel.delegate = nil;
    carousel.dataSource = nil;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self rotationScreen:UIInterfaceOrientationPortrait];
	NSLog(@"mainViewController viewWillAppear");
}
-(void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	NSLog(@"mainViewController viewDidDisappear");
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	NSLog(@"mainViewController viewDidAppear");
	[self.loadingView setHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self rotationScreen:UIInterfaceOrientationPortrait];
    [self setIsRotationLock:YES];
    self.isCarouselHidden = YES;
    [self.carousel setHidden:YES];
    self.carousel.type = iCarouselTypeWheel;
	
	NSMutableArray *ctp_kor_nm = [self loadJSONFile:[NSString stringWithFormat:@"%@",@"ctp_kor_nm"]];
	NSMutableArray *sig_kor_nm = [self loadJSONFile:@"sig_kor_nm"];
	
	[[UnityBridge sharedInstance] setCTP_NM:ctp_kor_nm];
	[[UnityBridge sharedInstance] setSIG_NM:sig_kor_nm];
	NSLog(@"ctp_kor_nm : [ %@ ]", [[UnityBridge sharedInstance] CTP_NM]);
	NSLog(@"sig_kor_nm : [ %@ ]", [[UnityBridge sharedInstance] SIG_NM]);
}

- (NSMutableArray *) loadJSONFile:(NSString *)nm
{
	NSMutableArray *returnData;
	
	NSArray *jsonArray = [[NSBundle mainBundle] pathsForResourcesOfType:@"json" inDirectory:nil];
	
	for(int i=0; i<[jsonArray count]; i++){
		NSString *path = [jsonArray objectAtIndex:i];
		if([path containsString:nm]){
			NSData *data = [NSData dataWithContentsOfFile:path];
			returnData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
			break;
		}
	}
	
	return returnData;
}

- (IBAction)btnPlusPressed:(id)sender {
    
    self.isCarouselHidden = !self.isCarouselHidden;
    
    if (self.isCarouselHidden)
    {
        [self.btnPlus setImage:[UIImage imageNamed:@"mainPlus"] forState:UIControlStateNormal];
    }
    else
    {
        //TODO:확인 필요
//		[kUnityBridge adbrixCustomAction:@"Main_Menu_open_menu_btn"];
        [self.btnPlus setImage:[UIImage imageNamed:@"mainClose"] forState:UIControlStateNormal];
    }
    [self.carousel setHidden:self.isCarouselHidden];
}

- (void)gotoRootVC
{
	NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray: self.navigationController.viewControllers];
	[self.navigationController popToViewController:[allViewControllers objectAtIndex:0] animated:NO];
}

- (void) selectMenu:(NSInteger) idx
{
	
	[self.loadingView setHidden:NO];
	
    if (idx == 0) //매트리스 분석
    {
		[kUnityBridge adbrixCustomAction:@"Main_Menu_AR3_btn"];
        if([[NSUserDefaults standardUserDefaults] boolForKey:USER_KEY_FIRST_AR_MATTRESS] != YES){
            [kUnityBridge OpenTutorial:SCENE_AR_MATTRESS closeHandler:^{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_KEY_FIRST_AR_MATTRESS];
                [self gotoRootVC];
                [kAcePlugin changeSceneARMattress];
            }];
        }else{
            [self gotoRootVC];
            [kAcePlugin changeSceneARMattress];
        }
    }
    else if (idx == 1) //AR침대배치
    {
		[kUnityBridge adbrixCustomAction:@"Main_Menu_AR2_btn"];
		if([[NSUserDefaults standardUserDefaults] boolForKey:USER_KEY_FIRST_AR_LENS] != YES){
			
			[kUnityBridge OpenTutorial:NAME_AR_LENS closeHandler:^{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_KEY_FIRST_AR_LENS];
				[kUnityBridge ArViewOpen];
            }];
			
		}else{
			[kUnityBridge ArViewOpen];
		}
		/*
        if([[NSUserDefaults standardUserDefaults] boolForKey:USER_KEY_FIRST_AR_LENS] != YES){
            [kUnityBridge OpenTutorial:NAME_AR_LENS closeHandler:^{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_KEY_FIRST_AR_LENS];
                ARLensVC *vc = [[ARLensVC alloc] initWithNibName:@"ARLensVC" bundle:nil];
                [self.navigationController pushViewController:vc animated:YES];
            }];
        }else{
            ARLensVC *vc = [[ARLensVC alloc] initWithNibName:@"ARLensVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:YES];
        }
		 */
    }
    else //@"3D홈디자인"
    {
		[kUnityBridge adbrixCustomAction:@"Main_Menu_AR1_btn"];
		if([[NSUserDefaults standardUserDefaults] boolForKey:USER_KEY_FIRST_HOME_EDITOR] != YES){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_KEY_FIRST_HOME_EDITOR];
            [kUnityBridge OpenTutorial:SCENE_HOME_EDITOR closeHandler:^{
                PlanMapVC *vc = [[PlanMapVC alloc] initWithNibName:@"PlanMapVC" bundle:nil];
				[self.navigationController pushViewController:vc animated:YES];
            }];
        }else{
            PlanMapVC *vc = [[PlanMapVC alloc] initWithNibName:@"PlanMapVC" bundle:nil];
			[self.navigationController pushViewController:vc animated:YES];
        }
    }
    
}

- (void) setTitleText:(NSInteger) idx
{
    if (idx == 0)
    {
        self.lbDebug.text = @"매트리스 분석";
    }
    else if (idx == 1)
    {
        self.lbDebug.text = @"AR침대배치";
    }
    else
    {
        self.lbDebug.text = @"3D홈디자인";
    }
    
}

#pragma mark - 메뉴 UI 구성용 iCarousel
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel
{
    return (NSInteger)[self.items count];
}

- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    
    UIImageView *onView = nil;
    UIImageView *offView = nil;
    
    int idx = index % 3;
    
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_WIDTH)];
        
        onView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 72, 72)];
        [onView setCenter:CGPointMake(VIEW_WIDTH / 2 , VIEW_WIDTH / 2)];
        
        onView.image = [UIImage imageNamed:[NSString stringWithFormat:@"menu%dOn", idx + 1]];
        onView.contentMode = UIViewContentModeScaleAspectFit;
        onView.tag = 1;
        
        [view addSubview:onView];
        
        offView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 61, 61)];
        [offView setCenter:CGPointMake(VIEW_WIDTH / 2 , VIEW_WIDTH / 2)];
        
        offView.image = [UIImage imageNamed:[NSString stringWithFormat:@"menu%d", idx + 1]];
        offView.contentMode = UIViewContentModeScaleAspectFit;
        offView.tag = 2;
        [view addSubview:offView];
        
        if (index == 0)
        {
            [onView setHidden:NO];
            [offView setHidden:YES];
        }
        else
        {
            [onView setHidden:YES];
            [offView setHidden:NO];
        }
        
        
    }
    else
    {

        onView = (UIImageView *)[view viewWithTag:1];
        onView.image = [UIImage imageNamed:[NSString stringWithFormat:@"menu%dOn", idx + 1]];
        offView = (UIImageView *)[view viewWithTag:2];
        offView.image = [UIImage imageNamed:[NSString stringWithFormat:@"menu%d", idx + 1]];
        [onView setHidden:YES];
        [offView setHidden:NO];
    }

    return view;
}



//- (CATransform3D)carousel:(__unused iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
//{
//    //implement 'flip3D' style carousel
//    transform = CATransform3DRotate(transform, M_PI / 8.0, 0.0, 1.0, 0.0);
//    return CATransform3DTranslate(transform, 0.0, 0.0, offset * self.carousel.itemWidth*2);
//}

- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return self.wrap;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.15;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}

#pragma mark -
#pragma mark iCarousel taps

- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"---------didSelectItemAtIndex");
    NSNumber *item = (self.items)[(NSUInteger)index];
    NSLog(@"Tapped view number: %@", item);
//    NSLog(@"Tapped index: %d", index);
//    NSLog(@"current index: %d", self.carousel.currentItemIndex);
//    
    if (index == self.carousel.currentItemIndex)
    {
        [self selectMenu:item.integerValue];
    }
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel
{
    NSLog(@"---------carouselCurrentItemIndexDidChange");
    [carousel reloadData];
}

- (void)carouselWillBeginScrollingAnimation:(iCarousel *)carousel
{
    NSLog(@"---------carouselWillBeginScrollingAnimation");
}

-(void)carouselDidEndScrollingAnimation:(iCarousel *)carousel
{
    NSLog(@"++++++++++ carouselDidEndScrollingAnimation");
    for (int n = 0 ; n < self.items.count ; n ++)
    {
        UIView *view = (UIView *) [self.carousel itemViewAtIndex:n];
        UIImageView *onView = (UIImageView *) [view viewWithTag:1];
        UIImageView *offView = (UIImageView *) [view viewWithTag:2];

        if (n == self.carousel.currentItemIndex)
        {
            [onView setHidden:NO];
            [offView setHidden:YES];
        }
        else
        {
            [onView setHidden:YES];
            [offView setHidden:NO];
        }
    }

    [self setTitleText:self.carousel.currentItemIndex % 3];
}
-(void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate
{
    NSLog(@"++++++++++ carouselDidEndDragging");
}
- (void)carouselWillBeginDragging:(__unused iCarousel *)carousel
{
    NSLog(@"++++++++++ carouselWillBeginDragging");
    for (int n = 0 ; n < self.items.count ; n ++)
    {
        UIView *view = (UIView *) [self.carousel itemViewAtIndex:n];
        UIImageView *onView = (UIImageView *) [view viewWithTag:1];
        UIImageView *offView = (UIImageView *) [view viewWithTag:2];
        
        [onView setHidden:YES];
        [offView setHidden:NO];
    }
}
-(void)carouselDidEndDecelerating:(iCarousel *)carousel
{
    NSLog(@"++++++++++ carouselDidEndDecelerating");
//    for (int n = 0 ; n < self.items.count ; n ++)
//    {
//        UIView *view = (UIView *) [self.carousel itemViewAtIndex:n];
//        UIImageView *onView = (UIImageView *) [view viewWithTag:1];
//        UIImageView *offView = (UIImageView *) [view viewWithTag:2];
//
//        if (n == self.carousel.currentItemIndex)
//        {
//            [onView setHidden:NO];
//            [offView setHidden:YES];
//        }
//        else
//        {
//            [onView setHidden:YES];
//            [offView setHidden:NO];
//        }
//    }
//
//    [self setTitleText:self.carousel.currentItemIndex % 3];
}
- (void)carouselWillBeginDecelerating:(__unused iCarousel *)carousel
{
    NSLog(@"++++++++++ carouselWillBeginDecelerating");
//    for (int n = 0 ; n < self.items.count ; n ++)
//    {
//        UIView *view = (UIView *) [self.carousel itemViewAtIndex:n];
//        UIImageView *onView = (UIImageView *) [view viewWithTag:1];
//        UIImageView *offView = (UIImageView *) [view viewWithTag:2];
//
//        [onView setHidden:YES];
//        [offView setHidden:NO];
//    }
}


////////////

- (IBAction)onClickHorizontal:(id)sender {
    
//    [NSThread sleepForTimeInterval:2.000];
    
    [self rotationScreen:UIInterfaceOrientationLandscapeLeft];
    PlanMapVC *vc = [[PlanMapVC alloc] initWithNibName:@"PlanMapVC" bundle:nil];
    [self presentViewController:vc animated:YES completion:^{
        //        [self dismissViewControllerAnimated:NO completion:nil];
//        [kAcePlugin callUnityLotationLandscape];
    }];
}

- (IBAction)onClickVertical:(id)sender {
//    [self rotationScreen:UIInterfaceOrientationPortrait];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"file path : %@", paths);
    for (NSString *path in paths) {
        
        NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
        for (int i = 0; i < directoryContent.count; i++) {
            NSLog(@"root %@ File %d: %@", path, i, [directoryContent objectAtIndex:i]);
        }
    }
    
    
//    UIImage *screenshot = [UIImage imageWithContentsOfFile:@"file://var/mobile/Containers/Data/Application/CD4BD14D-8688-475F-98FA-7A9279CB9B05/Documents/SS_10-9-2019_11-45-20_PM.png"];
//    NSArray *items = @[@"Ace Room", screenshot];
//    // build an activity view controller
//    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
//    // and present it
//    UIViewController *rootVC = UnityGetGLViewController();
//    [rootVC presentViewController:controller animated:YES completion:^{
//        // executes after the user selects something
//    }];
}

/*!
   테스트용 이벤트. 사용안함
*/
- (IBAction)onClickMoveHomeEditorTest:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    [self rotationScreen:UIInterfaceOrientationLandscapeLeft];
    [kAcePlugin changeSceneHomeEditor:@"{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1c271da53ab8ad712e9.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}"];
    
//    [self dismissViewControllerAnimated:YES completion:^{
//        [self rotationScreen:UIInterfaceOrientationLandscapeRight];
//        [kAcePlugin changeSceneHomeEditor:@"{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1c271da53ab8ad712e9.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}"];
//    }];
//
//    [self dismissViewControllerAnimated:YES completion:^{
//        [self rotationScreen:UIInterfaceOrientationLandscapeRight];
//        [kAcePlugin changeSceneHomeEditor:@"{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1c271da53ab8ad712e9.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}"];
//    }];
    
//    [self rotationScreen:UIInterfaceOrientationLandscapeLeft];
//    
//    LGAlertView *alert = [LGAlertView getHomeEditorOptionAlertWithOption:@"{\"effect\":2,\"collision\":true}"];
//    alert.dismissOnAction = NO;
//    [alert showAnimated:YES completionHandler:^{
//        
//    }];
    
}


@end
