//
//  MainViewController.h
//  StrangeUI
//
//  Created by Jeonghoon Lee on 25/09/2019.
//  Copyright © 2019 codefac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
NS_ASSUME_NONNULL_BEGIN
/*!
   메인 메뉴 화면
*/
@interface MainViewController : XCCommonVC <iCarouselDataSource, iCarouselDelegate>
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (nonatomic, strong) IBOutlet UIButton *btnPlus;
@property (nonatomic, strong) IBOutlet UILabel *lbDebug;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@end

NS_ASSUME_NONNULL_END
