//
//  CommonUtil.h
//  AceRoom
//
//  Created by 김태원 on 09/04/2019.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/*!
    공통 메소드 모음
 */
@interface CommonUtil : NSObject
/*!
    NSDate타입을 YYYYMMDD 형식에 맞춰 리턴
 */
+(NSString *)dateFormatYYYYMMDD:(NSDate *)date decimal:(NSString *)decimal;
/*!
   YYYYMMDD 포멧으로 되어있는 String을 NSDate로 변환
*/
+ (NSDate *)stringToDateYYYYMMDD:(NSString *)date decimal:(NSString *)decimal;

/*!
   두 날짜 간 차이(일수)를 반환
*/
+ (int)compareDateFrom:(NSDate *)fromDate to:(NSDate *)toDate;

/*!
   UUID 생성
*/
+(NSString *)getUUID;
/*!
   "Y" 또는 "N"을 bool 값으로 리턴
*/
+(NSString *)getYN:(BOOL)isYN;
/*!
   NSDictionary(Json 타입) 을 String으로 변환해서 반환
*/
+(NSString*) convertDictionaryToString:(NSMutableDictionary*) dict;
/*!
   디바이스에 노치가 있는지 유무
*/
+(BOOL)isNortchPhone;
/*!
   UIInterfaceOrientation을 UIInterfaceOrientationMask으로 변환
*/
+(UIInterfaceOrientationMask)getUIInterfaceOrientationMaskFromOrientations:(UIInterfaceOrientation) orientation;

+(BOOL)hasTopNotch;
@end

NS_ASSUME_NONNULL_END
