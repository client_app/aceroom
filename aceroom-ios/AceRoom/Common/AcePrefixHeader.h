//
//  AcePrefixHeader.h
//  Unity-iPhone
//
//  Created by 김태원 on 04/09/2019.
//

#ifndef AcePrefixHeader_h
#define AcePrefixHeader_h

#pragma mark - Apple Framework
#import <UserNotifications/UserNotifications.h>

#pragma mark - Pods
#include <BlocksKit/BlocksKit.h>
#include <BlocksKit/BlocksKit+UIKit.h>
#include <Toast/UIView+Toast.h>
#include <AFNetworking/AFNetworking.h>
#include <KeychainItemWrapper/KeychainItemWrapper.h>
#include <JSONModel/JSONModel.h>
#include <DBFlatPicker/DBFlatPicker-umbrella.h>
#include <ARViewer.framework/Headers/ARViewer.h>
#import <AdBrixRM/AdBrixRM.h>



#pragma mark - Common
#include "../AceRoom/Common/Environment.h"
#include "../AceRoom/Common/CommonUtil.h"
#import "../AceRoom/Common/UI/XCCommonVC.h"
#import "../AceRoom/Common/UI/XCCommonNVC.h"
#import "../AceRoom/Common/UI/XCAlert.h"

#pragma mark - Category
#import "NSDictionary+XC.h"
#import "NSDate+XC.h"
#import "NSArray+XC.h"
#import "UIAlertController+XC.h"
#import "NSString+XC.h"
#import "UIButton+XC.h"
#import "UIColor+XC.h"
#import "UIView+XC.h"
#import "LGAlertView+Acebed.h"
#import "CRToast+Ace.h"
#import "UIImage+XC.h"
#import "XCPickerCellView.h"

#pragma mark - UnityPlugin

#import "../../Libraries/Plugins/iOS/AcePlugin.h"
#import "../UnityPlugin/UnityBridge.h"

#endif /* AcePrefixHeader_h */
