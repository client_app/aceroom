//
//  XCCommonVC.h
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^XCVcCloseHandler)(void);

@interface XCCommonVC : UIViewController
@property (nonatomic) BOOL isRotationLock;

-(void)rotationScreen:(UIInterfaceOrientation) orientation;

-(void)setCloseHandler:(XCVcCloseHandler)closeHandler;


@end

NS_ASSUME_NONNULL_END
