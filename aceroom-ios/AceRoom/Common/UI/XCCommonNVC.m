//
//  XCCommonNVC.m
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "XCCommonNVC.h"

@interface XCCommonNVC ()

@end

@implementation XCCommonNVC

-(id)initWithRootViewController:(UIViewController *)rootViewController
{
	self = [super initWithRootViewController:rootViewController];
	if(self)
		self.delegate = self;
	
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	self.delegate = self;
    // Do any additional setup after loading the view.
    _mUInterfaceOrientationMask = UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return _mUInterfaceOrientationMask;
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    NSLog(@"didShow1 ViewController:%@", viewController);
    NSLog(@"didShow2 ViewController:%@", _pushedVC);

    if (self.completionBlock && [_pushedVC isEqual:viewController]) {
        self.completionBlock();
		
		self.completionBlock = nil;
		self.pushedVC = nil;
    }
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated completion:(dispatch_block_t)completion {
    self.pushedVC = viewController;
	NSLog(@"pushed ViewController:%@", _pushedVC);
    self.completionBlock = completion;
    [self pushViewController:viewController animated:animated];
}

@end
