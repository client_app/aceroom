//
//  XCAlert.m
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "XCAlert.h"

@interface XCAlert ()

@property UIAlertActionStyle actionStyle;

@end

@implementation XCAlert

+ (instancetype)init{
    
    XCAlert *alert =[XCAlert
                     alertControllerWithTitle:@""
                     message:@""
                     preferredStyle:UIAlertControllerStyleAlert];
    
    return alert;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)addTitle:(NSString *)title{
    [self setTitle:title];
}
- (void)addMessage:(NSString *)message{
    [self setMessage:message];
}

- (void)addButton:(NSString *)title action:(XCAlertAction)actionBlock
{
    [self addButton:title actionStyle:UIAlertActionStyleDefault action:actionBlock];
}

- (void)addCancelButton:(NSString *)title action:(XCAlertAction)actionBlock
{
    [self addButton:title actionStyle:UIAlertActionStyleCancel action:actionBlock];
}
- (void)addDestructiveButton:(NSString *)title action:(XCAlertAction)actionBlock
{
    [self addButton:title actionStyle:UIAlertActionStyleDestructive action:actionBlock];
}


- (void)addButton:(NSString *)title actionStyle:(UIAlertActionStyle)style action:(XCAlertAction) actionBlock
{
    UIAlertAction *addAction = [UIAlertAction
                                   actionWithTitle:title
                                   style:style
                                   handler:^(UIAlertAction *action)
                                   {
                                       if(actionBlock){
                                            actionBlock();
                                       }
                                   }];
    [self addAction:addAction];
}
- (void)addView:(UIView *)view{
    
}
- (void)show{
    
    XCCommonNVC *parentNC = (XCCommonNVC *)[[[[UIApplication sharedApplication]delegate] window] rootViewController];
    
    if([parentNC isKindOfClass:[XCCommonNVC class]]){
        
        NSArray *vcList = [parentNC viewControllers];
        UIViewController *lastVC = [vcList lastObject];
        [lastVC presentViewController:self animated:YES completion:nil];
        
    }else{
        [parentNC presentViewController:self animated:YES completion:nil];
    }
}

- (void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)makeLodingIndecator{
    [self setTitle:@"잠시 기다려주세요...\n\n"];
    
    UIActivityIndicatorView* indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.color = [UIColor blackColor];
    indicator.translatesAutoresizingMaskIntoConstraints=NO;
    [self.view addSubview:indicator];
    NSDictionary * views = @{@"pending" : self.view, @"indicator" : indicator};
    
    NSArray * constraintsVertical = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[indicator]-(20)-|" options:0 metrics:nil views:views];
    NSArray * constraintsHorizontal = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[indicator]|" options:0 metrics:nil views:views];
    NSArray * constraints = [constraintsVertical arrayByAddingObjectsFromArray:constraintsHorizontal];
    [self.view addConstraints:constraints];
    [indicator setUserInteractionEnabled:NO];
    [indicator startAnimating];
}


- (void)showErrorAlertWithMessage:(NSString *)message action:(nullable XCAlertAction)actionBlock{
    [self addTitle:@"알림"];
    [self addMessage:message];
    [self addButton:@"확인" action:^{
        if(actionBlock != nil){
            actionBlock();
        }
    }];
    [self show];
}

@end
