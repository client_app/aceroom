//
//  XCPickerCellView.m
//  Unity-iPhone
//
//  Created by SangYeonE on 24/10/2019.
//

#import "XCPickerCellView.h"

@implementation XCPickerCellView

-(id)init
{
	self = [super init];
	
	if(self){
		
	}
	
	return self;
}

-(id)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	
	if(self){
		
	}
	
	return self;
}

-(void)setSelected:(BOOL)isSel
{
	if(isSel==YES){
		[self setBackgroundColor:[UIColor colorWithRed:233.0/255 green:245.0/255 blue:251.0/255 alpha:1]];
	}else{
		[self setBackgroundColor:[UIColor whiteColor]];
	}
}

@end
