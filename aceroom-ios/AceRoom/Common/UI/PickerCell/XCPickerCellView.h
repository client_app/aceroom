//
//  XCPickerCellView.h
//  Unity-iPhone
//
//  Created by SangYeonE on 24/10/2019.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XCPickerCellView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lbl_type;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;

-(void)setSelected:(BOOL)isSel;
@end

NS_ASSUME_NONNULL_END
