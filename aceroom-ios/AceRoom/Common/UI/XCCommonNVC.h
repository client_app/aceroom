//
//  XCCommonNVC.h
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XCCommonNVC : UINavigationController <UINavigationControllerDelegate>

@property(nonatomic) UIInterfaceOrientationMask mUInterfaceOrientationMask;

@property (nonatomic,copy) dispatch_block_t completionBlock;
@property (nonatomic,strong) UIViewController * pushedVC;
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated completion:(dispatch_block_t)completion;

@end

NS_ASSUME_NONNULL_END
