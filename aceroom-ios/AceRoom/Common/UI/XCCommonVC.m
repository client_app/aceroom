//
//  XCCommonVC.m
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "XCCommonVC.h"

@interface XCCommonVC ()
@property (strong, nonatomic, nullable) XCVcCloseHandler closeHandler;
@property (nonatomic) UIInterfaceOrientation mLastOrientation;
@end

@implementation XCCommonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setLastInterfaceOrientation:[[UIDevice currentDevice] orientation]];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self rotationScreen:_mLastOrientation];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if(_closeHandler != nil){
        _closeHandler();
    }
}

-(BOOL)isBeingDismissed{
    return [super isBeingDismissed];
}

-(void)setCloseHandler:(XCVcCloseHandler)closeHandler{
    _closeHandler = closeHandler;
}



-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if(self.isRotationLock == YES){
        return [CommonUtil getUIInterfaceOrientationMaskFromOrientations:self.mLastOrientation];
    }else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

-(void)rotationScreen:(UIInterfaceOrientation) orientation
{
    self.mLastOrientation = orientation;
    [kUnityBridge setOrientation:[CommonUtil getUIInterfaceOrientationMaskFromOrientations:orientation]];
//    deviceRotation = [CommonUtil getUIInterfaceOrientationMaskFromOrientations:orientation];
//    [kUnityBridge setDeviceMask:[CommonUtil getUIInterfaceOrientationMaskFromOrientations:orientation]];
    
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger: orientation] forKey:@"orientation"];
}

-(void)setLastInterfaceOrientation:(UIDeviceOrientation) deviceOrientation
{
    switch (deviceOrientation) {
        case UIDeviceOrientationUnknown:             // Device oriented vertically, home button on the bottom
            {
                _mLastOrientation = UIInterfaceOrientationUnknown;
            }
            break;
        case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
            {
                _mLastOrientation = UIInterfaceOrientationPortrait;
            }
            break;
        case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
            {
                _mLastOrientation = UIInterfaceOrientationPortraitUpsideDown;
            }
            break;
        case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
            {
                _mLastOrientation = UIInterfaceOrientationLandscapeLeft;
            }
            break;
        case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
            {
                _mLastOrientation = UIInterfaceOrientationLandscapeRight;
            }
            break;
        case UIDeviceOrientationFaceUp:              // Device oriented flat, face up
            {
                _mLastOrientation = UIInterfaceOrientationUnknown;
            }
            break;
        case UIDeviceOrientationFaceDown:            // Device oriented flat, face down
            {
                _mLastOrientation = UIInterfaceOrientationUnknown;
            }
            break;
        default:
            {
                _mLastOrientation = UIInterfaceOrientationUnknown;
            }
            break;
    }
}

@end
