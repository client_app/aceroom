//
//  XCAlert.h
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^XCAlertAction)(void);

@interface XCAlert : UIAlertController

+ (instancetype)init;

- (void)addTitle:(NSString *)title;
- (void)addMessage:(NSString *)message;
- (void)addButton:(NSString *)title action:(XCAlertAction)actionBlock;
- (void)addCancelButton:(NSString *)title action:(XCAlertAction)actionBlock;
- (void)addDestructiveButton:(NSString *)title action:(XCAlertAction)actionBlock;
- (void)addButton:(NSString *)title actionStyle:(UIAlertActionStyle)style action:(XCAlertAction)actionBlock;
- (void)addView:(UIView *)view;
- (void)show;
- (void)dismiss;
- (void)makeLodingIndecator;
- (void)showErrorAlertWithMessage:(NSString *)message action:(nullable XCAlertAction)actionBlock;

@end

NS_ASSUME_NONNULL_END
