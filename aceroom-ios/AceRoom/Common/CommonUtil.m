//
//  CommonUtil.m
//  AceRoom
//
//  Created by 김태원 on 09/04/2019.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "CommonUtil.h"
#import <AdSupport/ASIdentifierManager.h>

@implementation CommonUtil

+ (NSString *)dateFormatYYYYMMDD:(NSDate *)date decimal:(NSString *)decimal{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    NSString * formatString = [NSString stringWithFormat:@"YYYY%@MM%@dd", decimal, decimal];
    [formatter setDateFormat:formatString];
    return [formatter stringFromDate:date];
}

+ (NSDate *)stringToDateYYYYMMDD:(NSString *)date decimal:(NSString *)decimal{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[NSString stringWithFormat:@"YYYY%@MM%@dd", decimal, decimal]];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    NSDate *ddate = [dateFormatter dateFromString: date];
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *components=[[NSDateComponents alloc] init];
    components.day=1;
    NSDate *targetDate =[calendar dateByAddingComponents:components toDate:ddate options: 0];
    
    return targetDate;
    
}

+ (int)compareDateFrom:(NSDate *)fromDate to:(NSDate *)toDate{
    
    double dFromDate = [fromDate timeIntervalSince1970];
    double dToDate = [toDate timeIntervalSince1970];
    
    double dDaySecond = dToDate - dFromDate;
    
    double dDay = ((dDaySecond / 60) / 60) / 24;
    
    return ceil(dDay);
}


+(NSString *)getUUID{
    CommonUtil *util = [[CommonUtil alloc] init];
    KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:KEYCHAIN_KEY_UUID accessGroup:nil];
    NSString *uuid = [keychain objectForKey: (id)kSecValueData];
    
    if(uuid == nil || uuid.length == 0){
        uuid = [util udidUsingiOS6ASIdentifier];  //광고식별자이용
        if(uuid == nil || uuid.length == 0){
            uuid = [util udidUsingiOS6UIDevice];  //배포계정앱
            if(uuid == nil || uuid.length == 0){
                //기존 UUID. 앱 새로 설치때마다 바뀐다
                CFUUIDRef uuidRef = CFUUIDCreate(NULL);
                CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef); CFRelease(uuidRef);
                uuid = [NSString stringWithString:(NSString *) CFBridgingRelease(uuidStringRef)];
                CFRelease(uuidStringRef);
            }
        }
    }
    
    // save UUID in keychain [wrapper setObject:uuid forKey:kSecAttrAccount];
    [util updateUUID:uuid];
    
    return uuid;
}

/**
 kSecAttrAccount : Account 정보
 kSecAttrLabel : 라벨 정보
 kSecAttrDescription : 설명
 kSecValueData : Data
 
 출처: http://10apps.tistory.com/139 [10 Apps - iPhone 실전 프로젝트 따라하기]
 */
-(void)updateUUID:(NSString *)uuid{
     KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:KEYCHAIN_KEY_UUID accessGroup:nil];
    [keychain setObject: BUNDLE_ID forKey: (id)kSecAttrAccount];
    [keychain setObject: uuid forKey: (id)kSecAttrAccount];
}

/**
 * 배포계정 앱 쉐어용
 * 앱 제작자의 앱이 1개라도 남아있다면 동일 키값을 받게 된다. 모든 앱 삭제시 값 변경됨
 **/
- (NSString*) udidUsingiOS6UIDevice {
    // can use iOS 6.0 and later
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

/**
 * 광고식별자(광고추적) 이용
 * 사용자가 광고허용을 취소하면 값이 바뀐다
 **/
- (NSString*) udidUsingiOS6ASIdentifier {
    // can use iOS 6.0 and later
    ASIdentifierManager *manager = [ASIdentifierManager sharedManager]; return [[manager advertisingIdentifier] UUIDString];
    
}

+(NSString *)getYN:(BOOL)isYN{
    return isYN == YES ? @"Y" : @"N";
}

+(NSString*) convertDictionaryToString:(NSMutableDictionary*) dict
{
    NSError* error;
    NSDictionary* tempDict = [dict copy]; // get Dictionary from mutable Dictionary
    //giving error as it takes dic, array,etc only. not custom object.
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:tempDict
                                                       options:NSJSONReadingMutableLeaves error:&error];
    NSString* nsJson=  [[NSString alloc] initWithData:jsonData
                                             encoding:NSUTF8StringEncoding];
    return nsJson;
}

+(BOOL)isNortchPhone
{
    UINavigationController *nav = (UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController;
    if(nav.view.safeAreaInsets.top > 20){
        return YES;
    }else{
        return NO;
    }
}


+(UIInterfaceOrientationMask)getUIInterfaceOrientationMaskFromOrientations:(UIInterfaceOrientation) orientation
{
    switch (orientation) {
        case UIDeviceOrientationUnknown:             // Device oriented vertically, home button on the bottom
            {
                return UIInterfaceOrientationMaskPortrait;
            }
            break;
        case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
            {
                return UIInterfaceOrientationMaskPortrait;
            }
            break;
        case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
            {
                return UIInterfaceOrientationMaskPortraitUpsideDown;
            }
            break;
        case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
            {
                return UIInterfaceOrientationMaskLandscapeLeft;
            }
            break;
        case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
            {
                return UIInterfaceOrientationMaskLandscapeRight;
            }
            break;
        default:
            {
                return UIInterfaceOrientationMaskPortrait;
            }
            break;
    }
}

+(BOOL)hasTopNotch {
    if (@available(iOS 11.0, *)) {
        return [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.top > 20.0;
    }

    return  NO;
}

@end
