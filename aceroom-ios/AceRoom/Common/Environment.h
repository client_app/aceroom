//
//  Environment.h
//  AceRoom
//
//  Created by 김태원 on 09/04/2019.
//  Copyright © 2018 김태원. All rights reserved.
//

#ifndef Environment_h
#define Environment_h

/* 유틸 */

#define APPDELEGATE [[UIApplication sharedApplication] delegate]
#define APP_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
#define STRING(TEXT) NSLocalizedString(TEXT, @"")
#define BUNDLE_ID [[NSBundle mainBundle] bundleIdentifier]
#define KEYCHAIN_KEY_UUID [NSString stringWithFormat:@"%@.%@", BUNDLE_ID, @"uuid"]
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)


/* Unity Plugin */

#define kAcePlugin [AcePlugin sharedInstance]
#define kUnityBridge [UnityBridge sharedInstance]



/* API URL */
#define URBAN_API_DOMAIN @"https://apis.urbanbase.com/"
#define URBAN_MAP_API_URL [NSString stringWithFormat:@"%@v3/buildings",URBAN_API_DOMAIN]
#define URBAN_BUILINGS_API_URL(uuid) [NSString stringWithFormat:@"%@v3/buildings/%@",URBAN_API_DOMAIN, uuid]
#define URBAN_PRODUCT_API_DOMAIN @"http://dtribe.youyoung.net"
#define URBAN_3D_API_DOMAIN @"https://d1wjr0mdrcotdx.cloudfront.net"
//#define API_BED @"/json/productList.json"
#define API_BED @"/json/productList_addLanding.json" // 랜딩페이지 테스트용
//#define API_BED @"/json/productList_200214.json" //어반베이스 테스트용
//#define API_BED @"/json/productList_twin.json"
//#define API_BED @"/json/productList_200318.json"
//#define API_BED @"/json/productList_200414.json"
#define API_WALL @"/json/wallfloorList.json" 
#define API_EVENT @"/json/event_ace.json" //어반베이스 AR용
//#define API_EVENT @"/json/event_ace_200106.json" //어반베이스 AR용

/* UserDefault Key*/
#define USER_KEY_FIRST_AR_MATTRESS @"FirstContractArMattress"
#define USER_KEY_FIRST_AR_LENS @"FirstContractArLens"
#define USER_KEY_FIRST_HOME_EDITOR @"FirstContractHomeEditor"

//홈에디터 - 기존 지도 위치 저장용
#define LAST_MAP_POSITION @"LastMapPosition"


//Util
#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
#define IPHONE   UIUserInterfaceIdiomPhone

//AdBrix
#define kADBRIX_KEY_APP @"fHo66gcr5kmK5IaXsgUAeQ"
#define kADBRIX_KEY_SECRET @"ygiU06Xq50eXyvMUs1NHSg"

#endif /* Environment_h */
