//
//  NSDate+XC.m
//  eexams
//
//  Created by 김태원 on 07/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "NSDate+XC.h"

@implementation NSDate (XC)

-(NSDate *)dateTimeToZero{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: self];
    [components setHour: 12];
    [components setMinute: 00];
    [components setSecond: 00];
    return [gregorian dateFromComponents: components];
}

+ (NSString *)getTodayYYYYMMDD{
    return [NSDate dateFormatYYYYMMDD:[NSDate date] decimal:@"-"];
}

+ (NSString *)dateFormatYYYYMMDD:(NSDate *)date decimal:(NSString *)decimal{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    NSString * formatString = [NSString stringWithFormat:@"YYYY%@MM%@dd", decimal, decimal];
    [formatter setDateFormat:formatString];
    return [formatter stringFromDate:date];
}

 - (NSString *)getYYYYMMDDWithDecimal:(NSString *)decimal{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    NSString * formatString = [NSString stringWithFormat:@"YYYY%@MM%@dd", decimal, decimal];
    [formatter setDateFormat:formatString];
    return [formatter stringFromDate:self];
}

- (double)dateToServerDate{
    double date = [self timeIntervalSince1970];
    return date * 1000;
}

- (NSString *)dateToServerString{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    NSString * formatString = @"YYYY-MM-dd HH:mm:ss";
    [formatter setDateFormat:formatString];
    return [formatter stringFromDate:self];
}



@end
