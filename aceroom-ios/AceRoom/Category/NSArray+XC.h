//
//  NSArray+XC.h
//  eexams
//
//  Created by 김태원 on 11/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (XC)
/**
 NSArray를 NSMutableArray로 변환한다.
 
 @return NSMutableArray
 */
- (NSMutableArray*)mutableArray;

@end

NS_ASSUME_NONNULL_END
