//
//  LGAlertView+Acebed.m
//  Unity-iPhone
//
//  Created by 김태원 on 07/10/2019.
//

#import "LGAlertView+Acebed.h"
#import "HomeEditorOptionAlertView.h"


@implementation LGAlertView (Acebed)

+(_Nonnull instancetype) makeAlertTitle:(nullable NSString *)title
                                message:(nonnull NSString *)message
                          confirmButtons:(nullable NSArray*)confirmButtons
                           cancelButton:(nullable NSString *)cancel
                              disButton:(nullable NSString *)disButton
                               delegate:(nullable id<LGAlertViewDelegate>)delegate{
    
    return [self alertViewWithTitle:title
                            message:message
                              style:LGAlertViewStyleAlert
                       buttonTitles:confirmButtons
                  cancelButtonTitle:cancel
             destructiveButtonTitle:disButton
                           delegate:delegate];
}


+(void)defaultAlertNoTitle:(NSString *)message {
    
     LGAlertView *alertView = [self makeAlertTitle:nil
                                           message:message
                                    confirmButtons:@[@"확인"]
                                      cancelButton:nil
                                         disButton:nil
                                          delegate:nil];
    
    [alertView showAnimated];
    
}


+(void)defaultAlertTitle:(NSString * )title message:(NSString *)message {
    
    NSString *strTitle = nil;
    if(title && title.length > 0){
        strTitle = title;
    }
    
    LGAlertView *alertView = [self makeAlertTitle:strTitle
                                          message:message
                                   confirmButtons:@[@"확인"]
                                     cancelButton:nil
                                        disButton:nil
                                         delegate:nil];
    
    [alertView showAnimated];
    
}



+(LGAlertView *)makeCustomViewAlertWithStyle:(LGAlertViewStyle)style
                                          view:(nullable UIView *)view
                                  buttonTitles:(nullable NSArray<NSString *> *)buttonTitles
                             cancelButtonTitle:(nullable NSString *)cancelButtonTitle
                        destructiveButtonTitle:(nullable NSString *)destructiveButtonTitle
                                      delegate:(nullable id<LGAlertViewDelegate>)delegate{
    
//    LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:nil
//                                                               message:nil
//                                                                 style:LGAlertViewStyleAlert
//                                                                  view:view
//                                                          buttonTitles:buttonTitles
//                                                     cancelButtonTitle:cancelButtonTitle
//                                                destructiveButtonTitle:destructiveButtonTitle
//                                                              delegate:delegate];
    
    
    LGAlertView *av = [LGAlertView alertViewWithViewAndTitle:nil message:nil style:LGAlertViewStyleAlert view:view buttonTitles:buttonTitles cancelButtonTitle:cancelButtonTitle destructiveButtonTitle:destructiveButtonTitle delegate:delegate];
//    [alertView setStyleAce];
    
    return av;

}

+(LGAlertView *)getHomeEditorOptionAlertWithOption:(NSString *)optionJson
{
    
    NSLog(@"JSON!!!!! %@", optionJson);
    HomeEditorOptionAlertView *alertView = (HomeEditorOptionAlertView *)[UIView getCustomViewFromXib:@"HomeEditorAlert" WithTag:0];
    LGAlertView *alert = [self makeCustomViewAlertWithStyle:LGAlertViewStyleAlert
                                                      view:alertView
                                              buttonTitles:nil
                                         cancelButtonTitle:nil
                                    destructiveButtonTitle:nil
                                                  delegate:nil];
    
    float viewWidth = 384.5f;
    float viewHeight = 287.f;
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    //가로 세로 중 짧은 쪽을 기준으로 가로너비를 변경(지원 앱 중 사양 상 가장 낮은 기기(iPhone SE(4인치)) 대응
    float screenMinSize = 0.f;
    if(screenSize.width > screenSize.height)
    {
        screenMinSize = screenSize.height;
    }else{
        screenMinSize = screenSize.width;
    }
    //4.7인치 기기(iphone 6, 6s, 7, 8)은 스크린 확대 기능을 쓰지 않는 이상 세로기준 가로너비가 375
    if(screenMinSize < 375.f)
    {
        viewWidth = 320.f;
    }
    
    alertView.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    
    [alertView setResultHandler:^(NSString * _Nonnull jsonString, BOOL isClose) {
        //option send?
        
        if(isClose){
            [alert dismiss];
        }
    }];
    
    [alertView setOption:optionJson];
    
    [alert setStyleAce];
    
    alert.width = viewWidth;
    alert.heightMax = viewHeight;
    
//    alert.innerView
//    CGRect frame = alert.innerView.frame;
//    frame.origin.y = frame.origin.y - 16.f;
//    alert.innerView.frame = frame;
//    [alert setShowsVerticalScrollIndicator:NO];
    
    
    
    return  alert;
}

#define ALERT_MARGIN 18.f
#define ALERT_WIDTH SCREEN_WIDTH - (ALERT_MARGIN * 2)
-(void)setStyleAce{
//    self.coverColor = [UIColor blackAlpha80];
    //    alertView.coverBlurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    self.coverAlpha = 0.80;
    self.layerCornerRadius = 0.0;
    self.layerBorderWidth = 0.0;
    self.backgroundColor = [UIColor whiteColor];
    self.width = ALERT_WIDTH;
    self.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
}

@end
