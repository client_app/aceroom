//
//  UIColor+XC.h
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (XC)
+(UIColor *)XCGreen;
+(UIColor *)XCYellow;
+(UIColor *)XCWhite;
+(UIColor *)XCGreen2;

/**
 Hex Color를 UIColor로 변환한다.

 @param hexString Hex ColorCode
 @return UIColor
 */
+ (UIColor *) colorWithHexString: (NSString *) hexString;
@end

NS_ASSUME_NONNULL_END
