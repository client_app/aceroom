//
//  NSString+XC.m
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "NSString+XC.h"

@implementation NSString (XC)
- (NSString *)trim{
    if(self == nil){
        return @"";
    }
    
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)validateEmptyText
{
    return [self isEqualToString:@""] || self.length == 0;
}

- (BOOL)validateEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

-(BOOL)validateTelNo{
    NSString *phoneNumber = self;
    phoneNumber = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet]invertedSet]]componentsJoinedByString:@""];
    
    NSString *someRegexp = @"^0\\d{9,10}$";
    NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", someRegexp];
    
    return [myTest evaluateWithObject: phoneNumber];
}

@end
