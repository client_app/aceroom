//
//  UIView+HPoint.m
//  HPoint
//
//  Created by Jay Lee on 2017. 2. 7..
//  Copyright © 2017년 HPoint. All rights reserved.
//

#import "UIView+XC.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (XC)
- (UIImage*)captureWithFrame:(CGRect)frame {
    UIImage *image = nil;
    CGFloat x = frame.origin.x;
    CGFloat y = frame.origin.y;
    CGSize size = frame.size;
    
    UIGraphicsBeginImageContext(size);
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(c, -x, -y);
    [self.layer renderInContext:c];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
    
}

-(void)drawRoundedBorderRadius:(CGFloat)radius borderWidth:(NSInteger)width color:(UIColor *)color{
    [self drawRoundRadius:radius];
    [self drawBoderWidth:width color:color];
}

-(void)drawBoderWidth:(NSInteger)width color:(UIColor *)color{
    self.layer.borderWidth = width;  // 테두리 두께
    self.layer.borderColor = color.CGColor;  // 테두리 컬러
}

-(void)drawRoundRadius:(CGFloat)radius{
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
}

+(UIView *)getCustomViewFromXib:(NSString *)xibName WithTag:(NSInteger) tag{
    NSArray *customViewList = [[NSBundle mainBundle] loadNibNamed:xibName owner:self options:nil];
    for (UIView *rowView in customViewList) {
        if(rowView.tag == tag){
            return rowView;
        }
    }
    
    return nil;
}

+(UIView *)getCustomViewFromXib:(NSString *)xibName WithIdentifier:(NSString *) identifier{
    NSArray *customViewList = [[NSBundle mainBundle] loadNibNamed:xibName owner:self options:nil];
    for (UIView *rowView in customViewList) {
        if([rowView.restorationIdentifier isEqualToString:identifier]){
            return rowView;
        }
    }
    
    return nil;
}


-(UIView *)viewWithIdentifier:(NSString *) identifier{
    UIView *view = nil;
    for (UIView *rowView in self.subviews) {
        if([rowView.restorationIdentifier isEqualToString:identifier]){
            view = rowView;
            break;
        }else{
            //아닌경우 하위 View에서 찾는다.
            if([rowView isKindOfClass:[UIView class]]){
                view = [rowView viewWithIdentifier:identifier];
            }
            
        }
    }
    return view;
}

- (void)developToast:(NSString *)message{
    [self makeToast:[NSString stringWithFormat:@"준비중입니다.\n%@", message]];
}

@end
