//
//  LGAlertView+Acebed.h
//  Unity-iPhone
//
//  Created by 김태원 on 07/10/2019.
//


#import <LGAlertView/LGAlertView.h>

NS_ASSUME_NONNULL_BEGIN

@interface LGAlertView (Acebed)

/**
 기본 Alert 생성

 @param title 제목
 @param message 내용
 @param confirmButtons Positive Button Name Array
 @param cancel 취소버튼 이름
 @param disButton 무시버튼 이름
 @param delegate LGAlertViewDelegate
 @return LGAlertView
 */
+(_Nonnull instancetype) makeAlertTitle:(nullable NSString *)title
                               message:(nonnull NSString *)message
                        confirmButtons:(nullable NSArray*)confirmButtons
                          cancelButton:(nullable NSString *)cancel
                             disButton:(nullable NSString *)disButton
                              delegate:(nullable id<LGAlertViewDelegate>)delegate;
/**
 상단 제목이 없는 기본 얼럿

 @param message 내용
 */
+(void)defaultAlertNoTitle:(nullable NSString *)message;

/**
 기본 메세지 표시용 얼럿

 @param title 제목
 @param message 내용
 */
+(void)defaultAlertTitle:(nullable NSString * )title message:(nullable NSString *)message;


+(LGAlertView * _Nonnull)makeCustomViewAlertWithStyle:(LGAlertViewStyle)style
                                                  view:(nullable UIView *)view
                                          buttonTitles:(nullable NSArray<NSString *> *)buttonTitles
                                     cancelButtonTitle:(nullable NSString *)cancelButtonTitle
                                destructiveButtonTitle:(nullable NSString *)destructiveButtonTitle
                                              delegate:(nullable id<LGAlertViewDelegate>)delegate;


+(LGAlertView *)getHomeEditorOptionAlertWithOption:(NSString *)optionJson;
@end

NS_ASSUME_NONNULL_END
