//
//  UIColor+XC.m
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "UIColor+XC.h"

@implementation UIColor (XC)
+(UIColor *)XCGreen{
    return [UIColor colorWithRed:0.00 green:0.67 blue:0.62 alpha:1.00];
}

+(UIColor *)XCYellow{
    return [UIColor colorWithRed:1.00 green:0.94 blue:0.21 alpha:1.00];
}

+(UIColor *)XCWhite{
    return [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.00];
}

+(UIColor *)XCGreen2{
    return [UIColor colorWithRed:0.36 green:0.67 blue:0.62 alpha:1.00];
}

+ (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    
    CGFloat alpha, red, blue, green;
    
    // #RGB
    alpha = 1.0f;
    red   = [[self class] colorComponentFrom: colorString start: 0 length: 2];
    green = [[self class] colorComponentFrom: colorString start: 2 length: 2];
    blue  = [[self class] colorComponentFrom: colorString start: 4 length: 2];
    
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

@end
