//
//  UIAlertController+XC.m
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "UIAlertController+XC.h"

@implementation UIAlertController (XC)

+ (UIAlertController *)developAlert{
    __block UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"알림"
                                         message:@"개발 중 입니다."
                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"확인"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    [alert addAction:cancelAction];
    return alert;
}


+ (UIAlertController *)confirmAlertWithMessage:(NSString *)message{
    return [self confirmAlertWithTitle:@"알림" message:message];
}

+ (UIAlertController *)confirmAlertWithTitle:(NSString *)title message:(NSString *)message{
    return [self confirmAlertWithTitle:@"알림" message:message confirmButton:@"확인"];
}

+ (UIAlertController *)confirmAlertWithTitle:(NSString *)title message:(NSString *)message confirmButton:(NSString *)confirmText{
    __block UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:title
                                         message:message
                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:confirmText
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    [alert addAction:cancelAction];
    return alert;
}

@end
