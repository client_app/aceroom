//
//  CRToast+Ace.m
//  Unity-iPhone
//
//  Created by 김태원 on 11/09/2019.
//

#import "CRToast+Ace.h"

@implementation CRToast (Ace)

+(void)showToastMessage:(NSString *)message{
    NSDictionary *options = @{
                              kCRToastTextKey : message,
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColor redColor],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionLeft),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionRight)
                              };
    
    [CRToast showToastMessage:message withOption:options];
    
}
+(void)showToastMessage:(NSString *)message withOption:(NSDictionary *)options{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:options];
    [dic setObject:message forKey:kCRToastTextKey];
    
    [CRToastManager showNotificationWithOptions:dic
                                completionBlock:^{
                                    NSLog(@"Completed");
                                }];
}

@end
