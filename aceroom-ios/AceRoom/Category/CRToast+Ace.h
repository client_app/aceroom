//
//  CRToast+Ace.h
//  Unity-iPhone
//
//  Created by 김태원 on 11/09/2019.
//

#import <CRToast/CRToast.h>

NS_ASSUME_NONNULL_BEGIN

@interface CRToast (Ace)

+(void)showToastMessage:(NSString *)message;
+(void)showToastMessage:(NSString *)message withOption:(NSDictionary *)options;
@end

NS_ASSUME_NONNULL_END
