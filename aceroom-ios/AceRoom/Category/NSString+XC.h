//
//  NSString+XC.h
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (XC)
- (NSString *)trim;
- (BOOL)validateEmptyText;
- (BOOL)validateEmail;
-(BOOL)validateTelNo;
@end

NS_ASSUME_NONNULL_END
