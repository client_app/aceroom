//
//  NSArray+XC.m
//  eexams
//
//  Created by 김태원 on 11/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "NSArray+XC.h"

@implementation NSArray (XC)

- (NSMutableArray*)mutableArray {
    return [NSMutableArray arrayWithArray:self];
}

@end
