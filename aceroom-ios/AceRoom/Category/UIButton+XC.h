//
//  UIButton+XC.h
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (XC)

/**
 버튼 글자 색상 설정

 @param disalbeColor 비활성화일때 텍스트 색상
 @param enableColor 활정화일때 텍스트 색상
 */
- (void)setButtonTitleDisableColor:(UIColor *)disalbeColor EnableColor:(UIColor *)enableColor;

/**
 버튼을 선택했을때 글자 색상 설정

 @param selectedColor 선택됀 상태
 @param deselectedColor 선택되지 않은 상태
 */
- (void)setButtonTitleSelectedColor:(UIColor *)selectedColor DeselectedColor:(UIColor *)deselectedColor;

/**
 버튼 글자를 설정한다

 @param title 버튼명
 */
- (void)setButtonTitleSetting:(NSString *)title;

/**
 버튼을 선택했을때 글자 설정

 @param title 평상시 표시될 글자
 @param selectTitle 버튼 선택시 표시할 글자
 */
- (void)setButtonTitleSettingDefault:(NSString *)title select:(NSString *)selectTitle;

/**
 버튼 이미지 설정

 @param imageName 버튼이지미명(Assets)
 */
- (void)setButtonImageSetting:(NSString *)imageName;

/**
 버튼 선택했을때 이미지를 설정

 @param defaulImageName 평상시 이미지
 @param selectImageName 선택시 이미지
 */
- (void)setButtonImageSettingDefault:(NSString *)defaulImageName select:(NSString *)selectImageName;

/**
 버튼 배경이미지 설정

 @param image 버튼 배경이미지
 */
- (void)setButtonBackgroundImageSettingImage:(UIImage *)image;

/**
 버튼의 글자를 NSMutableAttributedString로 설정

 @param title NSMutableAttributedString 버튼 글자
 */
- (void)setButtonAttributedTitleSetting:(NSMutableAttributedString *)title;

/**
 버튼 선택했을때 배경이미지 설정

 @param image 배경이미지
 @param selectImage 선택 시 배경이미지
 */
- (void)setButtonBackgroundImageSettingDefaultImage:(UIImage *)image select:(UIImage *)selectImage;

/**
 버튼 상태에 따라 다르게 표시되는 이미지(Asset 이미지명)

 @param defaulImageName 평상시 이미지
 @param selectImageName 선택시 이미지
 @param disableImageName 비활성화 시 이미지
 */
- (void)setButtonImageSettingDefault:(NSString *)defaulImageName select:(NSString *)selectImageName disableImageName:(NSString *)disableImageName;
@end

NS_ASSUME_NONNULL_END
