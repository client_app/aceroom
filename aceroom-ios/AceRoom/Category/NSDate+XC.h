//
//  NSDate+XC.h
//  eexams
//
//  Created by 김태원 on 07/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (XC)

-(NSDate *)dateTimeToZero;

+ (NSString *)getTodayYYYYMMDD;
+ (NSString *)dateFormatYYYYMMDD:(NSDate *)date decimal:(NSString *)decima;

- (NSString *)getYYYYMMDDWithDecimal:(NSString *)decimal;
- (double)dateToServerDate;
- (NSString *)dateToServerString;
@end

NS_ASSUME_NONNULL_END
