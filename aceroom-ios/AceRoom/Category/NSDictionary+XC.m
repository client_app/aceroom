//
//  NSDictionary+XC.m
//  eexams
//
//  Created by 김태원 on 07/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "NSDictionary+XC.h"

@implementation NSDictionary (XC)
- (NSMutableDictionary*)mutableDictionary {
    return [NSMutableDictionary dictionaryWithDictionary:self];
}

-(NSDate *)dateForKey:(NSString *)key{
    double date = [[self objectForKey:key] doubleValue];
    date = date / 1000; //밀리세컨드 단위까지는 오지 않음.
    NSDate *online = [NSDate date];
    online = [NSDate dateWithTimeIntervalSince1970:date];
    
    return online;
}
@end
