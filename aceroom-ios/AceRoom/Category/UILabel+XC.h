//
//  UILabel+XC.h
//  Unity-iPhone
//
//  Created by 김태원 on 08/01/2020.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (XC)

-(CGFloat)actualScaleFactor;
-(void)setFontSize:(CGFloat) fontSize;

@end

NS_ASSUME_NONNULL_END
