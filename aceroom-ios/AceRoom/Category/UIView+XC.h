//
//  UIView+XC.h
//  HPoint
//
//  Created by Jay Lee on 2017. 2. 7..
//  Copyright © 2017년 HPoint. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 @brief 기본 UIView의 확장 클래스
 */
@interface UIView (XC)

/**
 해당 뷰를 캡쳐한다

 @param frame 캡쳐할 크기
 @return 캡쳐이미지
 */
- (nonnull UIImage*)captureWithFrame:(CGRect)frame;

/**
 테두리가 둥글고 테두리에 선이 있는 뷰를 만든다.

 @param radius 테두리의 반지름
 @param width 테두리의 너비
 @param color 테두리 색상
 */
- (void)drawRoundedBorderRadius:(CGFloat)radius borderWidth:(NSInteger)width color:(nonnull UIColor *)color;

/**
 테두리가 그려져 있는 뷰를 만든다.

 @param width 테두리의 너비
 @param color 테두리의 색상
 */
- (void)drawBoderWidth:(NSInteger)width color:(nonnull UIColor *)color;

/**
 테두리를 둥글게 뷰를 만든다.

 @param radius 반지름
 */
- (void)drawRoundRadius:(CGFloat)radius;


/**
 XIB에 만들어진 UIView를 가져온다

 @param xibName XIB 파일명
 @param tag 가져올 View의 tag
 @return UIView의 하위 모든 View
 */
+(UIView *_Nullable)getCustomViewFromXib: (NSString *_Nullable)xibName WithTag:(NSInteger) tag;


/**
 XIB에 만들어진 UIView를 가져온다

 @param xibName XIB 파일명
 @param identifier 가져올 View의 Identifier
 @return UIView의 하위 모든 View
 */
+(nonnull UIView *)getCustomViewFromXib:(nonnull NSString *)xibName WithIdentifier:(nonnull NSString *) identifier;

/**
 자식 뷰중 해당하는 Identifier를 지닌 View를 가져온다.
 바로 하위의 View만 가져올 수 있으며(1단계), 2단계 자식뷰는 가져올 수 없다.

 @param identifier 가져올 자식뷰의 Identifier
 @return 자식뷰
 */
-(nonnull UIView *)viewWithIdentifier:(nonnull NSString *) identifier;


/**
 개발용 안내 토스트메세지를 띄운다.
 내용 앞에 "준비중입니다." 라는 메세지가 붙여진다.

 @param message 보여줄 메세지
 */
- (void)developToast:(nonnull NSString *)message;
@end
