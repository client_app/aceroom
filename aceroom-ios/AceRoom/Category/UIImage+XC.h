//
//  UIImage+XC.h
//  Unity-iPhone
//
//  Created by 김태원 on 10/10/2019.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (XC)
/**
 지정된 색상을 이미지로 만든다.
 단일 색상이므로 늘이거느 줄여도 변화는 없다.
 사이즈는 1*1이다.

 @param color 색상
 @return UIImage
 */
+ (UIImage *)imageWithColor:(UIColor *)color;

/**
 지정된 색상을 이미지로 만든다.

 @param color 색상
 @param rect 이미지 사이즈
 @return UIImage
 */
+ (UIImage *)imageWithColor:(UIColor *)color rect:(CGRect)rect;

/**
 이미지를 원형으로 만들고 지정된 크기로 리사이즈

 @param size 사이즈
 @return 원형으로 자른 이미지
 */
-(UIImage *)getRoundedImage:(CGSize)size;
@end

NS_ASSUME_NONNULL_END
