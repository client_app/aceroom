//
//  NSDictionary+XC.h
//  eexams
//
//  Created by 김태원 on 07/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (XC)
/**
 NSDictionary를 NSMutableDictionary로 변환한다.
 
 @return NSMutableDictionary
 */
- (NSMutableDictionary*)mutableDictionary;
-(NSDate *)dateForKey:(NSString *)key;
@end

NS_ASSUME_NONNULL_END
