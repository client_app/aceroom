//
//  UILabel+XC.m
//  Unity-iPhone
//
//  Created by 김태원 on 08/01/2020.
//

#import "UILabel+XC.h"

@implementation UILabel (XC)
-(CGFloat)actualScaleFactor{
	NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
	context.minimumScaleFactor = self.minimumScaleFactor;
	NSDictionary *attributes = @{NSFontAttributeName : self.font};
	NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:self.text attributes:attributes];
	[attributedString boundingRectWithSize:self.bounds.size
										   options:NSStringDrawingUsesLineFragmentOrigin
										   context:context];
	return context.actualScaleFactor;
}

-(void)setFontSize:(CGFloat) fontSize{
	
	NSDictionary *attributes = @{NSFontAttributeName : [UIFont fontWithName:self.font.fontName size:fontSize]};
	NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:self.text attributes:attributes];
	self.attributedText = attributedString;
}


@end
