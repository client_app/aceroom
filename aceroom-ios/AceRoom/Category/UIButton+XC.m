//
//  UIButton+XC.m
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import "UIButton+XC.h"

@implementation UIButton (XC)

- (void)setButtonTitleDisableColor:(UIColor *)disalbeColor EnableColor:(UIColor *)enableColor{
    [self setTitleColor:disalbeColor forState:UIControlStateDisabled];
    [self setTitleColor:enableColor forState:UIControlStateNormal];
    [self setTitleColor:enableColor forState:UIControlStateSelected];
    [self setTitleColor:enableColor forState:UIControlStateHighlighted];
}

- (void)setButtonTitleSelectedColor:(UIColor *)selectedColor DeselectedColor:(UIColor *)deselectedColor{
    [self setTitleColor:deselectedColor forState:UIControlStateDisabled];
    [self setTitleColor:deselectedColor forState:UIControlStateNormal];
    [self setTitleColor:selectedColor forState:UIControlStateSelected];
    [self setTitleColor:deselectedColor forState:UIControlStateHighlighted];
}

- (void)setButtonTitleSetting:(NSString *)title{
    self.titleLabel.text = title;
    [self setTitle:title forState:UIControlStateDisabled];
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitle:title forState:UIControlStateSelected];
    [self setTitle:title forState:UIControlStateHighlighted];
}

- (void)setButtonTitleSettingDefault:(NSString *)title select:(NSString *)selectTitle{
    [self setTitle:title forState:UIControlStateDisabled];
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitle:selectTitle forState:UIControlStateSelected];
    [self setTitle:title forState:UIControlStateHighlighted];
}

- (void)setButtonImageSetting:(NSString *)imageName{
    [self setImage:[UIImage imageNamed:imageName] forState:UIControlStateDisabled];
    [self setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:imageName] forState:UIControlStateSelected];
    [self setImage:[UIImage imageNamed:imageName] forState:UIControlStateHighlighted];
}

- (void)setButtonImageSettingDefault:(NSString *)defaulImageName select:(NSString *)selectImageName{
    [self setImage:[UIImage imageNamed:defaulImageName] forState:UIControlStateDisabled];
    [self setImage:[UIImage imageNamed:defaulImageName] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:selectImageName] forState:UIControlStateSelected];
    [self setImage:[UIImage imageNamed:defaulImageName] forState:UIControlStateHighlighted];
}

- (void)setButtonImageSettingDefault:(NSString *)defaulImageName select:(NSString *)selectImageName disableImageName:(NSString *)disableImageName{
    [self setImage:[UIImage imageNamed:disableImageName] forState:UIControlStateDisabled];
    [self setImage:[UIImage imageNamed:defaulImageName] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:selectImageName] forState:UIControlStateSelected];
    [self setImage:[UIImage imageNamed:defaulImageName] forState:UIControlStateHighlighted];
}

- (void)setButtonBackgroundImageSettingImage:(UIImage *)image{
    [self setBackgroundImage:image forState:UIControlStateDisabled];
    [self setBackgroundImage:image forState:UIControlStateNormal];
    [self setBackgroundImage:image forState:UIControlStateSelected];
    [self setBackgroundImage:image forState:UIControlStateHighlighted];
    [self setBackgroundImage:image forState:UIControlStateFocused];
}


- (void)setButtonBackgroundImageSettingDefaultImage:(UIImage *)image select:(UIImage *)selectImage{
    [self setBackgroundImage:image forState:UIControlStateDisabled];
    [self setBackgroundImage:image forState:UIControlStateNormal];
    [self setBackgroundImage:selectImage forState:UIControlStateSelected];
    [self setBackgroundImage:image forState:UIControlStateHighlighted];
}

- (void)setButtonAttributedTitleSetting:(NSMutableAttributedString *)title{
    [self setAttributedTitle:title forState:UIControlStateDisabled];
    [self setAttributedTitle:title forState:UIControlStateNormal];
    [self setAttributedTitle:title forState:UIControlStateSelected];
    [self setAttributedTitle:title forState:UIControlStateHighlighted];
}

@end
