//
//  UIAlertController+XC.h
//  eexams
//
//  Created by 김태원 on 13/11/2018.
//  Copyright © 2018 김태원. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIAlertController (XC)

+ (UIAlertController *)developAlert;


+ (UIAlertController *)confirmAlertWithMessage:(NSString *)message;
+ (UIAlertController *)confirmAlertWithTitle:(NSString *)title message:(NSString *)message;
+ (UIAlertController *)confirmAlertWithTitle:(NSString *)title message:(NSString *)message confirmButton:(NSString *)confirmText;
@end

NS_ASSUME_NONNULL_END
