//
//  UnityBridge.m
//  Unity-iPhone
//
//  Created by 김태원 on 09/09/2019.
//

#import "UnityBridge.h"
#import <Foundation/Foundation.h>
#import "../UI/Test/TestVC.h"
#import "MainViewController.h"
#import "PlanMapVC.h"
#import "HomeEditorOptionAlertView.h"
#import "HomeEditorProductVC.h"
#import <LGAlertView/LGAlertView.h>

@implementation UnityBridge
#pragma mark - Singleton
+ (instancetype)sharedInstance {
    static UnityBridge *shared = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[UnityBridge alloc] init];
		[shared appInitialize];
    });
    
    return shared;
}

-(void)appInitialize
{
//	MainViewController *mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
	UIViewController *rootVC = UnityGetGLViewController();
	UIWindow *window = [UIApplication sharedApplication].delegate.window;
	
	window.rootViewController = nil;
	
	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:rootVC];
	nav.navigationBarHidden = YES;
	window.rootViewController = nav;
	
	[self setRootViewController:nav];
	
//	[nav pushViewController:mainViewController animated:YES];
}

#pragma mark - Plugin Util
-(void)CallMainMenu
{

	HomeEditorProductVC *vc = [[HomeEditorProductVC alloc] initWithNibName:@"HomeEditorProductVC" bundle:nil];
//	MainViewController *vc = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
	UINavigationController *nav = (UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController;
	[nav pushViewController:vc animated:NO];
	
}

/**
 유니티 홈에디터 이후에 다시 맵으로 뜨는 경우.
 */
-(void)CallMapVC
{
//    PlanMapVC *vc = [[PlanMapVC alloc] initWithNibName:@"PlanMapVC" bundle:nil];
//    UIViewController *rootVC = UnityGetGLViewController();
//    [rootVC presentViewController:vc animated:YES completion:nil];
	
	
	MainViewController *mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
	PlanMapVC *planMapVC = [[PlanMapVC alloc] initWithNibName:@"PlanMapVC" bundle:nil];
	
	
	UINavigationController *nav = (UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController;
	NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray: nav.viewControllers];
	
	[allViewControllers addObject:mainViewController];
	nav.viewControllers = allViewControllers;
	
	[nav pushViewController:planMapVC animated:YES];
	
}

-(void)OpenHomeEditorOption:(NSString *)optionString
{
    //{"effect":2,"collision":true}
    
    NSLog(@"OpenHomeEditorOption : %@", optionString);
    NSLog(@"OpenHomeEditorOption : TEST : {\"effect\":2,\"collision\":true}");
//    LGAlertView *alert = [LGAlertView getHomeEditorOptionAlertWithOption:@"{\"effect\":2,\"collision\":true}"];
    LGAlertView *alert = [LGAlertView getHomeEditorOptionAlertWithOption:optionString];
    alert.dismissOnAction = NO;
    [alert showAnimated:YES completionHandler:^{
        
    }];
}

-(void)OpenHomeEditorProduct
{
	NSLog(@"왜???");
	
	HomeEditorProductVC *vc = [[HomeEditorProductVC alloc] initWithNibName:@"HomeEditorProductVC" bundle:nil];
	UINavigationController *nav = (UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController;
	[nav pushViewController:vc animated:NO];
}

-(void)shareScreenShot:(NSString *)imgPath
{
    NSLog(@"공유 호출:%@", imgPath);
    
    NSString *imgFilePath = [@"file://" stringByAppendingString:imgPath];
    NSData *imgData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imgFilePath]];
    UIImage *screenshot = [[UIImage alloc] initWithData:imgData];
    
    NSArray *items = @[@"나의 에이스는 어떤가요?", screenshot];
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    // and present it
    UIViewController *rootVC = UnityGetGLViewController();
    [rootVC presentViewController:controller animated:YES completion:^{
        // executes after the user selects something
        [self removeImage:imgFilePath];
    }];
}

-(void)saveScreenShot:(NSString *)imgPath
{
    NSLog(@"스크린샷 호출:%@", imgPath);
    
    NSString *imgFilePath = [@"file://" stringByAppendingString:imgPath];

    NSData *imgData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imgFilePath]];
    UIImage *screenshot = [[UIImage alloc] initWithData:imgData];

    UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil);

    [LGAlertView defaultAlertTitle:@"알림" message:@"스크린샷이 저장되었습니다."];
    
    [self removeImage:imgFilePath];
}

- (void)removeImage:(NSString *)filePath
{
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"File Deleted : %@", filePath);
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    
    //App Document 내 모든 파일 제거
    [self removeAllDocumentScreenshotFile];
    
    [self printAllDocumentFiles];
}

-(void)removeAllDocumentScreenshotFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSLog(@"file path : %@", paths);
        for (NSString *path in paths) {
            NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
            for (int i = 0; i < directoryContent.count; i++) {
                NSString *fileName = [directoryContent objectAtIndex:i];
                NSLog(@"root %@ File %d: %@", path, i, fileName);
                
                if([fileName hasPrefix:@"SS_"] && ([fileName hasSuffix:@"png"] || [fileName hasSuffix:@"PNG"]))
                {
                    NSString *fileFullPath = [NSString stringWithFormat:@"%@/%@", path, fileName];
                    NSError *error;
                    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:fileFullPath error:&error];
                    if (success) {
                        NSLog(@"File Deleted : %@", fileName);
                    }
                    else
                    {
                        NSLog(@"2Could not delete file -:%@ ",[error localizedDescription]);
                    }
                }else{
                    NSLog(@"file not match");
                }
                
                
            }
        }
}

-(void)removeAllDocumentFiles
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSLog(@"file path : %@", paths);
        for (NSString *path in paths) {
            NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
            for (int i = 0; i < directoryContent.count; i++) {
                NSString *fileName = [directoryContent objectAtIndex:i];
                NSLog(@"root %@ File %d: %@", path, i, fileName);
                NSString *fileFullPath = [NSString stringWithFormat:@"%@/%@", path, fileName];
                NSError *error;
                BOOL success = [[NSFileManager defaultManager] removeItemAtPath:fileFullPath error:&error];
                if (success) {
                    NSLog(@"File Deleted : %@", fileName);
                }
                else
                {
                    NSLog(@"2Could not delete file -:%@ ",[error localizedDescription]);
                }
            }
        }
}

-(void)printAllDocumentFiles
{
    NSLog(@"===========================");
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSLog(@"file path : %@", paths);
        for (NSString *path in paths) {
    
            NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
            for (int i = 0; i < directoryContent.count; i++) {
                NSLog(@"root %@ File %d: %@", path, i, [directoryContent objectAtIndex:i]);
            }
        }
}
@end
