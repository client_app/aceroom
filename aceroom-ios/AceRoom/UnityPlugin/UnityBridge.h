//
//  UnityBridge.h
//  Unity-iPhone
//
//  Created by 김태원 on 09/09/2019.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/*!
	유니티 플러그인과 네이티브의 통신을 위한 플러그인
        Unity에서 호출 - UnityuPlugin에서 UnityBridge를 호출하며, UnityBridge는각 화면을 호출
        Native에서 호출 -각 화면에서 UnityBridge를 호출 후 UnityPlugin을 호출하여 전달
 */
@interface UnityBridge : NSObject
{
	UIWindow *thisWindow;
	XCCommonNVC *thisNavi;
	UIView *loadingView;
}
+ (instancetype)sharedInstance;

/*!
   지도 구성용 기본데이터. ctp_cor_nm.json 파일을 불러온다.
    자세한 사용법은 어반베이스에 문의
*/
@property(nonatomic, strong) NSMutableArray *CTP_NM;
/*!
   지도 구성용 기본데이터. sig_kor_nm.json 파일을 불러온다.
    자세한 사용법은 어반베이스에 문의
*/
@property(nonatomic, strong) NSMutableArray *SIG_NM;
@property(nonatomic) id rootViewController;


/*!
   메인 메뉴 호출
*/
-(void)CallMainMenu;
/*!
   홈에디터 - 지도화면 호출
*/
-(void)CallMapVC;
/*!
   홈에디터 - 유니티 설정용 팝업 호출
*/
-(void)OpenHomeEditorOption:(NSString *)optionString;
/*!
   홈에디터 - 제품 선택용 카탈로그 화면 호출
*/
-(void)OpenHomeEditorProduct;
/*!
   홈에디터 - 스크린샷 공유
*/
-(void)shareScreenShot:(NSString *)imgPath;
/*!
   홈에디터 -스크린샷 저장
*/
-(void)saveScreenShot:(NSString *)imgPath;
/*!
   튜토리얼 화면 호출
*/
-(void)OpenTutorial:(NSString *)sceneName;
/*!
   튜토리얼 화면 호출 + 튜토리얼 화면이 닫힐 때 콜백
*/
-(void)OpenTutorial:(NSString *)sceneName closeHandler:(nullable XCVcCloseHandler)handler;
/*!
   화면 회전 변경
*/
-(void)setOrientation:(UIInterfaceOrientationMask)orientation;
/*!
   토스트 메세지를 화면에 출력 - 테스트용
*/
-(void)showToast:(NSString *)message;

/*!
   AR 침대배치 - 화면 전환
*/
-(void)ArViewOpen;
/*!
   [사용안함]AR 침대배치 - 화면 종료
*/
-(void)ArViewClose;

/*!
   Adbrix Custom Action 용 메소드
*/
-(void)adbrixCustomAction:(NSString *)action;
/*!
   Adbrix Custom Action 용 메소드 + 커스텀 파라미터
*/
-(void)adbrixCustomAction:(NSString *)action withAttrData:(NSDictionary *)attrData;
@end

NS_ASSUME_NONNULL_END
