//
//  UnityBridge.h
//  Unity-iPhone
//
//  Created by 김태원 on 09/09/2019.
//

#import <Foundation/Foundation.h>
#import "HomeEditorProductVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface UnityBridge : NSObject
+ (instancetype)sharedInstance;

@property(nonatomic, strong) NSMutableArray *CTP_NM;
@property(nonatomic, strong) NSMutableArray *SIG_NM;
@property(nonatomic) UIViewController* rootViewController;

-(void)CallMainMenu;
-(void)CallMapVC;

-(void)OpenHomeEditorOption:(NSString *)optionString;
-(void)OpenHomeEditorProduct;

-(void)shareScreenShot:(NSString *)imgPath;
-(void)saveScreenShot:(NSString *)imgPath;
@end

NS_ASSUME_NONNULL_END
