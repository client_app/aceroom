//
//  UnityBridge.m
//  Unity-iPhone
//
//  Created by 김태원 on 09/09/2019.
//

#import "UnityBridge.h"
#import <Foundation/Foundation.h>
#import "../UI/Test/TestVC.h"
#import "MainViewController.h"
#import "PlanMapVC.h"
#import "HomeEditorOptionAlertView.h"
#import "HomeEditorProductVC.h"
#import "TutorialVC.h"
#import "ARLensVC.h"
#import <LGAlertView/LGAlertView.h>

@interface UnityBridge()

//어반베이스 요청으로 앱델리게이트 파트로 추가.
@property (strong, nonatomic) ARViewer *arView;

@end

@implementation UnityBridge


#pragma mark - Singleton
+ (instancetype)sharedInstance {
    static UnityBridge *shared = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[UnityBridge alloc] init];
		[shared appInitialize];
		[shared hiddenLoadingView:YES];
    });
    
    return shared;
}

-(void)appInitialize
{
//	MainViewController *mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
	UIViewController *rootVC = UnityGetGLViewController();
	thisWindow = [UIApplication sharedApplication].delegate.window;
	
	thisWindow.rootViewController = nil;
	
	if(thisNavi==nil)
		thisNavi = [[XCCommonNVC alloc] initWithRootViewController:rootVC];
	thisNavi.navigationBarHidden = YES;
	
	thisWindow.rootViewController = thisNavi;
	
	[self setRootViewController:thisNavi];
	
	if(loadingView ==nil){
		UINib* nib = [UINib nibWithNibName:@"LaunchScreen-iPhone" bundle:nil];
		NSArray *customViewList = [nib instantiateWithOwner:self options:nil];
		
		loadingView = (UIView *)[customViewList firstObject];
		[loadingView setFrame:thisWindow.bounds];
		[thisWindow addSubview:loadingView];
	}
	
}

-(void)checkRootVC
{
	UIViewController *rootVC = UnityGetGLViewController();
	UIWindow *window = [UIApplication sharedApplication].delegate.window;
	id chck = window.rootViewController;
	NSLog(@"rootVC : %@", rootVC);
	NSLog(@"window : %@", window);
	NSLog(@"chck : %@", chck);
	NSLog(@"thisNavi : %@", thisNavi);
	NSLog(@"thisWindow : %@", thisWindow);
	
	if(![chck isKindOfClass:[XCCommonNVC class]]){
		[self appInitialize];
	}
}

#pragma mark - Plugin Util
-(void)hiddenLoadingView:(BOOL)hidden
{
	if(hidden == NO){
		[self->loadingView setHidden:hidden];
	}else{
		[UIView animateWithDuration:2.f animations:^{
			[self->loadingView setHidden:hidden];
		}];
	}
}

-(void)CallMainMenu
{
    [self checkRootVC];
    [kAcePlugin unitySetIsNotch:[CommonUtil hasTopNotch]];
    
	//홈에디터에서 지도화면으로 돌아오는 경우 기존 위치로 이동하기 위한 소스코드 - 메인화면 이동 시 기존 위치를 삭제
	[[NSUserDefaults standardUserDefaults] removeObjectForKey:LAST_MAP_POSITION];
    
//	HomeEditorProductVC *vc = [[HomeEditorProductVC alloc] initWithNibName:@"HomeEditorProductVC" bundle:nil];
	MainViewController *vc = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
	[thisNavi pushViewController:vc animated:NO];
		
}

#pragma mark - 홈에디터 관련
/**
 유니티 홈에디터 이후에 다시 맵으로 뜨는 경우.
 */
-(void)CallMapVC
{
	[self hiddenLoadingView:NO];
    [self checkRootVC];
    
	MainViewController *mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
	PlanMapVC *planMapVC = [[PlanMapVC alloc] initWithNibName:@"PlanMapVC" bundle:nil];
	
	NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray: thisNavi.viewControllers];
	
	[allViewControllers addObject:mainViewController];
	thisNavi.viewControllers = allViewControllers;
	
	[thisNavi pushViewController:planMapVC animated:YES completion:^{
		[self hiddenLoadingView:YES];
	}];
	
	[thisWindow bringSubviewToFront:loadingView];
	
}

//홈에디터 설정 창 오픈.
-(void)OpenHomeEditorOption:(NSString *)optionString
{
    //{"effect":2,"collision":true}
    
    NSLog(@"OpenHomeEditorOption : %@", optionString);
    NSLog(@"OpenHomeEditorOption : TEST : {\"effect\":2,\"collision\":true}");
//    LGAlertView *alert = [LGAlertView getHomeEditorOptionAlertWithOption:@"{\"effect\":2,\"collision\":true}"];
    LGAlertView *alert = [LGAlertView getHomeEditorOptionAlertWithOption:optionString];
    alert.dismissOnAction = NO;
    [alert showAnimated:YES completionHandler:^{
        
    }];
}

//홈에디터 제품 카탈로그 오픈.
-(void)OpenHomeEditorProduct
{
//	[kUnityBridge adbrixCustomAction:@"AR1_see_item_btn"];
	
	[self checkRootVC];
	
	HomeEditorProductVC *vc = [[HomeEditorProductVC alloc] initWithNibName:@"HomeEditorProductVC" bundle:nil];
	[thisNavi pushViewController:vc animated:NO];
}

#pragma mark AR 렌즈 관련
//AR Lens
-(ARViewer *)arView
{
	if(_arView==nil){
		self.arView = [[ARViewer alloc] initWithFrame:CGRectZero];
		UnityAppController *appDelegate = GetAppController();
		[appDelegate.window addSubview:_arView];
	}
	
	return _arView;
}

-(void)ArViewOpen
{
	ARLensVC *vc = [[ARLensVC alloc] initWithNibName:@"ARLensVC" bundle:nil];
	vc.arView = self.arView;
	[thisNavi pushViewController:vc animated:YES];
}

-(void)ArViewClose
{
	
	
}

-(void)shareScreenShot:(NSString *)imgPath
{
    NSLog(@"공유 호출:%@", imgPath);
    
    NSString *imgFilePath = [@"file://" stringByAppendingString:imgPath];
    NSData *imgData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imgFilePath]];
    UIImage *screenshot = [[UIImage alloc] initWithData:imgData];
    
    NSArray *items = @[@"나의 에이스는 어떤가요?", screenshot];
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    // and present it
    UIViewController *rootVC = UnityGetGLViewController();
    [rootVC presentViewController:controller animated:YES completion:^{
        // executes after the user selects something
        [self removeImage:imgFilePath];
    }];
}

-(void)saveScreenShot:(NSString *)imgPath
{
    NSLog(@"스크린샷 호출:%@", imgPath);
    
    NSString *imgFilePath = [@"file://" stringByAppendingString:imgPath];

    NSData *imgData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imgFilePath]];
    UIImage *screenshot = [[UIImage alloc] initWithData:imgData];

    UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil);

    [LGAlertView defaultAlertTitle:@"알림" message:@"스크린샷이 저장되었습니다."];
    
    [self removeImage:imgFilePath];
}

- (void)removeImage:(NSString *)filePath
{
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"File Deleted : %@", filePath);
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    
    //App Document 내 모든 파일 제거
    [self removeAllDocumentScreenshotFile];
    
    [self printAllDocumentFiles];
}

-(void)removeAllDocumentScreenshotFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSLog(@"file path : %@", paths);
        for (NSString *path in paths) {
            NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
            for (int i = 0; i < directoryContent.count; i++) {
                NSString *fileName = [directoryContent objectAtIndex:i];
                NSLog(@"root %@ File %d: %@", path, i, fileName);
                
                if([fileName hasPrefix:@"SS_"] && ([fileName hasSuffix:@"png"] || [fileName hasSuffix:@"PNG"]))
                {
                    NSString *fileFullPath = [NSString stringWithFormat:@"%@/%@", path, fileName];
                    NSError *error;
                    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:fileFullPath error:&error];
                    if (success) {
                        NSLog(@"File Deleted : %@", fileName);
                    }
                    else
                    {
                        NSLog(@"2Could not delete file -:%@ ",[error localizedDescription]);
                    }
                }else{
                    NSLog(@"file not match");
                }
                
                
            }
        }
}

-(void)removeAllDocumentFiles
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSLog(@"file path : %@", paths);
        for (NSString *path in paths) {
            NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
            for (int i = 0; i < directoryContent.count; i++) {
                NSString *fileName = [directoryContent objectAtIndex:i];
                NSLog(@"root %@ File %d: %@", path, i, fileName);
                NSString *fileFullPath = [NSString stringWithFormat:@"%@/%@", path, fileName];
                NSError *error;
                BOOL success = [[NSFileManager defaultManager] removeItemAtPath:fileFullPath error:&error];
                if (success) {
                    NSLog(@"File Deleted : %@", fileName);
                }
                else
                {
                    NSLog(@"2Could not delete file -:%@ ",[error localizedDescription]);
                }
            }
        }
}

-(void)printAllDocumentFiles
{
    NSLog(@"===========================");
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSLog(@"file path : %@", paths);
        for (NSString *path in paths) {
    
            NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
            for (int i = 0; i < directoryContent.count; i++) {
                NSLog(@"root %@ File %d: %@", path, i, [directoryContent objectAtIndex:i]);
            }
        }
}

-(void)OpenTutorial:(NSString *)sceneName closeHandler:(nullable XCVcCloseHandler)handler
{
//    UINavigationController *nav = (UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController;
    
//    NSLog(@"OpenTutorial - safeAreaLayoutGuide Width : %f", nav.topViewController.view.safeAreaLayoutGuide.layoutFrame.size.width);
    
    TutorialVC *vc;
    if([SCENE_AR_MATTRESS isEqualToString:sceneName])
    {
        vc = [[TutorialVC alloc] initWithNibName:@"TutorialVC" bundle:nil];
//        vc.pageViewWidth = nav.topViewController.view.safeAreaLayoutGuide.layoutFrame.size.width;
        [vc setTutorialType:TUTORIAL_TYPE_AR_VUFORIA];
    }
    else if([SCENE_HOME_EDITOR isEqualToString:sceneName])
    {
        vc = [[TutorialVC alloc] initWithNibName:@"TutorialVC_horizontal" bundle:nil];
        if([CommonUtil isNortchPhone]){
          vc.pageViewWidth = thisNavi.topViewController.view.safeAreaLayoutGuide.layoutFrame.size.height;
        }else{
            vc.pageViewWidth = thisNavi.topViewController.view.frame.size.height;
        }
        
        [vc setTutorialType:TUTORIAL_TYPE_HOME_EDITOR];
    }
    else if([NAME_AR_LENS isEqualToString:sceneName])
    {
        vc = [[TutorialVC alloc] initWithNibName:@"TutorialVC" bundle:nil];
//        vc.pageViewWidth = nav.topViewController.view.safeAreaLayoutGuide.layoutFrame.size.width;
        [vc setTutorialType:TUTORIAL_TYPE_AR_LENS];
    }
	
    if(handler != nil){
        [vc setCloseHandler:handler];
    }
    
//    [thisNavi presentViewController:vc animated:YES completion:^{}];
	[thisNavi pushViewController:vc animated:YES];
}

-(void)OpenTutorial:(NSString *)sceneName
{
    [self OpenTutorial:sceneName closeHandler:nil];
}

-(void)setOrientation:(UIInterfaceOrientationMask)orientation
{
    [thisNavi setMUInterfaceOrientationMask:orientation];
}

-(void)showToast:(NSString *)message
{
    
//    ToastMessage *view = (ToastMessage *)[UIView getCustomViewFromXib:@"ToastMessage" WithTag:0];
//    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0);
//    [view setContent:message];
//
//    [view bk_whenTapped:^{
//        [view hideView];
//    }];
//
//
//    [[UIApplication sharedApplication].delegate.window addSubview:view];
//
//    [view showAnimated:YES];
    
    [[UIApplication sharedApplication].delegate.window makeToast:message];
    
//    [[UnityAppController getInstance] showToast:message];
}


/*!
   Adbrix Custom Action 용 메소드
*/
-(void)adbrixCustomAction:(NSString *)action {
	[self adbrixCustomAction:action withAttrData:nil];
}
/*!
   Adbrix Custom Action 용 메소드
*/
-(void)adbrixCustomAction:(NSString *)action withAttrData:(NSDictionary *)attrData {
	AdBrixRM *adBrix = [AdBrixRM sharedInstance];
	NSLog(@"%@", [NSString stringWithFormat:@"[%@] ::: start]", action]);
	AdBrixRmAttrModel * attrModel = [AdBrixRmAttrModel alloc];
	if(attrData != nil || attrData.count > 0){
		[attrData enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
			[attrModel setAttrDataString:key :obj];
			NSLog(@"%@", [NSString stringWithFormat:@"[%@] ::: key[%@] value[%@]", action, key, obj]);
		}];
	}
	
	[adBrix eventWithAttrWithEventName:action value:attrModel];
}

@end
