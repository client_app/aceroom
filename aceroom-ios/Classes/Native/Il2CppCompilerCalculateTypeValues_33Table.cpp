﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_tE2F19E30ECDF39669C80D0E7DA21064D10C1EE2F;
// System.Action`1<Vuforia.TrackableBehaviour/Status>
struct Action_1_tB2D157B76595F25A0403EA2A0C32F01CE30B4F98;
// System.Action`2<Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/StatusInfo>
struct Action_2_t936B0D5682B789A67828A9CF1951CB8FF29DEF13;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera/StereoscopicEye,UnityEngine.Vector2>
struct Dictionary_2_tA50FC73B50AEAE38268B60A47025C7B19AB363C7;
// System.Collections.Generic.ICollection`1<Vuforia.AnchorBehaviour>
struct ICollection_1_tE10B01F648B08FB3B2EBE3AF876C1CF3F1B841BC;
// System.Collections.Generic.List`1<Vuforia.AValidatableVideoBackgroundConfigProperty>
struct List_1_tBCEF61DE12E8CFFEDECE9D4B935725C756A78F77;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82;
// System.Func`2<Vuforia.ImageTargetBehaviour,System.Boolean>
struct Func_2_tF5361435C33503A15254888BBE249E44BD973485;
// System.Func`2<Vuforia.TrackableBehaviour,System.Boolean>
struct Func_2_tB1EDC94D5DE2903BAA30A4C2DF00098781945AA6;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Vuforia.Anchor
struct Anchor_tBEF556F294A0BF27AA05F1B2A611BAE1DE5CF335;
// Vuforia.AnchorBehaviour
struct AnchorBehaviour_t44D245039CC4D9728F7594297E43EEE0885CB23E;
// Vuforia.AnchorInputListenerBehaviour/InputReceivedEvent
struct InputReceivedEvent_t30AD5C108997667B53F1C6C844EF0B80096BD49C;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338;
// Vuforia.ContentPlacedEvent
struct ContentPlacedEvent_t6F76CC86D8FF827E64E67C1DB8D770FB7A085997;
// Vuforia.ContentPositioningBehaviour
struct ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B;
// Vuforia.DataSet
struct DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644;
// Vuforia.DigitalEyewearARController/SerializableViewerParameters
struct SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9;
// Vuforia.GuideView
struct GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956;
// Vuforia.GuideViewContainer
struct GuideViewContainer_t763A0014C14D0068439C96239D87117C86633E0D;
// Vuforia.GuideViewRenderingBehaviour
struct GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62;
// Vuforia.HideExcessAreaUtility
struct HideExcessAreaUtility_t4DEC8A8570DC0458B45E1B21A6ABB435D6C63B32;
// Vuforia.HitTestEvent
struct HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F;
// Vuforia.HitTestResult
struct HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE;
// Vuforia.IBoundingBox
struct IBoundingBox_tD4F0CD39AB5435E3B1CE66D47080551C8D3469D3;
// Vuforia.IProjectMatrixProvider
struct IProjectMatrixProvider_tA598C4CFDC0F0AE0F5FFAC9FEF949B238C470172;
// Vuforia.ITargetSize
struct ITargetSize_t93FE85335C3D5B63DDB1C5EE2CE85A928F405898;
// Vuforia.InstanceIdImpl
struct InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371;
// Vuforia.InternalModelTarget
struct InternalModelTarget_t423462994B1F6AB6566A2E38C4B21FB5FAFCEA47;
// Vuforia.MidAirPositionerBehaviour/AnchorPositionConfirmedEvent
struct AnchorPositionConfirmedEvent_t63B8BA26EFA42FC76914E18C5AF9E3E9223B5093;
// Vuforia.ModelRecoBehaviour
struct ModelRecoBehaviour_t136409F3AECD7AEB24CC39E5CA3A50A4CAAF2291;
// Vuforia.ModelTargetBehaviour
struct ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0;
// Vuforia.PositionalDeviceTracker
struct PositionalDeviceTracker_tEFAA5FDB9CF19DA74081AE51B41B4B5D9F40D8EE;
// Vuforia.SmartTerrain
struct SmartTerrain_t09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47;
// Vuforia.StereoProjMatrixStore
struct StereoProjMatrixStore_t666C738477A3F36E8FDD7E872AC154108291348A;
// Vuforia.Trackable
struct Trackable_t2A23C572321E7D4FEAC9A1019DFA0AA144FC9B8F;
// Vuforia.VRDeviceController
struct VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044;
// Vuforia.VideoBackgroundBehaviour
struct VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00;
// Vuforia.VideoBackgroundConfigValidator
struct VideoBackgroundConfigValidator_tC6D2432439D8AAAF1E72C8124FC620504DC11EB0;
// Vuforia.VideoBackgroundDefaultProvider
struct VideoBackgroundDefaultProvider_t0B2CEB4665A61EAF6C799309C48637D82A26FE4A;
// Vuforia.VuMarkTemplate
struct VuMarkTemplate_t77A35C669991A2CC63075E8FCF018DA013A799DD;
// Vuforia.VuMarkTemplateImpl
struct VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8;
// Vuforia.VuforiaARController
struct VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2;
// Vuforia.VuforiaConfiguration/DeviceTrackerConfiguration
struct DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F;
// Vuforia.VuforiaConfiguration/VideoBackgroundConfiguration
struct VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#define ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293  : public RuntimeObject
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293, ___mVuforiaBehaviour_0)); }
	inline VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#ifndef AVALIDATABLEVIDEOBACKGROUNDCONFIGPROPERTY_T7C670179380AA43CB486E420F6BAB3B08B01F6B4_H
#define AVALIDATABLEVIDEOBACKGROUNDCONFIGPROPERTY_T7C670179380AA43CB486E420F6BAB3B08B01F6B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.AValidatableVideoBackgroundConfigProperty
struct  AValidatableVideoBackgroundConfigProperty_t7C670179380AA43CB486E420F6BAB3B08B01F6B4  : public RuntimeObject
{
public:
	// Vuforia.VuforiaConfiguration_VideoBackgroundConfiguration Vuforia.AValidatableVideoBackgroundConfigProperty::Config
	VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17 * ___Config_0;
	// Vuforia.VideoBackgroundDefaultProvider Vuforia.AValidatableVideoBackgroundConfigProperty::DefaultProvider
	VideoBackgroundDefaultProvider_t0B2CEB4665A61EAF6C799309C48637D82A26FE4A * ___DefaultProvider_1;

public:
	inline static int32_t get_offset_of_Config_0() { return static_cast<int32_t>(offsetof(AValidatableVideoBackgroundConfigProperty_t7C670179380AA43CB486E420F6BAB3B08B01F6B4, ___Config_0)); }
	inline VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17 * get_Config_0() const { return ___Config_0; }
	inline VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17 ** get_address_of_Config_0() { return &___Config_0; }
	inline void set_Config_0(VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17 * value)
	{
		___Config_0 = value;
		Il2CppCodeGenWriteBarrier((&___Config_0), value);
	}

	inline static int32_t get_offset_of_DefaultProvider_1() { return static_cast<int32_t>(offsetof(AValidatableVideoBackgroundConfigProperty_t7C670179380AA43CB486E420F6BAB3B08B01F6B4, ___DefaultProvider_1)); }
	inline VideoBackgroundDefaultProvider_t0B2CEB4665A61EAF6C799309C48637D82A26FE4A * get_DefaultProvider_1() const { return ___DefaultProvider_1; }
	inline VideoBackgroundDefaultProvider_t0B2CEB4665A61EAF6C799309C48637D82A26FE4A ** get_address_of_DefaultProvider_1() { return &___DefaultProvider_1; }
	inline void set_DefaultProvider_1(VideoBackgroundDefaultProvider_t0B2CEB4665A61EAF6C799309C48637D82A26FE4A * value)
	{
		___DefaultProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultProvider_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVALIDATABLEVIDEOBACKGROUNDCONFIGPROPERTY_T7C670179380AA43CB486E420F6BAB3B08B01F6B4_H
#ifndef ANCHORIMPL_T187011BD4C3999DD142AA584FB40EDB48A4794DC_H
#define ANCHORIMPL_T187011BD4C3999DD142AA584FB40EDB48A4794DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.AnchorImpl
struct  AnchorImpl_t187011BD4C3999DD142AA584FB40EDB48A4794DC  : public RuntimeObject
{
public:
	// System.String Vuforia.AnchorImpl::mName
	String_t* ___mName_0;
	// System.Int32 Vuforia.AnchorImpl::mId
	int32_t ___mId_1;

public:
	inline static int32_t get_offset_of_mName_0() { return static_cast<int32_t>(offsetof(AnchorImpl_t187011BD4C3999DD142AA584FB40EDB48A4794DC, ___mName_0)); }
	inline String_t* get_mName_0() const { return ___mName_0; }
	inline String_t** get_address_of_mName_0() { return &___mName_0; }
	inline void set_mName_0(String_t* value)
	{
		___mName_0 = value;
		Il2CppCodeGenWriteBarrier((&___mName_0), value);
	}

	inline static int32_t get_offset_of_mId_1() { return static_cast<int32_t>(offsetof(AnchorImpl_t187011BD4C3999DD142AA584FB40EDB48A4794DC, ___mId_1)); }
	inline int32_t get_mId_1() const { return ___mId_1; }
	inline int32_t* get_address_of_mId_1() { return &___mId_1; }
	inline void set_mId_1(int32_t value)
	{
		___mId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORIMPL_T187011BD4C3999DD142AA584FB40EDB48A4794DC_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_TBA0EADD82169074AB68C3D86F7B4C4D898AEB74B_H
#define U3CU3EC__DISPLAYCLASS10_0_TBA0EADD82169074AB68C3D86F7B4C4D898AEB74B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ContentPositioningBehaviour_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_tBA0EADD82169074AB68C3D86F7B4C4D898AEB74B  : public RuntimeObject
{
public:
	// Vuforia.ContentPositioningBehaviour Vuforia.ContentPositioningBehaviour_<>c__DisplayClass10_0::<>4__this
	ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B * ___U3CU3E4__this_0;
	// Vuforia.HitTestResult Vuforia.ContentPositioningBehaviour_<>c__DisplayClass10_0::hitTestResult
	HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE * ___hitTestResult_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_tBA0EADD82169074AB68C3D86F7B4C4D898AEB74B, ___U3CU3E4__this_0)); }
	inline ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_hitTestResult_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_tBA0EADD82169074AB68C3D86F7B4C4D898AEB74B, ___hitTestResult_1)); }
	inline HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE * get_hitTestResult_1() const { return ___hitTestResult_1; }
	inline HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE ** get_address_of_hitTestResult_1() { return &___hitTestResult_1; }
	inline void set_hitTestResult_1(HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE * value)
	{
		___hitTestResult_1 = value;
		Il2CppCodeGenWriteBarrier((&___hitTestResult_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_TBA0EADD82169074AB68C3D86F7B4C4D898AEB74B_H
#ifndef U3CU3EC__DISPLAYCLASS14_0_TF0169B1436EE9D9EDC1FFE5C6D70D8B55F3CFBCF_H
#define U3CU3EC__DISPLAYCLASS14_0_TF0169B1436EE9D9EDC1FFE5C6D70D8B55F3CFBCF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ContentPositioningBehaviour_<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_tF0169B1436EE9D9EDC1FFE5C6D70D8B55F3CFBCF  : public RuntimeObject
{
public:
	// Vuforia.Anchor Vuforia.ContentPositioningBehaviour_<>c__DisplayClass14_0::anchor
	RuntimeObject* ___anchor_0;

public:
	inline static int32_t get_offset_of_anchor_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_tF0169B1436EE9D9EDC1FFE5C6D70D8B55F3CFBCF, ___anchor_0)); }
	inline RuntimeObject* get_anchor_0() const { return ___anchor_0; }
	inline RuntimeObject** get_address_of_anchor_0() { return &___anchor_0; }
	inline void set_anchor_0(RuntimeObject* value)
	{
		___anchor_0 = value;
		Il2CppCodeGenWriteBarrier((&___anchor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS14_0_TF0169B1436EE9D9EDC1FFE5C6D70D8B55F3CFBCF_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T1277B477D9D8C49B826E61FAE04E9D10516278C4_H
#define U3CU3EC__DISPLAYCLASS9_0_T1277B477D9D8C49B826E61FAE04E9D10516278C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ContentPositioningBehaviour_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t1277B477D9D8C49B826E61FAE04E9D10516278C4  : public RuntimeObject
{
public:
	// Vuforia.ContentPositioningBehaviour Vuforia.ContentPositioningBehaviour_<>c__DisplayClass9_0::<>4__this
	ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B * ___U3CU3E4__this_0;
	// UnityEngine.Transform Vuforia.ContentPositioningBehaviour_<>c__DisplayClass9_0::pose
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___pose_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t1277B477D9D8C49B826E61FAE04E9D10516278C4, ___U3CU3E4__this_0)); }
	inline ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_pose_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t1277B477D9D8C49B826E61FAE04E9D10516278C4, ___pose_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_pose_1() const { return ___pose_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_pose_1() { return &___pose_1; }
	inline void set_pose_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___pose_1 = value;
		Il2CppCodeGenWriteBarrier((&___pose_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T1277B477D9D8C49B826E61FAE04E9D10516278C4_H
#ifndef DELEGATEHELPER_T027CD1F95062C9C98103DBB523743A9BD59F91DF_H
#define DELEGATEHELPER_T027CD1F95062C9C98103DBB523743A9BD59F91DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DelegateHelper
struct  DelegateHelper_t027CD1F95062C9C98103DBB523743A9BD59F91DF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEHELPER_T027CD1F95062C9C98103DBB523743A9BD59F91DF_H
#ifndef DEVICE_T19A8D91DE8CD359592F4BFF8F0F025C06AE1B175_H
#define DEVICE_T19A8D91DE8CD359592F4BFF8F0F025C06AE1B175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Device
struct  Device_t19A8D91DE8CD359592F4BFF8F0F025C06AE1B175  : public RuntimeObject
{
public:

public:
};

struct Device_t19A8D91DE8CD359592F4BFF8F0F025C06AE1B175_StaticFields
{
public:
	// Vuforia.Device Vuforia.Device::mInstance
	Device_t19A8D91DE8CD359592F4BFF8F0F025C06AE1B175 * ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(Device_t19A8D91DE8CD359592F4BFF8F0F025C06AE1B175_StaticFields, ___mInstance_0)); }
	inline Device_t19A8D91DE8CD359592F4BFF8F0F025C06AE1B175 * get_mInstance_0() const { return ___mInstance_0; }
	inline Device_t19A8D91DE8CD359592F4BFF8F0F025C06AE1B175 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(Device_t19A8D91DE8CD359592F4BFF8F0F025C06AE1B175 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICE_T19A8D91DE8CD359592F4BFF8F0F025C06AE1B175_H
#ifndef U3CSETCHILDOFVUFORIAANCHORU3ED__24_T7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49_H
#define U3CSETCHILDOFVUFORIAANCHORU3ED__24_T7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.GuideViewRenderingBehaviour_<SetChildOfVuforiaAnchor>d__24
struct  U3CSetChildOfVuforiaAnchorU3Ed__24_t7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49  : public RuntimeObject
{
public:
	// System.Int32 Vuforia.GuideViewRenderingBehaviour_<SetChildOfVuforiaAnchor>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Vuforia.GuideViewRenderingBehaviour_<SetChildOfVuforiaAnchor>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Vuforia.GuideViewRenderingBehaviour Vuforia.GuideViewRenderingBehaviour_<SetChildOfVuforiaAnchor>d__24::<>4__this
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSetChildOfVuforiaAnchorU3Ed__24_t7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSetChildOfVuforiaAnchorU3Ed__24_t7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSetChildOfVuforiaAnchorU3Ed__24_t7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49, ___U3CU3E4__this_2)); }
	inline GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETCHILDOFVUFORIAANCHORU3ED__24_T7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49_H
#ifndef U3CSHOWGUIDEVIEWAFTERU3ED__23_T984B10E61E2B87AC3915A224894BB695D03C8095_H
#define U3CSHOWGUIDEVIEWAFTERU3ED__23_T984B10E61E2B87AC3915A224894BB695D03C8095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.GuideViewRenderingBehaviour_<ShowGuideViewAfter>d__23
struct  U3CShowGuideViewAfterU3Ed__23_t984B10E61E2B87AC3915A224894BB695D03C8095  : public RuntimeObject
{
public:
	// System.Int32 Vuforia.GuideViewRenderingBehaviour_<ShowGuideViewAfter>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Vuforia.GuideViewRenderingBehaviour_<ShowGuideViewAfter>d__23::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Vuforia.GuideViewRenderingBehaviour_<ShowGuideViewAfter>d__23::seconds
	float ___seconds_2;
	// Vuforia.GuideViewRenderingBehaviour Vuforia.GuideViewRenderingBehaviour_<ShowGuideViewAfter>d__23::<>4__this
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowGuideViewAfterU3Ed__23_t984B10E61E2B87AC3915A224894BB695D03C8095, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShowGuideViewAfterU3Ed__23_t984B10E61E2B87AC3915A224894BB695D03C8095, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_seconds_2() { return static_cast<int32_t>(offsetof(U3CShowGuideViewAfterU3Ed__23_t984B10E61E2B87AC3915A224894BB695D03C8095, ___seconds_2)); }
	inline float get_seconds_2() const { return ___seconds_2; }
	inline float* get_address_of_seconds_2() { return &___seconds_2; }
	inline void set_seconds_2(float value)
	{
		___seconds_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CShowGuideViewAfterU3Ed__23_t984B10E61E2B87AC3915A224894BB695D03C8095, ___U3CU3E4__this_3)); }
	inline GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWGUIDEVIEWAFTERU3ED__23_T984B10E61E2B87AC3915A224894BB695D03C8095_H
#ifndef IENUMERABLEEXTENSIONMETHODS_T1C224FF0D4BE50ABEFC3BA491E7DE9595D314E74_H
#define IENUMERABLEEXTENSIONMETHODS_T1C224FF0D4BE50ABEFC3BA491E7DE9595D314E74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.IEnumerableExtensionMethods
struct  IEnumerableExtensionMethods_t1C224FF0D4BE50ABEFC3BA491E7DE9595D314E74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IENUMERABLEEXTENSIONMETHODS_T1C224FF0D4BE50ABEFC3BA491E7DE9595D314E74_H
#ifndef NULLHOLOLENSAPIABSTRACTION_TA2A1D3CFDA4B08ADA1A6C72DCE50CBE9C148DEAD_H
#define NULLHOLOLENSAPIABSTRACTION_TA2A1D3CFDA4B08ADA1A6C72DCE50CBE9C148DEAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullHoloLensApiAbstraction
struct  NullHoloLensApiAbstraction_tA2A1D3CFDA4B08ADA1A6C72DCE50CBE9C148DEAD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLHOLOLENSAPIABSTRACTION_TA2A1D3CFDA4B08ADA1A6C72DCE50CBE9C148DEAD_H
#ifndef PIXELFORMATEXTENSIONS_T55FFDEBBF53782357AAF85CCF49F8F3A1E0A5558_H
#define PIXELFORMATEXTENSIONS_T55FFDEBBF53782357AAF85CCF49F8F3A1E0A5558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PixelFormatExtensions
struct  PixelFormatExtensions_t55FFDEBBF53782357AAF85CCF49F8F3A1E0A5558  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXELFORMATEXTENSIONS_T55FFDEBBF53782357AAF85CCF49F8F3A1E0A5558_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_T409774273F602E27B578D7F5EB161A47FC3B0980_H
#define U3CU3EC__DISPLAYCLASS29_0_T409774273F602E27B578D7F5EB161A47FC3B0980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlaneFinderBehaviour_<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_t409774273F602E27B578D7F5EB161A47FC3B0980  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.PlaneFinderBehaviour_<>c__DisplayClass29_0::isVisible
	bool ___isVisible_0;

public:
	inline static int32_t get_offset_of_isVisible_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t409774273F602E27B578D7F5EB161A47FC3B0980, ___isVisible_0)); }
	inline bool get_isVisible_0() const { return ___isVisible_0; }
	inline bool* get_address_of_isVisible_0() { return &___isVisible_0; }
	inline void set_isVisible_0(bool value)
	{
		___isVisible_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_T409774273F602E27B578D7F5EB161A47FC3B0980_H
#ifndef U3CU3EC_T2255C26323B70789F1DEDBCAA38CD03AB66B600C_H
#define U3CU3EC_T2255C26323B70789F1DEDBCAA38CD03AB66B600C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlaymodeSmartTerrainImpl_<>c
struct  U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C_StaticFields
{
public:
	// Vuforia.PlaymodeSmartTerrainImpl_<>c Vuforia.PlaymodeSmartTerrainImpl_<>c::<>9
	U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C * ___U3CU3E9_0;
	// System.Func`2<Vuforia.TrackableBehaviour,System.Boolean> Vuforia.PlaymodeSmartTerrainImpl_<>c::<>9__7_0
	Func_2_tB1EDC94D5DE2903BAA30A4C2DF00098781945AA6 * ___U3CU3E9__7_0_1;
	// System.Func`2<Vuforia.ImageTargetBehaviour,System.Boolean> Vuforia.PlaymodeSmartTerrainImpl_<>c::<>9__7_1
	Func_2_tF5361435C33503A15254888BBE249E44BD973485 * ___U3CU3E9__7_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_2_tB1EDC94D5DE2903BAA30A4C2DF00098781945AA6 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_2_tB1EDC94D5DE2903BAA30A4C2DF00098781945AA6 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_2_tB1EDC94D5DE2903BAA30A4C2DF00098781945AA6 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C_StaticFields, ___U3CU3E9__7_1_2)); }
	inline Func_2_tF5361435C33503A15254888BBE249E44BD973485 * get_U3CU3E9__7_1_2() const { return ___U3CU3E9__7_1_2; }
	inline Func_2_tF5361435C33503A15254888BBE249E44BD973485 ** get_address_of_U3CU3E9__7_1_2() { return &___U3CU3E9__7_1_2; }
	inline void set_U3CU3E9__7_1_2(Func_2_tF5361435C33503A15254888BBE249E44BD973485 * value)
	{
		___U3CU3E9__7_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2255C26323B70789F1DEDBCAA38CD03AB66B600C_H
#ifndef TRACKABLEIMPL_T97E7D175DE38822AF55FC03FCA26FB62B7E290A2_H
#define TRACKABLEIMPL_T97E7D175DE38822AF55FC03FCA26FB62B7E290A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableImpl
struct  TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2  : public RuntimeObject
{
public:
	// System.String Vuforia.TrackableImpl::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 Vuforia.TrackableImpl::<ID>k__BackingField
	int32_t ___U3CIDU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2, ___U3CIDU3Ek__BackingField_1)); }
	inline int32_t get_U3CIDU3Ek__BackingField_1() const { return ___U3CIDU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIDU3Ek__BackingField_1() { return &___U3CIDU3Ek__BackingField_1; }
	inline void set_U3CIDU3Ek__BackingField_1(int32_t value)
	{
		___U3CIDU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIMPL_T97E7D175DE38822AF55FC03FCA26FB62B7E290A2_H
#ifndef TRACKER_T11C8E7B84615512E8125186CDC5DF90D9D7B58F1_H
#define TRACKER_T11C8E7B84615512E8125186CDC5DF90D9D7B58F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T11C8E7B84615512E8125186CDC5DF90D9D7B58F1_H
#ifndef TRACKERDATA_TE9DAA69F73C99DF652AAA629FFB9418FC62EBDE1_H
#define TRACKERDATA_TE9DAA69F73C99DF652AAA629FFB9418FC62EBDE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData
struct  TrackerData_tE9DAA69F73C99DF652AAA629FFB9418FC62EBDE1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERDATA_TE9DAA69F73C99DF652AAA629FFB9418FC62EBDE1_H
#ifndef UNITYCOMPONENTEXTENSIONS_T9F0045DBCEFA6E3D0D61EF91365CB0A9703DF288_H
#define UNITYCOMPONENTEXTENSIONS_T9F0045DBCEFA6E3D0D61EF91365CB0A9703DF288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityComponentExtensions
struct  UnityComponentExtensions_t9F0045DBCEFA6E3D0D61EF91365CB0A9703DF288  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCOMPONENTEXTENSIONS_T9F0045DBCEFA6E3D0D61EF91365CB0A9703DF288_H
#ifndef VIDEOBACKGROUNDCONFIGVALIDATOR_TC6D2432439D8AAAF1E72C8124FC620504DC11EB0_H
#define VIDEOBACKGROUNDCONFIGVALIDATOR_TC6D2432439D8AAAF1E72C8124FC620504DC11EB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundConfigValidator
struct  VideoBackgroundConfigValidator_tC6D2432439D8AAAF1E72C8124FC620504DC11EB0  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Vuforia.AValidatableVideoBackgroundConfigProperty> Vuforia.VideoBackgroundConfigValidator::mValidatableProperties
	List_1_tBCEF61DE12E8CFFEDECE9D4B935725C756A78F77 * ___mValidatableProperties_0;

public:
	inline static int32_t get_offset_of_mValidatableProperties_0() { return static_cast<int32_t>(offsetof(VideoBackgroundConfigValidator_tC6D2432439D8AAAF1E72C8124FC620504DC11EB0, ___mValidatableProperties_0)); }
	inline List_1_tBCEF61DE12E8CFFEDECE9D4B935725C756A78F77 * get_mValidatableProperties_0() const { return ___mValidatableProperties_0; }
	inline List_1_tBCEF61DE12E8CFFEDECE9D4B935725C756A78F77 ** get_address_of_mValidatableProperties_0() { return &___mValidatableProperties_0; }
	inline void set_mValidatableProperties_0(List_1_tBCEF61DE12E8CFFEDECE9D4B935725C756A78F77 * value)
	{
		___mValidatableProperties_0 = value;
		Il2CppCodeGenWriteBarrier((&___mValidatableProperties_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDCONFIGVALIDATOR_TC6D2432439D8AAAF1E72C8124FC620504DC11EB0_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TDA1C694A3B22700FA2A2B9A65EC6A49E864F2750_H
#define U3CU3EC__DISPLAYCLASS2_0_TDA1C694A3B22700FA2A2B9A65EC6A49E864F2750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundConfigValidator_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tDA1C694A3B22700FA2A2B9A65EC6A49E864F2750  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.VideoBackgroundConfigValidator_<>c__DisplayClass2_0::res
	bool ___res_0;

public:
	inline static int32_t get_offset_of_res_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tDA1C694A3B22700FA2A2B9A65EC6A49E864F2750, ___res_0)); }
	inline bool get_res_0() const { return ___res_0; }
	inline bool* get_address_of_res_0() { return &___res_0; }
	inline void set_res_0(bool value)
	{
		___res_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TDA1C694A3B22700FA2A2B9A65EC6A49E864F2750_H
#ifndef VUFORIAEXTENDEDTRACKINGMANAGER_TAACFA22C32AEA72AA454280FB0E854CE8FF6D383_H
#define VUFORIAEXTENDEDTRACKINGMANAGER_TAACFA22C32AEA72AA454280FB0E854CE8FF6D383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaExtendedTrackingManager
struct  VuforiaExtendedTrackingManager_tAACFA22C32AEA72AA454280FB0E854CE8FF6D383  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAEXTENDEDTRACKINGMANAGER_TAACFA22C32AEA72AA454280FB0E854CE8FF6D383_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef UNITYEVENT_1_T257C104996F98E724B7742D8D6AAA98EBEDC82AF_H
#define UNITYEVENT_1_T257C104996F98E724B7742D8D6AAA98EBEDC82AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>
struct  UnityEvent_1_t257C104996F98E724B7742D8D6AAA98EBEDC82AF  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t257C104996F98E724B7742D8D6AAA98EBEDC82AF, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T257C104996F98E724B7742D8D6AAA98EBEDC82AF_H
#ifndef UNITYEVENT_1_TD15FCA738F4102B872C1480A6678ED16A8A51845_H
#define UNITYEVENT_1_TD15FCA738F4102B872C1480A6678ED16A8A51845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Transform>
struct  UnityEvent_1_tD15FCA738F4102B872C1480A6678ED16A8A51845  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_tD15FCA738F4102B872C1480A6678ED16A8A51845, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TD15FCA738F4102B872C1480A6678ED16A8A51845_H
#ifndef UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#define UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#ifndef UNITYEVENT_1_T829D31AB9F9A4E0C21C8062405B1EF98DDD81ADE_H
#define UNITYEVENT_1_T829D31AB9F9A4E0C21C8062405B1EF98DDD81ADE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Vuforia.HitTestResult>
struct  UnityEvent_1_t829D31AB9F9A4E0C21C8062405B1EF98DDD81ADE  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t829D31AB9F9A4E0C21C8062405B1EF98DDD81ADE, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T829D31AB9F9A4E0C21C8062405B1EF98DDD81ADE_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef ANCHORDATA_TC476940A18BABB720CE1D5A2B78153B01DF407FF_H
#define ANCHORDATA_TC476940A18BABB720CE1D5A2B78153B01DF407FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.AnchorData
#pragma pack(push, tp, 1)
struct  AnchorData_tC476940A18BABB720CE1D5A2B78153B01DF407FF 
{
public:
	// System.Int32 Vuforia.AnchorData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.AnchorData::unused
	int32_t ___unused_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(AnchorData_tC476940A18BABB720CE1D5A2B78153B01DF407FF, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_unused_1() { return static_cast<int32_t>(offsetof(AnchorData_tC476940A18BABB720CE1D5A2B78153B01DF407FF, ___unused_1)); }
	inline int32_t get_unused_1() const { return ___unused_1; }
	inline int32_t* get_address_of_unused_1() { return &___unused_1; }
	inline void set_unused_1(int32_t value)
	{
		___unused_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORDATA_TC476940A18BABB720CE1D5A2B78153B01DF407FF_H
#ifndef DEVICETRACKER_TD6E28B77342C2CBE99C14112AFA51C0798EC3086_H
#define DEVICETRACKER_TD6E28B77342C2CBE99C14112AFA51C0798EC3086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTracker
struct  DeviceTracker_tD6E28B77342C2CBE99C14112AFA51C0798EC3086  : public Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKER_TD6E28B77342C2CBE99C14112AFA51C0798EC3086_H
#ifndef EYEWEARDEVICE_T083CA7719B929A57A532B386322C61658849AA1E_H
#define EYEWEARDEVICE_T083CA7719B929A57A532B386322C61658849AA1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearDevice
struct  EyewearDevice_t083CA7719B929A57A532B386322C61658849AA1E  : public Device_t19A8D91DE8CD359592F4BFF8F0F025C06AE1B175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARDEVICE_T083CA7719B929A57A532B386322C61658849AA1E_H
#ifndef MATTESHADERPROPERTY_T135A3774A258638E6E504EC3C593105CD140D2FD_H
#define MATTESHADERPROPERTY_T135A3774A258638E6E504EC3C593105CD140D2FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MatteShaderProperty
struct  MatteShaderProperty_t135A3774A258638E6E504EC3C593105CD140D2FD  : public AValidatableVideoBackgroundConfigProperty_t7C670179380AA43CB486E420F6BAB3B08B01F6B4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATTESHADERPROPERTY_T135A3774A258638E6E504EC3C593105CD140D2FD_H
#ifndef NUMDIVISIONSPROPERTY_T256C036FE34F79188C444C116986E14905657527_H
#define NUMDIVISIONSPROPERTY_T256C036FE34F79188C444C116986E14905657527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NumDivisionsProperty
struct  NumDivisionsProperty_t256C036FE34F79188C444C116986E14905657527  : public AValidatableVideoBackgroundConfigProperty_t7C670179380AA43CB486E420F6BAB3B08B01F6B4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMDIVISIONSPROPERTY_T256C036FE34F79188C444C116986E14905657527_H
#ifndef OBJECTTARGETIMPL_TEDDDACB8EF83100B0896957801DF1F85DB6A126A_H
#define OBJECTTARGETIMPL_TEDDDACB8EF83100B0896957801DF1F85DB6A126A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetImpl
struct  ObjectTargetImpl_tEDDDACB8EF83100B0896957801DF1F85DB6A126A  : public TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2
{
public:
	// Vuforia.ITargetSize Vuforia.ObjectTargetImpl::mTargetSizeImpl
	RuntimeObject* ___mTargetSizeImpl_2;

public:
	inline static int32_t get_offset_of_mTargetSizeImpl_2() { return static_cast<int32_t>(offsetof(ObjectTargetImpl_tEDDDACB8EF83100B0896957801DF1F85DB6A126A, ___mTargetSizeImpl_2)); }
	inline RuntimeObject* get_mTargetSizeImpl_2() const { return ___mTargetSizeImpl_2; }
	inline RuntimeObject** get_address_of_mTargetSizeImpl_2() { return &___mTargetSizeImpl_2; }
	inline void set_mTargetSizeImpl_2(RuntimeObject* value)
	{
		___mTargetSizeImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetSizeImpl_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETIMPL_TEDDDACB8EF83100B0896957801DF1F85DB6A126A_H
#ifndef SMARTTERRAIN_T09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47_H
#define SMARTTERRAIN_T09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrain
struct  SmartTerrain_t09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47  : public Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAIN_T09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47_H
#ifndef VIRTUALBUTTONDATA_TF16C663C156A49F65553E38299D185C298EFB1BF_H
#define VIRTUALBUTTONDATA_TF16C663C156A49F65553E38299D185C298EFB1BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_VirtualButtonData
#pragma pack(push, tp, 1)
struct  VirtualButtonData_tF16C663C156A49F65553E38299D185C298EFB1BF 
{
public:
	// System.Int32 Vuforia.TrackerData_VirtualButtonData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.TrackerData_VirtualButtonData::isPressed
	int32_t ___isPressed_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(VirtualButtonData_tF16C663C156A49F65553E38299D185C298EFB1BF, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_isPressed_1() { return static_cast<int32_t>(offsetof(VirtualButtonData_tF16C663C156A49F65553E38299D185C298EFB1BF, ___isPressed_1)); }
	inline int32_t get_isPressed_1() const { return ___isPressed_1; }
	inline int32_t* get_address_of_isPressed_1() { return &___isPressed_1; }
	inline void set_isPressed_1(int32_t value)
	{
		___isPressed_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONDATA_TF16C663C156A49F65553E38299D185C298EFB1BF_H
#ifndef VIDEOBACKGROUNDSHADERPROPERTY_T162378EC0A889704F1DA266B52C01CF58B875B49_H
#define VIDEOBACKGROUNDSHADERPROPERTY_T162378EC0A889704F1DA266B52C01CF58B875B49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundShaderProperty
struct  VideoBackgroundShaderProperty_t162378EC0A889704F1DA266B52C01CF58B875B49  : public AValidatableVideoBackgroundConfigProperty_t7C670179380AA43CB486E420F6BAB3B08B01F6B4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDSHADERPROPERTY_T162378EC0A889704F1DA266B52C01CF58B875B49_H
#ifndef DEPTHTEXTUREMODE_T284833A8AB245ACA7E27BE611BE03B18B0249F01_H
#define DEPTHTEXTUREMODE_T284833A8AB245ACA7E27BE611BE03B18B0249F01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DepthTextureMode
struct  DepthTextureMode_t284833A8AB245ACA7E27BE611BE03B18B0249F01 
{
public:
	// System.Int32 UnityEngine.DepthTextureMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DepthTextureMode_t284833A8AB245ACA7E27BE611BE03B18B0249F01, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHTEXTUREMODE_T284833A8AB245ACA7E27BE611BE03B18B0249F01_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#define SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenOrientation_t4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#ifndef INPUTRECEIVEDEVENT_T30AD5C108997667B53F1C6C844EF0B80096BD49C_H
#define INPUTRECEIVEDEVENT_T30AD5C108997667B53F1C6C844EF0B80096BD49C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.AnchorInputListenerBehaviour_InputReceivedEvent
struct  InputReceivedEvent_t30AD5C108997667B53F1C6C844EF0B80096BD49C  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTRECEIVEDEVENT_T30AD5C108997667B53F1C6C844EF0B80096BD49C_H
#ifndef PROJECTIONMATRIXDATA_T68F1EB028D7C3DDEE47E84B50FC93F6495CCD018_H
#define PROJECTIONMATRIXDATA_T68F1EB028D7C3DDEE47E84B50FC93F6495CCD018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BackgroundPlaneBehaviour_ProjectionMatrixData
struct  ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018 
{
public:
	// UnityEngine.Matrix4x4 Vuforia.BackgroundPlaneBehaviour_ProjectionMatrixData::ProjectionMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___ProjectionMatrix_0;
	// UnityEngine.Matrix4x4 Vuforia.BackgroundPlaneBehaviour_ProjectionMatrixData::InverseMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___InverseMatrix_1;
	// System.Single Vuforia.BackgroundPlaneBehaviour_ProjectionMatrixData::HorizontalFoV
	float ___HorizontalFoV_2;
	// System.Single Vuforia.BackgroundPlaneBehaviour_ProjectionMatrixData::VerticalFoV
	float ___VerticalFoV_3;
	// UnityEngine.Vector3 Vuforia.BackgroundPlaneBehaviour_ProjectionMatrixData::CenterEyeRayFrom
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CenterEyeRayFrom_4;
	// UnityEngine.Vector3 Vuforia.BackgroundPlaneBehaviour_ProjectionMatrixData::CenterEyeRayTo
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___CenterEyeRayTo_5;
	// System.Single Vuforia.BackgroundPlaneBehaviour_ProjectionMatrixData::MaxDepth
	float ___MaxDepth_6;

public:
	inline static int32_t get_offset_of_ProjectionMatrix_0() { return static_cast<int32_t>(offsetof(ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018, ___ProjectionMatrix_0)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_ProjectionMatrix_0() const { return ___ProjectionMatrix_0; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_ProjectionMatrix_0() { return &___ProjectionMatrix_0; }
	inline void set_ProjectionMatrix_0(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___ProjectionMatrix_0 = value;
	}

	inline static int32_t get_offset_of_InverseMatrix_1() { return static_cast<int32_t>(offsetof(ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018, ___InverseMatrix_1)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_InverseMatrix_1() const { return ___InverseMatrix_1; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_InverseMatrix_1() { return &___InverseMatrix_1; }
	inline void set_InverseMatrix_1(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___InverseMatrix_1 = value;
	}

	inline static int32_t get_offset_of_HorizontalFoV_2() { return static_cast<int32_t>(offsetof(ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018, ___HorizontalFoV_2)); }
	inline float get_HorizontalFoV_2() const { return ___HorizontalFoV_2; }
	inline float* get_address_of_HorizontalFoV_2() { return &___HorizontalFoV_2; }
	inline void set_HorizontalFoV_2(float value)
	{
		___HorizontalFoV_2 = value;
	}

	inline static int32_t get_offset_of_VerticalFoV_3() { return static_cast<int32_t>(offsetof(ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018, ___VerticalFoV_3)); }
	inline float get_VerticalFoV_3() const { return ___VerticalFoV_3; }
	inline float* get_address_of_VerticalFoV_3() { return &___VerticalFoV_3; }
	inline void set_VerticalFoV_3(float value)
	{
		___VerticalFoV_3 = value;
	}

	inline static int32_t get_offset_of_CenterEyeRayFrom_4() { return static_cast<int32_t>(offsetof(ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018, ___CenterEyeRayFrom_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CenterEyeRayFrom_4() const { return ___CenterEyeRayFrom_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CenterEyeRayFrom_4() { return &___CenterEyeRayFrom_4; }
	inline void set_CenterEyeRayFrom_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CenterEyeRayFrom_4 = value;
	}

	inline static int32_t get_offset_of_CenterEyeRayTo_5() { return static_cast<int32_t>(offsetof(ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018, ___CenterEyeRayTo_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_CenterEyeRayTo_5() const { return ___CenterEyeRayTo_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_CenterEyeRayTo_5() { return &___CenterEyeRayTo_5; }
	inline void set_CenterEyeRayTo_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___CenterEyeRayTo_5 = value;
	}

	inline static int32_t get_offset_of_MaxDepth_6() { return static_cast<int32_t>(offsetof(ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018, ___MaxDepth_6)); }
	inline float get_MaxDepth_6() const { return ___MaxDepth_6; }
	inline float* get_address_of_MaxDepth_6() { return &___MaxDepth_6; }
	inline void set_MaxDepth_6(float value)
	{
		___MaxDepth_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONMATRIXDATA_T68F1EB028D7C3DDEE47E84B50FC93F6495CCD018_H
#ifndef CAMERACONFIGURATIONUTILITY_TFDDDF578E2D5422A9997F43A0658E6AB085EAA44_H
#define CAMERACONFIGURATIONUTILITY_TFDDDF578E2D5422A9997F43A0658E6AB085EAA44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraConfigurationUtility
struct  CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44  : public RuntimeObject
{
public:

public:
};

struct CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields
{
public:
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MIN_CENTER
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___MIN_CENTER_0;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_CENTER
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___MAX_CENTER_1;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MIN_BOTTOM
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___MIN_BOTTOM_2;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MIN_TOP
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___MIN_TOP_3;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MIN_LEFT
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___MIN_LEFT_4;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MIN_RIGHT
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___MIN_RIGHT_5;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_BOTTOM
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___MAX_BOTTOM_6;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_TOP
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___MAX_TOP_7;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_LEFT
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___MAX_LEFT_8;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_RIGHT
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___MAX_RIGHT_9;

public:
	inline static int32_t get_offset_of_MIN_CENTER_0() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields, ___MIN_CENTER_0)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_MIN_CENTER_0() const { return ___MIN_CENTER_0; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_MIN_CENTER_0() { return &___MIN_CENTER_0; }
	inline void set_MIN_CENTER_0(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___MIN_CENTER_0 = value;
	}

	inline static int32_t get_offset_of_MAX_CENTER_1() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields, ___MAX_CENTER_1)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_MAX_CENTER_1() const { return ___MAX_CENTER_1; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_MAX_CENTER_1() { return &___MAX_CENTER_1; }
	inline void set_MAX_CENTER_1(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___MAX_CENTER_1 = value;
	}

	inline static int32_t get_offset_of_MIN_BOTTOM_2() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields, ___MIN_BOTTOM_2)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_MIN_BOTTOM_2() const { return ___MIN_BOTTOM_2; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_MIN_BOTTOM_2() { return &___MIN_BOTTOM_2; }
	inline void set_MIN_BOTTOM_2(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___MIN_BOTTOM_2 = value;
	}

	inline static int32_t get_offset_of_MIN_TOP_3() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields, ___MIN_TOP_3)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_MIN_TOP_3() const { return ___MIN_TOP_3; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_MIN_TOP_3() { return &___MIN_TOP_3; }
	inline void set_MIN_TOP_3(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___MIN_TOP_3 = value;
	}

	inline static int32_t get_offset_of_MIN_LEFT_4() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields, ___MIN_LEFT_4)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_MIN_LEFT_4() const { return ___MIN_LEFT_4; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_MIN_LEFT_4() { return &___MIN_LEFT_4; }
	inline void set_MIN_LEFT_4(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___MIN_LEFT_4 = value;
	}

	inline static int32_t get_offset_of_MIN_RIGHT_5() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields, ___MIN_RIGHT_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_MIN_RIGHT_5() const { return ___MIN_RIGHT_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_MIN_RIGHT_5() { return &___MIN_RIGHT_5; }
	inline void set_MIN_RIGHT_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___MIN_RIGHT_5 = value;
	}

	inline static int32_t get_offset_of_MAX_BOTTOM_6() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields, ___MAX_BOTTOM_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_MAX_BOTTOM_6() const { return ___MAX_BOTTOM_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_MAX_BOTTOM_6() { return &___MAX_BOTTOM_6; }
	inline void set_MAX_BOTTOM_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___MAX_BOTTOM_6 = value;
	}

	inline static int32_t get_offset_of_MAX_TOP_7() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields, ___MAX_TOP_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_MAX_TOP_7() const { return ___MAX_TOP_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_MAX_TOP_7() { return &___MAX_TOP_7; }
	inline void set_MAX_TOP_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___MAX_TOP_7 = value;
	}

	inline static int32_t get_offset_of_MAX_LEFT_8() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields, ___MAX_LEFT_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_MAX_LEFT_8() const { return ___MAX_LEFT_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_MAX_LEFT_8() { return &___MAX_LEFT_8; }
	inline void set_MAX_LEFT_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___MAX_LEFT_8 = value;
	}

	inline static int32_t get_offset_of_MAX_RIGHT_9() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields, ___MAX_RIGHT_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_MAX_RIGHT_9() const { return ___MAX_RIGHT_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_MAX_RIGHT_9() { return &___MAX_RIGHT_9; }
	inline void set_MAX_RIGHT_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___MAX_RIGHT_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONFIGURATIONUTILITY_TFDDDF578E2D5422A9997F43A0658E6AB085EAA44_H
#ifndef CAMERADEVICEMODE_T31CE15C1D60CED5FC63DF3962D53D5DAADD40589_H
#define CAMERADEVICEMODE_T31CE15C1D60CED5FC63DF3962D53D5DAADD40589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice_CameraDeviceMode
struct  CameraDeviceMode_t31CE15C1D60CED5FC63DF3962D53D5DAADD40589 
{
public:
	// System.Int32 Vuforia.CameraDevice_CameraDeviceMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t31CE15C1D60CED5FC63DF3962D53D5DAADD40589, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T31CE15C1D60CED5FC63DF3962D53D5DAADD40589_H
#ifndef CONTENTPLACEDEVENT_T6F76CC86D8FF827E64E67C1DB8D770FB7A085997_H
#define CONTENTPLACEDEVENT_T6F76CC86D8FF827E64E67C1DB8D770FB7A085997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ContentPlacedEvent
struct  ContentPlacedEvent_t6F76CC86D8FF827E64E67C1DB8D770FB7A085997  : public UnityEvent_1_t257C104996F98E724B7742D8D6AAA98EBEDC82AF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTPLACEDEVENT_T6F76CC86D8FF827E64E67C1DB8D770FB7A085997_H
#ifndef DATASETOBJECTTARGETIMPL_TC2D896B84A1FDE730F7E2F20045D12CBC6F30C98_H
#define DATASETOBJECTTARGETIMPL_TC2D896B84A1FDE730F7E2F20045D12CBC6F30C98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetObjectTargetImpl
struct  DataSetObjectTargetImpl_tC2D896B84A1FDE730F7E2F20045D12CBC6F30C98  : public ObjectTargetImpl_tEDDDACB8EF83100B0896957801DF1F85DB6A126A
{
public:
	// Vuforia.DataSet Vuforia.DataSetObjectTargetImpl::mDataSet
	DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * ___mDataSet_3;

public:
	inline static int32_t get_offset_of_mDataSet_3() { return static_cast<int32_t>(offsetof(DataSetObjectTargetImpl_tC2D896B84A1FDE730F7E2F20045D12CBC6F30C98, ___mDataSet_3)); }
	inline DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * get_mDataSet_3() const { return ___mDataSet_3; }
	inline DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 ** get_address_of_mDataSet_3() { return &___mDataSet_3; }
	inline void set_mDataSet_3(DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * value)
	{
		___mDataSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETOBJECTTARGETIMPL_TC2D896B84A1FDE730F7E2F20045D12CBC6F30C98_H
#ifndef DATASETTARGETSIZE_T8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517_H
#define DATASETTARGETSIZE_T8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTargetSize
struct  DataSetTargetSize_t8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Vuforia.DataSetTargetSize::mSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mSize_0;
	// System.String Vuforia.DataSetTargetSize::mName
	String_t* ___mName_1;
	// Vuforia.DataSet Vuforia.DataSetTargetSize::mDataSet
	DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * ___mDataSet_2;

public:
	inline static int32_t get_offset_of_mSize_0() { return static_cast<int32_t>(offsetof(DataSetTargetSize_t8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517, ___mSize_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mSize_0() const { return ___mSize_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mSize_0() { return &___mSize_0; }
	inline void set_mSize_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mSize_0 = value;
	}

	inline static int32_t get_offset_of_mName_1() { return static_cast<int32_t>(offsetof(DataSetTargetSize_t8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517, ___mName_1)); }
	inline String_t* get_mName_1() const { return ___mName_1; }
	inline String_t** get_address_of_mName_1() { return &___mName_1; }
	inline void set_mName_1(String_t* value)
	{
		___mName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mName_1), value);
	}

	inline static int32_t get_offset_of_mDataSet_2() { return static_cast<int32_t>(offsetof(DataSetTargetSize_t8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517, ___mDataSet_2)); }
	inline DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * get_mDataSet_2() const { return ___mDataSet_2; }
	inline DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 ** get_address_of_mDataSet_2() { return &___mDataSet_2; }
	inline void set_mDataSet_2(DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * value)
	{
		___mDataSet_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSet_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTARGETSIZE_T8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517_H
#ifndef DEDICATEDEYEWEARDEVICE_T636A2A6A914BA544F7C74C9E1C34565A0838B400_H
#define DEDICATEDEYEWEARDEVICE_T636A2A6A914BA544F7C74C9E1C34565A0838B400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DedicatedEyewearDevice
struct  DedicatedEyewearDevice_t636A2A6A914BA544F7C74C9E1C34565A0838B400  : public EyewearDevice_t083CA7719B929A57A532B386322C61658849AA1E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEDICATEDEYEWEARDEVICE_T636A2A6A914BA544F7C74C9E1C34565A0838B400_H
#ifndef TRACKING_MODE_T2ABEC66C8561190920E2F2637F28B37437D2E5E0_H
#define TRACKING_MODE_T2ABEC66C8561190920E2F2637F28B37437D2E5E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTracker_TRACKING_MODE
struct  TRACKING_MODE_t2ABEC66C8561190920E2F2637F28B37437D2E5E0 
{
public:
	// System.Int32 Vuforia.DeviceTracker_TRACKING_MODE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TRACKING_MODE_t2ABEC66C8561190920E2F2637F28B37437D2E5E0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKING_MODE_T2ABEC66C8561190920E2F2637F28B37437D2E5E0_H
#ifndef EYEWEARTYPE_T0DF90F97DCCF9F068455C2AF59B1758E89615C13_H
#define EYEWEARTYPE_T0DF90F97DCCF9F068455C2AF59B1758E89615C13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController_EyewearType
struct  EyewearType_t0DF90F97DCCF9F068455C2AF59B1758E89615C13 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController_EyewearType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EyewearType_t0DF90F97DCCF9F068455C2AF59B1758E89615C13, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARTYPE_T0DF90F97DCCF9F068455C2AF59B1758E89615C13_H
#ifndef SEETHROUGHCONFIGURATION_TF34F5E33DC51F5EEB23599A905DD112FF00C8E34_H
#define SEETHROUGHCONFIGURATION_TF34F5E33DC51F5EEB23599A905DD112FF00C8E34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController_SeeThroughConfiguration
struct  SeeThroughConfiguration_tF34F5E33DC51F5EEB23599A905DD112FF00C8E34 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController_SeeThroughConfiguration::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SeeThroughConfiguration_tF34F5E33DC51F5EEB23599A905DD112FF00C8E34, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEETHROUGHCONFIGURATION_TF34F5E33DC51F5EEB23599A905DD112FF00C8E34_H
#ifndef STEREOFRAMEWORK_T95A56F9A03F0EBAFFC34ABBE8309C936859E13BA_H
#define STEREOFRAMEWORK_T95A56F9A03F0EBAFFC34ABBE8309C936859E13BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController_StereoFramework
struct  StereoFramework_t95A56F9A03F0EBAFFC34ABBE8309C936859E13BA 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController_StereoFramework::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StereoFramework_t95A56F9A03F0EBAFFC34ABBE8309C936859E13BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOFRAMEWORK_T95A56F9A03F0EBAFFC34ABBE8309C936859E13BA_H
#ifndef DISABLEDSETTARGETSIZE_TED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA_H
#define DISABLEDSETTARGETSIZE_TED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DisabledSetTargetSize
struct  DisabledSetTargetSize_tED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Vuforia.DisabledSetTargetSize::mSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mSize_0;

public:
	inline static int32_t get_offset_of_mSize_0() { return static_cast<int32_t>(offsetof(DisabledSetTargetSize_tED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA, ___mSize_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mSize_0() const { return ___mSize_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mSize_0() { return &___mSize_0; }
	inline void set_mSize_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mSize_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEDSETTARGETSIZE_TED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA_H
#ifndef FUSIONPROVIDERTYPE_T0FD88A0D7F1544448DAEF1A9EAB5FBB591A495C2_H
#define FUSIONPROVIDERTYPE_T0FD88A0D7F1544448DAEF1A9EAB5FBB591A495C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.FusionProviderType
struct  FusionProviderType_t0FD88A0D7F1544448DAEF1A9EAB5FBB591A495C2 
{
public:
	// System.Int32 Vuforia.FusionProviderType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FusionProviderType_t0FD88A0D7F1544448DAEF1A9EAB5FBB591A495C2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUSIONPROVIDERTYPE_T0FD88A0D7F1544448DAEF1A9EAB5FBB591A495C2_H
#ifndef GUIDEVIEW_T0732A5D3A8536A322937D1E3FB0DF808ED666956_H
#define GUIDEVIEW_T0732A5D3A8536A322937D1E3FB0DF808ED666956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.GuideView
struct  GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.GuideView::mInstancePtr
	intptr_t ___mInstancePtr_0;
	// UnityEngine.Matrix4x4 Vuforia.GuideView::mPose
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___mPose_1;
	// System.String Vuforia.GuideView::mName
	String_t* ___mName_2;
	// System.Boolean Vuforia.GuideView::mCustomPose
	bool ___mCustomPose_3;
	// UnityEngine.Texture2D Vuforia.GuideView::mImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___mImage_4;
	// System.ComponentModel.PropertyChangedEventHandler Vuforia.GuideView::PropertyChanged
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ___PropertyChanged_5;

public:
	inline static int32_t get_offset_of_mInstancePtr_0() { return static_cast<int32_t>(offsetof(GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956, ___mInstancePtr_0)); }
	inline intptr_t get_mInstancePtr_0() const { return ___mInstancePtr_0; }
	inline intptr_t* get_address_of_mInstancePtr_0() { return &___mInstancePtr_0; }
	inline void set_mInstancePtr_0(intptr_t value)
	{
		___mInstancePtr_0 = value;
	}

	inline static int32_t get_offset_of_mPose_1() { return static_cast<int32_t>(offsetof(GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956, ___mPose_1)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_mPose_1() const { return ___mPose_1; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_mPose_1() { return &___mPose_1; }
	inline void set_mPose_1(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___mPose_1 = value;
	}

	inline static int32_t get_offset_of_mName_2() { return static_cast<int32_t>(offsetof(GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956, ___mName_2)); }
	inline String_t* get_mName_2() const { return ___mName_2; }
	inline String_t** get_address_of_mName_2() { return &___mName_2; }
	inline void set_mName_2(String_t* value)
	{
		___mName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mName_2), value);
	}

	inline static int32_t get_offset_of_mCustomPose_3() { return static_cast<int32_t>(offsetof(GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956, ___mCustomPose_3)); }
	inline bool get_mCustomPose_3() const { return ___mCustomPose_3; }
	inline bool* get_address_of_mCustomPose_3() { return &___mCustomPose_3; }
	inline void set_mCustomPose_3(bool value)
	{
		___mCustomPose_3 = value;
	}

	inline static int32_t get_offset_of_mImage_4() { return static_cast<int32_t>(offsetof(GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956, ___mImage_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_mImage_4() const { return ___mImage_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_mImage_4() { return &___mImage_4; }
	inline void set_mImage_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___mImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___mImage_4), value);
	}

	inline static int32_t get_offset_of_PropertyChanged_5() { return static_cast<int32_t>(offsetof(GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956, ___PropertyChanged_5)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get_PropertyChanged_5() const { return ___PropertyChanged_5; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of_PropertyChanged_5() { return &___PropertyChanged_5; }
	inline void set_PropertyChanged_5(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		___PropertyChanged_5 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanged_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDEVIEW_T0732A5D3A8536A322937D1E3FB0DF808ED666956_H
#ifndef HITTESTEVENT_T63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F_H
#define HITTESTEVENT_T63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HitTestEvent
struct  HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F  : public UnityEvent_1_t829D31AB9F9A4E0C21C8062405B1EF98DDD81ADE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITTESTEVENT_T63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F_H
#ifndef HITTESTMODE_TA27F6512F1F271CA16946F6B0D654F766469973B_H
#define HITTESTMODE_TA27F6512F1F271CA16946F6B0D654F766469973B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HitTestMode
struct  HitTestMode_tA27F6512F1F271CA16946F6B0D654F766469973B 
{
public:
	// System.Int32 Vuforia.HitTestMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HitTestMode_tA27F6512F1F271CA16946F6B0D654F766469973B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITTESTMODE_TA27F6512F1F271CA16946F6B0D654F766469973B_H
#ifndef HITTESTRESULT_T53FC02E2A830035AD9ED88C46D9975FD1B5C59DE_H
#define HITTESTRESULT_T53FC02E2A830035AD9ED88C46D9975FD1B5C59DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HitTestResult
struct  HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.HitTestResult::mPtr
	intptr_t ___mPtr_0;
	// UnityEngine.Vector3 Vuforia.HitTestResult::mPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mPosition_1;
	// UnityEngine.Quaternion Vuforia.HitTestResult::mOrientation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___mOrientation_2;

public:
	inline static int32_t get_offset_of_mPtr_0() { return static_cast<int32_t>(offsetof(HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE, ___mPtr_0)); }
	inline intptr_t get_mPtr_0() const { return ___mPtr_0; }
	inline intptr_t* get_address_of_mPtr_0() { return &___mPtr_0; }
	inline void set_mPtr_0(intptr_t value)
	{
		___mPtr_0 = value;
	}

	inline static int32_t get_offset_of_mPosition_1() { return static_cast<int32_t>(offsetof(HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE, ___mPosition_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mPosition_1() const { return ___mPosition_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mPosition_1() { return &___mPosition_1; }
	inline void set_mPosition_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mPosition_1 = value;
	}

	inline static int32_t get_offset_of_mOrientation_2() { return static_cast<int32_t>(offsetof(HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE, ___mOrientation_2)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_mOrientation_2() const { return ___mOrientation_2; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_mOrientation_2() { return &___mOrientation_2; }
	inline void set_mOrientation_2(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___mOrientation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITTESTRESULT_T53FC02E2A830035AD9ED88C46D9975FD1B5C59DE_H
#ifndef HITTESTRESULTDATA_T67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3_H
#define HITTESTRESULTDATA_T67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HitTestResultData
struct  HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3 
{
public:
	// System.IntPtr Vuforia.HitTestResultData::HitTestResultPtr
	intptr_t ___HitTestResultPtr_0;
	// System.Single[] Vuforia.HitTestResultData::Orientation
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___Orientation_1;
	// System.Single[] Vuforia.HitTestResultData::Position
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___Position_2;
	// System.Single Vuforia.HitTestResultData::Unused
	float ___Unused_3;

public:
	inline static int32_t get_offset_of_HitTestResultPtr_0() { return static_cast<int32_t>(offsetof(HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3, ___HitTestResultPtr_0)); }
	inline intptr_t get_HitTestResultPtr_0() const { return ___HitTestResultPtr_0; }
	inline intptr_t* get_address_of_HitTestResultPtr_0() { return &___HitTestResultPtr_0; }
	inline void set_HitTestResultPtr_0(intptr_t value)
	{
		___HitTestResultPtr_0 = value;
	}

	inline static int32_t get_offset_of_Orientation_1() { return static_cast<int32_t>(offsetof(HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3, ___Orientation_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_Orientation_1() const { return ___Orientation_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_Orientation_1() { return &___Orientation_1; }
	inline void set_Orientation_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___Orientation_1 = value;
		Il2CppCodeGenWriteBarrier((&___Orientation_1), value);
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3, ___Position_2)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_Position_2() const { return ___Position_2; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___Position_2 = value;
		Il2CppCodeGenWriteBarrier((&___Position_2), value);
	}

	inline static int32_t get_offset_of_Unused_3() { return static_cast<int32_t>(offsetof(HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3, ___Unused_3)); }
	inline float get_Unused_3() const { return ___Unused_3; }
	inline float* get_address_of_Unused_3() { return &___Unused_3; }
	inline void set_Unused_3(float value)
	{
		___Unused_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.HitTestResultData
#pragma pack(push, tp, 1)
struct HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3_marshaled_pinvoke
{
	intptr_t ___HitTestResultPtr_0;
	float ___Orientation_1[4];
	float ___Position_2[3];
	float ___Unused_3;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of Vuforia.HitTestResultData
#pragma pack(push, tp, 1)
struct HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3_marshaled_com
{
	intptr_t ___HitTestResultPtr_0;
	float ___Orientation_1[4];
	float ___Position_2[3];
	float ___Unused_3;
};
#pragma pack(pop, tp)
#endif // HITTESTRESULTDATA_T67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3_H
#ifndef INSTANCEIDTYPE_T2B8C4E1066D9FFD1C8BAA369E303860476BABF0D_H
#define INSTANCEIDTYPE_T2B8C4E1066D9FFD1C8BAA369E303860476BABF0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.InstanceIdType
struct  InstanceIdType_t2B8C4E1066D9FFD1C8BAA369E303860476BABF0D 
{
public:
	// System.Int32 Vuforia.InstanceIdType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InstanceIdType_t2B8C4E1066D9FFD1C8BAA369E303860476BABF0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDTYPE_T2B8C4E1066D9FFD1C8BAA369E303860476BABF0D_H
#ifndef ANCHORPOSITIONCONFIRMEDEVENT_T63B8BA26EFA42FC76914E18C5AF9E3E9223B5093_H
#define ANCHORPOSITIONCONFIRMEDEVENT_T63B8BA26EFA42FC76914E18C5AF9E3E9223B5093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MidAirPositionerBehaviour_AnchorPositionConfirmedEvent
struct  AnchorPositionConfirmedEvent_t63B8BA26EFA42FC76914E18C5AF9E3E9223B5093  : public UnityEvent_1_tD15FCA738F4102B872C1480A6678ED16A8A51845
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORPOSITIONCONFIRMEDEVENT_T63B8BA26EFA42FC76914E18C5AF9E3E9223B5093_H
#ifndef GUIDEVIEWDISPLAYMODE_T88D0866B25C697FC57941C3608F5C0C58928533D_H
#define GUIDEVIEWDISPLAYMODE_T88D0866B25C697FC57941C3608F5C0C58928533D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ModelTargetBehaviour_GuideViewDisplayMode
struct  GuideViewDisplayMode_t88D0866B25C697FC57941C3608F5C0C58928533D 
{
public:
	// System.Int32 Vuforia.ModelTargetBehaviour_GuideViewDisplayMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GuideViewDisplayMode_t88D0866B25C697FC57941C3608F5C0C58928533D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDEVIEWDISPLAYMODE_T88D0866B25C697FC57941C3608F5C0C58928533D_H
#ifndef PIXEL_FORMAT_T2C0F763FCD84C633340917492F06896787FD9383_H
#define PIXEL_FORMAT_T2C0F763FCD84C633340917492F06896787FD9383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PIXEL_FORMAT
struct  PIXEL_FORMAT_t2C0F763FCD84C633340917492F06896787FD9383 
{
public:
	// System.Int32 Vuforia.PIXEL_FORMAT::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PIXEL_FORMAT_t2C0F763FCD84C633340917492F06896787FD9383, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_FORMAT_T2C0F763FCD84C633340917492F06896787FD9383_H
#ifndef PLAYMODEEYEWEARDEVICE_T23F975E41EB03A9E26FD9408058C481B8550950F_H
#define PLAYMODEEYEWEARDEVICE_T23F975E41EB03A9E26FD9408058C481B8550950F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeEyewearDevice
struct  PlayModeEyewearDevice_t23F975E41EB03A9E26FD9408058C481B8550950F  : public EyewearDevice_t083CA7719B929A57A532B386322C61658849AA1E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODEEYEWEARDEVICE_T23F975E41EB03A9E26FD9408058C481B8550950F_H
#ifndef PLAYMODESMARTTERRAINIMPL_TFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B_H
#define PLAYMODESMARTTERRAINIMPL_TFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlaymodeSmartTerrainImpl
struct  PlaymodeSmartTerrainImpl_tFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B  : public SmartTerrain_t09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47
{
public:
	// UnityEngine.Vector3 Vuforia.PlaymodeSmartTerrainImpl::mEmulatorPlaneSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mEmulatorPlaneSize_1;
	// UnityEngine.GameObject Vuforia.PlaymodeSmartTerrainImpl::mEmulatorGroundPlane
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mEmulatorGroundPlane_2;
	// UnityEngine.GameObject Vuforia.PlaymodeSmartTerrainImpl::mGroundPlaneImageTarget
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mGroundPlaneImageTarget_3;

public:
	inline static int32_t get_offset_of_mEmulatorPlaneSize_1() { return static_cast<int32_t>(offsetof(PlaymodeSmartTerrainImpl_tFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B, ___mEmulatorPlaneSize_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mEmulatorPlaneSize_1() const { return ___mEmulatorPlaneSize_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mEmulatorPlaneSize_1() { return &___mEmulatorPlaneSize_1; }
	inline void set_mEmulatorPlaneSize_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mEmulatorPlaneSize_1 = value;
	}

	inline static int32_t get_offset_of_mEmulatorGroundPlane_2() { return static_cast<int32_t>(offsetof(PlaymodeSmartTerrainImpl_tFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B, ___mEmulatorGroundPlane_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mEmulatorGroundPlane_2() const { return ___mEmulatorGroundPlane_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mEmulatorGroundPlane_2() { return &___mEmulatorGroundPlane_2; }
	inline void set_mEmulatorGroundPlane_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mEmulatorGroundPlane_2 = value;
		Il2CppCodeGenWriteBarrier((&___mEmulatorGroundPlane_2), value);
	}

	inline static int32_t get_offset_of_mGroundPlaneImageTarget_3() { return static_cast<int32_t>(offsetof(PlaymodeSmartTerrainImpl_tFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B, ___mGroundPlaneImageTarget_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mGroundPlaneImageTarget_3() const { return ___mGroundPlaneImageTarget_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mGroundPlaneImageTarget_3() { return &___mGroundPlaneImageTarget_3; }
	inline void set_mGroundPlaneImageTarget_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mGroundPlaneImageTarget_3 = value;
		Il2CppCodeGenWriteBarrier((&___mGroundPlaneImageTarget_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODESMARTTERRAINIMPL_TFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B_H
#ifndef ROTATIONALDEVICETRACKER_TFDD94B013A18CCDE7AED9F6A13C8147742E66DEA_H
#define ROTATIONALDEVICETRACKER_TFDD94B013A18CCDE7AED9F6A13C8147742E66DEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalDeviceTracker
struct  RotationalDeviceTracker_tFDD94B013A18CCDE7AED9F6A13C8147742E66DEA  : public DeviceTracker_tD6E28B77342C2CBE99C14112AFA51C0798EC3086
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONALDEVICETRACKER_TFDD94B013A18CCDE7AED9F6A13C8147742E66DEA_H
#ifndef MODEL_CORRECTION_MODE_TA628D33CBBD6F312044315FED05B471AD6AEA17E_H
#define MODEL_CORRECTION_MODE_TA628D33CBBD6F312044315FED05B471AD6AEA17E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalDeviceTracker_MODEL_CORRECTION_MODE
struct  MODEL_CORRECTION_MODE_tA628D33CBBD6F312044315FED05B471AD6AEA17E 
{
public:
	// System.Int32 Vuforia.RotationalDeviceTracker_MODEL_CORRECTION_MODE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MODEL_CORRECTION_MODE_tA628D33CBBD6F312044315FED05B471AD6AEA17E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODEL_CORRECTION_MODE_TA628D33CBBD6F312044315FED05B471AD6AEA17E_H
#ifndef STATE_T3FDFF603F7E24F1BA6D3159D9F4CC7E1B4F8F4CE_H
#define STATE_T3FDFF603F7E24F1BA6D3159D9F4CC7E1B4F8F4CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainARController_State
struct  State_t3FDFF603F7E24F1BA6D3159D9F4CC7E1B4F8F4CE 
{
public:
	// System.Int32 Vuforia.SmartTerrainARController_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t3FDFF603F7E24F1BA6D3159D9F4CC7E1B4F8F4CE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T3FDFF603F7E24F1BA6D3159D9F4CC7E1B4F8F4CE_H
#ifndef SMARTTERRAINIMPL_TF475FBE8773C95101B5CB6A85B1F31D982402340_H
#define SMARTTERRAINIMPL_TF475FBE8773C95101B5CB6A85B1F31D982402340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainImpl
struct  SmartTerrainImpl_tF475FBE8773C95101B5CB6A85B1F31D982402340  : public SmartTerrain_t09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINIMPL_TF475FBE8773C95101B5CB6A85B1F31D982402340_H
#ifndef STAGETYPE_T365B0DEB2DCA1C8053A874CB86AAA2AD99C93AC6_H
#define STAGETYPE_T365B0DEB2DCA1C8053A874CB86AAA2AD99C93AC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StageType
struct  StageType_t365B0DEB2DCA1C8053A874CB86AAA2AD99C93AC6 
{
public:
	// System.Int32 Vuforia.StageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StageType_t365B0DEB2DCA1C8053A874CB86AAA2AD99C93AC6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STAGETYPE_T365B0DEB2DCA1C8053A874CB86AAA2AD99C93AC6_H
#ifndef MODE_TFAC11BEAD3F06C10D033DA80E27982CADB4008C8_H
#define MODE_TFAC11BEAD3F06C10D033DA80E27982CADB4008C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StereoProjMatrixStore_Mode
struct  Mode_tFAC11BEAD3F06C10D033DA80E27982CADB4008C8 
{
public:
	// System.Int32 Vuforia.StereoProjMatrixStore_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_tFAC11BEAD3F06C10D033DA80E27982CADB4008C8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_TFAC11BEAD3F06C10D033DA80E27982CADB4008C8_H
#ifndef STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#define STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour_Status
struct  Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifndef STATUSINFO_T5507FB8CC09640E7771385EBE27221431A2FEB4E_H
#define STATUSINFO_T5507FB8CC09640E7771385EBE27221431A2FEB4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour_StatusInfo
struct  StatusInfo_t5507FB8CC09640E7771385EBE27221431A2FEB4E 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_StatusInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StatusInfo_t5507FB8CC09640E7771385EBE27221431A2FEB4E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSINFO_T5507FB8CC09640E7771385EBE27221431A2FEB4E_H
#ifndef FRAMESTATE_T40C5EF3D259514DAB23DEDCD72218A934C5645E0_H
#define FRAMESTATE_T40C5EF3D259514DAB23DEDCD72218A934C5645E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_FrameState
#pragma pack(push, tp, 1)
struct  FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0 
{
public:
	// System.IntPtr Vuforia.TrackerData_FrameState::trackableDataArray
	intptr_t ___trackableDataArray_0;
	// System.IntPtr Vuforia.TrackerData_FrameState::vbDataArray
	intptr_t ___vbDataArray_1;
	// System.IntPtr Vuforia.TrackerData_FrameState::vuMarkResultArray
	intptr_t ___vuMarkResultArray_2;
	// System.IntPtr Vuforia.TrackerData_FrameState::newVuMarkDataArray
	intptr_t ___newVuMarkDataArray_3;
	// System.IntPtr Vuforia.TrackerData_FrameState::illuminationData
	intptr_t ___illuminationData_4;
	// System.Int32 Vuforia.TrackerData_FrameState::numTrackableResults
	int32_t ___numTrackableResults_5;
	// System.Int32 Vuforia.TrackerData_FrameState::numVirtualButtonResults
	int32_t ___numVirtualButtonResults_6;
	// System.Int32 Vuforia.TrackerData_FrameState::frameIndex
	int32_t ___frameIndex_7;
	// System.Int32 Vuforia.TrackerData_FrameState::numVuMarkResults
	int32_t ___numVuMarkResults_8;
	// System.Int32 Vuforia.TrackerData_FrameState::numNewVuMarks
	int32_t ___numNewVuMarks_9;
	// System.Int32 Vuforia.TrackerData_FrameState::deviceTrackableId
	int32_t ___deviceTrackableId_10;
	// System.Int32 Vuforia.TrackerData_FrameState::deviceTrackableStatusInfo
	int32_t ___deviceTrackableStatusInfo_11;
	// UnityEngine.Vector4 Vuforia.TrackerData_FrameState::minCameraCalibration
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___minCameraCalibration_12;

public:
	inline static int32_t get_offset_of_trackableDataArray_0() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___trackableDataArray_0)); }
	inline intptr_t get_trackableDataArray_0() const { return ___trackableDataArray_0; }
	inline intptr_t* get_address_of_trackableDataArray_0() { return &___trackableDataArray_0; }
	inline void set_trackableDataArray_0(intptr_t value)
	{
		___trackableDataArray_0 = value;
	}

	inline static int32_t get_offset_of_vbDataArray_1() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___vbDataArray_1)); }
	inline intptr_t get_vbDataArray_1() const { return ___vbDataArray_1; }
	inline intptr_t* get_address_of_vbDataArray_1() { return &___vbDataArray_1; }
	inline void set_vbDataArray_1(intptr_t value)
	{
		___vbDataArray_1 = value;
	}

	inline static int32_t get_offset_of_vuMarkResultArray_2() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___vuMarkResultArray_2)); }
	inline intptr_t get_vuMarkResultArray_2() const { return ___vuMarkResultArray_2; }
	inline intptr_t* get_address_of_vuMarkResultArray_2() { return &___vuMarkResultArray_2; }
	inline void set_vuMarkResultArray_2(intptr_t value)
	{
		___vuMarkResultArray_2 = value;
	}

	inline static int32_t get_offset_of_newVuMarkDataArray_3() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___newVuMarkDataArray_3)); }
	inline intptr_t get_newVuMarkDataArray_3() const { return ___newVuMarkDataArray_3; }
	inline intptr_t* get_address_of_newVuMarkDataArray_3() { return &___newVuMarkDataArray_3; }
	inline void set_newVuMarkDataArray_3(intptr_t value)
	{
		___newVuMarkDataArray_3 = value;
	}

	inline static int32_t get_offset_of_illuminationData_4() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___illuminationData_4)); }
	inline intptr_t get_illuminationData_4() const { return ___illuminationData_4; }
	inline intptr_t* get_address_of_illuminationData_4() { return &___illuminationData_4; }
	inline void set_illuminationData_4(intptr_t value)
	{
		___illuminationData_4 = value;
	}

	inline static int32_t get_offset_of_numTrackableResults_5() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numTrackableResults_5)); }
	inline int32_t get_numTrackableResults_5() const { return ___numTrackableResults_5; }
	inline int32_t* get_address_of_numTrackableResults_5() { return &___numTrackableResults_5; }
	inline void set_numTrackableResults_5(int32_t value)
	{
		___numTrackableResults_5 = value;
	}

	inline static int32_t get_offset_of_numVirtualButtonResults_6() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numVirtualButtonResults_6)); }
	inline int32_t get_numVirtualButtonResults_6() const { return ___numVirtualButtonResults_6; }
	inline int32_t* get_address_of_numVirtualButtonResults_6() { return &___numVirtualButtonResults_6; }
	inline void set_numVirtualButtonResults_6(int32_t value)
	{
		___numVirtualButtonResults_6 = value;
	}

	inline static int32_t get_offset_of_frameIndex_7() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___frameIndex_7)); }
	inline int32_t get_frameIndex_7() const { return ___frameIndex_7; }
	inline int32_t* get_address_of_frameIndex_7() { return &___frameIndex_7; }
	inline void set_frameIndex_7(int32_t value)
	{
		___frameIndex_7 = value;
	}

	inline static int32_t get_offset_of_numVuMarkResults_8() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numVuMarkResults_8)); }
	inline int32_t get_numVuMarkResults_8() const { return ___numVuMarkResults_8; }
	inline int32_t* get_address_of_numVuMarkResults_8() { return &___numVuMarkResults_8; }
	inline void set_numVuMarkResults_8(int32_t value)
	{
		___numVuMarkResults_8 = value;
	}

	inline static int32_t get_offset_of_numNewVuMarks_9() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numNewVuMarks_9)); }
	inline int32_t get_numNewVuMarks_9() const { return ___numNewVuMarks_9; }
	inline int32_t* get_address_of_numNewVuMarks_9() { return &___numNewVuMarks_9; }
	inline void set_numNewVuMarks_9(int32_t value)
	{
		___numNewVuMarks_9 = value;
	}

	inline static int32_t get_offset_of_deviceTrackableId_10() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___deviceTrackableId_10)); }
	inline int32_t get_deviceTrackableId_10() const { return ___deviceTrackableId_10; }
	inline int32_t* get_address_of_deviceTrackableId_10() { return &___deviceTrackableId_10; }
	inline void set_deviceTrackableId_10(int32_t value)
	{
		___deviceTrackableId_10 = value;
	}

	inline static int32_t get_offset_of_deviceTrackableStatusInfo_11() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___deviceTrackableStatusInfo_11)); }
	inline int32_t get_deviceTrackableStatusInfo_11() const { return ___deviceTrackableStatusInfo_11; }
	inline int32_t* get_address_of_deviceTrackableStatusInfo_11() { return &___deviceTrackableStatusInfo_11; }
	inline void set_deviceTrackableStatusInfo_11(int32_t value)
	{
		___deviceTrackableStatusInfo_11 = value;
	}

	inline static int32_t get_offset_of_minCameraCalibration_12() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___minCameraCalibration_12)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_minCameraCalibration_12() const { return ___minCameraCalibration_12; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_minCameraCalibration_12() { return &___minCameraCalibration_12; }
	inline void set_minCameraCalibration_12(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___minCameraCalibration_12 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMESTATE_T40C5EF3D259514DAB23DEDCD72218A934C5645E0_H
#ifndef ILLUMINATIONDATA_TD92AC3353F4A03A75AC592B597C88238195BF12C_H
#define ILLUMINATIONDATA_TD92AC3353F4A03A75AC592B597C88238195BF12C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_IlluminationData
#pragma pack(push, tp, 1)
struct  IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C 
{
public:
	// System.Single Vuforia.TrackerData_IlluminationData::ambientIntensity
	float ___ambientIntensity_0;
	// System.Single Vuforia.TrackerData_IlluminationData::ambientColorTemperature
	float ___ambientColorTemperature_1;
	// System.Single Vuforia.TrackerData_IlluminationData::intensityCorrection
	float ___intensityCorrection_2;
	// UnityEngine.Vector4 Vuforia.TrackerData_IlluminationData::colorCorrection
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___colorCorrection_3;

public:
	inline static int32_t get_offset_of_ambientIntensity_0() { return static_cast<int32_t>(offsetof(IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C, ___ambientIntensity_0)); }
	inline float get_ambientIntensity_0() const { return ___ambientIntensity_0; }
	inline float* get_address_of_ambientIntensity_0() { return &___ambientIntensity_0; }
	inline void set_ambientIntensity_0(float value)
	{
		___ambientIntensity_0 = value;
	}

	inline static int32_t get_offset_of_ambientColorTemperature_1() { return static_cast<int32_t>(offsetof(IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C, ___ambientColorTemperature_1)); }
	inline float get_ambientColorTemperature_1() const { return ___ambientColorTemperature_1; }
	inline float* get_address_of_ambientColorTemperature_1() { return &___ambientColorTemperature_1; }
	inline void set_ambientColorTemperature_1(float value)
	{
		___ambientColorTemperature_1 = value;
	}

	inline static int32_t get_offset_of_intensityCorrection_2() { return static_cast<int32_t>(offsetof(IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C, ___intensityCorrection_2)); }
	inline float get_intensityCorrection_2() const { return ___intensityCorrection_2; }
	inline float* get_address_of_intensityCorrection_2() { return &___intensityCorrection_2; }
	inline void set_intensityCorrection_2(float value)
	{
		___intensityCorrection_2 = value;
	}

	inline static int32_t get_offset_of_colorCorrection_3() { return static_cast<int32_t>(offsetof(IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C, ___colorCorrection_3)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_colorCorrection_3() const { return ___colorCorrection_3; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_colorCorrection_3() { return &___colorCorrection_3; }
	inline void set_colorCorrection_3(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___colorCorrection_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ILLUMINATIONDATA_TD92AC3353F4A03A75AC592B597C88238195BF12C_H
#ifndef IMAGEHEADERDATA_TFC0673E37281D53EB3031F0A59652D09EEE08121_H
#define IMAGEHEADERDATA_TFC0673E37281D53EB3031F0A59652D09EEE08121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_ImageHeaderData
#pragma pack(push, tp, 1)
struct  ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121 
{
public:
	// System.IntPtr Vuforia.TrackerData_ImageHeaderData::data
	intptr_t ___data_0;
	// System.Int32 Vuforia.TrackerData_ImageHeaderData::width
	int32_t ___width_1;
	// System.Int32 Vuforia.TrackerData_ImageHeaderData::height
	int32_t ___height_2;
	// System.Int32 Vuforia.TrackerData_ImageHeaderData::stride
	int32_t ___stride_3;
	// System.Int32 Vuforia.TrackerData_ImageHeaderData::bufferWidth
	int32_t ___bufferWidth_4;
	// System.Int32 Vuforia.TrackerData_ImageHeaderData::bufferHeight
	int32_t ___bufferHeight_5;
	// System.Int32 Vuforia.TrackerData_ImageHeaderData::format
	int32_t ___format_6;
	// System.Int32 Vuforia.TrackerData_ImageHeaderData::reallocate
	int32_t ___reallocate_7;
	// System.Int32 Vuforia.TrackerData_ImageHeaderData::updated
	int32_t ___updated_8;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121, ___data_0)); }
	inline intptr_t get_data_0() const { return ___data_0; }
	inline intptr_t* get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(intptr_t value)
	{
		___data_0 = value;
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}

	inline static int32_t get_offset_of_stride_3() { return static_cast<int32_t>(offsetof(ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121, ___stride_3)); }
	inline int32_t get_stride_3() const { return ___stride_3; }
	inline int32_t* get_address_of_stride_3() { return &___stride_3; }
	inline void set_stride_3(int32_t value)
	{
		___stride_3 = value;
	}

	inline static int32_t get_offset_of_bufferWidth_4() { return static_cast<int32_t>(offsetof(ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121, ___bufferWidth_4)); }
	inline int32_t get_bufferWidth_4() const { return ___bufferWidth_4; }
	inline int32_t* get_address_of_bufferWidth_4() { return &___bufferWidth_4; }
	inline void set_bufferWidth_4(int32_t value)
	{
		___bufferWidth_4 = value;
	}

	inline static int32_t get_offset_of_bufferHeight_5() { return static_cast<int32_t>(offsetof(ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121, ___bufferHeight_5)); }
	inline int32_t get_bufferHeight_5() const { return ___bufferHeight_5; }
	inline int32_t* get_address_of_bufferHeight_5() { return &___bufferHeight_5; }
	inline void set_bufferHeight_5(int32_t value)
	{
		___bufferHeight_5 = value;
	}

	inline static int32_t get_offset_of_format_6() { return static_cast<int32_t>(offsetof(ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121, ___format_6)); }
	inline int32_t get_format_6() const { return ___format_6; }
	inline int32_t* get_address_of_format_6() { return &___format_6; }
	inline void set_format_6(int32_t value)
	{
		___format_6 = value;
	}

	inline static int32_t get_offset_of_reallocate_7() { return static_cast<int32_t>(offsetof(ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121, ___reallocate_7)); }
	inline int32_t get_reallocate_7() const { return ___reallocate_7; }
	inline int32_t* get_address_of_reallocate_7() { return &___reallocate_7; }
	inline void set_reallocate_7(int32_t value)
	{
		___reallocate_7 = value;
	}

	inline static int32_t get_offset_of_updated_8() { return static_cast<int32_t>(offsetof(ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121, ___updated_8)); }
	inline int32_t get_updated_8() const { return ___updated_8; }
	inline int32_t* get_address_of_updated_8() { return &___updated_8; }
	inline void set_updated_8(int32_t value)
	{
		___updated_8 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEHEADERDATA_TFC0673E37281D53EB3031F0A59652D09EEE08121_H
#ifndef INSTANCEIDDATA_TA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2_H
#define INSTANCEIDDATA_TA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_InstanceIdData
#pragma pack(push, tp, 1)
struct  InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2 
{
public:
	// System.UInt64 Vuforia.TrackerData_InstanceIdData::numericValue
	uint64_t ___numericValue_0;
	// System.IntPtr Vuforia.TrackerData_InstanceIdData::buffer
	intptr_t ___buffer_1;
	// System.IntPtr Vuforia.TrackerData_InstanceIdData::reserved
	intptr_t ___reserved_2;
	// System.UInt32 Vuforia.TrackerData_InstanceIdData::dataLength
	uint32_t ___dataLength_3;
	// System.Int32 Vuforia.TrackerData_InstanceIdData::dataType
	int32_t ___dataType_4;

public:
	inline static int32_t get_offset_of_numericValue_0() { return static_cast<int32_t>(offsetof(InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2, ___numericValue_0)); }
	inline uint64_t get_numericValue_0() const { return ___numericValue_0; }
	inline uint64_t* get_address_of_numericValue_0() { return &___numericValue_0; }
	inline void set_numericValue_0(uint64_t value)
	{
		___numericValue_0 = value;
	}

	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2, ___buffer_1)); }
	inline intptr_t get_buffer_1() const { return ___buffer_1; }
	inline intptr_t* get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(intptr_t value)
	{
		___buffer_1 = value;
	}

	inline static int32_t get_offset_of_reserved_2() { return static_cast<int32_t>(offsetof(InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2, ___reserved_2)); }
	inline intptr_t get_reserved_2() const { return ___reserved_2; }
	inline intptr_t* get_address_of_reserved_2() { return &___reserved_2; }
	inline void set_reserved_2(intptr_t value)
	{
		___reserved_2 = value;
	}

	inline static int32_t get_offset_of_dataLength_3() { return static_cast<int32_t>(offsetof(InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2, ___dataLength_3)); }
	inline uint32_t get_dataLength_3() const { return ___dataLength_3; }
	inline uint32_t* get_address_of_dataLength_3() { return &___dataLength_3; }
	inline void set_dataLength_3(uint32_t value)
	{
		___dataLength_3 = value;
	}

	inline static int32_t get_offset_of_dataType_4() { return static_cast<int32_t>(offsetof(InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2, ___dataType_4)); }
	inline int32_t get_dataType_4() const { return ___dataType_4; }
	inline int32_t* get_address_of_dataType_4() { return &___dataType_4; }
	inline void set_dataType_4(int32_t value)
	{
		___dataType_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDDATA_TA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2_H
#ifndef POSEDATA_TBEB2E3213824EA43B0606A888A09A32D6433881B_H
#define POSEDATA_TBEB2E3213824EA43B0606A888A09A32D6433881B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_PoseData
#pragma pack(push, tp, 1)
struct  PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B 
{
public:
	// UnityEngine.Vector3 Vuforia.TrackerData_PoseData::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Quaternion Vuforia.TrackerData_PoseData::orientation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___orientation_1;
	// System.Int32 Vuforia.TrackerData_PoseData::unused
	int32_t ___unused_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_orientation_1() { return static_cast<int32_t>(offsetof(PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B, ___orientation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_orientation_1() const { return ___orientation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_orientation_1() { return &___orientation_1; }
	inline void set_orientation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___orientation_1 = value;
	}

	inline static int32_t get_offset_of_unused_2() { return static_cast<int32_t>(offsetof(PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B, ___unused_2)); }
	inline int32_t get_unused_2() const { return ___unused_2; }
	inline int32_t* get_address_of_unused_2() { return &___unused_2; }
	inline void set_unused_2(int32_t value)
	{
		___unused_2 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEDATA_TBEB2E3213824EA43B0606A888A09A32D6433881B_H
#ifndef STRUCTLIST_T838B13CFE20420E6A031EF2069821BF66948F28F_H
#define STRUCTLIST_T838B13CFE20420E6A031EF2069821BF66948F28F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_StructList
#pragma pack(push, tp, 1)
struct  StructList_t838B13CFE20420E6A031EF2069821BF66948F28F 
{
public:
	// System.IntPtr Vuforia.TrackerData_StructList::arrayPtr
	intptr_t ___arrayPtr_0;
	// System.Int32 Vuforia.TrackerData_StructList::numResults
	int32_t ___numResults_1;
	// System.Int32 Vuforia.TrackerData_StructList::unused
	int32_t ___unused_2;

public:
	inline static int32_t get_offset_of_arrayPtr_0() { return static_cast<int32_t>(offsetof(StructList_t838B13CFE20420E6A031EF2069821BF66948F28F, ___arrayPtr_0)); }
	inline intptr_t get_arrayPtr_0() const { return ___arrayPtr_0; }
	inline intptr_t* get_address_of_arrayPtr_0() { return &___arrayPtr_0; }
	inline void set_arrayPtr_0(intptr_t value)
	{
		___arrayPtr_0 = value;
	}

	inline static int32_t get_offset_of_numResults_1() { return static_cast<int32_t>(offsetof(StructList_t838B13CFE20420E6A031EF2069821BF66948F28F, ___numResults_1)); }
	inline int32_t get_numResults_1() const { return ___numResults_1; }
	inline int32_t* get_address_of_numResults_1() { return &___numResults_1; }
	inline void set_numResults_1(int32_t value)
	{
		___numResults_1 = value;
	}

	inline static int32_t get_offset_of_unused_2() { return static_cast<int32_t>(offsetof(StructList_t838B13CFE20420E6A031EF2069821BF66948F28F, ___unused_2)); }
	inline int32_t get_unused_2() const { return ___unused_2; }
	inline int32_t* get_address_of_unused_2() { return &___unused_2; }
	inline void set_unused_2(int32_t value)
	{
		___unused_2 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRUCTLIST_T838B13CFE20420E6A031EF2069821BF66948F28F_H
#ifndef VIEWERBUTTONTYPE_TD40BFED34EDFA6EF946FC9E75CC0BAEC0FE4C1ED_H
#define VIEWERBUTTONTYPE_TD40BFED34EDFA6EF946FC9E75CC0BAEC0FE4C1ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerButtonType
struct  ViewerButtonType_tD40BFED34EDFA6EF946FC9E75CC0BAEC0FE4C1ED 
{
public:
	// System.Int32 Vuforia.ViewerButtonType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ViewerButtonType_tD40BFED34EDFA6EF946FC9E75CC0BAEC0FE4C1ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERBUTTONTYPE_TD40BFED34EDFA6EF946FC9E75CC0BAEC0FE4C1ED_H
#ifndef VIEWERPARAMETERS_T382177593203E057F8920BAB05625FCDF7FEE3A7_H
#define VIEWERPARAMETERS_T382177593203E057F8920BAB05625FCDF7FEE3A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerParameters
struct  ViewerParameters_t382177593203E057F8920BAB05625FCDF7FEE3A7  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.ViewerParameters::mNativeVP
	intptr_t ___mNativeVP_0;

public:
	inline static int32_t get_offset_of_mNativeVP_0() { return static_cast<int32_t>(offsetof(ViewerParameters_t382177593203E057F8920BAB05625FCDF7FEE3A7, ___mNativeVP_0)); }
	inline intptr_t get_mNativeVP_0() const { return ___mNativeVP_0; }
	inline intptr_t* get_address_of_mNativeVP_0() { return &___mNativeVP_0; }
	inline void set_mNativeVP_0(intptr_t value)
	{
		___mNativeVP_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERPARAMETERS_T382177593203E057F8920BAB05625FCDF7FEE3A7_H
#ifndef VIEWERTRAYALIGNMENT_T5528CE704439AEADBE3566805CF4E7441A11A444_H
#define VIEWERTRAYALIGNMENT_T5528CE704439AEADBE3566805CF4E7441A11A444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerTrayAlignment
struct  ViewerTrayAlignment_t5528CE704439AEADBE3566805CF4E7441A11A444 
{
public:
	// System.Int32 Vuforia.ViewerTrayAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ViewerTrayAlignment_t5528CE704439AEADBE3566805CF4E7441A11A444, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERTRAYALIGNMENT_T5528CE704439AEADBE3566805CF4E7441A11A444_H
#ifndef VUMARKTARGETIMPL_T6455B455669A18BE0FCA10863C50CB61F7043A91_H
#define VUMARKTARGETIMPL_T6455B455669A18BE0FCA10863C50CB61F7043A91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkTargetImpl
struct  VuMarkTargetImpl_t6455B455669A18BE0FCA10863C50CB61F7043A91  : public ObjectTargetImpl_tEDDDACB8EF83100B0896957801DF1F85DB6A126A
{
public:
	// Vuforia.VuMarkTemplateImpl Vuforia.VuMarkTargetImpl::mVuMarkTemplate
	VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8 * ___mVuMarkTemplate_3;
	// Vuforia.InstanceIdImpl Vuforia.VuMarkTargetImpl::mInstanceId
	InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371 * ___mInstanceId_4;

public:
	inline static int32_t get_offset_of_mVuMarkTemplate_3() { return static_cast<int32_t>(offsetof(VuMarkTargetImpl_t6455B455669A18BE0FCA10863C50CB61F7043A91, ___mVuMarkTemplate_3)); }
	inline VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8 * get_mVuMarkTemplate_3() const { return ___mVuMarkTemplate_3; }
	inline VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8 ** get_address_of_mVuMarkTemplate_3() { return &___mVuMarkTemplate_3; }
	inline void set_mVuMarkTemplate_3(VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8 * value)
	{
		___mVuMarkTemplate_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTemplate_3), value);
	}

	inline static int32_t get_offset_of_mInstanceId_4() { return static_cast<int32_t>(offsetof(VuMarkTargetImpl_t6455B455669A18BE0FCA10863C50CB61F7043A91, ___mInstanceId_4)); }
	inline InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371 * get_mInstanceId_4() const { return ___mInstanceId_4; }
	inline InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371 ** get_address_of_mInstanceId_4() { return &___mInstanceId_4; }
	inline void set_mInstanceId_4(InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371 * value)
	{
		___mInstanceId_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstanceId_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTARGETIMPL_T6455B455669A18BE0FCA10863C50CB61F7043A91_H
#ifndef WORLDCENTERMODE_T53E1430BD989A54F75332A2DA9D61C93545897E6_H
#define WORLDCENTERMODE_T53E1430BD989A54F75332A2DA9D61C93545897E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController_WorldCenterMode
struct  WorldCenterMode_t53E1430BD989A54F75332A2DA9D61C93545897E6 
{
public:
	// System.Int32 Vuforia.VuforiaARController_WorldCenterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WorldCenterMode_t53E1430BD989A54F75332A2DA9D61C93545897E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCENTERMODE_T53E1430BD989A54F75332A2DA9D61C93545897E6_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BASECAMERACONFIGURATION_TF625F5FF38D44D6E2D1D929C1F29DA533F97642A_H
#define BASECAMERACONFIGURATION_TF625F5FF38D44D6E2D1D929C1F29DA533F97642A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BaseCameraConfiguration
struct  BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A  : public RuntimeObject
{
public:
	// Vuforia.CameraDevice_CameraDeviceMode Vuforia.BaseCameraConfiguration::mCameraDeviceMode
	int32_t ___mCameraDeviceMode_0;
	// System.Action Vuforia.BaseCameraConfiguration::mOnVideoBackgroundConfigChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnVideoBackgroundConfigChanged_1;
	// Vuforia.VideoBackgroundBehaviour Vuforia.BaseCameraConfiguration::mVideoBackgroundBehaviour
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00 * ___mVideoBackgroundBehaviour_2;
	// UnityEngine.Rect Vuforia.BaseCameraConfiguration::mVideoBackgroundViewportRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___mVideoBackgroundViewportRect_3;
	// System.Boolean Vuforia.BaseCameraConfiguration::mRenderVideoBackground
	bool ___mRenderVideoBackground_4;
	// UnityEngine.ScreenOrientation Vuforia.BaseCameraConfiguration::mProjectionOrientation
	int32_t ___mProjectionOrientation_5;
	// Vuforia.BackgroundPlaneBehaviour Vuforia.BaseCameraConfiguration::mBackgroundPlaneBehaviour
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 * ___mBackgroundPlaneBehaviour_6;
	// System.Boolean Vuforia.BaseCameraConfiguration::mDeviceModeChanged
	bool ___mDeviceModeChanged_7;
	// System.Boolean Vuforia.BaseCameraConfiguration::mProjectionParamsChanged
	bool ___mProjectionParamsChanged_8;

public:
	inline static int32_t get_offset_of_mCameraDeviceMode_0() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A, ___mCameraDeviceMode_0)); }
	inline int32_t get_mCameraDeviceMode_0() const { return ___mCameraDeviceMode_0; }
	inline int32_t* get_address_of_mCameraDeviceMode_0() { return &___mCameraDeviceMode_0; }
	inline void set_mCameraDeviceMode_0(int32_t value)
	{
		___mCameraDeviceMode_0 = value;
	}

	inline static int32_t get_offset_of_mOnVideoBackgroundConfigChanged_1() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A, ___mOnVideoBackgroundConfigChanged_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnVideoBackgroundConfigChanged_1() const { return ___mOnVideoBackgroundConfigChanged_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnVideoBackgroundConfigChanged_1() { return &___mOnVideoBackgroundConfigChanged_1; }
	inline void set_mOnVideoBackgroundConfigChanged_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnVideoBackgroundConfigChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVideoBackgroundConfigChanged_1), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundBehaviour_2() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A, ___mVideoBackgroundBehaviour_2)); }
	inline VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00 * get_mVideoBackgroundBehaviour_2() const { return ___mVideoBackgroundBehaviour_2; }
	inline VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00 ** get_address_of_mVideoBackgroundBehaviour_2() { return &___mVideoBackgroundBehaviour_2; }
	inline void set_mVideoBackgroundBehaviour_2(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00 * value)
	{
		___mVideoBackgroundBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundBehaviour_2), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundViewportRect_3() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A, ___mVideoBackgroundViewportRect_3)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_mVideoBackgroundViewportRect_3() const { return ___mVideoBackgroundViewportRect_3; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_mVideoBackgroundViewportRect_3() { return &___mVideoBackgroundViewportRect_3; }
	inline void set_mVideoBackgroundViewportRect_3(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___mVideoBackgroundViewportRect_3 = value;
	}

	inline static int32_t get_offset_of_mRenderVideoBackground_4() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A, ___mRenderVideoBackground_4)); }
	inline bool get_mRenderVideoBackground_4() const { return ___mRenderVideoBackground_4; }
	inline bool* get_address_of_mRenderVideoBackground_4() { return &___mRenderVideoBackground_4; }
	inline void set_mRenderVideoBackground_4(bool value)
	{
		___mRenderVideoBackground_4 = value;
	}

	inline static int32_t get_offset_of_mProjectionOrientation_5() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A, ___mProjectionOrientation_5)); }
	inline int32_t get_mProjectionOrientation_5() const { return ___mProjectionOrientation_5; }
	inline int32_t* get_address_of_mProjectionOrientation_5() { return &___mProjectionOrientation_5; }
	inline void set_mProjectionOrientation_5(int32_t value)
	{
		___mProjectionOrientation_5 = value;
	}

	inline static int32_t get_offset_of_mBackgroundPlaneBehaviour_6() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A, ___mBackgroundPlaneBehaviour_6)); }
	inline BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 * get_mBackgroundPlaneBehaviour_6() const { return ___mBackgroundPlaneBehaviour_6; }
	inline BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 ** get_address_of_mBackgroundPlaneBehaviour_6() { return &___mBackgroundPlaneBehaviour_6; }
	inline void set_mBackgroundPlaneBehaviour_6(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 * value)
	{
		___mBackgroundPlaneBehaviour_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundPlaneBehaviour_6), value);
	}

	inline static int32_t get_offset_of_mDeviceModeChanged_7() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A, ___mDeviceModeChanged_7)); }
	inline bool get_mDeviceModeChanged_7() const { return ___mDeviceModeChanged_7; }
	inline bool* get_address_of_mDeviceModeChanged_7() { return &___mDeviceModeChanged_7; }
	inline void set_mDeviceModeChanged_7(bool value)
	{
		___mDeviceModeChanged_7 = value;
	}

	inline static int32_t get_offset_of_mProjectionParamsChanged_8() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A, ___mProjectionParamsChanged_8)); }
	inline bool get_mProjectionParamsChanged_8() const { return ___mProjectionParamsChanged_8; }
	inline bool* get_address_of_mProjectionParamsChanged_8() { return &___mProjectionParamsChanged_8; }
	inline void set_mProjectionParamsChanged_8(bool value)
	{
		___mProjectionParamsChanged_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASECAMERACONFIGURATION_TF625F5FF38D44D6E2D1D929C1F29DA533F97642A_H
#ifndef CUSTOMDATASETTARGETSIZE_T2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031_H
#define CUSTOMDATASETTARGETSIZE_T2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CustomDataSetTargetSize
struct  CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031  : public DataSetTargetSize_t8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517
{
public:
	// System.Boolean Vuforia.CustomDataSetTargetSize::mInvokeBeforeNativeCall
	bool ___mInvokeBeforeNativeCall_3;
	// System.Action`1<UnityEngine.Vector3> Vuforia.CustomDataSetTargetSize::mSetSizeAction
	Action_1_tE2F19E30ECDF39669C80D0E7DA21064D10C1EE2F * ___mSetSizeAction_4;

public:
	inline static int32_t get_offset_of_mInvokeBeforeNativeCall_3() { return static_cast<int32_t>(offsetof(CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031, ___mInvokeBeforeNativeCall_3)); }
	inline bool get_mInvokeBeforeNativeCall_3() const { return ___mInvokeBeforeNativeCall_3; }
	inline bool* get_address_of_mInvokeBeforeNativeCall_3() { return &___mInvokeBeforeNativeCall_3; }
	inline void set_mInvokeBeforeNativeCall_3(bool value)
	{
		___mInvokeBeforeNativeCall_3 = value;
	}

	inline static int32_t get_offset_of_mSetSizeAction_4() { return static_cast<int32_t>(offsetof(CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031, ___mSetSizeAction_4)); }
	inline Action_1_tE2F19E30ECDF39669C80D0E7DA21064D10C1EE2F * get_mSetSizeAction_4() const { return ___mSetSizeAction_4; }
	inline Action_1_tE2F19E30ECDF39669C80D0E7DA21064D10C1EE2F ** get_address_of_mSetSizeAction_4() { return &___mSetSizeAction_4; }
	inline void set_mSetSizeAction_4(Action_1_tE2F19E30ECDF39669C80D0E7DA21064D10C1EE2F * value)
	{
		___mSetSizeAction_4 = value;
		Il2CppCodeGenWriteBarrier((&___mSetSizeAction_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMDATASETTARGETSIZE_T2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031_H
#ifndef CUSTOMVIEWERPARAMETERS_T5F3D3A911D28E3D98760A6BCA1F2E984542E2075_H
#define CUSTOMVIEWERPARAMETERS_T5F3D3A911D28E3D98760A6BCA1F2E984542E2075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CustomViewerParameters
struct  CustomViewerParameters_t5F3D3A911D28E3D98760A6BCA1F2E984542E2075  : public ViewerParameters_t382177593203E057F8920BAB05625FCDF7FEE3A7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMVIEWERPARAMETERS_T5F3D3A911D28E3D98760A6BCA1F2E984542E2075_H
#ifndef DEVICETRACKERARCONTROLLER_T144EF27DF432AA01FA50AA677B1EBFC37116BBC6_H
#define DEVICETRACKERARCONTROLLER_T144EF27DF432AA01FA50AA677B1EBFC37116BBC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTrackerARController
struct  DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6  : public ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293
{
public:
	// Vuforia.DeviceTracker_TRACKING_MODE Vuforia.DeviceTrackerARController::mTrackingMode
	int32_t ___mTrackingMode_3;
	// System.Boolean Vuforia.DeviceTrackerARController::mAutoInitTracker
	bool ___mAutoInitTracker_4;
	// System.Boolean Vuforia.DeviceTrackerARController::mAutoStartTracker
	bool ___mAutoStartTracker_5;
	// System.Boolean Vuforia.DeviceTrackerARController::mPosePrediction
	bool ___mPosePrediction_6;
	// Vuforia.RotationalDeviceTracker_MODEL_CORRECTION_MODE Vuforia.DeviceTrackerARController::mModelCorrectionMode
	int32_t ___mModelCorrectionMode_7;
	// System.Boolean Vuforia.DeviceTrackerARController::mModelTransformEnabled
	bool ___mModelTransformEnabled_8;
	// UnityEngine.Vector3 Vuforia.DeviceTrackerARController::mModelTransform
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mModelTransform_9;
	// System.Action Vuforia.DeviceTrackerARController::mTrackerStarted
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mTrackerStarted_10;
	// System.Boolean Vuforia.DeviceTrackerARController::mTrackerWasActiveBeforePause
	bool ___mTrackerWasActiveBeforePause_11;
	// System.Boolean Vuforia.DeviceTrackerARController::mTrackerWasActiveBeforeDisabling
	bool ___mTrackerWasActiveBeforeDisabling_12;
	// System.Single Vuforia.DeviceTrackerARController::mEmulatorPositionVelocity
	float ___mEmulatorPositionVelocity_13;
	// Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration Vuforia.DeviceTrackerARController::mDeviceTrackerConfiguration
	DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F * ___mDeviceTrackerConfiguration_14;

public:
	inline static int32_t get_offset_of_mTrackingMode_3() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mTrackingMode_3)); }
	inline int32_t get_mTrackingMode_3() const { return ___mTrackingMode_3; }
	inline int32_t* get_address_of_mTrackingMode_3() { return &___mTrackingMode_3; }
	inline void set_mTrackingMode_3(int32_t value)
	{
		___mTrackingMode_3 = value;
	}

	inline static int32_t get_offset_of_mAutoInitTracker_4() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mAutoInitTracker_4)); }
	inline bool get_mAutoInitTracker_4() const { return ___mAutoInitTracker_4; }
	inline bool* get_address_of_mAutoInitTracker_4() { return &___mAutoInitTracker_4; }
	inline void set_mAutoInitTracker_4(bool value)
	{
		___mAutoInitTracker_4 = value;
	}

	inline static int32_t get_offset_of_mAutoStartTracker_5() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mAutoStartTracker_5)); }
	inline bool get_mAutoStartTracker_5() const { return ___mAutoStartTracker_5; }
	inline bool* get_address_of_mAutoStartTracker_5() { return &___mAutoStartTracker_5; }
	inline void set_mAutoStartTracker_5(bool value)
	{
		___mAutoStartTracker_5 = value;
	}

	inline static int32_t get_offset_of_mPosePrediction_6() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mPosePrediction_6)); }
	inline bool get_mPosePrediction_6() const { return ___mPosePrediction_6; }
	inline bool* get_address_of_mPosePrediction_6() { return &___mPosePrediction_6; }
	inline void set_mPosePrediction_6(bool value)
	{
		___mPosePrediction_6 = value;
	}

	inline static int32_t get_offset_of_mModelCorrectionMode_7() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mModelCorrectionMode_7)); }
	inline int32_t get_mModelCorrectionMode_7() const { return ___mModelCorrectionMode_7; }
	inline int32_t* get_address_of_mModelCorrectionMode_7() { return &___mModelCorrectionMode_7; }
	inline void set_mModelCorrectionMode_7(int32_t value)
	{
		___mModelCorrectionMode_7 = value;
	}

	inline static int32_t get_offset_of_mModelTransformEnabled_8() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mModelTransformEnabled_8)); }
	inline bool get_mModelTransformEnabled_8() const { return ___mModelTransformEnabled_8; }
	inline bool* get_address_of_mModelTransformEnabled_8() { return &___mModelTransformEnabled_8; }
	inline void set_mModelTransformEnabled_8(bool value)
	{
		___mModelTransformEnabled_8 = value;
	}

	inline static int32_t get_offset_of_mModelTransform_9() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mModelTransform_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mModelTransform_9() const { return ___mModelTransform_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mModelTransform_9() { return &___mModelTransform_9; }
	inline void set_mModelTransform_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mModelTransform_9 = value;
	}

	inline static int32_t get_offset_of_mTrackerStarted_10() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mTrackerStarted_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mTrackerStarted_10() const { return ___mTrackerStarted_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mTrackerStarted_10() { return &___mTrackerStarted_10; }
	inline void set_mTrackerStarted_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mTrackerStarted_10 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerStarted_10), value);
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforePause_11() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mTrackerWasActiveBeforePause_11)); }
	inline bool get_mTrackerWasActiveBeforePause_11() const { return ___mTrackerWasActiveBeforePause_11; }
	inline bool* get_address_of_mTrackerWasActiveBeforePause_11() { return &___mTrackerWasActiveBeforePause_11; }
	inline void set_mTrackerWasActiveBeforePause_11(bool value)
	{
		___mTrackerWasActiveBeforePause_11 = value;
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforeDisabling_12() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mTrackerWasActiveBeforeDisabling_12)); }
	inline bool get_mTrackerWasActiveBeforeDisabling_12() const { return ___mTrackerWasActiveBeforeDisabling_12; }
	inline bool* get_address_of_mTrackerWasActiveBeforeDisabling_12() { return &___mTrackerWasActiveBeforeDisabling_12; }
	inline void set_mTrackerWasActiveBeforeDisabling_12(bool value)
	{
		___mTrackerWasActiveBeforeDisabling_12 = value;
	}

	inline static int32_t get_offset_of_mEmulatorPositionVelocity_13() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mEmulatorPositionVelocity_13)); }
	inline float get_mEmulatorPositionVelocity_13() const { return ___mEmulatorPositionVelocity_13; }
	inline float* get_address_of_mEmulatorPositionVelocity_13() { return &___mEmulatorPositionVelocity_13; }
	inline void set_mEmulatorPositionVelocity_13(float value)
	{
		___mEmulatorPositionVelocity_13 = value;
	}

	inline static int32_t get_offset_of_mDeviceTrackerConfiguration_14() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6, ___mDeviceTrackerConfiguration_14)); }
	inline DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F * get_mDeviceTrackerConfiguration_14() const { return ___mDeviceTrackerConfiguration_14; }
	inline DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F ** get_address_of_mDeviceTrackerConfiguration_14() { return &___mDeviceTrackerConfiguration_14; }
	inline void set_mDeviceTrackerConfiguration_14(DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F * value)
	{
		___mDeviceTrackerConfiguration_14 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceTrackerConfiguration_14), value);
	}
};

struct DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6_StaticFields
{
public:
	// UnityEngine.Vector3 Vuforia.DeviceTrackerARController::DEFAULT_HEAD_PIVOT
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DEFAULT_HEAD_PIVOT_1;
	// UnityEngine.Vector3 Vuforia.DeviceTrackerARController::DEFAULT_HANDHELD_PIVOT
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DEFAULT_HANDHELD_PIVOT_2;
	// Vuforia.DeviceTrackerARController Vuforia.DeviceTrackerARController::mInstance
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6 * ___mInstance_15;
	// System.Object Vuforia.DeviceTrackerARController::mPadlock
	RuntimeObject * ___mPadlock_16;

public:
	inline static int32_t get_offset_of_DEFAULT_HEAD_PIVOT_1() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6_StaticFields, ___DEFAULT_HEAD_PIVOT_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DEFAULT_HEAD_PIVOT_1() const { return ___DEFAULT_HEAD_PIVOT_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DEFAULT_HEAD_PIVOT_1() { return &___DEFAULT_HEAD_PIVOT_1; }
	inline void set_DEFAULT_HEAD_PIVOT_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DEFAULT_HEAD_PIVOT_1 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_HANDHELD_PIVOT_2() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6_StaticFields, ___DEFAULT_HANDHELD_PIVOT_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DEFAULT_HANDHELD_PIVOT_2() const { return ___DEFAULT_HANDHELD_PIVOT_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DEFAULT_HANDHELD_PIVOT_2() { return &___DEFAULT_HANDHELD_PIVOT_2; }
	inline void set_DEFAULT_HANDHELD_PIVOT_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DEFAULT_HANDHELD_PIVOT_2 = value;
	}

	inline static int32_t get_offset_of_mInstance_15() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6_StaticFields, ___mInstance_15)); }
	inline DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6 * get_mInstance_15() const { return ___mInstance_15; }
	inline DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6 ** get_address_of_mInstance_15() { return &___mInstance_15; }
	inline void set_mInstance_15(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6 * value)
	{
		___mInstance_15 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_15), value);
	}

	inline static int32_t get_offset_of_mPadlock_16() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6_StaticFields, ___mPadlock_16)); }
	inline RuntimeObject * get_mPadlock_16() const { return ___mPadlock_16; }
	inline RuntimeObject ** get_address_of_mPadlock_16() { return &___mPadlock_16; }
	inline void set_mPadlock_16(RuntimeObject * value)
	{
		___mPadlock_16 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKERARCONTROLLER_T144EF27DF432AA01FA50AA677B1EBFC37116BBC6_H
#ifndef DEVICETRACKINGMANAGER_T1D264AEA989AA45ED5A85F7FB142F38DB3EE482A_H
#define DEVICETRACKINGMANAGER_T1D264AEA989AA45ED5A85F7FB142F38DB3EE482A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTrackingManager
struct  DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Vuforia.DeviceTrackingManager::mDeviceTrackerPositonOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mDeviceTrackerPositonOffset_0;
	// UnityEngine.Quaternion Vuforia.DeviceTrackingManager::mDeviceTrackerRotationOffset
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___mDeviceTrackerRotationOffset_1;
	// System.Action Vuforia.DeviceTrackingManager::mBeforeDevicePoseUpdated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mBeforeDevicePoseUpdated_2;
	// System.Action Vuforia.DeviceTrackingManager::mAfterDevicePoseUpdated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mAfterDevicePoseUpdated_3;
	// System.Action`1<Vuforia.TrackableBehaviour_Status> Vuforia.DeviceTrackingManager::mStatusChanged
	Action_1_tB2D157B76595F25A0403EA2A0C32F01CE30B4F98 * ___mStatusChanged_4;
	// System.Action`2<Vuforia.TrackableBehaviour_Status,Vuforia.TrackableBehaviour_StatusInfo> Vuforia.DeviceTrackingManager::mStatusOrInfoChanged
	Action_2_t936B0D5682B789A67828A9CF1951CB8FF29DEF13 * ___mStatusOrInfoChanged_5;
	// Vuforia.TrackableBehaviour_Status Vuforia.DeviceTrackingManager::mCurrentTrackableStatus
	int32_t ___mCurrentTrackableStatus_6;
	// Vuforia.TrackableBehaviour_StatusInfo Vuforia.DeviceTrackingManager::mCurrentTrackableStatusInfo
	int32_t ___mCurrentTrackableStatusInfo_7;

public:
	inline static int32_t get_offset_of_mDeviceTrackerPositonOffset_0() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A, ___mDeviceTrackerPositonOffset_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mDeviceTrackerPositonOffset_0() const { return ___mDeviceTrackerPositonOffset_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mDeviceTrackerPositonOffset_0() { return &___mDeviceTrackerPositonOffset_0; }
	inline void set_mDeviceTrackerPositonOffset_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mDeviceTrackerPositonOffset_0 = value;
	}

	inline static int32_t get_offset_of_mDeviceTrackerRotationOffset_1() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A, ___mDeviceTrackerRotationOffset_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_mDeviceTrackerRotationOffset_1() const { return ___mDeviceTrackerRotationOffset_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_mDeviceTrackerRotationOffset_1() { return &___mDeviceTrackerRotationOffset_1; }
	inline void set_mDeviceTrackerRotationOffset_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___mDeviceTrackerRotationOffset_1 = value;
	}

	inline static int32_t get_offset_of_mBeforeDevicePoseUpdated_2() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A, ___mBeforeDevicePoseUpdated_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mBeforeDevicePoseUpdated_2() const { return ___mBeforeDevicePoseUpdated_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mBeforeDevicePoseUpdated_2() { return &___mBeforeDevicePoseUpdated_2; }
	inline void set_mBeforeDevicePoseUpdated_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mBeforeDevicePoseUpdated_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBeforeDevicePoseUpdated_2), value);
	}

	inline static int32_t get_offset_of_mAfterDevicePoseUpdated_3() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A, ___mAfterDevicePoseUpdated_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mAfterDevicePoseUpdated_3() const { return ___mAfterDevicePoseUpdated_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mAfterDevicePoseUpdated_3() { return &___mAfterDevicePoseUpdated_3; }
	inline void set_mAfterDevicePoseUpdated_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mAfterDevicePoseUpdated_3 = value;
		Il2CppCodeGenWriteBarrier((&___mAfterDevicePoseUpdated_3), value);
	}

	inline static int32_t get_offset_of_mStatusChanged_4() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A, ___mStatusChanged_4)); }
	inline Action_1_tB2D157B76595F25A0403EA2A0C32F01CE30B4F98 * get_mStatusChanged_4() const { return ___mStatusChanged_4; }
	inline Action_1_tB2D157B76595F25A0403EA2A0C32F01CE30B4F98 ** get_address_of_mStatusChanged_4() { return &___mStatusChanged_4; }
	inline void set_mStatusChanged_4(Action_1_tB2D157B76595F25A0403EA2A0C32F01CE30B4F98 * value)
	{
		___mStatusChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___mStatusChanged_4), value);
	}

	inline static int32_t get_offset_of_mStatusOrInfoChanged_5() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A, ___mStatusOrInfoChanged_5)); }
	inline Action_2_t936B0D5682B789A67828A9CF1951CB8FF29DEF13 * get_mStatusOrInfoChanged_5() const { return ___mStatusOrInfoChanged_5; }
	inline Action_2_t936B0D5682B789A67828A9CF1951CB8FF29DEF13 ** get_address_of_mStatusOrInfoChanged_5() { return &___mStatusOrInfoChanged_5; }
	inline void set_mStatusOrInfoChanged_5(Action_2_t936B0D5682B789A67828A9CF1951CB8FF29DEF13 * value)
	{
		___mStatusOrInfoChanged_5 = value;
		Il2CppCodeGenWriteBarrier((&___mStatusOrInfoChanged_5), value);
	}

	inline static int32_t get_offset_of_mCurrentTrackableStatus_6() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A, ___mCurrentTrackableStatus_6)); }
	inline int32_t get_mCurrentTrackableStatus_6() const { return ___mCurrentTrackableStatus_6; }
	inline int32_t* get_address_of_mCurrentTrackableStatus_6() { return &___mCurrentTrackableStatus_6; }
	inline void set_mCurrentTrackableStatus_6(int32_t value)
	{
		___mCurrentTrackableStatus_6 = value;
	}

	inline static int32_t get_offset_of_mCurrentTrackableStatusInfo_7() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A, ___mCurrentTrackableStatusInfo_7)); }
	inline int32_t get_mCurrentTrackableStatusInfo_7() const { return ___mCurrentTrackableStatusInfo_7; }
	inline int32_t* get_address_of_mCurrentTrackableStatusInfo_7() { return &___mCurrentTrackableStatusInfo_7; }
	inline void set_mCurrentTrackableStatusInfo_7(int32_t value)
	{
		___mCurrentTrackableStatusInfo_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKINGMANAGER_T1D264AEA989AA45ED5A85F7FB142F38DB3EE482A_H
#ifndef DIGITALEYEWEARARCONTROLLER_T973FDCC2DBCE328656150191FBC1A0E49189D9E4_H
#define DIGITALEYEWEARARCONTROLLER_T973FDCC2DBCE328656150191FBC1A0E49189D9E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController
struct  DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4  : public ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293
{
public:
	// System.Single Vuforia.DigitalEyewearARController::mCameraOffset
	float ___mCameraOffset_5;
	// System.Int32 Vuforia.DigitalEyewearARController::mDistortionRenderingLayer
	int32_t ___mDistortionRenderingLayer_6;
	// Vuforia.DigitalEyewearARController_EyewearType Vuforia.DigitalEyewearARController::mEyewearType
	int32_t ___mEyewearType_7;
	// Vuforia.DigitalEyewearARController_StereoFramework Vuforia.DigitalEyewearARController::mStereoFramework
	int32_t ___mStereoFramework_8;
	// Vuforia.DigitalEyewearARController_SeeThroughConfiguration Vuforia.DigitalEyewearARController::mSeeThroughConfiguration
	int32_t ___mSeeThroughConfiguration_9;
	// System.String Vuforia.DigitalEyewearARController::mViewerName
	String_t* ___mViewerName_10;
	// System.String Vuforia.DigitalEyewearARController::mViewerManufacturer
	String_t* ___mViewerManufacturer_11;
	// System.Boolean Vuforia.DigitalEyewearARController::mUseCustomViewer
	bool ___mUseCustomViewer_12;
	// Vuforia.DigitalEyewearARController_SerializableViewerParameters Vuforia.DigitalEyewearARController::mCustomViewer
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9 * ___mCustomViewer_13;
	// UnityEngine.Transform Vuforia.DigitalEyewearARController::mCentralAnchorPoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mCentralAnchorPoint_14;
	// UnityEngine.Camera Vuforia.DigitalEyewearARController::mPrimaryCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mPrimaryCamera_15;
	// Vuforia.VuforiaARController Vuforia.DigitalEyewearARController::mVuforiaBehaviour
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * ___mVuforiaBehaviour_16;
	// System.Boolean Vuforia.DigitalEyewearARController::mSetFocusPlaneAutomatically
	bool ___mSetFocusPlaneAutomatically_17;
	// Vuforia.VRDeviceController Vuforia.DigitalEyewearARController::mVRDeviceController
	VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044 * ___mVRDeviceController_18;

public:
	inline static int32_t get_offset_of_mCameraOffset_5() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mCameraOffset_5)); }
	inline float get_mCameraOffset_5() const { return ___mCameraOffset_5; }
	inline float* get_address_of_mCameraOffset_5() { return &___mCameraOffset_5; }
	inline void set_mCameraOffset_5(float value)
	{
		___mCameraOffset_5 = value;
	}

	inline static int32_t get_offset_of_mDistortionRenderingLayer_6() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mDistortionRenderingLayer_6)); }
	inline int32_t get_mDistortionRenderingLayer_6() const { return ___mDistortionRenderingLayer_6; }
	inline int32_t* get_address_of_mDistortionRenderingLayer_6() { return &___mDistortionRenderingLayer_6; }
	inline void set_mDistortionRenderingLayer_6(int32_t value)
	{
		___mDistortionRenderingLayer_6 = value;
	}

	inline static int32_t get_offset_of_mEyewearType_7() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mEyewearType_7)); }
	inline int32_t get_mEyewearType_7() const { return ___mEyewearType_7; }
	inline int32_t* get_address_of_mEyewearType_7() { return &___mEyewearType_7; }
	inline void set_mEyewearType_7(int32_t value)
	{
		___mEyewearType_7 = value;
	}

	inline static int32_t get_offset_of_mStereoFramework_8() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mStereoFramework_8)); }
	inline int32_t get_mStereoFramework_8() const { return ___mStereoFramework_8; }
	inline int32_t* get_address_of_mStereoFramework_8() { return &___mStereoFramework_8; }
	inline void set_mStereoFramework_8(int32_t value)
	{
		___mStereoFramework_8 = value;
	}

	inline static int32_t get_offset_of_mSeeThroughConfiguration_9() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mSeeThroughConfiguration_9)); }
	inline int32_t get_mSeeThroughConfiguration_9() const { return ___mSeeThroughConfiguration_9; }
	inline int32_t* get_address_of_mSeeThroughConfiguration_9() { return &___mSeeThroughConfiguration_9; }
	inline void set_mSeeThroughConfiguration_9(int32_t value)
	{
		___mSeeThroughConfiguration_9 = value;
	}

	inline static int32_t get_offset_of_mViewerName_10() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mViewerName_10)); }
	inline String_t* get_mViewerName_10() const { return ___mViewerName_10; }
	inline String_t** get_address_of_mViewerName_10() { return &___mViewerName_10; }
	inline void set_mViewerName_10(String_t* value)
	{
		___mViewerName_10 = value;
		Il2CppCodeGenWriteBarrier((&___mViewerName_10), value);
	}

	inline static int32_t get_offset_of_mViewerManufacturer_11() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mViewerManufacturer_11)); }
	inline String_t* get_mViewerManufacturer_11() const { return ___mViewerManufacturer_11; }
	inline String_t** get_address_of_mViewerManufacturer_11() { return &___mViewerManufacturer_11; }
	inline void set_mViewerManufacturer_11(String_t* value)
	{
		___mViewerManufacturer_11 = value;
		Il2CppCodeGenWriteBarrier((&___mViewerManufacturer_11), value);
	}

	inline static int32_t get_offset_of_mUseCustomViewer_12() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mUseCustomViewer_12)); }
	inline bool get_mUseCustomViewer_12() const { return ___mUseCustomViewer_12; }
	inline bool* get_address_of_mUseCustomViewer_12() { return &___mUseCustomViewer_12; }
	inline void set_mUseCustomViewer_12(bool value)
	{
		___mUseCustomViewer_12 = value;
	}

	inline static int32_t get_offset_of_mCustomViewer_13() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mCustomViewer_13)); }
	inline SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9 * get_mCustomViewer_13() const { return ___mCustomViewer_13; }
	inline SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9 ** get_address_of_mCustomViewer_13() { return &___mCustomViewer_13; }
	inline void set_mCustomViewer_13(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9 * value)
	{
		___mCustomViewer_13 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomViewer_13), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_14() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mCentralAnchorPoint_14)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mCentralAnchorPoint_14() const { return ___mCentralAnchorPoint_14; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mCentralAnchorPoint_14() { return &___mCentralAnchorPoint_14; }
	inline void set_mCentralAnchorPoint_14(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mCentralAnchorPoint_14 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_14), value);
	}

	inline static int32_t get_offset_of_mPrimaryCamera_15() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mPrimaryCamera_15)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mPrimaryCamera_15() const { return ___mPrimaryCamera_15; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mPrimaryCamera_15() { return &___mPrimaryCamera_15; }
	inline void set_mPrimaryCamera_15(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mPrimaryCamera_15 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_15), value);
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_16() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mVuforiaBehaviour_16)); }
	inline VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * get_mVuforiaBehaviour_16() const { return ___mVuforiaBehaviour_16; }
	inline VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 ** get_address_of_mVuforiaBehaviour_16() { return &___mVuforiaBehaviour_16; }
	inline void set_mVuforiaBehaviour_16(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * value)
	{
		___mVuforiaBehaviour_16 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_16), value);
	}

	inline static int32_t get_offset_of_mSetFocusPlaneAutomatically_17() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mSetFocusPlaneAutomatically_17)); }
	inline bool get_mSetFocusPlaneAutomatically_17() const { return ___mSetFocusPlaneAutomatically_17; }
	inline bool* get_address_of_mSetFocusPlaneAutomatically_17() { return &___mSetFocusPlaneAutomatically_17; }
	inline void set_mSetFocusPlaneAutomatically_17(bool value)
	{
		___mSetFocusPlaneAutomatically_17 = value;
	}

	inline static int32_t get_offset_of_mVRDeviceController_18() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4, ___mVRDeviceController_18)); }
	inline VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044 * get_mVRDeviceController_18() const { return ___mVRDeviceController_18; }
	inline VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044 ** get_address_of_mVRDeviceController_18() { return &___mVRDeviceController_18; }
	inline void set_mVRDeviceController_18(VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044 * value)
	{
		___mVRDeviceController_18 = value;
		Il2CppCodeGenWriteBarrier((&___mVRDeviceController_18), value);
	}
};

struct DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4_StaticFields
{
public:
	// Vuforia.DigitalEyewearARController Vuforia.DigitalEyewearARController::mInstance
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 * ___mInstance_19;
	// System.Object Vuforia.DigitalEyewearARController::mPadlock
	RuntimeObject * ___mPadlock_20;

public:
	inline static int32_t get_offset_of_mInstance_19() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4_StaticFields, ___mInstance_19)); }
	inline DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 * get_mInstance_19() const { return ___mInstance_19; }
	inline DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 ** get_address_of_mInstance_19() { return &___mInstance_19; }
	inline void set_mInstance_19(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 * value)
	{
		___mInstance_19 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_19), value);
	}

	inline static int32_t get_offset_of_mPadlock_20() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4_StaticFields, ___mPadlock_20)); }
	inline RuntimeObject * get_mPadlock_20() const { return ___mPadlock_20; }
	inline RuntimeObject ** get_address_of_mPadlock_20() { return &___mPadlock_20; }
	inline void set_mPadlock_20(RuntimeObject * value)
	{
		___mPadlock_20 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALEYEWEARARCONTROLLER_T973FDCC2DBCE328656150191FBC1A0E49189D9E4_H
#ifndef SERIALIZABLEVIEWERPARAMETERS_T5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9_H
#define SERIALIZABLEVIEWERPARAMETERS_T5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController_SerializableViewerParameters
struct  SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9  : public RuntimeObject
{
public:
	// System.Single Vuforia.DigitalEyewearARController_SerializableViewerParameters::Version
	float ___Version_0;
	// System.String Vuforia.DigitalEyewearARController_SerializableViewerParameters::Name
	String_t* ___Name_1;
	// System.String Vuforia.DigitalEyewearARController_SerializableViewerParameters::Manufacturer
	String_t* ___Manufacturer_2;
	// Vuforia.ViewerButtonType Vuforia.DigitalEyewearARController_SerializableViewerParameters::ButtonType
	int32_t ___ButtonType_3;
	// System.Single Vuforia.DigitalEyewearARController_SerializableViewerParameters::ScreenToLensDistance
	float ___ScreenToLensDistance_4;
	// System.Single Vuforia.DigitalEyewearARController_SerializableViewerParameters::InterLensDistance
	float ___InterLensDistance_5;
	// Vuforia.ViewerTrayAlignment Vuforia.DigitalEyewearARController_SerializableViewerParameters::TrayAlignment
	int32_t ___TrayAlignment_6;
	// System.Single Vuforia.DigitalEyewearARController_SerializableViewerParameters::LensCenterToTrayDistance
	float ___LensCenterToTrayDistance_7;
	// UnityEngine.Vector2 Vuforia.DigitalEyewearARController_SerializableViewerParameters::DistortionCoefficients
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___DistortionCoefficients_8;
	// UnityEngine.Vector4 Vuforia.DigitalEyewearARController_SerializableViewerParameters::FieldOfView
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___FieldOfView_9;
	// System.Boolean Vuforia.DigitalEyewearARController_SerializableViewerParameters::ContainsMagnet
	bool ___ContainsMagnet_10;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___Version_0)); }
	inline float get_Version_0() const { return ___Version_0; }
	inline float* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(float value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_Manufacturer_2() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___Manufacturer_2)); }
	inline String_t* get_Manufacturer_2() const { return ___Manufacturer_2; }
	inline String_t** get_address_of_Manufacturer_2() { return &___Manufacturer_2; }
	inline void set_Manufacturer_2(String_t* value)
	{
		___Manufacturer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Manufacturer_2), value);
	}

	inline static int32_t get_offset_of_ButtonType_3() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___ButtonType_3)); }
	inline int32_t get_ButtonType_3() const { return ___ButtonType_3; }
	inline int32_t* get_address_of_ButtonType_3() { return &___ButtonType_3; }
	inline void set_ButtonType_3(int32_t value)
	{
		___ButtonType_3 = value;
	}

	inline static int32_t get_offset_of_ScreenToLensDistance_4() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___ScreenToLensDistance_4)); }
	inline float get_ScreenToLensDistance_4() const { return ___ScreenToLensDistance_4; }
	inline float* get_address_of_ScreenToLensDistance_4() { return &___ScreenToLensDistance_4; }
	inline void set_ScreenToLensDistance_4(float value)
	{
		___ScreenToLensDistance_4 = value;
	}

	inline static int32_t get_offset_of_InterLensDistance_5() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___InterLensDistance_5)); }
	inline float get_InterLensDistance_5() const { return ___InterLensDistance_5; }
	inline float* get_address_of_InterLensDistance_5() { return &___InterLensDistance_5; }
	inline void set_InterLensDistance_5(float value)
	{
		___InterLensDistance_5 = value;
	}

	inline static int32_t get_offset_of_TrayAlignment_6() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___TrayAlignment_6)); }
	inline int32_t get_TrayAlignment_6() const { return ___TrayAlignment_6; }
	inline int32_t* get_address_of_TrayAlignment_6() { return &___TrayAlignment_6; }
	inline void set_TrayAlignment_6(int32_t value)
	{
		___TrayAlignment_6 = value;
	}

	inline static int32_t get_offset_of_LensCenterToTrayDistance_7() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___LensCenterToTrayDistance_7)); }
	inline float get_LensCenterToTrayDistance_7() const { return ___LensCenterToTrayDistance_7; }
	inline float* get_address_of_LensCenterToTrayDistance_7() { return &___LensCenterToTrayDistance_7; }
	inline void set_LensCenterToTrayDistance_7(float value)
	{
		___LensCenterToTrayDistance_7 = value;
	}

	inline static int32_t get_offset_of_DistortionCoefficients_8() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___DistortionCoefficients_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_DistortionCoefficients_8() const { return ___DistortionCoefficients_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_DistortionCoefficients_8() { return &___DistortionCoefficients_8; }
	inline void set_DistortionCoefficients_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___DistortionCoefficients_8 = value;
	}

	inline static int32_t get_offset_of_FieldOfView_9() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___FieldOfView_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_FieldOfView_9() const { return ___FieldOfView_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_FieldOfView_9() { return &___FieldOfView_9; }
	inline void set_FieldOfView_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___FieldOfView_9 = value;
	}

	inline static int32_t get_offset_of_ContainsMagnet_10() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9, ___ContainsMagnet_10)); }
	inline bool get_ContainsMagnet_10() const { return ___ContainsMagnet_10; }
	inline bool* get_address_of_ContainsMagnet_10() { return &___ContainsMagnet_10; }
	inline void set_ContainsMagnet_10(bool value)
	{
		___ContainsMagnet_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVIEWERPARAMETERS_T5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9_H
#ifndef IMAGEDESCRIPTION_T8C30B261E58382BB9D3D3399113CB0478EA2E307_H
#define IMAGEDESCRIPTION_T8C30B261E58382BB9D3D3399113CB0478EA2E307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageDescription
struct  ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307  : public RuntimeObject
{
public:
	// System.Int32 Vuforia.ImageDescription::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_0;
	// System.Int32 Vuforia.ImageDescription::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_1;
	// System.Int32 Vuforia.ImageDescription::<Stride>k__BackingField
	int32_t ___U3CStrideU3Ek__BackingField_2;
	// System.Int32 Vuforia.ImageDescription::<BufferWidth>k__BackingField
	int32_t ___U3CBufferWidthU3Ek__BackingField_3;
	// System.Int32 Vuforia.ImageDescription::<BufferHeight>k__BackingField
	int32_t ___U3CBufferHeightU3Ek__BackingField_4;
	// Vuforia.PIXEL_FORMAT Vuforia.ImageDescription::<PixelFormat>k__BackingField
	int32_t ___U3CPixelFormatU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307, ___U3CWidthU3Ek__BackingField_0)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_0() const { return ___U3CWidthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_0() { return &___U3CWidthU3Ek__BackingField_0; }
	inline void set_U3CWidthU3Ek__BackingField_0(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307, ___U3CHeightU3Ek__BackingField_1)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_1() const { return ___U3CHeightU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_1() { return &___U3CHeightU3Ek__BackingField_1; }
	inline void set_U3CHeightU3Ek__BackingField_1(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CStrideU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307, ___U3CStrideU3Ek__BackingField_2)); }
	inline int32_t get_U3CStrideU3Ek__BackingField_2() const { return ___U3CStrideU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CStrideU3Ek__BackingField_2() { return &___U3CStrideU3Ek__BackingField_2; }
	inline void set_U3CStrideU3Ek__BackingField_2(int32_t value)
	{
		___U3CStrideU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CBufferWidthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307, ___U3CBufferWidthU3Ek__BackingField_3)); }
	inline int32_t get_U3CBufferWidthU3Ek__BackingField_3() const { return ___U3CBufferWidthU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CBufferWidthU3Ek__BackingField_3() { return &___U3CBufferWidthU3Ek__BackingField_3; }
	inline void set_U3CBufferWidthU3Ek__BackingField_3(int32_t value)
	{
		___U3CBufferWidthU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CBufferHeightU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307, ___U3CBufferHeightU3Ek__BackingField_4)); }
	inline int32_t get_U3CBufferHeightU3Ek__BackingField_4() const { return ___U3CBufferHeightU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CBufferHeightU3Ek__BackingField_4() { return &___U3CBufferHeightU3Ek__BackingField_4; }
	inline void set_U3CBufferHeightU3Ek__BackingField_4(int32_t value)
	{
		___U3CBufferHeightU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CPixelFormatU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307, ___U3CPixelFormatU3Ek__BackingField_5)); }
	inline int32_t get_U3CPixelFormatU3Ek__BackingField_5() const { return ___U3CPixelFormatU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CPixelFormatU3Ek__BackingField_5() { return &___U3CPixelFormatU3Ek__BackingField_5; }
	inline void set_U3CPixelFormatU3Ek__BackingField_5(int32_t value)
	{
		___U3CPixelFormatU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEDESCRIPTION_T8C30B261E58382BB9D3D3399113CB0478EA2E307_H
#ifndef INSTANCEIDIMPL_TB528D9E690D089FBF42027CEF1983B298A9C9371_H
#define INSTANCEIDIMPL_TB528D9E690D089FBF42027CEF1983B298A9C9371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.InstanceIdImpl
struct  InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371  : public RuntimeObject
{
public:
	// Vuforia.InstanceIdType Vuforia.InstanceIdImpl::mDataType
	int32_t ___mDataType_0;
	// System.Byte[] Vuforia.InstanceIdImpl::mBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mBuffer_1;
	// System.UInt64 Vuforia.InstanceIdImpl::mNumericValue
	uint64_t ___mNumericValue_2;
	// System.UInt32 Vuforia.InstanceIdImpl::mDataLength
	uint32_t ___mDataLength_3;
	// System.String Vuforia.InstanceIdImpl::mCachedStringValue
	String_t* ___mCachedStringValue_4;

public:
	inline static int32_t get_offset_of_mDataType_0() { return static_cast<int32_t>(offsetof(InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371, ___mDataType_0)); }
	inline int32_t get_mDataType_0() const { return ___mDataType_0; }
	inline int32_t* get_address_of_mDataType_0() { return &___mDataType_0; }
	inline void set_mDataType_0(int32_t value)
	{
		___mDataType_0 = value;
	}

	inline static int32_t get_offset_of_mBuffer_1() { return static_cast<int32_t>(offsetof(InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371, ___mBuffer_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mBuffer_1() const { return ___mBuffer_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mBuffer_1() { return &___mBuffer_1; }
	inline void set_mBuffer_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___mBuffer_1), value);
	}

	inline static int32_t get_offset_of_mNumericValue_2() { return static_cast<int32_t>(offsetof(InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371, ___mNumericValue_2)); }
	inline uint64_t get_mNumericValue_2() const { return ___mNumericValue_2; }
	inline uint64_t* get_address_of_mNumericValue_2() { return &___mNumericValue_2; }
	inline void set_mNumericValue_2(uint64_t value)
	{
		___mNumericValue_2 = value;
	}

	inline static int32_t get_offset_of_mDataLength_3() { return static_cast<int32_t>(offsetof(InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371, ___mDataLength_3)); }
	inline uint32_t get_mDataLength_3() const { return ___mDataLength_3; }
	inline uint32_t* get_address_of_mDataLength_3() { return &___mDataLength_3; }
	inline void set_mDataLength_3(uint32_t value)
	{
		___mDataLength_3 = value;
	}

	inline static int32_t get_offset_of_mCachedStringValue_4() { return static_cast<int32_t>(offsetof(InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371, ___mCachedStringValue_4)); }
	inline String_t* get_mCachedStringValue_4() const { return ___mCachedStringValue_4; }
	inline String_t** get_address_of_mCachedStringValue_4() { return &___mCachedStringValue_4; }
	inline void set_mCachedStringValue_4(String_t* value)
	{
		___mCachedStringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCachedStringValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDIMPL_TB528D9E690D089FBF42027CEF1983B298A9C9371_H
#ifndef MODELTARGETIMPL_TD484936FF7A7ED1644951F50A18D4574B095480E_H
#define MODELTARGETIMPL_TD484936FF7A7ED1644951F50A18D4574B095480E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ModelTargetImpl
struct  ModelTargetImpl_tD484936FF7A7ED1644951F50A18D4574B095480E  : public DataSetObjectTargetImpl_tC2D896B84A1FDE730F7E2F20045D12CBC6F30C98
{
public:
	// Vuforia.IBoundingBox Vuforia.ModelTargetImpl::mBoundingBoxImpl
	RuntimeObject* ___mBoundingBoxImpl_4;
	// Vuforia.GuideViewContainer Vuforia.ModelTargetImpl::mGuideViewContainer
	GuideViewContainer_t763A0014C14D0068439C96239D87117C86633E0D * ___mGuideViewContainer_5;

public:
	inline static int32_t get_offset_of_mBoundingBoxImpl_4() { return static_cast<int32_t>(offsetof(ModelTargetImpl_tD484936FF7A7ED1644951F50A18D4574B095480E, ___mBoundingBoxImpl_4)); }
	inline RuntimeObject* get_mBoundingBoxImpl_4() const { return ___mBoundingBoxImpl_4; }
	inline RuntimeObject** get_address_of_mBoundingBoxImpl_4() { return &___mBoundingBoxImpl_4; }
	inline void set_mBoundingBoxImpl_4(RuntimeObject* value)
	{
		___mBoundingBoxImpl_4 = value;
		Il2CppCodeGenWriteBarrier((&___mBoundingBoxImpl_4), value);
	}

	inline static int32_t get_offset_of_mGuideViewContainer_5() { return static_cast<int32_t>(offsetof(ModelTargetImpl_tD484936FF7A7ED1644951F50A18D4574B095480E, ___mGuideViewContainer_5)); }
	inline GuideViewContainer_t763A0014C14D0068439C96239D87117C86633E0D * get_mGuideViewContainer_5() const { return ___mGuideViewContainer_5; }
	inline GuideViewContainer_t763A0014C14D0068439C96239D87117C86633E0D ** get_address_of_mGuideViewContainer_5() { return &___mGuideViewContainer_5; }
	inline void set_mGuideViewContainer_5(GuideViewContainer_t763A0014C14D0068439C96239D87117C86633E0D * value)
	{
		___mGuideViewContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___mGuideViewContainer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELTARGETIMPL_TD484936FF7A7ED1644951F50A18D4574B095480E_H
#ifndef SMARTTERRAINARCONTROLLER_T71E6FA089F9760C9BA027E3808EE43568BF0BD3D_H
#define SMARTTERRAINARCONTROLLER_T71E6FA089F9760C9BA027E3808EE43568BF0BD3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainARController
struct  SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D  : public ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293
{
public:
	// System.Boolean Vuforia.SmartTerrainARController::mAutoInitAndStartTracker
	bool ___mAutoInitAndStartTracker_2;
	// Vuforia.SmartTerrainARController_State Vuforia.SmartTerrainARController::mState
	int32_t ___mState_3;

public:
	inline static int32_t get_offset_of_mAutoInitAndStartTracker_2() { return static_cast<int32_t>(offsetof(SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D, ___mAutoInitAndStartTracker_2)); }
	inline bool get_mAutoInitAndStartTracker_2() const { return ___mAutoInitAndStartTracker_2; }
	inline bool* get_address_of_mAutoInitAndStartTracker_2() { return &___mAutoInitAndStartTracker_2; }
	inline void set_mAutoInitAndStartTracker_2(bool value)
	{
		___mAutoInitAndStartTracker_2 = value;
	}

	inline static int32_t get_offset_of_mState_3() { return static_cast<int32_t>(offsetof(SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D, ___mState_3)); }
	inline int32_t get_mState_3() const { return ___mState_3; }
	inline int32_t* get_address_of_mState_3() { return &___mState_3; }
	inline void set_mState_3(int32_t value)
	{
		___mState_3 = value;
	}
};

struct SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D_StaticFields
{
public:
	// Vuforia.SmartTerrainARController Vuforia.SmartTerrainARController::mInstance
	SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D * ___mInstance_1;

public:
	inline static int32_t get_offset_of_mInstance_1() { return static_cast<int32_t>(offsetof(SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D_StaticFields, ___mInstance_1)); }
	inline SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D * get_mInstance_1() const { return ___mInstance_1; }
	inline SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D ** get_address_of_mInstance_1() { return &___mInstance_1; }
	inline void set_mInstance_1(SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D * value)
	{
		___mInstance_1 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINARCONTROLLER_T71E6FA089F9760C9BA027E3808EE43568BF0BD3D_H
#ifndef TRACKABLERESULTDATA_T8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA_H
#define TRACKABLERESULTDATA_T8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_TrackableResultData
#pragma pack(push, tp, 1)
struct  TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA 
{
public:
	// Vuforia.TrackerData_PoseData Vuforia.TrackerData_TrackableResultData::pose
	PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B  ___pose_0;
	// System.Double Vuforia.TrackerData_TrackableResultData::timeStamp
	double ___timeStamp_1;
	// System.Int32 Vuforia.TrackerData_TrackableResultData::statusInteger
	int32_t ___statusInteger_2;
	// System.Int32 Vuforia.TrackerData_TrackableResultData::statusInfo
	int32_t ___statusInfo_3;
	// System.Int32 Vuforia.TrackerData_TrackableResultData::id
	int32_t ___id_4;
	// System.Int32 Vuforia.TrackerData_TrackableResultData::unused
	int32_t ___unused_5;

public:
	inline static int32_t get_offset_of_pose_0() { return static_cast<int32_t>(offsetof(TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA, ___pose_0)); }
	inline PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B  get_pose_0() const { return ___pose_0; }
	inline PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B * get_address_of_pose_0() { return &___pose_0; }
	inline void set_pose_0(PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B  value)
	{
		___pose_0 = value;
	}

	inline static int32_t get_offset_of_timeStamp_1() { return static_cast<int32_t>(offsetof(TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA, ___timeStamp_1)); }
	inline double get_timeStamp_1() const { return ___timeStamp_1; }
	inline double* get_address_of_timeStamp_1() { return &___timeStamp_1; }
	inline void set_timeStamp_1(double value)
	{
		___timeStamp_1 = value;
	}

	inline static int32_t get_offset_of_statusInteger_2() { return static_cast<int32_t>(offsetof(TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA, ___statusInteger_2)); }
	inline int32_t get_statusInteger_2() const { return ___statusInteger_2; }
	inline int32_t* get_address_of_statusInteger_2() { return &___statusInteger_2; }
	inline void set_statusInteger_2(int32_t value)
	{
		___statusInteger_2 = value;
	}

	inline static int32_t get_offset_of_statusInfo_3() { return static_cast<int32_t>(offsetof(TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA, ___statusInfo_3)); }
	inline int32_t get_statusInfo_3() const { return ___statusInfo_3; }
	inline int32_t* get_address_of_statusInfo_3() { return &___statusInfo_3; }
	inline void set_statusInfo_3(int32_t value)
	{
		___statusInfo_3 = value;
	}

	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA, ___id_4)); }
	inline int32_t get_id_4() const { return ___id_4; }
	inline int32_t* get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(int32_t value)
	{
		___id_4 = value;
	}

	inline static int32_t get_offset_of_unused_5() { return static_cast<int32_t>(offsetof(TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA, ___unused_5)); }
	inline int32_t get_unused_5() const { return ___unused_5; }
	inline int32_t* get_address_of_unused_5() { return &___unused_5; }
	inline void set_unused_5(int32_t value)
	{
		___unused_5 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLERESULTDATA_T8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA_H
#ifndef VUMARKTARGETDATA_T634A3794662DF21263248A3F7E1A1E77E3085F81_H
#define VUMARKTARGETDATA_T634A3794662DF21263248A3F7E1A1E77E3085F81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_VuMarkTargetData
#pragma pack(push, tp, 1)
struct  VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81 
{
public:
	// Vuforia.TrackerData_InstanceIdData Vuforia.TrackerData_VuMarkTargetData::instanceId
	InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2  ___instanceId_0;
	// System.Int32 Vuforia.TrackerData_VuMarkTargetData::id
	int32_t ___id_1;
	// System.Int32 Vuforia.TrackerData_VuMarkTargetData::templateId
	int32_t ___templateId_2;
	// UnityEngine.Vector3 Vuforia.TrackerData_VuMarkTargetData::size
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___size_3;
	// System.Int32 Vuforia.TrackerData_VuMarkTargetData::unused
	int32_t ___unused_4;

public:
	inline static int32_t get_offset_of_instanceId_0() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81, ___instanceId_0)); }
	inline InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2  get_instanceId_0() const { return ___instanceId_0; }
	inline InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2 * get_address_of_instanceId_0() { return &___instanceId_0; }
	inline void set_instanceId_0(InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2  value)
	{
		___instanceId_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_templateId_2() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81, ___templateId_2)); }
	inline int32_t get_templateId_2() const { return ___templateId_2; }
	inline int32_t* get_address_of_templateId_2() { return &___templateId_2; }
	inline void set_templateId_2(int32_t value)
	{
		___templateId_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81, ___size_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_size_3() const { return ___size_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_unused_4() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81, ___unused_4)); }
	inline int32_t get_unused_4() const { return ___unused_4; }
	inline int32_t* get_address_of_unused_4() { return &___unused_4; }
	inline void set_unused_4(int32_t value)
	{
		___unused_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTARGETDATA_T634A3794662DF21263248A3F7E1A1E77E3085F81_H
#ifndef VUMARKTARGETRESULTDATA_TD7EC910F62A1C9C07A914277BA322562E242B8FE_H
#define VUMARKTARGETRESULTDATA_TD7EC910F62A1C9C07A914277BA322562E242B8FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_VuMarkTargetResultData
#pragma pack(push, tp, 1)
struct  VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE 
{
public:
	// Vuforia.TrackerData_PoseData Vuforia.TrackerData_VuMarkTargetResultData::pose
	PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B  ___pose_0;
	// System.Double Vuforia.TrackerData_VuMarkTargetResultData::timeStamp
	double ___timeStamp_1;
	// System.Int32 Vuforia.TrackerData_VuMarkTargetResultData::statusInteger
	int32_t ___statusInteger_2;
	// System.Int32 Vuforia.TrackerData_VuMarkTargetResultData::targetID
	int32_t ___targetID_3;
	// System.Int32 Vuforia.TrackerData_VuMarkTargetResultData::resultID
	int32_t ___resultID_4;
	// System.Int32 Vuforia.TrackerData_VuMarkTargetResultData::unused
	int32_t ___unused_5;

public:
	inline static int32_t get_offset_of_pose_0() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE, ___pose_0)); }
	inline PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B  get_pose_0() const { return ___pose_0; }
	inline PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B * get_address_of_pose_0() { return &___pose_0; }
	inline void set_pose_0(PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B  value)
	{
		___pose_0 = value;
	}

	inline static int32_t get_offset_of_timeStamp_1() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE, ___timeStamp_1)); }
	inline double get_timeStamp_1() const { return ___timeStamp_1; }
	inline double* get_address_of_timeStamp_1() { return &___timeStamp_1; }
	inline void set_timeStamp_1(double value)
	{
		___timeStamp_1 = value;
	}

	inline static int32_t get_offset_of_statusInteger_2() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE, ___statusInteger_2)); }
	inline int32_t get_statusInteger_2() const { return ___statusInteger_2; }
	inline int32_t* get_address_of_statusInteger_2() { return &___statusInteger_2; }
	inline void set_statusInteger_2(int32_t value)
	{
		___statusInteger_2 = value;
	}

	inline static int32_t get_offset_of_targetID_3() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE, ___targetID_3)); }
	inline int32_t get_targetID_3() const { return ___targetID_3; }
	inline int32_t* get_address_of_targetID_3() { return &___targetID_3; }
	inline void set_targetID_3(int32_t value)
	{
		___targetID_3 = value;
	}

	inline static int32_t get_offset_of_resultID_4() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE, ___resultID_4)); }
	inline int32_t get_resultID_4() const { return ___resultID_4; }
	inline int32_t* get_address_of_resultID_4() { return &___resultID_4; }
	inline void set_resultID_4(int32_t value)
	{
		___resultID_4 = value;
	}

	inline static int32_t get_offset_of_unused_5() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE, ___unused_5)); }
	inline int32_t get_unused_5() const { return ___unused_5; }
	inline int32_t* get_address_of_unused_5() { return &___unused_5; }
	inline void set_unused_5(int32_t value)
	{
		___unused_5 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTARGETRESULTDATA_TD7EC910F62A1C9C07A914277BA322562E242B8FE_H
#ifndef VUMARKSETTARGETSIZE_TA607278FCA4215B58CB8D03F1698A1A75B52E321_H
#define VUMARKSETTARGETSIZE_TA607278FCA4215B58CB8D03F1698A1A75B52E321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkTargetImpl_VuMarkSetTargetSize
struct  VuMarkSetTargetSize_tA607278FCA4215B58CB8D03F1698A1A75B52E321  : public DisabledSetTargetSize_tED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA
{
public:
	// Vuforia.VuMarkTemplate Vuforia.VuMarkTargetImpl_VuMarkSetTargetSize::mTemplate
	RuntimeObject* ___mTemplate_1;

public:
	inline static int32_t get_offset_of_mTemplate_1() { return static_cast<int32_t>(offsetof(VuMarkSetTargetSize_tA607278FCA4215B58CB8D03F1698A1A75B52E321, ___mTemplate_1)); }
	inline RuntimeObject* get_mTemplate_1() const { return ___mTemplate_1; }
	inline RuntimeObject** get_address_of_mTemplate_1() { return &___mTemplate_1; }
	inline void set_mTemplate_1(RuntimeObject* value)
	{
		___mTemplate_1 = value;
		Il2CppCodeGenWriteBarrier((&___mTemplate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKSETTARGETSIZE_TA607278FCA4215B58CB8D03F1698A1A75B52E321_H
#ifndef VUMARKTEMPLATEIMPL_T70BFD60A41858A19817826C4468AA5EE736E4ED8_H
#define VUMARKTEMPLATEIMPL_T70BFD60A41858A19817826C4468AA5EE736E4ED8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkTemplateImpl
struct  VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8  : public DataSetObjectTargetImpl_tC2D896B84A1FDE730F7E2F20045D12CBC6F30C98
{
public:
	// UnityEngine.Vector2 Vuforia.VuMarkTemplateImpl::mOrigin
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mOrigin_4;
	// System.Boolean Vuforia.VuMarkTemplateImpl::mTrackingFromRuntimeAppearance
	bool ___mTrackingFromRuntimeAppearance_5;

public:
	inline static int32_t get_offset_of_mOrigin_4() { return static_cast<int32_t>(offsetof(VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8, ___mOrigin_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mOrigin_4() const { return ___mOrigin_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mOrigin_4() { return &___mOrigin_4; }
	inline void set_mOrigin_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mOrigin_4 = value;
	}

	inline static int32_t get_offset_of_mTrackingFromRuntimeAppearance_5() { return static_cast<int32_t>(offsetof(VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8, ___mTrackingFromRuntimeAppearance_5)); }
	inline bool get_mTrackingFromRuntimeAppearance_5() const { return ___mTrackingFromRuntimeAppearance_5; }
	inline bool* get_address_of_mTrackingFromRuntimeAppearance_5() { return &___mTrackingFromRuntimeAppearance_5; }
	inline void set_mTrackingFromRuntimeAppearance_5(bool value)
	{
		___mTrackingFromRuntimeAppearance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTEMPLATEIMPL_T70BFD60A41858A19817826C4468AA5EE736E4ED8_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef VRDEVICECAMERACONFIGURATION_T647AFB774A37819F464C5D5F01599CAD29FD7E57_H
#define VRDEVICECAMERACONFIGURATION_T647AFB774A37819F464C5D5F01599CAD29FD7E57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VRDeviceCameraConfiguration
struct  VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57  : public BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A
{
public:
	// UnityEngine.Camera Vuforia.VRDeviceCameraConfiguration::mCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mCamera_9;
	// Vuforia.StereoProjMatrixStore Vuforia.VRDeviceCameraConfiguration::mMatrixStore
	StereoProjMatrixStore_t666C738477A3F36E8FDD7E872AC154108291348A * ___mMatrixStore_10;
	// UnityEngine.Matrix4x4 Vuforia.VRDeviceCameraConfiguration::mLeftMatrixUsedForVBPlacement
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___mLeftMatrixUsedForVBPlacement_11;
	// System.Single Vuforia.VRDeviceCameraConfiguration::mLastAppliedNearClipPlane
	float ___mLastAppliedNearClipPlane_12;
	// System.Single Vuforia.VRDeviceCameraConfiguration::mLastAppliedFarClipPlane
	float ___mLastAppliedFarClipPlane_13;
	// System.Single Vuforia.VRDeviceCameraConfiguration::mMaxDepthForVideoBackground
	float ___mMaxDepthForVideoBackground_14;
	// System.Single Vuforia.VRDeviceCameraConfiguration::mMinDepthForVideoBackground
	float ___mMinDepthForVideoBackground_15;
	// System.Int32 Vuforia.VRDeviceCameraConfiguration::mScreenWidth
	int32_t ___mScreenWidth_16;
	// System.Int32 Vuforia.VRDeviceCameraConfiguration::mScreenHeight
	int32_t ___mScreenHeight_17;
	// System.Single Vuforia.VRDeviceCameraConfiguration::mStereoDepth
	float ___mStereoDepth_18;
	// System.Boolean Vuforia.VRDeviceCameraConfiguration::mResetMatrix
	bool ___mResetMatrix_19;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera_StereoscopicEye,UnityEngine.Vector2> Vuforia.VRDeviceCameraConfiguration::mVuforiaFrustumSkew
	Dictionary_2_tA50FC73B50AEAE38268B60A47025C7B19AB363C7 * ___mVuforiaFrustumSkew_20;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera_StereoscopicEye,UnityEngine.Vector2> Vuforia.VRDeviceCameraConfiguration::mCenterToEyeAxis
	Dictionary_2_tA50FC73B50AEAE38268B60A47025C7B19AB363C7 * ___mCenterToEyeAxis_21;
	// Vuforia.VRDeviceController Vuforia.VRDeviceCameraConfiguration::mVrDeviceController
	VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044 * ___mVrDeviceController_22;

public:
	inline static int32_t get_offset_of_mCamera_9() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mCamera_9)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mCamera_9() const { return ___mCamera_9; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mCamera_9() { return &___mCamera_9; }
	inline void set_mCamera_9(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mCamera_9 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_9), value);
	}

	inline static int32_t get_offset_of_mMatrixStore_10() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mMatrixStore_10)); }
	inline StereoProjMatrixStore_t666C738477A3F36E8FDD7E872AC154108291348A * get_mMatrixStore_10() const { return ___mMatrixStore_10; }
	inline StereoProjMatrixStore_t666C738477A3F36E8FDD7E872AC154108291348A ** get_address_of_mMatrixStore_10() { return &___mMatrixStore_10; }
	inline void set_mMatrixStore_10(StereoProjMatrixStore_t666C738477A3F36E8FDD7E872AC154108291348A * value)
	{
		___mMatrixStore_10 = value;
		Il2CppCodeGenWriteBarrier((&___mMatrixStore_10), value);
	}

	inline static int32_t get_offset_of_mLeftMatrixUsedForVBPlacement_11() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mLeftMatrixUsedForVBPlacement_11)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_mLeftMatrixUsedForVBPlacement_11() const { return ___mLeftMatrixUsedForVBPlacement_11; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_mLeftMatrixUsedForVBPlacement_11() { return &___mLeftMatrixUsedForVBPlacement_11; }
	inline void set_mLeftMatrixUsedForVBPlacement_11(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___mLeftMatrixUsedForVBPlacement_11 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedNearClipPlane_12() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mLastAppliedNearClipPlane_12)); }
	inline float get_mLastAppliedNearClipPlane_12() const { return ___mLastAppliedNearClipPlane_12; }
	inline float* get_address_of_mLastAppliedNearClipPlane_12() { return &___mLastAppliedNearClipPlane_12; }
	inline void set_mLastAppliedNearClipPlane_12(float value)
	{
		___mLastAppliedNearClipPlane_12 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedFarClipPlane_13() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mLastAppliedFarClipPlane_13)); }
	inline float get_mLastAppliedFarClipPlane_13() const { return ___mLastAppliedFarClipPlane_13; }
	inline float* get_address_of_mLastAppliedFarClipPlane_13() { return &___mLastAppliedFarClipPlane_13; }
	inline void set_mLastAppliedFarClipPlane_13(float value)
	{
		___mLastAppliedFarClipPlane_13 = value;
	}

	inline static int32_t get_offset_of_mMaxDepthForVideoBackground_14() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mMaxDepthForVideoBackground_14)); }
	inline float get_mMaxDepthForVideoBackground_14() const { return ___mMaxDepthForVideoBackground_14; }
	inline float* get_address_of_mMaxDepthForVideoBackground_14() { return &___mMaxDepthForVideoBackground_14; }
	inline void set_mMaxDepthForVideoBackground_14(float value)
	{
		___mMaxDepthForVideoBackground_14 = value;
	}

	inline static int32_t get_offset_of_mMinDepthForVideoBackground_15() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mMinDepthForVideoBackground_15)); }
	inline float get_mMinDepthForVideoBackground_15() const { return ___mMinDepthForVideoBackground_15; }
	inline float* get_address_of_mMinDepthForVideoBackground_15() { return &___mMinDepthForVideoBackground_15; }
	inline void set_mMinDepthForVideoBackground_15(float value)
	{
		___mMinDepthForVideoBackground_15 = value;
	}

	inline static int32_t get_offset_of_mScreenWidth_16() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mScreenWidth_16)); }
	inline int32_t get_mScreenWidth_16() const { return ___mScreenWidth_16; }
	inline int32_t* get_address_of_mScreenWidth_16() { return &___mScreenWidth_16; }
	inline void set_mScreenWidth_16(int32_t value)
	{
		___mScreenWidth_16 = value;
	}

	inline static int32_t get_offset_of_mScreenHeight_17() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mScreenHeight_17)); }
	inline int32_t get_mScreenHeight_17() const { return ___mScreenHeight_17; }
	inline int32_t* get_address_of_mScreenHeight_17() { return &___mScreenHeight_17; }
	inline void set_mScreenHeight_17(int32_t value)
	{
		___mScreenHeight_17 = value;
	}

	inline static int32_t get_offset_of_mStereoDepth_18() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mStereoDepth_18)); }
	inline float get_mStereoDepth_18() const { return ___mStereoDepth_18; }
	inline float* get_address_of_mStereoDepth_18() { return &___mStereoDepth_18; }
	inline void set_mStereoDepth_18(float value)
	{
		___mStereoDepth_18 = value;
	}

	inline static int32_t get_offset_of_mResetMatrix_19() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mResetMatrix_19)); }
	inline bool get_mResetMatrix_19() const { return ___mResetMatrix_19; }
	inline bool* get_address_of_mResetMatrix_19() { return &___mResetMatrix_19; }
	inline void set_mResetMatrix_19(bool value)
	{
		___mResetMatrix_19 = value;
	}

	inline static int32_t get_offset_of_mVuforiaFrustumSkew_20() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mVuforiaFrustumSkew_20)); }
	inline Dictionary_2_tA50FC73B50AEAE38268B60A47025C7B19AB363C7 * get_mVuforiaFrustumSkew_20() const { return ___mVuforiaFrustumSkew_20; }
	inline Dictionary_2_tA50FC73B50AEAE38268B60A47025C7B19AB363C7 ** get_address_of_mVuforiaFrustumSkew_20() { return &___mVuforiaFrustumSkew_20; }
	inline void set_mVuforiaFrustumSkew_20(Dictionary_2_tA50FC73B50AEAE38268B60A47025C7B19AB363C7 * value)
	{
		___mVuforiaFrustumSkew_20 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaFrustumSkew_20), value);
	}

	inline static int32_t get_offset_of_mCenterToEyeAxis_21() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mCenterToEyeAxis_21)); }
	inline Dictionary_2_tA50FC73B50AEAE38268B60A47025C7B19AB363C7 * get_mCenterToEyeAxis_21() const { return ___mCenterToEyeAxis_21; }
	inline Dictionary_2_tA50FC73B50AEAE38268B60A47025C7B19AB363C7 ** get_address_of_mCenterToEyeAxis_21() { return &___mCenterToEyeAxis_21; }
	inline void set_mCenterToEyeAxis_21(Dictionary_2_tA50FC73B50AEAE38268B60A47025C7B19AB363C7 * value)
	{
		___mCenterToEyeAxis_21 = value;
		Il2CppCodeGenWriteBarrier((&___mCenterToEyeAxis_21), value);
	}

	inline static int32_t get_offset_of_mVrDeviceController_22() { return static_cast<int32_t>(offsetof(VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57, ___mVrDeviceController_22)); }
	inline VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044 * get_mVrDeviceController_22() const { return ___mVrDeviceController_22; }
	inline VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044 ** get_address_of_mVrDeviceController_22() { return &___mVrDeviceController_22; }
	inline void set_mVrDeviceController_22(VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044 * value)
	{
		___mVrDeviceController_22 = value;
		Il2CppCodeGenWriteBarrier((&___mVrDeviceController_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRDEVICECAMERACONFIGURATION_T647AFB774A37819F464C5D5F01599CAD29FD7E57_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef EXTERNALVRDEVICECAMERACONFIGURATION_T22E28FE2FAB2D078AC355155BECEF402BEB11EA2_H
#define EXTERNALVRDEVICECAMERACONFIGURATION_T22E28FE2FAB2D078AC355155BECEF402BEB11EA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ExternalVRDeviceCameraConfiguration
struct  ExternalVRDeviceCameraConfiguration_t22E28FE2FAB2D078AC355155BECEF402BEB11EA2  : public VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57
{
public:
	// Vuforia.VuforiaARController_WorldCenterMode Vuforia.ExternalVRDeviceCameraConfiguration::mLastWorldCenterMode
	int32_t ___mLastWorldCenterMode_23;

public:
	inline static int32_t get_offset_of_mLastWorldCenterMode_23() { return static_cast<int32_t>(offsetof(ExternalVRDeviceCameraConfiguration_t22E28FE2FAB2D078AC355155BECEF402BEB11EA2, ___mLastWorldCenterMode_23)); }
	inline int32_t get_mLastWorldCenterMode_23() const { return ___mLastWorldCenterMode_23; }
	inline int32_t* get_address_of_mLastWorldCenterMode_23() { return &___mLastWorldCenterMode_23; }
	inline void set_mLastWorldCenterMode_23(int32_t value)
	{
		___mLastWorldCenterMode_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALVRDEVICECAMERACONFIGURATION_T22E28FE2FAB2D078AC355155BECEF402BEB11EA2_H
#ifndef VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#define VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifndef ANCHORINPUTLISTENERBEHAVIOUR_TF944EDC39D85497FCE2C5AF520164C0B759EA404_H
#define ANCHORINPUTLISTENERBEHAVIOUR_TF944EDC39D85497FCE2C5AF520164C0B759EA404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.AnchorInputListenerBehaviour
struct  AnchorInputListenerBehaviour_tF944EDC39D85497FCE2C5AF520164C0B759EA404  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// Vuforia.AnchorInputListenerBehaviour_InputReceivedEvent Vuforia.AnchorInputListenerBehaviour::OnInputReceivedEvent
	InputReceivedEvent_t30AD5C108997667B53F1C6C844EF0B80096BD49C * ___OnInputReceivedEvent_4;
	// System.Boolean Vuforia.AnchorInputListenerBehaviour::mDisplayAdvanced
	bool ___mDisplayAdvanced_5;

public:
	inline static int32_t get_offset_of_OnInputReceivedEvent_4() { return static_cast<int32_t>(offsetof(AnchorInputListenerBehaviour_tF944EDC39D85497FCE2C5AF520164C0B759EA404, ___OnInputReceivedEvent_4)); }
	inline InputReceivedEvent_t30AD5C108997667B53F1C6C844EF0B80096BD49C * get_OnInputReceivedEvent_4() const { return ___OnInputReceivedEvent_4; }
	inline InputReceivedEvent_t30AD5C108997667B53F1C6C844EF0B80096BD49C ** get_address_of_OnInputReceivedEvent_4() { return &___OnInputReceivedEvent_4; }
	inline void set_OnInputReceivedEvent_4(InputReceivedEvent_t30AD5C108997667B53F1C6C844EF0B80096BD49C * value)
	{
		___OnInputReceivedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnInputReceivedEvent_4), value);
	}

	inline static int32_t get_offset_of_mDisplayAdvanced_5() { return static_cast<int32_t>(offsetof(AnchorInputListenerBehaviour_tF944EDC39D85497FCE2C5AF520164C0B759EA404, ___mDisplayAdvanced_5)); }
	inline bool get_mDisplayAdvanced_5() const { return ___mDisplayAdvanced_5; }
	inline bool* get_address_of_mDisplayAdvanced_5() { return &___mDisplayAdvanced_5; }
	inline void set_mDisplayAdvanced_5(bool value)
	{
		___mDisplayAdvanced_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORINPUTLISTENERBEHAVIOUR_TF944EDC39D85497FCE2C5AF520164C0B759EA404_H
#ifndef BACKGROUNDPLANEBEHAVIOUR_T2615248F9F83AF94A06C6585EBB6C1D3BCF64338_H
#define BACKGROUNDPLANEBEHAVIOUR_T2615248F9F83AF94A06C6585EBB6C1D3BCF64338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BackgroundPlaneBehaviour
struct  BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// UnityEngine.Vector2 Vuforia.BackgroundPlaneBehaviour::mTextureSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mTextureSize_4;
	// UnityEngine.Vector2 Vuforia.BackgroundPlaneBehaviour::mImageSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mImageSize_5;
	// UnityEngine.Transform Vuforia.BackgroundPlaneBehaviour::mCameraTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mCameraTransform_6;
	// UnityEngine.Mesh Vuforia.BackgroundPlaneBehaviour::mMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mMesh_8;
	// System.Single Vuforia.BackgroundPlaneBehaviour::mStereoDepth
	float ___mStereoDepth_9;
	// System.Boolean Vuforia.BackgroundPlaneBehaviour::mProjectionMatrixSetExternally
	bool ___mProjectionMatrixSetExternally_10;
	// UnityEngine.Vector3 Vuforia.BackgroundPlaneBehaviour::mBackgroundOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mBackgroundOffset_11;
	// Vuforia.VuforiaARController Vuforia.BackgroundPlaneBehaviour::mVuforiaBehaviour
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * ___mVuforiaBehaviour_12;
	// System.Action Vuforia.BackgroundPlaneBehaviour::mBackgroundPlacedCallback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mBackgroundPlacedCallback_13;
	// Vuforia.VideoBackgroundConfigValidator Vuforia.BackgroundPlaneBehaviour::mValidator
	VideoBackgroundConfigValidator_tC6D2432439D8AAAF1E72C8124FC620504DC11EB0 * ___mValidator_14;
	// Vuforia.IProjectMatrixProvider Vuforia.BackgroundPlaneBehaviour::mProjectMatrixProvider
	RuntimeObject* ___mProjectMatrixProvider_15;
	// System.Int32 Vuforia.BackgroundPlaneBehaviour::mNumFramesToUpdateVideoBg
	int32_t ___mNumFramesToUpdateVideoBg_16;
	// Vuforia.BackgroundPlaneBehaviour_ProjectionMatrixData Vuforia.BackgroundPlaneBehaviour::mLastUsedProjectioMatrix
	ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018  ___mLastUsedProjectioMatrix_17;
	// System.Int32 Vuforia.BackgroundPlaneBehaviour::mNumDivisions
	int32_t ___mNumDivisions_18;
	// Vuforia.HideExcessAreaUtility Vuforia.BackgroundPlaneBehaviour::mHideExcessAreaUtility
	HideExcessAreaUtility_t4DEC8A8570DC0458B45E1B21A6ABB435D6C63B32 * ___mHideExcessAreaUtility_19;

public:
	inline static int32_t get_offset_of_mTextureSize_4() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mTextureSize_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mTextureSize_4() const { return ___mTextureSize_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mTextureSize_4() { return &___mTextureSize_4; }
	inline void set_mTextureSize_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mTextureSize_4 = value;
	}

	inline static int32_t get_offset_of_mImageSize_5() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mImageSize_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mImageSize_5() const { return ___mImageSize_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mImageSize_5() { return &___mImageSize_5; }
	inline void set_mImageSize_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mImageSize_5 = value;
	}

	inline static int32_t get_offset_of_mCameraTransform_6() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mCameraTransform_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mCameraTransform_6() const { return ___mCameraTransform_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mCameraTransform_6() { return &___mCameraTransform_6; }
	inline void set_mCameraTransform_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mCameraTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraTransform_6), value);
	}

	inline static int32_t get_offset_of_mMesh_8() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mMesh_8)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_mMesh_8() const { return ___mMesh_8; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_mMesh_8() { return &___mMesh_8; }
	inline void set_mMesh_8(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___mMesh_8 = value;
		Il2CppCodeGenWriteBarrier((&___mMesh_8), value);
	}

	inline static int32_t get_offset_of_mStereoDepth_9() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mStereoDepth_9)); }
	inline float get_mStereoDepth_9() const { return ___mStereoDepth_9; }
	inline float* get_address_of_mStereoDepth_9() { return &___mStereoDepth_9; }
	inline void set_mStereoDepth_9(float value)
	{
		___mStereoDepth_9 = value;
	}

	inline static int32_t get_offset_of_mProjectionMatrixSetExternally_10() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mProjectionMatrixSetExternally_10)); }
	inline bool get_mProjectionMatrixSetExternally_10() const { return ___mProjectionMatrixSetExternally_10; }
	inline bool* get_address_of_mProjectionMatrixSetExternally_10() { return &___mProjectionMatrixSetExternally_10; }
	inline void set_mProjectionMatrixSetExternally_10(bool value)
	{
		___mProjectionMatrixSetExternally_10 = value;
	}

	inline static int32_t get_offset_of_mBackgroundOffset_11() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mBackgroundOffset_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mBackgroundOffset_11() const { return ___mBackgroundOffset_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mBackgroundOffset_11() { return &___mBackgroundOffset_11; }
	inline void set_mBackgroundOffset_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mBackgroundOffset_11 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_12() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mVuforiaBehaviour_12)); }
	inline VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * get_mVuforiaBehaviour_12() const { return ___mVuforiaBehaviour_12; }
	inline VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 ** get_address_of_mVuforiaBehaviour_12() { return &___mVuforiaBehaviour_12; }
	inline void set_mVuforiaBehaviour_12(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * value)
	{
		___mVuforiaBehaviour_12 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_12), value);
	}

	inline static int32_t get_offset_of_mBackgroundPlacedCallback_13() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mBackgroundPlacedCallback_13)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mBackgroundPlacedCallback_13() const { return ___mBackgroundPlacedCallback_13; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mBackgroundPlacedCallback_13() { return &___mBackgroundPlacedCallback_13; }
	inline void set_mBackgroundPlacedCallback_13(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mBackgroundPlacedCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundPlacedCallback_13), value);
	}

	inline static int32_t get_offset_of_mValidator_14() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mValidator_14)); }
	inline VideoBackgroundConfigValidator_tC6D2432439D8AAAF1E72C8124FC620504DC11EB0 * get_mValidator_14() const { return ___mValidator_14; }
	inline VideoBackgroundConfigValidator_tC6D2432439D8AAAF1E72C8124FC620504DC11EB0 ** get_address_of_mValidator_14() { return &___mValidator_14; }
	inline void set_mValidator_14(VideoBackgroundConfigValidator_tC6D2432439D8AAAF1E72C8124FC620504DC11EB0 * value)
	{
		___mValidator_14 = value;
		Il2CppCodeGenWriteBarrier((&___mValidator_14), value);
	}

	inline static int32_t get_offset_of_mProjectMatrixProvider_15() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mProjectMatrixProvider_15)); }
	inline RuntimeObject* get_mProjectMatrixProvider_15() const { return ___mProjectMatrixProvider_15; }
	inline RuntimeObject** get_address_of_mProjectMatrixProvider_15() { return &___mProjectMatrixProvider_15; }
	inline void set_mProjectMatrixProvider_15(RuntimeObject* value)
	{
		___mProjectMatrixProvider_15 = value;
		Il2CppCodeGenWriteBarrier((&___mProjectMatrixProvider_15), value);
	}

	inline static int32_t get_offset_of_mNumFramesToUpdateVideoBg_16() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mNumFramesToUpdateVideoBg_16)); }
	inline int32_t get_mNumFramesToUpdateVideoBg_16() const { return ___mNumFramesToUpdateVideoBg_16; }
	inline int32_t* get_address_of_mNumFramesToUpdateVideoBg_16() { return &___mNumFramesToUpdateVideoBg_16; }
	inline void set_mNumFramesToUpdateVideoBg_16(int32_t value)
	{
		___mNumFramesToUpdateVideoBg_16 = value;
	}

	inline static int32_t get_offset_of_mLastUsedProjectioMatrix_17() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mLastUsedProjectioMatrix_17)); }
	inline ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018  get_mLastUsedProjectioMatrix_17() const { return ___mLastUsedProjectioMatrix_17; }
	inline ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018 * get_address_of_mLastUsedProjectioMatrix_17() { return &___mLastUsedProjectioMatrix_17; }
	inline void set_mLastUsedProjectioMatrix_17(ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018  value)
	{
		___mLastUsedProjectioMatrix_17 = value;
	}

	inline static int32_t get_offset_of_mNumDivisions_18() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mNumDivisions_18)); }
	inline int32_t get_mNumDivisions_18() const { return ___mNumDivisions_18; }
	inline int32_t* get_address_of_mNumDivisions_18() { return &___mNumDivisions_18; }
	inline void set_mNumDivisions_18(int32_t value)
	{
		___mNumDivisions_18 = value;
	}

	inline static int32_t get_offset_of_mHideExcessAreaUtility_19() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338, ___mHideExcessAreaUtility_19)); }
	inline HideExcessAreaUtility_t4DEC8A8570DC0458B45E1B21A6ABB435D6C63B32 * get_mHideExcessAreaUtility_19() const { return ___mHideExcessAreaUtility_19; }
	inline HideExcessAreaUtility_t4DEC8A8570DC0458B45E1B21A6ABB435D6C63B32 ** get_address_of_mHideExcessAreaUtility_19() { return &___mHideExcessAreaUtility_19; }
	inline void set_mHideExcessAreaUtility_19(HideExcessAreaUtility_t4DEC8A8570DC0458B45E1B21A6ABB435D6C63B32 * value)
	{
		___mHideExcessAreaUtility_19 = value;
		Il2CppCodeGenWriteBarrier((&___mHideExcessAreaUtility_19), value);
	}
};

struct BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338_StaticFields
{
public:
	// System.Single Vuforia.BackgroundPlaneBehaviour::maxDisplacement
	float ___maxDisplacement_7;

public:
	inline static int32_t get_offset_of_maxDisplacement_7() { return static_cast<int32_t>(offsetof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338_StaticFields, ___maxDisplacement_7)); }
	inline float get_maxDisplacement_7() const { return ___maxDisplacement_7; }
	inline float* get_address_of_maxDisplacement_7() { return &___maxDisplacement_7; }
	inline void set_maxDisplacement_7(float value)
	{
		___maxDisplacement_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDPLANEBEHAVIOUR_T2615248F9F83AF94A06C6585EBB6C1D3BCF64338_H
#ifndef CONTENTPOSITIONINGBEHAVIOUR_T8B3CF7741A8FE98EAAE4210540CDA194E4379B5B_H
#define CONTENTPOSITIONINGBEHAVIOUR_T8B3CF7741A8FE98EAAE4210540CDA194E4379B5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ContentPositioningBehaviour
struct  ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// Vuforia.AnchorBehaviour Vuforia.ContentPositioningBehaviour::AnchorStage
	AnchorBehaviour_t44D245039CC4D9728F7594297E43EEE0885CB23E * ___AnchorStage_4;
	// System.Boolean Vuforia.ContentPositioningBehaviour::DuplicateStage
	bool ___DuplicateStage_5;
	// Vuforia.ContentPlacedEvent Vuforia.ContentPositioningBehaviour::OnContentPlaced
	ContentPlacedEvent_t6F76CC86D8FF827E64E67C1DB8D770FB7A085997 * ___OnContentPlaced_6;
	// System.Boolean Vuforia.ContentPositioningBehaviour::mDisplayAdvanced
	bool ___mDisplayAdvanced_7;
	// Vuforia.PositionalDeviceTracker Vuforia.ContentPositioningBehaviour::mDeviceTracker
	PositionalDeviceTracker_tEFAA5FDB9CF19DA74081AE51B41B4B5D9F40D8EE * ___mDeviceTracker_10;
	// System.Collections.Generic.ICollection`1<Vuforia.AnchorBehaviour> Vuforia.ContentPositioningBehaviour::mInstantiatedAnchors
	RuntimeObject* ___mInstantiatedAnchors_11;

public:
	inline static int32_t get_offset_of_AnchorStage_4() { return static_cast<int32_t>(offsetof(ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B, ___AnchorStage_4)); }
	inline AnchorBehaviour_t44D245039CC4D9728F7594297E43EEE0885CB23E * get_AnchorStage_4() const { return ___AnchorStage_4; }
	inline AnchorBehaviour_t44D245039CC4D9728F7594297E43EEE0885CB23E ** get_address_of_AnchorStage_4() { return &___AnchorStage_4; }
	inline void set_AnchorStage_4(AnchorBehaviour_t44D245039CC4D9728F7594297E43EEE0885CB23E * value)
	{
		___AnchorStage_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorStage_4), value);
	}

	inline static int32_t get_offset_of_DuplicateStage_5() { return static_cast<int32_t>(offsetof(ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B, ___DuplicateStage_5)); }
	inline bool get_DuplicateStage_5() const { return ___DuplicateStage_5; }
	inline bool* get_address_of_DuplicateStage_5() { return &___DuplicateStage_5; }
	inline void set_DuplicateStage_5(bool value)
	{
		___DuplicateStage_5 = value;
	}

	inline static int32_t get_offset_of_OnContentPlaced_6() { return static_cast<int32_t>(offsetof(ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B, ___OnContentPlaced_6)); }
	inline ContentPlacedEvent_t6F76CC86D8FF827E64E67C1DB8D770FB7A085997 * get_OnContentPlaced_6() const { return ___OnContentPlaced_6; }
	inline ContentPlacedEvent_t6F76CC86D8FF827E64E67C1DB8D770FB7A085997 ** get_address_of_OnContentPlaced_6() { return &___OnContentPlaced_6; }
	inline void set_OnContentPlaced_6(ContentPlacedEvent_t6F76CC86D8FF827E64E67C1DB8D770FB7A085997 * value)
	{
		___OnContentPlaced_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnContentPlaced_6), value);
	}

	inline static int32_t get_offset_of_mDisplayAdvanced_7() { return static_cast<int32_t>(offsetof(ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B, ___mDisplayAdvanced_7)); }
	inline bool get_mDisplayAdvanced_7() const { return ___mDisplayAdvanced_7; }
	inline bool* get_address_of_mDisplayAdvanced_7() { return &___mDisplayAdvanced_7; }
	inline void set_mDisplayAdvanced_7(bool value)
	{
		___mDisplayAdvanced_7 = value;
	}

	inline static int32_t get_offset_of_mDeviceTracker_10() { return static_cast<int32_t>(offsetof(ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B, ___mDeviceTracker_10)); }
	inline PositionalDeviceTracker_tEFAA5FDB9CF19DA74081AE51B41B4B5D9F40D8EE * get_mDeviceTracker_10() const { return ___mDeviceTracker_10; }
	inline PositionalDeviceTracker_tEFAA5FDB9CF19DA74081AE51B41B4B5D9F40D8EE ** get_address_of_mDeviceTracker_10() { return &___mDeviceTracker_10; }
	inline void set_mDeviceTracker_10(PositionalDeviceTracker_tEFAA5FDB9CF19DA74081AE51B41B4B5D9F40D8EE * value)
	{
		___mDeviceTracker_10 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceTracker_10), value);
	}

	inline static int32_t get_offset_of_mInstantiatedAnchors_11() { return static_cast<int32_t>(offsetof(ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B, ___mInstantiatedAnchors_11)); }
	inline RuntimeObject* get_mInstantiatedAnchors_11() const { return ___mInstantiatedAnchors_11; }
	inline RuntimeObject** get_address_of_mInstantiatedAnchors_11() { return &___mInstantiatedAnchors_11; }
	inline void set_mInstantiatedAnchors_11(RuntimeObject* value)
	{
		___mInstantiatedAnchors_11 = value;
		Il2CppCodeGenWriteBarrier((&___mInstantiatedAnchors_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTPOSITIONINGBEHAVIOUR_T8B3CF7741A8FE98EAAE4210540CDA194E4379B5B_H
#ifndef GLERRORHANDLER_T0E814EDCBE6293FD3D378715730ED229429AB93A_H
#define GLERRORHANDLER_T0E814EDCBE6293FD3D378715730ED229429AB93A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.GLErrorHandler
struct  GLErrorHandler_t0E814EDCBE6293FD3D378715730ED229429AB93A  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:

public:
};

struct GLErrorHandler_t0E814EDCBE6293FD3D378715730ED229429AB93A_StaticFields
{
public:
	// System.String Vuforia.GLErrorHandler::mErrorText
	String_t* ___mErrorText_4;
	// System.Boolean Vuforia.GLErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_5;

public:
	inline static int32_t get_offset_of_mErrorText_4() { return static_cast<int32_t>(offsetof(GLErrorHandler_t0E814EDCBE6293FD3D378715730ED229429AB93A_StaticFields, ___mErrorText_4)); }
	inline String_t* get_mErrorText_4() const { return ___mErrorText_4; }
	inline String_t** get_address_of_mErrorText_4() { return &___mErrorText_4; }
	inline void set_mErrorText_4(String_t* value)
	{
		___mErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_4), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_5() { return static_cast<int32_t>(offsetof(GLErrorHandler_t0E814EDCBE6293FD3D378715730ED229429AB93A_StaticFields, ___mErrorOccurred_5)); }
	inline bool get_mErrorOccurred_5() const { return ___mErrorOccurred_5; }
	inline bool* get_address_of_mErrorOccurred_5() { return &___mErrorOccurred_5; }
	inline void set_mErrorOccurred_5(bool value)
	{
		___mErrorOccurred_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLERRORHANDLER_T0E814EDCBE6293FD3D378715730ED229429AB93A_H
#ifndef GUIDEVIEW2DBEHAVIOUR_T2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B_H
#define GUIDEVIEW2DBEHAVIOUR_T2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.GuideView2DBehaviour
struct  GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.Single Vuforia.GuideView2DBehaviour::mCameraAspect
	float ___mCameraAspect_4;
	// System.Single Vuforia.GuideView2DBehaviour::mCameraFOV
	float ___mCameraFOV_5;
	// System.Single Vuforia.GuideView2DBehaviour::mCameraNearPlane
	float ___mCameraNearPlane_6;
	// UnityEngine.Texture2D Vuforia.GuideView2DBehaviour::mGuideViewTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___mGuideViewTexture_7;
	// System.Boolean Vuforia.GuideView2DBehaviour::mFlipImageVertically
	bool ___mFlipImageVertically_8;

public:
	inline static int32_t get_offset_of_mCameraAspect_4() { return static_cast<int32_t>(offsetof(GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B, ___mCameraAspect_4)); }
	inline float get_mCameraAspect_4() const { return ___mCameraAspect_4; }
	inline float* get_address_of_mCameraAspect_4() { return &___mCameraAspect_4; }
	inline void set_mCameraAspect_4(float value)
	{
		___mCameraAspect_4 = value;
	}

	inline static int32_t get_offset_of_mCameraFOV_5() { return static_cast<int32_t>(offsetof(GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B, ___mCameraFOV_5)); }
	inline float get_mCameraFOV_5() const { return ___mCameraFOV_5; }
	inline float* get_address_of_mCameraFOV_5() { return &___mCameraFOV_5; }
	inline void set_mCameraFOV_5(float value)
	{
		___mCameraFOV_5 = value;
	}

	inline static int32_t get_offset_of_mCameraNearPlane_6() { return static_cast<int32_t>(offsetof(GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B, ___mCameraNearPlane_6)); }
	inline float get_mCameraNearPlane_6() const { return ___mCameraNearPlane_6; }
	inline float* get_address_of_mCameraNearPlane_6() { return &___mCameraNearPlane_6; }
	inline void set_mCameraNearPlane_6(float value)
	{
		___mCameraNearPlane_6 = value;
	}

	inline static int32_t get_offset_of_mGuideViewTexture_7() { return static_cast<int32_t>(offsetof(GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B, ___mGuideViewTexture_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_mGuideViewTexture_7() const { return ___mGuideViewTexture_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_mGuideViewTexture_7() { return &___mGuideViewTexture_7; }
	inline void set_mGuideViewTexture_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___mGuideViewTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___mGuideViewTexture_7), value);
	}

	inline static int32_t get_offset_of_mFlipImageVertically_8() { return static_cast<int32_t>(offsetof(GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B, ___mFlipImageVertically_8)); }
	inline bool get_mFlipImageVertically_8() const { return ___mFlipImageVertically_8; }
	inline bool* get_address_of_mFlipImageVertically_8() { return &___mFlipImageVertically_8; }
	inline void set_mFlipImageVertically_8(bool value)
	{
		___mFlipImageVertically_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDEVIEW2DBEHAVIOUR_T2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B_H
#ifndef GUIDEVIEW3DBEHAVIOUR_TAFB7AAB771DAFC4E0CC71309E8B7B87FAC63EDC0_H
#define GUIDEVIEW3DBEHAVIOUR_TAFB7AAB771DAFC4E0CC71309E8B7B87FAC63EDC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.GuideView3DBehaviour
struct  GuideView3DBehaviour_tAFB7AAB771DAFC4E0CC71309E8B7B87FAC63EDC0  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// Vuforia.GuideView Vuforia.GuideView3DBehaviour::mCurrentGuideView
	GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956 * ___mCurrentGuideView_4;
	// UnityEngine.GameObject Vuforia.GuideView3DBehaviour::m3DModel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m3DModel_5;

public:
	inline static int32_t get_offset_of_mCurrentGuideView_4() { return static_cast<int32_t>(offsetof(GuideView3DBehaviour_tAFB7AAB771DAFC4E0CC71309E8B7B87FAC63EDC0, ___mCurrentGuideView_4)); }
	inline GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956 * get_mCurrentGuideView_4() const { return ___mCurrentGuideView_4; }
	inline GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956 ** get_address_of_mCurrentGuideView_4() { return &___mCurrentGuideView_4; }
	inline void set_mCurrentGuideView_4(GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956 * value)
	{
		___mCurrentGuideView_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCurrentGuideView_4), value);
	}

	inline static int32_t get_offset_of_m3DModel_5() { return static_cast<int32_t>(offsetof(GuideView3DBehaviour_tAFB7AAB771DAFC4E0CC71309E8B7B87FAC63EDC0, ___m3DModel_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m3DModel_5() const { return ___m3DModel_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m3DModel_5() { return &___m3DModel_5; }
	inline void set_m3DModel_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m3DModel_5 = value;
		Il2CppCodeGenWriteBarrier((&___m3DModel_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDEVIEW3DBEHAVIOUR_TAFB7AAB771DAFC4E0CC71309E8B7B87FAC63EDC0_H
#ifndef GUIDEVIEWRENDERINGBEHAVIOUR_TF142A04A9437EFC6298F1D6CD1629DD8C334FD62_H
#define GUIDEVIEWRENDERINGBEHAVIOUR_TF142A04A9437EFC6298F1D6CD1629DD8C334FD62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.GuideViewRenderingBehaviour
struct  GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.Single Vuforia.GuideViewRenderingBehaviour::guideReappearanceDelay
	float ___guideReappearanceDelay_4;
	// Vuforia.ModelTargetBehaviour Vuforia.GuideViewRenderingBehaviour::mTrackedTarget
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0 * ___mTrackedTarget_6;
	// Vuforia.ModelTargetBehaviour_GuideViewDisplayMode Vuforia.GuideViewRenderingBehaviour::mGuideViewDisplayMode
	int32_t ___mGuideViewDisplayMode_7;
	// Vuforia.GuideView Vuforia.GuideViewRenderingBehaviour::mGuideView
	GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956 * ___mGuideView_8;
	// System.Int32 Vuforia.GuideViewRenderingBehaviour::mLastActiveGuideViewIndex
	int32_t ___mLastActiveGuideViewIndex_9;
	// System.Boolean Vuforia.GuideViewRenderingBehaviour::mGuideViewInitialized
	bool ___mGuideViewInitialized_10;
	// System.Collections.IEnumerator Vuforia.GuideViewRenderingBehaviour::mShowGuideViewCoroutine
	RuntimeObject* ___mShowGuideViewCoroutine_11;
	// UnityEngine.GameObject Vuforia.GuideViewRenderingBehaviour::mGuideViewGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mGuideViewGameObject_12;
	// System.Boolean Vuforia.GuideViewRenderingBehaviour::mGuideViewShown
	bool ___mGuideViewShown_13;
	// UnityEngine.DepthTextureMode Vuforia.GuideViewRenderingBehaviour::mPrevDepthTextureMode
	int32_t ___mPrevDepthTextureMode_14;

public:
	inline static int32_t get_offset_of_guideReappearanceDelay_4() { return static_cast<int32_t>(offsetof(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62, ___guideReappearanceDelay_4)); }
	inline float get_guideReappearanceDelay_4() const { return ___guideReappearanceDelay_4; }
	inline float* get_address_of_guideReappearanceDelay_4() { return &___guideReappearanceDelay_4; }
	inline void set_guideReappearanceDelay_4(float value)
	{
		___guideReappearanceDelay_4 = value;
	}

	inline static int32_t get_offset_of_mTrackedTarget_6() { return static_cast<int32_t>(offsetof(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62, ___mTrackedTarget_6)); }
	inline ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0 * get_mTrackedTarget_6() const { return ___mTrackedTarget_6; }
	inline ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0 ** get_address_of_mTrackedTarget_6() { return &___mTrackedTarget_6; }
	inline void set_mTrackedTarget_6(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0 * value)
	{
		___mTrackedTarget_6 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackedTarget_6), value);
	}

	inline static int32_t get_offset_of_mGuideViewDisplayMode_7() { return static_cast<int32_t>(offsetof(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62, ___mGuideViewDisplayMode_7)); }
	inline int32_t get_mGuideViewDisplayMode_7() const { return ___mGuideViewDisplayMode_7; }
	inline int32_t* get_address_of_mGuideViewDisplayMode_7() { return &___mGuideViewDisplayMode_7; }
	inline void set_mGuideViewDisplayMode_7(int32_t value)
	{
		___mGuideViewDisplayMode_7 = value;
	}

	inline static int32_t get_offset_of_mGuideView_8() { return static_cast<int32_t>(offsetof(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62, ___mGuideView_8)); }
	inline GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956 * get_mGuideView_8() const { return ___mGuideView_8; }
	inline GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956 ** get_address_of_mGuideView_8() { return &___mGuideView_8; }
	inline void set_mGuideView_8(GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956 * value)
	{
		___mGuideView_8 = value;
		Il2CppCodeGenWriteBarrier((&___mGuideView_8), value);
	}

	inline static int32_t get_offset_of_mLastActiveGuideViewIndex_9() { return static_cast<int32_t>(offsetof(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62, ___mLastActiveGuideViewIndex_9)); }
	inline int32_t get_mLastActiveGuideViewIndex_9() const { return ___mLastActiveGuideViewIndex_9; }
	inline int32_t* get_address_of_mLastActiveGuideViewIndex_9() { return &___mLastActiveGuideViewIndex_9; }
	inline void set_mLastActiveGuideViewIndex_9(int32_t value)
	{
		___mLastActiveGuideViewIndex_9 = value;
	}

	inline static int32_t get_offset_of_mGuideViewInitialized_10() { return static_cast<int32_t>(offsetof(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62, ___mGuideViewInitialized_10)); }
	inline bool get_mGuideViewInitialized_10() const { return ___mGuideViewInitialized_10; }
	inline bool* get_address_of_mGuideViewInitialized_10() { return &___mGuideViewInitialized_10; }
	inline void set_mGuideViewInitialized_10(bool value)
	{
		___mGuideViewInitialized_10 = value;
	}

	inline static int32_t get_offset_of_mShowGuideViewCoroutine_11() { return static_cast<int32_t>(offsetof(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62, ___mShowGuideViewCoroutine_11)); }
	inline RuntimeObject* get_mShowGuideViewCoroutine_11() const { return ___mShowGuideViewCoroutine_11; }
	inline RuntimeObject** get_address_of_mShowGuideViewCoroutine_11() { return &___mShowGuideViewCoroutine_11; }
	inline void set_mShowGuideViewCoroutine_11(RuntimeObject* value)
	{
		___mShowGuideViewCoroutine_11 = value;
		Il2CppCodeGenWriteBarrier((&___mShowGuideViewCoroutine_11), value);
	}

	inline static int32_t get_offset_of_mGuideViewGameObject_12() { return static_cast<int32_t>(offsetof(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62, ___mGuideViewGameObject_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mGuideViewGameObject_12() const { return ___mGuideViewGameObject_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mGuideViewGameObject_12() { return &___mGuideViewGameObject_12; }
	inline void set_mGuideViewGameObject_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mGuideViewGameObject_12 = value;
		Il2CppCodeGenWriteBarrier((&___mGuideViewGameObject_12), value);
	}

	inline static int32_t get_offset_of_mGuideViewShown_13() { return static_cast<int32_t>(offsetof(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62, ___mGuideViewShown_13)); }
	inline bool get_mGuideViewShown_13() const { return ___mGuideViewShown_13; }
	inline bool* get_address_of_mGuideViewShown_13() { return &___mGuideViewShown_13; }
	inline void set_mGuideViewShown_13(bool value)
	{
		___mGuideViewShown_13 = value;
	}

	inline static int32_t get_offset_of_mPrevDepthTextureMode_14() { return static_cast<int32_t>(offsetof(GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62, ___mPrevDepthTextureMode_14)); }
	inline int32_t get_mPrevDepthTextureMode_14() const { return ___mPrevDepthTextureMode_14; }
	inline int32_t* get_address_of_mPrevDepthTextureMode_14() { return &___mPrevDepthTextureMode_14; }
	inline void set_mPrevDepthTextureMode_14(int32_t value)
	{
		___mPrevDepthTextureMode_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDEVIEWRENDERINGBEHAVIOUR_TF142A04A9437EFC6298F1D6CD1629DD8C334FD62_H
#ifndef MASKOUTBEHAVIOUR_T1D183DD82AF2945A6334465A9E6759268AB689F4_H
#define MASKOUTBEHAVIOUR_T1D183DD82AF2945A6334465A9E6759268AB689F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MaskOutBehaviour
struct  MaskOutBehaviour_t1D183DD82AF2945A6334465A9E6759268AB689F4  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// UnityEngine.Material Vuforia.MaskOutBehaviour::maskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___maskMaterial_4;

public:
	inline static int32_t get_offset_of_maskMaterial_4() { return static_cast<int32_t>(offsetof(MaskOutBehaviour_t1D183DD82AF2945A6334465A9E6759268AB689F4, ___maskMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_maskMaterial_4() const { return ___maskMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_maskMaterial_4() { return &___maskMaterial_4; }
	inline void set_maskMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___maskMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___maskMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKOUTBEHAVIOUR_T1D183DD82AF2945A6334465A9E6759268AB689F4_H
#ifndef MIDAIRPOSITIONERBEHAVIOUR_T1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0_H
#define MIDAIRPOSITIONERBEHAVIOUR_T1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MidAirPositionerBehaviour
struct  MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.Single Vuforia.MidAirPositionerBehaviour::DistanceToCamera
	float ___DistanceToCamera_4;
	// UnityEngine.GameObject Vuforia.MidAirPositionerBehaviour::MidAirIndicator
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___MidAirIndicator_5;
	// Vuforia.MidAirPositionerBehaviour_AnchorPositionConfirmedEvent Vuforia.MidAirPositionerBehaviour::OnAnchorPositionConfirmed
	AnchorPositionConfirmedEvent_t63B8BA26EFA42FC76914E18C5AF9E3E9223B5093 * ___OnAnchorPositionConfirmed_6;
	// UnityEngine.Camera Vuforia.MidAirPositionerBehaviour::mCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mCamera_7;
	// System.Boolean Vuforia.MidAirPositionerBehaviour::mDisplayAdvanced
	bool ___mDisplayAdvanced_8;

public:
	inline static int32_t get_offset_of_DistanceToCamera_4() { return static_cast<int32_t>(offsetof(MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0, ___DistanceToCamera_4)); }
	inline float get_DistanceToCamera_4() const { return ___DistanceToCamera_4; }
	inline float* get_address_of_DistanceToCamera_4() { return &___DistanceToCamera_4; }
	inline void set_DistanceToCamera_4(float value)
	{
		___DistanceToCamera_4 = value;
	}

	inline static int32_t get_offset_of_MidAirIndicator_5() { return static_cast<int32_t>(offsetof(MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0, ___MidAirIndicator_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_MidAirIndicator_5() const { return ___MidAirIndicator_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_MidAirIndicator_5() { return &___MidAirIndicator_5; }
	inline void set_MidAirIndicator_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___MidAirIndicator_5 = value;
		Il2CppCodeGenWriteBarrier((&___MidAirIndicator_5), value);
	}

	inline static int32_t get_offset_of_OnAnchorPositionConfirmed_6() { return static_cast<int32_t>(offsetof(MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0, ___OnAnchorPositionConfirmed_6)); }
	inline AnchorPositionConfirmedEvent_t63B8BA26EFA42FC76914E18C5AF9E3E9223B5093 * get_OnAnchorPositionConfirmed_6() const { return ___OnAnchorPositionConfirmed_6; }
	inline AnchorPositionConfirmedEvent_t63B8BA26EFA42FC76914E18C5AF9E3E9223B5093 ** get_address_of_OnAnchorPositionConfirmed_6() { return &___OnAnchorPositionConfirmed_6; }
	inline void set_OnAnchorPositionConfirmed_6(AnchorPositionConfirmedEvent_t63B8BA26EFA42FC76914E18C5AF9E3E9223B5093 * value)
	{
		___OnAnchorPositionConfirmed_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAnchorPositionConfirmed_6), value);
	}

	inline static int32_t get_offset_of_mCamera_7() { return static_cast<int32_t>(offsetof(MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0, ___mCamera_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mCamera_7() const { return ___mCamera_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mCamera_7() { return &___mCamera_7; }
	inline void set_mCamera_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_7), value);
	}

	inline static int32_t get_offset_of_mDisplayAdvanced_8() { return static_cast<int32_t>(offsetof(MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0, ___mDisplayAdvanced_8)); }
	inline bool get_mDisplayAdvanced_8() const { return ___mDisplayAdvanced_8; }
	inline bool* get_address_of_mDisplayAdvanced_8() { return &___mDisplayAdvanced_8; }
	inline void set_mDisplayAdvanced_8(bool value)
	{
		___mDisplayAdvanced_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIDAIRPOSITIONERBEHAVIOUR_T1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0_H
#ifndef PLANEFINDERBEHAVIOUR_T217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9_H
#define PLANEFINDERBEHAVIOUR_T217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlaneFinderBehaviour
struct  PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// UnityEngine.GameObject Vuforia.PlaneFinderBehaviour::PlaneIndicator
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___PlaneIndicator_4;
	// System.Single Vuforia.PlaneFinderBehaviour::Height
	float ___Height_5;
	// Vuforia.HitTestEvent Vuforia.PlaneFinderBehaviour::OnInteractiveHitTest
	HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F * ___OnInteractiveHitTest_6;
	// Vuforia.HitTestEvent Vuforia.PlaneFinderBehaviour::OnAutomaticHitTest
	HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F * ___OnAutomaticHitTest_7;
	// Vuforia.SmartTerrain Vuforia.PlaneFinderBehaviour::mSmartTerrain
	SmartTerrain_t09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47 * ___mSmartTerrain_8;
	// UnityEngine.Vector2 Vuforia.PlaneFinderBehaviour::mViewportCenter
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mViewportCenter_9;
	// System.Single Vuforia.PlaneFinderBehaviour::mLastUpdate
	float ___mLastUpdate_10;
	// System.Boolean Vuforia.PlaneFinderBehaviour::mPreviouslyVisible
	bool ___mPreviouslyVisible_11;
	// System.Boolean Vuforia.PlaneFinderBehaviour::mDisplayAdvanced
	bool ___mDisplayAdvanced_13;
	// Vuforia.HitTestMode Vuforia.PlaneFinderBehaviour::mHitTestMode
	int32_t ___mHitTestMode_14;

public:
	inline static int32_t get_offset_of_PlaneIndicator_4() { return static_cast<int32_t>(offsetof(PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9, ___PlaneIndicator_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_PlaneIndicator_4() const { return ___PlaneIndicator_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_PlaneIndicator_4() { return &___PlaneIndicator_4; }
	inline void set_PlaneIndicator_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___PlaneIndicator_4 = value;
		Il2CppCodeGenWriteBarrier((&___PlaneIndicator_4), value);
	}

	inline static int32_t get_offset_of_Height_5() { return static_cast<int32_t>(offsetof(PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9, ___Height_5)); }
	inline float get_Height_5() const { return ___Height_5; }
	inline float* get_address_of_Height_5() { return &___Height_5; }
	inline void set_Height_5(float value)
	{
		___Height_5 = value;
	}

	inline static int32_t get_offset_of_OnInteractiveHitTest_6() { return static_cast<int32_t>(offsetof(PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9, ___OnInteractiveHitTest_6)); }
	inline HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F * get_OnInteractiveHitTest_6() const { return ___OnInteractiveHitTest_6; }
	inline HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F ** get_address_of_OnInteractiveHitTest_6() { return &___OnInteractiveHitTest_6; }
	inline void set_OnInteractiveHitTest_6(HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F * value)
	{
		___OnInteractiveHitTest_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnInteractiveHitTest_6), value);
	}

	inline static int32_t get_offset_of_OnAutomaticHitTest_7() { return static_cast<int32_t>(offsetof(PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9, ___OnAutomaticHitTest_7)); }
	inline HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F * get_OnAutomaticHitTest_7() const { return ___OnAutomaticHitTest_7; }
	inline HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F ** get_address_of_OnAutomaticHitTest_7() { return &___OnAutomaticHitTest_7; }
	inline void set_OnAutomaticHitTest_7(HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F * value)
	{
		___OnAutomaticHitTest_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnAutomaticHitTest_7), value);
	}

	inline static int32_t get_offset_of_mSmartTerrain_8() { return static_cast<int32_t>(offsetof(PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9, ___mSmartTerrain_8)); }
	inline SmartTerrain_t09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47 * get_mSmartTerrain_8() const { return ___mSmartTerrain_8; }
	inline SmartTerrain_t09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47 ** get_address_of_mSmartTerrain_8() { return &___mSmartTerrain_8; }
	inline void set_mSmartTerrain_8(SmartTerrain_t09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47 * value)
	{
		___mSmartTerrain_8 = value;
		Il2CppCodeGenWriteBarrier((&___mSmartTerrain_8), value);
	}

	inline static int32_t get_offset_of_mViewportCenter_9() { return static_cast<int32_t>(offsetof(PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9, ___mViewportCenter_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mViewportCenter_9() const { return ___mViewportCenter_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mViewportCenter_9() { return &___mViewportCenter_9; }
	inline void set_mViewportCenter_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mViewportCenter_9 = value;
	}

	inline static int32_t get_offset_of_mLastUpdate_10() { return static_cast<int32_t>(offsetof(PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9, ___mLastUpdate_10)); }
	inline float get_mLastUpdate_10() const { return ___mLastUpdate_10; }
	inline float* get_address_of_mLastUpdate_10() { return &___mLastUpdate_10; }
	inline void set_mLastUpdate_10(float value)
	{
		___mLastUpdate_10 = value;
	}

	inline static int32_t get_offset_of_mPreviouslyVisible_11() { return static_cast<int32_t>(offsetof(PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9, ___mPreviouslyVisible_11)); }
	inline bool get_mPreviouslyVisible_11() const { return ___mPreviouslyVisible_11; }
	inline bool* get_address_of_mPreviouslyVisible_11() { return &___mPreviouslyVisible_11; }
	inline void set_mPreviouslyVisible_11(bool value)
	{
		___mPreviouslyVisible_11 = value;
	}

	inline static int32_t get_offset_of_mDisplayAdvanced_13() { return static_cast<int32_t>(offsetof(PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9, ___mDisplayAdvanced_13)); }
	inline bool get_mDisplayAdvanced_13() const { return ___mDisplayAdvanced_13; }
	inline bool* get_address_of_mDisplayAdvanced_13() { return &___mDisplayAdvanced_13; }
	inline void set_mDisplayAdvanced_13(bool value)
	{
		___mDisplayAdvanced_13 = value;
	}

	inline static int32_t get_offset_of_mHitTestMode_14() { return static_cast<int32_t>(offsetof(PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9, ___mHitTestMode_14)); }
	inline int32_t get_mHitTestMode_14() const { return ___mHitTestMode_14; }
	inline int32_t* get_address_of_mHitTestMode_14() { return &___mHitTestMode_14; }
	inline void set_mHitTestMode_14(int32_t value)
	{
		___mHitTestMode_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEFINDERBEHAVIOUR_T217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9_H
#ifndef TRACKABLEBEHAVIOUR_T579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_H
#define TRACKABLEBEHAVIOUR_T579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_4;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_5;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_6;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_7;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mPreviousScale_8;
	// Vuforia.TrackableBehaviour_Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_9;
	// Vuforia.TrackableBehaviour_StatusInfo Vuforia.TrackableBehaviour::mStatusInfo
	int32_t ___mStatusInfo_10;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_11;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B * ___mTrackableEventHandlers_12;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___U3CTimeStampU3Ek__BackingField_4)); }
	inline double get_U3CTimeStampU3Ek__BackingField_4() const { return ___U3CTimeStampU3Ek__BackingField_4; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_4() { return &___U3CTimeStampU3Ek__BackingField_4; }
	inline void set_U3CTimeStampU3Ek__BackingField_4(double value)
	{
		___U3CTimeStampU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackableName_5)); }
	inline String_t* get_mTrackableName_5() const { return ___mTrackableName_5; }
	inline String_t** get_address_of_mTrackableName_5() { return &___mTrackableName_5; }
	inline void set_mTrackableName_5(String_t* value)
	{
		___mTrackableName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_5), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mPreserveChildSize_6)); }
	inline bool get_mPreserveChildSize_6() const { return ___mPreserveChildSize_6; }
	inline bool* get_address_of_mPreserveChildSize_6() { return &___mPreserveChildSize_6; }
	inline void set_mPreserveChildSize_6(bool value)
	{
		___mPreserveChildSize_6 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mInitializedInEditor_7)); }
	inline bool get_mInitializedInEditor_7() const { return ___mInitializedInEditor_7; }
	inline bool* get_address_of_mInitializedInEditor_7() { return &___mInitializedInEditor_7; }
	inline void set_mInitializedInEditor_7(bool value)
	{
		___mInitializedInEditor_7 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mPreviousScale_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mPreviousScale_8() const { return ___mPreviousScale_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mPreviousScale_8() { return &___mPreviousScale_8; }
	inline void set_mPreviousScale_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mPreviousScale_8 = value;
	}

	inline static int32_t get_offset_of_mStatus_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mStatus_9)); }
	inline int32_t get_mStatus_9() const { return ___mStatus_9; }
	inline int32_t* get_address_of_mStatus_9() { return &___mStatus_9; }
	inline void set_mStatus_9(int32_t value)
	{
		___mStatus_9 = value;
	}

	inline static int32_t get_offset_of_mStatusInfo_10() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mStatusInfo_10)); }
	inline int32_t get_mStatusInfo_10() const { return ___mStatusInfo_10; }
	inline int32_t* get_address_of_mStatusInfo_10() { return &___mStatusInfo_10; }
	inline void set_mStatusInfo_10(int32_t value)
	{
		___mStatusInfo_10 = value;
	}

	inline static int32_t get_offset_of_mTrackable_11() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackable_11)); }
	inline RuntimeObject* get_mTrackable_11() const { return ___mTrackable_11; }
	inline RuntimeObject** get_address_of_mTrackable_11() { return &___mTrackable_11; }
	inline void set_mTrackable_11(RuntimeObject* value)
	{
		___mTrackable_11 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_11), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_12() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackableEventHandlers_12)); }
	inline List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B * get_mTrackableEventHandlers_12() const { return ___mTrackableEventHandlers_12; }
	inline List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B ** get_address_of_mTrackableEventHandlers_12() { return &___mTrackableEventHandlers_12; }
	inline void set_mTrackableEventHandlers_12(List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B * value)
	{
		___mTrackableEventHandlers_12 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_H
#ifndef TURNOFFBEHAVIOUR_T086F35AFC9757F6CB78FB7F1387D531C419C4703_H
#define TURNOFFBEHAVIOUR_T086F35AFC9757F6CB78FB7F1387D531C419C4703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TurnOffBehaviour
struct  TurnOffBehaviour_t086F35AFC9757F6CB78FB7F1387D531C419C4703  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNOFFBEHAVIOUR_T086F35AFC9757F6CB78FB7F1387D531C419C4703_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T84B7BB3C959046F38CC73E423800BD2F8859E706_H
#define DATASETTRACKABLEBEHAVIOUR_T84B7BB3C959046F38CC73E423800BD2F8859E706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706  : public TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_13;

public:
	inline static int32_t get_offset_of_mDataSetPath_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706, ___mDataSetPath_13)); }
	inline String_t* get_mDataSetPath_13() const { return ___mDataSetPath_13; }
	inline String_t** get_address_of_mDataSetPath_13() { return &___mDataSetPath_13; }
	inline void set_mDataSetPath_13(String_t* value)
	{
		___mDataSetPath_13 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T84B7BB3C959046F38CC73E423800BD2F8859E706_H
#ifndef MODELTARGETBEHAVIOUR_T283F7A0B136589E033A458B5FE0C42F3248CE0B0_H
#define MODELTARGETBEHAVIOUR_T283F7A0B136589E033A458B5FE0C42F3248CE0B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ModelTargetBehaviour
struct  ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0  : public DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706
{
public:
	// Vuforia.InternalModelTarget Vuforia.ModelTargetBehaviour::mModelTarget
	RuntimeObject* ___mModelTarget_15;
	// UnityEngine.GameObject Vuforia.ModelTargetBehaviour::mGuideViewRenderer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mGuideViewRenderer_16;
	// Vuforia.PositionalDeviceTracker Vuforia.ModelTargetBehaviour::mDeviceTracker
	PositionalDeviceTracker_tEFAA5FDB9CF19DA74081AE51B41B4B5D9F40D8EE * ___mDeviceTracker_17;
	// System.Single Vuforia.ModelTargetBehaviour::mBaseSize
	float ___mBaseSize_18;
	// System.Single Vuforia.ModelTargetBehaviour::mAspectRatioXY
	float ___mAspectRatioXY_19;
	// System.Single Vuforia.ModelTargetBehaviour::mAspectRatioXZ
	float ___mAspectRatioXZ_20;
	// System.Boolean Vuforia.ModelTargetBehaviour::mShowBoundingBox
	bool ___mShowBoundingBox_21;
	// System.Boolean Vuforia.ModelTargetBehaviour::mOverrideSnappingPose
	bool ___mOverrideSnappingPose_22;
	// UnityEngine.Vector3 Vuforia.ModelTargetBehaviour::mBBoxMin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mBBoxMin_23;
	// UnityEngine.Vector3 Vuforia.ModelTargetBehaviour::mBBoxMax
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mBBoxMax_24;
	// UnityEngine.Texture2D Vuforia.ModelTargetBehaviour::mPreviewImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___mPreviewImage_25;
	// System.Single Vuforia.ModelTargetBehaviour::mLength
	float ___mLength_26;
	// System.Single Vuforia.ModelTargetBehaviour::mWidth
	float ___mWidth_27;
	// System.Single Vuforia.ModelTargetBehaviour::mHeight
	float ___mHeight_28;
	// UnityEngine.GameObject Vuforia.ModelTargetBehaviour::m3DGuideViewModel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m3DGuideViewModel_29;
	// UnityEngine.Texture2D Vuforia.ModelTargetBehaviour::m2DGuideViewImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m2DGuideViewImage_30;
	// Vuforia.ModelTargetBehaviour_GuideViewDisplayMode Vuforia.ModelTargetBehaviour::mGuideViewDisplayMode
	int32_t ___mGuideViewDisplayMode_31;
	// UnityEngine.Material Vuforia.ModelTargetBehaviour::m2DGuideViewMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m2DGuideViewMaterial_32;
	// UnityEngine.Material Vuforia.ModelTargetBehaviour::m3DGuideViewMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m3DGuideViewMaterial_33;
	// System.String Vuforia.ModelTargetBehaviour::mSelectedGuideView
	String_t* ___mSelectedGuideView_34;
	// UnityEngine.Vector3 Vuforia.ModelTargetBehaviour::mLastTransformScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mLastTransformScale_35;
	// UnityEngine.Vector3 Vuforia.ModelTargetBehaviour::mLastSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mLastSize_36;
	// System.Boolean Vuforia.ModelTargetBehaviour::mDataSetIsActive
	bool ___mDataSetIsActive_37;
	// Vuforia.ModelRecoBehaviour Vuforia.ModelTargetBehaviour::mModelRecoBehaviour
	ModelRecoBehaviour_t136409F3AECD7AEB24CC39E5CA3A50A4CAAF2291 * ___mModelRecoBehaviour_38;

public:
	inline static int32_t get_offset_of_mModelTarget_15() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mModelTarget_15)); }
	inline RuntimeObject* get_mModelTarget_15() const { return ___mModelTarget_15; }
	inline RuntimeObject** get_address_of_mModelTarget_15() { return &___mModelTarget_15; }
	inline void set_mModelTarget_15(RuntimeObject* value)
	{
		___mModelTarget_15 = value;
		Il2CppCodeGenWriteBarrier((&___mModelTarget_15), value);
	}

	inline static int32_t get_offset_of_mGuideViewRenderer_16() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mGuideViewRenderer_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mGuideViewRenderer_16() const { return ___mGuideViewRenderer_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mGuideViewRenderer_16() { return &___mGuideViewRenderer_16; }
	inline void set_mGuideViewRenderer_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mGuideViewRenderer_16 = value;
		Il2CppCodeGenWriteBarrier((&___mGuideViewRenderer_16), value);
	}

	inline static int32_t get_offset_of_mDeviceTracker_17() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mDeviceTracker_17)); }
	inline PositionalDeviceTracker_tEFAA5FDB9CF19DA74081AE51B41B4B5D9F40D8EE * get_mDeviceTracker_17() const { return ___mDeviceTracker_17; }
	inline PositionalDeviceTracker_tEFAA5FDB9CF19DA74081AE51B41B4B5D9F40D8EE ** get_address_of_mDeviceTracker_17() { return &___mDeviceTracker_17; }
	inline void set_mDeviceTracker_17(PositionalDeviceTracker_tEFAA5FDB9CF19DA74081AE51B41B4B5D9F40D8EE * value)
	{
		___mDeviceTracker_17 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceTracker_17), value);
	}

	inline static int32_t get_offset_of_mBaseSize_18() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mBaseSize_18)); }
	inline float get_mBaseSize_18() const { return ___mBaseSize_18; }
	inline float* get_address_of_mBaseSize_18() { return &___mBaseSize_18; }
	inline void set_mBaseSize_18(float value)
	{
		___mBaseSize_18 = value;
	}

	inline static int32_t get_offset_of_mAspectRatioXY_19() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mAspectRatioXY_19)); }
	inline float get_mAspectRatioXY_19() const { return ___mAspectRatioXY_19; }
	inline float* get_address_of_mAspectRatioXY_19() { return &___mAspectRatioXY_19; }
	inline void set_mAspectRatioXY_19(float value)
	{
		___mAspectRatioXY_19 = value;
	}

	inline static int32_t get_offset_of_mAspectRatioXZ_20() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mAspectRatioXZ_20)); }
	inline float get_mAspectRatioXZ_20() const { return ___mAspectRatioXZ_20; }
	inline float* get_address_of_mAspectRatioXZ_20() { return &___mAspectRatioXZ_20; }
	inline void set_mAspectRatioXZ_20(float value)
	{
		___mAspectRatioXZ_20 = value;
	}

	inline static int32_t get_offset_of_mShowBoundingBox_21() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mShowBoundingBox_21)); }
	inline bool get_mShowBoundingBox_21() const { return ___mShowBoundingBox_21; }
	inline bool* get_address_of_mShowBoundingBox_21() { return &___mShowBoundingBox_21; }
	inline void set_mShowBoundingBox_21(bool value)
	{
		___mShowBoundingBox_21 = value;
	}

	inline static int32_t get_offset_of_mOverrideSnappingPose_22() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mOverrideSnappingPose_22)); }
	inline bool get_mOverrideSnappingPose_22() const { return ___mOverrideSnappingPose_22; }
	inline bool* get_address_of_mOverrideSnappingPose_22() { return &___mOverrideSnappingPose_22; }
	inline void set_mOverrideSnappingPose_22(bool value)
	{
		___mOverrideSnappingPose_22 = value;
	}

	inline static int32_t get_offset_of_mBBoxMin_23() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mBBoxMin_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mBBoxMin_23() const { return ___mBBoxMin_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mBBoxMin_23() { return &___mBBoxMin_23; }
	inline void set_mBBoxMin_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mBBoxMin_23 = value;
	}

	inline static int32_t get_offset_of_mBBoxMax_24() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mBBoxMax_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mBBoxMax_24() const { return ___mBBoxMax_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mBBoxMax_24() { return &___mBBoxMax_24; }
	inline void set_mBBoxMax_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mBBoxMax_24 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_25() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mPreviewImage_25)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_mPreviewImage_25() const { return ___mPreviewImage_25; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_mPreviewImage_25() { return &___mPreviewImage_25; }
	inline void set_mPreviewImage_25(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___mPreviewImage_25 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewImage_25), value);
	}

	inline static int32_t get_offset_of_mLength_26() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mLength_26)); }
	inline float get_mLength_26() const { return ___mLength_26; }
	inline float* get_address_of_mLength_26() { return &___mLength_26; }
	inline void set_mLength_26(float value)
	{
		___mLength_26 = value;
	}

	inline static int32_t get_offset_of_mWidth_27() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mWidth_27)); }
	inline float get_mWidth_27() const { return ___mWidth_27; }
	inline float* get_address_of_mWidth_27() { return &___mWidth_27; }
	inline void set_mWidth_27(float value)
	{
		___mWidth_27 = value;
	}

	inline static int32_t get_offset_of_mHeight_28() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mHeight_28)); }
	inline float get_mHeight_28() const { return ___mHeight_28; }
	inline float* get_address_of_mHeight_28() { return &___mHeight_28; }
	inline void set_mHeight_28(float value)
	{
		___mHeight_28 = value;
	}

	inline static int32_t get_offset_of_m3DGuideViewModel_29() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___m3DGuideViewModel_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m3DGuideViewModel_29() const { return ___m3DGuideViewModel_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m3DGuideViewModel_29() { return &___m3DGuideViewModel_29; }
	inline void set_m3DGuideViewModel_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m3DGuideViewModel_29 = value;
		Il2CppCodeGenWriteBarrier((&___m3DGuideViewModel_29), value);
	}

	inline static int32_t get_offset_of_m2DGuideViewImage_30() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___m2DGuideViewImage_30)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m2DGuideViewImage_30() const { return ___m2DGuideViewImage_30; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m2DGuideViewImage_30() { return &___m2DGuideViewImage_30; }
	inline void set_m2DGuideViewImage_30(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m2DGuideViewImage_30 = value;
		Il2CppCodeGenWriteBarrier((&___m2DGuideViewImage_30), value);
	}

	inline static int32_t get_offset_of_mGuideViewDisplayMode_31() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mGuideViewDisplayMode_31)); }
	inline int32_t get_mGuideViewDisplayMode_31() const { return ___mGuideViewDisplayMode_31; }
	inline int32_t* get_address_of_mGuideViewDisplayMode_31() { return &___mGuideViewDisplayMode_31; }
	inline void set_mGuideViewDisplayMode_31(int32_t value)
	{
		___mGuideViewDisplayMode_31 = value;
	}

	inline static int32_t get_offset_of_m2DGuideViewMaterial_32() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___m2DGuideViewMaterial_32)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m2DGuideViewMaterial_32() const { return ___m2DGuideViewMaterial_32; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m2DGuideViewMaterial_32() { return &___m2DGuideViewMaterial_32; }
	inline void set_m2DGuideViewMaterial_32(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m2DGuideViewMaterial_32 = value;
		Il2CppCodeGenWriteBarrier((&___m2DGuideViewMaterial_32), value);
	}

	inline static int32_t get_offset_of_m3DGuideViewMaterial_33() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___m3DGuideViewMaterial_33)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m3DGuideViewMaterial_33() const { return ___m3DGuideViewMaterial_33; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m3DGuideViewMaterial_33() { return &___m3DGuideViewMaterial_33; }
	inline void set_m3DGuideViewMaterial_33(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m3DGuideViewMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m3DGuideViewMaterial_33), value);
	}

	inline static int32_t get_offset_of_mSelectedGuideView_34() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mSelectedGuideView_34)); }
	inline String_t* get_mSelectedGuideView_34() const { return ___mSelectedGuideView_34; }
	inline String_t** get_address_of_mSelectedGuideView_34() { return &___mSelectedGuideView_34; }
	inline void set_mSelectedGuideView_34(String_t* value)
	{
		___mSelectedGuideView_34 = value;
		Il2CppCodeGenWriteBarrier((&___mSelectedGuideView_34), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_35() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mLastTransformScale_35)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mLastTransformScale_35() const { return ___mLastTransformScale_35; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mLastTransformScale_35() { return &___mLastTransformScale_35; }
	inline void set_mLastTransformScale_35(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mLastTransformScale_35 = value;
	}

	inline static int32_t get_offset_of_mLastSize_36() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mLastSize_36)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mLastSize_36() const { return ___mLastSize_36; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mLastSize_36() { return &___mLastSize_36; }
	inline void set_mLastSize_36(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mLastSize_36 = value;
	}

	inline static int32_t get_offset_of_mDataSetIsActive_37() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mDataSetIsActive_37)); }
	inline bool get_mDataSetIsActive_37() const { return ___mDataSetIsActive_37; }
	inline bool* get_address_of_mDataSetIsActive_37() { return &___mDataSetIsActive_37; }
	inline void set_mDataSetIsActive_37(bool value)
	{
		___mDataSetIsActive_37 = value;
	}

	inline static int32_t get_offset_of_mModelRecoBehaviour_38() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0, ___mModelRecoBehaviour_38)); }
	inline ModelRecoBehaviour_t136409F3AECD7AEB24CC39E5CA3A50A4CAAF2291 * get_mModelRecoBehaviour_38() const { return ___mModelRecoBehaviour_38; }
	inline ModelRecoBehaviour_t136409F3AECD7AEB24CC39E5CA3A50A4CAAF2291 ** get_address_of_mModelRecoBehaviour_38() { return &___mModelRecoBehaviour_38; }
	inline void set_mModelRecoBehaviour_38(ModelRecoBehaviour_t136409F3AECD7AEB24CC39E5CA3A50A4CAAF2291 * value)
	{
		___mModelRecoBehaviour_38 = value;
		Il2CppCodeGenWriteBarrier((&___mModelRecoBehaviour_38), value);
	}
};

struct ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0_StaticFields
{
public:
	// System.String Vuforia.ModelTargetBehaviour::GUIDE_VIEW_RENDERER_NAME
	String_t* ___GUIDE_VIEW_RENDERER_NAME_14;

public:
	inline static int32_t get_offset_of_GUIDE_VIEW_RENDERER_NAME_14() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0_StaticFields, ___GUIDE_VIEW_RENDERER_NAME_14)); }
	inline String_t* get_GUIDE_VIEW_RENDERER_NAME_14() const { return ___GUIDE_VIEW_RENDERER_NAME_14; }
	inline String_t** get_address_of_GUIDE_VIEW_RENDERER_NAME_14() { return &___GUIDE_VIEW_RENDERER_NAME_14; }
	inline void set_GUIDE_VIEW_RENDERER_NAME_14(String_t* value)
	{
		___GUIDE_VIEW_RENDERER_NAME_14 = value;
		Il2CppCodeGenWriteBarrier((&___GUIDE_VIEW_RENDERER_NAME_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELTARGETBEHAVIOUR_T283F7A0B136589E033A458B5FE0C42F3248CE0B0_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (Mode_tFAC11BEAD3F06C10D033DA80E27982CADB4008C8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3300[3] = 
{
	Mode_tFAC11BEAD3F06C10D033DA80E27982CADB4008C8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (ExternalVRDeviceCameraConfiguration_t22E28FE2FAB2D078AC355155BECEF402BEB11EA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3301[1] = 
{
	ExternalVRDeviceCameraConfiguration_t22E28FE2FAB2D078AC355155BECEF402BEB11EA2::get_offset_of_mLastWorldCenterMode_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3302[14] = 
{
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mCamera_9(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mMatrixStore_10(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mLeftMatrixUsedForVBPlacement_11(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mLastAppliedNearClipPlane_12(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mLastAppliedFarClipPlane_13(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mMaxDepthForVideoBackground_14(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mMinDepthForVideoBackground_15(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mScreenWidth_16(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mScreenHeight_17(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mStereoDepth_18(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mResetMatrix_19(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mVuforiaFrustumSkew_20(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mCenterToEyeAxis_21(),
	VRDeviceCameraConfiguration_t647AFB774A37819F464C5D5F01599CAD29FD7E57::get_offset_of_mVrDeviceController_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4), -1, sizeof(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3303[20] = 
{
	0,
	0,
	0,
	0,
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mCameraOffset_5(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mDistortionRenderingLayer_6(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mEyewearType_7(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mStereoFramework_8(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mSeeThroughConfiguration_9(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mViewerName_10(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mViewerManufacturer_11(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mUseCustomViewer_12(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mCustomViewer_13(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mCentralAnchorPoint_14(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mPrimaryCamera_15(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mVuforiaBehaviour_16(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mSetFocusPlaneAutomatically_17(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4::get_offset_of_mVRDeviceController_18(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4_StaticFields::get_offset_of_mInstance_19(),
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4_StaticFields::get_offset_of_mPadlock_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (EyewearType_t0DF90F97DCCF9F068455C2AF59B1758E89615C13)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3304[4] = 
{
	EyewearType_t0DF90F97DCCF9F068455C2AF59B1758E89615C13::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (StereoFramework_t95A56F9A03F0EBAFFC34ABBE8309C936859E13BA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3305[2] = 
{
	StereoFramework_t95A56F9A03F0EBAFFC34ABBE8309C936859E13BA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (SeeThroughConfiguration_tF34F5E33DC51F5EEB23599A905DD112FF00C8E34)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3306[3] = 
{
	SeeThroughConfiguration_tF34F5E33DC51F5EEB23599A905DD112FF00C8E34::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3307[11] = 
{
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_Version_0(),
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_Name_1(),
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_Manufacturer_2(),
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_ButtonType_3(),
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_ScreenToLensDistance_4(),
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_InterLensDistance_5(),
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_TrayAlignment_6(),
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_LensCenterToTrayDistance_7(),
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_DistortionCoefficients_8(),
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_FieldOfView_9(),
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9::get_offset_of_ContainsMagnet_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (UnityComponentExtensions_t9F0045DBCEFA6E3D0D61EF91365CB0A9703DF288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (AValidatableVideoBackgroundConfigProperty_t7C670179380AA43CB486E420F6BAB3B08B01F6B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3309[2] = 
{
	AValidatableVideoBackgroundConfigProperty_t7C670179380AA43CB486E420F6BAB3B08B01F6B4::get_offset_of_Config_0(),
	AValidatableVideoBackgroundConfigProperty_t7C670179380AA43CB486E420F6BAB3B08B01F6B4::get_offset_of_DefaultProvider_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (MatteShaderProperty_t135A3774A258638E6E504EC3C593105CD140D2FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (NumDivisionsProperty_t256C036FE34F79188C444C116986E14905657527), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (VideoBackgroundConfigValidator_tC6D2432439D8AAAF1E72C8124FC620504DC11EB0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3312[1] = 
{
	VideoBackgroundConfigValidator_tC6D2432439D8AAAF1E72C8124FC620504DC11EB0::get_offset_of_mValidatableProperties_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (U3CU3Ec__DisplayClass2_0_tDA1C694A3B22700FA2A2B9A65EC6A49E864F2750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3313[1] = 
{
	U3CU3Ec__DisplayClass2_0_tDA1C694A3B22700FA2A2B9A65EC6A49E864F2750::get_offset_of_res_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (VideoBackgroundShaderProperty_t162378EC0A889704F1DA266B52C01CF58B875B49), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (EyewearDevice_t083CA7719B929A57A532B386322C61658849AA1E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[6] = 
{
	GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956::get_offset_of_mInstancePtr_0(),
	GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956::get_offset_of_mPose_1(),
	GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956::get_offset_of_mName_2(),
	GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956::get_offset_of_mCustomPose_3(),
	GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956::get_offset_of_mImage_4(),
	GuideView_t0732A5D3A8536A322937D1E3FB0DF808ED666956::get_offset_of_PropertyChanged_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[5] = 
{
	GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B::get_offset_of_mCameraAspect_4(),
	GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B::get_offset_of_mCameraFOV_5(),
	GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B::get_offset_of_mCameraNearPlane_6(),
	GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B::get_offset_of_mGuideViewTexture_7(),
	GuideView2DBehaviour_t2B9C7F5CED5A2EE6DCEF7BBE93B74C012A2EE52B::get_offset_of_mFlipImageVertically_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (GuideView3DBehaviour_tAFB7AAB771DAFC4E0CC71309E8B7B87FAC63EDC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[2] = 
{
	GuideView3DBehaviour_tAFB7AAB771DAFC4E0CC71309E8B7B87FAC63EDC0::get_offset_of_mCurrentGuideView_4(),
	GuideView3DBehaviour_tAFB7AAB771DAFC4E0CC71309E8B7B87FAC63EDC0::get_offset_of_m3DModel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[11] = 
{
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62::get_offset_of_guideReappearanceDelay_4(),
	0,
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62::get_offset_of_mTrackedTarget_6(),
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62::get_offset_of_mGuideViewDisplayMode_7(),
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62::get_offset_of_mGuideView_8(),
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62::get_offset_of_mLastActiveGuideViewIndex_9(),
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62::get_offset_of_mGuideViewInitialized_10(),
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62::get_offset_of_mShowGuideViewCoroutine_11(),
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62::get_offset_of_mGuideViewGameObject_12(),
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62::get_offset_of_mGuideViewShown_13(),
	GuideViewRenderingBehaviour_tF142A04A9437EFC6298F1D6CD1629DD8C334FD62::get_offset_of_mPrevDepthTextureMode_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (U3CShowGuideViewAfterU3Ed__23_t984B10E61E2B87AC3915A224894BB695D03C8095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[4] = 
{
	U3CShowGuideViewAfterU3Ed__23_t984B10E61E2B87AC3915A224894BB695D03C8095::get_offset_of_U3CU3E1__state_0(),
	U3CShowGuideViewAfterU3Ed__23_t984B10E61E2B87AC3915A224894BB695D03C8095::get_offset_of_U3CU3E2__current_1(),
	U3CShowGuideViewAfterU3Ed__23_t984B10E61E2B87AC3915A224894BB695D03C8095::get_offset_of_seconds_2(),
	U3CShowGuideViewAfterU3Ed__23_t984B10E61E2B87AC3915A224894BB695D03C8095::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (U3CSetChildOfVuforiaAnchorU3Ed__24_t7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3321[3] = 
{
	U3CSetChildOfVuforiaAnchorU3Ed__24_t7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49::get_offset_of_U3CU3E1__state_0(),
	U3CSetChildOfVuforiaAnchorU3Ed__24_t7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49::get_offset_of_U3CU3E2__current_1(),
	U3CSetChildOfVuforiaAnchorU3Ed__24_t7E20E29FD0DDFFE0BA0307CA487DAA93F3DD4E49::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (ModelTargetImpl_tD484936FF7A7ED1644951F50A18D4574B095480E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3323[2] = 
{
	ModelTargetImpl_tD484936FF7A7ED1644951F50A18D4574B095480E::get_offset_of_mBoundingBoxImpl_4(),
	ModelTargetImpl_tD484936FF7A7ED1644951F50A18D4574B095480E::get_offset_of_mGuideViewContainer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0), -1, sizeof(ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3326[25] = 
{
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0_StaticFields::get_offset_of_GUIDE_VIEW_RENDERER_NAME_14(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mModelTarget_15(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mGuideViewRenderer_16(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mDeviceTracker_17(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mBaseSize_18(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mAspectRatioXY_19(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mAspectRatioXZ_20(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mShowBoundingBox_21(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mOverrideSnappingPose_22(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mBBoxMin_23(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mBBoxMax_24(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mPreviewImage_25(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mLength_26(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mWidth_27(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mHeight_28(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_m3DGuideViewModel_29(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_m2DGuideViewImage_30(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mGuideViewDisplayMode_31(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_m2DGuideViewMaterial_32(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_m3DGuideViewMaterial_33(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mSelectedGuideView_34(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mLastTransformScale_35(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mLastSize_36(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mDataSetIsActive_37(),
	ModelTargetBehaviour_t283F7A0B136589E033A458B5FE0C42F3248CE0B0::get_offset_of_mModelRecoBehaviour_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (GuideViewDisplayMode_t88D0866B25C697FC57941C3608F5C0C58928533D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3327[4] = 
{
	GuideViewDisplayMode_t88D0866B25C697FC57941C3608F5C0C58928533D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (GLErrorHandler_t0E814EDCBE6293FD3D378715730ED229429AB93A), -1, sizeof(GLErrorHandler_t0E814EDCBE6293FD3D378715730ED229429AB93A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3328[3] = 
{
	GLErrorHandler_t0E814EDCBE6293FD3D378715730ED229429AB93A_StaticFields::get_offset_of_mErrorText_4(),
	GLErrorHandler_t0E814EDCBE6293FD3D378715730ED229429AB93A_StaticFields::get_offset_of_mErrorOccurred_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (MaskOutBehaviour_t1D183DD82AF2945A6334465A9E6759268AB689F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3329[1] = 
{
	MaskOutBehaviour_t1D183DD82AF2945A6334465A9E6759268AB689F4::get_offset_of_maskMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (AnchorData_tC476940A18BABB720CE1D5A2B78153B01DF407FF)+ sizeof (RuntimeObject), sizeof(AnchorData_tC476940A18BABB720CE1D5A2B78153B01DF407FF ), 0, 0 };
extern const int32_t g_FieldOffsetTable3330[2] = 
{
	AnchorData_tC476940A18BABB720CE1D5A2B78153B01DF407FF::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnchorData_tC476940A18BABB720CE1D5A2B78153B01DF407FF::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D), -1, sizeof(SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3331[3] = 
{
	SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D_StaticFields::get_offset_of_mInstance_1(),
	SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D::get_offset_of_mAutoInitAndStartTracker_2(),
	SmartTerrainARController_t71E6FA089F9760C9BA027E3808EE43568BF0BD3D::get_offset_of_mState_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (State_t3FDFF603F7E24F1BA6D3159D9F4CC7E1B4F8F4CE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3332[4] = 
{
	State_t3FDFF603F7E24F1BA6D3159D9F4CC7E1B4F8F4CE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (StageType_t365B0DEB2DCA1C8053A874CB86AAA2AD99C93AC6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3333[3] = 
{
	StageType_t365B0DEB2DCA1C8053A874CB86AAA2AD99C93AC6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (ContentPlacedEvent_t6F76CC86D8FF827E64E67C1DB8D770FB7A085997), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3335[8] = 
{
	ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B::get_offset_of_AnchorStage_4(),
	ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B::get_offset_of_DuplicateStage_5(),
	ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B::get_offset_of_OnContentPlaced_6(),
	ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B::get_offset_of_mDisplayAdvanced_7(),
	0,
	0,
	ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B::get_offset_of_mDeviceTracker_10(),
	ContentPositioningBehaviour_t8B3CF7741A8FE98EAAE4210540CDA194E4379B5B::get_offset_of_mInstantiatedAnchors_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (U3CU3Ec__DisplayClass9_0_t1277B477D9D8C49B826E61FAE04E9D10516278C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3336[2] = 
{
	U3CU3Ec__DisplayClass9_0_t1277B477D9D8C49B826E61FAE04E9D10516278C4::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass9_0_t1277B477D9D8C49B826E61FAE04E9D10516278C4::get_offset_of_pose_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (U3CU3Ec__DisplayClass10_0_tBA0EADD82169074AB68C3D86F7B4C4D898AEB74B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3337[2] = 
{
	U3CU3Ec__DisplayClass10_0_tBA0EADD82169074AB68C3D86F7B4C4D898AEB74B::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass10_0_tBA0EADD82169074AB68C3D86F7B4C4D898AEB74B::get_offset_of_hitTestResult_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (U3CU3Ec__DisplayClass14_0_tF0169B1436EE9D9EDC1FFE5C6D70D8B55F3CFBCF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3338[1] = 
{
	U3CU3Ec__DisplayClass14_0_tF0169B1436EE9D9EDC1FFE5C6D70D8B55F3CFBCF::get_offset_of_anchor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (AnchorImpl_t187011BD4C3999DD142AA584FB40EDB48A4794DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3339[2] = 
{
	AnchorImpl_t187011BD4C3999DD142AA584FB40EDB48A4794DC::get_offset_of_mName_0(),
	AnchorImpl_t187011BD4C3999DD142AA584FB40EDB48A4794DC::get_offset_of_mId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3340[5] = 
{
	MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0::get_offset_of_DistanceToCamera_4(),
	MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0::get_offset_of_MidAirIndicator_5(),
	MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0::get_offset_of_OnAnchorPositionConfirmed_6(),
	MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0::get_offset_of_mCamera_7(),
	MidAirPositionerBehaviour_t1A3E8D1CA52D8CD157FA40A50045FE02FFDD12B0::get_offset_of_mDisplayAdvanced_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (AnchorPositionConfirmedEvent_t63B8BA26EFA42FC76914E18C5AF9E3E9223B5093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (AnchorInputListenerBehaviour_tF944EDC39D85497FCE2C5AF520164C0B759EA404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3342[2] = 
{
	AnchorInputListenerBehaviour_tF944EDC39D85497FCE2C5AF520164C0B759EA404::get_offset_of_OnInputReceivedEvent_4(),
	AnchorInputListenerBehaviour_tF944EDC39D85497FCE2C5AF520164C0B759EA404::get_offset_of_mDisplayAdvanced_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (InputReceivedEvent_t30AD5C108997667B53F1C6C844EF0B80096BD49C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (HitTestEvent_t63AB9C2D85E4DB67BA4AEF13B8E65862E90FED4F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (HitTestMode_tA27F6512F1F271CA16946F6B0D654F766469973B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3345[3] = 
{
	HitTestMode_tA27F6512F1F271CA16946F6B0D654F766469973B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3)+ sizeof (RuntimeObject), sizeof(HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3346[4] = 
{
	HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3::get_offset_of_HitTestResultPtr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3::get_offset_of_Orientation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3::get_offset_of_Position_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HitTestResultData_t67DDE76CFB43E7C2EF3EA7CA447F2F66C0201FE3::get_offset_of_Unused_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (PlaymodeSmartTerrainImpl_tFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3347[3] = 
{
	PlaymodeSmartTerrainImpl_tFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B::get_offset_of_mEmulatorPlaneSize_1(),
	PlaymodeSmartTerrainImpl_tFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B::get_offset_of_mEmulatorGroundPlane_2(),
	PlaymodeSmartTerrainImpl_tFC4B251FFDB1C30DC842D1422F07BD37CCCF8C8B::get_offset_of_mGroundPlaneImageTarget_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C), -1, sizeof(U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3348[3] = 
{
	U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
	U3CU3Ec_t2255C26323B70789F1DEDBCAA38CD03AB66B600C_StaticFields::get_offset_of_U3CU3E9__7_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (SmartTerrain_t09E2ED8DC63BA7169F0DCDCC1BA4419BDB878E47), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3350[3] = 
{
	HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE::get_offset_of_mPtr_0(),
	HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE::get_offset_of_mPosition_1(),
	HitTestResult_t53FC02E2A830035AD9ED88C46D9975FD1B5C59DE::get_offset_of_mOrientation_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3351[11] = 
{
	PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9::get_offset_of_PlaneIndicator_4(),
	PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9::get_offset_of_Height_5(),
	PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9::get_offset_of_OnInteractiveHitTest_6(),
	PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9::get_offset_of_OnAutomaticHitTest_7(),
	PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9::get_offset_of_mSmartTerrain_8(),
	PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9::get_offset_of_mViewportCenter_9(),
	PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9::get_offset_of_mLastUpdate_10(),
	PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9::get_offset_of_mPreviouslyVisible_11(),
	0,
	PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9::get_offset_of_mDisplayAdvanced_13(),
	PlaneFinderBehaviour_t217EBD6AC6946BB53BF52F3525158C0BD4B4EDB9::get_offset_of_mHitTestMode_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (U3CU3Ec__DisplayClass29_0_t409774273F602E27B578D7F5EB161A47FC3B0980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[1] = 
{
	U3CU3Ec__DisplayClass29_0_t409774273F602E27B578D7F5EB161A47FC3B0980::get_offset_of_isVisible_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (SmartTerrainImpl_tF475FBE8773C95101B5CB6A85B1F31D982402340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (NullHoloLensApiAbstraction_tA2A1D3CFDA4B08ADA1A6C72DCE50CBE9C148DEAD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (DeviceTracker_tD6E28B77342C2CBE99C14112AFA51C0798EC3086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (TRACKING_MODE_t2ABEC66C8561190920E2F2637F28B37437D2E5E0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3358[3] = 
{
	TRACKING_MODE_t2ABEC66C8561190920E2F2637F28B37437D2E5E0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6), -1, sizeof(DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3359[16] = 
{
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6_StaticFields::get_offset_of_DEFAULT_HEAD_PIVOT_1(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6_StaticFields::get_offset_of_DEFAULT_HANDHELD_PIVOT_2(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mTrackingMode_3(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mAutoInitTracker_4(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mAutoStartTracker_5(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mPosePrediction_6(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mModelCorrectionMode_7(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mModelTransformEnabled_8(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mModelTransform_9(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mTrackerStarted_10(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mTrackerWasActiveBeforePause_11(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mTrackerWasActiveBeforeDisabling_12(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mEmulatorPositionVelocity_13(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6::get_offset_of_mDeviceTrackerConfiguration_14(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6_StaticFields::get_offset_of_mInstance_15(),
	DeviceTrackerARController_t144EF27DF432AA01FA50AA677B1EBFC37116BBC6_StaticFields::get_offset_of_mPadlock_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (ObjectTargetImpl_tEDDDACB8EF83100B0896957801DF1F85DB6A126A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3360[1] = 
{
	ObjectTargetImpl_tEDDDACB8EF83100B0896957801DF1F85DB6A126A::get_offset_of_mTargetSizeImpl_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (DataSetTargetSize_t8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3362[3] = 
{
	DataSetTargetSize_t8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517::get_offset_of_mSize_0(),
	DataSetTargetSize_t8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517::get_offset_of_mName_1(),
	DataSetTargetSize_t8B732BB6F36DEB1B2232D34B1E1425BD3FFAE517::get_offset_of_mDataSet_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3363[2] = 
{
	CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031::get_offset_of_mInvokeBeforeNativeCall_3(),
	CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031::get_offset_of_mSetSizeAction_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (DisabledSetTargetSize_tED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3364[1] = 
{
	DisabledSetTargetSize_tED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA::get_offset_of_mSize_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (TurnOffBehaviour_t086F35AFC9757F6CB78FB7F1387D531C419C4703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (FusionProviderType_t0FD88A0D7F1544448DAEF1A9EAB5FBB591A495C2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3366[6] = 
{
	FusionProviderType_t0FD88A0D7F1544448DAEF1A9EAB5FBB591A495C2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (PIXEL_FORMAT_t2C0F763FCD84C633340917492F06896787FD9383)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3367[11] = 
{
	PIXEL_FORMAT_t2C0F763FCD84C633340917492F06896787FD9383::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3368[6] = 
{
	ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307::get_offset_of_U3CWidthU3Ek__BackingField_0(),
	ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307::get_offset_of_U3CHeightU3Ek__BackingField_1(),
	ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307::get_offset_of_U3CStrideU3Ek__BackingField_2(),
	ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307::get_offset_of_U3CBufferWidthU3Ek__BackingField_3(),
	ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307::get_offset_of_U3CBufferHeightU3Ek__BackingField_4(),
	ImageDescription_t8C30B261E58382BB9D3D3399113CB0478EA2E307::get_offset_of_U3CPixelFormatU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (PixelFormatExtensions_t55FFDEBBF53782357AAF85CCF49F8F3A1E0A5558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (IEnumerableExtensionMethods_t1C224FF0D4BE50ABEFC3BA491E7DE9595D314E74), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (DelegateHelper_t027CD1F95062C9C98103DBB523743A9BD59F91DF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (PlayModeEyewearDevice_t23F975E41EB03A9E26FD9408058C481B8550950F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (DedicatedEyewearDevice_t636A2A6A914BA544F7C74C9E1C34565A0838B400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44), -1, sizeof(CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3374[10] = 
{
	CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields::get_offset_of_MIN_CENTER_0(),
	CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields::get_offset_of_MAX_CENTER_1(),
	CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields::get_offset_of_MIN_BOTTOM_2(),
	CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields::get_offset_of_MIN_TOP_3(),
	CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields::get_offset_of_MIN_LEFT_4(),
	CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields::get_offset_of_MIN_RIGHT_5(),
	CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields::get_offset_of_MAX_BOTTOM_6(),
	CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields::get_offset_of_MAX_TOP_7(),
	CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields::get_offset_of_MAX_LEFT_8(),
	CameraConfigurationUtility_tFDDDF578E2D5422A9997F43A0658E6AB085EAA44_StaticFields::get_offset_of_MAX_RIGHT_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3375[9] = 
{
	BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A::get_offset_of_mCameraDeviceMode_0(),
	BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A::get_offset_of_mOnVideoBackgroundConfigChanged_1(),
	BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A::get_offset_of_mVideoBackgroundBehaviour_2(),
	BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A::get_offset_of_mVideoBackgroundViewportRect_3(),
	BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A::get_offset_of_mRenderVideoBackground_4(),
	BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A::get_offset_of_mProjectionOrientation_5(),
	BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A::get_offset_of_mBackgroundPlaneBehaviour_6(),
	BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A::get_offset_of_mDeviceModeChanged_7(),
	BaseCameraConfiguration_tF625F5FF38D44D6E2D1D929C1F29DA533F97642A::get_offset_of_mProjectionParamsChanged_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (VuforiaExtendedTrackingManager_tAACFA22C32AEA72AA454280FB0E854CE8FF6D383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (TrackerData_tE9DAA69F73C99DF652AAA629FFB9418FC62EBDE1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B)+ sizeof (RuntimeObject), sizeof(PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B ), 0, 0 };
extern const int32_t g_FieldOffsetTable3380[3] = 
{
	PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseData_tBEB2E3213824EA43B0606A888A09A32D6433881B::get_offset_of_unused_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA)+ sizeof (RuntimeObject), sizeof(TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA ), 0, 0 };
extern const int32_t g_FieldOffsetTable3381[6] = 
{
	TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA::get_offset_of_statusInfo_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA::get_offset_of_id_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableResultData_t8F9DBC763DE9DD5D9C7EB55F00ADABC5B2C1FBFA::get_offset_of_unused_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (VirtualButtonData_tF16C663C156A49F65553E38299D185C298EFB1BF)+ sizeof (RuntimeObject), sizeof(VirtualButtonData_tF16C663C156A49F65553E38299D185C298EFB1BF ), 0, 0 };
extern const int32_t g_FieldOffsetTable3382[2] = 
{
	VirtualButtonData_tF16C663C156A49F65553E38299D185C298EFB1BF::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VirtualButtonData_tF16C663C156A49F65553E38299D185C298EFB1BF::get_offset_of_isPressed_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121)+ sizeof (RuntimeObject), sizeof(ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3383[9] = 
{
	ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121::get_offset_of_data_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121::get_offset_of_width_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121::get_offset_of_height_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121::get_offset_of_stride_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121::get_offset_of_bufferWidth_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121::get_offset_of_bufferHeight_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121::get_offset_of_format_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121::get_offset_of_reallocate_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_tFC0673E37281D53EB3031F0A59652D09EEE08121::get_offset_of_updated_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2)+ sizeof (RuntimeObject), sizeof(InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3384[5] = 
{
	InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2::get_offset_of_numericValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2::get_offset_of_buffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2::get_offset_of_reserved_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2::get_offset_of_dataLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InstanceIdData_tA9961D073D40B8F890FC58FACDB0E3BC89E2CBA2::get_offset_of_dataType_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81)+ sizeof (RuntimeObject), sizeof(VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3385[5] = 
{
	VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81::get_offset_of_instanceId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81::get_offset_of_id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81::get_offset_of_templateId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81::get_offset_of_size_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetData_t634A3794662DF21263248A3F7E1A1E77E3085F81::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE)+ sizeof (RuntimeObject), sizeof(VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE ), 0, 0 };
extern const int32_t g_FieldOffsetTable3386[6] = 
{
	VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE::get_offset_of_targetID_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE::get_offset_of_resultID_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetResultData_tD7EC910F62A1C9C07A914277BA322562E242B8FE::get_offset_of_unused_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C)+ sizeof (RuntimeObject), sizeof(IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C ), 0, 0 };
extern const int32_t g_FieldOffsetTable3387[4] = 
{
	IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C::get_offset_of_ambientIntensity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C::get_offset_of_ambientColorTemperature_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C::get_offset_of_intensityCorrection_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IlluminationData_tD92AC3353F4A03A75AC592B597C88238195BF12C::get_offset_of_colorCorrection_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0)+ sizeof (RuntimeObject), sizeof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3388[13] = 
{
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_trackableDataArray_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_vbDataArray_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_vuMarkResultArray_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_newVuMarkDataArray_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_illuminationData_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_numTrackableResults_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_numVirtualButtonResults_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_frameIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_numVuMarkResults_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_numNewVuMarks_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_deviceTrackableId_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_deviceTrackableStatusInfo_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0::get_offset_of_minCameraCalibration_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (StructList_t838B13CFE20420E6A031EF2069821BF66948F28F)+ sizeof (RuntimeObject), sizeof(StructList_t838B13CFE20420E6A031EF2069821BF66948F28F ), 0, 0 };
extern const int32_t g_FieldOffsetTable3389[3] = 
{
	StructList_t838B13CFE20420E6A031EF2069821BF66948F28F::get_offset_of_arrayPtr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StructList_t838B13CFE20420E6A031EF2069821BF66948F28F::get_offset_of_numResults_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StructList_t838B13CFE20420E6A031EF2069821BF66948F28F::get_offset_of_unused_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[5] = 
{
	InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371::get_offset_of_mDataType_0(),
	InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371::get_offset_of_mBuffer_1(),
	InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371::get_offset_of_mNumericValue_2(),
	InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371::get_offset_of_mDataLength_3(),
	InstanceIdImpl_tB528D9E690D089FBF42027CEF1983B298A9C9371::get_offset_of_mCachedStringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (VuMarkTargetImpl_t6455B455669A18BE0FCA10863C50CB61F7043A91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3391[2] = 
{
	VuMarkTargetImpl_t6455B455669A18BE0FCA10863C50CB61F7043A91::get_offset_of_mVuMarkTemplate_3(),
	VuMarkTargetImpl_t6455B455669A18BE0FCA10863C50CB61F7043A91::get_offset_of_mInstanceId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (VuMarkSetTargetSize_tA607278FCA4215B58CB8D03F1698A1A75B52E321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3392[1] = 
{
	VuMarkSetTargetSize_tA607278FCA4215B58CB8D03F1698A1A75B52E321::get_offset_of_mTemplate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3393[2] = 
{
	VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8::get_offset_of_mOrigin_4(),
	VuMarkTemplateImpl_t70BFD60A41858A19817826C4468AA5EE736E4ED8::get_offset_of_mTrackingFromRuntimeAppearance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (RotationalDeviceTracker_tFDD94B013A18CCDE7AED9F6A13C8147742E66DEA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (MODEL_CORRECTION_MODE_tA628D33CBBD6F312044315FED05B471AD6AEA17E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3395[4] = 
{
	MODEL_CORRECTION_MODE_tA628D33CBBD6F312044315FED05B471AD6AEA17E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (CustomViewerParameters_t5F3D3A911D28E3D98760A6BCA1F2E984542E2075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3397[8] = 
{
	DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A::get_offset_of_mDeviceTrackerPositonOffset_0(),
	DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A::get_offset_of_mDeviceTrackerRotationOffset_1(),
	DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A::get_offset_of_mBeforeDevicePoseUpdated_2(),
	DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A::get_offset_of_mAfterDevicePoseUpdated_3(),
	DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A::get_offset_of_mStatusChanged_4(),
	DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A::get_offset_of_mStatusOrInfoChanged_5(),
	DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A::get_offset_of_mCurrentTrackableStatus_6(),
	DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A::get_offset_of_mCurrentTrackableStatusInfo_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338), -1, sizeof(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3398[16] = 
{
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mTextureSize_4(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mImageSize_5(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mCameraTransform_6(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338_StaticFields::get_offset_of_maxDisplacement_7(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mMesh_8(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mStereoDepth_9(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mProjectionMatrixSetExternally_10(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mBackgroundOffset_11(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mVuforiaBehaviour_12(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mBackgroundPlacedCallback_13(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mValidator_14(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mProjectMatrixProvider_15(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mNumFramesToUpdateVideoBg_16(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mLastUsedProjectioMatrix_17(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mNumDivisions_18(),
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338::get_offset_of_mHideExcessAreaUtility_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018)+ sizeof (RuntimeObject), sizeof(ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3399[7] = 
{
	ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018::get_offset_of_ProjectionMatrix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018::get_offset_of_InverseMatrix_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018::get_offset_of_HorizontalFoV_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018::get_offset_of_VerticalFoV_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018::get_offset_of_CenterEyeRayFrom_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018::get_offset_of_CenterEyeRayTo_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProjectionMatrixData_t68F1EB028D7C3DDEE47E84B50FC93F6495CCD018::get_offset_of_MaxDepth_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
