﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// CommandTerminal.CommandAutocomplete
struct CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527;
// CommandTerminal.CommandHistory
struct CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994;
// CommandTerminal.CommandLog
struct CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03;
// CommandTerminal.CommandShell
struct CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811;
// DLLCore.Product
struct Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C;
// DLLCore.UnitComponents.Unit
struct Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D;
// LinkedSet`1<Outline>
struct LinkedSet_1_tF77FC771D12DBA9E848B2192E7A6A776AA24BACA;
// System.Action`1<CommandTerminal.CommandArg[]>
struct Action_1_t72CC68949B9675FE3448E73FB5593A8F0272A96B;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_tCB8FD71FF95FF2996E13313CC98AD98CBA243D60;
// System.Collections.Generic.Dictionary`2<System.String,CommandTerminal.CommandArg>
struct Dictionary_2_t725909DB837149B01DE4F53BFE547BEDC576FB43;
// System.Collections.Generic.Dictionary`2<System.String,CommandTerminal.CommandInfo>
struct Dictionary_2_tE92558D5E7E11FC90D2673F94867CF5ED9AF761B;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Events.UnityEvent>
struct Dictionary_2_t144E26F2FE60EC50B62AC7C9425BF26CB5CE269F;
// System.Collections.Generic.List`1<CommandTerminal.CommandArg>
struct List_1_t3C6BF0661351EF776AE02CCD1842DF9278C69694;
// System.Collections.Generic.List`1<CommandTerminal.LogItem>
struct List_1_tA11395D0D93D834BDC1BAB96D70D321DA6ED6DC9;
// System.Collections.Generic.List`1<DLLCore.ChangeColorCustomBoxCmd>
struct List_1_t1655834406C14992F14A10174AA79CD92C544459;
// System.Collections.Generic.List`1<DLLCore.RemoveProductCmd>
struct List_1_tFFB210EC4337C96C385A54CCD6121FE9B11A5CDA;
// System.Collections.Generic.List`1<DLLCore.TransformProductCmd>
struct List_1_t9F69BC68304CCD8A2998F704D14096654CBB28E8;
// System.Collections.Generic.List`1<JSONObject>
struct List_1_t1630C08B48AE897B94285B7FBC907DE65EBC5032;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_tB4444E94203E77FC813B61173EB21B8410EB7155;
// System.Collections.Generic.List`1<UnityEngine.MeshRenderer>
struct List_1_t8AAAE01B52C46C2908BE55FF2B1A8170BADE6833;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Diagnostics.Stopwatch
struct Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4;
// System.Func`2<OutlineEffect,System.Boolean>
struct Func_2_t33797B6949FFF9C5087A12D1F4931E2DFAD4AC8A;
// System.Func`2<UnityEngine.Camera,OutlineEffect>
struct Func_2_t770A9839757921C80B34ED7AD02139D4AC076371;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.Cubemap
struct Cubemap_tBFAC336F35E8D7499397F07A41505BD98F4491AF;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Gradient
struct Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t06431062CF438D12908F0B93305795CB645DCCA8;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.Renderer[]
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Skybox
struct Skybox_tCAAEF216A36B4CD9FD498E4EED256EAE7B81DDDD;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextEditor
struct TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_TEB1E4002BF64622D06B314BD39D49373F350A28A_H
#define U3CMODULEU3E_TEB1E4002BF64622D06B314BD39D49373F350A28A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tEB1E4002BF64622D06B314BD39D49373F350A28A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TEB1E4002BF64622D06B314BD39D49373F350A28A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COMMANDAUTOCOMPLETE_T36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527_H
#define COMMANDAUTOCOMPLETE_T36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.CommandAutocomplete
struct  CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> CommandTerminal.CommandAutocomplete::a
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___a_0;
	// System.Collections.Generic.List`1<System.String> CommandTerminal.CommandAutocomplete::b
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527, ___a_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_a_0() const { return ___a_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527, ___b_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_b_1() const { return ___b_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDAUTOCOMPLETE_T36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527_H
#ifndef COMMANDHISTORY_T9914EDC51E0F873B6462D9C174345F15B0490994_H
#define COMMANDHISTORY_T9914EDC51E0F873B6462D9C174345F15B0490994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.CommandHistory
struct  CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> CommandTerminal.CommandHistory::a
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___a_0;
	// System.Int32 CommandTerminal.CommandHistory::b
	int32_t ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994, ___a_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_a_0() const { return ___a_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994, ___b_1)); }
	inline int32_t get_b_1() const { return ___b_1; }
	inline int32_t* get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(int32_t value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDHISTORY_T9914EDC51E0F873B6462D9C174345F15B0490994_H
#ifndef COMMANDLOG_T05960F7917F3A41127E5E7A6E0B3EC4307200C03_H
#define COMMANDLOG_T05960F7917F3A41127E5E7A6E0B3EC4307200C03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.CommandLog
struct  CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<CommandTerminal.LogItem> CommandTerminal.CommandLog::a
	List_1_tA11395D0D93D834BDC1BAB96D70D321DA6ED6DC9 * ___a_0;
	// System.Int32 CommandTerminal.CommandLog::b
	int32_t ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03, ___a_0)); }
	inline List_1_tA11395D0D93D834BDC1BAB96D70D321DA6ED6DC9 * get_a_0() const { return ___a_0; }
	inline List_1_tA11395D0D93D834BDC1BAB96D70D321DA6ED6DC9 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(List_1_tA11395D0D93D834BDC1BAB96D70D321DA6ED6DC9 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03, ___b_1)); }
	inline int32_t get_b_1() const { return ___b_1; }
	inline int32_t* get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(int32_t value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDLOG_T05960F7917F3A41127E5E7A6E0B3EC4307200C03_H
#ifndef COMMANDSHELL_T509DD5DF61641E83DB3BF4DBCBE22D40122ED811_H
#define COMMANDSHELL_T509DD5DF61641E83DB3BF4DBCBE22D40122ED811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.CommandShell
struct  CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,CommandTerminal.CommandInfo> CommandTerminal.CommandShell::a
	Dictionary_2_tE92558D5E7E11FC90D2673F94867CF5ED9AF761B * ___a_0;
	// System.Collections.Generic.Dictionary`2<System.String,CommandTerminal.CommandArg> CommandTerminal.CommandShell::b
	Dictionary_2_t725909DB837149B01DE4F53BFE547BEDC576FB43 * ___b_1;
	// System.Collections.Generic.List`1<CommandTerminal.CommandArg> CommandTerminal.CommandShell::c
	List_1_t3C6BF0661351EF776AE02CCD1842DF9278C69694 * ___c_2;
	// System.String CommandTerminal.CommandShell::d
	String_t* ___d_3;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811, ___a_0)); }
	inline Dictionary_2_tE92558D5E7E11FC90D2673F94867CF5ED9AF761B * get_a_0() const { return ___a_0; }
	inline Dictionary_2_tE92558D5E7E11FC90D2673F94867CF5ED9AF761B ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Dictionary_2_tE92558D5E7E11FC90D2673F94867CF5ED9AF761B * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811, ___b_1)); }
	inline Dictionary_2_t725909DB837149B01DE4F53BFE547BEDC576FB43 * get_b_1() const { return ___b_1; }
	inline Dictionary_2_t725909DB837149B01DE4F53BFE547BEDC576FB43 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Dictionary_2_t725909DB837149B01DE4F53BFE547BEDC576FB43 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811, ___c_2)); }
	inline List_1_t3C6BF0661351EF776AE02CCD1842DF9278C69694 * get_c_2() const { return ___c_2; }
	inline List_1_t3C6BF0661351EF776AE02CCD1842DF9278C69694 ** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(List_1_t3C6BF0661351EF776AE02CCD1842DF9278C69694 * value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811, ___d_3)); }
	inline String_t* get_d_3() const { return ___d_3; }
	inline String_t** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(String_t* value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDSHELL_T509DD5DF61641E83DB3BF4DBCBE22D40122ED811_H
#ifndef COMMAND_TB160475765D7EACBCEC6D380382F8C9ACDEFF8AA_H
#define COMMAND_TB160475765D7EACBCEC6D380382F8C9ACDEFF8AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.Command
struct  Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMAND_TB160475765D7EACBCEC6D380382F8C9ACDEFF8AA_H
#ifndef CUSTOMEXTENSIONS_TDB5543760418AA0C993F99F697CD258EAC9A1C02_H
#define CUSTOMEXTENSIONS_TDB5543760418AA0C993F99F697CD258EAC9A1C02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.CustomExtensions
struct  CustomExtensions_tDB5543760418AA0C993F99F697CD258EAC9A1C02  : public RuntimeObject
{
public:

public:
};

struct CustomExtensions_tDB5543760418AA0C993F99F697CD258EAC9A1C02_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> DLLCore.CustomExtensions::materialOpaqueDictionary
	Dictionary_2_tCB8FD71FF95FF2996E13313CC98AD98CBA243D60 * ___materialOpaqueDictionary_0;

public:
	inline static int32_t get_offset_of_materialOpaqueDictionary_0() { return static_cast<int32_t>(offsetof(CustomExtensions_tDB5543760418AA0C993F99F697CD258EAC9A1C02_StaticFields, ___materialOpaqueDictionary_0)); }
	inline Dictionary_2_tCB8FD71FF95FF2996E13313CC98AD98CBA243D60 * get_materialOpaqueDictionary_0() const { return ___materialOpaqueDictionary_0; }
	inline Dictionary_2_tCB8FD71FF95FF2996E13313CC98AD98CBA243D60 ** get_address_of_materialOpaqueDictionary_0() { return &___materialOpaqueDictionary_0; }
	inline void set_materialOpaqueDictionary_0(Dictionary_2_tCB8FD71FF95FF2996E13313CC98AD98CBA243D60 * value)
	{
		___materialOpaqueDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___materialOpaqueDictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMEXTENSIONS_TDB5543760418AA0C993F99F697CD258EAC9A1C02_H
#ifndef MATHEXTENSIONS_T940FB68F17E977C6019FD5ECD6732F381E1EA7CA_H
#define MATHEXTENSIONS_T940FB68F17E977C6019FD5ECD6732F381E1EA7CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MathExtensions
struct  MathExtensions_t940FB68F17E977C6019FD5ECD6732F381E1EA7CA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHEXTENSIONS_T940FB68F17E977C6019FD5ECD6732F381E1EA7CA_H
#ifndef GAMEOBJECTEXTENSIONS_T68EE1EA4DF6A44FF56DD25E2CB497AE73634E93C_H
#define GAMEOBJECTEXTENSIONS_T68EE1EA4DF6A44FF56DD25E2CB497AE73634E93C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameObjectExtensions
struct  GameObjectExtensions_t68EE1EA4DF6A44FF56DD25E2CB497AE73634E93C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSIONS_T68EE1EA4DF6A44FF56DD25E2CB497AE73634E93C_H
#ifndef U3CU3EC_T2432C58F806854EB5C36683E4414157BC67E3D92_H
#define U3CU3EC_T2432C58F806854EB5C36683E4414157BC67E3D92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outline_<>c
struct  U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields
{
public:
	// Outline_<>c Outline_<>c::<>9
	U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Camera,OutlineEffect> Outline_<>c::<>9__5_0
	Func_2_t770A9839757921C80B34ED7AD02139D4AC076371 * ___U3CU3E9__5_0_1;
	// System.Func`2<OutlineEffect,System.Boolean> Outline_<>c::<>9__5_1
	Func_2_t33797B6949FFF9C5087A12D1F4931E2DFAD4AC8A * ___U3CU3E9__5_1_2;
	// System.Func`2<UnityEngine.Camera,OutlineEffect> Outline_<>c::<>9__6_0
	Func_2_t770A9839757921C80B34ED7AD02139D4AC076371 * ___U3CU3E9__6_0_3;
	// System.Func`2<OutlineEffect,System.Boolean> Outline_<>c::<>9__6_1
	Func_2_t33797B6949FFF9C5087A12D1F4931E2DFAD4AC8A * ___U3CU3E9__6_1_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields, ___U3CU3E9__5_0_1)); }
	inline Func_2_t770A9839757921C80B34ED7AD02139D4AC076371 * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline Func_2_t770A9839757921C80B34ED7AD02139D4AC076371 ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(Func_2_t770A9839757921C80B34ED7AD02139D4AC076371 * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields, ___U3CU3E9__5_1_2)); }
	inline Func_2_t33797B6949FFF9C5087A12D1F4931E2DFAD4AC8A * get_U3CU3E9__5_1_2() const { return ___U3CU3E9__5_1_2; }
	inline Func_2_t33797B6949FFF9C5087A12D1F4931E2DFAD4AC8A ** get_address_of_U3CU3E9__5_1_2() { return &___U3CU3E9__5_1_2; }
	inline void set_U3CU3E9__5_1_2(Func_2_t33797B6949FFF9C5087A12D1F4931E2DFAD4AC8A * value)
	{
		___U3CU3E9__5_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields, ___U3CU3E9__6_0_3)); }
	inline Func_2_t770A9839757921C80B34ED7AD02139D4AC076371 * get_U3CU3E9__6_0_3() const { return ___U3CU3E9__6_0_3; }
	inline Func_2_t770A9839757921C80B34ED7AD02139D4AC076371 ** get_address_of_U3CU3E9__6_0_3() { return &___U3CU3E9__6_0_3; }
	inline void set_U3CU3E9__6_0_3(Func_2_t770A9839757921C80B34ED7AD02139D4AC076371 * value)
	{
		___U3CU3E9__6_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields, ___U3CU3E9__6_1_4)); }
	inline Func_2_t33797B6949FFF9C5087A12D1F4931E2DFAD4AC8A * get_U3CU3E9__6_1_4() const { return ___U3CU3E9__6_1_4; }
	inline Func_2_t33797B6949FFF9C5087A12D1F4931E2DFAD4AC8A ** get_address_of_U3CU3E9__6_1_4() { return &___U3CU3E9__6_1_4; }
	inline void set_U3CU3E9__6_1_4(Func_2_t33797B6949FFF9C5087A12D1F4931E2DFAD4AC8A * value)
	{
		___U3CU3E9__6_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2432C58F806854EB5C36683E4414157BC67E3D92_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef TRANSFORMUTILS_T2CCC4B40267B7F0D5D544ABC6134517EBAB751A1_H
#define TRANSFORMUTILS_T2CCC4B40267B7F0D5D544ABC6134517EBAB751A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransformUtils
struct  TransformUtils_t2CCC4B40267B7F0D5D544ABC6134517EBAB751A1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMUTILS_T2CCC4B40267B7F0D5D544ABC6134517EBAB751A1_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___rectTransform_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#ifndef LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#define LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD, ___m_ToRebuild_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mgU24cache0
	ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache0
	Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache1
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache2
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache3
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache4
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#ifndef LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#define LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache0
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache1
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache2
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache3
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache4
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache5
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache6
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache7
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#ifndef RECTANGULARVERTEXCLIPPER_T6C47856C4F775A5799A49A100196C2BB14C5DD91_H
#define RECTANGULARVERTEXCLIPPER_T6C47856C4F775A5799A49A100196C2BB14C5DD91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RectangularVertexClipper
struct  RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_WorldCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_WorldCorners_0;
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_CanvasCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_CanvasCorners_1;

public:
	inline static int32_t get_offset_of_m_WorldCorners_0() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91, ___m_WorldCorners_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_WorldCorners_0() const { return ___m_WorldCorners_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_WorldCorners_0() { return &___m_WorldCorners_0; }
	inline void set_m_WorldCorners_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_WorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorldCorners_0), value);
	}

	inline static int32_t get_offset_of_m_CanvasCorners_1() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91, ___m_CanvasCorners_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_CanvasCorners_1() const { return ___m_CanvasCorners_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_CanvasCorners_1() { return &___m_CanvasCorners_1; }
	inline void set_m_CanvasCorners_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_CanvasCorners_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasCorners_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGULARVERTEXCLIPPER_T6C47856C4F775A5799A49A100196C2BB14C5DD91_H
#ifndef REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#define REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3D_0)); }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast2D_2)); }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifndef U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#define U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifndef COMMANDARG_T89F4623B4FEB9EED561AB60014C25228A1027A27_H
#define COMMANDARG_T89F4623B4FEB9EED561AB60014C25228A1027A27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.CommandArg
struct  CommandArg_t89F4623B4FEB9EED561AB60014C25228A1027A27 
{
public:
	// System.String CommandTerminal.CommandArg::a
	String_t* ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(CommandArg_t89F4623B4FEB9EED561AB60014C25228A1027A27, ___a_0)); }
	inline String_t* get_a_0() const { return ___a_0; }
	inline String_t** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(String_t* value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CommandTerminal.CommandArg
struct CommandArg_t89F4623B4FEB9EED561AB60014C25228A1027A27_marshaled_pinvoke
{
	char* ___a_0;
};
// Native definition for COM marshalling of CommandTerminal.CommandArg
struct CommandArg_t89F4623B4FEB9EED561AB60014C25228A1027A27_marshaled_com
{
	Il2CppChar* ___a_0;
};
#endif // COMMANDARG_T89F4623B4FEB9EED561AB60014C25228A1027A27_H
#ifndef COMMANDINFO_T40AFE2ECD393889504D1D82A0925CE324B910328_H
#define COMMANDINFO_T40AFE2ECD393889504D1D82A0925CE324B910328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.CommandInfo
struct  CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328 
{
public:
	// System.Action`1<CommandTerminal.CommandArg[]> CommandTerminal.CommandInfo::proc
	Action_1_t72CC68949B9675FE3448E73FB5593A8F0272A96B * ___proc_0;
	// System.Int32 CommandTerminal.CommandInfo::maxArgCount
	int32_t ___maxArgCount_1;
	// System.Int32 CommandTerminal.CommandInfo::minArgCount
	int32_t ___minArgCount_2;
	// System.String CommandTerminal.CommandInfo::help
	String_t* ___help_3;
	// System.String CommandTerminal.CommandInfo::hint
	String_t* ___hint_4;

public:
	inline static int32_t get_offset_of_proc_0() { return static_cast<int32_t>(offsetof(CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328, ___proc_0)); }
	inline Action_1_t72CC68949B9675FE3448E73FB5593A8F0272A96B * get_proc_0() const { return ___proc_0; }
	inline Action_1_t72CC68949B9675FE3448E73FB5593A8F0272A96B ** get_address_of_proc_0() { return &___proc_0; }
	inline void set_proc_0(Action_1_t72CC68949B9675FE3448E73FB5593A8F0272A96B * value)
	{
		___proc_0 = value;
		Il2CppCodeGenWriteBarrier((&___proc_0), value);
	}

	inline static int32_t get_offset_of_maxArgCount_1() { return static_cast<int32_t>(offsetof(CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328, ___maxArgCount_1)); }
	inline int32_t get_maxArgCount_1() const { return ___maxArgCount_1; }
	inline int32_t* get_address_of_maxArgCount_1() { return &___maxArgCount_1; }
	inline void set_maxArgCount_1(int32_t value)
	{
		___maxArgCount_1 = value;
	}

	inline static int32_t get_offset_of_minArgCount_2() { return static_cast<int32_t>(offsetof(CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328, ___minArgCount_2)); }
	inline int32_t get_minArgCount_2() const { return ___minArgCount_2; }
	inline int32_t* get_address_of_minArgCount_2() { return &___minArgCount_2; }
	inline void set_minArgCount_2(int32_t value)
	{
		___minArgCount_2 = value;
	}

	inline static int32_t get_offset_of_help_3() { return static_cast<int32_t>(offsetof(CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328, ___help_3)); }
	inline String_t* get_help_3() const { return ___help_3; }
	inline String_t** get_address_of_help_3() { return &___help_3; }
	inline void set_help_3(String_t* value)
	{
		___help_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_3), value);
	}

	inline static int32_t get_offset_of_hint_4() { return static_cast<int32_t>(offsetof(CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328, ___hint_4)); }
	inline String_t* get_hint_4() const { return ___hint_4; }
	inline String_t** get_address_of_hint_4() { return &___hint_4; }
	inline void set_hint_4(String_t* value)
	{
		___hint_4 = value;
		Il2CppCodeGenWriteBarrier((&___hint_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CommandTerminal.CommandInfo
struct CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328_marshaled_pinvoke
{
	Il2CppMethodPointer ___proc_0;
	int32_t ___maxArgCount_1;
	int32_t ___minArgCount_2;
	char* ___help_3;
	char* ___hint_4;
};
// Native definition for COM marshalling of CommandTerminal.CommandInfo
struct CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328_marshaled_com
{
	Il2CppMethodPointer ___proc_0;
	int32_t ___maxArgCount_1;
	int32_t ___minArgCount_2;
	Il2CppChar* ___help_3;
	Il2CppChar* ___hint_4;
};
#endif // COMMANDINFO_T40AFE2ECD393889504D1D82A0925CE324B910328_H
#ifndef REGISTERCOMMANDATTRIBUTE_T17944266D35BB70793C86DF6E95601F14DBAEDA5_H
#define REGISTERCOMMANDATTRIBUTE_T17944266D35BB70793C86DF6E95601F14DBAEDA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.RegisterCommandAttribute
struct  RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 CommandTerminal.RegisterCommandAttribute::a
	int32_t ___a_0;
	// System.Int32 CommandTerminal.RegisterCommandAttribute::b
	int32_t ___b_1;
	// System.String CommandTerminal.RegisterCommandAttribute::c
	String_t* ___c_2;
	// System.String CommandTerminal.RegisterCommandAttribute::d
	String_t* ___d_3;
	// System.String CommandTerminal.RegisterCommandAttribute::e
	String_t* ___e_4;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5, ___b_1)); }
	inline int32_t get_b_1() const { return ___b_1; }
	inline int32_t* get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(int32_t value)
	{
		___b_1 = value;
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5, ___c_2)); }
	inline String_t* get_c_2() const { return ___c_2; }
	inline String_t** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(String_t* value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5, ___d_3)); }
	inline String_t* get_d_3() const { return ___d_3; }
	inline String_t** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(String_t* value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5, ___e_4)); }
	inline String_t* get_e_4() const { return ___e_4; }
	inline String_t** get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(String_t* value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier((&___e_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGISTERCOMMANDATTRIBUTE_T17944266D35BB70793C86DF6E95601F14DBAEDA5_H
#ifndef CHANGECOLORMULTICUSTOMBOXCMD_TC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2_H
#define CHANGECOLORMULTICUSTOMBOXCMD_TC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ChangeColorMultiCustomBoxCmd
struct  ChangeColorMultiCustomBoxCmd_tC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2  : public Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA
{
public:
	// System.Collections.Generic.List`1<DLLCore.ChangeColorCustomBoxCmd> DLLCore.ChangeColorMultiCustomBoxCmd::a
	List_1_t1655834406C14992F14A10174AA79CD92C544459 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(ChangeColorMultiCustomBoxCmd_tC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2, ___a_0)); }
	inline List_1_t1655834406C14992F14A10174AA79CD92C544459 * get_a_0() const { return ___a_0; }
	inline List_1_t1655834406C14992F14A10174AA79CD92C544459 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(List_1_t1655834406C14992F14A10174AA79CD92C544459 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGECOLORMULTICUSTOMBOXCMD_TC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2_H
#ifndef CHANGEMATERIALUNITCMD_TFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9_H
#define CHANGEMATERIALUNITCMD_TFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ChangeMaterialUnitCmd
struct  ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9  : public Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA
{
public:
	// System.Collections.Generic.List`1<UnityEngine.MeshRenderer> DLLCore.ChangeMaterialUnitCmd::a
	List_1_t8AAAE01B52C46C2908BE55FF2B1A8170BADE6833 * ___a_0;
	// System.String DLLCore.ChangeMaterialUnitCmd::b
	String_t* ___b_1;
	// System.String DLLCore.ChangeMaterialUnitCmd::c
	String_t* ___c_2;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9, ___a_0)); }
	inline List_1_t8AAAE01B52C46C2908BE55FF2B1A8170BADE6833 * get_a_0() const { return ___a_0; }
	inline List_1_t8AAAE01B52C46C2908BE55FF2B1A8170BADE6833 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(List_1_t8AAAE01B52C46C2908BE55FF2B1A8170BADE6833 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9, ___b_1)); }
	inline String_t* get_b_1() const { return ___b_1; }
	inline String_t** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(String_t* value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9, ___c_2)); }
	inline String_t* get_c_2() const { return ___c_2; }
	inline String_t** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(String_t* value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGEMATERIALUNITCMD_TFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9_H
#ifndef JSONREADEREXCEPTION_TD2ADF3D96D3884825DB6B37F7ADFBD46E77A817D_H
#define JSONREADEREXCEPTION_TD2ADF3D96D3884825DB6B37F7ADFBD46E77A817D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.JSONReaderException
struct  JSONReaderException_tD2ADF3D96D3884825DB6B37F7ADFBD46E77A817D  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADEREXCEPTION_TD2ADF3D96D3884825DB6B37F7ADFBD46E77A817D_H
#ifndef NOTALLOWEDPLAYERCONTROLMODEEXCEPTION_T464712422DA5748F0976008E2D09BB03F06F7FCE_H
#define NOTALLOWEDPLAYERCONTROLMODEEXCEPTION_T464712422DA5748F0976008E2D09BB03F06F7FCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.NotAllowedPlayerControlModeException
struct  NotAllowedPlayerControlModeException_t464712422DA5748F0976008E2D09BB03F06F7FCE  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTALLOWEDPLAYERCONTROLMODEEXCEPTION_T464712422DA5748F0976008E2D09BB03F06F7FCE_H
#ifndef NOTALLOWEDSDKMODEEXCEPTION_T7524021C6069CD30D91A5280310D2AF58B855ACB_H
#define NOTALLOWEDSDKMODEEXCEPTION_T7524021C6069CD30D91A5280310D2AF58B855ACB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.NotAllowedSDKModeException
struct  NotAllowedSDKModeException_t7524021C6069CD30D91A5280310D2AF58B855ACB  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTALLOWEDSDKMODEEXCEPTION_T7524021C6069CD30D91A5280310D2AF58B855ACB_H
#ifndef REMOVEMULTIPRODUCTCMD_TE18C91D6311E0A86F382026E45BA6566D25E0582_H
#define REMOVEMULTIPRODUCTCMD_TE18C91D6311E0A86F382026E45BA6566D25E0582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.RemoveMultiProductCmd
struct  RemoveMultiProductCmd_tE18C91D6311E0A86F382026E45BA6566D25E0582  : public Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA
{
public:
	// System.Collections.Generic.List`1<DLLCore.RemoveProductCmd> DLLCore.RemoveMultiProductCmd::a
	List_1_tFFB210EC4337C96C385A54CCD6121FE9B11A5CDA * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(RemoveMultiProductCmd_tE18C91D6311E0A86F382026E45BA6566D25E0582, ___a_0)); }
	inline List_1_tFFB210EC4337C96C385A54CCD6121FE9B11A5CDA * get_a_0() const { return ___a_0; }
	inline List_1_tFFB210EC4337C96C385A54CCD6121FE9B11A5CDA ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(List_1_tFFB210EC4337C96C385A54CCD6121FE9B11A5CDA * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOVEMULTIPRODUCTCMD_TE18C91D6311E0A86F382026E45BA6566D25E0582_H
#ifndef REMOVEPRODUCTCMD_T7682BFA01F52AB1FB6D3035DF130ED79DC63F9AF_H
#define REMOVEPRODUCTCMD_T7682BFA01F52AB1FB6D3035DF130ED79DC63F9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.RemoveProductCmd
struct  RemoveProductCmd_t7682BFA01F52AB1FB6D3035DF130ED79DC63F9AF  : public Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA
{
public:
	// DLLCore.Product DLLCore.RemoveProductCmd::a
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * ___a_0;
	// System.String DLLCore.RemoveProductCmd::b
	String_t* ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(RemoveProductCmd_t7682BFA01F52AB1FB6D3035DF130ED79DC63F9AF, ___a_0)); }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * get_a_0() const { return ___a_0; }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(RemoveProductCmd_t7682BFA01F52AB1FB6D3035DF130ED79DC63F9AF, ___b_1)); }
	inline String_t* get_b_1() const { return ___b_1; }
	inline String_t** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(String_t* value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOVEPRODUCTCMD_T7682BFA01F52AB1FB6D3035DF130ED79DC63F9AF_H
#ifndef TRANSFORMMULTIPRODUCTCMD_T5018111EC13328126AE853EFB6E2BAA9D3E49059_H
#define TRANSFORMMULTIPRODUCTCMD_T5018111EC13328126AE853EFB6E2BAA9D3E49059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.TransformMultiProductCmd
struct  TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059  : public Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA
{
public:
	// System.Collections.Generic.List`1<DLLCore.TransformProductCmd> DLLCore.TransformMultiProductCmd::a
	List_1_t9F69BC68304CCD8A2998F704D14096654CBB28E8 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059, ___a_0)); }
	inline List_1_t9F69BC68304CCD8A2998F704D14096654CBB28E8 * get_a_0() const { return ___a_0; }
	inline List_1_t9F69BC68304CCD8A2998F704D14096654CBB28E8 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(List_1_t9F69BC68304CCD8A2998F704D14096654CBB28E8 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMMULTIPRODUCTCMD_T5018111EC13328126AE853EFB6E2BAA9D3E49059_H
#ifndef NOTALLOWEDPLAYERPROJECTIONMODEEXCEPTION_T8A2572169F7DCF6B25C680F2C88857D66BC7AF32_H
#define NOTALLOWEDPLAYERPROJECTIONMODEEXCEPTION_T8A2572169F7DCF6B25C680F2C88857D66BC7AF32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotAllowedPlayerProjectionModeException
struct  NotAllowedPlayerProjectionModeException_t8A2572169F7DCF6B25C680F2C88857D66BC7AF32  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTALLOWEDPLAYERPROJECTIONMODEEXCEPTION_T8A2572169F7DCF6B25C680F2C88857D66BC7AF32_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#define DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef A_TF08E05BEBCCB7007505B2FDCCAC98CD9A986586E_H
#define A_TF08E05BEBCCB7007505B2FDCCAC98CD9A986586E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// a_a
struct  a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_TF08E05BEBCCB7007505B2FDCCAC98CD9A986586E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields
{
public:
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifndef TERMINALLOGTYPE_TB6DBBAEBB8CAF943A4E35E241CFBA9011B514A25_H
#define TERMINALLOGTYPE_TB6DBBAEBB8CAF943A4E35E241CFBA9011B514A25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.TerminalLogType
struct  TerminalLogType_tB6DBBAEBB8CAF943A4E35E241CFBA9011B514A25 
{
public:
	// System.Int32 CommandTerminal.TerminalLogType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TerminalLogType_tB6DBBAEBB8CAF943A4E35E241CFBA9011B514A25, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERMINALLOGTYPE_TB6DBBAEBB8CAF943A4E35E241CFBA9011B514A25_H
#ifndef TERMINALSTATE_T24F35A9E2965E4E9D9414D639A37FEC5C927EC53_H
#define TERMINALSTATE_T24F35A9E2965E4E9D9414D639A37FEC5C927EC53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.TerminalState
struct  TerminalState_t24F35A9E2965E4E9D9414D639A37FEC5C927EC53 
{
public:
	// System.Int32 CommandTerminal.TerminalState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TerminalState_t24F35A9E2965E4E9D9414D639A37FEC5C927EC53, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERMINALSTATE_T24F35A9E2965E4E9D9414D639A37FEC5C927EC53_H
#ifndef CHANGECOLORCUSTOMBOXCMD_TB3B996485420AF5714BDEF530BD7528AFF7AD8E7_H
#define CHANGECOLORCUSTOMBOXCMD_TB3B996485420AF5714BDEF530BD7528AFF7AD8E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ChangeColorCustomBoxCmd
struct  ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7  : public Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA
{
public:
	// DLLCore.Product DLLCore.ChangeColorCustomBoxCmd::a
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * ___a_0;
	// System.String DLLCore.ChangeColorCustomBoxCmd::b
	String_t* ___b_1;
	// UnityEngine.Color DLLCore.ChangeColorCustomBoxCmd::c
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___c_2;
	// UnityEngine.Color DLLCore.ChangeColorCustomBoxCmd::d
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___d_3;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7, ___a_0)); }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * get_a_0() const { return ___a_0; }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7, ___b_1)); }
	inline String_t* get_b_1() const { return ___b_1; }
	inline String_t** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(String_t* value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7, ___c_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_c_2() const { return ___c_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7, ___d_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_d_3() const { return ___d_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___d_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGECOLORCUSTOMBOXCMD_TB3B996485420AF5714BDEF530BD7528AFF7AD8E7_H
#ifndef SDKMODE_T68489B85D9A5BF965F530F7415C6F9239F949348_H
#define SDKMODE_T68489B85D9A5BF965F530F7415C6F9239F949348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GeneralSetting_SDKMode
struct  SDKMode_t68489B85D9A5BF965F530F7415C6F9239F949348 
{
public:
	// System.Int32 DLLCore.GeneralSetting_SDKMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SDKMode_t68489B85D9A5BF965F530F7415C6F9239F949348, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SDKMODE_T68489B85D9A5BF965F530F7415C6F9239F949348_H
#ifndef VIEWMODE_T65A9F115674EDCD52D7677D41A5D56F687458B3E_H
#define VIEWMODE_T65A9F115674EDCD52D7677D41A5D56F687458B3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GeneralSetting_ViewMode
struct  ViewMode_t65A9F115674EDCD52D7677D41A5D56F687458B3E 
{
public:
	// System.Int32 DLLCore.GeneralSetting_ViewMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ViewMode_t65A9F115674EDCD52D7677D41A5D56F687458B3E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWMODE_T65A9F115674EDCD52D7677D41A5D56F687458B3E_H
#ifndef LOADPRODUCTCMD_T1E80AF3306BB0373169E88D9BDD43715D4461EDD_H
#define LOADPRODUCTCMD_T1E80AF3306BB0373169E88D9BDD43715D4461EDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.LoadProductCmd
struct  LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD  : public Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA
{
public:
	// DLLCore.Product DLLCore.LoadProductCmd::a
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * ___a_0;
	// System.String DLLCore.LoadProductCmd::b
	String_t* ___b_1;
	// UnityEngine.Vector3 DLLCore.LoadProductCmd::c
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___c_2;
	// UnityEngine.Quaternion DLLCore.LoadProductCmd::d
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___d_3;
	// UnityEngine.Vector3 DLLCore.LoadProductCmd::e
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___e_4;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD, ___a_0)); }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * get_a_0() const { return ___a_0; }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD, ___b_1)); }
	inline String_t* get_b_1() const { return ___b_1; }
	inline String_t** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(String_t* value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD, ___c_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_c_2() const { return ___c_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD, ___d_3)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_d_3() const { return ___d_3; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___d_3 = value;
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD, ___e_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_e_4() const { return ___e_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___e_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADPRODUCTCMD_T1E80AF3306BB0373169E88D9BDD43715D4461EDD_H
#ifndef PRESETTYPE_T7E6F36433E4BAA3E292B376663A830C4FF667F3C_H
#define PRESETTYPE_T7E6F36433E4BAA3E292B376663A830C4FF667F3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.OrbitControls_PresetType
struct  PresetType_t7E6F36433E4BAA3E292B376663A830C4FF667F3C 
{
public:
	// System.Int32 DLLCore.OrbitControls_PresetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PresetType_t7E6F36433E4BAA3E292B376663A830C4FF667F3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESETTYPE_T7E6F36433E4BAA3E292B376663A830C4FF667F3C_H
#ifndef TRANSFORMPRODUCTCMD_T4C58C7BCB16C7B00099083784135BF420BF832D7_H
#define TRANSFORMPRODUCTCMD_T4C58C7BCB16C7B00099083784135BF420BF832D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.TransformProductCmd
struct  TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7  : public Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA
{
public:
	// DLLCore.Product DLLCore.TransformProductCmd::a
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * ___a_0;
	// System.String DLLCore.TransformProductCmd::b
	String_t* ___b_1;
	// UnityEngine.Vector3 DLLCore.TransformProductCmd::c
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___c_2;
	// UnityEngine.Vector3 DLLCore.TransformProductCmd::d
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___d_3;
	// UnityEngine.Quaternion DLLCore.TransformProductCmd::e
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___e_4;
	// UnityEngine.Quaternion DLLCore.TransformProductCmd::f
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___f_5;
	// UnityEngine.Vector3 DLLCore.TransformProductCmd::g
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___g_6;
	// UnityEngine.Vector3 DLLCore.TransformProductCmd::h
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___h_7;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7, ___a_0)); }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * get_a_0() const { return ___a_0; }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7, ___b_1)); }
	inline String_t* get_b_1() const { return ___b_1; }
	inline String_t** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(String_t* value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7, ___c_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_c_2() const { return ___c_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7, ___d_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_d_3() const { return ___d_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___d_3 = value;
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7, ___e_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_e_4() const { return ___e_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___e_4 = value;
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7, ___f_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_f_5() const { return ___f_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___f_5 = value;
	}

	inline static int32_t get_offset_of_g_6() { return static_cast<int32_t>(offsetof(TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7, ___g_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_g_6() const { return ___g_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_g_6() { return &___g_6; }
	inline void set_g_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___g_6 = value;
	}

	inline static int32_t get_offset_of_h_7() { return static_cast<int32_t>(offsetof(TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7, ___h_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_h_7() const { return ___h_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_h_7() { return &___h_7; }
	inline void set_h_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___h_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMPRODUCTCMD_T4C58C7BCB16C7B00099083784135BF420BF832D7_H
#ifndef TRANSFORMUNITCMD_T4420A5F41F20145B45284090530353ECF209595C_H
#define TRANSFORMUNITCMD_T4420A5F41F20145B45284090530353ECF209595C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.TransformUnitCmd
struct  TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C  : public Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA
{
public:
	// DLLCore.UnitComponents.Unit DLLCore.TransformUnitCmd::a
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * ___a_0;
	// UnityEngine.Vector3 DLLCore.TransformUnitCmd::b
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b_1;
	// UnityEngine.Vector3 DLLCore.TransformUnitCmd::c
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___c_2;
	// UnityEngine.Quaternion DLLCore.TransformUnitCmd::d
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___d_3;
	// UnityEngine.Quaternion DLLCore.TransformUnitCmd::e
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___e_4;
	// UnityEngine.Vector3 DLLCore.TransformUnitCmd::f
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___f_5;
	// UnityEngine.Vector3 DLLCore.TransformUnitCmd::g
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___g_6;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C, ___a_0)); }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * get_a_0() const { return ___a_0; }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C, ___b_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_b_1() const { return ___b_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___b_1 = value;
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C, ___c_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_c_2() const { return ___c_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C, ___d_3)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_d_3() const { return ___d_3; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___d_3 = value;
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C, ___e_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_e_4() const { return ___e_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___e_4 = value;
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C, ___f_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_f_5() const { return ___f_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___f_5 = value;
	}

	inline static int32_t get_offset_of_g_6() { return static_cast<int32_t>(offsetof(TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C, ___g_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_g_6() const { return ___g_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_g_6() { return &___g_6; }
	inline void set_g_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___g_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMUNITCMD_T4420A5F41F20145B45284090530353ECF209595C_H
#ifndef TYPE_T0F33033AB9EC1EE6644E22D132AEA0764439B072_H
#define TYPE_T0F33033AB9EC1EE6644E22D132AEA0764439B072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject_Type
struct  Type_t0F33033AB9EC1EE6644E22D132AEA0764439B072 
{
public:
	// System.Int32 JSONObject_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t0F33033AB9EC1EE6644E22D132AEA0764439B072, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T0F33033AB9EC1EE6644E22D132AEA0764439B072_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef TRANSFORMDATA_T5720CDB59D545F630361A92B5D01C6C4DAAA5C3B_H
#define TRANSFORMDATA_T5720CDB59D545F630361A92B5D01C6C4DAAA5C3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransformData
struct  TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B 
{
public:
	// UnityEngine.Vector3 TransformData::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Quaternion TransformData::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;
	// UnityEngine.Vector3 TransformData::localPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localPosition_2;
	// UnityEngine.Vector3 TransformData::localScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localScale_3;
	// UnityEngine.Quaternion TransformData::localRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___localRotation_4;
	// UnityEngine.Transform TransformData::parent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent_5;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B, ___rotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_1 = value;
	}

	inline static int32_t get_offset_of_localPosition_2() { return static_cast<int32_t>(offsetof(TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B, ___localPosition_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_localPosition_2() const { return ___localPosition_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_localPosition_2() { return &___localPosition_2; }
	inline void set_localPosition_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___localPosition_2 = value;
	}

	inline static int32_t get_offset_of_localScale_3() { return static_cast<int32_t>(offsetof(TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B, ___localScale_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_localScale_3() const { return ___localScale_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_localScale_3() { return &___localScale_3; }
	inline void set_localScale_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___localScale_3 = value;
	}

	inline static int32_t get_offset_of_localRotation_4() { return static_cast<int32_t>(offsetof(TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B, ___localRotation_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_localRotation_4() const { return ___localRotation_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_localRotation_4() { return &___localRotation_4; }
	inline void set_localRotation_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___localRotation_4 = value;
	}

	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B, ___parent_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_parent_5() const { return ___parent_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((&___parent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TransformData
struct TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B_marshaled_pinvoke
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localPosition_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localScale_3;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___localRotation_4;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent_5;
};
// Native definition for COM marshalling of TransformData
struct TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B_marshaled_com
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localPosition_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localScale_3;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___localRotation_4;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent_5;
};
#endif // TRANSFORMDATA_T5720CDB59D545F630361A92B5D01C6C4DAAA5C3B_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#define RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#define RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Centroid_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Point_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Normal_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifndef TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#define TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifndef ASPECTMODE_T2D8C205891B8E63CA16B6AC3BA1D41320903C65A_H
#define ASPECTMODE_T2D8C205891B8E63CA16B6AC3BA1D41320903C65A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter_AspectMode
struct  AspectMode_t2D8C205891B8E63CA16B6AC3BA1D41320903C65A 
{
public:
	// System.Int32 UnityEngine.UI.AspectRatioFitter_AspectMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AspectMode_t2D8C205891B8E63CA16B6AC3BA1D41320903C65A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTMODE_T2D8C205891B8E63CA16B6AC3BA1D41320903C65A_H
#ifndef SCALEMODE_T38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6_H
#define SCALEMODE_T38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler_ScaleMode
struct  ScaleMode_t38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler_ScaleMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScaleMode_t38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEMODE_T38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6_H
#ifndef SCREENMATCHMODE_T61C3A62F8F54F705D47C2C37B06DC8083238C133_H
#define SCREENMATCHMODE_T61C3A62F8F54F705D47C2C37B06DC8083238C133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler_ScreenMatchMode
struct  ScreenMatchMode_t61C3A62F8F54F705D47C2C37B06DC8083238C133 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler_ScreenMatchMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenMatchMode_t61C3A62F8F54F705D47C2C37B06DC8083238C133, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENMATCHMODE_T61C3A62F8F54F705D47C2C37B06DC8083238C133_H
#ifndef UNIT_TD24A4DB24016D1A6B46579640E170359F76F8313_H
#define UNIT_TD24A4DB24016D1A6B46579640E170359F76F8313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler_Unit
struct  Unit_tD24A4DB24016D1A6B46579640E170359F76F8313 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler_Unit::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Unit_tD24A4DB24016D1A6B46579640E170359F76F8313, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_TD24A4DB24016D1A6B46579640E170359F76F8313_H
#ifndef FITMODE_TBF783E77415F7063B468C18E758F738D83D60A08_H
#define FITMODE_TBF783E77415F7063B468C18E758F738D83D60A08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter_FitMode
struct  FitMode_tBF783E77415F7063B468C18E758F738D83D60A08 
{
public:
	// System.Int32 UnityEngine.UI.ContentSizeFitter_FitMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FitMode_tBF783E77415F7063B468C18E758F738D83D60A08, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FITMODE_TBF783E77415F7063B468C18E758F738D83D60A08_H
#ifndef AXIS_TD4645F3B274E7AE7793C038A2BA2E68C6F0F02F0_H
#define AXIS_TD4645F3B274E7AE7793C038A2BA2E68C6F0F02F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup_Axis
struct  Axis_tD4645F3B274E7AE7793C038A2BA2E68C6F0F02F0 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup_Axis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Axis_tD4645F3B274E7AE7793C038A2BA2E68C6F0F02F0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_TD4645F3B274E7AE7793C038A2BA2E68C6F0F02F0_H
#ifndef CONSTRAINT_TF471E55525B89D1E7C938CC0AF7515709494C59D_H
#define CONSTRAINT_TF471E55525B89D1E7C938CC0AF7515709494C59D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup_Constraint
struct  Constraint_tF471E55525B89D1E7C938CC0AF7515709494C59D 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup_Constraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Constraint_tF471E55525B89D1E7C938CC0AF7515709494C59D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_TF471E55525B89D1E7C938CC0AF7515709494C59D_H
#ifndef CORNER_TD61F36EC56D401A65DA06BE1A21689319201D18E_H
#define CORNER_TD61F36EC56D401A65DA06BE1A21689319201D18E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup_Corner
struct  Corner_tD61F36EC56D401A65DA06BE1A21689319201D18E 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup_Corner::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Corner_tD61F36EC56D401A65DA06BE1A21689319201D18E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_TD61F36EC56D401A65DA06BE1A21689319201D18E_H
#ifndef VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#define VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Positions_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Colors_1)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv0S_2)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv1S_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv2S_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv3S_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Normals_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Tangents_7)); }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Indices_8)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifndef A_TB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_H
#define A_TB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// a
struct  a_tB29FDBA317931B7D50F19992FEB1BC2EC7C616FC  : public RuntimeObject
{
public:

public:
};

struct a_tB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_StaticFields
{
public:
	// a_a a::a
	a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E  ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_tB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_StaticFields, ___a_0)); }
	inline a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E  get_a_0() const { return ___a_0; }
	inline a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E * get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E  value)
	{
		___a_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_TB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_H
#ifndef LOGITEM_TC5B07EC021DE250618B9F1E373E71741BC8D4CB1_H
#define LOGITEM_TC5B07EC021DE250618B9F1E373E71741BC8D4CB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.LogItem
struct  LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1 
{
public:
	// CommandTerminal.TerminalLogType CommandTerminal.LogItem::type
	int32_t ___type_0;
	// System.String CommandTerminal.LogItem::message
	String_t* ___message_1;
	// System.String CommandTerminal.LogItem::stackTrace
	String_t* ___stackTrace_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}

	inline static int32_t get_offset_of_stackTrace_2() { return static_cast<int32_t>(offsetof(LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1, ___stackTrace_2)); }
	inline String_t* get_stackTrace_2() const { return ___stackTrace_2; }
	inline String_t** get_address_of_stackTrace_2() { return &___stackTrace_2; }
	inline void set_stackTrace_2(String_t* value)
	{
		___stackTrace_2 = value;
		Il2CppCodeGenWriteBarrier((&___stackTrace_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CommandTerminal.LogItem
struct LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1_marshaled_pinvoke
{
	int32_t ___type_0;
	char* ___message_1;
	char* ___stackTrace_2;
};
// Native definition for COM marshalling of CommandTerminal.LogItem
struct LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1_marshaled_com
{
	int32_t ___type_0;
	Il2CppChar* ___message_1;
	Il2CppChar* ___stackTrace_2;
};
#endif // LOGITEM_TC5B07EC021DE250618B9F1E373E71741BC8D4CB1_H
#ifndef JSONOBJECT_T483DCB025D825510C7827AE094325C194CE9B946_H
#define JSONOBJECT_T483DCB025D825510C7827AE094325C194CE9B946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONObject
struct  JSONObject_t483DCB025D825510C7827AE094325C194CE9B946  : public RuntimeObject
{
public:
	// JSONObject_Type JSONObject::type
	int32_t ___type_1;
	// System.Collections.Generic.List`1<JSONObject> JSONObject::list
	List_1_t1630C08B48AE897B94285B7FBC907DE65EBC5032 * ___list_2;
	// System.Collections.Generic.List`1<System.String> JSONObject::keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___keys_3;
	// System.String JSONObject::str
	String_t* ___str_4;
	// System.Single JSONObject::n
	float ___n_5;
	// System.Boolean JSONObject::useInt
	bool ___useInt_6;
	// System.Int64 JSONObject::i
	int64_t ___i_7;
	// System.Boolean JSONObject::b
	bool ___b_8;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946, ___list_2)); }
	inline List_1_t1630C08B48AE897B94285B7FBC907DE65EBC5032 * get_list_2() const { return ___list_2; }
	inline List_1_t1630C08B48AE897B94285B7FBC907DE65EBC5032 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(List_1_t1630C08B48AE897B94285B7FBC907DE65EBC5032 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of_keys_3() { return static_cast<int32_t>(offsetof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946, ___keys_3)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_keys_3() const { return ___keys_3; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_keys_3() { return &___keys_3; }
	inline void set_keys_3(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___keys_3 = value;
		Il2CppCodeGenWriteBarrier((&___keys_3), value);
	}

	inline static int32_t get_offset_of_str_4() { return static_cast<int32_t>(offsetof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946, ___str_4)); }
	inline String_t* get_str_4() const { return ___str_4; }
	inline String_t** get_address_of_str_4() { return &___str_4; }
	inline void set_str_4(String_t* value)
	{
		___str_4 = value;
		Il2CppCodeGenWriteBarrier((&___str_4), value);
	}

	inline static int32_t get_offset_of_n_5() { return static_cast<int32_t>(offsetof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946, ___n_5)); }
	inline float get_n_5() const { return ___n_5; }
	inline float* get_address_of_n_5() { return &___n_5; }
	inline void set_n_5(float value)
	{
		___n_5 = value;
	}

	inline static int32_t get_offset_of_useInt_6() { return static_cast<int32_t>(offsetof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946, ___useInt_6)); }
	inline bool get_useInt_6() const { return ___useInt_6; }
	inline bool* get_address_of_useInt_6() { return &___useInt_6; }
	inline void set_useInt_6(bool value)
	{
		___useInt_6 = value;
	}

	inline static int32_t get_offset_of_i_7() { return static_cast<int32_t>(offsetof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946, ___i_7)); }
	inline int64_t get_i_7() const { return ___i_7; }
	inline int64_t* get_address_of_i_7() { return &___i_7; }
	inline void set_i_7(int64_t value)
	{
		___i_7 = value;
	}

	inline static int32_t get_offset_of_b_8() { return static_cast<int32_t>(offsetof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946, ___b_8)); }
	inline bool get_b_8() const { return ___b_8; }
	inline bool* get_address_of_b_8() { return &___b_8; }
	inline void set_b_8(bool value)
	{
		___b_8 = value;
	}
};

struct JSONObject_t483DCB025D825510C7827AE094325C194CE9B946_StaticFields
{
public:
	// System.Char[] JSONObject::WHITESPACE
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___WHITESPACE_0;
	// System.Diagnostics.Stopwatch JSONObject::j
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ___j_9;

public:
	inline static int32_t get_offset_of_WHITESPACE_0() { return static_cast<int32_t>(offsetof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946_StaticFields, ___WHITESPACE_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_WHITESPACE_0() const { return ___WHITESPACE_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_WHITESPACE_0() { return &___WHITESPACE_0; }
	inline void set_WHITESPACE_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___WHITESPACE_0 = value;
		Il2CppCodeGenWriteBarrier((&___WHITESPACE_0), value);
	}

	inline static int32_t get_offset_of_j_9() { return static_cast<int32_t>(offsetof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946_StaticFields, ___j_9)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get_j_9() const { return ___j_9; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of_j_9() { return &___j_9; }
	inline void set_j_9(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		___j_9 = value;
		Il2CppCodeGenWriteBarrier((&___j_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T483DCB025D825510C7827AE094325C194CE9B946_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#define GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifndef GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#define GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifndef RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#define RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback
struct  Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifndef RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#define RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback
struct  Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifndef RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#define RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback
struct  RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef TERMINAL_T0EC32061DBB6EE11865FC3F6B30EA2E784368841_H
#define TERMINAL_T0EC32061DBB6EE11865FC3F6B30EA2E784368841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandTerminal.Terminal
struct  Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CommandTerminal.Terminal::a
	float ___a_4;
	// System.String CommandTerminal.Terminal::b
	String_t* ___b_5;
	// System.Int32 CommandTerminal.Terminal::c
	int32_t ___c_6;
	// UnityEngine.Font CommandTerminal.Terminal::d
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___d_7;
	// System.String CommandTerminal.Terminal::e
	String_t* ___e_8;
	// UnityEngine.Color CommandTerminal.Terminal::f
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___f_9;
	// UnityEngine.Color CommandTerminal.Terminal::g
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___g_10;
	// UnityEngine.Color CommandTerminal.Terminal::h
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___h_11;
	// UnityEngine.Color CommandTerminal.Terminal::i
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___i_12;
	// UnityEngine.Color CommandTerminal.Terminal::j
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___j_13;
	// UnityEngine.Color CommandTerminal.Terminal::k
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___k_14;
	// CommandTerminal.TerminalState CommandTerminal.Terminal::l
	int32_t ___l_15;
	// UnityEngine.TextEditor CommandTerminal.Terminal::m
	TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * ___m_16;
	// System.Boolean CommandTerminal.Terminal::n
	bool ___n_17;
	// System.Boolean CommandTerminal.Terminal::o
	bool ___o_18;
	// System.Boolean CommandTerminal.Terminal::p
	bool ___p_19;
	// UnityEngine.Rect CommandTerminal.Terminal::q
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___q_20;
	// System.Single CommandTerminal.Terminal::r
	float ___r_21;
	// System.Single CommandTerminal.Terminal::s
	float ___s_22;
	// System.Single CommandTerminal.Terminal::t
	float ___t_23;
	// System.String CommandTerminal.Terminal::u
	String_t* ___u_24;
	// System.String CommandTerminal.Terminal::v
	String_t* ___v_25;
	// UnityEngine.Vector2 CommandTerminal.Terminal::w
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___w_26;
	// UnityEngine.GUIStyle CommandTerminal.Terminal::x
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___x_27;
	// UnityEngine.GUIStyle CommandTerminal.Terminal::y
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___y_28;
	// UnityEngine.GUIStyle CommandTerminal.Terminal::z
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___z_29;
	// UnityEngine.Texture2D CommandTerminal.Terminal::aa
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___aa_30;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___a_4)); }
	inline float get_a_4() const { return ___a_4; }
	inline float* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(float value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___b_5)); }
	inline String_t* get_b_5() const { return ___b_5; }
	inline String_t** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(String_t* value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___c_6)); }
	inline int32_t get_c_6() const { return ___c_6; }
	inline int32_t* get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(int32_t value)
	{
		___c_6 = value;
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___d_7)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_d_7() const { return ___d_7; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}

	inline static int32_t get_offset_of_e_8() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___e_8)); }
	inline String_t* get_e_8() const { return ___e_8; }
	inline String_t** get_address_of_e_8() { return &___e_8; }
	inline void set_e_8(String_t* value)
	{
		___e_8 = value;
		Il2CppCodeGenWriteBarrier((&___e_8), value);
	}

	inline static int32_t get_offset_of_f_9() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___f_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_f_9() const { return ___f_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_f_9() { return &___f_9; }
	inline void set_f_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___f_9 = value;
	}

	inline static int32_t get_offset_of_g_10() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___g_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_g_10() const { return ___g_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_g_10() { return &___g_10; }
	inline void set_g_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___g_10 = value;
	}

	inline static int32_t get_offset_of_h_11() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___h_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_h_11() const { return ___h_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_h_11() { return &___h_11; }
	inline void set_h_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___h_11 = value;
	}

	inline static int32_t get_offset_of_i_12() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___i_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_i_12() const { return ___i_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_i_12() { return &___i_12; }
	inline void set_i_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___i_12 = value;
	}

	inline static int32_t get_offset_of_j_13() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___j_13)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_j_13() const { return ___j_13; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_j_13() { return &___j_13; }
	inline void set_j_13(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___j_13 = value;
	}

	inline static int32_t get_offset_of_k_14() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___k_14)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_k_14() const { return ___k_14; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_k_14() { return &___k_14; }
	inline void set_k_14(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___k_14 = value;
	}

	inline static int32_t get_offset_of_l_15() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___l_15)); }
	inline int32_t get_l_15() const { return ___l_15; }
	inline int32_t* get_address_of_l_15() { return &___l_15; }
	inline void set_l_15(int32_t value)
	{
		___l_15 = value;
	}

	inline static int32_t get_offset_of_m_16() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___m_16)); }
	inline TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * get_m_16() const { return ___m_16; }
	inline TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 ** get_address_of_m_16() { return &___m_16; }
	inline void set_m_16(TextEditor_t72CB6095A5C38226E08CD8073D5B6AD98579D440 * value)
	{
		___m_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_16), value);
	}

	inline static int32_t get_offset_of_n_17() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___n_17)); }
	inline bool get_n_17() const { return ___n_17; }
	inline bool* get_address_of_n_17() { return &___n_17; }
	inline void set_n_17(bool value)
	{
		___n_17 = value;
	}

	inline static int32_t get_offset_of_o_18() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___o_18)); }
	inline bool get_o_18() const { return ___o_18; }
	inline bool* get_address_of_o_18() { return &___o_18; }
	inline void set_o_18(bool value)
	{
		___o_18 = value;
	}

	inline static int32_t get_offset_of_p_19() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___p_19)); }
	inline bool get_p_19() const { return ___p_19; }
	inline bool* get_address_of_p_19() { return &___p_19; }
	inline void set_p_19(bool value)
	{
		___p_19 = value;
	}

	inline static int32_t get_offset_of_q_20() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___q_20)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_q_20() const { return ___q_20; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_q_20() { return &___q_20; }
	inline void set_q_20(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___q_20 = value;
	}

	inline static int32_t get_offset_of_r_21() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___r_21)); }
	inline float get_r_21() const { return ___r_21; }
	inline float* get_address_of_r_21() { return &___r_21; }
	inline void set_r_21(float value)
	{
		___r_21 = value;
	}

	inline static int32_t get_offset_of_s_22() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___s_22)); }
	inline float get_s_22() const { return ___s_22; }
	inline float* get_address_of_s_22() { return &___s_22; }
	inline void set_s_22(float value)
	{
		___s_22 = value;
	}

	inline static int32_t get_offset_of_t_23() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___t_23)); }
	inline float get_t_23() const { return ___t_23; }
	inline float* get_address_of_t_23() { return &___t_23; }
	inline void set_t_23(float value)
	{
		___t_23 = value;
	}

	inline static int32_t get_offset_of_u_24() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___u_24)); }
	inline String_t* get_u_24() const { return ___u_24; }
	inline String_t** get_address_of_u_24() { return &___u_24; }
	inline void set_u_24(String_t* value)
	{
		___u_24 = value;
		Il2CppCodeGenWriteBarrier((&___u_24), value);
	}

	inline static int32_t get_offset_of_v_25() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___v_25)); }
	inline String_t* get_v_25() const { return ___v_25; }
	inline String_t** get_address_of_v_25() { return &___v_25; }
	inline void set_v_25(String_t* value)
	{
		___v_25 = value;
		Il2CppCodeGenWriteBarrier((&___v_25), value);
	}

	inline static int32_t get_offset_of_w_26() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___w_26)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_w_26() const { return ___w_26; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_w_26() { return &___w_26; }
	inline void set_w_26(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___w_26 = value;
	}

	inline static int32_t get_offset_of_x_27() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___x_27)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_x_27() const { return ___x_27; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_x_27() { return &___x_27; }
	inline void set_x_27(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___x_27 = value;
		Il2CppCodeGenWriteBarrier((&___x_27), value);
	}

	inline static int32_t get_offset_of_y_28() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___y_28)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_y_28() const { return ___y_28; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_y_28() { return &___y_28; }
	inline void set_y_28(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___y_28 = value;
		Il2CppCodeGenWriteBarrier((&___y_28), value);
	}

	inline static int32_t get_offset_of_z_29() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___z_29)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_z_29() const { return ___z_29; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_z_29() { return &___z_29; }
	inline void set_z_29(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___z_29 = value;
		Il2CppCodeGenWriteBarrier((&___z_29), value);
	}

	inline static int32_t get_offset_of_aa_30() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841, ___aa_30)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_aa_30() const { return ___aa_30; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_aa_30() { return &___aa_30; }
	inline void set_aa_30(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___aa_30 = value;
		Il2CppCodeGenWriteBarrier((&___aa_30), value);
	}
};

struct Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841_StaticFields
{
public:
	// CommandTerminal.CommandLog CommandTerminal.Terminal::ab
	CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03 * ___ab_31;
	// CommandTerminal.CommandShell CommandTerminal.Terminal::ac
	CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811 * ___ac_32;
	// CommandTerminal.CommandHistory CommandTerminal.Terminal::ad
	CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994 * ___ad_33;
	// CommandTerminal.CommandAutocomplete CommandTerminal.Terminal::ae
	CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527 * ___ae_34;

public:
	inline static int32_t get_offset_of_ab_31() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841_StaticFields, ___ab_31)); }
	inline CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03 * get_ab_31() const { return ___ab_31; }
	inline CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03 ** get_address_of_ab_31() { return &___ab_31; }
	inline void set_ab_31(CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03 * value)
	{
		___ab_31 = value;
		Il2CppCodeGenWriteBarrier((&___ab_31), value);
	}

	inline static int32_t get_offset_of_ac_32() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841_StaticFields, ___ac_32)); }
	inline CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811 * get_ac_32() const { return ___ac_32; }
	inline CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811 ** get_address_of_ac_32() { return &___ac_32; }
	inline void set_ac_32(CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811 * value)
	{
		___ac_32 = value;
		Il2CppCodeGenWriteBarrier((&___ac_32), value);
	}

	inline static int32_t get_offset_of_ad_33() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841_StaticFields, ___ad_33)); }
	inline CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994 * get_ad_33() const { return ___ad_33; }
	inline CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994 ** get_address_of_ad_33() { return &___ad_33; }
	inline void set_ad_33(CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994 * value)
	{
		___ad_33 = value;
		Il2CppCodeGenWriteBarrier((&___ad_33), value);
	}

	inline static int32_t get_offset_of_ae_34() { return static_cast<int32_t>(offsetof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841_StaticFields, ___ae_34)); }
	inline CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527 * get_ae_34() const { return ___ae_34; }
	inline CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527 ** get_address_of_ae_34() { return &___ae_34; }
	inline void set_ae_34(CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527 * value)
	{
		___ae_34 = value;
		Il2CppCodeGenWriteBarrier((&___ae_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERMINAL_T0EC32061DBB6EE11865FC3F6B30EA2E784368841_H
#ifndef AUTOINTENSITY_TB8A446E08F3472C1D33DD750E69C7C341863F029_H
#define AUTOINTENSITY_TB8A446E08F3472C1D33DD750E69C7C341863F029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.AutoIntensity
struct  AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Gradient DLLCore.AutoIntensity::nightDayColor
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * ___nightDayColor_4;
	// System.Single DLLCore.AutoIntensity::maxIntensity
	float ___maxIntensity_5;
	// System.Single DLLCore.AutoIntensity::minIntensity
	float ___minIntensity_6;
	// System.Single DLLCore.AutoIntensity::minPoint
	float ___minPoint_7;
	// System.Single DLLCore.AutoIntensity::maxAmbient
	float ___maxAmbient_8;
	// System.Single DLLCore.AutoIntensity::minAmbient
	float ___minAmbient_9;
	// System.Single DLLCore.AutoIntensity::minAmbientPoint
	float ___minAmbientPoint_10;
	// UnityEngine.Gradient DLLCore.AutoIntensity::nightDayFogColor
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * ___nightDayFogColor_11;
	// UnityEngine.AnimationCurve DLLCore.AutoIntensity::fogDensityCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___fogDensityCurve_12;
	// System.Single DLLCore.AutoIntensity::fogScale
	float ___fogScale_13;
	// System.Single DLLCore.AutoIntensity::dayAtmosphereThickness
	float ___dayAtmosphereThickness_14;
	// System.Single DLLCore.AutoIntensity::nightAtmosphereThickness
	float ___nightAtmosphereThickness_15;
	// UnityEngine.Vector3 DLLCore.AutoIntensity::dayRotateSpeed
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dayRotateSpeed_16;
	// UnityEngine.Vector3 DLLCore.AutoIntensity::nightRotateSpeed
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___nightRotateSpeed_17;
	// System.Single DLLCore.AutoIntensity::a
	float ___a_18;
	// System.Single DLLCore.AutoIntensity::ambientLightFactor
	float ___ambientLightFactor_19;
	// UnityEngine.Light DLLCore.AutoIntensity::b
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___b_20;
	// UnityEngine.Skybox DLLCore.AutoIntensity::c
	Skybox_tCAAEF216A36B4CD9FD498E4EED256EAE7B81DDDD * ___c_21;
	// UnityEngine.Material DLLCore.AutoIntensity::d
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___d_22;

public:
	inline static int32_t get_offset_of_nightDayColor_4() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___nightDayColor_4)); }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * get_nightDayColor_4() const { return ___nightDayColor_4; }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A ** get_address_of_nightDayColor_4() { return &___nightDayColor_4; }
	inline void set_nightDayColor_4(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * value)
	{
		___nightDayColor_4 = value;
		Il2CppCodeGenWriteBarrier((&___nightDayColor_4), value);
	}

	inline static int32_t get_offset_of_maxIntensity_5() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___maxIntensity_5)); }
	inline float get_maxIntensity_5() const { return ___maxIntensity_5; }
	inline float* get_address_of_maxIntensity_5() { return &___maxIntensity_5; }
	inline void set_maxIntensity_5(float value)
	{
		___maxIntensity_5 = value;
	}

	inline static int32_t get_offset_of_minIntensity_6() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___minIntensity_6)); }
	inline float get_minIntensity_6() const { return ___minIntensity_6; }
	inline float* get_address_of_minIntensity_6() { return &___minIntensity_6; }
	inline void set_minIntensity_6(float value)
	{
		___minIntensity_6 = value;
	}

	inline static int32_t get_offset_of_minPoint_7() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___minPoint_7)); }
	inline float get_minPoint_7() const { return ___minPoint_7; }
	inline float* get_address_of_minPoint_7() { return &___minPoint_7; }
	inline void set_minPoint_7(float value)
	{
		___minPoint_7 = value;
	}

	inline static int32_t get_offset_of_maxAmbient_8() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___maxAmbient_8)); }
	inline float get_maxAmbient_8() const { return ___maxAmbient_8; }
	inline float* get_address_of_maxAmbient_8() { return &___maxAmbient_8; }
	inline void set_maxAmbient_8(float value)
	{
		___maxAmbient_8 = value;
	}

	inline static int32_t get_offset_of_minAmbient_9() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___minAmbient_9)); }
	inline float get_minAmbient_9() const { return ___minAmbient_9; }
	inline float* get_address_of_minAmbient_9() { return &___minAmbient_9; }
	inline void set_minAmbient_9(float value)
	{
		___minAmbient_9 = value;
	}

	inline static int32_t get_offset_of_minAmbientPoint_10() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___minAmbientPoint_10)); }
	inline float get_minAmbientPoint_10() const { return ___minAmbientPoint_10; }
	inline float* get_address_of_minAmbientPoint_10() { return &___minAmbientPoint_10; }
	inline void set_minAmbientPoint_10(float value)
	{
		___minAmbientPoint_10 = value;
	}

	inline static int32_t get_offset_of_nightDayFogColor_11() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___nightDayFogColor_11)); }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * get_nightDayFogColor_11() const { return ___nightDayFogColor_11; }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A ** get_address_of_nightDayFogColor_11() { return &___nightDayFogColor_11; }
	inline void set_nightDayFogColor_11(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * value)
	{
		___nightDayFogColor_11 = value;
		Il2CppCodeGenWriteBarrier((&___nightDayFogColor_11), value);
	}

	inline static int32_t get_offset_of_fogDensityCurve_12() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___fogDensityCurve_12)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_fogDensityCurve_12() const { return ___fogDensityCurve_12; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_fogDensityCurve_12() { return &___fogDensityCurve_12; }
	inline void set_fogDensityCurve_12(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___fogDensityCurve_12 = value;
		Il2CppCodeGenWriteBarrier((&___fogDensityCurve_12), value);
	}

	inline static int32_t get_offset_of_fogScale_13() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___fogScale_13)); }
	inline float get_fogScale_13() const { return ___fogScale_13; }
	inline float* get_address_of_fogScale_13() { return &___fogScale_13; }
	inline void set_fogScale_13(float value)
	{
		___fogScale_13 = value;
	}

	inline static int32_t get_offset_of_dayAtmosphereThickness_14() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___dayAtmosphereThickness_14)); }
	inline float get_dayAtmosphereThickness_14() const { return ___dayAtmosphereThickness_14; }
	inline float* get_address_of_dayAtmosphereThickness_14() { return &___dayAtmosphereThickness_14; }
	inline void set_dayAtmosphereThickness_14(float value)
	{
		___dayAtmosphereThickness_14 = value;
	}

	inline static int32_t get_offset_of_nightAtmosphereThickness_15() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___nightAtmosphereThickness_15)); }
	inline float get_nightAtmosphereThickness_15() const { return ___nightAtmosphereThickness_15; }
	inline float* get_address_of_nightAtmosphereThickness_15() { return &___nightAtmosphereThickness_15; }
	inline void set_nightAtmosphereThickness_15(float value)
	{
		___nightAtmosphereThickness_15 = value;
	}

	inline static int32_t get_offset_of_dayRotateSpeed_16() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___dayRotateSpeed_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_dayRotateSpeed_16() const { return ___dayRotateSpeed_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_dayRotateSpeed_16() { return &___dayRotateSpeed_16; }
	inline void set_dayRotateSpeed_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___dayRotateSpeed_16 = value;
	}

	inline static int32_t get_offset_of_nightRotateSpeed_17() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___nightRotateSpeed_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_nightRotateSpeed_17() const { return ___nightRotateSpeed_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_nightRotateSpeed_17() { return &___nightRotateSpeed_17; }
	inline void set_nightRotateSpeed_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___nightRotateSpeed_17 = value;
	}

	inline static int32_t get_offset_of_a_18() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___a_18)); }
	inline float get_a_18() const { return ___a_18; }
	inline float* get_address_of_a_18() { return &___a_18; }
	inline void set_a_18(float value)
	{
		___a_18 = value;
	}

	inline static int32_t get_offset_of_ambientLightFactor_19() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___ambientLightFactor_19)); }
	inline float get_ambientLightFactor_19() const { return ___ambientLightFactor_19; }
	inline float* get_address_of_ambientLightFactor_19() { return &___ambientLightFactor_19; }
	inline void set_ambientLightFactor_19(float value)
	{
		___ambientLightFactor_19 = value;
	}

	inline static int32_t get_offset_of_b_20() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___b_20)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_b_20() const { return ___b_20; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_b_20() { return &___b_20; }
	inline void set_b_20(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___b_20 = value;
		Il2CppCodeGenWriteBarrier((&___b_20), value);
	}

	inline static int32_t get_offset_of_c_21() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___c_21)); }
	inline Skybox_tCAAEF216A36B4CD9FD498E4EED256EAE7B81DDDD * get_c_21() const { return ___c_21; }
	inline Skybox_tCAAEF216A36B4CD9FD498E4EED256EAE7B81DDDD ** get_address_of_c_21() { return &___c_21; }
	inline void set_c_21(Skybox_tCAAEF216A36B4CD9FD498E4EED256EAE7B81DDDD * value)
	{
		___c_21 = value;
		Il2CppCodeGenWriteBarrier((&___c_21), value);
	}

	inline static int32_t get_offset_of_d_22() { return static_cast<int32_t>(offsetof(AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029, ___d_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_d_22() const { return ___d_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_d_22() { return &___d_22; }
	inline void set_d_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___d_22 = value;
		Il2CppCodeGenWriteBarrier((&___d_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOINTENSITY_TB8A446E08F3472C1D33DD750E69C7C341863F029_H
#ifndef CONTROLLER_T3FE6F0551E59C0E1D1745D16A978680C8137DB90_H
#define CONTROLLER_T3FE6F0551E59C0E1D1745D16A978680C8137DB90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.Controller
struct  Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLER_T3FE6F0551E59C0E1D1745D16A978680C8137DB90_H
#ifndef EVENTMANAGER_TDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966_H
#define EVENTMANAGER_TDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.EventManager
struct  EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Events.UnityEvent> DLLCore.EventManager::a
	Dictionary_2_t144E26F2FE60EC50B62AC7C9425BF26CB5CE269F * ___a_4;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966, ___a_4)); }
	inline Dictionary_2_t144E26F2FE60EC50B62AC7C9425BF26CB5CE269F * get_a_4() const { return ___a_4; }
	inline Dictionary_2_t144E26F2FE60EC50B62AC7C9425BF26CB5CE269F ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Dictionary_2_t144E26F2FE60EC50B62AC7C9425BF26CB5CE269F * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}
};

struct EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966_StaticFields
{
public:
	// DLLCore.EventManager DLLCore.EventManager::b
	EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966 * ___b_5;

public:
	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966_StaticFields, ___b_5)); }
	inline EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966 * get_b_5() const { return ___b_5; }
	inline EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966 ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966 * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTMANAGER_TDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966_H
#ifndef FPSCONTROLS_TB869CAEE43D61103A67C795EDCDEDF515D5E6FEE_H
#define FPSCONTROLS_TB869CAEE43D61103A67C795EDCDEDF515D5E6FEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.FPSControls
struct  FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single DLLCore.FPSControls::speed
	float ___speed_4;
	// System.Single DLLCore.FPSControls::sensitivity
	float ___sensitivity_5;
	// UnityEngine.CharacterController DLLCore.FPSControls::a
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___a_6;
	// System.Single DLLCore.FPSControls::b
	float ___b_7;
	// System.Single DLLCore.FPSControls::c
	float ___c_8;
	// System.Single DLLCore.FPSControls::d
	float ___d_9;
	// System.Boolean DLLCore.FPSControls::e
	bool ___e_10;
	// System.Single DLLCore.FPSControls::f
	float ___f_11;
	// System.Single DLLCore.FPSControls::g
	float ___g_12;
	// System.Boolean DLLCore.FPSControls::h
	bool ___h_13;
	// System.Boolean DLLCore.FPSControls::i
	bool ___i_14;
	// System.Single DLLCore.FPSControls::j
	float ___j_15;
	// System.Single DLLCore.FPSControls::k
	float ___k_16;
	// System.Single DLLCore.FPSControls::l
	float ___l_17;
	// System.Single DLLCore.FPSControls::m
	float ___m_18;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_sensitivity_5() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___sensitivity_5)); }
	inline float get_sensitivity_5() const { return ___sensitivity_5; }
	inline float* get_address_of_sensitivity_5() { return &___sensitivity_5; }
	inline void set_sensitivity_5(float value)
	{
		___sensitivity_5 = value;
	}

	inline static int32_t get_offset_of_a_6() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___a_6)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_a_6() const { return ___a_6; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_a_6() { return &___a_6; }
	inline void set_a_6(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___a_6 = value;
		Il2CppCodeGenWriteBarrier((&___a_6), value);
	}

	inline static int32_t get_offset_of_b_7() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___b_7)); }
	inline float get_b_7() const { return ___b_7; }
	inline float* get_address_of_b_7() { return &___b_7; }
	inline void set_b_7(float value)
	{
		___b_7 = value;
	}

	inline static int32_t get_offset_of_c_8() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___c_8)); }
	inline float get_c_8() const { return ___c_8; }
	inline float* get_address_of_c_8() { return &___c_8; }
	inline void set_c_8(float value)
	{
		___c_8 = value;
	}

	inline static int32_t get_offset_of_d_9() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___d_9)); }
	inline float get_d_9() const { return ___d_9; }
	inline float* get_address_of_d_9() { return &___d_9; }
	inline void set_d_9(float value)
	{
		___d_9 = value;
	}

	inline static int32_t get_offset_of_e_10() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___e_10)); }
	inline bool get_e_10() const { return ___e_10; }
	inline bool* get_address_of_e_10() { return &___e_10; }
	inline void set_e_10(bool value)
	{
		___e_10 = value;
	}

	inline static int32_t get_offset_of_f_11() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___f_11)); }
	inline float get_f_11() const { return ___f_11; }
	inline float* get_address_of_f_11() { return &___f_11; }
	inline void set_f_11(float value)
	{
		___f_11 = value;
	}

	inline static int32_t get_offset_of_g_12() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___g_12)); }
	inline float get_g_12() const { return ___g_12; }
	inline float* get_address_of_g_12() { return &___g_12; }
	inline void set_g_12(float value)
	{
		___g_12 = value;
	}

	inline static int32_t get_offset_of_h_13() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___h_13)); }
	inline bool get_h_13() const { return ___h_13; }
	inline bool* get_address_of_h_13() { return &___h_13; }
	inline void set_h_13(bool value)
	{
		___h_13 = value;
	}

	inline static int32_t get_offset_of_i_14() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___i_14)); }
	inline bool get_i_14() const { return ___i_14; }
	inline bool* get_address_of_i_14() { return &___i_14; }
	inline void set_i_14(bool value)
	{
		___i_14 = value;
	}

	inline static int32_t get_offset_of_j_15() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___j_15)); }
	inline float get_j_15() const { return ___j_15; }
	inline float* get_address_of_j_15() { return &___j_15; }
	inline void set_j_15(float value)
	{
		___j_15 = value;
	}

	inline static int32_t get_offset_of_k_16() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___k_16)); }
	inline float get_k_16() const { return ___k_16; }
	inline float* get_address_of_k_16() { return &___k_16; }
	inline void set_k_16(float value)
	{
		___k_16 = value;
	}

	inline static int32_t get_offset_of_l_17() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___l_17)); }
	inline float get_l_17() const { return ___l_17; }
	inline float* get_address_of_l_17() { return &___l_17; }
	inline void set_l_17(float value)
	{
		___l_17 = value;
	}

	inline static int32_t get_offset_of_m_18() { return static_cast<int32_t>(offsetof(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE, ___m_18)); }
	inline float get_m_18() const { return ___m_18; }
	inline float* get_address_of_m_18() { return &___m_18; }
	inline void set_m_18(float value)
	{
		___m_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCONTROLS_TB869CAEE43D61103A67C795EDCDEDF515D5E6FEE_H
#ifndef GENERALSETTING_TDF026F5046230C935795FC7E9B91103E4CB2F91D_H
#define GENERALSETTING_TDF026F5046230C935795FC7E9B91103E4CB2F91D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GeneralSetting
struct  GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DLLCore.GeneralSetting_SDKMode DLLCore.GeneralSetting::a
	int32_t ___a_4;
	// DLLCore.GeneralSetting_ViewMode DLLCore.GeneralSetting::b
	int32_t ___b_5;
	// System.Boolean DLLCore.GeneralSetting::bCollision
	bool ___bCollision_6;
	// System.Boolean DLLCore.GeneralSetting::c
	bool ___c_7;
	// System.Boolean DLLCore.GeneralSetting::d
	bool ___d_8;
	// UnityEngine.Camera DLLCore.GeneralSetting::e
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___e_9;
	// UnityEngine.Cubemap DLLCore.GeneralSetting::cubemap
	Cubemap_tBFAC336F35E8D7499397F07A41505BD98F4491AF * ___cubemap_10;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D, ___a_4)); }
	inline int32_t get_a_4() const { return ___a_4; }
	inline int32_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(int32_t value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D, ___b_5)); }
	inline int32_t get_b_5() const { return ___b_5; }
	inline int32_t* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(int32_t value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_bCollision_6() { return static_cast<int32_t>(offsetof(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D, ___bCollision_6)); }
	inline bool get_bCollision_6() const { return ___bCollision_6; }
	inline bool* get_address_of_bCollision_6() { return &___bCollision_6; }
	inline void set_bCollision_6(bool value)
	{
		___bCollision_6 = value;
	}

	inline static int32_t get_offset_of_c_7() { return static_cast<int32_t>(offsetof(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D, ___c_7)); }
	inline bool get_c_7() const { return ___c_7; }
	inline bool* get_address_of_c_7() { return &___c_7; }
	inline void set_c_7(bool value)
	{
		___c_7 = value;
	}

	inline static int32_t get_offset_of_d_8() { return static_cast<int32_t>(offsetof(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D, ___d_8)); }
	inline bool get_d_8() const { return ___d_8; }
	inline bool* get_address_of_d_8() { return &___d_8; }
	inline void set_d_8(bool value)
	{
		___d_8 = value;
	}

	inline static int32_t get_offset_of_e_9() { return static_cast<int32_t>(offsetof(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D, ___e_9)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_e_9() const { return ___e_9; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_e_9() { return &___e_9; }
	inline void set_e_9(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___e_9 = value;
		Il2CppCodeGenWriteBarrier((&___e_9), value);
	}

	inline static int32_t get_offset_of_cubemap_10() { return static_cast<int32_t>(offsetof(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D, ___cubemap_10)); }
	inline Cubemap_tBFAC336F35E8D7499397F07A41505BD98F4491AF * get_cubemap_10() const { return ___cubemap_10; }
	inline Cubemap_tBFAC336F35E8D7499397F07A41505BD98F4491AF ** get_address_of_cubemap_10() { return &___cubemap_10; }
	inline void set_cubemap_10(Cubemap_tBFAC336F35E8D7499397F07A41505BD98F4491AF * value)
	{
		___cubemap_10 = value;
		Il2CppCodeGenWriteBarrier((&___cubemap_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALSETTING_TDF026F5046230C935795FC7E9B91103E4CB2F91D_H
#ifndef MEASUREAREA_TC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7_H
#define MEASUREAREA_TC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MeasureArea
struct  MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GUIStyle DLLCore.MeasureArea::a
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___a_4;
	// UnityEngine.Texture2D DLLCore.MeasureArea::b
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___b_5;
	// UnityEngine.Vector3[] DLLCore.MeasureArea::c
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___c_6;
	// UnityEngine.Vector3[] DLLCore.MeasureArea::d
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___d_7;
	// UnityEngine.Vector3[] DLLCore.MeasureArea::e
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___e_8;
	// System.Int32 DLLCore.MeasureArea::f
	int32_t ___f_9;
	// UnityEngine.Color DLLCore.MeasureArea::g
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___g_10;
	// UnityEngine.Color DLLCore.MeasureArea::h
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___h_11;
	// System.Single DLLCore.MeasureArea::i
	float ___i_12;
	// UnityEngine.Material DLLCore.MeasureArea::j
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___j_13;
	// System.Int32 DLLCore.MeasureArea::k
	int32_t ___k_14;
	// UnityEngine.Camera DLLCore.MeasureArea::l
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___l_15;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___a_4)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_a_4() const { return ___a_4; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___b_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_b_5() const { return ___b_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___c_6)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_c_6() const { return ___c_6; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___d_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_d_7() const { return ___d_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}

	inline static int32_t get_offset_of_e_8() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___e_8)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_e_8() const { return ___e_8; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_e_8() { return &___e_8; }
	inline void set_e_8(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___e_8 = value;
		Il2CppCodeGenWriteBarrier((&___e_8), value);
	}

	inline static int32_t get_offset_of_f_9() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___f_9)); }
	inline int32_t get_f_9() const { return ___f_9; }
	inline int32_t* get_address_of_f_9() { return &___f_9; }
	inline void set_f_9(int32_t value)
	{
		___f_9 = value;
	}

	inline static int32_t get_offset_of_g_10() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___g_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_g_10() const { return ___g_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_g_10() { return &___g_10; }
	inline void set_g_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___g_10 = value;
	}

	inline static int32_t get_offset_of_h_11() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___h_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_h_11() const { return ___h_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_h_11() { return &___h_11; }
	inline void set_h_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___h_11 = value;
	}

	inline static int32_t get_offset_of_i_12() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___i_12)); }
	inline float get_i_12() const { return ___i_12; }
	inline float* get_address_of_i_12() { return &___i_12; }
	inline void set_i_12(float value)
	{
		___i_12 = value;
	}

	inline static int32_t get_offset_of_j_13() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___j_13)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_j_13() const { return ___j_13; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_j_13() { return &___j_13; }
	inline void set_j_13(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___j_13 = value;
		Il2CppCodeGenWriteBarrier((&___j_13), value);
	}

	inline static int32_t get_offset_of_k_14() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___k_14)); }
	inline int32_t get_k_14() const { return ___k_14; }
	inline int32_t* get_address_of_k_14() { return &___k_14; }
	inline void set_k_14(int32_t value)
	{
		___k_14 = value;
	}

	inline static int32_t get_offset_of_l_15() { return static_cast<int32_t>(offsetof(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7, ___l_15)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_l_15() const { return ___l_15; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_l_15() { return &___l_15; }
	inline void set_l_15(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___l_15 = value;
		Il2CppCodeGenWriteBarrier((&___l_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEASUREAREA_TC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7_H
#ifndef MEASUREDISTANCE_T5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA_H
#define MEASUREDISTANCE_T5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MeasureDistance
struct  MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GUIStyle DLLCore.MeasureDistance::a
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___a_4;
	// UnityEngine.Texture2D DLLCore.MeasureDistance::b
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___b_5;
	// UnityEngine.Vector3[] DLLCore.MeasureDistance::c
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___c_6;
	// UnityEngine.Vector3[] DLLCore.MeasureDistance::d
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___d_7;
	// UnityEngine.Vector3[] DLLCore.MeasureDistance::e
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___e_8;
	// System.Int32 DLLCore.MeasureDistance::f
	int32_t ___f_9;
	// UnityEngine.Color DLLCore.MeasureDistance::g
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___g_10;
	// System.Int32 DLLCore.MeasureDistance::h
	int32_t ___h_11;
	// UnityEngine.Camera DLLCore.MeasureDistance::i
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___i_12;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA, ___a_4)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_a_4() const { return ___a_4; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA, ___b_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_b_5() const { return ___b_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA, ___c_6)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_c_6() const { return ___c_6; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA, ___d_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_d_7() const { return ___d_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}

	inline static int32_t get_offset_of_e_8() { return static_cast<int32_t>(offsetof(MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA, ___e_8)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_e_8() const { return ___e_8; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_e_8() { return &___e_8; }
	inline void set_e_8(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___e_8 = value;
		Il2CppCodeGenWriteBarrier((&___e_8), value);
	}

	inline static int32_t get_offset_of_f_9() { return static_cast<int32_t>(offsetof(MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA, ___f_9)); }
	inline int32_t get_f_9() const { return ___f_9; }
	inline int32_t* get_address_of_f_9() { return &___f_9; }
	inline void set_f_9(int32_t value)
	{
		___f_9 = value;
	}

	inline static int32_t get_offset_of_g_10() { return static_cast<int32_t>(offsetof(MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA, ___g_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_g_10() const { return ___g_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_g_10() { return &___g_10; }
	inline void set_g_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___g_10 = value;
	}

	inline static int32_t get_offset_of_h_11() { return static_cast<int32_t>(offsetof(MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA, ___h_11)); }
	inline int32_t get_h_11() const { return ___h_11; }
	inline int32_t* get_address_of_h_11() { return &___h_11; }
	inline void set_h_11(int32_t value)
	{
		___h_11 = value;
	}

	inline static int32_t get_offset_of_i_12() { return static_cast<int32_t>(offsetof(MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA, ___i_12)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_i_12() const { return ___i_12; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_i_12() { return &___i_12; }
	inline void set_i_12(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___i_12 = value;
		Il2CppCodeGenWriteBarrier((&___i_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEASUREDISTANCE_T5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA_H
#ifndef MINIMAPCONTROLS_T24945DFD5710E6BA3278D23CAF9377F85B3EF0FF_H
#define MINIMAPCONTROLS_T24945DFD5710E6BA3278D23CAF9377F85B3EF0FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MiniMapControls
struct  MiniMapControls_t24945DFD5710E6BA3278D23CAF9377F85B3EF0FF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector2 DLLCore.MiniMapControls::a
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a_4;
	// UnityEngine.Sprite DLLCore.MiniMapControls::b
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___b_5;
	// UnityEngine.GameObject DLLCore.MiniMapControls::c
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___c_6;
	// UnityEngine.Camera DLLCore.MiniMapControls::d
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___d_7;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(MiniMapControls_t24945DFD5710E6BA3278D23CAF9377F85B3EF0FF, ___a_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_a_4() const { return ___a_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(MiniMapControls_t24945DFD5710E6BA3278D23CAF9377F85B3EF0FF, ___b_5)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_b_5() const { return ___b_5; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(MiniMapControls_t24945DFD5710E6BA3278D23CAF9377F85B3EF0FF, ___c_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_c_6() const { return ___c_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(MiniMapControls_t24945DFD5710E6BA3278D23CAF9377F85B3EF0FF, ___d_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_d_7() const { return ___d_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMAPCONTROLS_T24945DFD5710E6BA3278D23CAF9377F85B3EF0FF_H
#ifndef ORBITCONTROLS_TB10120B3A0D9C3857C7A0D3E8772DF91C88E0403_H
#define ORBITCONTROLS_TB10120B3A0D9C3857C7A0D3E8772DF91C88E0403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.OrbitControls
struct  OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject DLLCore.OrbitControls::a
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___a_4;
	// UnityEngine.Vector3 DLLCore.OrbitControls::b
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b_5;
	// System.Single DLLCore.OrbitControls::c
	float ___c_6;
	// System.Single DLLCore.OrbitControls::d
	float ___d_7;
	// System.Single DLLCore.OrbitControls::e
	float ___e_8;
	// System.Single DLLCore.OrbitControls::f
	float ___f_9;
	// System.Single DLLCore.OrbitControls::g
	float ___g_10;
	// System.Single DLLCore.OrbitControls::h
	float ___h_11;
	// System.Int32 DLLCore.OrbitControls::i
	int32_t ___i_12;
	// System.Int32 DLLCore.OrbitControls::j
	int32_t ___j_13;
	// System.Single DLLCore.OrbitControls::k
	float ___k_14;
	// System.Single DLLCore.OrbitControls::l
	float ___l_15;
	// UnityEngine.Vector3 DLLCore.OrbitControls::_rotDegreeEuler
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ____rotDegreeEuler_16;
	// System.Single DLLCore.OrbitControls::m
	float ___m_17;
	// System.Single DLLCore.OrbitControls::n
	float ___n_18;
	// UnityEngine.Vector3 DLLCore.OrbitControls::o
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___o_19;
	// System.Single DLLCore.OrbitControls::p
	float ___p_20;
	// System.Single DLLCore.OrbitControls::q
	float ___q_21;
	// System.Single DLLCore.OrbitControls::r
	float ___r_22;
	// System.Single DLLCore.OrbitControls::s
	float ___s_23;
	// System.Single DLLCore.OrbitControls::t
	float ___t_24;
	// System.Single DLLCore.OrbitControls::u
	float ___u_25;
	// System.Single DLLCore.OrbitControls::v
	float ___v_26;
	// System.Boolean DLLCore.OrbitControls::w
	bool ___w_27;
	// System.Boolean DLLCore.OrbitControls::x
	bool ___x_28;
	// System.Boolean DLLCore.OrbitControls::y
	bool ___y_29;
	// UnityEngine.Vector3 DLLCore.OrbitControls::z
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___z_30;
	// System.Boolean DLLCore.OrbitControls::aa
	bool ___aa_31;
	// System.Single DLLCore.OrbitControls::ab
	float ___ab_32;
	// UnityEngine.Camera DLLCore.OrbitControls::ac
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___ac_33;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___a_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_a_4() const { return ___a_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___b_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_b_5() const { return ___b_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___c_6)); }
	inline float get_c_6() const { return ___c_6; }
	inline float* get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(float value)
	{
		___c_6 = value;
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___d_7)); }
	inline float get_d_7() const { return ___d_7; }
	inline float* get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(float value)
	{
		___d_7 = value;
	}

	inline static int32_t get_offset_of_e_8() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___e_8)); }
	inline float get_e_8() const { return ___e_8; }
	inline float* get_address_of_e_8() { return &___e_8; }
	inline void set_e_8(float value)
	{
		___e_8 = value;
	}

	inline static int32_t get_offset_of_f_9() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___f_9)); }
	inline float get_f_9() const { return ___f_9; }
	inline float* get_address_of_f_9() { return &___f_9; }
	inline void set_f_9(float value)
	{
		___f_9 = value;
	}

	inline static int32_t get_offset_of_g_10() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___g_10)); }
	inline float get_g_10() const { return ___g_10; }
	inline float* get_address_of_g_10() { return &___g_10; }
	inline void set_g_10(float value)
	{
		___g_10 = value;
	}

	inline static int32_t get_offset_of_h_11() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___h_11)); }
	inline float get_h_11() const { return ___h_11; }
	inline float* get_address_of_h_11() { return &___h_11; }
	inline void set_h_11(float value)
	{
		___h_11 = value;
	}

	inline static int32_t get_offset_of_i_12() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___i_12)); }
	inline int32_t get_i_12() const { return ___i_12; }
	inline int32_t* get_address_of_i_12() { return &___i_12; }
	inline void set_i_12(int32_t value)
	{
		___i_12 = value;
	}

	inline static int32_t get_offset_of_j_13() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___j_13)); }
	inline int32_t get_j_13() const { return ___j_13; }
	inline int32_t* get_address_of_j_13() { return &___j_13; }
	inline void set_j_13(int32_t value)
	{
		___j_13 = value;
	}

	inline static int32_t get_offset_of_k_14() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___k_14)); }
	inline float get_k_14() const { return ___k_14; }
	inline float* get_address_of_k_14() { return &___k_14; }
	inline void set_k_14(float value)
	{
		___k_14 = value;
	}

	inline static int32_t get_offset_of_l_15() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___l_15)); }
	inline float get_l_15() const { return ___l_15; }
	inline float* get_address_of_l_15() { return &___l_15; }
	inline void set_l_15(float value)
	{
		___l_15 = value;
	}

	inline static int32_t get_offset_of__rotDegreeEuler_16() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ____rotDegreeEuler_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get__rotDegreeEuler_16() const { return ____rotDegreeEuler_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of__rotDegreeEuler_16() { return &____rotDegreeEuler_16; }
	inline void set__rotDegreeEuler_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		____rotDegreeEuler_16 = value;
	}

	inline static int32_t get_offset_of_m_17() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___m_17)); }
	inline float get_m_17() const { return ___m_17; }
	inline float* get_address_of_m_17() { return &___m_17; }
	inline void set_m_17(float value)
	{
		___m_17 = value;
	}

	inline static int32_t get_offset_of_n_18() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___n_18)); }
	inline float get_n_18() const { return ___n_18; }
	inline float* get_address_of_n_18() { return &___n_18; }
	inline void set_n_18(float value)
	{
		___n_18 = value;
	}

	inline static int32_t get_offset_of_o_19() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___o_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_o_19() const { return ___o_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_o_19() { return &___o_19; }
	inline void set_o_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___o_19 = value;
	}

	inline static int32_t get_offset_of_p_20() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___p_20)); }
	inline float get_p_20() const { return ___p_20; }
	inline float* get_address_of_p_20() { return &___p_20; }
	inline void set_p_20(float value)
	{
		___p_20 = value;
	}

	inline static int32_t get_offset_of_q_21() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___q_21)); }
	inline float get_q_21() const { return ___q_21; }
	inline float* get_address_of_q_21() { return &___q_21; }
	inline void set_q_21(float value)
	{
		___q_21 = value;
	}

	inline static int32_t get_offset_of_r_22() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___r_22)); }
	inline float get_r_22() const { return ___r_22; }
	inline float* get_address_of_r_22() { return &___r_22; }
	inline void set_r_22(float value)
	{
		___r_22 = value;
	}

	inline static int32_t get_offset_of_s_23() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___s_23)); }
	inline float get_s_23() const { return ___s_23; }
	inline float* get_address_of_s_23() { return &___s_23; }
	inline void set_s_23(float value)
	{
		___s_23 = value;
	}

	inline static int32_t get_offset_of_t_24() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___t_24)); }
	inline float get_t_24() const { return ___t_24; }
	inline float* get_address_of_t_24() { return &___t_24; }
	inline void set_t_24(float value)
	{
		___t_24 = value;
	}

	inline static int32_t get_offset_of_u_25() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___u_25)); }
	inline float get_u_25() const { return ___u_25; }
	inline float* get_address_of_u_25() { return &___u_25; }
	inline void set_u_25(float value)
	{
		___u_25 = value;
	}

	inline static int32_t get_offset_of_v_26() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___v_26)); }
	inline float get_v_26() const { return ___v_26; }
	inline float* get_address_of_v_26() { return &___v_26; }
	inline void set_v_26(float value)
	{
		___v_26 = value;
	}

	inline static int32_t get_offset_of_w_27() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___w_27)); }
	inline bool get_w_27() const { return ___w_27; }
	inline bool* get_address_of_w_27() { return &___w_27; }
	inline void set_w_27(bool value)
	{
		___w_27 = value;
	}

	inline static int32_t get_offset_of_x_28() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___x_28)); }
	inline bool get_x_28() const { return ___x_28; }
	inline bool* get_address_of_x_28() { return &___x_28; }
	inline void set_x_28(bool value)
	{
		___x_28 = value;
	}

	inline static int32_t get_offset_of_y_29() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___y_29)); }
	inline bool get_y_29() const { return ___y_29; }
	inline bool* get_address_of_y_29() { return &___y_29; }
	inline void set_y_29(bool value)
	{
		___y_29 = value;
	}

	inline static int32_t get_offset_of_z_30() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___z_30)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_z_30() const { return ___z_30; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_z_30() { return &___z_30; }
	inline void set_z_30(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___z_30 = value;
	}

	inline static int32_t get_offset_of_aa_31() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___aa_31)); }
	inline bool get_aa_31() const { return ___aa_31; }
	inline bool* get_address_of_aa_31() { return &___aa_31; }
	inline void set_aa_31(bool value)
	{
		___aa_31 = value;
	}

	inline static int32_t get_offset_of_ab_32() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___ab_32)); }
	inline float get_ab_32() const { return ___ab_32; }
	inline float* get_address_of_ab_32() { return &___ab_32; }
	inline void set_ab_32(float value)
	{
		___ab_32 = value;
	}

	inline static int32_t get_offset_of_ac_33() { return static_cast<int32_t>(offsetof(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403, ___ac_33)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_ac_33() const { return ___ac_33; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_ac_33() { return &___ac_33; }
	inline void set_ac_33(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___ac_33 = value;
		Il2CppCodeGenWriteBarrier((&___ac_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORBITCONTROLS_TB10120B3A0D9C3857C7A0D3E8772DF91C88E0403_H
#ifndef SENATURALBLOOMANDDIRTYLENS_T55A288C47AC896F7CC9231E333EF2CDA4A2BDF84_H
#define SENATURALBLOOMANDDIRTYLENS_T55A288C47AC896F7CC9231E333EF2CDA4A2BDF84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.SENaturalBloomAndDirtyLens
struct  SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single DLLCore.SENaturalBloomAndDirtyLens::bloomIntensity
	float ___bloomIntensity_4;
	// UnityEngine.Shader DLLCore.SENaturalBloomAndDirtyLens::shader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___shader_5;
	// UnityEngine.Material DLLCore.SENaturalBloomAndDirtyLens::a
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___a_6;
	// UnityEngine.Texture2D DLLCore.SENaturalBloomAndDirtyLens::lensDirtTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___lensDirtTexture_7;
	// System.Single DLLCore.SENaturalBloomAndDirtyLens::lensDirtIntensity
	float ___lensDirtIntensity_8;
	// System.Boolean DLLCore.SENaturalBloomAndDirtyLens::b
	bool ___b_9;
	// System.Single DLLCore.SENaturalBloomAndDirtyLens::c
	float ___c_10;
	// System.Boolean DLLCore.SENaturalBloomAndDirtyLens::inputIsHDR
	bool ___inputIsHDR_11;

public:
	inline static int32_t get_offset_of_bloomIntensity_4() { return static_cast<int32_t>(offsetof(SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84, ___bloomIntensity_4)); }
	inline float get_bloomIntensity_4() const { return ___bloomIntensity_4; }
	inline float* get_address_of_bloomIntensity_4() { return &___bloomIntensity_4; }
	inline void set_bloomIntensity_4(float value)
	{
		___bloomIntensity_4 = value;
	}

	inline static int32_t get_offset_of_shader_5() { return static_cast<int32_t>(offsetof(SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84, ___shader_5)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_shader_5() const { return ___shader_5; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_shader_5() { return &___shader_5; }
	inline void set_shader_5(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___shader_5 = value;
		Il2CppCodeGenWriteBarrier((&___shader_5), value);
	}

	inline static int32_t get_offset_of_a_6() { return static_cast<int32_t>(offsetof(SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84, ___a_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_a_6() const { return ___a_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_a_6() { return &___a_6; }
	inline void set_a_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___a_6 = value;
		Il2CppCodeGenWriteBarrier((&___a_6), value);
	}

	inline static int32_t get_offset_of_lensDirtTexture_7() { return static_cast<int32_t>(offsetof(SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84, ___lensDirtTexture_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_lensDirtTexture_7() const { return ___lensDirtTexture_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_lensDirtTexture_7() { return &___lensDirtTexture_7; }
	inline void set_lensDirtTexture_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___lensDirtTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___lensDirtTexture_7), value);
	}

	inline static int32_t get_offset_of_lensDirtIntensity_8() { return static_cast<int32_t>(offsetof(SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84, ___lensDirtIntensity_8)); }
	inline float get_lensDirtIntensity_8() const { return ___lensDirtIntensity_8; }
	inline float* get_address_of_lensDirtIntensity_8() { return &___lensDirtIntensity_8; }
	inline void set_lensDirtIntensity_8(float value)
	{
		___lensDirtIntensity_8 = value;
	}

	inline static int32_t get_offset_of_b_9() { return static_cast<int32_t>(offsetof(SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84, ___b_9)); }
	inline bool get_b_9() const { return ___b_9; }
	inline bool* get_address_of_b_9() { return &___b_9; }
	inline void set_b_9(bool value)
	{
		___b_9 = value;
	}

	inline static int32_t get_offset_of_c_10() { return static_cast<int32_t>(offsetof(SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84, ___c_10)); }
	inline float get_c_10() const { return ___c_10; }
	inline float* get_address_of_c_10() { return &___c_10; }
	inline void set_c_10(float value)
	{
		___c_10 = value;
	}

	inline static int32_t get_offset_of_inputIsHDR_11() { return static_cast<int32_t>(offsetof(SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84, ___inputIsHDR_11)); }
	inline bool get_inputIsHDR_11() const { return ___inputIsHDR_11; }
	inline bool* get_address_of_inputIsHDR_11() { return &___inputIsHDR_11; }
	inline void set_inputIsHDR_11(bool value)
	{
		___inputIsHDR_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENATURALBLOOMANDDIRTYLENS_T55A288C47AC896F7CC9231E333EF2CDA4A2BDF84_H
#ifndef OUTLINE_T1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3_H
#define OUTLINE_T1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outline
struct  Outline_t1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer[] Outline::a
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* ___a_4;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Outline_t1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3, ___a_4)); }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* get_a_4() const { return ___a_4; }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3_H
#ifndef OUTLINEEFFECT_TFEB9908FEA7A02A88D086B424872A2E21775B3C0_H
#define OUTLINEEFFECT_TFEB9908FEA7A02A88D086B424872A2E21775B3C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OutlineEffect
struct  OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// LinkedSet`1<Outline> OutlineEffect::b
	LinkedSet_1_tF77FC771D12DBA9E848B2192E7A6A776AA24BACA * ___b_5;
	// System.Single OutlineEffect::lineThickness
	float ___lineThickness_6;
	// System.Single OutlineEffect::lineIntensity
	float ___lineIntensity_7;
	// System.Single OutlineEffect::fillAmount
	float ___fillAmount_8;
	// UnityEngine.Color OutlineEffect::lineColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___lineColor_9;
	// System.Boolean OutlineEffect::additiveRendering
	bool ___additiveRendering_10;
	// System.Boolean OutlineEffect::backfaceCulling
	bool ___backfaceCulling_11;
	// System.Boolean OutlineEffect::cornerOutlines
	bool ___cornerOutlines_12;
	// System.Boolean OutlineEffect::addLinesBetweenColors
	bool ___addLinesBetweenColors_13;
	// System.Boolean OutlineEffect::scaleWithScreenSize
	bool ___scaleWithScreenSize_14;
	// System.Single OutlineEffect::alphaCutoff
	float ___alphaCutoff_15;
	// System.Boolean OutlineEffect::flipY
	bool ___flipY_16;
	// UnityEngine.Camera OutlineEffect::sourceCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___sourceCamera_17;
	// UnityEngine.Camera OutlineEffect::outlineCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___outlineCamera_18;
	// UnityEngine.Material OutlineEffect::c
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___c_19;
	// UnityEngine.Material OutlineEffect::d
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___d_20;
	// UnityEngine.Material OutlineEffect::e
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___e_21;
	// UnityEngine.Shader OutlineEffect::f
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___f_22;
	// UnityEngine.Shader OutlineEffect::g
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___g_23;
	// UnityEngine.Material OutlineEffect::outlineShaderMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___outlineShaderMaterial_24;
	// UnityEngine.RenderTexture OutlineEffect::renderTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___renderTexture_25;
	// UnityEngine.RenderTexture OutlineEffect::extraRenderTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___extraRenderTexture_26;
	// UnityEngine.Rendering.CommandBuffer OutlineEffect::h
	CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * ___h_27;
	// System.Collections.Generic.List`1<UnityEngine.Material> OutlineEffect::i
	List_1_tB4444E94203E77FC813B61173EB21B8410EB7155 * ___i_28;

public:
	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___b_5)); }
	inline LinkedSet_1_tF77FC771D12DBA9E848B2192E7A6A776AA24BACA * get_b_5() const { return ___b_5; }
	inline LinkedSet_1_tF77FC771D12DBA9E848B2192E7A6A776AA24BACA ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(LinkedSet_1_tF77FC771D12DBA9E848B2192E7A6A776AA24BACA * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_lineThickness_6() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___lineThickness_6)); }
	inline float get_lineThickness_6() const { return ___lineThickness_6; }
	inline float* get_address_of_lineThickness_6() { return &___lineThickness_6; }
	inline void set_lineThickness_6(float value)
	{
		___lineThickness_6 = value;
	}

	inline static int32_t get_offset_of_lineIntensity_7() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___lineIntensity_7)); }
	inline float get_lineIntensity_7() const { return ___lineIntensity_7; }
	inline float* get_address_of_lineIntensity_7() { return &___lineIntensity_7; }
	inline void set_lineIntensity_7(float value)
	{
		___lineIntensity_7 = value;
	}

	inline static int32_t get_offset_of_fillAmount_8() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___fillAmount_8)); }
	inline float get_fillAmount_8() const { return ___fillAmount_8; }
	inline float* get_address_of_fillAmount_8() { return &___fillAmount_8; }
	inline void set_fillAmount_8(float value)
	{
		___fillAmount_8 = value;
	}

	inline static int32_t get_offset_of_lineColor_9() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___lineColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_lineColor_9() const { return ___lineColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_lineColor_9() { return &___lineColor_9; }
	inline void set_lineColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___lineColor_9 = value;
	}

	inline static int32_t get_offset_of_additiveRendering_10() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___additiveRendering_10)); }
	inline bool get_additiveRendering_10() const { return ___additiveRendering_10; }
	inline bool* get_address_of_additiveRendering_10() { return &___additiveRendering_10; }
	inline void set_additiveRendering_10(bool value)
	{
		___additiveRendering_10 = value;
	}

	inline static int32_t get_offset_of_backfaceCulling_11() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___backfaceCulling_11)); }
	inline bool get_backfaceCulling_11() const { return ___backfaceCulling_11; }
	inline bool* get_address_of_backfaceCulling_11() { return &___backfaceCulling_11; }
	inline void set_backfaceCulling_11(bool value)
	{
		___backfaceCulling_11 = value;
	}

	inline static int32_t get_offset_of_cornerOutlines_12() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___cornerOutlines_12)); }
	inline bool get_cornerOutlines_12() const { return ___cornerOutlines_12; }
	inline bool* get_address_of_cornerOutlines_12() { return &___cornerOutlines_12; }
	inline void set_cornerOutlines_12(bool value)
	{
		___cornerOutlines_12 = value;
	}

	inline static int32_t get_offset_of_addLinesBetweenColors_13() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___addLinesBetweenColors_13)); }
	inline bool get_addLinesBetweenColors_13() const { return ___addLinesBetweenColors_13; }
	inline bool* get_address_of_addLinesBetweenColors_13() { return &___addLinesBetweenColors_13; }
	inline void set_addLinesBetweenColors_13(bool value)
	{
		___addLinesBetweenColors_13 = value;
	}

	inline static int32_t get_offset_of_scaleWithScreenSize_14() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___scaleWithScreenSize_14)); }
	inline bool get_scaleWithScreenSize_14() const { return ___scaleWithScreenSize_14; }
	inline bool* get_address_of_scaleWithScreenSize_14() { return &___scaleWithScreenSize_14; }
	inline void set_scaleWithScreenSize_14(bool value)
	{
		___scaleWithScreenSize_14 = value;
	}

	inline static int32_t get_offset_of_alphaCutoff_15() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___alphaCutoff_15)); }
	inline float get_alphaCutoff_15() const { return ___alphaCutoff_15; }
	inline float* get_address_of_alphaCutoff_15() { return &___alphaCutoff_15; }
	inline void set_alphaCutoff_15(float value)
	{
		___alphaCutoff_15 = value;
	}

	inline static int32_t get_offset_of_flipY_16() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___flipY_16)); }
	inline bool get_flipY_16() const { return ___flipY_16; }
	inline bool* get_address_of_flipY_16() { return &___flipY_16; }
	inline void set_flipY_16(bool value)
	{
		___flipY_16 = value;
	}

	inline static int32_t get_offset_of_sourceCamera_17() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___sourceCamera_17)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_sourceCamera_17() const { return ___sourceCamera_17; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_sourceCamera_17() { return &___sourceCamera_17; }
	inline void set_sourceCamera_17(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___sourceCamera_17 = value;
		Il2CppCodeGenWriteBarrier((&___sourceCamera_17), value);
	}

	inline static int32_t get_offset_of_outlineCamera_18() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___outlineCamera_18)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_outlineCamera_18() const { return ___outlineCamera_18; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_outlineCamera_18() { return &___outlineCamera_18; }
	inline void set_outlineCamera_18(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___outlineCamera_18 = value;
		Il2CppCodeGenWriteBarrier((&___outlineCamera_18), value);
	}

	inline static int32_t get_offset_of_c_19() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___c_19)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_c_19() const { return ___c_19; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_c_19() { return &___c_19; }
	inline void set_c_19(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___c_19 = value;
		Il2CppCodeGenWriteBarrier((&___c_19), value);
	}

	inline static int32_t get_offset_of_d_20() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___d_20)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_d_20() const { return ___d_20; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_d_20() { return &___d_20; }
	inline void set_d_20(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___d_20 = value;
		Il2CppCodeGenWriteBarrier((&___d_20), value);
	}

	inline static int32_t get_offset_of_e_21() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___e_21)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_e_21() const { return ___e_21; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_e_21() { return &___e_21; }
	inline void set_e_21(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___e_21 = value;
		Il2CppCodeGenWriteBarrier((&___e_21), value);
	}

	inline static int32_t get_offset_of_f_22() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___f_22)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_f_22() const { return ___f_22; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_f_22() { return &___f_22; }
	inline void set_f_22(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___f_22 = value;
		Il2CppCodeGenWriteBarrier((&___f_22), value);
	}

	inline static int32_t get_offset_of_g_23() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___g_23)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_g_23() const { return ___g_23; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_g_23() { return &___g_23; }
	inline void set_g_23(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___g_23 = value;
		Il2CppCodeGenWriteBarrier((&___g_23), value);
	}

	inline static int32_t get_offset_of_outlineShaderMaterial_24() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___outlineShaderMaterial_24)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_outlineShaderMaterial_24() const { return ___outlineShaderMaterial_24; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_outlineShaderMaterial_24() { return &___outlineShaderMaterial_24; }
	inline void set_outlineShaderMaterial_24(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___outlineShaderMaterial_24 = value;
		Il2CppCodeGenWriteBarrier((&___outlineShaderMaterial_24), value);
	}

	inline static int32_t get_offset_of_renderTexture_25() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___renderTexture_25)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_renderTexture_25() const { return ___renderTexture_25; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_renderTexture_25() { return &___renderTexture_25; }
	inline void set_renderTexture_25(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___renderTexture_25 = value;
		Il2CppCodeGenWriteBarrier((&___renderTexture_25), value);
	}

	inline static int32_t get_offset_of_extraRenderTexture_26() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___extraRenderTexture_26)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_extraRenderTexture_26() const { return ___extraRenderTexture_26; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_extraRenderTexture_26() { return &___extraRenderTexture_26; }
	inline void set_extraRenderTexture_26(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___extraRenderTexture_26 = value;
		Il2CppCodeGenWriteBarrier((&___extraRenderTexture_26), value);
	}

	inline static int32_t get_offset_of_h_27() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___h_27)); }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * get_h_27() const { return ___h_27; }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD ** get_address_of_h_27() { return &___h_27; }
	inline void set_h_27(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * value)
	{
		___h_27 = value;
		Il2CppCodeGenWriteBarrier((&___h_27), value);
	}

	inline static int32_t get_offset_of_i_28() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0, ___i_28)); }
	inline List_1_tB4444E94203E77FC813B61173EB21B8410EB7155 * get_i_28() const { return ___i_28; }
	inline List_1_tB4444E94203E77FC813B61173EB21B8410EB7155 ** get_address_of_i_28() { return &___i_28; }
	inline void set_i_28(List_1_tB4444E94203E77FC813B61173EB21B8410EB7155 * value)
	{
		___i_28 = value;
		Il2CppCodeGenWriteBarrier((&___i_28), value);
	}
};

struct OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0_StaticFields
{
public:
	// OutlineEffect OutlineEffect::a
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0 * ___a_4;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0_StaticFields, ___a_4)); }
	inline OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0 * get_a_4() const { return ___a_4; }
	inline OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINEEFFECT_TFEB9908FEA7A02A88D086B424872A2E21775B3C0_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef DAYLIGHTCONTROLLER_T72313C4151D9FABA537C31965E03B41E05941BD2_H
#define DAYLIGHTCONTROLLER_T72313C4151D9FABA537C31965E03B41E05941BD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.DaylightController
struct  DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2  : public Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90
{
public:
	// System.Int32 DLLCore.DaylightController::a
	int32_t ___a_4;
	// System.Int32 DLLCore.DaylightController::b
	int32_t ___b_5;
	// System.Int32 DLLCore.DaylightController::c
	int32_t ___c_6;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2, ___a_4)); }
	inline int32_t get_a_4() const { return ___a_4; }
	inline int32_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(int32_t value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2, ___b_5)); }
	inline int32_t get_b_5() const { return ___b_5; }
	inline int32_t* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(int32_t value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2, ___c_6)); }
	inline int32_t get_c_6() const { return ___c_6; }
	inline int32_t* get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(int32_t value)
	{
		___c_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAYLIGHTCONTROLLER_T72313C4151D9FABA537C31965E03B41E05941BD2_H
#ifndef ASPECTRATIOFITTER_T3CA8A085831067C09B872C67F6E7F6F4EBB967B6_H
#define ASPECTRATIOFITTER_T3CA8A085831067C09B872C67F6E7F6F4EBB967B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter
struct  AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.AspectRatioFitter_AspectMode UnityEngine.UI.AspectRatioFitter::m_AspectMode
	int32_t ___m_AspectMode_4;
	// System.Single UnityEngine.UI.AspectRatioFitter::m_AspectRatio
	float ___m_AspectRatio_5;
	// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::m_Rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Rect_6;
	// System.Boolean UnityEngine.UI.AspectRatioFitter::m_DelayedSetDirty
	bool ___m_DelayedSetDirty_7;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.AspectRatioFitter::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_8;

public:
	inline static int32_t get_offset_of_m_AspectMode_4() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6, ___m_AspectMode_4)); }
	inline int32_t get_m_AspectMode_4() const { return ___m_AspectMode_4; }
	inline int32_t* get_address_of_m_AspectMode_4() { return &___m_AspectMode_4; }
	inline void set_m_AspectMode_4(int32_t value)
	{
		___m_AspectMode_4 = value;
	}

	inline static int32_t get_offset_of_m_AspectRatio_5() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6, ___m_AspectRatio_5)); }
	inline float get_m_AspectRatio_5() const { return ___m_AspectRatio_5; }
	inline float* get_address_of_m_AspectRatio_5() { return &___m_AspectRatio_5; }
	inline void set_m_AspectRatio_5(float value)
	{
		___m_AspectRatio_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6, ___m_Rect_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_DelayedSetDirty_7() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6, ___m_DelayedSetDirty_7)); }
	inline bool get_m_DelayedSetDirty_7() const { return ___m_DelayedSetDirty_7; }
	inline bool* get_address_of_m_DelayedSetDirty_7() { return &___m_DelayedSetDirty_7; }
	inline void set_m_DelayedSetDirty_7(bool value)
	{
		___m_DelayedSetDirty_7 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_8() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6, ___m_Tracker_8)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_8() const { return ___m_Tracker_8; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_8() { return &___m_Tracker_8; }
	inline void set_m_Tracker_8(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOFITTER_T3CA8A085831067C09B872C67F6E7F6F4EBB967B6_H
#ifndef BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#define BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5, ___m_Graphic_4)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifndef CANVASSCALER_T304BA6F47EDB7402EBA405DD36CA7D6ADF723564_H
#define CANVASSCALER_T304BA6F47EDB7402EBA405DD36CA7D6ADF723564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler
struct  CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.CanvasScaler_ScaleMode UnityEngine.UI.CanvasScaler::m_UiScaleMode
	int32_t ___m_UiScaleMode_4;
	// System.Single UnityEngine.UI.CanvasScaler::m_ReferencePixelsPerUnit
	float ___m_ReferencePixelsPerUnit_5;
	// System.Single UnityEngine.UI.CanvasScaler::m_ScaleFactor
	float ___m_ScaleFactor_6;
	// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::m_ReferenceResolution
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_ReferenceResolution_7;
	// UnityEngine.UI.CanvasScaler_ScreenMatchMode UnityEngine.UI.CanvasScaler::m_ScreenMatchMode
	int32_t ___m_ScreenMatchMode_8;
	// System.Single UnityEngine.UI.CanvasScaler::m_MatchWidthOrHeight
	float ___m_MatchWidthOrHeight_9;
	// UnityEngine.UI.CanvasScaler_Unit UnityEngine.UI.CanvasScaler::m_PhysicalUnit
	int32_t ___m_PhysicalUnit_11;
	// System.Single UnityEngine.UI.CanvasScaler::m_FallbackScreenDPI
	float ___m_FallbackScreenDPI_12;
	// System.Single UnityEngine.UI.CanvasScaler::m_DefaultSpriteDPI
	float ___m_DefaultSpriteDPI_13;
	// System.Single UnityEngine.UI.CanvasScaler::m_DynamicPixelsPerUnit
	float ___m_DynamicPixelsPerUnit_14;
	// UnityEngine.Canvas UnityEngine.UI.CanvasScaler::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_15;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevScaleFactor
	float ___m_PrevScaleFactor_16;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevReferencePixelsPerUnit
	float ___m_PrevReferencePixelsPerUnit_17;

public:
	inline static int32_t get_offset_of_m_UiScaleMode_4() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_UiScaleMode_4)); }
	inline int32_t get_m_UiScaleMode_4() const { return ___m_UiScaleMode_4; }
	inline int32_t* get_address_of_m_UiScaleMode_4() { return &___m_UiScaleMode_4; }
	inline void set_m_UiScaleMode_4(int32_t value)
	{
		___m_UiScaleMode_4 = value;
	}

	inline static int32_t get_offset_of_m_ReferencePixelsPerUnit_5() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_ReferencePixelsPerUnit_5)); }
	inline float get_m_ReferencePixelsPerUnit_5() const { return ___m_ReferencePixelsPerUnit_5; }
	inline float* get_address_of_m_ReferencePixelsPerUnit_5() { return &___m_ReferencePixelsPerUnit_5; }
	inline void set_m_ReferencePixelsPerUnit_5(float value)
	{
		___m_ReferencePixelsPerUnit_5 = value;
	}

	inline static int32_t get_offset_of_m_ScaleFactor_6() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_ScaleFactor_6)); }
	inline float get_m_ScaleFactor_6() const { return ___m_ScaleFactor_6; }
	inline float* get_address_of_m_ScaleFactor_6() { return &___m_ScaleFactor_6; }
	inline void set_m_ScaleFactor_6(float value)
	{
		___m_ScaleFactor_6 = value;
	}

	inline static int32_t get_offset_of_m_ReferenceResolution_7() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_ReferenceResolution_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_ReferenceResolution_7() const { return ___m_ReferenceResolution_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_ReferenceResolution_7() { return &___m_ReferenceResolution_7; }
	inline void set_m_ReferenceResolution_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_ReferenceResolution_7 = value;
	}

	inline static int32_t get_offset_of_m_ScreenMatchMode_8() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_ScreenMatchMode_8)); }
	inline int32_t get_m_ScreenMatchMode_8() const { return ___m_ScreenMatchMode_8; }
	inline int32_t* get_address_of_m_ScreenMatchMode_8() { return &___m_ScreenMatchMode_8; }
	inline void set_m_ScreenMatchMode_8(int32_t value)
	{
		___m_ScreenMatchMode_8 = value;
	}

	inline static int32_t get_offset_of_m_MatchWidthOrHeight_9() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_MatchWidthOrHeight_9)); }
	inline float get_m_MatchWidthOrHeight_9() const { return ___m_MatchWidthOrHeight_9; }
	inline float* get_address_of_m_MatchWidthOrHeight_9() { return &___m_MatchWidthOrHeight_9; }
	inline void set_m_MatchWidthOrHeight_9(float value)
	{
		___m_MatchWidthOrHeight_9 = value;
	}

	inline static int32_t get_offset_of_m_PhysicalUnit_11() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_PhysicalUnit_11)); }
	inline int32_t get_m_PhysicalUnit_11() const { return ___m_PhysicalUnit_11; }
	inline int32_t* get_address_of_m_PhysicalUnit_11() { return &___m_PhysicalUnit_11; }
	inline void set_m_PhysicalUnit_11(int32_t value)
	{
		___m_PhysicalUnit_11 = value;
	}

	inline static int32_t get_offset_of_m_FallbackScreenDPI_12() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_FallbackScreenDPI_12)); }
	inline float get_m_FallbackScreenDPI_12() const { return ___m_FallbackScreenDPI_12; }
	inline float* get_address_of_m_FallbackScreenDPI_12() { return &___m_FallbackScreenDPI_12; }
	inline void set_m_FallbackScreenDPI_12(float value)
	{
		___m_FallbackScreenDPI_12 = value;
	}

	inline static int32_t get_offset_of_m_DefaultSpriteDPI_13() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_DefaultSpriteDPI_13)); }
	inline float get_m_DefaultSpriteDPI_13() const { return ___m_DefaultSpriteDPI_13; }
	inline float* get_address_of_m_DefaultSpriteDPI_13() { return &___m_DefaultSpriteDPI_13; }
	inline void set_m_DefaultSpriteDPI_13(float value)
	{
		___m_DefaultSpriteDPI_13 = value;
	}

	inline static int32_t get_offset_of_m_DynamicPixelsPerUnit_14() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_DynamicPixelsPerUnit_14)); }
	inline float get_m_DynamicPixelsPerUnit_14() const { return ___m_DynamicPixelsPerUnit_14; }
	inline float* get_address_of_m_DynamicPixelsPerUnit_14() { return &___m_DynamicPixelsPerUnit_14; }
	inline void set_m_DynamicPixelsPerUnit_14(float value)
	{
		___m_DynamicPixelsPerUnit_14 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_15() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_Canvas_15)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_15() const { return ___m_Canvas_15; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_15() { return &___m_Canvas_15; }
	inline void set_m_Canvas_15(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_15), value);
	}

	inline static int32_t get_offset_of_m_PrevScaleFactor_16() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_PrevScaleFactor_16)); }
	inline float get_m_PrevScaleFactor_16() const { return ___m_PrevScaleFactor_16; }
	inline float* get_address_of_m_PrevScaleFactor_16() { return &___m_PrevScaleFactor_16; }
	inline void set_m_PrevScaleFactor_16(float value)
	{
		___m_PrevScaleFactor_16 = value;
	}

	inline static int32_t get_offset_of_m_PrevReferencePixelsPerUnit_17() { return static_cast<int32_t>(offsetof(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564, ___m_PrevReferencePixelsPerUnit_17)); }
	inline float get_m_PrevReferencePixelsPerUnit_17() const { return ___m_PrevReferencePixelsPerUnit_17; }
	inline float* get_address_of_m_PrevReferencePixelsPerUnit_17() { return &___m_PrevReferencePixelsPerUnit_17; }
	inline void set_m_PrevReferencePixelsPerUnit_17(float value)
	{
		___m_PrevReferencePixelsPerUnit_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSCALER_T304BA6F47EDB7402EBA405DD36CA7D6ADF723564_H
#ifndef CONTENTSIZEFITTER_T4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8_H
#define CONTENTSIZEFITTER_T4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter
struct  ContentSizeFitter_t4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.ContentSizeFitter_FitMode UnityEngine.UI.ContentSizeFitter::m_HorizontalFit
	int32_t ___m_HorizontalFit_4;
	// UnityEngine.UI.ContentSizeFitter_FitMode UnityEngine.UI.ContentSizeFitter::m_VerticalFit
	int32_t ___m_VerticalFit_5;
	// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::m_Rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ContentSizeFitter::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_7;

public:
	inline static int32_t get_offset_of_m_HorizontalFit_4() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8, ___m_HorizontalFit_4)); }
	inline int32_t get_m_HorizontalFit_4() const { return ___m_HorizontalFit_4; }
	inline int32_t* get_address_of_m_HorizontalFit_4() { return &___m_HorizontalFit_4; }
	inline void set_m_HorizontalFit_4(int32_t value)
	{
		___m_HorizontalFit_4 = value;
	}

	inline static int32_t get_offset_of_m_VerticalFit_5() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8, ___m_VerticalFit_5)); }
	inline int32_t get_m_VerticalFit_5() const { return ___m_VerticalFit_5; }
	inline int32_t* get_address_of_m_VerticalFit_5() { return &___m_VerticalFit_5; }
	inline void set_m_VerticalFit_5(int32_t value)
	{
		___m_VerticalFit_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8, ___m_Rect_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTSIZEFITTER_T4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8_H
#ifndef LAYOUTELEMENT_TD503826DB41B6EA85AC689292F8B2661B3C1048B_H
#define LAYOUTELEMENT_TD503826DB41B6EA85AC689292F8B2661B3C1048B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutElement
struct  LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Boolean UnityEngine.UI.LayoutElement::m_IgnoreLayout
	bool ___m_IgnoreLayout_4;
	// System.Single UnityEngine.UI.LayoutElement::m_MinWidth
	float ___m_MinWidth_5;
	// System.Single UnityEngine.UI.LayoutElement::m_MinHeight
	float ___m_MinHeight_6;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredWidth
	float ___m_PreferredWidth_7;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredHeight
	float ___m_PreferredHeight_8;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleWidth
	float ___m_FlexibleWidth_9;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleHeight
	float ___m_FlexibleHeight_10;
	// System.Int32 UnityEngine.UI.LayoutElement::m_LayoutPriority
	int32_t ___m_LayoutPriority_11;

public:
	inline static int32_t get_offset_of_m_IgnoreLayout_4() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_IgnoreLayout_4)); }
	inline bool get_m_IgnoreLayout_4() const { return ___m_IgnoreLayout_4; }
	inline bool* get_address_of_m_IgnoreLayout_4() { return &___m_IgnoreLayout_4; }
	inline void set_m_IgnoreLayout_4(bool value)
	{
		___m_IgnoreLayout_4 = value;
	}

	inline static int32_t get_offset_of_m_MinWidth_5() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_MinWidth_5)); }
	inline float get_m_MinWidth_5() const { return ___m_MinWidth_5; }
	inline float* get_address_of_m_MinWidth_5() { return &___m_MinWidth_5; }
	inline void set_m_MinWidth_5(float value)
	{
		___m_MinWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_MinHeight_6() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_MinHeight_6)); }
	inline float get_m_MinHeight_6() const { return ___m_MinHeight_6; }
	inline float* get_address_of_m_MinHeight_6() { return &___m_MinHeight_6; }
	inline void set_m_MinHeight_6(float value)
	{
		___m_MinHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_PreferredWidth_7() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_PreferredWidth_7)); }
	inline float get_m_PreferredWidth_7() const { return ___m_PreferredWidth_7; }
	inline float* get_address_of_m_PreferredWidth_7() { return &___m_PreferredWidth_7; }
	inline void set_m_PreferredWidth_7(float value)
	{
		___m_PreferredWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_PreferredHeight_8() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_PreferredHeight_8)); }
	inline float get_m_PreferredHeight_8() const { return ___m_PreferredHeight_8; }
	inline float* get_address_of_m_PreferredHeight_8() { return &___m_PreferredHeight_8; }
	inline void set_m_PreferredHeight_8(float value)
	{
		___m_PreferredHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleWidth_9() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_FlexibleWidth_9)); }
	inline float get_m_FlexibleWidth_9() const { return ___m_FlexibleWidth_9; }
	inline float* get_address_of_m_FlexibleWidth_9() { return &___m_FlexibleWidth_9; }
	inline void set_m_FlexibleWidth_9(float value)
	{
		___m_FlexibleWidth_9 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleHeight_10() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_FlexibleHeight_10)); }
	inline float get_m_FlexibleHeight_10() const { return ___m_FlexibleHeight_10; }
	inline float* get_address_of_m_FlexibleHeight_10() { return &___m_FlexibleHeight_10; }
	inline void set_m_FlexibleHeight_10(float value)
	{
		___m_FlexibleHeight_10 = value;
	}

	inline static int32_t get_offset_of_m_LayoutPriority_11() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_LayoutPriority_11)); }
	inline int32_t get_m_LayoutPriority_11() const { return ___m_LayoutPriority_11; }
	inline int32_t* get_address_of_m_LayoutPriority_11() { return &___m_LayoutPriority_11; }
	inline void set_m_LayoutPriority_11(int32_t value)
	{
		___m_LayoutPriority_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTELEMENT_TD503826DB41B6EA85AC689292F8B2661B3C1048B_H
#ifndef LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#define LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Padding_4;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_5;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalMinSize_8;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalPreferredSize_9;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalFlexibleSize_10;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * ___m_RectChildren_11;

public:
	inline static int32_t get_offset_of_m_Padding_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Padding_4)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Padding_4() const { return ___m_Padding_4; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Padding_4() { return &___m_Padding_4; }
	inline void set_m_Padding_4(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Padding_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_4), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_ChildAlignment_5)); }
	inline int32_t get_m_ChildAlignment_5() const { return ___m_ChildAlignment_5; }
	inline int32_t* get_address_of_m_ChildAlignment_5() { return &___m_ChildAlignment_5; }
	inline void set_m_ChildAlignment_5(int32_t value)
	{
		___m_ChildAlignment_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Rect_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalMinSize_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalMinSize_8() const { return ___m_TotalMinSize_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalMinSize_8() { return &___m_TotalMinSize_8; }
	inline void set_m_TotalMinSize_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalMinSize_8 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalPreferredSize_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalPreferredSize_9() const { return ___m_TotalPreferredSize_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalPreferredSize_9() { return &___m_TotalPreferredSize_9; }
	inline void set_m_TotalPreferredSize_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalPreferredSize_9 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_10() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalFlexibleSize_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalFlexibleSize_10() const { return ___m_TotalFlexibleSize_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalFlexibleSize_10() { return &___m_TotalFlexibleSize_10; }
	inline void set_m_TotalFlexibleSize_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalFlexibleSize_10 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_11() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_RectChildren_11)); }
	inline List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * get_m_RectChildren_11() const { return ___m_RectChildren_11; }
	inline List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C ** get_address_of_m_RectChildren_11() { return &___m_RectChildren_11; }
	inline void set_m_RectChildren_11(List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * value)
	{
		___m_RectChildren_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#ifndef GRIDLAYOUTGROUP_T1C70294BD2567FD584672222A8BFD5A0DF1CA2A8_H
#define GRIDLAYOUTGROUP_T1C70294BD2567FD584672222A8BFD5A0DF1CA2A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup
struct  GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8  : public LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4
{
public:
	// UnityEngine.UI.GridLayoutGroup_Corner UnityEngine.UI.GridLayoutGroup::m_StartCorner
	int32_t ___m_StartCorner_12;
	// UnityEngine.UI.GridLayoutGroup_Axis UnityEngine.UI.GridLayoutGroup::m_StartAxis
	int32_t ___m_StartAxis_13;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_CellSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_CellSize_14;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_Spacing
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Spacing_15;
	// UnityEngine.UI.GridLayoutGroup_Constraint UnityEngine.UI.GridLayoutGroup::m_Constraint
	int32_t ___m_Constraint_16;
	// System.Int32 UnityEngine.UI.GridLayoutGroup::m_ConstraintCount
	int32_t ___m_ConstraintCount_17;

public:
	inline static int32_t get_offset_of_m_StartCorner_12() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8, ___m_StartCorner_12)); }
	inline int32_t get_m_StartCorner_12() const { return ___m_StartCorner_12; }
	inline int32_t* get_address_of_m_StartCorner_12() { return &___m_StartCorner_12; }
	inline void set_m_StartCorner_12(int32_t value)
	{
		___m_StartCorner_12 = value;
	}

	inline static int32_t get_offset_of_m_StartAxis_13() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8, ___m_StartAxis_13)); }
	inline int32_t get_m_StartAxis_13() const { return ___m_StartAxis_13; }
	inline int32_t* get_address_of_m_StartAxis_13() { return &___m_StartAxis_13; }
	inline void set_m_StartAxis_13(int32_t value)
	{
		___m_StartAxis_13 = value;
	}

	inline static int32_t get_offset_of_m_CellSize_14() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8, ___m_CellSize_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_CellSize_14() const { return ___m_CellSize_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_CellSize_14() { return &___m_CellSize_14; }
	inline void set_m_CellSize_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_CellSize_14 = value;
	}

	inline static int32_t get_offset_of_m_Spacing_15() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8, ___m_Spacing_15)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Spacing_15() const { return ___m_Spacing_15; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Spacing_15() { return &___m_Spacing_15; }
	inline void set_m_Spacing_15(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Spacing_15 = value;
	}

	inline static int32_t get_offset_of_m_Constraint_16() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8, ___m_Constraint_16)); }
	inline int32_t get_m_Constraint_16() const { return ___m_Constraint_16; }
	inline int32_t* get_address_of_m_Constraint_16() { return &___m_Constraint_16; }
	inline void set_m_Constraint_16(int32_t value)
	{
		___m_Constraint_16 = value;
	}

	inline static int32_t get_offset_of_m_ConstraintCount_17() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8, ___m_ConstraintCount_17)); }
	inline int32_t get_m_ConstraintCount_17() const { return ___m_ConstraintCount_17; }
	inline int32_t* get_address_of_m_ConstraintCount_17() { return &___m_ConstraintCount_17; }
	inline void set_m_ConstraintCount_17(int32_t value)
	{
		___m_ConstraintCount_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDLAYOUTGROUP_T1C70294BD2567FD584672222A8BFD5A0DF1CA2A8_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#define HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB  : public LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_14;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_15;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_16;

public:
	inline static int32_t get_offset_of_m_Spacing_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_Spacing_12)); }
	inline float get_m_Spacing_12() const { return ___m_Spacing_12; }
	inline float* get_address_of_m_Spacing_12() { return &___m_Spacing_12; }
	inline void set_m_Spacing_12(float value)
	{
		___m_Spacing_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildForceExpandWidth_13)); }
	inline bool get_m_ChildForceExpandWidth_13() const { return ___m_ChildForceExpandWidth_13; }
	inline bool* get_address_of_m_ChildForceExpandWidth_13() { return &___m_ChildForceExpandWidth_13; }
	inline void set_m_ChildForceExpandWidth_13(bool value)
	{
		___m_ChildForceExpandWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildForceExpandHeight_14)); }
	inline bool get_m_ChildForceExpandHeight_14() const { return ___m_ChildForceExpandHeight_14; }
	inline bool* get_address_of_m_ChildForceExpandHeight_14() { return &___m_ChildForceExpandHeight_14; }
	inline void set_m_ChildForceExpandHeight_14(bool value)
	{
		___m_ChildForceExpandHeight_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_15() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildControlWidth_15)); }
	inline bool get_m_ChildControlWidth_15() const { return ___m_ChildControlWidth_15; }
	inline bool* get_address_of_m_ChildControlWidth_15() { return &___m_ChildControlWidth_15; }
	inline void set_m_ChildControlWidth_15(bool value)
	{
		___m_ChildControlWidth_15 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_16() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildControlHeight_16)); }
	inline bool get_m_ChildControlHeight_16() const { return ___m_ChildControlHeight_16; }
	inline bool* get_address_of_m_ChildControlHeight_16() { return &___m_ChildControlHeight_16; }
	inline void set_m_ChildControlHeight_16(bool value)
	{
		___m_ChildControlHeight_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#ifndef POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#define POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifndef SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#define SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectDistance_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifndef HORIZONTALLAYOUTGROUP_TEFAFA0DDCCE4FC89CC2C0BE96E7C025D243CFE37_H
#define HORIZONTALLAYOUTGROUP_TEFAFA0DDCCE4FC89CC2C0BE96E7C025D243CFE37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalLayoutGroup
struct  HorizontalLayoutGroup_tEFAFA0DDCCE4FC89CC2C0BE96E7C025D243CFE37  : public HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALLAYOUTGROUP_TEFAFA0DDCCE4FC89CC2C0BE96E7C025D243CFE37_H
#ifndef OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#define OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_tB750E496976B072E79142D51C0A991AC20183095  : public Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifndef VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H
#define VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11  : public HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2902[2] = 
{
	RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t6C47856C4F775A5799A49A100196C2BB14C5DD91::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2903[5] = 
{
	AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6::get_offset_of_m_AspectMode_4(),
	AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6::get_offset_of_m_AspectRatio_5(),
	AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6::get_offset_of_m_Rect_6(),
	AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6::get_offset_of_m_DelayedSetDirty_7(),
	AspectRatioFitter_t3CA8A085831067C09B872C67F6E7F6F4EBB967B6::get_offset_of_m_Tracker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (AspectMode_t2D8C205891B8E63CA16B6AC3BA1D41320903C65A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2904[6] = 
{
	AspectMode_t2D8C205891B8E63CA16B6AC3BA1D41320903C65A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2905[14] = 
{
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_UiScaleMode_4(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_ReferencePixelsPerUnit_5(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_ScaleFactor_6(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_ReferenceResolution_7(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_ScreenMatchMode_8(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_MatchWidthOrHeight_9(),
	0,
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_PhysicalUnit_11(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_FallbackScreenDPI_12(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_DefaultSpriteDPI_13(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_DynamicPixelsPerUnit_14(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_Canvas_15(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_PrevScaleFactor_16(),
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564::get_offset_of_m_PrevReferencePixelsPerUnit_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (ScaleMode_t38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2906[4] = 
{
	ScaleMode_t38950B182EA5E1C8589AB5E02F36FEABB8A5CAA6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (ScreenMatchMode_t61C3A62F8F54F705D47C2C37B06DC8083238C133)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2907[4] = 
{
	ScreenMatchMode_t61C3A62F8F54F705D47C2C37B06DC8083238C133::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (Unit_tD24A4DB24016D1A6B46579640E170359F76F8313)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2908[6] = 
{
	Unit_tD24A4DB24016D1A6B46579640E170359F76F8313::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (ContentSizeFitter_t4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[4] = 
{
	ContentSizeFitter_t4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8::get_offset_of_m_HorizontalFit_4(),
	ContentSizeFitter_t4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8::get_offset_of_m_VerticalFit_5(),
	ContentSizeFitter_t4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8::get_offset_of_m_Rect_6(),
	ContentSizeFitter_t4EA7B51457F7EFAD3BAAC51613C7D4E0C26BF4A8::get_offset_of_m_Tracker_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (FitMode_tBF783E77415F7063B468C18E758F738D83D60A08)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2910[4] = 
{
	FitMode_tBF783E77415F7063B468C18E758F738D83D60A08::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[6] = 
{
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8::get_offset_of_m_StartCorner_12(),
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8::get_offset_of_m_StartAxis_13(),
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8::get_offset_of_m_CellSize_14(),
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8::get_offset_of_m_Spacing_15(),
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8::get_offset_of_m_Constraint_16(),
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8::get_offset_of_m_ConstraintCount_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (Corner_tD61F36EC56D401A65DA06BE1A21689319201D18E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2912[5] = 
{
	Corner_tD61F36EC56D401A65DA06BE1A21689319201D18E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (Axis_tD4645F3B274E7AE7793C038A2BA2E68C6F0F02F0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2913[3] = 
{
	Axis_tD4645F3B274E7AE7793C038A2BA2E68C6F0F02F0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (Constraint_tF471E55525B89D1E7C938CC0AF7515709494C59D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2914[4] = 
{
	Constraint_tF471E55525B89D1E7C938CC0AF7515709494C59D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (HorizontalLayoutGroup_tEFAFA0DDCCE4FC89CC2C0BE96E7C025D243CFE37), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[5] = 
{
	HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB::get_offset_of_m_Spacing_12(),
	HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB::get_offset_of_m_ChildForceExpandWidth_13(),
	HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB::get_offset_of_m_ChildForceExpandHeight_14(),
	HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB::get_offset_of_m_ChildControlWidth_15(),
	HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB::get_offset_of_m_ChildControlHeight_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[8] = 
{
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_IgnoreLayout_4(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_MinWidth_5(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_MinHeight_6(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_PreferredWidth_7(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_PreferredHeight_8(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_FlexibleWidth_9(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_FlexibleHeight_10(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_LayoutPriority_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[8] = 
{
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Padding_4(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_ChildAlignment_5(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Rect_6(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Tracker_7(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalMinSize_8(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalPreferredSize_9(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalFlexibleSize_10(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_RectChildren_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD), -1, sizeof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2925[9] = 
{
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5), -1, sizeof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2926[8] = 
{
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2930[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2931[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A), -1, sizeof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2932[7] = 
{
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F), -1, sizeof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2939[12] = 
{
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Positions_0(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Colors_1(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv0S_2(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv1S_3(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv2S_4(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv3S_5(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Normals_6(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Tangents_7(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Indices_8(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultNormal_10(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_ListsInitalized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[1] = 
{
	BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (Outline_tB750E496976B072E79142D51C0A991AC20183095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[4] = 
{
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectColor_5(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectDistance_6(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537), -1, sizeof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2945[1] = 
{
	U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (U3CModuleU3E_tEB1E4002BF64622D06B314BD39D49373F350A28A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (JSONObject_t483DCB025D825510C7827AE094325C194CE9B946), -1, sizeof(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2948[10] = 
{
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946_StaticFields::get_offset_of_WHITESPACE_0(),
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946::get_offset_of_type_1(),
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946::get_offset_of_list_2(),
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946::get_offset_of_keys_3(),
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946::get_offset_of_str_4(),
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946::get_offset_of_n_5(),
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946::get_offset_of_useInt_6(),
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946::get_offset_of_i_7(),
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946::get_offset_of_b_8(),
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946_StaticFields::get_offset_of_j_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (Type_t0F33033AB9EC1EE6644E22D132AEA0764439B072)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2949[8] = 
{
	Type_t0F33033AB9EC1EE6644E22D132AEA0764439B072::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (Outline_t1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2951[1] = 
{
	Outline_t1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3::get_offset_of_a_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92), -1, sizeof(U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2952[5] = 
{
	U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
	U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields::get_offset_of_U3CU3E9__5_1_2(),
	U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields::get_offset_of_U3CU3E9__6_0_3(),
	U3CU3Ec_t2432C58F806854EB5C36683E4414157BC67E3D92_StaticFields::get_offset_of_U3CU3E9__6_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0), -1, sizeof(OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2953[25] = 
{
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0_StaticFields::get_offset_of_a_4(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_b_5(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_lineThickness_6(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_lineIntensity_7(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_fillAmount_8(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_lineColor_9(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_additiveRendering_10(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_backfaceCulling_11(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_cornerOutlines_12(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_addLinesBetweenColors_13(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_scaleWithScreenSize_14(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_alphaCutoff_15(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_flipY_16(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_sourceCamera_17(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_outlineCamera_18(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_c_19(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_d_20(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_e_21(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_f_22(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_g_23(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_outlineShaderMaterial_24(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_renderTexture_25(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_extraRenderTexture_26(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_h_27(),
	OutlineEffect_tFEB9908FEA7A02A88D086B424872A2E21775B3C0::get_offset_of_i_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[6] = 
{
	TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B::get_offset_of_rotation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B::get_offset_of_localPosition_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B::get_offset_of_localScale_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B::get_offset_of_localRotation_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformData_t5720CDB59D545F630361A92B5D01C6C4DAAA5C3B::get_offset_of_parent_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (TransformUtils_t2CCC4B40267B7F0D5D544ABC6134517EBAB751A1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (NotAllowedPlayerProjectionModeException_t8A2572169F7DCF6B25C680F2C88857D66BC7AF32), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (GameObjectExtensions_t68EE1EA4DF6A44FF56DD25E2CB497AE73634E93C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (a_tB29FDBA317931B7D50F19992FEB1BC2EC7C616FC), -1, sizeof(a_tB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2958[1] = 
{
	a_tB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_StaticFields::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E)+ sizeof (RuntimeObject), sizeof(a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[2] = 
{
	CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527::get_offset_of_a_0(),
	CommandAutocomplete_t36FD2EE44A421FC6A30A85F6E6ED8BB15DD45527::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2961[2] = 
{
	CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994::get_offset_of_a_0(),
	CommandHistory_t9914EDC51E0F873B6462D9C174345F15B0490994::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (TerminalLogType_tB6DBBAEBB8CAF943A4E35E241CFBA9011B514A25)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2962[8] = 
{
	TerminalLogType_tB6DBBAEBB8CAF943A4E35E241CFBA9011B514A25::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1)+ sizeof (RuntimeObject), sizeof(LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2963[3] = 
{
	LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1::get_offset_of_message_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogItem_tC5B07EC021DE250618B9F1E373E71741BC8D4CB1::get_offset_of_stackTrace_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[2] = 
{
	CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03::get_offset_of_a_0(),
	CommandLog_t05960F7917F3A41127E5E7A6E0B3EC4307200C03::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328)+ sizeof (RuntimeObject), sizeof(CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2965[5] = 
{
	CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328::get_offset_of_proc_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328::get_offset_of_maxArgCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328::get_offset_of_minArgCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328::get_offset_of_help_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CommandInfo_t40AFE2ECD393889504D1D82A0925CE324B910328::get_offset_of_hint_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (CommandArg_t89F4623B4FEB9EED561AB60014C25228A1027A27)+ sizeof (RuntimeObject), sizeof(CommandArg_t89F4623B4FEB9EED561AB60014C25228A1027A27_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2966[1] = 
{
	CommandArg_t89F4623B4FEB9EED561AB60014C25228A1027A27::get_offset_of_a_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[4] = 
{
	CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811::get_offset_of_a_0(),
	CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811::get_offset_of_b_1(),
	CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811::get_offset_of_c_2(),
	CommandShell_t509DD5DF61641E83DB3BF4DBCBE22D40122ED811::get_offset_of_d_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[5] = 
{
	RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5::get_offset_of_a_0(),
	RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5::get_offset_of_b_1(),
	RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5::get_offset_of_c_2(),
	RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5::get_offset_of_d_3(),
	RegisterCommandAttribute_t17944266D35BB70793C86DF6E95601F14DBAEDA5::get_offset_of_e_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (TerminalState_t24F35A9E2965E4E9D9414D639A37FEC5C927EC53)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2969[3] = 
{
	TerminalState_t24F35A9E2965E4E9D9414D639A37FEC5C927EC53::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841), -1, sizeof(Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2970[31] = 
{
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_a_4(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_b_5(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_c_6(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_d_7(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_e_8(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_f_9(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_g_10(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_h_11(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_i_12(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_j_13(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_k_14(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_l_15(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_m_16(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_n_17(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_o_18(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_p_19(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_q_20(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_r_21(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_s_22(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_t_23(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_u_24(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_v_25(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_w_26(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_x_27(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_y_28(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_z_29(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841::get_offset_of_aa_30(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841_StaticFields::get_offset_of_ab_31(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841_StaticFields::get_offset_of_ac_32(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841_StaticFields::get_offset_of_ad_33(),
	Terminal_t0EC32061DBB6EE11865FC3F6B30EA2E784368841_StaticFields::get_offset_of_ae_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[4] = 
{
	ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7::get_offset_of_a_0(),
	ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7::get_offset_of_b_1(),
	ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7::get_offset_of_c_2(),
	ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7::get_offset_of_d_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (Command_tB160475765D7EACBCEC6D380382F8C9ACDEFF8AA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (ChangeColorMultiCustomBoxCmd_tC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[1] = 
{
	ChangeColorMultiCustomBoxCmd_tC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2974[3] = 
{
	ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9::get_offset_of_a_0(),
	ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9::get_offset_of_b_1(),
	ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9::get_offset_of_c_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2975[5] = 
{
	LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD::get_offset_of_a_0(),
	LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD::get_offset_of_b_1(),
	LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD::get_offset_of_c_2(),
	LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD::get_offset_of_d_3(),
	LoadProductCmd_t1E80AF3306BB0373169E88D9BDD43715D4461EDD::get_offset_of_e_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (RemoveMultiProductCmd_tE18C91D6311E0A86F382026E45BA6566D25E0582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[1] = 
{
	RemoveMultiProductCmd_tE18C91D6311E0A86F382026E45BA6566D25E0582::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (RemoveProductCmd_t7682BFA01F52AB1FB6D3035DF130ED79DC63F9AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2977[2] = 
{
	RemoveProductCmd_t7682BFA01F52AB1FB6D3035DF130ED79DC63F9AF::get_offset_of_a_0(),
	RemoveProductCmd_t7682BFA01F52AB1FB6D3035DF130ED79DC63F9AF::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[1] = 
{
	TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[8] = 
{
	TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7::get_offset_of_a_0(),
	TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7::get_offset_of_b_1(),
	TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7::get_offset_of_c_2(),
	TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7::get_offset_of_d_3(),
	TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7::get_offset_of_e_4(),
	TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7::get_offset_of_f_5(),
	TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7::get_offset_of_g_6(),
	TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7::get_offset_of_h_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2980[7] = 
{
	TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C::get_offset_of_a_0(),
	TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C::get_offset_of_b_1(),
	TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C::get_offset_of_c_2(),
	TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C::get_offset_of_d_3(),
	TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C::get_offset_of_e_4(),
	TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C::get_offset_of_f_5(),
	TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C::get_offset_of_g_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (JSONReaderException_tD2ADF3D96D3884825DB6B37F7ADFBD46E77A817D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (NotAllowedPlayerControlModeException_t464712422DA5748F0976008E2D09BB03F06F7FCE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (NotAllowedSDKModeException_t7524021C6069CD30D91A5280310D2AF58B855ACB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (CustomExtensions_tDB5543760418AA0C993F99F697CD258EAC9A1C02), -1, sizeof(CustomExtensions_tDB5543760418AA0C993F99F697CD258EAC9A1C02_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2985[1] = 
{
	CustomExtensions_tDB5543760418AA0C993F99F697CD258EAC9A1C02_StaticFields::get_offset_of_materialOpaqueDictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[3] = 
{
	DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2::get_offset_of_a_4(),
	DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2::get_offset_of_b_5(),
	DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2::get_offset_of_c_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (MathExtensions_t940FB68F17E977C6019FD5ECD6732F381E1EA7CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[19] = 
{
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_nightDayColor_4(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_maxIntensity_5(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_minIntensity_6(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_minPoint_7(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_maxAmbient_8(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_minAmbient_9(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_minAmbientPoint_10(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_nightDayFogColor_11(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_fogDensityCurve_12(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_fogScale_13(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_dayAtmosphereThickness_14(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_nightAtmosphereThickness_15(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_dayRotateSpeed_16(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_nightRotateSpeed_17(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_a_18(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_ambientLightFactor_19(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_b_20(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_c_21(),
	AutoIntensity_tB8A446E08F3472C1D33DD750E69C7C341863F029::get_offset_of_d_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966), -1, sizeof(EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2989[2] = 
{
	EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966::get_offset_of_a_4(),
	EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966_StaticFields::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2990[15] = 
{
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_speed_4(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_sensitivity_5(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_a_6(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_b_7(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_c_8(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_d_9(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_e_10(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_f_11(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_g_12(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_h_13(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_i_14(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_j_15(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_k_16(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_l_17(),
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE::get_offset_of_m_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2991[12] = 
{
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_a_4(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_b_5(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_c_6(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_d_7(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_e_8(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_f_9(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_g_10(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_h_11(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_i_12(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_j_13(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_k_14(),
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7::get_offset_of_l_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2992[9] = 
{
	MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA::get_offset_of_a_4(),
	MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA::get_offset_of_b_5(),
	MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA::get_offset_of_c_6(),
	MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA::get_offset_of_d_7(),
	MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA::get_offset_of_e_8(),
	MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA::get_offset_of_f_9(),
	MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA::get_offset_of_g_10(),
	MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA::get_offset_of_h_11(),
	MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA::get_offset_of_i_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (MiniMapControls_t24945DFD5710E6BA3278D23CAF9377F85B3EF0FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2993[4] = 
{
	MiniMapControls_t24945DFD5710E6BA3278D23CAF9377F85B3EF0FF::get_offset_of_a_4(),
	MiniMapControls_t24945DFD5710E6BA3278D23CAF9377F85B3EF0FF::get_offset_of_b_5(),
	MiniMapControls_t24945DFD5710E6BA3278D23CAF9377F85B3EF0FF::get_offset_of_c_6(),
	MiniMapControls_t24945DFD5710E6BA3278D23CAF9377F85B3EF0FF::get_offset_of_d_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2994[30] = 
{
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_a_4(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_b_5(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_c_6(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_d_7(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_e_8(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_f_9(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_g_10(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_h_11(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_i_12(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_j_13(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_k_14(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_l_15(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of__rotDegreeEuler_16(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_m_17(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_n_18(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_o_19(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_p_20(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_q_21(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_r_22(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_s_23(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_t_24(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_u_25(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_v_26(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_w_27(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_x_28(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_y_29(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_z_30(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_aa_31(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_ab_32(),
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403::get_offset_of_ac_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (PresetType_t7E6F36433E4BAA3E292B376663A830C4FF667F3C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2995[9] = 
{
	PresetType_t7E6F36433E4BAA3E292B376663A830C4FF667F3C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2996[8] = 
{
	SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84::get_offset_of_bloomIntensity_4(),
	SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84::get_offset_of_shader_5(),
	SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84::get_offset_of_a_6(),
	SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84::get_offset_of_lensDirtTexture_7(),
	SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84::get_offset_of_lensDirtIntensity_8(),
	SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84::get_offset_of_b_9(),
	SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84::get_offset_of_c_10(),
	SENaturalBloomAndDirtyLens_t55A288C47AC896F7CC9231E333EF2CDA4A2BDF84::get_offset_of_inputIsHDR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[7] = 
{
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D::get_offset_of_a_4(),
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D::get_offset_of_b_5(),
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D::get_offset_of_bCollision_6(),
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D::get_offset_of_c_7(),
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D::get_offset_of_d_8(),
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D::get_offset_of_e_9(),
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D::get_offset_of_cubemap_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (SDKMode_t68489B85D9A5BF965F530F7415C6F9239F949348)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2998[6] = 
{
	SDKMode_t68489B85D9A5BF965F530F7415C6F9239F949348::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (ViewMode_t65A9F115674EDCD52D7677D41A5D56F687458B3E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2999[4] = 
{
	ViewMode_t65A9F115674EDCD52D7677D41A5D56F687458B3E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
