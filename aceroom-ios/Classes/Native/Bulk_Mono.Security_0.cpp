﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct VirtFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericVirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct GenericVirtFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct InterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct InterfaceFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct InterfaceFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericInterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct GenericInterfaceFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};

// Mono.Security.Cryptography.MD4
struct MD4_t932C1DEA44D4B8650873251E88AA4096164BB380;
// Mono.Security.Cryptography.MD4Managed
struct MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1;
// Mono.Security.Interface.CipherSuiteCode[]
struct CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4;
// Mono.Security.Interface.ICertificateValidator
struct ICertificateValidator_t0C1A54E00D408ADCBA27E600BFAA216E7E7D31A3;
// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A;
// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6;
// Mono.Security.Interface.MonoTlsSettings
struct MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF;
// Mono.Security.Protocol.Ntlm.ChallengeResponse
struct ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B;
// Mono.Security.Protocol.Ntlm.MessageBase
struct MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0;
// Mono.Security.Protocol.Ntlm.Type1Message
struct Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C;
// Mono.Security.Protocol.Ntlm.Type2Message
struct Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398;
// Mono.Security.Protocol.Ntlm.Type3Message
struct Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C;
// System.ArgumentException
struct ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1;
// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Globalization.Calendar
struct Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB;
// System.Globalization.CompareInfo
struct CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1;
// System.Globalization.CultureData
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8;
// System.Globalization.TextInfo
struct TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.InvalidOperationException
struct InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1;
// System.ObjectDisposedException
struct ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.DES
struct DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0;
// System.Security.Cryptography.HMACMD5
struct HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E;
// System.Security.Cryptography.MD5
struct MD5_tCED753745572EC20FE5D31D15F132736B5343EE6;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833;
// System.Security.Cryptography.X509Certificates.X509CertificateImpl
struct X509CertificateImpl_t89610BFDE87B872143A4623CFC7F17275EB48313;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538;
// System.Security.Cryptography.X509Certificates.X509ChainImpl
struct X509ChainImpl_t34FABF07BEA0CFB6D88717BCDDE0607D9DA13A67;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.DecoderFallback
struct DecoderFallback_t128445EB7676870485230893338EF044F6B72F60;
// System.Text.EncoderFallback
struct EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

extern RuntimeClass* ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var;
extern RuntimeClass* BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_il2cpp_TypeInfo_var;
extern RuntimeClass* ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var;
extern RuntimeClass* ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var;
extern RuntimeClass* ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var;
extern RuntimeClass* CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var;
extern RuntimeClass* DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0_il2cpp_TypeInfo_var;
extern RuntimeClass* DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_il2cpp_TypeInfo_var;
extern RuntimeClass* GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var;
extern RuntimeClass* HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B_il2cpp_TypeInfo_var;
extern RuntimeClass* ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
extern RuntimeClass* InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var;
extern RuntimeClass* MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1_il2cpp_TypeInfo_var;
extern RuntimeClass* Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var;
extern RuntimeClass* MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var;
extern RuntimeClass* MonoSslPolicyErrors_t5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7_il2cpp_TypeInfo_var;
extern RuntimeClass* MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_il2cpp_TypeInfo_var;
extern RuntimeClass* NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var;
extern RuntimeClass* UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB_il2cpp_TypeInfo_var;
extern RuntimeClass* X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA____16968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0_FieldInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA____6FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1_FieldInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA____AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2_FieldInfo_var;
extern String_t* _stringLiteral42BBEE886171CED8B81918E0F830F24966193E05;
extern String_t* _stringLiteral43C5083891C69B860FC78499995E820029745FE8;
extern String_t* _stringLiteral636C307C2499B64E58C024BD8EC39A968AF2D4A9;
extern String_t* _stringLiteral6F9B9AF3CD6E8B8A73C2CDCED37FE9F59226E27D;
extern String_t* _stringLiteralB8B0EDE7ABBF3F7F6738DC0C3F6D05656BAD431B;
extern String_t* _stringLiteralBB9D653F4E12DEED891E16FF7E7D9376E13F075D;
extern String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
extern const RuntimeMethod* ChallengeResponse2_Compute_mAA312CA925226C75A829516B6BDC2089840D389D_RuntimeMethod_var;
extern const RuntimeMethod* ChallengeResponse_get_LM_m3916048E028CFCA867E801A83FEB949F7C089263_RuntimeMethod_var;
extern const RuntimeMethod* ChallengeResponse_get_NT_mEC9F2FDFDB8FADF415D4BA8A1564A23024FC3437_RuntimeMethod_var;
extern const RuntimeMethod* ChallengeResponse_set_Challenge_mD747C1A002528A6E9AFDE848AA257FD7B1B85E6F_RuntimeMethod_var;
extern const RuntimeMethod* ChallengeResponse_set_Password_m530EB94179C374BED9B9AAE4BB30AB3FF14F07E2_RuntimeMethod_var;
extern const RuntimeMethod* MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537_RuntimeMethod_var;
extern const RuntimeMethod* Type3Message_GetBytes_m9C22065DB20CD016FCA226AA2A78952E111CF118_RuntimeMethod_var;
extern const uint32_t BitConverterLE_GetULongBytes_mCBAC987169706D10F05AFF42559A4EA1D88E1876_MetadataUsageId;
extern const uint32_t BitConverterLE_UIntFromBytes_m91E16C3362E794444D849A4AD5B9F746BF2A4FCB_MetadataUsageId;
extern const uint32_t BitConverterLE_UShortFromBytes_mBC051D16FFC95E9695F110AFDEAB018BD76F84A9_MetadataUsageId;
extern const uint32_t ChallengeResponse2_Compute_LM_m3A1F9371E4F1E41B044787FFB3BE985E92A3AC22_MetadataUsageId;
extern const uint32_t ChallengeResponse2_Compute_NTLM_Password_mD27D4A18DBD712B0E80B0F2A1CB296B353312C41_MetadataUsageId;
extern const uint32_t ChallengeResponse2_Compute_NTLM_mA1DCA878A3A7A5517DB8BB0F2BEDD29353573976_MetadataUsageId;
extern const uint32_t ChallengeResponse2_Compute_NTLMv2_Session_mFB6537BF7FC8D9D7CCE55BFCDF6A7C371460E296_MetadataUsageId;
extern const uint32_t ChallengeResponse2_Compute_NTLMv2_mBCFF2DF7375AD035B98FC84253201D269201CE43_MetadataUsageId;
extern const uint32_t ChallengeResponse2_Compute_mAA312CA925226C75A829516B6BDC2089840D389D_MetadataUsageId;
extern const uint32_t ChallengeResponse2_GetResponse_mE39699CD2453921E373BF9768180993EDCD810E7_MetadataUsageId;
extern const uint32_t ChallengeResponse2_PasswordToKey_m2871E605818DF2DE4BC5B1B163831BA8F64006D8_MetadataUsageId;
extern const uint32_t ChallengeResponse2_PrepareDESKey_m428EF8F37B18E0B4FC5895BFF02A681740CF7608_MetadataUsageId;
extern const uint32_t ChallengeResponse2__cctor_mCF5C3FE5989C7BB7777C3BAADD67E9F8576A8C23_MetadataUsageId;
extern const uint32_t ChallengeResponse_Dispose_mD6C08D1EDA541DC5A9B287744FB18E3149627434_MetadataUsageId;
extern const uint32_t ChallengeResponse_GetResponse_m526E49021AB29DD12995CF8BB12DC9F03F2A583F_MetadataUsageId;
extern const uint32_t ChallengeResponse_PasswordToKey_m522B84CA0312284486A2C4E10FEE2D74BF4FF163_MetadataUsageId;
extern const uint32_t ChallengeResponse_PrepareDESKey_m32B2174E0B63E959CE08204F3C39AAA01799A3B3_MetadataUsageId;
extern const uint32_t ChallengeResponse__cctor_m069BF87DE471BEDD893664B52BBD066119DCD55C_MetadataUsageId;
extern const uint32_t ChallengeResponse__ctor_m1E0300839CAF582A720DB0F4F9E425B6EE12B258_MetadataUsageId;
extern const uint32_t ChallengeResponse_get_LM_m3916048E028CFCA867E801A83FEB949F7C089263_MetadataUsageId;
extern const uint32_t ChallengeResponse_get_NT_mEC9F2FDFDB8FADF415D4BA8A1564A23024FC3437_MetadataUsageId;
extern const uint32_t ChallengeResponse_set_Challenge_mD747C1A002528A6E9AFDE848AA257FD7B1B85E6F_MetadataUsageId;
extern const uint32_t ChallengeResponse_set_Password_m530EB94179C374BED9B9AAE4BB30AB3FF14F07E2_MetadataUsageId;
extern const uint32_t MD4Managed_HashFinal_m4853C50C4026A1F75D7C75FB28FD4E146E95F6AC_MetadataUsageId;
extern const uint32_t MD4Managed_Padding_m7AD58C8D8178AC6CD6738C93A146312D73ED0476_MetadataUsageId;
extern const uint32_t MD4Managed__ctor_mDB4352DBB43112E1DF337DF241A3E25435C2C3C5_MetadataUsageId;
extern const uint32_t MD4_Create_m2D436A4CC284704A7DA0EEF4C4D5860F69D0BB93_MetadataUsageId;
extern const uint32_t MessageBase_CheckHeader_m427014E264FA451B68062CFF8A1939DC3A04FB5A_MetadataUsageId;
extern const uint32_t MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537_MetadataUsageId;
extern const uint32_t MessageBase_PrepareMessage_m6B0C0C463C16D086924EC49DB07C3ADE95C11958_MetadataUsageId;
extern const uint32_t MessageBase__cctor_m4DF505F352AB2529D7E6EE09E6B096C076663B0F_MetadataUsageId;
extern const uint32_t MonoRemoteCertificateValidationCallback_BeginInvoke_mE8A7228A3C1E37CED0138DA272D8A9A6ED52E2D1_MetadataUsageId;
extern const uint32_t MonoTlsSettings_CloneWithValidator_m46363CAC421939D9904660815CF1D8131C8DC3B1_MetadataUsageId;
extern const uint32_t MonoTlsSettings_Clone_mF28F7F627B12CBD0BD1ABD6F35DD0B4BAD2E3840_MetadataUsageId;
extern const uint32_t MonoTlsSettings__ctor_m3D336E73C9393401BE9BE856B7204BA176F52B17_MetadataUsageId;
extern const uint32_t MonoTlsSettings_get_DefaultSettings_m49A7CECC7D687F62790DD374D560278D3916B887_MetadataUsageId;
extern const uint32_t NtlmSettings__cctor_mCE3B588B6BFFB63D73ECA11941E875814A5CE9BB_MetadataUsageId;
extern const uint32_t Type1Message_GetBytes_m5705CD87750C73B44B8700A02FF0E4C2DE294C9E_MetadataUsageId;
extern const uint32_t Type1Message__ctor_mF11CFA44C4BF05765612B0D3CCBB4EBB433D7B23_MetadataUsageId;
extern const uint32_t Type1Message_set_Domain_m14675CA2220D6338E39DA862B822553AE6DCFD12_MetadataUsageId;
extern const uint32_t Type1Message_set_Host_mC0ADD586CC5A1F9FD5489AF1D8F14DF9AA4F2D68_MetadataUsageId;
extern const uint32_t Type2Message_Decode_m480774BBEA24F18E5C8765273C8DD6A641D9E441_MetadataUsageId;
extern const uint32_t Type2Message__ctor_m85E73F15F691FC25B93503B84B24D25805B5FBD9_MetadataUsageId;
extern const uint32_t Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1_MetadataUsageId;
extern const uint32_t Type2Message_get_TargetInfo_m5E0F0E5A6B32B7512393EDC2DFE9E8A6D79DB485_MetadataUsageId;
extern const uint32_t Type3Message_Decode_m4624A3F2775E1E590627C69F188816C994270B6D_MetadataUsageId;
extern const uint32_t Type3Message_EncodeString_m431F1D808D738A2C9CE57DE1084F9A42C036AA5A_MetadataUsageId;
extern const uint32_t Type3Message_GetBytes_m9C22065DB20CD016FCA226AA2A78952E111CF118_MetadataUsageId;
extern const uint32_t Type3Message__ctor_mBA583598EA5F842A0076F882C4A3205B919419D6_MetadataUsageId;
extern const uint32_t Type3Message_set_Domain_m04440D54FDAA49E5C82380E22C46DBF09C22DBF5_MetadataUsageId;
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_com;
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_pinvoke;
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com;
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4;
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;


#ifndef U3CMODULEU3E_T239CA80C3AF3E763FA4B9A9F3CFADF0768B426EE_H
#define U3CMODULEU3E_T239CA80C3AF3E763FA4B9A9F3CFADF0768B426EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t239CA80C3AF3E763FA4B9A9F3CFADF0768B426EE 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T239CA80C3AF3E763FA4B9A9F3CFADF0768B426EE_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TF21437707AFAA06797AEDEE07C84D4D3CC2837FA_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TF21437707AFAA06797AEDEE07C84D4D3CC2837FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA_StaticFields
{
public:
	// System.Int64 <PrivateImplementationDetails>::16968835DEF6DD3BB86EABA9DEC53BF41851CD6D
	int64_t ___16968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0;
	// System.Int64 <PrivateImplementationDetails>::6FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF
	int64_t ___6FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1;
	// System.Int64 <PrivateImplementationDetails>::AEA5F1CC5CFE1660539EDD691FE017F775F63A0D
	int64_t ___AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2;

public:
	inline static int32_t get_offset_of_U316968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA_StaticFields, ___16968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0)); }
	inline int64_t get_U316968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0() const { return ___16968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0; }
	inline int64_t* get_address_of_U316968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0() { return &___16968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0; }
	inline void set_U316968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0(int64_t value)
	{
		___16968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0 = value;
	}

	inline static int32_t get_offset_of_U36FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA_StaticFields, ___6FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1)); }
	inline int64_t get_U36FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1() const { return ___6FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1; }
	inline int64_t* get_address_of_U36FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1() { return &___6FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1; }
	inline void set_U36FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1(int64_t value)
	{
		___6FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1 = value;
	}

	inline static int32_t get_offset_of_AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA_StaticFields, ___AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2)); }
	inline int64_t get_AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2() const { return ___AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2; }
	inline int64_t* get_address_of_AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2() { return &___AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2; }
	inline void set_AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2(int64_t value)
	{
		___AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TF21437707AFAA06797AEDEE07C84D4D3CC2837FA_H
#ifndef LOCALE_T3E5C8C81DE8D62AC0972B423D543F499CE5BA9E3_H
#define LOCALE_T3E5C8C81DE8D62AC0972B423D543F499CE5BA9E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Locale
struct  Locale_t3E5C8C81DE8D62AC0972B423D543F499CE5BA9E3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALE_T3E5C8C81DE8D62AC0972B423D543F499CE5BA9E3_H
#ifndef BITCONVERTERLE_T4CE9DF1164753ED72B6F4F33581C35FBCAEEC109_H
#define BITCONVERTERLE_T4CE9DF1164753ED72B6F4F33581C35FBCAEEC109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.BitConverterLE
struct  BitConverterLE_t4CE9DF1164753ED72B6F4F33581C35FBCAEEC109  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITCONVERTERLE_T4CE9DF1164753ED72B6F4F33581C35FBCAEEC109_H
#ifndef MONOTLSPROVIDER_TDCD056C5BBBE59ED6BAF63F25952B406C1143C27_H
#define MONOTLSPROVIDER_TDCD056C5BBBE59ED6BAF63F25952B406C1143C27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsProvider
struct  MonoTlsProvider_tDCD056C5BBBE59ED6BAF63F25952B406C1143C27  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDER_TDCD056C5BBBE59ED6BAF63F25952B406C1143C27_H
#ifndef CHALLENGERESPONSE_T2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_H
#define CHALLENGERESPONSE_T2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.ChallengeResponse
struct  ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B  : public RuntimeObject
{
public:
	// System.Boolean Mono.Security.Protocol.Ntlm.ChallengeResponse::_disposed
	bool ____disposed_2;
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::_challenge
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____challenge_3;
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::_lmpwd
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____lmpwd_4;
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::_ntpwd
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____ntpwd_5;

public:
	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}

	inline static int32_t get_offset_of__challenge_3() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B, ____challenge_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__challenge_3() const { return ____challenge_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__challenge_3() { return &____challenge_3; }
	inline void set__challenge_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____challenge_3 = value;
		Il2CppCodeGenWriteBarrier((&____challenge_3), value);
	}

	inline static int32_t get_offset_of__lmpwd_4() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B, ____lmpwd_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__lmpwd_4() const { return ____lmpwd_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__lmpwd_4() { return &____lmpwd_4; }
	inline void set__lmpwd_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____lmpwd_4 = value;
		Il2CppCodeGenWriteBarrier((&____lmpwd_4), value);
	}

	inline static int32_t get_offset_of__ntpwd_5() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B, ____ntpwd_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__ntpwd_5() const { return ____ntpwd_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__ntpwd_5() { return &____ntpwd_5; }
	inline void set__ntpwd_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____ntpwd_5 = value;
		Il2CppCodeGenWriteBarrier((&____ntpwd_5), value);
	}
};

struct ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::magic
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___magic_0;
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::nullEncMagic
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nullEncMagic_1;

public:
	inline static int32_t get_offset_of_magic_0() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields, ___magic_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_magic_0() const { return ___magic_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_magic_0() { return &___magic_0; }
	inline void set_magic_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___magic_0 = value;
		Il2CppCodeGenWriteBarrier((&___magic_0), value);
	}

	inline static int32_t get_offset_of_nullEncMagic_1() { return static_cast<int32_t>(offsetof(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields, ___nullEncMagic_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nullEncMagic_1() const { return ___nullEncMagic_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nullEncMagic_1() { return &___nullEncMagic_1; }
	inline void set_nullEncMagic_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nullEncMagic_1 = value;
		Il2CppCodeGenWriteBarrier((&___nullEncMagic_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGERESPONSE_T2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_H
#ifndef CHALLENGERESPONSE2_T2F5817D7717011A7F4C0DE0015D144A2292CAD77_H
#define CHALLENGERESPONSE2_T2F5817D7717011A7F4C0DE0015D144A2292CAD77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.ChallengeResponse2
struct  ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77  : public RuntimeObject
{
public:

public:
};

struct ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::magic
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___magic_0;
	// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::nullEncMagic
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nullEncMagic_1;

public:
	inline static int32_t get_offset_of_magic_0() { return static_cast<int32_t>(offsetof(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields, ___magic_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_magic_0() const { return ___magic_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_magic_0() { return &___magic_0; }
	inline void set_magic_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___magic_0 = value;
		Il2CppCodeGenWriteBarrier((&___magic_0), value);
	}

	inline static int32_t get_offset_of_nullEncMagic_1() { return static_cast<int32_t>(offsetof(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields, ___nullEncMagic_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nullEncMagic_1() const { return ___nullEncMagic_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nullEncMagic_1() { return &___nullEncMagic_1; }
	inline void set_nullEncMagic_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nullEncMagic_1 = value;
		Il2CppCodeGenWriteBarrier((&___nullEncMagic_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHALLENGERESPONSE2_T2F5817D7717011A7F4C0DE0015D144A2292CAD77_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef BITCONVERTER_TD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_H
#define BITCONVERTER_TD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.BitConverter
struct  BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE  : public RuntimeObject
{
public:

public:
};

struct BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_StaticFields
{
public:
	// System.Boolean System.BitConverter::IsLittleEndian
	bool ___IsLittleEndian_0;

public:
	inline static int32_t get_offset_of_IsLittleEndian_0() { return static_cast<int32_t>(offsetof(BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_StaticFields, ___IsLittleEndian_0)); }
	inline bool get_IsLittleEndian_0() const { return ___IsLittleEndian_0; }
	inline bool* get_address_of_IsLittleEndian_0() { return &___IsLittleEndian_0; }
	inline void set_IsLittleEndian_0(bool value)
	{
		___IsLittleEndian_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITCONVERTER_TD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_H
#ifndef COLLECTIONBASE_TF5D4583FF325726066A9803839A04E9C0084ED01_H
#define COLLECTIONBASE_TF5D4583FF325726066A9803839A04E9C0084ED01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01, ___list_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_TF5D4583FF325726066A9803839A04E9C0084ED01_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef CULTUREINFO_T345AC6924134F039ED9A11F3E03F8E91B6A3225F_H
#define CULTUREINFO_T345AC6924134F039ED9A11F3E03F8E91B6A3225F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___numInfo_10)); }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_10), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_11), value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___textInfo_12)); }
	inline TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_12), value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_13), value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_14), value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_15), value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_16), value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_17), value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_18), value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((&___territory_19), value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___native_calendar_names_20)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((&___native_calendar_names_20), value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___compareInfo_21)); }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_21), value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___calendar_24)); }
	inline Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_24), value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___parent_culture_25)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_25), value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_27), value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_cultureData_28)); }
	inline CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_cultureData_28), value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_0), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_1), value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((&___default_current_culture_2), value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultThreadCurrentUICulture_33), value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultThreadCurrentCulture_34), value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_35), value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_36), value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	uint8_t* ___cached_serialized_form_27;
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	uint8_t* ___cached_serialized_form_27;
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
#endif // CULTUREINFO_T345AC6924134F039ED9A11F3E03F8E91B6A3225F_H
#ifndef HASHALGORITHM_T65659695B16C0BBF05707BF45191A97DC156D6BA_H
#define HASHALGORITHM_T65659695B16C0BBF05707BF45191A97DC156D6BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_0;
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___HashValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::m_bDisposed
	bool ___m_bDisposed_3;

public:
	inline static int32_t get_offset_of_HashSizeValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA, ___HashSizeValue_0)); }
	inline int32_t get_HashSizeValue_0() const { return ___HashSizeValue_0; }
	inline int32_t* get_address_of_HashSizeValue_0() { return &___HashSizeValue_0; }
	inline void set_HashSizeValue_0(int32_t value)
	{
		___HashSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_HashValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA, ___HashValue_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_HashValue_1() const { return ___HashValue_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_HashValue_1() { return &___HashValue_1; }
	inline void set_HashValue_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___HashValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_1), value);
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_m_bDisposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA, ___m_bDisposed_3)); }
	inline bool get_m_bDisposed_3() const { return ___m_bDisposed_3; }
	inline bool* get_address_of_m_bDisposed_3() { return &___m_bDisposed_3; }
	inline void set_m_bDisposed_3(bool value)
	{
		___m_bDisposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T65659695B16C0BBF05707BF45191A97DC156D6BA_H
#ifndef RANDOMNUMBERGENERATOR_T12277F7F965BA79C54E4B3BFABD27A5FFB725EE2_H
#define RANDOMNUMBERGENERATOR_T12277F7F965BA79C54E4B3BFABD27A5FFB725EE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RandomNumberGenerator
struct  RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMNUMBERGENERATOR_T12277F7F965BA79C54E4B3BFABD27A5FFB725EE2_H
#ifndef X509CERTIFICATE_T6859B8914E252B6831D6F59A2A720CD23F7FA7B2_H
#define X509CERTIFICATE_T6859B8914E252B6831D6F59A2A720CD23F7FA7B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate
struct  X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509CertificateImpl System.Security.Cryptography.X509Certificates.X509Certificate::impl
	X509CertificateImpl_t89610BFDE87B872143A4623CFC7F17275EB48313 * ___impl_0;
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::hideDates
	bool ___hideDates_1;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::issuer_name
	String_t* ___issuer_name_2;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::subject_name
	String_t* ___subject_name_3;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2, ___impl_0)); }
	inline X509CertificateImpl_t89610BFDE87B872143A4623CFC7F17275EB48313 * get_impl_0() const { return ___impl_0; }
	inline X509CertificateImpl_t89610BFDE87B872143A4623CFC7F17275EB48313 ** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(X509CertificateImpl_t89610BFDE87B872143A4623CFC7F17275EB48313 * value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier((&___impl_0), value);
	}

	inline static int32_t get_offset_of_hideDates_1() { return static_cast<int32_t>(offsetof(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2, ___hideDates_1)); }
	inline bool get_hideDates_1() const { return ___hideDates_1; }
	inline bool* get_address_of_hideDates_1() { return &___hideDates_1; }
	inline void set_hideDates_1(bool value)
	{
		___hideDates_1 = value;
	}

	inline static int32_t get_offset_of_issuer_name_2() { return static_cast<int32_t>(offsetof(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2, ___issuer_name_2)); }
	inline String_t* get_issuer_name_2() const { return ___issuer_name_2; }
	inline String_t** get_address_of_issuer_name_2() { return &___issuer_name_2; }
	inline void set_issuer_name_2(String_t* value)
	{
		___issuer_name_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_name_2), value);
	}

	inline static int32_t get_offset_of_subject_name_3() { return static_cast<int32_t>(offsetof(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2, ___subject_name_3)); }
	inline String_t* get_subject_name_3() const { return ___subject_name_3; }
	inline String_t** get_address_of_subject_name_3() { return &___subject_name_3; }
	inline void set_subject_name_3(String_t* value)
	{
		___subject_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___subject_name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE_T6859B8914E252B6831D6F59A2A720CD23F7FA7B2_H
#ifndef X509CHAIN_T4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538_H
#define X509CHAIN_T4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Chain
struct  X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainImpl System.Security.Cryptography.X509Certificates.X509Chain::impl
	X509ChainImpl_t34FABF07BEA0CFB6D88717BCDDE0607D9DA13A67 * ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538, ___impl_0)); }
	inline X509ChainImpl_t34FABF07BEA0CFB6D88717BCDDE0607D9DA13A67 * get_impl_0() const { return ___impl_0; }
	inline X509ChainImpl_t34FABF07BEA0CFB6D88717BCDDE0607D9DA13A67 ** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(X509ChainImpl_t34FABF07BEA0CFB6D88717BCDDE0607D9DA13A67 * value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier((&___impl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAIN_T4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef ENCODING_T7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_H
#define ENCODING_T7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___dataItem_10)); }
	inline CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((&___dataItem_10), value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___encoderFallback_13)); }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___encoderFallback_13), value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___decoderFallback_14)); }
	inline DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___decoderFallback_14), value);
	}
};

struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_0), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_1), value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUnicode_2), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_3), value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((&___utf8Encoding_4), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_5), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_6), value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___latin1Encoding_7), value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___encodings_8)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_8), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef MD4_T932C1DEA44D4B8650873251E88AA4096164BB380_H
#define MD4_T932C1DEA44D4B8650873251E88AA4096164BB380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD4
struct  MD4_t932C1DEA44D4B8650873251E88AA4096164BB380  : public HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD4_T932C1DEA44D4B8650873251E88AA4096164BB380_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#define BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT16_T823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_H
#define INT16_T823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int16
struct  Int16_t823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_t823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16_T823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef KEYEDHASHALGORITHM_T83CFA2CA5A4F0F39B747E61D013CB5EB919D218B_H
#define KEYEDHASHALGORITHM_T83CFA2CA5A4F0F39B747E61D013CB5EB919D218B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.KeyedHashAlgorithm
struct  KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B  : public HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA
{
public:
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___KeyValue_4;

public:
	inline static int32_t get_offset_of_KeyValue_4() { return static_cast<int32_t>(offsetof(KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B, ___KeyValue_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_KeyValue_4() const { return ___KeyValue_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_KeyValue_4() { return &___KeyValue_4; }
	inline void set_KeyValue_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___KeyValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEDHASHALGORITHM_T83CFA2CA5A4F0F39B747E61D013CB5EB919D218B_H
#ifndef MD5_TCED753745572EC20FE5D31D15F132736B5343EE6_H
#define MD5_TCED753745572EC20FE5D31D15F132736B5343EE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.MD5
struct  MD5_tCED753745572EC20FE5D31D15F132736B5343EE6  : public HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5_TCED753745572EC20FE5D31D15F132736B5343EE6_H
#ifndef X509CERTIFICATECOLLECTION_T824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833_H
#define X509CERTIFICATECOLLECTION_T824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct  X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833  : public CollectionBase_tF5D4583FF325726066A9803839A04E9C0084ED01
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATECOLLECTION_T824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef UINT16_TAE45CEF73BF720100519F6867F32145D075F928E_H
#define UINT16_TAE45CEF73BF720100519F6867F32145D075F928E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt16
struct  UInt16_tAE45CEF73BF720100519F6867F32145D075F928E 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_tAE45CEF73BF720100519F6867F32145D075F928E, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16_TAE45CEF73BF720100519F6867F32145D075F928E_H
#ifndef UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#define UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef MD4MANAGED_T1FC9A6CDB0A89A71416414689B3A0FF0283759D1_H
#define MD4MANAGED_T1FC9A6CDB0A89A71416414689B3A0FF0283759D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD4Managed
struct  MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1  : public MD4_t932C1DEA44D4B8650873251E88AA4096164BB380
{
public:
	// System.UInt32[] Mono.Security.Cryptography.MD4Managed::state
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___state_4;
	// System.Byte[] Mono.Security.Cryptography.MD4Managed::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_5;
	// System.UInt32[] Mono.Security.Cryptography.MD4Managed::count
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___count_6;
	// System.UInt32[] Mono.Security.Cryptography.MD4Managed::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_7;
	// System.Byte[] Mono.Security.Cryptography.MD4Managed::digest
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___digest_8;

public:
	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1, ___state_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_state_4() const { return ___state_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1, ___buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_5() const { return ___buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_5), value);
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1, ___count_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_count_6() const { return ___count_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___count_6 = value;
		Il2CppCodeGenWriteBarrier((&___count_6), value);
	}

	inline static int32_t get_offset_of_x_7() { return static_cast<int32_t>(offsetof(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1, ___x_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_7() const { return ___x_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_7() { return &___x_7; }
	inline void set_x_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_7 = value;
		Il2CppCodeGenWriteBarrier((&___x_7), value);
	}

	inline static int32_t get_offset_of_digest_8() { return static_cast<int32_t>(offsetof(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1, ___digest_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_digest_8() const { return ___digest_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_digest_8() { return &___digest_8; }
	inline void set_digest_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___digest_8 = value;
		Il2CppCodeGenWriteBarrier((&___digest_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD4MANAGED_T1FC9A6CDB0A89A71416414689B3A0FF0283759D1_H
#ifndef CIPHERSUITECODE_T32674B07A5C552605FA138AEACFFA20474A255F1_H
#define CIPHERSUITECODE_T32674B07A5C552605FA138AEACFFA20474A255F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.CipherSuiteCode
struct  CipherSuiteCode_t32674B07A5C552605FA138AEACFFA20474A255F1 
{
public:
	// System.UInt16 Mono.Security.Interface.CipherSuiteCode::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherSuiteCode_t32674B07A5C552605FA138AEACFFA20474A255F1, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSUITECODE_T32674B07A5C552605FA138AEACFFA20474A255F1_H
#ifndef MONOSSLPOLICYERRORS_T5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7_H
#define MONOSSLPOLICYERRORS_T5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoSslPolicyErrors
struct  MonoSslPolicyErrors_t5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7 
{
public:
	// System.Int32 Mono.Security.Interface.MonoSslPolicyErrors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MonoSslPolicyErrors_t5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSSLPOLICYERRORS_T5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7_H
#ifndef TLSPROTOCOLS_T25D1B0EFE5CC77B30D19258E7AC462AB4D828163_H
#define TLSPROTOCOLS_T25D1B0EFE5CC77B30D19258E7AC462AB4D828163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.TlsProtocols
struct  TlsProtocols_t25D1B0EFE5CC77B30D19258E7AC462AB4D828163 
{
public:
	// System.Int32 Mono.Security.Interface.TlsProtocols::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TlsProtocols_t25D1B0EFE5CC77B30D19258E7AC462AB4D828163, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSPROTOCOLS_T25D1B0EFE5CC77B30D19258E7AC462AB4D828163_H
#ifndef NTLMAUTHLEVEL_TF1354DE8BF43C36E20D475A077E035BB11936015_H
#define NTLMAUTHLEVEL_TF1354DE8BF43C36E20D475A077E035BB11936015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.NtlmAuthLevel
struct  NtlmAuthLevel_tF1354DE8BF43C36E20D475A077E035BB11936015 
{
public:
	// System.Int32 Mono.Security.Protocol.Ntlm.NtlmAuthLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NtlmAuthLevel_tF1354DE8BF43C36E20D475A077E035BB11936015, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMAUTHLEVEL_TF1354DE8BF43C36E20D475A077E035BB11936015_H
#ifndef NTLMFLAGS_T9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578_H
#define NTLMFLAGS_T9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.NtlmFlags
struct  NtlmFlags_t9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578 
{
public:
	// System.Int32 Mono.Security.Protocol.Ntlm.NtlmFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NtlmFlags_t9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMFLAGS_T9AC7D2604BC2E16EDEF1C9D2066E6BEC0D8A1578_H
#ifndef ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#define ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_paramName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#define INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifndef NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#define NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 
{
public:
	// T System.Nullable`1::value
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___value_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_value_0() const { return ___value_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifndef RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#define RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#ifndef CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#define CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t1DC3069D617AC3D17A2608F5BB36C0F115D229DF 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherMode_t1DC3069D617AC3D17A2608F5BB36C0F115D229DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#ifndef HMAC_T85BE56AD7946F3A3D42B52448301D8995DEB7C90_H
#define HMAC_T85BE56AD7946F3A3D42B52448301D8995DEB7C90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HMAC
struct  HMAC_t85BE56AD7946F3A3D42B52448301D8995DEB7C90  : public KeyedHashAlgorithm_t83CFA2CA5A4F0F39B747E61D013CB5EB919D218B
{
public:
	// System.Int32 System.Security.Cryptography.HMAC::blockSizeValue
	int32_t ___blockSizeValue_5;
	// System.String System.Security.Cryptography.HMAC::m_hashName
	String_t* ___m_hashName_6;
	// System.Security.Cryptography.HashAlgorithm System.Security.Cryptography.HMAC::m_hash1
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___m_hash1_7;
	// System.Security.Cryptography.HashAlgorithm System.Security.Cryptography.HMAC::m_hash2
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___m_hash2_8;
	// System.Byte[] System.Security.Cryptography.HMAC::m_inner
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_inner_9;
	// System.Byte[] System.Security.Cryptography.HMAC::m_outer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_outer_10;
	// System.Boolean System.Security.Cryptography.HMAC::m_hashing
	bool ___m_hashing_11;

public:
	inline static int32_t get_offset_of_blockSizeValue_5() { return static_cast<int32_t>(offsetof(HMAC_t85BE56AD7946F3A3D42B52448301D8995DEB7C90, ___blockSizeValue_5)); }
	inline int32_t get_blockSizeValue_5() const { return ___blockSizeValue_5; }
	inline int32_t* get_address_of_blockSizeValue_5() { return &___blockSizeValue_5; }
	inline void set_blockSizeValue_5(int32_t value)
	{
		___blockSizeValue_5 = value;
	}

	inline static int32_t get_offset_of_m_hashName_6() { return static_cast<int32_t>(offsetof(HMAC_t85BE56AD7946F3A3D42B52448301D8995DEB7C90, ___m_hashName_6)); }
	inline String_t* get_m_hashName_6() const { return ___m_hashName_6; }
	inline String_t** get_address_of_m_hashName_6() { return &___m_hashName_6; }
	inline void set_m_hashName_6(String_t* value)
	{
		___m_hashName_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashName_6), value);
	}

	inline static int32_t get_offset_of_m_hash1_7() { return static_cast<int32_t>(offsetof(HMAC_t85BE56AD7946F3A3D42B52448301D8995DEB7C90, ___m_hash1_7)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_m_hash1_7() const { return ___m_hash1_7; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_m_hash1_7() { return &___m_hash1_7; }
	inline void set_m_hash1_7(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___m_hash1_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_hash1_7), value);
	}

	inline static int32_t get_offset_of_m_hash2_8() { return static_cast<int32_t>(offsetof(HMAC_t85BE56AD7946F3A3D42B52448301D8995DEB7C90, ___m_hash2_8)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_m_hash2_8() const { return ___m_hash2_8; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_m_hash2_8() { return &___m_hash2_8; }
	inline void set_m_hash2_8(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___m_hash2_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_hash2_8), value);
	}

	inline static int32_t get_offset_of_m_inner_9() { return static_cast<int32_t>(offsetof(HMAC_t85BE56AD7946F3A3D42B52448301D8995DEB7C90, ___m_inner_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_inner_9() const { return ___m_inner_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_inner_9() { return &___m_inner_9; }
	inline void set_m_inner_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_inner_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_inner_9), value);
	}

	inline static int32_t get_offset_of_m_outer_10() { return static_cast<int32_t>(offsetof(HMAC_t85BE56AD7946F3A3D42B52448301D8995DEB7C90, ___m_outer_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_outer_10() const { return ___m_outer_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_outer_10() { return &___m_outer_10; }
	inline void set_m_outer_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_outer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_10), value);
	}

	inline static int32_t get_offset_of_m_hashing_11() { return static_cast<int32_t>(offsetof(HMAC_t85BE56AD7946F3A3D42B52448301D8995DEB7C90, ___m_hashing_11)); }
	inline bool get_m_hashing_11() const { return ___m_hashing_11; }
	inline bool* get_address_of_m_hashing_11() { return &___m_hashing_11; }
	inline void set_m_hashing_11(bool value)
	{
		___m_hashing_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMAC_T85BE56AD7946F3A3D42B52448301D8995DEB7C90_H
#ifndef PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#define PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_tA6F228B2795D29C9554F2D6824DB9FF67519A0E0 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PaddingMode_tA6F228B2795D29C9554F2D6824DB9FF67519A0E0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#ifndef MESSAGEBASE_T504D166CC4021DEB56DED308D5E82C67F47F26C0_H
#define MESSAGEBASE_T504D166CC4021DEB56DED308D5E82C67F47F26C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.MessageBase
struct  MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0  : public RuntimeObject
{
public:
	// System.Int32 Mono.Security.Protocol.Ntlm.MessageBase::_type
	int32_t ____type_1;
	// Mono.Security.Protocol.Ntlm.NtlmFlags Mono.Security.Protocol.Ntlm.MessageBase::_flags
	int32_t ____flags_2;

public:
	inline static int32_t get_offset_of__type_1() { return static_cast<int32_t>(offsetof(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0, ____type_1)); }
	inline int32_t get__type_1() const { return ____type_1; }
	inline int32_t* get_address_of__type_1() { return &____type_1; }
	inline void set__type_1(int32_t value)
	{
		____type_1 = value;
	}

	inline static int32_t get_offset_of__flags_2() { return static_cast<int32_t>(offsetof(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0, ____flags_2)); }
	inline int32_t get__flags_2() const { return ____flags_2; }
	inline int32_t* get_address_of__flags_2() { return &____flags_2; }
	inline void set__flags_2(int32_t value)
	{
		____flags_2 = value;
	}
};

struct MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.MessageBase::header
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_StaticFields, ___header_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_header_0() const { return ___header_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEBASE_T504D166CC4021DEB56DED308D5E82C67F47F26C0_H
#ifndef NTLMSETTINGS_TC673E811873A17EA73FCA0EFD6D33839B5036009_H
#define NTLMSETTINGS_TC673E811873A17EA73FCA0EFD6D33839B5036009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.NtlmSettings
struct  NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009  : public RuntimeObject
{
public:

public:
};

struct NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_StaticFields
{
public:
	// Mono.Security.Protocol.Ntlm.NtlmAuthLevel Mono.Security.Protocol.Ntlm.NtlmSettings::defaultAuthLevel
	int32_t ___defaultAuthLevel_0;

public:
	inline static int32_t get_offset_of_defaultAuthLevel_0() { return static_cast<int32_t>(offsetof(NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_StaticFields, ___defaultAuthLevel_0)); }
	inline int32_t get_defaultAuthLevel_0() const { return ___defaultAuthLevel_0; }
	inline int32_t* get_address_of_defaultAuthLevel_0() { return &___defaultAuthLevel_0; }
	inline void set_defaultAuthLevel_0(int32_t value)
	{
		___defaultAuthLevel_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTLMSETTINGS_TC673E811873A17EA73FCA0EFD6D33839B5036009_H
#ifndef ARGUMENTNULLEXCEPTION_T581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_H
#define ARGUMENTNULLEXCEPTION_T581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T94D19DF918A54511AEDF4784C9A08741BAD1DEDA_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T94D19DF918A54511AEDF4784C9A08741BAD1DEDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_actualValue_19), value);
	}
};

struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((&____rangeMessage_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T94D19DF918A54511AEDF4784C9A08741BAD1DEDA_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T601798BE10C3F3F37B6755E475BB1B3760DCBB10_H
#define NULLABLE_1_T601798BE10C3F3F37B6755E475BB1B3760DCBB10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Mono.Security.Interface.TlsProtocols>
struct  Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T601798BE10C3F3F37B6755E475BB1B3760DCBB10_H
#ifndef OBJECTDISPOSEDEXCEPTION_TF68E471ECD1419AD7C51137B742837395F50B69A_H
#define OBJECTDISPOSEDEXCEPTION_TF68E471ECD1419AD7C51137B742837395F50B69A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObjectDisposedException
struct  ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A  : public InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1
{
public:
	// System.String System.ObjectDisposedException::objectName
	String_t* ___objectName_17;

public:
	inline static int32_t get_offset_of_objectName_17() { return static_cast<int32_t>(offsetof(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A, ___objectName_17)); }
	inline String_t* get_objectName_17() const { return ___objectName_17; }
	inline String_t** get_address_of_objectName_17() { return &___objectName_17; }
	inline void set_objectName_17(String_t* value)
	{
		___objectName_17 = value;
		Il2CppCodeGenWriteBarrier((&___objectName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDISPOSEDEXCEPTION_TF68E471ECD1419AD7C51137B742837395F50B69A_H
#ifndef HMACMD5_T8C6693E41EEA9BF26BBAF880B405CC170C43F11B_H
#define HMACMD5_T8C6693E41EEA9BF26BBAF880B405CC170C43F11B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HMACMD5
struct  HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B  : public HMAC_t85BE56AD7946F3A3D42B52448301D8995DEB7C90
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMACMD5_T8C6693E41EEA9BF26BBAF880B405CC170C43F11B_H
#ifndef SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#define SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SymmetricAlgorithm
struct  SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::BlockSizeValue
	int32_t ___BlockSizeValue_0;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::FeedbackSizeValue
	int32_t ___FeedbackSizeValue_1;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::IVValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IVValue_2;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::KeyValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___KeyValue_3;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalBlockSizesValue
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___LegalBlockSizesValue_4;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___LegalKeySizesValue_5;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_6;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::ModeValue
	int32_t ___ModeValue_7;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::PaddingValue
	int32_t ___PaddingValue_8;

public:
	inline static int32_t get_offset_of_BlockSizeValue_0() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___BlockSizeValue_0)); }
	inline int32_t get_BlockSizeValue_0() const { return ___BlockSizeValue_0; }
	inline int32_t* get_address_of_BlockSizeValue_0() { return &___BlockSizeValue_0; }
	inline void set_BlockSizeValue_0(int32_t value)
	{
		___BlockSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_FeedbackSizeValue_1() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___FeedbackSizeValue_1)); }
	inline int32_t get_FeedbackSizeValue_1() const { return ___FeedbackSizeValue_1; }
	inline int32_t* get_address_of_FeedbackSizeValue_1() { return &___FeedbackSizeValue_1; }
	inline void set_FeedbackSizeValue_1(int32_t value)
	{
		___FeedbackSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_IVValue_2() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___IVValue_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IVValue_2() const { return ___IVValue_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IVValue_2() { return &___IVValue_2; }
	inline void set_IVValue_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IVValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___IVValue_2), value);
	}

	inline static int32_t get_offset_of_KeyValue_3() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___KeyValue_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_KeyValue_3() const { return ___KeyValue_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_KeyValue_3() { return &___KeyValue_3; }
	inline void set_KeyValue_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___KeyValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_3), value);
	}

	inline static int32_t get_offset_of_LegalBlockSizesValue_4() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___LegalBlockSizesValue_4)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_LegalBlockSizesValue_4() const { return ___LegalBlockSizesValue_4; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_LegalBlockSizesValue_4() { return &___LegalBlockSizesValue_4; }
	inline void set_LegalBlockSizesValue_4(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___LegalBlockSizesValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___LegalBlockSizesValue_4), value);
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_5() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___LegalKeySizesValue_5)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_LegalKeySizesValue_5() const { return ___LegalKeySizesValue_5; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_LegalKeySizesValue_5() { return &___LegalKeySizesValue_5; }
	inline void set_LegalKeySizesValue_5(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___LegalKeySizesValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_5), value);
	}

	inline static int32_t get_offset_of_KeySizeValue_6() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___KeySizeValue_6)); }
	inline int32_t get_KeySizeValue_6() const { return ___KeySizeValue_6; }
	inline int32_t* get_address_of_KeySizeValue_6() { return &___KeySizeValue_6; }
	inline void set_KeySizeValue_6(int32_t value)
	{
		___KeySizeValue_6 = value;
	}

	inline static int32_t get_offset_of_ModeValue_7() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___ModeValue_7)); }
	inline int32_t get_ModeValue_7() const { return ___ModeValue_7; }
	inline int32_t* get_address_of_ModeValue_7() { return &___ModeValue_7; }
	inline void set_ModeValue_7(int32_t value)
	{
		___ModeValue_7 = value;
	}

	inline static int32_t get_offset_of_PaddingValue_8() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___PaddingValue_8)); }
	inline int32_t get_PaddingValue_8() const { return ___PaddingValue_8; }
	inline int32_t* get_address_of_PaddingValue_8() { return &___PaddingValue_8; }
	inline void set_PaddingValue_8(int32_t value)
	{
		___PaddingValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#ifndef MONOLOCALCERTIFICATESELECTIONCALLBACK_T657381EF916D4EDC456FA5A6AC948EFD7A481F0A_H
#define MONOLOCALCERTIFICATESELECTIONCALLBACK_T657381EF916D4EDC456FA5A6AC948EFD7A481F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct  MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOLOCALCERTIFICATESELECTIONCALLBACK_T657381EF916D4EDC456FA5A6AC948EFD7A481F0A_H
#ifndef MONOREMOTECERTIFICATEVALIDATIONCALLBACK_T7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6_H
#define MONOREMOTECERTIFICATEVALIDATIONCALLBACK_T7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct  MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOREMOTECERTIFICATEVALIDATIONCALLBACK_T7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6_H
#ifndef MONOTLSSETTINGS_T5905C7532C92A87F88C8F3440165DF8AA49A1BBF_H
#define MONOTLSSETTINGS_T5905C7532C92A87F88C8F3440165DF8AA49A1BBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsSettings
struct  MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoRemoteCertificateValidationCallback Mono.Security.Interface.MonoTlsSettings::<RemoteCertificateValidationCallback>k__BackingField
	MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0;
	// Mono.Security.Interface.MonoLocalCertificateSelectionCallback Mono.Security.Interface.MonoTlsSettings::<ClientCertificateSelectionCallback>k__BackingField
	MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1;
	// System.Nullable`1<System.DateTime> Mono.Security.Interface.MonoTlsSettings::<CertificateValidationTime>k__BackingField
	Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  ___U3CCertificateValidationTimeU3Ek__BackingField_2;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Security.Interface.MonoTlsSettings::<TrustAnchors>k__BackingField
	X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * ___U3CTrustAnchorsU3Ek__BackingField_3;
	// System.Object Mono.Security.Interface.MonoTlsSettings::<UserSettings>k__BackingField
	RuntimeObject * ___U3CUserSettingsU3Ek__BackingField_4;
	// System.String[] Mono.Security.Interface.MonoTlsSettings::<CertificateSearchPaths>k__BackingField
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___U3CCertificateSearchPathsU3Ek__BackingField_5;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::<SendCloseNotify>k__BackingField
	bool ___U3CSendCloseNotifyU3Ek__BackingField_6;
	// System.Nullable`1<Mono.Security.Interface.TlsProtocols> Mono.Security.Interface.MonoTlsSettings::<EnabledProtocols>k__BackingField
	Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10  ___U3CEnabledProtocolsU3Ek__BackingField_7;
	// Mono.Security.Interface.CipherSuiteCode[] Mono.Security.Interface.MonoTlsSettings::<EnabledCiphers>k__BackingField
	CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4* ___U3CEnabledCiphersU3Ek__BackingField_8;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::cloned
	bool ___cloned_9;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::checkCertName
	bool ___checkCertName_10;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::checkCertRevocationStatus
	bool ___checkCertRevocationStatus_11;
	// System.Nullable`1<System.Boolean> Mono.Security.Interface.MonoTlsSettings::useServicePointManagerCallback
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___useServicePointManagerCallback_12;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::skipSystemValidators
	bool ___skipSystemValidators_13;
	// System.Boolean Mono.Security.Interface.MonoTlsSettings::callbackNeedsChain
	bool ___callbackNeedsChain_14;
	// Mono.Security.Interface.ICertificateValidator Mono.Security.Interface.MonoTlsSettings::certificateValidator
	RuntimeObject* ___certificateValidator_15;

public:
	inline static int32_t get_offset_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0)); }
	inline MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * get_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() const { return ___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0; }
	inline MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 ** get_address_of_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0() { return &___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0; }
	inline void set_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0(MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * value)
	{
		___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1)); }
	inline MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * get_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() const { return ___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1; }
	inline MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A ** get_address_of_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1() { return &___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1; }
	inline void set_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1(MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * value)
	{
		___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientCertificateSelectionCallbackU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CCertificateValidationTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CCertificateValidationTimeU3Ek__BackingField_2)); }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  get_U3CCertificateValidationTimeU3Ek__BackingField_2() const { return ___U3CCertificateValidationTimeU3Ek__BackingField_2; }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 * get_address_of_U3CCertificateValidationTimeU3Ek__BackingField_2() { return &___U3CCertificateValidationTimeU3Ek__BackingField_2; }
	inline void set_U3CCertificateValidationTimeU3Ek__BackingField_2(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  value)
	{
		___U3CCertificateValidationTimeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTrustAnchorsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CTrustAnchorsU3Ek__BackingField_3)); }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * get_U3CTrustAnchorsU3Ek__BackingField_3() const { return ___U3CTrustAnchorsU3Ek__BackingField_3; }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 ** get_address_of_U3CTrustAnchorsU3Ek__BackingField_3() { return &___U3CTrustAnchorsU3Ek__BackingField_3; }
	inline void set_U3CTrustAnchorsU3Ek__BackingField_3(X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * value)
	{
		___U3CTrustAnchorsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrustAnchorsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CUserSettingsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CUserSettingsU3Ek__BackingField_4)); }
	inline RuntimeObject * get_U3CUserSettingsU3Ek__BackingField_4() const { return ___U3CUserSettingsU3Ek__BackingField_4; }
	inline RuntimeObject ** get_address_of_U3CUserSettingsU3Ek__BackingField_4() { return &___U3CUserSettingsU3Ek__BackingField_4; }
	inline void set_U3CUserSettingsU3Ek__BackingField_4(RuntimeObject * value)
	{
		___U3CUserSettingsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserSettingsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCertificateSearchPathsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CCertificateSearchPathsU3Ek__BackingField_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_U3CCertificateSearchPathsU3Ek__BackingField_5() const { return ___U3CCertificateSearchPathsU3Ek__BackingField_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_U3CCertificateSearchPathsU3Ek__BackingField_5() { return &___U3CCertificateSearchPathsU3Ek__BackingField_5; }
	inline void set_U3CCertificateSearchPathsU3Ek__BackingField_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___U3CCertificateSearchPathsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCertificateSearchPathsU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendCloseNotifyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CSendCloseNotifyU3Ek__BackingField_6)); }
	inline bool get_U3CSendCloseNotifyU3Ek__BackingField_6() const { return ___U3CSendCloseNotifyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CSendCloseNotifyU3Ek__BackingField_6() { return &___U3CSendCloseNotifyU3Ek__BackingField_6; }
	inline void set_U3CSendCloseNotifyU3Ek__BackingField_6(bool value)
	{
		___U3CSendCloseNotifyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CEnabledProtocolsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CEnabledProtocolsU3Ek__BackingField_7)); }
	inline Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10  get_U3CEnabledProtocolsU3Ek__BackingField_7() const { return ___U3CEnabledProtocolsU3Ek__BackingField_7; }
	inline Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10 * get_address_of_U3CEnabledProtocolsU3Ek__BackingField_7() { return &___U3CEnabledProtocolsU3Ek__BackingField_7; }
	inline void set_U3CEnabledProtocolsU3Ek__BackingField_7(Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10  value)
	{
		___U3CEnabledProtocolsU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CEnabledCiphersU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___U3CEnabledCiphersU3Ek__BackingField_8)); }
	inline CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4* get_U3CEnabledCiphersU3Ek__BackingField_8() const { return ___U3CEnabledCiphersU3Ek__BackingField_8; }
	inline CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4** get_address_of_U3CEnabledCiphersU3Ek__BackingField_8() { return &___U3CEnabledCiphersU3Ek__BackingField_8; }
	inline void set_U3CEnabledCiphersU3Ek__BackingField_8(CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4* value)
	{
		___U3CEnabledCiphersU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnabledCiphersU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_cloned_9() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___cloned_9)); }
	inline bool get_cloned_9() const { return ___cloned_9; }
	inline bool* get_address_of_cloned_9() { return &___cloned_9; }
	inline void set_cloned_9(bool value)
	{
		___cloned_9 = value;
	}

	inline static int32_t get_offset_of_checkCertName_10() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___checkCertName_10)); }
	inline bool get_checkCertName_10() const { return ___checkCertName_10; }
	inline bool* get_address_of_checkCertName_10() { return &___checkCertName_10; }
	inline void set_checkCertName_10(bool value)
	{
		___checkCertName_10 = value;
	}

	inline static int32_t get_offset_of_checkCertRevocationStatus_11() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___checkCertRevocationStatus_11)); }
	inline bool get_checkCertRevocationStatus_11() const { return ___checkCertRevocationStatus_11; }
	inline bool* get_address_of_checkCertRevocationStatus_11() { return &___checkCertRevocationStatus_11; }
	inline void set_checkCertRevocationStatus_11(bool value)
	{
		___checkCertRevocationStatus_11 = value;
	}

	inline static int32_t get_offset_of_useServicePointManagerCallback_12() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___useServicePointManagerCallback_12)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_useServicePointManagerCallback_12() const { return ___useServicePointManagerCallback_12; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_useServicePointManagerCallback_12() { return &___useServicePointManagerCallback_12; }
	inline void set_useServicePointManagerCallback_12(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___useServicePointManagerCallback_12 = value;
	}

	inline static int32_t get_offset_of_skipSystemValidators_13() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___skipSystemValidators_13)); }
	inline bool get_skipSystemValidators_13() const { return ___skipSystemValidators_13; }
	inline bool* get_address_of_skipSystemValidators_13() { return &___skipSystemValidators_13; }
	inline void set_skipSystemValidators_13(bool value)
	{
		___skipSystemValidators_13 = value;
	}

	inline static int32_t get_offset_of_callbackNeedsChain_14() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___callbackNeedsChain_14)); }
	inline bool get_callbackNeedsChain_14() const { return ___callbackNeedsChain_14; }
	inline bool* get_address_of_callbackNeedsChain_14() { return &___callbackNeedsChain_14; }
	inline void set_callbackNeedsChain_14(bool value)
	{
		___callbackNeedsChain_14 = value;
	}

	inline static int32_t get_offset_of_certificateValidator_15() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF, ___certificateValidator_15)); }
	inline RuntimeObject* get_certificateValidator_15() const { return ___certificateValidator_15; }
	inline RuntimeObject** get_address_of_certificateValidator_15() { return &___certificateValidator_15; }
	inline void set_certificateValidator_15(RuntimeObject* value)
	{
		___certificateValidator_15 = value;
		Il2CppCodeGenWriteBarrier((&___certificateValidator_15), value);
	}
};

struct MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_StaticFields
{
public:
	// Mono.Security.Interface.MonoTlsSettings Mono.Security.Interface.MonoTlsSettings::defaultSettings
	MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * ___defaultSettings_16;

public:
	inline static int32_t get_offset_of_defaultSettings_16() { return static_cast<int32_t>(offsetof(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_StaticFields, ___defaultSettings_16)); }
	inline MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * get_defaultSettings_16() const { return ___defaultSettings_16; }
	inline MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF ** get_address_of_defaultSettings_16() { return &___defaultSettings_16; }
	inline void set_defaultSettings_16(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * value)
	{
		___defaultSettings_16 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSettings_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSSETTINGS_T5905C7532C92A87F88C8F3440165DF8AA49A1BBF_H
#ifndef TYPE1MESSAGE_TF2DA0014BB300ABA864D84752FFA278EC6E6519C_H
#define TYPE1MESSAGE_TF2DA0014BB300ABA864D84752FFA278EC6E6519C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.Type1Message
struct  Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C  : public MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0
{
public:
	// System.String Mono.Security.Protocol.Ntlm.Type1Message::_host
	String_t* ____host_3;
	// System.String Mono.Security.Protocol.Ntlm.Type1Message::_domain
	String_t* ____domain_4;

public:
	inline static int32_t get_offset_of__host_3() { return static_cast<int32_t>(offsetof(Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C, ____host_3)); }
	inline String_t* get__host_3() const { return ____host_3; }
	inline String_t** get_address_of__host_3() { return &____host_3; }
	inline void set__host_3(String_t* value)
	{
		____host_3 = value;
		Il2CppCodeGenWriteBarrier((&____host_3), value);
	}

	inline static int32_t get_offset_of__domain_4() { return static_cast<int32_t>(offsetof(Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C, ____domain_4)); }
	inline String_t* get__domain_4() const { return ____domain_4; }
	inline String_t** get_address_of__domain_4() { return &____domain_4; }
	inline void set__domain_4(String_t* value)
	{
		____domain_4 = value;
		Il2CppCodeGenWriteBarrier((&____domain_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE1MESSAGE_TF2DA0014BB300ABA864D84752FFA278EC6E6519C_H
#ifndef TYPE2MESSAGE_T990283F48D41AECF1FFBDAA3A194CDE9C9078398_H
#define TYPE2MESSAGE_T990283F48D41AECF1FFBDAA3A194CDE9C9078398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.Type2Message
struct  Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398  : public MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::_nonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____nonce_3;
	// System.String Mono.Security.Protocol.Ntlm.Type2Message::_targetName
	String_t* ____targetName_4;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::_targetInfo
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____targetInfo_5;

public:
	inline static int32_t get_offset_of__nonce_3() { return static_cast<int32_t>(offsetof(Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398, ____nonce_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__nonce_3() const { return ____nonce_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__nonce_3() { return &____nonce_3; }
	inline void set__nonce_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____nonce_3 = value;
		Il2CppCodeGenWriteBarrier((&____nonce_3), value);
	}

	inline static int32_t get_offset_of__targetName_4() { return static_cast<int32_t>(offsetof(Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398, ____targetName_4)); }
	inline String_t* get__targetName_4() const { return ____targetName_4; }
	inline String_t** get_address_of__targetName_4() { return &____targetName_4; }
	inline void set__targetName_4(String_t* value)
	{
		____targetName_4 = value;
		Il2CppCodeGenWriteBarrier((&____targetName_4), value);
	}

	inline static int32_t get_offset_of__targetInfo_5() { return static_cast<int32_t>(offsetof(Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398, ____targetInfo_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__targetInfo_5() const { return ____targetInfo_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__targetInfo_5() { return &____targetInfo_5; }
	inline void set__targetInfo_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____targetInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&____targetInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE2MESSAGE_T990283F48D41AECF1FFBDAA3A194CDE9C9078398_H
#ifndef TYPE3MESSAGE_T6D21CF9E3D56192F8D9B6E2B29474773E838846C_H
#define TYPE3MESSAGE_T6D21CF9E3D56192F8D9B6E2B29474773E838846C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.Type3Message
struct  Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C  : public MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0
{
public:
	// Mono.Security.Protocol.Ntlm.NtlmAuthLevel Mono.Security.Protocol.Ntlm.Type3Message::_level
	int32_t ____level_3;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::_challenge
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____challenge_4;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_host
	String_t* ____host_5;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_domain
	String_t* ____domain_6;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_username
	String_t* ____username_7;
	// System.String Mono.Security.Protocol.Ntlm.Type3Message::_password
	String_t* ____password_8;
	// Mono.Security.Protocol.Ntlm.Type2Message Mono.Security.Protocol.Ntlm.Type3Message::_type2
	Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * ____type2_9;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::_lm
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____lm_10;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::_nt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____nt_11;

public:
	inline static int32_t get_offset_of__level_3() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____level_3)); }
	inline int32_t get__level_3() const { return ____level_3; }
	inline int32_t* get_address_of__level_3() { return &____level_3; }
	inline void set__level_3(int32_t value)
	{
		____level_3 = value;
	}

	inline static int32_t get_offset_of__challenge_4() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____challenge_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__challenge_4() const { return ____challenge_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__challenge_4() { return &____challenge_4; }
	inline void set__challenge_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____challenge_4 = value;
		Il2CppCodeGenWriteBarrier((&____challenge_4), value);
	}

	inline static int32_t get_offset_of__host_5() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____host_5)); }
	inline String_t* get__host_5() const { return ____host_5; }
	inline String_t** get_address_of__host_5() { return &____host_5; }
	inline void set__host_5(String_t* value)
	{
		____host_5 = value;
		Il2CppCodeGenWriteBarrier((&____host_5), value);
	}

	inline static int32_t get_offset_of__domain_6() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____domain_6)); }
	inline String_t* get__domain_6() const { return ____domain_6; }
	inline String_t** get_address_of__domain_6() { return &____domain_6; }
	inline void set__domain_6(String_t* value)
	{
		____domain_6 = value;
		Il2CppCodeGenWriteBarrier((&____domain_6), value);
	}

	inline static int32_t get_offset_of__username_7() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____username_7)); }
	inline String_t* get__username_7() const { return ____username_7; }
	inline String_t** get_address_of__username_7() { return &____username_7; }
	inline void set__username_7(String_t* value)
	{
		____username_7 = value;
		Il2CppCodeGenWriteBarrier((&____username_7), value);
	}

	inline static int32_t get_offset_of__password_8() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____password_8)); }
	inline String_t* get__password_8() const { return ____password_8; }
	inline String_t** get_address_of__password_8() { return &____password_8; }
	inline void set__password_8(String_t* value)
	{
		____password_8 = value;
		Il2CppCodeGenWriteBarrier((&____password_8), value);
	}

	inline static int32_t get_offset_of__type2_9() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____type2_9)); }
	inline Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * get__type2_9() const { return ____type2_9; }
	inline Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 ** get_address_of__type2_9() { return &____type2_9; }
	inline void set__type2_9(Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * value)
	{
		____type2_9 = value;
		Il2CppCodeGenWriteBarrier((&____type2_9), value);
	}

	inline static int32_t get_offset_of__lm_10() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____lm_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__lm_10() const { return ____lm_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__lm_10() { return &____lm_10; }
	inline void set__lm_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____lm_10 = value;
		Il2CppCodeGenWriteBarrier((&____lm_10), value);
	}

	inline static int32_t get_offset_of__nt_11() { return static_cast<int32_t>(offsetof(Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C, ____nt_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__nt_11() const { return ____nt_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__nt_11() { return &____nt_11; }
	inline void set__nt_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____nt_11 = value;
		Il2CppCodeGenWriteBarrier((&____nt_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE3MESSAGE_T6D21CF9E3D56192F8D9B6E2B29474773E838846C_H
#ifndef ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#define ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifndef DES_TFB993FE8AF9722A555B0737FE730332CCD86F6F0_H
#define DES_TFB993FE8AF9722A555B0737FE730332CCD86F6F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.DES
struct  DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0  : public SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789
{
public:

public:
};

struct DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0_StaticFields
{
public:
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.DES::s_legalBlockSizes
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___s_legalBlockSizes_9;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.DES::s_legalKeySizes
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___s_legalKeySizes_10;

public:
	inline static int32_t get_offset_of_s_legalBlockSizes_9() { return static_cast<int32_t>(offsetof(DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0_StaticFields, ___s_legalBlockSizes_9)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_s_legalBlockSizes_9() const { return ___s_legalBlockSizes_9; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_s_legalBlockSizes_9() { return &___s_legalBlockSizes_9; }
	inline void set_s_legalBlockSizes_9(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___s_legalBlockSizes_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalBlockSizes_9), value);
	}

	inline static int32_t get_offset_of_s_legalKeySizes_10() { return static_cast<int32_t>(offsetof(DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0_StaticFields, ___s_legalKeySizes_10)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_s_legalKeySizes_10() const { return ___s_legalKeySizes_10; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_s_legalKeySizes_10() { return &___s_legalKeySizes_10; }
	inline void set_s_legalKeySizes_10(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___s_legalKeySizes_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalKeySizes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DES_TFB993FE8AF9722A555B0737FE730332CCD86F6F0_H
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Security.Interface.CipherSuiteCode[]
struct CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint16_t m_Items[1];

public:
	inline uint16_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint16_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint16_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint16_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint16_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint16_t value)
	{
		m_Items[index] = value;
	}
};



// System.Byte[] Mono.Security.BitConverterLE::GetULongBytes(System.Byte*)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* BitConverterLE_GetULongBytes_mCBAC987169706D10F05AFF42559A4EA1D88E1876 (uint8_t* ___bytes0, const RuntimeMethod* method);
// System.Void Mono.Security.BitConverterLE::UShortFromBytes(System.Byte*,System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void BitConverterLE_UShortFromBytes_mBC051D16FFC95E9695F110AFDEAB018BD76F84A9 (uint8_t* ___dst0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___src1, int32_t ___startIndex2, const RuntimeMethod* method);
// System.Void Mono.Security.BitConverterLE::UIntFromBytes(System.Byte*,System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void BitConverterLE_UIntFromBytes_m91E16C3362E794444D849A4AD5B9F746BF2A4FCB (uint8_t* ___dst0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___src1, int32_t ___startIndex2, const RuntimeMethod* method);
// System.Void System.Security.Cryptography.HashAlgorithm::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HashAlgorithm__ctor_mC0C923AB8904FC11889F63B39177507FBB35AA43 (HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * __this, const RuntimeMethod* method);
// System.Void Mono.Security.Cryptography.MD4Managed::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MD4Managed__ctor_mDB4352DBB43112E1DF337DF241A3E25435C2C3C5 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, const RuntimeMethod* method);
// System.Void Mono.Security.Cryptography.MD4::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MD4__ctor_m6AD2F7EC5132898B34FA52CB4EAB9A82F0FEFF59 (MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * __this, const RuntimeMethod* method);
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E (RuntimeArray * p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void System.Buffer::BlockCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353 (RuntimeArray * p0, int32_t p1, RuntimeArray * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
// System.Void Mono.Security.Cryptography.MD4Managed::MD4Transform(System.UInt32[],System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_MD4Transform_m385B46D14814EB12B82642AC995E26FB99B6816C (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___state0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___block1, int32_t ___index2, const RuntimeMethod* method);
// System.Void Mono.Security.Cryptography.MD4Managed::Encode(System.Byte[],System.UInt32[])
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_Encode_m4EAEC7D200AF9F9BBB8D4AAF0ABD4B5E9D9C4AF2 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___output0, UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___input1, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Cryptography.MD4Managed::Padding(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* MD4Managed_Padding_m7AD58C8D8178AC6CD6738C93A146312D73ED0476 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, int32_t ___nLength0, const RuntimeMethod* method);
// System.UInt32 Mono.Security.Cryptography.MD4Managed::F(System.UInt32,System.UInt32,System.UInt32)
extern "C" IL2CPP_METHOD_ATTR uint32_t MD4Managed_F_mB457E78CC0BDBD1ADCD8E687A6891E0D6B454174 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method);
// System.UInt32 Mono.Security.Cryptography.MD4Managed::ROL(System.UInt32,System.Byte)
extern "C" IL2CPP_METHOD_ATTR uint32_t MD4Managed_ROL_mAD186591B4745F5050E7B34A5805A7476448D7F0 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t ___x0, uint8_t ___n1, const RuntimeMethod* method);
// System.UInt32 Mono.Security.Cryptography.MD4Managed::G(System.UInt32,System.UInt32,System.UInt32)
extern "C" IL2CPP_METHOD_ATTR uint32_t MD4Managed_G_mCF22020BF90E65B0E322FAE8962502BBC22A157F (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method);
// System.UInt32 Mono.Security.Cryptography.MD4Managed::H(System.UInt32,System.UInt32,System.UInt32)
extern "C" IL2CPP_METHOD_ATTR uint32_t MD4Managed_H_mDF220E77C694540EF16BD892D7F03B34AE055225 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method);
// System.Void Mono.Security.Cryptography.MD4Managed::Decode(System.UInt32[],System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_Decode_m450A7CE51E89191B352AC0DD4527135B343443D6 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___output0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___input1, int32_t ___index2, const RuntimeMethod* method);
// System.Void Mono.Security.Cryptography.MD4Managed::FF(System.UInt32&,System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Byte)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t* ___a0, uint32_t ___b1, uint32_t ___c2, uint32_t ___d3, uint32_t ___x4, uint8_t ___s5, const RuntimeMethod* method);
// System.Void Mono.Security.Cryptography.MD4Managed::GG(System.UInt32&,System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Byte)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t* ___a0, uint32_t ___b1, uint32_t ___c2, uint32_t ___d3, uint32_t ___x4, uint8_t ___s5, const RuntimeMethod* method);
// System.Void Mono.Security.Cryptography.MD4Managed::HH(System.UInt32&,System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Byte)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t* ___a0, uint32_t ___b1, uint32_t ___c2, uint32_t ___d3, uint32_t ___x4, uint8_t ___s5, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Mono.Security.Interface.MonoTlsSettings::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoTlsSettings__ctor_m09A7B7F0B1CA2775C8A24B8A6BEC032E6B1C0A72 (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * __this, const RuntimeMethod* method);
// Mono.Security.Interface.MonoTlsSettings Mono.Security.Interface.MonoTlsSettings::get_DefaultSettings()
extern "C" IL2CPP_METHOD_ATTR MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * MonoTlsSettings_get_DefaultSettings_m49A7CECC7D687F62790DD374D560278D3916B887 (const RuntimeMethod* method);
// Mono.Security.Interface.MonoTlsSettings Mono.Security.Interface.MonoTlsSettings::Clone()
extern "C" IL2CPP_METHOD_ATTR MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * MonoTlsSettings_Clone_mF28F7F627B12CBD0BD1ABD6F35DD0B4BAD2E3840 (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * __this, const RuntimeMethod* method);
// System.Void Mono.Security.Interface.MonoTlsSettings::.ctor(Mono.Security.Interface.MonoTlsSettings)
extern "C" IL2CPP_METHOD_ATTR void MonoTlsSettings__ctor_m3D336E73C9393401BE9BE856B7204BA176F52B17 (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * __this, MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * ___other0, const RuntimeMethod* method);
// System.Void System.Security.Cryptography.X509Certificates.X509CertificateCollection::.ctor(System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern "C" IL2CPP_METHOD_ATTR void X509CertificateCollection__ctor_m24BAA500237577256DCA1BD814ED88D163E702E3 (X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * __this, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * p0, const RuntimeMethod* method);
// System.Void System.Array::CopyTo(System.Array,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Array_CopyTo_m455300D414FFB0EBFE53EA4E8BBD31532006EBB7 (RuntimeArray * __this, RuntimeArray * p0, int32_t p1, const RuntimeMethod* method);
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse__ctor_m1E0300839CAF582A720DB0F4F9E425B6EE12B258 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, const RuntimeMethod* method);
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::set_Password(System.String)
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse_set_Password_m530EB94179C374BED9B9AAE4BB30AB3FF14F07E2 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::set_Challenge(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse_set_Challenge_mD747C1A002528A6E9AFDE848AA257FD7B1B85E6F (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___value0, const RuntimeMethod* method);
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::Dispose()
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse_Dispose_mD6C08D1EDA541DC5A9B287744FB18E3149627434 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, const RuntimeMethod* method);
// System.Void System.Object::Finalize()
extern "C" IL2CPP_METHOD_ATTR void Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ObjectDisposedException__ctor_m8B5D23EA08E42BDE6BC5233CC666295F19BBD2F9 (ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A * __this, String_t* p0, const RuntimeMethod* method);
// System.Security.Cryptography.DES System.Security.Cryptography.DES::Create()
extern "C" IL2CPP_METHOD_ATTR DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * DES_Create_m5EE267FBCD5AA18E04C29247C796430D12247CC5 (const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::PasswordToKey(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse_PasswordToKey_m522B84CA0312284486A2C4E10FEE2D74BF4FF163 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, String_t* ___password0, int32_t ___position1, const RuntimeMethod* method);
// Mono.Security.Cryptography.MD4 Mono.Security.Cryptography.MD4::Create()
extern "C" IL2CPP_METHOD_ATTR MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * MD4_Create_m2D436A4CC284704A7DA0EEF4C4D5860F69D0BB93 (const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_Unicode()
extern "C" IL2CPP_METHOD_ATTR Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * Encoding_get_Unicode_m86CC470F70F9BB52DDB26721F0C0D6EDAFC318AA (const RuntimeMethod* method);
// System.Byte[] System.Security.Cryptography.HashAlgorithm::ComputeHash(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* HashAlgorithm_ComputeHash_m18501D3068AEBEB5FA83EA72BE780E371DB0C122 (HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* p0, const RuntimeMethod* method);
// System.Void System.Security.Cryptography.SymmetricAlgorithm::Clear()
extern "C" IL2CPP_METHOD_ATTR void SymmetricAlgorithm_Clear_m8487379B135918E72684597CFE388EF7FCA733D2 (SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789 * __this, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* p0, const RuntimeMethod* method);
// System.Object System.Array::Clone()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Array_Clone_mE8C710213E323617A6F46F2B36DCDDD4C7CF5176 (RuntimeArray * __this, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::GetResponse(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse_GetResponse_m526E49021AB29DD12995CF8BB12DC9F03F2A583F (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pwd0, const RuntimeMethod* method);
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::Dispose(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse_Dispose_mF3B015B967C89BE1E139EFA40740208B10441AED (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, bool ___disposing0, const RuntimeMethod* method);
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C" IL2CPP_METHOD_ATTR void GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::PrepareDESKey(System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse_PrepareDESKey_m32B2174E0B63E959CE08204F3C39AAA01799A3B3 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key56bits0, int32_t ___position1, const RuntimeMethod* method);
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Math_Min_mC950438198519FB2B0260FCB91220847EE4BB525 (int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_ASCII()
extern "C" IL2CPP_METHOD_ATTR Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * Encoding_get_ASCII_m9B673AE3152AB04D07CADE6E5E142C785B5BC94E (const RuntimeMethod* method);
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_CurrentCulture()
extern "C" IL2CPP_METHOD_ATTR CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * CultureInfo_get_CurrentCulture_mD86F3D8E5D332FB304F80D9B9CA4DE849C2A6831 (const RuntimeMethod* method);
// System.String System.String::ToUpper(System.Globalization.CultureInfo)
extern "C" IL2CPP_METHOD_ATTR String_t* String_ToUpper_m8C69D974350ABA8BA0BC3A66996004CCEFD64293 (String_t* __this, CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * p0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C" IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A (RuntimeArray * p0, RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  p1, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::PasswordToKey(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_PasswordToKey_m2871E605818DF2DE4BC5B1B163831BA8F64006D8 (String_t* ___password0, int32_t ___position1, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::GetResponse(System.Byte[],System.Byte[])
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_GetResponse_mE39699CD2453921E373BF9768180993EDCD810E7 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___challenge0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pwd1, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute_NTLM_Password(System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_Compute_NTLM_Password_mD27D4A18DBD712B0E80B0F2A1CB296B353312C41 (String_t* ___password0, const RuntimeMethod* method);
// System.Security.Cryptography.RandomNumberGenerator System.Security.Cryptography.RandomNumberGenerator::Create()
extern "C" IL2CPP_METHOD_ATTR RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * RandomNumberGenerator_Create_mB84B1BA538B29D0679F310D3B574A7D5BAA890C4 (const RuntimeMethod* method);
// System.Security.Cryptography.MD5 System.Security.Cryptography.MD5::Create()
extern "C" IL2CPP_METHOD_ATTR MD5_tCED753745572EC20FE5D31D15F132736B5343EE6 * MD5_Create_m87EB14601AD6AF168032C29DA938E18454CA05AE (const RuntimeMethod* method);
// System.Void System.Array::Copy(System.Array,System.Array,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Array_Copy_m2D96731C600DE8A167348CA8BA796344E64F7434 (RuntimeArray * p0, RuntimeArray * p1, int32_t p2, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute_NTLM(System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_Compute_NTLM_mA1DCA878A3A7A5517DB8BB0F2BEDD29353573976 (String_t* ___password0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___challenge1, const RuntimeMethod* method);
// System.String System.String::ToUpperInvariant()
extern "C" IL2CPP_METHOD_ATTR String_t* String_ToUpperInvariant_m0AA42416F4CACA4D0E3B89D97E534D88AB136338 (String_t* __this, const RuntimeMethod* method);
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6 (RuntimeArray * p0, int32_t p1, RuntimeArray * p2, int32_t p3, int32_t p4, const RuntimeMethod* method);
// System.Void System.Security.Cryptography.HMACMD5::.ctor(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void HMACMD5__ctor_m246E639FCF66A7C0A443CF06F461A3C5C26EB6F9 (HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* p0, const RuntimeMethod* method);
// System.Void System.Security.Cryptography.HashAlgorithm::Clear()
extern "C" IL2CPP_METHOD_ATTR void HashAlgorithm_Clear_m2E975EB7B42C1E241B24578CDF15AD41F35A5A8D (HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * __this, const RuntimeMethod* method);
// System.DateTime System.DateTime::get_Now()
extern "C" IL2CPP_METHOD_ATTR DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  DateTime_get_Now_mB464D30F15C97069F92C1F910DCDDC3DFCC7F7D2 (const RuntimeMethod* method);
// System.Int64 System.DateTime::get_Ticks()
extern "C" IL2CPP_METHOD_ATTR int64_t DateTime_get_Ticks_mBCB529E43D065E498EAF08971D2EB49D5CB59D60 (DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * __this, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::get_TargetInfo()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Type2Message_get_TargetInfo_m5E0F0E5A6B32B7512393EDC2DFE9E8A6D79DB485 (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * __this, const RuntimeMethod* method);
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.Int64)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* BitConverterLE_GetBytes_m8B2BFEBCB4830C010E4572C925AE3C3A3CC14031 (int64_t ___value0, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::get_Nonce()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1 (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * __this, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute_LM(System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_Compute_LM_m3A1F9371E4F1E41B044787FFB3BE985E92A3AC22 (String_t* ___password0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___challenge1, const RuntimeMethod* method);
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute_NTLMv2_Session(System.String,System.Byte[],System.Byte[]&,System.Byte[]&)
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse2_Compute_NTLMv2_Session_mFB6537BF7FC8D9D7CCE55BFCDF6A7C371460E296 (String_t* ___password0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___challenge1, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** ___lm2, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** ___ntlm3, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute_NTLMv2(Mono.Security.Protocol.Ntlm.Type2Message,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_Compute_NTLMv2_mBCFF2DF7375AD035B98FC84253201D269201CE43 (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * ___type20, String_t* ___username1, String_t* ___password2, String_t* ___domain3, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * __this, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::PrepareDESKey(System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_PrepareDESKey_m428EF8F37B18E0B4FC5895BFF02A681740CF7608 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key56bits0, int32_t ___position1, const RuntimeMethod* method);
// System.String Locale::GetText(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* Locale_GetText_m67B66557188C94648AA7A23F6A7501BE7D455ADA (String_t* ___msg0, const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.Object,System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m755B01B4B4595B447596E3281F22FD7CE6DAE378 (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * __this, String_t* p0, RuntimeObject * p1, String_t* p2, const RuntimeMethod* method);
// System.Boolean Mono.Security.Protocol.Ntlm.MessageBase::CheckHeader(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR bool MessageBase_CheckHeader_m427014E264FA451B68062CFF8A1939DC3A04FB5A (MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___message0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA (String_t* p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentException__ctor_m26DC3463C6F3C98BF33EA39598DD2B32F0249CA8 (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.UInt32 Mono.Security.BitConverterLE::ToUInt32(System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR uint32_t BitConverterLE_ToUInt32_m898E287439DDB0A31E4E39FF83DD032B1C43FA26 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___value0, int32_t ___startIndex1, const RuntimeMethod* method);
// System.Void Mono.Security.Protocol.Ntlm.MessageBase::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MessageBase__ctor_m32BA1280CB1020E88C6EF6DFA9C3ABABF3B36E59 (MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 * __this, int32_t ___messageType0, const RuntimeMethod* method);
// System.String System.Environment::get_UserDomainName()
extern "C" IL2CPP_METHOD_ATTR String_t* Environment_get_UserDomainName_mC55D253D7319CBE9030836E420FF9518921C3A52 (const RuntimeMethod* method);
// System.String System.Environment::get_MachineName()
extern "C" IL2CPP_METHOD_ATTR String_t* Environment_get_MachineName_m0300D26C1A5348D90800793717D33B1F629AF10D (const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void Mono.Security.Protocol.Ntlm.MessageBase::Decode(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537 (MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___message0, const RuntimeMethod* method);
// System.UInt16 Mono.Security.BitConverterLE::ToUInt16(System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR uint16_t BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___value0, int32_t ___startIndex1, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.MessageBase::PrepareMessage(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* MessageBase_PrepareMessage_m6B0C0C463C16D086924EC49DB07C3ADE95C11958 (MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 * __this, int32_t ___messageSize0, const RuntimeMethod* method);
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C" IL2CPP_METHOD_ATTR CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72 (const RuntimeMethod* method);
// System.String System.Environment::get_UserName()
extern "C" IL2CPP_METHOD_ATTR String_t* Environment_get_UserName_m089F0073984A6341BC214F4EB3DE42ACD19D067B (const RuntimeMethod* method);
// System.String Mono.Security.Protocol.Ntlm.Type3Message::DecodeString(System.Byte[],System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* Type3Message_DecodeString_mCB1797B1FA97CD96E788DCE096FDBC275685C42F (Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer0, int32_t ___offset1, int32_t ___len2, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::EncodeString(System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Type3Message_EncodeString_m431F1D808D738A2C9CE57DE1084F9A42C036AA5A (Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C * __this, String_t* ___text0, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706 (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::.ctor(System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse__ctor_mF80EAE315F35264F1DA0167B3ED7A8CD8E2D1FFA (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, String_t* ___password0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___challenge1, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::get_LM()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse_get_LM_m3916048E028CFCA867E801A83FEB949F7C089263 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, const RuntimeMethod* method);
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::get_NT()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse_get_NT_mEC9F2FDFDB8FADF415D4BA8A1564A23024FC3437 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, const RuntimeMethod* method);
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute(Mono.Security.Protocol.Ntlm.Type2Message,Mono.Security.Protocol.Ntlm.NtlmAuthLevel,System.String,System.String,System.String,System.Byte[]&,System.Byte[]&)
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse2_Compute_mAA312CA925226C75A829516B6BDC2089840D389D (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * ___type20, int32_t ___level1, String_t* ___username2, String_t* ___password3, String_t* ___domain4, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** ___lm5, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** ___ntlm6, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Locale::GetText(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* Locale_GetText_m67B66557188C94648AA7A23F6A7501BE7D455ADA (String_t* ___msg0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___msg0;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] Mono.Security.BitConverterLE::GetULongBytes(System.Byte*)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* BitConverterLE_GetULongBytes_mCBAC987169706D10F05AFF42559A4EA1D88E1876 (uint8_t* ___bytes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BitConverterLE_GetULongBytes_mCBAC987169706D10F05AFF42559A4EA1D88E1876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_il2cpp_TypeInfo_var);
		bool L_0 = ((BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
		if (!L_0)
		{
			goto IL_0044;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = L_1;
		uint8_t* L_3 = ___bytes0;
		int32_t L_4 = *((uint8_t*)L_3);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_4);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = L_2;
		uint8_t* L_6 = ___bytes0;
		int32_t L_7 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_6, (int32_t)1)));
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)L_7);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = L_5;
		uint8_t* L_9 = ___bytes0;
		int32_t L_10 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_9, (int32_t)2)));
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)L_10);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = L_8;
		uint8_t* L_12 = ___bytes0;
		int32_t L_13 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_12, (int32_t)3)));
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)L_13);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = L_11;
		uint8_t* L_15 = ___bytes0;
		int32_t L_16 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_15, (int32_t)4)));
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)L_16);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_17 = L_14;
		uint8_t* L_18 = ___bytes0;
		int32_t L_19 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_18, (int32_t)5)));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)L_19);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = L_17;
		uint8_t* L_21 = ___bytes0;
		int32_t L_22 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_21, (int32_t)6)));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)L_22);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = L_20;
		uint8_t* L_24 = ___bytes0;
		int32_t L_25 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_24, (int32_t)7)));
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)L_25);
		return L_23;
	}

IL_0044:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_26 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_27 = L_26;
		uint8_t* L_28 = ___bytes0;
		int32_t L_29 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_28, (int32_t)7)));
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_29);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_30 = L_27;
		uint8_t* L_31 = ___bytes0;
		int32_t L_32 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_31, (int32_t)6)));
		NullCheck(L_30);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)L_32);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_33 = L_30;
		uint8_t* L_34 = ___bytes0;
		int32_t L_35 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_34, (int32_t)5)));
		NullCheck(L_33);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)L_35);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_36 = L_33;
		uint8_t* L_37 = ___bytes0;
		int32_t L_38 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_37, (int32_t)4)));
		NullCheck(L_36);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)L_38);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_39 = L_36;
		uint8_t* L_40 = ___bytes0;
		int32_t L_41 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_40, (int32_t)3)));
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)L_41);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_42 = L_39;
		uint8_t* L_43 = ___bytes0;
		int32_t L_44 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_43, (int32_t)2)));
		NullCheck(L_42);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)L_44);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_45 = L_42;
		uint8_t* L_46 = ___bytes0;
		int32_t L_47 = *((uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_46, (int32_t)1)));
		NullCheck(L_45);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)L_47);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_48 = L_45;
		uint8_t* L_49 = ___bytes0;
		int32_t L_50 = *((uint8_t*)L_49);
		NullCheck(L_48);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)L_50);
		return L_48;
	}
}
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.Int64)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* BitConverterLE_GetBytes_m8B2BFEBCB4830C010E4572C925AE3C3A3CC14031 (int64_t ___value0, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = BitConverterLE_GetULongBytes_mCBAC987169706D10F05AFF42559A4EA1D88E1876((uint8_t*)(uint8_t*)(((uintptr_t)(&___value0))), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Mono.Security.BitConverterLE::UShortFromBytes(System.Byte*,System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void BitConverterLE_UShortFromBytes_mBC051D16FFC95E9695F110AFDEAB018BD76F84A9 (uint8_t* ___dst0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___src1, int32_t ___startIndex2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BitConverterLE_UShortFromBytes_mBC051D16FFC95E9695F110AFDEAB018BD76F84A9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_il2cpp_TypeInfo_var);
		bool L_0 = ((BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint8_t* L_1 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___src1;
		int32_t L_3 = ___startIndex2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		*((int8_t*)L_1) = (int8_t)L_5;
		uint8_t* L_6 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ___src1;
		int32_t L_8 = ___startIndex2;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		uint8_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		*((int8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_6, (int32_t)1))) = (int8_t)L_10;
		return;
	}

IL_0016:
	{
		uint8_t* L_11 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = ___src1;
		int32_t L_13 = ___startIndex2;
		NullCheck(L_12);
		int32_t L_14 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		uint8_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		*((int8_t*)L_11) = (int8_t)L_15;
		uint8_t* L_16 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_17 = ___src1;
		int32_t L_18 = ___startIndex2;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		uint8_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		*((int8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_16, (int32_t)1))) = (int8_t)L_20;
		return;
	}
}
// System.Void Mono.Security.BitConverterLE::UIntFromBytes(System.Byte*,System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void BitConverterLE_UIntFromBytes_m91E16C3362E794444D849A4AD5B9F746BF2A4FCB (uint8_t* ___dst0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___src1, int32_t ___startIndex2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BitConverterLE_UIntFromBytes_m91E16C3362E794444D849A4AD5B9F746BF2A4FCB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_il2cpp_TypeInfo_var);
		bool L_0 = ((BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_il2cpp_TypeInfo_var))->get_IsLittleEndian_0();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		uint8_t* L_1 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___src1;
		int32_t L_3 = ___startIndex2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		*((int8_t*)L_1) = (int8_t)L_5;
		uint8_t* L_6 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ___src1;
		int32_t L_8 = ___startIndex2;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		uint8_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		*((int8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_6, (int32_t)1))) = (int8_t)L_10;
		uint8_t* L_11 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = ___src1;
		int32_t L_13 = ___startIndex2;
		NullCheck(L_12);
		int32_t L_14 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)2));
		uint8_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		*((int8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_11, (int32_t)2))) = (int8_t)L_15;
		uint8_t* L_16 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_17 = ___src1;
		int32_t L_18 = ___startIndex2;
		NullCheck(L_17);
		int32_t L_19 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)3));
		uint8_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		*((int8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_16, (int32_t)3))) = (int8_t)L_20;
		return;
	}

IL_0028:
	{
		uint8_t* L_21 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_22 = ___src1;
		int32_t L_23 = ___startIndex2;
		NullCheck(L_22);
		int32_t L_24 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)3));
		uint8_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		*((int8_t*)L_21) = (int8_t)L_25;
		uint8_t* L_26 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_27 = ___src1;
		int32_t L_28 = ___startIndex2;
		NullCheck(L_27);
		int32_t L_29 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)2));
		uint8_t L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		*((int8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_26, (int32_t)1))) = (int8_t)L_30;
		uint8_t* L_31 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_32 = ___src1;
		int32_t L_33 = ___startIndex2;
		NullCheck(L_32);
		int32_t L_34 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
		uint8_t L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		*((int8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_31, (int32_t)2))) = (int8_t)L_35;
		uint8_t* L_36 = ___dst0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_37 = ___src1;
		int32_t L_38 = ___startIndex2;
		NullCheck(L_37);
		int32_t L_39 = L_38;
		uint8_t L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		*((int8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_36, (int32_t)3))) = (int8_t)L_40;
		return;
	}
}
// System.UInt16 Mono.Security.BitConverterLE::ToUInt16(System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR uint16_t BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___value0, int32_t ___startIndex1, const RuntimeMethod* method)
{
	uint16_t V_0 = 0;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___value0;
		int32_t L_1 = ___startIndex1;
		BitConverterLE_UShortFromBytes_mBC051D16FFC95E9695F110AFDEAB018BD76F84A9((uint8_t*)(uint8_t*)(((uintptr_t)(&V_0))), L_0, L_1, /*hidden argument*/NULL);
		uint16_t L_2 = V_0;
		return L_2;
	}
}
// System.UInt32 Mono.Security.BitConverterLE::ToUInt32(System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR uint32_t BitConverterLE_ToUInt32_m898E287439DDB0A31E4E39FF83DD032B1C43FA26 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___value0, int32_t ___startIndex1, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___value0;
		int32_t L_1 = ___startIndex1;
		BitConverterLE_UIntFromBytes_m91E16C3362E794444D849A4AD5B9F746BF2A4FCB((uint8_t*)(uint8_t*)(((uintptr_t)(&V_0))), L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Cryptography.MD4::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MD4__ctor_m6AD2F7EC5132898B34FA52CB4EAB9A82F0FEFF59 (MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * __this, const RuntimeMethod* method)
{
	{
		HashAlgorithm__ctor_mC0C923AB8904FC11889F63B39177507FBB35AA43(__this, /*hidden argument*/NULL);
		((HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA *)__this)->set_HashSizeValue_0(((int32_t)128));
		return;
	}
}
// Mono.Security.Cryptography.MD4 Mono.Security.Cryptography.MD4::Create()
extern "C" IL2CPP_METHOD_ATTR MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * MD4_Create_m2D436A4CC284704A7DA0EEF4C4D5860F69D0BB93 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MD4_Create_m2D436A4CC284704A7DA0EEF4C4D5860F69D0BB93_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * L_0 = (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 *)il2cpp_codegen_object_new(MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1_il2cpp_TypeInfo_var);
		MD4Managed__ctor_mDB4352DBB43112E1DF337DF241A3E25435C2C3C5(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Cryptography.MD4Managed::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MD4Managed__ctor_mDB4352DBB43112E1DF337DF241A3E25435C2C3C5 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MD4Managed__ctor_mDB4352DBB43112E1DF337DF241A3E25435C2C3C5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MD4__ctor_m6AD2F7EC5132898B34FA52CB4EAB9A82F0FEFF59(__this, /*hidden argument*/NULL);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_0 = (UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB*)SZArrayNew(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB_il2cpp_TypeInfo_var, (uint32_t)4);
		__this->set_state_4(L_0);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_1 = (UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB*)SZArrayNew(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB_il2cpp_TypeInfo_var, (uint32_t)2);
		__this->set_count_6(L_1);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)64));
		__this->set_buffer_5(L_2);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		__this->set_digest_8(L_3);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_4 = (UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB*)SZArrayNew(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		__this->set_x_7(L_4);
		VirtActionInvoker0::Invoke(15 /* System.Void System.Security.Cryptography.HashAlgorithm::Initialize() */, __this);
		return;
	}
}
// System.Void Mono.Security.Cryptography.MD4Managed::Initialize()
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_Initialize_m40CDDA70F2946B6B942EB66BE77D5AC25AEC56EB (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, const RuntimeMethod* method)
{
	{
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_0 = __this->get_count_6();
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint32_t)0);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_1 = __this->get_count_6();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint32_t)0);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_2 = __this->get_state_4();
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint32_t)((int32_t)1732584193));
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_3 = __this->get_state_4();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint32_t)((int32_t)-271733879));
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_4 = __this->get_state_4();
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint32_t)((int32_t)-1732584194));
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_5 = __this->get_state_4();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint32_t)((int32_t)271733878));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = __this->get_buffer_5();
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_6, 0, ((int32_t)64), /*hidden argument*/NULL);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_7 = __this->get_x_7();
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_7, 0, ((int32_t)16), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.Cryptography.MD4Managed::HashCore(System.Byte[],System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_HashCore_mB3D2B92E80F6D0D1EEE5F41F2DB21F7A88DC41AB (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___array0, int32_t ___ibStart1, int32_t ___cbSize2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_0 = __this->get_count_6();
		NullCheck(L_0);
		int32_t L_1 = 0;
		uint32_t L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		V_0 = ((int32_t)((int32_t)((int32_t)((uint32_t)L_2>>3))&(int32_t)((int32_t)63)));
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_3 = __this->get_count_6();
		NullCheck(L_3);
		uint32_t* L_4 = ((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)));
		int32_t L_5 = *((uint32_t*)L_4);
		int32_t L_6 = ___cbSize2;
		*((int32_t*)L_4) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)((int32_t)((int32_t)L_6<<(int32_t)3))));
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_7 = __this->get_count_6();
		NullCheck(L_7);
		int32_t L_8 = 0;
		uint32_t L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		int32_t L_10 = ___cbSize2;
		if ((((int64_t)(((int64_t)((uint64_t)L_9)))) >= ((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)L_10<<(int32_t)3))))))))
		{
			goto IL_0041;
		}
	}
	{
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_11 = __this->get_count_6();
		NullCheck(L_11);
		uint32_t* L_12 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)));
		int32_t L_13 = *((uint32_t*)L_12);
		*((int32_t*)L_12) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0041:
	{
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_14 = __this->get_count_6();
		NullCheck(L_14);
		uint32_t* L_15 = ((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)));
		int32_t L_16 = *((uint32_t*)L_15);
		int32_t L_17 = ___cbSize2;
		*((int32_t*)L_15) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)((int32_t)((int32_t)L_17>>(int32_t)((int32_t)29)))));
		int32_t L_18 = V_0;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)64), (int32_t)L_18));
		V_2 = 0;
		int32_t L_19 = ___cbSize2;
		int32_t L_20 = V_1;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_00a4;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_21 = ___array0;
		int32_t L_22 = ___ibStart1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = __this->get_buffer_5();
		int32_t L_24 = V_0;
		int32_t L_25 = V_1;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_21, L_22, (RuntimeArray *)(RuntimeArray *)L_23, L_24, L_25, /*hidden argument*/NULL);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_26 = __this->get_state_4();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_27 = __this->get_buffer_5();
		MD4Managed_MD4Transform_m385B46D14814EB12B82642AC995E26FB99B6816C(__this, L_26, L_27, 0, /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_2 = L_28;
		goto IL_009b;
	}

IL_0086:
	{
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_29 = __this->get_state_4();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_30 = ___array0;
		int32_t L_31 = ___ibStart1;
		int32_t L_32 = V_2;
		MD4Managed_MD4Transform_m385B46D14814EB12B82642AC995E26FB99B6816C(__this, L_29, L_30, ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)L_32)), /*hidden argument*/NULL);
		int32_t L_33 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)((int32_t)64)));
	}

IL_009b:
	{
		int32_t L_34 = V_2;
		int32_t L_35 = ___cbSize2;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)((int32_t)63)))) < ((int32_t)L_35)))
		{
			goto IL_0086;
		}
	}
	{
		V_0 = 0;
	}

IL_00a4:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_36 = ___array0;
		int32_t L_37 = ___ibStart1;
		int32_t L_38 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_39 = __this->get_buffer_5();
		int32_t L_40 = V_0;
		int32_t L_41 = ___cbSize2;
		int32_t L_42 = V_2;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_36, ((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)L_38)), (RuntimeArray *)(RuntimeArray *)L_39, L_40, ((int32_t)il2cpp_codegen_subtract((int32_t)L_41, (int32_t)L_42)), /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] Mono.Security.Cryptography.MD4Managed::HashFinal()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* MD4Managed_HashFinal_m4853C50C4026A1F75D7C75FB28FD4E146E95F6AC (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MD4Managed_HashFinal_m4853C50C4026A1F75D7C75FB28FD4E146E95F6AC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	uint32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		V_0 = L_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_2 = __this->get_count_6();
		MD4Managed_Encode_m4EAEC7D200AF9F9BBB8D4AAF0ABD4B5E9D9C4AF2(__this, L_1, L_2, /*hidden argument*/NULL);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_3 = __this->get_count_6();
		NullCheck(L_3);
		int32_t L_4 = 0;
		uint32_t L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = ((int32_t)((int32_t)((int32_t)((uint32_t)L_5>>3))&(int32_t)((int32_t)63)));
		uint32_t L_6 = V_1;
		if ((!(((uint32_t)L_6) >= ((uint32_t)((int32_t)56)))))
		{
			goto IL_002d;
		}
	}
	{
		uint32_t L_7 = V_1;
		G_B3_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)120), (int32_t)L_7));
		goto IL_0031;
	}

IL_002d:
	{
		uint32_t L_8 = V_1;
		G_B3_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)56), (int32_t)L_8));
	}

IL_0031:
	{
		V_2 = G_B3_0;
		int32_t L_9 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = MD4Managed_Padding_m7AD58C8D8178AC6CD6738C93A146312D73ED0476(__this, L_9, /*hidden argument*/NULL);
		int32_t L_11 = V_2;
		VirtActionInvoker3< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(16 /* System.Void System.Security.Cryptography.HashAlgorithm::HashCore(System.Byte[],System.Int32,System.Int32) */, __this, L_10, 0, L_11);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = V_0;
		VirtActionInvoker3< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(16 /* System.Void System.Security.Cryptography.HashAlgorithm::HashCore(System.Byte[],System.Int32,System.Int32) */, __this, L_12, 0, 8);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = __this->get_digest_8();
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_14 = __this->get_state_4();
		MD4Managed_Encode_m4EAEC7D200AF9F9BBB8D4AAF0ABD4B5E9D9C4AF2(__this, L_13, L_14, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(15 /* System.Void System.Security.Cryptography.HashAlgorithm::Initialize() */, __this);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = __this->get_digest_8();
		return L_15;
	}
}
// System.Byte[] Mono.Security.Cryptography.MD4Managed::Padding(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* MD4Managed_Padding_m7AD58C8D8178AC6CD6738C93A146312D73ED0476 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, int32_t ___nLength0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MD4Managed_Padding_m7AD58C8D8178AC6CD6738C93A146312D73ED0476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___nLength0;
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___nLength0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_1);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = L_2;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)((int32_t)128));
		return L_3;
	}

IL_0013:
	{
		return (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)NULL;
	}
}
// System.UInt32 Mono.Security.Cryptography.MD4Managed::F(System.UInt32,System.UInt32,System.UInt32)
extern "C" IL2CPP_METHOD_ATTR uint32_t MD4Managed_F_mB457E78CC0BDBD1ADCD8E687A6891E0D6B454174 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___x0;
		uint32_t L_3 = ___z2;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0&(int32_t)L_1))|(int32_t)((int32_t)((int32_t)((~L_2))&(int32_t)L_3))));
	}
}
// System.UInt32 Mono.Security.Cryptography.MD4Managed::G(System.UInt32,System.UInt32,System.UInt32)
extern "C" IL2CPP_METHOD_ATTR uint32_t MD4Managed_G_mCF22020BF90E65B0E322FAE8962502BBC22A157F (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___x0;
		uint32_t L_3 = ___z2;
		uint32_t L_4 = ___y1;
		uint32_t L_5 = ___z2;
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0&(int32_t)L_1))|(int32_t)((int32_t)((int32_t)L_2&(int32_t)L_3))))|(int32_t)((int32_t)((int32_t)L_4&(int32_t)L_5))));
	}
}
// System.UInt32 Mono.Security.Cryptography.MD4Managed::H(System.UInt32,System.UInt32,System.UInt32)
extern "C" IL2CPP_METHOD_ATTR uint32_t MD4Managed_H_mDF220E77C694540EF16BD892D7F03B34AE055225 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___z2;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_1))^(int32_t)L_2));
	}
}
// System.UInt32 Mono.Security.Cryptography.MD4Managed::ROL(System.UInt32,System.Byte)
extern "C" IL2CPP_METHOD_ATTR uint32_t MD4Managed_ROL_mAD186591B4745F5050E7B34A5805A7476448D7F0 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t ___x0, uint8_t ___n1, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___x0;
		uint8_t L_1 = ___n1;
		uint32_t L_2 = ___x0;
		uint8_t L_3 = ___n1;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0<<(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))))|(int32_t)((int32_t)((uint32_t)L_2>>((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)32), (int32_t)L_3))&(int32_t)((int32_t)31)))))));
	}
}
// System.Void Mono.Security.Cryptography.MD4Managed::FF(System.UInt32U26,System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Byte)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t* ___a0, uint32_t ___b1, uint32_t ___c2, uint32_t ___d3, uint32_t ___x4, uint8_t ___s5, const RuntimeMethod* method)
{
	{
		uint32_t* L_0 = ___a0;
		uint32_t* L_1 = ___a0;
		int32_t L_2 = *((uint32_t*)L_1);
		uint32_t L_3 = ___b1;
		uint32_t L_4 = ___c2;
		uint32_t L_5 = ___d3;
		uint32_t L_6 = MD4Managed_F_mB457E78CC0BDBD1ADCD8E687A6891E0D6B454174(__this, L_3, L_4, L_5, /*hidden argument*/NULL);
		uint32_t L_7 = ___x4;
		*((int32_t*)L_0) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)L_7))));
		uint32_t* L_8 = ___a0;
		uint32_t* L_9 = ___a0;
		int32_t L_10 = *((uint32_t*)L_9);
		uint8_t L_11 = ___s5;
		uint32_t L_12 = MD4Managed_ROL_mAD186591B4745F5050E7B34A5805A7476448D7F0(__this, L_10, L_11, /*hidden argument*/NULL);
		*((int32_t*)L_8) = (int32_t)L_12;
		return;
	}
}
// System.Void Mono.Security.Cryptography.MD4Managed::GG(System.UInt32U26,System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Byte)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t* ___a0, uint32_t ___b1, uint32_t ___c2, uint32_t ___d3, uint32_t ___x4, uint8_t ___s5, const RuntimeMethod* method)
{
	{
		uint32_t* L_0 = ___a0;
		uint32_t* L_1 = ___a0;
		int32_t L_2 = *((uint32_t*)L_1);
		uint32_t L_3 = ___b1;
		uint32_t L_4 = ___c2;
		uint32_t L_5 = ___d3;
		uint32_t L_6 = MD4Managed_G_mCF22020BF90E65B0E322FAE8962502BBC22A157F(__this, L_3, L_4, L_5, /*hidden argument*/NULL);
		uint32_t L_7 = ___x4;
		*((int32_t*)L_0) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)L_7)), (int32_t)((int32_t)1518500249)))));
		uint32_t* L_8 = ___a0;
		uint32_t* L_9 = ___a0;
		int32_t L_10 = *((uint32_t*)L_9);
		uint8_t L_11 = ___s5;
		uint32_t L_12 = MD4Managed_ROL_mAD186591B4745F5050E7B34A5805A7476448D7F0(__this, L_10, L_11, /*hidden argument*/NULL);
		*((int32_t*)L_8) = (int32_t)L_12;
		return;
	}
}
// System.Void Mono.Security.Cryptography.MD4Managed::HH(System.UInt32U26,System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Byte)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, uint32_t* ___a0, uint32_t ___b1, uint32_t ___c2, uint32_t ___d3, uint32_t ___x4, uint8_t ___s5, const RuntimeMethod* method)
{
	{
		uint32_t* L_0 = ___a0;
		uint32_t* L_1 = ___a0;
		int32_t L_2 = *((uint32_t*)L_1);
		uint32_t L_3 = ___b1;
		uint32_t L_4 = ___c2;
		uint32_t L_5 = ___d3;
		uint32_t L_6 = MD4Managed_H_mDF220E77C694540EF16BD892D7F03B34AE055225(__this, L_3, L_4, L_5, /*hidden argument*/NULL);
		uint32_t L_7 = ___x4;
		*((int32_t*)L_0) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)L_7)), (int32_t)((int32_t)1859775393)))));
		uint32_t* L_8 = ___a0;
		uint32_t* L_9 = ___a0;
		int32_t L_10 = *((uint32_t*)L_9);
		uint8_t L_11 = ___s5;
		uint32_t L_12 = MD4Managed_ROL_mAD186591B4745F5050E7B34A5805A7476448D7F0(__this, L_10, L_11, /*hidden argument*/NULL);
		*((int32_t*)L_8) = (int32_t)L_12;
		return;
	}
}
// System.Void Mono.Security.Cryptography.MD4Managed::Encode(System.Byte[],System.UInt32[])
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_Encode_m4EAEC7D200AF9F9BBB8D4AAF0ABD4B5E9D9C4AF2 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___output0, UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___input1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_0038;
	}

IL_0006:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___output0;
		int32_t L_1 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_2 = ___input1;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (uint8_t)(((int32_t)((uint8_t)L_5))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = ___output0;
		int32_t L_7 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_8 = ___input1;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		uint32_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_11>>8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = ___output0;
		int32_t L_13 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_14 = ___input1;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		uint32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)2))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_17>>((int32_t)16)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_18 = ___output0;
		int32_t L_19 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_20 = ___input1;
		int32_t L_21 = V_0;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		uint32_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)3))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_23>>((int32_t)24)))))));
		int32_t L_24 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
		int32_t L_25 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)4));
	}

IL_0038:
	{
		int32_t L_26 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_27 = ___output0;
		NullCheck(L_27);
		if ((((int32_t)L_26) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_27)->max_length)))))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Mono.Security.Cryptography.MD4Managed::Decode(System.UInt32[],System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_Decode_m450A7CE51E89191B352AC0DD4527135B343443D6 (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___output0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___input1, int32_t ___index2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___index2;
		V_1 = L_0;
		goto IL_002e;
	}

IL_0006:
	{
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_1 = ___output0;
		int32_t L_2 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ___input1;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		uint8_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ___input1;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		uint8_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = ___input1;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)2));
		uint8_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = ___input1;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)3));
		uint8_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6|(int32_t)((int32_t)((int32_t)L_10<<(int32_t)8))))|(int32_t)((int32_t)((int32_t)L_14<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_18<<(int32_t)((int32_t)24))))));
		int32_t L_19 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
		int32_t L_20 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)4));
	}

IL_002e:
	{
		int32_t L_21 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_22 = ___output0;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length)))))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Mono.Security.Cryptography.MD4Managed::MD4Transform(System.UInt32[],System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MD4Managed_MD4Transform_m385B46D14814EB12B82642AC995E26FB99B6816C (MD4Managed_t1FC9A6CDB0A89A71416414689B3A0FF0283759D1 * __this, UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___state0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___block1, int32_t ___index2, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	{
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_0 = ___state0;
		NullCheck(L_0);
		int32_t L_1 = 0;
		uint32_t L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		V_0 = L_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_3 = ___state0;
		NullCheck(L_3);
		int32_t L_4 = 1;
		uint32_t L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_6 = ___state0;
		NullCheck(L_6);
		int32_t L_7 = 2;
		uint32_t L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_2 = L_8;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_9 = ___state0;
		NullCheck(L_9);
		int32_t L_10 = 3;
		uint32_t L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_3 = L_11;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_12 = __this->get_x_7();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = ___block1;
		int32_t L_14 = ___index2;
		MD4Managed_Decode_m450A7CE51E89191B352AC0DD4527135B343443D6(__this, L_12, L_13, L_14, /*hidden argument*/NULL);
		uint32_t L_15 = V_1;
		uint32_t L_16 = V_2;
		uint32_t L_17 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_18 = __this->get_x_7();
		NullCheck(L_18);
		int32_t L_19 = 0;
		uint32_t L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_0), L_15, L_16, L_17, L_20, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_21 = V_0;
		uint32_t L_22 = V_1;
		uint32_t L_23 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_24 = __this->get_x_7();
		NullCheck(L_24);
		int32_t L_25 = 1;
		uint32_t L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_3), L_21, L_22, L_23, L_26, (uint8_t)7, /*hidden argument*/NULL);
		uint32_t L_27 = V_3;
		uint32_t L_28 = V_0;
		uint32_t L_29 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_30 = __this->get_x_7();
		NullCheck(L_30);
		int32_t L_31 = 2;
		uint32_t L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_2), L_27, L_28, L_29, L_32, (uint8_t)((int32_t)11), /*hidden argument*/NULL);
		uint32_t L_33 = V_2;
		uint32_t L_34 = V_3;
		uint32_t L_35 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_36 = __this->get_x_7();
		NullCheck(L_36);
		int32_t L_37 = 3;
		uint32_t L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_1), L_33, L_34, L_35, L_38, (uint8_t)((int32_t)19), /*hidden argument*/NULL);
		uint32_t L_39 = V_1;
		uint32_t L_40 = V_2;
		uint32_t L_41 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_42 = __this->get_x_7();
		NullCheck(L_42);
		int32_t L_43 = 4;
		uint32_t L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_0), L_39, L_40, L_41, L_44, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_45 = V_0;
		uint32_t L_46 = V_1;
		uint32_t L_47 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_48 = __this->get_x_7();
		NullCheck(L_48);
		int32_t L_49 = 5;
		uint32_t L_50 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_3), L_45, L_46, L_47, L_50, (uint8_t)7, /*hidden argument*/NULL);
		uint32_t L_51 = V_3;
		uint32_t L_52 = V_0;
		uint32_t L_53 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_54 = __this->get_x_7();
		NullCheck(L_54);
		int32_t L_55 = 6;
		uint32_t L_56 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_2), L_51, L_52, L_53, L_56, (uint8_t)((int32_t)11), /*hidden argument*/NULL);
		uint32_t L_57 = V_2;
		uint32_t L_58 = V_3;
		uint32_t L_59 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_60 = __this->get_x_7();
		NullCheck(L_60);
		int32_t L_61 = 7;
		uint32_t L_62 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_1), L_57, L_58, L_59, L_62, (uint8_t)((int32_t)19), /*hidden argument*/NULL);
		uint32_t L_63 = V_1;
		uint32_t L_64 = V_2;
		uint32_t L_65 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_66 = __this->get_x_7();
		NullCheck(L_66);
		int32_t L_67 = 8;
		uint32_t L_68 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_0), L_63, L_64, L_65, L_68, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_69 = V_0;
		uint32_t L_70 = V_1;
		uint32_t L_71 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_72 = __this->get_x_7();
		NullCheck(L_72);
		int32_t L_73 = ((int32_t)9);
		uint32_t L_74 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_3), L_69, L_70, L_71, L_74, (uint8_t)7, /*hidden argument*/NULL);
		uint32_t L_75 = V_3;
		uint32_t L_76 = V_0;
		uint32_t L_77 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_78 = __this->get_x_7();
		NullCheck(L_78);
		int32_t L_79 = ((int32_t)10);
		uint32_t L_80 = (L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_2), L_75, L_76, L_77, L_80, (uint8_t)((int32_t)11), /*hidden argument*/NULL);
		uint32_t L_81 = V_2;
		uint32_t L_82 = V_3;
		uint32_t L_83 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_84 = __this->get_x_7();
		NullCheck(L_84);
		int32_t L_85 = ((int32_t)11);
		uint32_t L_86 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_85));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_1), L_81, L_82, L_83, L_86, (uint8_t)((int32_t)19), /*hidden argument*/NULL);
		uint32_t L_87 = V_1;
		uint32_t L_88 = V_2;
		uint32_t L_89 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_90 = __this->get_x_7();
		NullCheck(L_90);
		int32_t L_91 = ((int32_t)12);
		uint32_t L_92 = (L_90)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_0), L_87, L_88, L_89, L_92, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_93 = V_0;
		uint32_t L_94 = V_1;
		uint32_t L_95 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_96 = __this->get_x_7();
		NullCheck(L_96);
		int32_t L_97 = ((int32_t)13);
		uint32_t L_98 = (L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_97));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_3), L_93, L_94, L_95, L_98, (uint8_t)7, /*hidden argument*/NULL);
		uint32_t L_99 = V_3;
		uint32_t L_100 = V_0;
		uint32_t L_101 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_102 = __this->get_x_7();
		NullCheck(L_102);
		int32_t L_103 = ((int32_t)14);
		uint32_t L_104 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_2), L_99, L_100, L_101, L_104, (uint8_t)((int32_t)11), /*hidden argument*/NULL);
		uint32_t L_105 = V_2;
		uint32_t L_106 = V_3;
		uint32_t L_107 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_108 = __this->get_x_7();
		NullCheck(L_108);
		int32_t L_109 = ((int32_t)15);
		uint32_t L_110 = (L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_109));
		MD4Managed_FF_m8750957E572840973D5E0792CD61594F819861C3(__this, (uint32_t*)(&V_1), L_105, L_106, L_107, L_110, (uint8_t)((int32_t)19), /*hidden argument*/NULL);
		uint32_t L_111 = V_1;
		uint32_t L_112 = V_2;
		uint32_t L_113 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_114 = __this->get_x_7();
		NullCheck(L_114);
		int32_t L_115 = 0;
		uint32_t L_116 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_115));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_0), L_111, L_112, L_113, L_116, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_117 = V_0;
		uint32_t L_118 = V_1;
		uint32_t L_119 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_120 = __this->get_x_7();
		NullCheck(L_120);
		int32_t L_121 = 4;
		uint32_t L_122 = (L_120)->GetAt(static_cast<il2cpp_array_size_t>(L_121));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_3), L_117, L_118, L_119, L_122, (uint8_t)5, /*hidden argument*/NULL);
		uint32_t L_123 = V_3;
		uint32_t L_124 = V_0;
		uint32_t L_125 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_126 = __this->get_x_7();
		NullCheck(L_126);
		int32_t L_127 = 8;
		uint32_t L_128 = (L_126)->GetAt(static_cast<il2cpp_array_size_t>(L_127));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_2), L_123, L_124, L_125, L_128, (uint8_t)((int32_t)9), /*hidden argument*/NULL);
		uint32_t L_129 = V_2;
		uint32_t L_130 = V_3;
		uint32_t L_131 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_132 = __this->get_x_7();
		NullCheck(L_132);
		int32_t L_133 = ((int32_t)12);
		uint32_t L_134 = (L_132)->GetAt(static_cast<il2cpp_array_size_t>(L_133));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_1), L_129, L_130, L_131, L_134, (uint8_t)((int32_t)13), /*hidden argument*/NULL);
		uint32_t L_135 = V_1;
		uint32_t L_136 = V_2;
		uint32_t L_137 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_138 = __this->get_x_7();
		NullCheck(L_138);
		int32_t L_139 = 1;
		uint32_t L_140 = (L_138)->GetAt(static_cast<il2cpp_array_size_t>(L_139));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_0), L_135, L_136, L_137, L_140, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_141 = V_0;
		uint32_t L_142 = V_1;
		uint32_t L_143 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_144 = __this->get_x_7();
		NullCheck(L_144);
		int32_t L_145 = 5;
		uint32_t L_146 = (L_144)->GetAt(static_cast<il2cpp_array_size_t>(L_145));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_3), L_141, L_142, L_143, L_146, (uint8_t)5, /*hidden argument*/NULL);
		uint32_t L_147 = V_3;
		uint32_t L_148 = V_0;
		uint32_t L_149 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_150 = __this->get_x_7();
		NullCheck(L_150);
		int32_t L_151 = ((int32_t)9);
		uint32_t L_152 = (L_150)->GetAt(static_cast<il2cpp_array_size_t>(L_151));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_2), L_147, L_148, L_149, L_152, (uint8_t)((int32_t)9), /*hidden argument*/NULL);
		uint32_t L_153 = V_2;
		uint32_t L_154 = V_3;
		uint32_t L_155 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_156 = __this->get_x_7();
		NullCheck(L_156);
		int32_t L_157 = ((int32_t)13);
		uint32_t L_158 = (L_156)->GetAt(static_cast<il2cpp_array_size_t>(L_157));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_1), L_153, L_154, L_155, L_158, (uint8_t)((int32_t)13), /*hidden argument*/NULL);
		uint32_t L_159 = V_1;
		uint32_t L_160 = V_2;
		uint32_t L_161 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_162 = __this->get_x_7();
		NullCheck(L_162);
		int32_t L_163 = 2;
		uint32_t L_164 = (L_162)->GetAt(static_cast<il2cpp_array_size_t>(L_163));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_0), L_159, L_160, L_161, L_164, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_165 = V_0;
		uint32_t L_166 = V_1;
		uint32_t L_167 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_168 = __this->get_x_7();
		NullCheck(L_168);
		int32_t L_169 = 6;
		uint32_t L_170 = (L_168)->GetAt(static_cast<il2cpp_array_size_t>(L_169));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_3), L_165, L_166, L_167, L_170, (uint8_t)5, /*hidden argument*/NULL);
		uint32_t L_171 = V_3;
		uint32_t L_172 = V_0;
		uint32_t L_173 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_174 = __this->get_x_7();
		NullCheck(L_174);
		int32_t L_175 = ((int32_t)10);
		uint32_t L_176 = (L_174)->GetAt(static_cast<il2cpp_array_size_t>(L_175));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_2), L_171, L_172, L_173, L_176, (uint8_t)((int32_t)9), /*hidden argument*/NULL);
		uint32_t L_177 = V_2;
		uint32_t L_178 = V_3;
		uint32_t L_179 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_180 = __this->get_x_7();
		NullCheck(L_180);
		int32_t L_181 = ((int32_t)14);
		uint32_t L_182 = (L_180)->GetAt(static_cast<il2cpp_array_size_t>(L_181));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_1), L_177, L_178, L_179, L_182, (uint8_t)((int32_t)13), /*hidden argument*/NULL);
		uint32_t L_183 = V_1;
		uint32_t L_184 = V_2;
		uint32_t L_185 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_186 = __this->get_x_7();
		NullCheck(L_186);
		int32_t L_187 = 3;
		uint32_t L_188 = (L_186)->GetAt(static_cast<il2cpp_array_size_t>(L_187));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_0), L_183, L_184, L_185, L_188, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_189 = V_0;
		uint32_t L_190 = V_1;
		uint32_t L_191 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_192 = __this->get_x_7();
		NullCheck(L_192);
		int32_t L_193 = 7;
		uint32_t L_194 = (L_192)->GetAt(static_cast<il2cpp_array_size_t>(L_193));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_3), L_189, L_190, L_191, L_194, (uint8_t)5, /*hidden argument*/NULL);
		uint32_t L_195 = V_3;
		uint32_t L_196 = V_0;
		uint32_t L_197 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_198 = __this->get_x_7();
		NullCheck(L_198);
		int32_t L_199 = ((int32_t)11);
		uint32_t L_200 = (L_198)->GetAt(static_cast<il2cpp_array_size_t>(L_199));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_2), L_195, L_196, L_197, L_200, (uint8_t)((int32_t)9), /*hidden argument*/NULL);
		uint32_t L_201 = V_2;
		uint32_t L_202 = V_3;
		uint32_t L_203 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_204 = __this->get_x_7();
		NullCheck(L_204);
		int32_t L_205 = ((int32_t)15);
		uint32_t L_206 = (L_204)->GetAt(static_cast<il2cpp_array_size_t>(L_205));
		MD4Managed_GG_m00A4648827B25DF55EABDF6FFB5C96E871C60E36(__this, (uint32_t*)(&V_1), L_201, L_202, L_203, L_206, (uint8_t)((int32_t)13), /*hidden argument*/NULL);
		uint32_t L_207 = V_1;
		uint32_t L_208 = V_2;
		uint32_t L_209 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_210 = __this->get_x_7();
		NullCheck(L_210);
		int32_t L_211 = 0;
		uint32_t L_212 = (L_210)->GetAt(static_cast<il2cpp_array_size_t>(L_211));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_0), L_207, L_208, L_209, L_212, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_213 = V_0;
		uint32_t L_214 = V_1;
		uint32_t L_215 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_216 = __this->get_x_7();
		NullCheck(L_216);
		int32_t L_217 = 8;
		uint32_t L_218 = (L_216)->GetAt(static_cast<il2cpp_array_size_t>(L_217));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_3), L_213, L_214, L_215, L_218, (uint8_t)((int32_t)9), /*hidden argument*/NULL);
		uint32_t L_219 = V_3;
		uint32_t L_220 = V_0;
		uint32_t L_221 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_222 = __this->get_x_7();
		NullCheck(L_222);
		int32_t L_223 = 4;
		uint32_t L_224 = (L_222)->GetAt(static_cast<il2cpp_array_size_t>(L_223));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_2), L_219, L_220, L_221, L_224, (uint8_t)((int32_t)11), /*hidden argument*/NULL);
		uint32_t L_225 = V_2;
		uint32_t L_226 = V_3;
		uint32_t L_227 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_228 = __this->get_x_7();
		NullCheck(L_228);
		int32_t L_229 = ((int32_t)12);
		uint32_t L_230 = (L_228)->GetAt(static_cast<il2cpp_array_size_t>(L_229));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_1), L_225, L_226, L_227, L_230, (uint8_t)((int32_t)15), /*hidden argument*/NULL);
		uint32_t L_231 = V_1;
		uint32_t L_232 = V_2;
		uint32_t L_233 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_234 = __this->get_x_7();
		NullCheck(L_234);
		int32_t L_235 = 2;
		uint32_t L_236 = (L_234)->GetAt(static_cast<il2cpp_array_size_t>(L_235));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_0), L_231, L_232, L_233, L_236, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_237 = V_0;
		uint32_t L_238 = V_1;
		uint32_t L_239 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_240 = __this->get_x_7();
		NullCheck(L_240);
		int32_t L_241 = ((int32_t)10);
		uint32_t L_242 = (L_240)->GetAt(static_cast<il2cpp_array_size_t>(L_241));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_3), L_237, L_238, L_239, L_242, (uint8_t)((int32_t)9), /*hidden argument*/NULL);
		uint32_t L_243 = V_3;
		uint32_t L_244 = V_0;
		uint32_t L_245 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_246 = __this->get_x_7();
		NullCheck(L_246);
		int32_t L_247 = 6;
		uint32_t L_248 = (L_246)->GetAt(static_cast<il2cpp_array_size_t>(L_247));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_2), L_243, L_244, L_245, L_248, (uint8_t)((int32_t)11), /*hidden argument*/NULL);
		uint32_t L_249 = V_2;
		uint32_t L_250 = V_3;
		uint32_t L_251 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_252 = __this->get_x_7();
		NullCheck(L_252);
		int32_t L_253 = ((int32_t)14);
		uint32_t L_254 = (L_252)->GetAt(static_cast<il2cpp_array_size_t>(L_253));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_1), L_249, L_250, L_251, L_254, (uint8_t)((int32_t)15), /*hidden argument*/NULL);
		uint32_t L_255 = V_1;
		uint32_t L_256 = V_2;
		uint32_t L_257 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_258 = __this->get_x_7();
		NullCheck(L_258);
		int32_t L_259 = 1;
		uint32_t L_260 = (L_258)->GetAt(static_cast<il2cpp_array_size_t>(L_259));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_0), L_255, L_256, L_257, L_260, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_261 = V_0;
		uint32_t L_262 = V_1;
		uint32_t L_263 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_264 = __this->get_x_7();
		NullCheck(L_264);
		int32_t L_265 = ((int32_t)9);
		uint32_t L_266 = (L_264)->GetAt(static_cast<il2cpp_array_size_t>(L_265));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_3), L_261, L_262, L_263, L_266, (uint8_t)((int32_t)9), /*hidden argument*/NULL);
		uint32_t L_267 = V_3;
		uint32_t L_268 = V_0;
		uint32_t L_269 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_270 = __this->get_x_7();
		NullCheck(L_270);
		int32_t L_271 = 5;
		uint32_t L_272 = (L_270)->GetAt(static_cast<il2cpp_array_size_t>(L_271));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_2), L_267, L_268, L_269, L_272, (uint8_t)((int32_t)11), /*hidden argument*/NULL);
		uint32_t L_273 = V_2;
		uint32_t L_274 = V_3;
		uint32_t L_275 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_276 = __this->get_x_7();
		NullCheck(L_276);
		int32_t L_277 = ((int32_t)13);
		uint32_t L_278 = (L_276)->GetAt(static_cast<il2cpp_array_size_t>(L_277));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_1), L_273, L_274, L_275, L_278, (uint8_t)((int32_t)15), /*hidden argument*/NULL);
		uint32_t L_279 = V_1;
		uint32_t L_280 = V_2;
		uint32_t L_281 = V_3;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_282 = __this->get_x_7();
		NullCheck(L_282);
		int32_t L_283 = 3;
		uint32_t L_284 = (L_282)->GetAt(static_cast<il2cpp_array_size_t>(L_283));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_0), L_279, L_280, L_281, L_284, (uint8_t)3, /*hidden argument*/NULL);
		uint32_t L_285 = V_0;
		uint32_t L_286 = V_1;
		uint32_t L_287 = V_2;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_288 = __this->get_x_7();
		NullCheck(L_288);
		int32_t L_289 = ((int32_t)11);
		uint32_t L_290 = (L_288)->GetAt(static_cast<il2cpp_array_size_t>(L_289));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_3), L_285, L_286, L_287, L_290, (uint8_t)((int32_t)9), /*hidden argument*/NULL);
		uint32_t L_291 = V_3;
		uint32_t L_292 = V_0;
		uint32_t L_293 = V_1;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_294 = __this->get_x_7();
		NullCheck(L_294);
		int32_t L_295 = 7;
		uint32_t L_296 = (L_294)->GetAt(static_cast<il2cpp_array_size_t>(L_295));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_2), L_291, L_292, L_293, L_296, (uint8_t)((int32_t)11), /*hidden argument*/NULL);
		uint32_t L_297 = V_2;
		uint32_t L_298 = V_3;
		uint32_t L_299 = V_0;
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_300 = __this->get_x_7();
		NullCheck(L_300);
		int32_t L_301 = ((int32_t)15);
		uint32_t L_302 = (L_300)->GetAt(static_cast<il2cpp_array_size_t>(L_301));
		MD4Managed_HH_mC0581C10D440E254F218664593ED5291035038F6(__this, (uint32_t*)(&V_1), L_297, L_298, L_299, L_302, (uint8_t)((int32_t)15), /*hidden argument*/NULL);
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_303 = ___state0;
		NullCheck(L_303);
		uint32_t* L_304 = ((L_303)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)));
		int32_t L_305 = *((uint32_t*)L_304);
		uint32_t L_306 = V_0;
		*((int32_t*)L_304) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_305, (int32_t)L_306));
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_307 = ___state0;
		NullCheck(L_307);
		uint32_t* L_308 = ((L_307)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)));
		int32_t L_309 = *((uint32_t*)L_308);
		uint32_t L_310 = V_1;
		*((int32_t*)L_308) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_309, (int32_t)L_310));
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_311 = ___state0;
		NullCheck(L_311);
		uint32_t* L_312 = ((L_311)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)));
		int32_t L_313 = *((uint32_t*)L_312);
		uint32_t L_314 = V_2;
		*((int32_t*)L_312) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_313, (int32_t)L_314));
		UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* L_315 = ___state0;
		NullCheck(L_315);
		uint32_t* L_316 = ((L_315)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)));
		int32_t L_317 = *((uint32_t*)L_316);
		uint32_t L_318 = V_3;
		*((int32_t*)L_316) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_317, (int32_t)L_318));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Interface.MonoLocalCertificateSelectionCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void MonoLocalCertificateSelectionCallback__ctor_m8408C75BD0A84C2E5B7BFED75CAAA62B0FC07F60 (MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Interface.MonoLocalCertificateSelectionCallback::Invoke(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern "C" IL2CPP_METHOD_ATTR X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * MonoLocalCertificateSelectionCallback_Invoke_mFB921E62D2252D1D50A5FCCFEC1E62BA719A381B (MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * __this, String_t* ___targetHost0, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * ___localCertificates1, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___remoteCertificate2, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___acceptableIssuers3, const RuntimeMethod* method)
{
	X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 4)
				{
					// open
					typedef X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * (*FunctionPointerType) (String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3, targetMethod);
				}
				else
				{
					// closed
					typedef X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * (*FunctionPointerType) (void*, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3, targetMethod);
				}
			}
			else if (___parameterCount != 4)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker3< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(targetMethod, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
							else
								result = GenericVirtFuncInvoker3< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(targetMethod, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker3< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
							else
								result = VirtFuncInvoker3< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
						}
					}
				}
				else
				{
					typedef X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * (*FunctionPointerType) (String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * (*FunctionPointerType) (String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, const RuntimeMethod*);
							result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker4< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(targetMethod, targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
							else
								result = GenericVirtFuncInvoker4< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(targetMethod, targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker4< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
							else
								result = VirtFuncInvoker4< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
						}
					}
				}
				else
				{
					typedef X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * (*FunctionPointerType) (void*, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 4)
			{
				// open
				typedef X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * (*FunctionPointerType) (String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3, targetMethod);
			}
			else
			{
				// closed
				typedef X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * (*FunctionPointerType) (void*, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3, targetMethod);
			}
		}
		else if (___parameterCount != 4)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker3< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(targetMethod, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
						else
							result = GenericVirtFuncInvoker3< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(targetMethod, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker3< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
						else
							result = VirtFuncInvoker3< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
					}
				}
			}
			else
			{
				typedef X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * (*FunctionPointerType) (String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * (*FunctionPointerType) (String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, const RuntimeMethod*);
						result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker4< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(targetMethod, targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
						else
							result = GenericVirtFuncInvoker4< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(targetMethod, targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker4< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
						else
							result = VirtFuncInvoker4< X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3);
					}
				}
			}
			else
			{
				typedef X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * (*FunctionPointerType) (void*, String_t*, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___targetHost0, ___localCertificates1, ___remoteCertificate2, ___acceptableIssuers3, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult Mono.Security.Interface.MonoLocalCertificateSelectionCallback::BeginInvoke(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[],System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MonoLocalCertificateSelectionCallback_BeginInvoke_m860460761E2C341DB4549D1D2A27A72A0E618746 (MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * __this, String_t* ___targetHost0, X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * ___localCertificates1, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___remoteCertificate2, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___acceptableIssuers3, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback4, RuntimeObject * ___object5, const RuntimeMethod* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___targetHost0;
	__d_args[1] = ___localCertificates1;
	__d_args[2] = ___remoteCertificate2;
	__d_args[3] = ___acceptableIssuers3;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback4, (RuntimeObject*)___object5);
}
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Interface.MonoLocalCertificateSelectionCallback::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * MonoLocalCertificateSelectionCallback_EndInvoke_m6D484538EE84A8D910EFF7288158979751BDF447 (MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Interface.MonoRemoteCertificateValidationCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void MonoRemoteCertificateValidationCallback__ctor_mC3C556F0B5102353E3EFCE5098B7338B039819FA (MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean Mono.Security.Interface.MonoRemoteCertificateValidationCallback::Invoke(System.String,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,Mono.Security.Interface.MonoSslPolicyErrors)
extern "C" IL2CPP_METHOD_ATTR bool MonoRemoteCertificateValidationCallback_Invoke_m405859123BB9004917076DD131EA8522AA18D990 (MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * __this, String_t* ___targetHost0, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___certificate1, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 * ___chain2, int32_t ___sslPolicyErrors3, const RuntimeMethod* method)
{
	bool result = false;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 4)
				{
					// open
					typedef bool (*FunctionPointerType) (String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3, targetMethod);
				}
				else
				{
					// closed
					typedef bool (*FunctionPointerType) (void*, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3, targetMethod);
				}
			}
			else if (___parameterCount != 4)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker3< bool, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(targetMethod, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
							else
								result = GenericVirtFuncInvoker3< bool, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(targetMethod, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker3< bool, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
							else
								result = VirtFuncInvoker3< bool, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
						}
					}
				}
				else
				{
					typedef bool (*FunctionPointerType) (String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef bool (*FunctionPointerType) (String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t, const RuntimeMethod*);
							result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker4< bool, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(targetMethod, targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
							else
								result = GenericVirtFuncInvoker4< bool, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(targetMethod, targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker4< bool, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
							else
								result = VirtFuncInvoker4< bool, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
						}
					}
				}
				else
				{
					typedef bool (*FunctionPointerType) (void*, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 4)
			{
				// open
				typedef bool (*FunctionPointerType) (String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3, targetMethod);
			}
			else
			{
				// closed
				typedef bool (*FunctionPointerType) (void*, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3, targetMethod);
			}
		}
		else if (___parameterCount != 4)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker3< bool, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(targetMethod, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
						else
							result = GenericVirtFuncInvoker3< bool, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(targetMethod, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker3< bool, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
						else
							result = VirtFuncInvoker3< bool, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
					}
				}
			}
			else
			{
				typedef bool (*FunctionPointerType) (String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef bool (*FunctionPointerType) (String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t, const RuntimeMethod*);
						result = ((FunctionPointerType)targetMethodPointer)(___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker4< bool, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(targetMethod, targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
						else
							result = GenericVirtFuncInvoker4< bool, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(targetMethod, targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker4< bool, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
						else
							result = VirtFuncInvoker4< bool, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3);
					}
				}
			}
			else
			{
				typedef bool (*FunctionPointerType) (void*, String_t*, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 *, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 *, int32_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___targetHost0, ___certificate1, ___chain2, ___sslPolicyErrors3, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult Mono.Security.Interface.MonoRemoteCertificateValidationCallback::BeginInvoke(System.String,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,Mono.Security.Interface.MonoSslPolicyErrors,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MonoRemoteCertificateValidationCallback_BeginInvoke_mE8A7228A3C1E37CED0138DA272D8A9A6ED52E2D1 (MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * __this, String_t* ___targetHost0, X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___certificate1, X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 * ___chain2, int32_t ___sslPolicyErrors3, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback4, RuntimeObject * ___object5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoRemoteCertificateValidationCallback_BeginInvoke_mE8A7228A3C1E37CED0138DA272D8A9A6ED52E2D1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___targetHost0;
	__d_args[1] = ___certificate1;
	__d_args[2] = ___chain2;
	__d_args[3] = Box(MonoSslPolicyErrors_t5F32A4E793EAB8B8A8128A6A3E7690D2E1F666C7_il2cpp_TypeInfo_var, &___sslPolicyErrors3);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback4, (RuntimeObject*)___object5);
}
// System.Boolean Mono.Security.Interface.MonoRemoteCertificateValidationCallback::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR bool MonoRemoteCertificateValidationCallback_EndInvoke_m64D2BF4965D6F6274107AD36B0F06ADE8DAF8958 (MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Interface.MonoTlsSettings::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoTlsSettings__ctor_m09A7B7F0B1CA2775C8A24B8A6BEC032E6B1C0A72 (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * __this, const RuntimeMethod* method)
{
	{
		__this->set_checkCertName_10((bool)1);
		__this->set_callbackNeedsChain_14((bool)1);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Security.Interface.MonoTlsSettings Mono.Security.Interface.MonoTlsSettings::get_DefaultSettings()
extern "C" IL2CPP_METHOD_ATTR MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * MonoTlsSettings_get_DefaultSettings_m49A7CECC7D687F62790DD374D560278D3916B887 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoTlsSettings_get_DefaultSettings_m49A7CECC7D687F62790DD374D560278D3916B887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_0 = ((MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_StaticFields*)il2cpp_codegen_static_fields_for(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_il2cpp_TypeInfo_var))->get_defaultSettings_16();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_1 = (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF *)il2cpp_codegen_object_new(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_il2cpp_TypeInfo_var);
		MonoTlsSettings__ctor_m09A7B7F0B1CA2775C8A24B8A6BEC032E6B1C0A72(L_1, /*hidden argument*/NULL);
		InterlockedCompareExchangeImpl<MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF *>((MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF **)(((MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_StaticFields*)il2cpp_codegen_static_fields_for(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_il2cpp_TypeInfo_var))->get_address_of_defaultSettings_16()), L_1, (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF *)NULL);
	}

IL_0018:
	{
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_2 = ((MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_StaticFields*)il2cpp_codegen_static_fields_for(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_il2cpp_TypeInfo_var))->get_defaultSettings_16();
		return L_2;
	}
}
// Mono.Security.Interface.MonoTlsSettings Mono.Security.Interface.MonoTlsSettings::CopyDefaultSettings()
extern "C" IL2CPP_METHOD_ATTR MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * MonoTlsSettings_CopyDefaultSettings_m4CE2BE44BF064124CFD98CEF44E76FD5D6EA0AD2 (const RuntimeMethod* method)
{
	{
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_0 = MonoTlsSettings_get_DefaultSettings_m49A7CECC7D687F62790DD374D560278D3916B887(/*hidden argument*/NULL);
		NullCheck(L_0);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_1 = MonoTlsSettings_Clone_mF28F7F627B12CBD0BD1ABD6F35DD0B4BAD2E3840(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Mono.Security.Interface.MonoTlsSettings Mono.Security.Interface.MonoTlsSettings::CloneWithValidator(Mono.Security.Interface.ICertificateValidator)
extern "C" IL2CPP_METHOD_ATTR MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * MonoTlsSettings_CloneWithValidator_m46363CAC421939D9904660815CF1D8131C8DC3B1 (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * __this, RuntimeObject* ___validator0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoTlsSettings_CloneWithValidator_m46363CAC421939D9904660815CF1D8131C8DC3B1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_cloned_9();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		RuntimeObject* L_1 = ___validator0;
		__this->set_certificateValidator_15(L_1);
		return __this;
	}

IL_0011:
	{
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_2 = (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF *)il2cpp_codegen_object_new(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_il2cpp_TypeInfo_var);
		MonoTlsSettings__ctor_m3D336E73C9393401BE9BE856B7204BA176F52B17(L_2, __this, /*hidden argument*/NULL);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_3 = L_2;
		RuntimeObject* L_4 = ___validator0;
		NullCheck(L_3);
		L_3->set_certificateValidator_15(L_4);
		return L_3;
	}
}
// Mono.Security.Interface.MonoTlsSettings Mono.Security.Interface.MonoTlsSettings::Clone()
extern "C" IL2CPP_METHOD_ATTR MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * MonoTlsSettings_Clone_mF28F7F627B12CBD0BD1ABD6F35DD0B4BAD2E3840 (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoTlsSettings_Clone_mF28F7F627B12CBD0BD1ABD6F35DD0B4BAD2E3840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_0 = (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF *)il2cpp_codegen_object_new(MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF_il2cpp_TypeInfo_var);
		MonoTlsSettings__ctor_m3D336E73C9393401BE9BE856B7204BA176F52B17(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Mono.Security.Interface.MonoTlsSettings::.ctor(Mono.Security.Interface.MonoTlsSettings)
extern "C" IL2CPP_METHOD_ATTR void MonoTlsSettings__ctor_m3D336E73C9393401BE9BE856B7204BA176F52B17 (MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * __this, MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoTlsSettings__ctor_m3D336E73C9393401BE9BE856B7204BA176F52B17_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_checkCertName_10((bool)1);
		__this->set_callbackNeedsChain_14((bool)1);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_0 = ___other0;
		NullCheck(L_0);
		MonoRemoteCertificateValidationCallback_t7A8DAD12B70CE3BB19BAAD04F587D5ED02385CC6 * L_1 = L_0->get_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0();
		__this->set_U3CRemoteCertificateValidationCallbackU3Ek__BackingField_0(L_1);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_2 = ___other0;
		NullCheck(L_2);
		MonoLocalCertificateSelectionCallback_t657381EF916D4EDC456FA5A6AC948EFD7A481F0A * L_3 = L_2->get_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1();
		__this->set_U3CClientCertificateSelectionCallbackU3Ek__BackingField_1(L_3);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_4 = ___other0;
		NullCheck(L_4);
		bool L_5 = L_4->get_checkCertName_10();
		__this->set_checkCertName_10(L_5);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_6 = ___other0;
		NullCheck(L_6);
		bool L_7 = L_6->get_checkCertRevocationStatus_11();
		__this->set_checkCertRevocationStatus_11(L_7);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_8 = ___other0;
		NullCheck(L_8);
		Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  L_9 = L_8->get_useServicePointManagerCallback_12();
		__this->set_useServicePointManagerCallback_12(L_9);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_10 = ___other0;
		NullCheck(L_10);
		bool L_11 = L_10->get_skipSystemValidators_13();
		__this->set_skipSystemValidators_13(L_11);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_12 = ___other0;
		NullCheck(L_12);
		bool L_13 = L_12->get_callbackNeedsChain_14();
		__this->set_callbackNeedsChain_14(L_13);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_14 = ___other0;
		NullCheck(L_14);
		RuntimeObject * L_15 = L_14->get_U3CUserSettingsU3Ek__BackingField_4();
		__this->set_U3CUserSettingsU3Ek__BackingField_4(L_15);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_16 = ___other0;
		NullCheck(L_16);
		Nullable_1_t601798BE10C3F3F37B6755E475BB1B3760DCBB10  L_17 = L_16->get_U3CEnabledProtocolsU3Ek__BackingField_7();
		__this->set_U3CEnabledProtocolsU3Ek__BackingField_7(L_17);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_18 = ___other0;
		NullCheck(L_18);
		CipherSuiteCodeU5BU5D_t0EC37AD4A25BB94BA9AB4A9C0C4802BD79A07CC4* L_19 = L_18->get_U3CEnabledCiphersU3Ek__BackingField_8();
		__this->set_U3CEnabledCiphersU3Ek__BackingField_8(L_19);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_20 = ___other0;
		NullCheck(L_20);
		Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  L_21 = L_20->get_U3CCertificateValidationTimeU3Ek__BackingField_2();
		__this->set_U3CCertificateValidationTimeU3Ek__BackingField_2(L_21);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_22 = ___other0;
		NullCheck(L_22);
		bool L_23 = L_22->get_U3CSendCloseNotifyU3Ek__BackingField_6();
		__this->set_U3CSendCloseNotifyU3Ek__BackingField_6(L_23);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_24 = ___other0;
		NullCheck(L_24);
		X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * L_25 = L_24->get_U3CTrustAnchorsU3Ek__BackingField_3();
		if (!L_25)
		{
			goto IL_00bd;
		}
	}
	{
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_26 = ___other0;
		NullCheck(L_26);
		X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * L_27 = L_26->get_U3CTrustAnchorsU3Ek__BackingField_3();
		X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * L_28 = (X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 *)il2cpp_codegen_object_new(X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m24BAA500237577256DCA1BD814ED88D163E702E3(L_28, L_27, /*hidden argument*/NULL);
		__this->set_U3CTrustAnchorsU3Ek__BackingField_3(L_28);
	}

IL_00bd:
	{
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_29 = ___other0;
		NullCheck(L_29);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_30 = L_29->get_U3CCertificateSearchPathsU3Ek__BackingField_5();
		if (!L_30)
		{
			goto IL_00ea;
		}
	}
	{
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_31 = ___other0;
		NullCheck(L_31);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_32 = L_31->get_U3CCertificateSearchPathsU3Ek__BackingField_5();
		NullCheck(L_32);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_33 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_32)->max_length)))));
		__this->set_U3CCertificateSearchPathsU3Ek__BackingField_5(L_33);
		MonoTlsSettings_t5905C7532C92A87F88C8F3440165DF8AA49A1BBF * L_34 = ___other0;
		NullCheck(L_34);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_35 = L_34->get_U3CCertificateSearchPathsU3Ek__BackingField_5();
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_36 = __this->get_U3CCertificateSearchPathsU3Ek__BackingField_5();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_35);
		Array_CopyTo_m455300D414FFB0EBFE53EA4E8BBD31532006EBB7((RuntimeArray *)(RuntimeArray *)L_35, (RuntimeArray *)(RuntimeArray *)L_36, 0, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		__this->set_cloned_9((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse__ctor_m1E0300839CAF582A720DB0F4F9E425B6EE12B258 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse__ctor_m1E0300839CAF582A720DB0F4F9E425B6EE12B258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		__this->set__disposed_2((bool)0);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)21));
		__this->set__lmpwd_4(L_0);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)21));
		__this->set__ntpwd_5(L_1);
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::.ctor(System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse__ctor_mF80EAE315F35264F1DA0167B3ED7A8CD8E2D1FFA (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, String_t* ___password0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___challenge1, const RuntimeMethod* method)
{
	{
		ChallengeResponse__ctor_m1E0300839CAF582A720DB0F4F9E425B6EE12B258(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___password0;
		ChallengeResponse_set_Password_m530EB94179C374BED9B9AAE4BB30AB3FF14F07E2(__this, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ___challenge1;
		ChallengeResponse_set_Challenge_mD747C1A002528A6E9AFDE848AA257FD7B1B85E6F(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::Finalize()
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse_Finalize_m358598D53FC1FDD2180BBF5877FEAAE3C8606118 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = __this->get__disposed_2();
			if (L_0)
			{
				goto IL_000e;
			}
		}

IL_0008:
		{
			ChallengeResponse_Dispose_mD6C08D1EDA541DC5A9B287744FB18E3149627434(__this, /*hidden argument*/NULL);
		}

IL_000e:
		{
			IL2CPP_LEAVE(0x17, FINALLY_0010);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::set_Password(System.String)
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse_set_Password_m530EB94179C374BED9B9AAE4BB30AB3FF14F07E2 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse_set_Password_m530EB94179C374BED9B9AAE4BB30AB3FF14F07E2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * V_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_1 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * G_B12_0 = NULL;
	MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * G_B11_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* G_B13_0 = NULL;
	MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * G_B13_1 = NULL;
	{
		bool L_0 = __this->get__disposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A * L_1 = (ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A *)il2cpp_codegen_object_new(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8B5D23EA08E42BDE6BC5233CC666295F19BBD2F9(L_1, _stringLiteral42BBEE886171CED8B81918E0F830F24966193E05, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, ChallengeResponse_set_Password_m530EB94179C374BED9B9AAE4BB30AB3FF14F07E2_RuntimeMethod_var);
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0_il2cpp_TypeInfo_var);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_2 = DES_Create_m5EE267FBCD5AA18E04C29247C796430D12247CC5(/*hidden argument*/NULL);
		V_0 = L_2;
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Mode(System.Security.Cryptography.CipherMode) */, L_3, 2);
		String_t* L_4 = ___value0;
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_5 = ___value0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_m_stringLength_0();
		if ((((int32_t)L_6) >= ((int32_t)1)))
		{
			goto IL_0041;
		}
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ((ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var))->get_nullEncMagic_1();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = __this->get__lmpwd_4();
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_7, 0, (RuntimeArray *)(RuntimeArray *)L_8, 0, 8, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_0041:
	{
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_9 = V_0;
		String_t* L_10 = ___value0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = ChallengeResponse_PasswordToKey_m522B84CA0312284486A2C4E10FEE2D74BF4FF163(__this, L_10, 0, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(11 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_9, L_11);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_12 = V_0;
		NullCheck(L_12);
		RuntimeObject* L_13 = VirtFuncInvoker0< RuntimeObject* >::Invoke(16 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = ((ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var))->get_magic_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = __this->get__lmpwd_4();
		NullCheck(L_13);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var, L_13, L_14, 0, 8, L_15, 0);
	}

IL_0069:
	{
		String_t* L_16 = ___value0;
		if (!L_16)
		{
			goto IL_0075;
		}
	}
	{
		String_t* L_17 = ___value0;
		NullCheck(L_17);
		int32_t L_18 = L_17->get_m_stringLength_0();
		if ((((int32_t)L_18) >= ((int32_t)8)))
		{
			goto IL_008a;
		}
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_19 = ((ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var))->get_nullEncMagic_1();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = __this->get__lmpwd_4();
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_19, 0, (RuntimeArray *)(RuntimeArray *)L_20, 8, 8, /*hidden argument*/NULL);
		goto IL_00b2;
	}

IL_008a:
	{
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_21 = V_0;
		String_t* L_22 = ___value0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = ChallengeResponse_PasswordToKey_m522B84CA0312284486A2C4E10FEE2D74BF4FF163(__this, L_22, 7, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(11 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_21, L_23);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_24 = V_0;
		NullCheck(L_24);
		RuntimeObject* L_25 = VirtFuncInvoker0< RuntimeObject* >::Invoke(16 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_24);
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_26 = ((ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var))->get_magic_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_27 = __this->get__lmpwd_4();
		NullCheck(L_25);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var, L_25, L_26, 0, 8, L_27, 8);
	}

IL_00b2:
	{
		MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * L_28 = MD4_Create_m2D436A4CC284704A7DA0EEF4C4D5860F69D0BB93(/*hidden argument*/NULL);
		String_t* L_29 = ___value0;
		G_B11_0 = L_28;
		if (!L_29)
		{
			G_B12_0 = L_28;
			goto IL_00c7;
		}
	}
	{
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_30 = Encoding_get_Unicode_m86CC470F70F9BB52DDB26721F0C0D6EDAFC318AA(/*hidden argument*/NULL);
		String_t* L_31 = ___value0;
		NullCheck(L_30);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_32 = VirtFuncInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_30, L_31);
		G_B13_0 = L_32;
		G_B13_1 = G_B11_0;
		goto IL_00cd;
	}

IL_00c7:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_33 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)0);
		G_B13_0 = L_33;
		G_B13_1 = G_B12_0;
	}

IL_00cd:
	{
		V_1 = G_B13_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_34 = V_1;
		NullCheck(G_B13_1);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_35 = HashAlgorithm_ComputeHash_m18501D3068AEBEB5FA83EA72BE780E371DB0C122(G_B13_1, L_34, /*hidden argument*/NULL);
		V_2 = L_35;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_36 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_37 = __this->get__ntpwd_5();
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_36, 0, (RuntimeArray *)(RuntimeArray *)L_37, 0, ((int32_t)16), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_38 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_39 = V_1;
		NullCheck(L_39);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_38, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_39)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_40 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_41 = V_2;
		NullCheck(L_41);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_40, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_41)->max_length)))), /*hidden argument*/NULL);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_42 = V_0;
		NullCheck(L_42);
		SymmetricAlgorithm_Clear_m8487379B135918E72684597CFE388EF7FCA733D2(L_42, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::set_Challenge(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse_set_Challenge_mD747C1A002528A6E9AFDE848AA257FD7B1B85E6F (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse_set_Challenge_mD747C1A002528A6E9AFDE848AA257FD7B1B85E6F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, _stringLiteralBB9D653F4E12DEED891E16FF7E7D9376E13F075D, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, ChallengeResponse_set_Challenge_mD747C1A002528A6E9AFDE848AA257FD7B1B85E6F_RuntimeMethod_var);
	}

IL_000e:
	{
		bool L_2 = __this->get__disposed_2();
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A * L_3 = (ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A *)il2cpp_codegen_object_new(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8B5D23EA08E42BDE6BC5233CC666295F19BBD2F9(L_3, _stringLiteral42BBEE886171CED8B81918E0F830F24966193E05, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, ChallengeResponse_set_Challenge_mD747C1A002528A6E9AFDE848AA257FD7B1B85E6F_RuntimeMethod_var);
	}

IL_0021:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ___value0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_4);
		RuntimeObject * L_5 = Array_Clone_mE8C710213E323617A6F46F2B36DCDDD4C7CF5176((RuntimeArray *)(RuntimeArray *)L_4, /*hidden argument*/NULL);
		__this->set__challenge_3(((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)Castclass((RuntimeObject*)L_5, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::get_LM()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse_get_LM_m3916048E028CFCA867E801A83FEB949F7C089263 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse_get_LM_m3916048E028CFCA867E801A83FEB949F7C089263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__disposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A * L_1 = (ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A *)il2cpp_codegen_object_new(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8B5D23EA08E42BDE6BC5233CC666295F19BBD2F9(L_1, _stringLiteral42BBEE886171CED8B81918E0F830F24966193E05, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, ChallengeResponse_get_LM_m3916048E028CFCA867E801A83FEB949F7C089263_RuntimeMethod_var);
	}

IL_0013:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = __this->get__lmpwd_4();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ChallengeResponse_GetResponse_m526E49021AB29DD12995CF8BB12DC9F03F2A583F(__this, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::get_NT()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse_get_NT_mEC9F2FDFDB8FADF415D4BA8A1564A23024FC3437 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse_get_NT_mEC9F2FDFDB8FADF415D4BA8A1564A23024FC3437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__disposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A * L_1 = (ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A *)il2cpp_codegen_object_new(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8B5D23EA08E42BDE6BC5233CC666295F19BBD2F9(L_1, _stringLiteral42BBEE886171CED8B81918E0F830F24966193E05, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, ChallengeResponse_get_NT_mEC9F2FDFDB8FADF415D4BA8A1564A23024FC3437_RuntimeMethod_var);
	}

IL_0013:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = __this->get__ntpwd_5();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ChallengeResponse_GetResponse_m526E49021AB29DD12995CF8BB12DC9F03F2A583F(__this, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::Dispose()
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse_Dispose_mD6C08D1EDA541DC5A9B287744FB18E3149627434 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse_Dispose_mD6C08D1EDA541DC5A9B287744FB18E3149627434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ChallengeResponse_Dispose_mF3B015B967C89BE1E139EFA40740208B10441AED(__this, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::Dispose(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse_Dispose_mF3B015B967C89BE1E139EFA40740208B10441AED (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, bool ___disposing0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get__disposed_2();
		if (L_0)
		{
			goto IL_0053;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = __this->get__lmpwd_4();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = __this->get__lmpwd_4();
		NullCheck(L_2);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_1, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = __this->get__ntpwd_5();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = __this->get__ntpwd_5();
		NullCheck(L_4);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_3, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = __this->get__challenge_3();
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = __this->get__challenge_3();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = __this->get__challenge_3();
		NullCheck(L_7);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_6, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))), /*hidden argument*/NULL);
	}

IL_004c:
	{
		__this->set__disposed_2((bool)1);
	}

IL_0053:
	{
		return;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::GetResponse(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse_GetResponse_m526E49021AB29DD12995CF8BB12DC9F03F2A583F (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pwd0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse_GetResponse_m526E49021AB29DD12995CF8BB12DC9F03F2A583F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24));
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0_il2cpp_TypeInfo_var);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_1 = DES_Create_m5EE267FBCD5AA18E04C29247C796430D12247CC5(/*hidden argument*/NULL);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_2 = L_1;
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Mode(System.Security.Cryptography.CipherMode) */, L_2, 2);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_3 = L_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ___pwd0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = ChallengeResponse_PrepareDESKey_m32B2174E0B63E959CE08204F3C39AAA01799A3B3(__this, L_4, 0, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(11 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_3, L_5);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_6 = L_3;
		NullCheck(L_6);
		RuntimeObject* L_7 = VirtFuncInvoker0< RuntimeObject* >::Invoke(16 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_6);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = __this->get__challenge_3();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = V_0;
		NullCheck(L_7);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var, L_7, L_8, 0, 8, L_9, 0);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_10 = L_6;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = ___pwd0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = ChallengeResponse_PrepareDESKey_m32B2174E0B63E959CE08204F3C39AAA01799A3B3(__this, L_11, 7, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(11 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_10, L_12);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_13 = L_10;
		NullCheck(L_13);
		RuntimeObject* L_14 = VirtFuncInvoker0< RuntimeObject* >::Invoke(16 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_13);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = __this->get__challenge_3();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = V_0;
		NullCheck(L_14);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var, L_14, L_15, 0, 8, L_16, 8);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_17 = L_13;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_18 = ___pwd0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_19 = ChallengeResponse_PrepareDESKey_m32B2174E0B63E959CE08204F3C39AAA01799A3B3(__this, L_18, ((int32_t)14), /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(11 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_17, L_19);
		NullCheck(L_17);
		RuntimeObject* L_20 = VirtFuncInvoker0< RuntimeObject* >::Invoke(16 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_17);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_21 = __this->get__challenge_3();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_22 = V_0;
		NullCheck(L_20);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var, L_20, L_21, 0, 8, L_22, ((int32_t)16));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = V_0;
		return L_23;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::PrepareDESKey(System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse_PrepareDESKey_m32B2174E0B63E959CE08204F3C39AAA01799A3B3 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key56bits0, int32_t ___position1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse_PrepareDESKey_m32B2174E0B63E959CE08204F3C39AAA01799A3B3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = L_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___key56bits0;
		int32_t L_3 = ___position1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_5);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = L_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ___key56bits0;
		int32_t L_8 = ___position1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		uint8_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = ___key56bits0;
		int32_t L_12 = ___position1;
		NullCheck(L_11);
		int32_t L_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
		uint8_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10<<(int32_t)7))|(int32_t)((int32_t)((int32_t)L_14>>(int32_t)1))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = L_6;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = ___key56bits0;
		int32_t L_17 = ___position1;
		NullCheck(L_16);
		int32_t L_18 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
		uint8_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = ___key56bits0;
		int32_t L_21 = ___position1;
		NullCheck(L_20);
		int32_t L_22 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)2));
		uint8_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_19<<(int32_t)6))|(int32_t)((int32_t)((int32_t)L_23>>(int32_t)2))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = L_15;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_25 = ___key56bits0;
		int32_t L_26 = ___position1;
		NullCheck(L_25);
		int32_t L_27 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)2));
		uint8_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_29 = ___key56bits0;
		int32_t L_30 = ___position1;
		NullCheck(L_29);
		int32_t L_31 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)3));
		uint8_t L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_28<<(int32_t)5))|(int32_t)((int32_t)((int32_t)L_32>>(int32_t)3))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_33 = L_24;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_34 = ___key56bits0;
		int32_t L_35 = ___position1;
		NullCheck(L_34);
		int32_t L_36 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)3));
		uint8_t L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_38 = ___key56bits0;
		int32_t L_39 = ___position1;
		NullCheck(L_38);
		int32_t L_40 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)4));
		uint8_t L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		NullCheck(L_33);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_37<<(int32_t)4))|(int32_t)((int32_t)((int32_t)L_41>>(int32_t)4))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_42 = L_33;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_43 = ___key56bits0;
		int32_t L_44 = ___position1;
		NullCheck(L_43);
		int32_t L_45 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)4));
		uint8_t L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_47 = ___key56bits0;
		int32_t L_48 = ___position1;
		NullCheck(L_47);
		int32_t L_49 = ((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)5));
		uint8_t L_50 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		NullCheck(L_42);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_46<<(int32_t)3))|(int32_t)((int32_t)((int32_t)L_50>>(int32_t)5))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_51 = L_42;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_52 = ___key56bits0;
		int32_t L_53 = ___position1;
		NullCheck(L_52);
		int32_t L_54 = ((int32_t)il2cpp_codegen_add((int32_t)L_53, (int32_t)5));
		uint8_t L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_56 = ___key56bits0;
		int32_t L_57 = ___position1;
		NullCheck(L_56);
		int32_t L_58 = ((int32_t)il2cpp_codegen_add((int32_t)L_57, (int32_t)6));
		uint8_t L_59 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		NullCheck(L_51);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_55<<(int32_t)2))|(int32_t)((int32_t)((int32_t)L_59>>(int32_t)6))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_60 = L_51;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_61 = ___key56bits0;
		int32_t L_62 = ___position1;
		NullCheck(L_61);
		int32_t L_63 = ((int32_t)il2cpp_codegen_add((int32_t)L_62, (int32_t)6));
		uint8_t L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		NullCheck(L_60);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_64<<(int32_t)1))))));
		return L_60;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse::PasswordToKey(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse_PasswordToKey_m522B84CA0312284486A2C4E10FEE2D74BF4FF163 (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * __this, String_t* ___password0, int32_t ___position1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse_PasswordToKey_m522B84CA0312284486A2C4E10FEE2D74BF4FF163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)7);
		V_0 = L_0;
		String_t* L_1 = ___password0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_m_stringLength_0();
		int32_t L_3 = ___position1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		int32_t L_4 = Math_Min_mC950438198519FB2B0260FCB91220847EE4BB525(((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_3)), 7, /*hidden argument*/NULL);
		V_1 = L_4;
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_5 = Encoding_get_ASCII_m9B673AE3152AB04D07CADE6E5E142C785B5BC94E(/*hidden argument*/NULL);
		String_t* L_6 = ___password0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var);
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_7 = CultureInfo_get_CurrentCulture_mD86F3D8E5D332FB304F80D9B9CA4DE849C2A6831(/*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_8 = String_ToUpper_m8C69D974350ABA8BA0BC3A66996004CCEFD64293(L_6, L_7, /*hidden argument*/NULL);
		int32_t L_9 = ___position1;
		int32_t L_10 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = V_0;
		NullCheck(L_5);
		VirtFuncInvoker5< int32_t, String_t*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(17 /* System.Int32 System.Text.Encoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32) */, L_5, L_8, L_9, L_10, L_11, 0);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = ChallengeResponse_PrepareDESKey_m32B2174E0B63E959CE08204F3C39AAA01799A3B3(__this, L_12, 0, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = V_0;
		NullCheck(L_15);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_14, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_15)->max_length)))), /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse::.cctor()
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse__cctor_m069BF87DE471BEDD893664B52BBD066119DCD55C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse__cctor_m069BF87DE471BEDD893664B52BBD066119DCD55C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = L_0;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA____AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_1, L_2, /*hidden argument*/NULL);
		((ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var))->set_magic_0(L_1);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = L_3;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_5 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA____16968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_4, L_5, /*hidden argument*/NULL);
		((ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var))->set_nullEncMagic_1(L_4);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute_LM(System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_Compute_LM_m3A1F9371E4F1E41B044787FFB3BE985E92A3AC22 (String_t* ___password0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___challenge1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse2_Compute_LM_m3A1F9371E4F1E41B044787FFB3BE985E92A3AC22_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * V_1 = NULL;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)21));
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0_il2cpp_TypeInfo_var);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_1 = DES_Create_m5EE267FBCD5AA18E04C29247C796430D12247CC5(/*hidden argument*/NULL);
		V_1 = L_1;
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_2 = V_1;
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Mode(System.Security.Cryptography.CipherMode) */, L_2, 2);
		String_t* L_3 = ___password0;
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_4 = ___password0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_m_stringLength_0();
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_0031;
		}
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = ((ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var))->get_nullEncMagic_1();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = V_0;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_6, 0, (RuntimeArray *)(RuntimeArray *)L_7, 0, 8, /*hidden argument*/NULL);
		goto IL_0053;
	}

IL_0031:
	{
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_8 = V_1;
		String_t* L_9 = ___password0;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = ChallengeResponse2_PasswordToKey_m2871E605818DF2DE4BC5B1B163831BA8F64006D8(L_9, 0, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(11 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_8, L_10);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_11 = V_1;
		NullCheck(L_11);
		RuntimeObject* L_12 = VirtFuncInvoker0< RuntimeObject* >::Invoke(16 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_11);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = ((ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var))->get_magic_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = V_0;
		NullCheck(L_12);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var, L_12, L_13, 0, 8, L_14, 0);
	}

IL_0053:
	{
		String_t* L_15 = ___password0;
		if (!L_15)
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_16 = ___password0;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_m_stringLength_0();
		if ((((int32_t)L_17) >= ((int32_t)8)))
		{
			goto IL_006f;
		}
	}

IL_005f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_18 = ((ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var))->get_nullEncMagic_1();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_19 = V_0;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_18, 0, (RuntimeArray *)(RuntimeArray *)L_19, 8, 8, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_006f:
	{
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_20 = V_1;
		String_t* L_21 = ___password0;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_22 = ChallengeResponse2_PasswordToKey_m2871E605818DF2DE4BC5B1B163831BA8F64006D8(L_21, 7, /*hidden argument*/NULL);
		NullCheck(L_20);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(11 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_20, L_22);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_23 = V_1;
		NullCheck(L_23);
		RuntimeObject* L_24 = VirtFuncInvoker0< RuntimeObject* >::Invoke(16 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_23);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_25 = ((ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var))->get_magic_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_26 = V_0;
		NullCheck(L_24);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var, L_24, L_25, 0, 8, L_26, 8);
	}

IL_0091:
	{
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_27 = V_1;
		NullCheck(L_27);
		SymmetricAlgorithm_Clear_m8487379B135918E72684597CFE388EF7FCA733D2(L_27, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_28 = ___challenge1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_29 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_30 = ChallengeResponse2_GetResponse_mE39699CD2453921E373BF9768180993EDCD810E7(L_28, L_29, /*hidden argument*/NULL);
		return L_30;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute_NTLM_Password(System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_Compute_NTLM_Password_mD27D4A18DBD712B0E80B0F2A1CB296B353312C41 (String_t* ___password0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse2_Compute_NTLM_Password_mD27D4A18DBD712B0E80B0F2A1CB296B353312C41_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_1 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * G_B2_0 = NULL;
	MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * G_B1_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* G_B3_0 = NULL;
	MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * G_B3_1 = NULL;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)21));
		V_0 = L_0;
		MD4_t932C1DEA44D4B8650873251E88AA4096164BB380 * L_1 = MD4_Create_m2D436A4CC284704A7DA0EEF4C4D5860F69D0BB93(/*hidden argument*/NULL);
		String_t* L_2 = ___password0;
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_001d;
		}
	}
	{
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_3 = Encoding_get_Unicode_m86CC470F70F9BB52DDB26721F0C0D6EDAFC318AA(/*hidden argument*/NULL);
		String_t* L_4 = ___password0;
		NullCheck(L_3);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = VirtFuncInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_3, L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)0);
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		V_1 = G_B3_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = V_1;
		NullCheck(G_B3_1);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = HashAlgorithm_ComputeHash_m18501D3068AEBEB5FA83EA72BE780E371DB0C122(G_B3_1, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = V_0;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_9, 0, (RuntimeArray *)(RuntimeArray *)L_10, 0, ((int32_t)16), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = V_1;
		NullCheck(L_12);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_11, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = V_2;
		NullCheck(L_14);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_13, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_14)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = V_0;
		return L_15;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute_NTLM(System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_Compute_NTLM_mA1DCA878A3A7A5517DB8BB0F2BEDD29353573976 (String_t* ___password0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___challenge1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse2_Compute_NTLM_mA1DCA878A3A7A5517DB8BB0F2BEDD29353573976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		String_t* L_0 = ___password0;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ChallengeResponse2_Compute_NTLM_Password_mD27D4A18DBD712B0E80B0F2A1CB296B353312C41(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___challenge1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ChallengeResponse2_GetResponse_mE39699CD2453921E373BF9768180993EDCD810E7(L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute_NTLMv2_Session(System.String,System.Byte[],System.Byte[]U26,System.Byte[]U26)
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse2_Compute_NTLMv2_Session_mFB6537BF7FC8D9D7CCE55BFCDF6A7C371460E296 (String_t* ___password0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___challenge1, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** ___lm2, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** ___ntlm3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse2_Compute_NTLMv2_Session_mFB6537BF7FC8D9D7CCE55BFCDF6A7C371460E296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_1 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_3 = NULL;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		V_0 = L_0;
		RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * L_1 = RandomNumberGenerator_Create_mB84B1BA538B29D0679F310D3B574A7D5BAA890C4(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(6 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_1, L_2);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ___challenge1;
		NullCheck(L_3);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))), (int32_t)8)));
		V_1 = L_4;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = ___challenge1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = V_1;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_5);
		Array_CopyTo_m455300D414FFB0EBFE53EA4E8BBD31532006EBB7((RuntimeArray *)(RuntimeArray *)L_5, (RuntimeArray *)(RuntimeArray *)L_6, 0, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = ___challenge1;
		NullCheck(L_9);
		NullCheck((RuntimeArray *)(RuntimeArray *)L_7);
		Array_CopyTo_m455300D414FFB0EBFE53EA4E8BBD31532006EBB7((RuntimeArray *)(RuntimeArray *)L_7, (RuntimeArray *)(RuntimeArray *)L_8, (((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_10 = ___lm2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24));
		*((RuntimeObject **)L_10) = (RuntimeObject *)L_11;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_10, (RuntimeObject *)L_11);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_13 = ___lm2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = *((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821**)L_13);
		NullCheck((RuntimeArray *)(RuntimeArray *)L_12);
		Array_CopyTo_m455300D414FFB0EBFE53EA4E8BBD31532006EBB7((RuntimeArray *)(RuntimeArray *)L_12, (RuntimeArray *)(RuntimeArray *)L_14, 0, /*hidden argument*/NULL);
		MD5_tCED753745572EC20FE5D31D15F132736B5343EE6 * L_15 = MD5_Create_m87EB14601AD6AF168032C29DA938E18454CA05AE(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = V_1;
		NullCheck(L_15);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_17 = HashAlgorithm_ComputeHash_m18501D3068AEBEB5FA83EA72BE780E371DB0C122(L_15, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_18 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		V_3 = L_18;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_19 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = V_3;
		Array_Copy_m2D96731C600DE8A167348CA8BA796344E64F7434((RuntimeArray *)(RuntimeArray *)L_19, (RuntimeArray *)(RuntimeArray *)L_20, 8, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_21 = ___ntlm3;
		String_t* L_22 = ___password0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = ChallengeResponse2_Compute_NTLM_mA1DCA878A3A7A5517DB8BB0F2BEDD29353573976(L_22, L_23, /*hidden argument*/NULL);
		*((RuntimeObject **)L_21) = (RuntimeObject *)L_24;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_21, (RuntimeObject *)L_24);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_25 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_26 = V_0;
		NullCheck(L_26);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_25, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_26)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_27 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_28 = V_1;
		NullCheck(L_28);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_27, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_28)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_29 = V_3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_30 = V_3;
		NullCheck(L_30);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_29, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_30)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_31 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_32 = V_2;
		NullCheck(L_32);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_31, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_32)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute_NTLMv2(Mono.Security.Protocol.Ntlm.Type2Message,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_Compute_NTLMv2_mBCFF2DF7375AD035B98FC84253201D269201CE43 (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * ___type20, String_t* ___username1, String_t* ___password2, String_t* ___domain3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse2_Compute_NTLMv2_mBCFF2DF7375AD035B98FC84253201D269201CE43_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_1 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_3 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_4 = NULL;
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  V_5;
	memset(&V_5, 0, sizeof(V_5));
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_6 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_7 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_8 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_9 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_10 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_11 = NULL;
	{
		String_t* L_0 = ___password2;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ChallengeResponse2_Compute_NTLM_Password_mD27D4A18DBD712B0E80B0F2A1CB296B353312C41(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_2 = Encoding_get_Unicode_m86CC470F70F9BB52DDB26721F0C0D6EDAFC318AA(/*hidden argument*/NULL);
		String_t* L_3 = ___username1;
		NullCheck(L_3);
		String_t* L_4 = String_ToUpperInvariant_m0AA42416F4CACA4D0E3B89D97E534D88AB136338(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = VirtFuncInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_2, L_4);
		V_1 = L_5;
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_6 = Encoding_get_Unicode_m86CC470F70F9BB52DDB26721F0C0D6EDAFC318AA(/*hidden argument*/NULL);
		String_t* L_7 = ___domain3;
		NullCheck(L_6);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = VirtFuncInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, L_7);
		V_2 = L_8;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = V_1;
		NullCheck(L_9);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = V_2;
		NullCheck(L_10);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))));
		V_3 = L_11;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = V_3;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_12);
		Array_CopyTo_m455300D414FFB0EBFE53EA4E8BBD31532006EBB7((RuntimeArray *)(RuntimeArray *)L_12, (RuntimeArray *)(RuntimeArray *)L_13, 0, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = V_3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = V_1;
		NullCheck(L_16);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_17 = V_2;
		NullCheck(L_17);
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_14, 0, (RuntimeArray *)(RuntimeArray *)L_15, (((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))), (((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_18 = V_0;
		HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B * L_19 = (HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B *)il2cpp_codegen_object_new(HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B_il2cpp_TypeInfo_var);
		HMACMD5__ctor_m246E639FCF66A7C0A443CF06F461A3C5C26EB6F9(L_19, L_18, /*hidden argument*/NULL);
		HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B * L_20 = L_19;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_21 = V_3;
		NullCheck(L_20);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_22 = HashAlgorithm_ComputeHash_m18501D3068AEBEB5FA83EA72BE780E371DB0C122(L_20, L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = V_0;
		NullCheck(L_24);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_23, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_20);
		HashAlgorithm_Clear_m2E975EB7B42C1E241B24578CDF15AD41F35A5A8D(L_20, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_25 = V_4;
		HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B * L_26 = (HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B *)il2cpp_codegen_object_new(HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B_il2cpp_TypeInfo_var);
		HMACMD5__ctor_m246E639FCF66A7C0A443CF06F461A3C5C26EB6F9(L_26, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_il2cpp_TypeInfo_var);
		DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  L_27 = DateTime_get_Now_mB464D30F15C97069F92C1F910DCDDC3DFCC7F7D2(/*hidden argument*/NULL);
		V_5 = L_27;
		int64_t L_28 = DateTime_get_Ticks_mBCB529E43D065E498EAF08971D2EB49D5CB59D60((DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 *)(&V_5), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_29 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		V_6 = L_29;
		RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * L_30 = RandomNumberGenerator_Create_mB84B1BA538B29D0679F310D3B574A7D5BAA890C4(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_31 = V_6;
		NullCheck(L_30);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(6 /* System.Void System.Security.Cryptography.RandomNumberGenerator::GetBytes(System.Byte[]) */, L_30, L_31);
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_32 = ___type20;
		NullCheck(L_32);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_33 = Type2Message_get_TargetInfo_m5E0F0E5A6B32B7512393EDC2DFE9E8A6D79DB485(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_34 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)28), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_33)->max_length)))))));
		V_7 = L_34;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_35 = V_7;
		NullCheck(L_35);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)1);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_36 = V_7;
		NullCheck(L_36);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)1);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_37 = BitConverterLE_GetBytes_m8B2BFEBCB4830C010E4572C925AE3C3A3CC14031(((int64_t)il2cpp_codegen_subtract((int64_t)L_28, (int64_t)((int64_t)504911232000000000LL))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_38 = V_7;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_37, 0, (RuntimeArray *)(RuntimeArray *)L_38, 8, 8, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_39 = V_6;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_40 = V_7;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_39, 0, (RuntimeArray *)(RuntimeArray *)L_40, ((int32_t)16), 8, /*hidden argument*/NULL);
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_41 = ___type20;
		NullCheck(L_41);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_42 = Type2Message_get_TargetInfo_m5E0F0E5A6B32B7512393EDC2DFE9E8A6D79DB485(L_41, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_43 = V_7;
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_44 = ___type20;
		NullCheck(L_44);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_45 = Type2Message_get_TargetInfo_m5E0F0E5A6B32B7512393EDC2DFE9E8A6D79DB485(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_42, 0, (RuntimeArray *)(RuntimeArray *)L_43, ((int32_t)28), (((int32_t)((int32_t)(((RuntimeArray *)L_45)->max_length)))), /*hidden argument*/NULL);
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_46 = ___type20;
		NullCheck(L_46);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_47 = Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1(L_46, /*hidden argument*/NULL);
		V_8 = L_47;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_48 = V_8;
		NullCheck(L_48);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_49 = V_7;
		NullCheck(L_49);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_50 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_48)->max_length)))), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_49)->max_length)))))));
		V_9 = L_50;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_51 = V_8;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_52 = V_9;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_51);
		Array_CopyTo_m455300D414FFB0EBFE53EA4E8BBD31532006EBB7((RuntimeArray *)(RuntimeArray *)L_51, (RuntimeArray *)(RuntimeArray *)L_52, 0, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_53 = V_7;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_54 = V_9;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_55 = V_8;
		NullCheck(L_55);
		NullCheck((RuntimeArray *)(RuntimeArray *)L_53);
		Array_CopyTo_m455300D414FFB0EBFE53EA4E8BBD31532006EBB7((RuntimeArray *)(RuntimeArray *)L_53, (RuntimeArray *)(RuntimeArray *)L_54, (((int32_t)((int32_t)(((RuntimeArray *)L_55)->max_length)))), /*hidden argument*/NULL);
		HMACMD5_t8C6693E41EEA9BF26BBAF880B405CC170C43F11B * L_56 = L_26;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_57 = V_9;
		NullCheck(L_56);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_58 = HashAlgorithm_ComputeHash_m18501D3068AEBEB5FA83EA72BE780E371DB0C122(L_56, L_57, /*hidden argument*/NULL);
		V_10 = L_58;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_59 = V_7;
		NullCheck(L_59);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_60 = V_10;
		NullCheck(L_60);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_61 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_59)->max_length)))), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_60)->max_length)))))));
		V_11 = L_61;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_62 = V_10;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_63 = V_11;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_62);
		Array_CopyTo_m455300D414FFB0EBFE53EA4E8BBD31532006EBB7((RuntimeArray *)(RuntimeArray *)L_62, (RuntimeArray *)(RuntimeArray *)L_63, 0, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_64 = V_7;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_65 = V_11;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_66 = V_10;
		NullCheck(L_66);
		NullCheck((RuntimeArray *)(RuntimeArray *)L_64);
		Array_CopyTo_m455300D414FFB0EBFE53EA4E8BBD31532006EBB7((RuntimeArray *)(RuntimeArray *)L_64, (RuntimeArray *)(RuntimeArray *)L_65, (((int32_t)((int32_t)(((RuntimeArray *)L_66)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_67 = V_4;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_68 = V_4;
		NullCheck(L_68);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_67, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_68)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_56);
		HashAlgorithm_Clear_m2E975EB7B42C1E241B24578CDF15AD41F35A5A8D(L_56, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_69 = V_6;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_70 = V_6;
		NullCheck(L_70);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_69, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_70)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_71 = V_7;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_72 = V_7;
		NullCheck(L_72);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_71, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_72)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_73 = V_9;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_74 = V_9;
		NullCheck(L_74);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_73, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_74)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_75 = V_10;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_76 = V_10;
		NullCheck(L_76);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_75, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_76)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_77 = V_11;
		return L_77;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse2::Compute(Mono.Security.Protocol.Ntlm.Type2Message,Mono.Security.Protocol.Ntlm.NtlmAuthLevel,System.String,System.String,System.String,System.Byte[]U26,System.Byte[]U26)
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse2_Compute_mAA312CA925226C75A829516B6BDC2089840D389D (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * ___type20, int32_t ___level1, String_t* ___username2, String_t* ___password3, String_t* ___domain4, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** ___lm5, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** ___ntlm6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse2_Compute_mAA312CA925226C75A829516B6BDC2089840D389D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_0 = ___lm5;
		*((RuntimeObject **)L_0) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_0, (RuntimeObject *)NULL);
		int32_t L_1 = ___level1;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_003b;
			}
			case 2:
			{
				goto IL_005a;
			}
			case 3:
			{
				goto IL_0089;
			}
		}
	}
	{
		goto IL_0097;
	}

IL_001c:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_2 = ___lm5;
		String_t* L_3 = ___password3;
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_4 = ___type20;
		NullCheck(L_4);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = ChallengeResponse2_Compute_LM_m3A1F9371E4F1E41B044787FFB3BE985E92A3AC22(L_3, L_5, /*hidden argument*/NULL);
		*((RuntimeObject **)L_2) = (RuntimeObject *)L_6;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_2, (RuntimeObject *)L_6);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_7 = ___ntlm6;
		String_t* L_8 = ___password3;
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_9 = ___type20;
		NullCheck(L_9);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1(L_9, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = ChallengeResponse2_Compute_NTLM_mA1DCA878A3A7A5517DB8BB0F2BEDD29353573976(L_8, L_10, /*hidden argument*/NULL);
		*((RuntimeObject **)L_7) = (RuntimeObject *)L_11;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_7, (RuntimeObject *)L_11);
		return;
	}

IL_003b:
	{
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_12 = ___type20;
		NullCheck(L_12);
		int32_t L_13 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)L_12)->get__flags_2();
		if (!((int32_t)((int32_t)L_13&(int32_t)((int32_t)524288))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_14 = ___password3;
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_15 = ___type20;
		NullCheck(L_15);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1(L_15, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_17 = ___lm5;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_18 = ___ntlm6;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ChallengeResponse2_Compute_NTLMv2_Session_mFB6537BF7FC8D9D7CCE55BFCDF6A7C371460E296(L_14, L_16, (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821**)L_17, (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821**)L_18, /*hidden argument*/NULL);
		return;
	}

IL_005a:
	{
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_19 = ___type20;
		NullCheck(L_19);
		int32_t L_20 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)L_19)->get__flags_2();
		if (!((int32_t)((int32_t)L_20&(int32_t)((int32_t)524288))))
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_21 = ___password3;
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_22 = ___type20;
		NullCheck(L_22);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1(L_22, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_24 = ___lm5;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_25 = ___ntlm6;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ChallengeResponse2_Compute_NTLMv2_Session_mFB6537BF7FC8D9D7CCE55BFCDF6A7C371460E296(L_21, L_23, (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821**)L_24, (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821**)L_25, /*hidden argument*/NULL);
		return;
	}

IL_0079:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_26 = ___ntlm6;
		String_t* L_27 = ___password3;
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_28 = ___type20;
		NullCheck(L_28);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_29 = Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_30 = ChallengeResponse2_Compute_NTLM_mA1DCA878A3A7A5517DB8BB0F2BEDD29353573976(L_27, L_29, /*hidden argument*/NULL);
		*((RuntimeObject **)L_26) = (RuntimeObject *)L_30;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_26, (RuntimeObject *)L_30);
		return;
	}

IL_0089:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_31 = ___ntlm6;
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_32 = ___type20;
		String_t* L_33 = ___username2;
		String_t* L_34 = ___password3;
		String_t* L_35 = ___domain4;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_36 = ChallengeResponse2_Compute_NTLMv2_mBCFF2DF7375AD035B98FC84253201D269201CE43(L_32, L_33, L_34, L_35, /*hidden argument*/NULL);
		*((RuntimeObject **)L_31) = (RuntimeObject *)L_36;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_31, (RuntimeObject *)L_36);
		return;
	}

IL_0097:
	{
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_37 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_37, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_37, NULL, ChallengeResponse2_Compute_mAA312CA925226C75A829516B6BDC2089840D389D_RuntimeMethod_var);
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::GetResponse(System.Byte[],System.Byte[])
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_GetResponse_mE39699CD2453921E373BF9768180993EDCD810E7 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___challenge0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pwd1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse2_GetResponse_mE39699CD2453921E373BF9768180993EDCD810E7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24));
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0_il2cpp_TypeInfo_var);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_1 = DES_Create_m5EE267FBCD5AA18E04C29247C796430D12247CC5(/*hidden argument*/NULL);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_2 = L_1;
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Mode(System.Security.Cryptography.CipherMode) */, L_2, 2);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_3 = L_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ___pwd1;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = ChallengeResponse2_PrepareDESKey_m428EF8F37B18E0B4FC5895BFF02A681740CF7608(L_4, 0, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(11 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_3, L_5);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_6 = L_3;
		NullCheck(L_6);
		RuntimeObject* L_7 = VirtFuncInvoker0< RuntimeObject* >::Invoke(16 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_6);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = ___challenge0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = V_0;
		NullCheck(L_7);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var, L_7, L_8, 0, 8, L_9, 0);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_10 = L_6;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = ___pwd1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = ChallengeResponse2_PrepareDESKey_m428EF8F37B18E0B4FC5895BFF02A681740CF7608(L_11, 7, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(11 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_10, L_12);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_13 = L_10;
		NullCheck(L_13);
		RuntimeObject* L_14 = VirtFuncInvoker0< RuntimeObject* >::Invoke(16 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_13);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = ___challenge0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = V_0;
		NullCheck(L_14);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var, L_14, L_15, 0, 8, L_16, 8);
		DES_tFB993FE8AF9722A555B0737FE730332CCD86F6F0 * L_17 = L_13;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_18 = ___pwd1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_19 = ChallengeResponse2_PrepareDESKey_m428EF8F37B18E0B4FC5895BFF02A681740CF7608(L_18, ((int32_t)14), /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(11 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_17, L_19);
		NullCheck(L_17);
		RuntimeObject* L_20 = VirtFuncInvoker0< RuntimeObject* >::Invoke(16 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_17);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_21 = ___challenge0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_22 = V_0;
		NullCheck(L_20);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9_il2cpp_TypeInfo_var, L_20, L_21, 0, 8, L_22, ((int32_t)16));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = V_0;
		return L_23;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::PrepareDESKey(System.Byte[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_PrepareDESKey_m428EF8F37B18E0B4FC5895BFF02A681740CF7608 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key56bits0, int32_t ___position1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse2_PrepareDESKey_m428EF8F37B18E0B4FC5895BFF02A681740CF7608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = L_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___key56bits0;
		int32_t L_3 = ___position1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_5);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = L_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ___key56bits0;
		int32_t L_8 = ___position1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		uint8_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = ___key56bits0;
		int32_t L_12 = ___position1;
		NullCheck(L_11);
		int32_t L_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
		uint8_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10<<(int32_t)7))|(int32_t)((int32_t)((int32_t)L_14>>(int32_t)1))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = L_6;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = ___key56bits0;
		int32_t L_17 = ___position1;
		NullCheck(L_16);
		int32_t L_18 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
		uint8_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = ___key56bits0;
		int32_t L_21 = ___position1;
		NullCheck(L_20);
		int32_t L_22 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)2));
		uint8_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_19<<(int32_t)6))|(int32_t)((int32_t)((int32_t)L_23>>(int32_t)2))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = L_15;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_25 = ___key56bits0;
		int32_t L_26 = ___position1;
		NullCheck(L_25);
		int32_t L_27 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)2));
		uint8_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_29 = ___key56bits0;
		int32_t L_30 = ___position1;
		NullCheck(L_29);
		int32_t L_31 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)3));
		uint8_t L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_28<<(int32_t)5))|(int32_t)((int32_t)((int32_t)L_32>>(int32_t)3))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_33 = L_24;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_34 = ___key56bits0;
		int32_t L_35 = ___position1;
		NullCheck(L_34);
		int32_t L_36 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)3));
		uint8_t L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_38 = ___key56bits0;
		int32_t L_39 = ___position1;
		NullCheck(L_38);
		int32_t L_40 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)4));
		uint8_t L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		NullCheck(L_33);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_37<<(int32_t)4))|(int32_t)((int32_t)((int32_t)L_41>>(int32_t)4))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_42 = L_33;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_43 = ___key56bits0;
		int32_t L_44 = ___position1;
		NullCheck(L_43);
		int32_t L_45 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)4));
		uint8_t L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_47 = ___key56bits0;
		int32_t L_48 = ___position1;
		NullCheck(L_47);
		int32_t L_49 = ((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)5));
		uint8_t L_50 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		NullCheck(L_42);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_46<<(int32_t)3))|(int32_t)((int32_t)((int32_t)L_50>>(int32_t)5))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_51 = L_42;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_52 = ___key56bits0;
		int32_t L_53 = ___position1;
		NullCheck(L_52);
		int32_t L_54 = ((int32_t)il2cpp_codegen_add((int32_t)L_53, (int32_t)5));
		uint8_t L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_56 = ___key56bits0;
		int32_t L_57 = ___position1;
		NullCheck(L_56);
		int32_t L_58 = ((int32_t)il2cpp_codegen_add((int32_t)L_57, (int32_t)6));
		uint8_t L_59 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		NullCheck(L_51);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_55<<(int32_t)2))|(int32_t)((int32_t)((int32_t)L_59>>(int32_t)6))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_60 = L_51;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_61 = ___key56bits0;
		int32_t L_62 = ___position1;
		NullCheck(L_61);
		int32_t L_63 = ((int32_t)il2cpp_codegen_add((int32_t)L_62, (int32_t)6));
		uint8_t L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		NullCheck(L_60);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_64<<(int32_t)1))))));
		return L_60;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.ChallengeResponse2::PasswordToKey(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ChallengeResponse2_PasswordToKey_m2871E605818DF2DE4BC5B1B163831BA8F64006D8 (String_t* ___password0, int32_t ___position1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse2_PasswordToKey_m2871E605818DF2DE4BC5B1B163831BA8F64006D8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)7);
		V_0 = L_0;
		String_t* L_1 = ___password0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_m_stringLength_0();
		int32_t L_3 = ___position1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		int32_t L_4 = Math_Min_mC950438198519FB2B0260FCB91220847EE4BB525(((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_3)), 7, /*hidden argument*/NULL);
		V_1 = L_4;
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_5 = Encoding_get_ASCII_m9B673AE3152AB04D07CADE6E5E142C785B5BC94E(/*hidden argument*/NULL);
		String_t* L_6 = ___password0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var);
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_7 = CultureInfo_get_CurrentCulture_mD86F3D8E5D332FB304F80D9B9CA4DE849C2A6831(/*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_8 = String_ToUpper_m8C69D974350ABA8BA0BC3A66996004CCEFD64293(L_6, L_7, /*hidden argument*/NULL);
		int32_t L_9 = ___position1;
		int32_t L_10 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = V_0;
		NullCheck(L_5);
		VirtFuncInvoker5< int32_t, String_t*, int32_t, int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t >::Invoke(17 /* System.Int32 System.Text.Encoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32) */, L_5, L_8, L_9, L_10, L_11, 0);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = ChallengeResponse2_PrepareDESKey_m428EF8F37B18E0B4FC5895BFF02A681740CF7608(L_12, 0, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = V_0;
		NullCheck(L_15);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_14, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_15)->max_length)))), /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.ChallengeResponse2::.cctor()
extern "C" IL2CPP_METHOD_ATTR void ChallengeResponse2__cctor_mCF5C3FE5989C7BB7777C3BAADD67E9F8576A8C23 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChallengeResponse2__cctor_mCF5C3FE5989C7BB7777C3BAADD67E9F8576A8C23_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = L_0;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA____AEA5F1CC5CFE1660539EDD691FE017F775F63A0D_2_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_1, L_2, /*hidden argument*/NULL);
		((ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var))->set_magic_0(L_1);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = L_3;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_5 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA____16968835DEF6DD3BB86EABA9DEC53BF41851CD6D_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_4, L_5, /*hidden argument*/NULL);
		((ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_StaticFields*)il2cpp_codegen_static_fields_for(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var))->set_nullEncMagic_1(L_4);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Protocol.Ntlm.MessageBase::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MessageBase__ctor_m32BA1280CB1020E88C6EF6DFA9C3ABABF3B36E59 (MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 * __this, int32_t ___messageType0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___messageType0;
		__this->set__type_1(L_0);
		return;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.MessageBase::PrepareMessage(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* MessageBase_PrepareMessage_m6B0C0C463C16D086924EC49DB07C3ADE95C11958 (MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 * __this, int32_t ___messageSize0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageBase_PrepareMessage_m6B0C0C463C16D086924EC49DB07C3ADE95C11958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		int32_t L_0 = ___messageSize0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_0);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_StaticFields*)il2cpp_codegen_static_fields_for(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var))->get_header_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = V_0;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_2, 0, (RuntimeArray *)(RuntimeArray *)L_3, 0, 8, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = V_0;
		int32_t L_5 = __this->get__type_1();
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(8), (uint8_t)(((int32_t)((uint8_t)L_5))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = V_0;
		int32_t L_7 = __this->get__type_1();
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_7>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = V_0;
		int32_t L_9 = __this->get__type_1();
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_9>>(int32_t)((int32_t)16)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = V_0;
		int32_t L_11 = __this->get__type_1();
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_11>>(int32_t)((int32_t)24)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = V_0;
		return L_12;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.MessageBase::Decode(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537 (MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___message0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, _stringLiteral6F9B9AF3CD6E8B8A73C2CDCED37FE9F59226E27D, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537_RuntimeMethod_var);
	}

IL_000e:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___message0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))) >= ((int32_t)((int32_t)12))))
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m67B66557188C94648AA7A23F6A7501BE7D455ADA(_stringLiteral43C5083891C69B860FC78499995E820029745FE8, /*hidden argument*/NULL);
		V_0 = L_3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ___message0;
		NullCheck(L_4);
		int32_t L_5 = (((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length))));
		RuntimeObject * L_6 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7 = V_0;
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_8 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m755B01B4B4595B447596E3281F22FD7CE6DAE378(L_8, _stringLiteral6F9B9AF3CD6E8B8A73C2CDCED37FE9F59226E27D, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, NULL, MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537_RuntimeMethod_var);
	}

IL_0034:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = ___message0;
		bool L_10 = MessageBase_CheckHeader_m427014E264FA451B68062CFF8A1939DC3A04FB5A(__this, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m67B66557188C94648AA7A23F6A7501BE7D455ADA(_stringLiteralB8B0EDE7ABBF3F7F6738DC0C3F6D05656BAD431B, /*hidden argument*/NULL);
		int32_t L_12 = __this->get__type_1();
		int32_t L_13 = L_12;
		RuntimeObject * L_14 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_13);
		String_t* L_15 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(L_11, L_14, /*hidden argument*/NULL);
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_16 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m26DC3463C6F3C98BF33EA39598DD2B32F0249CA8(L_16, L_15, _stringLiteral6F9B9AF3CD6E8B8A73C2CDCED37FE9F59226E27D, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16, NULL, MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537_RuntimeMethod_var);
	}

IL_0062:
	{
		return;
	}
}
// System.Boolean Mono.Security.Protocol.Ntlm.MessageBase::CheckHeader(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR bool MessageBase_CheckHeader_m427014E264FA451B68062CFF8A1939DC3A04FB5A (MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageBase_CheckHeader_m427014E264FA451B68062CFF8A1939DC3A04FB5A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0016;
	}

IL_0004:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___message0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		IL2CPP_RUNTIME_CLASS_INIT(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_StaticFields*)il2cpp_codegen_static_fields_for(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var))->get_header_0();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		if ((((int32_t)L_3) == ((int32_t)L_7)))
		{
			goto IL_0012;
		}
	}
	{
		return (bool)0;
	}

IL_0012:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0016:
	{
		int32_t L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_StaticFields*)il2cpp_codegen_static_fields_for(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var))->get_header_0();
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = ___message0;
		uint32_t L_12 = BitConverterLE_ToUInt32_m898E287439DDB0A31E4E39FF83DD032B1C43FA26(L_11, 8, /*hidden argument*/NULL);
		int32_t L_13 = __this->get__type_1();
		return (bool)((((int64_t)(((int64_t)((uint64_t)L_12)))) == ((int64_t)(((int64_t)((int64_t)L_13)))))? 1 : 0);
	}
}
// System.Void Mono.Security.Protocol.Ntlm.MessageBase::.cctor()
extern "C" IL2CPP_METHOD_ATTR void MessageBase__cctor_m4DF505F352AB2529D7E6EE09E6B096C076663B0F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageBase__cctor_m4DF505F352AB2529D7E6EE09E6B096C076663B0F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = L_0;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tF21437707AFAA06797AEDEE07C84D4D3CC2837FA____6FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF_1_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_1, L_2, /*hidden argument*/NULL);
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_StaticFields*)il2cpp_codegen_static_fields_for(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var))->set_header_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Protocol.Ntlm.NtlmSettings::.cctor()
extern "C" IL2CPP_METHOD_ATTR void NtlmSettings__cctor_mCE3B588B6BFFB63D73ECA11941E875814A5CE9BB (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NtlmSettings__cctor_mCE3B588B6BFFB63D73ECA11941E875814A5CE9BB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_StaticFields*)il2cpp_codegen_static_fields_for(NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_il2cpp_TypeInfo_var))->set_defaultAuthLevel_0(1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Protocol.Ntlm.Type1Message::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Type1Message__ctor_mF11CFA44C4BF05765612B0D3CCBB4EBB433D7B23 (Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type1Message__ctor_mF11CFA44C4BF05765612B0D3CCBB4EBB433D7B23_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var);
		MessageBase__ctor_m32BA1280CB1020E88C6EF6DFA9C3ABABF3B36E59(__this, 1, /*hidden argument*/NULL);
		String_t* L_0 = Environment_get_UserDomainName_mC55D253D7319CBE9030836E420FF9518921C3A52(/*hidden argument*/NULL);
		__this->set__domain_4(L_0);
		String_t* L_1 = Environment_get_MachineName_m0300D26C1A5348D90800793717D33B1F629AF10D(/*hidden argument*/NULL);
		__this->set__host_3(L_1);
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)45575));
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.Type1Message::set_Domain(System.String)
extern "C" IL2CPP_METHOD_ATTR void Type1Message_set_Domain_m14675CA2220D6338E39DA862B822553AE6DCFD12 (Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type1Message_set_Domain_m14675CA2220D6338E39DA862B822553AE6DCFD12_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		___value0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_000a:
	{
		String_t* L_1 = ___value0;
		bool L_2 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_1, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)((int32_t)L_3&(int32_t)((int32_t)-4097))));
		goto IL_003d;
	}

IL_002b:
	{
		int32_t L_4 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)((int32_t)L_4|(int32_t)((int32_t)4096))));
	}

IL_003d:
	{
		String_t* L_5 = ___value0;
		__this->set__domain_4(L_5);
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.Type1Message::set_Host(System.String)
extern "C" IL2CPP_METHOD_ATTR void Type1Message_set_Host_mC0ADD586CC5A1F9FD5489AF1D8F14DF9AA4F2D68 (Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type1Message_set_Host_mC0ADD586CC5A1F9FD5489AF1D8F14DF9AA4F2D68_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		___value0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_000a:
	{
		String_t* L_1 = ___value0;
		bool L_2 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_1, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)((int32_t)L_3&(int32_t)((int32_t)-8193))));
		goto IL_003d;
	}

IL_002b:
	{
		int32_t L_4 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)((int32_t)L_4|(int32_t)((int32_t)8192))));
	}

IL_003d:
	{
		String_t* L_5 = ___value0;
		__this->set__host_3(L_5);
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.Type1Message::Decode(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void Type1Message_Decode_mBDBDE9E060DFA07CC9A93D2491446B9411E5D2EC (Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___message0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___message0;
		MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537(__this, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ___message0;
		uint32_t L_2 = BitConverterLE_ToUInt32_m898E287439DDB0A31E4E39FF83DD032B1C43FA26(L_1, ((int32_t)12), /*hidden argument*/NULL);
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(L_2);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ___message0;
		uint16_t L_4 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_3, ((int32_t)16), /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = ___message0;
		uint16_t L_6 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_5, ((int32_t)20), /*hidden argument*/NULL);
		V_1 = L_6;
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_7 = Encoding_get_ASCII_m9B673AE3152AB04D07CADE6E5E142C785B5BC94E(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = ___message0;
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		NullCheck(L_7);
		String_t* L_11 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, L_9, L_10);
		__this->set__domain_4(L_11);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = ___message0;
		uint16_t L_13 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_12, ((int32_t)24), /*hidden argument*/NULL);
		V_2 = L_13;
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_14 = Encoding_get_ASCII_m9B673AE3152AB04D07CADE6E5E142C785B5BC94E(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = ___message0;
		int32_t L_16 = V_2;
		NullCheck(L_14);
		String_t* L_17 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_14, L_15, ((int32_t)32), L_16);
		__this->set__host_3(L_17);
		return;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.Type1Message::GetBytes()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Type1Message_GetBytes_m5705CD87750C73B44B8700A02FF0E4C2DE294C9E (Type1Message_tF2DA0014BB300ABA864D84752FFA278EC6E6519C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type1Message_GetBytes_m5705CD87750C73B44B8700A02FF0E4C2DE294C9E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int16_t V_0 = 0;
	int16_t V_1 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	int16_t V_3 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_4 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_5 = NULL;
	{
		String_t* L_0 = __this->get__domain_4();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_m_stringLength_0();
		V_0 = (((int16_t)((int16_t)L_1)));
		String_t* L_2 = __this->get__host_3();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_m_stringLength_0();
		V_1 = (((int16_t)((int16_t)L_3)));
		int16_t L_4 = V_0;
		int16_t L_5 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = MessageBase_PrepareMessage_m6B0C0C463C16D086924EC49DB07C3ADE95C11958(__this, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)32), (int32_t)L_4)), (int32_t)L_5)), /*hidden argument*/NULL);
		V_2 = L_6;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = V_2;
		int32_t L_8 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (uint8_t)(((int32_t)((uint8_t)L_8))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = V_2;
		int32_t L_10 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_10>>8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = V_2;
		int32_t L_12 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_12>>((int32_t)16)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = V_2;
		int32_t L_14 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_14>>((int32_t)24)))))));
		int16_t L_15 = V_1;
		V_3 = (((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)32), (int32_t)L_15)))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = V_2;
		int16_t L_17 = V_0;
		NullCheck(L_16);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (uint8_t)(((int32_t)((uint8_t)L_17))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_18 = V_2;
		int16_t L_19 = V_0;
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_19>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_21 = V_2;
		NullCheck(L_21);
		int32_t L_22 = ((int32_t)16);
		uint8_t L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (uint8_t)L_23);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_25 = V_2;
		NullCheck(L_25);
		int32_t L_26 = ((int32_t)17);
		uint8_t L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (uint8_t)L_27);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_28 = V_2;
		int16_t L_29 = V_3;
		NullCheck(L_28);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (uint8_t)(((int32_t)((uint8_t)L_29))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_30 = V_2;
		int16_t L_31 = V_3;
		NullCheck(L_30);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_31>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_32 = V_2;
		int16_t L_33 = V_1;
		NullCheck(L_32);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)24)), (uint8_t)(((int32_t)((uint8_t)L_33))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_34 = V_2;
		int16_t L_35 = V_1;
		NullCheck(L_34);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)25)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_35>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_36 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_37 = V_2;
		NullCheck(L_37);
		int32_t L_38 = ((int32_t)24);
		uint8_t L_39 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		NullCheck(L_36);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)26)), (uint8_t)L_39);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_40 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_41 = V_2;
		NullCheck(L_41);
		int32_t L_42 = ((int32_t)25);
		uint8_t L_43 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_40);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)27)), (uint8_t)L_43);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_44 = V_2;
		NullCheck(L_44);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)28)), (uint8_t)((int32_t)32));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_45 = V_2;
		NullCheck(L_45);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)29)), (uint8_t)0);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_46 = Encoding_get_ASCII_m9B673AE3152AB04D07CADE6E5E142C785B5BC94E(/*hidden argument*/NULL);
		String_t* L_47 = __this->get__host_3();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var);
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_48 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
		NullCheck(L_47);
		String_t* L_49 = String_ToUpper_m8C69D974350ABA8BA0BC3A66996004CCEFD64293(L_47, L_48, /*hidden argument*/NULL);
		NullCheck(L_46);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_50 = VirtFuncInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_46, L_49);
		V_4 = L_50;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_51 = V_4;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_52 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_53 = V_4;
		NullCheck(L_53);
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_51, 0, (RuntimeArray *)(RuntimeArray *)L_52, ((int32_t)32), (((int32_t)((int32_t)(((RuntimeArray *)L_53)->max_length)))), /*hidden argument*/NULL);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_54 = Encoding_get_ASCII_m9B673AE3152AB04D07CADE6E5E142C785B5BC94E(/*hidden argument*/NULL);
		String_t* L_55 = __this->get__domain_4();
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_56 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
		NullCheck(L_55);
		String_t* L_57 = String_ToUpper_m8C69D974350ABA8BA0BC3A66996004CCEFD64293(L_55, L_56, /*hidden argument*/NULL);
		NullCheck(L_54);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_58 = VirtFuncInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_54, L_57);
		V_5 = L_58;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_59 = V_5;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_60 = V_2;
		int16_t L_61 = V_3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_62 = V_5;
		NullCheck(L_62);
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_59, 0, (RuntimeArray *)(RuntimeArray *)L_60, L_61, (((int32_t)((int32_t)(((RuntimeArray *)L_62)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_63 = V_2;
		return L_63;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Protocol.Ntlm.Type2Message::.ctor(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void Type2Message__ctor_m85E73F15F691FC25B93503B84B24D25805B5FBD9 (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type2Message__ctor_m85E73F15F691FC25B93503B84B24D25805B5FBD9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var);
		MessageBase__ctor_m32BA1280CB1020E88C6EF6DFA9C3ABABF3B36E59(__this, 2, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)8);
		__this->set__nonce_3(L_0);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ___message0;
		VirtActionInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(4 /* System.Void Mono.Security.Protocol.Ntlm.MessageBase::Decode(System.Byte[]) */, __this, L_1);
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.Type2Message::Finalize()
extern "C" IL2CPP_METHOD_ATTR void Type2Message_Finalize_mD48F7AD79A227C07BA7A47B4056D0BA1FD785CAA (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get__nonce_3();
			if (!L_0)
			{
				goto IL_001c;
			}
		}

IL_0008:
		{
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = __this->get__nonce_3();
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = __this->get__nonce_3();
			NullCheck(L_2);
			Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_1, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		}

IL_001c:
		{
			IL2CPP_LEAVE(0x25, FINALLY_001e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0025:
	{
		return;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::get_Nonce()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1 (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get__nonce_3();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_0);
		RuntimeObject * L_1 = Array_Clone_mE8C710213E323617A6F46F2B36DCDDD4C7CF5176((RuntimeArray *)(RuntimeArray *)L_0, /*hidden argument*/NULL);
		return ((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)Castclass((RuntimeObject*)L_1, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var));
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::get_TargetInfo()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Type2Message_get_TargetInfo_m5E0F0E5A6B32B7512393EDC2DFE9E8A6D79DB485 (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type2Message_get_TargetInfo_m5E0F0E5A6B32B7512393EDC2DFE9E8A6D79DB485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get__targetInfo_5();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_0);
		RuntimeObject * L_1 = Array_Clone_mE8C710213E323617A6F46F2B36DCDDD4C7CF5176((RuntimeArray *)(RuntimeArray *)L_0, /*hidden argument*/NULL);
		return ((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)Castclass((RuntimeObject*)L_1, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var));
	}
}
// System.Void Mono.Security.Protocol.Ntlm.Type2Message::Decode(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void Type2Message_Decode_m480774BBEA24F18E5C8765273C8DD6A641D9E441 (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type2Message_Decode_m480774BBEA24F18E5C8765273C8DD6A641D9E441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint16_t V_0 = 0;
	uint16_t V_1 = 0;
	uint16_t V_2 = 0;
	uint16_t V_3 = 0;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___message0;
		MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537(__this, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ___message0;
		uint32_t L_2 = BitConverterLE_ToUInt32_m898E287439DDB0A31E4E39FF83DD032B1C43FA26(L_1, ((int32_t)20), /*hidden argument*/NULL);
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(L_2);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ___message0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = __this->get__nonce_3();
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_3, ((int32_t)24), (RuntimeArray *)(RuntimeArray *)L_4, 0, 8, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = ___message0;
		uint16_t L_6 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_5, ((int32_t)12), /*hidden argument*/NULL);
		V_0 = L_6;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ___message0;
		uint16_t L_8 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_7, ((int32_t)16), /*hidden argument*/NULL);
		V_1 = L_8;
		uint16_t L_9 = V_0;
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_006d;
		}
	}
	{
		int32_t L_10 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		if (!((int32_t)((int32_t)L_10&(int32_t)2)))
		{
			goto IL_005a;
		}
	}
	{
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_11 = Encoding_get_ASCII_m9B673AE3152AB04D07CADE6E5E142C785B5BC94E(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = ___message0;
		uint16_t L_13 = V_1;
		uint16_t L_14 = V_0;
		NullCheck(L_11);
		String_t* L_15 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_11, L_12, L_13, L_14);
		__this->set__targetName_4(L_15);
		goto IL_006d;
	}

IL_005a:
	{
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_16 = Encoding_get_Unicode_m86CC470F70F9BB52DDB26721F0C0D6EDAFC318AA(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_17 = ___message0;
		uint16_t L_18 = V_1;
		uint16_t L_19 = V_0;
		NullCheck(L_16);
		String_t* L_20 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_16, L_17, L_18, L_19);
		__this->set__targetName_4(L_20);
	}

IL_006d:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_21 = ___message0;
		NullCheck(L_21);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_21)->max_length))))) < ((int32_t)((int32_t)48))))
		{
			goto IL_00a5;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_22 = ___message0;
		uint16_t L_23 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_22, ((int32_t)40), /*hidden argument*/NULL);
		V_2 = L_23;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = ___message0;
		uint16_t L_25 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_24, ((int32_t)44), /*hidden argument*/NULL);
		V_3 = L_25;
		uint16_t L_26 = V_2;
		if ((((int32_t)L_26) <= ((int32_t)0)))
		{
			goto IL_00a5;
		}
	}
	{
		uint16_t L_27 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_28 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_27);
		__this->set__targetInfo_5(L_28);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_29 = ___message0;
		uint16_t L_30 = V_3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_31 = __this->get__targetInfo_5();
		uint16_t L_32 = V_2;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_29, L_30, (RuntimeArray *)(RuntimeArray *)L_31, 0, L_32, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		return;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::GetBytes()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Type2Message_GetBytes_m91FFB26859C2D17A9D3830FF05458729F8B1AF63 (Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * __this, const RuntimeMethod* method)
{
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	int16_t V_1 = 0;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = MessageBase_PrepareMessage_m6B0C0C463C16D086924EC49DB07C3ADE95C11958(__this, ((int32_t)40), /*hidden argument*/NULL);
		V_0 = L_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = V_0;
		NullCheck(L_1);
		V_1 = (((int16_t)((int16_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = V_0;
		int16_t L_3 = V_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (uint8_t)(((int32_t)((uint8_t)L_3))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = V_0;
		int16_t L_5 = V_1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_5>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = V_0;
		int32_t L_7 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (uint8_t)(((int32_t)((uint8_t)L_7))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = V_0;
		int32_t L_9 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_9>>8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = V_0;
		int32_t L_11 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_11>>((int32_t)16)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = V_0;
		int32_t L_13 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_13>>((int32_t)24)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = __this->get__nonce_3();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = __this->get__nonce_3();
		NullCheck(L_16);
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_14, 0, (RuntimeArray *)(RuntimeArray *)L_15, ((int32_t)24), (((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_17 = V_0;
		return L_17;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Security.Protocol.Ntlm.Type3Message::.ctor(Mono.Security.Protocol.Ntlm.Type2Message)
extern "C" IL2CPP_METHOD_ATTR void Type3Message__ctor_mBA583598EA5F842A0076F882C4A3205B919419D6 (Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C * __this, Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * ___type20, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type3Message__ctor_mBA583598EA5F842A0076F882C4A3205B919419D6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0_il2cpp_TypeInfo_var);
		MessageBase__ctor_m32BA1280CB1020E88C6EF6DFA9C3ABABF3B36E59(__this, 3, /*hidden argument*/NULL);
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_0 = ___type20;
		__this->set__type2_9(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_il2cpp_TypeInfo_var);
		int32_t L_1 = ((NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_StaticFields*)il2cpp_codegen_static_fields_for(NtlmSettings_tC673E811873A17EA73FCA0EFD6D33839B5036009_il2cpp_TypeInfo_var))->get_defaultAuthLevel_0();
		__this->set__level_3(L_1);
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_2 = ___type20;
		NullCheck(L_2);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = Type2Message_get_Nonce_mEE9D40B2B299766F6B789195174BC580BDAEB4E1(L_2, /*hidden argument*/NULL);
		NullCheck((RuntimeArray *)(RuntimeArray *)L_3);
		RuntimeObject * L_4 = Array_Clone_mE8C710213E323617A6F46F2B36DCDDD4C7CF5176((RuntimeArray *)(RuntimeArray *)L_3, /*hidden argument*/NULL);
		__this->set__challenge_4(((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)Castclass((RuntimeObject*)L_4, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var)));
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_5 = ___type20;
		NullCheck(L_5);
		String_t* L_6 = L_5->get__targetName_4();
		__this->set__domain_6(L_6);
		String_t* L_7 = Environment_get_MachineName_m0300D26C1A5348D90800793717D33B1F629AF10D(/*hidden argument*/NULL);
		__this->set__host_5(L_7);
		String_t* L_8 = Environment_get_UserName_m089F0073984A6341BC214F4EB3DE42ACD19D067B(/*hidden argument*/NULL);
		__this->set__username_7(L_8);
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)33280));
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_9 = ___type20;
		NullCheck(L_9);
		int32_t L_10 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)L_9)->get__flags_2();
		if (!((int32_t)((int32_t)L_10&(int32_t)1)))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_11 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)((int32_t)L_11|(int32_t)1)));
		goto IL_0084;
	}

IL_0076:
	{
		int32_t L_12 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)((int32_t)L_12|(int32_t)2)));
	}

IL_0084:
	{
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_13 = ___type20;
		NullCheck(L_13);
		int32_t L_14 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)L_13)->get__flags_2();
		if (!((int32_t)((int32_t)L_14&(int32_t)((int32_t)524288))))
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_15 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)((int32_t)L_15|(int32_t)((int32_t)524288))));
	}

IL_00a4:
	{
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.Type3Message::Finalize()
extern "C" IL2CPP_METHOD_ATTR void Type3Message_Finalize_m3881D11D8E2CD25BE7DC27D8DE14B8824E9C5EE6 (Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get__challenge_4();
			if (!L_0)
			{
				goto IL_001c;
			}
		}

IL_0008:
		{
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = __this->get__challenge_4();
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = __this->get__challenge_4();
			NullCheck(L_2);
			Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_1, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		}

IL_001c:
		{
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = __this->get__lm_10();
			if (!L_3)
			{
				goto IL_0038;
			}
		}

IL_0024:
		{
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = __this->get__lm_10();
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = __this->get__lm_10();
			NullCheck(L_5);
			Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_4, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		}

IL_0038:
		{
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = __this->get__nt_11();
			if (!L_6)
			{
				goto IL_0054;
			}
		}

IL_0040:
		{
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = __this->get__nt_11();
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = __this->get__nt_11();
			NullCheck(L_8);
			Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_7, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length)))), /*hidden argument*/NULL);
		}

IL_0054:
		{
			IL2CPP_LEAVE(0x5D, FINALLY_0056);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(86)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_JUMP_TBL(0x5D, IL_005d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005d:
	{
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.Type3Message::set_Domain(System.String)
extern "C" IL2CPP_METHOD_ATTR void Type3Message_set_Domain_m04440D54FDAA49E5C82380E22C46DBF09C22DBF5 (Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type3Message_set_Domain_m04440D54FDAA49E5C82380E22C46DBF09C22DBF5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		___value0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_000a:
	{
		String_t* L_1 = ___value0;
		bool L_2 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_1, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)((int32_t)L_3&(int32_t)((int32_t)-4097))));
		goto IL_003d;
	}

IL_002b:
	{
		int32_t L_4 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)((int32_t)L_4|(int32_t)((int32_t)4096))));
	}

IL_003d:
	{
		String_t* L_5 = ___value0;
		__this->set__domain_6(L_5);
		return;
	}
}
// System.Void Mono.Security.Protocol.Ntlm.Type3Message::Decode(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void Type3Message_Decode_m4624A3F2775E1E590627C69F188816C994270B6D (Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type3Message_Decode_m4624A3F2775E1E590627C69F188816C994270B6D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___message0;
		MessageBase_Decode_m0994F2111010F3E105B94A83439BDBADA2E46537(__this, L_0, /*hidden argument*/NULL);
		__this->set__password_8((String_t*)NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ___message0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) < ((int32_t)((int32_t)64))))
		{
			goto IL_0025;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___message0;
		uint32_t L_3 = BitConverterLE_ToUInt32_m898E287439DDB0A31E4E39FF83DD032B1C43FA26(L_2, ((int32_t)60), /*hidden argument*/NULL);
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(L_3);
		goto IL_0030;
	}

IL_0025:
	{
		((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->set__flags_2(((int32_t)33281));
	}

IL_0030:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ___message0;
		uint16_t L_5 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_4, ((int32_t)12), /*hidden argument*/NULL);
		V_0 = L_5;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = ___message0;
		uint16_t L_7 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_6, ((int32_t)16), /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_8);
		__this->set__lm_10(L_9);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = ___message0;
		int32_t L_11 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_12 = __this->get__lm_10();
		int32_t L_13 = V_0;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_10, L_11, (RuntimeArray *)(RuntimeArray *)L_12, 0, L_13, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = ___message0;
		uint16_t L_15 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_14, ((int32_t)20), /*hidden argument*/NULL);
		V_2 = L_15;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = ___message0;
		uint16_t L_17 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_16, ((int32_t)24), /*hidden argument*/NULL);
		V_3 = L_17;
		int32_t L_18 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_19 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_18);
		__this->set__nt_11(L_19);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = ___message0;
		int32_t L_21 = V_3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_22 = __this->get__nt_11();
		int32_t L_23 = V_2;
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_20, L_21, (RuntimeArray *)(RuntimeArray *)L_22, 0, L_23, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = ___message0;
		uint16_t L_25 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_24, ((int32_t)28), /*hidden argument*/NULL);
		V_4 = L_25;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_26 = ___message0;
		uint16_t L_27 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_26, ((int32_t)32), /*hidden argument*/NULL);
		V_5 = L_27;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_28 = ___message0;
		int32_t L_29 = V_5;
		int32_t L_30 = V_4;
		String_t* L_31 = Type3Message_DecodeString_mCB1797B1FA97CD96E788DCE096FDBC275685C42F(__this, L_28, L_29, L_30, /*hidden argument*/NULL);
		__this->set__domain_6(L_31);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_32 = ___message0;
		uint16_t L_33 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_32, ((int32_t)36), /*hidden argument*/NULL);
		V_6 = L_33;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_34 = ___message0;
		uint16_t L_35 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_34, ((int32_t)40), /*hidden argument*/NULL);
		V_7 = L_35;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_36 = ___message0;
		int32_t L_37 = V_7;
		int32_t L_38 = V_6;
		String_t* L_39 = Type3Message_DecodeString_mCB1797B1FA97CD96E788DCE096FDBC275685C42F(__this, L_36, L_37, L_38, /*hidden argument*/NULL);
		__this->set__username_7(L_39);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_40 = ___message0;
		uint16_t L_41 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_40, ((int32_t)44), /*hidden argument*/NULL);
		V_8 = L_41;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_42 = ___message0;
		uint16_t L_43 = BitConverterLE_ToUInt16_mFC8811706681807666F91EE89A28ADF75DF6EFCC(L_42, ((int32_t)48), /*hidden argument*/NULL);
		V_9 = L_43;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_44 = ___message0;
		int32_t L_45 = V_9;
		int32_t L_46 = V_8;
		String_t* L_47 = Type3Message_DecodeString_mCB1797B1FA97CD96E788DCE096FDBC275685C42F(__this, L_44, L_45, L_46, /*hidden argument*/NULL);
		__this->set__host_5(L_47);
		return;
	}
}
// System.String Mono.Security.Protocol.Ntlm.Type3Message::DecodeString(System.Byte[],System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* Type3Message_DecodeString_mCB1797B1FA97CD96E788DCE096FDBC275685C42F (Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer0, int32_t ___offset1, int32_t ___len2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0018;
		}
	}
	{
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_1 = Encoding_get_Unicode_m86CC470F70F9BB52DDB26721F0C0D6EDAFC318AA(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___buffer0;
		int32_t L_3 = ___offset1;
		int32_t L_4 = ___len2;
		NullCheck(L_1);
		String_t* L_5 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_1, L_2, L_3, L_4);
		return L_5;
	}

IL_0018:
	{
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_6 = Encoding_get_ASCII_m9B673AE3152AB04D07CADE6E5E142C785B5BC94E(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ___buffer0;
		int32_t L_8 = ___offset1;
		int32_t L_9 = ___len2;
		NullCheck(L_6);
		String_t* L_10 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_6, L_7, L_8, L_9);
		return L_10;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::EncodeString(System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Type3Message_EncodeString_m431F1D808D738A2C9CE57DE1084F9A42C036AA5A (Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C * __this, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type3Message_EncodeString_m431F1D808D738A2C9CE57DE1084F9A42C036AA5A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___text0;
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)0);
		return L_1;
	}

IL_000a:
	{
		int32_t L_2 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		if (!((int32_t)((int32_t)L_2&(int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_3 = Encoding_get_Unicode_m86CC470F70F9BB52DDB26721F0C0D6EDAFC318AA(/*hidden argument*/NULL);
		String_t* L_4 = ___text0;
		NullCheck(L_3);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = VirtFuncInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_3, L_4);
		return L_5;
	}

IL_0020:
	{
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_6 = Encoding_get_ASCII_m9B673AE3152AB04D07CADE6E5E142C785B5BC94E(/*hidden argument*/NULL);
		String_t* L_7 = ___text0;
		NullCheck(L_6);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = VirtFuncInvoker1< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, String_t* >::Invoke(16 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, L_7);
		return L_8;
	}
}
// System.Byte[] Mono.Security.Protocol.Ntlm.Type3Message::GetBytes()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Type3Message_GetBytes_m9C22065DB20CD016FCA226AA2A78952E111CF118 (Type3Message_t6D21CF9E3D56192F8D9B6E2B29474773E838846C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Type3Message_GetBytes_m9C22065DB20CD016FCA226AA2A78952E111CF118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_1 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_3 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_7 = NULL;
	int16_t V_8 = 0;
	int16_t V_9 = 0;
	int16_t V_10 = 0;
	int16_t V_11 = 0;
	int16_t V_12 = 0;
	int16_t V_13 = 0;
	int16_t V_14 = 0;
	int16_t V_15 = 0;
	int16_t V_16 = 0;
	int32_t V_17 = 0;
	ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * V_18 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	int32_t G_B12_0 = 0;
	int32_t G_B15_0 = 0;
	{
		String_t* L_0 = __this->get__domain_6();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = Type3Message_EncodeString_m431F1D808D738A2C9CE57DE1084F9A42C036AA5A(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = __this->get__username_7();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = Type3Message_EncodeString_m431F1D808D738A2C9CE57DE1084F9A42C036AA5A(__this, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = __this->get__host_5();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = Type3Message_EncodeString_m431F1D808D738A2C9CE57DE1084F9A42C036AA5A(__this, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_6 = __this->get__type2_9();
		if (L_6)
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_7 = __this->get__level_3();
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_8 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_8, _stringLiteral636C307C2499B64E58C024BD8EC39A968AF2D4A9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, NULL, Type3Message_GetBytes_m9C22065DB20CD016FCA226AA2A78952E111CF118_RuntimeMethod_var);
	}

IL_0042:
	{
		String_t* L_9 = __this->get__password_8();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = __this->get__challenge_4();
		ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * L_11 = (ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B *)il2cpp_codegen_object_new(ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B_il2cpp_TypeInfo_var);
		ChallengeResponse__ctor_mF80EAE315F35264F1DA0167B3ED7A8CD8E2D1FFA(L_11, L_9, L_10, /*hidden argument*/NULL);
		V_18 = L_11;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * L_12 = V_18;
		NullCheck(L_12);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = ChallengeResponse_get_LM_m3916048E028CFCA867E801A83FEB949F7C089263(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * L_14 = V_18;
		NullCheck(L_14);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = ChallengeResponse_get_NT_mEC9F2FDFDB8FADF415D4BA8A1564A23024FC3437(L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		IL2CPP_LEAVE(0x9B, FINALLY_0068);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0068;
	}

FINALLY_0068:
	{ // begin finally (depth: 1)
		{
			ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * L_16 = V_18;
			if (!L_16)
			{
				goto IL_0073;
			}
		}

IL_006c:
		{
			ChallengeResponse_t2A954E3C6AB2D638ECA50E1B8ACB99E8F28A048B * L_17 = V_18;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_17);
		}

IL_0073:
		{
			IL2CPP_END_FINALLY(104)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(104)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0074:
	{
		Type2Message_t990283F48D41AECF1FFBDAA3A194CDE9C9078398 * L_18 = __this->get__type2_9();
		int32_t L_19 = __this->get__level_3();
		String_t* L_20 = __this->get__username_7();
		String_t* L_21 = __this->get__password_8();
		String_t* L_22 = __this->get__domain_6();
		IL2CPP_RUNTIME_CLASS_INIT(ChallengeResponse2_t2F5817D7717011A7F4C0DE0015D144A2292CAD77_il2cpp_TypeInfo_var);
		ChallengeResponse2_Compute_mAA312CA925226C75A829516B6BDC2089840D389D(L_18, L_19, L_20, L_21, L_22, (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821**)(&V_3), (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821**)(&V_4), /*hidden argument*/NULL);
	}

IL_009b:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = V_3;
		if (L_23)
		{
			goto IL_00a1;
		}
	}
	{
		G_B12_0 = 0;
		goto IL_00a4;
	}

IL_00a1:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = V_3;
		NullCheck(L_24);
		G_B12_0 = (((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length))));
	}

IL_00a4:
	{
		V_5 = G_B12_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_25 = V_4;
		if (L_25)
		{
			goto IL_00ad;
		}
	}
	{
		G_B15_0 = 0;
		goto IL_00b1;
	}

IL_00ad:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_26 = V_4;
		NullCheck(L_26);
		G_B15_0 = (((int32_t)((int32_t)(((RuntimeArray *)L_26)->max_length))));
	}

IL_00b1:
	{
		V_6 = G_B15_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_27 = V_0;
		NullCheck(L_27);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_28 = V_1;
		NullCheck(L_28);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_29 = V_2;
		NullCheck(L_29);
		int32_t L_30 = V_5;
		int32_t L_31 = V_6;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_32 = MessageBase_PrepareMessage_m6B0C0C463C16D086924EC49DB07C3ADE95C11958(__this, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)64), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_27)->max_length)))))), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_28)->max_length)))))), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_29)->max_length)))))), (int32_t)L_30)), (int32_t)L_31)), /*hidden argument*/NULL);
		V_7 = L_32;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_33 = V_0;
		NullCheck(L_33);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_34 = V_1;
		NullCheck(L_34);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_35 = V_2;
		NullCheck(L_35);
		V_8 = (((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)64), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_33)->max_length)))))), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_34)->max_length)))))), (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length)))))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_36 = V_7;
		int32_t L_37 = V_5;
		NullCheck(L_36);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (uint8_t)(((int32_t)((uint8_t)L_37))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_38 = V_7;
		NullCheck(L_38);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (uint8_t)0);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_39 = V_7;
		int32_t L_40 = V_5;
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (uint8_t)(((int32_t)((uint8_t)L_40))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_41 = V_7;
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (uint8_t)0);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_42 = V_7;
		int16_t L_43 = V_8;
		NullCheck(L_42);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (uint8_t)(((int32_t)((uint8_t)L_43))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_44 = V_7;
		int16_t L_45 = V_8;
		NullCheck(L_44);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_45>>(int32_t)8))))));
		int16_t L_46 = V_8;
		int32_t L_47 = V_5;
		V_9 = (((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)L_47)))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_48 = V_7;
		int32_t L_49 = V_6;
		NullCheck(L_48);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (uint8_t)(((int32_t)((uint8_t)L_49))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_50 = V_7;
		int32_t L_51 = V_6;
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_51>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_52 = V_7;
		int32_t L_53 = V_6;
		NullCheck(L_52);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (uint8_t)(((int32_t)((uint8_t)L_53))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_54 = V_7;
		int32_t L_55 = V_6;
		NullCheck(L_54);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_55>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_56 = V_7;
		int16_t L_57 = V_9;
		NullCheck(L_56);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)24)), (uint8_t)(((int32_t)((uint8_t)L_57))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_58 = V_7;
		int16_t L_59 = V_9;
		NullCheck(L_58);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)25)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_59>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_60 = V_0;
		NullCheck(L_60);
		V_10 = (((int16_t)((int16_t)(((int32_t)((int32_t)(((RuntimeArray *)L_60)->max_length)))))));
		V_11 = (int16_t)((int32_t)64);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_61 = V_7;
		int16_t L_62 = V_10;
		NullCheck(L_61);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)28)), (uint8_t)(((int32_t)((uint8_t)L_62))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_63 = V_7;
		int16_t L_64 = V_10;
		NullCheck(L_63);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)29)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_64>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_65 = V_7;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_66 = V_7;
		NullCheck(L_66);
		int32_t L_67 = ((int32_t)28);
		uint8_t L_68 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		NullCheck(L_65);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)30)), (uint8_t)L_68);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_69 = V_7;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_70 = V_7;
		NullCheck(L_70);
		int32_t L_71 = ((int32_t)29);
		uint8_t L_72 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		NullCheck(L_69);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)31)), (uint8_t)L_72);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_73 = V_7;
		int16_t L_74 = V_11;
		NullCheck(L_73);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)32)), (uint8_t)(((int32_t)((uint8_t)L_74))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_75 = V_7;
		int16_t L_76 = V_11;
		NullCheck(L_75);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)33)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_76>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_77 = V_1;
		NullCheck(L_77);
		V_12 = (((int16_t)((int16_t)(((int32_t)((int32_t)(((RuntimeArray *)L_77)->max_length)))))));
		int16_t L_78 = V_11;
		int16_t L_79 = V_10;
		V_13 = (((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_78, (int32_t)L_79)))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_80 = V_7;
		int16_t L_81 = V_12;
		NullCheck(L_80);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)36)), (uint8_t)(((int32_t)((uint8_t)L_81))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_82 = V_7;
		int16_t L_83 = V_12;
		NullCheck(L_82);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)37)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_83>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_84 = V_7;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_85 = V_7;
		NullCheck(L_85);
		int32_t L_86 = ((int32_t)36);
		uint8_t L_87 = (L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		NullCheck(L_84);
		(L_84)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)38)), (uint8_t)L_87);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_88 = V_7;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_89 = V_7;
		NullCheck(L_89);
		int32_t L_90 = ((int32_t)37);
		uint8_t L_91 = (L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_90));
		NullCheck(L_88);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)39)), (uint8_t)L_91);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_92 = V_7;
		int16_t L_93 = V_13;
		NullCheck(L_92);
		(L_92)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)40)), (uint8_t)(((int32_t)((uint8_t)L_93))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_94 = V_7;
		int16_t L_95 = V_13;
		NullCheck(L_94);
		(L_94)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)41)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_95>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_96 = V_2;
		NullCheck(L_96);
		V_14 = (((int16_t)((int16_t)(((int32_t)((int32_t)(((RuntimeArray *)L_96)->max_length)))))));
		int16_t L_97 = V_13;
		int16_t L_98 = V_12;
		V_15 = (((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_97, (int32_t)L_98)))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_99 = V_7;
		int16_t L_100 = V_14;
		NullCheck(L_99);
		(L_99)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)44)), (uint8_t)(((int32_t)((uint8_t)L_100))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_101 = V_7;
		int16_t L_102 = V_14;
		NullCheck(L_101);
		(L_101)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)45)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_102>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_103 = V_7;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_104 = V_7;
		NullCheck(L_104);
		int32_t L_105 = ((int32_t)44);
		uint8_t L_106 = (L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_105));
		NullCheck(L_103);
		(L_103)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)46)), (uint8_t)L_106);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_107 = V_7;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_108 = V_7;
		NullCheck(L_108);
		int32_t L_109 = ((int32_t)45);
		uint8_t L_110 = (L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_109));
		NullCheck(L_107);
		(L_107)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)47)), (uint8_t)L_110);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_111 = V_7;
		int16_t L_112 = V_15;
		NullCheck(L_111);
		(L_111)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)48)), (uint8_t)(((int32_t)((uint8_t)L_112))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_113 = V_7;
		int16_t L_114 = V_15;
		NullCheck(L_113);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)49)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_114>>(int32_t)8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_115 = V_7;
		NullCheck(L_115);
		V_16 = (((int16_t)((int16_t)(((int32_t)((int32_t)(((RuntimeArray *)L_115)->max_length)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_116 = V_7;
		int16_t L_117 = V_16;
		NullCheck(L_116);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)56)), (uint8_t)(((int32_t)((uint8_t)L_117))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_118 = V_7;
		int16_t L_119 = V_16;
		NullCheck(L_118);
		(L_118)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)57)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_119>>(int32_t)8))))));
		int32_t L_120 = ((MessageBase_t504D166CC4021DEB56DED308D5E82C67F47F26C0 *)__this)->get__flags_2();
		V_17 = L_120;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_121 = V_7;
		int32_t L_122 = V_17;
		NullCheck(L_121);
		(L_121)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)60)), (uint8_t)(((int32_t)((uint8_t)L_122))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_123 = V_7;
		int32_t L_124 = V_17;
		NullCheck(L_123);
		(L_123)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)61)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_124>>8))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_125 = V_7;
		int32_t L_126 = V_17;
		NullCheck(L_125);
		(L_125)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)62)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_126>>((int32_t)16)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_127 = V_7;
		int32_t L_128 = V_17;
		NullCheck(L_127);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)63)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_128>>((int32_t)24)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_129 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_130 = V_7;
		int16_t L_131 = V_11;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_132 = V_0;
		NullCheck(L_132);
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_129, 0, (RuntimeArray *)(RuntimeArray *)L_130, L_131, (((int32_t)((int32_t)(((RuntimeArray *)L_132)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_133 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_134 = V_7;
		int16_t L_135 = V_13;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_136 = V_1;
		NullCheck(L_136);
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_133, 0, (RuntimeArray *)(RuntimeArray *)L_134, L_135, (((int32_t)((int32_t)(((RuntimeArray *)L_136)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_137 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_138 = V_7;
		int16_t L_139 = V_15;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_140 = V_2;
		NullCheck(L_140);
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_137, 0, (RuntimeArray *)(RuntimeArray *)L_138, L_139, (((int32_t)((int32_t)(((RuntimeArray *)L_140)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_141 = V_3;
		if (!L_141)
		{
			goto IL_02a8;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_142 = V_3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_143 = V_7;
		int16_t L_144 = V_8;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_145 = V_3;
		NullCheck(L_145);
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_142, 0, (RuntimeArray *)(RuntimeArray *)L_143, L_144, (((int32_t)((int32_t)(((RuntimeArray *)L_145)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_146 = V_3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_147 = V_3;
		NullCheck(L_147);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_146, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_147)->max_length)))), /*hidden argument*/NULL);
	}

IL_02a8:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_148 = V_4;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_149 = V_7;
		int16_t L_150 = V_9;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_151 = V_4;
		NullCheck(L_151);
		Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_148, 0, (RuntimeArray *)(RuntimeArray *)L_149, L_150, (((int32_t)((int32_t)(((RuntimeArray *)L_151)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_152 = V_4;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_153 = V_4;
		NullCheck(L_153);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_152, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_153)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_154 = V_7;
		return L_154;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
