﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DLLCore.ChangeColorCustomBoxCmd
struct ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7;
// DLLCore.ChangeColorMultiCustomBoxCmd
struct ChangeColorMultiCustomBoxCmd_tC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2;
// DLLCore.ChangeMaterialUnitCmd
struct ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9;
// DLLCore.Controller
struct Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90;
// DLLCore.DaylightController
struct DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2;
// DLLCore.FPSControls
struct FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE;
// DLLCore.GeneralSetting
struct GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D;
// DLLCore.GizmoClickDetection[]
struct GizmoClickDetectionU5BU5D_t2BB934C874E1A0505C5BB28C65B52B1587CBAE3D;
// DLLCore.GraphicSetting
struct GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8;
// DLLCore.History
struct History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58;
// DLLCore.Main
struct Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83;
// DLLCore.MaterialContainer
struct MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462;
// DLLCore.MaterialContainer/a
struct a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630;
// DLLCore.MaterialLoader
struct MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE;
// DLLCore.MeasureArea
struct MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7;
// DLLCore.MeasureDistance
struct MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA;
// DLLCore.ModelLoader
struct ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D;
// DLLCore.ObserverController
struct ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA;
// DLLCore.OrbitControls
struct OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403;
// DLLCore.PlayerController
struct PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286;
// DLLCore.Product
struct Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C;
// DLLCore.ProductComponents.Detector
struct Detector_tE440186490449828DD72FAED201287D60C7E9498;
// DLLCore.ProductComponents.Loader
struct Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7;
// DLLCore.ProductComponents.Loader/ProductData
struct ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4;
// DLLCore.ProductComponents.Remover
struct Remover_t7D8C8F8B0950D56CCC1ACD94500C096AE133D701;
// DLLCore.ProductComponents.Store
struct Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932;
// DLLCore.ProductComponents.Styler
struct Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10;
// DLLCore.ProductComponents.Transformer
struct Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8;
// DLLCore.ProductContextMenu
struct ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54;
// DLLCore.ProductController
struct ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7;
// DLLCore.ProductController/LoadCallback
struct LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B;
// DLLCore.ProductController/a
struct a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7;
// DLLCore.ProductController/k
struct k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59;
// DLLCore.ReflectionControls
struct ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1;
// DLLCore.ToolController
struct ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980;
// DLLCore.TransformMultiProductCmd
struct TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059;
// DLLCore.TransformProductCmd
struct TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7;
// DLLCore.TransformUnitCmd
struct TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C;
// DLLCore.UnitComponents.Loader
struct Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF;
// DLLCore.UnitComponents.Remover
struct Remover_t014C01D9DA3634857EDA3B77F140905C24E69561;
// DLLCore.UnitComponents.SplitedWall[]
struct SplitedWallU5BU5D_t3541A89596F8B7FF5A0F02394BB9392A0132ACDE;
// DLLCore.UnitComponents.SplitedWalls
struct SplitedWalls_t3D6E1370EC4D5F1E735EF893FFBE5357C2FA9A3D;
// DLLCore.UnitComponents.Store
struct Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35;
// DLLCore.UnitComponents.Styler
struct Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0;
// DLLCore.UnitComponents.Transformer
struct Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4;
// DLLCore.UnitComponents.Unit
struct Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D;
// DLLCore.UnitComponents.WallColliderChanger
struct WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D;
// DLLCore.UnitComponents.WallOpacityChanger
struct WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC;
// DLLCore.UnitController
struct UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB;
// DLLCore.UnitController/LoadCallback
struct LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B;
// JSONObject
struct JSONObject_t483DCB025D825510C7827AE094325C194CE9B946;
// Outline
struct Outline_t1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<DLLCore.Product>
struct Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F;
// System.Action`1<DLLCore.UnitComponents.Unit>
struct Action_1_t220989B187DABA98AFD263EAD812A25996DD77A1;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96;
// System.Action`1<UnityEngine.Material>
struct Action_1_tB415D0A3AE617D10674931B88A79DEAAD126F9A7;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<UnityEngine.MeshRenderer,UnityEngine.Material>
struct Dictionary_2_tC5140A4BEF241546EB4109AF3928A6AE7BC126CF;
// System.Collections.Generic.LinkedList`1<DLLCore.Command>
struct LinkedList_1_t286C5D1B6ADFB4D98F1EE9A186935777F97BC48C;
// System.Collections.Generic.List`1<DLLCore.Product>
struct List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.Queue`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.MeshRenderer>>
struct Queue_1_t6261BA7435D63FF1F2C491627C0581281B591DBE;
// System.Converter`2<DLLCore.Product,TransformData>
struct Converter_2_tC59A9FCE257B0B4603D23D9058F057CFD62E199C;
// System.Converter`2<DLLCore.Product,UnityEngine.Quaternion>
struct Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4;
// System.Converter`2<DLLCore.Product,UnityEngine.Vector3>
struct Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5;
// System.Converter`2<UnityEngine.GameObject,DLLCore.Product>
struct Converter_2_tDE3CC9B66B376B4F8CE9FE97C9948D2D32634E8E;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Predicate`1<DLLCore.Product>
struct Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TransformData[]
struct TransformDataU5BU5D_tAAAA03F0DBD452D4D0E62FB73297A56DA0F3D842;
// TriLib.AssetDownloader
struct AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B;
// TriLib.AssetLoaderOptions
struct AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531;
// TriLib.AssimpInterop/ProgressCallback
struct ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6;
// TriLib.ObjectLoadedHandle
struct ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044;
// UnityEngine.BoxCollider
struct BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BROWSERINTERACTING_TD17A4F74B4CABFFDD0227C0BA1B8DD95CC22BA74_H
#define BROWSERINTERACTING_TD17A4F74B4CABFFDD0227C0BA1B8DD95CC22BA74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.BrowserInteracting
struct  BrowserInteracting_tD17A4F74B4CABFFDD0227C0BA1B8DD95CC22BA74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BROWSERINTERACTING_TD17A4F74B4CABFFDD0227C0BA1B8DD95CC22BA74_H
#ifndef A_T433051DDF90FB1423BC75EFB809D4367CA4EA9B7_H
#define A_T433051DDF90FB1423BC75EFB809D4367CA4EA9B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GeneralSetting_a
struct  a_t433051DDF90FB1423BC75EFB809D4367CA4EA9B7  : public RuntimeObject
{
public:
	// DLLCore.GeneralSetting DLLCore.GeneralSetting_a::a
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * ___a_0;
	// DLLCore.Controller DLLCore.GeneralSetting_a::b
	Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90 * ___b_1;
	// System.Action DLLCore.GeneralSetting_a::c
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___c_2;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t433051DDF90FB1423BC75EFB809D4367CA4EA9B7, ___a_0)); }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * get_a_0() const { return ___a_0; }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_t433051DDF90FB1423BC75EFB809D4367CA4EA9B7, ___b_1)); }
	inline Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90 * get_b_1() const { return ___b_1; }
	inline Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(a_t433051DDF90FB1423BC75EFB809D4367CA4EA9B7, ___c_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_c_2() const { return ___c_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T433051DDF90FB1423BC75EFB809D4367CA4EA9B7_H
#ifndef B_T57312502347FBF707316CAD0D119A027A625EC84_H
#define B_T57312502347FBF707316CAD0D119A027A625EC84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GeneralSetting_b
struct  b_t57312502347FBF707316CAD0D119A027A625EC84  : public RuntimeObject
{
public:
	// System.Int32 DLLCore.GeneralSetting_b::a
	int32_t ___a_0;
	// System.Object DLLCore.GeneralSetting_b::b
	RuntimeObject * ___b_1;
	// DLLCore.GeneralSetting DLLCore.GeneralSetting_b::c
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * ___c_2;
	// UnityEngine.Transform DLLCore.GeneralSetting_b::d
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___d_3;
	// System.Single DLLCore.GeneralSetting_b::e
	float ___e_4;
	// System.Action DLLCore.GeneralSetting_b::f
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___f_5;
	// UnityEngine.WaitForEndOfFrame DLLCore.GeneralSetting_b::g
	WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * ___g_6;
	// System.Single DLLCore.GeneralSetting_b::h
	float ___h_7;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(b_t57312502347FBF707316CAD0D119A027A625EC84, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(b_t57312502347FBF707316CAD0D119A027A625EC84, ___b_1)); }
	inline RuntimeObject * get_b_1() const { return ___b_1; }
	inline RuntimeObject ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(RuntimeObject * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(b_t57312502347FBF707316CAD0D119A027A625EC84, ___c_2)); }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * get_c_2() const { return ___c_2; }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D ** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(b_t57312502347FBF707316CAD0D119A027A625EC84, ___d_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_d_3() const { return ___d_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(b_t57312502347FBF707316CAD0D119A027A625EC84, ___e_4)); }
	inline float get_e_4() const { return ___e_4; }
	inline float* get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(float value)
	{
		___e_4 = value;
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(b_t57312502347FBF707316CAD0D119A027A625EC84, ___f_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_f_5() const { return ___f_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___f_5 = value;
		Il2CppCodeGenWriteBarrier((&___f_5), value);
	}

	inline static int32_t get_offset_of_g_6() { return static_cast<int32_t>(offsetof(b_t57312502347FBF707316CAD0D119A027A625EC84, ___g_6)); }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * get_g_6() const { return ___g_6; }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA ** get_address_of_g_6() { return &___g_6; }
	inline void set_g_6(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * value)
	{
		___g_6 = value;
		Il2CppCodeGenWriteBarrier((&___g_6), value);
	}

	inline static int32_t get_offset_of_h_7() { return static_cast<int32_t>(offsetof(b_t57312502347FBF707316CAD0D119A027A625EC84, ___h_7)); }
	inline float get_h_7() const { return ___h_7; }
	inline float* get_address_of_h_7() { return &___h_7; }
	inline void set_h_7(float value)
	{
		___h_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // B_T57312502347FBF707316CAD0D119A027A625EC84_H
#ifndef GLOBALFUNC_T058C2D04752654C6A3FC7C3FA3C1E712BAB7DF1A_H
#define GLOBALFUNC_T058C2D04752654C6A3FC7C3FA3C1E712BAB7DF1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GlobalFunc
struct  GlobalFunc_t058C2D04752654C6A3FC7C3FA3C1E712BAB7DF1A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALFUNC_T058C2D04752654C6A3FC7C3FA3C1E712BAB7DF1A_H
#ifndef GLOBALVAR_TDC7D119577462115919042A6D78F28D6D8D0267E_H
#define GLOBALVAR_TDC7D119577462115919042A6D78F28D6D8D0267E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GlobalVar
struct  GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E  : public RuntimeObject
{
public:

public:
};

struct GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields
{
public:
	// UnityEngine.Material DLLCore.GlobalVar::DefaultMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___DefaultMaterial_0;
	// System.String DLLCore.GlobalVar::APIDomain
	String_t* ___APIDomain_1;
	// System.String DLLCore.GlobalVar::CDN_PATH
	String_t* ___CDN_PATH_2;
	// System.String DLLCore.GlobalVar::ACE_RESOURCE_PATH
	String_t* ___ACE_RESOURCE_PATH_3;
	// System.Int32 DLLCore.GlobalVar::mouseLeft
	int32_t ___mouseLeft_4;
	// System.Int32 DLLCore.GlobalVar::mouseRight
	int32_t ___mouseRight_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> DLLCore.GlobalVar::DefaultMaterialId
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___DefaultMaterialId_6;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> DLLCore.GlobalVar::CallbackStatus
	Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * ___CallbackStatus_7;
	// System.Int32 DLLCore.GlobalVar::floorLayerMask
	int32_t ___floorLayerMask_8;
	// System.Int32 DLLCore.GlobalVar::wallLayerMask
	int32_t ___wallLayerMask_9;
	// System.Int32 DLLCore.GlobalVar::ceilLayerMask
	int32_t ___ceilLayerMask_10;
	// System.Int32 DLLCore.GlobalVar::productsLayerMask
	int32_t ___productsLayerMask_11;
	// System.Int32 DLLCore.GlobalVar::outlineLayerMask
	int32_t ___outlineLayerMask_12;
	// System.Int32 DLLCore.GlobalVar::productLayerMask
	int32_t ___productLayerMask_13;
	// System.Int32 DLLCore.GlobalVar::productItemLayerMask
	int32_t ___productItemLayerMask_14;
	// System.Int32 DLLCore.GlobalVar::splitedWallLayerMask
	int32_t ___splitedWallLayerMask_15;

public:
	inline static int32_t get_offset_of_DefaultMaterial_0() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___DefaultMaterial_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_DefaultMaterial_0() const { return ___DefaultMaterial_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_DefaultMaterial_0() { return &___DefaultMaterial_0; }
	inline void set_DefaultMaterial_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___DefaultMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMaterial_0), value);
	}

	inline static int32_t get_offset_of_APIDomain_1() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___APIDomain_1)); }
	inline String_t* get_APIDomain_1() const { return ___APIDomain_1; }
	inline String_t** get_address_of_APIDomain_1() { return &___APIDomain_1; }
	inline void set_APIDomain_1(String_t* value)
	{
		___APIDomain_1 = value;
		Il2CppCodeGenWriteBarrier((&___APIDomain_1), value);
	}

	inline static int32_t get_offset_of_CDN_PATH_2() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___CDN_PATH_2)); }
	inline String_t* get_CDN_PATH_2() const { return ___CDN_PATH_2; }
	inline String_t** get_address_of_CDN_PATH_2() { return &___CDN_PATH_2; }
	inline void set_CDN_PATH_2(String_t* value)
	{
		___CDN_PATH_2 = value;
		Il2CppCodeGenWriteBarrier((&___CDN_PATH_2), value);
	}

	inline static int32_t get_offset_of_ACE_RESOURCE_PATH_3() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___ACE_RESOURCE_PATH_3)); }
	inline String_t* get_ACE_RESOURCE_PATH_3() const { return ___ACE_RESOURCE_PATH_3; }
	inline String_t** get_address_of_ACE_RESOURCE_PATH_3() { return &___ACE_RESOURCE_PATH_3; }
	inline void set_ACE_RESOURCE_PATH_3(String_t* value)
	{
		___ACE_RESOURCE_PATH_3 = value;
		Il2CppCodeGenWriteBarrier((&___ACE_RESOURCE_PATH_3), value);
	}

	inline static int32_t get_offset_of_mouseLeft_4() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___mouseLeft_4)); }
	inline int32_t get_mouseLeft_4() const { return ___mouseLeft_4; }
	inline int32_t* get_address_of_mouseLeft_4() { return &___mouseLeft_4; }
	inline void set_mouseLeft_4(int32_t value)
	{
		___mouseLeft_4 = value;
	}

	inline static int32_t get_offset_of_mouseRight_5() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___mouseRight_5)); }
	inline int32_t get_mouseRight_5() const { return ___mouseRight_5; }
	inline int32_t* get_address_of_mouseRight_5() { return &___mouseRight_5; }
	inline void set_mouseRight_5(int32_t value)
	{
		___mouseRight_5 = value;
	}

	inline static int32_t get_offset_of_DefaultMaterialId_6() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___DefaultMaterialId_6)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_DefaultMaterialId_6() const { return ___DefaultMaterialId_6; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_DefaultMaterialId_6() { return &___DefaultMaterialId_6; }
	inline void set_DefaultMaterialId_6(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___DefaultMaterialId_6 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMaterialId_6), value);
	}

	inline static int32_t get_offset_of_CallbackStatus_7() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___CallbackStatus_7)); }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * get_CallbackStatus_7() const { return ___CallbackStatus_7; }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C ** get_address_of_CallbackStatus_7() { return &___CallbackStatus_7; }
	inline void set_CallbackStatus_7(Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * value)
	{
		___CallbackStatus_7 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackStatus_7), value);
	}

	inline static int32_t get_offset_of_floorLayerMask_8() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___floorLayerMask_8)); }
	inline int32_t get_floorLayerMask_8() const { return ___floorLayerMask_8; }
	inline int32_t* get_address_of_floorLayerMask_8() { return &___floorLayerMask_8; }
	inline void set_floorLayerMask_8(int32_t value)
	{
		___floorLayerMask_8 = value;
	}

	inline static int32_t get_offset_of_wallLayerMask_9() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___wallLayerMask_9)); }
	inline int32_t get_wallLayerMask_9() const { return ___wallLayerMask_9; }
	inline int32_t* get_address_of_wallLayerMask_9() { return &___wallLayerMask_9; }
	inline void set_wallLayerMask_9(int32_t value)
	{
		___wallLayerMask_9 = value;
	}

	inline static int32_t get_offset_of_ceilLayerMask_10() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___ceilLayerMask_10)); }
	inline int32_t get_ceilLayerMask_10() const { return ___ceilLayerMask_10; }
	inline int32_t* get_address_of_ceilLayerMask_10() { return &___ceilLayerMask_10; }
	inline void set_ceilLayerMask_10(int32_t value)
	{
		___ceilLayerMask_10 = value;
	}

	inline static int32_t get_offset_of_productsLayerMask_11() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___productsLayerMask_11)); }
	inline int32_t get_productsLayerMask_11() const { return ___productsLayerMask_11; }
	inline int32_t* get_address_of_productsLayerMask_11() { return &___productsLayerMask_11; }
	inline void set_productsLayerMask_11(int32_t value)
	{
		___productsLayerMask_11 = value;
	}

	inline static int32_t get_offset_of_outlineLayerMask_12() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___outlineLayerMask_12)); }
	inline int32_t get_outlineLayerMask_12() const { return ___outlineLayerMask_12; }
	inline int32_t* get_address_of_outlineLayerMask_12() { return &___outlineLayerMask_12; }
	inline void set_outlineLayerMask_12(int32_t value)
	{
		___outlineLayerMask_12 = value;
	}

	inline static int32_t get_offset_of_productLayerMask_13() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___productLayerMask_13)); }
	inline int32_t get_productLayerMask_13() const { return ___productLayerMask_13; }
	inline int32_t* get_address_of_productLayerMask_13() { return &___productLayerMask_13; }
	inline void set_productLayerMask_13(int32_t value)
	{
		___productLayerMask_13 = value;
	}

	inline static int32_t get_offset_of_productItemLayerMask_14() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___productItemLayerMask_14)); }
	inline int32_t get_productItemLayerMask_14() const { return ___productItemLayerMask_14; }
	inline int32_t* get_address_of_productItemLayerMask_14() { return &___productItemLayerMask_14; }
	inline void set_productItemLayerMask_14(int32_t value)
	{
		___productItemLayerMask_14 = value;
	}

	inline static int32_t get_offset_of_splitedWallLayerMask_15() { return static_cast<int32_t>(offsetof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields, ___splitedWallLayerMask_15)); }
	inline int32_t get_splitedWallLayerMask_15() const { return ___splitedWallLayerMask_15; }
	inline int32_t* get_address_of_splitedWallLayerMask_15() { return &___splitedWallLayerMask_15; }
	inline void set_splitedWallLayerMask_15(int32_t value)
	{
		___splitedWallLayerMask_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALVAR_TDC7D119577462115919042A6D78F28D6D8D0267E_H
#ifndef U3CU3EC_T278118A1F44635CBC4D91D43E8A25548353EE0FF_H
#define U3CU3EC_T278118A1F44635CBC4D91D43E8A25548353EE0FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MaterialContainer_<>c
struct  U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF_StaticFields
{
public:
	// DLLCore.MaterialContainer_<>c DLLCore.MaterialContainer_<>c::<>9
	U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF * ___U3CU3E9_0;
	// System.Action`1<UnityEngine.Material> DLLCore.MaterialContainer_<>c::<>9__4_0
	Action_1_tB415D0A3AE617D10674931B88A79DEAAD126F9A7 * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Action_1_tB415D0A3AE617D10674931B88A79DEAAD126F9A7 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Action_1_tB415D0A3AE617D10674931B88A79DEAAD126F9A7 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Action_1_tB415D0A3AE617D10674931B88A79DEAAD126F9A7 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T278118A1F44635CBC4D91D43E8A25548353EE0FF_H
#ifndef A_T9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630_H
#define A_T9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MaterialContainer_a
struct  a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630  : public RuntimeObject
{
public:
	// DLLCore.MaterialContainer DLLCore.MaterialContainer_a::a
	MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * ___a_0;
	// System.String DLLCore.MaterialContainer_a::b
	String_t* ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630, ___a_0)); }
	inline MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * get_a_0() const { return ___a_0; }
	inline MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630, ___b_1)); }
	inline String_t* get_b_1() const { return ___b_1; }
	inline String_t** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(String_t* value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630_H
#ifndef B_T7466D9030918E1CAEC9A5F32F05E8825C8C9A54B_H
#define B_T7466D9030918E1CAEC9A5F32F05E8825C8C9A54B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MaterialContainer_b
struct  b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B  : public RuntimeObject
{
public:
	// System.Int32 DLLCore.MaterialContainer_b::a
	int32_t ___a_0;
	// System.Object DLLCore.MaterialContainer_b::b
	RuntimeObject * ___b_1;
	// DLLCore.MaterialContainer DLLCore.MaterialContainer_b::c
	MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * ___c_2;
	// System.String DLLCore.MaterialContainer_b::d
	String_t* ___d_3;
	// UnityEngine.MeshRenderer DLLCore.MaterialContainer_b::e
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___e_4;
	// DLLCore.MaterialContainer_a DLLCore.MaterialContainer_b::f
	a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630 * ___f_5;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B, ___b_1)); }
	inline RuntimeObject * get_b_1() const { return ___b_1; }
	inline RuntimeObject ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(RuntimeObject * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B, ___c_2)); }
	inline MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * get_c_2() const { return ___c_2; }
	inline MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 ** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B, ___d_3)); }
	inline String_t* get_d_3() const { return ___d_3; }
	inline String_t** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(String_t* value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B, ___e_4)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_e_4() const { return ___e_4; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier((&___e_4), value);
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B, ___f_5)); }
	inline a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630 * get_f_5() const { return ___f_5; }
	inline a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630 ** get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630 * value)
	{
		___f_5 = value;
		Il2CppCodeGenWriteBarrier((&___f_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // B_T7466D9030918E1CAEC9A5F32F05E8825C8C9A54B_H
#ifndef C_TA4679713E8BA96C80A0C87574F54F77383D92518_H
#define C_TA4679713E8BA96C80A0C87574F54F77383D92518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MaterialContainer_c
struct  c_tA4679713E8BA96C80A0C87574F54F77383D92518  : public RuntimeObject
{
public:
	// System.String DLLCore.MaterialContainer_c::a
	String_t* ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(c_tA4679713E8BA96C80A0C87574F54F77383D92518, ___a_0)); }
	inline String_t* get_a_0() const { return ___a_0; }
	inline String_t** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(String_t* value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C_TA4679713E8BA96C80A0C87574F54F77383D92518_H
#ifndef A_T3B414B8677031A1BD55E83017B9C9AEA64A60159_H
#define A_T3B414B8677031A1BD55E83017B9C9AEA64A60159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MaterialLoader_a
struct  a_t3B414B8677031A1BD55E83017B9C9AEA64A60159  : public RuntimeObject
{
public:
	// System.String DLLCore.MaterialLoader_a::a
	String_t* ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t3B414B8677031A1BD55E83017B9C9AEA64A60159, ___a_0)); }
	inline String_t* get_a_0() const { return ___a_0; }
	inline String_t** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(String_t* value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T3B414B8677031A1BD55E83017B9C9AEA64A60159_H
#ifndef B_TF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E_H
#define B_TF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MaterialLoader_b
struct  b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E  : public RuntimeObject
{
public:
	// System.Int32 DLLCore.MaterialLoader_b::a
	int32_t ___a_0;
	// System.Object DLLCore.MaterialLoader_b::b
	RuntimeObject * ___b_1;
	// System.String DLLCore.MaterialLoader_b::c
	String_t* ___c_2;
	// System.String DLLCore.MaterialLoader_b::d
	String_t* ___d_3;
	// System.Boolean DLLCore.MaterialLoader_b::e
	bool ___e_4;
	// JSONObject DLLCore.MaterialLoader_b::f
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___f_5;
	// JSONObject DLLCore.MaterialLoader_b::g
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___g_6;
	// UnityEngine.Material DLLCore.MaterialLoader_b::h
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___h_7;
	// JSONObject DLLCore.MaterialLoader_b::i
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___i_8;
	// JSONObject DLLCore.MaterialLoader_b::j
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___j_9;
	// UnityEngine.Networking.UnityWebRequest DLLCore.MaterialLoader_b::k
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___k_10;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___b_1)); }
	inline RuntimeObject * get_b_1() const { return ___b_1; }
	inline RuntimeObject ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(RuntimeObject * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___c_2)); }
	inline String_t* get_c_2() const { return ___c_2; }
	inline String_t** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(String_t* value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___d_3)); }
	inline String_t* get_d_3() const { return ___d_3; }
	inline String_t** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(String_t* value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___e_4)); }
	inline bool get_e_4() const { return ___e_4; }
	inline bool* get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(bool value)
	{
		___e_4 = value;
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___f_5)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_f_5() const { return ___f_5; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___f_5 = value;
		Il2CppCodeGenWriteBarrier((&___f_5), value);
	}

	inline static int32_t get_offset_of_g_6() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___g_6)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_g_6() const { return ___g_6; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_g_6() { return &___g_6; }
	inline void set_g_6(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___g_6 = value;
		Il2CppCodeGenWriteBarrier((&___g_6), value);
	}

	inline static int32_t get_offset_of_h_7() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___h_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_h_7() const { return ___h_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_h_7() { return &___h_7; }
	inline void set_h_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___h_7 = value;
		Il2CppCodeGenWriteBarrier((&___h_7), value);
	}

	inline static int32_t get_offset_of_i_8() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___i_8)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_i_8() const { return ___i_8; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_i_8() { return &___i_8; }
	inline void set_i_8(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___i_8 = value;
		Il2CppCodeGenWriteBarrier((&___i_8), value);
	}

	inline static int32_t get_offset_of_j_9() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___j_9)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_j_9() const { return ___j_9; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_j_9() { return &___j_9; }
	inline void set_j_9(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___j_9 = value;
		Il2CppCodeGenWriteBarrier((&___j_9), value);
	}

	inline static int32_t get_offset_of_k_10() { return static_cast<int32_t>(offsetof(b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E, ___k_10)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_k_10() const { return ___k_10; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_k_10() { return &___k_10; }
	inline void set_k_10(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___k_10 = value;
		Il2CppCodeGenWriteBarrier((&___k_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // B_TF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E_H
#ifndef B_T8776D45E5AD405680C193123F7FF0BBC46D6866A_H
#define B_T8776D45E5AD405680C193123F7FF0BBC46D6866A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ModelLoader_b
struct  b_t8776D45E5AD405680C193123F7FF0BBC46D6866A  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.GameObject> DLLCore.ModelLoader_b::a
	Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * ___a_0;
	// DLLCore.ModelLoader DLLCore.ModelLoader_b::b
	ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(b_t8776D45E5AD405680C193123F7FF0BBC46D6866A, ___a_0)); }
	inline Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * get_a_0() const { return ___a_0; }
	inline Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(b_t8776D45E5AD405680C193123F7FF0BBC46D6866A, ___b_1)); }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * get_b_1() const { return ___b_1; }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // B_T8776D45E5AD405680C193123F7FF0BBC46D6866A_H
#ifndef C_T01E2EC79FBD05D095F54BB893B966C010764FDE9_H
#define C_T01E2EC79FBD05D095F54BB893B966C010764FDE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ModelLoader_c
struct  c_t01E2EC79FBD05D095F54BB893B966C010764FDE9  : public RuntimeObject
{
public:
	// System.Int32 DLLCore.ModelLoader_c::a
	int32_t ___a_0;
	// System.Object DLLCore.ModelLoader_c::b
	RuntimeObject * ___b_1;
	// System.Action`1<UnityEngine.GameObject> DLLCore.ModelLoader_c::c
	Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * ___c_2;
	// DLLCore.ModelLoader DLLCore.ModelLoader_c::d
	ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * ___d_3;
	// System.String DLLCore.ModelLoader_c::e
	String_t* ___e_4;
	// System.String DLLCore.ModelLoader_c::f
	String_t* ___f_5;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(c_t01E2EC79FBD05D095F54BB893B966C010764FDE9, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(c_t01E2EC79FBD05D095F54BB893B966C010764FDE9, ___b_1)); }
	inline RuntimeObject * get_b_1() const { return ___b_1; }
	inline RuntimeObject ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(RuntimeObject * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(c_t01E2EC79FBD05D095F54BB893B966C010764FDE9, ___c_2)); }
	inline Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * get_c_2() const { return ___c_2; }
	inline Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 ** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(c_t01E2EC79FBD05D095F54BB893B966C010764FDE9, ___d_3)); }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * get_d_3() const { return ___d_3; }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D ** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(c_t01E2EC79FBD05D095F54BB893B966C010764FDE9, ___e_4)); }
	inline String_t* get_e_4() const { return ___e_4; }
	inline String_t** get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(String_t* value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier((&___e_4), value);
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(c_t01E2EC79FBD05D095F54BB893B966C010764FDE9, ___f_5)); }
	inline String_t* get_f_5() const { return ___f_5; }
	inline String_t** get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(String_t* value)
	{
		___f_5 = value;
		Il2CppCodeGenWriteBarrier((&___f_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C_T01E2EC79FBD05D095F54BB893B966C010764FDE9_H
#ifndef U3CU3EC_TCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_H
#define U3CU3EC_TCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Detector_<>c
struct  U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_StaticFields
{
public:
	// DLLCore.ProductComponents.Detector_<>c DLLCore.ProductComponents.Detector_<>c::<>9
	U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532 * ___U3CU3E9_0;
	// System.Action`1<DLLCore.Product> DLLCore.ProductComponents.Detector_<>c::<>9__11_0
	Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * ___U3CU3E9__11_0_1;
	// System.Action`1<DLLCore.Product> DLLCore.ProductComponents.Detector_<>c::<>9__13_0
	Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * ___U3CU3E9__13_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_StaticFields, ___U3CU3E9__11_0_1)); }
	inline Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__13_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_StaticFields, ___U3CU3E9__13_0_2)); }
	inline Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * get_U3CU3E9__13_0_2() const { return ___U3CU3E9__13_0_2; }
	inline Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F ** get_address_of_U3CU3E9__13_0_2() { return &___U3CU3E9__13_0_2; }
	inline void set_U3CU3E9__13_0_2(Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * value)
	{
		___U3CU3E9__13_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__13_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_H
#ifndef A_T8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7_H
#define A_T8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Loader_a
struct  a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7  : public RuntimeObject
{
public:
	// DLLCore.ProductComponents.Loader DLLCore.ProductComponents.Loader_a::a
	Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7 * ___a_0;
	// DLLCore.ProductComponents.Loader_ProductData DLLCore.ProductComponents.Loader_a::b
	ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4 * ___b_1;
	// JSONObject DLLCore.ProductComponents.Loader_a::c
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___c_2;
	// JSONObject DLLCore.ProductComponents.Loader_a::d
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___d_3;
	// System.Int32 DLLCore.ProductComponents.Loader_a::e
	int32_t ___e_4;
	// System.Action`1<DLLCore.Product> DLLCore.ProductComponents.Loader_a::f
	Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * ___f_5;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7, ___a_0)); }
	inline Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7 * get_a_0() const { return ___a_0; }
	inline Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7, ___b_1)); }
	inline ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4 * get_b_1() const { return ___b_1; }
	inline ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7, ___c_2)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_c_2() const { return ___c_2; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7, ___d_3)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_d_3() const { return ___d_3; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7, ___e_4)); }
	inline int32_t get_e_4() const { return ___e_4; }
	inline int32_t* get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(int32_t value)
	{
		___e_4 = value;
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7, ___f_5)); }
	inline Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * get_f_5() const { return ___f_5; }
	inline Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F ** get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * value)
	{
		___f_5 = value;
		Il2CppCodeGenWriteBarrier((&___f_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7_H
#ifndef U3CU3EC_T1DA47739A8DA80CF44CBD741AAFB7981198536A5_H
#define U3CU3EC_T1DA47739A8DA80CF44CBD741AAFB7981198536A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Store_<>c
struct  U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields
{
public:
	// DLLCore.ProductComponents.Store_<>c DLLCore.ProductComponents.Store_<>c::<>9
	U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5 * ___U3CU3E9_0;
	// System.Converter`2<UnityEngine.GameObject,DLLCore.Product> DLLCore.ProductComponents.Store_<>c::<>9__16_0
	Converter_2_tDE3CC9B66B376B4F8CE9FE97C9948D2D32634E8E * ___U3CU3E9__16_0_1;
	// System.Predicate`1<DLLCore.Product> DLLCore.ProductComponents.Store_<>c::<>9__17_1
	Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F * ___U3CU3E9__17_1_2;
	// System.Predicate`1<DLLCore.Product> DLLCore.ProductComponents.Store_<>c::<>9__17_0
	Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F * ___U3CU3E9__17_0_3;
	// System.Converter`2<DLLCore.Product,TransformData> DLLCore.ProductComponents.Store_<>c::<>9__21_0
	Converter_2_tC59A9FCE257B0B4603D23D9058F057CFD62E199C * ___U3CU3E9__21_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields, ___U3CU3E9__16_0_1)); }
	inline Converter_2_tDE3CC9B66B376B4F8CE9FE97C9948D2D32634E8E * get_U3CU3E9__16_0_1() const { return ___U3CU3E9__16_0_1; }
	inline Converter_2_tDE3CC9B66B376B4F8CE9FE97C9948D2D32634E8E ** get_address_of_U3CU3E9__16_0_1() { return &___U3CU3E9__16_0_1; }
	inline void set_U3CU3E9__16_0_1(Converter_2_tDE3CC9B66B376B4F8CE9FE97C9948D2D32634E8E * value)
	{
		___U3CU3E9__16_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields, ___U3CU3E9__17_1_2)); }
	inline Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F * get_U3CU3E9__17_1_2() const { return ___U3CU3E9__17_1_2; }
	inline Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F ** get_address_of_U3CU3E9__17_1_2() { return &___U3CU3E9__17_1_2; }
	inline void set_U3CU3E9__17_1_2(Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F * value)
	{
		___U3CU3E9__17_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields, ___U3CU3E9__17_0_3)); }
	inline Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F * get_U3CU3E9__17_0_3() const { return ___U3CU3E9__17_0_3; }
	inline Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F ** get_address_of_U3CU3E9__17_0_3() { return &___U3CU3E9__17_0_3; }
	inline void set_U3CU3E9__17_0_3(Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F * value)
	{
		___U3CU3E9__17_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__21_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields, ___U3CU3E9__21_0_4)); }
	inline Converter_2_tC59A9FCE257B0B4603D23D9058F057CFD62E199C * get_U3CU3E9__21_0_4() const { return ___U3CU3E9__21_0_4; }
	inline Converter_2_tC59A9FCE257B0B4603D23D9058F057CFD62E199C ** get_address_of_U3CU3E9__21_0_4() { return &___U3CU3E9__21_0_4; }
	inline void set_U3CU3E9__21_0_4(Converter_2_tC59A9FCE257B0B4603D23D9058F057CFD62E199C * value)
	{
		___U3CU3E9__21_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__21_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1DA47739A8DA80CF44CBD741AAFB7981198536A5_H
#ifndef A_T78342471E804DE10F0B22D3ABA92ED6BF7951969_H
#define A_T78342471E804DE10F0B22D3ABA92ED6BF7951969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Styler_a
struct  a_t78342471E804DE10F0B22D3ABA92ED6BF7951969  : public RuntimeObject
{
public:
	// UnityEngine.MeshRenderer DLLCore.ProductComponents.Styler_a::a
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___a_0;
	// DLLCore.ProductComponents.Styler DLLCore.ProductComponents.Styler_a::b
	Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10 * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t78342471E804DE10F0B22D3ABA92ED6BF7951969, ___a_0)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_a_0() const { return ___a_0; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_t78342471E804DE10F0B22D3ABA92ED6BF7951969, ___b_1)); }
	inline Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10 * get_b_1() const { return ___b_1; }
	inline Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T78342471E804DE10F0B22D3ABA92ED6BF7951969_H
#ifndef A_T243663C6B3AE837A9E7B0A9FE8712391B0F79DF7_H
#define A_T243663C6B3AE837A9E7B0A9FE8712391B0F79DF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Transformer_a
struct  a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7  : public RuntimeObject
{
public:
	// System.Int32 DLLCore.ProductComponents.Transformer_a::a
	int32_t ___a_0;
	// System.Object DLLCore.ProductComponents.Transformer_a::b
	RuntimeObject * ___b_1;
	// System.Single DLLCore.ProductComponents.Transformer_a::c
	float ___c_2;
	// DLLCore.ProductComponents.Transformer DLLCore.ProductComponents.Transformer_a::d
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8 * ___d_3;
	// DLLCore.Product DLLCore.ProductComponents.Transformer_a::e
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * ___e_4;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7, ___b_1)); }
	inline RuntimeObject * get_b_1() const { return ___b_1; }
	inline RuntimeObject ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(RuntimeObject * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7, ___c_2)); }
	inline float get_c_2() const { return ___c_2; }
	inline float* get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(float value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7, ___d_3)); }
	inline Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8 * get_d_3() const { return ___d_3; }
	inline Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8 ** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8 * value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7, ___e_4)); }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * get_e_4() const { return ___e_4; }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C ** get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier((&___e_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T243663C6B3AE837A9E7B0A9FE8712391B0F79DF7_H
#ifndef U3CU3EC_TEA6A685319A4B89331772EB538884340E01A101F_H
#define U3CU3EC_TEA6A685319A4B89331772EB538884340E01A101F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductContextMenu_<>c
struct  U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields
{
public:
	// DLLCore.ProductContextMenu_<>c DLLCore.ProductContextMenu_<>c::<>9
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F * ___U3CU3E9_0;
	// System.Converter`2<DLLCore.Product,UnityEngine.Vector3> DLLCore.ProductContextMenu_<>c::<>9__31_0
	Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * ___U3CU3E9__31_0_1;
	// System.Converter`2<DLLCore.Product,UnityEngine.Quaternion> DLLCore.ProductContextMenu_<>c::<>9__31_1
	Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 * ___U3CU3E9__31_1_2;
	// System.Converter`2<DLLCore.Product,UnityEngine.Vector3> DLLCore.ProductContextMenu_<>c::<>9__31_2
	Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * ___U3CU3E9__31_2_3;
	// System.Converter`2<DLLCore.Product,UnityEngine.Vector3> DLLCore.ProductContextMenu_<>c::<>9__37_0
	Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * ___U3CU3E9__37_0_4;
	// System.Converter`2<DLLCore.Product,UnityEngine.Quaternion> DLLCore.ProductContextMenu_<>c::<>9__37_1
	Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 * ___U3CU3E9__37_1_5;
	// System.Converter`2<DLLCore.Product,UnityEngine.Vector3> DLLCore.ProductContextMenu_<>c::<>9__37_2
	Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * ___U3CU3E9__37_2_6;
	// System.Converter`2<DLLCore.Product,UnityEngine.Vector3> DLLCore.ProductContextMenu_<>c::<>9__39_0
	Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * ___U3CU3E9__39_0_7;
	// System.Converter`2<DLLCore.Product,UnityEngine.Quaternion> DLLCore.ProductContextMenu_<>c::<>9__39_1
	Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 * ___U3CU3E9__39_1_8;
	// System.Converter`2<DLLCore.Product,UnityEngine.Vector3> DLLCore.ProductContextMenu_<>c::<>9__39_2
	Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * ___U3CU3E9__39_2_9;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields, ___U3CU3E9__31_0_1)); }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * get_U3CU3E9__31_0_1() const { return ___U3CU3E9__31_0_1; }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 ** get_address_of_U3CU3E9__31_0_1() { return &___U3CU3E9__31_0_1; }
	inline void set_U3CU3E9__31_0_1(Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * value)
	{
		___U3CU3E9__31_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields, ___U3CU3E9__31_1_2)); }
	inline Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 * get_U3CU3E9__31_1_2() const { return ___U3CU3E9__31_1_2; }
	inline Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 ** get_address_of_U3CU3E9__31_1_2() { return &___U3CU3E9__31_1_2; }
	inline void set_U3CU3E9__31_1_2(Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 * value)
	{
		___U3CU3E9__31_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields, ___U3CU3E9__31_2_3)); }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * get_U3CU3E9__31_2_3() const { return ___U3CU3E9__31_2_3; }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 ** get_address_of_U3CU3E9__31_2_3() { return &___U3CU3E9__31_2_3; }
	inline void set_U3CU3E9__31_2_3(Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * value)
	{
		___U3CU3E9__31_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields, ___U3CU3E9__37_0_4)); }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * get_U3CU3E9__37_0_4() const { return ___U3CU3E9__37_0_4; }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 ** get_address_of_U3CU3E9__37_0_4() { return &___U3CU3E9__37_0_4; }
	inline void set_U3CU3E9__37_0_4(Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * value)
	{
		___U3CU3E9__37_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_1_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields, ___U3CU3E9__37_1_5)); }
	inline Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 * get_U3CU3E9__37_1_5() const { return ___U3CU3E9__37_1_5; }
	inline Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 ** get_address_of_U3CU3E9__37_1_5() { return &___U3CU3E9__37_1_5; }
	inline void set_U3CU3E9__37_1_5(Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 * value)
	{
		___U3CU3E9__37_1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_2_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields, ___U3CU3E9__37_2_6)); }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * get_U3CU3E9__37_2_6() const { return ___U3CU3E9__37_2_6; }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 ** get_address_of_U3CU3E9__37_2_6() { return &___U3CU3E9__37_2_6; }
	inline void set_U3CU3E9__37_2_6(Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * value)
	{
		___U3CU3E9__37_2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_0_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields, ___U3CU3E9__39_0_7)); }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * get_U3CU3E9__39_0_7() const { return ___U3CU3E9__39_0_7; }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 ** get_address_of_U3CU3E9__39_0_7() { return &___U3CU3E9__39_0_7; }
	inline void set_U3CU3E9__39_0_7(Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * value)
	{
		___U3CU3E9__39_0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__39_0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_1_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields, ___U3CU3E9__39_1_8)); }
	inline Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 * get_U3CU3E9__39_1_8() const { return ___U3CU3E9__39_1_8; }
	inline Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 ** get_address_of_U3CU3E9__39_1_8() { return &___U3CU3E9__39_1_8; }
	inline void set_U3CU3E9__39_1_8(Converter_2_t2BE1947367624B82BD15A7C7F4161F0BB26962E4 * value)
	{
		___U3CU3E9__39_1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__39_1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_2_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields, ___U3CU3E9__39_2_9)); }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * get_U3CU3E9__39_2_9() const { return ___U3CU3E9__39_2_9; }
	inline Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 ** get_address_of_U3CU3E9__39_2_9() { return &___U3CU3E9__39_2_9; }
	inline void set_U3CU3E9__39_2_9(Converter_2_tCF3A4DA866CD337D9983D665CC4E5712082833A5 * value)
	{
		___U3CU3E9__39_2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__39_2_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TEA6A685319A4B89331772EB538884340E01A101F_H
#ifndef A_TF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7_H
#define A_TF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_a
struct  a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7  : public RuntimeObject
{
public:
	// DLLCore.ProductController DLLCore.ProductController_a::a
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * ___a_0;
	// DLLCore.ProductController_LoadCallback DLLCore.ProductController_a::b
	LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * ___b_1;
	// System.String DLLCore.ProductController_a::c
	String_t* ___c_2;
	// System.String DLLCore.ProductController_a::d
	String_t* ___d_3;
	// System.Int32 DLLCore.ProductController_a::e
	int32_t ___e_4;
	// System.String DLLCore.ProductController_a::f
	String_t* ___f_5;
	// System.String DLLCore.ProductController_a::g
	String_t* ___g_6;
	// JSONObject DLLCore.ProductController_a::h
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___h_7;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7, ___a_0)); }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * get_a_0() const { return ___a_0; }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7, ___b_1)); }
	inline LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * get_b_1() const { return ___b_1; }
	inline LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7, ___c_2)); }
	inline String_t* get_c_2() const { return ___c_2; }
	inline String_t** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(String_t* value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7, ___d_3)); }
	inline String_t* get_d_3() const { return ___d_3; }
	inline String_t** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(String_t* value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7, ___e_4)); }
	inline int32_t get_e_4() const { return ___e_4; }
	inline int32_t* get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(int32_t value)
	{
		___e_4 = value;
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7, ___f_5)); }
	inline String_t* get_f_5() const { return ___f_5; }
	inline String_t** get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(String_t* value)
	{
		___f_5 = value;
		Il2CppCodeGenWriteBarrier((&___f_5), value);
	}

	inline static int32_t get_offset_of_g_6() { return static_cast<int32_t>(offsetof(a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7, ___g_6)); }
	inline String_t* get_g_6() const { return ___g_6; }
	inline String_t** get_address_of_g_6() { return &___g_6; }
	inline void set_g_6(String_t* value)
	{
		___g_6 = value;
		Il2CppCodeGenWriteBarrier((&___g_6), value);
	}

	inline static int32_t get_offset_of_h_7() { return static_cast<int32_t>(offsetof(a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7, ___h_7)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_h_7() const { return ___h_7; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_h_7() { return &___h_7; }
	inline void set_h_7(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___h_7 = value;
		Il2CppCodeGenWriteBarrier((&___h_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_TF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7_H
#ifndef B_T9B6839F17B94EE25DD53F26E895F9F6C004F6894_H
#define B_T9B6839F17B94EE25DD53F26E895F9F6C004F6894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_b
struct  b_t9B6839F17B94EE25DD53F26E895F9F6C004F6894  : public RuntimeObject
{
public:
	// DLLCore.Product DLLCore.ProductController_b::a
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * ___a_0;
	// DLLCore.ProductController_a DLLCore.ProductController_b::b
	a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7 * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(b_t9B6839F17B94EE25DD53F26E895F9F6C004F6894, ___a_0)); }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * get_a_0() const { return ___a_0; }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(b_t9B6839F17B94EE25DD53F26E895F9F6C004F6894, ___b_1)); }
	inline a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7 * get_b_1() const { return ___b_1; }
	inline a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // B_T9B6839F17B94EE25DD53F26E895F9F6C004F6894_H
#ifndef C_TEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231_H
#define C_TEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_c
struct  c_tEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231  : public RuntimeObject
{
public:
	// DLLCore.ProductController DLLCore.ProductController_c::a
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * ___a_0;
	// JSONObject DLLCore.ProductController_c::b
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___b_1;
	// System.Boolean DLLCore.ProductController_c::c
	bool ___c_2;
	// DLLCore.Product DLLCore.ProductController_c::d
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * ___d_3;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(c_tEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231, ___a_0)); }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * get_a_0() const { return ___a_0; }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(c_tEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231, ___b_1)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_b_1() const { return ___b_1; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(c_tEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231, ___c_2)); }
	inline bool get_c_2() const { return ___c_2; }
	inline bool* get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(bool value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(c_tEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231, ___d_3)); }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * get_d_3() const { return ___d_3; }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C ** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C_TEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231_H
#ifndef D_T6431F5736DEC21FD831EF0C0B5D36DB1C874006E_H
#define D_T6431F5736DEC21FD831EF0C0B5D36DB1C874006E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_d
struct  d_t6431F5736DEC21FD831EF0C0B5D36DB1C874006E  : public RuntimeObject
{
public:
	// DLLCore.ProductController DLLCore.ProductController_d::a
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * ___a_0;
	// System.Collections.Generic.List`1<DLLCore.Product> DLLCore.ProductController_d::b
	List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * ___b_1;
	// System.Int32 DLLCore.ProductController_d::c
	int32_t ___c_2;
	// System.Action`1<DLLCore.Product> DLLCore.ProductController_d::d
	Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * ___d_3;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(d_t6431F5736DEC21FD831EF0C0B5D36DB1C874006E, ___a_0)); }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * get_a_0() const { return ___a_0; }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(d_t6431F5736DEC21FD831EF0C0B5D36DB1C874006E, ___b_1)); }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * get_b_1() const { return ___b_1; }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(d_t6431F5736DEC21FD831EF0C0B5D36DB1C874006E, ___c_2)); }
	inline int32_t get_c_2() const { return ___c_2; }
	inline int32_t* get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(int32_t value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(d_t6431F5736DEC21FD831EF0C0B5D36DB1C874006E, ___d_3)); }
	inline Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * get_d_3() const { return ___d_3; }
	inline Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F ** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(Action_1_t72EA168D9BAA4595FA410E860DCF454315BADD9F * value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // D_T6431F5736DEC21FD831EF0C0B5D36DB1C874006E_H
#ifndef E_T7A60F2C1D9BD8A6F89CF3F6D21AB79B6AEAF9C94_H
#define E_T7A60F2C1D9BD8A6F89CF3F6D21AB79B6AEAF9C94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_e
struct  e_t7A60F2C1D9BD8A6F89CF3F6D21AB79B6AEAF9C94  : public RuntimeObject
{
public:
	// JSONObject DLLCore.ProductController_e::a
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(e_t7A60F2C1D9BD8A6F89CF3F6D21AB79B6AEAF9C94, ___a_0)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_a_0() const { return ___a_0; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // E_T7A60F2C1D9BD8A6F89CF3F6D21AB79B6AEAF9C94_H
#ifndef F_T88AACE613CA2A687E8A5A1AEE9016897C30E82A3_H
#define F_T88AACE613CA2A687E8A5A1AEE9016897C30E82A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_f
struct  f_t88AACE613CA2A687E8A5A1AEE9016897C30E82A3  : public RuntimeObject
{
public:
	// JSONObject DLLCore.ProductController_f::a
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(f_t88AACE613CA2A687E8A5A1AEE9016897C30E82A3, ___a_0)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_a_0() const { return ___a_0; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // F_T88AACE613CA2A687E8A5A1AEE9016897C30E82A3_H
#ifndef G_TDFCE396CBF262E3AE5824793EC389E4EE1C9E84C_H
#define G_TDFCE396CBF262E3AE5824793EC389E4EE1C9E84C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_g
struct  g_tDFCE396CBF262E3AE5824793EC389E4EE1C9E84C  : public RuntimeObject
{
public:
	// JSONObject DLLCore.ProductController_g::a
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___a_0;
	// System.Predicate`1<DLLCore.Product> DLLCore.ProductController_g::b
	Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(g_tDFCE396CBF262E3AE5824793EC389E4EE1C9E84C, ___a_0)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_a_0() const { return ___a_0; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(g_tDFCE396CBF262E3AE5824793EC389E4EE1C9E84C, ___b_1)); }
	inline Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F * get_b_1() const { return ___b_1; }
	inline Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Predicate_1_t6F4E06B29DE17BB99C6F189D0DE56B707E471F0F * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_TDFCE396CBF262E3AE5824793EC389E4EE1C9E84C_H
#ifndef H_TB9C53A529D7F78DE545E5ECC45E29705E7954423_H
#define H_TB9C53A529D7F78DE545E5ECC45E29705E7954423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_h
struct  h_tB9C53A529D7F78DE545E5ECC45E29705E7954423  : public RuntimeObject
{
public:
	// DLLCore.ProductController DLLCore.ProductController_h::a
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * ___a_0;
	// System.Collections.Generic.List`1<DLLCore.Product> DLLCore.ProductController_h::b
	List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * ___b_1;
	// JSONObject DLLCore.ProductController_h::c
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___c_2;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(h_tB9C53A529D7F78DE545E5ECC45E29705E7954423, ___a_0)); }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * get_a_0() const { return ___a_0; }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(h_tB9C53A529D7F78DE545E5ECC45E29705E7954423, ___b_1)); }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * get_b_1() const { return ___b_1; }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(h_tB9C53A529D7F78DE545E5ECC45E29705E7954423, ___c_2)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_c_2() const { return ___c_2; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // H_TB9C53A529D7F78DE545E5ECC45E29705E7954423_H
#ifndef I_TBB96D77E10B6671D9ECF8EF3AF39C8AC14740DCC_H
#define I_TBB96D77E10B6671D9ECF8EF3AF39C8AC14740DCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_i
struct  i_tBB96D77E10B6671D9ECF8EF3AF39C8AC14740DCC  : public RuntimeObject
{
public:
	// JSONObject DLLCore.ProductController_i::a
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(i_tBB96D77E10B6671D9ECF8EF3AF39C8AC14740DCC, ___a_0)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_a_0() const { return ___a_0; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // I_TBB96D77E10B6671D9ECF8EF3AF39C8AC14740DCC_H
#ifndef J_T6F06E33F0796AB276A0784918BBAE6A38C89B71F_H
#define J_T6F06E33F0796AB276A0784918BBAE6A38C89B71F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_j
struct  j_t6F06E33F0796AB276A0784918BBAE6A38C89B71F  : public RuntimeObject
{
public:
	// JSONObject DLLCore.ProductController_j::a
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(j_t6F06E33F0796AB276A0784918BBAE6A38C89B71F, ___a_0)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_a_0() const { return ___a_0; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // J_T6F06E33F0796AB276A0784918BBAE6A38C89B71F_H
#ifndef K_T7B3D8F985495BEE9F625B1325CBE75D9BE72DB59_H
#define K_T7B3D8F985495BEE9F625B1325CBE75D9BE72DB59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_k
struct  k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59  : public RuntimeObject
{
public:
	// DLLCore.ProductController DLLCore.ProductController_k::a
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * ___a_0;
	// JSONObject DLLCore.ProductController_k::b
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59, ___a_0)); }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * get_a_0() const { return ___a_0; }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59, ___b_1)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_b_1() const { return ___b_1; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // K_T7B3D8F985495BEE9F625B1325CBE75D9BE72DB59_H
#ifndef L_TB3479A81FAB621B17F46AF6AAB6384D9FBD013AD_H
#define L_TB3479A81FAB621B17F46AF6AAB6384D9FBD013AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_l
struct  l_tB3479A81FAB621B17F46AF6AAB6384D9FBD013AD  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<DLLCore.Product> DLLCore.ProductController_l::a
	List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * ___a_0;
	// DLLCore.ProductController_k DLLCore.ProductController_l::b
	k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59 * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(l_tB3479A81FAB621B17F46AF6AAB6384D9FBD013AD, ___a_0)); }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * get_a_0() const { return ___a_0; }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(l_tB3479A81FAB621B17F46AF6AAB6384D9FBD013AD, ___b_1)); }
	inline k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59 * get_b_1() const { return ___b_1; }
	inline k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // L_TB3479A81FAB621B17F46AF6AAB6384D9FBD013AD_H
#ifndef M_TD4B72672F9F98BB0255C4EDAAAFCD8942B449C4A_H
#define M_TD4B72672F9F98BB0255C4EDAAAFCD8942B449C4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_m
struct  m_tD4B72672F9F98BB0255C4EDAAAFCD8942B449C4A  : public RuntimeObject
{
public:
	// JSONObject DLLCore.ProductController_m::a
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(m_tD4B72672F9F98BB0255C4EDAAAFCD8942B449C4A, ___a_0)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_a_0() const { return ___a_0; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // M_TD4B72672F9F98BB0255C4EDAAAFCD8942B449C4A_H
#ifndef N_TE03118E8C7A13F10091ECC827F165C0BC32167AE_H
#define N_TE03118E8C7A13F10091ECC827F165C0BC32167AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_n
struct  n_tE03118E8C7A13F10091ECC827F165C0BC32167AE  : public RuntimeObject
{
public:
	// JSONObject DLLCore.ProductController_n::a
	JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(n_tE03118E8C7A13F10091ECC827F165C0BC32167AE, ___a_0)); }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * get_a_0() const { return ___a_0; }
	inline JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(JSONObject_t483DCB025D825510C7827AE094325C194CE9B946 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // N_TE03118E8C7A13F10091ECC827F165C0BC32167AE_H
#ifndef U3CU3EC_TD19F9CC5923893F96999DF9CD879D10D43A20122_H
#define U3CU3EC_TD19F9CC5923893F96999DF9CD879D10D43A20122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ReflectionControls_<>c
struct  U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122_StaticFields
{
public:
	// DLLCore.ReflectionControls_<>c DLLCore.ReflectionControls_<>c::<>9
	U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122 * ___U3CU3E9_0;
	// System.Action`1<UnityEngine.GameObject> DLLCore.ReflectionControls_<>c::<>9__5_0
	Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * ___U3CU3E9__5_0_1;
	// System.Action`1<UnityEngine.GameObject> DLLCore.ReflectionControls_<>c::<>9__6_0
	Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * ___U3CU3E9__6_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122_StaticFields, ___U3CU3E9__5_0_1)); }
	inline Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122_StaticFields, ___U3CU3E9__6_0_2)); }
	inline Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * get_U3CU3E9__6_0_2() const { return ___U3CU3E9__6_0_2; }
	inline Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 ** get_address_of_U3CU3E9__6_0_2() { return &___U3CU3E9__6_0_2; }
	inline void set_U3CU3E9__6_0_2(Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * value)
	{
		___U3CU3E9__6_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD19F9CC5923893F96999DF9CD879D10D43A20122_H
#ifndef A_TFCC3CE3B73C6FDA385751DF01618D09CFD3B75E8_H
#define A_TFCC3CE3B73C6FDA385751DF01618D09CFD3B75E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.Loader_a
struct  a_tFCC3CE3B73C6FDA385751DF01618D09CFD3B75E8  : public RuntimeObject
{
public:
	// DLLCore.UnitComponents.Loader DLLCore.UnitComponents.Loader_a::a
	Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF * ___a_0;
	// System.Action`1<DLLCore.UnitComponents.Unit> DLLCore.UnitComponents.Loader_a::b
	Action_1_t220989B187DABA98AFD263EAD812A25996DD77A1 * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_tFCC3CE3B73C6FDA385751DF01618D09CFD3B75E8, ___a_0)); }
	inline Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF * get_a_0() const { return ___a_0; }
	inline Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_tFCC3CE3B73C6FDA385751DF01618D09CFD3B75E8, ___b_1)); }
	inline Action_1_t220989B187DABA98AFD263EAD812A25996DD77A1 * get_b_1() const { return ___b_1; }
	inline Action_1_t220989B187DABA98AFD263EAD812A25996DD77A1 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Action_1_t220989B187DABA98AFD263EAD812A25996DD77A1 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_TFCC3CE3B73C6FDA385751DF01618D09CFD3B75E8_H
#ifndef A_T3857EB8DC73AF2680C0FC9027FCFB9028E1D67B9_H
#define A_T3857EB8DC73AF2680C0FC9027FCFB9028E1D67B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitController_a
struct  a_t3857EB8DC73AF2680C0FC9027FCFB9028E1D67B9  : public RuntimeObject
{
public:
	// DLLCore.UnitController DLLCore.UnitController_a::a
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB * ___a_0;
	// DLLCore.UnitController_LoadCallback DLLCore.UnitController_a::b
	LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t3857EB8DC73AF2680C0FC9027FCFB9028E1D67B9, ___a_0)); }
	inline UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB * get_a_0() const { return ___a_0; }
	inline UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_t3857EB8DC73AF2680C0FC9027FCFB9028E1D67B9, ___b_1)); }
	inline LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B * get_b_1() const { return ___b_1; }
	inline LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T3857EB8DC73AF2680C0FC9027FCFB9028E1D67B9_H
#ifndef STBIMAGELOADER_TECC0F7620E7E36D06D430505E069CE761ED507F0_H
#define STBIMAGELOADER_TECC0F7620E7E36D06D430505E069CE761ED507F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// STB.STBImageLoader
struct  STBImageLoader_tECC0F7620E7E36D06D430505E069CE761ED507F0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STBIMAGELOADER_TECC0F7620E7E36D06D430505E069CE761ED507F0_H
#ifndef STBIMAGEINTEROP_T722FCD4F8AAFF38C9E42715D615241C102649E7E_H
#define STBIMAGEINTEROP_T722FCD4F8AAFF38C9E42715D615241C102649E7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// STBImage.STBImageInterop
struct  STBImageInterop_t722FCD4F8AAFF38C9E42715D615241C102649E7E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STBIMAGEINTEROP_T722FCD4F8AAFF38C9E42715D615241C102649E7E_H
#ifndef LIST_1_T8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB_H
#define LIST_1_T8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<DLLCore.UnitComponents.SplitedWall>
struct  List_1_t8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SplitedWallU5BU5D_t3541A89596F8B7FF5A0F02394BB9392A0132ACDE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB, ____items_1)); }
	inline SplitedWallU5BU5D_t3541A89596F8B7FF5A0F02394BB9392A0132ACDE* get__items_1() const { return ____items_1; }
	inline SplitedWallU5BU5D_t3541A89596F8B7FF5A0F02394BB9392A0132ACDE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SplitedWallU5BU5D_t3541A89596F8B7FF5A0F02394BB9392A0132ACDE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SplitedWallU5BU5D_t3541A89596F8B7FF5A0F02394BB9392A0132ACDE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB_StaticFields, ____emptyArray_5)); }
	inline SplitedWallU5BU5D_t3541A89596F8B7FF5A0F02394BB9392A0132ACDE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SplitedWallU5BU5D_t3541A89596F8B7FF5A0F02394BB9392A0132ACDE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SplitedWallU5BU5D_t3541A89596F8B7FF5A0F02394BB9392A0132ACDE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB_H
#ifndef LIST_1_TB4444E94203E77FC813B61173EB21B8410EB7155_H
#define LIST_1_TB4444E94203E77FC813B61173EB21B8410EB7155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Material>
struct  List_1_tB4444E94203E77FC813B61173EB21B8410EB7155  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tB4444E94203E77FC813B61173EB21B8410EB7155, ____items_1)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get__items_1() const { return ____items_1; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tB4444E94203E77FC813B61173EB21B8410EB7155, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tB4444E94203E77FC813B61173EB21B8410EB7155, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tB4444E94203E77FC813B61173EB21B8410EB7155, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tB4444E94203E77FC813B61173EB21B8410EB7155_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tB4444E94203E77FC813B61173EB21B8410EB7155_StaticFields, ____emptyArray_5)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get__emptyArray_5() const { return ____emptyArray_5; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TB4444E94203E77FC813B61173EB21B8410EB7155_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ASSETADVANCEDPROPERTYMETADATA_T3ACD72729D1D3D2BC0925E455FDEE6A5FD610DC7_H
#define ASSETADVANCEDPROPERTYMETADATA_T3ACD72729D1D3D2BC0925E455FDEE6A5FD610DC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetAdvancedPropertyMetadata
struct  AssetAdvancedPropertyMetadata_t3ACD72729D1D3D2BC0925E455FDEE6A5FD610DC7  : public RuntimeObject
{
public:

public:
};

struct AssetAdvancedPropertyMetadata_t3ACD72729D1D3D2BC0925E455FDEE6A5FD610DC7_StaticFields
{
public:
	// System.String[] TriLib.AssetAdvancedPropertyMetadata::ConfigKeys
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___ConfigKeys_0;

public:
	inline static int32_t get_offset_of_ConfigKeys_0() { return static_cast<int32_t>(offsetof(AssetAdvancedPropertyMetadata_t3ACD72729D1D3D2BC0925E455FDEE6A5FD610DC7_StaticFields, ___ConfigKeys_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_ConfigKeys_0() const { return ___ConfigKeys_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_ConfigKeys_0() { return &___ConfigKeys_0; }
	inline void set_ConfigKeys_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___ConfigKeys_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConfigKeys_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETADVANCEDPROPERTYMETADATA_T3ACD72729D1D3D2BC0925E455FDEE6A5FD610DC7_H
#ifndef A_T0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C_H
#define A_T0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetDownloader_a
struct  a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C  : public RuntimeObject
{
public:
	// System.Int32 TriLib.AssetDownloader_a::a
	int32_t ___a_0;
	// System.Object TriLib.AssetDownloader_a::b
	RuntimeObject * ___b_1;
	// TriLib.AssetDownloader TriLib.AssetDownloader_a::c
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B * ___c_2;
	// System.String TriLib.AssetDownloader_a::d
	String_t* ___d_3;
	// System.String TriLib.AssetDownloader_a::e
	String_t* ___e_4;
	// TriLib.AssetLoaderOptions TriLib.AssetDownloader_a::f
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531 * ___f_5;
	// UnityEngine.GameObject TriLib.AssetDownloader_a::g
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___g_6;
	// TriLib.ObjectLoadedHandle TriLib.AssetDownloader_a::h
	ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * ___h_7;
	// TriLib.AssimpInterop_ProgressCallback TriLib.AssetDownloader_a::i
	ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * ___i_8;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C, ___b_1)); }
	inline RuntimeObject * get_b_1() const { return ___b_1; }
	inline RuntimeObject ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(RuntimeObject * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C, ___c_2)); }
	inline AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B * get_c_2() const { return ___c_2; }
	inline AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B ** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B * value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C, ___d_3)); }
	inline String_t* get_d_3() const { return ___d_3; }
	inline String_t** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(String_t* value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C, ___e_4)); }
	inline String_t* get_e_4() const { return ___e_4; }
	inline String_t** get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(String_t* value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier((&___e_4), value);
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C, ___f_5)); }
	inline AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531 * get_f_5() const { return ___f_5; }
	inline AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531 ** get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531 * value)
	{
		___f_5 = value;
		Il2CppCodeGenWriteBarrier((&___f_5), value);
	}

	inline static int32_t get_offset_of_g_6() { return static_cast<int32_t>(offsetof(a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C, ___g_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_g_6() const { return ___g_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_g_6() { return &___g_6; }
	inline void set_g_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___g_6 = value;
		Il2CppCodeGenWriteBarrier((&___g_6), value);
	}

	inline static int32_t get_offset_of_h_7() { return static_cast<int32_t>(offsetof(a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C, ___h_7)); }
	inline ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * get_h_7() const { return ___h_7; }
	inline ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 ** get_address_of_h_7() { return &___h_7; }
	inline void set_h_7(ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * value)
	{
		___h_7 = value;
		Il2CppCodeGenWriteBarrier((&___h_7), value);
	}

	inline static int32_t get_offset_of_i_8() { return static_cast<int32_t>(offsetof(a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C, ___i_8)); }
	inline ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * get_i_8() const { return ___i_8; }
	inline ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 ** get_address_of_i_8() { return &___i_8; }
	inline void set_i_8(ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * value)
	{
		___i_8 = value;
		Il2CppCodeGenWriteBarrier((&___i_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C_H
#ifndef MATERIALCONTAINER_TE6A113C04D2C881D1750B6665F281A9006A90462_H
#define MATERIALCONTAINER_TE6A113C04D2C881D1750B6665F281A9006A90462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MaterialContainer
struct  MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462  : public List_1_tB4444E94203E77FC813B61173EB21B8410EB7155
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALCONTAINER_TE6A113C04D2C881D1750B6665F281A9006A90462_H
#ifndef SPLITEDWALLS_T3D6E1370EC4D5F1E735EF893FFBE5357C2FA9A3D_H
#define SPLITEDWALLS_T3D6E1370EC4D5F1E735EF893FFBE5357C2FA9A3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.SplitedWalls
struct  SplitedWalls_t3D6E1370EC4D5F1E735EF893FFBE5357C2FA9A3D  : public List_1_t8AE19B8B52DAA1C1CF5EF4CF17812B404EC5CBEB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLITEDWALLS_T3D6E1370EC4D5F1E735EF893FFBE5357C2FA9A3D_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef RAYCASTPLANE_TEAE141749806C54A382D3F1A075608EDA4B38B76_H
#define RAYCASTPLANE_TEAE141749806C54A382D3F1A075608EDA4B38B76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GizmoTranslateScript_RaycastPlane
struct  RaycastPlane_tEAE141749806C54A382D3F1A075608EDA4B38B76 
{
public:
	// System.Int32 DLLCore.GizmoTranslateScript_RaycastPlane::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RaycastPlane_tEAE141749806C54A382D3F1A075608EDA4B38B76, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTPLANE_TEAE141749806C54A382D3F1A075608EDA4B38B76_H
#ifndef MODELTYPE_T7F5AF03AF3B972D6B0D3B90C42D79957B4B06B08_H
#define MODELTYPE_T7F5AF03AF3B972D6B0D3B90C42D79957B4B06B08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GlobalVar_ModelType
struct  ModelType_t7F5AF03AF3B972D6B0D3B90C42D79957B4B06B08 
{
public:
	// System.Int32 DLLCore.GlobalVar_ModelType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModelType_t7F5AF03AF3B972D6B0D3B90C42D79957B4B06B08, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELTYPE_T7F5AF03AF3B972D6B0D3B90C42D79957B4B06B08_H
#ifndef UNITTYPE_TFB8DF4883EB91B9AA9862E065B3CA4AEBA32C4C6_H
#define UNITTYPE_TFB8DF4883EB91B9AA9862E065B3CA4AEBA32C4C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GlobalVar_UnitType
struct  UnitType_tFB8DF4883EB91B9AA9862E065B3CA4AEBA32C4C6 
{
public:
	// System.Int32 DLLCore.GlobalVar_UnitType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnitType_tFB8DF4883EB91B9AA9862E065B3CA4AEBA32C4C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITTYPE_TFB8DF4883EB91B9AA9862E065B3CA4AEBA32C4C6_H
#ifndef GUIHELPER_TA84A9D6AFB902F91D77F1942393821A419FD931C_H
#define GUIHELPER_TA84A9D6AFB902F91D77F1942393821A419FD931C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GuiHelper
struct  GuiHelper_tA84A9D6AFB902F91D77F1942393821A419FD931C  : public RuntimeObject
{
public:

public:
};

struct GuiHelper_tA84A9D6AFB902F91D77F1942393821A419FD931C_StaticFields
{
public:
	// UnityEngine.Texture2D DLLCore.GuiHelper::a
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___a_0;
	// UnityEngine.Color DLLCore.GuiHelper::b
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(GuiHelper_tA84A9D6AFB902F91D77F1942393821A419FD931C_StaticFields, ___a_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_a_0() const { return ___a_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(GuiHelper_tA84A9D6AFB902F91D77F1942393821A419FD931C_StaticFields, ___b_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_b_1() const { return ___b_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIHELPER_TA84A9D6AFB902F91D77F1942393821A419FD931C_H
#ifndef INPUTMANAGER_TE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1_H
#define INPUTMANAGER_TE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.InputManager
struct  InputManager_tE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1  : public RuntimeObject
{
public:

public:
};

struct InputManager_tE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1_StaticFields
{
public:
	// UnityEngine.Vector3 DLLCore.InputManager::a
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a_0;
	// UnityEngine.Vector3 DLLCore.InputManager::b
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(InputManager_tE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1_StaticFields, ___a_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_a_0() const { return ___a_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(InputManager_tE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1_StaticFields, ___b_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_b_1() const { return ___b_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMANAGER_TE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1_H
#ifndef ACTIONTYPE_T8BD049FCFBFDAFAB18966CA8E7B0AF1417BA2797_H
#define ACTIONTYPE_T8BD049FCFBFDAFAB18966CA8E7B0AF1417BA2797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Loader_ProductData_ActionType
struct  ActionType_t8BD049FCFBFDAFAB18966CA8E7B0AF1417BA2797 
{
public:
	// System.Int32 DLLCore.ProductComponents.Loader_ProductData_ActionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionType_t8BD049FCFBFDAFAB18966CA8E7B0AF1417BA2797, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTYPE_T8BD049FCFBFDAFAB18966CA8E7B0AF1417BA2797_H
#ifndef STYPE_T0761393A7DE32BC8F9A1DA121A4FDC1624794DDF_H
#define STYPE_T0761393A7DE32BC8F9A1DA121A4FDC1624794DDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Styler_SType
struct  SType_t0761393A7DE32BC8F9A1DA121A4FDC1624794DDF 
{
public:
	// System.Int32 DLLCore.ProductComponents.Styler_SType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SType_t0761393A7DE32BC8F9A1DA121A4FDC1624794DDF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYPE_T0761393A7DE32BC8F9A1DA121A4FDC1624794DDF_H
#ifndef TTYPE_T21E3FBFD0CAE6ADA425A45328916AADD4A7A745E_H
#define TTYPE_T21E3FBFD0CAE6ADA425A45328916AADD4A7A745E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Styler_TType
struct  TType_t21E3FBFD0CAE6ADA425A45328916AADD4A7A745E 
{
public:
	// System.Int32 DLLCore.ProductComponents.Styler_TType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TType_t21E3FBFD0CAE6ADA425A45328916AADD4A7A745E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TTYPE_T21E3FBFD0CAE6ADA425A45328916AADD4A7A745E_H
#ifndef STYPE_T161AED89893FAA2418C216E0255B4417A8203955_H
#define STYPE_T161AED89893FAA2418C216E0255B4417A8203955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Transformer_SType
struct  SType_t161AED89893FAA2418C216E0255B4417A8203955 
{
public:
	// System.Int32 DLLCore.ProductComponents.Transformer_SType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SType_t161AED89893FAA2418C216E0255B4417A8203955, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYPE_T161AED89893FAA2418C216E0255B4417A8203955_H
#ifndef TTYPE_T2D15E57A61E5C468944FC8F2CBED5DF23F7C150A_H
#define TTYPE_T2D15E57A61E5C468944FC8F2CBED5DF23F7C150A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Transformer_TType
struct  TType_t2D15E57A61E5C468944FC8F2CBED5DF23F7C150A 
{
public:
	// System.Int32 DLLCore.ProductComponents.Transformer_TType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TType_t2D15E57A61E5C468944FC8F2CBED5DF23F7C150A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TTYPE_T2D15E57A61E5C468944FC8F2CBED5DF23F7C150A_H
#ifndef ACTION_TE0BB1BA0E6F890762C80861F918D097AA89569D0_H
#define ACTION_TE0BB1BA0E6F890762C80861F918D097AA89569D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductContextMenu_Action
struct  Action_tE0BB1BA0E6F890762C80861F918D097AA89569D0 
{
public:
	// System.Int32 DLLCore.ProductContextMenu_Action::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Action_tE0BB1BA0E6F890762C80861F918D097AA89569D0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_TE0BB1BA0E6F890762C80861F918D097AA89569D0_H
#ifndef CONTROLMODE_T2D3B54565034AFA58722598B9C6C9A4580507668_H
#define CONTROLMODE_T2D3B54565034AFA58722598B9C6C9A4580507668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductContextMenu_ControlMode
struct  ControlMode_t2D3B54565034AFA58722598B9C6C9A4580507668 
{
public:
	// System.Int32 DLLCore.ProductContextMenu_ControlMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlMode_t2D3B54565034AFA58722598B9C6C9A4580507668, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLMODE_T2D3B54565034AFA58722598B9C6C9A4580507668_H
#ifndef MATERIALTYPE_TDDE095F7F22E9C1859D762C2744ED61C2B83EECB_H
#define MATERIALTYPE_TDDE095F7F22E9C1859D762C2744ED61C2B83EECB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.Styler_MaterialType
struct  MaterialType_tDDE095F7F22E9C1859D762C2744ED61C2B83EECB 
{
public:
	// System.Int32 DLLCore.UnitComponents.Styler_MaterialType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaterialType_tDDE095F7F22E9C1859D762C2744ED61C2B83EECB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALTYPE_TDDE095F7F22E9C1859D762C2744ED61C2B83EECB_H
#ifndef TTYPE_T2A813D5A1723CB1BA5EBA63B9F0D49B0F582200F_H
#define TTYPE_T2A813D5A1723CB1BA5EBA63B9F0D49B0F582200F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.Transformer_TType
struct  TType_t2A813D5A1723CB1BA5EBA63B9F0D49B0F582200F 
{
public:
	// System.Int32 DLLCore.UnitComponents.Transformer_TType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TType_t2A813D5A1723CB1BA5EBA63B9F0D49B0F582200F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TTYPE_T2A813D5A1723CB1BA5EBA63B9F0D49B0F582200F_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef NULLABLE_1_T0A971E79CDFE60BF894C5F366427CB71B7601EC2_H
#define NULLABLE_1_T0A971E79CDFE60BF894C5F366427CB71B7601EC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_t0A971E79CDFE60BF894C5F366427CB71B7601EC2 
{
public:
	// T System.Nullable`1::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0A971E79CDFE60BF894C5F366427CB71B7601EC2, ___value_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_0() const { return ___value_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0A971E79CDFE60BF894C5F366427CB71B7601EC2, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0A971E79CDFE60BF894C5F366427CB71B7601EC2_H
#ifndef ASSETADVANCEDCONFIG_TFE8A1C0D77BF163613125759158C33B1814CE7F6_H
#define ASSETADVANCEDCONFIG_TFE8A1C0D77BF163613125759158C33B1814CE7F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetAdvancedConfig
struct  AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6  : public RuntimeObject
{
public:
	// System.String TriLib.AssetAdvancedConfig::Key
	String_t* ___Key_0;
	// System.Int32 TriLib.AssetAdvancedConfig::IntValue
	int32_t ___IntValue_1;
	// System.Single TriLib.AssetAdvancedConfig::FloatValue
	float ___FloatValue_2;
	// System.Boolean TriLib.AssetAdvancedConfig::BoolValue
	bool ___BoolValue_3;
	// System.String TriLib.AssetAdvancedConfig::StringValue
	String_t* ___StringValue_4;
	// UnityEngine.Vector3 TriLib.AssetAdvancedConfig::TranslationValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___TranslationValue_5;
	// UnityEngine.Vector3 TriLib.AssetAdvancedConfig::RotationValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RotationValue_6;
	// UnityEngine.Vector3 TriLib.AssetAdvancedConfig::ScaleValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___ScaleValue_7;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_IntValue_1() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6, ___IntValue_1)); }
	inline int32_t get_IntValue_1() const { return ___IntValue_1; }
	inline int32_t* get_address_of_IntValue_1() { return &___IntValue_1; }
	inline void set_IntValue_1(int32_t value)
	{
		___IntValue_1 = value;
	}

	inline static int32_t get_offset_of_FloatValue_2() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6, ___FloatValue_2)); }
	inline float get_FloatValue_2() const { return ___FloatValue_2; }
	inline float* get_address_of_FloatValue_2() { return &___FloatValue_2; }
	inline void set_FloatValue_2(float value)
	{
		___FloatValue_2 = value;
	}

	inline static int32_t get_offset_of_BoolValue_3() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6, ___BoolValue_3)); }
	inline bool get_BoolValue_3() const { return ___BoolValue_3; }
	inline bool* get_address_of_BoolValue_3() { return &___BoolValue_3; }
	inline void set_BoolValue_3(bool value)
	{
		___BoolValue_3 = value;
	}

	inline static int32_t get_offset_of_StringValue_4() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6, ___StringValue_4)); }
	inline String_t* get_StringValue_4() const { return ___StringValue_4; }
	inline String_t** get_address_of_StringValue_4() { return &___StringValue_4; }
	inline void set_StringValue_4(String_t* value)
	{
		___StringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___StringValue_4), value);
	}

	inline static int32_t get_offset_of_TranslationValue_5() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6, ___TranslationValue_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_TranslationValue_5() const { return ___TranslationValue_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_TranslationValue_5() { return &___TranslationValue_5; }
	inline void set_TranslationValue_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___TranslationValue_5 = value;
	}

	inline static int32_t get_offset_of_RotationValue_6() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6, ___RotationValue_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RotationValue_6() const { return ___RotationValue_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RotationValue_6() { return &___RotationValue_6; }
	inline void set_RotationValue_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RotationValue_6 = value;
	}

	inline static int32_t get_offset_of_ScaleValue_7() { return static_cast<int32_t>(offsetof(AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6, ___ScaleValue_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_ScaleValue_7() const { return ___ScaleValue_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_ScaleValue_7() { return &___ScaleValue_7; }
	inline void set_ScaleValue_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___ScaleValue_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETADVANCEDCONFIG_TFE8A1C0D77BF163613125759158C33B1814CE7F6_H
#ifndef ASSETADVANCEDCONFIGTYPE_T774488E8F923B69FD6AEBE1610EF9CAD1B15BCCB_H
#define ASSETADVANCEDCONFIGTYPE_T774488E8F923B69FD6AEBE1610EF9CAD1B15BCCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetAdvancedConfigType
struct  AssetAdvancedConfigType_t774488E8F923B69FD6AEBE1610EF9CAD1B15BCCB 
{
public:
	// System.Int32 TriLib.AssetAdvancedConfigType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssetAdvancedConfigType_t774488E8F923B69FD6AEBE1610EF9CAD1B15BCCB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETADVANCEDCONFIGTYPE_T774488E8F923B69FD6AEBE1610EF9CAD1B15BCCB_H
#ifndef ASSETADVANCEDPROPERTYCLASSNAMES_T10FAFFA3553FF6537076DE42775100534BFB94CF_H
#define ASSETADVANCEDPROPERTYCLASSNAMES_T10FAFFA3553FF6537076DE42775100534BFB94CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetAdvancedPropertyClassNames
struct  AssetAdvancedPropertyClassNames_t10FAFFA3553FF6537076DE42775100534BFB94CF 
{
public:
	// System.Int32 TriLib.AssetAdvancedPropertyClassNames::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssetAdvancedPropertyClassNames_t10FAFFA3553FF6537076DE42775100534BFB94CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETADVANCEDPROPERTYCLASSNAMES_T10FAFFA3553FF6537076DE42775100534BFB94CF_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef A_TCC7A80333A05F0558D846DC2AAC0F4831F6F55B4_H
#define A_TCC7A80333A05F0558D846DC2AAC0F4831F6F55B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ModelLoader_a
struct  a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4  : public RuntimeObject
{
public:
	// System.Int32 DLLCore.ModelLoader_a::a
	int32_t ___a_0;
	// System.Object DLLCore.ModelLoader_a::b
	RuntimeObject * ___b_1;
	// DLLCore.ModelLoader DLLCore.ModelLoader_a::c
	ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * ___c_2;
	// DLLCore.GlobalVar_ModelType DLLCore.ModelLoader_a::d
	int32_t ___d_3;
	// System.String DLLCore.ModelLoader_a::e
	String_t* ___e_4;
	// System.String DLLCore.ModelLoader_a::f
	String_t* ___f_5;
	// System.Action`1<UnityEngine.GameObject> DLLCore.ModelLoader_a::g
	Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * ___g_6;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4, ___b_1)); }
	inline RuntimeObject * get_b_1() const { return ___b_1; }
	inline RuntimeObject ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(RuntimeObject * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4, ___c_2)); }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * get_c_2() const { return ___c_2; }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D ** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4, ___d_3)); }
	inline int32_t get_d_3() const { return ___d_3; }
	inline int32_t* get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(int32_t value)
	{
		___d_3 = value;
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4, ___e_4)); }
	inline String_t* get_e_4() const { return ___e_4; }
	inline String_t** get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(String_t* value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier((&___e_4), value);
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4, ___f_5)); }
	inline String_t* get_f_5() const { return ___f_5; }
	inline String_t** get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(String_t* value)
	{
		___f_5 = value;
		Il2CppCodeGenWriteBarrier((&___f_5), value);
	}

	inline static int32_t get_offset_of_g_6() { return static_cast<int32_t>(offsetof(a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4, ___g_6)); }
	inline Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * get_g_6() const { return ___g_6; }
	inline Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 ** get_address_of_g_6() { return &___g_6; }
	inline void set_g_6(Action_1_t04806D62F06C9B61C3EBC53AD3B2C16C26C49D96 * value)
	{
		___g_6 = value;
		Il2CppCodeGenWriteBarrier((&___g_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_TCC7A80333A05F0558D846DC2AAC0F4831F6F55B4_H
#ifndef A_TFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C_H
#define A_TFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Detector_a
struct  a_tFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C  : public RuntimeObject
{
public:
	// UnityEngine.RaycastHit DLLCore.ProductComponents.Detector_a::a
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  ___a_0;
	// DLLCore.ProductComponents.Detector DLLCore.ProductComponents.Detector_a::b
	Detector_tE440186490449828DD72FAED201287D60C7E9498 * ___b_1;
	// System.Action DLLCore.ProductComponents.Detector_a::c
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___c_2;
	// System.Action DLLCore.ProductComponents.Detector_a::d
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___d_3;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_tFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C, ___a_0)); }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  get_a_0() const { return ___a_0; }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_tFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C, ___b_1)); }
	inline Detector_tE440186490449828DD72FAED201287D60C7E9498 * get_b_1() const { return ___b_1; }
	inline Detector_tE440186490449828DD72FAED201287D60C7E9498 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Detector_tE440186490449828DD72FAED201287D60C7E9498 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(a_tFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C, ___c_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_c_2() const { return ___c_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(a_tFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C, ___d_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_d_3() const { return ___d_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_TFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C_H
#ifndef PRODUCTDATA_TCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4_H
#define PRODUCTDATA_TCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Loader_ProductData
struct  ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4  : public RuntimeObject
{
public:
	// System.String DLLCore.ProductComponents.Loader_ProductData::planeType
	String_t* ___planeType_0;
	// DLLCore.ProductComponents.Loader_ProductData_ActionType DLLCore.ProductComponents.Loader_ProductData::aType
	int32_t ___aType_1;
	// System.Int32 DLLCore.ProductComponents.Loader_ProductData::bedType
	int32_t ___bedType_2;

public:
	inline static int32_t get_offset_of_planeType_0() { return static_cast<int32_t>(offsetof(ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4, ___planeType_0)); }
	inline String_t* get_planeType_0() const { return ___planeType_0; }
	inline String_t** get_address_of_planeType_0() { return &___planeType_0; }
	inline void set_planeType_0(String_t* value)
	{
		___planeType_0 = value;
		Il2CppCodeGenWriteBarrier((&___planeType_0), value);
	}

	inline static int32_t get_offset_of_aType_1() { return static_cast<int32_t>(offsetof(ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4, ___aType_1)); }
	inline int32_t get_aType_1() const { return ___aType_1; }
	inline int32_t* get_address_of_aType_1() { return &___aType_1; }
	inline void set_aType_1(int32_t value)
	{
		___aType_1 = value;
	}

	inline static int32_t get_offset_of_bedType_2() { return static_cast<int32_t>(offsetof(ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4, ___bedType_2)); }
	inline int32_t get_bedType_2() const { return ___bedType_2; }
	inline int32_t* get_address_of_bedType_2() { return &___bedType_2; }
	inline void set_bedType_2(int32_t value)
	{
		___bedType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTDATA_TCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4_H
#ifndef A_TD907EAFF666CDD709FA14A29050C6750D114625C_H
#define A_TD907EAFF666CDD709FA14A29050C6750D114625C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.Styler_a
struct  a_tD907EAFF666CDD709FA14A29050C6750D114625C  : public RuntimeObject
{
public:
	// DLLCore.UnitComponents.Styler DLLCore.UnitComponents.Styler_a::a
	Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0 * ___a_0;
	// UnityEngine.RaycastHit DLLCore.UnitComponents.Styler_a::b
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_tD907EAFF666CDD709FA14A29050C6750D114625C, ___a_0)); }
	inline Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0 * get_a_0() const { return ___a_0; }
	inline Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_tD907EAFF666CDD709FA14A29050C6750D114625C, ___b_1)); }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  get_b_1() const { return ___b_1; }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_TD907EAFF666CDD709FA14A29050C6750D114625C_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef TOUCHDOWNCALLBACK_T0B197FB7BD9E767285C88BAF51E0C6D434C81EAC_H
#define TOUCHDOWNCALLBACK_T0B197FB7BD9E767285C88BAF51E0C6D434C81EAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.InputManager_TouchDownCallback
struct  TouchDownCallback_t0B197FB7BD9E767285C88BAF51E0C6D434C81EAC  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHDOWNCALLBACK_T0B197FB7BD9E767285C88BAF51E0C6D434C81EAC_H
#ifndef LOADCALLBACK_T45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B_H
#define LOADCALLBACK_T45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController_LoadCallback
struct  LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADCALLBACK_T45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B_H
#ifndef SCREENSHOTPATH_T5E0DCEF7D4B345890D428A110D3532816CD973A8_H
#define SCREENSHOTPATH_T5E0DCEF7D4B345890D428A110D3532816CD973A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ToolController_ScreenShotPath
struct  ScreenShotPath_t5E0DCEF7D4B345890D428A110D3532816CD973A8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSHOTPATH_T5E0DCEF7D4B345890D428A110D3532816CD973A8_H
#ifndef LOADCALLBACK_T2161C8E09694A1ECBB49F840CE6B9B148422059B_H
#define LOADCALLBACK_T2161C8E09694A1ECBB49F840CE6B9B148422059B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitController_LoadCallback
struct  LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADCALLBACK_T2161C8E09694A1ECBB49F840CE6B9B148422059B_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CONTROLLER_T3FE6F0551E59C0E1D1745D16A978680C8137DB90_H
#define CONTROLLER_T3FE6F0551E59C0E1D1745D16A978680C8137DB90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.Controller
struct  Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLER_T3FE6F0551E59C0E1D1745D16A978680C8137DB90_H
#ifndef GIZMOCLICKDETECTION_TDAAFFE3DF2CC333B58B4461B9117E21775762DC5_H
#define GIZMOCLICKDETECTION_TDAAFFE3DF2CC333B58B4461B9117E21775762DC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GizmoClickDetection
struct  GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera DLLCore.GizmoClickDetection::gizmoCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___gizmoCamera_5;
	// UnityEngine.LayerMask DLLCore.GizmoClickDetection::gizmoLayer
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___gizmoLayer_6;
	// UnityEngine.GameObject[] DLLCore.GizmoClickDetection::targets
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___targets_7;
	// UnityEngine.Material DLLCore.GizmoClickDetection::highlight
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___highlight_8;
	// System.Collections.Generic.Dictionary`2<UnityEngine.MeshRenderer,UnityEngine.Material> DLLCore.GizmoClickDetection::previousMaterials
	Dictionary_2_tC5140A4BEF241546EB4109AF3928A6AE7BC126CF * ___previousMaterials_9;
	// System.Boolean DLLCore.GizmoClickDetection::pressing
	bool ___pressing_10;
	// System.Boolean DLLCore.GizmoClickDetection::pressingPlane
	bool ___pressingPlane_11;
	// UnityEngine.Vector3 DLLCore.GizmoClickDetection::fromCenterToHitPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___fromCenterToHitPoint_12;
	// UnityEngine.Vector3 DLLCore.GizmoClickDetection::hitPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___hitPoint_13;
	// UnityEngine.Vector3 DLLCore.GizmoClickDetection::hitPointRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___hitPointRotation_14;

public:
	inline static int32_t get_offset_of_gizmoCamera_5() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5, ___gizmoCamera_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_gizmoCamera_5() const { return ___gizmoCamera_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_gizmoCamera_5() { return &___gizmoCamera_5; }
	inline void set_gizmoCamera_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___gizmoCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___gizmoCamera_5), value);
	}

	inline static int32_t get_offset_of_gizmoLayer_6() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5, ___gizmoLayer_6)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_gizmoLayer_6() const { return ___gizmoLayer_6; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_gizmoLayer_6() { return &___gizmoLayer_6; }
	inline void set_gizmoLayer_6(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___gizmoLayer_6 = value;
	}

	inline static int32_t get_offset_of_targets_7() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5, ___targets_7)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_targets_7() const { return ___targets_7; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_targets_7() { return &___targets_7; }
	inline void set_targets_7(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___targets_7 = value;
		Il2CppCodeGenWriteBarrier((&___targets_7), value);
	}

	inline static int32_t get_offset_of_highlight_8() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5, ___highlight_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_highlight_8() const { return ___highlight_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_highlight_8() { return &___highlight_8; }
	inline void set_highlight_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___highlight_8 = value;
		Il2CppCodeGenWriteBarrier((&___highlight_8), value);
	}

	inline static int32_t get_offset_of_previousMaterials_9() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5, ___previousMaterials_9)); }
	inline Dictionary_2_tC5140A4BEF241546EB4109AF3928A6AE7BC126CF * get_previousMaterials_9() const { return ___previousMaterials_9; }
	inline Dictionary_2_tC5140A4BEF241546EB4109AF3928A6AE7BC126CF ** get_address_of_previousMaterials_9() { return &___previousMaterials_9; }
	inline void set_previousMaterials_9(Dictionary_2_tC5140A4BEF241546EB4109AF3928A6AE7BC126CF * value)
	{
		___previousMaterials_9 = value;
		Il2CppCodeGenWriteBarrier((&___previousMaterials_9), value);
	}

	inline static int32_t get_offset_of_pressing_10() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5, ___pressing_10)); }
	inline bool get_pressing_10() const { return ___pressing_10; }
	inline bool* get_address_of_pressing_10() { return &___pressing_10; }
	inline void set_pressing_10(bool value)
	{
		___pressing_10 = value;
	}

	inline static int32_t get_offset_of_pressingPlane_11() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5, ___pressingPlane_11)); }
	inline bool get_pressingPlane_11() const { return ___pressingPlane_11; }
	inline bool* get_address_of_pressingPlane_11() { return &___pressingPlane_11; }
	inline void set_pressingPlane_11(bool value)
	{
		___pressingPlane_11 = value;
	}

	inline static int32_t get_offset_of_fromCenterToHitPoint_12() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5, ___fromCenterToHitPoint_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_fromCenterToHitPoint_12() const { return ___fromCenterToHitPoint_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_fromCenterToHitPoint_12() { return &___fromCenterToHitPoint_12; }
	inline void set_fromCenterToHitPoint_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___fromCenterToHitPoint_12 = value;
	}

	inline static int32_t get_offset_of_hitPoint_13() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5, ___hitPoint_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_hitPoint_13() const { return ___hitPoint_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_hitPoint_13() { return &___hitPoint_13; }
	inline void set_hitPoint_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___hitPoint_13 = value;
	}

	inline static int32_t get_offset_of_hitPointRotation_14() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5, ___hitPointRotation_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_hitPointRotation_14() const { return ___hitPointRotation_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_hitPointRotation_14() { return &___hitPointRotation_14; }
	inline void set_hitPointRotation_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___hitPointRotation_14 = value;
	}
};

struct GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5_StaticFields
{
public:
	// System.Boolean DLLCore.GizmoClickDetection::ALREADY_CLICKED
	bool ___ALREADY_CLICKED_4;

public:
	inline static int32_t get_offset_of_ALREADY_CLICKED_4() { return static_cast<int32_t>(offsetof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5_StaticFields, ___ALREADY_CLICKED_4)); }
	inline bool get_ALREADY_CLICKED_4() const { return ___ALREADY_CLICKED_4; }
	inline bool* get_address_of_ALREADY_CLICKED_4() { return &___ALREADY_CLICKED_4; }
	inline void set_ALREADY_CLICKED_4(bool value)
	{
		___ALREADY_CLICKED_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIZMOCLICKDETECTION_TDAAFFE3DF2CC333B58B4461B9117E21775762DC5_H
#ifndef GIZMOSCALESCRIPT_TC9961504A0C6F393CB85758C3DDB56FB52CC8F37_H
#define GIZMOSCALESCRIPT_TC9961504A0C6F393CB85758C3DDB56FB52CC8F37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GizmoScaleScript
struct  GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single DLLCore.GizmoScaleScript::a
	float ___a_4;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::xHandle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___xHandle_5;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::yHandle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___yHandle_6;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::zHandle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___zHandle_7;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::xCube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___xCube_8;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::xCylinder
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___xCylinder_9;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::yCube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___yCube_10;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::yCylinder
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___yCylinder_11;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::zCube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___zCube_12;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::zCylinder
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___zCylinder_13;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::centerHandle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___centerHandle_14;
	// UnityEngine.GameObject DLLCore.GizmoScaleScript::scaleTarget
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___scaleTarget_15;
	// DLLCore.GizmoClickDetection[] DLLCore.GizmoScaleScript::b
	GizmoClickDetectionU5BU5D_t2BB934C874E1A0505C5BB28C65B52B1587CBAE3D* ___b_16;
	// System.Nullable`1<UnityEngine.Vector3> DLLCore.GizmoScaleScript::c
	Nullable_1_t0A971E79CDFE60BF894C5F366427CB71B7601EC2  ___c_17;
	// System.Boolean DLLCore.GizmoScaleScript::d
	bool ___d_18;
	// System.Int32 DLLCore.GizmoScaleScript::e
	int32_t ___e_19;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::f
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___f_20;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::g
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___g_21;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::h
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___h_22;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::i
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___i_23;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::j
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___j_24;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::k
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___k_25;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::l
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___l_26;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::m
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_27;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::n
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___n_28;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::o
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___o_29;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::p
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p_30;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::q
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___q_31;
	// DLLCore.GizmoTranslateScript_RaycastPlane DLLCore.GizmoScaleScript::r
	int32_t ___r_32;
	// System.Boolean DLLCore.GizmoScaleScript::s
	bool ___s_33;
	// UnityEngine.Vector3 DLLCore.GizmoScaleScript::t
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___t_34;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___a_4)); }
	inline float get_a_4() const { return ___a_4; }
	inline float* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(float value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_xHandle_5() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___xHandle_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_xHandle_5() const { return ___xHandle_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_xHandle_5() { return &___xHandle_5; }
	inline void set_xHandle_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___xHandle_5 = value;
		Il2CppCodeGenWriteBarrier((&___xHandle_5), value);
	}

	inline static int32_t get_offset_of_yHandle_6() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___yHandle_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_yHandle_6() const { return ___yHandle_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_yHandle_6() { return &___yHandle_6; }
	inline void set_yHandle_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___yHandle_6 = value;
		Il2CppCodeGenWriteBarrier((&___yHandle_6), value);
	}

	inline static int32_t get_offset_of_zHandle_7() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___zHandle_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_zHandle_7() const { return ___zHandle_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_zHandle_7() { return &___zHandle_7; }
	inline void set_zHandle_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___zHandle_7 = value;
		Il2CppCodeGenWriteBarrier((&___zHandle_7), value);
	}

	inline static int32_t get_offset_of_xCube_8() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___xCube_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_xCube_8() const { return ___xCube_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_xCube_8() { return &___xCube_8; }
	inline void set_xCube_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___xCube_8 = value;
		Il2CppCodeGenWriteBarrier((&___xCube_8), value);
	}

	inline static int32_t get_offset_of_xCylinder_9() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___xCylinder_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_xCylinder_9() const { return ___xCylinder_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_xCylinder_9() { return &___xCylinder_9; }
	inline void set_xCylinder_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___xCylinder_9 = value;
		Il2CppCodeGenWriteBarrier((&___xCylinder_9), value);
	}

	inline static int32_t get_offset_of_yCube_10() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___yCube_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_yCube_10() const { return ___yCube_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_yCube_10() { return &___yCube_10; }
	inline void set_yCube_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___yCube_10 = value;
		Il2CppCodeGenWriteBarrier((&___yCube_10), value);
	}

	inline static int32_t get_offset_of_yCylinder_11() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___yCylinder_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_yCylinder_11() const { return ___yCylinder_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_yCylinder_11() { return &___yCylinder_11; }
	inline void set_yCylinder_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___yCylinder_11 = value;
		Il2CppCodeGenWriteBarrier((&___yCylinder_11), value);
	}

	inline static int32_t get_offset_of_zCube_12() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___zCube_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_zCube_12() const { return ___zCube_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_zCube_12() { return &___zCube_12; }
	inline void set_zCube_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___zCube_12 = value;
		Il2CppCodeGenWriteBarrier((&___zCube_12), value);
	}

	inline static int32_t get_offset_of_zCylinder_13() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___zCylinder_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_zCylinder_13() const { return ___zCylinder_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_zCylinder_13() { return &___zCylinder_13; }
	inline void set_zCylinder_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___zCylinder_13 = value;
		Il2CppCodeGenWriteBarrier((&___zCylinder_13), value);
	}

	inline static int32_t get_offset_of_centerHandle_14() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___centerHandle_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_centerHandle_14() const { return ___centerHandle_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_centerHandle_14() { return &___centerHandle_14; }
	inline void set_centerHandle_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___centerHandle_14 = value;
		Il2CppCodeGenWriteBarrier((&___centerHandle_14), value);
	}

	inline static int32_t get_offset_of_scaleTarget_15() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___scaleTarget_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_scaleTarget_15() const { return ___scaleTarget_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_scaleTarget_15() { return &___scaleTarget_15; }
	inline void set_scaleTarget_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___scaleTarget_15 = value;
		Il2CppCodeGenWriteBarrier((&___scaleTarget_15), value);
	}

	inline static int32_t get_offset_of_b_16() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___b_16)); }
	inline GizmoClickDetectionU5BU5D_t2BB934C874E1A0505C5BB28C65B52B1587CBAE3D* get_b_16() const { return ___b_16; }
	inline GizmoClickDetectionU5BU5D_t2BB934C874E1A0505C5BB28C65B52B1587CBAE3D** get_address_of_b_16() { return &___b_16; }
	inline void set_b_16(GizmoClickDetectionU5BU5D_t2BB934C874E1A0505C5BB28C65B52B1587CBAE3D* value)
	{
		___b_16 = value;
		Il2CppCodeGenWriteBarrier((&___b_16), value);
	}

	inline static int32_t get_offset_of_c_17() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___c_17)); }
	inline Nullable_1_t0A971E79CDFE60BF894C5F366427CB71B7601EC2  get_c_17() const { return ___c_17; }
	inline Nullable_1_t0A971E79CDFE60BF894C5F366427CB71B7601EC2 * get_address_of_c_17() { return &___c_17; }
	inline void set_c_17(Nullable_1_t0A971E79CDFE60BF894C5F366427CB71B7601EC2  value)
	{
		___c_17 = value;
	}

	inline static int32_t get_offset_of_d_18() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___d_18)); }
	inline bool get_d_18() const { return ___d_18; }
	inline bool* get_address_of_d_18() { return &___d_18; }
	inline void set_d_18(bool value)
	{
		___d_18 = value;
	}

	inline static int32_t get_offset_of_e_19() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___e_19)); }
	inline int32_t get_e_19() const { return ___e_19; }
	inline int32_t* get_address_of_e_19() { return &___e_19; }
	inline void set_e_19(int32_t value)
	{
		___e_19 = value;
	}

	inline static int32_t get_offset_of_f_20() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___f_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_f_20() const { return ___f_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_f_20() { return &___f_20; }
	inline void set_f_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___f_20 = value;
	}

	inline static int32_t get_offset_of_g_21() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___g_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_g_21() const { return ___g_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_g_21() { return &___g_21; }
	inline void set_g_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___g_21 = value;
	}

	inline static int32_t get_offset_of_h_22() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___h_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_h_22() const { return ___h_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_h_22() { return &___h_22; }
	inline void set_h_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___h_22 = value;
	}

	inline static int32_t get_offset_of_i_23() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___i_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_i_23() const { return ___i_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_i_23() { return &___i_23; }
	inline void set_i_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___i_23 = value;
	}

	inline static int32_t get_offset_of_j_24() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___j_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_j_24() const { return ___j_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_j_24() { return &___j_24; }
	inline void set_j_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___j_24 = value;
	}

	inline static int32_t get_offset_of_k_25() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___k_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_k_25() const { return ___k_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_k_25() { return &___k_25; }
	inline void set_k_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___k_25 = value;
	}

	inline static int32_t get_offset_of_l_26() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___l_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_l_26() const { return ___l_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_l_26() { return &___l_26; }
	inline void set_l_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___l_26 = value;
	}

	inline static int32_t get_offset_of_m_27() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___m_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_27() const { return ___m_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_27() { return &___m_27; }
	inline void set_m_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_27 = value;
	}

	inline static int32_t get_offset_of_n_28() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___n_28)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_n_28() const { return ___n_28; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_n_28() { return &___n_28; }
	inline void set_n_28(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___n_28 = value;
	}

	inline static int32_t get_offset_of_o_29() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___o_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_o_29() const { return ___o_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_o_29() { return &___o_29; }
	inline void set_o_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___o_29 = value;
	}

	inline static int32_t get_offset_of_p_30() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___p_30)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_p_30() const { return ___p_30; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_p_30() { return &___p_30; }
	inline void set_p_30(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___p_30 = value;
	}

	inline static int32_t get_offset_of_q_31() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___q_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_q_31() const { return ___q_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_q_31() { return &___q_31; }
	inline void set_q_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___q_31 = value;
	}

	inline static int32_t get_offset_of_r_32() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___r_32)); }
	inline int32_t get_r_32() const { return ___r_32; }
	inline int32_t* get_address_of_r_32() { return &___r_32; }
	inline void set_r_32(int32_t value)
	{
		___r_32 = value;
	}

	inline static int32_t get_offset_of_s_33() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___s_33)); }
	inline bool get_s_33() const { return ___s_33; }
	inline bool* get_address_of_s_33() { return &___s_33; }
	inline void set_s_33(bool value)
	{
		___s_33 = value;
	}

	inline static int32_t get_offset_of_t_34() { return static_cast<int32_t>(offsetof(GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37, ___t_34)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_t_34() const { return ___t_34; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_t_34() { return &___t_34; }
	inline void set_t_34(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___t_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIZMOSCALESCRIPT_TC9961504A0C6F393CB85758C3DDB56FB52CC8F37_H
#ifndef GIZMOTRANSLATESCRIPT_TF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4_H
#define GIZMOTRANSLATESCRIPT_TF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GizmoTranslateScript
struct  GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject DLLCore.GizmoTranslateScript::xAxisObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___xAxisObject_4;
	// UnityEngine.GameObject DLLCore.GizmoTranslateScript::yAxisObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___yAxisObject_5;
	// UnityEngine.GameObject DLLCore.GizmoTranslateScript::zAxisObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___zAxisObject_6;
	// UnityEngine.GameObject DLLCore.GizmoTranslateScript::translateTarget
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___translateTarget_7;
	// DLLCore.GizmoClickDetection[] DLLCore.GizmoTranslateScript::a
	GizmoClickDetectionU5BU5D_t2BB934C874E1A0505C5BB28C65B52B1587CBAE3D* ___a_8;
	// System.Boolean DLLCore.GizmoTranslateScript::b
	bool ___b_9;
	// UnityEngine.Vector3 DLLCore.GizmoTranslateScript::c
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___c_10;
	// DLLCore.GizmoTranslateScript_RaycastPlane DLLCore.GizmoTranslateScript::d
	int32_t ___d_11;

public:
	inline static int32_t get_offset_of_xAxisObject_4() { return static_cast<int32_t>(offsetof(GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4, ___xAxisObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_xAxisObject_4() const { return ___xAxisObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_xAxisObject_4() { return &___xAxisObject_4; }
	inline void set_xAxisObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___xAxisObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___xAxisObject_4), value);
	}

	inline static int32_t get_offset_of_yAxisObject_5() { return static_cast<int32_t>(offsetof(GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4, ___yAxisObject_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_yAxisObject_5() const { return ___yAxisObject_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_yAxisObject_5() { return &___yAxisObject_5; }
	inline void set_yAxisObject_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___yAxisObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___yAxisObject_5), value);
	}

	inline static int32_t get_offset_of_zAxisObject_6() { return static_cast<int32_t>(offsetof(GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4, ___zAxisObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_zAxisObject_6() const { return ___zAxisObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_zAxisObject_6() { return &___zAxisObject_6; }
	inline void set_zAxisObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___zAxisObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___zAxisObject_6), value);
	}

	inline static int32_t get_offset_of_translateTarget_7() { return static_cast<int32_t>(offsetof(GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4, ___translateTarget_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_translateTarget_7() const { return ___translateTarget_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_translateTarget_7() { return &___translateTarget_7; }
	inline void set_translateTarget_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___translateTarget_7 = value;
		Il2CppCodeGenWriteBarrier((&___translateTarget_7), value);
	}

	inline static int32_t get_offset_of_a_8() { return static_cast<int32_t>(offsetof(GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4, ___a_8)); }
	inline GizmoClickDetectionU5BU5D_t2BB934C874E1A0505C5BB28C65B52B1587CBAE3D* get_a_8() const { return ___a_8; }
	inline GizmoClickDetectionU5BU5D_t2BB934C874E1A0505C5BB28C65B52B1587CBAE3D** get_address_of_a_8() { return &___a_8; }
	inline void set_a_8(GizmoClickDetectionU5BU5D_t2BB934C874E1A0505C5BB28C65B52B1587CBAE3D* value)
	{
		___a_8 = value;
		Il2CppCodeGenWriteBarrier((&___a_8), value);
	}

	inline static int32_t get_offset_of_b_9() { return static_cast<int32_t>(offsetof(GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4, ___b_9)); }
	inline bool get_b_9() const { return ___b_9; }
	inline bool* get_address_of_b_9() { return &___b_9; }
	inline void set_b_9(bool value)
	{
		___b_9 = value;
	}

	inline static int32_t get_offset_of_c_10() { return static_cast<int32_t>(offsetof(GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4, ___c_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_c_10() const { return ___c_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_c_10() { return &___c_10; }
	inline void set_c_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___c_10 = value;
	}

	inline static int32_t get_offset_of_d_11() { return static_cast<int32_t>(offsetof(GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4, ___d_11)); }
	inline int32_t get_d_11() const { return ___d_11; }
	inline int32_t* get_address_of_d_11() { return &___d_11; }
	inline void set_d_11(int32_t value)
	{
		___d_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GIZMOTRANSLATESCRIPT_TF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4_H
#ifndef GRAPHICSETTING_TCF22B8E10279661682C3A923EAB75DF65F651AA8_H
#define GRAPHICSETTING_TCF22B8E10279661682C3A923EAB75DF65F651AA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.GraphicSetting
struct  GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 DLLCore.GraphicSetting::a
	int32_t ___a_4;
	// System.Int32 DLLCore.GraphicSetting::b
	int32_t ___b_5;
	// System.Single DLLCore.GraphicSetting::c
	float ___c_6;
	// System.Int32 DLLCore.GraphicSetting::d
	int32_t ___d_7;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8, ___a_4)); }
	inline int32_t get_a_4() const { return ___a_4; }
	inline int32_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(int32_t value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8, ___b_5)); }
	inline int32_t get_b_5() const { return ___b_5; }
	inline int32_t* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(int32_t value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8, ___c_6)); }
	inline float get_c_6() const { return ___c_6; }
	inline float* get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(float value)
	{
		___c_6 = value;
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8, ___d_7)); }
	inline int32_t get_d_7() const { return ___d_7; }
	inline int32_t* get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(int32_t value)
	{
		___d_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICSETTING_TCF22B8E10279661682C3A923EAB75DF65F651AA8_H
#ifndef MATERIALLOADER_T99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE_H
#define MATERIALLOADER_T99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.MaterialLoader
struct  MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Shader DLLCore.MaterialLoader::a
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___a_4;
	// UnityEngine.Shader DLLCore.MaterialLoader::b
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___b_5;
	// System.Collections.Generic.Queue`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.MeshRenderer>> DLLCore.MaterialLoader::c
	Queue_1_t6261BA7435D63FF1F2C491627C0581281B591DBE * ___c_6;
	// DLLCore.MaterialContainer DLLCore.MaterialLoader::d
	MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * ___d_7;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE, ___a_4)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_a_4() const { return ___a_4; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE, ___b_5)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_b_5() const { return ___b_5; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE, ___c_6)); }
	inline Queue_1_t6261BA7435D63FF1F2C491627C0581281B591DBE * get_c_6() const { return ___c_6; }
	inline Queue_1_t6261BA7435D63FF1F2C491627C0581281B591DBE ** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(Queue_1_t6261BA7435D63FF1F2C491627C0581281B591DBE * value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE, ___d_7)); }
	inline MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * get_d_7() const { return ___d_7; }
	inline MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 ** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALLOADER_T99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE_H
#ifndef MODELLOADER_T264992D6536DF569BE72CB7094F9A23CAD68299D_H
#define MODELLOADER_T264992D6536DF569BE72CB7094F9A23CAD68299D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ModelLoader
struct  ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TriLib.AssetDownloader DLLCore.ModelLoader::a
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B * ___a_4;
	// System.Boolean DLLCore.ModelLoader::b
	bool ___b_5;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D, ___a_4)); }
	inline AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B * get_a_4() const { return ___a_4; }
	inline AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D, ___b_5)); }
	inline bool get_b_5() const { return ___b_5; }
	inline bool* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(bool value)
	{
		___b_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELLOADER_T264992D6536DF569BE72CB7094F9A23CAD68299D_H
#ifndef PRODUCT_TD53AAA882EC6DD96DE9DB23D381C84047A40297C_H
#define PRODUCT_TD53AAA882EC6DD96DE9DB23D381C84047A40297C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.Product
struct  Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String DLLCore.Product::a
	String_t* ___a_4;
	// System.Boolean DLLCore.Product::b
	bool ___b_5;
	// System.String DLLCore.Product::c
	String_t* ___c_6;
	// Outline DLLCore.Product::d
	Outline_t1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3 * ___d_7;
	// UnityEngine.BoxCollider DLLCore.Product::e
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___e_8;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C, ___a_4)); }
	inline String_t* get_a_4() const { return ___a_4; }
	inline String_t** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(String_t* value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C, ___b_5)); }
	inline bool get_b_5() const { return ___b_5; }
	inline bool* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(bool value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C, ___c_6)); }
	inline String_t* get_c_6() const { return ___c_6; }
	inline String_t** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(String_t* value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C, ___d_7)); }
	inline Outline_t1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3 * get_d_7() const { return ___d_7; }
	inline Outline_t1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3 ** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(Outline_t1422FDB6FA41986E1E73650E350ACEC8F2B4E4F3 * value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}

	inline static int32_t get_offset_of_e_8() { return static_cast<int32_t>(offsetof(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C, ___e_8)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_e_8() const { return ___e_8; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_e_8() { return &___e_8; }
	inline void set_e_8(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___e_8 = value;
		Il2CppCodeGenWriteBarrier((&___e_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCT_TD53AAA882EC6DD96DE9DB23D381C84047A40297C_H
#ifndef DETECTOR_TE440186490449828DD72FAED201287D60C7E9498_H
#define DETECTOR_TE440186490449828DD72FAED201287D60C7E9498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Detector
struct  Detector_tE440186490449828DD72FAED201287D60C7E9498  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DLLCore.ProductComponents.Store DLLCore.ProductComponents.Detector::a
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * ___a_4;
	// DLLCore.GeneralSetting DLLCore.ProductComponents.Detector::b
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * ___b_5;
	// DLLCore.Product DLLCore.ProductComponents.Detector::c
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * ___c_6;
	// UnityEngine.Camera DLLCore.ProductComponents.Detector::d
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___d_7;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Detector_tE440186490449828DD72FAED201287D60C7E9498, ___a_4)); }
	inline Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * get_a_4() const { return ___a_4; }
	inline Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Detector_tE440186490449828DD72FAED201287D60C7E9498, ___b_5)); }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * get_b_5() const { return ___b_5; }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Detector_tE440186490449828DD72FAED201287D60C7E9498, ___c_6)); }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * get_c_6() const { return ___c_6; }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C ** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(Detector_tE440186490449828DD72FAED201287D60C7E9498, ___d_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_d_7() const { return ___d_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTOR_TE440186490449828DD72FAED201287D60C7E9498_H
#ifndef LOADER_TB0FF4878450ED419A5701379AFC8CCAF5FD066A7_H
#define LOADER_TB0FF4878450ED419A5701379AFC8CCAF5FD066A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Loader
struct  Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DLLCore.ProductComponents.Store DLLCore.ProductComponents.Loader::a
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * ___a_4;
	// DLLCore.ModelLoader DLLCore.ProductComponents.Loader::b
	ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * ___b_5;
	// DLLCore.MaterialLoader DLLCore.ProductComponents.Loader::c
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * ___c_6;
	// DLLCore.MaterialContainer DLLCore.ProductComponents.Loader::d
	MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * ___d_7;
	// DLLCore.GeneralSetting DLLCore.ProductComponents.Loader::e
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * ___e_8;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7, ___a_4)); }
	inline Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * get_a_4() const { return ___a_4; }
	inline Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7, ___b_5)); }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * get_b_5() const { return ___b_5; }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7, ___c_6)); }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * get_c_6() const { return ___c_6; }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE ** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7, ___d_7)); }
	inline MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * get_d_7() const { return ___d_7; }
	inline MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 ** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}

	inline static int32_t get_offset_of_e_8() { return static_cast<int32_t>(offsetof(Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7, ___e_8)); }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * get_e_8() const { return ___e_8; }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D ** get_address_of_e_8() { return &___e_8; }
	inline void set_e_8(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * value)
	{
		___e_8 = value;
		Il2CppCodeGenWriteBarrier((&___e_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADER_TB0FF4878450ED419A5701379AFC8CCAF5FD066A7_H
#ifndef REMOVER_T7D8C8F8B0950D56CCC1ACD94500C096AE133D701_H
#define REMOVER_T7D8C8F8B0950D56CCC1ACD94500C096AE133D701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Remover
struct  Remover_t7D8C8F8B0950D56CCC1ACD94500C096AE133D701  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOVER_T7D8C8F8B0950D56CCC1ACD94500C096AE133D701_H
#ifndef STORE_T9B09CB3FF242495A8082BFFAC901834FF8FCF932_H
#define STORE_T9B09CB3FF242495A8082BFFAC901834FF8FCF932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Store
struct  Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<DLLCore.Product> DLLCore.ProductComponents.Store::loadedProducts
	List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * ___loadedProducts_4;
	// System.Collections.Generic.List`1<DLLCore.Product> DLLCore.ProductComponents.Store::selectedProducts
	List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * ___selectedProducts_5;
	// UnityEngine.Vector3 DLLCore.ProductComponents.Store::centerOfSelectedProducts
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___centerOfSelectedProducts_6;
	// UnityEngine.Vector3[] DLLCore.ProductComponents.Store::distVecOfSelectedProducts
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___distVecOfSelectedProducts_7;
	// System.Int32 DLLCore.ProductComponents.Store::a
	int32_t ___a_8;
	// DLLCore.ProductContextMenu DLLCore.ProductComponents.Store::b
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54 * ___b_9;
	// UnityEngine.BoxCollider DLLCore.ProductComponents.Store::selectedBoxCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___selectedBoxCollider_10;
	// TransformData[] DLLCore.ProductComponents.Store::selectedProductsTransform
	TransformDataU5BU5D_tAAAA03F0DBD452D4D0E62FB73297A56DA0F3D842* ___selectedProductsTransform_11;
	// System.Boolean DLLCore.ProductComponents.Store::c
	bool ___c_12;
	// System.Boolean DLLCore.ProductComponents.Store::d
	bool ___d_13;
	// System.Boolean DLLCore.ProductComponents.Store::e
	bool ___e_14;
	// System.Boolean DLLCore.ProductComponents.Store::f
	bool ___f_15;
	// UnityEngine.Camera DLLCore.ProductComponents.Store::g
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___g_16;

public:
	inline static int32_t get_offset_of_loadedProducts_4() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___loadedProducts_4)); }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * get_loadedProducts_4() const { return ___loadedProducts_4; }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 ** get_address_of_loadedProducts_4() { return &___loadedProducts_4; }
	inline void set_loadedProducts_4(List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * value)
	{
		___loadedProducts_4 = value;
		Il2CppCodeGenWriteBarrier((&___loadedProducts_4), value);
	}

	inline static int32_t get_offset_of_selectedProducts_5() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___selectedProducts_5)); }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * get_selectedProducts_5() const { return ___selectedProducts_5; }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 ** get_address_of_selectedProducts_5() { return &___selectedProducts_5; }
	inline void set_selectedProducts_5(List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * value)
	{
		___selectedProducts_5 = value;
		Il2CppCodeGenWriteBarrier((&___selectedProducts_5), value);
	}

	inline static int32_t get_offset_of_centerOfSelectedProducts_6() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___centerOfSelectedProducts_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_centerOfSelectedProducts_6() const { return ___centerOfSelectedProducts_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_centerOfSelectedProducts_6() { return &___centerOfSelectedProducts_6; }
	inline void set_centerOfSelectedProducts_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___centerOfSelectedProducts_6 = value;
	}

	inline static int32_t get_offset_of_distVecOfSelectedProducts_7() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___distVecOfSelectedProducts_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_distVecOfSelectedProducts_7() const { return ___distVecOfSelectedProducts_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_distVecOfSelectedProducts_7() { return &___distVecOfSelectedProducts_7; }
	inline void set_distVecOfSelectedProducts_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___distVecOfSelectedProducts_7 = value;
		Il2CppCodeGenWriteBarrier((&___distVecOfSelectedProducts_7), value);
	}

	inline static int32_t get_offset_of_a_8() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___a_8)); }
	inline int32_t get_a_8() const { return ___a_8; }
	inline int32_t* get_address_of_a_8() { return &___a_8; }
	inline void set_a_8(int32_t value)
	{
		___a_8 = value;
	}

	inline static int32_t get_offset_of_b_9() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___b_9)); }
	inline ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54 * get_b_9() const { return ___b_9; }
	inline ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54 ** get_address_of_b_9() { return &___b_9; }
	inline void set_b_9(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54 * value)
	{
		___b_9 = value;
		Il2CppCodeGenWriteBarrier((&___b_9), value);
	}

	inline static int32_t get_offset_of_selectedBoxCollider_10() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___selectedBoxCollider_10)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_selectedBoxCollider_10() const { return ___selectedBoxCollider_10; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_selectedBoxCollider_10() { return &___selectedBoxCollider_10; }
	inline void set_selectedBoxCollider_10(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___selectedBoxCollider_10 = value;
		Il2CppCodeGenWriteBarrier((&___selectedBoxCollider_10), value);
	}

	inline static int32_t get_offset_of_selectedProductsTransform_11() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___selectedProductsTransform_11)); }
	inline TransformDataU5BU5D_tAAAA03F0DBD452D4D0E62FB73297A56DA0F3D842* get_selectedProductsTransform_11() const { return ___selectedProductsTransform_11; }
	inline TransformDataU5BU5D_tAAAA03F0DBD452D4D0E62FB73297A56DA0F3D842** get_address_of_selectedProductsTransform_11() { return &___selectedProductsTransform_11; }
	inline void set_selectedProductsTransform_11(TransformDataU5BU5D_tAAAA03F0DBD452D4D0E62FB73297A56DA0F3D842* value)
	{
		___selectedProductsTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___selectedProductsTransform_11), value);
	}

	inline static int32_t get_offset_of_c_12() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___c_12)); }
	inline bool get_c_12() const { return ___c_12; }
	inline bool* get_address_of_c_12() { return &___c_12; }
	inline void set_c_12(bool value)
	{
		___c_12 = value;
	}

	inline static int32_t get_offset_of_d_13() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___d_13)); }
	inline bool get_d_13() const { return ___d_13; }
	inline bool* get_address_of_d_13() { return &___d_13; }
	inline void set_d_13(bool value)
	{
		___d_13 = value;
	}

	inline static int32_t get_offset_of_e_14() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___e_14)); }
	inline bool get_e_14() const { return ___e_14; }
	inline bool* get_address_of_e_14() { return &___e_14; }
	inline void set_e_14(bool value)
	{
		___e_14 = value;
	}

	inline static int32_t get_offset_of_f_15() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___f_15)); }
	inline bool get_f_15() const { return ___f_15; }
	inline bool* get_address_of_f_15() { return &___f_15; }
	inline void set_f_15(bool value)
	{
		___f_15 = value;
	}

	inline static int32_t get_offset_of_g_16() { return static_cast<int32_t>(offsetof(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932, ___g_16)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_g_16() const { return ___g_16; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_g_16() { return &___g_16; }
	inline void set_g_16(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___g_16 = value;
		Il2CppCodeGenWriteBarrier((&___g_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORE_T9B09CB3FF242495A8082BFFAC901834FF8FCF932_H
#ifndef STYLER_T4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10_H
#define STYLER_T4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Styler
struct  Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DLLCore.ProductComponents.Styler_SType DLLCore.ProductComponents.Styler::a
	int32_t ___a_4;
	// DLLCore.ProductComponents.Styler_TType DLLCore.ProductComponents.Styler::b
	int32_t ___b_5;
	// DLLCore.Product DLLCore.ProductComponents.Styler::c
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * ___c_6;
	// System.Collections.Generic.List`1<DLLCore.Product> DLLCore.ProductComponents.Styler::d
	List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * ___d_7;
	// UnityEngine.Color DLLCore.ProductComponents.Styler::e
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___e_8;
	// DLLCore.ChangeColorCustomBoxCmd DLLCore.ProductComponents.Styler::f
	ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7 * ___f_9;
	// DLLCore.ChangeColorMultiCustomBoxCmd DLLCore.ProductComponents.Styler::g
	ChangeColorMultiCustomBoxCmd_tC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2 * ___g_10;
	// DLLCore.MaterialLoader DLLCore.ProductComponents.Styler::h
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * ___h_11;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10, ___a_4)); }
	inline int32_t get_a_4() const { return ___a_4; }
	inline int32_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(int32_t value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10, ___b_5)); }
	inline int32_t get_b_5() const { return ___b_5; }
	inline int32_t* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(int32_t value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10, ___c_6)); }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * get_c_6() const { return ___c_6; }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C ** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10, ___d_7)); }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * get_d_7() const { return ___d_7; }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 ** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}

	inline static int32_t get_offset_of_e_8() { return static_cast<int32_t>(offsetof(Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10, ___e_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_e_8() const { return ___e_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_e_8() { return &___e_8; }
	inline void set_e_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___e_8 = value;
	}

	inline static int32_t get_offset_of_f_9() { return static_cast<int32_t>(offsetof(Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10, ___f_9)); }
	inline ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7 * get_f_9() const { return ___f_9; }
	inline ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7 ** get_address_of_f_9() { return &___f_9; }
	inline void set_f_9(ChangeColorCustomBoxCmd_tB3B996485420AF5714BDEF530BD7528AFF7AD8E7 * value)
	{
		___f_9 = value;
		Il2CppCodeGenWriteBarrier((&___f_9), value);
	}

	inline static int32_t get_offset_of_g_10() { return static_cast<int32_t>(offsetof(Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10, ___g_10)); }
	inline ChangeColorMultiCustomBoxCmd_tC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2 * get_g_10() const { return ___g_10; }
	inline ChangeColorMultiCustomBoxCmd_tC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2 ** get_address_of_g_10() { return &___g_10; }
	inline void set_g_10(ChangeColorMultiCustomBoxCmd_tC70D8A0B3A11ADFBC9E35E59C9575D91A038C3F2 * value)
	{
		___g_10 = value;
		Il2CppCodeGenWriteBarrier((&___g_10), value);
	}

	inline static int32_t get_offset_of_h_11() { return static_cast<int32_t>(offsetof(Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10, ___h_11)); }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * get_h_11() const { return ___h_11; }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE ** get_address_of_h_11() { return &___h_11; }
	inline void set_h_11(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * value)
	{
		___h_11 = value;
		Il2CppCodeGenWriteBarrier((&___h_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLER_T4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10_H
#ifndef TRANSFORMER_T14C1129412B89EBD1DAFDB665B3530E01EA8D5F8_H
#define TRANSFORMER_T14C1129412B89EBD1DAFDB665B3530E01EA8D5F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductComponents.Transformer
struct  Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DLLCore.ProductComponents.Transformer_SType DLLCore.ProductComponents.Transformer::a
	int32_t ___a_4;
	// DLLCore.ProductComponents.Transformer_TType DLLCore.ProductComponents.Transformer::b
	int32_t ___b_5;
	// UnityEngine.Vector3 DLLCore.ProductComponents.Transformer::c
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___c_6;
	// UnityEngine.Quaternion DLLCore.ProductComponents.Transformer::d
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___d_7;
	// UnityEngine.Vector3 DLLCore.ProductComponents.Transformer::e
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___e_8;
	// DLLCore.Product DLLCore.ProductComponents.Transformer::f
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * ___f_9;
	// System.Collections.Generic.List`1<DLLCore.Product> DLLCore.ProductComponents.Transformer::g
	List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * ___g_10;
	// DLLCore.TransformProductCmd DLLCore.ProductComponents.Transformer::h
	TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7 * ___h_11;
	// DLLCore.TransformMultiProductCmd DLLCore.ProductComponents.Transformer::i
	TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059 * ___i_12;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8, ___a_4)); }
	inline int32_t get_a_4() const { return ___a_4; }
	inline int32_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(int32_t value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8, ___b_5)); }
	inline int32_t get_b_5() const { return ___b_5; }
	inline int32_t* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(int32_t value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8, ___c_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_c_6() const { return ___c_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___c_6 = value;
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8, ___d_7)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_d_7() const { return ___d_7; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___d_7 = value;
	}

	inline static int32_t get_offset_of_e_8() { return static_cast<int32_t>(offsetof(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8, ___e_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_e_8() const { return ___e_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_e_8() { return &___e_8; }
	inline void set_e_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___e_8 = value;
	}

	inline static int32_t get_offset_of_f_9() { return static_cast<int32_t>(offsetof(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8, ___f_9)); }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * get_f_9() const { return ___f_9; }
	inline Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C ** get_address_of_f_9() { return &___f_9; }
	inline void set_f_9(Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C * value)
	{
		___f_9 = value;
		Il2CppCodeGenWriteBarrier((&___f_9), value);
	}

	inline static int32_t get_offset_of_g_10() { return static_cast<int32_t>(offsetof(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8, ___g_10)); }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * get_g_10() const { return ___g_10; }
	inline List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 ** get_address_of_g_10() { return &___g_10; }
	inline void set_g_10(List_1_t6B77975BAF76BDBF0E1A2F33B2E1CD452B0567A6 * value)
	{
		___g_10 = value;
		Il2CppCodeGenWriteBarrier((&___g_10), value);
	}

	inline static int32_t get_offset_of_h_11() { return static_cast<int32_t>(offsetof(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8, ___h_11)); }
	inline TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7 * get_h_11() const { return ___h_11; }
	inline TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7 ** get_address_of_h_11() { return &___h_11; }
	inline void set_h_11(TransformProductCmd_t4C58C7BCB16C7B00099083784135BF420BF832D7 * value)
	{
		___h_11 = value;
		Il2CppCodeGenWriteBarrier((&___h_11), value);
	}

	inline static int32_t get_offset_of_i_12() { return static_cast<int32_t>(offsetof(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8, ___i_12)); }
	inline TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059 * get_i_12() const { return ___i_12; }
	inline TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059 ** get_address_of_i_12() { return &___i_12; }
	inline void set_i_12(TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059 * value)
	{
		___i_12 = value;
		Il2CppCodeGenWriteBarrier((&___i_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMER_T14C1129412B89EBD1DAFDB665B3530E01EA8D5F8_H
#ifndef PRODUCTCONTEXTMENU_T03EE77264706C40D7CBBFBD17B4076731B57EA54_H
#define PRODUCTCONTEXTMENU_T03EE77264706C40D7CBBFBD17B4076731B57EA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductContextMenu
struct  ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DLLCore.ProductComponents.Store DLLCore.ProductContextMenu::a
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * ___a_4;
	// DLLCore.ProductContextMenu_ControlMode DLLCore.ProductContextMenu::controlMode
	int32_t ___controlMode_5;
	// UnityEngine.GameObject DLLCore.ProductContextMenu::simpleRotatePanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___simpleRotatePanel_6;
	// UnityEngine.GameObject DLLCore.ProductContextMenu::rotateDirectionGo
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___rotateDirectionGo_7;
	// UnityEngine.GameObject DLLCore.ProductContextMenu::simpleMoveGo
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___simpleMoveGo_8;
	// UnityEngine.UI.Button DLLCore.ProductContextMenu::simpleRotateButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___simpleRotateButton_9;
	// UnityEngine.UI.Button DLLCore.ProductContextMenu::simpleMoveButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___simpleMoveButton_10;
	// UnityEngine.UI.Button DLLCore.ProductContextMenu::duplicateButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___duplicateButton_11;
	// UnityEngine.UI.Button DLLCore.ProductContextMenu::removeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___removeButton_12;
	// UnityEngine.Vector2 DLLCore.ProductContextMenu::b
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b_13;
	// UnityEngine.Vector2 DLLCore.ProductContextMenu::c
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___c_14;
	// DLLCore.ProductContextMenu_Action DLLCore.ProductContextMenu::d
	int32_t ___d_15;
	// System.Boolean DLLCore.ProductContextMenu::needsUpdate
	bool ___needsUpdate_16;
	// System.Boolean DLLCore.ProductContextMenu::e
	bool ___e_17;
	// DLLCore.TransformMultiProductCmd DLLCore.ProductContextMenu::f
	TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059 * ___f_18;
	// UnityEngine.Camera DLLCore.ProductContextMenu::g
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___g_19;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___a_4)); }
	inline Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * get_a_4() const { return ___a_4; }
	inline Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_controlMode_5() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___controlMode_5)); }
	inline int32_t get_controlMode_5() const { return ___controlMode_5; }
	inline int32_t* get_address_of_controlMode_5() { return &___controlMode_5; }
	inline void set_controlMode_5(int32_t value)
	{
		___controlMode_5 = value;
	}

	inline static int32_t get_offset_of_simpleRotatePanel_6() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___simpleRotatePanel_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_simpleRotatePanel_6() const { return ___simpleRotatePanel_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_simpleRotatePanel_6() { return &___simpleRotatePanel_6; }
	inline void set_simpleRotatePanel_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___simpleRotatePanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___simpleRotatePanel_6), value);
	}

	inline static int32_t get_offset_of_rotateDirectionGo_7() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___rotateDirectionGo_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_rotateDirectionGo_7() const { return ___rotateDirectionGo_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_rotateDirectionGo_7() { return &___rotateDirectionGo_7; }
	inline void set_rotateDirectionGo_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___rotateDirectionGo_7 = value;
		Il2CppCodeGenWriteBarrier((&___rotateDirectionGo_7), value);
	}

	inline static int32_t get_offset_of_simpleMoveGo_8() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___simpleMoveGo_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_simpleMoveGo_8() const { return ___simpleMoveGo_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_simpleMoveGo_8() { return &___simpleMoveGo_8; }
	inline void set_simpleMoveGo_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___simpleMoveGo_8 = value;
		Il2CppCodeGenWriteBarrier((&___simpleMoveGo_8), value);
	}

	inline static int32_t get_offset_of_simpleRotateButton_9() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___simpleRotateButton_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_simpleRotateButton_9() const { return ___simpleRotateButton_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_simpleRotateButton_9() { return &___simpleRotateButton_9; }
	inline void set_simpleRotateButton_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___simpleRotateButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___simpleRotateButton_9), value);
	}

	inline static int32_t get_offset_of_simpleMoveButton_10() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___simpleMoveButton_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_simpleMoveButton_10() const { return ___simpleMoveButton_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_simpleMoveButton_10() { return &___simpleMoveButton_10; }
	inline void set_simpleMoveButton_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___simpleMoveButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___simpleMoveButton_10), value);
	}

	inline static int32_t get_offset_of_duplicateButton_11() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___duplicateButton_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_duplicateButton_11() const { return ___duplicateButton_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_duplicateButton_11() { return &___duplicateButton_11; }
	inline void set_duplicateButton_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___duplicateButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___duplicateButton_11), value);
	}

	inline static int32_t get_offset_of_removeButton_12() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___removeButton_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_removeButton_12() const { return ___removeButton_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_removeButton_12() { return &___removeButton_12; }
	inline void set_removeButton_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___removeButton_12 = value;
		Il2CppCodeGenWriteBarrier((&___removeButton_12), value);
	}

	inline static int32_t get_offset_of_b_13() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___b_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_b_13() const { return ___b_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_b_13() { return &___b_13; }
	inline void set_b_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___b_13 = value;
	}

	inline static int32_t get_offset_of_c_14() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___c_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_c_14() const { return ___c_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_c_14() { return &___c_14; }
	inline void set_c_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___c_14 = value;
	}

	inline static int32_t get_offset_of_d_15() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___d_15)); }
	inline int32_t get_d_15() const { return ___d_15; }
	inline int32_t* get_address_of_d_15() { return &___d_15; }
	inline void set_d_15(int32_t value)
	{
		___d_15 = value;
	}

	inline static int32_t get_offset_of_needsUpdate_16() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___needsUpdate_16)); }
	inline bool get_needsUpdate_16() const { return ___needsUpdate_16; }
	inline bool* get_address_of_needsUpdate_16() { return &___needsUpdate_16; }
	inline void set_needsUpdate_16(bool value)
	{
		___needsUpdate_16 = value;
	}

	inline static int32_t get_offset_of_e_17() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___e_17)); }
	inline bool get_e_17() const { return ___e_17; }
	inline bool* get_address_of_e_17() { return &___e_17; }
	inline void set_e_17(bool value)
	{
		___e_17 = value;
	}

	inline static int32_t get_offset_of_f_18() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___f_18)); }
	inline TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059 * get_f_18() const { return ___f_18; }
	inline TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059 ** get_address_of_f_18() { return &___f_18; }
	inline void set_f_18(TransformMultiProductCmd_t5018111EC13328126AE853EFB6E2BAA9D3E49059 * value)
	{
		___f_18 = value;
		Il2CppCodeGenWriteBarrier((&___f_18), value);
	}

	inline static int32_t get_offset_of_g_19() { return static_cast<int32_t>(offsetof(ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54, ___g_19)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_g_19() const { return ___g_19; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_g_19() { return &___g_19; }
	inline void set_g_19(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___g_19 = value;
		Il2CppCodeGenWriteBarrier((&___g_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCONTEXTMENU_T03EE77264706C40D7CBBFBD17B4076731B57EA54_H
#ifndef REFLECTIONCONTROLS_TF4260F82066379B747D088BFA20909E0B6DF3AA1_H
#define REFLECTIONCONTROLS_TF4260F82066379B747D088BFA20909E0B6DF3AA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ReflectionControls
struct  ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> DLLCore.ReflectionControls::a
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___a_4;
	// DLLCore.UnitComponents.Unit DLLCore.ReflectionControls::b
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * ___b_5;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1, ___a_4)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_a_4() const { return ___a_4; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1, ___b_5)); }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * get_b_5() const { return ___b_5; }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONCONTROLS_TF4260F82066379B747D088BFA20909E0B6DF3AA1_H
#ifndef SINGLETON_1_T63CBC98B5F5500F123C1A0DA3DE5E46DB61D237E_H
#define SINGLETON_1_T63CBC98B5F5500F123C1A0DA3DE5E46DB61D237E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.Singleton`1<DLLCore.History>
struct  Singleton_1_t63CBC98B5F5500F123C1A0DA3DE5E46DB61D237E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t63CBC98B5F5500F123C1A0DA3DE5E46DB61D237E_StaticFields
{
public:
	// T DLLCore.Singleton`1::a
	History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58 * ___a_4;
	// System.Object DLLCore.Singleton`1::b
	RuntimeObject * ___b_5;
	// System.Boolean DLLCore.Singleton`1::c
	bool ___c_6;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Singleton_1_t63CBC98B5F5500F123C1A0DA3DE5E46DB61D237E_StaticFields, ___a_4)); }
	inline History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58 * get_a_4() const { return ___a_4; }
	inline History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Singleton_1_t63CBC98B5F5500F123C1A0DA3DE5E46DB61D237E_StaticFields, ___b_5)); }
	inline RuntimeObject * get_b_5() const { return ___b_5; }
	inline RuntimeObject ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(RuntimeObject * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Singleton_1_t63CBC98B5F5500F123C1A0DA3DE5E46DB61D237E_StaticFields, ___c_6)); }
	inline bool get_c_6() const { return ___c_6; }
	inline bool* get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(bool value)
	{
		___c_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T63CBC98B5F5500F123C1A0DA3DE5E46DB61D237E_H
#ifndef SINGLETON_1_T7A42E23FC7FB5A90A1EAD0F3F06B11B91542B13D_H
#define SINGLETON_1_T7A42E23FC7FB5A90A1EAD0F3F06B11B91542B13D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.Singleton`1<DLLCore.Main>
struct  Singleton_1_t7A42E23FC7FB5A90A1EAD0F3F06B11B91542B13D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t7A42E23FC7FB5A90A1EAD0F3F06B11B91542B13D_StaticFields
{
public:
	// T DLLCore.Singleton`1::a
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83 * ___a_4;
	// System.Object DLLCore.Singleton`1::b
	RuntimeObject * ___b_5;
	// System.Boolean DLLCore.Singleton`1::c
	bool ___c_6;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Singleton_1_t7A42E23FC7FB5A90A1EAD0F3F06B11B91542B13D_StaticFields, ___a_4)); }
	inline Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83 * get_a_4() const { return ___a_4; }
	inline Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Singleton_1_t7A42E23FC7FB5A90A1EAD0F3F06B11B91542B13D_StaticFields, ___b_5)); }
	inline RuntimeObject * get_b_5() const { return ___b_5; }
	inline RuntimeObject ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(RuntimeObject * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Singleton_1_t7A42E23FC7FB5A90A1EAD0F3F06B11B91542B13D_StaticFields, ___c_6)); }
	inline bool get_c_6() const { return ___c_6; }
	inline bool* get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(bool value)
	{
		___c_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T7A42E23FC7FB5A90A1EAD0F3F06B11B91542B13D_H
#ifndef LOADER_T2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF_H
#define LOADER_T2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.Loader
struct  Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject DLLCore.UnitComponents.Loader::a
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___a_4;
	// DLLCore.MaterialLoader DLLCore.UnitComponents.Loader::b
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * ___b_5;
	// DLLCore.ModelLoader DLLCore.UnitComponents.Loader::c
	ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * ___c_6;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF, ___a_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_a_4() const { return ___a_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF, ___b_5)); }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * get_b_5() const { return ___b_5; }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF, ___c_6)); }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * get_c_6() const { return ___c_6; }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D ** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADER_T2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF_H
#ifndef REMOVER_T014C01D9DA3634857EDA3B77F140905C24E69561_H
#define REMOVER_T014C01D9DA3634857EDA3B77F140905C24E69561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.Remover
struct  Remover_t014C01D9DA3634857EDA3B77F140905C24E69561  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOVER_T014C01D9DA3634857EDA3B77F140905C24E69561_H
#ifndef SPLITEDWALL_T43081FE6D3CC34E084B169E8E46F5C0FA32FABDB_H
#define SPLITEDWALL_T43081FE6D3CC34E084B169E8E46F5C0FA32FABDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.SplitedWall
struct  SplitedWall_t43081FE6D3CC34E084B169E8E46F5C0FA32FABDB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.Int32> DLLCore.UnitComponents.SplitedWall::vertexIndices
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___vertexIndices_4;
	// System.Single DLLCore.UnitComponents.SplitedWall::area
	float ___area_5;

public:
	inline static int32_t get_offset_of_vertexIndices_4() { return static_cast<int32_t>(offsetof(SplitedWall_t43081FE6D3CC34E084B169E8E46F5C0FA32FABDB, ___vertexIndices_4)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_vertexIndices_4() const { return ___vertexIndices_4; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_vertexIndices_4() { return &___vertexIndices_4; }
	inline void set_vertexIndices_4(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___vertexIndices_4 = value;
		Il2CppCodeGenWriteBarrier((&___vertexIndices_4), value);
	}

	inline static int32_t get_offset_of_area_5() { return static_cast<int32_t>(offsetof(SplitedWall_t43081FE6D3CC34E084B169E8E46F5C0FA32FABDB, ___area_5)); }
	inline float get_area_5() const { return ___area_5; }
	inline float* get_address_of_area_5() { return &___area_5; }
	inline void set_area_5(float value)
	{
		___area_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLITEDWALL_T43081FE6D3CC34E084B169E8E46F5C0FA32FABDB_H
#ifndef STORE_T483BC3798BF69BBB929C9E3848F58BCC0B9CCC35_H
#define STORE_T483BC3798BF69BBB929C9E3848F58BCC0B9CCC35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.Store
struct  Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DLLCore.UnitComponents.Unit DLLCore.UnitComponents.Store::unit
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * ___unit_4;
	// System.Boolean DLLCore.UnitComponents.Store::updateCeilingVisibility
	bool ___updateCeilingVisibility_5;
	// System.Boolean DLLCore.UnitComponents.Store::updateCeilingOpacity
	bool ___updateCeilingOpacity_6;
	// UnityEngine.Camera DLLCore.UnitComponents.Store::a
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___a_7;

public:
	inline static int32_t get_offset_of_unit_4() { return static_cast<int32_t>(offsetof(Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35, ___unit_4)); }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * get_unit_4() const { return ___unit_4; }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D ** get_address_of_unit_4() { return &___unit_4; }
	inline void set_unit_4(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * value)
	{
		___unit_4 = value;
		Il2CppCodeGenWriteBarrier((&___unit_4), value);
	}

	inline static int32_t get_offset_of_updateCeilingVisibility_5() { return static_cast<int32_t>(offsetof(Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35, ___updateCeilingVisibility_5)); }
	inline bool get_updateCeilingVisibility_5() const { return ___updateCeilingVisibility_5; }
	inline bool* get_address_of_updateCeilingVisibility_5() { return &___updateCeilingVisibility_5; }
	inline void set_updateCeilingVisibility_5(bool value)
	{
		___updateCeilingVisibility_5 = value;
	}

	inline static int32_t get_offset_of_updateCeilingOpacity_6() { return static_cast<int32_t>(offsetof(Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35, ___updateCeilingOpacity_6)); }
	inline bool get_updateCeilingOpacity_6() const { return ___updateCeilingOpacity_6; }
	inline bool* get_address_of_updateCeilingOpacity_6() { return &___updateCeilingOpacity_6; }
	inline void set_updateCeilingOpacity_6(bool value)
	{
		___updateCeilingOpacity_6 = value;
	}

	inline static int32_t get_offset_of_a_7() { return static_cast<int32_t>(offsetof(Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35, ___a_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_a_7() const { return ___a_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_a_7() { return &___a_7; }
	inline void set_a_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___a_7 = value;
		Il2CppCodeGenWriteBarrier((&___a_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORE_T483BC3798BF69BBB929C9E3848F58BCC0B9CCC35_H
#ifndef STYLER_T4C603A1C7019365B9B5D1E90331C836C785136D0_H
#define STYLER_T4C603A1C7019365B9B5D1E90331C836C785136D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.Styler
struct  Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean DLLCore.UnitComponents.Styler::isRunning
	bool ___isRunning_4;
	// DLLCore.UnitComponents.Styler_MaterialType DLLCore.UnitComponents.Styler::a
	int32_t ___a_5;
	// DLLCore.UnitComponents.Unit DLLCore.UnitComponents.Styler::b
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * ___b_6;
	// DLLCore.MaterialLoader DLLCore.UnitComponents.Styler::c
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * ___c_7;
	// System.String DLLCore.UnitComponents.Styler::d
	String_t* ___d_8;
	// DLLCore.ChangeMaterialUnitCmd DLLCore.UnitComponents.Styler::e
	ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9 * ___e_9;
	// UnityEngine.UI.RawImage DLLCore.UnitComponents.Styler::f
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___f_10;
	// UnityEngine.Camera DLLCore.UnitComponents.Styler::g
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___g_11;

public:
	inline static int32_t get_offset_of_isRunning_4() { return static_cast<int32_t>(offsetof(Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0, ___isRunning_4)); }
	inline bool get_isRunning_4() const { return ___isRunning_4; }
	inline bool* get_address_of_isRunning_4() { return &___isRunning_4; }
	inline void set_isRunning_4(bool value)
	{
		___isRunning_4 = value;
	}

	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0, ___a_5)); }
	inline int32_t get_a_5() const { return ___a_5; }
	inline int32_t* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(int32_t value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_b_6() { return static_cast<int32_t>(offsetof(Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0, ___b_6)); }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * get_b_6() const { return ___b_6; }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D ** get_address_of_b_6() { return &___b_6; }
	inline void set_b_6(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * value)
	{
		___b_6 = value;
		Il2CppCodeGenWriteBarrier((&___b_6), value);
	}

	inline static int32_t get_offset_of_c_7() { return static_cast<int32_t>(offsetof(Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0, ___c_7)); }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * get_c_7() const { return ___c_7; }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE ** get_address_of_c_7() { return &___c_7; }
	inline void set_c_7(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * value)
	{
		___c_7 = value;
		Il2CppCodeGenWriteBarrier((&___c_7), value);
	}

	inline static int32_t get_offset_of_d_8() { return static_cast<int32_t>(offsetof(Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0, ___d_8)); }
	inline String_t* get_d_8() const { return ___d_8; }
	inline String_t** get_address_of_d_8() { return &___d_8; }
	inline void set_d_8(String_t* value)
	{
		___d_8 = value;
		Il2CppCodeGenWriteBarrier((&___d_8), value);
	}

	inline static int32_t get_offset_of_e_9() { return static_cast<int32_t>(offsetof(Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0, ___e_9)); }
	inline ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9 * get_e_9() const { return ___e_9; }
	inline ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9 ** get_address_of_e_9() { return &___e_9; }
	inline void set_e_9(ChangeMaterialUnitCmd_tFE83D8D234AD2E7CBCCDCCCE56604924BF79E1C9 * value)
	{
		___e_9 = value;
		Il2CppCodeGenWriteBarrier((&___e_9), value);
	}

	inline static int32_t get_offset_of_f_10() { return static_cast<int32_t>(offsetof(Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0, ___f_10)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_f_10() const { return ___f_10; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_f_10() { return &___f_10; }
	inline void set_f_10(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___f_10 = value;
		Il2CppCodeGenWriteBarrier((&___f_10), value);
	}

	inline static int32_t get_offset_of_g_11() { return static_cast<int32_t>(offsetof(Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0, ___g_11)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_g_11() const { return ___g_11; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_g_11() { return &___g_11; }
	inline void set_g_11(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___g_11 = value;
		Il2CppCodeGenWriteBarrier((&___g_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STYLER_T4C603A1C7019365B9B5D1E90331C836C785136D0_H
#ifndef TRANSFORMER_TCBBA2E12184B07139C763FB3473B6CCEE529A3A4_H
#define TRANSFORMER_TCBBA2E12184B07139C763FB3473B6CCEE529A3A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.Transformer
struct  Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DLLCore.UnitComponents.Transformer_TType DLLCore.UnitComponents.Transformer::a
	int32_t ___a_4;
	// DLLCore.UnitComponents.Unit DLLCore.UnitComponents.Transformer::b
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * ___b_5;
	// DLLCore.TransformUnitCmd DLLCore.UnitComponents.Transformer::c
	TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C * ___c_6;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4, ___a_4)); }
	inline int32_t get_a_4() const { return ___a_4; }
	inline int32_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(int32_t value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4, ___b_5)); }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * get_b_5() const { return ___b_5; }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4, ___c_6)); }
	inline TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C * get_c_6() const { return ___c_6; }
	inline TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C ** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(TransformUnitCmd_t4420A5F41F20145B45284090530353ECF209595C * value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMER_TCBBA2E12184B07139C763FB3473B6CCEE529A3A4_H
#ifndef UNIT_T83E45E1757CCC1C02672CD4806ADD805FCE2270D_H
#define UNIT_T83E45E1757CCC1C02672CD4806ADD805FCE2270D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.Unit
struct  Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String DLLCore.UnitComponents.Unit::a
	String_t* ___a_4;
	// UnityEngine.Bounds DLLCore.UnitComponents.Unit::bounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___bounds_5;
	// System.Single DLLCore.UnitComponents.Unit::wallOpacity
	float ___wallOpacity_6;
	// DLLCore.UnitComponents.SplitedWalls DLLCore.UnitComponents.Unit::splitedWalls
	SplitedWalls_t3D6E1370EC4D5F1E735EF893FFBE5357C2FA9A3D * ___splitedWalls_7;
	// UnityEngine.GameObject DLLCore.UnitComponents.Unit::splitedWallsGo
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___splitedWallsGo_8;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D, ___a_4)); }
	inline String_t* get_a_4() const { return ___a_4; }
	inline String_t** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(String_t* value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_bounds_5() { return static_cast<int32_t>(offsetof(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D, ___bounds_5)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_bounds_5() const { return ___bounds_5; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_bounds_5() { return &___bounds_5; }
	inline void set_bounds_5(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___bounds_5 = value;
	}

	inline static int32_t get_offset_of_wallOpacity_6() { return static_cast<int32_t>(offsetof(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D, ___wallOpacity_6)); }
	inline float get_wallOpacity_6() const { return ___wallOpacity_6; }
	inline float* get_address_of_wallOpacity_6() { return &___wallOpacity_6; }
	inline void set_wallOpacity_6(float value)
	{
		___wallOpacity_6 = value;
	}

	inline static int32_t get_offset_of_splitedWalls_7() { return static_cast<int32_t>(offsetof(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D, ___splitedWalls_7)); }
	inline SplitedWalls_t3D6E1370EC4D5F1E735EF893FFBE5357C2FA9A3D * get_splitedWalls_7() const { return ___splitedWalls_7; }
	inline SplitedWalls_t3D6E1370EC4D5F1E735EF893FFBE5357C2FA9A3D ** get_address_of_splitedWalls_7() { return &___splitedWalls_7; }
	inline void set_splitedWalls_7(SplitedWalls_t3D6E1370EC4D5F1E735EF893FFBE5357C2FA9A3D * value)
	{
		___splitedWalls_7 = value;
		Il2CppCodeGenWriteBarrier((&___splitedWalls_7), value);
	}

	inline static int32_t get_offset_of_splitedWallsGo_8() { return static_cast<int32_t>(offsetof(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D, ___splitedWallsGo_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_splitedWallsGo_8() const { return ___splitedWallsGo_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_splitedWallsGo_8() { return &___splitedWallsGo_8; }
	inline void set_splitedWallsGo_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___splitedWallsGo_8 = value;
		Il2CppCodeGenWriteBarrier((&___splitedWallsGo_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T83E45E1757CCC1C02672CD4806ADD805FCE2270D_H
#ifndef WALLCOLLIDERCHANGER_T834A1B1E16CBF40D89B17BFCFB6130B6C164723D_H
#define WALLCOLLIDERCHANGER_T834A1B1E16CBF40D89B17BFCFB6130B6C164723D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.WallColliderChanger
struct  WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DLLCore.UnitComponents.Unit DLLCore.UnitComponents.WallColliderChanger::a
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * ___a_4;
	// System.Boolean DLLCore.UnitComponents.WallColliderChanger::b
	bool ___b_5;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D, ___a_4)); }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * get_a_4() const { return ___a_4; }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D, ___b_5)); }
	inline bool get_b_5() const { return ___b_5; }
	inline bool* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(bool value)
	{
		___b_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WALLCOLLIDERCHANGER_T834A1B1E16CBF40D89B17BFCFB6130B6C164723D_H
#ifndef WALLOPACITYCHANGER_TACA7B02D312F3E3584729A634AE4E175510C5DCC_H
#define WALLOPACITYCHANGER_TACA7B02D312F3E3584729A634AE4E175510C5DCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitComponents.WallOpacityChanger
struct  WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// DLLCore.UnitComponents.Unit DLLCore.UnitComponents.WallOpacityChanger::a
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * ___a_4;
	// System.Single DLLCore.UnitComponents.WallOpacityChanger::b
	float ___b_5;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC, ___a_4)); }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * get_a_4() const { return ___a_4; }
	inline Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC, ___b_5)); }
	inline float get_b_5() const { return ___b_5; }
	inline float* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(float value)
	{
		___b_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WALLOPACITYCHANGER_TACA7B02D312F3E3584729A634AE4E175510C5DCC_H
#ifndef ASSETDOWNLOADER_TE0A11D81277C91FD04D01FF023FFBE84A6CFA30B_H
#define ASSETDOWNLOADER_TE0A11D81277C91FD04D01FF023FFBE84A6CFA30B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetDownloader
struct  AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean TriLib.AssetDownloader::AutoStart
	bool ___AutoStart_4;
	// System.String TriLib.AssetDownloader::AssetURI
	String_t* ___AssetURI_5;
	// System.Int32 TriLib.AssetDownloader::Timeout
	int32_t ___Timeout_6;
	// System.String TriLib.AssetDownloader::AssetExtension
	String_t* ___AssetExtension_7;
	// UnityEngine.GameObject TriLib.AssetDownloader::WrapperGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___WrapperGameObject_8;
	// System.Boolean TriLib.AssetDownloader::ShowProgress
	bool ___ShowProgress_9;
	// System.Boolean TriLib.AssetDownloader::Async
	bool ___Async_10;
	// TriLib.AssimpInterop_ProgressCallback TriLib.AssetDownloader::ProgressCallback
	ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * ___ProgressCallback_11;
	// UnityEngine.Networking.UnityWebRequest TriLib.AssetDownloader::a
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___a_12;
	// UnityEngine.GUIStyle TriLib.AssetDownloader::b
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___b_13;
	// System.String TriLib.AssetDownloader::c
	String_t* ___c_14;

public:
	inline static int32_t get_offset_of_AutoStart_4() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___AutoStart_4)); }
	inline bool get_AutoStart_4() const { return ___AutoStart_4; }
	inline bool* get_address_of_AutoStart_4() { return &___AutoStart_4; }
	inline void set_AutoStart_4(bool value)
	{
		___AutoStart_4 = value;
	}

	inline static int32_t get_offset_of_AssetURI_5() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___AssetURI_5)); }
	inline String_t* get_AssetURI_5() const { return ___AssetURI_5; }
	inline String_t** get_address_of_AssetURI_5() { return &___AssetURI_5; }
	inline void set_AssetURI_5(String_t* value)
	{
		___AssetURI_5 = value;
		Il2CppCodeGenWriteBarrier((&___AssetURI_5), value);
	}

	inline static int32_t get_offset_of_Timeout_6() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___Timeout_6)); }
	inline int32_t get_Timeout_6() const { return ___Timeout_6; }
	inline int32_t* get_address_of_Timeout_6() { return &___Timeout_6; }
	inline void set_Timeout_6(int32_t value)
	{
		___Timeout_6 = value;
	}

	inline static int32_t get_offset_of_AssetExtension_7() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___AssetExtension_7)); }
	inline String_t* get_AssetExtension_7() const { return ___AssetExtension_7; }
	inline String_t** get_address_of_AssetExtension_7() { return &___AssetExtension_7; }
	inline void set_AssetExtension_7(String_t* value)
	{
		___AssetExtension_7 = value;
		Il2CppCodeGenWriteBarrier((&___AssetExtension_7), value);
	}

	inline static int32_t get_offset_of_WrapperGameObject_8() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___WrapperGameObject_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_WrapperGameObject_8() const { return ___WrapperGameObject_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_WrapperGameObject_8() { return &___WrapperGameObject_8; }
	inline void set_WrapperGameObject_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___WrapperGameObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___WrapperGameObject_8), value);
	}

	inline static int32_t get_offset_of_ShowProgress_9() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___ShowProgress_9)); }
	inline bool get_ShowProgress_9() const { return ___ShowProgress_9; }
	inline bool* get_address_of_ShowProgress_9() { return &___ShowProgress_9; }
	inline void set_ShowProgress_9(bool value)
	{
		___ShowProgress_9 = value;
	}

	inline static int32_t get_offset_of_Async_10() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___Async_10)); }
	inline bool get_Async_10() const { return ___Async_10; }
	inline bool* get_address_of_Async_10() { return &___Async_10; }
	inline void set_Async_10(bool value)
	{
		___Async_10 = value;
	}

	inline static int32_t get_offset_of_ProgressCallback_11() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___ProgressCallback_11)); }
	inline ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * get_ProgressCallback_11() const { return ___ProgressCallback_11; }
	inline ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 ** get_address_of_ProgressCallback_11() { return &___ProgressCallback_11; }
	inline void set_ProgressCallback_11(ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * value)
	{
		___ProgressCallback_11 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressCallback_11), value);
	}

	inline static int32_t get_offset_of_a_12() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___a_12)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_a_12() const { return ___a_12; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_a_12() { return &___a_12; }
	inline void set_a_12(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___a_12 = value;
		Il2CppCodeGenWriteBarrier((&___a_12), value);
	}

	inline static int32_t get_offset_of_b_13() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___b_13)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_b_13() const { return ___b_13; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_b_13() { return &___b_13; }
	inline void set_b_13(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___b_13 = value;
		Il2CppCodeGenWriteBarrier((&___b_13), value);
	}

	inline static int32_t get_offset_of_c_14() { return static_cast<int32_t>(offsetof(AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B, ___c_14)); }
	inline String_t* get_c_14() const { return ___c_14; }
	inline String_t** get_address_of_c_14() { return &___c_14; }
	inline void set_c_14(String_t* value)
	{
		___c_14 = value;
		Il2CppCodeGenWriteBarrier((&___c_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETDOWNLOADER_TE0A11D81277C91FD04D01FF023FFBE84A6CFA30B_H
#ifndef HISTORY_T881F67A6F4DD36D446DB2BCD12289F4E1E335C58_H
#define HISTORY_T881F67A6F4DD36D446DB2BCD12289F4E1E335C58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.History
struct  History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58  : public Singleton_1_t63CBC98B5F5500F123C1A0DA3DE5E46DB61D237E
{
public:
	// System.Collections.Generic.LinkedList`1<DLLCore.Command> DLLCore.History::undos
	LinkedList_1_t286C5D1B6ADFB4D98F1EE9A186935777F97BC48C * ___undos_8;
	// System.Collections.Generic.LinkedList`1<DLLCore.Command> DLLCore.History::redos
	LinkedList_1_t286C5D1B6ADFB4D98F1EE9A186935777F97BC48C * ___redos_9;

public:
	inline static int32_t get_offset_of_undos_8() { return static_cast<int32_t>(offsetof(History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58, ___undos_8)); }
	inline LinkedList_1_t286C5D1B6ADFB4D98F1EE9A186935777F97BC48C * get_undos_8() const { return ___undos_8; }
	inline LinkedList_1_t286C5D1B6ADFB4D98F1EE9A186935777F97BC48C ** get_address_of_undos_8() { return &___undos_8; }
	inline void set_undos_8(LinkedList_1_t286C5D1B6ADFB4D98F1EE9A186935777F97BC48C * value)
	{
		___undos_8 = value;
		Il2CppCodeGenWriteBarrier((&___undos_8), value);
	}

	inline static int32_t get_offset_of_redos_9() { return static_cast<int32_t>(offsetof(History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58, ___redos_9)); }
	inline LinkedList_1_t286C5D1B6ADFB4D98F1EE9A186935777F97BC48C * get_redos_9() const { return ___redos_9; }
	inline LinkedList_1_t286C5D1B6ADFB4D98F1EE9A186935777F97BC48C ** get_address_of_redos_9() { return &___redos_9; }
	inline void set_redos_9(LinkedList_1_t286C5D1B6ADFB4D98F1EE9A186935777F97BC48C * value)
	{
		___redos_9 = value;
		Il2CppCodeGenWriteBarrier((&___redos_9), value);
	}
};

struct History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58_StaticFields
{
public:
	// System.Int32 DLLCore.History::a
	int32_t ___a_7;

public:
	inline static int32_t get_offset_of_a_7() { return static_cast<int32_t>(offsetof(History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58_StaticFields, ___a_7)); }
	inline int32_t get_a_7() const { return ___a_7; }
	inline int32_t* get_address_of_a_7() { return &___a_7; }
	inline void set_a_7(int32_t value)
	{
		___a_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HISTORY_T881F67A6F4DD36D446DB2BCD12289F4E1E335C58_H
#ifndef MAIN_T9518C93280EA9A864C63D04EA6A44DAE6CF9DF83_H
#define MAIN_T9518C93280EA9A864C63D04EA6A44DAE6CF9DF83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.Main
struct  Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83  : public Singleton_1_t7A42E23FC7FB5A90A1EAD0F3F06B11B91542B13D
{
public:
	// DLLCore.MaterialLoader DLLCore.Main::materialLoader
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * ___materialLoader_7;
	// DLLCore.ModelLoader DLLCore.Main::modelLoader
	ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * ___modelLoader_8;
	// DLLCore.MaterialContainer DLLCore.Main::materialContainer
	MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * ___materialContainer_9;
	// DLLCore.ObserverController DLLCore.Main::observerController
	ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA * ___observerController_10;
	// DLLCore.PlayerController DLLCore.Main::playerController
	PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286 * ___playerController_11;
	// DLLCore.DaylightController DLLCore.Main::daylightController
	DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2 * ___daylightController_12;
	// DLLCore.ProductController DLLCore.Main::productController
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * ___productController_13;
	// DLLCore.UnitController DLLCore.Main::unitController
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB * ___unitController_14;
	// DLLCore.ToolController DLLCore.Main::toolController
	ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980 * ___toolController_15;
	// DLLCore.GeneralSetting DLLCore.Main::generalSetting
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * ___generalSetting_16;
	// DLLCore.GraphicSetting DLLCore.Main::graphicSetting
	GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 * ___graphicSetting_17;
	// DLLCore.ReflectionControls DLLCore.Main::reflectionControls
	ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1 * ___reflectionControls_18;

public:
	inline static int32_t get_offset_of_materialLoader_7() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___materialLoader_7)); }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * get_materialLoader_7() const { return ___materialLoader_7; }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE ** get_address_of_materialLoader_7() { return &___materialLoader_7; }
	inline void set_materialLoader_7(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * value)
	{
		___materialLoader_7 = value;
		Il2CppCodeGenWriteBarrier((&___materialLoader_7), value);
	}

	inline static int32_t get_offset_of_modelLoader_8() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___modelLoader_8)); }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * get_modelLoader_8() const { return ___modelLoader_8; }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D ** get_address_of_modelLoader_8() { return &___modelLoader_8; }
	inline void set_modelLoader_8(ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * value)
	{
		___modelLoader_8 = value;
		Il2CppCodeGenWriteBarrier((&___modelLoader_8), value);
	}

	inline static int32_t get_offset_of_materialContainer_9() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___materialContainer_9)); }
	inline MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * get_materialContainer_9() const { return ___materialContainer_9; }
	inline MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 ** get_address_of_materialContainer_9() { return &___materialContainer_9; }
	inline void set_materialContainer_9(MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462 * value)
	{
		___materialContainer_9 = value;
		Il2CppCodeGenWriteBarrier((&___materialContainer_9), value);
	}

	inline static int32_t get_offset_of_observerController_10() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___observerController_10)); }
	inline ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA * get_observerController_10() const { return ___observerController_10; }
	inline ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA ** get_address_of_observerController_10() { return &___observerController_10; }
	inline void set_observerController_10(ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA * value)
	{
		___observerController_10 = value;
		Il2CppCodeGenWriteBarrier((&___observerController_10), value);
	}

	inline static int32_t get_offset_of_playerController_11() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___playerController_11)); }
	inline PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286 * get_playerController_11() const { return ___playerController_11; }
	inline PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286 ** get_address_of_playerController_11() { return &___playerController_11; }
	inline void set_playerController_11(PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286 * value)
	{
		___playerController_11 = value;
		Il2CppCodeGenWriteBarrier((&___playerController_11), value);
	}

	inline static int32_t get_offset_of_daylightController_12() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___daylightController_12)); }
	inline DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2 * get_daylightController_12() const { return ___daylightController_12; }
	inline DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2 ** get_address_of_daylightController_12() { return &___daylightController_12; }
	inline void set_daylightController_12(DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2 * value)
	{
		___daylightController_12 = value;
		Il2CppCodeGenWriteBarrier((&___daylightController_12), value);
	}

	inline static int32_t get_offset_of_productController_13() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___productController_13)); }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * get_productController_13() const { return ___productController_13; }
	inline ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 ** get_address_of_productController_13() { return &___productController_13; }
	inline void set_productController_13(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7 * value)
	{
		___productController_13 = value;
		Il2CppCodeGenWriteBarrier((&___productController_13), value);
	}

	inline static int32_t get_offset_of_unitController_14() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___unitController_14)); }
	inline UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB * get_unitController_14() const { return ___unitController_14; }
	inline UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB ** get_address_of_unitController_14() { return &___unitController_14; }
	inline void set_unitController_14(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB * value)
	{
		___unitController_14 = value;
		Il2CppCodeGenWriteBarrier((&___unitController_14), value);
	}

	inline static int32_t get_offset_of_toolController_15() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___toolController_15)); }
	inline ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980 * get_toolController_15() const { return ___toolController_15; }
	inline ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980 ** get_address_of_toolController_15() { return &___toolController_15; }
	inline void set_toolController_15(ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980 * value)
	{
		___toolController_15 = value;
		Il2CppCodeGenWriteBarrier((&___toolController_15), value);
	}

	inline static int32_t get_offset_of_generalSetting_16() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___generalSetting_16)); }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * get_generalSetting_16() const { return ___generalSetting_16; }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D ** get_address_of_generalSetting_16() { return &___generalSetting_16; }
	inline void set_generalSetting_16(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * value)
	{
		___generalSetting_16 = value;
		Il2CppCodeGenWriteBarrier((&___generalSetting_16), value);
	}

	inline static int32_t get_offset_of_graphicSetting_17() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___graphicSetting_17)); }
	inline GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 * get_graphicSetting_17() const { return ___graphicSetting_17; }
	inline GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 ** get_address_of_graphicSetting_17() { return &___graphicSetting_17; }
	inline void set_graphicSetting_17(GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 * value)
	{
		___graphicSetting_17 = value;
		Il2CppCodeGenWriteBarrier((&___graphicSetting_17), value);
	}

	inline static int32_t get_offset_of_reflectionControls_18() { return static_cast<int32_t>(offsetof(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83, ___reflectionControls_18)); }
	inline ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1 * get_reflectionControls_18() const { return ___reflectionControls_18; }
	inline ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1 ** get_address_of_reflectionControls_18() { return &___reflectionControls_18; }
	inline void set_reflectionControls_18(ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1 * value)
	{
		___reflectionControls_18 = value;
		Il2CppCodeGenWriteBarrier((&___reflectionControls_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAIN_T9518C93280EA9A864C63D04EA6A44DAE6CF9DF83_H
#ifndef OBSERVERCONTROLLER_T12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA_H
#define OBSERVERCONTROLLER_T12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ObserverController
struct  ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA  : public Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90
{
public:
	// UnityEngine.Camera DLLCore.ObserverController::a
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___a_4;
	// DLLCore.OrbitControls DLLCore.ObserverController::b
	OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403 * ___b_5;
	// System.Boolean DLLCore.ObserverController::isMoving
	bool ___isMoving_6;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA, ___a_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_a_4() const { return ___a_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA, ___b_5)); }
	inline OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403 * get_b_5() const { return ___b_5; }
	inline OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403 ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(OrbitControls_tB10120B3A0D9C3857C7A0D3E8772DF91C88E0403 * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_isMoving_6() { return static_cast<int32_t>(offsetof(ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA, ___isMoving_6)); }
	inline bool get_isMoving_6() const { return ___isMoving_6; }
	inline bool* get_address_of_isMoving_6() { return &___isMoving_6; }
	inline void set_isMoving_6(bool value)
	{
		___isMoving_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVERCONTROLLER_T12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA_H
#ifndef PLAYERCONTROLLER_T70DC56E27C8FF9383B046E5234D5B6551CE63286_H
#define PLAYERCONTROLLER_T70DC56E27C8FF9383B046E5234D5B6551CE63286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.PlayerController
struct  PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286  : public Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90
{
public:
	// DLLCore.FPSControls DLLCore.PlayerController::a
	FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE * ___a_4;
	// UnityEngine.Camera DLLCore.PlayerController::b
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___b_5;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286, ___a_4)); }
	inline FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE * get_a_4() const { return ___a_4; }
	inline FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(FPSControls_tB869CAEE43D61103A67C795EDCDEDF515D5E6FEE * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286, ___b_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_b_5() const { return ___b_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T70DC56E27C8FF9383B046E5234D5B6551CE63286_H
#ifndef PRODUCTCONTROLLER_TF9AC64156F5441BFD58386AC71C527C29D65E1B7_H
#define PRODUCTCONTROLLER_TF9AC64156F5441BFD58386AC71C527C29D65E1B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ProductController
struct  ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7  : public Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90
{
public:
	// DLLCore.ProductComponents.Store DLLCore.ProductController::store
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * ___store_4;
	// DLLCore.ProductComponents.Loader DLLCore.ProductController::loader
	Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7 * ___loader_5;
	// DLLCore.ProductComponents.Detector DLLCore.ProductController::detector
	Detector_tE440186490449828DD72FAED201287D60C7E9498 * ___detector_6;
	// DLLCore.ProductComponents.Transformer DLLCore.ProductController::transformer
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8 * ___transformer_7;
	// DLLCore.ProductComponents.Remover DLLCore.ProductController::remover
	Remover_t7D8C8F8B0950D56CCC1ACD94500C096AE133D701 * ___remover_8;
	// DLLCore.ProductComponents.Styler DLLCore.ProductController::styler
	Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10 * ___styler_9;
	// DLLCore.GeneralSetting DLLCore.ProductController::a
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * ___a_10;
	// System.Boolean DLLCore.ProductController::b
	bool ___b_11;

public:
	inline static int32_t get_offset_of_store_4() { return static_cast<int32_t>(offsetof(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7, ___store_4)); }
	inline Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * get_store_4() const { return ___store_4; }
	inline Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 ** get_address_of_store_4() { return &___store_4; }
	inline void set_store_4(Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932 * value)
	{
		___store_4 = value;
		Il2CppCodeGenWriteBarrier((&___store_4), value);
	}

	inline static int32_t get_offset_of_loader_5() { return static_cast<int32_t>(offsetof(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7, ___loader_5)); }
	inline Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7 * get_loader_5() const { return ___loader_5; }
	inline Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7 ** get_address_of_loader_5() { return &___loader_5; }
	inline void set_loader_5(Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7 * value)
	{
		___loader_5 = value;
		Il2CppCodeGenWriteBarrier((&___loader_5), value);
	}

	inline static int32_t get_offset_of_detector_6() { return static_cast<int32_t>(offsetof(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7, ___detector_6)); }
	inline Detector_tE440186490449828DD72FAED201287D60C7E9498 * get_detector_6() const { return ___detector_6; }
	inline Detector_tE440186490449828DD72FAED201287D60C7E9498 ** get_address_of_detector_6() { return &___detector_6; }
	inline void set_detector_6(Detector_tE440186490449828DD72FAED201287D60C7E9498 * value)
	{
		___detector_6 = value;
		Il2CppCodeGenWriteBarrier((&___detector_6), value);
	}

	inline static int32_t get_offset_of_transformer_7() { return static_cast<int32_t>(offsetof(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7, ___transformer_7)); }
	inline Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8 * get_transformer_7() const { return ___transformer_7; }
	inline Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8 ** get_address_of_transformer_7() { return &___transformer_7; }
	inline void set_transformer_7(Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8 * value)
	{
		___transformer_7 = value;
		Il2CppCodeGenWriteBarrier((&___transformer_7), value);
	}

	inline static int32_t get_offset_of_remover_8() { return static_cast<int32_t>(offsetof(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7, ___remover_8)); }
	inline Remover_t7D8C8F8B0950D56CCC1ACD94500C096AE133D701 * get_remover_8() const { return ___remover_8; }
	inline Remover_t7D8C8F8B0950D56CCC1ACD94500C096AE133D701 ** get_address_of_remover_8() { return &___remover_8; }
	inline void set_remover_8(Remover_t7D8C8F8B0950D56CCC1ACD94500C096AE133D701 * value)
	{
		___remover_8 = value;
		Il2CppCodeGenWriteBarrier((&___remover_8), value);
	}

	inline static int32_t get_offset_of_styler_9() { return static_cast<int32_t>(offsetof(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7, ___styler_9)); }
	inline Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10 * get_styler_9() const { return ___styler_9; }
	inline Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10 ** get_address_of_styler_9() { return &___styler_9; }
	inline void set_styler_9(Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10 * value)
	{
		___styler_9 = value;
		Il2CppCodeGenWriteBarrier((&___styler_9), value);
	}

	inline static int32_t get_offset_of_a_10() { return static_cast<int32_t>(offsetof(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7, ___a_10)); }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * get_a_10() const { return ___a_10; }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D ** get_address_of_a_10() { return &___a_10; }
	inline void set_a_10(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * value)
	{
		___a_10 = value;
		Il2CppCodeGenWriteBarrier((&___a_10), value);
	}

	inline static int32_t get_offset_of_b_11() { return static_cast<int32_t>(offsetof(ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7, ___b_11)); }
	inline bool get_b_11() const { return ___b_11; }
	inline bool* get_address_of_b_11() { return &___b_11; }
	inline void set_b_11(bool value)
	{
		___b_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCONTROLLER_TF9AC64156F5441BFD58386AC71C527C29D65E1B7_H
#ifndef TOOLCONTROLLER_T45FA2AC3D2AFFED1D80926743EB9F16A36A71980_H
#define TOOLCONTROLLER_T45FA2AC3D2AFFED1D80926743EB9F16A36A71980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.ToolController
struct  ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980  : public Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90
{
public:
	// DLLCore.MeasureDistance DLLCore.ToolController::a
	MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA * ___a_4;
	// DLLCore.MeasureArea DLLCore.ToolController::b
	MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7 * ___b_5;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980, ___a_4)); }
	inline MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA * get_a_4() const { return ___a_4; }
	inline MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(MeasureDistance_t5DAB4CBDD52149E1FB22D9B2BBE073AA487E65DA * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980, ___b_5)); }
	inline MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7 * get_b_5() const { return ___b_5; }
	inline MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7 ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(MeasureArea_tC4C101210A9BFEB694A6B83F8B9ED12DA69CD3F7 * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLCONTROLLER_T45FA2AC3D2AFFED1D80926743EB9F16A36A71980_H
#ifndef UNITCONTROLLER_TAD913A4917ACEDE85F80B574ABEDBD623007D7CB_H
#define UNITCONTROLLER_TAD913A4917ACEDE85F80B574ABEDBD623007D7CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.UnitController
struct  UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB  : public Controller_t3FE6F0551E59C0E1D1745D16A978680C8137DB90
{
public:
	// DLLCore.UnitComponents.Store DLLCore.UnitController::store
	Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35 * ___store_4;
	// DLLCore.UnitComponents.Loader DLLCore.UnitController::loader
	Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF * ___loader_5;
	// DLLCore.UnitComponents.Remover DLLCore.UnitController::remover
	Remover_t014C01D9DA3634857EDA3B77F140905C24E69561 * ___remover_6;
	// DLLCore.UnitComponents.Transformer DLLCore.UnitController::transformer
	Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4 * ___transformer_7;
	// DLLCore.UnitComponents.Styler DLLCore.UnitController::styler
	Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0 * ___styler_8;
	// DLLCore.UnitComponents.WallOpacityChanger DLLCore.UnitController::wallOpacityChanger
	WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC * ___wallOpacityChanger_9;
	// DLLCore.UnitComponents.WallColliderChanger DLLCore.UnitController::wallColliderChanger
	WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D * ___wallColliderChanger_10;
	// DLLCore.MaterialLoader DLLCore.UnitController::a
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * ___a_11;
	// System.Int32 DLLCore.UnitController::b
	int32_t ___b_12;

public:
	inline static int32_t get_offset_of_store_4() { return static_cast<int32_t>(offsetof(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB, ___store_4)); }
	inline Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35 * get_store_4() const { return ___store_4; }
	inline Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35 ** get_address_of_store_4() { return &___store_4; }
	inline void set_store_4(Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35 * value)
	{
		___store_4 = value;
		Il2CppCodeGenWriteBarrier((&___store_4), value);
	}

	inline static int32_t get_offset_of_loader_5() { return static_cast<int32_t>(offsetof(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB, ___loader_5)); }
	inline Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF * get_loader_5() const { return ___loader_5; }
	inline Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF ** get_address_of_loader_5() { return &___loader_5; }
	inline void set_loader_5(Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF * value)
	{
		___loader_5 = value;
		Il2CppCodeGenWriteBarrier((&___loader_5), value);
	}

	inline static int32_t get_offset_of_remover_6() { return static_cast<int32_t>(offsetof(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB, ___remover_6)); }
	inline Remover_t014C01D9DA3634857EDA3B77F140905C24E69561 * get_remover_6() const { return ___remover_6; }
	inline Remover_t014C01D9DA3634857EDA3B77F140905C24E69561 ** get_address_of_remover_6() { return &___remover_6; }
	inline void set_remover_6(Remover_t014C01D9DA3634857EDA3B77F140905C24E69561 * value)
	{
		___remover_6 = value;
		Il2CppCodeGenWriteBarrier((&___remover_6), value);
	}

	inline static int32_t get_offset_of_transformer_7() { return static_cast<int32_t>(offsetof(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB, ___transformer_7)); }
	inline Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4 * get_transformer_7() const { return ___transformer_7; }
	inline Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4 ** get_address_of_transformer_7() { return &___transformer_7; }
	inline void set_transformer_7(Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4 * value)
	{
		___transformer_7 = value;
		Il2CppCodeGenWriteBarrier((&___transformer_7), value);
	}

	inline static int32_t get_offset_of_styler_8() { return static_cast<int32_t>(offsetof(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB, ___styler_8)); }
	inline Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0 * get_styler_8() const { return ___styler_8; }
	inline Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0 ** get_address_of_styler_8() { return &___styler_8; }
	inline void set_styler_8(Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0 * value)
	{
		___styler_8 = value;
		Il2CppCodeGenWriteBarrier((&___styler_8), value);
	}

	inline static int32_t get_offset_of_wallOpacityChanger_9() { return static_cast<int32_t>(offsetof(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB, ___wallOpacityChanger_9)); }
	inline WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC * get_wallOpacityChanger_9() const { return ___wallOpacityChanger_9; }
	inline WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC ** get_address_of_wallOpacityChanger_9() { return &___wallOpacityChanger_9; }
	inline void set_wallOpacityChanger_9(WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC * value)
	{
		___wallOpacityChanger_9 = value;
		Il2CppCodeGenWriteBarrier((&___wallOpacityChanger_9), value);
	}

	inline static int32_t get_offset_of_wallColliderChanger_10() { return static_cast<int32_t>(offsetof(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB, ___wallColliderChanger_10)); }
	inline WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D * get_wallColliderChanger_10() const { return ___wallColliderChanger_10; }
	inline WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D ** get_address_of_wallColliderChanger_10() { return &___wallColliderChanger_10; }
	inline void set_wallColliderChanger_10(WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D * value)
	{
		___wallColliderChanger_10 = value;
		Il2CppCodeGenWriteBarrier((&___wallColliderChanger_10), value);
	}

	inline static int32_t get_offset_of_a_11() { return static_cast<int32_t>(offsetof(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB, ___a_11)); }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * get_a_11() const { return ___a_11; }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE ** get_address_of_a_11() { return &___a_11; }
	inline void set_a_11(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * value)
	{
		___a_11 = value;
		Il2CppCodeGenWriteBarrier((&___a_11), value);
	}

	inline static int32_t get_offset_of_b_12() { return static_cast<int32_t>(offsetof(UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB, ___b_12)); }
	inline int32_t get_b_12() const { return ___b_12; }
	inline int32_t* get_address_of_b_12() { return &___b_12; }
	inline void set_b_12(int32_t value)
	{
		___b_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITCONTROLLER_TAD913A4917ACEDE85F80B574ABEDBD623007D7CB_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (a_t433051DDF90FB1423BC75EFB809D4367CA4EA9B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3000[3] = 
{
	a_t433051DDF90FB1423BC75EFB809D4367CA4EA9B7::get_offset_of_a_0(),
	a_t433051DDF90FB1423BC75EFB809D4367CA4EA9B7::get_offset_of_b_1(),
	a_t433051DDF90FB1423BC75EFB809D4367CA4EA9B7::get_offset_of_c_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (b_t57312502347FBF707316CAD0D119A027A625EC84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[8] = 
{
	b_t57312502347FBF707316CAD0D119A027A625EC84::get_offset_of_a_0(),
	b_t57312502347FBF707316CAD0D119A027A625EC84::get_offset_of_b_1(),
	b_t57312502347FBF707316CAD0D119A027A625EC84::get_offset_of_c_2(),
	b_t57312502347FBF707316CAD0D119A027A625EC84::get_offset_of_d_3(),
	b_t57312502347FBF707316CAD0D119A027A625EC84::get_offset_of_e_4(),
	b_t57312502347FBF707316CAD0D119A027A625EC84::get_offset_of_f_5(),
	b_t57312502347FBF707316CAD0D119A027A625EC84::get_offset_of_g_6(),
	b_t57312502347FBF707316CAD0D119A027A625EC84::get_offset_of_h_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (GlobalFunc_t058C2D04752654C6A3FC7C3FA3C1E712BAB7DF1A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E), -1, sizeof(GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3003[16] = 
{
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_DefaultMaterial_0(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_APIDomain_1(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_CDN_PATH_2(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_ACE_RESOURCE_PATH_3(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_mouseLeft_4(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_mouseRight_5(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_DefaultMaterialId_6(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_CallbackStatus_7(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_floorLayerMask_8(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_wallLayerMask_9(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_ceilLayerMask_10(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_productsLayerMask_11(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_outlineLayerMask_12(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_productLayerMask_13(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_productItemLayerMask_14(),
	GlobalVar_tDC7D119577462115919042A6D78F28D6D8D0267E_StaticFields::get_offset_of_splitedWallLayerMask_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (ModelType_t7F5AF03AF3B972D6B0D3B90C42D79957B4B06B08)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3004[3] = 
{
	ModelType_t7F5AF03AF3B972D6B0D3B90C42D79957B4B06B08::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (UnitType_tFB8DF4883EB91B9AA9862E065B3CA4AEBA32C4C6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3005[19] = 
{
	UnitType_tFB8DF4883EB91B9AA9862E065B3CA4AEBA32C4C6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[7] = 
{
	GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8::get_offset_of_a_4(),
	GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8::get_offset_of_b_5(),
	GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8::get_offset_of_c_6(),
	GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8::get_offset_of_d_7(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58), -1, sizeof(History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3007[3] = 
{
	History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58_StaticFields::get_offset_of_a_7(),
	History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58::get_offset_of_undos_8(),
	History_t881F67A6F4DD36D446DB2BCD12289F4E1E335C58::get_offset_of_redos_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3008[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[12] = 
{
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_materialLoader_7(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_modelLoader_8(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_materialContainer_9(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_observerController_10(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_playerController_11(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_daylightController_12(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_productController_13(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_unitController_14(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_toolController_15(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_generalSetting_16(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_graphicSetting_17(),
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83::get_offset_of_reflectionControls_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (MaterialContainer_tE6A113C04D2C881D1750B6665F281A9006A90462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF), -1, sizeof(U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3011[2] = 
{
	U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t278118A1F44635CBC4D91D43E8A25548353EE0FF_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3012[2] = 
{
	a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630::get_offset_of_a_0(),
	a_t9FEFF7B3101E7C159E8CE2D3CE3143CC661A4630::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[6] = 
{
	b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B::get_offset_of_a_0(),
	b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B::get_offset_of_b_1(),
	b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B::get_offset_of_c_2(),
	b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B::get_offset_of_d_3(),
	b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B::get_offset_of_e_4(),
	b_t7466D9030918E1CAEC9A5F32F05E8825C8C9A54B::get_offset_of_f_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (c_tA4679713E8BA96C80A0C87574F54F77383D92518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[1] = 
{
	c_tA4679713E8BA96C80A0C87574F54F77383D92518::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3015[4] = 
{
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE::get_offset_of_a_4(),
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE::get_offset_of_b_5(),
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE::get_offset_of_c_6(),
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE::get_offset_of_d_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (a_t3B414B8677031A1BD55E83017B9C9AEA64A60159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[1] = 
{
	a_t3B414B8677031A1BD55E83017B9C9AEA64A60159::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[11] = 
{
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_a_0(),
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_b_1(),
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_c_2(),
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_d_3(),
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_e_4(),
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_f_5(),
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_g_6(),
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_h_7(),
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_i_8(),
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_j_9(),
	b_tF9CE10E66A8BAB1054E09A8FB1CCE6A837688F8E::get_offset_of_k_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3018[2] = 
{
	ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D::get_offset_of_a_4(),
	ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3019[7] = 
{
	a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4::get_offset_of_a_0(),
	a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4::get_offset_of_b_1(),
	a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4::get_offset_of_c_2(),
	a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4::get_offset_of_d_3(),
	a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4::get_offset_of_e_4(),
	a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4::get_offset_of_f_5(),
	a_tCC7A80333A05F0558D846DC2AAC0F4831F6F55B4::get_offset_of_g_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (b_t8776D45E5AD405680C193123F7FF0BBC46D6866A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3020[2] = 
{
	b_t8776D45E5AD405680C193123F7FF0BBC46D6866A::get_offset_of_a_0(),
	b_t8776D45E5AD405680C193123F7FF0BBC46D6866A::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (c_t01E2EC79FBD05D095F54BB893B966C010764FDE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3021[6] = 
{
	c_t01E2EC79FBD05D095F54BB893B966C010764FDE9::get_offset_of_a_0(),
	c_t01E2EC79FBD05D095F54BB893B966C010764FDE9::get_offset_of_b_1(),
	c_t01E2EC79FBD05D095F54BB893B966C010764FDE9::get_offset_of_c_2(),
	c_t01E2EC79FBD05D095F54BB893B966C010764FDE9::get_offset_of_d_3(),
	c_t01E2EC79FBD05D095F54BB893B966C010764FDE9::get_offset_of_e_4(),
	c_t01E2EC79FBD05D095F54BB893B966C010764FDE9::get_offset_of_f_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3022[3] = 
{
	ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA::get_offset_of_a_4(),
	ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA::get_offset_of_b_5(),
	ObserverController_t12AB96B44CDF27D9C9A80EDA30F83F0D707F6CBA::get_offset_of_isMoving_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[2] = 
{
	PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286::get_offset_of_a_4(),
	PlayerController_t70DC56E27C8FF9383B046E5234D5B6551CE63286::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3024[5] = 
{
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C::get_offset_of_a_4(),
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C::get_offset_of_b_5(),
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C::get_offset_of_c_6(),
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C::get_offset_of_d_7(),
	Product_tD53AAA882EC6DD96DE9DB23D381C84047A40297C::get_offset_of_e_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3025[8] = 
{
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7::get_offset_of_store_4(),
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7::get_offset_of_loader_5(),
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7::get_offset_of_detector_6(),
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7::get_offset_of_transformer_7(),
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7::get_offset_of_remover_8(),
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7::get_offset_of_styler_9(),
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7::get_offset_of_a_10(),
	ProductController_tF9AC64156F5441BFD58386AC71C527C29D65E1B7::get_offset_of_b_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3027[8] = 
{
	a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7::get_offset_of_a_0(),
	a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7::get_offset_of_b_1(),
	a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7::get_offset_of_c_2(),
	a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7::get_offset_of_d_3(),
	a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7::get_offset_of_e_4(),
	a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7::get_offset_of_f_5(),
	a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7::get_offset_of_g_6(),
	a_tF0CCF35C0C335DAEBBD1FE7D26E4B7896A52B4D7::get_offset_of_h_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (b_t9B6839F17B94EE25DD53F26E895F9F6C004F6894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3028[2] = 
{
	b_t9B6839F17B94EE25DD53F26E895F9F6C004F6894::get_offset_of_a_0(),
	b_t9B6839F17B94EE25DD53F26E895F9F6C004F6894::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (c_tEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[4] = 
{
	c_tEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231::get_offset_of_a_0(),
	c_tEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231::get_offset_of_b_1(),
	c_tEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231::get_offset_of_c_2(),
	c_tEA7DAEBC4B13CEB2D9E105464B206C0F0CC59231::get_offset_of_d_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (d_t6431F5736DEC21FD831EF0C0B5D36DB1C874006E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3030[4] = 
{
	d_t6431F5736DEC21FD831EF0C0B5D36DB1C874006E::get_offset_of_a_0(),
	d_t6431F5736DEC21FD831EF0C0B5D36DB1C874006E::get_offset_of_b_1(),
	d_t6431F5736DEC21FD831EF0C0B5D36DB1C874006E::get_offset_of_c_2(),
	d_t6431F5736DEC21FD831EF0C0B5D36DB1C874006E::get_offset_of_d_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (e_t7A60F2C1D9BD8A6F89CF3F6D21AB79B6AEAF9C94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3031[1] = 
{
	e_t7A60F2C1D9BD8A6F89CF3F6D21AB79B6AEAF9C94::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (f_t88AACE613CA2A687E8A5A1AEE9016897C30E82A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3032[1] = 
{
	f_t88AACE613CA2A687E8A5A1AEE9016897C30E82A3::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (g_tDFCE396CBF262E3AE5824793EC389E4EE1C9E84C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3033[2] = 
{
	g_tDFCE396CBF262E3AE5824793EC389E4EE1C9E84C::get_offset_of_a_0(),
	g_tDFCE396CBF262E3AE5824793EC389E4EE1C9E84C::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (h_tB9C53A529D7F78DE545E5ECC45E29705E7954423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3034[3] = 
{
	h_tB9C53A529D7F78DE545E5ECC45E29705E7954423::get_offset_of_a_0(),
	h_tB9C53A529D7F78DE545E5ECC45E29705E7954423::get_offset_of_b_1(),
	h_tB9C53A529D7F78DE545E5ECC45E29705E7954423::get_offset_of_c_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (i_tBB96D77E10B6671D9ECF8EF3AF39C8AC14740DCC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[1] = 
{
	i_tBB96D77E10B6671D9ECF8EF3AF39C8AC14740DCC::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (j_t6F06E33F0796AB276A0784918BBAE6A38C89B71F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[1] = 
{
	j_t6F06E33F0796AB276A0784918BBAE6A38C89B71F::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3037[2] = 
{
	k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59::get_offset_of_a_0(),
	k_t7B3D8F985495BEE9F625B1325CBE75D9BE72DB59::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (l_tB3479A81FAB621B17F46AF6AAB6384D9FBD013AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[2] = 
{
	l_tB3479A81FAB621B17F46AF6AAB6384D9FBD013AD::get_offset_of_a_0(),
	l_tB3479A81FAB621B17F46AF6AAB6384D9FBD013AD::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (m_tD4B72672F9F98BB0255C4EDAAAFCD8942B449C4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3039[1] = 
{
	m_tD4B72672F9F98BB0255C4EDAAAFCD8942B449C4A::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (n_tE03118E8C7A13F10091ECC827F165C0BC32167AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3040[1] = 
{
	n_tE03118E8C7A13F10091ECC827F165C0BC32167AE::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[2] = 
{
	ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1::get_offset_of_a_4(),
	ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122), -1, sizeof(U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3042[3] = 
{
	U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
	U3CU3Ec_tD19F9CC5923893F96999DF9CD879D10D43A20122_StaticFields::get_offset_of_U3CU3E9__6_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[2] = 
{
	ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980::get_offset_of_a_4(),
	ToolController_t45FA2AC3D2AFFED1D80926743EB9F16A36A71980::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (ScreenShotPath_t5E0DCEF7D4B345890D428A110D3532816CD973A8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3045[9] = 
{
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB::get_offset_of_store_4(),
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB::get_offset_of_loader_5(),
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB::get_offset_of_remover_6(),
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB::get_offset_of_transformer_7(),
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB::get_offset_of_styler_8(),
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB::get_offset_of_wallOpacityChanger_9(),
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB::get_offset_of_wallColliderChanger_10(),
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB::get_offset_of_a_11(),
	UnitController_tAD913A4917ACEDE85F80B574ABEDBD623007D7CB::get_offset_of_b_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (a_t3857EB8DC73AF2680C0FC9027FCFB9028E1D67B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3047[2] = 
{
	a_t3857EB8DC73AF2680C0FC9027FCFB9028E1D67B9::get_offset_of_a_0(),
	a_t3857EB8DC73AF2680C0FC9027FCFB9028E1D67B9::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5), -1, sizeof(GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3048[11] = 
{
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5_StaticFields::get_offset_of_ALREADY_CLICKED_4(),
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5::get_offset_of_gizmoCamera_5(),
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5::get_offset_of_gizmoLayer_6(),
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5::get_offset_of_targets_7(),
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5::get_offset_of_highlight_8(),
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5::get_offset_of_previousMaterials_9(),
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5::get_offset_of_pressing_10(),
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5::get_offset_of_pressingPlane_11(),
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5::get_offset_of_fromCenterToHitPoint_12(),
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5::get_offset_of_hitPoint_13(),
	GizmoClickDetection_tDAAFFE3DF2CC333B58B4461B9117E21775762DC5::get_offset_of_hitPointRotation_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3049[31] = 
{
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_a_4(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_xHandle_5(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_yHandle_6(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_zHandle_7(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_xCube_8(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_xCylinder_9(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_yCube_10(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_yCylinder_11(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_zCube_12(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_zCylinder_13(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_centerHandle_14(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_scaleTarget_15(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_b_16(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_c_17(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_d_18(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_e_19(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_f_20(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_g_21(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_h_22(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_i_23(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_j_24(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_k_25(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_l_26(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_m_27(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_n_28(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_o_29(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_p_30(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_q_31(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_r_32(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_s_33(),
	GizmoScaleScript_tC9961504A0C6F393CB85758C3DDB56FB52CC8F37::get_offset_of_t_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3050[8] = 
{
	GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4::get_offset_of_xAxisObject_4(),
	GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4::get_offset_of_yAxisObject_5(),
	GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4::get_offset_of_zAxisObject_6(),
	GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4::get_offset_of_translateTarget_7(),
	GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4::get_offset_of_a_8(),
	GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4::get_offset_of_b_9(),
	GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4::get_offset_of_c_10(),
	GizmoTranslateScript_tF6B2D395DB326ADB1AAF7EC84C2DB09B70BF19B4::get_offset_of_d_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (RaycastPlane_tEAE141749806C54A382D3F1A075608EDA4B38B76)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3051[4] = 
{
	RaycastPlane_tEAE141749806C54A382D3F1A075608EDA4B38B76::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (GuiHelper_tA84A9D6AFB902F91D77F1942393821A419FD931C), -1, sizeof(GuiHelper_tA84A9D6AFB902F91D77F1942393821A419FD931C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3052[2] = 
{
	GuiHelper_tA84A9D6AFB902F91D77F1942393821A419FD931C_StaticFields::get_offset_of_a_0(),
	GuiHelper_tA84A9D6AFB902F91D77F1942393821A419FD931C_StaticFields::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (InputManager_tE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1), -1, sizeof(InputManager_tE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3053[2] = 
{
	InputManager_tE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1_StaticFields::get_offset_of_a_0(),
	InputManager_tE283E3DBE7DF753FFFDBC7E922D027CFE3FE92A1_StaticFields::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (TouchDownCallback_t0B197FB7BD9E767285C88BAF51E0C6D434C81EAC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3055[16] = 
{
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_a_4(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_controlMode_5(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_simpleRotatePanel_6(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_rotateDirectionGo_7(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_simpleMoveGo_8(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_simpleRotateButton_9(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_simpleMoveButton_10(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_duplicateButton_11(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_removeButton_12(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_b_13(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_c_14(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_d_15(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_needsUpdate_16(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_e_17(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_f_18(),
	ProductContextMenu_t03EE77264706C40D7CBBFBD17B4076731B57EA54::get_offset_of_g_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (ControlMode_t2D3B54565034AFA58722598B9C6C9A4580507668)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3056[3] = 
{
	ControlMode_t2D3B54565034AFA58722598B9C6C9A4580507668::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (Action_tE0BB1BA0E6F890762C80861F918D097AA89569D0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3057[4] = 
{
	Action_tE0BB1BA0E6F890762C80861F918D097AA89569D0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F), -1, sizeof(U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3058[10] = 
{
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields::get_offset_of_U3CU3E9__31_0_1(),
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields::get_offset_of_U3CU3E9__31_1_2(),
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields::get_offset_of_U3CU3E9__31_2_3(),
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields::get_offset_of_U3CU3E9__37_0_4(),
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields::get_offset_of_U3CU3E9__37_1_5(),
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields::get_offset_of_U3CU3E9__37_2_6(),
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields::get_offset_of_U3CU3E9__39_0_7(),
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields::get_offset_of_U3CU3E9__39_1_8(),
	U3CU3Ec_tEA6A685319A4B89331772EB538884340E01A101F_StaticFields::get_offset_of_U3CU3E9__39_2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (BrowserInteracting_tD17A4F74B4CABFFDD0227C0BA1B8DD95CC22BA74), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3060[3] = 
{
	Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF::get_offset_of_a_4(),
	Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF::get_offset_of_b_5(),
	Loader_t2FBD38FAB6247E5F50F2F0C978BE1A0732AE86FF::get_offset_of_c_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (a_tFCC3CE3B73C6FDA385751DF01618D09CFD3B75E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3061[2] = 
{
	a_tFCC3CE3B73C6FDA385751DF01618D09CFD3B75E8::get_offset_of_a_0(),
	a_tFCC3CE3B73C6FDA385751DF01618D09CFD3B75E8::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (Remover_t014C01D9DA3634857EDA3B77F140905C24E69561), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3063[4] = 
{
	Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35::get_offset_of_unit_4(),
	Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35::get_offset_of_updateCeilingVisibility_5(),
	Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35::get_offset_of_updateCeilingOpacity_6(),
	Store_t483BC3798BF69BBB929C9E3848F58BCC0B9CCC35::get_offset_of_a_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3064[8] = 
{
	Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0::get_offset_of_isRunning_4(),
	Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0::get_offset_of_a_5(),
	Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0::get_offset_of_b_6(),
	Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0::get_offset_of_c_7(),
	Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0::get_offset_of_d_8(),
	Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0::get_offset_of_e_9(),
	Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0::get_offset_of_f_10(),
	Styler_t4C603A1C7019365B9B5D1E90331C836C785136D0::get_offset_of_g_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (MaterialType_tDDE095F7F22E9C1859D762C2744ED61C2B83EECB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3065[7] = 
{
	MaterialType_tDDE095F7F22E9C1859D762C2744ED61C2B83EECB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (a_tD907EAFF666CDD709FA14A29050C6750D114625C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[2] = 
{
	a_tD907EAFF666CDD709FA14A29050C6750D114625C::get_offset_of_a_0(),
	a_tD907EAFF666CDD709FA14A29050C6750D114625C::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3067[3] = 
{
	Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4::get_offset_of_a_4(),
	Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4::get_offset_of_b_5(),
	Transformer_tCBBA2E12184B07139C763FB3473B6CCEE529A3A4::get_offset_of_c_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (TType_t2A813D5A1723CB1BA5EBA63B9F0D49B0F582200F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3068[5] = 
{
	TType_t2A813D5A1723CB1BA5EBA63B9F0D49B0F582200F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3069[2] = 
{
	WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D::get_offset_of_a_4(),
	WallColliderChanger_t834A1B1E16CBF40D89B17BFCFB6130B6C164723D::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[2] = 
{
	WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC::get_offset_of_a_4(),
	WallOpacityChanger_tACA7B02D312F3E3584729A634AE4E175510C5DCC::get_offset_of_b_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (SplitedWalls_t3D6E1370EC4D5F1E735EF893FFBE5357C2FA9A3D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (SplitedWall_t43081FE6D3CC34E084B169E8E46F5C0FA32FABDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[2] = 
{
	SplitedWall_t43081FE6D3CC34E084B169E8E46F5C0FA32FABDB::get_offset_of_vertexIndices_4(),
	SplitedWall_t43081FE6D3CC34E084B169E8E46F5C0FA32FABDB::get_offset_of_area_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[5] = 
{
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D::get_offset_of_a_4(),
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D::get_offset_of_bounds_5(),
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D::get_offset_of_wallOpacity_6(),
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D::get_offset_of_splitedWalls_7(),
	Unit_t83E45E1757CCC1C02672CD4806ADD805FCE2270D::get_offset_of_splitedWallsGo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (Detector_tE440186490449828DD72FAED201287D60C7E9498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[4] = 
{
	Detector_tE440186490449828DD72FAED201287D60C7E9498::get_offset_of_a_4(),
	Detector_tE440186490449828DD72FAED201287D60C7E9498::get_offset_of_b_5(),
	Detector_tE440186490449828DD72FAED201287D60C7E9498::get_offset_of_c_6(),
	Detector_tE440186490449828DD72FAED201287D60C7E9498::get_offset_of_d_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (a_tFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[4] = 
{
	a_tFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C::get_offset_of_a_0(),
	a_tFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C::get_offset_of_b_1(),
	a_tFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C::get_offset_of_c_2(),
	a_tFEC9D3B21B9D6BD6643C6D467D5C2F0E56854E1C::get_offset_of_d_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532), -1, sizeof(U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3076[3] = 
{
	U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
	U3CU3Ec_tCFF7C9CEC7BBC4B2890F0DD9895A2043BF652532_StaticFields::get_offset_of_U3CU3E9__13_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3077[5] = 
{
	Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7::get_offset_of_a_4(),
	Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7::get_offset_of_b_5(),
	Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7::get_offset_of_c_6(),
	Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7::get_offset_of_d_7(),
	Loader_tB0FF4878450ED419A5701379AFC8CCAF5FD066A7::get_offset_of_e_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3078[3] = 
{
	ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4::get_offset_of_planeType_0(),
	ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4::get_offset_of_aType_1(),
	ProductData_tCBDE3F6BC0FDA2A7E1720D6352CFFBEF8D380CF4::get_offset_of_bedType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (ActionType_t8BD049FCFBFDAFAB18966CA8E7B0AF1417BA2797)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3079[5] = 
{
	ActionType_t8BD049FCFBFDAFAB18966CA8E7B0AF1417BA2797::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3080[6] = 
{
	a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7::get_offset_of_a_0(),
	a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7::get_offset_of_b_1(),
	a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7::get_offset_of_c_2(),
	a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7::get_offset_of_d_3(),
	a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7::get_offset_of_e_4(),
	a_t8DF8E5B44734C9AAACC4F0C4B8CE87D902A44AA7::get_offset_of_f_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (Remover_t7D8C8F8B0950D56CCC1ACD94500C096AE133D701), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3082[13] = 
{
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_loadedProducts_4(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_selectedProducts_5(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_centerOfSelectedProducts_6(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_distVecOfSelectedProducts_7(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_a_8(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_b_9(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_selectedBoxCollider_10(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_selectedProductsTransform_11(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_c_12(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_d_13(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_e_14(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_f_15(),
	Store_t9B09CB3FF242495A8082BFFAC901834FF8FCF932::get_offset_of_g_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5), -1, sizeof(U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3083[5] = 
{
	U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields::get_offset_of_U3CU3E9__16_0_1(),
	U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields::get_offset_of_U3CU3E9__17_1_2(),
	U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields::get_offset_of_U3CU3E9__17_0_3(),
	U3CU3Ec_t1DA47739A8DA80CF44CBD741AAFB7981198536A5_StaticFields::get_offset_of_U3CU3E9__21_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3084[8] = 
{
	Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10::get_offset_of_a_4(),
	Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10::get_offset_of_b_5(),
	Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10::get_offset_of_c_6(),
	Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10::get_offset_of_d_7(),
	Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10::get_offset_of_e_8(),
	Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10::get_offset_of_f_9(),
	Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10::get_offset_of_g_10(),
	Styler_t4DFF7DB60CCFA43DAAECD8AA19A150EFD4A17D10::get_offset_of_h_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (SType_t0761393A7DE32BC8F9A1DA121A4FDC1624794DDF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3085[3] = 
{
	SType_t0761393A7DE32BC8F9A1DA121A4FDC1624794DDF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (TType_t21E3FBFD0CAE6ADA425A45328916AADD4A7A745E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3086[4] = 
{
	TType_t21E3FBFD0CAE6ADA425A45328916AADD4A7A745E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (a_t78342471E804DE10F0B22D3ABA92ED6BF7951969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3087[2] = 
{
	a_t78342471E804DE10F0B22D3ABA92ED6BF7951969::get_offset_of_a_0(),
	a_t78342471E804DE10F0B22D3ABA92ED6BF7951969::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3088[9] = 
{
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8::get_offset_of_a_4(),
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8::get_offset_of_b_5(),
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8::get_offset_of_c_6(),
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8::get_offset_of_d_7(),
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8::get_offset_of_e_8(),
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8::get_offset_of_f_9(),
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8::get_offset_of_g_10(),
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8::get_offset_of_h_11(),
	Transformer_t14C1129412B89EBD1DAFDB665B3530E01EA8D5F8::get_offset_of_i_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (SType_t161AED89893FAA2418C216E0255B4417A8203955)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3089[3] = 
{
	SType_t161AED89893FAA2418C216E0255B4417A8203955::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (TType_t2D15E57A61E5C468944FC8F2CBED5DF23F7C150A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3090[7] = 
{
	TType_t2D15E57A61E5C468944FC8F2CBED5DF23F7C150A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3091[5] = 
{
	a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7::get_offset_of_a_0(),
	a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7::get_offset_of_b_1(),
	a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7::get_offset_of_c_2(),
	a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7::get_offset_of_d_3(),
	a_t243663C6B3AE837A9E7B0A9FE8712391B0F79DF7::get_offset_of_e_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (STBImageLoader_tECC0F7620E7E36D06D430505E069CE761ED507F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (STBImageInterop_t722FCD4F8AAFF38C9E42715D615241C102649E7E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (AssetAdvancedConfigType_t774488E8F923B69FD6AEBE1610EF9CAD1B15BCCB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3094[10] = 
{
	AssetAdvancedConfigType_t774488E8F923B69FD6AEBE1610EF9CAD1B15BCCB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (AssetAdvancedPropertyClassNames_t10FAFFA3553FF6537076DE42775100534BFB94CF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3095[66] = 
{
	AssetAdvancedPropertyClassNames_t10FAFFA3553FF6537076DE42775100534BFB94CF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (AssetAdvancedPropertyMetadata_t3ACD72729D1D3D2BC0925E455FDEE6A5FD610DC7), -1, sizeof(AssetAdvancedPropertyMetadata_t3ACD72729D1D3D2BC0925E455FDEE6A5FD610DC7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3096[1] = 
{
	AssetAdvancedPropertyMetadata_t3ACD72729D1D3D2BC0925E455FDEE6A5FD610DC7_StaticFields::get_offset_of_ConfigKeys_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3097[8] = 
{
	AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6::get_offset_of_Key_0(),
	AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6::get_offset_of_IntValue_1(),
	AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6::get_offset_of_FloatValue_2(),
	AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6::get_offset_of_BoolValue_3(),
	AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6::get_offset_of_StringValue_4(),
	AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6::get_offset_of_TranslationValue_5(),
	AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6::get_offset_of_RotationValue_6(),
	AssetAdvancedConfig_tFE8A1C0D77BF163613125759158C33B1814CE7F6::get_offset_of_ScaleValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3098[11] = 
{
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_AutoStart_4(),
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_AssetURI_5(),
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_Timeout_6(),
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_AssetExtension_7(),
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_WrapperGameObject_8(),
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_ShowProgress_9(),
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_Async_10(),
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_ProgressCallback_11(),
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_a_12(),
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_b_13(),
	AssetDownloader_tE0A11D81277C91FD04D01FF023FFBE84A6CFA30B::get_offset_of_c_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3099[9] = 
{
	a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C::get_offset_of_a_0(),
	a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C::get_offset_of_b_1(),
	a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C::get_offset_of_c_2(),
	a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C::get_offset_of_d_3(),
	a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C::get_offset_of_e_4(),
	a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C::get_offset_of_f_5(),
	a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C::get_offset_of_g_6(),
	a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C::get_offset_of_h_7(),
	a_t0B94BEC05FD5A7E53FEA62C63708A2AFFF7CB70C::get_offset_of_i_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
