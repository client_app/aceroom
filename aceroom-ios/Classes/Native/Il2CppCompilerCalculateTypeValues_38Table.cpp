﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ARUIController
struct ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1;
// AceAnimationHandler
struct AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0;
// BaseController
struct BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E;
// ColourVariableSetting
struct ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B;
// DLLCore.DaylightController
struct DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2;
// DLLCore.EventManager
struct EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966;
// DLLCore.GeneralSetting
struct GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D;
// DLLCore.GraphicSetting
struct GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8;
// DLLCore.Main
struct Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83;
// DLLCore.MaterialLoader
struct MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE;
// DLLCore.ModelLoader
struct ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D;
// DLLCore.ProductController/LoadCallback
struct LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B;
// DLLCore.ReflectionControls
struct ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1;
// DLLCore.ToolController/ScreenShotPath
struct ScreenShotPath_t5E0DCEF7D4B345890D428A110D3532816CD973A8;
// DLLCore.UnitController/LoadCallback
struct LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B;
// DefaultMainCamera
struct DefaultMainCamera_t5CCBA3F2B0D86DA34C24E30E5149A7FD6B112B6C;
// FloatVariableSetting
struct FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3;
// HSVPickerDemo.HSVPicker
struct HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565;
// ImportCoreDLL
struct ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692;
// MattressUIController
struct MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191;
// MobileBase
struct MobileBase_tA53736A507DA935FDC5873236F32953E0B2F886A;
// MobileManager
struct MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543;
// ModalController/Callback
struct Callback_t5433D5269B1108D8BFECDCD02079FCB1211A6CF8;
// ModalController/CallbackClose
struct CallbackClose_t4CBEE1DC735AF36C5814923F4360DBB1949CD115;
// ModalController/CallbackPop
struct CallbackPop_t11746E625CF12C9917AD4DB2B9A57E2597D32609;
// ModalController/CallbackType
struct CallbackType_tE5CC0E106E4AF1B685B5B9940F689A2464C63C41;
// ModelTargetController
struct ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A;
// ModelTargetMenuController
struct ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237;
// ProductListButton
struct ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618;
// ProductOptionUIManager
struct ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87;
// ProductScrollList
struct ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD;
// ProductViewController
struct ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0;
// RoomSceneUIController
struct RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937;
// SettingPanelController
struct SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,System.String[]>
struct Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072;
// System.Collections.Generic.List`1<ColourVariableSetting>[]
struct List_1U5BU5D_tFCFA1E1475DC0B4BD9CF6BC029DB77EB834CD4D4;
// System.Collections.Generic.List`1<FloatVariableSetting>[]
struct List_1U5BU5D_t175E3D9BA32BCDBC808EACD530EC3BEF73F75139;
// System.Collections.Generic.List`1<ProductListButton/ProductData>
struct List_1_tA01AB2108BB74F785875888713CA297D47449BB0;
// System.Collections.Generic.List`1<System.Action`1<UnityEngine.Color>>
struct List_1_t6E9DEDB83D3F4749BFC9685BCF6880091E6C2546;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<TextFx.PresetEffectSetting/VariableStateListener>
struct List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB;
// System.Collections.Generic.List`1<ToggleVariableSetting>[]
struct List_1U5BU5D_t8E82F1EFA45C7A2A85B854C2958583C5CDBA5A49;
// System.Collections.Generic.List`1<UIController/FrameData>
struct List_1_tFD496FCFB1F7C232809B0A18F6887706614F65F3;
// System.Collections.Generic.List`1<UIController/OptionData>
struct List_1_t5B38DDA976DDB66A172FD1855281F01EE9ED63F0;
// System.Collections.Generic.List`1<UIController/ProductJsonInfo>
struct List_1_t537386FE2177AFB667368B76654E8D513578BE29;
// System.Collections.Generic.List`1<Vector3VariableSetting>[]
struct List_1U5BU5D_t341F48BF0BBB99AD7A15EB05DF143D4F51E00E17;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TextFx.ObjectPool`1<ColourVariableSetting>
struct ObjectPool_1_tEBA797148C9EBAECCDF7A5C63A84E5BD48DBFFD6;
// TextFx.ObjectPool`1<FloatVariableSetting>
struct ObjectPool_1_tB80D5BA8A3F87AD8A47D4C01367054974FBB72D9;
// TextFx.ObjectPool`1<ToggleVariableSetting>
struct ObjectPool_1_t51704C3EDEED132C08C737E67EB7C575A554C826;
// TextFx.ObjectPool`1<Vector3VariableSetting>
struct ObjectPool_1_t6133E7CD47008A992DFB6630A04DD9E254682166;
// TextFx.TextFxAnimationManager/PresetAnimationSection
struct PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E;
// TextFx.TextFxAnimationManager/PresetAnimationSection[]
struct PresetAnimationSectionU5BU5D_t9664FB6764C6963FD3DC013B02B396E220CAF8C5;
// TextFx.TextFxUGUI
struct TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81;
// TextFxDemoManager
struct TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136;
// TextFxDemoManager/<>c__DisplayClass46_0
struct U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E;
// ToggleVariableSetting
struct ToggleVariableSetting_tE79CD9769496E78E42260EB965B78F11A4C4526B;
// UIController
struct UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE;
// UIController/ParseFunction
struct ParseFunction_tC04C029BB8EC10B09DE25A5B0AD388B75C177EA6;
// UIController/ParseFunctionWithPameter
struct ParseFunctionWithPameter_t43FB9510DA0A33033E4DA482AEACCCD943F8FC0A;
// UISingleton
struct UISingleton_t4848BA88F4BA51DBD493AEB09C803CA5D8B5D051;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E;
// UnityEngine.AsyncOperation
struct AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_tF94B8797BE321CF3ED1C3E6426CE13AFC8F1D478;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Renderer[]
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF;
// UnityEngine.ResourceRequest
struct ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486;
// UnityEngine.TextAsset
struct TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.Dropdown[]
struct DropdownU5BU5D_t3BF42BE9FA13EBBF708C115E950231B67CA3B3A9;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_tBC353C53E1DDE1197A0EF21D8016A713839A8F52;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// Vector3VariableSetting
struct Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CFADETOU3ED__6_TB7DE38D4E60439E6390B227410933907DD0B30A2_H
#define U3CFADETOU3ED__6_TB7DE38D4E60439E6390B227410933907DD0B30A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceAnimationHandler_<FadeTo>d__6
struct  U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2  : public RuntimeObject
{
public:
	// System.Int32 AceAnimationHandler_<FadeTo>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AceAnimationHandler_<FadeTo>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AceAnimationHandler AceAnimationHandler_<FadeTo>d__6::<>4__this
	AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0 * ___U3CU3E4__this_2;
	// System.Single AceAnimationHandler_<FadeTo>d__6::aValue
	float ___aValue_3;
	// System.Single AceAnimationHandler_<FadeTo>d__6::aTime
	float ___aTime_4;
	// System.Single AceAnimationHandler_<FadeTo>d__6::<defaultSize>5__2
	float ___U3CdefaultSizeU3E5__2_5;
	// System.Single AceAnimationHandler_<FadeTo>d__6::<t>5__3
	float ___U3CtU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2, ___U3CU3E4__this_2)); }
	inline AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_aValue_3() { return static_cast<int32_t>(offsetof(U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2, ___aValue_3)); }
	inline float get_aValue_3() const { return ___aValue_3; }
	inline float* get_address_of_aValue_3() { return &___aValue_3; }
	inline void set_aValue_3(float value)
	{
		___aValue_3 = value;
	}

	inline static int32_t get_offset_of_aTime_4() { return static_cast<int32_t>(offsetof(U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2, ___aTime_4)); }
	inline float get_aTime_4() const { return ___aTime_4; }
	inline float* get_address_of_aTime_4() { return &___aTime_4; }
	inline void set_aTime_4(float value)
	{
		___aTime_4 = value;
	}

	inline static int32_t get_offset_of_U3CdefaultSizeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2, ___U3CdefaultSizeU3E5__2_5)); }
	inline float get_U3CdefaultSizeU3E5__2_5() const { return ___U3CdefaultSizeU3E5__2_5; }
	inline float* get_address_of_U3CdefaultSizeU3E5__2_5() { return &___U3CdefaultSizeU3E5__2_5; }
	inline void set_U3CdefaultSizeU3E5__2_5(float value)
	{
		___U3CdefaultSizeU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2, ___U3CtU3E5__3_6)); }
	inline float get_U3CtU3E5__3_6() const { return ___U3CtU3E5__3_6; }
	inline float* get_address_of_U3CtU3E5__3_6() { return &___U3CtU3E5__3_6; }
	inline void set_U3CtU3E5__3_6(float value)
	{
		___U3CtU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADETOU3ED__6_TB7DE38D4E60439E6390B227410933907DD0B30A2_H
#ifndef U3CLOADSCENEU3ED__10_T3EB18C705C001ABBC54CA9C549377F15582AEB51_H
#define U3CLOADSCENEU3ED__10_T3EB18C705C001ABBC54CA9C549377F15582AEB51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseController_<LoadScene>d__10
struct  U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51  : public RuntimeObject
{
public:
	// System.Int32 BaseController_<LoadScene>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BaseController_<LoadScene>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BaseController BaseController_<LoadScene>d__10::<>4__this
	BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E * ___U3CU3E4__this_2;
	// System.String BaseController_<LoadScene>d__10::sceneName
	String_t* ___sceneName_3;
	// UnityEngine.AsyncOperation BaseController_<LoadScene>d__10::<async>5__2
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ___U3CasyncU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51, ___U3CU3E4__this_2)); }
	inline BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_sceneName_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51, ___sceneName_3)); }
	inline String_t* get_sceneName_3() const { return ___sceneName_3; }
	inline String_t** get_address_of_sceneName_3() { return &___sceneName_3; }
	inline void set_sceneName_3(String_t* value)
	{
		___sceneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_3), value);
	}

	inline static int32_t get_offset_of_U3CasyncU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51, ___U3CasyncU3E5__2_4)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get_U3CasyncU3E5__2_4() const { return ___U3CasyncU3E5__2_4; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of_U3CasyncU3E5__2_4() { return &___U3CasyncU3E5__2_4; }
	inline void set_U3CasyncU3E5__2_4(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		___U3CasyncU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasyncU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEU3ED__10_T3EB18C705C001ABBC54CA9C549377F15582AEB51_H
#ifndef U3CLOADSCENENONLOADINGU3ED__11_TBE89655DC3F8A54361E96EF9D83ABD667F4D704C_H
#define U3CLOADSCENENONLOADINGU3ED__11_TBE89655DC3F8A54361E96EF9D83ABD667F4D704C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseController_<LoadSceneNonLoading>d__11
struct  U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C  : public RuntimeObject
{
public:
	// System.Int32 BaseController_<LoadSceneNonLoading>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BaseController_<LoadSceneNonLoading>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BaseController BaseController_<LoadSceneNonLoading>d__11::<>4__this
	BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E * ___U3CU3E4__this_2;
	// System.String BaseController_<LoadSceneNonLoading>d__11::sceneName
	String_t* ___sceneName_3;
	// UnityEngine.AsyncOperation BaseController_<LoadSceneNonLoading>d__11::<async>5__2
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ___U3CasyncU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C, ___U3CU3E4__this_2)); }
	inline BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_sceneName_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C, ___sceneName_3)); }
	inline String_t* get_sceneName_3() const { return ___sceneName_3; }
	inline String_t** get_address_of_sceneName_3() { return &___sceneName_3; }
	inline void set_sceneName_3(String_t* value)
	{
		___sceneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_3), value);
	}

	inline static int32_t get_offset_of_U3CasyncU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C, ___U3CasyncU3E5__2_4)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get_U3CasyncU3E5__2_4() const { return ___U3CasyncU3E5__2_4; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of_U3CasyncU3E5__2_4() { return &___U3CasyncU3E5__2_4; }
	inline void set_U3CasyncU3E5__2_4(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		___U3CasyncU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasyncU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENENONLOADINGU3ED__11_TBE89655DC3F8A54361E96EF9D83ABD667F4D704C_H
#ifndef U3CRELOADSCENEPROCESSU3ED__29_TD9E3C3AD45C4DDC9959F865899D22C2D187B3ABD_H
#define U3CRELOADSCENEPROCESSU3ED__29_TD9E3C3AD45C4DDC9959F865899D22C2D187B3ABD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseController_<reloadSceneProcess>d__29
struct  U3CreloadSceneProcessU3Ed__29_tD9E3C3AD45C4DDC9959F865899D22C2D187B3ABD  : public RuntimeObject
{
public:
	// System.Int32 BaseController_<reloadSceneProcess>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BaseController_<reloadSceneProcess>d__29::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CreloadSceneProcessU3Ed__29_tD9E3C3AD45C4DDC9959F865899D22C2D187B3ABD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CreloadSceneProcessU3Ed__29_tD9E3C3AD45C4DDC9959F865899D22C2D187B3ABD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELOADSCENEPROCESSU3ED__29_TD9E3C3AD45C4DDC9959F865899D22C2D187B3ABD_H
#ifndef DEFINEWORD_T2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04_H
#define DEFINEWORD_T2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefineWord
struct  DefineWord_t2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04  : public RuntimeObject
{
public:

public:
};

struct DefineWord_t2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04_StaticFields
{
public:
	// System.Boolean DefineWord::__IS_SHOW_LOG
	bool _____IS_SHOW_LOG_0;
	// System.Boolean DefineWord::__IS_TEST
	bool _____IS_TEST_1;

public:
	inline static int32_t get_offset_of___IS_SHOW_LOG_0() { return static_cast<int32_t>(offsetof(DefineWord_t2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04_StaticFields, _____IS_SHOW_LOG_0)); }
	inline bool get___IS_SHOW_LOG_0() const { return _____IS_SHOW_LOG_0; }
	inline bool* get_address_of___IS_SHOW_LOG_0() { return &_____IS_SHOW_LOG_0; }
	inline void set___IS_SHOW_LOG_0(bool value)
	{
		_____IS_SHOW_LOG_0 = value;
	}

	inline static int32_t get_offset_of___IS_TEST_1() { return static_cast<int32_t>(offsetof(DefineWord_t2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04_StaticFields, _____IS_TEST_1)); }
	inline bool get___IS_TEST_1() const { return _____IS_TEST_1; }
	inline bool* get_address_of___IS_TEST_1() { return &_____IS_TEST_1; }
	inline void set___IS_TEST_1(bool value)
	{
		_____IS_TEST_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFINEWORD_T2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04_H
#ifndef EASINGMANAGER_TBB656F01E2A171C5EFEA690A9E294B5612936245_H
#define EASINGMANAGER_TBB656F01E2A171C5EFEA690A9E294B5612936245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasingManager
struct  EasingManager_tBB656F01E2A171C5EFEA690A9E294B5612936245  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASINGMANAGER_TBB656F01E2A171C5EFEA690A9E294B5612936245_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T161DB36F028399DE99B9B6DA061E4351DA20CA79_H
#define U3CU3EC__DISPLAYCLASS5_0_T161DB36F028399DE99B9B6DA061E4351DA20CA79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatVariableSetting_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t161DB36F028399DE99B9B6DA061E4351DA20CA79  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TextFx.PresetEffectSetting_VariableStateListener> FloatVariableSetting_<>c__DisplayClass5_0::varStateListeners
	List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB * ___varStateListeners_0;
	// System.Action FloatVariableSetting_<>c__DisplayClass5_0::valueChangedCallback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___valueChangedCallback_1;

public:
	inline static int32_t get_offset_of_varStateListeners_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t161DB36F028399DE99B9B6DA061E4351DA20CA79, ___varStateListeners_0)); }
	inline List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB * get_varStateListeners_0() const { return ___varStateListeners_0; }
	inline List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB ** get_address_of_varStateListeners_0() { return &___varStateListeners_0; }
	inline void set_varStateListeners_0(List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB * value)
	{
		___varStateListeners_0 = value;
		Il2CppCodeGenWriteBarrier((&___varStateListeners_0), value);
	}

	inline static int32_t get_offset_of_valueChangedCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t161DB36F028399DE99B9B6DA061E4351DA20CA79, ___valueChangedCallback_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_valueChangedCallback_1() const { return ___valueChangedCallback_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_valueChangedCallback_1() { return &___valueChangedCallback_1; }
	inline void set_valueChangedCallback_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___valueChangedCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T161DB36F028399DE99B9B6DA061E4351DA20CA79_H
#ifndef U3CADDMODELTARGETU3ED__12_T731D004438978B7025B321A45E502F1D6A9644CA_H
#define U3CADDMODELTARGETU3ED__12_T731D004438978B7025B321A45E502F1D6A9644CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelTargetController_<addModelTarget>d__12
struct  U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA  : public RuntimeObject
{
public:
	// System.Int32 ModelTargetController_<addModelTarget>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ModelTargetController_<addModelTarget>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ModelTargetController ModelTargetController_<addModelTarget>d__12::<>4__this
	ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A * ___U3CU3E4__this_2;
	// System.String ModelTargetController_<addModelTarget>d__12::<prefabName>5__2
	String_t* ___U3CprefabNameU3E5__2_3;
	// UnityEngine.ResourceRequest ModelTargetController_<addModelTarget>d__12::<loadAsync>5__3
	ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 * ___U3CloadAsyncU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA, ___U3CU3E4__this_2)); }
	inline ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CprefabNameU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA, ___U3CprefabNameU3E5__2_3)); }
	inline String_t* get_U3CprefabNameU3E5__2_3() const { return ___U3CprefabNameU3E5__2_3; }
	inline String_t** get_address_of_U3CprefabNameU3E5__2_3() { return &___U3CprefabNameU3E5__2_3; }
	inline void set_U3CprefabNameU3E5__2_3(String_t* value)
	{
		___U3CprefabNameU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprefabNameU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CloadAsyncU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA, ___U3CloadAsyncU3E5__3_4)); }
	inline ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 * get_U3CloadAsyncU3E5__3_4() const { return ___U3CloadAsyncU3E5__3_4; }
	inline ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 ** get_address_of_U3CloadAsyncU3E5__3_4() { return &___U3CloadAsyncU3E5__3_4; }
	inline void set_U3CloadAsyncU3E5__3_4(ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 * value)
	{
		___U3CloadAsyncU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadAsyncU3E5__3_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDMODELTARGETU3ED__12_T731D004438978B7025B321A45E502F1D6A9644CA_H
#ifndef U3CLOADSCENEU3ED__10_TE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C_H
#define U3CLOADSCENEU3ED__10_TE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelTargetMenuController_<LoadScene>d__10
struct  U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C  : public RuntimeObject
{
public:
	// System.Int32 ModelTargetMenuController_<LoadScene>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ModelTargetMenuController_<LoadScene>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ModelTargetMenuController ModelTargetMenuController_<LoadScene>d__10::<>4__this
	ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237 * ___U3CU3E4__this_2;
	// System.String ModelTargetMenuController_<LoadScene>d__10::modelTargetName
	String_t* ___modelTargetName_3;
	// UnityEngine.AsyncOperation ModelTargetMenuController_<LoadScene>d__10::<async>5__2
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ___U3CasyncU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C, ___U3CU3E4__this_2)); }
	inline ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_modelTargetName_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C, ___modelTargetName_3)); }
	inline String_t* get_modelTargetName_3() const { return ___modelTargetName_3; }
	inline String_t** get_address_of_modelTargetName_3() { return &___modelTargetName_3; }
	inline void set_modelTargetName_3(String_t* value)
	{
		___modelTargetName_3 = value;
		Il2CppCodeGenWriteBarrier((&___modelTargetName_3), value);
	}

	inline static int32_t get_offset_of_U3CasyncU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C, ___U3CasyncU3E5__2_4)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get_U3CasyncU3E5__2_4() const { return ___U3CasyncU3E5__2_4; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of_U3CasyncU3E5__2_4() { return &___U3CasyncU3E5__2_4; }
	inline void set_U3CasyncU3E5__2_4(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		___U3CasyncU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasyncU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEU3ED__10_TE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_TC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3_H
#define U3CU3EC__DISPLAYCLASS5_0_TC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductListButton_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3  : public RuntimeObject
{
public:
	// ProductListButton ProductListButton_<>c__DisplayClass5_0::<>4__this
	ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618 * ___U3CU3E4__this_0;
	// System.String ProductListButton_<>c__DisplayClass5_0::name
	String_t* ___name_1;
	// System.String ProductListButton_<>c__DisplayClass5_0::size
	String_t* ___size_2;
	// System.String ProductListButton_<>c__DisplayClass5_0::color
	String_t* ___color_3;
	// System.String ProductListButton_<>c__DisplayClass5_0::thumbnail
	String_t* ___thumbnail_4;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3, ___U3CU3E4__this_0)); }
	inline ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3, ___size_2)); }
	inline String_t* get_size_2() const { return ___size_2; }
	inline String_t** get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(String_t* value)
	{
		___size_2 = value;
		Il2CppCodeGenWriteBarrier((&___size_2), value);
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3, ___color_3)); }
	inline String_t* get_color_3() const { return ___color_3; }
	inline String_t** get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(String_t* value)
	{
		___color_3 = value;
		Il2CppCodeGenWriteBarrier((&___color_3), value);
	}

	inline static int32_t get_offset_of_thumbnail_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3, ___thumbnail_4)); }
	inline String_t* get_thumbnail_4() const { return ___thumbnail_4; }
	inline String_t** get_address_of_thumbnail_4() { return &___thumbnail_4; }
	inline void set_thumbnail_4(String_t* value)
	{
		___thumbnail_4 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_TC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_TE639376AE3E8B6EF41E0DFF630D85B48FC499252_H
#define U3CU3EC__DISPLAYCLASS9_0_TE639376AE3E8B6EF41E0DFF630D85B48FC499252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductListButton_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_tE639376AE3E8B6EF41E0DFF630D85B48FC499252  : public RuntimeObject
{
public:
	// System.String ProductListButton_<>c__DisplayClass9_0::frameID
	String_t* ___frameID_0;

public:
	inline static int32_t get_offset_of_frameID_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tE639376AE3E8B6EF41E0DFF630D85B48FC499252, ___frameID_0)); }
	inline String_t* get_frameID_0() const { return ___frameID_0; }
	inline String_t** get_address_of_frameID_0() { return &___frameID_0; }
	inline void set_frameID_0(String_t* value)
	{
		___frameID_0 = value;
		Il2CppCodeGenWriteBarrier((&___frameID_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_TE639376AE3E8B6EF41E0DFF630D85B48FC499252_H
#ifndef U3CGETTEXTUREU3ED__6_T2ADA756DD71C86ADD758DC706E76CF3A6AAFF452_H
#define U3CGETTEXTUREU3ED__6_T2ADA756DD71C86ADD758DC706E76CF3A6AAFF452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductListButton_<GetTexture>d__6
struct  U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452  : public RuntimeObject
{
public:
	// System.Int32 ProductListButton_<GetTexture>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ProductListButton_<GetTexture>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String ProductListButton_<GetTexture>d__6::path
	String_t* ___path_2;
	// UnityEngine.UI.Button ProductListButton_<GetTexture>d__6::button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___button_3;
	// UnityEngine.Networking.UnityWebRequest ProductListButton_<GetTexture>d__6::<www>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((&___path_2), value);
	}

	inline static int32_t get_offset_of_button_3() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452, ___button_3)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_button_3() const { return ___button_3; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_button_3() { return &___button_3; }
	inline void set_button_3(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___button_3 = value;
		Il2CppCodeGenWriteBarrier((&___button_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452, ___U3CwwwU3E5__2_4)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTEXTUREU3ED__6_T2ADA756DD71C86ADD758DC706E76CF3A6AAFF452_H
#ifndef PRODUCTDATA_T97D10D5108DB17AE26145FBB17907DAA56072F4A_H
#define PRODUCTDATA_T97D10D5108DB17AE26145FBB17907DAA56072F4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductListButton_ProductData
struct  ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A  : public RuntimeObject
{
public:
	// UnityEngine.UI.Button ProductListButton_ProductData::_button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____button_0;
	// System.String ProductListButton_ProductData::_id
	String_t* ____id_1;
	// System.String ProductListButton_ProductData::_path
	String_t* ____path_2;
	// System.String ProductListButton_ProductData::_size
	String_t* ____size_3;
	// System.String ProductListButton_ProductData::_color
	String_t* ____color_4;

public:
	inline static int32_t get_offset_of__button_0() { return static_cast<int32_t>(offsetof(ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A, ____button_0)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__button_0() const { return ____button_0; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__button_0() { return &____button_0; }
	inline void set__button_0(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____button_0 = value;
		Il2CppCodeGenWriteBarrier((&____button_0), value);
	}

	inline static int32_t get_offset_of__id_1() { return static_cast<int32_t>(offsetof(ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A, ____id_1)); }
	inline String_t* get__id_1() const { return ____id_1; }
	inline String_t** get_address_of__id_1() { return &____id_1; }
	inline void set__id_1(String_t* value)
	{
		____id_1 = value;
		Il2CppCodeGenWriteBarrier((&____id_1), value);
	}

	inline static int32_t get_offset_of__path_2() { return static_cast<int32_t>(offsetof(ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A, ____path_2)); }
	inline String_t* get__path_2() const { return ____path_2; }
	inline String_t** get_address_of__path_2() { return &____path_2; }
	inline void set__path_2(String_t* value)
	{
		____path_2 = value;
		Il2CppCodeGenWriteBarrier((&____path_2), value);
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A, ____size_3)); }
	inline String_t* get__size_3() const { return ____size_3; }
	inline String_t** get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(String_t* value)
	{
		____size_3 = value;
		Il2CppCodeGenWriteBarrier((&____size_3), value);
	}

	inline static int32_t get_offset_of__color_4() { return static_cast<int32_t>(offsetof(ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A, ____color_4)); }
	inline String_t* get__color_4() const { return ____color_4; }
	inline String_t** get_address_of__color_4() { return &____color_4; }
	inline void set__color_4(String_t* value)
	{
		____color_4 = value;
		Il2CppCodeGenWriteBarrier((&____color_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTDATA_T97D10D5108DB17AE26145FBB17907DAA56072F4A_H
#ifndef U3CLOADPRODUCTIMAGEU3ED__12_TBD789C839A9435513E33692AD0C3CADFD93EECE0_H
#define U3CLOADPRODUCTIMAGEU3ED__12_TBD789C839A9435513E33692AD0C3CADFD93EECE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductOptionUIManager_<LoadProductImage>d__12
struct  U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0  : public RuntimeObject
{
public:
	// System.Int32 ProductOptionUIManager_<LoadProductImage>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ProductOptionUIManager_<LoadProductImage>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String ProductOptionUIManager_<LoadProductImage>d__12::path
	String_t* ___path_2;
	// ProductOptionUIManager ProductOptionUIManager_<LoadProductImage>d__12::<>4__this
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87 * ___U3CU3E4__this_3;
	// UnityEngine.Networking.UnityWebRequest ProductOptionUIManager_<LoadProductImage>d__12::<www>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((&___path_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0, ___U3CU3E4__this_3)); }
	inline ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0, ___U3CwwwU3E5__2_4)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADPRODUCTIMAGEU3ED__12_TBD789C839A9435513E33692AD0C3CADFD93EECE0_H
#ifndef U3CU3EC_TFC53A04844766505D14CA1506B02CE5817038309_H
#define U3CU3EC_TFC53A04844766505D14CA1506B02CE5817038309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomSceneUIController_<>c
struct  U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309_StaticFields
{
public:
	// RoomSceneUIController_<>c RoomSceneUIController_<>c::<>9
	U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309 * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction RoomSceneUIController_<>c::<>9__34_4
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___U3CU3E9__34_4_1;
	// UnityEngine.Events.UnityAction RoomSceneUIController_<>c::<>9__34_5
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___U3CU3E9__34_5_2;
	// UnityEngine.Events.UnityAction RoomSceneUIController_<>c::<>9__34_6
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___U3CU3E9__34_6_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_4_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309_StaticFields, ___U3CU3E9__34_4_1)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_U3CU3E9__34_4_1() const { return ___U3CU3E9__34_4_1; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_U3CU3E9__34_4_1() { return &___U3CU3E9__34_4_1; }
	inline void set_U3CU3E9__34_4_1(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___U3CU3E9__34_4_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_4_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_5_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309_StaticFields, ___U3CU3E9__34_5_2)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_U3CU3E9__34_5_2() const { return ___U3CU3E9__34_5_2; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_U3CU3E9__34_5_2() { return &___U3CU3E9__34_5_2; }
	inline void set_U3CU3E9__34_5_2(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___U3CU3E9__34_5_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_5_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_6_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309_StaticFields, ___U3CU3E9__34_6_3)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_U3CU3E9__34_6_3() const { return ___U3CU3E9__34_6_3; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_U3CU3E9__34_6_3() { return &___U3CU3E9__34_6_3; }
	inline void set_U3CU3E9__34_6_3(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___U3CU3E9__34_6_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_6_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TFC53A04844766505D14CA1506B02CE5817038309_H
#ifndef U3CSCREENSHOTSAVEU3ED__37_TCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE_H
#define U3CSCREENSHOTSAVEU3ED__37_TCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomSceneUIController_<ScreenShotSave>d__37
struct  U3CScreenShotSaveU3Ed__37_tCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE  : public RuntimeObject
{
public:
	// System.Int32 RoomSceneUIController_<ScreenShotSave>d__37::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RoomSceneUIController_<ScreenShotSave>d__37::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RoomSceneUIController RoomSceneUIController_<ScreenShotSave>d__37::<>4__this
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScreenShotSaveU3Ed__37_tCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScreenShotSaveU3Ed__37_tCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CScreenShotSaveU3Ed__37_tCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE, ___U3CU3E4__this_2)); }
	inline RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCREENSHOTSAVEU3ED__37_TCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE_H
#ifndef U3CSCREENSHOTSAVECONFIRMU3ED__39_T537CCB2088709400A3703418EBB90A36FEDCDE72_H
#define U3CSCREENSHOTSAVECONFIRMU3ED__39_T537CCB2088709400A3703418EBB90A36FEDCDE72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomSceneUIController_<ScreenShotSaveConfirm>d__39
struct  U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72  : public RuntimeObject
{
public:
	// System.Int32 RoomSceneUIController_<ScreenShotSaveConfirm>d__39::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RoomSceneUIController_<ScreenShotSaveConfirm>d__39::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String RoomSceneUIController_<ScreenShotSaveConfirm>d__39::path
	String_t* ___path_2;
	// RoomSceneUIController RoomSceneUIController_<ScreenShotSaveConfirm>d__39::<>4__this
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * ___U3CU3E4__this_3;
	// System.Int32 RoomSceneUIController_<ScreenShotSaveConfirm>d__39::<count>5__2
	int32_t ___U3CcountU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((&___path_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72, ___U3CU3E4__this_3)); }
	inline RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CcountU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72, ___U3CcountU3E5__2_4)); }
	inline int32_t get_U3CcountU3E5__2_4() const { return ___U3CcountU3E5__2_4; }
	inline int32_t* get_address_of_U3CcountU3E5__2_4() { return &___U3CcountU3E5__2_4; }
	inline void set_U3CcountU3E5__2_4(int32_t value)
	{
		___U3CcountU3E5__2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCREENSHOTSAVECONFIRMU3ED__39_T537CCB2088709400A3703418EBB90A36FEDCDE72_H
#ifndef U3CSCREENSHOTSHAREU3ED__38_TEB503A12F87879EEFEB98DE6803E5E2C77876FDE_H
#define U3CSCREENSHOTSHAREU3ED__38_TEB503A12F87879EEFEB98DE6803E5E2C77876FDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomSceneUIController_<ScreenShotShare>d__38
struct  U3CScreenShotShareU3Ed__38_tEB503A12F87879EEFEB98DE6803E5E2C77876FDE  : public RuntimeObject
{
public:
	// System.Int32 RoomSceneUIController_<ScreenShotShare>d__38::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RoomSceneUIController_<ScreenShotShare>d__38::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RoomSceneUIController RoomSceneUIController_<ScreenShotShare>d__38::<>4__this
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScreenShotShareU3Ed__38_tEB503A12F87879EEFEB98DE6803E5E2C77876FDE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScreenShotShareU3Ed__38_tEB503A12F87879EEFEB98DE6803E5E2C77876FDE, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CScreenShotShareU3Ed__38_tEB503A12F87879EEFEB98DE6803E5E2C77876FDE, ___U3CU3E4__this_2)); }
	inline RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCREENSHOTSHAREU3ED__38_TEB503A12F87879EEFEB98DE6803E5E2C77876FDE_H
#ifndef U3CSCREENSHOTSHARECONFIRMU3ED__40_TB43F09C624893450A723A1178992A02A63E5066B_H
#define U3CSCREENSHOTSHARECONFIRMU3ED__40_TB43F09C624893450A723A1178992A02A63E5066B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomSceneUIController_<ScreenShotShareConfirm>d__40
struct  U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B  : public RuntimeObject
{
public:
	// System.Int32 RoomSceneUIController_<ScreenShotShareConfirm>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RoomSceneUIController_<ScreenShotShareConfirm>d__40::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String RoomSceneUIController_<ScreenShotShareConfirm>d__40::path
	String_t* ___path_2;
	// RoomSceneUIController RoomSceneUIController_<ScreenShotShareConfirm>d__40::<>4__this
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * ___U3CU3E4__this_3;
	// System.Int32 RoomSceneUIController_<ScreenShotShareConfirm>d__40::<count>5__2
	int32_t ___U3CcountU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((&___path_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B, ___U3CU3E4__this_3)); }
	inline RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CcountU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B, ___U3CcountU3E5__2_4)); }
	inline int32_t get_U3CcountU3E5__2_4() const { return ___U3CcountU3E5__2_4; }
	inline int32_t* get_address_of_U3CcountU3E5__2_4() { return &___U3CcountU3E5__2_4; }
	inline void set_U3CcountU3E5__2_4(int32_t value)
	{
		___U3CcountU3E5__2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCREENSHOTSHARECONFIRMU3ED__40_TB43F09C624893450A723A1178992A02A63E5066B_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CU3EC__DISPLAYCLASS46_0_T8DCC70B857AE08233984EF6952A3C1E6DE52A06E_H
#define U3CU3EC__DISPLAYCLASS46_0_T8DCC70B857AE08233984EF6952A3C1E6DE52A06E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFxDemoManager_<>c__DisplayClass46_0
struct  U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E  : public RuntimeObject
{
public:
	// TextFxDemoManager TextFxDemoManager_<>c__DisplayClass46_0::<>4__this
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * ___U3CU3E4__this_0;
	// TextFx.TextFxAnimationManager_PresetAnimationSection TextFxDemoManager_<>c__DisplayClass46_0::animation_section
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * ___animation_section_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E, ___U3CU3E4__this_0)); }
	inline TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_animation_section_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E, ___animation_section_1)); }
	inline PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * get_animation_section_1() const { return ___animation_section_1; }
	inline PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E ** get_address_of_animation_section_1() { return &___animation_section_1; }
	inline void set_animation_section_1(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * value)
	{
		___animation_section_1 = value;
		Il2CppCodeGenWriteBarrier((&___animation_section_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS46_0_T8DCC70B857AE08233984EF6952A3C1E6DE52A06E_H
#ifndef U3CU3EC__DISPLAYCLASS46_2_TF9FBD84656E1D0826FC1604255D0B37555C67826_H
#define U3CU3EC__DISPLAYCLASS46_2_TF9FBD84656E1D0826FC1604255D0B37555C67826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFxDemoManager_<>c__DisplayClass46_2
struct  U3CU3Ec__DisplayClass46_2_tF9FBD84656E1D0826FC1604255D0B37555C67826  : public RuntimeObject
{
public:
	// FloatVariableSetting TextFxDemoManager_<>c__DisplayClass46_2::floatVarSetting
	FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3 * ___floatVarSetting_0;
	// TextFxDemoManager_<>c__DisplayClass46_0 TextFxDemoManager_<>c__DisplayClass46_2::CSU24<>8__locals2
	U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E * ___CSU24U3CU3E8__locals2_1;

public:
	inline static int32_t get_offset_of_floatVarSetting_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_2_tF9FBD84656E1D0826FC1604255D0B37555C67826, ___floatVarSetting_0)); }
	inline FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3 * get_floatVarSetting_0() const { return ___floatVarSetting_0; }
	inline FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3 ** get_address_of_floatVarSetting_0() { return &___floatVarSetting_0; }
	inline void set_floatVarSetting_0(FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3 * value)
	{
		___floatVarSetting_0 = value;
		Il2CppCodeGenWriteBarrier((&___floatVarSetting_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_2_tF9FBD84656E1D0826FC1604255D0B37555C67826, ___CSU24U3CU3E8__locals2_1)); }
	inline U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E * get_CSU24U3CU3E8__locals2_1() const { return ___CSU24U3CU3E8__locals2_1; }
	inline U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E ** get_address_of_CSU24U3CU3E8__locals2_1() { return &___CSU24U3CU3E8__locals2_1; }
	inline void set_CSU24U3CU3E8__locals2_1(U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E * value)
	{
		___CSU24U3CU3E8__locals2_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS46_2_TF9FBD84656E1D0826FC1604255D0B37555C67826_H
#ifndef U3CHIGHLIGHTCONTINUEBUTTONU3ED__40_T36630BA04716C4FF967218B5C26EFBA81435A13D_H
#define U3CHIGHLIGHTCONTINUEBUTTONU3ED__40_T36630BA04716C4FF967218B5C26EFBA81435A13D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFxDemoManager_<HighlightContinueButton>d__40
struct  U3CHighlightContinueButtonU3Ed__40_t36630BA04716C4FF967218B5C26EFBA81435A13D  : public RuntimeObject
{
public:
	// System.Int32 TextFxDemoManager_<HighlightContinueButton>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TextFxDemoManager_<HighlightContinueButton>d__40::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TextFxDemoManager TextFxDemoManager_<HighlightContinueButton>d__40::<>4__this
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHighlightContinueButtonU3Ed__40_t36630BA04716C4FF967218B5C26EFBA81435A13D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHighlightContinueButtonU3Ed__40_t36630BA04716C4FF967218B5C26EFBA81435A13D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CHighlightContinueButtonU3Ed__40_t36630BA04716C4FF967218B5C26EFBA81435A13D, ___U3CU3E4__this_2)); }
	inline TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIGHLIGHTCONTINUEBUTTONU3ED__40_T36630BA04716C4FF967218B5C26EFBA81435A13D_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TB268DFAD785FE57474CE5D69DF3E29EC536796E4_H
#define U3CU3EC__DISPLAYCLASS1_0_TB268DFAD785FE57474CE5D69DF3E29EC536796E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleVariableSetting_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tB268DFAD785FE57474CE5D69DF3E29EC536796E4  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TextFx.PresetEffectSetting_VariableStateListener> ToggleVariableSetting_<>c__DisplayClass1_0::varStateListener
	List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB * ___varStateListener_0;
	// System.Action ToggleVariableSetting_<>c__DisplayClass1_0::valueChangedCallback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___valueChangedCallback_1;

public:
	inline static int32_t get_offset_of_varStateListener_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tB268DFAD785FE57474CE5D69DF3E29EC536796E4, ___varStateListener_0)); }
	inline List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB * get_varStateListener_0() const { return ___varStateListener_0; }
	inline List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB ** get_address_of_varStateListener_0() { return &___varStateListener_0; }
	inline void set_varStateListener_0(List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB * value)
	{
		___varStateListener_0 = value;
		Il2CppCodeGenWriteBarrier((&___varStateListener_0), value);
	}

	inline static int32_t get_offset_of_valueChangedCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tB268DFAD785FE57474CE5D69DF3E29EC536796E4, ___valueChangedCallback_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_valueChangedCallback_1() const { return ___valueChangedCallback_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_valueChangedCallback_1() { return &___valueChangedCallback_1; }
	inline void set_valueChangedCallback_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___valueChangedCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TB268DFAD785FE57474CE5D69DF3E29EC536796E4_H
#ifndef U3CU3EC_T4F418134860FCB9C317EE13C7F876D2FDE26BD5B_H
#define U3CU3EC_T4F418134860FCB9C317EE13C7F876D2FDE26BD5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_<>c
struct  U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields
{
public:
	// UIController_<>c UIController_<>c::<>9
	U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B * ___U3CU3E9_0;
	// System.Action UIController_<>c::<>9__68_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__68_0_1;
	// System.Action UIController_<>c::<>9__68_1
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__68_1_2;
	// DLLCore.ProductController_LoadCallback UIController_<>c::<>9__84_0
	LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * ___U3CU3E9__84_0_3;
	// DLLCore.ProductController_LoadCallback UIController_<>c::<>9__85_0
	LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * ___U3CU3E9__85_0_4;
	// DLLCore.UnitController_LoadCallback UIController_<>c::<>9__88_0
	LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B * ___U3CU3E9__88_0_5;
	// DLLCore.ToolController_ScreenShotPath UIController_<>c::<>9__95_0
	ScreenShotPath_t5E0DCEF7D4B345890D428A110D3532816CD973A8 * ___U3CU3E9__95_0_6;
	// DLLCore.ProductController_LoadCallback UIController_<>c::<>9__121_0
	LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * ___U3CU3E9__121_0_7;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__68_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields, ___U3CU3E9__68_0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__68_0_1() const { return ___U3CU3E9__68_0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__68_0_1() { return &___U3CU3E9__68_0_1; }
	inline void set_U3CU3E9__68_0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__68_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__68_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__68_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields, ___U3CU3E9__68_1_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__68_1_2() const { return ___U3CU3E9__68_1_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__68_1_2() { return &___U3CU3E9__68_1_2; }
	inline void set_U3CU3E9__68_1_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__68_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__68_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__84_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields, ___U3CU3E9__84_0_3)); }
	inline LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * get_U3CU3E9__84_0_3() const { return ___U3CU3E9__84_0_3; }
	inline LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B ** get_address_of_U3CU3E9__84_0_3() { return &___U3CU3E9__84_0_3; }
	inline void set_U3CU3E9__84_0_3(LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * value)
	{
		___U3CU3E9__84_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__84_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__85_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields, ___U3CU3E9__85_0_4)); }
	inline LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * get_U3CU3E9__85_0_4() const { return ___U3CU3E9__85_0_4; }
	inline LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B ** get_address_of_U3CU3E9__85_0_4() { return &___U3CU3E9__85_0_4; }
	inline void set_U3CU3E9__85_0_4(LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * value)
	{
		___U3CU3E9__85_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__85_0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__88_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields, ___U3CU3E9__88_0_5)); }
	inline LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B * get_U3CU3E9__88_0_5() const { return ___U3CU3E9__88_0_5; }
	inline LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B ** get_address_of_U3CU3E9__88_0_5() { return &___U3CU3E9__88_0_5; }
	inline void set_U3CU3E9__88_0_5(LoadCallback_t2161C8E09694A1ECBB49F840CE6B9B148422059B * value)
	{
		___U3CU3E9__88_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__88_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__95_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields, ___U3CU3E9__95_0_6)); }
	inline ScreenShotPath_t5E0DCEF7D4B345890D428A110D3532816CD973A8 * get_U3CU3E9__95_0_6() const { return ___U3CU3E9__95_0_6; }
	inline ScreenShotPath_t5E0DCEF7D4B345890D428A110D3532816CD973A8 ** get_address_of_U3CU3E9__95_0_6() { return &___U3CU3E9__95_0_6; }
	inline void set_U3CU3E9__95_0_6(ScreenShotPath_t5E0DCEF7D4B345890D428A110D3532816CD973A8 * value)
	{
		___U3CU3E9__95_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__95_0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__121_0_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields, ___U3CU3E9__121_0_7)); }
	inline LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * get_U3CU3E9__121_0_7() const { return ___U3CU3E9__121_0_7; }
	inline LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B ** get_address_of_U3CU3E9__121_0_7() { return &___U3CU3E9__121_0_7; }
	inline void set_U3CU3E9__121_0_7(LoadCallback_t45B2E8C0BFB2EE8E7AC566533160FF6D20B5AB3B * value)
	{
		___U3CU3E9__121_0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__121_0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4F418134860FCB9C317EE13C7F876D2FDE26BD5B_H
#ifndef U3CU3EC__DISPLAYCLASS112_0_TB46A03BC2A3B17DFAF7050D62CEA5FEC3C107C07_H
#define U3CU3EC__DISPLAYCLASS112_0_TB46A03BC2A3B17DFAF7050D62CEA5FEC3C107C07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_<>c__DisplayClass112_0
struct  U3CU3Ec__DisplayClass112_0_tB46A03BC2A3B17DFAF7050D62CEA5FEC3C107C07  : public RuntimeObject
{
public:
	// System.String UIController_<>c__DisplayClass112_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass112_0_tB46A03BC2A3B17DFAF7050D62CEA5FEC3C107C07, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS112_0_TB46A03BC2A3B17DFAF7050D62CEA5FEC3C107C07_H
#ifndef U3CU3EC__DISPLAYCLASS113_0_T450925695AB92BBDA7EF4569CB58F31B212284DC_H
#define U3CU3EC__DISPLAYCLASS113_0_T450925695AB92BBDA7EF4569CB58F31B212284DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_<>c__DisplayClass113_0
struct  U3CU3Ec__DisplayClass113_0_t450925695AB92BBDA7EF4569CB58F31B212284DC  : public RuntimeObject
{
public:
	// System.String UIController_<>c__DisplayClass113_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t450925695AB92BBDA7EF4569CB58F31B212284DC, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS113_0_T450925695AB92BBDA7EF4569CB58F31B212284DC_H
#ifndef U3CGETPARSEFROMJSONU3ED__106_T4555CCD989BED3756776F08E2246A2B29A285697_H
#define U3CGETPARSEFROMJSONU3ED__106_T4555CCD989BED3756776F08E2246A2B29A285697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_<GetParseFromJSON>d__106
struct  U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697  : public RuntimeObject
{
public:
	// System.Int32 UIController_<GetParseFromJSON>d__106::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UIController_<GetParseFromJSON>d__106::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UIController_<GetParseFromJSON>d__106::url
	String_t* ___url_2;
	// UIController_ParseFunction UIController_<GetParseFromJSON>d__106::callback
	ParseFunction_tC04C029BB8EC10B09DE25A5B0AD388B75C177EA6 * ___callback_3;
	// UnityEngine.Networking.UnityWebRequest UIController_<GetParseFromJSON>d__106::<www>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697, ___callback_3)); }
	inline ParseFunction_tC04C029BB8EC10B09DE25A5B0AD388B75C177EA6 * get_callback_3() const { return ___callback_3; }
	inline ParseFunction_tC04C029BB8EC10B09DE25A5B0AD388B75C177EA6 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(ParseFunction_tC04C029BB8EC10B09DE25A5B0AD388B75C177EA6 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697, ___U3CwwwU3E5__2_4)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPARSEFROMJSONU3ED__106_T4555CCD989BED3756776F08E2246A2B29A285697_H
#ifndef U3CGETPARSEFROMJSONANDCALLBACKU3ED__107_TBECA1D38B02262689AF1EE4BD318DF51BEBE6437_H
#define U3CGETPARSEFROMJSONANDCALLBACKU3ED__107_TBECA1D38B02262689AF1EE4BD318DF51BEBE6437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_<GetParseFromJSONAndCallback>d__107
struct  U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437  : public RuntimeObject
{
public:
	// System.Int32 UIController_<GetParseFromJSONAndCallback>d__107::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UIController_<GetParseFromJSONAndCallback>d__107::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UIController_<GetParseFromJSONAndCallback>d__107::url
	String_t* ___url_2;
	// UIController_ParseFunctionWithPameter UIController_<GetParseFromJSONAndCallback>d__107::callback
	ParseFunctionWithPameter_t43FB9510DA0A33033E4DA482AEACCCD943F8FC0A * ___callback_3;
	// System.Boolean UIController_<GetParseFromJSONAndCallback>d__107::createUI
	bool ___createUI_4;
	// UnityEngine.Networking.UnityWebRequest UIController_<GetParseFromJSONAndCallback>d__107::<www>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437, ___callback_3)); }
	inline ParseFunctionWithPameter_t43FB9510DA0A33033E4DA482AEACCCD943F8FC0A * get_callback_3() const { return ___callback_3; }
	inline ParseFunctionWithPameter_t43FB9510DA0A33033E4DA482AEACCCD943F8FC0A ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(ParseFunctionWithPameter_t43FB9510DA0A33033E4DA482AEACCCD943F8FC0A * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_createUI_4() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437, ___createUI_4)); }
	inline bool get_createUI_4() const { return ___createUI_4; }
	inline bool* get_address_of_createUI_4() { return &___createUI_4; }
	inline void set_createUI_4(bool value)
	{
		___createUI_4 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437, ___U3CwwwU3E5__2_5)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E5__2_5() const { return ___U3CwwwU3E5__2_5; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E5__2_5() { return &___U3CwwwU3E5__2_5; }
	inline void set_U3CwwwU3E5__2_5(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPARSEFROMJSONANDCALLBACKU3ED__107_TBECA1D38B02262689AF1EE4BD318DF51BEBE6437_H
#ifndef U3CSCREENSHOTU3ED__95_TE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47_H
#define U3CSCREENSHOTU3ED__95_TE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_<ScreenShot>d__95
struct  U3CScreenShotU3Ed__95_tE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47  : public RuntimeObject
{
public:
	// System.Int32 UIController_<ScreenShot>d__95::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UIController_<ScreenShot>d__95::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UIController UIController_<ScreenShot>d__95::<>4__this
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScreenShotU3Ed__95_tE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScreenShotU3Ed__95_tE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CScreenShotU3Ed__95_tE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47, ___U3CU3E4__this_2)); }
	inline UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCREENSHOTU3ED__95_TE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47_H
#ifndef FRAMEDATA_T0374CCD41A750DB3016CDFC0D537D19C95F05D22_H
#define FRAMEDATA_T0374CCD41A750DB3016CDFC0D537D19C95F05D22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_FrameData
struct  FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22  : public RuntimeObject
{
public:
	// System.String UIController_FrameData::frameID
	String_t* ___frameID_0;
	// System.String UIController_FrameData::frameName
	String_t* ___frameName_1;
	// System.String UIController_FrameData::frameSize
	String_t* ___frameSize_2;
	// System.String UIController_FrameData::frameColor
	String_t* ___frameColor_3;
	// System.String UIController_FrameData::path
	String_t* ___path_4;
	// System.String UIController_FrameData::thumbnail
	String_t* ___thumbnail_5;

public:
	inline static int32_t get_offset_of_frameID_0() { return static_cast<int32_t>(offsetof(FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22, ___frameID_0)); }
	inline String_t* get_frameID_0() const { return ___frameID_0; }
	inline String_t** get_address_of_frameID_0() { return &___frameID_0; }
	inline void set_frameID_0(String_t* value)
	{
		___frameID_0 = value;
		Il2CppCodeGenWriteBarrier((&___frameID_0), value);
	}

	inline static int32_t get_offset_of_frameName_1() { return static_cast<int32_t>(offsetof(FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22, ___frameName_1)); }
	inline String_t* get_frameName_1() const { return ___frameName_1; }
	inline String_t** get_address_of_frameName_1() { return &___frameName_1; }
	inline void set_frameName_1(String_t* value)
	{
		___frameName_1 = value;
		Il2CppCodeGenWriteBarrier((&___frameName_1), value);
	}

	inline static int32_t get_offset_of_frameSize_2() { return static_cast<int32_t>(offsetof(FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22, ___frameSize_2)); }
	inline String_t* get_frameSize_2() const { return ___frameSize_2; }
	inline String_t** get_address_of_frameSize_2() { return &___frameSize_2; }
	inline void set_frameSize_2(String_t* value)
	{
		___frameSize_2 = value;
		Il2CppCodeGenWriteBarrier((&___frameSize_2), value);
	}

	inline static int32_t get_offset_of_frameColor_3() { return static_cast<int32_t>(offsetof(FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22, ___frameColor_3)); }
	inline String_t* get_frameColor_3() const { return ___frameColor_3; }
	inline String_t** get_address_of_frameColor_3() { return &___frameColor_3; }
	inline void set_frameColor_3(String_t* value)
	{
		___frameColor_3 = value;
		Il2CppCodeGenWriteBarrier((&___frameColor_3), value);
	}

	inline static int32_t get_offset_of_path_4() { return static_cast<int32_t>(offsetof(FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22, ___path_4)); }
	inline String_t* get_path_4() const { return ___path_4; }
	inline String_t** get_address_of_path_4() { return &___path_4; }
	inline void set_path_4(String_t* value)
	{
		___path_4 = value;
		Il2CppCodeGenWriteBarrier((&___path_4), value);
	}

	inline static int32_t get_offset_of_thumbnail_5() { return static_cast<int32_t>(offsetof(FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22, ___thumbnail_5)); }
	inline String_t* get_thumbnail_5() const { return ___thumbnail_5; }
	inline String_t** get_address_of_thumbnail_5() { return &___thumbnail_5; }
	inline void set_thumbnail_5(String_t* value)
	{
		___thumbnail_5 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEDATA_T0374CCD41A750DB3016CDFC0D537D19C95F05D22_H
#ifndef OPTIONDATA_T2EA1EFBB927710E7ABFE05D6F203E09FD3649927_H
#define OPTIONDATA_T2EA1EFBB927710E7ABFE05D6F203E09FD3649927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_OptionData
struct  OptionData_t2EA1EFBB927710E7ABFE05D6F203E09FD3649927  : public RuntimeObject
{
public:
	// System.String UIController_OptionData::name
	String_t* ___name_0;
	// System.String[] UIController_OptionData::sizeOption
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___sizeOption_1;
	// System.String[] UIController_OptionData::colorOption
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___colorOption_2;
	// System.String UIController_OptionData::filter
	String_t* ___filter_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(OptionData_t2EA1EFBB927710E7ABFE05D6F203E09FD3649927, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_sizeOption_1() { return static_cast<int32_t>(offsetof(OptionData_t2EA1EFBB927710E7ABFE05D6F203E09FD3649927, ___sizeOption_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_sizeOption_1() const { return ___sizeOption_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_sizeOption_1() { return &___sizeOption_1; }
	inline void set_sizeOption_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___sizeOption_1 = value;
		Il2CppCodeGenWriteBarrier((&___sizeOption_1), value);
	}

	inline static int32_t get_offset_of_colorOption_2() { return static_cast<int32_t>(offsetof(OptionData_t2EA1EFBB927710E7ABFE05D6F203E09FD3649927, ___colorOption_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_colorOption_2() const { return ___colorOption_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_colorOption_2() { return &___colorOption_2; }
	inline void set_colorOption_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___colorOption_2 = value;
		Il2CppCodeGenWriteBarrier((&___colorOption_2), value);
	}

	inline static int32_t get_offset_of_filter_3() { return static_cast<int32_t>(offsetof(OptionData_t2EA1EFBB927710E7ABFE05D6F203E09FD3649927, ___filter_3)); }
	inline String_t* get_filter_3() const { return ___filter_3; }
	inline String_t** get_address_of_filter_3() { return &___filter_3; }
	inline void set_filter_3(String_t* value)
	{
		___filter_3 = value;
		Il2CppCodeGenWriteBarrier((&___filter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONDATA_T2EA1EFBB927710E7ABFE05D6F203E09FD3649927_H
#ifndef PRODUCTJSONINFO_TD05B0BADB053B76F25882940F9FA94857EEBD7C0_H
#define PRODUCTJSONINFO_TD05B0BADB053B76F25882940F9FA94857EEBD7C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_ProductJsonInfo
struct  ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0  : public RuntimeObject
{
public:
	// System.String UIController_ProductJsonInfo::frameId
	String_t* ___frameId_0;
	// System.String UIController_ProductJsonInfo::name
	String_t* ___name_1;
	// System.String UIController_ProductJsonInfo::size
	String_t* ___size_2;
	// System.String UIController_ProductJsonInfo::color
	String_t* ___color_3;
	// System.String UIController_ProductJsonInfo::framePath
	String_t* ___framePath_4;
	// System.String UIController_ProductJsonInfo::thumbnailPath
	String_t* ___thumbnailPath_5;
	// System.String UIController_ProductJsonInfo::catalogThumbnail
	String_t* ___catalogThumbnail_6;

public:
	inline static int32_t get_offset_of_frameId_0() { return static_cast<int32_t>(offsetof(ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0, ___frameId_0)); }
	inline String_t* get_frameId_0() const { return ___frameId_0; }
	inline String_t** get_address_of_frameId_0() { return &___frameId_0; }
	inline void set_frameId_0(String_t* value)
	{
		___frameId_0 = value;
		Il2CppCodeGenWriteBarrier((&___frameId_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0, ___size_2)); }
	inline String_t* get_size_2() const { return ___size_2; }
	inline String_t** get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(String_t* value)
	{
		___size_2 = value;
		Il2CppCodeGenWriteBarrier((&___size_2), value);
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0, ___color_3)); }
	inline String_t* get_color_3() const { return ___color_3; }
	inline String_t** get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(String_t* value)
	{
		___color_3 = value;
		Il2CppCodeGenWriteBarrier((&___color_3), value);
	}

	inline static int32_t get_offset_of_framePath_4() { return static_cast<int32_t>(offsetof(ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0, ___framePath_4)); }
	inline String_t* get_framePath_4() const { return ___framePath_4; }
	inline String_t** get_address_of_framePath_4() { return &___framePath_4; }
	inline void set_framePath_4(String_t* value)
	{
		___framePath_4 = value;
		Il2CppCodeGenWriteBarrier((&___framePath_4), value);
	}

	inline static int32_t get_offset_of_thumbnailPath_5() { return static_cast<int32_t>(offsetof(ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0, ___thumbnailPath_5)); }
	inline String_t* get_thumbnailPath_5() const { return ___thumbnailPath_5; }
	inline String_t** get_address_of_thumbnailPath_5() { return &___thumbnailPath_5; }
	inline void set_thumbnailPath_5(String_t* value)
	{
		___thumbnailPath_5 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnailPath_5), value);
	}

	inline static int32_t get_offset_of_catalogThumbnail_6() { return static_cast<int32_t>(offsetof(ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0, ___catalogThumbnail_6)); }
	inline String_t* get_catalogThumbnail_6() const { return ___catalogThumbnail_6; }
	inline String_t** get_address_of_catalogThumbnail_6() { return &___catalogThumbnail_6; }
	inline void set_catalogThumbnail_6(String_t* value)
	{
		___catalogThumbnail_6 = value;
		Il2CppCodeGenWriteBarrier((&___catalogThumbnail_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTJSONINFO_TD05B0BADB053B76F25882940F9FA94857EEBD7C0_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T723BBB5CDAFC9BEBBEDDD19569579B41C065F15E_H
#define U3CU3EC__DISPLAYCLASS6_0_T723BBB5CDAFC9BEBBEDDD19569579B41C065F15E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vector3VariableSetting_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t723BBB5CDAFC9BEBBEDDD19569579B41C065F15E  : public RuntimeObject
{
public:
	// Vector3VariableSetting Vector3VariableSetting_<>c__DisplayClass6_0::<>4__this
	Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<TextFx.PresetEffectSetting_VariableStateListener> Vector3VariableSetting_<>c__DisplayClass6_0::varStateListeners
	List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB * ___varStateListeners_1;
	// System.Action Vector3VariableSetting_<>c__DisplayClass6_0::valueChangedCallback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___valueChangedCallback_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t723BBB5CDAFC9BEBBEDDD19569579B41C065F15E, ___U3CU3E4__this_0)); }
	inline Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_varStateListeners_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t723BBB5CDAFC9BEBBEDDD19569579B41C065F15E, ___varStateListeners_1)); }
	inline List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB * get_varStateListeners_1() const { return ___varStateListeners_1; }
	inline List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB ** get_address_of_varStateListeners_1() { return &___varStateListeners_1; }
	inline void set_varStateListeners_1(List_1_tE50AA5F6A3ACD2AEA2C6518AB56C4CF7A89EB3BB * value)
	{
		___varStateListeners_1 = value;
		Il2CppCodeGenWriteBarrier((&___varStateListeners_1), value);
	}

	inline static int32_t get_offset_of_valueChangedCallback_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t723BBB5CDAFC9BEBBEDDD19569579B41C065F15E, ___valueChangedCallback_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_valueChangedCallback_2() const { return ___valueChangedCallback_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_valueChangedCallback_2() { return &___valueChangedCallback_2; }
	inline void set_valueChangedCallback_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___valueChangedCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedCallback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T723BBB5CDAFC9BEBBEDDD19569579B41C065F15E_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#define DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef GUISTATE_TF63BEA966B3EDD0C31938A1DFC1D973A9F511DB9_H
#define GUISTATE_TF63BEA966B3EDD0C31938A1DFC1D973A9F511DB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARUIController_GUIState
struct  GUIState_tF63BEA966B3EDD0C31938A1DFC1D973A9F511DB9 
{
public:
	// System.Byte ARUIController_GUIState::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GUIState_tF63BEA966B3EDD0C31938A1DFC1D973A9F511DB9, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISTATE_TF63BEA966B3EDD0C31938A1DFC1D973A9F511DB9_H
#ifndef MATTRESSANIMATION_TF86AB46699E5B0F061EC62D9D95B5F4330C221D3_H
#define MATTRESSANIMATION_TF86AB46699E5B0F061EC62D9D95B5F4330C221D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARUIController_MattressAnimation
struct  MattressAnimation_tF86AB46699E5B0F061EC62D9D95B5F4330C221D3 
{
public:
	// System.Int32 ARUIController_MattressAnimation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MattressAnimation_tF86AB46699E5B0F061EC62D9D95B5F4330C221D3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATTRESSANIMATION_TF86AB46699E5B0F061EC62D9D95B5F4330C221D3_H
#ifndef ACESCENEMODE_T07127C4981D5FBF5B79A781E16FD26B6C08E2C72_H
#define ACESCENEMODE_T07127C4981D5FBF5B79A781E16FD26B6C08E2C72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseController_ACESceneMode
struct  ACESceneMode_t07127C4981D5FBF5B79A781E16FD26B6C08E2C72 
{
public:
	// System.Byte BaseController_ACESceneMode::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ACESceneMode_t07127C4981D5FBF5B79A781E16FD26B6C08E2C72, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACESCENEMODE_T07127C4981D5FBF5B79A781E16FD26B6C08E2C72_H
#ifndef EASINGEQUATION_TCA0CBD0561A15202778424F90084F965713F1C82_H
#define EASINGEQUATION_TCA0CBD0561A15202778424F90084F965713F1C82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasingEquation
struct  EasingEquation_tCA0CBD0561A15202778424F90084F965713F1C82 
{
public:
	// System.Int32 EasingEquation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EasingEquation_tCA0CBD0561A15202778424F90084F965713F1C82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASINGEQUATION_TCA0CBD0561A15202778424F90084F965713F1C82_H
#ifndef MATTRESSANIMATION_T43B6DA74DA83A07619E3174B2DC51AD84C3F259B_H
#define MATTRESSANIMATION_T43B6DA74DA83A07619E3174B2DC51AD84C3F259B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MattressUIController_MattressAnimation
struct  MattressAnimation_t43B6DA74DA83A07619E3174B2DC51AD84C3F259B 
{
public:
	// System.Int32 MattressUIController_MattressAnimation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MattressAnimation_t43B6DA74DA83A07619E3174B2DC51AD84C3F259B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATTRESSANIMATION_T43B6DA74DA83A07619E3174B2DC51AD84C3F259B_H
#ifndef MODELTARGET_TF1115D7753DC988A2537D2F4ECE4D584A186B6D1_H
#define MODELTARGET_TF1115D7753DC988A2537D2F4ECE4D584A186B6D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelTargetController_ModelTarget
struct  ModelTarget_tF1115D7753DC988A2537D2F4ECE4D584A186B6D1 
{
public:
	// System.Byte ModelTargetController_ModelTarget::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModelTarget_tF1115D7753DC988A2537D2F4ECE4D584A186B6D1, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELTARGET_TF1115D7753DC988A2537D2F4ECE4D584A186B6D1_H
#ifndef MODELTARGET_T6C8F9568FC79DFAD1E368E2ED61C817E12004274_H
#define MODELTARGET_T6C8F9568FC79DFAD1E368E2ED61C817E12004274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelTargetMenuController_ModelTarget
struct  ModelTarget_t6C8F9568FC79DFAD1E368E2ED61C817E12004274 
{
public:
	// System.Byte ModelTargetMenuController_ModelTarget::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModelTarget_t6C8F9568FC79DFAD1E368E2ED61C817E12004274, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELTARGET_T6C8F9568FC79DFAD1E368E2ED61C817E12004274_H
#ifndef TOGGLELEVEL_TCA87FAE3CA87EF43267012272682E9356CDAE806_H
#define TOGGLELEVEL_TCA87FAE3CA87EF43267012272682E9356CDAE806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingPanelController_ToggleLevel
struct  ToggleLevel_tCA87FAE3CA87EF43267012272682E9356CDAE806 
{
public:
	// System.Int32 SettingPanelController_ToggleLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToggleLevel_tCA87FAE3CA87EF43267012272682E9356CDAE806, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLELEVEL_TCA87FAE3CA87EF43267012272682E9356CDAE806_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef ANIMATION_DATA_TYPE_TDD2F3FCCB7592775968BB2ECF653025B830D831C_H
#define ANIMATION_DATA_TYPE_TDD2F3FCCB7592775968BB2ECF653025B830D831C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ANIMATION_DATA_TYPE
struct  ANIMATION_DATA_TYPE_tDD2F3FCCB7592775968BB2ECF653025B830D831C 
{
public:
	// System.Int32 TextFx.ANIMATION_DATA_TYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ANIMATION_DATA_TYPE_tDD2F3FCCB7592775968BB2ECF653025B830D831C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_DATA_TYPE_TDD2F3FCCB7592775968BB2ECF653025B830D831C_H
#ifndef PRESET_ANIMATION_SECTION_TBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D_H
#define PRESET_ANIMATION_SECTION_TBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxAnimationManager_PRESET_ANIMATION_SECTION
struct  PRESET_ANIMATION_SECTION_tBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D 
{
public:
	// System.Int32 TextFx.TextFxAnimationManager_PRESET_ANIMATION_SECTION::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PRESET_ANIMATION_SECTION_tBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESET_ANIMATION_SECTION_TBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D_H
#ifndef U3CDOINTROU3ED__36_TA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE_H
#define U3CDOINTROU3ED__36_TA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFxDemoManager_<DoIntro>d__36
struct  U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE  : public RuntimeObject
{
public:
	// System.Int32 TextFxDemoManager_<DoIntro>d__36::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TextFxDemoManager_<DoIntro>d__36::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TextFxDemoManager TextFxDemoManager_<DoIntro>d__36::<>4__this
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * ___U3CU3E4__this_2;
	// System.Boolean TextFxDemoManager_<DoIntro>d__36::skipTitleEffect
	bool ___skipTitleEffect_3;
	// System.Single TextFxDemoManager_<DoIntro>d__36::waitTime
	float ___waitTime_4;
	// System.Single TextFxDemoManager_<DoIntro>d__36::<timer>5__2
	float ___U3CtimerU3E5__2_5;
	// UnityEngine.Quaternion TextFxDemoManager_<DoIntro>d__36::<rotation>5__3
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3CrotationU3E5__3_6;
	// UnityEngine.Vector3 TextFxDemoManager_<DoIntro>d__36::<rotationVec>5__4
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CrotationVecU3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE, ___U3CU3E4__this_2)); }
	inline TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_skipTitleEffect_3() { return static_cast<int32_t>(offsetof(U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE, ___skipTitleEffect_3)); }
	inline bool get_skipTitleEffect_3() const { return ___skipTitleEffect_3; }
	inline bool* get_address_of_skipTitleEffect_3() { return &___skipTitleEffect_3; }
	inline void set_skipTitleEffect_3(bool value)
	{
		___skipTitleEffect_3 = value;
	}

	inline static int32_t get_offset_of_waitTime_4() { return static_cast<int32_t>(offsetof(U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE, ___waitTime_4)); }
	inline float get_waitTime_4() const { return ___waitTime_4; }
	inline float* get_address_of_waitTime_4() { return &___waitTime_4; }
	inline void set_waitTime_4(float value)
	{
		___waitTime_4 = value;
	}

	inline static int32_t get_offset_of_U3CtimerU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE, ___U3CtimerU3E5__2_5)); }
	inline float get_U3CtimerU3E5__2_5() const { return ___U3CtimerU3E5__2_5; }
	inline float* get_address_of_U3CtimerU3E5__2_5() { return &___U3CtimerU3E5__2_5; }
	inline void set_U3CtimerU3E5__2_5(float value)
	{
		___U3CtimerU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CrotationU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE, ___U3CrotationU3E5__3_6)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_U3CrotationU3E5__3_6() const { return ___U3CrotationU3E5__3_6; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_U3CrotationU3E5__3_6() { return &___U3CrotationU3E5__3_6; }
	inline void set_U3CrotationU3E5__3_6(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___U3CrotationU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CrotationVecU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE, ___U3CrotationVecU3E5__4_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CrotationVecU3E5__4_7() const { return ___U3CrotationVecU3E5__4_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CrotationVecU3E5__4_7() { return &___U3CrotationVecU3E5__4_7; }
	inline void set_U3CrotationVecU3E5__4_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CrotationVecU3E5__4_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOINTROU3ED__36_TA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE_H
#ifndef TEXTFXTEXTANCHOR_T2BA9F20AF53C6DC0F23CEFDE9802E9F886BF359C_H
#define TEXTFXTEXTANCHOR_T2BA9F20AF53C6DC0F23CEFDE9802E9F886BF359C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextfxTextAnchor
struct  TextfxTextAnchor_t2BA9F20AF53C6DC0F23CEFDE9802E9F886BF359C 
{
public:
	// System.Int32 TextfxTextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextfxTextAnchor_t2BA9F20AF53C6DC0F23CEFDE9802E9F886BF359C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXTEXTANCHOR_T2BA9F20AF53C6DC0F23CEFDE9802E9F886BF359C_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef MOVEMENTTYPE_T78F2436465C40CA3C70631E1E5F088EA7A15C97A_H
#define MOVEMENTTYPE_T78F2436465C40CA3C70631E1E5F088EA7A15C97A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect_MovementType
struct  MovementType_t78F2436465C40CA3C70631E1E5F088EA7A15C97A 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect_MovementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementType_t78F2436465C40CA3C70631E1E5F088EA7A15C97A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T78F2436465C40CA3C70631E1E5F088EA7A15C97A_H
#ifndef SCROLLBARVISIBILITY_T4D6A5D8EF1681A91CED9F04283D0C882DCE1531F_H
#define SCROLLBARVISIBILITY_T4D6A5D8EF1681A91CED9F04283D0C882DCE1531F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect_ScrollbarVisibility
struct  ScrollbarVisibility_t4D6A5D8EF1681A91CED9F04283D0C882DCE1531F 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect_ScrollbarVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t4D6A5D8EF1681A91CED9F04283D0C882DCE1531F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBARVISIBILITY_T4D6A5D8EF1681A91CED9F04283D0C882DCE1531F_H
#ifndef STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#define STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour_Status
struct  Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifndef U3CPLAYANIMATIONU3ED__24_T53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B_H
#define U3CPLAYANIMATIONU3ED__24_T53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARUIController_<PlayAnimation>d__24
struct  U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B  : public RuntimeObject
{
public:
	// System.Int32 ARUIController_<PlayAnimation>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ARUIController_<PlayAnimation>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ARUIController ARUIController_<PlayAnimation>d__24::<>4__this
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1 * ___U3CU3E4__this_2;
	// ARUIController_MattressAnimation ARUIController_<PlayAnimation>d__24::aniNo
	int32_t ___aniNo_3;
	// UnityEngine.Vector3 ARUIController_<PlayAnimation>d__24::<position>5__2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CpositionU3E5__2_4;
	// UnityEngine.ResourceRequest ARUIController_<PlayAnimation>d__24::<loadAsync>5__3
	ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 * ___U3CloadAsyncU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B, ___U3CU3E4__this_2)); }
	inline ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_aniNo_3() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B, ___aniNo_3)); }
	inline int32_t get_aniNo_3() const { return ___aniNo_3; }
	inline int32_t* get_address_of_aniNo_3() { return &___aniNo_3; }
	inline void set_aniNo_3(int32_t value)
	{
		___aniNo_3 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B, ___U3CpositionU3E5__2_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CpositionU3E5__2_4() const { return ___U3CpositionU3E5__2_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CpositionU3E5__2_4() { return &___U3CpositionU3E5__2_4; }
	inline void set_U3CpositionU3E5__2_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CpositionU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CloadAsyncU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B, ___U3CloadAsyncU3E5__3_5)); }
	inline ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 * get_U3CloadAsyncU3E5__3_5() const { return ___U3CloadAsyncU3E5__3_5; }
	inline ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 ** get_address_of_U3CloadAsyncU3E5__3_5() { return &___U3CloadAsyncU3E5__3_5; }
	inline void set_U3CloadAsyncU3E5__3_5(ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 * value)
	{
		___U3CloadAsyncU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadAsyncU3E5__3_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYANIMATIONU3ED__24_T53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B_H
#ifndef U3CPLAYANIMATIONU3ED__18_T7AC85838E12FB0A91A76AC4971F61B2E25F644F4_H
#define U3CPLAYANIMATIONU3ED__18_T7AC85838E12FB0A91A76AC4971F61B2E25F644F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MattressUIController_<PlayAnimation>d__18
struct  U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4  : public RuntimeObject
{
public:
	// System.Int32 MattressUIController_<PlayAnimation>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MattressUIController_<PlayAnimation>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MattressUIController MattressUIController_<PlayAnimation>d__18::<>4__this
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191 * ___U3CU3E4__this_2;
	// MattressUIController_MattressAnimation MattressUIController_<PlayAnimation>d__18::aniNo
	int32_t ___aniNo_3;
	// UnityEngine.Vector3 MattressUIController_<PlayAnimation>d__18::<position>5__2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CpositionU3E5__2_4;
	// UnityEngine.ResourceRequest MattressUIController_<PlayAnimation>d__18::<loadAsync>5__3
	ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 * ___U3CloadAsyncU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4, ___U3CU3E4__this_2)); }
	inline MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_aniNo_3() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4, ___aniNo_3)); }
	inline int32_t get_aniNo_3() const { return ___aniNo_3; }
	inline int32_t* get_address_of_aniNo_3() { return &___aniNo_3; }
	inline void set_aniNo_3(int32_t value)
	{
		___aniNo_3 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4, ___U3CpositionU3E5__2_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CpositionU3E5__2_4() const { return ___U3CpositionU3E5__2_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CpositionU3E5__2_4() { return &___U3CpositionU3E5__2_4; }
	inline void set_U3CpositionU3E5__2_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CpositionU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CloadAsyncU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4, ___U3CloadAsyncU3E5__3_5)); }
	inline ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 * get_U3CloadAsyncU3E5__3_5() const { return ___U3CloadAsyncU3E5__3_5; }
	inline ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 ** get_address_of_U3CloadAsyncU3E5__3_5() { return &___U3CloadAsyncU3E5__3_5; }
	inline void set_U3CloadAsyncU3E5__3_5(ResourceRequest_t22744D420D4DEF7C924A01EB117C0FEC6B07D486 * value)
	{
		___U3CloadAsyncU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadAsyncU3E5__3_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYANIMATIONU3ED__18_T7AC85838E12FB0A91A76AC4971F61B2E25F644F4_H
#ifndef MOBILEBASE_TA53736A507DA935FDC5873236F32953E0B2F886A_H
#define MOBILEBASE_TA53736A507DA935FDC5873236F32953E0B2F886A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobileBase
struct  MobileBase_tA53736A507DA935FDC5873236F32953E0B2F886A  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEBASE_TA53736A507DA935FDC5873236F32953E0B2F886A_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_TCC418BF00BC4442CCA5F5EF613B8BA9EC9D04296_H
#define U3CU3EC__DISPLAYCLASS35_0_TCC418BF00BC4442CCA5F5EF613B8BA9EC9D04296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFxDemoManager_<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_tCC418BF00BC4442CCA5F5EF613B8BA9EC9D04296  : public RuntimeObject
{
public:
	// TextFx.TextFxAnimationManager_PRESET_ANIMATION_SECTION TextFxDemoManager_<>c__DisplayClass35_0::animSection
	int32_t ___animSection_0;
	// TextFxDemoManager TextFxDemoManager_<>c__DisplayClass35_0::<>4__this
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_animSection_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tCC418BF00BC4442CCA5F5EF613B8BA9EC9D04296, ___animSection_0)); }
	inline int32_t get_animSection_0() const { return ___animSection_0; }
	inline int32_t* get_address_of_animSection_0() { return &___animSection_0; }
	inline void set_animSection_0(int32_t value)
	{
		___animSection_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tCC418BF00BC4442CCA5F5EF613B8BA9EC9D04296, ___U3CU3E4__this_1)); }
	inline TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_TCC418BF00BC4442CCA5F5EF613B8BA9EC9D04296_H
#ifndef U3CU3EC__DISPLAYCLASS46_1_TA6570BCFA826B4804D08290D1CA0F8E1C2594054_H
#define U3CU3EC__DISPLAYCLASS46_1_TA6570BCFA826B4804D08290D1CA0F8E1C2594054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFxDemoManager_<>c__DisplayClass46_1
struct  U3CU3Ec__DisplayClass46_1_tA6570BCFA826B4804D08290D1CA0F8E1C2594054  : public RuntimeObject
{
public:
	// TextFx.ANIMATION_DATA_TYPE TextFxDemoManager_<>c__DisplayClass46_1::dataType
	int32_t ___dataType_0;
	// TextFx.ANIMATION_DATA_TYPE TextFxDemoManager_<>c__DisplayClass46_1::vecDataType
	int32_t ___vecDataType_1;
	// TextFx.ANIMATION_DATA_TYPE TextFxDemoManager_<>c__DisplayClass46_1::colDataType
	int32_t ___colDataType_2;
	// TextFxDemoManager_<>c__DisplayClass46_0 TextFxDemoManager_<>c__DisplayClass46_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E * ___CSU24U3CU3E8__locals1_3;

public:
	inline static int32_t get_offset_of_dataType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_1_tA6570BCFA826B4804D08290D1CA0F8E1C2594054, ___dataType_0)); }
	inline int32_t get_dataType_0() const { return ___dataType_0; }
	inline int32_t* get_address_of_dataType_0() { return &___dataType_0; }
	inline void set_dataType_0(int32_t value)
	{
		___dataType_0 = value;
	}

	inline static int32_t get_offset_of_vecDataType_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_1_tA6570BCFA826B4804D08290D1CA0F8E1C2594054, ___vecDataType_1)); }
	inline int32_t get_vecDataType_1() const { return ___vecDataType_1; }
	inline int32_t* get_address_of_vecDataType_1() { return &___vecDataType_1; }
	inline void set_vecDataType_1(int32_t value)
	{
		___vecDataType_1 = value;
	}

	inline static int32_t get_offset_of_colDataType_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_1_tA6570BCFA826B4804D08290D1CA0F8E1C2594054, ___colDataType_2)); }
	inline int32_t get_colDataType_2() const { return ___colDataType_2; }
	inline int32_t* get_address_of_colDataType_2() { return &___colDataType_2; }
	inline void set_colDataType_2(int32_t value)
	{
		___colDataType_2 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_1_tA6570BCFA826B4804D08290D1CA0F8E1C2594054, ___CSU24U3CU3E8__locals1_3)); }
	inline U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E * get_CSU24U3CU3E8__locals1_3() const { return ___CSU24U3CU3E8__locals1_3; }
	inline U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E ** get_address_of_CSU24U3CU3E8__locals1_3() { return &___CSU24U3CU3E8__locals1_3; }
	inline void set_CSU24U3CU3E8__locals1_3(U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E * value)
	{
		___CSU24U3CU3E8__locals1_3 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS46_1_TA6570BCFA826B4804D08290D1CA0F8E1C2594054_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef MOBILEANDROID_T731945698A11BE5075D526156BDB071A99BD9688_H
#define MOBILEANDROID_T731945698A11BE5075D526156BDB071A99BD9688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobileAndroid
struct  MobileAndroid_t731945698A11BE5075D526156BDB071A99BD9688  : public MobileBase_tA53736A507DA935FDC5873236F32953E0B2F886A
{
public:
	// UnityEngine.AndroidJavaObject MobileAndroid::UnityPlayerActivity
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___UnityPlayerActivity_4;

public:
	inline static int32_t get_offset_of_UnityPlayerActivity_4() { return static_cast<int32_t>(offsetof(MobileAndroid_t731945698A11BE5075D526156BDB071A99BD9688, ___UnityPlayerActivity_4)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_UnityPlayerActivity_4() const { return ___UnityPlayerActivity_4; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_UnityPlayerActivity_4() { return &___UnityPlayerActivity_4; }
	inline void set_UnityPlayerActivity_4(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___UnityPlayerActivity_4 = value;
		Il2CppCodeGenWriteBarrier((&___UnityPlayerActivity_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEANDROID_T731945698A11BE5075D526156BDB071A99BD9688_H
#ifndef MOBILEIOS_T765B18F4DA004AB004993862B663DA65E57BF62A_H
#define MOBILEIOS_T765B18F4DA004AB004993862B663DA65E57BF62A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobileIOS
struct  MobileIOS_t765B18F4DA004AB004993862B663DA65E57BF62A  : public MobileBase_tA53736A507DA935FDC5873236F32953E0B2F886A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEIOS_T765B18F4DA004AB004993862B663DA65E57BF62A_H
#ifndef CALLBACK_T5433D5269B1108D8BFECDCD02079FCB1211A6CF8_H
#define CALLBACK_T5433D5269B1108D8BFECDCD02079FCB1211A6CF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModalController_Callback
struct  Callback_t5433D5269B1108D8BFECDCD02079FCB1211A6CF8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACK_T5433D5269B1108D8BFECDCD02079FCB1211A6CF8_H
#ifndef CALLBACKCLOSE_T4CBEE1DC735AF36C5814923F4360DBB1949CD115_H
#define CALLBACKCLOSE_T4CBEE1DC735AF36C5814923F4360DBB1949CD115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModalController_CallbackClose
struct  CallbackClose_t4CBEE1DC735AF36C5814923F4360DBB1949CD115  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKCLOSE_T4CBEE1DC735AF36C5814923F4360DBB1949CD115_H
#ifndef CALLBACKPOP_T11746E625CF12C9917AD4DB2B9A57E2597D32609_H
#define CALLBACKPOP_T11746E625CF12C9917AD4DB2B9A57E2597D32609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModalController_CallbackPop
struct  CallbackPop_t11746E625CF12C9917AD4DB2B9A57E2597D32609  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKPOP_T11746E625CF12C9917AD4DB2B9A57E2597D32609_H
#ifndef CALLBACKTYPE_TE5CC0E106E4AF1B685B5B9940F689A2464C63C41_H
#define CALLBACKTYPE_TE5CC0E106E4AF1B685B5B9940F689A2464C63C41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModalController_CallbackType
struct  CallbackType_tE5CC0E106E4AF1B685B5B9940F689A2464C63C41  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKTYPE_TE5CC0E106E4AF1B685B5B9940F689A2464C63C41_H
#ifndef PARSEFUNCTION_TC04C029BB8EC10B09DE25A5B0AD388B75C177EA6_H
#define PARSEFUNCTION_TC04C029BB8EC10B09DE25A5B0AD388B75C177EA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_ParseFunction
struct  ParseFunction_tC04C029BB8EC10B09DE25A5B0AD388B75C177EA6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSEFUNCTION_TC04C029BB8EC10B09DE25A5B0AD388B75C177EA6_H
#ifndef PARSEFUNCTIONWITHPAMETER_T43FB9510DA0A33033E4DA482AEACCCD943F8FC0A_H
#define PARSEFUNCTIONWITHPAMETER_T43FB9510DA0A33033E4DA482AEACCCD943F8FC0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController_ParseFunctionWithPameter
struct  ParseFunctionWithPameter_t43FB9510DA0A33033E4DA482AEACCCD943F8FC0A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSEFUNCTIONWITHPAMETER_T43FB9510DA0A33033E4DA482AEACCCD943F8FC0A_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ARCAMERACONTROLLER_TD9594B7DFD1FB5066CE4F4BBFA65504FD03CE832_H
#define ARCAMERACONTROLLER_TD9594B7DFD1FB5066CE4F4BBFA65504FD03CE832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARCameraController
struct  ARCameraController_tD9594B7DFD1FB5066CE4F4BBFA65504FD03CE832  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct ARCameraController_tD9594B7DFD1FB5066CE4F4BBFA65504FD03CE832_StaticFields
{
public:
	// System.Boolean ARCameraController::isInitVuforia
	bool ___isInitVuforia_4;

public:
	inline static int32_t get_offset_of_isInitVuforia_4() { return static_cast<int32_t>(offsetof(ARCameraController_tD9594B7DFD1FB5066CE4F4BBFA65504FD03CE832_StaticFields, ___isInitVuforia_4)); }
	inline bool get_isInitVuforia_4() const { return ___isInitVuforia_4; }
	inline bool* get_address_of_isInitVuforia_4() { return &___isInitVuforia_4; }
	inline void set_isInitVuforia_4(bool value)
	{
		___isInitVuforia_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCAMERACONTROLLER_TD9594B7DFD1FB5066CE4F4BBFA65504FD03CE832_H
#ifndef ACEANIMATIONEVENTLISTENER_TABD975A2D6CC5919672960472446DEE0D0F1F9EC_H
#define ACEANIMATIONEVENTLISTENER_TABD975A2D6CC5919672960472446DEE0D0F1F9EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceAnimationEventListener
struct  AceAnimationEventListener_tABD975A2D6CC5919672960472446DEE0D0F1F9EC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACEANIMATIONEVENTLISTENER_TABD975A2D6CC5919672960472446DEE0D0F1F9EC_H
#ifndef ACEANIMATIONHANDLER_T8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0_H
#define ACEANIMATIONHANDLER_T8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceAnimationHandler
struct  AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer AceAnimationHandler::m_ObjectRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___m_ObjectRenderer_4;
	// System.Single AceAnimationHandler::fadePerSecond
	float ___fadePerSecond_5;

public:
	inline static int32_t get_offset_of_m_ObjectRenderer_4() { return static_cast<int32_t>(offsetof(AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0, ___m_ObjectRenderer_4)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_m_ObjectRenderer_4() const { return ___m_ObjectRenderer_4; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_m_ObjectRenderer_4() { return &___m_ObjectRenderer_4; }
	inline void set_m_ObjectRenderer_4(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___m_ObjectRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectRenderer_4), value);
	}

	inline static int32_t get_offset_of_fadePerSecond_5() { return static_cast<int32_t>(offsetof(AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0, ___fadePerSecond_5)); }
	inline float get_fadePerSecond_5() const { return ___fadePerSecond_5; }
	inline float* get_address_of_fadePerSecond_5() { return &___fadePerSecond_5; }
	inline void set_fadePerSecond_5(float value)
	{
		___fadePerSecond_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACEANIMATIONHANDLER_T8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0_H
#ifndef BASECONTROLLER_TAA16D17567994685DA8E4473CFF8D6445DA5179E_H
#define BASECONTROLLER_TAA16D17567994685DA8E4473CFF8D6445DA5179E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseController
struct  BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E_StaticFields
{
public:
	// BaseController_ACESceneMode BaseController::_sceneMode
	uint8_t ____sceneMode_4;
	// BaseController_ACESceneMode BaseController::_oldSceneMode
	uint8_t ____oldSceneMode_5;
	// MobileManager BaseController::mobileManager
	MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543 * ___mobileManager_6;
	// System.Boolean BaseController::isReloadScene
	bool ___isReloadScene_7;

public:
	inline static int32_t get_offset_of__sceneMode_4() { return static_cast<int32_t>(offsetof(BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E_StaticFields, ____sceneMode_4)); }
	inline uint8_t get__sceneMode_4() const { return ____sceneMode_4; }
	inline uint8_t* get_address_of__sceneMode_4() { return &____sceneMode_4; }
	inline void set__sceneMode_4(uint8_t value)
	{
		____sceneMode_4 = value;
	}

	inline static int32_t get_offset_of__oldSceneMode_5() { return static_cast<int32_t>(offsetof(BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E_StaticFields, ____oldSceneMode_5)); }
	inline uint8_t get__oldSceneMode_5() const { return ____oldSceneMode_5; }
	inline uint8_t* get_address_of__oldSceneMode_5() { return &____oldSceneMode_5; }
	inline void set__oldSceneMode_5(uint8_t value)
	{
		____oldSceneMode_5 = value;
	}

	inline static int32_t get_offset_of_mobileManager_6() { return static_cast<int32_t>(offsetof(BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E_StaticFields, ___mobileManager_6)); }
	inline MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543 * get_mobileManager_6() const { return ___mobileManager_6; }
	inline MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543 ** get_address_of_mobileManager_6() { return &___mobileManager_6; }
	inline void set_mobileManager_6(MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543 * value)
	{
		___mobileManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___mobileManager_6), value);
	}

	inline static int32_t get_offset_of_isReloadScene_7() { return static_cast<int32_t>(offsetof(BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E_StaticFields, ___isReloadScene_7)); }
	inline bool get_isReloadScene_7() const { return ___isReloadScene_7; }
	inline bool* get_address_of_isReloadScene_7() { return &___isReloadScene_7; }
	inline void set_isReloadScene_7(bool value)
	{
		___isReloadScene_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASECONTROLLER_TAA16D17567994685DA8E4473CFF8D6445DA5179E_H
#ifndef BASEVARIABLESETTING_T8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB_H
#define BASEVARIABLESETTING_T8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseVariableSetting
struct  BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Color BaseVariableSetting::SETTING_COLOUR
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SETTING_COLOUR_4;
	// UnityEngine.Color BaseVariableSetting::SUB_SETTING_COLOUR
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SUB_SETTING_COLOUR_5;
	// UnityEngine.UI.Text BaseVariableSetting::m_labelText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_labelText_6;
	// UnityEngine.UI.Image BaseVariableSetting::m_bgImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_bgImage_7;
	// System.Boolean BaseVariableSetting::m_subSetting
	bool ___m_subSetting_8;
	// System.Boolean BaseVariableSetting::m_subSettingActive
	bool ___m_subSettingActive_9;

public:
	inline static int32_t get_offset_of_SETTING_COLOUR_4() { return static_cast<int32_t>(offsetof(BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB, ___SETTING_COLOUR_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SETTING_COLOUR_4() const { return ___SETTING_COLOUR_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SETTING_COLOUR_4() { return &___SETTING_COLOUR_4; }
	inline void set_SETTING_COLOUR_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SETTING_COLOUR_4 = value;
	}

	inline static int32_t get_offset_of_SUB_SETTING_COLOUR_5() { return static_cast<int32_t>(offsetof(BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB, ___SUB_SETTING_COLOUR_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SUB_SETTING_COLOUR_5() const { return ___SUB_SETTING_COLOUR_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SUB_SETTING_COLOUR_5() { return &___SUB_SETTING_COLOUR_5; }
	inline void set_SUB_SETTING_COLOUR_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SUB_SETTING_COLOUR_5 = value;
	}

	inline static int32_t get_offset_of_m_labelText_6() { return static_cast<int32_t>(offsetof(BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB, ___m_labelText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_labelText_6() const { return ___m_labelText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_labelText_6() { return &___m_labelText_6; }
	inline void set_m_labelText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_labelText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_labelText_6), value);
	}

	inline static int32_t get_offset_of_m_bgImage_7() { return static_cast<int32_t>(offsetof(BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB, ___m_bgImage_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_bgImage_7() const { return ___m_bgImage_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_bgImage_7() { return &___m_bgImage_7; }
	inline void set_m_bgImage_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_bgImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_bgImage_7), value);
	}

	inline static int32_t get_offset_of_m_subSetting_8() { return static_cast<int32_t>(offsetof(BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB, ___m_subSetting_8)); }
	inline bool get_m_subSetting_8() const { return ___m_subSetting_8; }
	inline bool* get_address_of_m_subSetting_8() { return &___m_subSetting_8; }
	inline void set_m_subSetting_8(bool value)
	{
		___m_subSetting_8 = value;
	}

	inline static int32_t get_offset_of_m_subSettingActive_9() { return static_cast<int32_t>(offsetof(BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB, ___m_subSettingActive_9)); }
	inline bool get_m_subSettingActive_9() const { return ___m_subSettingActive_9; }
	inline bool* get_address_of_m_subSettingActive_9() { return &___m_subSettingActive_9; }
	inline void set_m_subSettingActive_9(bool value)
	{
		___m_subSettingActive_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVARIABLESETTING_T8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB_H
#ifndef SINGLETON_1_T4FD9E70C85FC3861708122924623B217140B5723_H
#define SINGLETON_1_T4FD9E70C85FC3861708122924623B217140B5723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.Singleton`1<ImportCoreDLL>
struct  Singleton_1_t4FD9E70C85FC3861708122924623B217140B5723  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t4FD9E70C85FC3861708122924623B217140B5723_StaticFields
{
public:
	// T DLLCore.Singleton`1::a
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692 * ___a_4;
	// System.Object DLLCore.Singleton`1::b
	RuntimeObject * ___b_5;
	// System.Boolean DLLCore.Singleton`1::c
	bool ___c_6;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Singleton_1_t4FD9E70C85FC3861708122924623B217140B5723_StaticFields, ___a_4)); }
	inline ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692 * get_a_4() const { return ___a_4; }
	inline ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Singleton_1_t4FD9E70C85FC3861708122924623B217140B5723_StaticFields, ___b_5)); }
	inline RuntimeObject * get_b_5() const { return ___b_5; }
	inline RuntimeObject ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(RuntimeObject * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Singleton_1_t4FD9E70C85FC3861708122924623B217140B5723_StaticFields, ___c_6)); }
	inline bool get_c_6() const { return ___c_6; }
	inline bool* get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(bool value)
	{
		___c_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T4FD9E70C85FC3861708122924623B217140B5723_H
#ifndef SINGLETON_1_TCE7C077749BEBC40D136084465AAA2F04D1A3DA5_H
#define SINGLETON_1_TCE7C077749BEBC40D136084465AAA2F04D1A3DA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.Singleton`1<UISingleton>
struct  Singleton_1_tCE7C077749BEBC40D136084465AAA2F04D1A3DA5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tCE7C077749BEBC40D136084465AAA2F04D1A3DA5_StaticFields
{
public:
	// T DLLCore.Singleton`1::a
	UISingleton_t4848BA88F4BA51DBD493AEB09C803CA5D8B5D051 * ___a_4;
	// System.Object DLLCore.Singleton`1::b
	RuntimeObject * ___b_5;
	// System.Boolean DLLCore.Singleton`1::c
	bool ___c_6;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Singleton_1_tCE7C077749BEBC40D136084465AAA2F04D1A3DA5_StaticFields, ___a_4)); }
	inline UISingleton_t4848BA88F4BA51DBD493AEB09C803CA5D8B5D051 * get_a_4() const { return ___a_4; }
	inline UISingleton_t4848BA88F4BA51DBD493AEB09C803CA5D8B5D051 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(UISingleton_t4848BA88F4BA51DBD493AEB09C803CA5D8B5D051 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Singleton_1_tCE7C077749BEBC40D136084465AAA2F04D1A3DA5_StaticFields, ___b_5)); }
	inline RuntimeObject * get_b_5() const { return ___b_5; }
	inline RuntimeObject ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(RuntimeObject * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Singleton_1_tCE7C077749BEBC40D136084465AAA2F04D1A3DA5_StaticFields, ___c_6)); }
	inline bool get_c_6() const { return ___c_6; }
	inline bool* get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(bool value)
	{
		___c_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TCE7C077749BEBC40D136084465AAA2F04D1A3DA5_H
#ifndef GNBCONTROLLER_T2BCB4DA5E1243B852370C0779679F133FC062CF0_H
#define GNBCONTROLLER_T2BCB4DA5E1243B852370C0779679F133FC062CF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GNBController
struct  GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button GNBController::map
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___map_4;
	// UnityEngine.UI.Button GNBController::style
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___style_5;
	// UnityEngine.UI.Button GNBController::design
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___design_6;
	// UnityEngine.UI.Button GNBController::help
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___help_7;
	// UnityEngine.GameObject GNBController::mapView
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mapView_8;

public:
	inline static int32_t get_offset_of_map_4() { return static_cast<int32_t>(offsetof(GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0, ___map_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_map_4() const { return ___map_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_map_4() { return &___map_4; }
	inline void set_map_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___map_4 = value;
		Il2CppCodeGenWriteBarrier((&___map_4), value);
	}

	inline static int32_t get_offset_of_style_5() { return static_cast<int32_t>(offsetof(GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0, ___style_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_style_5() const { return ___style_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_style_5() { return &___style_5; }
	inline void set_style_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___style_5 = value;
		Il2CppCodeGenWriteBarrier((&___style_5), value);
	}

	inline static int32_t get_offset_of_design_6() { return static_cast<int32_t>(offsetof(GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0, ___design_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_design_6() const { return ___design_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_design_6() { return &___design_6; }
	inline void set_design_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___design_6 = value;
		Il2CppCodeGenWriteBarrier((&___design_6), value);
	}

	inline static int32_t get_offset_of_help_7() { return static_cast<int32_t>(offsetof(GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0, ___help_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_help_7() const { return ___help_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_help_7() { return &___help_7; }
	inline void set_help_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___help_7 = value;
		Il2CppCodeGenWriteBarrier((&___help_7), value);
	}

	inline static int32_t get_offset_of_mapView_8() { return static_cast<int32_t>(offsetof(GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0, ___mapView_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mapView_8() const { return ___mapView_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mapView_8() { return &___mapView_8; }
	inline void set_mapView_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mapView_8 = value;
		Il2CppCodeGenWriteBarrier((&___mapView_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GNBCONTROLLER_T2BCB4DA5E1243B852370C0779679F133FC062CF0_H
#ifndef HIDESYSTEMUI_T4D36167744172806784B6EC2B2CC36B81B004D0E_H
#define HIDESYSTEMUI_T4D36167744172806784B6EC2B2CC36B81B004D0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HideSystemUI
struct  HideSystemUI_t4D36167744172806784B6EC2B2CC36B81B004D0E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDESYSTEMUI_T4D36167744172806784B6EC2B2CC36B81B004D0E_H
#ifndef HSVSLIDERPICKER_TC67F8542E7009A8F93936EC8468CE1934AE24DEB_H
#define HSVSLIDERPICKER_TC67F8542E7009A8F93936EC8468CE1934AE24DEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HsvSliderPicker
struct  HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HSVPickerDemo.HSVPicker HsvSliderPicker::picker
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * ___picker_4;
	// UnityEngine.UI.Slider HsvSliderPicker::slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___slider_5;

public:
	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB, ___picker_4)); }
	inline HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * get_picker_4() const { return ___picker_4; }
	inline HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier((&___picker_4), value);
	}

	inline static int32_t get_offset_of_slider_5() { return static_cast<int32_t>(offsetof(HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB, ___slider_5)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_slider_5() const { return ___slider_5; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_slider_5() { return &___slider_5; }
	inline void set_slider_5(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___slider_5 = value;
		Il2CppCodeGenWriteBarrier((&___slider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVSLIDERPICKER_TC67F8542E7009A8F93936EC8468CE1934AE24DEB_H
#ifndef MATH3D_TD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65_H
#define MATH3D_TD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Math3d
struct  Math3d_tD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Math3d_tD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65_StaticFields
{
public:
	// UnityEngine.Transform Math3d::tempChild
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___tempChild_4;
	// UnityEngine.Transform Math3d::tempParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___tempParent_5;

public:
	inline static int32_t get_offset_of_tempChild_4() { return static_cast<int32_t>(offsetof(Math3d_tD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65_StaticFields, ___tempChild_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_tempChild_4() const { return ___tempChild_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_tempChild_4() { return &___tempChild_4; }
	inline void set_tempChild_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___tempChild_4 = value;
		Il2CppCodeGenWriteBarrier((&___tempChild_4), value);
	}

	inline static int32_t get_offset_of_tempParent_5() { return static_cast<int32_t>(offsetof(Math3d_tD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65_StaticFields, ___tempParent_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_tempParent_5() const { return ___tempParent_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_tempParent_5() { return &___tempParent_5; }
	inline void set_tempParent_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___tempParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___tempParent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATH3D_TD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65_H
#ifndef MICHANDLE_T489A8D31AD74830D6936CF0463843049A2C8CBF9_H
#define MICHANDLE_T489A8D31AD74830D6936CF0463843049A2C8CBF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MicHandle
struct  MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text MicHandle::resultDisplay
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___resultDisplay_9;
	// UnityEngine.UI.Text MicHandle::blowDisplay
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___blowDisplay_10;
	// System.Int32 MicHandle::recordedLength
	int32_t ___recordedLength_11;
	// System.Int32 MicHandle::requiedBlowTime
	int32_t ___requiedBlowTime_12;
	// System.Int32 MicHandle::clamp
	int32_t ___clamp_13;
	// System.Single MicHandle::rmsValue
	float ___rmsValue_14;
	// System.Single MicHandle::dbValue
	float ___dbValue_15;
	// System.Single MicHandle::pitchValue
	float ___pitchValue_16;
	// System.Int32 MicHandle::blowingTime
	int32_t ___blowingTime_17;
	// System.Single MicHandle::lowPassResults
	float ___lowPassResults_18;
	// System.Single MicHandle::peakPowerForChannel
	float ___peakPowerForChannel_19;
	// System.Single[] MicHandle::samples
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___samples_20;
	// System.Single[] MicHandle::spectrum
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___spectrum_21;
	// System.Collections.Generic.List`1<System.Single> MicHandle::dbValues
	List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * ___dbValues_22;
	// System.Collections.Generic.List`1<System.Single> MicHandle::pitchValues
	List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * ___pitchValues_23;
	// UnityEngine.AudioSource MicHandle::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_24;
	// System.Single MicHandle::sensitivity
	float ___sensitivity_25;
	// System.Single MicHandle::loudness
	float ___loudness_26;
	// UnityEngine.AudioSource MicHandle::_audio
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ____audio_27;

public:
	inline static int32_t get_offset_of_resultDisplay_9() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___resultDisplay_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_resultDisplay_9() const { return ___resultDisplay_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_resultDisplay_9() { return &___resultDisplay_9; }
	inline void set_resultDisplay_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___resultDisplay_9 = value;
		Il2CppCodeGenWriteBarrier((&___resultDisplay_9), value);
	}

	inline static int32_t get_offset_of_blowDisplay_10() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___blowDisplay_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_blowDisplay_10() const { return ___blowDisplay_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_blowDisplay_10() { return &___blowDisplay_10; }
	inline void set_blowDisplay_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___blowDisplay_10 = value;
		Il2CppCodeGenWriteBarrier((&___blowDisplay_10), value);
	}

	inline static int32_t get_offset_of_recordedLength_11() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___recordedLength_11)); }
	inline int32_t get_recordedLength_11() const { return ___recordedLength_11; }
	inline int32_t* get_address_of_recordedLength_11() { return &___recordedLength_11; }
	inline void set_recordedLength_11(int32_t value)
	{
		___recordedLength_11 = value;
	}

	inline static int32_t get_offset_of_requiedBlowTime_12() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___requiedBlowTime_12)); }
	inline int32_t get_requiedBlowTime_12() const { return ___requiedBlowTime_12; }
	inline int32_t* get_address_of_requiedBlowTime_12() { return &___requiedBlowTime_12; }
	inline void set_requiedBlowTime_12(int32_t value)
	{
		___requiedBlowTime_12 = value;
	}

	inline static int32_t get_offset_of_clamp_13() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___clamp_13)); }
	inline int32_t get_clamp_13() const { return ___clamp_13; }
	inline int32_t* get_address_of_clamp_13() { return &___clamp_13; }
	inline void set_clamp_13(int32_t value)
	{
		___clamp_13 = value;
	}

	inline static int32_t get_offset_of_rmsValue_14() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___rmsValue_14)); }
	inline float get_rmsValue_14() const { return ___rmsValue_14; }
	inline float* get_address_of_rmsValue_14() { return &___rmsValue_14; }
	inline void set_rmsValue_14(float value)
	{
		___rmsValue_14 = value;
	}

	inline static int32_t get_offset_of_dbValue_15() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___dbValue_15)); }
	inline float get_dbValue_15() const { return ___dbValue_15; }
	inline float* get_address_of_dbValue_15() { return &___dbValue_15; }
	inline void set_dbValue_15(float value)
	{
		___dbValue_15 = value;
	}

	inline static int32_t get_offset_of_pitchValue_16() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___pitchValue_16)); }
	inline float get_pitchValue_16() const { return ___pitchValue_16; }
	inline float* get_address_of_pitchValue_16() { return &___pitchValue_16; }
	inline void set_pitchValue_16(float value)
	{
		___pitchValue_16 = value;
	}

	inline static int32_t get_offset_of_blowingTime_17() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___blowingTime_17)); }
	inline int32_t get_blowingTime_17() const { return ___blowingTime_17; }
	inline int32_t* get_address_of_blowingTime_17() { return &___blowingTime_17; }
	inline void set_blowingTime_17(int32_t value)
	{
		___blowingTime_17 = value;
	}

	inline static int32_t get_offset_of_lowPassResults_18() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___lowPassResults_18)); }
	inline float get_lowPassResults_18() const { return ___lowPassResults_18; }
	inline float* get_address_of_lowPassResults_18() { return &___lowPassResults_18; }
	inline void set_lowPassResults_18(float value)
	{
		___lowPassResults_18 = value;
	}

	inline static int32_t get_offset_of_peakPowerForChannel_19() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___peakPowerForChannel_19)); }
	inline float get_peakPowerForChannel_19() const { return ___peakPowerForChannel_19; }
	inline float* get_address_of_peakPowerForChannel_19() { return &___peakPowerForChannel_19; }
	inline void set_peakPowerForChannel_19(float value)
	{
		___peakPowerForChannel_19 = value;
	}

	inline static int32_t get_offset_of_samples_20() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___samples_20)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_samples_20() const { return ___samples_20; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_samples_20() { return &___samples_20; }
	inline void set_samples_20(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___samples_20 = value;
		Il2CppCodeGenWriteBarrier((&___samples_20), value);
	}

	inline static int32_t get_offset_of_spectrum_21() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___spectrum_21)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_spectrum_21() const { return ___spectrum_21; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_spectrum_21() { return &___spectrum_21; }
	inline void set_spectrum_21(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___spectrum_21 = value;
		Il2CppCodeGenWriteBarrier((&___spectrum_21), value);
	}

	inline static int32_t get_offset_of_dbValues_22() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___dbValues_22)); }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * get_dbValues_22() const { return ___dbValues_22; }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 ** get_address_of_dbValues_22() { return &___dbValues_22; }
	inline void set_dbValues_22(List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * value)
	{
		___dbValues_22 = value;
		Il2CppCodeGenWriteBarrier((&___dbValues_22), value);
	}

	inline static int32_t get_offset_of_pitchValues_23() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___pitchValues_23)); }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * get_pitchValues_23() const { return ___pitchValues_23; }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 ** get_address_of_pitchValues_23() { return &___pitchValues_23; }
	inline void set_pitchValues_23(List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * value)
	{
		___pitchValues_23 = value;
		Il2CppCodeGenWriteBarrier((&___pitchValues_23), value);
	}

	inline static int32_t get_offset_of_audioSource_24() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___audioSource_24)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_24() const { return ___audioSource_24; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_24() { return &___audioSource_24; }
	inline void set_audioSource_24(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_24 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_24), value);
	}

	inline static int32_t get_offset_of_sensitivity_25() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___sensitivity_25)); }
	inline float get_sensitivity_25() const { return ___sensitivity_25; }
	inline float* get_address_of_sensitivity_25() { return &___sensitivity_25; }
	inline void set_sensitivity_25(float value)
	{
		___sensitivity_25 = value;
	}

	inline static int32_t get_offset_of_loudness_26() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ___loudness_26)); }
	inline float get_loudness_26() const { return ___loudness_26; }
	inline float* get_address_of_loudness_26() { return &___loudness_26; }
	inline void set_loudness_26(float value)
	{
		___loudness_26 = value;
	}

	inline static int32_t get_offset_of__audio_27() { return static_cast<int32_t>(offsetof(MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9, ____audio_27)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get__audio_27() const { return ____audio_27; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of__audio_27() { return &____audio_27; }
	inline void set__audio_27(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		____audio_27 = value;
		Il2CppCodeGenWriteBarrier((&____audio_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICHANDLE_T489A8D31AD74830D6936CF0463843049A2C8CBF9_H
#ifndef OBJECTCLIP2_TC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435_H
#define OBJECTCLIP2_TC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectClip2
struct  ObjectClip2_tC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer[] ObjectClip2::mesh
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* ___mesh_4;
	// UnityEngine.Material[] ObjectClip2::material
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___material_5;
	// UnityEngine.Color[] ObjectClip2::col
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___col_6;
	// System.Single ObjectClip2::alpha
	float ___alpha_7;

public:
	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(ObjectClip2_tC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435, ___mesh_4)); }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* get_mesh_4() const { return ___mesh_4; }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(ObjectClip2_tC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435, ___material_5)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_material_5() const { return ___material_5; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_col_6() { return static_cast<int32_t>(offsetof(ObjectClip2_tC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435, ___col_6)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_col_6() const { return ___col_6; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_col_6() { return &___col_6; }
	inline void set_col_6(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___col_6 = value;
		Il2CppCodeGenWriteBarrier((&___col_6), value);
	}

	inline static int32_t get_offset_of_alpha_7() { return static_cast<int32_t>(offsetof(ObjectClip2_tC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435, ___alpha_7)); }
	inline float get_alpha_7() const { return ___alpha_7; }
	inline float* get_address_of_alpha_7() { return &___alpha_7; }
	inline void set_alpha_7(float value)
	{
		___alpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCLIP2_TC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435_H
#ifndef PRODUCTLISTBUTTON_T106B62A2104F017BE699A92DD9A2D55CC500E618_H
#define PRODUCTLISTBUTTON_T106B62A2104F017BE699A92DD9A2D55CC500E618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductListButton
struct  ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button ProductListButton::prefabButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___prefabButton_4;
	// System.Collections.Generic.List`1<ProductListButton_ProductData> ProductListButton::productListButtons
	List_1_tA01AB2108BB74F785875888713CA297D47449BB0 * ___productListButtons_5;

public:
	inline static int32_t get_offset_of_prefabButton_4() { return static_cast<int32_t>(offsetof(ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618, ___prefabButton_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_prefabButton_4() const { return ___prefabButton_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_prefabButton_4() { return &___prefabButton_4; }
	inline void set_prefabButton_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___prefabButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefabButton_4), value);
	}

	inline static int32_t get_offset_of_productListButtons_5() { return static_cast<int32_t>(offsetof(ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618, ___productListButtons_5)); }
	inline List_1_tA01AB2108BB74F785875888713CA297D47449BB0 * get_productListButtons_5() const { return ___productListButtons_5; }
	inline List_1_tA01AB2108BB74F785875888713CA297D47449BB0 ** get_address_of_productListButtons_5() { return &___productListButtons_5; }
	inline void set_productListButtons_5(List_1_tA01AB2108BB74F785875888713CA297D47449BB0 * value)
	{
		___productListButtons_5 = value;
		Il2CppCodeGenWriteBarrier((&___productListButtons_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTLISTBUTTON_T106B62A2104F017BE699A92DD9A2D55CC500E618_H
#ifndef PRODUCTOPTIONUIMANAGER_TD33196F16077A1DB55EFFB396D6D06984747AB87_H
#define PRODUCTOPTIONUIMANAGER_TD33196F16077A1DB55EFFB396D6D06984747AB87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductOptionUIManager
struct  ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button ProductOptionUIManager::backButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___backButton_4;
	// UnityEngine.UI.Button ProductOptionUIManager::placeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___placeButton_5;
	// UnityEngine.UI.Text ProductOptionUIManager::productName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___productName_6;
	// UnityEngine.UI.Dropdown ProductOptionUIManager::sizeDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___sizeDropdown_7;
	// UnityEngine.UI.Toggle[] ProductOptionUIManager::colorButtons
	ToggleU5BU5D_tBC353C53E1DDE1197A0EF21D8016A713839A8F52* ___colorButtons_8;
	// UnityEngine.UI.Dropdown ProductOptionUIManager::mattressDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___mattressDropdown_9;
	// UnityEngine.UI.RawImage ProductOptionUIManager::productImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___productImage_10;
	// System.Int32 ProductOptionUIManager::sizeIndex
	int32_t ___sizeIndex_11;
	// System.Int32 ProductOptionUIManager::colorIndex
	int32_t ___colorIndex_12;
	// System.Int32 ProductOptionUIManager::mattressIndex
	int32_t ___mattressIndex_13;

public:
	inline static int32_t get_offset_of_backButton_4() { return static_cast<int32_t>(offsetof(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87, ___backButton_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_backButton_4() const { return ___backButton_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_backButton_4() { return &___backButton_4; }
	inline void set_backButton_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___backButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___backButton_4), value);
	}

	inline static int32_t get_offset_of_placeButton_5() { return static_cast<int32_t>(offsetof(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87, ___placeButton_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_placeButton_5() const { return ___placeButton_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_placeButton_5() { return &___placeButton_5; }
	inline void set_placeButton_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___placeButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___placeButton_5), value);
	}

	inline static int32_t get_offset_of_productName_6() { return static_cast<int32_t>(offsetof(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87, ___productName_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_productName_6() const { return ___productName_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_productName_6() { return &___productName_6; }
	inline void set_productName_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___productName_6 = value;
		Il2CppCodeGenWriteBarrier((&___productName_6), value);
	}

	inline static int32_t get_offset_of_sizeDropdown_7() { return static_cast<int32_t>(offsetof(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87, ___sizeDropdown_7)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_sizeDropdown_7() const { return ___sizeDropdown_7; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_sizeDropdown_7() { return &___sizeDropdown_7; }
	inline void set_sizeDropdown_7(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___sizeDropdown_7 = value;
		Il2CppCodeGenWriteBarrier((&___sizeDropdown_7), value);
	}

	inline static int32_t get_offset_of_colorButtons_8() { return static_cast<int32_t>(offsetof(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87, ___colorButtons_8)); }
	inline ToggleU5BU5D_tBC353C53E1DDE1197A0EF21D8016A713839A8F52* get_colorButtons_8() const { return ___colorButtons_8; }
	inline ToggleU5BU5D_tBC353C53E1DDE1197A0EF21D8016A713839A8F52** get_address_of_colorButtons_8() { return &___colorButtons_8; }
	inline void set_colorButtons_8(ToggleU5BU5D_tBC353C53E1DDE1197A0EF21D8016A713839A8F52* value)
	{
		___colorButtons_8 = value;
		Il2CppCodeGenWriteBarrier((&___colorButtons_8), value);
	}

	inline static int32_t get_offset_of_mattressDropdown_9() { return static_cast<int32_t>(offsetof(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87, ___mattressDropdown_9)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_mattressDropdown_9() const { return ___mattressDropdown_9; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_mattressDropdown_9() { return &___mattressDropdown_9; }
	inline void set_mattressDropdown_9(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___mattressDropdown_9 = value;
		Il2CppCodeGenWriteBarrier((&___mattressDropdown_9), value);
	}

	inline static int32_t get_offset_of_productImage_10() { return static_cast<int32_t>(offsetof(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87, ___productImage_10)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_productImage_10() const { return ___productImage_10; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_productImage_10() { return &___productImage_10; }
	inline void set_productImage_10(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___productImage_10 = value;
		Il2CppCodeGenWriteBarrier((&___productImage_10), value);
	}

	inline static int32_t get_offset_of_sizeIndex_11() { return static_cast<int32_t>(offsetof(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87, ___sizeIndex_11)); }
	inline int32_t get_sizeIndex_11() const { return ___sizeIndex_11; }
	inline int32_t* get_address_of_sizeIndex_11() { return &___sizeIndex_11; }
	inline void set_sizeIndex_11(int32_t value)
	{
		___sizeIndex_11 = value;
	}

	inline static int32_t get_offset_of_colorIndex_12() { return static_cast<int32_t>(offsetof(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87, ___colorIndex_12)); }
	inline int32_t get_colorIndex_12() const { return ___colorIndex_12; }
	inline int32_t* get_address_of_colorIndex_12() { return &___colorIndex_12; }
	inline void set_colorIndex_12(int32_t value)
	{
		___colorIndex_12 = value;
	}

	inline static int32_t get_offset_of_mattressIndex_13() { return static_cast<int32_t>(offsetof(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87, ___mattressIndex_13)); }
	inline int32_t get_mattressIndex_13() const { return ___mattressIndex_13; }
	inline int32_t* get_address_of_mattressIndex_13() { return &___mattressIndex_13; }
	inline void set_mattressIndex_13(int32_t value)
	{
		___mattressIndex_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTOPTIONUIMANAGER_TD33196F16077A1DB55EFFB396D6D06984747AB87_H
#ifndef PRODUCTSCROLLLIST_TC73A3012458878C80956F749EE055A016FA363FD_H
#define PRODUCTSCROLLLIST_TC73A3012458878C80956F749EE055A016FA363FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductScrollList
struct  ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button ProductScrollList::scrollUpButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___scrollUpButton_8;
	// UnityEngine.UI.Dropdown ProductScrollList::filter
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___filter_9;

public:
	inline static int32_t get_offset_of_scrollUpButton_8() { return static_cast<int32_t>(offsetof(ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD, ___scrollUpButton_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_scrollUpButton_8() const { return ___scrollUpButton_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_scrollUpButton_8() { return &___scrollUpButton_8; }
	inline void set_scrollUpButton_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___scrollUpButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___scrollUpButton_8), value);
	}

	inline static int32_t get_offset_of_filter_9() { return static_cast<int32_t>(offsetof(ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD, ___filter_9)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_filter_9() const { return ___filter_9; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_filter_9() { return &___filter_9; }
	inline void set_filter_9(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___filter_9 = value;
		Il2CppCodeGenWriteBarrier((&___filter_9), value);
	}
};

struct ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD_StaticFields
{
public:
	// System.String ProductScrollList::FILTER_TOTAL
	String_t* ___FILTER_TOTAL_4;
	// System.String ProductScrollList::FILTER_WEDDING
	String_t* ___FILTER_WEDDING_5;
	// System.String ProductScrollList::FILTER_SUPERSINGLE
	String_t* ___FILTER_SUPERSINGLE_6;
	// System.String ProductScrollList::FILTER_FAMILY
	String_t* ___FILTER_FAMILY_7;

public:
	inline static int32_t get_offset_of_FILTER_TOTAL_4() { return static_cast<int32_t>(offsetof(ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD_StaticFields, ___FILTER_TOTAL_4)); }
	inline String_t* get_FILTER_TOTAL_4() const { return ___FILTER_TOTAL_4; }
	inline String_t** get_address_of_FILTER_TOTAL_4() { return &___FILTER_TOTAL_4; }
	inline void set_FILTER_TOTAL_4(String_t* value)
	{
		___FILTER_TOTAL_4 = value;
		Il2CppCodeGenWriteBarrier((&___FILTER_TOTAL_4), value);
	}

	inline static int32_t get_offset_of_FILTER_WEDDING_5() { return static_cast<int32_t>(offsetof(ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD_StaticFields, ___FILTER_WEDDING_5)); }
	inline String_t* get_FILTER_WEDDING_5() const { return ___FILTER_WEDDING_5; }
	inline String_t** get_address_of_FILTER_WEDDING_5() { return &___FILTER_WEDDING_5; }
	inline void set_FILTER_WEDDING_5(String_t* value)
	{
		___FILTER_WEDDING_5 = value;
		Il2CppCodeGenWriteBarrier((&___FILTER_WEDDING_5), value);
	}

	inline static int32_t get_offset_of_FILTER_SUPERSINGLE_6() { return static_cast<int32_t>(offsetof(ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD_StaticFields, ___FILTER_SUPERSINGLE_6)); }
	inline String_t* get_FILTER_SUPERSINGLE_6() const { return ___FILTER_SUPERSINGLE_6; }
	inline String_t** get_address_of_FILTER_SUPERSINGLE_6() { return &___FILTER_SUPERSINGLE_6; }
	inline void set_FILTER_SUPERSINGLE_6(String_t* value)
	{
		___FILTER_SUPERSINGLE_6 = value;
		Il2CppCodeGenWriteBarrier((&___FILTER_SUPERSINGLE_6), value);
	}

	inline static int32_t get_offset_of_FILTER_FAMILY_7() { return static_cast<int32_t>(offsetof(ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD_StaticFields, ___FILTER_FAMILY_7)); }
	inline String_t* get_FILTER_FAMILY_7() const { return ___FILTER_FAMILY_7; }
	inline String_t** get_address_of_FILTER_FAMILY_7() { return &___FILTER_FAMILY_7; }
	inline void set_FILTER_FAMILY_7(String_t* value)
	{
		___FILTER_FAMILY_7 = value;
		Il2CppCodeGenWriteBarrier((&___FILTER_FAMILY_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTSCROLLLIST_TC73A3012458878C80956F749EE055A016FA363FD_H
#ifndef SETTINGPANELCONTROLLER_T9363FFE1338F83CFD866857A3B9462FD8E9F1510_H
#define SETTINGPANELCONTROLLER_T9363FFE1338F83CFD866857A3B9462FD8E9F1510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingPanelController
struct  SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button SettingPanelController::closeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___closeButton_4;
	// UnityEngine.UI.Toggle SettingPanelController::highToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___highToggle_5;
	// UnityEngine.UI.Toggle SettingPanelController::middleToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___middleToggle_6;
	// UnityEngine.UI.Toggle SettingPanelController::lowToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___lowToggle_7;
	// UnityEngine.UI.Toggle SettingPanelController::wallColliderOffToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___wallColliderOffToggle_8;
	// UnityEngine.UI.Button SettingPanelController::linkButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___linkButton_9;

public:
	inline static int32_t get_offset_of_closeButton_4() { return static_cast<int32_t>(offsetof(SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510, ___closeButton_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_closeButton_4() const { return ___closeButton_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_closeButton_4() { return &___closeButton_4; }
	inline void set_closeButton_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___closeButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___closeButton_4), value);
	}

	inline static int32_t get_offset_of_highToggle_5() { return static_cast<int32_t>(offsetof(SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510, ___highToggle_5)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_highToggle_5() const { return ___highToggle_5; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_highToggle_5() { return &___highToggle_5; }
	inline void set_highToggle_5(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___highToggle_5 = value;
		Il2CppCodeGenWriteBarrier((&___highToggle_5), value);
	}

	inline static int32_t get_offset_of_middleToggle_6() { return static_cast<int32_t>(offsetof(SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510, ___middleToggle_6)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_middleToggle_6() const { return ___middleToggle_6; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_middleToggle_6() { return &___middleToggle_6; }
	inline void set_middleToggle_6(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___middleToggle_6 = value;
		Il2CppCodeGenWriteBarrier((&___middleToggle_6), value);
	}

	inline static int32_t get_offset_of_lowToggle_7() { return static_cast<int32_t>(offsetof(SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510, ___lowToggle_7)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_lowToggle_7() const { return ___lowToggle_7; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_lowToggle_7() { return &___lowToggle_7; }
	inline void set_lowToggle_7(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___lowToggle_7 = value;
		Il2CppCodeGenWriteBarrier((&___lowToggle_7), value);
	}

	inline static int32_t get_offset_of_wallColliderOffToggle_8() { return static_cast<int32_t>(offsetof(SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510, ___wallColliderOffToggle_8)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_wallColliderOffToggle_8() const { return ___wallColliderOffToggle_8; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_wallColliderOffToggle_8() { return &___wallColliderOffToggle_8; }
	inline void set_wallColliderOffToggle_8(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___wallColliderOffToggle_8 = value;
		Il2CppCodeGenWriteBarrier((&___wallColliderOffToggle_8), value);
	}

	inline static int32_t get_offset_of_linkButton_9() { return static_cast<int32_t>(offsetof(SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510, ___linkButton_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_linkButton_9() const { return ___linkButton_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_linkButton_9() { return &___linkButton_9; }
	inline void set_linkButton_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___linkButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___linkButton_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGPANELCONTROLLER_T9363FFE1338F83CFD866857A3B9462FD8E9F1510_H
#ifndef TESTINVISIBLEOBJ_T15F42714A2709618DA7FA5DE3DF98317C576AD96_H
#define TESTINVISIBLEOBJ_T15F42714A2709618DA7FA5DE3DF98317C576AD96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestInvisibleObj
struct  TestInvisibleObj_t15F42714A2709618DA7FA5DE3DF98317C576AD96  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTINVISIBLEOBJ_T15F42714A2709618DA7FA5DE3DF98317C576AD96_H
#ifndef TESTSCALECONTROLLER_TA2062EA7A2A0F815B94591A5DF41332FDC904852_H
#define TESTSCALECONTROLLER_TA2062EA7A2A0F815B94591A5DF41332FDC904852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestScaleController
struct  TestScaleController_tA2062EA7A2A0F815B94591A5DF41332FDC904852  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button TestScaleController::btnSizeUp
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnSizeUp_4;
	// UnityEngine.UI.Button TestScaleController::btnSizeDown
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnSizeDown_5;
	// UnityEngine.UI.Text TestScaleController::textSize
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textSize_6;

public:
	inline static int32_t get_offset_of_btnSizeUp_4() { return static_cast<int32_t>(offsetof(TestScaleController_tA2062EA7A2A0F815B94591A5DF41332FDC904852, ___btnSizeUp_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnSizeUp_4() const { return ___btnSizeUp_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnSizeUp_4() { return &___btnSizeUp_4; }
	inline void set_btnSizeUp_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnSizeUp_4 = value;
		Il2CppCodeGenWriteBarrier((&___btnSizeUp_4), value);
	}

	inline static int32_t get_offset_of_btnSizeDown_5() { return static_cast<int32_t>(offsetof(TestScaleController_tA2062EA7A2A0F815B94591A5DF41332FDC904852, ___btnSizeDown_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnSizeDown_5() const { return ___btnSizeDown_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnSizeDown_5() { return &___btnSizeDown_5; }
	inline void set_btnSizeDown_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnSizeDown_5 = value;
		Il2CppCodeGenWriteBarrier((&___btnSizeDown_5), value);
	}

	inline static int32_t get_offset_of_textSize_6() { return static_cast<int32_t>(offsetof(TestScaleController_tA2062EA7A2A0F815B94591A5DF41332FDC904852, ___textSize_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textSize_6() const { return ___textSize_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textSize_6() { return &___textSize_6; }
	inline void set_textSize_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textSize_6 = value;
		Il2CppCodeGenWriteBarrier((&___textSize_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTSCALECONTROLLER_TA2062EA7A2A0F815B94591A5DF41332FDC904852_H
#ifndef TESTVISIBLEOBJ_T03EFFD04D370B3ECCAB2774C5FDC2575EA7FA051_H
#define TESTVISIBLEOBJ_T03EFFD04D370B3ECCAB2774C5FDC2575EA7FA051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestVisibleObj
struct  TestVisibleObj_t03EFFD04D370B3ECCAB2774C5FDC2575EA7FA051  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTVISIBLEOBJ_T03EFFD04D370B3ECCAB2774C5FDC2575EA7FA051_H
#ifndef TEXTFXDEMOMANAGER_TF0E9B4458C1CADF282EC3E0D2766DE050B3D3136_H
#define TEXTFXDEMOMANAGER_TF0E9B4458C1CADF282EC3E0D2766DE050B3D3136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFxDemoManager
struct  TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TextFx.TextFxUGUI TextFxDemoManager::m_titleEffect
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81 * ___m_titleEffect_4;
	// UnityEngine.RectTransform TextFxDemoManager::m_animEditorContent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_animEditorContent_5;
	// TextFx.TextFxUGUI TextFxDemoManager::m_effect
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81 * ___m_effect_6;
	// UnityEngine.Color TextFxDemoManager::SECTION_INACTIVE_COLOUR
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SECTION_INACTIVE_COLOUR_7;
	// UnityEngine.Color TextFxDemoManager::SECTION_ACTIVE_COLOUR
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SECTION_ACTIVE_COLOUR_8;
	// UnityEngine.Color TextFxDemoManager::SECTION_SELECTED_COLOUR
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SECTION_SELECTED_COLOUR_9;
	// UnityEngine.UI.Image[] TextFxDemoManager::m_sectionHeaderImages
	ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* ___m_sectionHeaderImages_10;
	// UnityEngine.RectTransform TextFxDemoManager::m_menuSectionsContainer
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_menuSectionsContainer_11;
	// UnityEngine.RectTransform[] TextFxDemoManager::m_animSectionTransforms
	RectTransformU5BU5D_tF94B8797BE321CF3ED1C3E6426CE13AFC8F1D478* ___m_animSectionTransforms_12;
	// UnityEngine.UI.Dropdown[] TextFxDemoManager::m_animSectionDropdowns
	DropdownU5BU5D_t3BF42BE9FA13EBBF708C115E950231B67CA3B3A9* ___m_animSectionDropdowns_13;
	// UnityEngine.UI.Button TextFxDemoManager::m_playButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_playButton_14;
	// UnityEngine.UI.Text TextFxDemoManager::m_playButtonText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_playButtonText_15;
	// UnityEngine.UI.Button TextFxDemoManager::m_resetButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_resetButton_16;
	// UnityEngine.UI.Button TextFxDemoManager::m_continueButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_continueButton_17;
	// FloatVariableSetting TextFxDemoManager::m_floatVariableSettingPrefab
	FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3 * ___m_floatVariableSettingPrefab_18;
	// Vector3VariableSetting TextFxDemoManager::m_vector3VariableSettingPrefab
	Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F * ___m_vector3VariableSettingPrefab_19;
	// ToggleVariableSetting TextFxDemoManager::m_toggleVariableSettingPrefab
	ToggleVariableSetting_tE79CD9769496E78E42260EB965B78F11A4C4526B * ___m_toggleVariableSettingPrefab_20;
	// ColourVariableSetting TextFxDemoManager::m_colourVariableSettingPrefab
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B * ___m_colourVariableSettingPrefab_21;
	// UnityEngine.UI.InputField TextFxDemoManager::m_textInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___m_textInput_22;
	// System.Int32 TextFxDemoManager::m_introStartEffect
	int32_t ___m_introStartEffect_23;
	// System.Int32 TextFxDemoManager::m_mainStartEffect
	int32_t ___m_mainStartEffect_24;
	// System.Int32 TextFxDemoManager::m_outroStartEffect
	int32_t ___m_outroStartEffect_25;
	// System.Boolean TextFxDemoManager::m_skipTitleEffectIntro
	bool ___m_skipTitleEffectIntro_26;
	// TextFx.TextFxAnimationManager_PresetAnimationSection[] TextFxDemoManager::m_animationSections
	PresetAnimationSectionU5BU5D_t9664FB6764C6963FD3DC013B02B396E220CAF8C5* ___m_animationSections_27;
	// TextFx.ObjectPool`1<FloatVariableSetting> TextFxDemoManager::m_floatVariableSettingsPool
	ObjectPool_1_tB80D5BA8A3F87AD8A47D4C01367054974FBB72D9 * ___m_floatVariableSettingsPool_28;
	// TextFx.ObjectPool`1<Vector3VariableSetting> TextFxDemoManager::m_vector3VariableSettingsPool
	ObjectPool_1_t6133E7CD47008A992DFB6630A04DD9E254682166 * ___m_vector3VariableSettingsPool_29;
	// TextFx.ObjectPool`1<ToggleVariableSetting> TextFxDemoManager::m_toggleVariableSettingsPool
	ObjectPool_1_t51704C3EDEED132C08C737E67EB7C575A554C826 * ___m_toggleVariableSettingsPool_30;
	// TextFx.ObjectPool`1<ColourVariableSetting> TextFxDemoManager::m_colourVariableSettingsPool
	ObjectPool_1_tEBA797148C9EBAECCDF7A5C63A84E5BD48DBFFD6 * ___m_colourVariableSettingsPool_31;
	// System.Collections.Generic.List`1<FloatVariableSetting>[] TextFxDemoManager::m_floatVariableSettingsInUse
	List_1U5BU5D_t175E3D9BA32BCDBC808EACD530EC3BEF73F75139* ___m_floatVariableSettingsInUse_32;
	// System.Collections.Generic.List`1<Vector3VariableSetting>[] TextFxDemoManager::m_vector3VariableSettingsInUse
	List_1U5BU5D_t341F48BF0BBB99AD7A15EB05DF143D4F51E00E17* ___m_vector3VariableSettingsInUse_33;
	// System.Collections.Generic.List`1<ToggleVariableSetting>[] TextFxDemoManager::m_toggleVariableSettingsInUse
	List_1U5BU5D_t8E82F1EFA45C7A2A85B854C2958583C5CDBA5A49* ___m_toggleVariableSettingsInUse_34;
	// System.Collections.Generic.List`1<ColourVariableSetting>[] TextFxDemoManager::m_colourVariableSettingsInUse
	List_1U5BU5D_tFCFA1E1475DC0B4BD9CF6BC029DB77EB834CD4D4* ___m_colourVariableSettingsInUse_35;
	// System.Boolean[] TextFxDemoManager::m_sectionsActive
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___m_sectionsActive_36;
	// System.Int32 TextFxDemoManager::m_activeSectionIndex
	int32_t ___m_activeSectionIndex_37;
	// UnityEngine.Color TextFxDemoManager::m_playbackButtonHighlightColour
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_playbackButtonHighlightColour_38;

public:
	inline static int32_t get_offset_of_m_titleEffect_4() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_titleEffect_4)); }
	inline TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81 * get_m_titleEffect_4() const { return ___m_titleEffect_4; }
	inline TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81 ** get_address_of_m_titleEffect_4() { return &___m_titleEffect_4; }
	inline void set_m_titleEffect_4(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81 * value)
	{
		___m_titleEffect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_titleEffect_4), value);
	}

	inline static int32_t get_offset_of_m_animEditorContent_5() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_animEditorContent_5)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_animEditorContent_5() const { return ___m_animEditorContent_5; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_animEditorContent_5() { return &___m_animEditorContent_5; }
	inline void set_m_animEditorContent_5(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_animEditorContent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_animEditorContent_5), value);
	}

	inline static int32_t get_offset_of_m_effect_6() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_effect_6)); }
	inline TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81 * get_m_effect_6() const { return ___m_effect_6; }
	inline TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81 ** get_address_of_m_effect_6() { return &___m_effect_6; }
	inline void set_m_effect_6(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81 * value)
	{
		___m_effect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_effect_6), value);
	}

	inline static int32_t get_offset_of_SECTION_INACTIVE_COLOUR_7() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___SECTION_INACTIVE_COLOUR_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SECTION_INACTIVE_COLOUR_7() const { return ___SECTION_INACTIVE_COLOUR_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SECTION_INACTIVE_COLOUR_7() { return &___SECTION_INACTIVE_COLOUR_7; }
	inline void set_SECTION_INACTIVE_COLOUR_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SECTION_INACTIVE_COLOUR_7 = value;
	}

	inline static int32_t get_offset_of_SECTION_ACTIVE_COLOUR_8() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___SECTION_ACTIVE_COLOUR_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SECTION_ACTIVE_COLOUR_8() const { return ___SECTION_ACTIVE_COLOUR_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SECTION_ACTIVE_COLOUR_8() { return &___SECTION_ACTIVE_COLOUR_8; }
	inline void set_SECTION_ACTIVE_COLOUR_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SECTION_ACTIVE_COLOUR_8 = value;
	}

	inline static int32_t get_offset_of_SECTION_SELECTED_COLOUR_9() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___SECTION_SELECTED_COLOUR_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SECTION_SELECTED_COLOUR_9() const { return ___SECTION_SELECTED_COLOUR_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SECTION_SELECTED_COLOUR_9() { return &___SECTION_SELECTED_COLOUR_9; }
	inline void set_SECTION_SELECTED_COLOUR_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SECTION_SELECTED_COLOUR_9 = value;
	}

	inline static int32_t get_offset_of_m_sectionHeaderImages_10() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_sectionHeaderImages_10)); }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* get_m_sectionHeaderImages_10() const { return ___m_sectionHeaderImages_10; }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D** get_address_of_m_sectionHeaderImages_10() { return &___m_sectionHeaderImages_10; }
	inline void set_m_sectionHeaderImages_10(ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* value)
	{
		___m_sectionHeaderImages_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_sectionHeaderImages_10), value);
	}

	inline static int32_t get_offset_of_m_menuSectionsContainer_11() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_menuSectionsContainer_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_menuSectionsContainer_11() const { return ___m_menuSectionsContainer_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_menuSectionsContainer_11() { return &___m_menuSectionsContainer_11; }
	inline void set_m_menuSectionsContainer_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_menuSectionsContainer_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_menuSectionsContainer_11), value);
	}

	inline static int32_t get_offset_of_m_animSectionTransforms_12() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_animSectionTransforms_12)); }
	inline RectTransformU5BU5D_tF94B8797BE321CF3ED1C3E6426CE13AFC8F1D478* get_m_animSectionTransforms_12() const { return ___m_animSectionTransforms_12; }
	inline RectTransformU5BU5D_tF94B8797BE321CF3ED1C3E6426CE13AFC8F1D478** get_address_of_m_animSectionTransforms_12() { return &___m_animSectionTransforms_12; }
	inline void set_m_animSectionTransforms_12(RectTransformU5BU5D_tF94B8797BE321CF3ED1C3E6426CE13AFC8F1D478* value)
	{
		___m_animSectionTransforms_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_animSectionTransforms_12), value);
	}

	inline static int32_t get_offset_of_m_animSectionDropdowns_13() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_animSectionDropdowns_13)); }
	inline DropdownU5BU5D_t3BF42BE9FA13EBBF708C115E950231B67CA3B3A9* get_m_animSectionDropdowns_13() const { return ___m_animSectionDropdowns_13; }
	inline DropdownU5BU5D_t3BF42BE9FA13EBBF708C115E950231B67CA3B3A9** get_address_of_m_animSectionDropdowns_13() { return &___m_animSectionDropdowns_13; }
	inline void set_m_animSectionDropdowns_13(DropdownU5BU5D_t3BF42BE9FA13EBBF708C115E950231B67CA3B3A9* value)
	{
		___m_animSectionDropdowns_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_animSectionDropdowns_13), value);
	}

	inline static int32_t get_offset_of_m_playButton_14() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_playButton_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_playButton_14() const { return ___m_playButton_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_playButton_14() { return &___m_playButton_14; }
	inline void set_m_playButton_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_playButton_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_playButton_14), value);
	}

	inline static int32_t get_offset_of_m_playButtonText_15() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_playButtonText_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_playButtonText_15() const { return ___m_playButtonText_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_playButtonText_15() { return &___m_playButtonText_15; }
	inline void set_m_playButtonText_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_playButtonText_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_playButtonText_15), value);
	}

	inline static int32_t get_offset_of_m_resetButton_16() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_resetButton_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_resetButton_16() const { return ___m_resetButton_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_resetButton_16() { return &___m_resetButton_16; }
	inline void set_m_resetButton_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_resetButton_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_resetButton_16), value);
	}

	inline static int32_t get_offset_of_m_continueButton_17() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_continueButton_17)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_continueButton_17() const { return ___m_continueButton_17; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_continueButton_17() { return &___m_continueButton_17; }
	inline void set_m_continueButton_17(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_continueButton_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_continueButton_17), value);
	}

	inline static int32_t get_offset_of_m_floatVariableSettingPrefab_18() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_floatVariableSettingPrefab_18)); }
	inline FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3 * get_m_floatVariableSettingPrefab_18() const { return ___m_floatVariableSettingPrefab_18; }
	inline FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3 ** get_address_of_m_floatVariableSettingPrefab_18() { return &___m_floatVariableSettingPrefab_18; }
	inline void set_m_floatVariableSettingPrefab_18(FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3 * value)
	{
		___m_floatVariableSettingPrefab_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatVariableSettingPrefab_18), value);
	}

	inline static int32_t get_offset_of_m_vector3VariableSettingPrefab_19() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_vector3VariableSettingPrefab_19)); }
	inline Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F * get_m_vector3VariableSettingPrefab_19() const { return ___m_vector3VariableSettingPrefab_19; }
	inline Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F ** get_address_of_m_vector3VariableSettingPrefab_19() { return &___m_vector3VariableSettingPrefab_19; }
	inline void set_m_vector3VariableSettingPrefab_19(Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F * value)
	{
		___m_vector3VariableSettingPrefab_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_vector3VariableSettingPrefab_19), value);
	}

	inline static int32_t get_offset_of_m_toggleVariableSettingPrefab_20() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_toggleVariableSettingPrefab_20)); }
	inline ToggleVariableSetting_tE79CD9769496E78E42260EB965B78F11A4C4526B * get_m_toggleVariableSettingPrefab_20() const { return ___m_toggleVariableSettingPrefab_20; }
	inline ToggleVariableSetting_tE79CD9769496E78E42260EB965B78F11A4C4526B ** get_address_of_m_toggleVariableSettingPrefab_20() { return &___m_toggleVariableSettingPrefab_20; }
	inline void set_m_toggleVariableSettingPrefab_20(ToggleVariableSetting_tE79CD9769496E78E42260EB965B78F11A4C4526B * value)
	{
		___m_toggleVariableSettingPrefab_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggleVariableSettingPrefab_20), value);
	}

	inline static int32_t get_offset_of_m_colourVariableSettingPrefab_21() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_colourVariableSettingPrefab_21)); }
	inline ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B * get_m_colourVariableSettingPrefab_21() const { return ___m_colourVariableSettingPrefab_21; }
	inline ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B ** get_address_of_m_colourVariableSettingPrefab_21() { return &___m_colourVariableSettingPrefab_21; }
	inline void set_m_colourVariableSettingPrefab_21(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B * value)
	{
		___m_colourVariableSettingPrefab_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_colourVariableSettingPrefab_21), value);
	}

	inline static int32_t get_offset_of_m_textInput_22() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_textInput_22)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_m_textInput_22() const { return ___m_textInput_22; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_m_textInput_22() { return &___m_textInput_22; }
	inline void set_m_textInput_22(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___m_textInput_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInput_22), value);
	}

	inline static int32_t get_offset_of_m_introStartEffect_23() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_introStartEffect_23)); }
	inline int32_t get_m_introStartEffect_23() const { return ___m_introStartEffect_23; }
	inline int32_t* get_address_of_m_introStartEffect_23() { return &___m_introStartEffect_23; }
	inline void set_m_introStartEffect_23(int32_t value)
	{
		___m_introStartEffect_23 = value;
	}

	inline static int32_t get_offset_of_m_mainStartEffect_24() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_mainStartEffect_24)); }
	inline int32_t get_m_mainStartEffect_24() const { return ___m_mainStartEffect_24; }
	inline int32_t* get_address_of_m_mainStartEffect_24() { return &___m_mainStartEffect_24; }
	inline void set_m_mainStartEffect_24(int32_t value)
	{
		___m_mainStartEffect_24 = value;
	}

	inline static int32_t get_offset_of_m_outroStartEffect_25() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_outroStartEffect_25)); }
	inline int32_t get_m_outroStartEffect_25() const { return ___m_outroStartEffect_25; }
	inline int32_t* get_address_of_m_outroStartEffect_25() { return &___m_outroStartEffect_25; }
	inline void set_m_outroStartEffect_25(int32_t value)
	{
		___m_outroStartEffect_25 = value;
	}

	inline static int32_t get_offset_of_m_skipTitleEffectIntro_26() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_skipTitleEffectIntro_26)); }
	inline bool get_m_skipTitleEffectIntro_26() const { return ___m_skipTitleEffectIntro_26; }
	inline bool* get_address_of_m_skipTitleEffectIntro_26() { return &___m_skipTitleEffectIntro_26; }
	inline void set_m_skipTitleEffectIntro_26(bool value)
	{
		___m_skipTitleEffectIntro_26 = value;
	}

	inline static int32_t get_offset_of_m_animationSections_27() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_animationSections_27)); }
	inline PresetAnimationSectionU5BU5D_t9664FB6764C6963FD3DC013B02B396E220CAF8C5* get_m_animationSections_27() const { return ___m_animationSections_27; }
	inline PresetAnimationSectionU5BU5D_t9664FB6764C6963FD3DC013B02B396E220CAF8C5** get_address_of_m_animationSections_27() { return &___m_animationSections_27; }
	inline void set_m_animationSections_27(PresetAnimationSectionU5BU5D_t9664FB6764C6963FD3DC013B02B396E220CAF8C5* value)
	{
		___m_animationSections_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_animationSections_27), value);
	}

	inline static int32_t get_offset_of_m_floatVariableSettingsPool_28() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_floatVariableSettingsPool_28)); }
	inline ObjectPool_1_tB80D5BA8A3F87AD8A47D4C01367054974FBB72D9 * get_m_floatVariableSettingsPool_28() const { return ___m_floatVariableSettingsPool_28; }
	inline ObjectPool_1_tB80D5BA8A3F87AD8A47D4C01367054974FBB72D9 ** get_address_of_m_floatVariableSettingsPool_28() { return &___m_floatVariableSettingsPool_28; }
	inline void set_m_floatVariableSettingsPool_28(ObjectPool_1_tB80D5BA8A3F87AD8A47D4C01367054974FBB72D9 * value)
	{
		___m_floatVariableSettingsPool_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatVariableSettingsPool_28), value);
	}

	inline static int32_t get_offset_of_m_vector3VariableSettingsPool_29() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_vector3VariableSettingsPool_29)); }
	inline ObjectPool_1_t6133E7CD47008A992DFB6630A04DD9E254682166 * get_m_vector3VariableSettingsPool_29() const { return ___m_vector3VariableSettingsPool_29; }
	inline ObjectPool_1_t6133E7CD47008A992DFB6630A04DD9E254682166 ** get_address_of_m_vector3VariableSettingsPool_29() { return &___m_vector3VariableSettingsPool_29; }
	inline void set_m_vector3VariableSettingsPool_29(ObjectPool_1_t6133E7CD47008A992DFB6630A04DD9E254682166 * value)
	{
		___m_vector3VariableSettingsPool_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_vector3VariableSettingsPool_29), value);
	}

	inline static int32_t get_offset_of_m_toggleVariableSettingsPool_30() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_toggleVariableSettingsPool_30)); }
	inline ObjectPool_1_t51704C3EDEED132C08C737E67EB7C575A554C826 * get_m_toggleVariableSettingsPool_30() const { return ___m_toggleVariableSettingsPool_30; }
	inline ObjectPool_1_t51704C3EDEED132C08C737E67EB7C575A554C826 ** get_address_of_m_toggleVariableSettingsPool_30() { return &___m_toggleVariableSettingsPool_30; }
	inline void set_m_toggleVariableSettingsPool_30(ObjectPool_1_t51704C3EDEED132C08C737E67EB7C575A554C826 * value)
	{
		___m_toggleVariableSettingsPool_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggleVariableSettingsPool_30), value);
	}

	inline static int32_t get_offset_of_m_colourVariableSettingsPool_31() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_colourVariableSettingsPool_31)); }
	inline ObjectPool_1_tEBA797148C9EBAECCDF7A5C63A84E5BD48DBFFD6 * get_m_colourVariableSettingsPool_31() const { return ___m_colourVariableSettingsPool_31; }
	inline ObjectPool_1_tEBA797148C9EBAECCDF7A5C63A84E5BD48DBFFD6 ** get_address_of_m_colourVariableSettingsPool_31() { return &___m_colourVariableSettingsPool_31; }
	inline void set_m_colourVariableSettingsPool_31(ObjectPool_1_tEBA797148C9EBAECCDF7A5C63A84E5BD48DBFFD6 * value)
	{
		___m_colourVariableSettingsPool_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_colourVariableSettingsPool_31), value);
	}

	inline static int32_t get_offset_of_m_floatVariableSettingsInUse_32() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_floatVariableSettingsInUse_32)); }
	inline List_1U5BU5D_t175E3D9BA32BCDBC808EACD530EC3BEF73F75139* get_m_floatVariableSettingsInUse_32() const { return ___m_floatVariableSettingsInUse_32; }
	inline List_1U5BU5D_t175E3D9BA32BCDBC808EACD530EC3BEF73F75139** get_address_of_m_floatVariableSettingsInUse_32() { return &___m_floatVariableSettingsInUse_32; }
	inline void set_m_floatVariableSettingsInUse_32(List_1U5BU5D_t175E3D9BA32BCDBC808EACD530EC3BEF73F75139* value)
	{
		___m_floatVariableSettingsInUse_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatVariableSettingsInUse_32), value);
	}

	inline static int32_t get_offset_of_m_vector3VariableSettingsInUse_33() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_vector3VariableSettingsInUse_33)); }
	inline List_1U5BU5D_t341F48BF0BBB99AD7A15EB05DF143D4F51E00E17* get_m_vector3VariableSettingsInUse_33() const { return ___m_vector3VariableSettingsInUse_33; }
	inline List_1U5BU5D_t341F48BF0BBB99AD7A15EB05DF143D4F51E00E17** get_address_of_m_vector3VariableSettingsInUse_33() { return &___m_vector3VariableSettingsInUse_33; }
	inline void set_m_vector3VariableSettingsInUse_33(List_1U5BU5D_t341F48BF0BBB99AD7A15EB05DF143D4F51E00E17* value)
	{
		___m_vector3VariableSettingsInUse_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_vector3VariableSettingsInUse_33), value);
	}

	inline static int32_t get_offset_of_m_toggleVariableSettingsInUse_34() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_toggleVariableSettingsInUse_34)); }
	inline List_1U5BU5D_t8E82F1EFA45C7A2A85B854C2958583C5CDBA5A49* get_m_toggleVariableSettingsInUse_34() const { return ___m_toggleVariableSettingsInUse_34; }
	inline List_1U5BU5D_t8E82F1EFA45C7A2A85B854C2958583C5CDBA5A49** get_address_of_m_toggleVariableSettingsInUse_34() { return &___m_toggleVariableSettingsInUse_34; }
	inline void set_m_toggleVariableSettingsInUse_34(List_1U5BU5D_t8E82F1EFA45C7A2A85B854C2958583C5CDBA5A49* value)
	{
		___m_toggleVariableSettingsInUse_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggleVariableSettingsInUse_34), value);
	}

	inline static int32_t get_offset_of_m_colourVariableSettingsInUse_35() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_colourVariableSettingsInUse_35)); }
	inline List_1U5BU5D_tFCFA1E1475DC0B4BD9CF6BC029DB77EB834CD4D4* get_m_colourVariableSettingsInUse_35() const { return ___m_colourVariableSettingsInUse_35; }
	inline List_1U5BU5D_tFCFA1E1475DC0B4BD9CF6BC029DB77EB834CD4D4** get_address_of_m_colourVariableSettingsInUse_35() { return &___m_colourVariableSettingsInUse_35; }
	inline void set_m_colourVariableSettingsInUse_35(List_1U5BU5D_tFCFA1E1475DC0B4BD9CF6BC029DB77EB834CD4D4* value)
	{
		___m_colourVariableSettingsInUse_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_colourVariableSettingsInUse_35), value);
	}

	inline static int32_t get_offset_of_m_sectionsActive_36() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_sectionsActive_36)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_m_sectionsActive_36() const { return ___m_sectionsActive_36; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_m_sectionsActive_36() { return &___m_sectionsActive_36; }
	inline void set_m_sectionsActive_36(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___m_sectionsActive_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_sectionsActive_36), value);
	}

	inline static int32_t get_offset_of_m_activeSectionIndex_37() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_activeSectionIndex_37)); }
	inline int32_t get_m_activeSectionIndex_37() const { return ___m_activeSectionIndex_37; }
	inline int32_t* get_address_of_m_activeSectionIndex_37() { return &___m_activeSectionIndex_37; }
	inline void set_m_activeSectionIndex_37(int32_t value)
	{
		___m_activeSectionIndex_37 = value;
	}

	inline static int32_t get_offset_of_m_playbackButtonHighlightColour_38() { return static_cast<int32_t>(offsetof(TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136, ___m_playbackButtonHighlightColour_38)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_playbackButtonHighlightColour_38() const { return ___m_playbackButtonHighlightColour_38; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_playbackButtonHighlightColour_38() { return &___m_playbackButtonHighlightColour_38; }
	inline void set_m_playbackButtonHighlightColour_38(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_playbackButtonHighlightColour_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXDEMOMANAGER_TF0E9B4458C1CADF282EC3E0D2766DE050B3D3136_H
#ifndef TEXTFXQUICKSETUPANIMCONFIGS_TF0E7F63EB9D33575003B7216752A08CCCABF2286_H
#define TEXTFXQUICKSETUPANIMCONFIGS_TF0E7F63EB9D33575003B7216752A08CCCABF2286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFxQuickSetupAnimConfigs
struct  TextFxQuickSetupAnimConfigs_tF0E7F63EB9D33575003B7216752A08CCCABF2286  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct TextFxQuickSetupAnimConfigs_tF0E7F63EB9D33575003B7216752A08CCCABF2286_StaticFields
{
public:
	// System.String[] TextFxQuickSetupAnimConfigs::m_introAnimNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_introAnimNames_7;
	// System.String[] TextFxQuickSetupAnimConfigs::m_mainAnimNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_mainAnimNames_8;
	// System.String[] TextFxQuickSetupAnimConfigs::m_outroAnimNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_outroAnimNames_9;

public:
	inline static int32_t get_offset_of_m_introAnimNames_7() { return static_cast<int32_t>(offsetof(TextFxQuickSetupAnimConfigs_tF0E7F63EB9D33575003B7216752A08CCCABF2286_StaticFields, ___m_introAnimNames_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_introAnimNames_7() const { return ___m_introAnimNames_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_introAnimNames_7() { return &___m_introAnimNames_7; }
	inline void set_m_introAnimNames_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_introAnimNames_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_introAnimNames_7), value);
	}

	inline static int32_t get_offset_of_m_mainAnimNames_8() { return static_cast<int32_t>(offsetof(TextFxQuickSetupAnimConfigs_tF0E7F63EB9D33575003B7216752A08CCCABF2286_StaticFields, ___m_mainAnimNames_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_mainAnimNames_8() const { return ___m_mainAnimNames_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_mainAnimNames_8() { return &___m_mainAnimNames_8; }
	inline void set_m_mainAnimNames_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_mainAnimNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_mainAnimNames_8), value);
	}

	inline static int32_t get_offset_of_m_outroAnimNames_9() { return static_cast<int32_t>(offsetof(TextFxQuickSetupAnimConfigs_tF0E7F63EB9D33575003B7216752A08CCCABF2286_StaticFields, ___m_outroAnimNames_9)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_outroAnimNames_9() const { return ___m_outroAnimNames_9; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_outroAnimNames_9() { return &___m_outroAnimNames_9; }
	inline void set_m_outroAnimNames_9(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_outroAnimNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_outroAnimNames_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXQUICKSETUPANIMCONFIGS_TF0E7F63EB9D33575003B7216752A08CCCABF2286_H
#ifndef UICONTROLLER_T06FB25185FB9B776EF16705D93C83EE95CE1D1EE_H
#define UICONTROLLER_T06FB25185FB9B776EF16705D93C83EE95CE1D1EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController
struct  UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button UIController::loadUnitA
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___loadUnitA_5;
	// UnityEngine.UI.Button UIController::loadUnitB
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___loadUnitB_6;
	// UnityEngine.UI.Button UIController::loadUnitC
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___loadUnitC_7;
	// UnityEngine.UI.Button UIController::calcDistance
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___calcDistance_8;
	// UnityEngine.UI.Button UIController::endCalcDistance
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___endCalcDistance_9;
	// UnityEngine.UI.Button UIController::calcArea
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___calcArea_10;
	// UnityEngine.UI.Button UIController::endCalcArea
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___endCalcArea_11;
	// UnityEngine.UI.Button UIController::loadProduct
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___loadProduct_12;
	// UnityEngine.UI.Button UIController::loadProduct2
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___loadProduct2_13;
	// UnityEngine.UI.Button UIController::loadProduct3
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___loadProduct3_14;
	// UnityEngine.UI.Text UIController::lightDirectionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___lightDirectionText_15;
	// UnityEngine.UI.Slider UIController::lightDirection
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___lightDirection_16;
	// UnityEngine.UI.Button UIController::clearUnit
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___clearUnit_17;
	// UnityEngine.UI.Button UIController::requestMapAPIBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___requestMapAPIBtn_18;
	// UnityEngine.UI.InputField UIController::inputFieldSearchAddress
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___inputFieldSearchAddress_19;
	// UnityEngine.UI.Button UIController::pinButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___pinButton_20;
	// UnityEngine.GameObject UIController::joystickCotrol
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___joystickCotrol_21;
	// UnityEngine.GameObject UIController::viewRotationControls
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___viewRotationControls_22;
	// System.Boolean UIController::_fpsViewMode
	bool ____fpsViewMode_23;
	// UnityEngine.GameObject UIController::topPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___topPanel_24;
	// UnityEngine.GameObject UIController::rightMenuPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___rightMenuPanel_25;
	// UnityEngine.UI.Button UIController::backToMain
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___backToMain_26;
	// UnityEngine.UI.Button UIController::screenShot
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___screenShot_27;
	// UnityEngine.UI.Button UIController::alignment
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___alignment_28;
	// UnityEngine.UI.Button UIController::undo
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___undo_29;
	// UnityEngine.UI.Button UIController::redo
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___redo_30;
	// UnityEngine.UI.Button UIController::cameraModePanning
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___cameraModePanning_31;
	// UnityEngine.UI.Button UIController::cameraModeOrbit
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___cameraModeOrbit_32;
	// UnityEngine.UI.Button UIController::settings
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___settings_33;
	// UnityEngine.UI.Toggle UIController::fpsModeToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___fpsModeToggle_34;
	// UnityEngine.UI.Toggle UIController::orbitModeToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___orbitModeToggle_35;
	// UnityEngine.UI.Button UIController::stopStyling
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___stopStyling_36;
	// UnityEngine.GameObject UIController::canvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_37;
	// ProductScrollList UIController::productScrollList
	ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD * ___productScrollList_38;
	// ProductListButton UIController::productListButton
	ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618 * ___productListButton_39;
	// UnityEngine.UI.Button UIController::showItemList
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___showItemList_40;
	// UnityEngine.GameObject UIController::itemListPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___itemListPanel_41;
	// UnityEngine.UI.Button UIController::hideItemList
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___hideItemList_42;
	// UnityEngine.UI.Toggle UIController::tabBed
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___tabBed_43;
	// UnityEngine.UI.Toggle UIController::tabWall
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___tabWall_44;
	// UnityEngine.UI.Toggle UIController::tabFloor
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___tabFloor_45;
	// UnityEngine.GameObject UIController::wallpaperListPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___wallpaperListPanel_46;
	// UnityEngine.GameObject UIController::floorListPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___floorListPanel_47;
	// UnityEngine.UI.Button[] UIController::wallpaperButtons
	ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B* ___wallpaperButtons_48;
	// UnityEngine.UI.Button[] UIController::floorButtons
	ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B* ___floorButtons_49;
	// System.Boolean UIController::_panningMode
	bool ____panningMode_50;
	// System.Collections.Generic.List`1<UIController_OptionData> UIController::_listOptionCombination
	List_1_t5B38DDA976DDB66A172FD1855281F01EE9ED63F0 * ____listOptionCombination_51;
	// System.Collections.Generic.List`1<UIController_FrameData> UIController::_frameProductList
	List_1_tFD496FCFB1F7C232809B0A18F6887706614F65F3 * ____frameProductList_52;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UIController::_mattressProductList
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____mattressProductList_53;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UIController::_mattressPathMap
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____mattressPathMap_54;
	// System.Collections.Generic.Dictionary`2<System.String,System.String[]> UIController::_mattressListSize
	Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * ____mattressListSize_55;
	// ProductOptionUIManager UIController::productOptionUIManager
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87 * ___productOptionUIManager_56;
	// SettingPanelController UIController::settingPanelController
	SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510 * ___settingPanelController_57;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UIController::_wallpaperJsonMap
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____wallpaperJsonMap_58;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UIController::_floorJsonMap
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____floorJsonMap_59;
	// System.Collections.Generic.List`1<System.String> UIController::_wallpaperIdList
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____wallpaperIdList_60;
	// System.Collections.Generic.List`1<System.String> UIController::_floorIdList
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____floorIdList_61;
	// System.Collections.Generic.List`1<UIController_ProductJsonInfo> UIController::_productJsonList
	List_1_t537386FE2177AFB667368B76654E8D513578BE29 * ____productJsonList_62;
	// UnityEngine.GameObject[] UIController::_pointLightsForUnit
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ____pointLightsForUnit_63;
	// UnityEngine.GameObject UIController::pointLightPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pointLightPrefab_64;

public:
	inline static int32_t get_offset_of_loadUnitA_5() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___loadUnitA_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_loadUnitA_5() const { return ___loadUnitA_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_loadUnitA_5() { return &___loadUnitA_5; }
	inline void set_loadUnitA_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___loadUnitA_5 = value;
		Il2CppCodeGenWriteBarrier((&___loadUnitA_5), value);
	}

	inline static int32_t get_offset_of_loadUnitB_6() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___loadUnitB_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_loadUnitB_6() const { return ___loadUnitB_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_loadUnitB_6() { return &___loadUnitB_6; }
	inline void set_loadUnitB_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___loadUnitB_6 = value;
		Il2CppCodeGenWriteBarrier((&___loadUnitB_6), value);
	}

	inline static int32_t get_offset_of_loadUnitC_7() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___loadUnitC_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_loadUnitC_7() const { return ___loadUnitC_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_loadUnitC_7() { return &___loadUnitC_7; }
	inline void set_loadUnitC_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___loadUnitC_7 = value;
		Il2CppCodeGenWriteBarrier((&___loadUnitC_7), value);
	}

	inline static int32_t get_offset_of_calcDistance_8() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___calcDistance_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_calcDistance_8() const { return ___calcDistance_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_calcDistance_8() { return &___calcDistance_8; }
	inline void set_calcDistance_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___calcDistance_8 = value;
		Il2CppCodeGenWriteBarrier((&___calcDistance_8), value);
	}

	inline static int32_t get_offset_of_endCalcDistance_9() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___endCalcDistance_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_endCalcDistance_9() const { return ___endCalcDistance_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_endCalcDistance_9() { return &___endCalcDistance_9; }
	inline void set_endCalcDistance_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___endCalcDistance_9 = value;
		Il2CppCodeGenWriteBarrier((&___endCalcDistance_9), value);
	}

	inline static int32_t get_offset_of_calcArea_10() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___calcArea_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_calcArea_10() const { return ___calcArea_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_calcArea_10() { return &___calcArea_10; }
	inline void set_calcArea_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___calcArea_10 = value;
		Il2CppCodeGenWriteBarrier((&___calcArea_10), value);
	}

	inline static int32_t get_offset_of_endCalcArea_11() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___endCalcArea_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_endCalcArea_11() const { return ___endCalcArea_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_endCalcArea_11() { return &___endCalcArea_11; }
	inline void set_endCalcArea_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___endCalcArea_11 = value;
		Il2CppCodeGenWriteBarrier((&___endCalcArea_11), value);
	}

	inline static int32_t get_offset_of_loadProduct_12() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___loadProduct_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_loadProduct_12() const { return ___loadProduct_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_loadProduct_12() { return &___loadProduct_12; }
	inline void set_loadProduct_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___loadProduct_12 = value;
		Il2CppCodeGenWriteBarrier((&___loadProduct_12), value);
	}

	inline static int32_t get_offset_of_loadProduct2_13() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___loadProduct2_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_loadProduct2_13() const { return ___loadProduct2_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_loadProduct2_13() { return &___loadProduct2_13; }
	inline void set_loadProduct2_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___loadProduct2_13 = value;
		Il2CppCodeGenWriteBarrier((&___loadProduct2_13), value);
	}

	inline static int32_t get_offset_of_loadProduct3_14() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___loadProduct3_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_loadProduct3_14() const { return ___loadProduct3_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_loadProduct3_14() { return &___loadProduct3_14; }
	inline void set_loadProduct3_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___loadProduct3_14 = value;
		Il2CppCodeGenWriteBarrier((&___loadProduct3_14), value);
	}

	inline static int32_t get_offset_of_lightDirectionText_15() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___lightDirectionText_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_lightDirectionText_15() const { return ___lightDirectionText_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_lightDirectionText_15() { return &___lightDirectionText_15; }
	inline void set_lightDirectionText_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___lightDirectionText_15 = value;
		Il2CppCodeGenWriteBarrier((&___lightDirectionText_15), value);
	}

	inline static int32_t get_offset_of_lightDirection_16() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___lightDirection_16)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_lightDirection_16() const { return ___lightDirection_16; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_lightDirection_16() { return &___lightDirection_16; }
	inline void set_lightDirection_16(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___lightDirection_16 = value;
		Il2CppCodeGenWriteBarrier((&___lightDirection_16), value);
	}

	inline static int32_t get_offset_of_clearUnit_17() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___clearUnit_17)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_clearUnit_17() const { return ___clearUnit_17; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_clearUnit_17() { return &___clearUnit_17; }
	inline void set_clearUnit_17(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___clearUnit_17 = value;
		Il2CppCodeGenWriteBarrier((&___clearUnit_17), value);
	}

	inline static int32_t get_offset_of_requestMapAPIBtn_18() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___requestMapAPIBtn_18)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_requestMapAPIBtn_18() const { return ___requestMapAPIBtn_18; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_requestMapAPIBtn_18() { return &___requestMapAPIBtn_18; }
	inline void set_requestMapAPIBtn_18(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___requestMapAPIBtn_18 = value;
		Il2CppCodeGenWriteBarrier((&___requestMapAPIBtn_18), value);
	}

	inline static int32_t get_offset_of_inputFieldSearchAddress_19() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___inputFieldSearchAddress_19)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_inputFieldSearchAddress_19() const { return ___inputFieldSearchAddress_19; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_inputFieldSearchAddress_19() { return &___inputFieldSearchAddress_19; }
	inline void set_inputFieldSearchAddress_19(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___inputFieldSearchAddress_19 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldSearchAddress_19), value);
	}

	inline static int32_t get_offset_of_pinButton_20() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___pinButton_20)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_pinButton_20() const { return ___pinButton_20; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_pinButton_20() { return &___pinButton_20; }
	inline void set_pinButton_20(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___pinButton_20 = value;
		Il2CppCodeGenWriteBarrier((&___pinButton_20), value);
	}

	inline static int32_t get_offset_of_joystickCotrol_21() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___joystickCotrol_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_joystickCotrol_21() const { return ___joystickCotrol_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_joystickCotrol_21() { return &___joystickCotrol_21; }
	inline void set_joystickCotrol_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___joystickCotrol_21 = value;
		Il2CppCodeGenWriteBarrier((&___joystickCotrol_21), value);
	}

	inline static int32_t get_offset_of_viewRotationControls_22() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___viewRotationControls_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_viewRotationControls_22() const { return ___viewRotationControls_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_viewRotationControls_22() { return &___viewRotationControls_22; }
	inline void set_viewRotationControls_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___viewRotationControls_22 = value;
		Il2CppCodeGenWriteBarrier((&___viewRotationControls_22), value);
	}

	inline static int32_t get_offset_of__fpsViewMode_23() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____fpsViewMode_23)); }
	inline bool get__fpsViewMode_23() const { return ____fpsViewMode_23; }
	inline bool* get_address_of__fpsViewMode_23() { return &____fpsViewMode_23; }
	inline void set__fpsViewMode_23(bool value)
	{
		____fpsViewMode_23 = value;
	}

	inline static int32_t get_offset_of_topPanel_24() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___topPanel_24)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_topPanel_24() const { return ___topPanel_24; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_topPanel_24() { return &___topPanel_24; }
	inline void set_topPanel_24(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___topPanel_24 = value;
		Il2CppCodeGenWriteBarrier((&___topPanel_24), value);
	}

	inline static int32_t get_offset_of_rightMenuPanel_25() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___rightMenuPanel_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_rightMenuPanel_25() const { return ___rightMenuPanel_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_rightMenuPanel_25() { return &___rightMenuPanel_25; }
	inline void set_rightMenuPanel_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___rightMenuPanel_25 = value;
		Il2CppCodeGenWriteBarrier((&___rightMenuPanel_25), value);
	}

	inline static int32_t get_offset_of_backToMain_26() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___backToMain_26)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_backToMain_26() const { return ___backToMain_26; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_backToMain_26() { return &___backToMain_26; }
	inline void set_backToMain_26(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___backToMain_26 = value;
		Il2CppCodeGenWriteBarrier((&___backToMain_26), value);
	}

	inline static int32_t get_offset_of_screenShot_27() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___screenShot_27)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_screenShot_27() const { return ___screenShot_27; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_screenShot_27() { return &___screenShot_27; }
	inline void set_screenShot_27(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___screenShot_27 = value;
		Il2CppCodeGenWriteBarrier((&___screenShot_27), value);
	}

	inline static int32_t get_offset_of_alignment_28() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___alignment_28)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_alignment_28() const { return ___alignment_28; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_alignment_28() { return &___alignment_28; }
	inline void set_alignment_28(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___alignment_28 = value;
		Il2CppCodeGenWriteBarrier((&___alignment_28), value);
	}

	inline static int32_t get_offset_of_undo_29() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___undo_29)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_undo_29() const { return ___undo_29; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_undo_29() { return &___undo_29; }
	inline void set_undo_29(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___undo_29 = value;
		Il2CppCodeGenWriteBarrier((&___undo_29), value);
	}

	inline static int32_t get_offset_of_redo_30() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___redo_30)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_redo_30() const { return ___redo_30; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_redo_30() { return &___redo_30; }
	inline void set_redo_30(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___redo_30 = value;
		Il2CppCodeGenWriteBarrier((&___redo_30), value);
	}

	inline static int32_t get_offset_of_cameraModePanning_31() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___cameraModePanning_31)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_cameraModePanning_31() const { return ___cameraModePanning_31; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_cameraModePanning_31() { return &___cameraModePanning_31; }
	inline void set_cameraModePanning_31(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___cameraModePanning_31 = value;
		Il2CppCodeGenWriteBarrier((&___cameraModePanning_31), value);
	}

	inline static int32_t get_offset_of_cameraModeOrbit_32() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___cameraModeOrbit_32)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_cameraModeOrbit_32() const { return ___cameraModeOrbit_32; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_cameraModeOrbit_32() { return &___cameraModeOrbit_32; }
	inline void set_cameraModeOrbit_32(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___cameraModeOrbit_32 = value;
		Il2CppCodeGenWriteBarrier((&___cameraModeOrbit_32), value);
	}

	inline static int32_t get_offset_of_settings_33() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___settings_33)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_settings_33() const { return ___settings_33; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_settings_33() { return &___settings_33; }
	inline void set_settings_33(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___settings_33 = value;
		Il2CppCodeGenWriteBarrier((&___settings_33), value);
	}

	inline static int32_t get_offset_of_fpsModeToggle_34() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___fpsModeToggle_34)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_fpsModeToggle_34() const { return ___fpsModeToggle_34; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_fpsModeToggle_34() { return &___fpsModeToggle_34; }
	inline void set_fpsModeToggle_34(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___fpsModeToggle_34 = value;
		Il2CppCodeGenWriteBarrier((&___fpsModeToggle_34), value);
	}

	inline static int32_t get_offset_of_orbitModeToggle_35() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___orbitModeToggle_35)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_orbitModeToggle_35() const { return ___orbitModeToggle_35; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_orbitModeToggle_35() { return &___orbitModeToggle_35; }
	inline void set_orbitModeToggle_35(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___orbitModeToggle_35 = value;
		Il2CppCodeGenWriteBarrier((&___orbitModeToggle_35), value);
	}

	inline static int32_t get_offset_of_stopStyling_36() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___stopStyling_36)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_stopStyling_36() const { return ___stopStyling_36; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_stopStyling_36() { return &___stopStyling_36; }
	inline void set_stopStyling_36(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___stopStyling_36 = value;
		Il2CppCodeGenWriteBarrier((&___stopStyling_36), value);
	}

	inline static int32_t get_offset_of_canvas_37() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___canvas_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_37() const { return ___canvas_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_37() { return &___canvas_37; }
	inline void set_canvas_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_37 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_37), value);
	}

	inline static int32_t get_offset_of_productScrollList_38() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___productScrollList_38)); }
	inline ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD * get_productScrollList_38() const { return ___productScrollList_38; }
	inline ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD ** get_address_of_productScrollList_38() { return &___productScrollList_38; }
	inline void set_productScrollList_38(ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD * value)
	{
		___productScrollList_38 = value;
		Il2CppCodeGenWriteBarrier((&___productScrollList_38), value);
	}

	inline static int32_t get_offset_of_productListButton_39() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___productListButton_39)); }
	inline ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618 * get_productListButton_39() const { return ___productListButton_39; }
	inline ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618 ** get_address_of_productListButton_39() { return &___productListButton_39; }
	inline void set_productListButton_39(ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618 * value)
	{
		___productListButton_39 = value;
		Il2CppCodeGenWriteBarrier((&___productListButton_39), value);
	}

	inline static int32_t get_offset_of_showItemList_40() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___showItemList_40)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_showItemList_40() const { return ___showItemList_40; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_showItemList_40() { return &___showItemList_40; }
	inline void set_showItemList_40(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___showItemList_40 = value;
		Il2CppCodeGenWriteBarrier((&___showItemList_40), value);
	}

	inline static int32_t get_offset_of_itemListPanel_41() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___itemListPanel_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_itemListPanel_41() const { return ___itemListPanel_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_itemListPanel_41() { return &___itemListPanel_41; }
	inline void set_itemListPanel_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___itemListPanel_41 = value;
		Il2CppCodeGenWriteBarrier((&___itemListPanel_41), value);
	}

	inline static int32_t get_offset_of_hideItemList_42() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___hideItemList_42)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_hideItemList_42() const { return ___hideItemList_42; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_hideItemList_42() { return &___hideItemList_42; }
	inline void set_hideItemList_42(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___hideItemList_42 = value;
		Il2CppCodeGenWriteBarrier((&___hideItemList_42), value);
	}

	inline static int32_t get_offset_of_tabBed_43() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___tabBed_43)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_tabBed_43() const { return ___tabBed_43; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_tabBed_43() { return &___tabBed_43; }
	inline void set_tabBed_43(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___tabBed_43 = value;
		Il2CppCodeGenWriteBarrier((&___tabBed_43), value);
	}

	inline static int32_t get_offset_of_tabWall_44() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___tabWall_44)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_tabWall_44() const { return ___tabWall_44; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_tabWall_44() { return &___tabWall_44; }
	inline void set_tabWall_44(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___tabWall_44 = value;
		Il2CppCodeGenWriteBarrier((&___tabWall_44), value);
	}

	inline static int32_t get_offset_of_tabFloor_45() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___tabFloor_45)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_tabFloor_45() const { return ___tabFloor_45; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_tabFloor_45() { return &___tabFloor_45; }
	inline void set_tabFloor_45(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___tabFloor_45 = value;
		Il2CppCodeGenWriteBarrier((&___tabFloor_45), value);
	}

	inline static int32_t get_offset_of_wallpaperListPanel_46() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___wallpaperListPanel_46)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_wallpaperListPanel_46() const { return ___wallpaperListPanel_46; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_wallpaperListPanel_46() { return &___wallpaperListPanel_46; }
	inline void set_wallpaperListPanel_46(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___wallpaperListPanel_46 = value;
		Il2CppCodeGenWriteBarrier((&___wallpaperListPanel_46), value);
	}

	inline static int32_t get_offset_of_floorListPanel_47() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___floorListPanel_47)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_floorListPanel_47() const { return ___floorListPanel_47; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_floorListPanel_47() { return &___floorListPanel_47; }
	inline void set_floorListPanel_47(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___floorListPanel_47 = value;
		Il2CppCodeGenWriteBarrier((&___floorListPanel_47), value);
	}

	inline static int32_t get_offset_of_wallpaperButtons_48() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___wallpaperButtons_48)); }
	inline ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B* get_wallpaperButtons_48() const { return ___wallpaperButtons_48; }
	inline ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B** get_address_of_wallpaperButtons_48() { return &___wallpaperButtons_48; }
	inline void set_wallpaperButtons_48(ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B* value)
	{
		___wallpaperButtons_48 = value;
		Il2CppCodeGenWriteBarrier((&___wallpaperButtons_48), value);
	}

	inline static int32_t get_offset_of_floorButtons_49() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___floorButtons_49)); }
	inline ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B* get_floorButtons_49() const { return ___floorButtons_49; }
	inline ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B** get_address_of_floorButtons_49() { return &___floorButtons_49; }
	inline void set_floorButtons_49(ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B* value)
	{
		___floorButtons_49 = value;
		Il2CppCodeGenWriteBarrier((&___floorButtons_49), value);
	}

	inline static int32_t get_offset_of__panningMode_50() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____panningMode_50)); }
	inline bool get__panningMode_50() const { return ____panningMode_50; }
	inline bool* get_address_of__panningMode_50() { return &____panningMode_50; }
	inline void set__panningMode_50(bool value)
	{
		____panningMode_50 = value;
	}

	inline static int32_t get_offset_of__listOptionCombination_51() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____listOptionCombination_51)); }
	inline List_1_t5B38DDA976DDB66A172FD1855281F01EE9ED63F0 * get__listOptionCombination_51() const { return ____listOptionCombination_51; }
	inline List_1_t5B38DDA976DDB66A172FD1855281F01EE9ED63F0 ** get_address_of__listOptionCombination_51() { return &____listOptionCombination_51; }
	inline void set__listOptionCombination_51(List_1_t5B38DDA976DDB66A172FD1855281F01EE9ED63F0 * value)
	{
		____listOptionCombination_51 = value;
		Il2CppCodeGenWriteBarrier((&____listOptionCombination_51), value);
	}

	inline static int32_t get_offset_of__frameProductList_52() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____frameProductList_52)); }
	inline List_1_tFD496FCFB1F7C232809B0A18F6887706614F65F3 * get__frameProductList_52() const { return ____frameProductList_52; }
	inline List_1_tFD496FCFB1F7C232809B0A18F6887706614F65F3 ** get_address_of__frameProductList_52() { return &____frameProductList_52; }
	inline void set__frameProductList_52(List_1_tFD496FCFB1F7C232809B0A18F6887706614F65F3 * value)
	{
		____frameProductList_52 = value;
		Il2CppCodeGenWriteBarrier((&____frameProductList_52), value);
	}

	inline static int32_t get_offset_of__mattressProductList_53() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____mattressProductList_53)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__mattressProductList_53() const { return ____mattressProductList_53; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__mattressProductList_53() { return &____mattressProductList_53; }
	inline void set__mattressProductList_53(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____mattressProductList_53 = value;
		Il2CppCodeGenWriteBarrier((&____mattressProductList_53), value);
	}

	inline static int32_t get_offset_of__mattressPathMap_54() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____mattressPathMap_54)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__mattressPathMap_54() const { return ____mattressPathMap_54; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__mattressPathMap_54() { return &____mattressPathMap_54; }
	inline void set__mattressPathMap_54(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____mattressPathMap_54 = value;
		Il2CppCodeGenWriteBarrier((&____mattressPathMap_54), value);
	}

	inline static int32_t get_offset_of__mattressListSize_55() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____mattressListSize_55)); }
	inline Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * get__mattressListSize_55() const { return ____mattressListSize_55; }
	inline Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 ** get_address_of__mattressListSize_55() { return &____mattressListSize_55; }
	inline void set__mattressListSize_55(Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * value)
	{
		____mattressListSize_55 = value;
		Il2CppCodeGenWriteBarrier((&____mattressListSize_55), value);
	}

	inline static int32_t get_offset_of_productOptionUIManager_56() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___productOptionUIManager_56)); }
	inline ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87 * get_productOptionUIManager_56() const { return ___productOptionUIManager_56; }
	inline ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87 ** get_address_of_productOptionUIManager_56() { return &___productOptionUIManager_56; }
	inline void set_productOptionUIManager_56(ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87 * value)
	{
		___productOptionUIManager_56 = value;
		Il2CppCodeGenWriteBarrier((&___productOptionUIManager_56), value);
	}

	inline static int32_t get_offset_of_settingPanelController_57() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___settingPanelController_57)); }
	inline SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510 * get_settingPanelController_57() const { return ___settingPanelController_57; }
	inline SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510 ** get_address_of_settingPanelController_57() { return &___settingPanelController_57; }
	inline void set_settingPanelController_57(SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510 * value)
	{
		___settingPanelController_57 = value;
		Il2CppCodeGenWriteBarrier((&___settingPanelController_57), value);
	}

	inline static int32_t get_offset_of__wallpaperJsonMap_58() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____wallpaperJsonMap_58)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__wallpaperJsonMap_58() const { return ____wallpaperJsonMap_58; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__wallpaperJsonMap_58() { return &____wallpaperJsonMap_58; }
	inline void set__wallpaperJsonMap_58(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____wallpaperJsonMap_58 = value;
		Il2CppCodeGenWriteBarrier((&____wallpaperJsonMap_58), value);
	}

	inline static int32_t get_offset_of__floorJsonMap_59() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____floorJsonMap_59)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__floorJsonMap_59() const { return ____floorJsonMap_59; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__floorJsonMap_59() { return &____floorJsonMap_59; }
	inline void set__floorJsonMap_59(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____floorJsonMap_59 = value;
		Il2CppCodeGenWriteBarrier((&____floorJsonMap_59), value);
	}

	inline static int32_t get_offset_of__wallpaperIdList_60() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____wallpaperIdList_60)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__wallpaperIdList_60() const { return ____wallpaperIdList_60; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__wallpaperIdList_60() { return &____wallpaperIdList_60; }
	inline void set__wallpaperIdList_60(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____wallpaperIdList_60 = value;
		Il2CppCodeGenWriteBarrier((&____wallpaperIdList_60), value);
	}

	inline static int32_t get_offset_of__floorIdList_61() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____floorIdList_61)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__floorIdList_61() const { return ____floorIdList_61; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__floorIdList_61() { return &____floorIdList_61; }
	inline void set__floorIdList_61(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____floorIdList_61 = value;
		Il2CppCodeGenWriteBarrier((&____floorIdList_61), value);
	}

	inline static int32_t get_offset_of__productJsonList_62() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____productJsonList_62)); }
	inline List_1_t537386FE2177AFB667368B76654E8D513578BE29 * get__productJsonList_62() const { return ____productJsonList_62; }
	inline List_1_t537386FE2177AFB667368B76654E8D513578BE29 ** get_address_of__productJsonList_62() { return &____productJsonList_62; }
	inline void set__productJsonList_62(List_1_t537386FE2177AFB667368B76654E8D513578BE29 * value)
	{
		____productJsonList_62 = value;
		Il2CppCodeGenWriteBarrier((&____productJsonList_62), value);
	}

	inline static int32_t get_offset_of__pointLightsForUnit_63() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ____pointLightsForUnit_63)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get__pointLightsForUnit_63() const { return ____pointLightsForUnit_63; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of__pointLightsForUnit_63() { return &____pointLightsForUnit_63; }
	inline void set__pointLightsForUnit_63(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		____pointLightsForUnit_63 = value;
		Il2CppCodeGenWriteBarrier((&____pointLightsForUnit_63), value);
	}

	inline static int32_t get_offset_of_pointLightPrefab_64() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE, ___pointLightPrefab_64)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pointLightPrefab_64() const { return ___pointLightPrefab_64; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pointLightPrefab_64() { return &___pointLightPrefab_64; }
	inline void set_pointLightPrefab_64(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pointLightPrefab_64 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightPrefab_64), value);
	}
};

struct UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE_StaticFields
{
public:
	// System.String UIController::BASE_PATH
	String_t* ___BASE_PATH_4;

public:
	inline static int32_t get_offset_of_BASE_PATH_4() { return static_cast<int32_t>(offsetof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE_StaticFields, ___BASE_PATH_4)); }
	inline String_t* get_BASE_PATH_4() const { return ___BASE_PATH_4; }
	inline String_t** get_address_of_BASE_PATH_4() { return &___BASE_PATH_4; }
	inline void set_BASE_PATH_4(String_t* value)
	{
		___BASE_PATH_4 = value;
		Il2CppCodeGenWriteBarrier((&___BASE_PATH_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICONTROLLER_T06FB25185FB9B776EF16705D93C83EE95CE1D1EE_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef TEST_TBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246_H
#define TEST_TBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// test
struct  test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer[] test::mesh
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* ___mesh_4;
	// UnityEngine.Material[] test::material
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___material_5;
	// UnityEngine.Color[] test::col
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___col_6;
	// System.Single test::alpha
	float ___alpha_7;

public:
	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246, ___mesh_4)); }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* get_mesh_4() const { return ___mesh_4; }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246, ___material_5)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_material_5() const { return ___material_5; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_col_6() { return static_cast<int32_t>(offsetof(test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246, ___col_6)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_col_6() const { return ___col_6; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_col_6() { return &___col_6; }
	inline void set_col_6(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___col_6 = value;
		Il2CppCodeGenWriteBarrier((&___col_6), value);
	}

	inline static int32_t get_offset_of_alpha_7() { return static_cast<int32_t>(offsetof(test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246, ___alpha_7)); }
	inline float get_alpha_7() const { return ___alpha_7; }
	inline float* get_address_of_alpha_7() { return &___alpha_7; }
	inline void set_alpha_7(float value)
	{
		___alpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEST_TBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246_H
#ifndef ACESINGLETON_1_T8A0EA087B5B8AE910E71DA068C071DE3895A7A81_H
#define ACESINGLETON_1_T8A0EA087B5B8AE910E71DA068C071DE3895A7A81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceSingleton`1<ARUIController>
struct  AceSingleton_1_t8A0EA087B5B8AE910E71DA068C071DE3895A7A81  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:

public:
};

struct AceSingleton_1_t8A0EA087B5B8AE910E71DA068C071DE3895A7A81_StaticFields
{
public:
	// T AceSingleton`1::_instance
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1 * ____instance_8;
	// System.Object AceSingleton`1::_lock
	RuntimeObject * ____lock_9;
	// System.Boolean AceSingleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_10;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(AceSingleton_1_t8A0EA087B5B8AE910E71DA068C071DE3895A7A81_StaticFields, ____instance_8)); }
	inline ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1 * get__instance_8() const { return ____instance_8; }
	inline ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier((&____instance_8), value);
	}

	inline static int32_t get_offset_of__lock_9() { return static_cast<int32_t>(offsetof(AceSingleton_1_t8A0EA087B5B8AE910E71DA068C071DE3895A7A81_StaticFields, ____lock_9)); }
	inline RuntimeObject * get__lock_9() const { return ____lock_9; }
	inline RuntimeObject ** get_address_of__lock_9() { return &____lock_9; }
	inline void set__lock_9(RuntimeObject * value)
	{
		____lock_9 = value;
		Il2CppCodeGenWriteBarrier((&____lock_9), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_10() { return static_cast<int32_t>(offsetof(AceSingleton_1_t8A0EA087B5B8AE910E71DA068C071DE3895A7A81_StaticFields, ___applicationIsQuitting_10)); }
	inline bool get_applicationIsQuitting_10() const { return ___applicationIsQuitting_10; }
	inline bool* get_address_of_applicationIsQuitting_10() { return &___applicationIsQuitting_10; }
	inline void set_applicationIsQuitting_10(bool value)
	{
		___applicationIsQuitting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACESINGLETON_1_T8A0EA087B5B8AE910E71DA068C071DE3895A7A81_H
#ifndef ACESINGLETON_1_T240F0701B0402BD7549E844088F346CE3BB66A16_H
#define ACESINGLETON_1_T240F0701B0402BD7549E844088F346CE3BB66A16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceSingleton`1<DefaultMainCamera>
struct  AceSingleton_1_t240F0701B0402BD7549E844088F346CE3BB66A16  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:

public:
};

struct AceSingleton_1_t240F0701B0402BD7549E844088F346CE3BB66A16_StaticFields
{
public:
	// T AceSingleton`1::_instance
	DefaultMainCamera_t5CCBA3F2B0D86DA34C24E30E5149A7FD6B112B6C * ____instance_8;
	// System.Object AceSingleton`1::_lock
	RuntimeObject * ____lock_9;
	// System.Boolean AceSingleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_10;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(AceSingleton_1_t240F0701B0402BD7549E844088F346CE3BB66A16_StaticFields, ____instance_8)); }
	inline DefaultMainCamera_t5CCBA3F2B0D86DA34C24E30E5149A7FD6B112B6C * get__instance_8() const { return ____instance_8; }
	inline DefaultMainCamera_t5CCBA3F2B0D86DA34C24E30E5149A7FD6B112B6C ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(DefaultMainCamera_t5CCBA3F2B0D86DA34C24E30E5149A7FD6B112B6C * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier((&____instance_8), value);
	}

	inline static int32_t get_offset_of__lock_9() { return static_cast<int32_t>(offsetof(AceSingleton_1_t240F0701B0402BD7549E844088F346CE3BB66A16_StaticFields, ____lock_9)); }
	inline RuntimeObject * get__lock_9() const { return ____lock_9; }
	inline RuntimeObject ** get_address_of__lock_9() { return &____lock_9; }
	inline void set__lock_9(RuntimeObject * value)
	{
		____lock_9 = value;
		Il2CppCodeGenWriteBarrier((&____lock_9), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_10() { return static_cast<int32_t>(offsetof(AceSingleton_1_t240F0701B0402BD7549E844088F346CE3BB66A16_StaticFields, ___applicationIsQuitting_10)); }
	inline bool get_applicationIsQuitting_10() const { return ___applicationIsQuitting_10; }
	inline bool* get_address_of_applicationIsQuitting_10() { return &___applicationIsQuitting_10; }
	inline void set_applicationIsQuitting_10(bool value)
	{
		___applicationIsQuitting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACESINGLETON_1_T240F0701B0402BD7549E844088F346CE3BB66A16_H
#ifndef ACESINGLETON_1_T540130C7D96D421F44F6626E683954D64291A316_H
#define ACESINGLETON_1_T540130C7D96D421F44F6626E683954D64291A316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceSingleton`1<MobileManager>
struct  AceSingleton_1_t540130C7D96D421F44F6626E683954D64291A316  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:

public:
};

struct AceSingleton_1_t540130C7D96D421F44F6626E683954D64291A316_StaticFields
{
public:
	// T AceSingleton`1::_instance
	MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543 * ____instance_8;
	// System.Object AceSingleton`1::_lock
	RuntimeObject * ____lock_9;
	// System.Boolean AceSingleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_10;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(AceSingleton_1_t540130C7D96D421F44F6626E683954D64291A316_StaticFields, ____instance_8)); }
	inline MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543 * get__instance_8() const { return ____instance_8; }
	inline MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier((&____instance_8), value);
	}

	inline static int32_t get_offset_of__lock_9() { return static_cast<int32_t>(offsetof(AceSingleton_1_t540130C7D96D421F44F6626E683954D64291A316_StaticFields, ____lock_9)); }
	inline RuntimeObject * get__lock_9() const { return ____lock_9; }
	inline RuntimeObject ** get_address_of__lock_9() { return &____lock_9; }
	inline void set__lock_9(RuntimeObject * value)
	{
		____lock_9 = value;
		Il2CppCodeGenWriteBarrier((&____lock_9), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_10() { return static_cast<int32_t>(offsetof(AceSingleton_1_t540130C7D96D421F44F6626E683954D64291A316_StaticFields, ___applicationIsQuitting_10)); }
	inline bool get_applicationIsQuitting_10() const { return ___applicationIsQuitting_10; }
	inline bool* get_address_of_applicationIsQuitting_10() { return &___applicationIsQuitting_10; }
	inline void set_applicationIsQuitting_10(bool value)
	{
		___applicationIsQuitting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACESINGLETON_1_T540130C7D96D421F44F6626E683954D64291A316_H
#ifndef ACESINGLETON_1_TF21F82CEC86CA5D0D7E4541F037D397FF6F97CE5_H
#define ACESINGLETON_1_TF21F82CEC86CA5D0D7E4541F037D397FF6F97CE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceSingleton`1<ModelTargetController>
struct  AceSingleton_1_tF21F82CEC86CA5D0D7E4541F037D397FF6F97CE5  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:

public:
};

struct AceSingleton_1_tF21F82CEC86CA5D0D7E4541F037D397FF6F97CE5_StaticFields
{
public:
	// T AceSingleton`1::_instance
	ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A * ____instance_8;
	// System.Object AceSingleton`1::_lock
	RuntimeObject * ____lock_9;
	// System.Boolean AceSingleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_10;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(AceSingleton_1_tF21F82CEC86CA5D0D7E4541F037D397FF6F97CE5_StaticFields, ____instance_8)); }
	inline ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A * get__instance_8() const { return ____instance_8; }
	inline ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier((&____instance_8), value);
	}

	inline static int32_t get_offset_of__lock_9() { return static_cast<int32_t>(offsetof(AceSingleton_1_tF21F82CEC86CA5D0D7E4541F037D397FF6F97CE5_StaticFields, ____lock_9)); }
	inline RuntimeObject * get__lock_9() const { return ____lock_9; }
	inline RuntimeObject ** get_address_of__lock_9() { return &____lock_9; }
	inline void set__lock_9(RuntimeObject * value)
	{
		____lock_9 = value;
		Il2CppCodeGenWriteBarrier((&____lock_9), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_10() { return static_cast<int32_t>(offsetof(AceSingleton_1_tF21F82CEC86CA5D0D7E4541F037D397FF6F97CE5_StaticFields, ___applicationIsQuitting_10)); }
	inline bool get_applicationIsQuitting_10() const { return ___applicationIsQuitting_10; }
	inline bool* get_address_of_applicationIsQuitting_10() { return &___applicationIsQuitting_10; }
	inline void set_applicationIsQuitting_10(bool value)
	{
		___applicationIsQuitting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACESINGLETON_1_TF21F82CEC86CA5D0D7E4541F037D397FF6F97CE5_H
#ifndef ACESINGLETON_1_TB26A8D9141B90D6F5D5598A18B433B8379A34FC5_H
#define ACESINGLETON_1_TB26A8D9141B90D6F5D5598A18B433B8379A34FC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceSingleton`1<ModelTargetMenuController>
struct  AceSingleton_1_tB26A8D9141B90D6F5D5598A18B433B8379A34FC5  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:

public:
};

struct AceSingleton_1_tB26A8D9141B90D6F5D5598A18B433B8379A34FC5_StaticFields
{
public:
	// T AceSingleton`1::_instance
	ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237 * ____instance_8;
	// System.Object AceSingleton`1::_lock
	RuntimeObject * ____lock_9;
	// System.Boolean AceSingleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_10;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(AceSingleton_1_tB26A8D9141B90D6F5D5598A18B433B8379A34FC5_StaticFields, ____instance_8)); }
	inline ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237 * get__instance_8() const { return ____instance_8; }
	inline ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier((&____instance_8), value);
	}

	inline static int32_t get_offset_of__lock_9() { return static_cast<int32_t>(offsetof(AceSingleton_1_tB26A8D9141B90D6F5D5598A18B433B8379A34FC5_StaticFields, ____lock_9)); }
	inline RuntimeObject * get__lock_9() const { return ____lock_9; }
	inline RuntimeObject ** get_address_of__lock_9() { return &____lock_9; }
	inline void set__lock_9(RuntimeObject * value)
	{
		____lock_9 = value;
		Il2CppCodeGenWriteBarrier((&____lock_9), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_10() { return static_cast<int32_t>(offsetof(AceSingleton_1_tB26A8D9141B90D6F5D5598A18B433B8379A34FC5_StaticFields, ___applicationIsQuitting_10)); }
	inline bool get_applicationIsQuitting_10() const { return ___applicationIsQuitting_10; }
	inline bool* get_address_of_applicationIsQuitting_10() { return &___applicationIsQuitting_10; }
	inline void set_applicationIsQuitting_10(bool value)
	{
		___applicationIsQuitting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACESINGLETON_1_TB26A8D9141B90D6F5D5598A18B433B8379A34FC5_H
#ifndef ACESINGLETON_1_T11C1EE5F4E8F5C185C78A910C81A57A0BB192AC4_H
#define ACESINGLETON_1_T11C1EE5F4E8F5C185C78A910C81A57A0BB192AC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceSingleton`1<ProductViewController>
struct  AceSingleton_1_t11C1EE5F4E8F5C185C78A910C81A57A0BB192AC4  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:

public:
};

struct AceSingleton_1_t11C1EE5F4E8F5C185C78A910C81A57A0BB192AC4_StaticFields
{
public:
	// T AceSingleton`1::_instance
	ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0 * ____instance_8;
	// System.Object AceSingleton`1::_lock
	RuntimeObject * ____lock_9;
	// System.Boolean AceSingleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_10;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(AceSingleton_1_t11C1EE5F4E8F5C185C78A910C81A57A0BB192AC4_StaticFields, ____instance_8)); }
	inline ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0 * get__instance_8() const { return ____instance_8; }
	inline ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier((&____instance_8), value);
	}

	inline static int32_t get_offset_of__lock_9() { return static_cast<int32_t>(offsetof(AceSingleton_1_t11C1EE5F4E8F5C185C78A910C81A57A0BB192AC4_StaticFields, ____lock_9)); }
	inline RuntimeObject * get__lock_9() const { return ____lock_9; }
	inline RuntimeObject ** get_address_of__lock_9() { return &____lock_9; }
	inline void set__lock_9(RuntimeObject * value)
	{
		____lock_9 = value;
		Il2CppCodeGenWriteBarrier((&____lock_9), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_10() { return static_cast<int32_t>(offsetof(AceSingleton_1_t11C1EE5F4E8F5C185C78A910C81A57A0BB192AC4_StaticFields, ___applicationIsQuitting_10)); }
	inline bool get_applicationIsQuitting_10() const { return ___applicationIsQuitting_10; }
	inline bool* get_address_of_applicationIsQuitting_10() { return &___applicationIsQuitting_10; }
	inline void set_applicationIsQuitting_10(bool value)
	{
		___applicationIsQuitting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACESINGLETON_1_T11C1EE5F4E8F5C185C78A910C81A57A0BB192AC4_H
#ifndef ACESINGLETON_1_T73730D3AF237C36C9AFBE1DE2458F56FF0180612_H
#define ACESINGLETON_1_T73730D3AF237C36C9AFBE1DE2458F56FF0180612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceSingleton`1<RoomSceneUIController>
struct  AceSingleton_1_t73730D3AF237C36C9AFBE1DE2458F56FF0180612  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:

public:
};

struct AceSingleton_1_t73730D3AF237C36C9AFBE1DE2458F56FF0180612_StaticFields
{
public:
	// T AceSingleton`1::_instance
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * ____instance_8;
	// System.Object AceSingleton`1::_lock
	RuntimeObject * ____lock_9;
	// System.Boolean AceSingleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_10;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(AceSingleton_1_t73730D3AF237C36C9AFBE1DE2458F56FF0180612_StaticFields, ____instance_8)); }
	inline RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * get__instance_8() const { return ____instance_8; }
	inline RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier((&____instance_8), value);
	}

	inline static int32_t get_offset_of__lock_9() { return static_cast<int32_t>(offsetof(AceSingleton_1_t73730D3AF237C36C9AFBE1DE2458F56FF0180612_StaticFields, ____lock_9)); }
	inline RuntimeObject * get__lock_9() const { return ____lock_9; }
	inline RuntimeObject ** get_address_of__lock_9() { return &____lock_9; }
	inline void set__lock_9(RuntimeObject * value)
	{
		____lock_9 = value;
		Il2CppCodeGenWriteBarrier((&____lock_9), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_10() { return static_cast<int32_t>(offsetof(AceSingleton_1_t73730D3AF237C36C9AFBE1DE2458F56FF0180612_StaticFields, ___applicationIsQuitting_10)); }
	inline bool get_applicationIsQuitting_10() const { return ___applicationIsQuitting_10; }
	inline bool* get_address_of_applicationIsQuitting_10() { return &___applicationIsQuitting_10; }
	inline void set_applicationIsQuitting_10(bool value)
	{
		___applicationIsQuitting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACESINGLETON_1_T73730D3AF237C36C9AFBE1DE2458F56FF0180612_H
#ifndef ACETRACKABLEEVENTHANDLER_T1C1007F64F025B29E9AB06AFD048456F53F42C2A_H
#define ACETRACKABLEEVENTHANDLER_T1C1007F64F025B29E9AB06AFD048456F53F42C2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AceTrackableEventHandler
struct  AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:
	// Vuforia.TrackableBehaviour AceTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_8;
	// Vuforia.TrackableBehaviour_Status AceTrackableEventHandler::m_PreviousStatus
	int32_t ___m_PreviousStatus_9;
	// Vuforia.TrackableBehaviour_Status AceTrackableEventHandler::m_NewStatus
	int32_t ___m_NewStatus_10;
	// UnityEngine.GameObject AceTrackableEventHandler::guidePanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___guidePanel_11;
	// UnityEngine.UI.Text AceTrackableEventHandler::textTestInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___textTestInfo_12;
	// System.Int32 AceTrackableEventHandler::count
	int32_t ___count_13;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_8() { return static_cast<int32_t>(offsetof(AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A, ___mTrackableBehaviour_8)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_8() const { return ___mTrackableBehaviour_8; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_8() { return &___mTrackableBehaviour_8; }
	inline void set_mTrackableBehaviour_8(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_8), value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_9() { return static_cast<int32_t>(offsetof(AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A, ___m_PreviousStatus_9)); }
	inline int32_t get_m_PreviousStatus_9() const { return ___m_PreviousStatus_9; }
	inline int32_t* get_address_of_m_PreviousStatus_9() { return &___m_PreviousStatus_9; }
	inline void set_m_PreviousStatus_9(int32_t value)
	{
		___m_PreviousStatus_9 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_10() { return static_cast<int32_t>(offsetof(AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A, ___m_NewStatus_10)); }
	inline int32_t get_m_NewStatus_10() const { return ___m_NewStatus_10; }
	inline int32_t* get_address_of_m_NewStatus_10() { return &___m_NewStatus_10; }
	inline void set_m_NewStatus_10(int32_t value)
	{
		___m_NewStatus_10 = value;
	}

	inline static int32_t get_offset_of_guidePanel_11() { return static_cast<int32_t>(offsetof(AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A, ___guidePanel_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_guidePanel_11() const { return ___guidePanel_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_guidePanel_11() { return &___guidePanel_11; }
	inline void set_guidePanel_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___guidePanel_11 = value;
		Il2CppCodeGenWriteBarrier((&___guidePanel_11), value);
	}

	inline static int32_t get_offset_of_textTestInfo_12() { return static_cast<int32_t>(offsetof(AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A, ___textTestInfo_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_textTestInfo_12() const { return ___textTestInfo_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_textTestInfo_12() { return &___textTestInfo_12; }
	inline void set_textTestInfo_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___textTestInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___textTestInfo_12), value);
	}

	inline static int32_t get_offset_of_count_13() { return static_cast<int32_t>(offsetof(AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A, ___count_13)); }
	inline int32_t get_count_13() const { return ___count_13; }
	inline int32_t* get_address_of_count_13() { return &___count_13; }
	inline void set_count_13(int32_t value)
	{
		___count_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACETRACKABLEEVENTHANDLER_T1C1007F64F025B29E9AB06AFD048456F53F42C2A_H
#ifndef COLOURVARIABLESETTING_T6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B_H
#define COLOURVARIABLESETTING_T6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColourVariableSetting
struct  ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B  : public BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB
{
public:
	// UnityEngine.UI.Image ColourVariableSetting::m_fromValue
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_fromValue_10;
	// UnityEngine.UI.Image ColourVariableSetting::m_toValue
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_toValue_11;
	// UnityEngine.UI.Image ColourVariableSetting::m_thenValue
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_thenValue_12;
	// UnityEngine.GameObject ColourVariableSetting::m_fromHighlight
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_fromHighlight_13;
	// UnityEngine.GameObject ColourVariableSetting::m_toHighlight
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_toHighlight_14;
	// UnityEngine.GameObject ColourVariableSetting::m_thenHighlight
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_thenHighlight_15;
	// UnityEngine.GameObject ColourVariableSetting::m_toValueObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_toValueObject_16;
	// UnityEngine.GameObject ColourVariableSetting::m_thenValueObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_thenValueObject_17;
	// UnityEngine.GameObject ColourVariableSetting::m_colourPickerSection
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_colourPickerSection_18;
	// HSVPickerDemo.HSVPicker ColourVariableSetting::m_colourPicker
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * ___m_colourPicker_19;
	// UnityEngine.UI.LayoutElement ColourVariableSetting::m_sectionLayoutElement
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___m_sectionLayoutElement_20;
	// System.Boolean ColourVariableSetting::m_colourSelected
	bool ___m_colourSelected_21;
	// System.Int32 ColourVariableSetting::m_currentColourIndex
	int32_t ___m_currentColourIndex_22;
	// System.Collections.Generic.List`1<System.Action`1<UnityEngine.Color>> ColourVariableSetting::m_stateListenerCallbacks
	List_1_t6E9DEDB83D3F4749BFC9685BCF6880091E6C2546 * ___m_stateListenerCallbacks_23;
	// System.Action ColourVariableSetting::m_valueChangedCallback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_valueChangedCallback_24;
	// UnityEngine.UI.Image[] ColourVariableSetting::m_colours
	ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* ___m_colours_25;

public:
	inline static int32_t get_offset_of_m_fromValue_10() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_fromValue_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_fromValue_10() const { return ___m_fromValue_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_fromValue_10() { return &___m_fromValue_10; }
	inline void set_m_fromValue_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_fromValue_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_fromValue_10), value);
	}

	inline static int32_t get_offset_of_m_toValue_11() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_toValue_11)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_toValue_11() const { return ___m_toValue_11; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_toValue_11() { return &___m_toValue_11; }
	inline void set_m_toValue_11(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_toValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_toValue_11), value);
	}

	inline static int32_t get_offset_of_m_thenValue_12() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_thenValue_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_thenValue_12() const { return ___m_thenValue_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_thenValue_12() { return &___m_thenValue_12; }
	inline void set_m_thenValue_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_thenValue_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_thenValue_12), value);
	}

	inline static int32_t get_offset_of_m_fromHighlight_13() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_fromHighlight_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_fromHighlight_13() const { return ___m_fromHighlight_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_fromHighlight_13() { return &___m_fromHighlight_13; }
	inline void set_m_fromHighlight_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_fromHighlight_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_fromHighlight_13), value);
	}

	inline static int32_t get_offset_of_m_toHighlight_14() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_toHighlight_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_toHighlight_14() const { return ___m_toHighlight_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_toHighlight_14() { return &___m_toHighlight_14; }
	inline void set_m_toHighlight_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_toHighlight_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_toHighlight_14), value);
	}

	inline static int32_t get_offset_of_m_thenHighlight_15() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_thenHighlight_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_thenHighlight_15() const { return ___m_thenHighlight_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_thenHighlight_15() { return &___m_thenHighlight_15; }
	inline void set_m_thenHighlight_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_thenHighlight_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_thenHighlight_15), value);
	}

	inline static int32_t get_offset_of_m_toValueObject_16() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_toValueObject_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_toValueObject_16() const { return ___m_toValueObject_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_toValueObject_16() { return &___m_toValueObject_16; }
	inline void set_m_toValueObject_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_toValueObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_toValueObject_16), value);
	}

	inline static int32_t get_offset_of_m_thenValueObject_17() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_thenValueObject_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_thenValueObject_17() const { return ___m_thenValueObject_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_thenValueObject_17() { return &___m_thenValueObject_17; }
	inline void set_m_thenValueObject_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_thenValueObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_thenValueObject_17), value);
	}

	inline static int32_t get_offset_of_m_colourPickerSection_18() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_colourPickerSection_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_colourPickerSection_18() const { return ___m_colourPickerSection_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_colourPickerSection_18() { return &___m_colourPickerSection_18; }
	inline void set_m_colourPickerSection_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_colourPickerSection_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_colourPickerSection_18), value);
	}

	inline static int32_t get_offset_of_m_colourPicker_19() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_colourPicker_19)); }
	inline HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * get_m_colourPicker_19() const { return ___m_colourPicker_19; }
	inline HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 ** get_address_of_m_colourPicker_19() { return &___m_colourPicker_19; }
	inline void set_m_colourPicker_19(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * value)
	{
		___m_colourPicker_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_colourPicker_19), value);
	}

	inline static int32_t get_offset_of_m_sectionLayoutElement_20() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_sectionLayoutElement_20)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_m_sectionLayoutElement_20() const { return ___m_sectionLayoutElement_20; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_m_sectionLayoutElement_20() { return &___m_sectionLayoutElement_20; }
	inline void set_m_sectionLayoutElement_20(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___m_sectionLayoutElement_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_sectionLayoutElement_20), value);
	}

	inline static int32_t get_offset_of_m_colourSelected_21() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_colourSelected_21)); }
	inline bool get_m_colourSelected_21() const { return ___m_colourSelected_21; }
	inline bool* get_address_of_m_colourSelected_21() { return &___m_colourSelected_21; }
	inline void set_m_colourSelected_21(bool value)
	{
		___m_colourSelected_21 = value;
	}

	inline static int32_t get_offset_of_m_currentColourIndex_22() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_currentColourIndex_22)); }
	inline int32_t get_m_currentColourIndex_22() const { return ___m_currentColourIndex_22; }
	inline int32_t* get_address_of_m_currentColourIndex_22() { return &___m_currentColourIndex_22; }
	inline void set_m_currentColourIndex_22(int32_t value)
	{
		___m_currentColourIndex_22 = value;
	}

	inline static int32_t get_offset_of_m_stateListenerCallbacks_23() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_stateListenerCallbacks_23)); }
	inline List_1_t6E9DEDB83D3F4749BFC9685BCF6880091E6C2546 * get_m_stateListenerCallbacks_23() const { return ___m_stateListenerCallbacks_23; }
	inline List_1_t6E9DEDB83D3F4749BFC9685BCF6880091E6C2546 ** get_address_of_m_stateListenerCallbacks_23() { return &___m_stateListenerCallbacks_23; }
	inline void set_m_stateListenerCallbacks_23(List_1_t6E9DEDB83D3F4749BFC9685BCF6880091E6C2546 * value)
	{
		___m_stateListenerCallbacks_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateListenerCallbacks_23), value);
	}

	inline static int32_t get_offset_of_m_valueChangedCallback_24() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_valueChangedCallback_24)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_valueChangedCallback_24() const { return ___m_valueChangedCallback_24; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_valueChangedCallback_24() { return &___m_valueChangedCallback_24; }
	inline void set_m_valueChangedCallback_24(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_valueChangedCallback_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_valueChangedCallback_24), value);
	}

	inline static int32_t get_offset_of_m_colours_25() { return static_cast<int32_t>(offsetof(ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B, ___m_colours_25)); }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* get_m_colours_25() const { return ___m_colours_25; }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D** get_address_of_m_colours_25() { return &___m_colours_25; }
	inline void set_m_colours_25(ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* value)
	{
		___m_colours_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_colours_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOURVARIABLESETTING_T6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B_H
#ifndef DEFAULTSCENEUICONTROLLER_T4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21_H
#define DEFAULTSCENEUICONTROLLER_T4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultSceneUIController
struct  DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:
	// UnityEngine.UI.Button DefaultSceneUIController::btnLoadARTargetScene
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnLoadARTargetScene_8;
	// UnityEngine.UI.Button DefaultSceneUIController::btnLoadRoomScene
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnLoadRoomScene_9;
	// UnityEngine.UI.Button DefaultSceneUIController::btnLoadUrbanRoomScene
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnLoadUrbanRoomScene_10;
	// UnityEngine.UI.Button DefaultSceneUIController::btnLoadUrbanProductViewerScene
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnLoadUrbanProductViewerScene_11;
	// UnityEngine.UI.Button DefaultSceneUIController::btnLoadModeltTargetScene
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnLoadModeltTargetScene_12;
	// UnityEngine.UI.Button DefaultSceneUIController::btnCallToast
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnCallToast_13;
	// UnityEngine.UI.Button DefaultSceneUIController::btnVersionCheck
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnVersionCheck_14;
	// UnityEngine.UI.Button DefaultSceneUIController::btnLoadVuTestScene1
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnLoadVuTestScene1_15;
	// UnityEngine.UI.Button DefaultSceneUIController::btnLoadVuTestScene2
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnLoadVuTestScene2_16;
	// UnityEngine.GameObject DefaultSceneUIController::panelTest
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___panelTest_17;

public:
	inline static int32_t get_offset_of_btnLoadARTargetScene_8() { return static_cast<int32_t>(offsetof(DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21, ___btnLoadARTargetScene_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnLoadARTargetScene_8() const { return ___btnLoadARTargetScene_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnLoadARTargetScene_8() { return &___btnLoadARTargetScene_8; }
	inline void set_btnLoadARTargetScene_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnLoadARTargetScene_8 = value;
		Il2CppCodeGenWriteBarrier((&___btnLoadARTargetScene_8), value);
	}

	inline static int32_t get_offset_of_btnLoadRoomScene_9() { return static_cast<int32_t>(offsetof(DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21, ___btnLoadRoomScene_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnLoadRoomScene_9() const { return ___btnLoadRoomScene_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnLoadRoomScene_9() { return &___btnLoadRoomScene_9; }
	inline void set_btnLoadRoomScene_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnLoadRoomScene_9 = value;
		Il2CppCodeGenWriteBarrier((&___btnLoadRoomScene_9), value);
	}

	inline static int32_t get_offset_of_btnLoadUrbanRoomScene_10() { return static_cast<int32_t>(offsetof(DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21, ___btnLoadUrbanRoomScene_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnLoadUrbanRoomScene_10() const { return ___btnLoadUrbanRoomScene_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnLoadUrbanRoomScene_10() { return &___btnLoadUrbanRoomScene_10; }
	inline void set_btnLoadUrbanRoomScene_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnLoadUrbanRoomScene_10 = value;
		Il2CppCodeGenWriteBarrier((&___btnLoadUrbanRoomScene_10), value);
	}

	inline static int32_t get_offset_of_btnLoadUrbanProductViewerScene_11() { return static_cast<int32_t>(offsetof(DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21, ___btnLoadUrbanProductViewerScene_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnLoadUrbanProductViewerScene_11() const { return ___btnLoadUrbanProductViewerScene_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnLoadUrbanProductViewerScene_11() { return &___btnLoadUrbanProductViewerScene_11; }
	inline void set_btnLoadUrbanProductViewerScene_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnLoadUrbanProductViewerScene_11 = value;
		Il2CppCodeGenWriteBarrier((&___btnLoadUrbanProductViewerScene_11), value);
	}

	inline static int32_t get_offset_of_btnLoadModeltTargetScene_12() { return static_cast<int32_t>(offsetof(DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21, ___btnLoadModeltTargetScene_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnLoadModeltTargetScene_12() const { return ___btnLoadModeltTargetScene_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnLoadModeltTargetScene_12() { return &___btnLoadModeltTargetScene_12; }
	inline void set_btnLoadModeltTargetScene_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnLoadModeltTargetScene_12 = value;
		Il2CppCodeGenWriteBarrier((&___btnLoadModeltTargetScene_12), value);
	}

	inline static int32_t get_offset_of_btnCallToast_13() { return static_cast<int32_t>(offsetof(DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21, ___btnCallToast_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnCallToast_13() const { return ___btnCallToast_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnCallToast_13() { return &___btnCallToast_13; }
	inline void set_btnCallToast_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnCallToast_13 = value;
		Il2CppCodeGenWriteBarrier((&___btnCallToast_13), value);
	}

	inline static int32_t get_offset_of_btnVersionCheck_14() { return static_cast<int32_t>(offsetof(DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21, ___btnVersionCheck_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnVersionCheck_14() const { return ___btnVersionCheck_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnVersionCheck_14() { return &___btnVersionCheck_14; }
	inline void set_btnVersionCheck_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnVersionCheck_14 = value;
		Il2CppCodeGenWriteBarrier((&___btnVersionCheck_14), value);
	}

	inline static int32_t get_offset_of_btnLoadVuTestScene1_15() { return static_cast<int32_t>(offsetof(DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21, ___btnLoadVuTestScene1_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnLoadVuTestScene1_15() const { return ___btnLoadVuTestScene1_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnLoadVuTestScene1_15() { return &___btnLoadVuTestScene1_15; }
	inline void set_btnLoadVuTestScene1_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnLoadVuTestScene1_15 = value;
		Il2CppCodeGenWriteBarrier((&___btnLoadVuTestScene1_15), value);
	}

	inline static int32_t get_offset_of_btnLoadVuTestScene2_16() { return static_cast<int32_t>(offsetof(DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21, ___btnLoadVuTestScene2_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnLoadVuTestScene2_16() const { return ___btnLoadVuTestScene2_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnLoadVuTestScene2_16() { return &___btnLoadVuTestScene2_16; }
	inline void set_btnLoadVuTestScene2_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnLoadVuTestScene2_16 = value;
		Il2CppCodeGenWriteBarrier((&___btnLoadVuTestScene2_16), value);
	}

	inline static int32_t get_offset_of_panelTest_17() { return static_cast<int32_t>(offsetof(DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21, ___panelTest_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_panelTest_17() const { return ___panelTest_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_panelTest_17() { return &___panelTest_17; }
	inline void set_panelTest_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___panelTest_17 = value;
		Il2CppCodeGenWriteBarrier((&___panelTest_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSCENEUICONTROLLER_T4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21_H
#ifndef FLOATVARIABLESETTING_TF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3_H
#define FLOATVARIABLESETTING_TF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatVariableSetting
struct  FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3  : public BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB
{
public:
	// UnityEngine.UI.InputField FloatVariableSetting::m_fromValue
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___m_fromValue_10;
	// UnityEngine.UI.InputField FloatVariableSetting::m_toValue
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___m_toValue_11;
	// UnityEngine.UI.InputField FloatVariableSetting::m_thenValue
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___m_thenValue_12;
	// UnityEngine.GameObject FloatVariableSetting::m_toValueObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_toValueObject_13;
	// UnityEngine.GameObject FloatVariableSetting::m_thenValueObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_thenValueObject_14;

public:
	inline static int32_t get_offset_of_m_fromValue_10() { return static_cast<int32_t>(offsetof(FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3, ___m_fromValue_10)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_m_fromValue_10() const { return ___m_fromValue_10; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_m_fromValue_10() { return &___m_fromValue_10; }
	inline void set_m_fromValue_10(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___m_fromValue_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_fromValue_10), value);
	}

	inline static int32_t get_offset_of_m_toValue_11() { return static_cast<int32_t>(offsetof(FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3, ___m_toValue_11)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_m_toValue_11() const { return ___m_toValue_11; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_m_toValue_11() { return &___m_toValue_11; }
	inline void set_m_toValue_11(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___m_toValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_toValue_11), value);
	}

	inline static int32_t get_offset_of_m_thenValue_12() { return static_cast<int32_t>(offsetof(FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3, ___m_thenValue_12)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_m_thenValue_12() const { return ___m_thenValue_12; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_m_thenValue_12() { return &___m_thenValue_12; }
	inline void set_m_thenValue_12(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___m_thenValue_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_thenValue_12), value);
	}

	inline static int32_t get_offset_of_m_toValueObject_13() { return static_cast<int32_t>(offsetof(FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3, ___m_toValueObject_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_toValueObject_13() const { return ___m_toValueObject_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_toValueObject_13() { return &___m_toValueObject_13; }
	inline void set_m_toValueObject_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_toValueObject_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_toValueObject_13), value);
	}

	inline static int32_t get_offset_of_m_thenValueObject_14() { return static_cast<int32_t>(offsetof(FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3, ___m_thenValueObject_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_thenValueObject_14() const { return ___m_thenValueObject_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_thenValueObject_14() { return &___m_thenValueObject_14; }
	inline void set_m_thenValueObject_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_thenValueObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_thenValueObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATVARIABLESETTING_TF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3_H
#ifndef IMPORTCOREDLL_T6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692_H
#define IMPORTCOREDLL_T6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImportCoreDLL
struct  ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692  : public Singleton_1_t4FD9E70C85FC3861708122924623B217140B5723
{
public:
	// UnityEngine.TextAsset ImportCoreDLL::assembly
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___assembly_7;
	// System.String ImportCoreDLL::gainedKey
	String_t* ___gainedKey_8;
	// System.String ImportCoreDLL::gainedIv
	String_t* ___gainedIv_9;
	// DLLCore.Main ImportCoreDLL::_main
	Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83 * ____main_10;
	// DLLCore.GeneralSetting ImportCoreDLL::generalSetting
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * ___generalSetting_11;
	// DLLCore.GraphicSetting ImportCoreDLL::graphicSetting
	GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 * ___graphicSetting_12;
	// UnityEngine.GameObject ImportCoreDLL::terminal
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___terminal_13;
	// DLLCore.EventManager ImportCoreDLL::eventManager
	EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966 * ___eventManager_14;
	// DLLCore.ReflectionControls ImportCoreDLL::reflectionControls
	ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1 * ___reflectionControls_15;
	// DLLCore.DaylightController ImportCoreDLL::daylightController
	DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2 * ___daylightController_16;
	// DLLCore.MaterialLoader ImportCoreDLL::materialLoader
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * ___materialLoader_17;
	// DLLCore.ModelLoader ImportCoreDLL::modelLoader
	ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * ___modelLoader_18;

public:
	inline static int32_t get_offset_of_assembly_7() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___assembly_7)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_assembly_7() const { return ___assembly_7; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_assembly_7() { return &___assembly_7; }
	inline void set_assembly_7(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___assembly_7 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_7), value);
	}

	inline static int32_t get_offset_of_gainedKey_8() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___gainedKey_8)); }
	inline String_t* get_gainedKey_8() const { return ___gainedKey_8; }
	inline String_t** get_address_of_gainedKey_8() { return &___gainedKey_8; }
	inline void set_gainedKey_8(String_t* value)
	{
		___gainedKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___gainedKey_8), value);
	}

	inline static int32_t get_offset_of_gainedIv_9() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___gainedIv_9)); }
	inline String_t* get_gainedIv_9() const { return ___gainedIv_9; }
	inline String_t** get_address_of_gainedIv_9() { return &___gainedIv_9; }
	inline void set_gainedIv_9(String_t* value)
	{
		___gainedIv_9 = value;
		Il2CppCodeGenWriteBarrier((&___gainedIv_9), value);
	}

	inline static int32_t get_offset_of__main_10() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ____main_10)); }
	inline Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83 * get__main_10() const { return ____main_10; }
	inline Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83 ** get_address_of__main_10() { return &____main_10; }
	inline void set__main_10(Main_t9518C93280EA9A864C63D04EA6A44DAE6CF9DF83 * value)
	{
		____main_10 = value;
		Il2CppCodeGenWriteBarrier((&____main_10), value);
	}

	inline static int32_t get_offset_of_generalSetting_11() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___generalSetting_11)); }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * get_generalSetting_11() const { return ___generalSetting_11; }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D ** get_address_of_generalSetting_11() { return &___generalSetting_11; }
	inline void set_generalSetting_11(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * value)
	{
		___generalSetting_11 = value;
		Il2CppCodeGenWriteBarrier((&___generalSetting_11), value);
	}

	inline static int32_t get_offset_of_graphicSetting_12() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___graphicSetting_12)); }
	inline GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 * get_graphicSetting_12() const { return ___graphicSetting_12; }
	inline GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 ** get_address_of_graphicSetting_12() { return &___graphicSetting_12; }
	inline void set_graphicSetting_12(GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 * value)
	{
		___graphicSetting_12 = value;
		Il2CppCodeGenWriteBarrier((&___graphicSetting_12), value);
	}

	inline static int32_t get_offset_of_terminal_13() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___terminal_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_terminal_13() const { return ___terminal_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_terminal_13() { return &___terminal_13; }
	inline void set_terminal_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___terminal_13 = value;
		Il2CppCodeGenWriteBarrier((&___terminal_13), value);
	}

	inline static int32_t get_offset_of_eventManager_14() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___eventManager_14)); }
	inline EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966 * get_eventManager_14() const { return ___eventManager_14; }
	inline EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966 ** get_address_of_eventManager_14() { return &___eventManager_14; }
	inline void set_eventManager_14(EventManager_tDFEE5D55B262AC7EF90C9D292267D1A0D3B3F966 * value)
	{
		___eventManager_14 = value;
		Il2CppCodeGenWriteBarrier((&___eventManager_14), value);
	}

	inline static int32_t get_offset_of_reflectionControls_15() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___reflectionControls_15)); }
	inline ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1 * get_reflectionControls_15() const { return ___reflectionControls_15; }
	inline ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1 ** get_address_of_reflectionControls_15() { return &___reflectionControls_15; }
	inline void set_reflectionControls_15(ReflectionControls_tF4260F82066379B747D088BFA20909E0B6DF3AA1 * value)
	{
		___reflectionControls_15 = value;
		Il2CppCodeGenWriteBarrier((&___reflectionControls_15), value);
	}

	inline static int32_t get_offset_of_daylightController_16() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___daylightController_16)); }
	inline DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2 * get_daylightController_16() const { return ___daylightController_16; }
	inline DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2 ** get_address_of_daylightController_16() { return &___daylightController_16; }
	inline void set_daylightController_16(DaylightController_t72313C4151D9FABA537C31965E03B41E05941BD2 * value)
	{
		___daylightController_16 = value;
		Il2CppCodeGenWriteBarrier((&___daylightController_16), value);
	}

	inline static int32_t get_offset_of_materialLoader_17() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___materialLoader_17)); }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * get_materialLoader_17() const { return ___materialLoader_17; }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE ** get_address_of_materialLoader_17() { return &___materialLoader_17; }
	inline void set_materialLoader_17(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * value)
	{
		___materialLoader_17 = value;
		Il2CppCodeGenWriteBarrier((&___materialLoader_17), value);
	}

	inline static int32_t get_offset_of_modelLoader_18() { return static_cast<int32_t>(offsetof(ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692, ___modelLoader_18)); }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * get_modelLoader_18() const { return ___modelLoader_18; }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D ** get_address_of_modelLoader_18() { return &___modelLoader_18; }
	inline void set_modelLoader_18(ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * value)
	{
		___modelLoader_18 = value;
		Il2CppCodeGenWriteBarrier((&___modelLoader_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTCOREDLL_T6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692_H
#ifndef MODALCONTROLLER_TD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD_H
#define MODALCONTROLLER_TD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModalController
struct  ModalController_tD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:
	// ModalController_CallbackType ModalController::callbackType
	CallbackType_tE5CC0E106E4AF1B685B5B9940F689A2464C63C41 * ___callbackType_8;
	// ModalController_Callback ModalController::callback
	Callback_t5433D5269B1108D8BFECDCD02079FCB1211A6CF8 * ___callback_9;
	// ModalController_CallbackClose ModalController::callbackClose
	CallbackClose_t4CBEE1DC735AF36C5814923F4360DBB1949CD115 * ___callbackClose_10;
	// ModalController_CallbackPop ModalController::callbackPop
	CallbackPop_t11746E625CF12C9917AD4DB2B9A57E2597D32609 * ___callbackPop_11;

public:
	inline static int32_t get_offset_of_callbackType_8() { return static_cast<int32_t>(offsetof(ModalController_tD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD, ___callbackType_8)); }
	inline CallbackType_tE5CC0E106E4AF1B685B5B9940F689A2464C63C41 * get_callbackType_8() const { return ___callbackType_8; }
	inline CallbackType_tE5CC0E106E4AF1B685B5B9940F689A2464C63C41 ** get_address_of_callbackType_8() { return &___callbackType_8; }
	inline void set_callbackType_8(CallbackType_tE5CC0E106E4AF1B685B5B9940F689A2464C63C41 * value)
	{
		___callbackType_8 = value;
		Il2CppCodeGenWriteBarrier((&___callbackType_8), value);
	}

	inline static int32_t get_offset_of_callback_9() { return static_cast<int32_t>(offsetof(ModalController_tD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD, ___callback_9)); }
	inline Callback_t5433D5269B1108D8BFECDCD02079FCB1211A6CF8 * get_callback_9() const { return ___callback_9; }
	inline Callback_t5433D5269B1108D8BFECDCD02079FCB1211A6CF8 ** get_address_of_callback_9() { return &___callback_9; }
	inline void set_callback_9(Callback_t5433D5269B1108D8BFECDCD02079FCB1211A6CF8 * value)
	{
		___callback_9 = value;
		Il2CppCodeGenWriteBarrier((&___callback_9), value);
	}

	inline static int32_t get_offset_of_callbackClose_10() { return static_cast<int32_t>(offsetof(ModalController_tD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD, ___callbackClose_10)); }
	inline CallbackClose_t4CBEE1DC735AF36C5814923F4360DBB1949CD115 * get_callbackClose_10() const { return ___callbackClose_10; }
	inline CallbackClose_t4CBEE1DC735AF36C5814923F4360DBB1949CD115 ** get_address_of_callbackClose_10() { return &___callbackClose_10; }
	inline void set_callbackClose_10(CallbackClose_t4CBEE1DC735AF36C5814923F4360DBB1949CD115 * value)
	{
		___callbackClose_10 = value;
		Il2CppCodeGenWriteBarrier((&___callbackClose_10), value);
	}

	inline static int32_t get_offset_of_callbackPop_11() { return static_cast<int32_t>(offsetof(ModalController_tD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD, ___callbackPop_11)); }
	inline CallbackPop_t11746E625CF12C9917AD4DB2B9A57E2597D32609 * get_callbackPop_11() const { return ___callbackPop_11; }
	inline CallbackPop_t11746E625CF12C9917AD4DB2B9A57E2597D32609 ** get_address_of_callbackPop_11() { return &___callbackPop_11; }
	inline void set_callbackPop_11(CallbackPop_t11746E625CF12C9917AD4DB2B9A57E2597D32609 * value)
	{
		___callbackPop_11 = value;
		Il2CppCodeGenWriteBarrier((&___callbackPop_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODALCONTROLLER_TD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD_H
#ifndef MODELTRACKERBLEEVENTHANDLER_T6355CEED827CB2BBF9179447C1FF6968597ADBEB_H
#define MODELTRACKERBLEEVENTHANDLER_T6355CEED827CB2BBF9179447C1FF6968597ADBEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelTrackerbleEventHandler
struct  ModelTrackerbleEventHandler_t6355CEED827CB2BBF9179447C1FF6968597ADBEB  : public BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E
{
public:
	// Vuforia.TrackableBehaviour ModelTrackerbleEventHandler::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_8;
	// Vuforia.TrackableBehaviour_Status ModelTrackerbleEventHandler::m_PreviousStatus
	int32_t ___m_PreviousStatus_9;
	// Vuforia.TrackableBehaviour_Status ModelTrackerbleEventHandler::m_NewStatus
	int32_t ___m_NewStatus_10;
	// System.Int32 ModelTrackerbleEventHandler::count
	int32_t ___count_11;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_8() { return static_cast<int32_t>(offsetof(ModelTrackerbleEventHandler_t6355CEED827CB2BBF9179447C1FF6968597ADBEB, ___mTrackableBehaviour_8)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_8() const { return ___mTrackableBehaviour_8; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_8() { return &___mTrackableBehaviour_8; }
	inline void set_mTrackableBehaviour_8(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_8), value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_9() { return static_cast<int32_t>(offsetof(ModelTrackerbleEventHandler_t6355CEED827CB2BBF9179447C1FF6968597ADBEB, ___m_PreviousStatus_9)); }
	inline int32_t get_m_PreviousStatus_9() const { return ___m_PreviousStatus_9; }
	inline int32_t* get_address_of_m_PreviousStatus_9() { return &___m_PreviousStatus_9; }
	inline void set_m_PreviousStatus_9(int32_t value)
	{
		___m_PreviousStatus_9 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_10() { return static_cast<int32_t>(offsetof(ModelTrackerbleEventHandler_t6355CEED827CB2BBF9179447C1FF6968597ADBEB, ___m_NewStatus_10)); }
	inline int32_t get_m_NewStatus_10() const { return ___m_NewStatus_10; }
	inline int32_t* get_address_of_m_NewStatus_10() { return &___m_NewStatus_10; }
	inline void set_m_NewStatus_10(int32_t value)
	{
		___m_NewStatus_10 = value;
	}

	inline static int32_t get_offset_of_count_11() { return static_cast<int32_t>(offsetof(ModelTrackerbleEventHandler_t6355CEED827CB2BBF9179447C1FF6968597ADBEB, ___count_11)); }
	inline int32_t get_count_11() const { return ___count_11; }
	inline int32_t* get_address_of_count_11() { return &___count_11; }
	inline void set_count_11(int32_t value)
	{
		___count_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELTRACKERBLEEVENTHANDLER_T6355CEED827CB2BBF9179447C1FF6968597ADBEB_H
#ifndef TOGGLEVARIABLESETTING_TE79CD9769496E78E42260EB965B78F11A4C4526B_H
#define TOGGLEVARIABLESETTING_TE79CD9769496E78E42260EB965B78F11A4C4526B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleVariableSetting
struct  ToggleVariableSetting_tE79CD9769496E78E42260EB965B78F11A4C4526B  : public BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB
{
public:
	// UnityEngine.UI.Toggle ToggleVariableSetting::m_toggleInput
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___m_toggleInput_10;

public:
	inline static int32_t get_offset_of_m_toggleInput_10() { return static_cast<int32_t>(offsetof(ToggleVariableSetting_tE79CD9769496E78E42260EB965B78F11A4C4526B, ___m_toggleInput_10)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_m_toggleInput_10() const { return ___m_toggleInput_10; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_m_toggleInput_10() { return &___m_toggleInput_10; }
	inline void set_m_toggleInput_10(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___m_toggleInput_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggleInput_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEVARIABLESETTING_TE79CD9769496E78E42260EB965B78F11A4C4526B_H
#ifndef UISINGLETON_T4848BA88F4BA51DBD493AEB09C803CA5D8B5D051_H
#define UISINGLETON_T4848BA88F4BA51DBD493AEB09C803CA5D8B5D051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISingleton
struct  UISingleton_t4848BA88F4BA51DBD493AEB09C803CA5D8B5D051  : public Singleton_1_tCE7C077749BEBC40D136084465AAA2F04D1A3DA5
{
public:
	// UIController UISingleton::_uiController
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE * ____uiController_7;

public:
	inline static int32_t get_offset_of__uiController_7() { return static_cast<int32_t>(offsetof(UISingleton_t4848BA88F4BA51DBD493AEB09C803CA5D8B5D051, ____uiController_7)); }
	inline UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE * get__uiController_7() const { return ____uiController_7; }
	inline UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE ** get_address_of__uiController_7() { return &____uiController_7; }
	inline void set__uiController_7(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE * value)
	{
		____uiController_7 = value;
		Il2CppCodeGenWriteBarrier((&____uiController_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISINGLETON_T4848BA88F4BA51DBD493AEB09C803CA5D8B5D051_H
#ifndef SCROLLRECT_TAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51_H
#define SCROLLRECT_TAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect
struct  ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Content_4;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_5;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_6;
	// UnityEngine.UI.ScrollRect_MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_7;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_8;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_9;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_10;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_11;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Viewport_12;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * ___m_HorizontalScrollbar_13;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * ___m_VerticalScrollbar_14;
	// UnityEngine.UI.ScrollRect_ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_15;
	// UnityEngine.UI.ScrollRect_ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_16;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_17;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_18;
	// UnityEngine.UI.ScrollRect_ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB * ___m_OnValueChanged_19;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PointerStartLocalCursor_20;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_ContentStartPosition_21;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_ViewRect_22;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___m_ContentBounds_23;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___m_ViewBounds_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Velocity_25;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_26;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Scrolling
	bool ___m_Scrolling_27;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PrevPosition_28;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___m_PrevContentBounds_29;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___m_PrevViewBounds_30;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_31;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_32;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_33;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_34;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_35;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Rect_36;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HorizontalScrollbarRect_37;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_VerticalScrollbarRect_38;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_39;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_40;

public:
	inline static int32_t get_offset_of_m_Content_4() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Content_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Content_4() const { return ___m_Content_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Content_4() { return &___m_Content_4; }
	inline void set_m_Content_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Content_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_4), value);
	}

	inline static int32_t get_offset_of_m_Horizontal_5() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Horizontal_5)); }
	inline bool get_m_Horizontal_5() const { return ___m_Horizontal_5; }
	inline bool* get_address_of_m_Horizontal_5() { return &___m_Horizontal_5; }
	inline void set_m_Horizontal_5(bool value)
	{
		___m_Horizontal_5 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_6() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Vertical_6)); }
	inline bool get_m_Vertical_6() const { return ___m_Vertical_6; }
	inline bool* get_address_of_m_Vertical_6() { return &___m_Vertical_6; }
	inline void set_m_Vertical_6(bool value)
	{
		___m_Vertical_6 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_7() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_MovementType_7)); }
	inline int32_t get_m_MovementType_7() const { return ___m_MovementType_7; }
	inline int32_t* get_address_of_m_MovementType_7() { return &___m_MovementType_7; }
	inline void set_m_MovementType_7(int32_t value)
	{
		___m_MovementType_7 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_8() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Elasticity_8)); }
	inline float get_m_Elasticity_8() const { return ___m_Elasticity_8; }
	inline float* get_address_of_m_Elasticity_8() { return &___m_Elasticity_8; }
	inline void set_m_Elasticity_8(float value)
	{
		___m_Elasticity_8 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_9() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Inertia_9)); }
	inline bool get_m_Inertia_9() const { return ___m_Inertia_9; }
	inline bool* get_address_of_m_Inertia_9() { return &___m_Inertia_9; }
	inline void set_m_Inertia_9(bool value)
	{
		___m_Inertia_9 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_10() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_DecelerationRate_10)); }
	inline float get_m_DecelerationRate_10() const { return ___m_DecelerationRate_10; }
	inline float* get_address_of_m_DecelerationRate_10() { return &___m_DecelerationRate_10; }
	inline void set_m_DecelerationRate_10(float value)
	{
		___m_DecelerationRate_10 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_11() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_ScrollSensitivity_11)); }
	inline float get_m_ScrollSensitivity_11() const { return ___m_ScrollSensitivity_11; }
	inline float* get_address_of_m_ScrollSensitivity_11() { return &___m_ScrollSensitivity_11; }
	inline void set_m_ScrollSensitivity_11(float value)
	{
		___m_ScrollSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_12() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Viewport_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Viewport_12() const { return ___m_Viewport_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Viewport_12() { return &___m_Viewport_12; }
	inline void set_m_Viewport_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Viewport_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Viewport_12), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_13() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HorizontalScrollbar_13)); }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * get_m_HorizontalScrollbar_13() const { return ___m_HorizontalScrollbar_13; }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 ** get_address_of_m_HorizontalScrollbar_13() { return &___m_HorizontalScrollbar_13; }
	inline void set_m_HorizontalScrollbar_13(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * value)
	{
		___m_HorizontalScrollbar_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbar_13), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_14() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VerticalScrollbar_14)); }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * get_m_VerticalScrollbar_14() const { return ___m_VerticalScrollbar_14; }
	inline Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 ** get_address_of_m_VerticalScrollbar_14() { return &___m_VerticalScrollbar_14; }
	inline void set_m_VerticalScrollbar_14(Scrollbar_t8F8679D0EAFACBCBD603E6B0E741E6A783DB3389 * value)
	{
		___m_VerticalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbar_14), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_15() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HorizontalScrollbarVisibility_15)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_15() const { return ___m_HorizontalScrollbarVisibility_15; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_15() { return &___m_HorizontalScrollbarVisibility_15; }
	inline void set_m_HorizontalScrollbarVisibility_15(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_16() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VerticalScrollbarVisibility_16)); }
	inline int32_t get_m_VerticalScrollbarVisibility_16() const { return ___m_VerticalScrollbarVisibility_16; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_16() { return &___m_VerticalScrollbarVisibility_16; }
	inline void set_m_VerticalScrollbarVisibility_16(int32_t value)
	{
		___m_VerticalScrollbarVisibility_16 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_17() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HorizontalScrollbarSpacing_17)); }
	inline float get_m_HorizontalScrollbarSpacing_17() const { return ___m_HorizontalScrollbarSpacing_17; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_17() { return &___m_HorizontalScrollbarSpacing_17; }
	inline void set_m_HorizontalScrollbarSpacing_17(float value)
	{
		___m_HorizontalScrollbarSpacing_17 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_18() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VerticalScrollbarSpacing_18)); }
	inline float get_m_VerticalScrollbarSpacing_18() const { return ___m_VerticalScrollbarSpacing_18; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_18() { return &___m_VerticalScrollbarSpacing_18; }
	inline void set_m_VerticalScrollbarSpacing_18(float value)
	{
		___m_VerticalScrollbarSpacing_18 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_19() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_OnValueChanged_19)); }
	inline ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB * get_m_OnValueChanged_19() const { return ___m_OnValueChanged_19; }
	inline ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB ** get_address_of_m_OnValueChanged_19() { return &___m_OnValueChanged_19; }
	inline void set_m_OnValueChanged_19(ScrollRectEvent_t8995F69D65BA823FB862144B12E6D3504236FEEB * value)
	{
		___m_OnValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_19), value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_20() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_PointerStartLocalCursor_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PointerStartLocalCursor_20() const { return ___m_PointerStartLocalCursor_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PointerStartLocalCursor_20() { return &___m_PointerStartLocalCursor_20; }
	inline void set_m_PointerStartLocalCursor_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PointerStartLocalCursor_20 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_21() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_ContentStartPosition_21)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_ContentStartPosition_21() const { return ___m_ContentStartPosition_21; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_ContentStartPosition_21() { return &___m_ContentStartPosition_21; }
	inline void set_m_ContentStartPosition_21(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_ContentStartPosition_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_22() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_ViewRect_22)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_ViewRect_22() const { return ___m_ViewRect_22; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_ViewRect_22() { return &___m_ViewRect_22; }
	inline void set_m_ViewRect_22(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_ViewRect_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ViewRect_22), value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_23() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_ContentBounds_23)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_m_ContentBounds_23() const { return ___m_ContentBounds_23; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_m_ContentBounds_23() { return &___m_ContentBounds_23; }
	inline void set_m_ContentBounds_23(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___m_ContentBounds_23 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_24() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_ViewBounds_24)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_m_ViewBounds_24() const { return ___m_ViewBounds_24; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_m_ViewBounds_24() { return &___m_ViewBounds_24; }
	inline void set_m_ViewBounds_24(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___m_ViewBounds_24 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_25() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Velocity_25)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Velocity_25() const { return ___m_Velocity_25; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Velocity_25() { return &___m_Velocity_25; }
	inline void set_m_Velocity_25(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Velocity_25 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_26() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Dragging_26)); }
	inline bool get_m_Dragging_26() const { return ___m_Dragging_26; }
	inline bool* get_address_of_m_Dragging_26() { return &___m_Dragging_26; }
	inline void set_m_Dragging_26(bool value)
	{
		___m_Dragging_26 = value;
	}

	inline static int32_t get_offset_of_m_Scrolling_27() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Scrolling_27)); }
	inline bool get_m_Scrolling_27() const { return ___m_Scrolling_27; }
	inline bool* get_address_of_m_Scrolling_27() { return &___m_Scrolling_27; }
	inline void set_m_Scrolling_27(bool value)
	{
		___m_Scrolling_27 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_28() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_PrevPosition_28)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PrevPosition_28() const { return ___m_PrevPosition_28; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PrevPosition_28() { return &___m_PrevPosition_28; }
	inline void set_m_PrevPosition_28(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PrevPosition_28 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_29() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_PrevContentBounds_29)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_m_PrevContentBounds_29() const { return ___m_PrevContentBounds_29; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_m_PrevContentBounds_29() { return &___m_PrevContentBounds_29; }
	inline void set_m_PrevContentBounds_29(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___m_PrevContentBounds_29 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_30() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_PrevViewBounds_30)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_m_PrevViewBounds_30() const { return ___m_PrevViewBounds_30; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_m_PrevViewBounds_30() { return &___m_PrevViewBounds_30; }
	inline void set_m_PrevViewBounds_30(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___m_PrevViewBounds_30 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_31() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HasRebuiltLayout_31)); }
	inline bool get_m_HasRebuiltLayout_31() const { return ___m_HasRebuiltLayout_31; }
	inline bool* get_address_of_m_HasRebuiltLayout_31() { return &___m_HasRebuiltLayout_31; }
	inline void set_m_HasRebuiltLayout_31(bool value)
	{
		___m_HasRebuiltLayout_31 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_32() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HSliderExpand_32)); }
	inline bool get_m_HSliderExpand_32() const { return ___m_HSliderExpand_32; }
	inline bool* get_address_of_m_HSliderExpand_32() { return &___m_HSliderExpand_32; }
	inline void set_m_HSliderExpand_32(bool value)
	{
		___m_HSliderExpand_32 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_33() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VSliderExpand_33)); }
	inline bool get_m_VSliderExpand_33() const { return ___m_VSliderExpand_33; }
	inline bool* get_address_of_m_VSliderExpand_33() { return &___m_VSliderExpand_33; }
	inline void set_m_VSliderExpand_33(bool value)
	{
		___m_VSliderExpand_33 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_34() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HSliderHeight_34)); }
	inline float get_m_HSliderHeight_34() const { return ___m_HSliderHeight_34; }
	inline float* get_address_of_m_HSliderHeight_34() { return &___m_HSliderHeight_34; }
	inline void set_m_HSliderHeight_34(float value)
	{
		___m_HSliderHeight_34 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_35() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VSliderWidth_35)); }
	inline float get_m_VSliderWidth_35() const { return ___m_VSliderWidth_35; }
	inline float* get_address_of_m_VSliderWidth_35() { return &___m_VSliderWidth_35; }
	inline void set_m_VSliderWidth_35(float value)
	{
		___m_VSliderWidth_35 = value;
	}

	inline static int32_t get_offset_of_m_Rect_36() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Rect_36)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Rect_36() const { return ___m_Rect_36; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Rect_36() { return &___m_Rect_36; }
	inline void set_m_Rect_36(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Rect_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_36), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_37() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_HorizontalScrollbarRect_37)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HorizontalScrollbarRect_37() const { return ___m_HorizontalScrollbarRect_37; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HorizontalScrollbarRect_37() { return &___m_HorizontalScrollbarRect_37; }
	inline void set_m_HorizontalScrollbarRect_37(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HorizontalScrollbarRect_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbarRect_37), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_38() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_VerticalScrollbarRect_38)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_VerticalScrollbarRect_38() const { return ___m_VerticalScrollbarRect_38; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_VerticalScrollbarRect_38() { return &___m_VerticalScrollbarRect_38; }
	inline void set_m_VerticalScrollbarRect_38(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_VerticalScrollbarRect_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbarRect_38), value);
	}

	inline static int32_t get_offset_of_m_Tracker_39() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Tracker_39)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_39() const { return ___m_Tracker_39; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_39() { return &___m_Tracker_39; }
	inline void set_m_Tracker_39(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_39 = value;
	}

	inline static int32_t get_offset_of_m_Corners_40() { return static_cast<int32_t>(offsetof(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51, ___m_Corners_40)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_40() const { return ___m_Corners_40; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_40() { return &___m_Corners_40; }
	inline void set_m_Corners_40(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECT_TAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51_H
#ifndef VECTOR3VARIABLESETTING_T6ECFD0E1334B82EB9DABD491BC601427AA7D403F_H
#define VECTOR3VARIABLESETTING_T6ECFD0E1334B82EB9DABD491BC601427AA7D403F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vector3VariableSetting
struct  Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F  : public BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB
{
public:
	// UnityEngine.UI.InputField Vector3VariableSetting::m_fromXValue
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___m_fromXValue_10;
	// UnityEngine.UI.InputField Vector3VariableSetting::m_fromYValue
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___m_fromYValue_11;
	// UnityEngine.UI.InputField Vector3VariableSetting::m_fromZValue
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___m_fromZValue_12;
	// UnityEngine.GameObject Vector3VariableSetting::m_toValueObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_toValueObject_13;
	// UnityEngine.GameObject Vector3VariableSetting::m_thenValueObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_thenValueObject_14;
	// UnityEngine.Vector3 Vector3VariableSetting::m_fromVec
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_fromVec_15;

public:
	inline static int32_t get_offset_of_m_fromXValue_10() { return static_cast<int32_t>(offsetof(Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F, ___m_fromXValue_10)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_m_fromXValue_10() const { return ___m_fromXValue_10; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_m_fromXValue_10() { return &___m_fromXValue_10; }
	inline void set_m_fromXValue_10(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___m_fromXValue_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_fromXValue_10), value);
	}

	inline static int32_t get_offset_of_m_fromYValue_11() { return static_cast<int32_t>(offsetof(Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F, ___m_fromYValue_11)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_m_fromYValue_11() const { return ___m_fromYValue_11; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_m_fromYValue_11() { return &___m_fromYValue_11; }
	inline void set_m_fromYValue_11(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___m_fromYValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_fromYValue_11), value);
	}

	inline static int32_t get_offset_of_m_fromZValue_12() { return static_cast<int32_t>(offsetof(Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F, ___m_fromZValue_12)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_m_fromZValue_12() const { return ___m_fromZValue_12; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_m_fromZValue_12() { return &___m_fromZValue_12; }
	inline void set_m_fromZValue_12(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___m_fromZValue_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_fromZValue_12), value);
	}

	inline static int32_t get_offset_of_m_toValueObject_13() { return static_cast<int32_t>(offsetof(Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F, ___m_toValueObject_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_toValueObject_13() const { return ___m_toValueObject_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_toValueObject_13() { return &___m_toValueObject_13; }
	inline void set_m_toValueObject_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_toValueObject_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_toValueObject_13), value);
	}

	inline static int32_t get_offset_of_m_thenValueObject_14() { return static_cast<int32_t>(offsetof(Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F, ___m_thenValueObject_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_thenValueObject_14() const { return ___m_thenValueObject_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_thenValueObject_14() { return &___m_thenValueObject_14; }
	inline void set_m_thenValueObject_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_thenValueObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_thenValueObject_14), value);
	}

	inline static int32_t get_offset_of_m_fromVec_15() { return static_cast<int32_t>(offsetof(Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F, ___m_fromVec_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_fromVec_15() const { return ___m_fromVec_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_fromVec_15() { return &___m_fromVec_15; }
	inline void set_m_fromVec_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_fromVec_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3VARIABLESETTING_T6ECFD0E1334B82EB9DABD491BC601427AA7D403F_H
#ifndef ARUICONTROLLER_TAAA4895E4957981A07F95C8FB2941F2BE51CA8F1_H
#define ARUICONTROLLER_TAAA4895E4957981A07F95C8FB2941F2BE51CA8F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARUIController
struct  ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1  : public AceSingleton_1_t8A0EA087B5B8AE910E71DA068C071DE3895A7A81
{
public:
	// System.String[] ARUIController::animationNameList
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___animationNameList_11;
	// System.Boolean ARUIController::_IsDetected
	bool ____IsDetected_12;
	// UnityEngine.UI.Button ARUIController::btnBack
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnBack_13;
	// UnityEngine.UI.Button ARUIController::btnTutorial
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnTutorial_14;
	// UnityEngine.GameObject ARUIController::guidePanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___guidePanel_15;
	// UnityEngine.GameObject ARUIController::panelMenu
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___panelMenu_16;
	// UnityEngine.UI.Button ARUIController::btnAni1
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnAni1_17;
	// UnityEngine.UI.Button ARUIController::btnAni2
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnAni2_18;
	// UnityEngine.UI.Button ARUIController::btnAni3
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnAni3_19;
	// UnityEngine.GameObject ARUIController::panelEnd
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___panelEnd_20;
	// UnityEngine.UI.Button ARUIController::btnMoveMenu
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnMoveMenu_21;
	// UnityEngine.GameObject ARUIController::midAirPosition
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___midAirPosition_22;
	// System.Int32 ARUIController::IntAniObjScale
	int32_t ___IntAniObjScale_23;
	// System.String ARUIController::animationName
	String_t* ___animationName_24;
	// ARUIController_GUIState ARUIController::lastState
	uint8_t ___lastState_25;

public:
	inline static int32_t get_offset_of_animationNameList_11() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___animationNameList_11)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_animationNameList_11() const { return ___animationNameList_11; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_animationNameList_11() { return &___animationNameList_11; }
	inline void set_animationNameList_11(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___animationNameList_11 = value;
		Il2CppCodeGenWriteBarrier((&___animationNameList_11), value);
	}

	inline static int32_t get_offset_of__IsDetected_12() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ____IsDetected_12)); }
	inline bool get__IsDetected_12() const { return ____IsDetected_12; }
	inline bool* get_address_of__IsDetected_12() { return &____IsDetected_12; }
	inline void set__IsDetected_12(bool value)
	{
		____IsDetected_12 = value;
	}

	inline static int32_t get_offset_of_btnBack_13() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___btnBack_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnBack_13() const { return ___btnBack_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnBack_13() { return &___btnBack_13; }
	inline void set_btnBack_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnBack_13 = value;
		Il2CppCodeGenWriteBarrier((&___btnBack_13), value);
	}

	inline static int32_t get_offset_of_btnTutorial_14() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___btnTutorial_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnTutorial_14() const { return ___btnTutorial_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnTutorial_14() { return &___btnTutorial_14; }
	inline void set_btnTutorial_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnTutorial_14 = value;
		Il2CppCodeGenWriteBarrier((&___btnTutorial_14), value);
	}

	inline static int32_t get_offset_of_guidePanel_15() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___guidePanel_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_guidePanel_15() const { return ___guidePanel_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_guidePanel_15() { return &___guidePanel_15; }
	inline void set_guidePanel_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___guidePanel_15 = value;
		Il2CppCodeGenWriteBarrier((&___guidePanel_15), value);
	}

	inline static int32_t get_offset_of_panelMenu_16() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___panelMenu_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_panelMenu_16() const { return ___panelMenu_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_panelMenu_16() { return &___panelMenu_16; }
	inline void set_panelMenu_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___panelMenu_16 = value;
		Il2CppCodeGenWriteBarrier((&___panelMenu_16), value);
	}

	inline static int32_t get_offset_of_btnAni1_17() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___btnAni1_17)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnAni1_17() const { return ___btnAni1_17; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnAni1_17() { return &___btnAni1_17; }
	inline void set_btnAni1_17(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnAni1_17 = value;
		Il2CppCodeGenWriteBarrier((&___btnAni1_17), value);
	}

	inline static int32_t get_offset_of_btnAni2_18() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___btnAni2_18)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnAni2_18() const { return ___btnAni2_18; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnAni2_18() { return &___btnAni2_18; }
	inline void set_btnAni2_18(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnAni2_18 = value;
		Il2CppCodeGenWriteBarrier((&___btnAni2_18), value);
	}

	inline static int32_t get_offset_of_btnAni3_19() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___btnAni3_19)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnAni3_19() const { return ___btnAni3_19; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnAni3_19() { return &___btnAni3_19; }
	inline void set_btnAni3_19(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnAni3_19 = value;
		Il2CppCodeGenWriteBarrier((&___btnAni3_19), value);
	}

	inline static int32_t get_offset_of_panelEnd_20() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___panelEnd_20)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_panelEnd_20() const { return ___panelEnd_20; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_panelEnd_20() { return &___panelEnd_20; }
	inline void set_panelEnd_20(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___panelEnd_20 = value;
		Il2CppCodeGenWriteBarrier((&___panelEnd_20), value);
	}

	inline static int32_t get_offset_of_btnMoveMenu_21() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___btnMoveMenu_21)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnMoveMenu_21() const { return ___btnMoveMenu_21; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnMoveMenu_21() { return &___btnMoveMenu_21; }
	inline void set_btnMoveMenu_21(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnMoveMenu_21 = value;
		Il2CppCodeGenWriteBarrier((&___btnMoveMenu_21), value);
	}

	inline static int32_t get_offset_of_midAirPosition_22() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___midAirPosition_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_midAirPosition_22() const { return ___midAirPosition_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_midAirPosition_22() { return &___midAirPosition_22; }
	inline void set_midAirPosition_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___midAirPosition_22 = value;
		Il2CppCodeGenWriteBarrier((&___midAirPosition_22), value);
	}

	inline static int32_t get_offset_of_IntAniObjScale_23() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___IntAniObjScale_23)); }
	inline int32_t get_IntAniObjScale_23() const { return ___IntAniObjScale_23; }
	inline int32_t* get_address_of_IntAniObjScale_23() { return &___IntAniObjScale_23; }
	inline void set_IntAniObjScale_23(int32_t value)
	{
		___IntAniObjScale_23 = value;
	}

	inline static int32_t get_offset_of_animationName_24() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___animationName_24)); }
	inline String_t* get_animationName_24() const { return ___animationName_24; }
	inline String_t** get_address_of_animationName_24() { return &___animationName_24; }
	inline void set_animationName_24(String_t* value)
	{
		___animationName_24 = value;
		Il2CppCodeGenWriteBarrier((&___animationName_24), value);
	}

	inline static int32_t get_offset_of_lastState_25() { return static_cast<int32_t>(offsetof(ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1, ___lastState_25)); }
	inline uint8_t get_lastState_25() const { return ___lastState_25; }
	inline uint8_t* get_address_of_lastState_25() { return &___lastState_25; }
	inline void set_lastState_25(uint8_t value)
	{
		___lastState_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARUICONTROLLER_TAAA4895E4957981A07F95C8FB2941F2BE51CA8F1_H
#ifndef COLORSCROLLRECT_T67B3D6242798310021DFDAEA7BA67CD622B66267_H
#define COLORSCROLLRECT_T67B3D6242798310021DFDAEA7BA67CD622B66267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorScrollRect
struct  ColorScrollRect_t67B3D6242798310021DFDAEA7BA67CD622B66267  : public ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51
{
public:
	// System.Boolean ColorScrollRect::<Dragging>k__BackingField
	bool ___U3CDraggingU3Ek__BackingField_41;

public:
	inline static int32_t get_offset_of_U3CDraggingU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(ColorScrollRect_t67B3D6242798310021DFDAEA7BA67CD622B66267, ___U3CDraggingU3Ek__BackingField_41)); }
	inline bool get_U3CDraggingU3Ek__BackingField_41() const { return ___U3CDraggingU3Ek__BackingField_41; }
	inline bool* get_address_of_U3CDraggingU3Ek__BackingField_41() { return &___U3CDraggingU3Ek__BackingField_41; }
	inline void set_U3CDraggingU3Ek__BackingField_41(bool value)
	{
		___U3CDraggingU3Ek__BackingField_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSCROLLRECT_T67B3D6242798310021DFDAEA7BA67CD622B66267_H
#ifndef DEFAULTMAINCAMERA_T5CCBA3F2B0D86DA34C24E30E5149A7FD6B112B6C_H
#define DEFAULTMAINCAMERA_T5CCBA3F2B0D86DA34C24E30E5149A7FD6B112B6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultMainCamera
struct  DefaultMainCamera_t5CCBA3F2B0D86DA34C24E30E5149A7FD6B112B6C  : public AceSingleton_1_t240F0701B0402BD7549E844088F346CE3BB66A16
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTMAINCAMERA_T5CCBA3F2B0D86DA34C24E30E5149A7FD6B112B6C_H
#ifndef DEFAULTSCENECONTROLLER_T823BFAEBA080AA5B2E0951BDA43945120F4EF5E1_H
#define DEFAULTSCENECONTROLLER_T823BFAEBA080AA5B2E0951BDA43945120F4EF5E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultSceneController
struct  DefaultSceneController_t823BFAEBA080AA5B2E0951BDA43945120F4EF5E1  : public AceSingleton_1_t73730D3AF237C36C9AFBE1DE2458F56FF0180612
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSCENECONTROLLER_T823BFAEBA080AA5B2E0951BDA43945120F4EF5E1_H
#ifndef HOMETESTCONTROLLER_TF66A2DC20999ECA1956EA0CCEAFDDECB83652363_H
#define HOMETESTCONTROLLER_TF66A2DC20999ECA1956EA0CCEAFDDECB83652363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeTestController
struct  HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363  : public AceSingleton_1_t73730D3AF237C36C9AFBE1DE2458F56FF0180612
{
public:
	// UnityEngine.UI.Button HomeTestController::btnLoadRoom
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnLoadRoom_12;
	// UnityEngine.UI.Button HomeTestController::btnLoadProduct
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnLoadProduct_13;
	// UnityEngine.UI.Button HomeTestController::btnClearAll
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnClearAll_14;
	// UnityEngine.UI.Button HomeTestController::btnOptionHigh
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnOptionHigh_15;
	// UnityEngine.UI.Button HomeTestController::btnOptionMiddle
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnOptionMiddle_16;
	// UnityEngine.UI.Button HomeTestController::btnOptionLow
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnOptionLow_17;
	// UnityEngine.UI.Button HomeTestController::btnOptionCollision
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnOptionCollision_18;
	// UnityEngine.UI.Button HomeTestController::btnWallPaper
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnWallPaper_19;
	// UnityEngine.UI.Button HomeTestController::btnUnitFloor
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnUnitFloor_20;
	// UnityEngine.UI.Button HomeTestController::btnStyleEditorEnd
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnStyleEditorEnd_21;

public:
	inline static int32_t get_offset_of_btnLoadRoom_12() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363, ___btnLoadRoom_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnLoadRoom_12() const { return ___btnLoadRoom_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnLoadRoom_12() { return &___btnLoadRoom_12; }
	inline void set_btnLoadRoom_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnLoadRoom_12 = value;
		Il2CppCodeGenWriteBarrier((&___btnLoadRoom_12), value);
	}

	inline static int32_t get_offset_of_btnLoadProduct_13() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363, ___btnLoadProduct_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnLoadProduct_13() const { return ___btnLoadProduct_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnLoadProduct_13() { return &___btnLoadProduct_13; }
	inline void set_btnLoadProduct_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnLoadProduct_13 = value;
		Il2CppCodeGenWriteBarrier((&___btnLoadProduct_13), value);
	}

	inline static int32_t get_offset_of_btnClearAll_14() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363, ___btnClearAll_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnClearAll_14() const { return ___btnClearAll_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnClearAll_14() { return &___btnClearAll_14; }
	inline void set_btnClearAll_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnClearAll_14 = value;
		Il2CppCodeGenWriteBarrier((&___btnClearAll_14), value);
	}

	inline static int32_t get_offset_of_btnOptionHigh_15() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363, ___btnOptionHigh_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnOptionHigh_15() const { return ___btnOptionHigh_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnOptionHigh_15() { return &___btnOptionHigh_15; }
	inline void set_btnOptionHigh_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnOptionHigh_15 = value;
		Il2CppCodeGenWriteBarrier((&___btnOptionHigh_15), value);
	}

	inline static int32_t get_offset_of_btnOptionMiddle_16() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363, ___btnOptionMiddle_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnOptionMiddle_16() const { return ___btnOptionMiddle_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnOptionMiddle_16() { return &___btnOptionMiddle_16; }
	inline void set_btnOptionMiddle_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnOptionMiddle_16 = value;
		Il2CppCodeGenWriteBarrier((&___btnOptionMiddle_16), value);
	}

	inline static int32_t get_offset_of_btnOptionLow_17() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363, ___btnOptionLow_17)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnOptionLow_17() const { return ___btnOptionLow_17; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnOptionLow_17() { return &___btnOptionLow_17; }
	inline void set_btnOptionLow_17(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnOptionLow_17 = value;
		Il2CppCodeGenWriteBarrier((&___btnOptionLow_17), value);
	}

	inline static int32_t get_offset_of_btnOptionCollision_18() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363, ___btnOptionCollision_18)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnOptionCollision_18() const { return ___btnOptionCollision_18; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnOptionCollision_18() { return &___btnOptionCollision_18; }
	inline void set_btnOptionCollision_18(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnOptionCollision_18 = value;
		Il2CppCodeGenWriteBarrier((&___btnOptionCollision_18), value);
	}

	inline static int32_t get_offset_of_btnWallPaper_19() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363, ___btnWallPaper_19)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnWallPaper_19() const { return ___btnWallPaper_19; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnWallPaper_19() { return &___btnWallPaper_19; }
	inline void set_btnWallPaper_19(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnWallPaper_19 = value;
		Il2CppCodeGenWriteBarrier((&___btnWallPaper_19), value);
	}

	inline static int32_t get_offset_of_btnUnitFloor_20() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363, ___btnUnitFloor_20)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnUnitFloor_20() const { return ___btnUnitFloor_20; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnUnitFloor_20() { return &___btnUnitFloor_20; }
	inline void set_btnUnitFloor_20(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnUnitFloor_20 = value;
		Il2CppCodeGenWriteBarrier((&___btnUnitFloor_20), value);
	}

	inline static int32_t get_offset_of_btnStyleEditorEnd_21() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363, ___btnStyleEditorEnd_21)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnStyleEditorEnd_21() const { return ___btnStyleEditorEnd_21; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnStyleEditorEnd_21() { return &___btnStyleEditorEnd_21; }
	inline void set_btnStyleEditorEnd_21(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnStyleEditorEnd_21 = value;
		Il2CppCodeGenWriteBarrier((&___btnStyleEditorEnd_21), value);
	}
};

struct HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363_StaticFields
{
public:
	// System.String HomeTestController::BASE_PATH
	String_t* ___BASE_PATH_11;

public:
	inline static int32_t get_offset_of_BASE_PATH_11() { return static_cast<int32_t>(offsetof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363_StaticFields, ___BASE_PATH_11)); }
	inline String_t* get_BASE_PATH_11() const { return ___BASE_PATH_11; }
	inline String_t** get_address_of_BASE_PATH_11() { return &___BASE_PATH_11; }
	inline void set_BASE_PATH_11(String_t* value)
	{
		___BASE_PATH_11 = value;
		Il2CppCodeGenWriteBarrier((&___BASE_PATH_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOMETESTCONTROLLER_TF66A2DC20999ECA1956EA0CCEAFDDECB83652363_H
#ifndef MATTRESSUICONTROLLER_T99B279DB79F40633115ABF233E0592DFF622F191_H
#define MATTRESSUICONTROLLER_T99B279DB79F40633115ABF233E0592DFF622F191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MattressUIController
struct  MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191  : public AceSingleton_1_t8A0EA087B5B8AE910E71DA068C071DE3895A7A81
{
public:
	// System.String[] MattressUIController::animationNameList
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___animationNameList_11;
	// UnityEngine.UI.Button MattressUIController::btnBack
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnBack_12;
	// UnityEngine.UI.Button MattressUIController::btnTutorial
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnTutorial_13;
	// UnityEngine.GameObject MattressUIController::panelMenu
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___panelMenu_14;
	// UnityEngine.UI.Button MattressUIController::btnAni1
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnAni1_15;
	// UnityEngine.UI.Button MattressUIController::btnAni2
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnAni2_16;
	// UnityEngine.UI.Button MattressUIController::btnAni3
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnAni3_17;
	// UnityEngine.UI.Button MattressUIController::mattressButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___mattressButton_18;
	// UnityEngine.GameObject MattressUIController::midAirPosition
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___midAirPosition_19;
	// System.Int32 MattressUIController::IntAniObjScale
	int32_t ___IntAniObjScale_20;
	// System.String MattressUIController::animationName
	String_t* ___animationName_21;

public:
	inline static int32_t get_offset_of_animationNameList_11() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___animationNameList_11)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_animationNameList_11() const { return ___animationNameList_11; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_animationNameList_11() { return &___animationNameList_11; }
	inline void set_animationNameList_11(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___animationNameList_11 = value;
		Il2CppCodeGenWriteBarrier((&___animationNameList_11), value);
	}

	inline static int32_t get_offset_of_btnBack_12() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___btnBack_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnBack_12() const { return ___btnBack_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnBack_12() { return &___btnBack_12; }
	inline void set_btnBack_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnBack_12 = value;
		Il2CppCodeGenWriteBarrier((&___btnBack_12), value);
	}

	inline static int32_t get_offset_of_btnTutorial_13() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___btnTutorial_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnTutorial_13() const { return ___btnTutorial_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnTutorial_13() { return &___btnTutorial_13; }
	inline void set_btnTutorial_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnTutorial_13 = value;
		Il2CppCodeGenWriteBarrier((&___btnTutorial_13), value);
	}

	inline static int32_t get_offset_of_panelMenu_14() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___panelMenu_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_panelMenu_14() const { return ___panelMenu_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_panelMenu_14() { return &___panelMenu_14; }
	inline void set_panelMenu_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___panelMenu_14 = value;
		Il2CppCodeGenWriteBarrier((&___panelMenu_14), value);
	}

	inline static int32_t get_offset_of_btnAni1_15() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___btnAni1_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnAni1_15() const { return ___btnAni1_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnAni1_15() { return &___btnAni1_15; }
	inline void set_btnAni1_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnAni1_15 = value;
		Il2CppCodeGenWriteBarrier((&___btnAni1_15), value);
	}

	inline static int32_t get_offset_of_btnAni2_16() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___btnAni2_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnAni2_16() const { return ___btnAni2_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnAni2_16() { return &___btnAni2_16; }
	inline void set_btnAni2_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnAni2_16 = value;
		Il2CppCodeGenWriteBarrier((&___btnAni2_16), value);
	}

	inline static int32_t get_offset_of_btnAni3_17() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___btnAni3_17)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnAni3_17() const { return ___btnAni3_17; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnAni3_17() { return &___btnAni3_17; }
	inline void set_btnAni3_17(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnAni3_17 = value;
		Il2CppCodeGenWriteBarrier((&___btnAni3_17), value);
	}

	inline static int32_t get_offset_of_mattressButton_18() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___mattressButton_18)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_mattressButton_18() const { return ___mattressButton_18; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_mattressButton_18() { return &___mattressButton_18; }
	inline void set_mattressButton_18(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___mattressButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___mattressButton_18), value);
	}

	inline static int32_t get_offset_of_midAirPosition_19() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___midAirPosition_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_midAirPosition_19() const { return ___midAirPosition_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_midAirPosition_19() { return &___midAirPosition_19; }
	inline void set_midAirPosition_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___midAirPosition_19 = value;
		Il2CppCodeGenWriteBarrier((&___midAirPosition_19), value);
	}

	inline static int32_t get_offset_of_IntAniObjScale_20() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___IntAniObjScale_20)); }
	inline int32_t get_IntAniObjScale_20() const { return ___IntAniObjScale_20; }
	inline int32_t* get_address_of_IntAniObjScale_20() { return &___IntAniObjScale_20; }
	inline void set_IntAniObjScale_20(int32_t value)
	{
		___IntAniObjScale_20 = value;
	}

	inline static int32_t get_offset_of_animationName_21() { return static_cast<int32_t>(offsetof(MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191, ___animationName_21)); }
	inline String_t* get_animationName_21() const { return ___animationName_21; }
	inline String_t** get_address_of_animationName_21() { return &___animationName_21; }
	inline void set_animationName_21(String_t* value)
	{
		___animationName_21 = value;
		Il2CppCodeGenWriteBarrier((&___animationName_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATTRESSUICONTROLLER_T99B279DB79F40633115ABF233E0592DFF622F191_H
#ifndef MOBILEMANAGER_T9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543_H
#define MOBILEMANAGER_T9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobileManager
struct  MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543  : public AceSingleton_1_t540130C7D96D421F44F6626E683954D64291A316
{
public:
	// System.Boolean MobileManager::isNotch
	bool ___isNotch_11;
	// System.String MobileManager::strJsonHomeUnit
	String_t* ___strJsonHomeUnit_12;
	// System.String MobileManager::strModelTargetString
	String_t* ___strModelTargetString_13;
	// UnityEngine.Camera MobileManager::mainCam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCam_14;
	// UnityEngine.Camera MobileManager::loadingCam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___loadingCam_15;
	// MobileBase MobileManager::mobilePlugin
	MobileBase_tA53736A507DA935FDC5873236F32953E0B2F886A * ___mobilePlugin_16;

public:
	inline static int32_t get_offset_of_isNotch_11() { return static_cast<int32_t>(offsetof(MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543, ___isNotch_11)); }
	inline bool get_isNotch_11() const { return ___isNotch_11; }
	inline bool* get_address_of_isNotch_11() { return &___isNotch_11; }
	inline void set_isNotch_11(bool value)
	{
		___isNotch_11 = value;
	}

	inline static int32_t get_offset_of_strJsonHomeUnit_12() { return static_cast<int32_t>(offsetof(MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543, ___strJsonHomeUnit_12)); }
	inline String_t* get_strJsonHomeUnit_12() const { return ___strJsonHomeUnit_12; }
	inline String_t** get_address_of_strJsonHomeUnit_12() { return &___strJsonHomeUnit_12; }
	inline void set_strJsonHomeUnit_12(String_t* value)
	{
		___strJsonHomeUnit_12 = value;
		Il2CppCodeGenWriteBarrier((&___strJsonHomeUnit_12), value);
	}

	inline static int32_t get_offset_of_strModelTargetString_13() { return static_cast<int32_t>(offsetof(MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543, ___strModelTargetString_13)); }
	inline String_t* get_strModelTargetString_13() const { return ___strModelTargetString_13; }
	inline String_t** get_address_of_strModelTargetString_13() { return &___strModelTargetString_13; }
	inline void set_strModelTargetString_13(String_t* value)
	{
		___strModelTargetString_13 = value;
		Il2CppCodeGenWriteBarrier((&___strModelTargetString_13), value);
	}

	inline static int32_t get_offset_of_mainCam_14() { return static_cast<int32_t>(offsetof(MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543, ___mainCam_14)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mainCam_14() const { return ___mainCam_14; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mainCam_14() { return &___mainCam_14; }
	inline void set_mainCam_14(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mainCam_14 = value;
		Il2CppCodeGenWriteBarrier((&___mainCam_14), value);
	}

	inline static int32_t get_offset_of_loadingCam_15() { return static_cast<int32_t>(offsetof(MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543, ___loadingCam_15)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_loadingCam_15() const { return ___loadingCam_15; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_loadingCam_15() { return &___loadingCam_15; }
	inline void set_loadingCam_15(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___loadingCam_15 = value;
		Il2CppCodeGenWriteBarrier((&___loadingCam_15), value);
	}

	inline static int32_t get_offset_of_mobilePlugin_16() { return static_cast<int32_t>(offsetof(MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543, ___mobilePlugin_16)); }
	inline MobileBase_tA53736A507DA935FDC5873236F32953E0B2F886A * get_mobilePlugin_16() const { return ___mobilePlugin_16; }
	inline MobileBase_tA53736A507DA935FDC5873236F32953E0B2F886A ** get_address_of_mobilePlugin_16() { return &___mobilePlugin_16; }
	inline void set_mobilePlugin_16(MobileBase_tA53736A507DA935FDC5873236F32953E0B2F886A * value)
	{
		___mobilePlugin_16 = value;
		Il2CppCodeGenWriteBarrier((&___mobilePlugin_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEMANAGER_T9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543_H
#ifndef MODELTARGETCONTROLLER_TBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A_H
#define MODELTARGETCONTROLLER_TBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelTargetController
struct  ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A  : public AceSingleton_1_tF21F82CEC86CA5D0D7E4541F037D397FF6F97CE5
{
public:
	// UnityEngine.UI.Button ModelTargetController::buttonLunato3
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonLunato3_11;
	// UnityEngine.UI.Button ModelTargetController::buttonBra1439
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonBra1439_12;
	// UnityEngine.UI.Button ModelTargetController::buttonLunato3Real
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonLunato3Real_13;
	// UnityEngine.UI.Button ModelTargetController::buttonBra1439Real
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonBra1439Real_14;
	// UnityEngine.UI.Button ModelTargetController::buttonTestBox
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonTestBox_15;
	// UnityEngine.UI.Button ModelTargetController::buttonTestImage
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonTestImage_16;
	// UnityEngine.UI.Text ModelTargetController::labelInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___labelInfo_17;
	// UnityEngine.UI.Text ModelTargetController::labelDetecting
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___labelDetecting_18;

public:
	inline static int32_t get_offset_of_buttonLunato3_11() { return static_cast<int32_t>(offsetof(ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A, ___buttonLunato3_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonLunato3_11() const { return ___buttonLunato3_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonLunato3_11() { return &___buttonLunato3_11; }
	inline void set_buttonLunato3_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonLunato3_11 = value;
		Il2CppCodeGenWriteBarrier((&___buttonLunato3_11), value);
	}

	inline static int32_t get_offset_of_buttonBra1439_12() { return static_cast<int32_t>(offsetof(ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A, ___buttonBra1439_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonBra1439_12() const { return ___buttonBra1439_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonBra1439_12() { return &___buttonBra1439_12; }
	inline void set_buttonBra1439_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonBra1439_12 = value;
		Il2CppCodeGenWriteBarrier((&___buttonBra1439_12), value);
	}

	inline static int32_t get_offset_of_buttonLunato3Real_13() { return static_cast<int32_t>(offsetof(ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A, ___buttonLunato3Real_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonLunato3Real_13() const { return ___buttonLunato3Real_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonLunato3Real_13() { return &___buttonLunato3Real_13; }
	inline void set_buttonLunato3Real_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonLunato3Real_13 = value;
		Il2CppCodeGenWriteBarrier((&___buttonLunato3Real_13), value);
	}

	inline static int32_t get_offset_of_buttonBra1439Real_14() { return static_cast<int32_t>(offsetof(ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A, ___buttonBra1439Real_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonBra1439Real_14() const { return ___buttonBra1439Real_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonBra1439Real_14() { return &___buttonBra1439Real_14; }
	inline void set_buttonBra1439Real_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonBra1439Real_14 = value;
		Il2CppCodeGenWriteBarrier((&___buttonBra1439Real_14), value);
	}

	inline static int32_t get_offset_of_buttonTestBox_15() { return static_cast<int32_t>(offsetof(ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A, ___buttonTestBox_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonTestBox_15() const { return ___buttonTestBox_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonTestBox_15() { return &___buttonTestBox_15; }
	inline void set_buttonTestBox_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonTestBox_15 = value;
		Il2CppCodeGenWriteBarrier((&___buttonTestBox_15), value);
	}

	inline static int32_t get_offset_of_buttonTestImage_16() { return static_cast<int32_t>(offsetof(ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A, ___buttonTestImage_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonTestImage_16() const { return ___buttonTestImage_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonTestImage_16() { return &___buttonTestImage_16; }
	inline void set_buttonTestImage_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonTestImage_16 = value;
		Il2CppCodeGenWriteBarrier((&___buttonTestImage_16), value);
	}

	inline static int32_t get_offset_of_labelInfo_17() { return static_cast<int32_t>(offsetof(ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A, ___labelInfo_17)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_labelInfo_17() const { return ___labelInfo_17; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_labelInfo_17() { return &___labelInfo_17; }
	inline void set_labelInfo_17(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___labelInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___labelInfo_17), value);
	}

	inline static int32_t get_offset_of_labelDetecting_18() { return static_cast<int32_t>(offsetof(ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A, ___labelDetecting_18)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_labelDetecting_18() const { return ___labelDetecting_18; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_labelDetecting_18() { return &___labelDetecting_18; }
	inline void set_labelDetecting_18(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___labelDetecting_18 = value;
		Il2CppCodeGenWriteBarrier((&___labelDetecting_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELTARGETCONTROLLER_TBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A_H
#ifndef MODELTARGETMENUCONTROLLER_TF343FDB93BB5C0D774B57B5100D047C6E52F4237_H
#define MODELTARGETMENUCONTROLLER_TF343FDB93BB5C0D774B57B5100D047C6E52F4237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelTargetMenuController
struct  ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237  : public AceSingleton_1_tB26A8D9141B90D6F5D5598A18B433B8379A34FC5
{
public:
	// UnityEngine.UI.Button ModelTargetMenuController::buttonLunato3
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonLunato3_11;
	// UnityEngine.UI.Button ModelTargetMenuController::buttonBra1439
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonBra1439_12;
	// UnityEngine.UI.Button ModelTargetMenuController::buttonLunato3Real
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonLunato3Real_13;
	// UnityEngine.UI.Button ModelTargetMenuController::buttonBra1439Real
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonBra1439Real_14;
	// UnityEngine.UI.Button ModelTargetMenuController::buttonTestBox
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonTestBox_15;
	// UnityEngine.UI.Button ModelTargetMenuController::buttonTestImage
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___buttonTestImage_16;

public:
	inline static int32_t get_offset_of_buttonLunato3_11() { return static_cast<int32_t>(offsetof(ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237, ___buttonLunato3_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonLunato3_11() const { return ___buttonLunato3_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonLunato3_11() { return &___buttonLunato3_11; }
	inline void set_buttonLunato3_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonLunato3_11 = value;
		Il2CppCodeGenWriteBarrier((&___buttonLunato3_11), value);
	}

	inline static int32_t get_offset_of_buttonBra1439_12() { return static_cast<int32_t>(offsetof(ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237, ___buttonBra1439_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonBra1439_12() const { return ___buttonBra1439_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonBra1439_12() { return &___buttonBra1439_12; }
	inline void set_buttonBra1439_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonBra1439_12 = value;
		Il2CppCodeGenWriteBarrier((&___buttonBra1439_12), value);
	}

	inline static int32_t get_offset_of_buttonLunato3Real_13() { return static_cast<int32_t>(offsetof(ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237, ___buttonLunato3Real_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonLunato3Real_13() const { return ___buttonLunato3Real_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonLunato3Real_13() { return &___buttonLunato3Real_13; }
	inline void set_buttonLunato3Real_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonLunato3Real_13 = value;
		Il2CppCodeGenWriteBarrier((&___buttonLunato3Real_13), value);
	}

	inline static int32_t get_offset_of_buttonBra1439Real_14() { return static_cast<int32_t>(offsetof(ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237, ___buttonBra1439Real_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonBra1439Real_14() const { return ___buttonBra1439Real_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonBra1439Real_14() { return &___buttonBra1439Real_14; }
	inline void set_buttonBra1439Real_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonBra1439Real_14 = value;
		Il2CppCodeGenWriteBarrier((&___buttonBra1439Real_14), value);
	}

	inline static int32_t get_offset_of_buttonTestBox_15() { return static_cast<int32_t>(offsetof(ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237, ___buttonTestBox_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonTestBox_15() const { return ___buttonTestBox_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonTestBox_15() { return &___buttonTestBox_15; }
	inline void set_buttonTestBox_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonTestBox_15 = value;
		Il2CppCodeGenWriteBarrier((&___buttonTestBox_15), value);
	}

	inline static int32_t get_offset_of_buttonTestImage_16() { return static_cast<int32_t>(offsetof(ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237, ___buttonTestImage_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_buttonTestImage_16() const { return ___buttonTestImage_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_buttonTestImage_16() { return &___buttonTestImage_16; }
	inline void set_buttonTestImage_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___buttonTestImage_16 = value;
		Il2CppCodeGenWriteBarrier((&___buttonTestImage_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELTARGETMENUCONTROLLER_TF343FDB93BB5C0D774B57B5100D047C6E52F4237_H
#ifndef PRODUCTVIEWCONTROLLER_T453D130967D54AC1E1A79D81C59747BDD00A14B0_H
#define PRODUCTVIEWCONTROLLER_T453D130967D54AC1E1A79D81C59747BDD00A14B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductViewController
struct  ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0  : public AceSingleton_1_t11C1EE5F4E8F5C185C78A910C81A57A0BB192AC4
{
public:
	// DLLCore.GeneralSetting ProductViewController::generalSetting
	GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * ___generalSetting_11;
	// DLLCore.GraphicSetting ProductViewController::graphicSetting
	GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 * ___graphicSetting_12;
	// DLLCore.MaterialLoader ProductViewController::materialLoader
	MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * ___materialLoader_13;
	// DLLCore.ModelLoader ProductViewController::modelLoader
	ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * ___modelLoader_14;
	// UnityEngine.GameObject ProductViewController::pointLightPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pointLightPrefab_15;
	// UnityEngine.GameObject[] ProductViewController::_pointLightsForProduct
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ____pointLightsForProduct_16;

public:
	inline static int32_t get_offset_of_generalSetting_11() { return static_cast<int32_t>(offsetof(ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0, ___generalSetting_11)); }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * get_generalSetting_11() const { return ___generalSetting_11; }
	inline GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D ** get_address_of_generalSetting_11() { return &___generalSetting_11; }
	inline void set_generalSetting_11(GeneralSetting_tDF026F5046230C935795FC7E9B91103E4CB2F91D * value)
	{
		___generalSetting_11 = value;
		Il2CppCodeGenWriteBarrier((&___generalSetting_11), value);
	}

	inline static int32_t get_offset_of_graphicSetting_12() { return static_cast<int32_t>(offsetof(ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0, ___graphicSetting_12)); }
	inline GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 * get_graphicSetting_12() const { return ___graphicSetting_12; }
	inline GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 ** get_address_of_graphicSetting_12() { return &___graphicSetting_12; }
	inline void set_graphicSetting_12(GraphicSetting_tCF22B8E10279661682C3A923EAB75DF65F651AA8 * value)
	{
		___graphicSetting_12 = value;
		Il2CppCodeGenWriteBarrier((&___graphicSetting_12), value);
	}

	inline static int32_t get_offset_of_materialLoader_13() { return static_cast<int32_t>(offsetof(ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0, ___materialLoader_13)); }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * get_materialLoader_13() const { return ___materialLoader_13; }
	inline MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE ** get_address_of_materialLoader_13() { return &___materialLoader_13; }
	inline void set_materialLoader_13(MaterialLoader_t99203A90A5B9E9E23EB5E9FEAD6B18B1F9C448DE * value)
	{
		___materialLoader_13 = value;
		Il2CppCodeGenWriteBarrier((&___materialLoader_13), value);
	}

	inline static int32_t get_offset_of_modelLoader_14() { return static_cast<int32_t>(offsetof(ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0, ___modelLoader_14)); }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * get_modelLoader_14() const { return ___modelLoader_14; }
	inline ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D ** get_address_of_modelLoader_14() { return &___modelLoader_14; }
	inline void set_modelLoader_14(ModelLoader_t264992D6536DF569BE72CB7094F9A23CAD68299D * value)
	{
		___modelLoader_14 = value;
		Il2CppCodeGenWriteBarrier((&___modelLoader_14), value);
	}

	inline static int32_t get_offset_of_pointLightPrefab_15() { return static_cast<int32_t>(offsetof(ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0, ___pointLightPrefab_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pointLightPrefab_15() const { return ___pointLightPrefab_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pointLightPrefab_15() { return &___pointLightPrefab_15; }
	inline void set_pointLightPrefab_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pointLightPrefab_15 = value;
		Il2CppCodeGenWriteBarrier((&___pointLightPrefab_15), value);
	}

	inline static int32_t get_offset_of__pointLightsForProduct_16() { return static_cast<int32_t>(offsetof(ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0, ____pointLightsForProduct_16)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get__pointLightsForProduct_16() const { return ____pointLightsForProduct_16; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of__pointLightsForProduct_16() { return &____pointLightsForProduct_16; }
	inline void set__pointLightsForProduct_16(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		____pointLightsForProduct_16 = value;
		Il2CppCodeGenWriteBarrier((&____pointLightsForProduct_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTVIEWCONTROLLER_T453D130967D54AC1E1A79D81C59747BDD00A14B0_H
#ifndef ROOMSCENEUICONTROLLER_TFF371DC30499785C77FEF39142785A3E58E76937_H
#define ROOMSCENEUICONTROLLER_TFF371DC30499785C77FEF39142785A3E58E76937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomSceneUIController
struct  RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937  : public AceSingleton_1_t73730D3AF237C36C9AFBE1DE2458F56FF0180612
{
public:
	// System.Single RoomSceneUIController::btnSizeWidth
	float ___btnSizeWidth_11;
	// System.Single RoomSceneUIController::btnSizeNotchWidth
	float ___btnSizeNotchWidth_12;
	// System.Boolean RoomSceneUIController::isPanningMode
	bool ___isPanningMode_13;
	// System.Boolean RoomSceneUIController::isButtonLock
	bool ___isButtonLock_14;
	// UnityEngine.GameObject RoomSceneUIController::canvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_15;
	// UnityEngine.UI.Text RoomSceneUIController::productText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___productText_16;
	// UnityEngine.UI.Text RoomSceneUIController::startText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___startText_17;
	// UnityEngine.UI.Text RoomSceneUIController::endText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___endText_18;
	// UnityEngine.UI.Text RoomSceneUIController::timeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___timeText_19;
	// UnityEngine.UI.Button RoomSceneUIController::btnBack
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnBack_20;
	// UnityEngine.UI.Button RoomSceneUIController::btnShare
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnShare_21;
	// UnityEngine.UI.Button RoomSceneUIController::btnSave
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnSave_22;
	// UnityEngine.UI.Button RoomSceneUIController::btnItemTap
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnItemTap_23;
	// UnityEngine.UI.Button RoomSceneUIController::btnNorchItemTap
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnNorchItemTap_24;
	// UnityEngine.UI.Button RoomSceneUIController::btnItem
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnItem_25;
	// UnityEngine.UI.Button RoomSceneUIController::btnPosition
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnPosition_26;
	// UnityEngine.UI.Button RoomSceneUIController::btnUnDo
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnUnDo_27;
	// UnityEngine.UI.Button RoomSceneUIController::btnReDo
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnReDo_28;
	// UnityEngine.UI.Button RoomSceneUIController::btnHand
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnHand_29;
	// UnityEngine.UI.Button RoomSceneUIController::btnOption
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnOption_30;
	// UnityEngine.UI.Button RoomSceneUIController::btnStopStyling
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnStopStyling_31;
	// System.Boolean RoomSceneUIController::_isStyling
	bool ____isStyling_32;
	// UnityEngine.GameObject RoomSceneUIController::joystickCotrol
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___joystickCotrol_33;
	// UnityEngine.GameObject RoomSceneUIController::viewRotationControls
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___viewRotationControls_34;
	// System.Boolean RoomSceneUIController::_fpsViewMode
	bool ____fpsViewMode_35;
	// UnityEngine.GameObject RoomSceneUIController::panelTop
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___panelTop_36;
	// UnityEngine.GameObject RoomSceneUIController::panelTest
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___panelTest_37;
	// UnityEngine.GameObject RoomSceneUIController::panelRight
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___panelRight_38;
	// UnityEngine.GameObject RoomSceneUIController::panelSightMode
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___panelSightMode_39;
	// UnityEngine.UI.Button RoomSceneUIController::btnFPMode
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnFPMode_40;
	// UnityEngine.UI.Button RoomSceneUIController::btnTPMode
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btnTPMode_41;
	// UnityEngine.UI.Image RoomSceneUIController::imgFPMode
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___imgFPMode_42;
	// UnityEngine.UI.Image RoomSceneUIController::imgTPMode
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___imgTPMode_43;
	// UnityEngine.Camera RoomSceneUIController::mainCam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCam_44;
	// System.String RoomSceneUIController::imagePathRoot
	String_t* ___imagePathRoot_45;

public:
	inline static int32_t get_offset_of_btnSizeWidth_11() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnSizeWidth_11)); }
	inline float get_btnSizeWidth_11() const { return ___btnSizeWidth_11; }
	inline float* get_address_of_btnSizeWidth_11() { return &___btnSizeWidth_11; }
	inline void set_btnSizeWidth_11(float value)
	{
		___btnSizeWidth_11 = value;
	}

	inline static int32_t get_offset_of_btnSizeNotchWidth_12() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnSizeNotchWidth_12)); }
	inline float get_btnSizeNotchWidth_12() const { return ___btnSizeNotchWidth_12; }
	inline float* get_address_of_btnSizeNotchWidth_12() { return &___btnSizeNotchWidth_12; }
	inline void set_btnSizeNotchWidth_12(float value)
	{
		___btnSizeNotchWidth_12 = value;
	}

	inline static int32_t get_offset_of_isPanningMode_13() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___isPanningMode_13)); }
	inline bool get_isPanningMode_13() const { return ___isPanningMode_13; }
	inline bool* get_address_of_isPanningMode_13() { return &___isPanningMode_13; }
	inline void set_isPanningMode_13(bool value)
	{
		___isPanningMode_13 = value;
	}

	inline static int32_t get_offset_of_isButtonLock_14() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___isButtonLock_14)); }
	inline bool get_isButtonLock_14() const { return ___isButtonLock_14; }
	inline bool* get_address_of_isButtonLock_14() { return &___isButtonLock_14; }
	inline void set_isButtonLock_14(bool value)
	{
		___isButtonLock_14 = value;
	}

	inline static int32_t get_offset_of_canvas_15() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___canvas_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_15() const { return ___canvas_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_15() { return &___canvas_15; }
	inline void set_canvas_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_15 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_15), value);
	}

	inline static int32_t get_offset_of_productText_16() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___productText_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_productText_16() const { return ___productText_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_productText_16() { return &___productText_16; }
	inline void set_productText_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___productText_16 = value;
		Il2CppCodeGenWriteBarrier((&___productText_16), value);
	}

	inline static int32_t get_offset_of_startText_17() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___startText_17)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_startText_17() const { return ___startText_17; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_startText_17() { return &___startText_17; }
	inline void set_startText_17(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___startText_17 = value;
		Il2CppCodeGenWriteBarrier((&___startText_17), value);
	}

	inline static int32_t get_offset_of_endText_18() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___endText_18)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_endText_18() const { return ___endText_18; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_endText_18() { return &___endText_18; }
	inline void set_endText_18(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___endText_18 = value;
		Il2CppCodeGenWriteBarrier((&___endText_18), value);
	}

	inline static int32_t get_offset_of_timeText_19() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___timeText_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_timeText_19() const { return ___timeText_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_timeText_19() { return &___timeText_19; }
	inline void set_timeText_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___timeText_19 = value;
		Il2CppCodeGenWriteBarrier((&___timeText_19), value);
	}

	inline static int32_t get_offset_of_btnBack_20() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnBack_20)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnBack_20() const { return ___btnBack_20; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnBack_20() { return &___btnBack_20; }
	inline void set_btnBack_20(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnBack_20 = value;
		Il2CppCodeGenWriteBarrier((&___btnBack_20), value);
	}

	inline static int32_t get_offset_of_btnShare_21() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnShare_21)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnShare_21() const { return ___btnShare_21; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnShare_21() { return &___btnShare_21; }
	inline void set_btnShare_21(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnShare_21 = value;
		Il2CppCodeGenWriteBarrier((&___btnShare_21), value);
	}

	inline static int32_t get_offset_of_btnSave_22() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnSave_22)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnSave_22() const { return ___btnSave_22; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnSave_22() { return &___btnSave_22; }
	inline void set_btnSave_22(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnSave_22 = value;
		Il2CppCodeGenWriteBarrier((&___btnSave_22), value);
	}

	inline static int32_t get_offset_of_btnItemTap_23() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnItemTap_23)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnItemTap_23() const { return ___btnItemTap_23; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnItemTap_23() { return &___btnItemTap_23; }
	inline void set_btnItemTap_23(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnItemTap_23 = value;
		Il2CppCodeGenWriteBarrier((&___btnItemTap_23), value);
	}

	inline static int32_t get_offset_of_btnNorchItemTap_24() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnNorchItemTap_24)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnNorchItemTap_24() const { return ___btnNorchItemTap_24; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnNorchItemTap_24() { return &___btnNorchItemTap_24; }
	inline void set_btnNorchItemTap_24(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnNorchItemTap_24 = value;
		Il2CppCodeGenWriteBarrier((&___btnNorchItemTap_24), value);
	}

	inline static int32_t get_offset_of_btnItem_25() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnItem_25)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnItem_25() const { return ___btnItem_25; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnItem_25() { return &___btnItem_25; }
	inline void set_btnItem_25(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnItem_25 = value;
		Il2CppCodeGenWriteBarrier((&___btnItem_25), value);
	}

	inline static int32_t get_offset_of_btnPosition_26() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnPosition_26)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnPosition_26() const { return ___btnPosition_26; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnPosition_26() { return &___btnPosition_26; }
	inline void set_btnPosition_26(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnPosition_26 = value;
		Il2CppCodeGenWriteBarrier((&___btnPosition_26), value);
	}

	inline static int32_t get_offset_of_btnUnDo_27() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnUnDo_27)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnUnDo_27() const { return ___btnUnDo_27; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnUnDo_27() { return &___btnUnDo_27; }
	inline void set_btnUnDo_27(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnUnDo_27 = value;
		Il2CppCodeGenWriteBarrier((&___btnUnDo_27), value);
	}

	inline static int32_t get_offset_of_btnReDo_28() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnReDo_28)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnReDo_28() const { return ___btnReDo_28; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnReDo_28() { return &___btnReDo_28; }
	inline void set_btnReDo_28(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnReDo_28 = value;
		Il2CppCodeGenWriteBarrier((&___btnReDo_28), value);
	}

	inline static int32_t get_offset_of_btnHand_29() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnHand_29)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnHand_29() const { return ___btnHand_29; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnHand_29() { return &___btnHand_29; }
	inline void set_btnHand_29(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnHand_29 = value;
		Il2CppCodeGenWriteBarrier((&___btnHand_29), value);
	}

	inline static int32_t get_offset_of_btnOption_30() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnOption_30)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnOption_30() const { return ___btnOption_30; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnOption_30() { return &___btnOption_30; }
	inline void set_btnOption_30(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnOption_30 = value;
		Il2CppCodeGenWriteBarrier((&___btnOption_30), value);
	}

	inline static int32_t get_offset_of_btnStopStyling_31() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnStopStyling_31)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnStopStyling_31() const { return ___btnStopStyling_31; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnStopStyling_31() { return &___btnStopStyling_31; }
	inline void set_btnStopStyling_31(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnStopStyling_31 = value;
		Il2CppCodeGenWriteBarrier((&___btnStopStyling_31), value);
	}

	inline static int32_t get_offset_of__isStyling_32() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ____isStyling_32)); }
	inline bool get__isStyling_32() const { return ____isStyling_32; }
	inline bool* get_address_of__isStyling_32() { return &____isStyling_32; }
	inline void set__isStyling_32(bool value)
	{
		____isStyling_32 = value;
	}

	inline static int32_t get_offset_of_joystickCotrol_33() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___joystickCotrol_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_joystickCotrol_33() const { return ___joystickCotrol_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_joystickCotrol_33() { return &___joystickCotrol_33; }
	inline void set_joystickCotrol_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___joystickCotrol_33 = value;
		Il2CppCodeGenWriteBarrier((&___joystickCotrol_33), value);
	}

	inline static int32_t get_offset_of_viewRotationControls_34() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___viewRotationControls_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_viewRotationControls_34() const { return ___viewRotationControls_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_viewRotationControls_34() { return &___viewRotationControls_34; }
	inline void set_viewRotationControls_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___viewRotationControls_34 = value;
		Il2CppCodeGenWriteBarrier((&___viewRotationControls_34), value);
	}

	inline static int32_t get_offset_of__fpsViewMode_35() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ____fpsViewMode_35)); }
	inline bool get__fpsViewMode_35() const { return ____fpsViewMode_35; }
	inline bool* get_address_of__fpsViewMode_35() { return &____fpsViewMode_35; }
	inline void set__fpsViewMode_35(bool value)
	{
		____fpsViewMode_35 = value;
	}

	inline static int32_t get_offset_of_panelTop_36() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___panelTop_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_panelTop_36() const { return ___panelTop_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_panelTop_36() { return &___panelTop_36; }
	inline void set_panelTop_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___panelTop_36 = value;
		Il2CppCodeGenWriteBarrier((&___panelTop_36), value);
	}

	inline static int32_t get_offset_of_panelTest_37() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___panelTest_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_panelTest_37() const { return ___panelTest_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_panelTest_37() { return &___panelTest_37; }
	inline void set_panelTest_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___panelTest_37 = value;
		Il2CppCodeGenWriteBarrier((&___panelTest_37), value);
	}

	inline static int32_t get_offset_of_panelRight_38() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___panelRight_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_panelRight_38() const { return ___panelRight_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_panelRight_38() { return &___panelRight_38; }
	inline void set_panelRight_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___panelRight_38 = value;
		Il2CppCodeGenWriteBarrier((&___panelRight_38), value);
	}

	inline static int32_t get_offset_of_panelSightMode_39() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___panelSightMode_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_panelSightMode_39() const { return ___panelSightMode_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_panelSightMode_39() { return &___panelSightMode_39; }
	inline void set_panelSightMode_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___panelSightMode_39 = value;
		Il2CppCodeGenWriteBarrier((&___panelSightMode_39), value);
	}

	inline static int32_t get_offset_of_btnFPMode_40() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnFPMode_40)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnFPMode_40() const { return ___btnFPMode_40; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnFPMode_40() { return &___btnFPMode_40; }
	inline void set_btnFPMode_40(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnFPMode_40 = value;
		Il2CppCodeGenWriteBarrier((&___btnFPMode_40), value);
	}

	inline static int32_t get_offset_of_btnTPMode_41() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___btnTPMode_41)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btnTPMode_41() const { return ___btnTPMode_41; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btnTPMode_41() { return &___btnTPMode_41; }
	inline void set_btnTPMode_41(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btnTPMode_41 = value;
		Il2CppCodeGenWriteBarrier((&___btnTPMode_41), value);
	}

	inline static int32_t get_offset_of_imgFPMode_42() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___imgFPMode_42)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_imgFPMode_42() const { return ___imgFPMode_42; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_imgFPMode_42() { return &___imgFPMode_42; }
	inline void set_imgFPMode_42(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___imgFPMode_42 = value;
		Il2CppCodeGenWriteBarrier((&___imgFPMode_42), value);
	}

	inline static int32_t get_offset_of_imgTPMode_43() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___imgTPMode_43)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_imgTPMode_43() const { return ___imgTPMode_43; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_imgTPMode_43() { return &___imgTPMode_43; }
	inline void set_imgTPMode_43(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___imgTPMode_43 = value;
		Il2CppCodeGenWriteBarrier((&___imgTPMode_43), value);
	}

	inline static int32_t get_offset_of_mainCam_44() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___mainCam_44)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mainCam_44() const { return ___mainCam_44; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mainCam_44() { return &___mainCam_44; }
	inline void set_mainCam_44(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mainCam_44 = value;
		Il2CppCodeGenWriteBarrier((&___mainCam_44), value);
	}

	inline static int32_t get_offset_of_imagePathRoot_45() { return static_cast<int32_t>(offsetof(RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937, ___imagePathRoot_45)); }
	inline String_t* get_imagePathRoot_45() const { return ___imagePathRoot_45; }
	inline String_t** get_address_of_imagePathRoot_45() { return &___imagePathRoot_45; }
	inline void set_imagePathRoot_45(String_t* value)
	{
		___imagePathRoot_45 = value;
		Il2CppCodeGenWriteBarrier((&___imagePathRoot_45), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMSCENEUICONTROLLER_TFF371DC30499785C77FEF39142785A3E58E76937_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (ObjectClip2_tC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3800[4] = 
{
	ObjectClip2_tC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435::get_offset_of_mesh_4(),
	ObjectClip2_tC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435::get_offset_of_material_5(),
	ObjectClip2_tC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435::get_offset_of_col_6(),
	ObjectClip2_tC0F6BA905B000D1C57FDC10FAEC3DC1EB8137435::get_offset_of_alpha_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3801[4] = 
{
	test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246::get_offset_of_mesh_4(),
	test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246::get_offset_of_material_5(),
	test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246::get_offset_of_col_6(),
	test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246::get_offset_of_alpha_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (AceAnimationEventListener_tABD975A2D6CC5919672960472446DEE0D0F1F9EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3803[2] = 
{
	AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0::get_offset_of_m_ObjectRenderer_4(),
	AceAnimationHandler_t8AB2C43CACEB82D3808A7D13EAE1BDB7AAB6F0B0::get_offset_of_fadePerSecond_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3804[7] = 
{
	U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2::get_offset_of_U3CU3E1__state_0(),
	U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2::get_offset_of_U3CU3E2__current_1(),
	U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2::get_offset_of_U3CU3E4__this_2(),
	U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2::get_offset_of_aValue_3(),
	U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2::get_offset_of_aTime_4(),
	U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2::get_offset_of_U3CdefaultSizeU3E5__2_5(),
	U3CFadeToU3Ed__6_tB7DE38D4E60439E6390B227410933907DD0B30A2::get_offset_of_U3CtU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { sizeof (AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3805[6] = 
{
	AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A::get_offset_of_mTrackableBehaviour_8(),
	AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A::get_offset_of_m_PreviousStatus_9(),
	AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A::get_offset_of_m_NewStatus_10(),
	AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A::get_offset_of_guidePanel_11(),
	AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A::get_offset_of_textTestInfo_12(),
	AceTrackableEventHandler_t1C1007F64F025B29E9AB06AFD048456F53F42C2A::get_offset_of_count_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { sizeof (ARCameraController_tD9594B7DFD1FB5066CE4F4BBFA65504FD03CE832), -1, sizeof(ARCameraController_tD9594B7DFD1FB5066CE4F4BBFA65504FD03CE832_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3806[1] = 
{
	ARCameraController_tD9594B7DFD1FB5066CE4F4BBFA65504FD03CE832_StaticFields::get_offset_of_isInitVuforia_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { sizeof (ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3807[15] = 
{
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_animationNameList_11(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of__IsDetected_12(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_btnBack_13(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_btnTutorial_14(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_guidePanel_15(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_panelMenu_16(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_btnAni1_17(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_btnAni2_18(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_btnAni3_19(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_panelEnd_20(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_btnMoveMenu_21(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_midAirPosition_22(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_IntAniObjScale_23(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_animationName_24(),
	ARUIController_tAAA4895E4957981A07F95C8FB2941F2BE51CA8F1::get_offset_of_lastState_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { sizeof (GUIState_tF63BEA966B3EDD0C31938A1DFC1D973A9F511DB9)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3808[5] = 
{
	GUIState_tF63BEA966B3EDD0C31938A1DFC1D973A9F511DB9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { sizeof (MattressAnimation_tF86AB46699E5B0F061EC62D9D95B5F4330C221D3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3809[4] = 
{
	MattressAnimation_tF86AB46699E5B0F061EC62D9D95B5F4330C221D3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3810[6] = 
{
	U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B::get_offset_of_U3CU3E1__state_0(),
	U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B::get_offset_of_U3CU3E2__current_1(),
	U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B::get_offset_of_U3CU3E4__this_2(),
	U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B::get_offset_of_aniNo_3(),
	U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B::get_offset_of_U3CpositionU3E5__2_4(),
	U3CPlayAnimationU3Ed__24_t53ADB8DD2FFDC9B6039ECE4AA3CEBAAC9570715B::get_offset_of_U3CloadAsyncU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (TestScaleController_tA2062EA7A2A0F815B94591A5DF41332FDC904852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3811[3] = 
{
	TestScaleController_tA2062EA7A2A0F815B94591A5DF41332FDC904852::get_offset_of_btnSizeUp_4(),
	TestScaleController_tA2062EA7A2A0F815B94591A5DF41332FDC904852::get_offset_of_btnSizeDown_5(),
	TestScaleController_tA2062EA7A2A0F815B94591A5DF41332FDC904852::get_offset_of_textSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3812[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E), -1, sizeof(BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3813[4] = 
{
	BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E_StaticFields::get_offset_of__sceneMode_4(),
	BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E_StaticFields::get_offset_of__oldSceneMode_5(),
	BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E_StaticFields::get_offset_of_mobileManager_6(),
	BaseController_tAA16D17567994685DA8E4473CFF8D6445DA5179E_StaticFields::get_offset_of_isReloadScene_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (ACESceneMode_t07127C4981D5FBF5B79A781E16FD26B6C08E2C72)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3814[10] = 
{
	ACESceneMode_t07127C4981D5FBF5B79A781E16FD26B6C08E2C72::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3815[5] = 
{
	U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51::get_offset_of_U3CU3E1__state_0(),
	U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51::get_offset_of_U3CU3E2__current_1(),
	U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51::get_offset_of_U3CU3E4__this_2(),
	U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51::get_offset_of_sceneName_3(),
	U3CLoadSceneU3Ed__10_t3EB18C705C001ABBC54CA9C549377F15582AEB51::get_offset_of_U3CasyncU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3816[5] = 
{
	U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C::get_offset_of_U3CU3E1__state_0(),
	U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C::get_offset_of_U3CU3E2__current_1(),
	U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C::get_offset_of_U3CU3E4__this_2(),
	U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C::get_offset_of_sceneName_3(),
	U3CLoadSceneNonLoadingU3Ed__11_tBE89655DC3F8A54361E96EF9D83ABD667F4D704C::get_offset_of_U3CasyncU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (U3CreloadSceneProcessU3Ed__29_tD9E3C3AD45C4DDC9959F865899D22C2D187B3ABD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3817[2] = 
{
	U3CreloadSceneProcessU3Ed__29_tD9E3C3AD45C4DDC9959F865899D22C2D187B3ABD::get_offset_of_U3CU3E1__state_0(),
	U3CreloadSceneProcessU3Ed__29_tD9E3C3AD45C4DDC9959F865899D22C2D187B3ABD::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (DefineWord_t2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04), -1, sizeof(DefineWord_t2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3818[2] = 
{
	DefineWord_t2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04_StaticFields::get_offset_of___IS_SHOW_LOG_0(),
	DefineWord_t2CAA0ABCA4E7ADE2929F8C37F8E789C9973B3F04_StaticFields::get_offset_of___IS_TEST_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (ModalController_tD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3819[4] = 
{
	ModalController_tD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD::get_offset_of_callbackType_8(),
	ModalController_tD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD::get_offset_of_callback_9(),
	ModalController_tD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD::get_offset_of_callbackClose_10(),
	ModalController_tD9D7FAC932CC5E1AE2893B4CB7D278B092BF9EBD::get_offset_of_callbackPop_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { sizeof (CallbackType_tE5CC0E106E4AF1B685B5B9940F689A2464C63C41), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (Callback_t5433D5269B1108D8BFECDCD02079FCB1211A6CF8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (CallbackClose_t4CBEE1DC735AF36C5814923F4360DBB1949CD115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (CallbackPop_t11746E625CF12C9917AD4DB2B9A57E2597D32609), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { sizeof (TestInvisibleObj_t15F42714A2709618DA7FA5DE3DF98317C576AD96), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (TestVisibleObj_t03EFFD04D370B3ECCAB2774C5FDC2575EA7FA051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { sizeof (DefaultMainCamera_t5CCBA3F2B0D86DA34C24E30E5149A7FD6B112B6C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { sizeof (DefaultSceneController_t823BFAEBA080AA5B2E0951BDA43945120F4EF5E1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { sizeof (DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3828[10] = 
{
	DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21::get_offset_of_btnLoadARTargetScene_8(),
	DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21::get_offset_of_btnLoadRoomScene_9(),
	DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21::get_offset_of_btnLoadUrbanRoomScene_10(),
	DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21::get_offset_of_btnLoadUrbanProductViewerScene_11(),
	DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21::get_offset_of_btnLoadModeltTargetScene_12(),
	DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21::get_offset_of_btnCallToast_13(),
	DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21::get_offset_of_btnVersionCheck_14(),
	DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21::get_offset_of_btnLoadVuTestScene1_15(),
	DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21::get_offset_of_btnLoadVuTestScene2_16(),
	DefaultSceneUIController_t4CE54DDE1F8E263B6AB8FF89B6DF1F795593BD21::get_offset_of_panelTest_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { sizeof (MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3829[24] = 
{
	0,
	0,
	0,
	0,
	0,
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_resultDisplay_9(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_blowDisplay_10(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_recordedLength_11(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_requiedBlowTime_12(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_clamp_13(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_rmsValue_14(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_dbValue_15(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_pitchValue_16(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_blowingTime_17(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_lowPassResults_18(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_peakPowerForChannel_19(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_samples_20(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_spectrum_21(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_dbValues_22(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_pitchValues_23(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_audioSource_24(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_sensitivity_25(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of_loudness_26(),
	MicHandle_t489A8D31AD74830D6936CF0463843049A2C8CBF9::get_offset_of__audio_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { sizeof (GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3830[5] = 
{
	GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0::get_offset_of_map_4(),
	GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0::get_offset_of_style_5(),
	GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0::get_offset_of_design_6(),
	GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0::get_offset_of_help_7(),
	GNBController_t2BCB4DA5E1243B852370C0779679F133FC062CF0::get_offset_of_mapView_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { sizeof (HideSystemUI_t4D36167744172806784B6EC2B2CC36B81B004D0E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { sizeof (ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3832[12] = 
{
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_assembly_7(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_gainedKey_8(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_gainedIv_9(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of__main_10(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_generalSetting_11(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_graphicSetting_12(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_terminal_13(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_eventManager_14(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_reflectionControls_15(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_daylightController_16(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_materialLoader_17(),
	ImportCoreDLL_t6A7D7920A8BC7EA7C1FBB338BD056659C3A6F692::get_offset_of_modelLoader_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { sizeof (Math3d_tD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65), -1, sizeof(Math3d_tD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3833[2] = 
{
	Math3d_tD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65_StaticFields::get_offset_of_tempChild_4(),
	Math3d_tD6EBFA7455AE4B7FCD0771460B3C3B20EB4CAA65_StaticFields::get_offset_of_tempParent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { sizeof (MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3834[11] = 
{
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_animationNameList_11(),
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_btnBack_12(),
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_btnTutorial_13(),
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_panelMenu_14(),
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_btnAni1_15(),
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_btnAni2_16(),
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_btnAni3_17(),
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_mattressButton_18(),
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_midAirPosition_19(),
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_IntAniObjScale_20(),
	MattressUIController_t99B279DB79F40633115ABF233E0592DFF622F191::get_offset_of_animationName_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { sizeof (MattressAnimation_t43B6DA74DA83A07619E3174B2DC51AD84C3F259B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3835[4] = 
{
	MattressAnimation_t43B6DA74DA83A07619E3174B2DC51AD84C3F259B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { sizeof (U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3836[6] = 
{
	U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4::get_offset_of_U3CU3E1__state_0(),
	U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4::get_offset_of_U3CU3E2__current_1(),
	U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4::get_offset_of_U3CU3E4__this_2(),
	U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4::get_offset_of_aniNo_3(),
	U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4::get_offset_of_U3CpositionU3E5__2_4(),
	U3CPlayAnimationU3Ed__18_t7AC85838E12FB0A91A76AC4971F61B2E25F644F4::get_offset_of_U3CloadAsyncU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { sizeof (MobileAndroid_t731945698A11BE5075D526156BDB071A99BD9688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3837[1] = 
{
	MobileAndroid_t731945698A11BE5075D526156BDB071A99BD9688::get_offset_of_UnityPlayerActivity_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (MobileBase_tA53736A507DA935FDC5873236F32953E0B2F886A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { sizeof (MobileIOS_t765B18F4DA004AB004993862B663DA65E57BF62A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { sizeof (MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3840[6] = 
{
	MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543::get_offset_of_isNotch_11(),
	MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543::get_offset_of_strJsonHomeUnit_12(),
	MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543::get_offset_of_strModelTargetString_13(),
	MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543::get_offset_of_mainCam_14(),
	MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543::get_offset_of_loadingCam_15(),
	MobileManager_t9B85439ACB8CB02F19AFAAD0EEB1BF21582F5543::get_offset_of_mobilePlugin_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3841[8] = 
{
	ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A::get_offset_of_buttonLunato3_11(),
	ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A::get_offset_of_buttonBra1439_12(),
	ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A::get_offset_of_buttonLunato3Real_13(),
	ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A::get_offset_of_buttonBra1439Real_14(),
	ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A::get_offset_of_buttonTestBox_15(),
	ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A::get_offset_of_buttonTestImage_16(),
	ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A::get_offset_of_labelInfo_17(),
	ModelTargetController_tBA3979F01CF90D7EA52C2C2A0B458BDCA69BB61A::get_offset_of_labelDetecting_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (ModelTarget_tF1115D7753DC988A2537D2F4ECE4D584A186B6D1)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3842[7] = 
{
	ModelTarget_tF1115D7753DC988A2537D2F4ECE4D584A186B6D1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3843[5] = 
{
	U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA::get_offset_of_U3CU3E1__state_0(),
	U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA::get_offset_of_U3CU3E2__current_1(),
	U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA::get_offset_of_U3CU3E4__this_2(),
	U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA::get_offset_of_U3CprefabNameU3E5__2_3(),
	U3CaddModelTargetU3Ed__12_t731D004438978B7025B321A45E502F1D6A9644CA::get_offset_of_U3CloadAsyncU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3844[6] = 
{
	ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237::get_offset_of_buttonLunato3_11(),
	ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237::get_offset_of_buttonBra1439_12(),
	ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237::get_offset_of_buttonLunato3Real_13(),
	ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237::get_offset_of_buttonBra1439Real_14(),
	ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237::get_offset_of_buttonTestBox_15(),
	ModelTargetMenuController_tF343FDB93BB5C0D774B57B5100D047C6E52F4237::get_offset_of_buttonTestImage_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { sizeof (ModelTarget_t6C8F9568FC79DFAD1E368E2ED61C817E12004274)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3845[7] = 
{
	ModelTarget_t6C8F9568FC79DFAD1E368E2ED61C817E12004274::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3846[5] = 
{
	U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C::get_offset_of_U3CU3E1__state_0(),
	U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C::get_offset_of_U3CU3E2__current_1(),
	U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C::get_offset_of_U3CU3E4__this_2(),
	U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C::get_offset_of_modelTargetName_3(),
	U3CLoadSceneU3Ed__10_tE0A8C2B0BC1238B41EE2BAD428BE68CCD75A820C::get_offset_of_U3CasyncU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (ModelTrackerbleEventHandler_t6355CEED827CB2BBF9179447C1FF6968597ADBEB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3847[4] = 
{
	ModelTrackerbleEventHandler_t6355CEED827CB2BBF9179447C1FF6968597ADBEB::get_offset_of_mTrackableBehaviour_8(),
	ModelTrackerbleEventHandler_t6355CEED827CB2BBF9179447C1FF6968597ADBEB::get_offset_of_m_PreviousStatus_9(),
	ModelTrackerbleEventHandler_t6355CEED827CB2BBF9179447C1FF6968597ADBEB::get_offset_of_m_NewStatus_10(),
	ModelTrackerbleEventHandler_t6355CEED827CB2BBF9179447C1FF6968597ADBEB::get_offset_of_count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { sizeof (ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3848[2] = 
{
	ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618::get_offset_of_prefabButton_4(),
	ProductListButton_t106B62A2104F017BE699A92DD9A2D55CC500E618::get_offset_of_productListButtons_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { sizeof (ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3849[5] = 
{
	ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A::get_offset_of__button_0(),
	ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A::get_offset_of__id_1(),
	ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A::get_offset_of__path_2(),
	ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A::get_offset_of__size_3(),
	ProductData_t97D10D5108DB17AE26145FBB17907DAA56072F4A::get_offset_of__color_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { sizeof (U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3850[5] = 
{
	U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3::get_offset_of_name_1(),
	U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3::get_offset_of_size_2(),
	U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3::get_offset_of_color_3(),
	U3CU3Ec__DisplayClass5_0_tC55BA8BB7F57362228974D9689BE7A4A0D0DE7C3::get_offset_of_thumbnail_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3851[5] = 
{
	U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452::get_offset_of_U3CU3E1__state_0(),
	U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452::get_offset_of_U3CU3E2__current_1(),
	U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452::get_offset_of_path_2(),
	U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452::get_offset_of_button_3(),
	U3CGetTextureU3Ed__6_t2ADA756DD71C86ADD758DC706E76CF3A6AAFF452::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { sizeof (U3CU3Ec__DisplayClass9_0_tE639376AE3E8B6EF41E0DFF630D85B48FC499252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3852[1] = 
{
	U3CU3Ec__DisplayClass9_0_tE639376AE3E8B6EF41E0DFF630D85B48FC499252::get_offset_of_frameID_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { sizeof (ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3853[10] = 
{
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87::get_offset_of_backButton_4(),
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87::get_offset_of_placeButton_5(),
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87::get_offset_of_productName_6(),
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87::get_offset_of_sizeDropdown_7(),
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87::get_offset_of_colorButtons_8(),
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87::get_offset_of_mattressDropdown_9(),
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87::get_offset_of_productImage_10(),
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87::get_offset_of_sizeIndex_11(),
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87::get_offset_of_colorIndex_12(),
	ProductOptionUIManager_tD33196F16077A1DB55EFFB396D6D06984747AB87::get_offset_of_mattressIndex_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { sizeof (U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3854[5] = 
{
	U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0::get_offset_of_U3CU3E1__state_0(),
	U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0::get_offset_of_U3CU3E2__current_1(),
	U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0::get_offset_of_path_2(),
	U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0::get_offset_of_U3CU3E4__this_3(),
	U3CLoadProductImageU3Ed__12_tBD789C839A9435513E33692AD0C3CADFD93EECE0::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { sizeof (ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD), -1, sizeof(ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3855[6] = 
{
	ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD_StaticFields::get_offset_of_FILTER_TOTAL_4(),
	ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD_StaticFields::get_offset_of_FILTER_WEDDING_5(),
	ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD_StaticFields::get_offset_of_FILTER_SUPERSINGLE_6(),
	ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD_StaticFields::get_offset_of_FILTER_FAMILY_7(),
	ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD::get_offset_of_scrollUpButton_8(),
	ProductScrollList_tC73A3012458878C80956F749EE055A016FA363FD::get_offset_of_filter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3856[6] = 
{
	ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0::get_offset_of_generalSetting_11(),
	ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0::get_offset_of_graphicSetting_12(),
	ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0::get_offset_of_materialLoader_13(),
	ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0::get_offset_of_modelLoader_14(),
	ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0::get_offset_of_pointLightPrefab_15(),
	ProductViewController_t453D130967D54AC1E1A79D81C59747BDD00A14B0::get_offset_of__pointLightsForProduct_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { sizeof (HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363), -1, sizeof(HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3857[11] = 
{
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363_StaticFields::get_offset_of_BASE_PATH_11(),
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363::get_offset_of_btnLoadRoom_12(),
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363::get_offset_of_btnLoadProduct_13(),
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363::get_offset_of_btnClearAll_14(),
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363::get_offset_of_btnOptionHigh_15(),
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363::get_offset_of_btnOptionMiddle_16(),
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363::get_offset_of_btnOptionLow_17(),
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363::get_offset_of_btnOptionCollision_18(),
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363::get_offset_of_btnWallPaper_19(),
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363::get_offset_of_btnUnitFloor_20(),
	HomeTestController_tF66A2DC20999ECA1956EA0CCEAFDDECB83652363::get_offset_of_btnStyleEditorEnd_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { sizeof (RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3858[35] = 
{
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnSizeWidth_11(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnSizeNotchWidth_12(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_isPanningMode_13(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_isButtonLock_14(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_canvas_15(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_productText_16(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_startText_17(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_endText_18(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_timeText_19(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnBack_20(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnShare_21(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnSave_22(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnItemTap_23(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnNorchItemTap_24(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnItem_25(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnPosition_26(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnUnDo_27(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnReDo_28(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnHand_29(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnOption_30(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnStopStyling_31(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of__isStyling_32(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_joystickCotrol_33(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_viewRotationControls_34(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of__fpsViewMode_35(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_panelTop_36(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_panelTest_37(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_panelRight_38(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_panelSightMode_39(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnFPMode_40(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_btnTPMode_41(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_imgFPMode_42(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_imgTPMode_43(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_mainCam_44(),
	RoomSceneUIController_tFF371DC30499785C77FEF39142785A3E58E76937::get_offset_of_imagePathRoot_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { sizeof (U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309), -1, sizeof(U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3859[4] = 
{
	U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309_StaticFields::get_offset_of_U3CU3E9__34_4_1(),
	U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309_StaticFields::get_offset_of_U3CU3E9__34_5_2(),
	U3CU3Ec_tFC53A04844766505D14CA1506B02CE5817038309_StaticFields::get_offset_of_U3CU3E9__34_6_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (U3CScreenShotSaveU3Ed__37_tCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3860[3] = 
{
	U3CScreenShotSaveU3Ed__37_tCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE::get_offset_of_U3CU3E1__state_0(),
	U3CScreenShotSaveU3Ed__37_tCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE::get_offset_of_U3CU3E2__current_1(),
	U3CScreenShotSaveU3Ed__37_tCA3FEC3DA8FE27D9710A7D7CA8C4B58F620FAAEE::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (U3CScreenShotShareU3Ed__38_tEB503A12F87879EEFEB98DE6803E5E2C77876FDE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3861[3] = 
{
	U3CScreenShotShareU3Ed__38_tEB503A12F87879EEFEB98DE6803E5E2C77876FDE::get_offset_of_U3CU3E1__state_0(),
	U3CScreenShotShareU3Ed__38_tEB503A12F87879EEFEB98DE6803E5E2C77876FDE::get_offset_of_U3CU3E2__current_1(),
	U3CScreenShotShareU3Ed__38_tEB503A12F87879EEFEB98DE6803E5E2C77876FDE::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3862[5] = 
{
	U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72::get_offset_of_U3CU3E1__state_0(),
	U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72::get_offset_of_U3CU3E2__current_1(),
	U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72::get_offset_of_path_2(),
	U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72::get_offset_of_U3CU3E4__this_3(),
	U3CScreenShotSaveConfirmU3Ed__39_t537CCB2088709400A3703418EBB90A36FEDCDE72::get_offset_of_U3CcountU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3863[5] = 
{
	U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B::get_offset_of_U3CU3E1__state_0(),
	U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B::get_offset_of_U3CU3E2__current_1(),
	U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B::get_offset_of_path_2(),
	U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B::get_offset_of_U3CU3E4__this_3(),
	U3CScreenShotShareConfirmU3Ed__40_tB43F09C624893450A723A1178992A02A63E5066B::get_offset_of_U3CcountU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3864[6] = 
{
	SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510::get_offset_of_closeButton_4(),
	SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510::get_offset_of_highToggle_5(),
	SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510::get_offset_of_middleToggle_6(),
	SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510::get_offset_of_lowToggle_7(),
	SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510::get_offset_of_wallColliderOffToggle_8(),
	SettingPanelController_t9363FFE1338F83CFD866857A3B9462FD8E9F1510::get_offset_of_linkButton_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { sizeof (ToggleLevel_tCA87FAE3CA87EF43267012272682E9356CDAE806)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3865[4] = 
{
	ToggleLevel_tCA87FAE3CA87EF43267012272682E9356CDAE806::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { sizeof (UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE), -1, sizeof(UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3866[61] = 
{
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE_StaticFields::get_offset_of_BASE_PATH_4(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_loadUnitA_5(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_loadUnitB_6(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_loadUnitC_7(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_calcDistance_8(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_endCalcDistance_9(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_calcArea_10(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_endCalcArea_11(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_loadProduct_12(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_loadProduct2_13(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_loadProduct3_14(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_lightDirectionText_15(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_lightDirection_16(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_clearUnit_17(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_requestMapAPIBtn_18(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_inputFieldSearchAddress_19(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_pinButton_20(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_joystickCotrol_21(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_viewRotationControls_22(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__fpsViewMode_23(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_topPanel_24(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_rightMenuPanel_25(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_backToMain_26(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_screenShot_27(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_alignment_28(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_undo_29(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_redo_30(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_cameraModePanning_31(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_cameraModeOrbit_32(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_settings_33(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_fpsModeToggle_34(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_orbitModeToggle_35(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_stopStyling_36(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_canvas_37(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_productScrollList_38(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_productListButton_39(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_showItemList_40(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_itemListPanel_41(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_hideItemList_42(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_tabBed_43(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_tabWall_44(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_tabFloor_45(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_wallpaperListPanel_46(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_floorListPanel_47(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_wallpaperButtons_48(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_floorButtons_49(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__panningMode_50(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__listOptionCombination_51(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__frameProductList_52(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__mattressProductList_53(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__mattressPathMap_54(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__mattressListSize_55(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_productOptionUIManager_56(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_settingPanelController_57(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__wallpaperJsonMap_58(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__floorJsonMap_59(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__wallpaperIdList_60(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__floorIdList_61(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__productJsonList_62(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of__pointLightsForUnit_63(),
	UIController_t06FB25185FB9B776EF16705D93C83EE95CE1D1EE::get_offset_of_pointLightPrefab_64(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { sizeof (OptionData_t2EA1EFBB927710E7ABFE05D6F203E09FD3649927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3867[4] = 
{
	OptionData_t2EA1EFBB927710E7ABFE05D6F203E09FD3649927::get_offset_of_name_0(),
	OptionData_t2EA1EFBB927710E7ABFE05D6F203E09FD3649927::get_offset_of_sizeOption_1(),
	OptionData_t2EA1EFBB927710E7ABFE05D6F203E09FD3649927::get_offset_of_colorOption_2(),
	OptionData_t2EA1EFBB927710E7ABFE05D6F203E09FD3649927::get_offset_of_filter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { sizeof (FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3868[6] = 
{
	FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22::get_offset_of_frameID_0(),
	FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22::get_offset_of_frameName_1(),
	FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22::get_offset_of_frameSize_2(),
	FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22::get_offset_of_frameColor_3(),
	FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22::get_offset_of_path_4(),
	FrameData_t0374CCD41A750DB3016CDFC0D537D19C95F05D22::get_offset_of_thumbnail_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { sizeof (ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3869[7] = 
{
	ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0::get_offset_of_frameId_0(),
	ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0::get_offset_of_name_1(),
	ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0::get_offset_of_size_2(),
	ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0::get_offset_of_color_3(),
	ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0::get_offset_of_framePath_4(),
	ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0::get_offset_of_thumbnailPath_5(),
	ProductJsonInfo_tD05B0BADB053B76F25882940F9FA94857EEBD7C0::get_offset_of_catalogThumbnail_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { sizeof (ParseFunction_tC04C029BB8EC10B09DE25A5B0AD388B75C177EA6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { sizeof (ParseFunctionWithPameter_t43FB9510DA0A33033E4DA482AEACCCD943F8FC0A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { sizeof (U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B), -1, sizeof(U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3872[8] = 
{
	U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields::get_offset_of_U3CU3E9__68_0_1(),
	U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields::get_offset_of_U3CU3E9__68_1_2(),
	U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields::get_offset_of_U3CU3E9__84_0_3(),
	U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields::get_offset_of_U3CU3E9__85_0_4(),
	U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields::get_offset_of_U3CU3E9__88_0_5(),
	U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields::get_offset_of_U3CU3E9__95_0_6(),
	U3CU3Ec_t4F418134860FCB9C317EE13C7F876D2FDE26BD5B_StaticFields::get_offset_of_U3CU3E9__121_0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { sizeof (U3CScreenShotU3Ed__95_tE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3873[3] = 
{
	U3CScreenShotU3Ed__95_tE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47::get_offset_of_U3CU3E1__state_0(),
	U3CScreenShotU3Ed__95_tE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47::get_offset_of_U3CU3E2__current_1(),
	U3CScreenShotU3Ed__95_tE9A5AA3A3C0F5DBDC58E7AFA5283C5697109EF47::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { sizeof (U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3874[5] = 
{
	U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697::get_offset_of_U3CU3E1__state_0(),
	U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697::get_offset_of_U3CU3E2__current_1(),
	U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697::get_offset_of_url_2(),
	U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697::get_offset_of_callback_3(),
	U3CGetParseFromJSONU3Ed__106_t4555CCD989BED3756776F08E2246A2B29A285697::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { sizeof (U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3875[6] = 
{
	U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437::get_offset_of_U3CU3E1__state_0(),
	U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437::get_offset_of_U3CU3E2__current_1(),
	U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437::get_offset_of_url_2(),
	U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437::get_offset_of_callback_3(),
	U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437::get_offset_of_createUI_4(),
	U3CGetParseFromJSONAndCallbackU3Ed__107_tBECA1D38B02262689AF1EE4BD318DF51BEBE6437::get_offset_of_U3CwwwU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (U3CU3Ec__DisplayClass112_0_tB46A03BC2A3B17DFAF7050D62CEA5FEC3C107C07), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3876[1] = 
{
	U3CU3Ec__DisplayClass112_0_tB46A03BC2A3B17DFAF7050D62CEA5FEC3C107C07::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (U3CU3Ec__DisplayClass113_0_t450925695AB92BBDA7EF4569CB58F31B212284DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3877[1] = 
{
	U3CU3Ec__DisplayClass113_0_t450925695AB92BBDA7EF4569CB58F31B212284DC::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (UISingleton_t4848BA88F4BA51DBD493AEB09C803CA5D8B5D051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3878[1] = 
{
	UISingleton_t4848BA88F4BA51DBD493AEB09C803CA5D8B5D051::get_offset_of__uiController_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3879[6] = 
{
	BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB::get_offset_of_SETTING_COLOUR_4(),
	BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB::get_offset_of_SUB_SETTING_COLOUR_5(),
	BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB::get_offset_of_m_labelText_6(),
	BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB::get_offset_of_m_bgImage_7(),
	BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB::get_offset_of_m_subSetting_8(),
	BaseVariableSetting_t8EE7D6CE7053CBC25280AF91B25E4C335CB24FDB::get_offset_of_m_subSettingActive_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3880[16] = 
{
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_fromValue_10(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_toValue_11(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_thenValue_12(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_fromHighlight_13(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_toHighlight_14(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_thenHighlight_15(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_toValueObject_16(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_thenValueObject_17(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_colourPickerSection_18(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_colourPicker_19(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_sectionLayoutElement_20(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_colourSelected_21(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_currentColourIndex_22(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_stateListenerCallbacks_23(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_valueChangedCallback_24(),
	ColourVariableSetting_t6EC7DCCDB5BC0987AEACCC1A1478017C9D33964B::get_offset_of_m_colours_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { sizeof (FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3881[5] = 
{
	FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3::get_offset_of_m_fromValue_10(),
	FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3::get_offset_of_m_toValue_11(),
	FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3::get_offset_of_m_thenValue_12(),
	FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3::get_offset_of_m_toValueObject_13(),
	FloatVariableSetting_tF2CEEA04C3A953A33DF12BD7991CA62A3F747CD3::get_offset_of_m_thenValueObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { sizeof (U3CU3Ec__DisplayClass5_0_t161DB36F028399DE99B9B6DA061E4351DA20CA79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3882[2] = 
{
	U3CU3Ec__DisplayClass5_0_t161DB36F028399DE99B9B6DA061E4351DA20CA79::get_offset_of_varStateListeners_0(),
	U3CU3Ec__DisplayClass5_0_t161DB36F028399DE99B9B6DA061E4351DA20CA79::get_offset_of_valueChangedCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { sizeof (HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3883[2] = 
{
	HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB::get_offset_of_picker_4(),
	HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB::get_offset_of_slider_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { sizeof (ColorScrollRect_t67B3D6242798310021DFDAEA7BA67CD622B66267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3884[1] = 
{
	ColorScrollRect_t67B3D6242798310021DFDAEA7BA67CD622B66267::get_offset_of_U3CDraggingU3Ek__BackingField_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { sizeof (TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3885[35] = 
{
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_titleEffect_4(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_animEditorContent_5(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_effect_6(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_SECTION_INACTIVE_COLOUR_7(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_SECTION_ACTIVE_COLOUR_8(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_SECTION_SELECTED_COLOUR_9(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_sectionHeaderImages_10(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_menuSectionsContainer_11(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_animSectionTransforms_12(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_animSectionDropdowns_13(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_playButton_14(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_playButtonText_15(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_resetButton_16(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_continueButton_17(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_floatVariableSettingPrefab_18(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_vector3VariableSettingPrefab_19(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_toggleVariableSettingPrefab_20(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_colourVariableSettingPrefab_21(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_textInput_22(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_introStartEffect_23(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_mainStartEffect_24(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_outroStartEffect_25(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_skipTitleEffectIntro_26(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_animationSections_27(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_floatVariableSettingsPool_28(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_vector3VariableSettingsPool_29(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_toggleVariableSettingsPool_30(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_colourVariableSettingsPool_31(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_floatVariableSettingsInUse_32(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_vector3VariableSettingsInUse_33(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_toggleVariableSettingsInUse_34(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_colourVariableSettingsInUse_35(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_sectionsActive_36(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_activeSectionIndex_37(),
	TextFxDemoManager_tF0E9B4458C1CADF282EC3E0D2766DE050B3D3136::get_offset_of_m_playbackButtonHighlightColour_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { sizeof (U3CU3Ec__DisplayClass35_0_tCC418BF00BC4442CCA5F5EF613B8BA9EC9D04296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3886[2] = 
{
	U3CU3Ec__DisplayClass35_0_tCC418BF00BC4442CCA5F5EF613B8BA9EC9D04296::get_offset_of_animSection_0(),
	U3CU3Ec__DisplayClass35_0_tCC418BF00BC4442CCA5F5EF613B8BA9EC9D04296::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { sizeof (U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3887[8] = 
{
	U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE::get_offset_of_U3CU3E1__state_0(),
	U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE::get_offset_of_U3CU3E2__current_1(),
	U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE::get_offset_of_U3CU3E4__this_2(),
	U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE::get_offset_of_skipTitleEffect_3(),
	U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE::get_offset_of_waitTime_4(),
	U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE::get_offset_of_U3CtimerU3E5__2_5(),
	U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE::get_offset_of_U3CrotationU3E5__3_6(),
	U3CDoIntroU3Ed__36_tA11DBD947A4B16B94F222F16143E6B7EC3ACE0DE::get_offset_of_U3CrotationVecU3E5__4_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { sizeof (U3CHighlightContinueButtonU3Ed__40_t36630BA04716C4FF967218B5C26EFBA81435A13D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3888[3] = 
{
	U3CHighlightContinueButtonU3Ed__40_t36630BA04716C4FF967218B5C26EFBA81435A13D::get_offset_of_U3CU3E1__state_0(),
	U3CHighlightContinueButtonU3Ed__40_t36630BA04716C4FF967218B5C26EFBA81435A13D::get_offset_of_U3CU3E2__current_1(),
	U3CHighlightContinueButtonU3Ed__40_t36630BA04716C4FF967218B5C26EFBA81435A13D::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { sizeof (U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3889[2] = 
{
	U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass46_0_t8DCC70B857AE08233984EF6952A3C1E6DE52A06E::get_offset_of_animation_section_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { sizeof (U3CU3Ec__DisplayClass46_1_tA6570BCFA826B4804D08290D1CA0F8E1C2594054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3890[4] = 
{
	U3CU3Ec__DisplayClass46_1_tA6570BCFA826B4804D08290D1CA0F8E1C2594054::get_offset_of_dataType_0(),
	U3CU3Ec__DisplayClass46_1_tA6570BCFA826B4804D08290D1CA0F8E1C2594054::get_offset_of_vecDataType_1(),
	U3CU3Ec__DisplayClass46_1_tA6570BCFA826B4804D08290D1CA0F8E1C2594054::get_offset_of_colDataType_2(),
	U3CU3Ec__DisplayClass46_1_tA6570BCFA826B4804D08290D1CA0F8E1C2594054::get_offset_of_CSU24U3CU3E8__locals1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { sizeof (U3CU3Ec__DisplayClass46_2_tF9FBD84656E1D0826FC1604255D0B37555C67826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3891[2] = 
{
	U3CU3Ec__DisplayClass46_2_tF9FBD84656E1D0826FC1604255D0B37555C67826::get_offset_of_floatVarSetting_0(),
	U3CU3Ec__DisplayClass46_2_tF9FBD84656E1D0826FC1604255D0B37555C67826::get_offset_of_CSU24U3CU3E8__locals2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { sizeof (ToggleVariableSetting_tE79CD9769496E78E42260EB965B78F11A4C4526B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3892[1] = 
{
	ToggleVariableSetting_tE79CD9769496E78E42260EB965B78F11A4C4526B::get_offset_of_m_toggleInput_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3893 = { sizeof (U3CU3Ec__DisplayClass1_0_tB268DFAD785FE57474CE5D69DF3E29EC536796E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3893[2] = 
{
	U3CU3Ec__DisplayClass1_0_tB268DFAD785FE57474CE5D69DF3E29EC536796E4::get_offset_of_varStateListener_0(),
	U3CU3Ec__DisplayClass1_0_tB268DFAD785FE57474CE5D69DF3E29EC536796E4::get_offset_of_valueChangedCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3894 = { sizeof (Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3894[6] = 
{
	Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F::get_offset_of_m_fromXValue_10(),
	Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F::get_offset_of_m_fromYValue_11(),
	Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F::get_offset_of_m_fromZValue_12(),
	Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F::get_offset_of_m_toValueObject_13(),
	Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F::get_offset_of_m_thenValueObject_14(),
	Vector3VariableSetting_t6ECFD0E1334B82EB9DABD491BC601427AA7D403F::get_offset_of_m_fromVec_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3895 = { sizeof (U3CU3Ec__DisplayClass6_0_t723BBB5CDAFC9BEBBEDDD19569579B41C065F15E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3895[3] = 
{
	U3CU3Ec__DisplayClass6_0_t723BBB5CDAFC9BEBBEDDD19569579B41C065F15E::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass6_0_t723BBB5CDAFC9BEBBEDDD19569579B41C065F15E::get_offset_of_varStateListeners_1(),
	U3CU3Ec__DisplayClass6_0_t723BBB5CDAFC9BEBBEDDD19569579B41C065F15E::get_offset_of_valueChangedCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3896 = { sizeof (EasingEquation_tCA0CBD0561A15202778424F90084F965713F1C82)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3896[42] = 
{
	EasingEquation_tCA0CBD0561A15202778424F90084F965713F1C82::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3897 = { sizeof (TextfxTextAnchor_t2BA9F20AF53C6DC0F23CEFDE9802E9F886BF359C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3897[10] = 
{
	TextfxTextAnchor_t2BA9F20AF53C6DC0F23CEFDE9802E9F886BF359C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3898 = { sizeof (EasingManager_tBB656F01E2A171C5EFEA690A9E294B5612936245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3899 = { sizeof (TextFxQuickSetupAnimConfigs_tF0E7F63EB9D33575003B7216752A08CCCABF2286), -1, sizeof(TextFxQuickSetupAnimConfigs_tF0E7F63EB9D33575003B7216752A08CCCABF2286_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3899[6] = 
{
	0,
	0,
	0,
	TextFxQuickSetupAnimConfigs_tF0E7F63EB9D33575003B7216752A08CCCABF2286_StaticFields::get_offset_of_m_introAnimNames_7(),
	TextFxQuickSetupAnimConfigs_tF0E7F63EB9D33575003B7216752A08CCCABF2286_StaticFields::get_offset_of_m_mainAnimNames_8(),
	TextFxQuickSetupAnimConfigs_tF0E7F63EB9D33575003B7216752A08CCCABF2286_StaticFields::get_offset_of_m_outroAnimNames_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
