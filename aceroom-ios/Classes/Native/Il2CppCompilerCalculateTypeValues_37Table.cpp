﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<UnityEngine.Camera>
struct Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C;
// System.Action`1<UnityEngine.Camera[]>
struct Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_t9CB40DA291556AD6A0449B52E9BB70FCA14CC1B3;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
struct Dictionary_2_t687EE995160D28585A45E2513C0F20E2CBE1626E;
// System.Collections.Generic.Dictionary`2<System.String,VertexAnimationTools_30.TasksStack/Task>
struct Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5;
// System.Collections.Generic.Dictionary`2<VertexAnimationTools_30.ObjData/NormalVertex,System.Int32>
struct Dictionary_2_t461EF7EEAE89A7F22A59DFCE39177B5156601359;
// System.Collections.Generic.Dictionary`2<VertexAnimationTools_30.ObjData/PositionVertex,VertexAnimationTools_30.ObjData/MapVertex>
struct Dictionary_2_tDBF08A9925144C332F48167D92287492E9FE58DC;
// System.Collections.Generic.Dictionary`2<VertexAnimationTools_30.ObjData/UMeshVertex,System.Int32>
struct Dictionary_2_t87B9F5539EE7653D4FAF5479CE8AD2F5E7CDFC2F;
// System.Collections.Generic.HashSet`1<VertexAnimationTools_30.BindingHelper/Edge>
struct HashSet_1_tFF95153E7989BBFED33E303A897B33F7D44340ED;
// System.Collections.Generic.HashSet`1<VertexAnimationTools_30.ObjData/AdjacentPolygon>
struct HashSet_1_t9CE364960B7559E086A7BC4684E47F2C76D38A61;
// System.Collections.Generic.HashSet`1<VertexAnimationTools_30.ObjData/Polygon>
struct HashSet_1_t082160374370AC2CA36B72A29CA36987E5A5024E;
// System.Collections.Generic.IEnumerator`1<VertexAnimationTools_30.TaskInfo>
struct IEnumerator_1_t20E089B7F3B71D22AB70588EA7690D0F31DB6055;
// System.Collections.Generic.List`1<System.Char>[]
struct List_1U5BU5D_tCFE86F23E94B084B1FD06EE1430019EAFE543FAD;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector3[]>
struct List_1_t7056E0D562E44CC218CC39274D1901F278A5E258;
// System.Collections.Generic.List`1<VertexAnimationTools_30.MaterialInfo>
struct List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA;
// System.Collections.Generic.List`1<VertexAnimationTools_30.MeshSequence/Frame>
struct List_1_t111085AD46AD6E1D7CB4FE6A22570DAB13DF787F;
// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData/Face>
struct List_1_t66E79D23D7ADC2EF0FD31EFD917D27BCEDB30B9A;
// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData/FaceLineParser/Corner>
struct List_1_t444C198AF488B4539B1EA5138ECDC668C73F89FB;
// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData/MapVertex>
struct List_1_tAD3493ED46F23195DB91638420814C881984D33F;
// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData/NormalVertex>
struct List_1_t2DA726067932CE665A45F5079B4C94BC61B0201D;
// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData/Polygon>
struct List_1_tFE744926BDF6D484BE450071B854C1455039953A;
// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData/PolygonTriangulator/Corner>
struct List_1_t8C567D98CE017240983A410E06A1A269099750E6;
// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData/PositionVertex>
struct List_1_t61ADFC8A10A8EA2BDA49284119FB4E47D9F6694E;
// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData/SubMesh>
struct List_1_t8EAE4C1C152B75E838C6B540F88D1D7FFB81401F;
// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData/UMeshVertex>
struct List_1_t242FB347922E119315CCCCF165827A41F1EF41DA;
// System.Collections.Generic.List`1<VertexAnimationTools_30.PointCache/PreImportConstraint>
struct List_1_tCCAD2DF763777A3380885B29580636BEE8FC7280;
// System.Collections.Generic.List`1<VertexAnimationTools_30.TasksStack/Task>
struct List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23;
// System.Diagnostics.Stopwatch
struct Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4;
// System.IO.BinaryReader
struct BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969;
// System.IO.FileInfo
struct FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C;
// System.IO.FileStream
struct FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418;
// System.IO.TextReader
struct TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshCollider
struct MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Renderer[]
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39;
// VertexAnimationTools_30.BindingHelper
struct BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8;
// VertexAnimationTools_30.BindingHelper/Edge[]
struct EdgeU5BU5D_t8B1C87C4045CACD9AD3C18548F8E38FCFE8DCC3A;
// VertexAnimationTools_30.ConstraintClip[]
struct ConstraintClipU5BU5D_tBD1D16543191F7472AE27DEB020BC21DE5E3A615;
// VertexAnimationTools_30.FramesArray
struct FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8;
// VertexAnimationTools_30.MeshSequence
struct MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA;
// VertexAnimationTools_30.MeshSequence/Frame
struct Frame_t569CB10118B510EF308215FA5E2BC6354316C27B;
// VertexAnimationTools_30.MeshSequenceInfo/ObjFileInfo[]
struct ObjFileInfoU5BU5D_tC51AD987D3261B2DCBB15872D8087CE87EBDBB75;
// VertexAnimationTools_30.MeshSequencePlayer
struct MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838;
// VertexAnimationTools_30.ObjData
struct ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62;
// VertexAnimationTools_30.ObjData/AdjacentPolygon[]
struct AdjacentPolygonU5BU5D_tE9B43C2AADAE61A55EABDEFD42DC7A5D8EB08D10;
// VertexAnimationTools_30.ObjData/FaceLineParser
struct FaceLineParser_t3456F4CAF39256EA2ED2F481EA54AAD232B26FC7;
// VertexAnimationTools_30.ObjData/MapVertex
struct MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8;
// VertexAnimationTools_30.ObjData/MapVerticesList
struct MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491;
// VertexAnimationTools_30.ObjData/NormalVertex
struct NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D;
// VertexAnimationTools_30.ObjData/NormalsList
struct NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001;
// VertexAnimationTools_30.ObjData/Polygon
struct Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2;
// VertexAnimationTools_30.ObjData/PolygonTriangulator/Corner
struct Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821;
// VertexAnimationTools_30.ObjData/Polygon[]
struct PolygonU5BU5D_t709AA0C218D02A8B11F862288748E0804824326C;
// VertexAnimationTools_30.ObjData/PositionVertex
struct PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186;
// VertexAnimationTools_30.ObjData/SubMesh
struct SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330;
// VertexAnimationTools_30.ObjData/SubMeshesList
struct SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA;
// VertexAnimationTools_30.ObjData/UMeshVertex
struct UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC;
// VertexAnimationTools_30.ObjData/UMeshVerticesList
struct UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997;
// VertexAnimationTools_30.PFU[]
struct PFUU5BU5D_tB9A4B2D6347C993AD9BCA0E312437179766474E8;
// VertexAnimationTools_30.PointCache
struct PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7;
// VertexAnimationTools_30.PointCache/Clip
struct Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553;
// VertexAnimationTools_30.PointCache/Clip[]
struct ClipU5BU5D_t9B7F45E85DBF342DB234CEFF7945F4E5F1F4D3CC;
// VertexAnimationTools_30.PointCache/PolygonMesh
struct PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C;
// VertexAnimationTools_30.PointCache/PolygonMesh[]
struct PolygonMeshU5BU5D_t8CB08AC2E64BEDFAF6ABB153CC60B9E09DDF7C52;
// VertexAnimationTools_30.PointCache/PostImportConstraint[]
struct PostImportConstraintU5BU5D_t8DAC6BA1EB4063452FAB12533B20372EB8D5C130;
// VertexAnimationTools_30.PointCacheData
struct PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5;
// VertexAnimationTools_30.PointCachePlayer
struct PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F;
// VertexAnimationTools_30.PointCachePlayer/Clip[]
struct ClipU5BU5D_tAA79AB3C6F96CA78E7D4DE8DBB9F3D785C413603;
// VertexAnimationTools_30.PointCachePlayer/Constraint[]
struct ConstraintU5BU5D_tA24CD0C95C240251A402CA5801AEEFBA2A56756A;
// VertexAnimationTools_30.ProjectionSamples/Samples[]
struct SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44;
// VertexAnimationTools_30.TasksStack
struct TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F;

struct ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846_marshaled_com;
struct ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846_marshaled_pinvoke;



#ifndef U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#define U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CROSSPLATFORMINPUTMANAGER_T538BC5CB3009F635C0D8B708E339CE82E4C6152E_H
#define CROSSPLATFORMINPUTMANAGER_T538BC5CB3009F635C0D8B708E339CE82E4C6152E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E  : public RuntimeObject
{
public:

public:
};

struct CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields
{
public:
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * ___s_HardwareInput_2;

public:
	inline static int32_t get_offset_of_activeInput_0() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields, ___activeInput_0)); }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * get_activeInput_0() const { return ___activeInput_0; }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 ** get_address_of_activeInput_0() { return &___activeInput_0; }
	inline void set_activeInput_0(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * value)
	{
		___activeInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeInput_0), value);
	}

	inline static int32_t get_offset_of_s_TouchInput_1() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields, ___s_TouchInput_1)); }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * get_s_TouchInput_1() const { return ___s_TouchInput_1; }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 ** get_address_of_s_TouchInput_1() { return &___s_TouchInput_1; }
	inline void set_s_TouchInput_1(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * value)
	{
		___s_TouchInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchInput_1), value);
	}

	inline static int32_t get_offset_of_s_HardwareInput_2() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields, ___s_HardwareInput_2)); }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * get_s_HardwareInput_2() const { return ___s_HardwareInput_2; }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 ** get_address_of_s_HardwareInput_2() { return &___s_HardwareInput_2; }
	inline void set_s_HardwareInput_2(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * value)
	{
		___s_HardwareInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_HardwareInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMINPUTMANAGER_T538BC5CB3009F635C0D8B708E339CE82E4C6152E_H
#ifndef VIRTUALAXIS_TE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B_H
#define VIRTUALAXIS_TE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis
struct  VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::m_Value
	float ___m_Value_1;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B, ___U3CmatchWithInputManagerU3Ek__BackingField_2)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_2() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return &___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_2(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALAXIS_TE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B_H
#ifndef VIRTUALBUTTON_TD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA_H
#define VIRTUALBUTTON_TD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton
struct  VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_1;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::m_LastPressedFrame
	int32_t ___m_LastPressedFrame_2;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::m_ReleasedFrame
	int32_t ___m_ReleasedFrame_3;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton::m_Pressed
	bool ___m_Pressed_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA, ___U3CmatchWithInputManagerU3Ek__BackingField_1)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_1() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return &___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_1(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA, ___m_LastPressedFrame_2)); }
	inline int32_t get_m_LastPressedFrame_2() const { return ___m_LastPressedFrame_2; }
	inline int32_t* get_address_of_m_LastPressedFrame_2() { return &___m_LastPressedFrame_2; }
	inline void set_m_LastPressedFrame_2(int32_t value)
	{
		___m_LastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_ReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA, ___m_ReleasedFrame_3)); }
	inline int32_t get_m_ReleasedFrame_3() const { return ___m_ReleasedFrame_3; }
	inline int32_t* get_address_of_m_ReleasedFrame_3() { return &___m_ReleasedFrame_3; }
	inline void set_m_ReleasedFrame_3(int32_t value)
	{
		___m_ReleasedFrame_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_TD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA_H
#ifndef CONSTRAINTCLIP_TD597EA3660DFD882F13B25065680AABEBDCF91A9_H
#define CONSTRAINTCLIP_TD597EA3660DFD882F13B25065680AABEBDCF91A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ConstraintClip
struct  ConstraintClip_tD597EA3660DFD882F13B25065680AABEBDCF91A9  : public RuntimeObject
{
public:
	// VertexAnimationTools_30.PFU[] VertexAnimationTools_30.ConstraintClip::Frames
	PFUU5BU5D_tB9A4B2D6347C993AD9BCA0E312437179766474E8* ___Frames_0;

public:
	inline static int32_t get_offset_of_Frames_0() { return static_cast<int32_t>(offsetof(ConstraintClip_tD597EA3660DFD882F13B25065680AABEBDCF91A9, ___Frames_0)); }
	inline PFUU5BU5D_tB9A4B2D6347C993AD9BCA0E312437179766474E8* get_Frames_0() const { return ___Frames_0; }
	inline PFUU5BU5D_tB9A4B2D6347C993AD9BCA0E312437179766474E8** get_address_of_Frames_0() { return &___Frames_0; }
	inline void set_Frames_0(PFUU5BU5D_tB9A4B2D6347C993AD9BCA0E312437179766474E8* value)
	{
		___Frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___Frames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTCLIP_TD597EA3660DFD882F13B25065680AABEBDCF91A9_H
#ifndef EXTENSION_TF6DBF61AFC70AA0040187AEAEDF04F47352C6795_H
#define EXTENSION_TF6DBF61AFC70AA0040187AEAEDF04F47352C6795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.Extension
struct  Extension_tF6DBF61AFC70AA0040187AEAEDF04F47352C6795  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSION_TF6DBF61AFC70AA0040187AEAEDF04F47352C6795_H
#ifndef MATERIALINFO_TEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0_H
#define MATERIALINFO_TEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MaterialInfo
struct  MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0  : public RuntimeObject
{
public:
	// UnityEngine.Material VertexAnimationTools_30.MaterialInfo::Mat
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___Mat_0;
	// System.Boolean VertexAnimationTools_30.MaterialInfo::Used
	bool ___Used_1;
	// System.String VertexAnimationTools_30.MaterialInfo::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_Mat_0() { return static_cast<int32_t>(offsetof(MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0, ___Mat_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_Mat_0() const { return ___Mat_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_Mat_0() { return &___Mat_0; }
	inline void set_Mat_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___Mat_0 = value;
		Il2CppCodeGenWriteBarrier((&___Mat_0), value);
	}

	inline static int32_t get_offset_of_Used_1() { return static_cast<int32_t>(offsetof(MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0, ___Used_1)); }
	inline bool get_Used_1() const { return ___Used_1; }
	inline bool* get_address_of_Used_1() { return &___Used_1; }
	inline void set_Used_1(bool value)
	{
		___Used_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALINFO_TEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0_H
#ifndef FRAME_T569CB10118B510EF308215FA5E2BC6354316C27B_H
#define FRAME_T569CB10118B510EF308215FA5E2BC6354316C27B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MeshSequence_Frame
struct  Frame_t569CB10118B510EF308215FA5E2BC6354316C27B  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.MeshSequence_Frame::Name
	String_t* ___Name_0;
	// System.Int32 VertexAnimationTools_30.MeshSequence_Frame::ObjPolygonsCount
	int32_t ___ObjPolygonsCount_1;
	// System.Int32 VertexAnimationTools_30.MeshSequence_Frame::ObjVerticesCount
	int32_t ___ObjVerticesCount_2;
	// System.Int32 VertexAnimationTools_30.MeshSequence_Frame::MeshVertsCount
	int32_t ___MeshVertsCount_3;
	// System.Int32 VertexAnimationTools_30.MeshSequence_Frame::MeshTrisCount
	int32_t ___MeshTrisCount_4;
	// UnityEngine.Mesh VertexAnimationTools_30.MeshSequence_Frame::FrameMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___FrameMesh_5;
	// UnityEngine.Material[] VertexAnimationTools_30.MeshSequence_Frame::Materials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___Materials_6;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Frame_t569CB10118B510EF308215FA5E2BC6354316C27B, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ObjPolygonsCount_1() { return static_cast<int32_t>(offsetof(Frame_t569CB10118B510EF308215FA5E2BC6354316C27B, ___ObjPolygonsCount_1)); }
	inline int32_t get_ObjPolygonsCount_1() const { return ___ObjPolygonsCount_1; }
	inline int32_t* get_address_of_ObjPolygonsCount_1() { return &___ObjPolygonsCount_1; }
	inline void set_ObjPolygonsCount_1(int32_t value)
	{
		___ObjPolygonsCount_1 = value;
	}

	inline static int32_t get_offset_of_ObjVerticesCount_2() { return static_cast<int32_t>(offsetof(Frame_t569CB10118B510EF308215FA5E2BC6354316C27B, ___ObjVerticesCount_2)); }
	inline int32_t get_ObjVerticesCount_2() const { return ___ObjVerticesCount_2; }
	inline int32_t* get_address_of_ObjVerticesCount_2() { return &___ObjVerticesCount_2; }
	inline void set_ObjVerticesCount_2(int32_t value)
	{
		___ObjVerticesCount_2 = value;
	}

	inline static int32_t get_offset_of_MeshVertsCount_3() { return static_cast<int32_t>(offsetof(Frame_t569CB10118B510EF308215FA5E2BC6354316C27B, ___MeshVertsCount_3)); }
	inline int32_t get_MeshVertsCount_3() const { return ___MeshVertsCount_3; }
	inline int32_t* get_address_of_MeshVertsCount_3() { return &___MeshVertsCount_3; }
	inline void set_MeshVertsCount_3(int32_t value)
	{
		___MeshVertsCount_3 = value;
	}

	inline static int32_t get_offset_of_MeshTrisCount_4() { return static_cast<int32_t>(offsetof(Frame_t569CB10118B510EF308215FA5E2BC6354316C27B, ___MeshTrisCount_4)); }
	inline int32_t get_MeshTrisCount_4() const { return ___MeshTrisCount_4; }
	inline int32_t* get_address_of_MeshTrisCount_4() { return &___MeshTrisCount_4; }
	inline void set_MeshTrisCount_4(int32_t value)
	{
		___MeshTrisCount_4 = value;
	}

	inline static int32_t get_offset_of_FrameMesh_5() { return static_cast<int32_t>(offsetof(Frame_t569CB10118B510EF308215FA5E2BC6354316C27B, ___FrameMesh_5)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_FrameMesh_5() const { return ___FrameMesh_5; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_FrameMesh_5() { return &___FrameMesh_5; }
	inline void set_FrameMesh_5(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___FrameMesh_5 = value;
		Il2CppCodeGenWriteBarrier((&___FrameMesh_5), value);
	}

	inline static int32_t get_offset_of_Materials_6() { return static_cast<int32_t>(offsetof(Frame_t569CB10118B510EF308215FA5E2BC6354316C27B, ___Materials_6)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_Materials_6() const { return ___Materials_6; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_Materials_6() { return &___Materials_6; }
	inline void set_Materials_6(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___Materials_6 = value;
		Il2CppCodeGenWriteBarrier((&___Materials_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAME_T569CB10118B510EF308215FA5E2BC6354316C27B_H
#ifndef NORMALIZEDSPLINE_T69AF6108C4A53B141ADA0C9952918B21F43779E7_H
#define NORMALIZEDSPLINE_T69AF6108C4A53B141ADA0C9952918B21F43779E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.NormalizedSpline
struct  NormalizedSpline_t69AF6108C4A53B141ADA0C9952918B21F43779E7  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] VertexAnimationTools_30.NormalizedSpline::Vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Vertices_0;
	// System.Single[] VertexAnimationTools_30.NormalizedSpline::Distances
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___Distances_1;
	// System.Single VertexAnimationTools_30.NormalizedSpline::Length
	float ___Length_2;

public:
	inline static int32_t get_offset_of_Vertices_0() { return static_cast<int32_t>(offsetof(NormalizedSpline_t69AF6108C4A53B141ADA0C9952918B21F43779E7, ___Vertices_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Vertices_0() const { return ___Vertices_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Vertices_0() { return &___Vertices_0; }
	inline void set_Vertices_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Vertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_0), value);
	}

	inline static int32_t get_offset_of_Distances_1() { return static_cast<int32_t>(offsetof(NormalizedSpline_t69AF6108C4A53B141ADA0C9952918B21F43779E7, ___Distances_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_Distances_1() const { return ___Distances_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_Distances_1() { return &___Distances_1; }
	inline void set_Distances_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___Distances_1 = value;
		Il2CppCodeGenWriteBarrier((&___Distances_1), value);
	}

	inline static int32_t get_offset_of_Length_2() { return static_cast<int32_t>(offsetof(NormalizedSpline_t69AF6108C4A53B141ADA0C9952918B21F43779E7, ___Length_2)); }
	inline float get_Length_2() const { return ___Length_2; }
	inline float* get_address_of_Length_2() { return &___Length_2; }
	inline void set_Length_2(float value)
	{
		___Length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALIZEDSPLINE_T69AF6108C4A53B141ADA0C9952918B21F43779E7_H
#ifndef ADJACENTPOLYGON_TDFA687AB2A5D03856F53EBFBE3CA16D5B1D7CA17_H
#define ADJACENTPOLYGON_TDFA687AB2A5D03856F53EBFBE3CA16D5B1D7CA17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_AdjacentPolygon
struct  AdjacentPolygon_tDFA687AB2A5D03856F53EBFBE3CA16D5B1D7CA17  : public RuntimeObject
{
public:
	// VertexAnimationTools_30.ObjData_Polygon VertexAnimationTools_30.ObjData_AdjacentPolygon::polygon
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2 * ___polygon_0;
	// System.Single VertexAnimationTools_30.ObjData_AdjacentPolygon::AreaMult
	float ___AreaMult_1;

public:
	inline static int32_t get_offset_of_polygon_0() { return static_cast<int32_t>(offsetof(AdjacentPolygon_tDFA687AB2A5D03856F53EBFBE3CA16D5B1D7CA17, ___polygon_0)); }
	inline Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2 * get_polygon_0() const { return ___polygon_0; }
	inline Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2 ** get_address_of_polygon_0() { return &___polygon_0; }
	inline void set_polygon_0(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2 * value)
	{
		___polygon_0 = value;
		Il2CppCodeGenWriteBarrier((&___polygon_0), value);
	}

	inline static int32_t get_offset_of_AreaMult_1() { return static_cast<int32_t>(offsetof(AdjacentPolygon_tDFA687AB2A5D03856F53EBFBE3CA16D5B1D7CA17, ___AreaMult_1)); }
	inline float get_AreaMult_1() const { return ___AreaMult_1; }
	inline float* get_address_of_AreaMult_1() { return &___AreaMult_1; }
	inline void set_AreaMult_1(float value)
	{
		___AreaMult_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADJACENTPOLYGON_TDFA687AB2A5D03856F53EBFBE3CA16D5B1D7CA17_H
#ifndef FACELINEPARSER_T3456F4CAF39256EA2ED2F481EA54AAD232B26FC7_H
#define FACELINEPARSER_T3456F4CAF39256EA2ED2F481EA54AAD232B26FC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_FaceLineParser
struct  FaceLineParser_t3456F4CAF39256EA2ED2F481EA54AAD232B26FC7  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_FaceLineParser_Corner> VertexAnimationTools_30.ObjData_FaceLineParser::corners
	List_1_t444C198AF488B4539B1EA5138ECDC668C73F89FB * ___corners_0;

public:
	inline static int32_t get_offset_of_corners_0() { return static_cast<int32_t>(offsetof(FaceLineParser_t3456F4CAF39256EA2ED2F481EA54AAD232B26FC7, ___corners_0)); }
	inline List_1_t444C198AF488B4539B1EA5138ECDC668C73F89FB * get_corners_0() const { return ___corners_0; }
	inline List_1_t444C198AF488B4539B1EA5138ECDC668C73F89FB ** get_address_of_corners_0() { return &___corners_0; }
	inline void set_corners_0(List_1_t444C198AF488B4539B1EA5138ECDC668C73F89FB * value)
	{
		___corners_0 = value;
		Il2CppCodeGenWriteBarrier((&___corners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACELINEPARSER_T3456F4CAF39256EA2ED2F481EA54AAD232B26FC7_H
#ifndef CORNER_T8E8024D987474216BA43F7AAB17DCEEBD453CBF8_H
#define CORNER_T8E8024D987474216BA43F7AAB17DCEEBD453CBF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_FaceLineParser_Corner
struct  Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.ObjData_FaceLineParser_Corner::VertIdx
	int32_t ___VertIdx_0;
	// System.Int32 VertexAnimationTools_30.ObjData_FaceLineParser_Corner::UvIdx
	int32_t ___UvIdx_1;
	// System.Int32 VertexAnimationTools_30.ObjData_FaceLineParser_Corner::NormalIdx
	int32_t ___NormalIdx_2;
	// System.Collections.Generic.List`1<System.Char>[] VertexAnimationTools_30.ObjData_FaceLineParser_Corner::chars
	List_1U5BU5D_tCFE86F23E94B084B1FD06EE1430019EAFE543FAD* ___chars_3;
	// System.Int32 VertexAnimationTools_30.ObjData_FaceLineParser_Corner::ci
	int32_t ___ci_4;

public:
	inline static int32_t get_offset_of_VertIdx_0() { return static_cast<int32_t>(offsetof(Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8, ___VertIdx_0)); }
	inline int32_t get_VertIdx_0() const { return ___VertIdx_0; }
	inline int32_t* get_address_of_VertIdx_0() { return &___VertIdx_0; }
	inline void set_VertIdx_0(int32_t value)
	{
		___VertIdx_0 = value;
	}

	inline static int32_t get_offset_of_UvIdx_1() { return static_cast<int32_t>(offsetof(Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8, ___UvIdx_1)); }
	inline int32_t get_UvIdx_1() const { return ___UvIdx_1; }
	inline int32_t* get_address_of_UvIdx_1() { return &___UvIdx_1; }
	inline void set_UvIdx_1(int32_t value)
	{
		___UvIdx_1 = value;
	}

	inline static int32_t get_offset_of_NormalIdx_2() { return static_cast<int32_t>(offsetof(Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8, ___NormalIdx_2)); }
	inline int32_t get_NormalIdx_2() const { return ___NormalIdx_2; }
	inline int32_t* get_address_of_NormalIdx_2() { return &___NormalIdx_2; }
	inline void set_NormalIdx_2(int32_t value)
	{
		___NormalIdx_2 = value;
	}

	inline static int32_t get_offset_of_chars_3() { return static_cast<int32_t>(offsetof(Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8, ___chars_3)); }
	inline List_1U5BU5D_tCFE86F23E94B084B1FD06EE1430019EAFE543FAD* get_chars_3() const { return ___chars_3; }
	inline List_1U5BU5D_tCFE86F23E94B084B1FD06EE1430019EAFE543FAD** get_address_of_chars_3() { return &___chars_3; }
	inline void set_chars_3(List_1U5BU5D_tCFE86F23E94B084B1FD06EE1430019EAFE543FAD* value)
	{
		___chars_3 = value;
		Il2CppCodeGenWriteBarrier((&___chars_3), value);
	}

	inline static int32_t get_offset_of_ci_4() { return static_cast<int32_t>(offsetof(Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8, ___ci_4)); }
	inline int32_t get_ci_4() const { return ___ci_4; }
	inline int32_t* get_address_of_ci_4() { return &___ci_4; }
	inline void set_ci_4(int32_t value)
	{
		___ci_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_T8E8024D987474216BA43F7AAB17DCEEBD453CBF8_H
#ifndef MAPVERTICESLIST_T5FF455607E356DD44247943103835EBF2183F491_H
#define MAPVERTICESLIST_T5FF455607E356DD44247943103835EBF2183F491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_MapVerticesList
struct  MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_MapVertex> VertexAnimationTools_30.ObjData_MapVerticesList::objmvs
	List_1_tAD3493ED46F23195DB91638420814C881984D33F * ___objmvs_0;
	// System.Collections.Generic.Dictionary`2<VertexAnimationTools_30.ObjData_PositionVertex,VertexAnimationTools_30.ObjData_MapVertex> VertexAnimationTools_30.ObjData_MapVerticesList::forgedMvs
	Dictionary_2_tDBF08A9925144C332F48167D92287492E9FE58DC * ___forgedMvs_1;

public:
	inline static int32_t get_offset_of_objmvs_0() { return static_cast<int32_t>(offsetof(MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491, ___objmvs_0)); }
	inline List_1_tAD3493ED46F23195DB91638420814C881984D33F * get_objmvs_0() const { return ___objmvs_0; }
	inline List_1_tAD3493ED46F23195DB91638420814C881984D33F ** get_address_of_objmvs_0() { return &___objmvs_0; }
	inline void set_objmvs_0(List_1_tAD3493ED46F23195DB91638420814C881984D33F * value)
	{
		___objmvs_0 = value;
		Il2CppCodeGenWriteBarrier((&___objmvs_0), value);
	}

	inline static int32_t get_offset_of_forgedMvs_1() { return static_cast<int32_t>(offsetof(MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491, ___forgedMvs_1)); }
	inline Dictionary_2_tDBF08A9925144C332F48167D92287492E9FE58DC * get_forgedMvs_1() const { return ___forgedMvs_1; }
	inline Dictionary_2_tDBF08A9925144C332F48167D92287492E9FE58DC ** get_address_of_forgedMvs_1() { return &___forgedMvs_1; }
	inline void set_forgedMvs_1(Dictionary_2_tDBF08A9925144C332F48167D92287492E9FE58DC * value)
	{
		___forgedMvs_1 = value;
		Il2CppCodeGenWriteBarrier((&___forgedMvs_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPVERTICESLIST_T5FF455607E356DD44247943103835EBF2183F491_H
#ifndef NORMALSLIST_TF0247CD879F6DA7D69B829C5D681042015C2E001_H
#define NORMALSLIST_TF0247CD879F6DA7D69B829C5D681042015C2E001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_NormalsList
struct  NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_NormalVertex> VertexAnimationTools_30.ObjData_NormalsList::items
	List_1_t2DA726067932CE665A45F5079B4C94BC61B0201D * ___items_0;
	// System.Collections.Generic.Dictionary`2<VertexAnimationTools_30.ObjData_NormalVertex,System.Int32> VertexAnimationTools_30.ObjData_NormalsList::itemsDict
	Dictionary_2_t461EF7EEAE89A7F22A59DFCE39177B5156601359 * ___itemsDict_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001, ___items_0)); }
	inline List_1_t2DA726067932CE665A45F5079B4C94BC61B0201D * get_items_0() const { return ___items_0; }
	inline List_1_t2DA726067932CE665A45F5079B4C94BC61B0201D ** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(List_1_t2DA726067932CE665A45F5079B4C94BC61B0201D * value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of_itemsDict_1() { return static_cast<int32_t>(offsetof(NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001, ___itemsDict_1)); }
	inline Dictionary_2_t461EF7EEAE89A7F22A59DFCE39177B5156601359 * get_itemsDict_1() const { return ___itemsDict_1; }
	inline Dictionary_2_t461EF7EEAE89A7F22A59DFCE39177B5156601359 ** get_address_of_itemsDict_1() { return &___itemsDict_1; }
	inline void set_itemsDict_1(Dictionary_2_t461EF7EEAE89A7F22A59DFCE39177B5156601359 * value)
	{
		___itemsDict_1 = value;
		Il2CppCodeGenWriteBarrier((&___itemsDict_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALSLIST_TF0247CD879F6DA7D69B829C5D681042015C2E001_H
#ifndef POLYGONTRIANGULATOR_T3CADF3489835B31F8D09A77FAA00C2DAA5091CFF_H
#define POLYGONTRIANGULATOR_T3CADF3489835B31F8D09A77FAA00C2DAA5091CFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_PolygonTriangulator
struct  PolygonTriangulator_t3CADF3489835B31F8D09A77FAA00C2DAA5091CFF  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_PolygonTriangulator_Corner> VertexAnimationTools_30.ObjData_PolygonTriangulator::corners
	List_1_t8C567D98CE017240983A410E06A1A269099750E6 * ___corners_0;
	// System.Collections.Generic.List`1<System.Int32> VertexAnimationTools_30.ObjData_PolygonTriangulator::trianglesIndeces
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___trianglesIndeces_1;
	// VertexAnimationTools_30.ObjData_PolygonTriangulator_Corner VertexAnimationTools_30.ObjData_PolygonTriangulator::minAngleCorner
	Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821 * ___minAngleCorner_2;

public:
	inline static int32_t get_offset_of_corners_0() { return static_cast<int32_t>(offsetof(PolygonTriangulator_t3CADF3489835B31F8D09A77FAA00C2DAA5091CFF, ___corners_0)); }
	inline List_1_t8C567D98CE017240983A410E06A1A269099750E6 * get_corners_0() const { return ___corners_0; }
	inline List_1_t8C567D98CE017240983A410E06A1A269099750E6 ** get_address_of_corners_0() { return &___corners_0; }
	inline void set_corners_0(List_1_t8C567D98CE017240983A410E06A1A269099750E6 * value)
	{
		___corners_0 = value;
		Il2CppCodeGenWriteBarrier((&___corners_0), value);
	}

	inline static int32_t get_offset_of_trianglesIndeces_1() { return static_cast<int32_t>(offsetof(PolygonTriangulator_t3CADF3489835B31F8D09A77FAA00C2DAA5091CFF, ___trianglesIndeces_1)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_trianglesIndeces_1() const { return ___trianglesIndeces_1; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_trianglesIndeces_1() { return &___trianglesIndeces_1; }
	inline void set_trianglesIndeces_1(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___trianglesIndeces_1 = value;
		Il2CppCodeGenWriteBarrier((&___trianglesIndeces_1), value);
	}

	inline static int32_t get_offset_of_minAngleCorner_2() { return static_cast<int32_t>(offsetof(PolygonTriangulator_t3CADF3489835B31F8D09A77FAA00C2DAA5091CFF, ___minAngleCorner_2)); }
	inline Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821 * get_minAngleCorner_2() const { return ___minAngleCorner_2; }
	inline Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821 ** get_address_of_minAngleCorner_2() { return &___minAngleCorner_2; }
	inline void set_minAngleCorner_2(Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821 * value)
	{
		___minAngleCorner_2 = value;
		Il2CppCodeGenWriteBarrier((&___minAngleCorner_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONTRIANGULATOR_T3CADF3489835B31F8D09A77FAA00C2DAA5091CFF_H
#ifndef SUBMESH_TADC64FBEFFF2B2DC28F7D212D60807B04FE6F330_H
#define SUBMESH_TADC64FBEFFF2B2DC28F7D212D60807B04FE6F330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_SubMesh
struct  SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.ObjData_SubMesh::MaterialName
	String_t* ___MaterialName_0;
	// System.Collections.Generic.List`1<System.Int32> VertexAnimationTools_30.ObjData_SubMesh::TrisIndeces
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___TrisIndeces_1;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_Polygon> VertexAnimationTools_30.ObjData_SubMesh::Polygons
	List_1_tFE744926BDF6D484BE450071B854C1455039953A * ___Polygons_2;

public:
	inline static int32_t get_offset_of_MaterialName_0() { return static_cast<int32_t>(offsetof(SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330, ___MaterialName_0)); }
	inline String_t* get_MaterialName_0() const { return ___MaterialName_0; }
	inline String_t** get_address_of_MaterialName_0() { return &___MaterialName_0; }
	inline void set_MaterialName_0(String_t* value)
	{
		___MaterialName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MaterialName_0), value);
	}

	inline static int32_t get_offset_of_TrisIndeces_1() { return static_cast<int32_t>(offsetof(SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330, ___TrisIndeces_1)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_TrisIndeces_1() const { return ___TrisIndeces_1; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_TrisIndeces_1() { return &___TrisIndeces_1; }
	inline void set_TrisIndeces_1(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___TrisIndeces_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrisIndeces_1), value);
	}

	inline static int32_t get_offset_of_Polygons_2() { return static_cast<int32_t>(offsetof(SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330, ___Polygons_2)); }
	inline List_1_tFE744926BDF6D484BE450071B854C1455039953A * get_Polygons_2() const { return ___Polygons_2; }
	inline List_1_tFE744926BDF6D484BE450071B854C1455039953A ** get_address_of_Polygons_2() { return &___Polygons_2; }
	inline void set_Polygons_2(List_1_tFE744926BDF6D484BE450071B854C1455039953A * value)
	{
		___Polygons_2 = value;
		Il2CppCodeGenWriteBarrier((&___Polygons_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMESH_TADC64FBEFFF2B2DC28F7D212D60807B04FE6F330_H
#ifndef SUBMESHESLIST_T835A463649E47CD647BC4EBA1975E42CA2A919FA_H
#define SUBMESHESLIST_T835A463649E47CD647BC4EBA1975E42CA2A919FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_SubMeshesList
struct  SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_SubMesh> VertexAnimationTools_30.ObjData_SubMeshesList::items
	List_1_t8EAE4C1C152B75E838C6B540F88D1D7FFB81401F * ___items_0;
	// VertexAnimationTools_30.ObjData_SubMesh VertexAnimationTools_30.ObjData_SubMeshesList::Current
	SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330 * ___Current_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA, ___items_0)); }
	inline List_1_t8EAE4C1C152B75E838C6B540F88D1D7FFB81401F * get_items_0() const { return ___items_0; }
	inline List_1_t8EAE4C1C152B75E838C6B540F88D1D7FFB81401F ** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(List_1_t8EAE4C1C152B75E838C6B540F88D1D7FFB81401F * value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of_Current_1() { return static_cast<int32_t>(offsetof(SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA, ___Current_1)); }
	inline SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330 * get_Current_1() const { return ___Current_1; }
	inline SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330 ** get_address_of_Current_1() { return &___Current_1; }
	inline void set_Current_1(SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330 * value)
	{
		___Current_1 = value;
		Il2CppCodeGenWriteBarrier((&___Current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMESHESLIST_T835A463649E47CD647BC4EBA1975E42CA2A919FA_H
#ifndef UMESHVERTICESLIST_TC475FE5EEBD1A9F01789D898112D00E602FFF997_H
#define UMESHVERTICESLIST_TC475FE5EEBD1A9F01789D898112D00E602FFF997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_UMeshVerticesList
struct  UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<VertexAnimationTools_30.ObjData_UMeshVertex,System.Int32> VertexAnimationTools_30.ObjData_UMeshVerticesList::itemsDict
	Dictionary_2_t87B9F5539EE7653D4FAF5479CE8AD2F5E7CDFC2F * ___itemsDict_0;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_UMeshVertex> VertexAnimationTools_30.ObjData_UMeshVerticesList::items
	List_1_t242FB347922E119315CCCCF165827A41F1EF41DA * ___items_1;

public:
	inline static int32_t get_offset_of_itemsDict_0() { return static_cast<int32_t>(offsetof(UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997, ___itemsDict_0)); }
	inline Dictionary_2_t87B9F5539EE7653D4FAF5479CE8AD2F5E7CDFC2F * get_itemsDict_0() const { return ___itemsDict_0; }
	inline Dictionary_2_t87B9F5539EE7653D4FAF5479CE8AD2F5E7CDFC2F ** get_address_of_itemsDict_0() { return &___itemsDict_0; }
	inline void set_itemsDict_0(Dictionary_2_t87B9F5539EE7653D4FAF5479CE8AD2F5E7CDFC2F * value)
	{
		___itemsDict_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemsDict_0), value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997, ___items_1)); }
	inline List_1_t242FB347922E119315CCCCF165827A41F1EF41DA * get_items_1() const { return ___items_1; }
	inline List_1_t242FB347922E119315CCCCF165827A41F1EF41DA ** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(List_1_t242FB347922E119315CCCCF165827A41F1EF41DA * value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((&___items_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UMESHVERTICESLIST_TC475FE5EEBD1A9F01789D898112D00E602FFF997_H
#ifndef POLYGONMESH_TD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C_H
#define POLYGONMESH_TD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCache_PolygonMesh
struct  PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.PointCache_PolygonMesh::Name
	String_t* ___Name_0;
	// System.String VertexAnimationTools_30.PointCache_PolygonMesh::Path
	String_t* ___Path_1;
	// System.String VertexAnimationTools_30.PointCache_PolygonMesh::MeshName
	String_t* ___MeshName_2;
	// System.Int32 VertexAnimationTools_30.PointCache_PolygonMesh::VertsCount
	int32_t ___VertsCount_3;
	// System.Int32 VertexAnimationTools_30.PointCache_PolygonMesh::PolygonsCount
	int32_t ___PolygonsCount_4;
	// System.Int32 VertexAnimationTools_30.PointCache_PolygonMesh::SubMeshesCount
	int32_t ___SubMeshesCount_5;
	// System.String VertexAnimationTools_30.PointCache_PolygonMesh::Info
	String_t* ___Info_6;
	// UnityEngine.Mesh VertexAnimationTools_30.PointCache_PolygonMesh::mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_7;
	// UnityEngine.Material[] VertexAnimationTools_30.PointCache_PolygonMesh::Materials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___Materials_8;
	// VertexAnimationTools_30.ObjData VertexAnimationTools_30.PointCache_PolygonMesh::od
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62 * ___od_9;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Path_1() { return static_cast<int32_t>(offsetof(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C, ___Path_1)); }
	inline String_t* get_Path_1() const { return ___Path_1; }
	inline String_t** get_address_of_Path_1() { return &___Path_1; }
	inline void set_Path_1(String_t* value)
	{
		___Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___Path_1), value);
	}

	inline static int32_t get_offset_of_MeshName_2() { return static_cast<int32_t>(offsetof(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C, ___MeshName_2)); }
	inline String_t* get_MeshName_2() const { return ___MeshName_2; }
	inline String_t** get_address_of_MeshName_2() { return &___MeshName_2; }
	inline void set_MeshName_2(String_t* value)
	{
		___MeshName_2 = value;
		Il2CppCodeGenWriteBarrier((&___MeshName_2), value);
	}

	inline static int32_t get_offset_of_VertsCount_3() { return static_cast<int32_t>(offsetof(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C, ___VertsCount_3)); }
	inline int32_t get_VertsCount_3() const { return ___VertsCount_3; }
	inline int32_t* get_address_of_VertsCount_3() { return &___VertsCount_3; }
	inline void set_VertsCount_3(int32_t value)
	{
		___VertsCount_3 = value;
	}

	inline static int32_t get_offset_of_PolygonsCount_4() { return static_cast<int32_t>(offsetof(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C, ___PolygonsCount_4)); }
	inline int32_t get_PolygonsCount_4() const { return ___PolygonsCount_4; }
	inline int32_t* get_address_of_PolygonsCount_4() { return &___PolygonsCount_4; }
	inline void set_PolygonsCount_4(int32_t value)
	{
		___PolygonsCount_4 = value;
	}

	inline static int32_t get_offset_of_SubMeshesCount_5() { return static_cast<int32_t>(offsetof(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C, ___SubMeshesCount_5)); }
	inline int32_t get_SubMeshesCount_5() const { return ___SubMeshesCount_5; }
	inline int32_t* get_address_of_SubMeshesCount_5() { return &___SubMeshesCount_5; }
	inline void set_SubMeshesCount_5(int32_t value)
	{
		___SubMeshesCount_5 = value;
	}

	inline static int32_t get_offset_of_Info_6() { return static_cast<int32_t>(offsetof(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C, ___Info_6)); }
	inline String_t* get_Info_6() const { return ___Info_6; }
	inline String_t** get_address_of_Info_6() { return &___Info_6; }
	inline void set_Info_6(String_t* value)
	{
		___Info_6 = value;
		Il2CppCodeGenWriteBarrier((&___Info_6), value);
	}

	inline static int32_t get_offset_of_mesh_7() { return static_cast<int32_t>(offsetof(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C, ___mesh_7)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_mesh_7() const { return ___mesh_7; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_mesh_7() { return &___mesh_7; }
	inline void set_mesh_7(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___mesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_7), value);
	}

	inline static int32_t get_offset_of_Materials_8() { return static_cast<int32_t>(offsetof(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C, ___Materials_8)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_Materials_8() const { return ___Materials_8; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_Materials_8() { return &___Materials_8; }
	inline void set_Materials_8(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___Materials_8 = value;
		Il2CppCodeGenWriteBarrier((&___Materials_8), value);
	}

	inline static int32_t get_offset_of_od_9() { return static_cast<int32_t>(offsetof(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C, ___od_9)); }
	inline ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62 * get_od_9() const { return ___od_9; }
	inline ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62 ** get_address_of_od_9() { return &___od_9; }
	inline void set_od_9(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62 * value)
	{
		___od_9 = value;
		Il2CppCodeGenWriteBarrier((&___od_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONMESH_TD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C_H
#ifndef SAMPLES_TCBF54D332EEA974DDC15636D1CFC5B13179D0D08_H
#define SAMPLES_TCBF54D332EEA974DDC15636D1CFC5B13179D0D08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ProjectionSamples_Samples
struct  Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.ProjectionSamples_Samples::Name
	String_t* ___Name_0;
	// UnityEngine.Vector3[] VertexAnimationTools_30.ProjectionSamples_Samples::Dirs
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Dirs_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Dirs_1() { return static_cast<int32_t>(offsetof(Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08, ___Dirs_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Dirs_1() const { return ___Dirs_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Dirs_1() { return &___Dirs_1; }
	inline void set_Dirs_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Dirs_1 = value;
		Il2CppCodeGenWriteBarrier((&___Dirs_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLES_TCBF54D332EEA974DDC15636D1CFC5B13179D0D08_H
#ifndef TASKSSTACK_TA7E298F5409030E4A9DB41C8074552889B7B927F_H
#define TASKSSTACK_TA7E298F5409030E4A9DB41C8074552889B7B927F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.TasksStack
struct  TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.TasksStack::Name
	String_t* ___Name_0;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.TasksStack_Task> VertexAnimationTools_30.TasksStack::items
	List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * ___items_1;
	// System.Collections.Generic.Dictionary`2<System.String,VertexAnimationTools_30.TasksStack_Task> VertexAnimationTools_30.TasksStack::itemsDict
	Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * ___itemsDict_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F, ___items_1)); }
	inline List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * get_items_1() const { return ___items_1; }
	inline List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 ** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((&___items_1), value);
	}

	inline static int32_t get_offset_of_itemsDict_2() { return static_cast<int32_t>(offsetof(TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F, ___itemsDict_2)); }
	inline Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * get_itemsDict_2() const { return ___itemsDict_2; }
	inline Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 ** get_address_of_itemsDict_2() { return &___itemsDict_2; }
	inline void set_itemsDict_2(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * value)
	{
		___itemsDict_2 = value;
		Il2CppCodeGenWriteBarrier((&___itemsDict_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKSSTACK_TA7E298F5409030E4A9DB41C8074552889B7B927F_H
#ifndef TASK_TC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4_H
#define TASK_TC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.TasksStack_Task
struct  Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.TasksStack_Task::Name
	String_t* ___Name_0;
	// System.Single VertexAnimationTools_30.TasksStack_Task::Iterations
	float ___Iterations_1;
	// System.Single VertexAnimationTools_30.TasksStack_Task::NormalizedFrom
	float ___NormalizedFrom_2;
	// System.Single VertexAnimationTools_30.TasksStack_Task::NormalizedTo
	float ___NormalizedTo_3;
	// System.Single VertexAnimationTools_30.TasksStack_Task::Weight
	float ___Weight_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Iterations_1() { return static_cast<int32_t>(offsetof(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4, ___Iterations_1)); }
	inline float get_Iterations_1() const { return ___Iterations_1; }
	inline float* get_address_of_Iterations_1() { return &___Iterations_1; }
	inline void set_Iterations_1(float value)
	{
		___Iterations_1 = value;
	}

	inline static int32_t get_offset_of_NormalizedFrom_2() { return static_cast<int32_t>(offsetof(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4, ___NormalizedFrom_2)); }
	inline float get_NormalizedFrom_2() const { return ___NormalizedFrom_2; }
	inline float* get_address_of_NormalizedFrom_2() { return &___NormalizedFrom_2; }
	inline void set_NormalizedFrom_2(float value)
	{
		___NormalizedFrom_2 = value;
	}

	inline static int32_t get_offset_of_NormalizedTo_3() { return static_cast<int32_t>(offsetof(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4, ___NormalizedTo_3)); }
	inline float get_NormalizedTo_3() const { return ___NormalizedTo_3; }
	inline float* get_address_of_NormalizedTo_3() { return &___NormalizedTo_3; }
	inline void set_NormalizedTo_3(float value)
	{
		___NormalizedTo_3 = value;
	}

	inline static int32_t get_offset_of_Weight_4() { return static_cast<int32_t>(offsetof(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4, ___Weight_4)); }
	inline float get_Weight_4() const { return ___Weight_4; }
	inline float* get_address_of_Weight_4() { return &___Weight_4; }
	inline void set_Weight_4(float value)
	{
		___Weight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASK_TC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4_H
#ifndef UNITYANDROIDPERMISSIONS_TA4CE60356A3424ECF5E1AB1F39264182BB49D7CF_H
#define UNITYANDROIDPERMISSIONS_TA4CE60356A3424ECF5E1AB1F39264182BB49D7CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_UnityAndroidPermissions
struct  UnityAndroidPermissions_tA4CE60356A3424ECF5E1AB1F39264182BB49D7CF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYANDROIDPERMISSIONS_TA4CE60356A3424ECF5E1AB1F39264182BB49D7CF_H
#ifndef UNITYRENDERPIPELINE_T62C711062D224A17F999C0F58CDB6F6E641560A4_H
#define UNITYRENDERPIPELINE_T62C711062D224A17F999C0F58CDB6F6E641560A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_UnityRenderPipeline
struct  UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.Camera[]> Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_UnityRenderPipeline::BeginFrameRendering
	Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A * ___BeginFrameRendering_0;
	// System.Action`1<UnityEngine.Camera> Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_UnityRenderPipeline::BeginCameraRendering
	Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C * ___BeginCameraRendering_1;

public:
	inline static int32_t get_offset_of_BeginFrameRendering_0() { return static_cast<int32_t>(offsetof(UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4, ___BeginFrameRendering_0)); }
	inline Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A * get_BeginFrameRendering_0() const { return ___BeginFrameRendering_0; }
	inline Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A ** get_address_of_BeginFrameRendering_0() { return &___BeginFrameRendering_0; }
	inline void set_BeginFrameRendering_0(Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A * value)
	{
		___BeginFrameRendering_0 = value;
		Il2CppCodeGenWriteBarrier((&___BeginFrameRendering_0), value);
	}

	inline static int32_t get_offset_of_BeginCameraRendering_1() { return static_cast<int32_t>(offsetof(UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4, ___BeginCameraRendering_1)); }
	inline Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C * get_BeginCameraRendering_1() const { return ___BeginCameraRendering_1; }
	inline Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C ** get_address_of_BeginCameraRendering_1() { return &___BeginCameraRendering_1; }
	inline void set_BeginCameraRendering_1(Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C * value)
	{
		___BeginCameraRendering_1 = value;
		Il2CppCodeGenWriteBarrier((&___BeginCameraRendering_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYRENDERPIPELINE_T62C711062D224A17F999C0F58CDB6F6E641560A4_H
#ifndef KEYVALUEPAIR_2_T63DF6C75421BB5CCFFB400D834ACE2CEA8A5BAFA_H
#define KEYVALUEPAIR_2_T63DF6C75421BB5CCFFB400D834ACE2CEA8A5BAFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<VertexAnimationTools_30.ObjData_PositionVertex,VertexAnimationTools_30.ObjData_MapVertex>
struct  KeyValuePair_2_t63DF6C75421BB5CCFFB400D834ACE2CEA8A5BAFA 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t63DF6C75421BB5CCFFB400D834ACE2CEA8A5BAFA, ___key_0)); }
	inline PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 * get_key_0() const { return ___key_0; }
	inline PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t63DF6C75421BB5CCFFB400D834ACE2CEA8A5BAFA, ___value_1)); }
	inline MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 * get_value_1() const { return ___value_1; }
	inline MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T63DF6C75421BB5CCFFB400D834ACE2CEA8A5BAFA_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef OBJFILEINFO_T9BE0BE91810D7410D2C388DDAF3BF8F841FF9846_H
#define OBJFILEINFO_T9BE0BE91810D7410D2C388DDAF3BF8F841FF9846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MeshSequenceInfo_ObjFileInfo
struct  ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846 
{
public:
	// System.String VertexAnimationTools_30.MeshSequenceInfo_ObjFileInfo::FileName
	String_t* ___FileName_0;
	// System.Int32 VertexAnimationTools_30.MeshSequenceInfo_ObjFileInfo::Hash
	int32_t ___Hash_1;
	// System.Int32 VertexAnimationTools_30.MeshSequenceInfo_ObjFileInfo::ParsedNumber
	int32_t ___ParsedNumber_2;
	// System.IO.FileInfo VertexAnimationTools_30.MeshSequenceInfo_ObjFileInfo::fi
	FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * ___fi_3;

public:
	inline static int32_t get_offset_of_FileName_0() { return static_cast<int32_t>(offsetof(ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846, ___FileName_0)); }
	inline String_t* get_FileName_0() const { return ___FileName_0; }
	inline String_t** get_address_of_FileName_0() { return &___FileName_0; }
	inline void set_FileName_0(String_t* value)
	{
		___FileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___FileName_0), value);
	}

	inline static int32_t get_offset_of_Hash_1() { return static_cast<int32_t>(offsetof(ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846, ___Hash_1)); }
	inline int32_t get_Hash_1() const { return ___Hash_1; }
	inline int32_t* get_address_of_Hash_1() { return &___Hash_1; }
	inline void set_Hash_1(int32_t value)
	{
		___Hash_1 = value;
	}

	inline static int32_t get_offset_of_ParsedNumber_2() { return static_cast<int32_t>(offsetof(ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846, ___ParsedNumber_2)); }
	inline int32_t get_ParsedNumber_2() const { return ___ParsedNumber_2; }
	inline int32_t* get_address_of_ParsedNumber_2() { return &___ParsedNumber_2; }
	inline void set_ParsedNumber_2(int32_t value)
	{
		___ParsedNumber_2 = value;
	}

	inline static int32_t get_offset_of_fi_3() { return static_cast<int32_t>(offsetof(ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846, ___fi_3)); }
	inline FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * get_fi_3() const { return ___fi_3; }
	inline FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C ** get_address_of_fi_3() { return &___fi_3; }
	inline void set_fi_3(FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * value)
	{
		___fi_3 = value;
		Il2CppCodeGenWriteBarrier((&___fi_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VertexAnimationTools_30.MeshSequenceInfo/ObjFileInfo
struct ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846_marshaled_pinvoke
{
	char* ___FileName_0;
	int32_t ___Hash_1;
	int32_t ___ParsedNumber_2;
	FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * ___fi_3;
};
// Native definition for COM marshalling of VertexAnimationTools_30.MeshSequenceInfo/ObjFileInfo
struct ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846_marshaled_com
{
	Il2CppChar* ___FileName_0;
	int32_t ___Hash_1;
	int32_t ___ParsedNumber_2;
	FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * ___fi_3;
};
#endif // OBJFILEINFO_T9BE0BE91810D7410D2C388DDAF3BF8F841FF9846_H
#ifndef NUMBERSRANGE_T8E4C7EFBF23B572F991E58128981D31BAC842775_H
#define NUMBERSRANGE_T8E4C7EFBF23B572F991E58128981D31BAC842775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.NumbersRange
struct  NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775 
{
public:
	// System.Int32 VertexAnimationTools_30.NumbersRange::Min
	int32_t ___Min_0;
	// System.Int32 VertexAnimationTools_30.NumbersRange::Max
	int32_t ___Max_1;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775, ___Min_0)); }
	inline int32_t get_Min_0() const { return ___Min_0; }
	inline int32_t* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(int32_t value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775, ___Max_1)); }
	inline int32_t get_Max_1() const { return ___Max_1; }
	inline int32_t* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(int32_t value)
	{
		___Max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERSRANGE_T8E4C7EFBF23B572F991E58128981D31BAC842775_H
#ifndef TASKINFO_T3FBF9A10362590974F251CE28A2FD4FFB8F778F1_H
#define TASKINFO_T3FBF9A10362590974F251CE28A2FD4FFB8F778F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.TaskInfo
struct  TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 
{
public:
	// System.String VertexAnimationTools_30.TaskInfo::Name
	String_t* ___Name_0;
	// System.Single VertexAnimationTools_30.TaskInfo::Persentage
	float ___Persentage_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Persentage_1() { return static_cast<int32_t>(offsetof(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1, ___Persentage_1)); }
	inline float get_Persentage_1() const { return ___Persentage_1; }
	inline float* get_address_of_Persentage_1() { return &___Persentage_1; }
	inline void set_Persentage_1(float value)
	{
		___Persentage_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VertexAnimationTools_30.TaskInfo
struct TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_pinvoke
{
	char* ___Name_0;
	float ___Persentage_1;
};
// Native definition for COM marshalling of VertexAnimationTools_30.TaskInfo
struct TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_com
{
	Il2CppChar* ___Name_0;
	float ___Persentage_1;
};
#endif // TASKINFO_T3FBF9A10362590974F251CE28A2FD4FFB8F778F1_H
#ifndef ENUMERATOR_T7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6_H
#define ENUMERATOR_T7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2_Enumerator<VertexAnimationTools_30.ObjData_PositionVertex,VertexAnimationTools_30.ObjData_MapVertex>
struct  Enumerator_t7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::dictionary
	Dictionary_2_tDBF08A9925144C332F48167D92287492E9FE58DC * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::current
	KeyValuePair_2_t63DF6C75421BB5CCFFB400D834ACE2CEA8A5BAFA  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6, ___dictionary_0)); }
	inline Dictionary_2_tDBF08A9925144C332F48167D92287492E9FE58DC * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tDBF08A9925144C332F48167D92287492E9FE58DC ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tDBF08A9925144C332F48167D92287492E9FE58DC * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_t7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_t7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6, ___current_3)); }
	inline KeyValuePair_2_t63DF6C75421BB5CCFFB400D834ACE2CEA8A5BAFA  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t63DF6C75421BB5CCFFB400D834ACE2CEA8A5BAFA * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t63DF6C75421BB5CCFFB400D834ACE2CEA8A5BAFA  value)
	{
		___current_3 = value;
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_t7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef INDEXFORMAT_T92C58F1C3C12C24A5F430966267EA43F6C4C16EB_H
#define INDEXFORMAT_T92C58F1C3C12C24A5F430966267EA43F6C4C16EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.IndexFormat
struct  IndexFormat_t92C58F1C3C12C24A5F430966267EA43F6C4C16EB 
{
public:
	// System.Int32 UnityEngine.Rendering.IndexFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IndexFormat_t92C58F1C3C12C24A5F430966267EA43F6C4C16EB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXFORMAT_T92C58F1C3C12C24A5F430966267EA43F6C4C16EB_H
#ifndef AXISOPTION_T2E4FAD7B3F55CE394680B6CEA9C2F01B92849786_H
#define AXISOPTION_T2E4FAD7B3F55CE394680B6CEA9C2F01B92849786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick_AxisOption
struct  AxisOption_t2E4FAD7B3F55CE394680B6CEA9C2F01B92849786 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick_AxisOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOption_t2E4FAD7B3F55CE394680B6CEA9C2F01B92849786, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T2E4FAD7B3F55CE394680B6CEA9C2F01B92849786_H
#ifndef MAPPINGTYPE_TA5DC189BC7DB7B5AE049E710D502B88A32BA87B3_H
#define MAPPINGTYPE_TA5DC189BC7DB7B5AE049E710D502B88A32BA87B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput_AxisMapping_MappingType
struct  MappingType_tA5DC189BC7DB7B5AE049E710D502B88A32BA87B3 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput_AxisMapping_MappingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MappingType_tA5DC189BC7DB7B5AE049E710D502B88A32BA87B3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_TA5DC189BC7DB7B5AE049E710D502B88A32BA87B3_H
#ifndef AXISOPTIONS_T7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71_H
#define AXISOPTIONS_T7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput_AxisOptions
struct  AxisOptions_t7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput_AxisOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOptions_t7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71_H
#ifndef AXISOPTION_TA48A0481101792CB4679A6C5D81F14DB1582FF11_H
#define AXISOPTION_TA48A0481101792CB4679A6C5D81F14DB1582FF11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad_AxisOption
struct  AxisOption_tA48A0481101792CB4679A6C5D81F14DB1582FF11 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad_AxisOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOption_tA48A0481101792CB4679A6C5D81F14DB1582FF11, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_TA48A0481101792CB4679A6C5D81F14DB1582FF11_H
#ifndef CONTROLSTYLE_T60A34C5903F112F7E6982BB8161F9F7372669CAC_H
#define CONTROLSTYLE_T60A34C5903F112F7E6982BB8161F9F7372669CAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad_ControlStyle
struct  ControlStyle_t60A34C5903F112F7E6982BB8161F9F7372669CAC 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad_ControlStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlStyle_t60A34C5903F112F7E6982BB8161F9F7372669CAC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTYLE_T60A34C5903F112F7E6982BB8161F9F7372669CAC_H
#ifndef VIRTUALINPUT_TCCB9F78E94F5E3C6E5162E7AA746292D827BAF39_H
#define VIRTUALINPUT_TCCB9F78E94F5E3C6E5162E7AA746292D827BAF39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct  VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::<virtualMousePosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CvirtualMousePositionU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualAxes
	Dictionary_2_t9CB40DA291556AD6A0449B52E9BB70FCA14CC1B3 * ___m_VirtualAxes_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualButton> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualButtons
	Dictionary_2_t687EE995160D28585A45E2513C0F20E2CBE1626E * ___m_VirtualButtons_2;
	// System.Collections.Generic.List`1<System.String> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_AlwaysUseVirtual
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_AlwaysUseVirtual_3;

public:
	inline static int32_t get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39, ___U3CvirtualMousePositionU3Ek__BackingField_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CvirtualMousePositionU3Ek__BackingField_0() const { return ___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return &___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline void set_U3CvirtualMousePositionU3Ek__BackingField_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CvirtualMousePositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualAxes_1() { return static_cast<int32_t>(offsetof(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39, ___m_VirtualAxes_1)); }
	inline Dictionary_2_t9CB40DA291556AD6A0449B52E9BB70FCA14CC1B3 * get_m_VirtualAxes_1() const { return ___m_VirtualAxes_1; }
	inline Dictionary_2_t9CB40DA291556AD6A0449B52E9BB70FCA14CC1B3 ** get_address_of_m_VirtualAxes_1() { return &___m_VirtualAxes_1; }
	inline void set_m_VirtualAxes_1(Dictionary_2_t9CB40DA291556AD6A0449B52E9BB70FCA14CC1B3 * value)
	{
		___m_VirtualAxes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualAxes_1), value);
	}

	inline static int32_t get_offset_of_m_VirtualButtons_2() { return static_cast<int32_t>(offsetof(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39, ___m_VirtualButtons_2)); }
	inline Dictionary_2_t687EE995160D28585A45E2513C0F20E2CBE1626E * get_m_VirtualButtons_2() const { return ___m_VirtualButtons_2; }
	inline Dictionary_2_t687EE995160D28585A45E2513C0F20E2CBE1626E ** get_address_of_m_VirtualButtons_2() { return &___m_VirtualButtons_2; }
	inline void set_m_VirtualButtons_2(Dictionary_2_t687EE995160D28585A45E2513C0F20E2CBE1626E * value)
	{
		___m_VirtualButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualButtons_2), value);
	}

	inline static int32_t get_offset_of_m_AlwaysUseVirtual_3() { return static_cast<int32_t>(offsetof(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39, ___m_AlwaysUseVirtual_3)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_AlwaysUseVirtual_3() const { return ___m_AlwaysUseVirtual_3; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_AlwaysUseVirtual_3() { return &___m_AlwaysUseVirtual_3; }
	inline void set_m_AlwaysUseVirtual_3(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_AlwaysUseVirtual_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlwaysUseVirtual_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALINPUT_TCCB9F78E94F5E3C6E5162E7AA746292D827BAF39_H
#ifndef AUTOPLAYBACKTYPEENUM_TF7A9D12E935B6A75A0B4D544954EF4DFF033BC44_H
#define AUTOPLAYBACKTYPEENUM_TF7A9D12E935B6A75A0B4D544954EF4DFF033BC44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.AutoPlaybackTypeEnum
struct  AutoPlaybackTypeEnum_tF7A9D12E935B6A75A0B4D544954EF4DFF033BC44 
{
public:
	// System.Int32 VertexAnimationTools_30.AutoPlaybackTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoPlaybackTypeEnum_tF7A9D12E935B6A75A0B4D544954EF4DFF033BC44, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAYBACKTYPEENUM_TF7A9D12E935B6A75A0B4D544954EF4DFF033BC44_H
#ifndef BINDINGHELPER_T94750971A156A320AB3463254F2A9A6702A772E8_H
#define BINDINGHELPER_T94750971A156A320AB3463254F2A9A6702A772E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.BindingHelper
struct  BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8  : public RuntimeObject
{
public:
	// UnityEngine.MeshCollider VertexAnimationTools_30.BindingHelper::mc
	MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * ___mc_0;
	// UnityEngine.MeshCollider VertexAnimationTools_30.BindingHelper::imc
	MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * ___imc_1;
	// System.Int32[] VertexAnimationTools_30.BindingHelper::tris
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___tris_2;
	// UnityEngine.Matrix4x4 VertexAnimationTools_30.BindingHelper::ltw
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___ltw_3;
	// UnityEngine.Vector3[] VertexAnimationTools_30.BindingHelper::dirs
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___dirs_4;
	// UnityEngine.Vector3[] VertexAnimationTools_30.BindingHelper::vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___vertices_5;
	// UnityEngine.Vector3[] VertexAnimationTools_30.BindingHelper::normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___normals_6;
	// UnityEngine.Vector3[] VertexAnimationTools_30.BindingHelper::tangents
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___tangents_7;
	// System.Collections.Generic.HashSet`1<VertexAnimationTools_30.BindingHelper_Edge> VertexAnimationTools_30.BindingHelper::edges
	HashSet_1_tFF95153E7989BBFED33E303A897B33F7D44340ED * ___edges_8;
	// VertexAnimationTools_30.BindingHelper_Edge[] VertexAnimationTools_30.BindingHelper::edgesArr
	EdgeU5BU5D_t8B1C87C4045CACD9AD3C18548F8E38FCFE8DCC3A* ___edgesArr_9;

public:
	inline static int32_t get_offset_of_mc_0() { return static_cast<int32_t>(offsetof(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8, ___mc_0)); }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * get_mc_0() const { return ___mc_0; }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE ** get_address_of_mc_0() { return &___mc_0; }
	inline void set_mc_0(MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * value)
	{
		___mc_0 = value;
		Il2CppCodeGenWriteBarrier((&___mc_0), value);
	}

	inline static int32_t get_offset_of_imc_1() { return static_cast<int32_t>(offsetof(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8, ___imc_1)); }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * get_imc_1() const { return ___imc_1; }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE ** get_address_of_imc_1() { return &___imc_1; }
	inline void set_imc_1(MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * value)
	{
		___imc_1 = value;
		Il2CppCodeGenWriteBarrier((&___imc_1), value);
	}

	inline static int32_t get_offset_of_tris_2() { return static_cast<int32_t>(offsetof(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8, ___tris_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_tris_2() const { return ___tris_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_tris_2() { return &___tris_2; }
	inline void set_tris_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___tris_2 = value;
		Il2CppCodeGenWriteBarrier((&___tris_2), value);
	}

	inline static int32_t get_offset_of_ltw_3() { return static_cast<int32_t>(offsetof(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8, ___ltw_3)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_ltw_3() const { return ___ltw_3; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_ltw_3() { return &___ltw_3; }
	inline void set_ltw_3(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___ltw_3 = value;
	}

	inline static int32_t get_offset_of_dirs_4() { return static_cast<int32_t>(offsetof(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8, ___dirs_4)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_dirs_4() const { return ___dirs_4; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_dirs_4() { return &___dirs_4; }
	inline void set_dirs_4(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___dirs_4 = value;
		Il2CppCodeGenWriteBarrier((&___dirs_4), value);
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8, ___vertices_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_vertices_5() const { return ___vertices_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_5), value);
	}

	inline static int32_t get_offset_of_normals_6() { return static_cast<int32_t>(offsetof(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8, ___normals_6)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_normals_6() const { return ___normals_6; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_normals_6() { return &___normals_6; }
	inline void set_normals_6(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___normals_6), value);
	}

	inline static int32_t get_offset_of_tangents_7() { return static_cast<int32_t>(offsetof(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8, ___tangents_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_tangents_7() const { return ___tangents_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_tangents_7() { return &___tangents_7; }
	inline void set_tangents_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_7), value);
	}

	inline static int32_t get_offset_of_edges_8() { return static_cast<int32_t>(offsetof(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8, ___edges_8)); }
	inline HashSet_1_tFF95153E7989BBFED33E303A897B33F7D44340ED * get_edges_8() const { return ___edges_8; }
	inline HashSet_1_tFF95153E7989BBFED33E303A897B33F7D44340ED ** get_address_of_edges_8() { return &___edges_8; }
	inline void set_edges_8(HashSet_1_tFF95153E7989BBFED33E303A897B33F7D44340ED * value)
	{
		___edges_8 = value;
		Il2CppCodeGenWriteBarrier((&___edges_8), value);
	}

	inline static int32_t get_offset_of_edgesArr_9() { return static_cast<int32_t>(offsetof(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8, ___edgesArr_9)); }
	inline EdgeU5BU5D_t8B1C87C4045CACD9AD3C18548F8E38FCFE8DCC3A* get_edgesArr_9() const { return ___edgesArr_9; }
	inline EdgeU5BU5D_t8B1C87C4045CACD9AD3C18548F8E38FCFE8DCC3A** get_address_of_edgesArr_9() { return &___edgesArr_9; }
	inline void set_edgesArr_9(EdgeU5BU5D_t8B1C87C4045CACD9AD3C18548F8E38FCFE8DCC3A* value)
	{
		___edgesArr_9 = value;
		Il2CppCodeGenWriteBarrier((&___edgesArr_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGHELPER_T94750971A156A320AB3463254F2A9A6702A772E8_H
#ifndef EDGE_T82512244422942C9C917667D89F2511A61F288B0_H
#define EDGE_T82512244422942C9C917667D89F2511A61F288B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.BindingHelper_Edge
struct  Edge_t82512244422942C9C917667D89F2511A61F288B0 
{
public:
	// System.Int32 VertexAnimationTools_30.BindingHelper_Edge::A
	int32_t ___A_0;
	// System.Int32 VertexAnimationTools_30.BindingHelper_Edge::B
	int32_t ___B_1;
	// UnityEngine.Vector3 VertexAnimationTools_30.BindingHelper_Edge::pA
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pA_2;
	// UnityEngine.Vector3 VertexAnimationTools_30.BindingHelper_Edge::pB
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pB_3;
	// UnityEngine.Vector3 VertexAnimationTools_30.BindingHelper_Edge::ab
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___ab_4;
	// System.Single VertexAnimationTools_30.BindingHelper_Edge::length
	float ___length_5;
	// System.Single VertexAnimationTools_30.BindingHelper_Edge::length2
	float ___length2_6;

public:
	inline static int32_t get_offset_of_A_0() { return static_cast<int32_t>(offsetof(Edge_t82512244422942C9C917667D89F2511A61F288B0, ___A_0)); }
	inline int32_t get_A_0() const { return ___A_0; }
	inline int32_t* get_address_of_A_0() { return &___A_0; }
	inline void set_A_0(int32_t value)
	{
		___A_0 = value;
	}

	inline static int32_t get_offset_of_B_1() { return static_cast<int32_t>(offsetof(Edge_t82512244422942C9C917667D89F2511A61F288B0, ___B_1)); }
	inline int32_t get_B_1() const { return ___B_1; }
	inline int32_t* get_address_of_B_1() { return &___B_1; }
	inline void set_B_1(int32_t value)
	{
		___B_1 = value;
	}

	inline static int32_t get_offset_of_pA_2() { return static_cast<int32_t>(offsetof(Edge_t82512244422942C9C917667D89F2511A61F288B0, ___pA_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pA_2() const { return ___pA_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pA_2() { return &___pA_2; }
	inline void set_pA_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pA_2 = value;
	}

	inline static int32_t get_offset_of_pB_3() { return static_cast<int32_t>(offsetof(Edge_t82512244422942C9C917667D89F2511A61F288B0, ___pB_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pB_3() const { return ___pB_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pB_3() { return &___pB_3; }
	inline void set_pB_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pB_3 = value;
	}

	inline static int32_t get_offset_of_ab_4() { return static_cast<int32_t>(offsetof(Edge_t82512244422942C9C917667D89F2511A61F288B0, ___ab_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_ab_4() const { return ___ab_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_ab_4() { return &___ab_4; }
	inline void set_ab_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___ab_4 = value;
	}

	inline static int32_t get_offset_of_length_5() { return static_cast<int32_t>(offsetof(Edge_t82512244422942C9C917667D89F2511A61F288B0, ___length_5)); }
	inline float get_length_5() const { return ___length_5; }
	inline float* get_address_of_length_5() { return &___length_5; }
	inline void set_length_5(float value)
	{
		___length_5 = value;
	}

	inline static int32_t get_offset_of_length2_6() { return static_cast<int32_t>(offsetof(Edge_t82512244422942C9C917667D89F2511A61F288B0, ___length2_6)); }
	inline float get_length2_6() const { return ___length2_6; }
	inline float* get_address_of_length2_6() { return &___length2_6; }
	inline void set_length2_6(float value)
	{
		___length2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGE_T82512244422942C9C917667D89F2511A61F288B0_H
#ifndef U3CSMOOTHIEU3ED__26_TC2C1944834010B560BEA42332BE6BD620C942BE8_H
#define U3CSMOOTHIEU3ED__26_TC2C1944834010B560BEA42332BE6BD620C942BE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.FramesArray_<SmoothIE>d__26
struct  U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// VertexAnimationTools_30.TaskInfo VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<>2__current
	TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  ___U3CU3E2__current_1;
	// System.Int32 VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// VertexAnimationTools_30.FramesArray VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<>4__this
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * ___U3CU3E4__this_3;
	// System.Int32 VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::iterations
	int32_t ___iterations_4;
	// System.Int32 VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<>3__iterations
	int32_t ___U3CU3E3__iterations_5;
	// System.Single VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::smoothMin
	float ___smoothMin_6;
	// System.Single VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<>3__smoothMin
	float ___U3CU3E3__smoothMin_7;
	// System.Single VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::smoothMax
	float ___smoothMax_8;
	// System.Single VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<>3__smoothMax
	float ___U3CU3E3__smoothMax_9;
	// System.Single VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::easeOffset
	float ___easeOffset_10;
	// System.Single VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<>3__easeOffset
	float ___U3CU3E3__easeOffset_11;
	// System.Single VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::easeLength
	float ___easeLength_12;
	// System.Single VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<>3__easeLength
	float ___U3CU3E3__easeLength_13;
	// VertexAnimationTools_30.FramesArray VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<smoothed>5__2
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * ___U3CsmoothedU3E5__2_14;
	// VertexAnimationTools_30.FramesArray VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<temp>5__3
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * ___U3CtempU3E5__3_15;
	// System.Int32 VertexAnimationTools_30.FramesArray_<SmoothIE>d__26::<i>5__4
	int32_t ___U3CiU3E5__4_16;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CU3E2__current_1)); }
	inline TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CU3E4__this_3)); }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_iterations_4() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___iterations_4)); }
	inline int32_t get_iterations_4() const { return ___iterations_4; }
	inline int32_t* get_address_of_iterations_4() { return &___iterations_4; }
	inline void set_iterations_4(int32_t value)
	{
		___iterations_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__iterations_5() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CU3E3__iterations_5)); }
	inline int32_t get_U3CU3E3__iterations_5() const { return ___U3CU3E3__iterations_5; }
	inline int32_t* get_address_of_U3CU3E3__iterations_5() { return &___U3CU3E3__iterations_5; }
	inline void set_U3CU3E3__iterations_5(int32_t value)
	{
		___U3CU3E3__iterations_5 = value;
	}

	inline static int32_t get_offset_of_smoothMin_6() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___smoothMin_6)); }
	inline float get_smoothMin_6() const { return ___smoothMin_6; }
	inline float* get_address_of_smoothMin_6() { return &___smoothMin_6; }
	inline void set_smoothMin_6(float value)
	{
		___smoothMin_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__smoothMin_7() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CU3E3__smoothMin_7)); }
	inline float get_U3CU3E3__smoothMin_7() const { return ___U3CU3E3__smoothMin_7; }
	inline float* get_address_of_U3CU3E3__smoothMin_7() { return &___U3CU3E3__smoothMin_7; }
	inline void set_U3CU3E3__smoothMin_7(float value)
	{
		___U3CU3E3__smoothMin_7 = value;
	}

	inline static int32_t get_offset_of_smoothMax_8() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___smoothMax_8)); }
	inline float get_smoothMax_8() const { return ___smoothMax_8; }
	inline float* get_address_of_smoothMax_8() { return &___smoothMax_8; }
	inline void set_smoothMax_8(float value)
	{
		___smoothMax_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__smoothMax_9() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CU3E3__smoothMax_9)); }
	inline float get_U3CU3E3__smoothMax_9() const { return ___U3CU3E3__smoothMax_9; }
	inline float* get_address_of_U3CU3E3__smoothMax_9() { return &___U3CU3E3__smoothMax_9; }
	inline void set_U3CU3E3__smoothMax_9(float value)
	{
		___U3CU3E3__smoothMax_9 = value;
	}

	inline static int32_t get_offset_of_easeOffset_10() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___easeOffset_10)); }
	inline float get_easeOffset_10() const { return ___easeOffset_10; }
	inline float* get_address_of_easeOffset_10() { return &___easeOffset_10; }
	inline void set_easeOffset_10(float value)
	{
		___easeOffset_10 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__easeOffset_11() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CU3E3__easeOffset_11)); }
	inline float get_U3CU3E3__easeOffset_11() const { return ___U3CU3E3__easeOffset_11; }
	inline float* get_address_of_U3CU3E3__easeOffset_11() { return &___U3CU3E3__easeOffset_11; }
	inline void set_U3CU3E3__easeOffset_11(float value)
	{
		___U3CU3E3__easeOffset_11 = value;
	}

	inline static int32_t get_offset_of_easeLength_12() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___easeLength_12)); }
	inline float get_easeLength_12() const { return ___easeLength_12; }
	inline float* get_address_of_easeLength_12() { return &___easeLength_12; }
	inline void set_easeLength_12(float value)
	{
		___easeLength_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__easeLength_13() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CU3E3__easeLength_13)); }
	inline float get_U3CU3E3__easeLength_13() const { return ___U3CU3E3__easeLength_13; }
	inline float* get_address_of_U3CU3E3__easeLength_13() { return &___U3CU3E3__easeLength_13; }
	inline void set_U3CU3E3__easeLength_13(float value)
	{
		___U3CU3E3__easeLength_13 = value;
	}

	inline static int32_t get_offset_of_U3CsmoothedU3E5__2_14() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CsmoothedU3E5__2_14)); }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * get_U3CsmoothedU3E5__2_14() const { return ___U3CsmoothedU3E5__2_14; }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 ** get_address_of_U3CsmoothedU3E5__2_14() { return &___U3CsmoothedU3E5__2_14; }
	inline void set_U3CsmoothedU3E5__2_14(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * value)
	{
		___U3CsmoothedU3E5__2_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsmoothedU3E5__2_14), value);
	}

	inline static int32_t get_offset_of_U3CtempU3E5__3_15() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CtempU3E5__3_15)); }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * get_U3CtempU3E5__3_15() const { return ___U3CtempU3E5__3_15; }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 ** get_address_of_U3CtempU3E5__3_15() { return &___U3CtempU3E5__3_15; }
	inline void set_U3CtempU3E5__3_15(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * value)
	{
		___U3CtempU3E5__3_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempU3E5__3_15), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__4_16() { return static_cast<int32_t>(offsetof(U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8, ___U3CiU3E5__4_16)); }
	inline int32_t get_U3CiU3E5__4_16() const { return ___U3CiU3E5__4_16; }
	inline int32_t* get_address_of_U3CiU3E5__4_16() { return &___U3CiU3E5__4_16; }
	inline void set_U3CiU3E5__4_16(int32_t value)
	{
		___U3CiU3E5__4_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHIEU3ED__26_TC2C1944834010B560BEA42332BE6BD620C942BE8_H
#ifndef INTERPOLATEMODEENUM_T4C69237CDE436AC34D5440295EF0FE18E6D18EA8_H
#define INTERPOLATEMODEENUM_T4C69237CDE436AC34D5440295EF0FE18E6D18EA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.InterpolateModeEnum
struct  InterpolateModeEnum_t4C69237CDE436AC34D5440295EF0FE18E6D18EA8 
{
public:
	// System.Int32 VertexAnimationTools_30.InterpolateModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolateModeEnum_t4C69237CDE436AC34D5440295EF0FE18E6D18EA8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEMODEENUM_T4C69237CDE436AC34D5440295EF0FE18E6D18EA8_H
#ifndef SORTMODEENUM_T00E0F838F904669A96A495F07E911ACEB856FADF_H
#define SORTMODEENUM_T00E0F838F904669A96A495F07E911ACEB856FADF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MeshSequenceInfo_SortModeEnum
struct  SortModeEnum_t00E0F838F904669A96A495F07E911ACEB856FADF 
{
public:
	// System.Int32 VertexAnimationTools_30.MeshSequenceInfo_SortModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SortModeEnum_t00E0F838F904669A96A495F07E911ACEB856FADF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTMODEENUM_T00E0F838F904669A96A495F07E911ACEB856FADF_H
#ifndef STATEENUM_T1A5CA96CCB6E733D9C08173F783ED35D49C3C242_H
#define STATEENUM_T1A5CA96CCB6E733D9C08173F783ED35D49C3C242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MeshSequenceInfo_StateEnum
struct  StateEnum_t1A5CA96CCB6E733D9C08173F783ED35D49C3C242 
{
public:
	// System.Int32 VertexAnimationTools_30.MeshSequenceInfo_StateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StateEnum_t1A5CA96CCB6E733D9C08173F783ED35D49C3C242, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEENUM_T1A5CA96CCB6E733D9C08173F783ED35D49C3C242_H
#ifndef U3CIMPORTIEU3ED__21_T723562B682F441D32174CAFFCA594171268B053D_H
#define U3CIMPORTIEU3ED__21_T723562B682F441D32174CAFFCA594171268B053D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21
struct  U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// VertexAnimationTools_30.TaskInfo VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::<>2__current
	TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  ___U3CU3E2__current_1;
	// System.Int32 VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// VertexAnimationTools_30.MeshSequence VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::t
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * ___t_3;
	// VertexAnimationTools_30.MeshSequence VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::<>3__t
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * ___U3CU3E3__t_4;
	// VertexAnimationTools_30.MeshSequencePlayer VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::<>4__this
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838 * ___U3CU3E4__this_5;
	// System.Int32 VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::<importTo>5__2
	int32_t ___U3CimportToU3E5__2_6;
	// System.Int32 VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::<totalFramesCount>5__3
	int32_t ___U3CtotalFramesCountU3E5__3_7;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.MeshSequence_Frame> VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::<nframes>5__4
	List_1_t111085AD46AD6E1D7CB4FE6A22570DAB13DF787F * ___U3CnframesU3E5__4_8;
	// System.Int32 VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::<counter>5__5
	int32_t ___U3CcounterU3E5__5_9;
	// System.Int32 VertexAnimationTools_30.MeshSequencePlayer_<ImportIE>d__21::<f>5__6
	int32_t ___U3CfU3E5__6_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___U3CU3E2__current_1)); }
	inline TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_t_3() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___t_3)); }
	inline MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * get_t_3() const { return ___t_3; }
	inline MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA ** get_address_of_t_3() { return &___t_3; }
	inline void set_t_3(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * value)
	{
		___t_3 = value;
		Il2CppCodeGenWriteBarrier((&___t_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__t_4() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___U3CU3E3__t_4)); }
	inline MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * get_U3CU3E3__t_4() const { return ___U3CU3E3__t_4; }
	inline MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA ** get_address_of_U3CU3E3__t_4() { return &___U3CU3E3__t_4; }
	inline void set_U3CU3E3__t_4(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * value)
	{
		___U3CU3E3__t_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__t_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___U3CU3E4__this_5)); }
	inline MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CimportToU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___U3CimportToU3E5__2_6)); }
	inline int32_t get_U3CimportToU3E5__2_6() const { return ___U3CimportToU3E5__2_6; }
	inline int32_t* get_address_of_U3CimportToU3E5__2_6() { return &___U3CimportToU3E5__2_6; }
	inline void set_U3CimportToU3E5__2_6(int32_t value)
	{
		___U3CimportToU3E5__2_6 = value;
	}

	inline static int32_t get_offset_of_U3CtotalFramesCountU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___U3CtotalFramesCountU3E5__3_7)); }
	inline int32_t get_U3CtotalFramesCountU3E5__3_7() const { return ___U3CtotalFramesCountU3E5__3_7; }
	inline int32_t* get_address_of_U3CtotalFramesCountU3E5__3_7() { return &___U3CtotalFramesCountU3E5__3_7; }
	inline void set_U3CtotalFramesCountU3E5__3_7(int32_t value)
	{
		___U3CtotalFramesCountU3E5__3_7 = value;
	}

	inline static int32_t get_offset_of_U3CnframesU3E5__4_8() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___U3CnframesU3E5__4_8)); }
	inline List_1_t111085AD46AD6E1D7CB4FE6A22570DAB13DF787F * get_U3CnframesU3E5__4_8() const { return ___U3CnframesU3E5__4_8; }
	inline List_1_t111085AD46AD6E1D7CB4FE6A22570DAB13DF787F ** get_address_of_U3CnframesU3E5__4_8() { return &___U3CnframesU3E5__4_8; }
	inline void set_U3CnframesU3E5__4_8(List_1_t111085AD46AD6E1D7CB4FE6A22570DAB13DF787F * value)
	{
		___U3CnframesU3E5__4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnframesU3E5__4_8), value);
	}

	inline static int32_t get_offset_of_U3CcounterU3E5__5_9() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___U3CcounterU3E5__5_9)); }
	inline int32_t get_U3CcounterU3E5__5_9() const { return ___U3CcounterU3E5__5_9; }
	inline int32_t* get_address_of_U3CcounterU3E5__5_9() { return &___U3CcounterU3E5__5_9; }
	inline void set_U3CcounterU3E5__5_9(int32_t value)
	{
		___U3CcounterU3E5__5_9 = value;
	}

	inline static int32_t get_offset_of_U3CfU3E5__6_10() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D, ___U3CfU3E5__6_10)); }
	inline int32_t get_U3CfU3E5__6_10() const { return ___U3CfU3E5__6_10; }
	inline int32_t* get_address_of_U3CfU3E5__6_10() { return &___U3CfU3E5__6_10; }
	inline void set_U3CfU3E5__6_10(int32_t value)
	{
		___U3CfU3E5__6_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CIMPORTIEU3ED__21_T723562B682F441D32174CAFFCA594171268B053D_H
#ifndef U3CBINDIEU3ED__56_T899FB537CA32CF081270FD9EFC641829CC5A193A_H
#define U3CBINDIEU3ED__56_T899FB537CA32CF081270FD9EFC641829CC5A193A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_<BindIE>d__56
struct  U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.ObjData_<BindIE>d__56::<>1__state
	int32_t ___U3CU3E1__state_0;
	// VertexAnimationTools_30.TaskInfo VertexAnimationTools_30.ObjData_<BindIE>d__56::<>2__current
	TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  ___U3CU3E2__current_1;
	// System.Int32 VertexAnimationTools_30.ObjData_<BindIE>d__56::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// VertexAnimationTools_30.ObjData VertexAnimationTools_30.ObjData_<BindIE>d__56::<>4__this
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62 * ___U3CU3E4__this_3;
	// VertexAnimationTools_30.BindingHelper VertexAnimationTools_30.ObjData_<BindIE>d__56::bh
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 * ___bh_4;
	// VertexAnimationTools_30.BindingHelper VertexAnimationTools_30.ObjData_<BindIE>d__56::<>3__bh
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 * ___U3CU3E3__bh_5;
	// System.String VertexAnimationTools_30.ObjData_<BindIE>d__56::<tiname>5__2
	String_t* ___U3CtinameU3E5__2_6;
	// System.Int32 VertexAnimationTools_30.ObjData_<BindIE>d__56::<i>5__3
	int32_t ___U3CiU3E5__3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A, ___U3CU3E2__current_1)); }
	inline TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A, ___U3CU3E4__this_3)); }
	inline ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_bh_4() { return static_cast<int32_t>(offsetof(U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A, ___bh_4)); }
	inline BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 * get_bh_4() const { return ___bh_4; }
	inline BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 ** get_address_of_bh_4() { return &___bh_4; }
	inline void set_bh_4(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 * value)
	{
		___bh_4 = value;
		Il2CppCodeGenWriteBarrier((&___bh_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__bh_5() { return static_cast<int32_t>(offsetof(U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A, ___U3CU3E3__bh_5)); }
	inline BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 * get_U3CU3E3__bh_5() const { return ___U3CU3E3__bh_5; }
	inline BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 ** get_address_of_U3CU3E3__bh_5() { return &___U3CU3E3__bh_5; }
	inline void set_U3CU3E3__bh_5(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 * value)
	{
		___U3CU3E3__bh_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__bh_5), value);
	}

	inline static int32_t get_offset_of_U3CtinameU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A, ___U3CtinameU3E5__2_6)); }
	inline String_t* get_U3CtinameU3E5__2_6() const { return ___U3CtinameU3E5__2_6; }
	inline String_t** get_address_of_U3CtinameU3E5__2_6() { return &___U3CtinameU3E5__2_6; }
	inline void set_U3CtinameU3E5__2_6(String_t* value)
	{
		___U3CtinameU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtinameU3E5__2_6), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A, ___U3CiU3E5__3_7)); }
	inline int32_t get_U3CiU3E5__3_7() const { return ___U3CiU3E5__3_7; }
	inline int32_t* get_address_of_U3CiU3E5__3_7() { return &___U3CiU3E5__3_7; }
	inline void set_U3CiU3E5__3_7(int32_t value)
	{
		___U3CiU3E5__3_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBINDIEU3ED__56_T899FB537CA32CF081270FD9EFC641829CC5A193A_H
#ifndef FACE_T9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB_H
#define FACE_T9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_Face
struct  Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.ObjData_Face::Idx
	int32_t ___Idx_0;
	// VertexAnimationTools_30.ObjData_UMeshVertex VertexAnimationTools_30.ObjData_Face::Va
	UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * ___Va_1;
	// VertexAnimationTools_30.ObjData_UMeshVertex VertexAnimationTools_30.ObjData_Face::Vb
	UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * ___Vb_2;
	// VertexAnimationTools_30.ObjData_UMeshVertex VertexAnimationTools_30.ObjData_Face::Vc
	UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * ___Vc_3;
	// VertexAnimationTools_30.ObjData_Polygon VertexAnimationTools_30.ObjData_Face::ParentPolygon
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2 * ___ParentPolygon_4;
	// System.Single VertexAnimationTools_30.ObjData_Face::TangentT
	float ___TangentT_5;
	// System.Single VertexAnimationTools_30.ObjData_Face::TangentW
	float ___TangentW_6;
	// System.Single VertexAnimationTools_30.ObjData_Face::Sign
	float ___Sign_7;
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_Face::Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Normal_8;
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_Face::Tangent
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Tangent_9;
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_Face::Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Center_10;
	// System.Single VertexAnimationTools_30.ObjData_Face::Area
	float ___Area_11;
	// System.Boolean VertexAnimationTools_30.ObjData_Face::isParallel
	bool ___isParallel_12;
	// System.Single VertexAnimationTools_30.ObjData_Face::tv
	float ___tv_13;

public:
	inline static int32_t get_offset_of_Idx_0() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___Idx_0)); }
	inline int32_t get_Idx_0() const { return ___Idx_0; }
	inline int32_t* get_address_of_Idx_0() { return &___Idx_0; }
	inline void set_Idx_0(int32_t value)
	{
		___Idx_0 = value;
	}

	inline static int32_t get_offset_of_Va_1() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___Va_1)); }
	inline UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * get_Va_1() const { return ___Va_1; }
	inline UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC ** get_address_of_Va_1() { return &___Va_1; }
	inline void set_Va_1(UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * value)
	{
		___Va_1 = value;
		Il2CppCodeGenWriteBarrier((&___Va_1), value);
	}

	inline static int32_t get_offset_of_Vb_2() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___Vb_2)); }
	inline UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * get_Vb_2() const { return ___Vb_2; }
	inline UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC ** get_address_of_Vb_2() { return &___Vb_2; }
	inline void set_Vb_2(UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * value)
	{
		___Vb_2 = value;
		Il2CppCodeGenWriteBarrier((&___Vb_2), value);
	}

	inline static int32_t get_offset_of_Vc_3() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___Vc_3)); }
	inline UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * get_Vc_3() const { return ___Vc_3; }
	inline UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC ** get_address_of_Vc_3() { return &___Vc_3; }
	inline void set_Vc_3(UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * value)
	{
		___Vc_3 = value;
		Il2CppCodeGenWriteBarrier((&___Vc_3), value);
	}

	inline static int32_t get_offset_of_ParentPolygon_4() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___ParentPolygon_4)); }
	inline Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2 * get_ParentPolygon_4() const { return ___ParentPolygon_4; }
	inline Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2 ** get_address_of_ParentPolygon_4() { return &___ParentPolygon_4; }
	inline void set_ParentPolygon_4(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2 * value)
	{
		___ParentPolygon_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParentPolygon_4), value);
	}

	inline static int32_t get_offset_of_TangentT_5() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___TangentT_5)); }
	inline float get_TangentT_5() const { return ___TangentT_5; }
	inline float* get_address_of_TangentT_5() { return &___TangentT_5; }
	inline void set_TangentT_5(float value)
	{
		___TangentT_5 = value;
	}

	inline static int32_t get_offset_of_TangentW_6() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___TangentW_6)); }
	inline float get_TangentW_6() const { return ___TangentW_6; }
	inline float* get_address_of_TangentW_6() { return &___TangentW_6; }
	inline void set_TangentW_6(float value)
	{
		___TangentW_6 = value;
	}

	inline static int32_t get_offset_of_Sign_7() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___Sign_7)); }
	inline float get_Sign_7() const { return ___Sign_7; }
	inline float* get_address_of_Sign_7() { return &___Sign_7; }
	inline void set_Sign_7(float value)
	{
		___Sign_7 = value;
	}

	inline static int32_t get_offset_of_Normal_8() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___Normal_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Normal_8() const { return ___Normal_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Normal_8() { return &___Normal_8; }
	inline void set_Normal_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Normal_8 = value;
	}

	inline static int32_t get_offset_of_Tangent_9() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___Tangent_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Tangent_9() const { return ___Tangent_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Tangent_9() { return &___Tangent_9; }
	inline void set_Tangent_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Tangent_9 = value;
	}

	inline static int32_t get_offset_of_Center_10() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___Center_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Center_10() const { return ___Center_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Center_10() { return &___Center_10; }
	inline void set_Center_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Center_10 = value;
	}

	inline static int32_t get_offset_of_Area_11() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___Area_11)); }
	inline float get_Area_11() const { return ___Area_11; }
	inline float* get_address_of_Area_11() { return &___Area_11; }
	inline void set_Area_11(float value)
	{
		___Area_11 = value;
	}

	inline static int32_t get_offset_of_isParallel_12() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___isParallel_12)); }
	inline bool get_isParallel_12() const { return ___isParallel_12; }
	inline bool* get_address_of_isParallel_12() { return &___isParallel_12; }
	inline void set_isParallel_12(bool value)
	{
		___isParallel_12 = value;
	}

	inline static int32_t get_offset_of_tv_13() { return static_cast<int32_t>(offsetof(Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB, ___tv_13)); }
	inline float get_tv_13() const { return ___tv_13; }
	inline float* get_address_of_tv_13() { return &___tv_13; }
	inline void set_tv_13(float value)
	{
		___tv_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACE_T9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB_H
#ifndef MAPVERTEX_T9B430990F9D13CEABE74D76884A4D730A72CD7D8_H
#define MAPVERTEX_T9B430990F9D13CEABE74D76884A4D730A72CD7D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_MapVertex
struct  MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8  : public RuntimeObject
{
public:
	// VertexAnimationTools_30.ObjData_UMeshVertex VertexAnimationTools_30.ObjData_MapVertex::UMV
	UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * ___UMV_0;
	// UnityEngine.Vector2 VertexAnimationTools_30.ObjData_MapVertex::Value
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Value_1;
	// System.Collections.Generic.HashSet`1<VertexAnimationTools_30.ObjData_Polygon> VertexAnimationTools_30.ObjData_MapVertex::AdjacentPolygons
	HashSet_1_t082160374370AC2CA36B72A29CA36987E5A5024E * ___AdjacentPolygons_2;
	// VertexAnimationTools_30.ObjData_Polygon[] VertexAnimationTools_30.ObjData_MapVertex::aAdjacentPolygons
	PolygonU5BU5D_t709AA0C218D02A8B11F862288748E0804824326C* ___aAdjacentPolygons_3;
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_MapVertex::Tangent
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Tangent_4;
	// System.Single VertexAnimationTools_30.ObjData_MapVertex::TangentW
	float ___TangentW_5;

public:
	inline static int32_t get_offset_of_UMV_0() { return static_cast<int32_t>(offsetof(MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8, ___UMV_0)); }
	inline UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * get_UMV_0() const { return ___UMV_0; }
	inline UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC ** get_address_of_UMV_0() { return &___UMV_0; }
	inline void set_UMV_0(UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC * value)
	{
		___UMV_0 = value;
		Il2CppCodeGenWriteBarrier((&___UMV_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8, ___Value_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Value_1() const { return ___Value_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Value_1 = value;
	}

	inline static int32_t get_offset_of_AdjacentPolygons_2() { return static_cast<int32_t>(offsetof(MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8, ___AdjacentPolygons_2)); }
	inline HashSet_1_t082160374370AC2CA36B72A29CA36987E5A5024E * get_AdjacentPolygons_2() const { return ___AdjacentPolygons_2; }
	inline HashSet_1_t082160374370AC2CA36B72A29CA36987E5A5024E ** get_address_of_AdjacentPolygons_2() { return &___AdjacentPolygons_2; }
	inline void set_AdjacentPolygons_2(HashSet_1_t082160374370AC2CA36B72A29CA36987E5A5024E * value)
	{
		___AdjacentPolygons_2 = value;
		Il2CppCodeGenWriteBarrier((&___AdjacentPolygons_2), value);
	}

	inline static int32_t get_offset_of_aAdjacentPolygons_3() { return static_cast<int32_t>(offsetof(MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8, ___aAdjacentPolygons_3)); }
	inline PolygonU5BU5D_t709AA0C218D02A8B11F862288748E0804824326C* get_aAdjacentPolygons_3() const { return ___aAdjacentPolygons_3; }
	inline PolygonU5BU5D_t709AA0C218D02A8B11F862288748E0804824326C** get_address_of_aAdjacentPolygons_3() { return &___aAdjacentPolygons_3; }
	inline void set_aAdjacentPolygons_3(PolygonU5BU5D_t709AA0C218D02A8B11F862288748E0804824326C* value)
	{
		___aAdjacentPolygons_3 = value;
		Il2CppCodeGenWriteBarrier((&___aAdjacentPolygons_3), value);
	}

	inline static int32_t get_offset_of_Tangent_4() { return static_cast<int32_t>(offsetof(MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8, ___Tangent_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Tangent_4() const { return ___Tangent_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Tangent_4() { return &___Tangent_4; }
	inline void set_Tangent_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Tangent_4 = value;
	}

	inline static int32_t get_offset_of_TangentW_5() { return static_cast<int32_t>(offsetof(MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8, ___TangentW_5)); }
	inline float get_TangentW_5() const { return ___TangentW_5; }
	inline float* get_address_of_TangentW_5() { return &___TangentW_5; }
	inline void set_TangentW_5(float value)
	{
		___TangentW_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPVERTEX_T9B430990F9D13CEABE74D76884A4D730A72CD7D8_H
#ifndef NORMALVERTEX_T82CB2897B045FD15284797267376441CA5ADAE4D_H
#define NORMALVERTEX_T82CB2897B045FD15284797267376441CA5ADAE4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_NormalVertex
struct  NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_NormalVertex::Value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Value_0;
	// System.Single VertexAnimationTools_30.ObjData_NormalVertex::AO
	float ___AO_1;
	// System.Boolean VertexAnimationTools_30.ObjData_NormalVertex::IsInnerAO
	bool ___IsInnerAO_2;
	// System.Int32 VertexAnimationTools_30.ObjData_NormalVertex::SmoothingGroup
	int32_t ___SmoothingGroup_3;
	// VertexAnimationTools_30.ObjData_PositionVertex VertexAnimationTools_30.ObjData_NormalVertex::Position
	PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 * ___Position_4;
	// System.Collections.Generic.HashSet`1<VertexAnimationTools_30.ObjData_AdjacentPolygon> VertexAnimationTools_30.ObjData_NormalVertex::AdjacentPolygons
	HashSet_1_t9CE364960B7559E086A7BC4684E47F2C76D38A61 * ___AdjacentPolygons_5;
	// VertexAnimationTools_30.ObjData_AdjacentPolygon[] VertexAnimationTools_30.ObjData_NormalVertex::aAdjacentPolygons
	AdjacentPolygonU5BU5D_tE9B43C2AADAE61A55EABDEFD42DC7A5D8EB08D10* ___aAdjacentPolygons_6;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_NormalVertex> VertexAnimationTools_30.ObjData_NormalVertex::AdjacentNormals
	List_1_t2DA726067932CE665A45F5079B4C94BC61B0201D * ___AdjacentNormals_7;
	// System.Single VertexAnimationTools_30.ObjData_NormalVertex::Cavity
	float ___Cavity_8;
	// System.Single VertexAnimationTools_30.ObjData_NormalVertex::AdjacentMult
	float ___AdjacentMult_9;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D, ___Value_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Value_0() const { return ___Value_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Value_0 = value;
	}

	inline static int32_t get_offset_of_AO_1() { return static_cast<int32_t>(offsetof(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D, ___AO_1)); }
	inline float get_AO_1() const { return ___AO_1; }
	inline float* get_address_of_AO_1() { return &___AO_1; }
	inline void set_AO_1(float value)
	{
		___AO_1 = value;
	}

	inline static int32_t get_offset_of_IsInnerAO_2() { return static_cast<int32_t>(offsetof(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D, ___IsInnerAO_2)); }
	inline bool get_IsInnerAO_2() const { return ___IsInnerAO_2; }
	inline bool* get_address_of_IsInnerAO_2() { return &___IsInnerAO_2; }
	inline void set_IsInnerAO_2(bool value)
	{
		___IsInnerAO_2 = value;
	}

	inline static int32_t get_offset_of_SmoothingGroup_3() { return static_cast<int32_t>(offsetof(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D, ___SmoothingGroup_3)); }
	inline int32_t get_SmoothingGroup_3() const { return ___SmoothingGroup_3; }
	inline int32_t* get_address_of_SmoothingGroup_3() { return &___SmoothingGroup_3; }
	inline void set_SmoothingGroup_3(int32_t value)
	{
		___SmoothingGroup_3 = value;
	}

	inline static int32_t get_offset_of_Position_4() { return static_cast<int32_t>(offsetof(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D, ___Position_4)); }
	inline PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 * get_Position_4() const { return ___Position_4; }
	inline PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 ** get_address_of_Position_4() { return &___Position_4; }
	inline void set_Position_4(PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 * value)
	{
		___Position_4 = value;
		Il2CppCodeGenWriteBarrier((&___Position_4), value);
	}

	inline static int32_t get_offset_of_AdjacentPolygons_5() { return static_cast<int32_t>(offsetof(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D, ___AdjacentPolygons_5)); }
	inline HashSet_1_t9CE364960B7559E086A7BC4684E47F2C76D38A61 * get_AdjacentPolygons_5() const { return ___AdjacentPolygons_5; }
	inline HashSet_1_t9CE364960B7559E086A7BC4684E47F2C76D38A61 ** get_address_of_AdjacentPolygons_5() { return &___AdjacentPolygons_5; }
	inline void set_AdjacentPolygons_5(HashSet_1_t9CE364960B7559E086A7BC4684E47F2C76D38A61 * value)
	{
		___AdjacentPolygons_5 = value;
		Il2CppCodeGenWriteBarrier((&___AdjacentPolygons_5), value);
	}

	inline static int32_t get_offset_of_aAdjacentPolygons_6() { return static_cast<int32_t>(offsetof(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D, ___aAdjacentPolygons_6)); }
	inline AdjacentPolygonU5BU5D_tE9B43C2AADAE61A55EABDEFD42DC7A5D8EB08D10* get_aAdjacentPolygons_6() const { return ___aAdjacentPolygons_6; }
	inline AdjacentPolygonU5BU5D_tE9B43C2AADAE61A55EABDEFD42DC7A5D8EB08D10** get_address_of_aAdjacentPolygons_6() { return &___aAdjacentPolygons_6; }
	inline void set_aAdjacentPolygons_6(AdjacentPolygonU5BU5D_tE9B43C2AADAE61A55EABDEFD42DC7A5D8EB08D10* value)
	{
		___aAdjacentPolygons_6 = value;
		Il2CppCodeGenWriteBarrier((&___aAdjacentPolygons_6), value);
	}

	inline static int32_t get_offset_of_AdjacentNormals_7() { return static_cast<int32_t>(offsetof(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D, ___AdjacentNormals_7)); }
	inline List_1_t2DA726067932CE665A45F5079B4C94BC61B0201D * get_AdjacentNormals_7() const { return ___AdjacentNormals_7; }
	inline List_1_t2DA726067932CE665A45F5079B4C94BC61B0201D ** get_address_of_AdjacentNormals_7() { return &___AdjacentNormals_7; }
	inline void set_AdjacentNormals_7(List_1_t2DA726067932CE665A45F5079B4C94BC61B0201D * value)
	{
		___AdjacentNormals_7 = value;
		Il2CppCodeGenWriteBarrier((&___AdjacentNormals_7), value);
	}

	inline static int32_t get_offset_of_Cavity_8() { return static_cast<int32_t>(offsetof(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D, ___Cavity_8)); }
	inline float get_Cavity_8() const { return ___Cavity_8; }
	inline float* get_address_of_Cavity_8() { return &___Cavity_8; }
	inline void set_Cavity_8(float value)
	{
		___Cavity_8 = value;
	}

	inline static int32_t get_offset_of_AdjacentMult_9() { return static_cast<int32_t>(offsetof(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D, ___AdjacentMult_9)); }
	inline float get_AdjacentMult_9() const { return ___AdjacentMult_9; }
	inline float* get_address_of_AdjacentMult_9() { return &___AdjacentMult_9; }
	inline void set_AdjacentMult_9(float value)
	{
		___AdjacentMult_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALVERTEX_T82CB2897B045FD15284797267376441CA5ADAE4D_H
#ifndef NORMALSRECALCULATIONMODEENUM_T0F9CC811A0AD33784B9A02EA5BB55CD8DC533A41_H
#define NORMALSRECALCULATIONMODEENUM_T0F9CC811A0AD33784B9A02EA5BB55CD8DC533A41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_NormalsRecalculationModeEnum
struct  NormalsRecalculationModeEnum_t0F9CC811A0AD33784B9A02EA5BB55CD8DC533A41 
{
public:
	// System.Int32 VertexAnimationTools_30.ObjData_NormalsRecalculationModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NormalsRecalculationModeEnum_t0F9CC811A0AD33784B9A02EA5BB55CD8DC533A41, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALSRECALCULATIONMODEENUM_T0F9CC811A0AD33784B9A02EA5BB55CD8DC533A41_H
#ifndef OBJLINEIDENUM_T7150015CDFE28B7C65B4E06AF07BAD11EC170370_H
#define OBJLINEIDENUM_T7150015CDFE28B7C65B4E06AF07BAD11EC170370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_ObjLineIdEnum
struct  ObjLineIdEnum_t7150015CDFE28B7C65B4E06AF07BAD11EC170370 
{
public:
	// System.Int32 VertexAnimationTools_30.ObjData_ObjLineIdEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjLineIdEnum_t7150015CDFE28B7C65B4E06AF07BAD11EC170370, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJLINEIDENUM_T7150015CDFE28B7C65B4E06AF07BAD11EC170370_H
#ifndef POLYGON_T5AD583D973314A72EACF4F8794C2471F93EDE8B2_H
#define POLYGON_T5AD583D973314A72EACF4F8794C2471F93EDE8B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_Polygon
struct  Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.ObjData_Polygon::SmoothingGroup
	int32_t ___SmoothingGroup_0;
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_Polygon::Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Normal_1;
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_Polygon::Tangent
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Tangent_2;
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_Polygon::Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Center_3;
	// System.Single VertexAnimationTools_30.ObjData_Polygon::Area
	float ___Area_4;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_UMeshVertex> VertexAnimationTools_30.ObjData_Polygon::corners
	List_1_t242FB347922E119315CCCCF165827A41F1EF41DA * ___corners_5;
	// System.Single VertexAnimationTools_30.ObjData_Polygon::AO
	float ___AO_6;
	// System.Single VertexAnimationTools_30.ObjData_Polygon::InnerAO
	float ___InnerAO_7;
	// System.Single VertexAnimationTools_30.ObjData_Polygon::Cavity
	float ___Cavity_8;
	// System.Single VertexAnimationTools_30.ObjData_Polygon::FacesMult
	float ___FacesMult_9;
	// System.Single VertexAnimationTools_30.ObjData_Polygon::CornersMult
	float ___CornersMult_10;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_Face> VertexAnimationTools_30.ObjData_Polygon::Faces
	List_1_t66E79D23D7ADC2EF0FD31EFD917D27BCEDB30B9A * ___Faces_11;

public:
	inline static int32_t get_offset_of_SmoothingGroup_0() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___SmoothingGroup_0)); }
	inline int32_t get_SmoothingGroup_0() const { return ___SmoothingGroup_0; }
	inline int32_t* get_address_of_SmoothingGroup_0() { return &___SmoothingGroup_0; }
	inline void set_SmoothingGroup_0(int32_t value)
	{
		___SmoothingGroup_0 = value;
	}

	inline static int32_t get_offset_of_Normal_1() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Normal_1() const { return ___Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Normal_1() { return &___Normal_1; }
	inline void set_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Normal_1 = value;
	}

	inline static int32_t get_offset_of_Tangent_2() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___Tangent_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Tangent_2() const { return ___Tangent_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Tangent_2() { return &___Tangent_2; }
	inline void set_Tangent_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Tangent_2 = value;
	}

	inline static int32_t get_offset_of_Center_3() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___Center_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Center_3() const { return ___Center_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Center_3() { return &___Center_3; }
	inline void set_Center_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Center_3 = value;
	}

	inline static int32_t get_offset_of_Area_4() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___Area_4)); }
	inline float get_Area_4() const { return ___Area_4; }
	inline float* get_address_of_Area_4() { return &___Area_4; }
	inline void set_Area_4(float value)
	{
		___Area_4 = value;
	}

	inline static int32_t get_offset_of_corners_5() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___corners_5)); }
	inline List_1_t242FB347922E119315CCCCF165827A41F1EF41DA * get_corners_5() const { return ___corners_5; }
	inline List_1_t242FB347922E119315CCCCF165827A41F1EF41DA ** get_address_of_corners_5() { return &___corners_5; }
	inline void set_corners_5(List_1_t242FB347922E119315CCCCF165827A41F1EF41DA * value)
	{
		___corners_5 = value;
		Il2CppCodeGenWriteBarrier((&___corners_5), value);
	}

	inline static int32_t get_offset_of_AO_6() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___AO_6)); }
	inline float get_AO_6() const { return ___AO_6; }
	inline float* get_address_of_AO_6() { return &___AO_6; }
	inline void set_AO_6(float value)
	{
		___AO_6 = value;
	}

	inline static int32_t get_offset_of_InnerAO_7() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___InnerAO_7)); }
	inline float get_InnerAO_7() const { return ___InnerAO_7; }
	inline float* get_address_of_InnerAO_7() { return &___InnerAO_7; }
	inline void set_InnerAO_7(float value)
	{
		___InnerAO_7 = value;
	}

	inline static int32_t get_offset_of_Cavity_8() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___Cavity_8)); }
	inline float get_Cavity_8() const { return ___Cavity_8; }
	inline float* get_address_of_Cavity_8() { return &___Cavity_8; }
	inline void set_Cavity_8(float value)
	{
		___Cavity_8 = value;
	}

	inline static int32_t get_offset_of_FacesMult_9() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___FacesMult_9)); }
	inline float get_FacesMult_9() const { return ___FacesMult_9; }
	inline float* get_address_of_FacesMult_9() { return &___FacesMult_9; }
	inline void set_FacesMult_9(float value)
	{
		___FacesMult_9 = value;
	}

	inline static int32_t get_offset_of_CornersMult_10() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___CornersMult_10)); }
	inline float get_CornersMult_10() const { return ___CornersMult_10; }
	inline float* get_address_of_CornersMult_10() { return &___CornersMult_10; }
	inline void set_CornersMult_10(float value)
	{
		___CornersMult_10 = value;
	}

	inline static int32_t get_offset_of_Faces_11() { return static_cast<int32_t>(offsetof(Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2, ___Faces_11)); }
	inline List_1_t66E79D23D7ADC2EF0FD31EFD917D27BCEDB30B9A * get_Faces_11() const { return ___Faces_11; }
	inline List_1_t66E79D23D7ADC2EF0FD31EFD917D27BCEDB30B9A ** get_address_of_Faces_11() { return &___Faces_11; }
	inline void set_Faces_11(List_1_t66E79D23D7ADC2EF0FD31EFD917D27BCEDB30B9A * value)
	{
		___Faces_11 = value;
		Il2CppCodeGenWriteBarrier((&___Faces_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGON_T5AD583D973314A72EACF4F8794C2471F93EDE8B2_H
#ifndef CORNER_TB30359CF24D1E97BB2B11F3700FB1CC8768F4821_H
#define CORNER_TB30359CF24D1E97BB2B11F3700FB1CC8768F4821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_PolygonTriangulator_Corner
struct  Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.ObjData_PolygonTriangulator_Corner::SourceIdx
	int32_t ___SourceIdx_0;
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_PolygonTriangulator_Corner::WorldPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___WorldPoint_1;
	// UnityEngine.Vector2 VertexAnimationTools_30.ObjData_PolygonTriangulator_Corner::PolygonSpacePoint
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___PolygonSpacePoint_2;
	// System.Single VertexAnimationTools_30.ObjData_PolygonTriangulator_Corner::Angle
	float ___Angle_3;

public:
	inline static int32_t get_offset_of_SourceIdx_0() { return static_cast<int32_t>(offsetof(Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821, ___SourceIdx_0)); }
	inline int32_t get_SourceIdx_0() const { return ___SourceIdx_0; }
	inline int32_t* get_address_of_SourceIdx_0() { return &___SourceIdx_0; }
	inline void set_SourceIdx_0(int32_t value)
	{
		___SourceIdx_0 = value;
	}

	inline static int32_t get_offset_of_WorldPoint_1() { return static_cast<int32_t>(offsetof(Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821, ___WorldPoint_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_WorldPoint_1() const { return ___WorldPoint_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_WorldPoint_1() { return &___WorldPoint_1; }
	inline void set_WorldPoint_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___WorldPoint_1 = value;
	}

	inline static int32_t get_offset_of_PolygonSpacePoint_2() { return static_cast<int32_t>(offsetof(Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821, ___PolygonSpacePoint_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_PolygonSpacePoint_2() const { return ___PolygonSpacePoint_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_PolygonSpacePoint_2() { return &___PolygonSpacePoint_2; }
	inline void set_PolygonSpacePoint_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___PolygonSpacePoint_2 = value;
	}

	inline static int32_t get_offset_of_Angle_3() { return static_cast<int32_t>(offsetof(Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821, ___Angle_3)); }
	inline float get_Angle_3() const { return ___Angle_3; }
	inline float* get_address_of_Angle_3() { return &___Angle_3; }
	inline void set_Angle_3(float value)
	{
		___Angle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_TB30359CF24D1E97BB2B11F3700FB1CC8768F4821_H
#ifndef POSITIONVERTEX_T734DA4E056F2DEFDA0D24559A8BEFB2D75837186_H
#define POSITIONVERTEX_T734DA4E056F2DEFDA0D24559A8BEFB2D75837186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_PositionVertex
struct  PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_PositionVertex::BindPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BindPos_0;
	// UnityEngine.Vector3 VertexAnimationTools_30.ObjData_PositionVertex::Value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Value_1;
	// System.Collections.Generic.HashSet`1<VertexAnimationTools_30.ObjData_AdjacentPolygon> VertexAnimationTools_30.ObjData_PositionVertex::AdjacentPolygons
	HashSet_1_t9CE364960B7559E086A7BC4684E47F2C76D38A61 * ___AdjacentPolygons_2;
	// System.Boolean VertexAnimationTools_30.ObjData_PositionVertex::IsInner
	bool ___IsInner_3;
	// System.Single VertexAnimationTools_30.ObjData_PositionVertex::InnerAO
	float ___InnerAO_4;

public:
	inline static int32_t get_offset_of_BindPos_0() { return static_cast<int32_t>(offsetof(PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186, ___BindPos_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_BindPos_0() const { return ___BindPos_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_BindPos_0() { return &___BindPos_0; }
	inline void set_BindPos_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___BindPos_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186, ___Value_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Value_1() const { return ___Value_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Value_1 = value;
	}

	inline static int32_t get_offset_of_AdjacentPolygons_2() { return static_cast<int32_t>(offsetof(PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186, ___AdjacentPolygons_2)); }
	inline HashSet_1_t9CE364960B7559E086A7BC4684E47F2C76D38A61 * get_AdjacentPolygons_2() const { return ___AdjacentPolygons_2; }
	inline HashSet_1_t9CE364960B7559E086A7BC4684E47F2C76D38A61 ** get_address_of_AdjacentPolygons_2() { return &___AdjacentPolygons_2; }
	inline void set_AdjacentPolygons_2(HashSet_1_t9CE364960B7559E086A7BC4684E47F2C76D38A61 * value)
	{
		___AdjacentPolygons_2 = value;
		Il2CppCodeGenWriteBarrier((&___AdjacentPolygons_2), value);
	}

	inline static int32_t get_offset_of_IsInner_3() { return static_cast<int32_t>(offsetof(PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186, ___IsInner_3)); }
	inline bool get_IsInner_3() const { return ___IsInner_3; }
	inline bool* get_address_of_IsInner_3() { return &___IsInner_3; }
	inline void set_IsInner_3(bool value)
	{
		___IsInner_3 = value;
	}

	inline static int32_t get_offset_of_InnerAO_4() { return static_cast<int32_t>(offsetof(PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186, ___InnerAO_4)); }
	inline float get_InnerAO_4() const { return ___InnerAO_4; }
	inline float* get_address_of_InnerAO_4() { return &___InnerAO_4; }
	inline void set_InnerAO_4(float value)
	{
		___InnerAO_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONVERTEX_T734DA4E056F2DEFDA0D24559A8BEFB2D75837186_H
#ifndef SMOOTHINGGROUPIMPORTMODEENUM_T8ED4A6B144003B5D3F74060965BB1B38FA190261_H
#define SMOOTHINGGROUPIMPORTMODEENUM_T8ED4A6B144003B5D3F74060965BB1B38FA190261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_SmoothingGroupImportModeEnum
struct  SmoothingGroupImportModeEnum_t8ED4A6B144003B5D3F74060965BB1B38FA190261 
{
public:
	// System.Int32 VertexAnimationTools_30.ObjData_SmoothingGroupImportModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SmoothingGroupImportModeEnum_t8ED4A6B144003B5D3F74060965BB1B38FA190261, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHINGGROUPIMPORTMODEENUM_T8ED4A6B144003B5D3F74060965BB1B38FA190261_H
#ifndef TRIANGLE2D_T62295746A1A9009D2518E16CBEEDDEE489C6BA8D_H
#define TRIANGLE2D_T62295746A1A9009D2518E16CBEEDDEE489C6BA8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_Triangle2d
struct  Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D 
{
public:
	// UnityEngine.Vector2 VertexAnimationTools_30.ObjData_Triangle2d::a
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a_0;
	// UnityEngine.Vector2 VertexAnimationTools_30.ObjData_Triangle2d::b
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b_1;
	// UnityEngine.Vector2 VertexAnimationTools_30.ObjData_Triangle2d::c
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___c_2;
	// UnityEngine.Vector2 VertexAnimationTools_30.ObjData_Triangle2d::v0
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v0_3;
	// UnityEngine.Vector2 VertexAnimationTools_30.ObjData_Triangle2d::v1
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v1_4;
	// System.Single VertexAnimationTools_30.ObjData_Triangle2d::dot00
	float ___dot00_5;
	// System.Single VertexAnimationTools_30.ObjData_Triangle2d::dot01
	float ___dot01_6;
	// System.Single VertexAnimationTools_30.ObjData_Triangle2d::dot11
	float ___dot11_7;
	// System.Single VertexAnimationTools_30.ObjData_Triangle2d::invDenom
	float ___invDenom_8;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D, ___a_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_a_0() const { return ___a_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D, ___b_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_b_1() const { return ___b_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___b_1 = value;
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D, ___c_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_c_2() const { return ___c_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_v0_3() { return static_cast<int32_t>(offsetof(Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D, ___v0_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_v0_3() const { return ___v0_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_v0_3() { return &___v0_3; }
	inline void set_v0_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___v0_3 = value;
	}

	inline static int32_t get_offset_of_v1_4() { return static_cast<int32_t>(offsetof(Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D, ___v1_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_v1_4() const { return ___v1_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_v1_4() { return &___v1_4; }
	inline void set_v1_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___v1_4 = value;
	}

	inline static int32_t get_offset_of_dot00_5() { return static_cast<int32_t>(offsetof(Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D, ___dot00_5)); }
	inline float get_dot00_5() const { return ___dot00_5; }
	inline float* get_address_of_dot00_5() { return &___dot00_5; }
	inline void set_dot00_5(float value)
	{
		___dot00_5 = value;
	}

	inline static int32_t get_offset_of_dot01_6() { return static_cast<int32_t>(offsetof(Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D, ___dot01_6)); }
	inline float get_dot01_6() const { return ___dot01_6; }
	inline float* get_address_of_dot01_6() { return &___dot01_6; }
	inline void set_dot01_6(float value)
	{
		___dot01_6 = value;
	}

	inline static int32_t get_offset_of_dot11_7() { return static_cast<int32_t>(offsetof(Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D, ___dot11_7)); }
	inline float get_dot11_7() const { return ___dot11_7; }
	inline float* get_address_of_dot11_7() { return &___dot11_7; }
	inline void set_dot11_7(float value)
	{
		___dot11_7 = value;
	}

	inline static int32_t get_offset_of_invDenom_8() { return static_cast<int32_t>(offsetof(Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D, ___invDenom_8)); }
	inline float get_invDenom_8() const { return ___invDenom_8; }
	inline float* get_address_of_invDenom_8() { return &___invDenom_8; }
	inline void set_invDenom_8(float value)
	{
		___invDenom_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGLE2D_T62295746A1A9009D2518E16CBEEDDEE489C6BA8D_H
#ifndef PFU_TD897B0E8199F5D54F4DBE9BFA91500B564EDB97D_H
#define PFU_TD897B0E8199F5D54F4DBE9BFA91500B564EDB97D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PFU
struct  PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D 
{
public:
	// UnityEngine.Vector3 VertexAnimationTools_30.PFU::P
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___P_0;
	// UnityEngine.Vector3 VertexAnimationTools_30.PFU::F
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___F_1;
	// UnityEngine.Vector3 VertexAnimationTools_30.PFU::U
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D, ___P_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_P_0() const { return ___P_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___P_0 = value;
	}

	inline static int32_t get_offset_of_F_1() { return static_cast<int32_t>(offsetof(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D, ___F_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_F_1() const { return ___F_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_F_1() { return &___F_1; }
	inline void set_F_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___F_1 = value;
	}

	inline static int32_t get_offset_of_U_2() { return static_cast<int32_t>(offsetof(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D, ___U_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U_2() const { return ___U_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U_2() { return &___U_2; }
	inline void set_U_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PFU_TD897B0E8199F5D54F4DBE9BFA91500B564EDB97D_H
#ifndef PLAYERUPDATEMODE_T69E6A34DE274376292987D8D567B1E7B30220293_H
#define PLAYERUPDATEMODE_T69E6A34DE274376292987D8D567B1E7B30220293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PlayerUpdateMode
struct  PlayerUpdateMode_t69E6A34DE274376292987D8D567B1E7B30220293 
{
public:
	// System.Int32 VertexAnimationTools_30.PlayerUpdateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlayerUpdateMode_t69E6A34DE274376292987D8D567B1E7B30220293, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERUPDATEMODE_T69E6A34DE274376292987D8D567B1E7B30220293_H
#ifndef PROJECTIONQUALITYENUM_T3EF48B93088C7D81A7804EF2F56E48CBB8C4564F_H
#define PROJECTIONQUALITYENUM_T3EF48B93088C7D81A7804EF2F56E48CBB8C4564F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ProjectionQualityEnum
struct  ProjectionQualityEnum_t3EF48B93088C7D81A7804EF2F56E48CBB8C4564F 
{
public:
	// System.Int32 VertexAnimationTools_30.ProjectionQualityEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProjectionQualityEnum_t3EF48B93088C7D81A7804EF2F56E48CBB8C4564F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONQUALITYENUM_T3EF48B93088C7D81A7804EF2F56E48CBB8C4564F_H
#ifndef TRANSITIONMODEENUM_TB044BF8C2E6D830051A88CC905F5E8BF910ED946_H
#define TRANSITIONMODEENUM_TB044BF8C2E6D830051A88CC905F5E8BF910ED946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.TransitionModeEnum
struct  TransitionModeEnum_tB044BF8C2E6D830051A88CC905F5E8BF910ED946 
{
public:
	// System.Int32 VertexAnimationTools_30.TransitionModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransitionModeEnum_tB044BF8C2E6D830051A88CC905F5E8BF910ED946, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITIONMODEENUM_TB044BF8C2E6D830051A88CC905F5E8BF910ED946_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef MOBILEINPUT_T219DDF915FF54DBE73004E0163F8BFCB4DA3BA44_H
#define MOBILEINPUT_T219DDF915FF54DBE73004E0163F8BFCB4DA3BA44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct  MobileInput_t219DDF915FF54DBE73004E0163F8BFCB4DA3BA44  : public VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T219DDF915FF54DBE73004E0163F8BFCB4DA3BA44_H
#ifndef STANDALONEINPUT_T9BFD53669697FC88D0D237110D8D1DF76400AFEB_H
#define STANDALONEINPUT_T9BFD53669697FC88D0D237110D8D1DF76400AFEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct  StandaloneInput_t9BFD53669697FC88D0D237110D8D1DF76400AFEB  : public VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUT_T9BFD53669697FC88D0D237110D8D1DF76400AFEB_H
#ifndef AXISMAPPING_T9B9CB140F322262AC1E126846B4BD1C92586923E_H
#define AXISMAPPING_T9B9CB140F322262AC1E126846B4BD1C92586923E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput_AxisMapping
struct  AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E  : public RuntimeObject
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput_AxisMapping_MappingType UnityStandardAssets.CrossPlatformInput.TiltInput_AxisMapping::type
	int32_t ___type_0;
	// System.String UnityStandardAssets.CrossPlatformInput.TiltInput_AxisMapping::axisName
	String_t* ___axisName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_axisName_1() { return static_cast<int32_t>(offsetof(AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E, ___axisName_1)); }
	inline String_t* get_axisName_1() const { return ___axisName_1; }
	inline String_t** get_address_of_axisName_1() { return &___axisName_1; }
	inline void set_axisName_1(String_t* value)
	{
		___axisName_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISMAPPING_T9B9CB140F322262AC1E126846B4BD1C92586923E_H
#ifndef BINDINFO_T75FBC726121A66B131BD615CEF61C9064A13AFA8_H
#define BINDINFO_T75FBC726121A66B131BD615CEF61C9064A13AFA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.BindInfo
struct  BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8 
{
public:
	// System.Int32 VertexAnimationTools_30.BindInfo::VidxA
	int32_t ___VidxA_0;
	// System.Int32 VertexAnimationTools_30.BindInfo::VidxB
	int32_t ___VidxB_1;
	// System.Int32 VertexAnimationTools_30.BindInfo::VidxC
	int32_t ___VidxC_2;
	// UnityEngine.Vector3 VertexAnimationTools_30.BindInfo::Bary
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Bary_3;
	// VertexAnimationTools_30.PFU VertexAnimationTools_30.BindInfo::TrisSpace
	PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  ___TrisSpace_4;

public:
	inline static int32_t get_offset_of_VidxA_0() { return static_cast<int32_t>(offsetof(BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8, ___VidxA_0)); }
	inline int32_t get_VidxA_0() const { return ___VidxA_0; }
	inline int32_t* get_address_of_VidxA_0() { return &___VidxA_0; }
	inline void set_VidxA_0(int32_t value)
	{
		___VidxA_0 = value;
	}

	inline static int32_t get_offset_of_VidxB_1() { return static_cast<int32_t>(offsetof(BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8, ___VidxB_1)); }
	inline int32_t get_VidxB_1() const { return ___VidxB_1; }
	inline int32_t* get_address_of_VidxB_1() { return &___VidxB_1; }
	inline void set_VidxB_1(int32_t value)
	{
		___VidxB_1 = value;
	}

	inline static int32_t get_offset_of_VidxC_2() { return static_cast<int32_t>(offsetof(BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8, ___VidxC_2)); }
	inline int32_t get_VidxC_2() const { return ___VidxC_2; }
	inline int32_t* get_address_of_VidxC_2() { return &___VidxC_2; }
	inline void set_VidxC_2(int32_t value)
	{
		___VidxC_2 = value;
	}

	inline static int32_t get_offset_of_Bary_3() { return static_cast<int32_t>(offsetof(BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8, ___Bary_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Bary_3() const { return ___Bary_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Bary_3() { return &___Bary_3; }
	inline void set_Bary_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Bary_3 = value;
	}

	inline static int32_t get_offset_of_TrisSpace_4() { return static_cast<int32_t>(offsetof(BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8, ___TrisSpace_4)); }
	inline PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  get_TrisSpace_4() const { return ___TrisSpace_4; }
	inline PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D * get_address_of_TrisSpace_4() { return &___TrisSpace_4; }
	inline void set_TrisSpace_4(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  value)
	{
		___TrisSpace_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINFO_T75FBC726121A66B131BD615CEF61C9064A13AFA8_H
#ifndef FRAMESARRAY_T3FA26E290063A6BFC3BE95E002191F09335FC4E8_H
#define FRAMESARRAY_T3FA26E290063A6BFC3BE95E002191F09335FC4E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.FramesArray
struct  FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3[]> VertexAnimationTools_30.FramesArray::frames
	List_1_t7056E0D562E44CC218CC39274D1901F278A5E258 * ___frames_0;
	// System.Boolean VertexAnimationTools_30.FramesArray::IsLoop
	bool ___IsLoop_1;
	// VertexAnimationTools_30.InterpolateModeEnum VertexAnimationTools_30.FramesArray::Interpolation
	int32_t ___Interpolation_2;
	// System.Single VertexAnimationTools_30.FramesArray::b1
	float ___b1_3;
	// System.Single VertexAnimationTools_30.FramesArray::b2
	float ___b2_4;
	// System.Single VertexAnimationTools_30.FramesArray::b3
	float ___b3_5;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8, ___frames_0)); }
	inline List_1_t7056E0D562E44CC218CC39274D1901F278A5E258 * get_frames_0() const { return ___frames_0; }
	inline List_1_t7056E0D562E44CC218CC39274D1901F278A5E258 ** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(List_1_t7056E0D562E44CC218CC39274D1901F278A5E258 * value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}

	inline static int32_t get_offset_of_IsLoop_1() { return static_cast<int32_t>(offsetof(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8, ___IsLoop_1)); }
	inline bool get_IsLoop_1() const { return ___IsLoop_1; }
	inline bool* get_address_of_IsLoop_1() { return &___IsLoop_1; }
	inline void set_IsLoop_1(bool value)
	{
		___IsLoop_1 = value;
	}

	inline static int32_t get_offset_of_Interpolation_2() { return static_cast<int32_t>(offsetof(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8, ___Interpolation_2)); }
	inline int32_t get_Interpolation_2() const { return ___Interpolation_2; }
	inline int32_t* get_address_of_Interpolation_2() { return &___Interpolation_2; }
	inline void set_Interpolation_2(int32_t value)
	{
		___Interpolation_2 = value;
	}

	inline static int32_t get_offset_of_b1_3() { return static_cast<int32_t>(offsetof(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8, ___b1_3)); }
	inline float get_b1_3() const { return ___b1_3; }
	inline float* get_address_of_b1_3() { return &___b1_3; }
	inline void set_b1_3(float value)
	{
		___b1_3 = value;
	}

	inline static int32_t get_offset_of_b2_4() { return static_cast<int32_t>(offsetof(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8, ___b2_4)); }
	inline float get_b2_4() const { return ___b2_4; }
	inline float* get_address_of_b2_4() { return &___b2_4; }
	inline void set_b2_4(float value)
	{
		___b2_4 = value;
	}

	inline static int32_t get_offset_of_b3_5() { return static_cast<int32_t>(offsetof(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8, ___b3_5)); }
	inline float get_b3_5() const { return ___b3_5; }
	inline float* get_address_of_b3_5() { return &___b3_5; }
	inline void set_b3_5(float value)
	{
		___b3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMESARRAY_T3FA26E290063A6BFC3BE95E002191F09335FC4E8_H
#ifndef MESHSEQUENCEINFO_TE6A3120CE71A5B79CC9C74CBE459B45F0279342C_H
#define MESHSEQUENCEINFO_TE6A3120CE71A5B79CC9C74CBE459B45F0279342C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MeshSequenceInfo
struct  MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C 
{
public:
	// VertexAnimationTools_30.MeshSequenceInfo_StateEnum VertexAnimationTools_30.MeshSequenceInfo::State
	int32_t ___State_0;
	// VertexAnimationTools_30.MeshSequenceInfo_ObjFileInfo[] VertexAnimationTools_30.MeshSequenceInfo::infos
	ObjFileInfoU5BU5D_tC51AD987D3261B2DCBB15872D8087CE87EBDBB75* ___infos_1;
	// System.String VertexAnimationTools_30.MeshSequenceInfo::SequenceName
	String_t* ___SequenceName_2;
	// System.String VertexAnimationTools_30.MeshSequenceInfo::DirectoryPath
	String_t* ___DirectoryPath_3;
	// VertexAnimationTools_30.MeshSequenceInfo_SortModeEnum VertexAnimationTools_30.MeshSequenceInfo::SortMode
	int32_t ___SortMode_4;

public:
	inline static int32_t get_offset_of_State_0() { return static_cast<int32_t>(offsetof(MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C, ___State_0)); }
	inline int32_t get_State_0() const { return ___State_0; }
	inline int32_t* get_address_of_State_0() { return &___State_0; }
	inline void set_State_0(int32_t value)
	{
		___State_0 = value;
	}

	inline static int32_t get_offset_of_infos_1() { return static_cast<int32_t>(offsetof(MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C, ___infos_1)); }
	inline ObjFileInfoU5BU5D_tC51AD987D3261B2DCBB15872D8087CE87EBDBB75* get_infos_1() const { return ___infos_1; }
	inline ObjFileInfoU5BU5D_tC51AD987D3261B2DCBB15872D8087CE87EBDBB75** get_address_of_infos_1() { return &___infos_1; }
	inline void set_infos_1(ObjFileInfoU5BU5D_tC51AD987D3261B2DCBB15872D8087CE87EBDBB75* value)
	{
		___infos_1 = value;
		Il2CppCodeGenWriteBarrier((&___infos_1), value);
	}

	inline static int32_t get_offset_of_SequenceName_2() { return static_cast<int32_t>(offsetof(MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C, ___SequenceName_2)); }
	inline String_t* get_SequenceName_2() const { return ___SequenceName_2; }
	inline String_t** get_address_of_SequenceName_2() { return &___SequenceName_2; }
	inline void set_SequenceName_2(String_t* value)
	{
		___SequenceName_2 = value;
		Il2CppCodeGenWriteBarrier((&___SequenceName_2), value);
	}

	inline static int32_t get_offset_of_DirectoryPath_3() { return static_cast<int32_t>(offsetof(MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C, ___DirectoryPath_3)); }
	inline String_t* get_DirectoryPath_3() const { return ___DirectoryPath_3; }
	inline String_t** get_address_of_DirectoryPath_3() { return &___DirectoryPath_3; }
	inline void set_DirectoryPath_3(String_t* value)
	{
		___DirectoryPath_3 = value;
		Il2CppCodeGenWriteBarrier((&___DirectoryPath_3), value);
	}

	inline static int32_t get_offset_of_SortMode_4() { return static_cast<int32_t>(offsetof(MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C, ___SortMode_4)); }
	inline int32_t get_SortMode_4() const { return ___SortMode_4; }
	inline int32_t* get_address_of_SortMode_4() { return &___SortMode_4; }
	inline void set_SortMode_4(int32_t value)
	{
		___SortMode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VertexAnimationTools_30.MeshSequenceInfo
struct MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C_marshaled_pinvoke
{
	int32_t ___State_0;
	ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846_marshaled_pinvoke* ___infos_1;
	char* ___SequenceName_2;
	char* ___DirectoryPath_3;
	int32_t ___SortMode_4;
};
// Native definition for COM marshalling of VertexAnimationTools_30.MeshSequenceInfo
struct MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C_marshaled_com
{
	int32_t ___State_0;
	ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846_marshaled_com* ___infos_1;
	Il2CppChar* ___SequenceName_2;
	Il2CppChar* ___DirectoryPath_3;
	int32_t ___SortMode_4;
};
#endif // MESHSEQUENCEINFO_TE6A3120CE71A5B79CC9C74CBE459B45F0279342C_H
#ifndef OBJDATA_TEFABA5323FBC9AA15B72ADA7B77993F69F66FA62_H
#define OBJDATA_TEFABA5323FBC9AA15B72ADA7B77993F69F66FA62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData
struct  ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.ObjData::Name
	String_t* ___Name_0;
	// System.Boolean VertexAnimationTools_30.ObjData::FlipNormals
	bool ___FlipNormals_1;
	// System.Boolean VertexAnimationTools_30.ObjData::CalculateNormals
	bool ___CalculateNormals_2;
	// System.Boolean VertexAnimationTools_30.ObjData::ImportUV
	bool ___ImportUV_3;
	// System.Boolean VertexAnimationTools_30.ObjData::CalculateTangents
	bool ___CalculateTangents_4;
	// VertexAnimationTools_30.ObjData_SmoothingGroupImportModeEnum VertexAnimationTools_30.ObjData::SmoothingGroupsMode
	int32_t ___SmoothingGroupsMode_5;
	// VertexAnimationTools_30.ObjData_NormalsRecalculationModeEnum VertexAnimationTools_30.ObjData::NormalsRecalculationMode
	int32_t ___NormalsRecalculationMode_6;
	// UnityEngine.Vector3[] VertexAnimationTools_30.ObjData::UM_vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___UM_vertices_7;
	// UnityEngine.Vector2[] VertexAnimationTools_30.ObjData::UM_uv
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___UM_uv_8;
	// UnityEngine.Vector3[] VertexAnimationTools_30.ObjData::UM_normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___UM_normals_9;
	// UnityEngine.Vector3[] VertexAnimationTools_30.ObjData::UM_v3tangents
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___UM_v3tangents_10;
	// UnityEngine.Color[] VertexAnimationTools_30.ObjData::UM_colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___UM_colors_11;
	// UnityEngine.Vector4[] VertexAnimationTools_30.ObjData::UM_v4tangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___UM_v4tangents_12;
	// UnityEngine.Bounds VertexAnimationTools_30.ObjData::UM_Bounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___UM_Bounds_13;
	// UnityEngine.Vector3[] VertexAnimationTools_30.ObjData::VerticesToSet
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___VerticesToSet_14;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_Face> VertexAnimationTools_30.ObjData::AllFaces
	List_1_t66E79D23D7ADC2EF0FD31EFD917D27BCEDB30B9A * ___AllFaces_15;
	// VertexAnimationTools_30.ObjData_SubMeshesList VertexAnimationTools_30.ObjData::SubMeshes
	SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA * ___SubMeshes_16;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_PositionVertex> VertexAnimationTools_30.ObjData::Vertices
	List_1_t61ADFC8A10A8EA2BDA49284119FB4E47D9F6694E * ___Vertices_17;
	// VertexAnimationTools_30.ObjData_NormalsList VertexAnimationTools_30.ObjData::Normals
	NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001 * ___Normals_18;
	// VertexAnimationTools_30.ObjData_MapVerticesList VertexAnimationTools_30.ObjData::mapVertices
	MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491 * ___mapVertices_19;
	// UnityEngine.Vector3[] VertexAnimationTools_30.ObjData::bindVerts
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___bindVerts_20;
	// UnityEngine.Vector3[] VertexAnimationTools_30.ObjData::bindNormals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___bindNormals_21;
	// UnityEngine.Vector3[] VertexAnimationTools_30.ObjData::bindTangents
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___bindTangents_22;
	// UnityEngine.Rendering.IndexFormat VertexAnimationTools_30.ObjData::IndexFormat
	int32_t ___IndexFormat_23;
	// VertexAnimationTools_30.ObjData_UMeshVerticesList VertexAnimationTools_30.ObjData::UnityVertices
	UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997 * ___UnityVertices_24;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.ObjData_Polygon> VertexAnimationTools_30.ObjData::AllPolygons
	List_1_tFE744926BDF6D484BE450071B854C1455039953A * ___AllPolygons_25;
	// UnityEngine.Mesh VertexAnimationTools_30.ObjData::refMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___refMesh_26;
	// System.String[] VertexAnimationTools_30.ObjData::subString
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___subString_27;
	// System.IO.TextReader VertexAnimationTools_30.ObjData::objFile
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___objFile_28;
	// System.Int32 VertexAnimationTools_30.ObjData::currentSmoothingGroup
	int32_t ___currentSmoothingGroup_29;
	// System.Boolean VertexAnimationTools_30.ObjData::offSGMode
	bool ___offSGMode_30;
	// System.Int32 VertexAnimationTools_30.ObjData::offSGcounter
	int32_t ___offSGcounter_31;
	// System.Char[] VertexAnimationTools_30.ObjData::spaceSeparator
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___spaceSeparator_32;
	// System.Single VertexAnimationTools_30.ObjData::objFileLength
	float ___objFileLength_33;
	// System.Diagnostics.Stopwatch VertexAnimationTools_30.ObjData::BuildSW
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ___BuildSW_34;
	// System.Diagnostics.Stopwatch VertexAnimationTools_30.ObjData::ApplySW
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ___ApplySW_35;
	// VertexAnimationTools_30.ObjData_FaceLineParser VertexAnimationTools_30.ObjData::fplp
	FaceLineParser_t3456F4CAF39256EA2ED2F481EA54AAD232B26FC7 * ___fplp_36;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_FlipNormals_1() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___FlipNormals_1)); }
	inline bool get_FlipNormals_1() const { return ___FlipNormals_1; }
	inline bool* get_address_of_FlipNormals_1() { return &___FlipNormals_1; }
	inline void set_FlipNormals_1(bool value)
	{
		___FlipNormals_1 = value;
	}

	inline static int32_t get_offset_of_CalculateNormals_2() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___CalculateNormals_2)); }
	inline bool get_CalculateNormals_2() const { return ___CalculateNormals_2; }
	inline bool* get_address_of_CalculateNormals_2() { return &___CalculateNormals_2; }
	inline void set_CalculateNormals_2(bool value)
	{
		___CalculateNormals_2 = value;
	}

	inline static int32_t get_offset_of_ImportUV_3() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___ImportUV_3)); }
	inline bool get_ImportUV_3() const { return ___ImportUV_3; }
	inline bool* get_address_of_ImportUV_3() { return &___ImportUV_3; }
	inline void set_ImportUV_3(bool value)
	{
		___ImportUV_3 = value;
	}

	inline static int32_t get_offset_of_CalculateTangents_4() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___CalculateTangents_4)); }
	inline bool get_CalculateTangents_4() const { return ___CalculateTangents_4; }
	inline bool* get_address_of_CalculateTangents_4() { return &___CalculateTangents_4; }
	inline void set_CalculateTangents_4(bool value)
	{
		___CalculateTangents_4 = value;
	}

	inline static int32_t get_offset_of_SmoothingGroupsMode_5() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___SmoothingGroupsMode_5)); }
	inline int32_t get_SmoothingGroupsMode_5() const { return ___SmoothingGroupsMode_5; }
	inline int32_t* get_address_of_SmoothingGroupsMode_5() { return &___SmoothingGroupsMode_5; }
	inline void set_SmoothingGroupsMode_5(int32_t value)
	{
		___SmoothingGroupsMode_5 = value;
	}

	inline static int32_t get_offset_of_NormalsRecalculationMode_6() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___NormalsRecalculationMode_6)); }
	inline int32_t get_NormalsRecalculationMode_6() const { return ___NormalsRecalculationMode_6; }
	inline int32_t* get_address_of_NormalsRecalculationMode_6() { return &___NormalsRecalculationMode_6; }
	inline void set_NormalsRecalculationMode_6(int32_t value)
	{
		___NormalsRecalculationMode_6 = value;
	}

	inline static int32_t get_offset_of_UM_vertices_7() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___UM_vertices_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_UM_vertices_7() const { return ___UM_vertices_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_UM_vertices_7() { return &___UM_vertices_7; }
	inline void set_UM_vertices_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___UM_vertices_7 = value;
		Il2CppCodeGenWriteBarrier((&___UM_vertices_7), value);
	}

	inline static int32_t get_offset_of_UM_uv_8() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___UM_uv_8)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_UM_uv_8() const { return ___UM_uv_8; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_UM_uv_8() { return &___UM_uv_8; }
	inline void set_UM_uv_8(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___UM_uv_8 = value;
		Il2CppCodeGenWriteBarrier((&___UM_uv_8), value);
	}

	inline static int32_t get_offset_of_UM_normals_9() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___UM_normals_9)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_UM_normals_9() const { return ___UM_normals_9; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_UM_normals_9() { return &___UM_normals_9; }
	inline void set_UM_normals_9(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___UM_normals_9 = value;
		Il2CppCodeGenWriteBarrier((&___UM_normals_9), value);
	}

	inline static int32_t get_offset_of_UM_v3tangents_10() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___UM_v3tangents_10)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_UM_v3tangents_10() const { return ___UM_v3tangents_10; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_UM_v3tangents_10() { return &___UM_v3tangents_10; }
	inline void set_UM_v3tangents_10(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___UM_v3tangents_10 = value;
		Il2CppCodeGenWriteBarrier((&___UM_v3tangents_10), value);
	}

	inline static int32_t get_offset_of_UM_colors_11() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___UM_colors_11)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_UM_colors_11() const { return ___UM_colors_11; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_UM_colors_11() { return &___UM_colors_11; }
	inline void set_UM_colors_11(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___UM_colors_11 = value;
		Il2CppCodeGenWriteBarrier((&___UM_colors_11), value);
	}

	inline static int32_t get_offset_of_UM_v4tangents_12() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___UM_v4tangents_12)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_UM_v4tangents_12() const { return ___UM_v4tangents_12; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_UM_v4tangents_12() { return &___UM_v4tangents_12; }
	inline void set_UM_v4tangents_12(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___UM_v4tangents_12 = value;
		Il2CppCodeGenWriteBarrier((&___UM_v4tangents_12), value);
	}

	inline static int32_t get_offset_of_UM_Bounds_13() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___UM_Bounds_13)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_UM_Bounds_13() const { return ___UM_Bounds_13; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_UM_Bounds_13() { return &___UM_Bounds_13; }
	inline void set_UM_Bounds_13(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___UM_Bounds_13 = value;
	}

	inline static int32_t get_offset_of_VerticesToSet_14() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___VerticesToSet_14)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_VerticesToSet_14() const { return ___VerticesToSet_14; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_VerticesToSet_14() { return &___VerticesToSet_14; }
	inline void set_VerticesToSet_14(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___VerticesToSet_14 = value;
		Il2CppCodeGenWriteBarrier((&___VerticesToSet_14), value);
	}

	inline static int32_t get_offset_of_AllFaces_15() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___AllFaces_15)); }
	inline List_1_t66E79D23D7ADC2EF0FD31EFD917D27BCEDB30B9A * get_AllFaces_15() const { return ___AllFaces_15; }
	inline List_1_t66E79D23D7ADC2EF0FD31EFD917D27BCEDB30B9A ** get_address_of_AllFaces_15() { return &___AllFaces_15; }
	inline void set_AllFaces_15(List_1_t66E79D23D7ADC2EF0FD31EFD917D27BCEDB30B9A * value)
	{
		___AllFaces_15 = value;
		Il2CppCodeGenWriteBarrier((&___AllFaces_15), value);
	}

	inline static int32_t get_offset_of_SubMeshes_16() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___SubMeshes_16)); }
	inline SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA * get_SubMeshes_16() const { return ___SubMeshes_16; }
	inline SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA ** get_address_of_SubMeshes_16() { return &___SubMeshes_16; }
	inline void set_SubMeshes_16(SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA * value)
	{
		___SubMeshes_16 = value;
		Il2CppCodeGenWriteBarrier((&___SubMeshes_16), value);
	}

	inline static int32_t get_offset_of_Vertices_17() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___Vertices_17)); }
	inline List_1_t61ADFC8A10A8EA2BDA49284119FB4E47D9F6694E * get_Vertices_17() const { return ___Vertices_17; }
	inline List_1_t61ADFC8A10A8EA2BDA49284119FB4E47D9F6694E ** get_address_of_Vertices_17() { return &___Vertices_17; }
	inline void set_Vertices_17(List_1_t61ADFC8A10A8EA2BDA49284119FB4E47D9F6694E * value)
	{
		___Vertices_17 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_17), value);
	}

	inline static int32_t get_offset_of_Normals_18() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___Normals_18)); }
	inline NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001 * get_Normals_18() const { return ___Normals_18; }
	inline NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001 ** get_address_of_Normals_18() { return &___Normals_18; }
	inline void set_Normals_18(NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001 * value)
	{
		___Normals_18 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_18), value);
	}

	inline static int32_t get_offset_of_mapVertices_19() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___mapVertices_19)); }
	inline MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491 * get_mapVertices_19() const { return ___mapVertices_19; }
	inline MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491 ** get_address_of_mapVertices_19() { return &___mapVertices_19; }
	inline void set_mapVertices_19(MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491 * value)
	{
		___mapVertices_19 = value;
		Il2CppCodeGenWriteBarrier((&___mapVertices_19), value);
	}

	inline static int32_t get_offset_of_bindVerts_20() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___bindVerts_20)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_bindVerts_20() const { return ___bindVerts_20; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_bindVerts_20() { return &___bindVerts_20; }
	inline void set_bindVerts_20(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___bindVerts_20 = value;
		Il2CppCodeGenWriteBarrier((&___bindVerts_20), value);
	}

	inline static int32_t get_offset_of_bindNormals_21() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___bindNormals_21)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_bindNormals_21() const { return ___bindNormals_21; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_bindNormals_21() { return &___bindNormals_21; }
	inline void set_bindNormals_21(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___bindNormals_21 = value;
		Il2CppCodeGenWriteBarrier((&___bindNormals_21), value);
	}

	inline static int32_t get_offset_of_bindTangents_22() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___bindTangents_22)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_bindTangents_22() const { return ___bindTangents_22; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_bindTangents_22() { return &___bindTangents_22; }
	inline void set_bindTangents_22(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___bindTangents_22 = value;
		Il2CppCodeGenWriteBarrier((&___bindTangents_22), value);
	}

	inline static int32_t get_offset_of_IndexFormat_23() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___IndexFormat_23)); }
	inline int32_t get_IndexFormat_23() const { return ___IndexFormat_23; }
	inline int32_t* get_address_of_IndexFormat_23() { return &___IndexFormat_23; }
	inline void set_IndexFormat_23(int32_t value)
	{
		___IndexFormat_23 = value;
	}

	inline static int32_t get_offset_of_UnityVertices_24() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___UnityVertices_24)); }
	inline UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997 * get_UnityVertices_24() const { return ___UnityVertices_24; }
	inline UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997 ** get_address_of_UnityVertices_24() { return &___UnityVertices_24; }
	inline void set_UnityVertices_24(UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997 * value)
	{
		___UnityVertices_24 = value;
		Il2CppCodeGenWriteBarrier((&___UnityVertices_24), value);
	}

	inline static int32_t get_offset_of_AllPolygons_25() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___AllPolygons_25)); }
	inline List_1_tFE744926BDF6D484BE450071B854C1455039953A * get_AllPolygons_25() const { return ___AllPolygons_25; }
	inline List_1_tFE744926BDF6D484BE450071B854C1455039953A ** get_address_of_AllPolygons_25() { return &___AllPolygons_25; }
	inline void set_AllPolygons_25(List_1_tFE744926BDF6D484BE450071B854C1455039953A * value)
	{
		___AllPolygons_25 = value;
		Il2CppCodeGenWriteBarrier((&___AllPolygons_25), value);
	}

	inline static int32_t get_offset_of_refMesh_26() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___refMesh_26)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_refMesh_26() const { return ___refMesh_26; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_refMesh_26() { return &___refMesh_26; }
	inline void set_refMesh_26(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___refMesh_26 = value;
		Il2CppCodeGenWriteBarrier((&___refMesh_26), value);
	}

	inline static int32_t get_offset_of_subString_27() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___subString_27)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_subString_27() const { return ___subString_27; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_subString_27() { return &___subString_27; }
	inline void set_subString_27(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___subString_27 = value;
		Il2CppCodeGenWriteBarrier((&___subString_27), value);
	}

	inline static int32_t get_offset_of_objFile_28() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___objFile_28)); }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * get_objFile_28() const { return ___objFile_28; }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A ** get_address_of_objFile_28() { return &___objFile_28; }
	inline void set_objFile_28(TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * value)
	{
		___objFile_28 = value;
		Il2CppCodeGenWriteBarrier((&___objFile_28), value);
	}

	inline static int32_t get_offset_of_currentSmoothingGroup_29() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___currentSmoothingGroup_29)); }
	inline int32_t get_currentSmoothingGroup_29() const { return ___currentSmoothingGroup_29; }
	inline int32_t* get_address_of_currentSmoothingGroup_29() { return &___currentSmoothingGroup_29; }
	inline void set_currentSmoothingGroup_29(int32_t value)
	{
		___currentSmoothingGroup_29 = value;
	}

	inline static int32_t get_offset_of_offSGMode_30() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___offSGMode_30)); }
	inline bool get_offSGMode_30() const { return ___offSGMode_30; }
	inline bool* get_address_of_offSGMode_30() { return &___offSGMode_30; }
	inline void set_offSGMode_30(bool value)
	{
		___offSGMode_30 = value;
	}

	inline static int32_t get_offset_of_offSGcounter_31() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___offSGcounter_31)); }
	inline int32_t get_offSGcounter_31() const { return ___offSGcounter_31; }
	inline int32_t* get_address_of_offSGcounter_31() { return &___offSGcounter_31; }
	inline void set_offSGcounter_31(int32_t value)
	{
		___offSGcounter_31 = value;
	}

	inline static int32_t get_offset_of_spaceSeparator_32() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___spaceSeparator_32)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_spaceSeparator_32() const { return ___spaceSeparator_32; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_spaceSeparator_32() { return &___spaceSeparator_32; }
	inline void set_spaceSeparator_32(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___spaceSeparator_32 = value;
		Il2CppCodeGenWriteBarrier((&___spaceSeparator_32), value);
	}

	inline static int32_t get_offset_of_objFileLength_33() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___objFileLength_33)); }
	inline float get_objFileLength_33() const { return ___objFileLength_33; }
	inline float* get_address_of_objFileLength_33() { return &___objFileLength_33; }
	inline void set_objFileLength_33(float value)
	{
		___objFileLength_33 = value;
	}

	inline static int32_t get_offset_of_BuildSW_34() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___BuildSW_34)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get_BuildSW_34() const { return ___BuildSW_34; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of_BuildSW_34() { return &___BuildSW_34; }
	inline void set_BuildSW_34(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		___BuildSW_34 = value;
		Il2CppCodeGenWriteBarrier((&___BuildSW_34), value);
	}

	inline static int32_t get_offset_of_ApplySW_35() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___ApplySW_35)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get_ApplySW_35() const { return ___ApplySW_35; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of_ApplySW_35() { return &___ApplySW_35; }
	inline void set_ApplySW_35(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		___ApplySW_35 = value;
		Il2CppCodeGenWriteBarrier((&___ApplySW_35), value);
	}

	inline static int32_t get_offset_of_fplp_36() { return static_cast<int32_t>(offsetof(ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62, ___fplp_36)); }
	inline FaceLineParser_t3456F4CAF39256EA2ED2F481EA54AAD232B26FC7 * get_fplp_36() const { return ___fplp_36; }
	inline FaceLineParser_t3456F4CAF39256EA2ED2F481EA54AAD232B26FC7 ** get_address_of_fplp_36() { return &___fplp_36; }
	inline void set_fplp_36(FaceLineParser_t3456F4CAF39256EA2ED2F481EA54AAD232B26FC7 * value)
	{
		___fplp_36 = value;
		Il2CppCodeGenWriteBarrier((&___fplp_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJDATA_TEFABA5323FBC9AA15B72ADA7B77993F69F66FA62_H
#ifndef U3CALLMAPVERTSU3ED__5_TC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E_H
#define U3CALLMAPVERTSU3ED__5_TC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_MapVerticesList_<AllMapVerts>d__5
struct  U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.ObjData_MapVerticesList_<AllMapVerts>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// VertexAnimationTools_30.ObjData_MapVertex VertexAnimationTools_30.ObjData_MapVerticesList_<AllMapVerts>d__5::<>2__current
	MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 * ___U3CU3E2__current_1;
	// System.Int32 VertexAnimationTools_30.ObjData_MapVerticesList_<AllMapVerts>d__5::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// VertexAnimationTools_30.ObjData_MapVerticesList VertexAnimationTools_30.ObjData_MapVerticesList_<AllMapVerts>d__5::<>4__this
	MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491 * ___U3CU3E4__this_3;
	// System.Int32 VertexAnimationTools_30.ObjData_MapVerticesList_<AllMapVerts>d__5::<i>5__2
	int32_t ___U3CiU3E5__2_4;
	// System.Collections.Generic.Dictionary`2_Enumerator<VertexAnimationTools_30.ObjData_PositionVertex,VertexAnimationTools_30.ObjData_MapVertex> VertexAnimationTools_30.ObjData_MapVerticesList_<AllMapVerts>d__5::<>7__wrap2
	Enumerator_t7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6  ___U3CU3E7__wrap2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E, ___U3CU3E2__current_1)); }
	inline MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E, ___U3CU3E4__this_3)); }
	inline MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E, ___U3CiU3E5__2_4)); }
	inline int32_t get_U3CiU3E5__2_4() const { return ___U3CiU3E5__2_4; }
	inline int32_t* get_address_of_U3CiU3E5__2_4() { return &___U3CiU3E5__2_4; }
	inline void set_U3CiU3E5__2_4(int32_t value)
	{
		___U3CiU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_5() { return static_cast<int32_t>(offsetof(U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E, ___U3CU3E7__wrap2_5)); }
	inline Enumerator_t7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6  get_U3CU3E7__wrap2_5() const { return ___U3CU3E7__wrap2_5; }
	inline Enumerator_t7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6 * get_address_of_U3CU3E7__wrap2_5() { return &___U3CU3E7__wrap2_5; }
	inline void set_U3CU3E7__wrap2_5(Enumerator_t7DD50C06CDF64CEA5F2A83A8BFF806024AE887A6  value)
	{
		___U3CU3E7__wrap2_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CALLMAPVERTSU3ED__5_TC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E_H
#ifndef VERTEXCOLORSSETTINGS_T53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B_H
#define VERTEXCOLORSSETTINGS_T53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_VertexColorsSettings
struct  VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B 
{
public:
	// UnityEngine.GameObject VertexAnimationTools_30.ObjData_VertexColorsSettings::go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_0;
	// VertexAnimationTools_30.ProjectionQualityEnum VertexAnimationTools_30.ObjData_VertexColorsSettings::quality
	int32_t ___quality_1;
	// System.Boolean VertexAnimationTools_30.ObjData_VertexColorsSettings::Cavity
	bool ___Cavity_2;
	// System.Single VertexAnimationTools_30.ObjData_VertexColorsSettings::CavityAmount
	float ___CavityAmount_3;
	// System.Single VertexAnimationTools_30.ObjData_VertexColorsSettings::CavityAngleMin
	float ___CavityAngleMin_4;
	// System.Single VertexAnimationTools_30.ObjData_VertexColorsSettings::CavityAngleMax
	float ___CavityAngleMax_5;
	// System.Single VertexAnimationTools_30.ObjData_VertexColorsSettings::CavityBlur
	float ___CavityBlur_6;
	// System.Int32 VertexAnimationTools_30.ObjData_VertexColorsSettings::CavityBlurIterations
	int32_t ___CavityBlurIterations_7;
	// System.Boolean VertexAnimationTools_30.ObjData_VertexColorsSettings::InnerVertexOcclusion
	bool ___InnerVertexOcclusion_8;
	// System.Single VertexAnimationTools_30.ObjData_VertexColorsSettings::InnerVertexOcclusionAmount
	float ___InnerVertexOcclusionAmount_9;
	// System.Single VertexAnimationTools_30.ObjData_VertexColorsSettings::InnerVertexOcclusionBlur
	float ___InnerVertexOcclusionBlur_10;
	// System.Int32 VertexAnimationTools_30.ObjData_VertexColorsSettings::InnerVertexOcclusionBlurIterations
	int32_t ___InnerVertexOcclusionBlurIterations_11;
	// System.Boolean VertexAnimationTools_30.ObjData_VertexColorsSettings::AmbientOcclusion
	bool ___AmbientOcclusion_12;
	// System.Single VertexAnimationTools_30.ObjData_VertexColorsSettings::AmbientOcclusionAmount
	float ___AmbientOcclusionAmount_13;
	// System.Single VertexAnimationTools_30.ObjData_VertexColorsSettings::AmbientOcclusionRadius
	float ___AmbientOcclusionRadius_14;
	// System.Single VertexAnimationTools_30.ObjData_VertexColorsSettings::AmbientOcclusionBlur
	float ___AmbientOcclusionBlur_15;
	// System.Int32 VertexAnimationTools_30.ObjData_VertexColorsSettings::AmbientOcclusionBlurIterations
	int32_t ___AmbientOcclusionBlurIterations_16;

public:
	inline static int32_t get_offset_of_go_0() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___go_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_go_0() const { return ___go_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_go_0() { return &___go_0; }
	inline void set_go_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___go_0 = value;
		Il2CppCodeGenWriteBarrier((&___go_0), value);
	}

	inline static int32_t get_offset_of_quality_1() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___quality_1)); }
	inline int32_t get_quality_1() const { return ___quality_1; }
	inline int32_t* get_address_of_quality_1() { return &___quality_1; }
	inline void set_quality_1(int32_t value)
	{
		___quality_1 = value;
	}

	inline static int32_t get_offset_of_Cavity_2() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___Cavity_2)); }
	inline bool get_Cavity_2() const { return ___Cavity_2; }
	inline bool* get_address_of_Cavity_2() { return &___Cavity_2; }
	inline void set_Cavity_2(bool value)
	{
		___Cavity_2 = value;
	}

	inline static int32_t get_offset_of_CavityAmount_3() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___CavityAmount_3)); }
	inline float get_CavityAmount_3() const { return ___CavityAmount_3; }
	inline float* get_address_of_CavityAmount_3() { return &___CavityAmount_3; }
	inline void set_CavityAmount_3(float value)
	{
		___CavityAmount_3 = value;
	}

	inline static int32_t get_offset_of_CavityAngleMin_4() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___CavityAngleMin_4)); }
	inline float get_CavityAngleMin_4() const { return ___CavityAngleMin_4; }
	inline float* get_address_of_CavityAngleMin_4() { return &___CavityAngleMin_4; }
	inline void set_CavityAngleMin_4(float value)
	{
		___CavityAngleMin_4 = value;
	}

	inline static int32_t get_offset_of_CavityAngleMax_5() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___CavityAngleMax_5)); }
	inline float get_CavityAngleMax_5() const { return ___CavityAngleMax_5; }
	inline float* get_address_of_CavityAngleMax_5() { return &___CavityAngleMax_5; }
	inline void set_CavityAngleMax_5(float value)
	{
		___CavityAngleMax_5 = value;
	}

	inline static int32_t get_offset_of_CavityBlur_6() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___CavityBlur_6)); }
	inline float get_CavityBlur_6() const { return ___CavityBlur_6; }
	inline float* get_address_of_CavityBlur_6() { return &___CavityBlur_6; }
	inline void set_CavityBlur_6(float value)
	{
		___CavityBlur_6 = value;
	}

	inline static int32_t get_offset_of_CavityBlurIterations_7() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___CavityBlurIterations_7)); }
	inline int32_t get_CavityBlurIterations_7() const { return ___CavityBlurIterations_7; }
	inline int32_t* get_address_of_CavityBlurIterations_7() { return &___CavityBlurIterations_7; }
	inline void set_CavityBlurIterations_7(int32_t value)
	{
		___CavityBlurIterations_7 = value;
	}

	inline static int32_t get_offset_of_InnerVertexOcclusion_8() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___InnerVertexOcclusion_8)); }
	inline bool get_InnerVertexOcclusion_8() const { return ___InnerVertexOcclusion_8; }
	inline bool* get_address_of_InnerVertexOcclusion_8() { return &___InnerVertexOcclusion_8; }
	inline void set_InnerVertexOcclusion_8(bool value)
	{
		___InnerVertexOcclusion_8 = value;
	}

	inline static int32_t get_offset_of_InnerVertexOcclusionAmount_9() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___InnerVertexOcclusionAmount_9)); }
	inline float get_InnerVertexOcclusionAmount_9() const { return ___InnerVertexOcclusionAmount_9; }
	inline float* get_address_of_InnerVertexOcclusionAmount_9() { return &___InnerVertexOcclusionAmount_9; }
	inline void set_InnerVertexOcclusionAmount_9(float value)
	{
		___InnerVertexOcclusionAmount_9 = value;
	}

	inline static int32_t get_offset_of_InnerVertexOcclusionBlur_10() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___InnerVertexOcclusionBlur_10)); }
	inline float get_InnerVertexOcclusionBlur_10() const { return ___InnerVertexOcclusionBlur_10; }
	inline float* get_address_of_InnerVertexOcclusionBlur_10() { return &___InnerVertexOcclusionBlur_10; }
	inline void set_InnerVertexOcclusionBlur_10(float value)
	{
		___InnerVertexOcclusionBlur_10 = value;
	}

	inline static int32_t get_offset_of_InnerVertexOcclusionBlurIterations_11() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___InnerVertexOcclusionBlurIterations_11)); }
	inline int32_t get_InnerVertexOcclusionBlurIterations_11() const { return ___InnerVertexOcclusionBlurIterations_11; }
	inline int32_t* get_address_of_InnerVertexOcclusionBlurIterations_11() { return &___InnerVertexOcclusionBlurIterations_11; }
	inline void set_InnerVertexOcclusionBlurIterations_11(int32_t value)
	{
		___InnerVertexOcclusionBlurIterations_11 = value;
	}

	inline static int32_t get_offset_of_AmbientOcclusion_12() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___AmbientOcclusion_12)); }
	inline bool get_AmbientOcclusion_12() const { return ___AmbientOcclusion_12; }
	inline bool* get_address_of_AmbientOcclusion_12() { return &___AmbientOcclusion_12; }
	inline void set_AmbientOcclusion_12(bool value)
	{
		___AmbientOcclusion_12 = value;
	}

	inline static int32_t get_offset_of_AmbientOcclusionAmount_13() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___AmbientOcclusionAmount_13)); }
	inline float get_AmbientOcclusionAmount_13() const { return ___AmbientOcclusionAmount_13; }
	inline float* get_address_of_AmbientOcclusionAmount_13() { return &___AmbientOcclusionAmount_13; }
	inline void set_AmbientOcclusionAmount_13(float value)
	{
		___AmbientOcclusionAmount_13 = value;
	}

	inline static int32_t get_offset_of_AmbientOcclusionRadius_14() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___AmbientOcclusionRadius_14)); }
	inline float get_AmbientOcclusionRadius_14() const { return ___AmbientOcclusionRadius_14; }
	inline float* get_address_of_AmbientOcclusionRadius_14() { return &___AmbientOcclusionRadius_14; }
	inline void set_AmbientOcclusionRadius_14(float value)
	{
		___AmbientOcclusionRadius_14 = value;
	}

	inline static int32_t get_offset_of_AmbientOcclusionBlur_15() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___AmbientOcclusionBlur_15)); }
	inline float get_AmbientOcclusionBlur_15() const { return ___AmbientOcclusionBlur_15; }
	inline float* get_address_of_AmbientOcclusionBlur_15() { return &___AmbientOcclusionBlur_15; }
	inline void set_AmbientOcclusionBlur_15(float value)
	{
		___AmbientOcclusionBlur_15 = value;
	}

	inline static int32_t get_offset_of_AmbientOcclusionBlurIterations_16() { return static_cast<int32_t>(offsetof(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B, ___AmbientOcclusionBlurIterations_16)); }
	inline int32_t get_AmbientOcclusionBlurIterations_16() const { return ___AmbientOcclusionBlurIterations_16; }
	inline int32_t* get_address_of_AmbientOcclusionBlurIterations_16() { return &___AmbientOcclusionBlurIterations_16; }
	inline void set_AmbientOcclusionBlurIterations_16(int32_t value)
	{
		___AmbientOcclusionBlurIterations_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VertexAnimationTools_30.ObjData/VertexColorsSettings
struct VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B_marshaled_pinvoke
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_0;
	int32_t ___quality_1;
	int32_t ___Cavity_2;
	float ___CavityAmount_3;
	float ___CavityAngleMin_4;
	float ___CavityAngleMax_5;
	float ___CavityBlur_6;
	int32_t ___CavityBlurIterations_7;
	int32_t ___InnerVertexOcclusion_8;
	float ___InnerVertexOcclusionAmount_9;
	float ___InnerVertexOcclusionBlur_10;
	int32_t ___InnerVertexOcclusionBlurIterations_11;
	int32_t ___AmbientOcclusion_12;
	float ___AmbientOcclusionAmount_13;
	float ___AmbientOcclusionRadius_14;
	float ___AmbientOcclusionBlur_15;
	int32_t ___AmbientOcclusionBlurIterations_16;
};
// Native definition for COM marshalling of VertexAnimationTools_30.ObjData/VertexColorsSettings
struct VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B_marshaled_com
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_0;
	int32_t ___quality_1;
	int32_t ___Cavity_2;
	float ___CavityAmount_3;
	float ___CavityAngleMin_4;
	float ___CavityAngleMax_5;
	float ___CavityBlur_6;
	int32_t ___CavityBlurIterations_7;
	int32_t ___InnerVertexOcclusion_8;
	float ___InnerVertexOcclusionAmount_9;
	float ___InnerVertexOcclusionBlur_10;
	int32_t ___InnerVertexOcclusionBlurIterations_11;
	int32_t ___AmbientOcclusion_12;
	float ___AmbientOcclusionAmount_13;
	float ___AmbientOcclusionRadius_14;
	float ___AmbientOcclusionBlur_15;
	int32_t ___AmbientOcclusionBlurIterations_16;
};
#endif // VERTEXCOLORSSETTINGS_T53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B_H
#ifndef IMPORTSETTINGS_TDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE_H
#define IMPORTSETTINGS_TDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCache_Clip_ImportSettings
struct  ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE 
{
public:
	// System.String VertexAnimationTools_30.PointCache_Clip_ImportSettings::Name
	String_t* ___Name_0;
	// System.Boolean VertexAnimationTools_30.PointCache_Clip_ImportSettings::FoldoutIsOpen
	bool ___FoldoutIsOpen_1;
	// System.String VertexAnimationTools_30.PointCache_Clip_ImportSettings::FilePath
	String_t* ___FilePath_2;
	// System.String VertexAnimationTools_30.PointCache_Clip_ImportSettings::FileName
	String_t* ___FileName_3;
	// System.Int32 VertexAnimationTools_30.PointCache_Clip_ImportSettings::FileVertsCount
	int32_t ___FileVertsCount_4;
	// System.Int32 VertexAnimationTools_30.PointCache_Clip_ImportSettings::FileFramesCount
	int32_t ___FileFramesCount_5;
	// System.String VertexAnimationTools_30.PointCache_Clip_ImportSettings::FileInfo
	String_t* ___FileInfo_6;
	// System.Boolean VertexAnimationTools_30.PointCache_Clip_ImportSettings::SwapYZAxis
	bool ___SwapYZAxis_7;
	// System.Single VertexAnimationTools_30.PointCache_Clip_ImportSettings::Scale
	float ___Scale_8;
	// System.Boolean VertexAnimationTools_30.PointCache_Clip_ImportSettings::IsLoop
	bool ___IsLoop_9;
	// System.Boolean VertexAnimationTools_30.PointCache_Clip_ImportSettings::EnableCustomRange
	bool ___EnableCustomRange_10;
	// System.Int32 VertexAnimationTools_30.PointCache_Clip_ImportSettings::CustomRangeFrom
	int32_t ___CustomRangeFrom_11;
	// System.Int32 VertexAnimationTools_30.PointCache_Clip_ImportSettings::CustomRangeTo
	int32_t ___CustomRangeTo_12;
	// VertexAnimationTools_30.TransitionModeEnum VertexAnimationTools_30.PointCache_Clip_ImportSettings::TransitionMode
	int32_t ___TransitionMode_13;
	// System.Int32 VertexAnimationTools_30.PointCache_Clip_ImportSettings::TransitionLength
	int32_t ___TransitionLength_14;
	// System.Boolean VertexAnimationTools_30.PointCache_Clip_ImportSettings::ChangeFramesCount
	bool ___ChangeFramesCount_15;
	// System.Int32 VertexAnimationTools_30.PointCache_Clip_ImportSettings::CustomFramesCount
	int32_t ___CustomFramesCount_16;
	// VertexAnimationTools_30.InterpolateModeEnum VertexAnimationTools_30.PointCache_Clip_ImportSettings::SubFrameInterpolation
	int32_t ___SubFrameInterpolation_17;
	// System.Int32 VertexAnimationTools_30.PointCache_Clip_ImportSettings::FrameIdxOffset
	int32_t ___FrameIdxOffset_18;
	// System.Boolean VertexAnimationTools_30.PointCache_Clip_ImportSettings::EnableMotionSmoothing
	bool ___EnableMotionSmoothing_19;
	// System.Int32 VertexAnimationTools_30.PointCache_Clip_ImportSettings::MotionSmoothIterations
	int32_t ___MotionSmoothIterations_20;
	// System.Single VertexAnimationTools_30.PointCache_Clip_ImportSettings::MotionSmoothAmountMin
	float ___MotionSmoothAmountMin_21;
	// System.Single VertexAnimationTools_30.PointCache_Clip_ImportSettings::MotionSmoothAmountMax
	float ___MotionSmoothAmountMax_22;
	// System.Single VertexAnimationTools_30.PointCache_Clip_ImportSettings::MotionSmoothEaseOffset
	float ___MotionSmoothEaseOffset_23;
	// System.Single VertexAnimationTools_30.PointCache_Clip_ImportSettings::MotionSmoothEaseLength
	float ___MotionSmoothEaseLength_24;
	// System.Boolean VertexAnimationTools_30.PointCache_Clip_ImportSettings::GenerageMotionPaths
	bool ___GenerageMotionPaths_25;
	// System.Int32 VertexAnimationTools_30.PointCache_Clip_ImportSettings::MotionPathsIndexStep
	int32_t ___MotionPathsIndexStep_26;
	// System.Boolean VertexAnimationTools_30.PointCache_Clip_ImportSettings::EnableNormalizeSpeed
	bool ___EnableNormalizeSpeed_27;
	// System.Single VertexAnimationTools_30.PointCache_Clip_ImportSettings::NormalizeSpeedPercentage
	float ___NormalizeSpeedPercentage_28;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_FoldoutIsOpen_1() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___FoldoutIsOpen_1)); }
	inline bool get_FoldoutIsOpen_1() const { return ___FoldoutIsOpen_1; }
	inline bool* get_address_of_FoldoutIsOpen_1() { return &___FoldoutIsOpen_1; }
	inline void set_FoldoutIsOpen_1(bool value)
	{
		___FoldoutIsOpen_1 = value;
	}

	inline static int32_t get_offset_of_FilePath_2() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___FilePath_2)); }
	inline String_t* get_FilePath_2() const { return ___FilePath_2; }
	inline String_t** get_address_of_FilePath_2() { return &___FilePath_2; }
	inline void set_FilePath_2(String_t* value)
	{
		___FilePath_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilePath_2), value);
	}

	inline static int32_t get_offset_of_FileName_3() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___FileName_3)); }
	inline String_t* get_FileName_3() const { return ___FileName_3; }
	inline String_t** get_address_of_FileName_3() { return &___FileName_3; }
	inline void set_FileName_3(String_t* value)
	{
		___FileName_3 = value;
		Il2CppCodeGenWriteBarrier((&___FileName_3), value);
	}

	inline static int32_t get_offset_of_FileVertsCount_4() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___FileVertsCount_4)); }
	inline int32_t get_FileVertsCount_4() const { return ___FileVertsCount_4; }
	inline int32_t* get_address_of_FileVertsCount_4() { return &___FileVertsCount_4; }
	inline void set_FileVertsCount_4(int32_t value)
	{
		___FileVertsCount_4 = value;
	}

	inline static int32_t get_offset_of_FileFramesCount_5() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___FileFramesCount_5)); }
	inline int32_t get_FileFramesCount_5() const { return ___FileFramesCount_5; }
	inline int32_t* get_address_of_FileFramesCount_5() { return &___FileFramesCount_5; }
	inline void set_FileFramesCount_5(int32_t value)
	{
		___FileFramesCount_5 = value;
	}

	inline static int32_t get_offset_of_FileInfo_6() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___FileInfo_6)); }
	inline String_t* get_FileInfo_6() const { return ___FileInfo_6; }
	inline String_t** get_address_of_FileInfo_6() { return &___FileInfo_6; }
	inline void set_FileInfo_6(String_t* value)
	{
		___FileInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___FileInfo_6), value);
	}

	inline static int32_t get_offset_of_SwapYZAxis_7() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___SwapYZAxis_7)); }
	inline bool get_SwapYZAxis_7() const { return ___SwapYZAxis_7; }
	inline bool* get_address_of_SwapYZAxis_7() { return &___SwapYZAxis_7; }
	inline void set_SwapYZAxis_7(bool value)
	{
		___SwapYZAxis_7 = value;
	}

	inline static int32_t get_offset_of_Scale_8() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___Scale_8)); }
	inline float get_Scale_8() const { return ___Scale_8; }
	inline float* get_address_of_Scale_8() { return &___Scale_8; }
	inline void set_Scale_8(float value)
	{
		___Scale_8 = value;
	}

	inline static int32_t get_offset_of_IsLoop_9() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___IsLoop_9)); }
	inline bool get_IsLoop_9() const { return ___IsLoop_9; }
	inline bool* get_address_of_IsLoop_9() { return &___IsLoop_9; }
	inline void set_IsLoop_9(bool value)
	{
		___IsLoop_9 = value;
	}

	inline static int32_t get_offset_of_EnableCustomRange_10() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___EnableCustomRange_10)); }
	inline bool get_EnableCustomRange_10() const { return ___EnableCustomRange_10; }
	inline bool* get_address_of_EnableCustomRange_10() { return &___EnableCustomRange_10; }
	inline void set_EnableCustomRange_10(bool value)
	{
		___EnableCustomRange_10 = value;
	}

	inline static int32_t get_offset_of_CustomRangeFrom_11() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___CustomRangeFrom_11)); }
	inline int32_t get_CustomRangeFrom_11() const { return ___CustomRangeFrom_11; }
	inline int32_t* get_address_of_CustomRangeFrom_11() { return &___CustomRangeFrom_11; }
	inline void set_CustomRangeFrom_11(int32_t value)
	{
		___CustomRangeFrom_11 = value;
	}

	inline static int32_t get_offset_of_CustomRangeTo_12() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___CustomRangeTo_12)); }
	inline int32_t get_CustomRangeTo_12() const { return ___CustomRangeTo_12; }
	inline int32_t* get_address_of_CustomRangeTo_12() { return &___CustomRangeTo_12; }
	inline void set_CustomRangeTo_12(int32_t value)
	{
		___CustomRangeTo_12 = value;
	}

	inline static int32_t get_offset_of_TransitionMode_13() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___TransitionMode_13)); }
	inline int32_t get_TransitionMode_13() const { return ___TransitionMode_13; }
	inline int32_t* get_address_of_TransitionMode_13() { return &___TransitionMode_13; }
	inline void set_TransitionMode_13(int32_t value)
	{
		___TransitionMode_13 = value;
	}

	inline static int32_t get_offset_of_TransitionLength_14() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___TransitionLength_14)); }
	inline int32_t get_TransitionLength_14() const { return ___TransitionLength_14; }
	inline int32_t* get_address_of_TransitionLength_14() { return &___TransitionLength_14; }
	inline void set_TransitionLength_14(int32_t value)
	{
		___TransitionLength_14 = value;
	}

	inline static int32_t get_offset_of_ChangeFramesCount_15() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___ChangeFramesCount_15)); }
	inline bool get_ChangeFramesCount_15() const { return ___ChangeFramesCount_15; }
	inline bool* get_address_of_ChangeFramesCount_15() { return &___ChangeFramesCount_15; }
	inline void set_ChangeFramesCount_15(bool value)
	{
		___ChangeFramesCount_15 = value;
	}

	inline static int32_t get_offset_of_CustomFramesCount_16() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___CustomFramesCount_16)); }
	inline int32_t get_CustomFramesCount_16() const { return ___CustomFramesCount_16; }
	inline int32_t* get_address_of_CustomFramesCount_16() { return &___CustomFramesCount_16; }
	inline void set_CustomFramesCount_16(int32_t value)
	{
		___CustomFramesCount_16 = value;
	}

	inline static int32_t get_offset_of_SubFrameInterpolation_17() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___SubFrameInterpolation_17)); }
	inline int32_t get_SubFrameInterpolation_17() const { return ___SubFrameInterpolation_17; }
	inline int32_t* get_address_of_SubFrameInterpolation_17() { return &___SubFrameInterpolation_17; }
	inline void set_SubFrameInterpolation_17(int32_t value)
	{
		___SubFrameInterpolation_17 = value;
	}

	inline static int32_t get_offset_of_FrameIdxOffset_18() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___FrameIdxOffset_18)); }
	inline int32_t get_FrameIdxOffset_18() const { return ___FrameIdxOffset_18; }
	inline int32_t* get_address_of_FrameIdxOffset_18() { return &___FrameIdxOffset_18; }
	inline void set_FrameIdxOffset_18(int32_t value)
	{
		___FrameIdxOffset_18 = value;
	}

	inline static int32_t get_offset_of_EnableMotionSmoothing_19() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___EnableMotionSmoothing_19)); }
	inline bool get_EnableMotionSmoothing_19() const { return ___EnableMotionSmoothing_19; }
	inline bool* get_address_of_EnableMotionSmoothing_19() { return &___EnableMotionSmoothing_19; }
	inline void set_EnableMotionSmoothing_19(bool value)
	{
		___EnableMotionSmoothing_19 = value;
	}

	inline static int32_t get_offset_of_MotionSmoothIterations_20() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___MotionSmoothIterations_20)); }
	inline int32_t get_MotionSmoothIterations_20() const { return ___MotionSmoothIterations_20; }
	inline int32_t* get_address_of_MotionSmoothIterations_20() { return &___MotionSmoothIterations_20; }
	inline void set_MotionSmoothIterations_20(int32_t value)
	{
		___MotionSmoothIterations_20 = value;
	}

	inline static int32_t get_offset_of_MotionSmoothAmountMin_21() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___MotionSmoothAmountMin_21)); }
	inline float get_MotionSmoothAmountMin_21() const { return ___MotionSmoothAmountMin_21; }
	inline float* get_address_of_MotionSmoothAmountMin_21() { return &___MotionSmoothAmountMin_21; }
	inline void set_MotionSmoothAmountMin_21(float value)
	{
		___MotionSmoothAmountMin_21 = value;
	}

	inline static int32_t get_offset_of_MotionSmoothAmountMax_22() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___MotionSmoothAmountMax_22)); }
	inline float get_MotionSmoothAmountMax_22() const { return ___MotionSmoothAmountMax_22; }
	inline float* get_address_of_MotionSmoothAmountMax_22() { return &___MotionSmoothAmountMax_22; }
	inline void set_MotionSmoothAmountMax_22(float value)
	{
		___MotionSmoothAmountMax_22 = value;
	}

	inline static int32_t get_offset_of_MotionSmoothEaseOffset_23() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___MotionSmoothEaseOffset_23)); }
	inline float get_MotionSmoothEaseOffset_23() const { return ___MotionSmoothEaseOffset_23; }
	inline float* get_address_of_MotionSmoothEaseOffset_23() { return &___MotionSmoothEaseOffset_23; }
	inline void set_MotionSmoothEaseOffset_23(float value)
	{
		___MotionSmoothEaseOffset_23 = value;
	}

	inline static int32_t get_offset_of_MotionSmoothEaseLength_24() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___MotionSmoothEaseLength_24)); }
	inline float get_MotionSmoothEaseLength_24() const { return ___MotionSmoothEaseLength_24; }
	inline float* get_address_of_MotionSmoothEaseLength_24() { return &___MotionSmoothEaseLength_24; }
	inline void set_MotionSmoothEaseLength_24(float value)
	{
		___MotionSmoothEaseLength_24 = value;
	}

	inline static int32_t get_offset_of_GenerageMotionPaths_25() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___GenerageMotionPaths_25)); }
	inline bool get_GenerageMotionPaths_25() const { return ___GenerageMotionPaths_25; }
	inline bool* get_address_of_GenerageMotionPaths_25() { return &___GenerageMotionPaths_25; }
	inline void set_GenerageMotionPaths_25(bool value)
	{
		___GenerageMotionPaths_25 = value;
	}

	inline static int32_t get_offset_of_MotionPathsIndexStep_26() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___MotionPathsIndexStep_26)); }
	inline int32_t get_MotionPathsIndexStep_26() const { return ___MotionPathsIndexStep_26; }
	inline int32_t* get_address_of_MotionPathsIndexStep_26() { return &___MotionPathsIndexStep_26; }
	inline void set_MotionPathsIndexStep_26(int32_t value)
	{
		___MotionPathsIndexStep_26 = value;
	}

	inline static int32_t get_offset_of_EnableNormalizeSpeed_27() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___EnableNormalizeSpeed_27)); }
	inline bool get_EnableNormalizeSpeed_27() const { return ___EnableNormalizeSpeed_27; }
	inline bool* get_address_of_EnableNormalizeSpeed_27() { return &___EnableNormalizeSpeed_27; }
	inline void set_EnableNormalizeSpeed_27(bool value)
	{
		___EnableNormalizeSpeed_27 = value;
	}

	inline static int32_t get_offset_of_NormalizeSpeedPercentage_28() { return static_cast<int32_t>(offsetof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE, ___NormalizeSpeedPercentage_28)); }
	inline float get_NormalizeSpeedPercentage_28() const { return ___NormalizeSpeedPercentage_28; }
	inline float* get_address_of_NormalizeSpeedPercentage_28() { return &___NormalizeSpeedPercentage_28; }
	inline void set_NormalizeSpeedPercentage_28(float value)
	{
		___NormalizeSpeedPercentage_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VertexAnimationTools_30.PointCache/Clip/ImportSettings
struct ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___FoldoutIsOpen_1;
	char* ___FilePath_2;
	char* ___FileName_3;
	int32_t ___FileVertsCount_4;
	int32_t ___FileFramesCount_5;
	char* ___FileInfo_6;
	int32_t ___SwapYZAxis_7;
	float ___Scale_8;
	int32_t ___IsLoop_9;
	int32_t ___EnableCustomRange_10;
	int32_t ___CustomRangeFrom_11;
	int32_t ___CustomRangeTo_12;
	int32_t ___TransitionMode_13;
	int32_t ___TransitionLength_14;
	int32_t ___ChangeFramesCount_15;
	int32_t ___CustomFramesCount_16;
	int32_t ___SubFrameInterpolation_17;
	int32_t ___FrameIdxOffset_18;
	int32_t ___EnableMotionSmoothing_19;
	int32_t ___MotionSmoothIterations_20;
	float ___MotionSmoothAmountMin_21;
	float ___MotionSmoothAmountMax_22;
	float ___MotionSmoothEaseOffset_23;
	float ___MotionSmoothEaseLength_24;
	int32_t ___GenerageMotionPaths_25;
	int32_t ___MotionPathsIndexStep_26;
	int32_t ___EnableNormalizeSpeed_27;
	float ___NormalizeSpeedPercentage_28;
};
// Native definition for COM marshalling of VertexAnimationTools_30.PointCache/Clip/ImportSettings
struct ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___FoldoutIsOpen_1;
	Il2CppChar* ___FilePath_2;
	Il2CppChar* ___FileName_3;
	int32_t ___FileVertsCount_4;
	int32_t ___FileFramesCount_5;
	Il2CppChar* ___FileInfo_6;
	int32_t ___SwapYZAxis_7;
	float ___Scale_8;
	int32_t ___IsLoop_9;
	int32_t ___EnableCustomRange_10;
	int32_t ___CustomRangeFrom_11;
	int32_t ___CustomRangeTo_12;
	int32_t ___TransitionMode_13;
	int32_t ___TransitionLength_14;
	int32_t ___ChangeFramesCount_15;
	int32_t ___CustomFramesCount_16;
	int32_t ___SubFrameInterpolation_17;
	int32_t ___FrameIdxOffset_18;
	int32_t ___EnableMotionSmoothing_19;
	int32_t ___MotionSmoothIterations_20;
	float ___MotionSmoothAmountMin_21;
	float ___MotionSmoothAmountMax_22;
	float ___MotionSmoothEaseOffset_23;
	float ___MotionSmoothEaseLength_24;
	int32_t ___GenerageMotionPaths_25;
	int32_t ___MotionPathsIndexStep_26;
	int32_t ___EnableNormalizeSpeed_27;
	float ___NormalizeSpeedPercentage_28;
};
#endif // IMPORTSETTINGS_TDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE_H
#ifndef IMPORTSETTINGS_T9869042A81D4717B91E68D4889002EED19751A3C_H
#define IMPORTSETTINGS_T9869042A81D4717B91E68D4889002EED19751A3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCache_ImportSettings
struct  ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C 
{
public:
	// System.Boolean VertexAnimationTools_30.PointCache_ImportSettings::SwapYZAxis
	bool ___SwapYZAxis_0;
	// System.Single VertexAnimationTools_30.PointCache_ImportSettings::ScaleFactor
	float ___ScaleFactor_1;
	// System.Boolean VertexAnimationTools_30.PointCache_ImportSettings::FlipNormals
	bool ___FlipNormals_2;
	// VertexAnimationTools_30.ObjData_SmoothingGroupImportModeEnum VertexAnimationTools_30.PointCache_ImportSettings::SmoothingGroupImportMode
	int32_t ___SmoothingGroupImportMode_3;
	// VertexAnimationTools_30.ObjData_NormalsRecalculationModeEnum VertexAnimationTools_30.PointCache_ImportSettings::NormalRecalculationMode
	int32_t ___NormalRecalculationMode_4;
	// System.Int32 VertexAnimationTools_30.PointCache_ImportSettings::MeshCompression
	int32_t ___MeshCompression_5;
	// System.Boolean VertexAnimationTools_30.PointCache_ImportSettings::OptimizeMesh
	bool ___OptimizeMesh_6;
	// System.Int32 VertexAnimationTools_30.PointCache_ImportSettings::UsedClipsCount
	int32_t ___UsedClipsCount_7;
	// System.Int32 VertexAnimationTools_30.PointCache_ImportSettings::UsedMeshesCount
	int32_t ___UsedMeshesCount_8;
	// System.Boolean VertexAnimationTools_30.PointCache_ImportSettings::GenerateMaterials
	bool ___GenerateMaterials_9;
	// System.Boolean VertexAnimationTools_30.PointCache_ImportSettings::SavePortableData
	bool ___SavePortableData_10;
	// UnityEngine.Rendering.IndexFormat VertexAnimationTools_30.PointCache_ImportSettings::IndexFormat
	int32_t ___IndexFormat_11;

public:
	inline static int32_t get_offset_of_SwapYZAxis_0() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___SwapYZAxis_0)); }
	inline bool get_SwapYZAxis_0() const { return ___SwapYZAxis_0; }
	inline bool* get_address_of_SwapYZAxis_0() { return &___SwapYZAxis_0; }
	inline void set_SwapYZAxis_0(bool value)
	{
		___SwapYZAxis_0 = value;
	}

	inline static int32_t get_offset_of_ScaleFactor_1() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___ScaleFactor_1)); }
	inline float get_ScaleFactor_1() const { return ___ScaleFactor_1; }
	inline float* get_address_of_ScaleFactor_1() { return &___ScaleFactor_1; }
	inline void set_ScaleFactor_1(float value)
	{
		___ScaleFactor_1 = value;
	}

	inline static int32_t get_offset_of_FlipNormals_2() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___FlipNormals_2)); }
	inline bool get_FlipNormals_2() const { return ___FlipNormals_2; }
	inline bool* get_address_of_FlipNormals_2() { return &___FlipNormals_2; }
	inline void set_FlipNormals_2(bool value)
	{
		___FlipNormals_2 = value;
	}

	inline static int32_t get_offset_of_SmoothingGroupImportMode_3() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___SmoothingGroupImportMode_3)); }
	inline int32_t get_SmoothingGroupImportMode_3() const { return ___SmoothingGroupImportMode_3; }
	inline int32_t* get_address_of_SmoothingGroupImportMode_3() { return &___SmoothingGroupImportMode_3; }
	inline void set_SmoothingGroupImportMode_3(int32_t value)
	{
		___SmoothingGroupImportMode_3 = value;
	}

	inline static int32_t get_offset_of_NormalRecalculationMode_4() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___NormalRecalculationMode_4)); }
	inline int32_t get_NormalRecalculationMode_4() const { return ___NormalRecalculationMode_4; }
	inline int32_t* get_address_of_NormalRecalculationMode_4() { return &___NormalRecalculationMode_4; }
	inline void set_NormalRecalculationMode_4(int32_t value)
	{
		___NormalRecalculationMode_4 = value;
	}

	inline static int32_t get_offset_of_MeshCompression_5() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___MeshCompression_5)); }
	inline int32_t get_MeshCompression_5() const { return ___MeshCompression_5; }
	inline int32_t* get_address_of_MeshCompression_5() { return &___MeshCompression_5; }
	inline void set_MeshCompression_5(int32_t value)
	{
		___MeshCompression_5 = value;
	}

	inline static int32_t get_offset_of_OptimizeMesh_6() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___OptimizeMesh_6)); }
	inline bool get_OptimizeMesh_6() const { return ___OptimizeMesh_6; }
	inline bool* get_address_of_OptimizeMesh_6() { return &___OptimizeMesh_6; }
	inline void set_OptimizeMesh_6(bool value)
	{
		___OptimizeMesh_6 = value;
	}

	inline static int32_t get_offset_of_UsedClipsCount_7() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___UsedClipsCount_7)); }
	inline int32_t get_UsedClipsCount_7() const { return ___UsedClipsCount_7; }
	inline int32_t* get_address_of_UsedClipsCount_7() { return &___UsedClipsCount_7; }
	inline void set_UsedClipsCount_7(int32_t value)
	{
		___UsedClipsCount_7 = value;
	}

	inline static int32_t get_offset_of_UsedMeshesCount_8() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___UsedMeshesCount_8)); }
	inline int32_t get_UsedMeshesCount_8() const { return ___UsedMeshesCount_8; }
	inline int32_t* get_address_of_UsedMeshesCount_8() { return &___UsedMeshesCount_8; }
	inline void set_UsedMeshesCount_8(int32_t value)
	{
		___UsedMeshesCount_8 = value;
	}

	inline static int32_t get_offset_of_GenerateMaterials_9() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___GenerateMaterials_9)); }
	inline bool get_GenerateMaterials_9() const { return ___GenerateMaterials_9; }
	inline bool* get_address_of_GenerateMaterials_9() { return &___GenerateMaterials_9; }
	inline void set_GenerateMaterials_9(bool value)
	{
		___GenerateMaterials_9 = value;
	}

	inline static int32_t get_offset_of_SavePortableData_10() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___SavePortableData_10)); }
	inline bool get_SavePortableData_10() const { return ___SavePortableData_10; }
	inline bool* get_address_of_SavePortableData_10() { return &___SavePortableData_10; }
	inline void set_SavePortableData_10(bool value)
	{
		___SavePortableData_10 = value;
	}

	inline static int32_t get_offset_of_IndexFormat_11() { return static_cast<int32_t>(offsetof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C, ___IndexFormat_11)); }
	inline int32_t get_IndexFormat_11() const { return ___IndexFormat_11; }
	inline int32_t* get_address_of_IndexFormat_11() { return &___IndexFormat_11; }
	inline void set_IndexFormat_11(int32_t value)
	{
		___IndexFormat_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VertexAnimationTools_30.PointCache/ImportSettings
struct ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C_marshaled_pinvoke
{
	int32_t ___SwapYZAxis_0;
	float ___ScaleFactor_1;
	int32_t ___FlipNormals_2;
	int32_t ___SmoothingGroupImportMode_3;
	int32_t ___NormalRecalculationMode_4;
	int32_t ___MeshCompression_5;
	int32_t ___OptimizeMesh_6;
	int32_t ___UsedClipsCount_7;
	int32_t ___UsedMeshesCount_8;
	int32_t ___GenerateMaterials_9;
	int32_t ___SavePortableData_10;
	int32_t ___IndexFormat_11;
};
// Native definition for COM marshalling of VertexAnimationTools_30.PointCache/ImportSettings
struct ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C_marshaled_com
{
	int32_t ___SwapYZAxis_0;
	float ___ScaleFactor_1;
	int32_t ___FlipNormals_2;
	int32_t ___SmoothingGroupImportMode_3;
	int32_t ___NormalRecalculationMode_4;
	int32_t ___MeshCompression_5;
	int32_t ___OptimizeMesh_6;
	int32_t ___UsedClipsCount_7;
	int32_t ___UsedMeshesCount_8;
	int32_t ___GenerateMaterials_9;
	int32_t ___SavePortableData_10;
	int32_t ___IndexFormat_11;
};
#endif // IMPORTSETTINGS_T9869042A81D4717B91E68D4889002EED19751A3C_H
#ifndef POSTIMPORTCONSTRAINT_T224DDBE801C7017C5D90625F743A0A22FDE88131_H
#define POSTIMPORTCONSTRAINT_T224DDBE801C7017C5D90625F743A0A22FDE88131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCache_PostImportConstraint
struct  PostImportConstraint_t224DDBE801C7017C5D90625F743A0A22FDE88131  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.PointCache_PostImportConstraint::Name
	String_t* ___Name_0;
	// VertexAnimationTools_30.PFU VertexAnimationTools_30.PointCache_PostImportConstraint::ObjSpace
	PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  ___ObjSpace_1;
	// VertexAnimationTools_30.ConstraintClip[] VertexAnimationTools_30.PointCache_PostImportConstraint::Clips
	ConstraintClipU5BU5D_tBD1D16543191F7472AE27DEB020BC21DE5E3A615* ___Clips_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(PostImportConstraint_t224DDBE801C7017C5D90625F743A0A22FDE88131, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ObjSpace_1() { return static_cast<int32_t>(offsetof(PostImportConstraint_t224DDBE801C7017C5D90625F743A0A22FDE88131, ___ObjSpace_1)); }
	inline PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  get_ObjSpace_1() const { return ___ObjSpace_1; }
	inline PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D * get_address_of_ObjSpace_1() { return &___ObjSpace_1; }
	inline void set_ObjSpace_1(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  value)
	{
		___ObjSpace_1 = value;
	}

	inline static int32_t get_offset_of_Clips_2() { return static_cast<int32_t>(offsetof(PostImportConstraint_t224DDBE801C7017C5D90625F743A0A22FDE88131, ___Clips_2)); }
	inline ConstraintClipU5BU5D_tBD1D16543191F7472AE27DEB020BC21DE5E3A615* get_Clips_2() const { return ___Clips_2; }
	inline ConstraintClipU5BU5D_tBD1D16543191F7472AE27DEB020BC21DE5E3A615** get_address_of_Clips_2() { return &___Clips_2; }
	inline void set_Clips_2(ConstraintClipU5BU5D_tBD1D16543191F7472AE27DEB020BC21DE5E3A615* value)
	{
		___Clips_2 = value;
		Il2CppCodeGenWriteBarrier((&___Clips_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTIMPORTCONSTRAINT_T224DDBE801C7017C5D90625F743A0A22FDE88131_H
#ifndef U3CIMPORTIEU3ED__37_T895AEC7C95EA33E148F5688524AB881236D75C9C_H
#define U3CIMPORTIEU3ED__37_T895AEC7C95EA33E148F5688524AB881236D75C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37
struct  U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<>1__state
	int32_t ___U3CU3E1__state_0;
	// VertexAnimationTools_30.TaskInfo VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<>2__current
	TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  ___U3CU3E2__current_1;
	// System.Int32 VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// VertexAnimationTools_30.PointCachePlayer VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<>4__this
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * ___U3CU3E4__this_3;
	// VertexAnimationTools_30.PointCache VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<pc>5__2
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * ___U3CpcU3E5__2_4;
	// VertexAnimationTools_30.TasksStack VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<ts>5__3
	TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * ___U3CtsU3E5__3_5;
	// VertexAnimationTools_30.PointCache_PolygonMesh VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<lod0>5__4
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C * ___U3Clod0U3E5__4_6;
	// UnityEngine.Bounds VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<bindPoseBounds>5__5
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___U3CbindPoseBoundsU3E5__5_7;
	// VertexAnimationTools_30.BindingHelper VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<bh>5__6
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 * ___U3CbhU3E5__6_8;
	// System.Boolean VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<boundsIsCreated>5__7
	bool ___U3CboundsIsCreatedU3E5__7_9;
	// UnityEngine.Bounds VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<bounds>5__8
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___U3CboundsU3E5__8_10;
	// System.Int32 VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<blendshapesCounter>5__9
	int32_t ___U3CblendshapesCounterU3E5__9_11;
	// System.Int32 VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<i>5__10
	int32_t ___U3CiU3E5__10_12;
	// System.String VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<taskName>5__11
	String_t* ___U3CtaskNameU3E5__11_13;
	// System.Collections.Generic.IEnumerator`1<VertexAnimationTools_30.TaskInfo> VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<>7__wrap11
	RuntimeObject* ___U3CU3E7__wrap11_14;
	// VertexAnimationTools_30.PointCache_Clip VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<clip>5__13
	Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553 * ___U3CclipU3E5__13_15;
	// VertexAnimationTools_30.PointCacheData VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<pcdata>5__14
	PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5 * ___U3CpcdataU3E5__14_16;
	// System.Int32 VertexAnimationTools_30.PointCachePlayer_<ImportIE>d__37::<f>5__15
	int32_t ___U3CfU3E5__15_17;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CU3E2__current_1)); }
	inline TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CU3E4__this_3)); }
	inline PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CpcU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CpcU3E5__2_4)); }
	inline PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * get_U3CpcU3E5__2_4() const { return ___U3CpcU3E5__2_4; }
	inline PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 ** get_address_of_U3CpcU3E5__2_4() { return &___U3CpcU3E5__2_4; }
	inline void set_U3CpcU3E5__2_4(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * value)
	{
		___U3CpcU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpcU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CtsU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CtsU3E5__3_5)); }
	inline TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * get_U3CtsU3E5__3_5() const { return ___U3CtsU3E5__3_5; }
	inline TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F ** get_address_of_U3CtsU3E5__3_5() { return &___U3CtsU3E5__3_5; }
	inline void set_U3CtsU3E5__3_5(TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * value)
	{
		___U3CtsU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtsU3E5__3_5), value);
	}

	inline static int32_t get_offset_of_U3Clod0U3E5__4_6() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3Clod0U3E5__4_6)); }
	inline PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C * get_U3Clod0U3E5__4_6() const { return ___U3Clod0U3E5__4_6; }
	inline PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C ** get_address_of_U3Clod0U3E5__4_6() { return &___U3Clod0U3E5__4_6; }
	inline void set_U3Clod0U3E5__4_6(PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C * value)
	{
		___U3Clod0U3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3Clod0U3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CbindPoseBoundsU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CbindPoseBoundsU3E5__5_7)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_U3CbindPoseBoundsU3E5__5_7() const { return ___U3CbindPoseBoundsU3E5__5_7; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_U3CbindPoseBoundsU3E5__5_7() { return &___U3CbindPoseBoundsU3E5__5_7; }
	inline void set_U3CbindPoseBoundsU3E5__5_7(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___U3CbindPoseBoundsU3E5__5_7 = value;
	}

	inline static int32_t get_offset_of_U3CbhU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CbhU3E5__6_8)); }
	inline BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 * get_U3CbhU3E5__6_8() const { return ___U3CbhU3E5__6_8; }
	inline BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 ** get_address_of_U3CbhU3E5__6_8() { return &___U3CbhU3E5__6_8; }
	inline void set_U3CbhU3E5__6_8(BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8 * value)
	{
		___U3CbhU3E5__6_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbhU3E5__6_8), value);
	}

	inline static int32_t get_offset_of_U3CboundsIsCreatedU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CboundsIsCreatedU3E5__7_9)); }
	inline bool get_U3CboundsIsCreatedU3E5__7_9() const { return ___U3CboundsIsCreatedU3E5__7_9; }
	inline bool* get_address_of_U3CboundsIsCreatedU3E5__7_9() { return &___U3CboundsIsCreatedU3E5__7_9; }
	inline void set_U3CboundsIsCreatedU3E5__7_9(bool value)
	{
		___U3CboundsIsCreatedU3E5__7_9 = value;
	}

	inline static int32_t get_offset_of_U3CboundsU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CboundsU3E5__8_10)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_U3CboundsU3E5__8_10() const { return ___U3CboundsU3E5__8_10; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_U3CboundsU3E5__8_10() { return &___U3CboundsU3E5__8_10; }
	inline void set_U3CboundsU3E5__8_10(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___U3CboundsU3E5__8_10 = value;
	}

	inline static int32_t get_offset_of_U3CblendshapesCounterU3E5__9_11() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CblendshapesCounterU3E5__9_11)); }
	inline int32_t get_U3CblendshapesCounterU3E5__9_11() const { return ___U3CblendshapesCounterU3E5__9_11; }
	inline int32_t* get_address_of_U3CblendshapesCounterU3E5__9_11() { return &___U3CblendshapesCounterU3E5__9_11; }
	inline void set_U3CblendshapesCounterU3E5__9_11(int32_t value)
	{
		___U3CblendshapesCounterU3E5__9_11 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__10_12() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CiU3E5__10_12)); }
	inline int32_t get_U3CiU3E5__10_12() const { return ___U3CiU3E5__10_12; }
	inline int32_t* get_address_of_U3CiU3E5__10_12() { return &___U3CiU3E5__10_12; }
	inline void set_U3CiU3E5__10_12(int32_t value)
	{
		___U3CiU3E5__10_12 = value;
	}

	inline static int32_t get_offset_of_U3CtaskNameU3E5__11_13() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CtaskNameU3E5__11_13)); }
	inline String_t* get_U3CtaskNameU3E5__11_13() const { return ___U3CtaskNameU3E5__11_13; }
	inline String_t** get_address_of_U3CtaskNameU3E5__11_13() { return &___U3CtaskNameU3E5__11_13; }
	inline void set_U3CtaskNameU3E5__11_13(String_t* value)
	{
		___U3CtaskNameU3E5__11_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtaskNameU3E5__11_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap11_14() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CU3E7__wrap11_14)); }
	inline RuntimeObject* get_U3CU3E7__wrap11_14() const { return ___U3CU3E7__wrap11_14; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap11_14() { return &___U3CU3E7__wrap11_14; }
	inline void set_U3CU3E7__wrap11_14(RuntimeObject* value)
	{
		___U3CU3E7__wrap11_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap11_14), value);
	}

	inline static int32_t get_offset_of_U3CclipU3E5__13_15() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CclipU3E5__13_15)); }
	inline Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553 * get_U3CclipU3E5__13_15() const { return ___U3CclipU3E5__13_15; }
	inline Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553 ** get_address_of_U3CclipU3E5__13_15() { return &___U3CclipU3E5__13_15; }
	inline void set_U3CclipU3E5__13_15(Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553 * value)
	{
		___U3CclipU3E5__13_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclipU3E5__13_15), value);
	}

	inline static int32_t get_offset_of_U3CpcdataU3E5__14_16() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CpcdataU3E5__14_16)); }
	inline PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5 * get_U3CpcdataU3E5__14_16() const { return ___U3CpcdataU3E5__14_16; }
	inline PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5 ** get_address_of_U3CpcdataU3E5__14_16() { return &___U3CpcdataU3E5__14_16; }
	inline void set_U3CpcdataU3E5__14_16(PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5 * value)
	{
		___U3CpcdataU3E5__14_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpcdataU3E5__14_16), value);
	}

	inline static int32_t get_offset_of_U3CfU3E5__15_17() { return static_cast<int32_t>(offsetof(U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C, ___U3CfU3E5__15_17)); }
	inline int32_t get_U3CfU3E5__15_17() const { return ___U3CfU3E5__15_17; }
	inline int32_t* get_address_of_U3CfU3E5__15_17() { return &___U3CfU3E5__15_17; }
	inline void set_U3CfU3E5__15_17(int32_t value)
	{
		___U3CfU3E5__15_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CIMPORTIEU3ED__37_T895AEC7C95EA33E148F5688524AB881236D75C9C_H
#ifndef CLIP_T7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77_H
#define CLIP_T7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCachePlayer_Clip
struct  Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.PointCachePlayer_Clip::Idx
	int32_t ___Idx_0;
	// VertexAnimationTools_30.PointCachePlayer VertexAnimationTools_30.PointCachePlayer_Clip::Player
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * ___Player_1;
	// VertexAnimationTools_30.AutoPlaybackTypeEnum VertexAnimationTools_30.PointCachePlayer_Clip::AutoPlaybackType
	int32_t ___AutoPlaybackType_2;
	// System.Single VertexAnimationTools_30.PointCachePlayer_Clip::DurationInSeconds
	float ___DurationInSeconds_3;
	// System.Boolean VertexAnimationTools_30.PointCachePlayer_Clip::DrawMotionPath
	bool ___DrawMotionPath_4;
	// System.Single VertexAnimationTools_30.PointCachePlayer_Clip::MotionPathIconSize
	float ___MotionPathIconSize_5;

public:
	inline static int32_t get_offset_of_Idx_0() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___Idx_0)); }
	inline int32_t get_Idx_0() const { return ___Idx_0; }
	inline int32_t* get_address_of_Idx_0() { return &___Idx_0; }
	inline void set_Idx_0(int32_t value)
	{
		___Idx_0 = value;
	}

	inline static int32_t get_offset_of_Player_1() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___Player_1)); }
	inline PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * get_Player_1() const { return ___Player_1; }
	inline PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F ** get_address_of_Player_1() { return &___Player_1; }
	inline void set_Player_1(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * value)
	{
		___Player_1 = value;
		Il2CppCodeGenWriteBarrier((&___Player_1), value);
	}

	inline static int32_t get_offset_of_AutoPlaybackType_2() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___AutoPlaybackType_2)); }
	inline int32_t get_AutoPlaybackType_2() const { return ___AutoPlaybackType_2; }
	inline int32_t* get_address_of_AutoPlaybackType_2() { return &___AutoPlaybackType_2; }
	inline void set_AutoPlaybackType_2(int32_t value)
	{
		___AutoPlaybackType_2 = value;
	}

	inline static int32_t get_offset_of_DurationInSeconds_3() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___DurationInSeconds_3)); }
	inline float get_DurationInSeconds_3() const { return ___DurationInSeconds_3; }
	inline float* get_address_of_DurationInSeconds_3() { return &___DurationInSeconds_3; }
	inline void set_DurationInSeconds_3(float value)
	{
		___DurationInSeconds_3 = value;
	}

	inline static int32_t get_offset_of_DrawMotionPath_4() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___DrawMotionPath_4)); }
	inline bool get_DrawMotionPath_4() const { return ___DrawMotionPath_4; }
	inline bool* get_address_of_DrawMotionPath_4() { return &___DrawMotionPath_4; }
	inline void set_DrawMotionPath_4(bool value)
	{
		___DrawMotionPath_4 = value;
	}

	inline static int32_t get_offset_of_MotionPathIconSize_5() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___MotionPathIconSize_5)); }
	inline float get_MotionPathIconSize_5() const { return ___MotionPathIconSize_5; }
	inline float* get_address_of_MotionPathIconSize_5() { return &___MotionPathIconSize_5; }
	inline void set_MotionPathIconSize_5(float value)
	{
		___MotionPathIconSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIP_T7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77_H
#ifndef CONSTRAINT_T5134256E17C3C59BE6CBE19E96E7369D7A88D6C1_H
#define CONSTRAINT_T5134256E17C3C59BE6CBE19E96E7369D7A88D6C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCachePlayer_Constraint
struct  Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.PointCachePlayer_Constraint::Name
	String_t* ___Name_0;
	// UnityEngine.Transform VertexAnimationTools_30.PointCachePlayer_Constraint::Tr
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Tr_1;
	// VertexAnimationTools_30.PFU VertexAnimationTools_30.PointCachePlayer_Constraint::utm
	PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  ___utm_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Tr_1() { return static_cast<int32_t>(offsetof(Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1, ___Tr_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_Tr_1() const { return ___Tr_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_Tr_1() { return &___Tr_1; }
	inline void set_Tr_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___Tr_1 = value;
		Il2CppCodeGenWriteBarrier((&___Tr_1), value);
	}

	inline static int32_t get_offset_of_utm_2() { return static_cast<int32_t>(offsetof(Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1, ___utm_2)); }
	inline PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  get_utm_2() const { return ___utm_2; }
	inline PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D * get_address_of_utm_2() { return &___utm_2; }
	inline void set_utm_2(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  value)
	{
		___utm_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T5134256E17C3C59BE6CBE19E96E7369D7A88D6C1_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef EDITORRESOURCESHOLDERSO_TE30C521C4E4E1D9B76961DCE71712ED165B83287_H
#define EDITORRESOURCESHOLDERSO_TE30C521C4E4E1D9B76961DCE71712ED165B83287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.EditorResourcesHolderSO
struct  EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::RemoveLight
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___RemoveLight_4;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::RemoveDark
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___RemoveDark_5;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::AddLight
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___AddLight_6;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::AddDark
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___AddDark_7;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::RefreshLight
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___RefreshLight_8;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::RefreshDark
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___RefreshDark_9;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::Graph0101BackgroundLight
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Graph0101BackgroundLight_10;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::Graph0101BackgroundDark
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Graph0101BackgroundDark_11;
	// UnityEngine.GUIStyle VertexAnimationTools_30.EditorResourcesHolderSO::QuadIconButtonStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___QuadIconButtonStyle_12;
	// UnityEngine.GUIStyle VertexAnimationTools_30.EditorResourcesHolderSO::TLabel
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___TLabel_13;
	// UnityEngine.GUIStyle VertexAnimationTools_30.EditorResourcesHolderSO::ConstraintLabel
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___ConstraintLabel_14;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::ConstraintIcon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___ConstraintIcon_15;
	// UnityEngine.GUIStyle VertexAnimationTools_30.EditorResourcesHolderSO::ConstraintEditLabel
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___ConstraintEditLabel_16;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::ConstraintEditIcon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___ConstraintEditIcon_17;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::MeshSequenceIcon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___MeshSequenceIcon_18;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::PointCacheIcon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___PointCacheIcon_19;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::RequireReimportLight
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___RequireReimportLight_20;
	// UnityEngine.Texture2D VertexAnimationTools_30.EditorResourcesHolderSO::RequireReimportDark
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___RequireReimportDark_21;
	// UnityEngine.GUIStyle VertexAnimationTools_30.EditorResourcesHolderSO::PressedButton
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___PressedButton_22;
	// UnityEngine.GUIStyle VertexAnimationTools_30.EditorResourcesHolderSO::FoldoutToggle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___FoldoutToggle_23;
	// UnityEngine.GUIStyle VertexAnimationTools_30.EditorResourcesHolderSO::FoldoutToggleDark
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___FoldoutToggleDark_24;

public:
	inline static int32_t get_offset_of_RemoveLight_4() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___RemoveLight_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_RemoveLight_4() const { return ___RemoveLight_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_RemoveLight_4() { return &___RemoveLight_4; }
	inline void set_RemoveLight_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___RemoveLight_4 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveLight_4), value);
	}

	inline static int32_t get_offset_of_RemoveDark_5() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___RemoveDark_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_RemoveDark_5() const { return ___RemoveDark_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_RemoveDark_5() { return &___RemoveDark_5; }
	inline void set_RemoveDark_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___RemoveDark_5 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveDark_5), value);
	}

	inline static int32_t get_offset_of_AddLight_6() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___AddLight_6)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_AddLight_6() const { return ___AddLight_6; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_AddLight_6() { return &___AddLight_6; }
	inline void set_AddLight_6(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___AddLight_6 = value;
		Il2CppCodeGenWriteBarrier((&___AddLight_6), value);
	}

	inline static int32_t get_offset_of_AddDark_7() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___AddDark_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_AddDark_7() const { return ___AddDark_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_AddDark_7() { return &___AddDark_7; }
	inline void set_AddDark_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___AddDark_7 = value;
		Il2CppCodeGenWriteBarrier((&___AddDark_7), value);
	}

	inline static int32_t get_offset_of_RefreshLight_8() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___RefreshLight_8)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_RefreshLight_8() const { return ___RefreshLight_8; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_RefreshLight_8() { return &___RefreshLight_8; }
	inline void set_RefreshLight_8(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___RefreshLight_8 = value;
		Il2CppCodeGenWriteBarrier((&___RefreshLight_8), value);
	}

	inline static int32_t get_offset_of_RefreshDark_9() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___RefreshDark_9)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_RefreshDark_9() const { return ___RefreshDark_9; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_RefreshDark_9() { return &___RefreshDark_9; }
	inline void set_RefreshDark_9(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___RefreshDark_9 = value;
		Il2CppCodeGenWriteBarrier((&___RefreshDark_9), value);
	}

	inline static int32_t get_offset_of_Graph0101BackgroundLight_10() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___Graph0101BackgroundLight_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Graph0101BackgroundLight_10() const { return ___Graph0101BackgroundLight_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Graph0101BackgroundLight_10() { return &___Graph0101BackgroundLight_10; }
	inline void set_Graph0101BackgroundLight_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Graph0101BackgroundLight_10 = value;
		Il2CppCodeGenWriteBarrier((&___Graph0101BackgroundLight_10), value);
	}

	inline static int32_t get_offset_of_Graph0101BackgroundDark_11() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___Graph0101BackgroundDark_11)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Graph0101BackgroundDark_11() const { return ___Graph0101BackgroundDark_11; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Graph0101BackgroundDark_11() { return &___Graph0101BackgroundDark_11; }
	inline void set_Graph0101BackgroundDark_11(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Graph0101BackgroundDark_11 = value;
		Il2CppCodeGenWriteBarrier((&___Graph0101BackgroundDark_11), value);
	}

	inline static int32_t get_offset_of_QuadIconButtonStyle_12() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___QuadIconButtonStyle_12)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_QuadIconButtonStyle_12() const { return ___QuadIconButtonStyle_12; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_QuadIconButtonStyle_12() { return &___QuadIconButtonStyle_12; }
	inline void set_QuadIconButtonStyle_12(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___QuadIconButtonStyle_12 = value;
		Il2CppCodeGenWriteBarrier((&___QuadIconButtonStyle_12), value);
	}

	inline static int32_t get_offset_of_TLabel_13() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___TLabel_13)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_TLabel_13() const { return ___TLabel_13; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_TLabel_13() { return &___TLabel_13; }
	inline void set_TLabel_13(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___TLabel_13 = value;
		Il2CppCodeGenWriteBarrier((&___TLabel_13), value);
	}

	inline static int32_t get_offset_of_ConstraintLabel_14() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___ConstraintLabel_14)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_ConstraintLabel_14() const { return ___ConstraintLabel_14; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_ConstraintLabel_14() { return &___ConstraintLabel_14; }
	inline void set_ConstraintLabel_14(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___ConstraintLabel_14 = value;
		Il2CppCodeGenWriteBarrier((&___ConstraintLabel_14), value);
	}

	inline static int32_t get_offset_of_ConstraintIcon_15() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___ConstraintIcon_15)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_ConstraintIcon_15() const { return ___ConstraintIcon_15; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_ConstraintIcon_15() { return &___ConstraintIcon_15; }
	inline void set_ConstraintIcon_15(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___ConstraintIcon_15 = value;
		Il2CppCodeGenWriteBarrier((&___ConstraintIcon_15), value);
	}

	inline static int32_t get_offset_of_ConstraintEditLabel_16() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___ConstraintEditLabel_16)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_ConstraintEditLabel_16() const { return ___ConstraintEditLabel_16; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_ConstraintEditLabel_16() { return &___ConstraintEditLabel_16; }
	inline void set_ConstraintEditLabel_16(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___ConstraintEditLabel_16 = value;
		Il2CppCodeGenWriteBarrier((&___ConstraintEditLabel_16), value);
	}

	inline static int32_t get_offset_of_ConstraintEditIcon_17() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___ConstraintEditIcon_17)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_ConstraintEditIcon_17() const { return ___ConstraintEditIcon_17; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_ConstraintEditIcon_17() { return &___ConstraintEditIcon_17; }
	inline void set_ConstraintEditIcon_17(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___ConstraintEditIcon_17 = value;
		Il2CppCodeGenWriteBarrier((&___ConstraintEditIcon_17), value);
	}

	inline static int32_t get_offset_of_MeshSequenceIcon_18() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___MeshSequenceIcon_18)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_MeshSequenceIcon_18() const { return ___MeshSequenceIcon_18; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_MeshSequenceIcon_18() { return &___MeshSequenceIcon_18; }
	inline void set_MeshSequenceIcon_18(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___MeshSequenceIcon_18 = value;
		Il2CppCodeGenWriteBarrier((&___MeshSequenceIcon_18), value);
	}

	inline static int32_t get_offset_of_PointCacheIcon_19() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___PointCacheIcon_19)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_PointCacheIcon_19() const { return ___PointCacheIcon_19; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_PointCacheIcon_19() { return &___PointCacheIcon_19; }
	inline void set_PointCacheIcon_19(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___PointCacheIcon_19 = value;
		Il2CppCodeGenWriteBarrier((&___PointCacheIcon_19), value);
	}

	inline static int32_t get_offset_of_RequireReimportLight_20() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___RequireReimportLight_20)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_RequireReimportLight_20() const { return ___RequireReimportLight_20; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_RequireReimportLight_20() { return &___RequireReimportLight_20; }
	inline void set_RequireReimportLight_20(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___RequireReimportLight_20 = value;
		Il2CppCodeGenWriteBarrier((&___RequireReimportLight_20), value);
	}

	inline static int32_t get_offset_of_RequireReimportDark_21() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___RequireReimportDark_21)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_RequireReimportDark_21() const { return ___RequireReimportDark_21; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_RequireReimportDark_21() { return &___RequireReimportDark_21; }
	inline void set_RequireReimportDark_21(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___RequireReimportDark_21 = value;
		Il2CppCodeGenWriteBarrier((&___RequireReimportDark_21), value);
	}

	inline static int32_t get_offset_of_PressedButton_22() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___PressedButton_22)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_PressedButton_22() const { return ___PressedButton_22; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_PressedButton_22() { return &___PressedButton_22; }
	inline void set_PressedButton_22(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___PressedButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___PressedButton_22), value);
	}

	inline static int32_t get_offset_of_FoldoutToggle_23() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___FoldoutToggle_23)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_FoldoutToggle_23() const { return ___FoldoutToggle_23; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_FoldoutToggle_23() { return &___FoldoutToggle_23; }
	inline void set_FoldoutToggle_23(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___FoldoutToggle_23 = value;
		Il2CppCodeGenWriteBarrier((&___FoldoutToggle_23), value);
	}

	inline static int32_t get_offset_of_FoldoutToggleDark_24() { return static_cast<int32_t>(offsetof(EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287, ___FoldoutToggleDark_24)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_FoldoutToggleDark_24() const { return ___FoldoutToggleDark_24; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_FoldoutToggleDark_24() { return &___FoldoutToggleDark_24; }
	inline void set_FoldoutToggleDark_24(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___FoldoutToggleDark_24 = value;
		Il2CppCodeGenWriteBarrier((&___FoldoutToggleDark_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORRESOURCESHOLDERSO_TE30C521C4E4E1D9B76961DCE71712ED165B83287_H
#ifndef IMPORTSETTINGS_TD2BA041D78A8DB7465355499EAFEC198F62CBA2D_H
#define IMPORTSETTINGS_TD2BA041D78A8DB7465355499EAFEC198F62CBA2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MeshSequence_ImportSettings
struct  ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D 
{
public:
	// System.String VertexAnimationTools_30.MeshSequence_ImportSettings::PathToObj
	String_t* ___PathToObj_0;
	// VertexAnimationTools_30.MeshSequenceInfo VertexAnimationTools_30.MeshSequence_ImportSettings::MSI
	MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C  ___MSI_1;
	// System.Boolean VertexAnimationTools_30.MeshSequence_ImportSettings::ImportCustomRange
	bool ___ImportCustomRange_2;
	// System.Int32 VertexAnimationTools_30.MeshSequence_ImportSettings::ImportFromFrame
	int32_t ___ImportFromFrame_3;
	// System.Int32 VertexAnimationTools_30.MeshSequence_ImportSettings::ImportToFrame
	int32_t ___ImportToFrame_4;
	// System.Boolean VertexAnimationTools_30.MeshSequence_ImportSettings::SwapYZAxis
	bool ___SwapYZAxis_5;
	// UnityEngine.Vector3 VertexAnimationTools_30.MeshSequence_ImportSettings::PivotOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___PivotOffset_6;
	// System.Single VertexAnimationTools_30.MeshSequence_ImportSettings::ScaleFactor
	float ___ScaleFactor_7;
	// System.Boolean VertexAnimationTools_30.MeshSequence_ImportSettings::FlipNormals
	bool ___FlipNormals_8;
	// System.Boolean VertexAnimationTools_30.MeshSequence_ImportSettings::ImportUV
	bool ___ImportUV_9;
	// System.Boolean VertexAnimationTools_30.MeshSequence_ImportSettings::CalculateNormals
	bool ___CalculateNormals_10;
	// System.Boolean VertexAnimationTools_30.MeshSequence_ImportSettings::CalculateTangents
	bool ___CalculateTangents_11;
	// VertexAnimationTools_30.ObjData_SmoothingGroupImportModeEnum VertexAnimationTools_30.MeshSequence_ImportSettings::SmoothingGroupImportMode
	int32_t ___SmoothingGroupImportMode_12;
	// VertexAnimationTools_30.ObjData_NormalsRecalculationModeEnum VertexAnimationTools_30.MeshSequence_ImportSettings::NormalRecalculationMode
	int32_t ___NormalRecalculationMode_13;
	// System.Int32 VertexAnimationTools_30.MeshSequence_ImportSettings::MeshCompression
	int32_t ___MeshCompression_14;
	// System.Boolean VertexAnimationTools_30.MeshSequence_ImportSettings::OptimizeMesh
	bool ___OptimizeMesh_15;
	// UnityEngine.Rendering.IndexFormat VertexAnimationTools_30.MeshSequence_ImportSettings::IndexFormat
	int32_t ___IndexFormat_16;
	// System.Boolean VertexAnimationTools_30.MeshSequence_ImportSettings::GenerateMaterials
	bool ___GenerateMaterials_17;
	// VertexAnimationTools_30.ObjData_VertexColorsSettings VertexAnimationTools_30.MeshSequence_ImportSettings::VColorSettings
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B  ___VColorSettings_18;
	// System.Single VertexAnimationTools_30.MeshSequence_ImportSettings::NormalizedPerFrame
	float ___NormalizedPerFrame_19;
	// VertexAnimationTools_30.MeshSequenceInfo_SortModeEnum VertexAnimationTools_30.MeshSequence_ImportSettings::FilesSortMode
	int32_t ___FilesSortMode_20;
	// System.String VertexAnimationTools_30.MeshSequence_ImportSettings::ImportDate
	String_t* ___ImportDate_21;

public:
	inline static int32_t get_offset_of_PathToObj_0() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___PathToObj_0)); }
	inline String_t* get_PathToObj_0() const { return ___PathToObj_0; }
	inline String_t** get_address_of_PathToObj_0() { return &___PathToObj_0; }
	inline void set_PathToObj_0(String_t* value)
	{
		___PathToObj_0 = value;
		Il2CppCodeGenWriteBarrier((&___PathToObj_0), value);
	}

	inline static int32_t get_offset_of_MSI_1() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___MSI_1)); }
	inline MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C  get_MSI_1() const { return ___MSI_1; }
	inline MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C * get_address_of_MSI_1() { return &___MSI_1; }
	inline void set_MSI_1(MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C  value)
	{
		___MSI_1 = value;
	}

	inline static int32_t get_offset_of_ImportCustomRange_2() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___ImportCustomRange_2)); }
	inline bool get_ImportCustomRange_2() const { return ___ImportCustomRange_2; }
	inline bool* get_address_of_ImportCustomRange_2() { return &___ImportCustomRange_2; }
	inline void set_ImportCustomRange_2(bool value)
	{
		___ImportCustomRange_2 = value;
	}

	inline static int32_t get_offset_of_ImportFromFrame_3() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___ImportFromFrame_3)); }
	inline int32_t get_ImportFromFrame_3() const { return ___ImportFromFrame_3; }
	inline int32_t* get_address_of_ImportFromFrame_3() { return &___ImportFromFrame_3; }
	inline void set_ImportFromFrame_3(int32_t value)
	{
		___ImportFromFrame_3 = value;
	}

	inline static int32_t get_offset_of_ImportToFrame_4() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___ImportToFrame_4)); }
	inline int32_t get_ImportToFrame_4() const { return ___ImportToFrame_4; }
	inline int32_t* get_address_of_ImportToFrame_4() { return &___ImportToFrame_4; }
	inline void set_ImportToFrame_4(int32_t value)
	{
		___ImportToFrame_4 = value;
	}

	inline static int32_t get_offset_of_SwapYZAxis_5() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___SwapYZAxis_5)); }
	inline bool get_SwapYZAxis_5() const { return ___SwapYZAxis_5; }
	inline bool* get_address_of_SwapYZAxis_5() { return &___SwapYZAxis_5; }
	inline void set_SwapYZAxis_5(bool value)
	{
		___SwapYZAxis_5 = value;
	}

	inline static int32_t get_offset_of_PivotOffset_6() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___PivotOffset_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_PivotOffset_6() const { return ___PivotOffset_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_PivotOffset_6() { return &___PivotOffset_6; }
	inline void set_PivotOffset_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___PivotOffset_6 = value;
	}

	inline static int32_t get_offset_of_ScaleFactor_7() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___ScaleFactor_7)); }
	inline float get_ScaleFactor_7() const { return ___ScaleFactor_7; }
	inline float* get_address_of_ScaleFactor_7() { return &___ScaleFactor_7; }
	inline void set_ScaleFactor_7(float value)
	{
		___ScaleFactor_7 = value;
	}

	inline static int32_t get_offset_of_FlipNormals_8() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___FlipNormals_8)); }
	inline bool get_FlipNormals_8() const { return ___FlipNormals_8; }
	inline bool* get_address_of_FlipNormals_8() { return &___FlipNormals_8; }
	inline void set_FlipNormals_8(bool value)
	{
		___FlipNormals_8 = value;
	}

	inline static int32_t get_offset_of_ImportUV_9() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___ImportUV_9)); }
	inline bool get_ImportUV_9() const { return ___ImportUV_9; }
	inline bool* get_address_of_ImportUV_9() { return &___ImportUV_9; }
	inline void set_ImportUV_9(bool value)
	{
		___ImportUV_9 = value;
	}

	inline static int32_t get_offset_of_CalculateNormals_10() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___CalculateNormals_10)); }
	inline bool get_CalculateNormals_10() const { return ___CalculateNormals_10; }
	inline bool* get_address_of_CalculateNormals_10() { return &___CalculateNormals_10; }
	inline void set_CalculateNormals_10(bool value)
	{
		___CalculateNormals_10 = value;
	}

	inline static int32_t get_offset_of_CalculateTangents_11() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___CalculateTangents_11)); }
	inline bool get_CalculateTangents_11() const { return ___CalculateTangents_11; }
	inline bool* get_address_of_CalculateTangents_11() { return &___CalculateTangents_11; }
	inline void set_CalculateTangents_11(bool value)
	{
		___CalculateTangents_11 = value;
	}

	inline static int32_t get_offset_of_SmoothingGroupImportMode_12() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___SmoothingGroupImportMode_12)); }
	inline int32_t get_SmoothingGroupImportMode_12() const { return ___SmoothingGroupImportMode_12; }
	inline int32_t* get_address_of_SmoothingGroupImportMode_12() { return &___SmoothingGroupImportMode_12; }
	inline void set_SmoothingGroupImportMode_12(int32_t value)
	{
		___SmoothingGroupImportMode_12 = value;
	}

	inline static int32_t get_offset_of_NormalRecalculationMode_13() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___NormalRecalculationMode_13)); }
	inline int32_t get_NormalRecalculationMode_13() const { return ___NormalRecalculationMode_13; }
	inline int32_t* get_address_of_NormalRecalculationMode_13() { return &___NormalRecalculationMode_13; }
	inline void set_NormalRecalculationMode_13(int32_t value)
	{
		___NormalRecalculationMode_13 = value;
	}

	inline static int32_t get_offset_of_MeshCompression_14() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___MeshCompression_14)); }
	inline int32_t get_MeshCompression_14() const { return ___MeshCompression_14; }
	inline int32_t* get_address_of_MeshCompression_14() { return &___MeshCompression_14; }
	inline void set_MeshCompression_14(int32_t value)
	{
		___MeshCompression_14 = value;
	}

	inline static int32_t get_offset_of_OptimizeMesh_15() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___OptimizeMesh_15)); }
	inline bool get_OptimizeMesh_15() const { return ___OptimizeMesh_15; }
	inline bool* get_address_of_OptimizeMesh_15() { return &___OptimizeMesh_15; }
	inline void set_OptimizeMesh_15(bool value)
	{
		___OptimizeMesh_15 = value;
	}

	inline static int32_t get_offset_of_IndexFormat_16() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___IndexFormat_16)); }
	inline int32_t get_IndexFormat_16() const { return ___IndexFormat_16; }
	inline int32_t* get_address_of_IndexFormat_16() { return &___IndexFormat_16; }
	inline void set_IndexFormat_16(int32_t value)
	{
		___IndexFormat_16 = value;
	}

	inline static int32_t get_offset_of_GenerateMaterials_17() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___GenerateMaterials_17)); }
	inline bool get_GenerateMaterials_17() const { return ___GenerateMaterials_17; }
	inline bool* get_address_of_GenerateMaterials_17() { return &___GenerateMaterials_17; }
	inline void set_GenerateMaterials_17(bool value)
	{
		___GenerateMaterials_17 = value;
	}

	inline static int32_t get_offset_of_VColorSettings_18() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___VColorSettings_18)); }
	inline VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B  get_VColorSettings_18() const { return ___VColorSettings_18; }
	inline VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B * get_address_of_VColorSettings_18() { return &___VColorSettings_18; }
	inline void set_VColorSettings_18(VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B  value)
	{
		___VColorSettings_18 = value;
	}

	inline static int32_t get_offset_of_NormalizedPerFrame_19() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___NormalizedPerFrame_19)); }
	inline float get_NormalizedPerFrame_19() const { return ___NormalizedPerFrame_19; }
	inline float* get_address_of_NormalizedPerFrame_19() { return &___NormalizedPerFrame_19; }
	inline void set_NormalizedPerFrame_19(float value)
	{
		___NormalizedPerFrame_19 = value;
	}

	inline static int32_t get_offset_of_FilesSortMode_20() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___FilesSortMode_20)); }
	inline int32_t get_FilesSortMode_20() const { return ___FilesSortMode_20; }
	inline int32_t* get_address_of_FilesSortMode_20() { return &___FilesSortMode_20; }
	inline void set_FilesSortMode_20(int32_t value)
	{
		___FilesSortMode_20 = value;
	}

	inline static int32_t get_offset_of_ImportDate_21() { return static_cast<int32_t>(offsetof(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D, ___ImportDate_21)); }
	inline String_t* get_ImportDate_21() const { return ___ImportDate_21; }
	inline String_t** get_address_of_ImportDate_21() { return &___ImportDate_21; }
	inline void set_ImportDate_21(String_t* value)
	{
		___ImportDate_21 = value;
		Il2CppCodeGenWriteBarrier((&___ImportDate_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VertexAnimationTools_30.MeshSequence/ImportSettings
struct ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D_marshaled_pinvoke
{
	char* ___PathToObj_0;
	MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C_marshaled_pinvoke ___MSI_1;
	int32_t ___ImportCustomRange_2;
	int32_t ___ImportFromFrame_3;
	int32_t ___ImportToFrame_4;
	int32_t ___SwapYZAxis_5;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___PivotOffset_6;
	float ___ScaleFactor_7;
	int32_t ___FlipNormals_8;
	int32_t ___ImportUV_9;
	int32_t ___CalculateNormals_10;
	int32_t ___CalculateTangents_11;
	int32_t ___SmoothingGroupImportMode_12;
	int32_t ___NormalRecalculationMode_13;
	int32_t ___MeshCompression_14;
	int32_t ___OptimizeMesh_15;
	int32_t ___IndexFormat_16;
	int32_t ___GenerateMaterials_17;
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B_marshaled_pinvoke ___VColorSettings_18;
	float ___NormalizedPerFrame_19;
	int32_t ___FilesSortMode_20;
	char* ___ImportDate_21;
};
// Native definition for COM marshalling of VertexAnimationTools_30.MeshSequence/ImportSettings
struct ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D_marshaled_com
{
	Il2CppChar* ___PathToObj_0;
	MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C_marshaled_com ___MSI_1;
	int32_t ___ImportCustomRange_2;
	int32_t ___ImportFromFrame_3;
	int32_t ___ImportToFrame_4;
	int32_t ___SwapYZAxis_5;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___PivotOffset_6;
	float ___ScaleFactor_7;
	int32_t ___FlipNormals_8;
	int32_t ___ImportUV_9;
	int32_t ___CalculateNormals_10;
	int32_t ___CalculateTangents_11;
	int32_t ___SmoothingGroupImportMode_12;
	int32_t ___NormalRecalculationMode_13;
	int32_t ___MeshCompression_14;
	int32_t ___OptimizeMesh_15;
	int32_t ___IndexFormat_16;
	int32_t ___GenerateMaterials_17;
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B_marshaled_com ___VColorSettings_18;
	float ___NormalizedPerFrame_19;
	int32_t ___FilesSortMode_20;
	Il2CppChar* ___ImportDate_21;
};
#endif // IMPORTSETTINGS_TD2BA041D78A8DB7465355499EAFEC198F62CBA2D_H
#ifndef UMESHVERTEX_TE2BF83A5C98E610175CE747D6657859E6EBD37CC_H
#define UMESHVERTEX_TE2BF83A5C98E610175CE747D6657859E6EBD37CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ObjData_UMeshVertex
struct  UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.ObjData_UMeshVertex::ThisIdx
	int32_t ___ThisIdx_0;
	// VertexAnimationTools_30.ObjData_PositionVertex VertexAnimationTools_30.ObjData_UMeshVertex::Position
	PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 * ___Position_1;
	// VertexAnimationTools_30.ObjData_NormalVertex VertexAnimationTools_30.ObjData_UMeshVertex::Normal
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D * ___Normal_2;
	// VertexAnimationTools_30.ObjData_MapVertex VertexAnimationTools_30.ObjData_UMeshVertex::UV
	MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 * ___UV_3;
	// VertexAnimationTools_30.BindInfo VertexAnimationTools_30.ObjData_UMeshVertex::Bi
	BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8  ___Bi_4;

public:
	inline static int32_t get_offset_of_ThisIdx_0() { return static_cast<int32_t>(offsetof(UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC, ___ThisIdx_0)); }
	inline int32_t get_ThisIdx_0() const { return ___ThisIdx_0; }
	inline int32_t* get_address_of_ThisIdx_0() { return &___ThisIdx_0; }
	inline void set_ThisIdx_0(int32_t value)
	{
		___ThisIdx_0 = value;
	}

	inline static int32_t get_offset_of_Position_1() { return static_cast<int32_t>(offsetof(UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC, ___Position_1)); }
	inline PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 * get_Position_1() const { return ___Position_1; }
	inline PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 ** get_address_of_Position_1() { return &___Position_1; }
	inline void set_Position_1(PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186 * value)
	{
		___Position_1 = value;
		Il2CppCodeGenWriteBarrier((&___Position_1), value);
	}

	inline static int32_t get_offset_of_Normal_2() { return static_cast<int32_t>(offsetof(UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC, ___Normal_2)); }
	inline NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D * get_Normal_2() const { return ___Normal_2; }
	inline NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D ** get_address_of_Normal_2() { return &___Normal_2; }
	inline void set_Normal_2(NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D * value)
	{
		___Normal_2 = value;
		Il2CppCodeGenWriteBarrier((&___Normal_2), value);
	}

	inline static int32_t get_offset_of_UV_3() { return static_cast<int32_t>(offsetof(UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC, ___UV_3)); }
	inline MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 * get_UV_3() const { return ___UV_3; }
	inline MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 ** get_address_of_UV_3() { return &___UV_3; }
	inline void set_UV_3(MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8 * value)
	{
		___UV_3 = value;
		Il2CppCodeGenWriteBarrier((&___UV_3), value);
	}

	inline static int32_t get_offset_of_Bi_4() { return static_cast<int32_t>(offsetof(UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC, ___Bi_4)); }
	inline BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8  get_Bi_4() const { return ___Bi_4; }
	inline BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8 * get_address_of_Bi_4() { return &___Bi_4; }
	inline void set_Bi_4(BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8  value)
	{
		___Bi_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UMESHVERTEX_TE2BF83A5C98E610175CE747D6657859E6EBD37CC_H
#ifndef CLIP_T60B69E5DD4C63C3D061B39C5EDCECD99F3F36553_H
#define CLIP_T60B69E5DD4C63C3D061B39C5EDCECD99F3F36553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCache_Clip
struct  Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553  : public RuntimeObject
{
public:
	// VertexAnimationTools_30.PointCache_Clip_ImportSettings VertexAnimationTools_30.PointCache_Clip::PreImport
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE  ___PreImport_0;
	// VertexAnimationTools_30.PointCache_Clip_ImportSettings VertexAnimationTools_30.PointCache_Clip::PostImport
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE  ___PostImport_1;
	// System.Boolean VertexAnimationTools_30.PointCache_Clip::FoldoutState
	bool ___FoldoutState_2;
	// System.Int32 VertexAnimationTools_30.PointCache_Clip::MotionPathsCount
	int32_t ___MotionPathsCount_3;
	// UnityEngine.Vector3[] VertexAnimationTools_30.PointCache_Clip::MotionPathVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___MotionPathVertices_4;

public:
	inline static int32_t get_offset_of_PreImport_0() { return static_cast<int32_t>(offsetof(Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553, ___PreImport_0)); }
	inline ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE  get_PreImport_0() const { return ___PreImport_0; }
	inline ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE * get_address_of_PreImport_0() { return &___PreImport_0; }
	inline void set_PreImport_0(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE  value)
	{
		___PreImport_0 = value;
	}

	inline static int32_t get_offset_of_PostImport_1() { return static_cast<int32_t>(offsetof(Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553, ___PostImport_1)); }
	inline ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE  get_PostImport_1() const { return ___PostImport_1; }
	inline ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE * get_address_of_PostImport_1() { return &___PostImport_1; }
	inline void set_PostImport_1(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE  value)
	{
		___PostImport_1 = value;
	}

	inline static int32_t get_offset_of_FoldoutState_2() { return static_cast<int32_t>(offsetof(Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553, ___FoldoutState_2)); }
	inline bool get_FoldoutState_2() const { return ___FoldoutState_2; }
	inline bool* get_address_of_FoldoutState_2() { return &___FoldoutState_2; }
	inline void set_FoldoutState_2(bool value)
	{
		___FoldoutState_2 = value;
	}

	inline static int32_t get_offset_of_MotionPathsCount_3() { return static_cast<int32_t>(offsetof(Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553, ___MotionPathsCount_3)); }
	inline int32_t get_MotionPathsCount_3() const { return ___MotionPathsCount_3; }
	inline int32_t* get_address_of_MotionPathsCount_3() { return &___MotionPathsCount_3; }
	inline void set_MotionPathsCount_3(int32_t value)
	{
		___MotionPathsCount_3 = value;
	}

	inline static int32_t get_offset_of_MotionPathVertices_4() { return static_cast<int32_t>(offsetof(Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553, ___MotionPathVertices_4)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_MotionPathVertices_4() const { return ___MotionPathVertices_4; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_MotionPathVertices_4() { return &___MotionPathVertices_4; }
	inline void set_MotionPathVertices_4(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___MotionPathVertices_4 = value;
		Il2CppCodeGenWriteBarrier((&___MotionPathVertices_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIP_T60B69E5DD4C63C3D061B39C5EDCECD99F3F36553_H
#ifndef PREIMPORTCONSTRAINT_T15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC_H
#define PREIMPORTCONSTRAINT_T15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCache_PreImportConstraint
struct  PreImportConstraint_t15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.PointCache_PreImportConstraint::Name
	String_t* ___Name_0;
	// VertexAnimationTools_30.PFU VertexAnimationTools_30.PointCache_PreImportConstraint::ObjSpace
	PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  ___ObjSpace_1;
	// VertexAnimationTools_30.BindInfo VertexAnimationTools_30.PointCache_PreImportConstraint::BI
	BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8  ___BI_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(PreImportConstraint_t15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ObjSpace_1() { return static_cast<int32_t>(offsetof(PreImportConstraint_t15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC, ___ObjSpace_1)); }
	inline PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  get_ObjSpace_1() const { return ___ObjSpace_1; }
	inline PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D * get_address_of_ObjSpace_1() { return &___ObjSpace_1; }
	inline void set_ObjSpace_1(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  value)
	{
		___ObjSpace_1 = value;
	}

	inline static int32_t get_offset_of_BI_2() { return static_cast<int32_t>(offsetof(PreImportConstraint_t15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC, ___BI_2)); }
	inline BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8  get_BI_2() const { return ___BI_2; }
	inline BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8 * get_address_of_BI_2() { return &___BI_2; }
	inline void set_BI_2(BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8  value)
	{
		___BI_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREIMPORTCONSTRAINT_T15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC_H
#ifndef POINTCACHEDATA_T5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5_H
#define POINTCACHEDATA_T5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCacheData
struct  PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.PointCacheData::Name
	String_t* ___Name_0;
	// VertexAnimationTools_30.FramesArray VertexAnimationTools_30.PointCacheData::Frames
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * ___Frames_1;
	// VertexAnimationTools_30.PointCache_Clip VertexAnimationTools_30.PointCacheData::clip
	Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553 * ___clip_2;
	// VertexAnimationTools_30.PointCache_Clip_ImportSettings VertexAnimationTools_30.PointCacheData::cis
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE  ___cis_3;
	// VertexAnimationTools_30.PointCache VertexAnimationTools_30.PointCacheData::pointCache
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * ___pointCache_4;
	// System.Int32 VertexAnimationTools_30.PointCacheData::geometryVerticesCount
	int32_t ___geometryVerticesCount_5;
	// System.Boolean VertexAnimationTools_30.PointCacheData::IsMeshSequence
	bool ___IsMeshSequence_6;
	// VertexAnimationTools_30.TasksStack VertexAnimationTools_30.PointCacheData::tstack
	TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * ___tstack_7;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Frames_1() { return static_cast<int32_t>(offsetof(PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5, ___Frames_1)); }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * get_Frames_1() const { return ___Frames_1; }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 ** get_address_of_Frames_1() { return &___Frames_1; }
	inline void set_Frames_1(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * value)
	{
		___Frames_1 = value;
		Il2CppCodeGenWriteBarrier((&___Frames_1), value);
	}

	inline static int32_t get_offset_of_clip_2() { return static_cast<int32_t>(offsetof(PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5, ___clip_2)); }
	inline Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553 * get_clip_2() const { return ___clip_2; }
	inline Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553 ** get_address_of_clip_2() { return &___clip_2; }
	inline void set_clip_2(Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553 * value)
	{
		___clip_2 = value;
		Il2CppCodeGenWriteBarrier((&___clip_2), value);
	}

	inline static int32_t get_offset_of_cis_3() { return static_cast<int32_t>(offsetof(PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5, ___cis_3)); }
	inline ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE  get_cis_3() const { return ___cis_3; }
	inline ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE * get_address_of_cis_3() { return &___cis_3; }
	inline void set_cis_3(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE  value)
	{
		___cis_3 = value;
	}

	inline static int32_t get_offset_of_pointCache_4() { return static_cast<int32_t>(offsetof(PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5, ___pointCache_4)); }
	inline PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * get_pointCache_4() const { return ___pointCache_4; }
	inline PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 ** get_address_of_pointCache_4() { return &___pointCache_4; }
	inline void set_pointCache_4(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * value)
	{
		___pointCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCache_4), value);
	}

	inline static int32_t get_offset_of_geometryVerticesCount_5() { return static_cast<int32_t>(offsetof(PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5, ___geometryVerticesCount_5)); }
	inline int32_t get_geometryVerticesCount_5() const { return ___geometryVerticesCount_5; }
	inline int32_t* get_address_of_geometryVerticesCount_5() { return &___geometryVerticesCount_5; }
	inline void set_geometryVerticesCount_5(int32_t value)
	{
		___geometryVerticesCount_5 = value;
	}

	inline static int32_t get_offset_of_IsMeshSequence_6() { return static_cast<int32_t>(offsetof(PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5, ___IsMeshSequence_6)); }
	inline bool get_IsMeshSequence_6() const { return ___IsMeshSequence_6; }
	inline bool* get_address_of_IsMeshSequence_6() { return &___IsMeshSequence_6; }
	inline void set_IsMeshSequence_6(bool value)
	{
		___IsMeshSequence_6 = value;
	}

	inline static int32_t get_offset_of_tstack_7() { return static_cast<int32_t>(offsetof(PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5, ___tstack_7)); }
	inline TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * get_tstack_7() const { return ___tstack_7; }
	inline TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F ** get_address_of_tstack_7() { return &___tstack_7; }
	inline void set_tstack_7(TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * value)
	{
		___tstack_7 = value;
		Il2CppCodeGenWriteBarrier((&___tstack_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCACHEDATA_T5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5_H
#ifndef U3CBUILDU3ED__10_TD0C96D53FA455C6CBB66EFA452911C69D3616E62_H
#define U3CBUILDU3ED__10_TD0C96D53FA455C6CBB66EFA452911C69D3616E62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCacheData_<Build>d__10
struct  U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.PointCacheData_<Build>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// VertexAnimationTools_30.TaskInfo VertexAnimationTools_30.PointCacheData_<Build>d__10::<>2__current
	TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  ___U3CU3E2__current_1;
	// System.Int32 VertexAnimationTools_30.PointCacheData_<Build>d__10::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// VertexAnimationTools_30.PointCacheData VertexAnimationTools_30.PointCacheData_<Build>d__10::<>4__this
	PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5 * ___U3CU3E4__this_3;
	// VertexAnimationTools_30.FramesArray VertexAnimationTools_30.PointCacheData_<Build>d__10::<TransitionFrames>5__2
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * ___U3CTransitionFramesU3E5__2_4;
	// System.Int32 VertexAnimationTools_30.PointCacheData_<Build>d__10::<readFramesCounter>5__3
	int32_t ___U3CreadFramesCounterU3E5__3_5;
	// VertexAnimationTools_30.MeshSequenceInfo VertexAnimationTools_30.PointCacheData_<Build>d__10::<msi>5__4
	MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C  ___U3CmsiU3E5__4_6;
	// System.Int32 VertexAnimationTools_30.PointCacheData_<Build>d__10::<f>5__5
	int32_t ___U3CfU3E5__5_7;
	// UnityEngine.Vector3[] VertexAnimationTools_30.PointCacheData_<Build>d__10::<objVertices>5__6
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___U3CobjVerticesU3E5__6_8;
	// System.IO.FileStream VertexAnimationTools_30.PointCacheData_<Build>d__10::<fs>5__7
	FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * ___U3CfsU3E5__7_9;
	// System.IO.BinaryReader VertexAnimationTools_30.PointCacheData_<Build>d__10::<binReader>5__8
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * ___U3CbinReaderU3E5__8_10;
	// VertexAnimationTools_30.FramesArray VertexAnimationTools_30.PointCacheData_<Build>d__10::<retimedFrames>5__9
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * ___U3CretimedFramesU3E5__9_11;
	// System.Single VertexAnimationTools_30.PointCacheData_<Build>d__10::<step>5__10
	float ___U3CstepU3E5__10_12;
	// System.Collections.Generic.IEnumerator`1<VertexAnimationTools_30.TaskInfo> VertexAnimationTools_30.PointCacheData_<Build>d__10::<>7__wrap10
	RuntimeObject* ___U3CU3E7__wrap10_13;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> VertexAnimationTools_30.PointCacheData_<Build>d__10::<mPathVertList>5__12
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___U3CmPathVertListU3E5__12_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CU3E2__current_1)); }
	inline TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CU3E4__this_3)); }
	inline PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CTransitionFramesU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CTransitionFramesU3E5__2_4)); }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * get_U3CTransitionFramesU3E5__2_4() const { return ___U3CTransitionFramesU3E5__2_4; }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 ** get_address_of_U3CTransitionFramesU3E5__2_4() { return &___U3CTransitionFramesU3E5__2_4; }
	inline void set_U3CTransitionFramesU3E5__2_4(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * value)
	{
		___U3CTransitionFramesU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTransitionFramesU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CreadFramesCounterU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CreadFramesCounterU3E5__3_5)); }
	inline int32_t get_U3CreadFramesCounterU3E5__3_5() const { return ___U3CreadFramesCounterU3E5__3_5; }
	inline int32_t* get_address_of_U3CreadFramesCounterU3E5__3_5() { return &___U3CreadFramesCounterU3E5__3_5; }
	inline void set_U3CreadFramesCounterU3E5__3_5(int32_t value)
	{
		___U3CreadFramesCounterU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CmsiU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CmsiU3E5__4_6)); }
	inline MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C  get_U3CmsiU3E5__4_6() const { return ___U3CmsiU3E5__4_6; }
	inline MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C * get_address_of_U3CmsiU3E5__4_6() { return &___U3CmsiU3E5__4_6; }
	inline void set_U3CmsiU3E5__4_6(MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C  value)
	{
		___U3CmsiU3E5__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CfU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CfU3E5__5_7)); }
	inline int32_t get_U3CfU3E5__5_7() const { return ___U3CfU3E5__5_7; }
	inline int32_t* get_address_of_U3CfU3E5__5_7() { return &___U3CfU3E5__5_7; }
	inline void set_U3CfU3E5__5_7(int32_t value)
	{
		___U3CfU3E5__5_7 = value;
	}

	inline static int32_t get_offset_of_U3CobjVerticesU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CobjVerticesU3E5__6_8)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_U3CobjVerticesU3E5__6_8() const { return ___U3CobjVerticesU3E5__6_8; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_U3CobjVerticesU3E5__6_8() { return &___U3CobjVerticesU3E5__6_8; }
	inline void set_U3CobjVerticesU3E5__6_8(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___U3CobjVerticesU3E5__6_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjVerticesU3E5__6_8), value);
	}

	inline static int32_t get_offset_of_U3CfsU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CfsU3E5__7_9)); }
	inline FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * get_U3CfsU3E5__7_9() const { return ___U3CfsU3E5__7_9; }
	inline FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 ** get_address_of_U3CfsU3E5__7_9() { return &___U3CfsU3E5__7_9; }
	inline void set_U3CfsU3E5__7_9(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * value)
	{
		___U3CfsU3E5__7_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfsU3E5__7_9), value);
	}

	inline static int32_t get_offset_of_U3CbinReaderU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CbinReaderU3E5__8_10)); }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * get_U3CbinReaderU3E5__8_10() const { return ___U3CbinReaderU3E5__8_10; }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 ** get_address_of_U3CbinReaderU3E5__8_10() { return &___U3CbinReaderU3E5__8_10; }
	inline void set_U3CbinReaderU3E5__8_10(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * value)
	{
		___U3CbinReaderU3E5__8_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbinReaderU3E5__8_10), value);
	}

	inline static int32_t get_offset_of_U3CretimedFramesU3E5__9_11() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CretimedFramesU3E5__9_11)); }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * get_U3CretimedFramesU3E5__9_11() const { return ___U3CretimedFramesU3E5__9_11; }
	inline FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 ** get_address_of_U3CretimedFramesU3E5__9_11() { return &___U3CretimedFramesU3E5__9_11; }
	inline void set_U3CretimedFramesU3E5__9_11(FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8 * value)
	{
		___U3CretimedFramesU3E5__9_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CretimedFramesU3E5__9_11), value);
	}

	inline static int32_t get_offset_of_U3CstepU3E5__10_12() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CstepU3E5__10_12)); }
	inline float get_U3CstepU3E5__10_12() const { return ___U3CstepU3E5__10_12; }
	inline float* get_address_of_U3CstepU3E5__10_12() { return &___U3CstepU3E5__10_12; }
	inline void set_U3CstepU3E5__10_12(float value)
	{
		___U3CstepU3E5__10_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap10_13() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CU3E7__wrap10_13)); }
	inline RuntimeObject* get_U3CU3E7__wrap10_13() const { return ___U3CU3E7__wrap10_13; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap10_13() { return &___U3CU3E7__wrap10_13; }
	inline void set_U3CU3E7__wrap10_13(RuntimeObject* value)
	{
		___U3CU3E7__wrap10_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap10_13), value);
	}

	inline static int32_t get_offset_of_U3CmPathVertListU3E5__12_14() { return static_cast<int32_t>(offsetof(U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62, ___U3CmPathVertListU3E5__12_14)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_U3CmPathVertListU3E5__12_14() const { return ___U3CmPathVertListU3E5__12_14; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_U3CmPathVertListU3E5__12_14() { return &___U3CmPathVertListU3E5__12_14; }
	inline void set_U3CmPathVertListU3E5__12_14(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___U3CmPathVertListU3E5__12_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmPathVertListU3E5__12_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUILDU3ED__10_TD0C96D53FA455C6CBB66EFA452911C69D3616E62_H
#ifndef PROJECTIONSAMPLES_T3F2EAB34ED2433A74795DB31B8790F3AFAC3F708_H
#define PROJECTIONSAMPLES_T3F2EAB34ED2433A74795DB31B8790F3AFAC3F708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ProjectionSamples
struct  ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// VertexAnimationTools_30.ProjectionSamples_Samples[] VertexAnimationTools_30.ProjectionSamples::SphereSamples
	SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* ___SphereSamples_4;
	// VertexAnimationTools_30.ProjectionSamples_Samples[] VertexAnimationTools_30.ProjectionSamples::DomeSamples
	SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* ___DomeSamples_5;

public:
	inline static int32_t get_offset_of_SphereSamples_4() { return static_cast<int32_t>(offsetof(ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708, ___SphereSamples_4)); }
	inline SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* get_SphereSamples_4() const { return ___SphereSamples_4; }
	inline SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44** get_address_of_SphereSamples_4() { return &___SphereSamples_4; }
	inline void set_SphereSamples_4(SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* value)
	{
		___SphereSamples_4 = value;
		Il2CppCodeGenWriteBarrier((&___SphereSamples_4), value);
	}

	inline static int32_t get_offset_of_DomeSamples_5() { return static_cast<int32_t>(offsetof(ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708, ___DomeSamples_5)); }
	inline SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* get_DomeSamples_5() const { return ___DomeSamples_5; }
	inline SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44** get_address_of_DomeSamples_5() { return &___DomeSamples_5; }
	inline void set_DomeSamples_5(SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* value)
	{
		___DomeSamples_5 = value;
		Il2CppCodeGenWriteBarrier((&___DomeSamples_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONSAMPLES_T3F2EAB34ED2433A74795DB31B8790F3AFAC3F708_H
#ifndef VERTEXANIMATIONTOOLSASSETBASE_T24D12B84EDE085A896DF88C598AEA5C55A648252_H
#define VERTEXANIMATIONTOOLSASSETBASE_T24D12B84EDE085A896DF88C598AEA5C55A648252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.VertexAnimationToolsAssetBase
struct  VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<VertexAnimationTools_30.MaterialInfo> VertexAnimationTools_30.VertexAnimationToolsAssetBase::Materials
	List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * ___Materials_4;

public:
	inline static int32_t get_offset_of_Materials_4() { return static_cast<int32_t>(offsetof(VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252, ___Materials_4)); }
	inline List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * get_Materials_4() const { return ___Materials_4; }
	inline List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA ** get_address_of_Materials_4() { return &___Materials_4; }
	inline void set_Materials_4(List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * value)
	{
		___Materials_4 = value;
		Il2CppCodeGenWriteBarrier((&___Materials_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXANIMATIONTOOLSASSETBASE_T24D12B84EDE085A896DF88C598AEA5C55A648252_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef MESHSEQUENCE_TCAA5510E0F5678834DCB299C86607D56EC8FC0CA_H
#define MESHSEQUENCE_TCAA5510E0F5678834DCB299C86607D56EC8FC0CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MeshSequence
struct  MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA  : public VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252
{
public:
	// VertexAnimationTools_30.MeshSequence_ImportSettings VertexAnimationTools_30.MeshSequence::PreImport
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D  ___PreImport_5;
	// VertexAnimationTools_30.MeshSequence_ImportSettings VertexAnimationTools_30.MeshSequence::PostImport
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D  ___PostImport_6;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.MeshSequence_Frame> VertexAnimationTools_30.MeshSequence::Frames
	List_1_t111085AD46AD6E1D7CB4FE6A22570DAB13DF787F * ___Frames_7;
	// VertexAnimationTools_30.NumbersRange VertexAnimationTools_30.MeshSequence::SequenceVerticesCount
	NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  ___SequenceVerticesCount_8;
	// VertexAnimationTools_30.NumbersRange VertexAnimationTools_30.MeshSequence::SequenceObjVerticesCount
	NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  ___SequenceObjVerticesCount_9;
	// VertexAnimationTools_30.NumbersRange VertexAnimationTools_30.MeshSequence::SequenceTrianglesCount
	NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  ___SequenceTrianglesCount_10;
	// VertexAnimationTools_30.NumbersRange VertexAnimationTools_30.MeshSequence::SequenceObjPolygonsCount
	NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  ___SequenceObjPolygonsCount_11;
	// VertexAnimationTools_30.NumbersRange VertexAnimationTools_30.MeshSequence::SequenceSubmeshesCount
	NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  ___SequenceSubmeshesCount_12;
	// System.Single VertexAnimationTools_30.MeshSequence::AssetFileSize
	float ___AssetFileSize_13;

public:
	inline static int32_t get_offset_of_PreImport_5() { return static_cast<int32_t>(offsetof(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA, ___PreImport_5)); }
	inline ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D  get_PreImport_5() const { return ___PreImport_5; }
	inline ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D * get_address_of_PreImport_5() { return &___PreImport_5; }
	inline void set_PreImport_5(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D  value)
	{
		___PreImport_5 = value;
	}

	inline static int32_t get_offset_of_PostImport_6() { return static_cast<int32_t>(offsetof(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA, ___PostImport_6)); }
	inline ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D  get_PostImport_6() const { return ___PostImport_6; }
	inline ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D * get_address_of_PostImport_6() { return &___PostImport_6; }
	inline void set_PostImport_6(ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D  value)
	{
		___PostImport_6 = value;
	}

	inline static int32_t get_offset_of_Frames_7() { return static_cast<int32_t>(offsetof(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA, ___Frames_7)); }
	inline List_1_t111085AD46AD6E1D7CB4FE6A22570DAB13DF787F * get_Frames_7() const { return ___Frames_7; }
	inline List_1_t111085AD46AD6E1D7CB4FE6A22570DAB13DF787F ** get_address_of_Frames_7() { return &___Frames_7; }
	inline void set_Frames_7(List_1_t111085AD46AD6E1D7CB4FE6A22570DAB13DF787F * value)
	{
		___Frames_7 = value;
		Il2CppCodeGenWriteBarrier((&___Frames_7), value);
	}

	inline static int32_t get_offset_of_SequenceVerticesCount_8() { return static_cast<int32_t>(offsetof(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA, ___SequenceVerticesCount_8)); }
	inline NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  get_SequenceVerticesCount_8() const { return ___SequenceVerticesCount_8; }
	inline NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775 * get_address_of_SequenceVerticesCount_8() { return &___SequenceVerticesCount_8; }
	inline void set_SequenceVerticesCount_8(NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  value)
	{
		___SequenceVerticesCount_8 = value;
	}

	inline static int32_t get_offset_of_SequenceObjVerticesCount_9() { return static_cast<int32_t>(offsetof(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA, ___SequenceObjVerticesCount_9)); }
	inline NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  get_SequenceObjVerticesCount_9() const { return ___SequenceObjVerticesCount_9; }
	inline NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775 * get_address_of_SequenceObjVerticesCount_9() { return &___SequenceObjVerticesCount_9; }
	inline void set_SequenceObjVerticesCount_9(NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  value)
	{
		___SequenceObjVerticesCount_9 = value;
	}

	inline static int32_t get_offset_of_SequenceTrianglesCount_10() { return static_cast<int32_t>(offsetof(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA, ___SequenceTrianglesCount_10)); }
	inline NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  get_SequenceTrianglesCount_10() const { return ___SequenceTrianglesCount_10; }
	inline NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775 * get_address_of_SequenceTrianglesCount_10() { return &___SequenceTrianglesCount_10; }
	inline void set_SequenceTrianglesCount_10(NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  value)
	{
		___SequenceTrianglesCount_10 = value;
	}

	inline static int32_t get_offset_of_SequenceObjPolygonsCount_11() { return static_cast<int32_t>(offsetof(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA, ___SequenceObjPolygonsCount_11)); }
	inline NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  get_SequenceObjPolygonsCount_11() const { return ___SequenceObjPolygonsCount_11; }
	inline NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775 * get_address_of_SequenceObjPolygonsCount_11() { return &___SequenceObjPolygonsCount_11; }
	inline void set_SequenceObjPolygonsCount_11(NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  value)
	{
		___SequenceObjPolygonsCount_11 = value;
	}

	inline static int32_t get_offset_of_SequenceSubmeshesCount_12() { return static_cast<int32_t>(offsetof(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA, ___SequenceSubmeshesCount_12)); }
	inline NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  get_SequenceSubmeshesCount_12() const { return ___SequenceSubmeshesCount_12; }
	inline NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775 * get_address_of_SequenceSubmeshesCount_12() { return &___SequenceSubmeshesCount_12; }
	inline void set_SequenceSubmeshesCount_12(NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775  value)
	{
		___SequenceSubmeshesCount_12 = value;
	}

	inline static int32_t get_offset_of_AssetFileSize_13() { return static_cast<int32_t>(offsetof(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA, ___AssetFileSize_13)); }
	inline float get_AssetFileSize_13() const { return ___AssetFileSize_13; }
	inline float* get_address_of_AssetFileSize_13() { return &___AssetFileSize_13; }
	inline void set_AssetFileSize_13(float value)
	{
		___AssetFileSize_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHSEQUENCE_TCAA5510E0F5678834DCB299C86607D56EC8FC0CA_H
#ifndef POINTCACHE_T08D0CD88824B54FB717F6B510FF8EA87D1FA49B7_H
#define POINTCACHE_T08D0CD88824B54FB717F6B510FF8EA87D1FA49B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCache
struct  PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7  : public VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252
{
public:
	// VertexAnimationTools_30.PointCache_ImportSettings VertexAnimationTools_30.PointCache::PreImport
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C  ___PreImport_5;
	// VertexAnimationTools_30.PointCache_ImportSettings VertexAnimationTools_30.PointCache::PostImport
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C  ___PostImport_6;
	// VertexAnimationTools_30.PointCache_Clip[] VertexAnimationTools_30.PointCache::Clips
	ClipU5BU5D_t9B7F45E85DBF342DB234CEFF7945F4E5F1F4D3CC* ___Clips_7;
	// VertexAnimationTools_30.PointCache_PolygonMesh[] VertexAnimationTools_30.PointCache::Meshes
	PolygonMeshU5BU5D_t8CB08AC2E64BEDFAF6ABB153CC60B9E09DDF7C52* ___Meshes_8;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.PointCache_PreImportConstraint> VertexAnimationTools_30.PointCache::PreConstraints
	List_1_tCCAD2DF763777A3380885B29580636BEE8FC7280 * ___PreConstraints_9;
	// VertexAnimationTools_30.PointCache_PostImportConstraint[] VertexAnimationTools_30.PointCache::PostConstraints
	PostImportConstraintU5BU5D_t8DAC6BA1EB4063452FAB12533B20372EB8D5C130* ___PostConstraints_10;
	// System.Boolean VertexAnimationTools_30.PointCache::ImportSettingsIsDirtyFlag
	bool ___ImportSettingsIsDirtyFlag_11;
	// System.Single VertexAnimationTools_30.PointCache::ConstraintHandlesSize
	float ___ConstraintHandlesSize_12;
	// System.Boolean VertexAnimationTools_30.PointCache::DrawConstraintHandlesName
	bool ___DrawConstraintHandlesName_13;
	// System.Int32 VertexAnimationTools_30.PointCache::SelectedTabIdx
	int32_t ___SelectedTabIdx_14;
	// System.Int32 VertexAnimationTools_30.PointCache::SelectedImportTabIdx
	int32_t ___SelectedImportTabIdx_15;
	// System.Single VertexAnimationTools_30.PointCache::AssetFileSize
	float ___AssetFileSize_16;
	// System.String VertexAnimationTools_30.PointCache::ImportingDate
	String_t* ___ImportingDate_17;

public:
	inline static int32_t get_offset_of_PreImport_5() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___PreImport_5)); }
	inline ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C  get_PreImport_5() const { return ___PreImport_5; }
	inline ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C * get_address_of_PreImport_5() { return &___PreImport_5; }
	inline void set_PreImport_5(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C  value)
	{
		___PreImport_5 = value;
	}

	inline static int32_t get_offset_of_PostImport_6() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___PostImport_6)); }
	inline ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C  get_PostImport_6() const { return ___PostImport_6; }
	inline ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C * get_address_of_PostImport_6() { return &___PostImport_6; }
	inline void set_PostImport_6(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C  value)
	{
		___PostImport_6 = value;
	}

	inline static int32_t get_offset_of_Clips_7() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___Clips_7)); }
	inline ClipU5BU5D_t9B7F45E85DBF342DB234CEFF7945F4E5F1F4D3CC* get_Clips_7() const { return ___Clips_7; }
	inline ClipU5BU5D_t9B7F45E85DBF342DB234CEFF7945F4E5F1F4D3CC** get_address_of_Clips_7() { return &___Clips_7; }
	inline void set_Clips_7(ClipU5BU5D_t9B7F45E85DBF342DB234CEFF7945F4E5F1F4D3CC* value)
	{
		___Clips_7 = value;
		Il2CppCodeGenWriteBarrier((&___Clips_7), value);
	}

	inline static int32_t get_offset_of_Meshes_8() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___Meshes_8)); }
	inline PolygonMeshU5BU5D_t8CB08AC2E64BEDFAF6ABB153CC60B9E09DDF7C52* get_Meshes_8() const { return ___Meshes_8; }
	inline PolygonMeshU5BU5D_t8CB08AC2E64BEDFAF6ABB153CC60B9E09DDF7C52** get_address_of_Meshes_8() { return &___Meshes_8; }
	inline void set_Meshes_8(PolygonMeshU5BU5D_t8CB08AC2E64BEDFAF6ABB153CC60B9E09DDF7C52* value)
	{
		___Meshes_8 = value;
		Il2CppCodeGenWriteBarrier((&___Meshes_8), value);
	}

	inline static int32_t get_offset_of_PreConstraints_9() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___PreConstraints_9)); }
	inline List_1_tCCAD2DF763777A3380885B29580636BEE8FC7280 * get_PreConstraints_9() const { return ___PreConstraints_9; }
	inline List_1_tCCAD2DF763777A3380885B29580636BEE8FC7280 ** get_address_of_PreConstraints_9() { return &___PreConstraints_9; }
	inline void set_PreConstraints_9(List_1_tCCAD2DF763777A3380885B29580636BEE8FC7280 * value)
	{
		___PreConstraints_9 = value;
		Il2CppCodeGenWriteBarrier((&___PreConstraints_9), value);
	}

	inline static int32_t get_offset_of_PostConstraints_10() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___PostConstraints_10)); }
	inline PostImportConstraintU5BU5D_t8DAC6BA1EB4063452FAB12533B20372EB8D5C130* get_PostConstraints_10() const { return ___PostConstraints_10; }
	inline PostImportConstraintU5BU5D_t8DAC6BA1EB4063452FAB12533B20372EB8D5C130** get_address_of_PostConstraints_10() { return &___PostConstraints_10; }
	inline void set_PostConstraints_10(PostImportConstraintU5BU5D_t8DAC6BA1EB4063452FAB12533B20372EB8D5C130* value)
	{
		___PostConstraints_10 = value;
		Il2CppCodeGenWriteBarrier((&___PostConstraints_10), value);
	}

	inline static int32_t get_offset_of_ImportSettingsIsDirtyFlag_11() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___ImportSettingsIsDirtyFlag_11)); }
	inline bool get_ImportSettingsIsDirtyFlag_11() const { return ___ImportSettingsIsDirtyFlag_11; }
	inline bool* get_address_of_ImportSettingsIsDirtyFlag_11() { return &___ImportSettingsIsDirtyFlag_11; }
	inline void set_ImportSettingsIsDirtyFlag_11(bool value)
	{
		___ImportSettingsIsDirtyFlag_11 = value;
	}

	inline static int32_t get_offset_of_ConstraintHandlesSize_12() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___ConstraintHandlesSize_12)); }
	inline float get_ConstraintHandlesSize_12() const { return ___ConstraintHandlesSize_12; }
	inline float* get_address_of_ConstraintHandlesSize_12() { return &___ConstraintHandlesSize_12; }
	inline void set_ConstraintHandlesSize_12(float value)
	{
		___ConstraintHandlesSize_12 = value;
	}

	inline static int32_t get_offset_of_DrawConstraintHandlesName_13() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___DrawConstraintHandlesName_13)); }
	inline bool get_DrawConstraintHandlesName_13() const { return ___DrawConstraintHandlesName_13; }
	inline bool* get_address_of_DrawConstraintHandlesName_13() { return &___DrawConstraintHandlesName_13; }
	inline void set_DrawConstraintHandlesName_13(bool value)
	{
		___DrawConstraintHandlesName_13 = value;
	}

	inline static int32_t get_offset_of_SelectedTabIdx_14() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___SelectedTabIdx_14)); }
	inline int32_t get_SelectedTabIdx_14() const { return ___SelectedTabIdx_14; }
	inline int32_t* get_address_of_SelectedTabIdx_14() { return &___SelectedTabIdx_14; }
	inline void set_SelectedTabIdx_14(int32_t value)
	{
		___SelectedTabIdx_14 = value;
	}

	inline static int32_t get_offset_of_SelectedImportTabIdx_15() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___SelectedImportTabIdx_15)); }
	inline int32_t get_SelectedImportTabIdx_15() const { return ___SelectedImportTabIdx_15; }
	inline int32_t* get_address_of_SelectedImportTabIdx_15() { return &___SelectedImportTabIdx_15; }
	inline void set_SelectedImportTabIdx_15(int32_t value)
	{
		___SelectedImportTabIdx_15 = value;
	}

	inline static int32_t get_offset_of_AssetFileSize_16() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___AssetFileSize_16)); }
	inline float get_AssetFileSize_16() const { return ___AssetFileSize_16; }
	inline float* get_address_of_AssetFileSize_16() { return &___AssetFileSize_16; }
	inline void set_AssetFileSize_16(float value)
	{
		___AssetFileSize_16 = value;
	}

	inline static int32_t get_offset_of_ImportingDate_17() { return static_cast<int32_t>(offsetof(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7, ___ImportingDate_17)); }
	inline String_t* get_ImportingDate_17() const { return ___ImportingDate_17; }
	inline String_t** get_address_of_ImportingDate_17() { return &___ImportingDate_17; }
	inline void set_ImportingDate_17(String_t* value)
	{
		___ImportingDate_17 = value;
		Il2CppCodeGenWriteBarrier((&___ImportingDate_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCACHE_T08D0CD88824B54FB717F6B510FF8EA87D1FA49B7_H
#ifndef ARROWSCRIPT_TECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2_H
#define ARROWSCRIPT_TECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArrowScript
struct  ArrowScript_tECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer[] ArrowScript::mesh
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* ___mesh_4;
	// UnityEngine.Material[] ArrowScript::material
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___material_5;
	// UnityEngine.Color[] ArrowScript::col
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___col_6;
	// System.Single ArrowScript::alpha
	float ___alpha_7;

public:
	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(ArrowScript_tECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2, ___mesh_4)); }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* get_mesh_4() const { return ___mesh_4; }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(ArrowScript_tECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2, ___material_5)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_material_5() const { return ___material_5; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_col_6() { return static_cast<int32_t>(offsetof(ArrowScript_tECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2, ___col_6)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_col_6() const { return ___col_6; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_col_6() { return &___col_6; }
	inline void set_col_6(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___col_6 = value;
		Il2CppCodeGenWriteBarrier((&___col_6), value);
	}

	inline static int32_t get_offset_of_alpha_7() { return static_cast<int32_t>(offsetof(ArrowScript_tECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2, ___alpha_7)); }
	inline float get_alpha_7() const { return ___alpha_7; }
	inline float* get_address_of_alpha_7() { return &___alpha_7; }
	inline void set_alpha_7(float value)
	{
		___alpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWSCRIPT_TECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2_H
#ifndef COUNT_T78AB461663390D23C5961F760F5F935C29C04BAC_H
#define COUNT_T78AB461663390D23C5961F760F5F935C29C04BAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Count
struct  Count_t78AB461663390D23C5961F760F5F935C29C04BAC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Count::countText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___countText_4;
	// System.Int32 Count::boundcount
	int32_t ___boundcount_5;

public:
	inline static int32_t get_offset_of_countText_4() { return static_cast<int32_t>(offsetof(Count_t78AB461663390D23C5961F760F5F935C29C04BAC, ___countText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_countText_4() const { return ___countText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_countText_4() { return &___countText_4; }
	inline void set_countText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___countText_4 = value;
		Il2CppCodeGenWriteBarrier((&___countText_4), value);
	}

	inline static int32_t get_offset_of_boundcount_5() { return static_cast<int32_t>(offsetof(Count_t78AB461663390D23C5961F760F5F935C29C04BAC, ___boundcount_5)); }
	inline int32_t get_boundcount_5() const { return ___boundcount_5; }
	inline int32_t* get_address_of_boundcount_5() { return &___boundcount_5; }
	inline void set_boundcount_5(int32_t value)
	{
		___boundcount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNT_T78AB461663390D23C5961F760F5F935C29C04BAC_H
#ifndef DEMOCONTROLLER_T57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0_H
#define DEMOCONTROLLER_T57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoController
struct  DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject DemoController::eff1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___eff1_4;
	// UnityEngine.GameObject DemoController::eff2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___eff2_5;
	// UnityEngine.GameObject DemoController::eff3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___eff3_6;
	// UnityEngine.GameObject DemoController::eff4
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___eff4_7;
	// UnityEngine.GameObject DemoController::eff5
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___eff5_8;
	// UnityEngine.GameObject DemoController::eff_name
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___eff_name_9;
	// System.Int32 DemoController::count
	int32_t ___count_10;

public:
	inline static int32_t get_offset_of_eff1_4() { return static_cast<int32_t>(offsetof(DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0, ___eff1_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_eff1_4() const { return ___eff1_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_eff1_4() { return &___eff1_4; }
	inline void set_eff1_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___eff1_4 = value;
		Il2CppCodeGenWriteBarrier((&___eff1_4), value);
	}

	inline static int32_t get_offset_of_eff2_5() { return static_cast<int32_t>(offsetof(DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0, ___eff2_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_eff2_5() const { return ___eff2_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_eff2_5() { return &___eff2_5; }
	inline void set_eff2_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___eff2_5 = value;
		Il2CppCodeGenWriteBarrier((&___eff2_5), value);
	}

	inline static int32_t get_offset_of_eff3_6() { return static_cast<int32_t>(offsetof(DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0, ___eff3_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_eff3_6() const { return ___eff3_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_eff3_6() { return &___eff3_6; }
	inline void set_eff3_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___eff3_6 = value;
		Il2CppCodeGenWriteBarrier((&___eff3_6), value);
	}

	inline static int32_t get_offset_of_eff4_7() { return static_cast<int32_t>(offsetof(DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0, ___eff4_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_eff4_7() const { return ___eff4_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_eff4_7() { return &___eff4_7; }
	inline void set_eff4_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___eff4_7 = value;
		Il2CppCodeGenWriteBarrier((&___eff4_7), value);
	}

	inline static int32_t get_offset_of_eff5_8() { return static_cast<int32_t>(offsetof(DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0, ___eff5_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_eff5_8() const { return ___eff5_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_eff5_8() { return &___eff5_8; }
	inline void set_eff5_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___eff5_8 = value;
		Il2CppCodeGenWriteBarrier((&___eff5_8), value);
	}

	inline static int32_t get_offset_of_eff_name_9() { return static_cast<int32_t>(offsetof(DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0, ___eff_name_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_eff_name_9() const { return ___eff_name_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_eff_name_9() { return &___eff_name_9; }
	inline void set_eff_name_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___eff_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___eff_name_9), value);
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOCONTROLLER_T57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0_H
#ifndef MANSCRIPT_TC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB_H
#define MANSCRIPT_TC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManScript
struct  ManScript_tC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer ManScript::mesh
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___mesh_4;
	// UnityEngine.Material ManScript::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_5;
	// System.Single ManScript::alpha
	float ___alpha_6;
	// UnityEngine.Color ManScript::color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color_7;

public:
	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(ManScript_tC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB, ___mesh_4)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_mesh_4() const { return ___mesh_4; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(ManScript_tC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB, ___material_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_5() const { return ___material_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_alpha_6() { return static_cast<int32_t>(offsetof(ManScript_tC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB, ___alpha_6)); }
	inline float get_alpha_6() const { return ___alpha_6; }
	inline float* get_address_of_alpha_6() { return &___alpha_6; }
	inline void set_alpha_6(float value)
	{
		___alpha_6 = value;
	}

	inline static int32_t get_offset_of_color_7() { return static_cast<int32_t>(offsetof(ManScript_tC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB, ___color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color_7() const { return ___color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color_7() { return &___color_7; }
	inline void set_color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANSCRIPT_TC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB_H
#ifndef OBJECTCLIP_T569A88667400491D3CE315544091A25CA3CB08FF_H
#define OBJECTCLIP_T569A88667400491D3CE315544091A25CA3CB08FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectClip
struct  ObjectClip_t569A88667400491D3CE315544091A25CA3CB08FF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer ObjectClip::mesh
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___mesh_4;
	// UnityEngine.Material ObjectClip::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_5;
	// System.Single ObjectClip::anitime
	float ___anitime_6;
	// System.Single ObjectClip::alpha
	float ___alpha_7;

public:
	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(ObjectClip_t569A88667400491D3CE315544091A25CA3CB08FF, ___mesh_4)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_mesh_4() const { return ___mesh_4; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(ObjectClip_t569A88667400491D3CE315544091A25CA3CB08FF, ___material_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_5() const { return ___material_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_anitime_6() { return static_cast<int32_t>(offsetof(ObjectClip_t569A88667400491D3CE315544091A25CA3CB08FF, ___anitime_6)); }
	inline float get_anitime_6() const { return ___anitime_6; }
	inline float* get_address_of_anitime_6() { return &___anitime_6; }
	inline void set_anitime_6(float value)
	{
		___anitime_6 = value;
	}

	inline static int32_t get_offset_of_alpha_7() { return static_cast<int32_t>(offsetof(ObjectClip_t569A88667400491D3CE315544091A25CA3CB08FF, ___alpha_7)); }
	inline float get_alpha_7() const { return ___alpha_7; }
	inline float* get_address_of_alpha_7() { return &___alpha_7; }
	inline void set_alpha_7(float value)
	{
		___alpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCLIP_T569A88667400491D3CE315544091A25CA3CB08FF_H
#ifndef TESTTIME_T535287267B5B8B1DF2E7AE75DD49C77C5AE8D195_H
#define TESTTIME_T535287267B5B8B1DF2E7AE75DD49C77C5AE8D195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestTime
struct  TestTime_t535287267B5B8B1DF2E7AE75DD49C77C5AE8D195  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text TestTime::timetext
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___timetext_4;
	// System.Single TestTime::etime
	float ___etime_5;

public:
	inline static int32_t get_offset_of_timetext_4() { return static_cast<int32_t>(offsetof(TestTime_t535287267B5B8B1DF2E7AE75DD49C77C5AE8D195, ___timetext_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_timetext_4() const { return ___timetext_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_timetext_4() { return &___timetext_4; }
	inline void set_timetext_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___timetext_4 = value;
		Il2CppCodeGenWriteBarrier((&___timetext_4), value);
	}

	inline static int32_t get_offset_of_etime_5() { return static_cast<int32_t>(offsetof(TestTime_t535287267B5B8B1DF2E7AE75DD49C77C5AE8D195, ___etime_5)); }
	inline float get_etime_5() const { return ___etime_5; }
	inline float* get_address_of_etime_5() { return &___etime_5; }
	inline void set_etime_5(float value)
	{
		___etime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTTIME_T535287267B5B8B1DF2E7AE75DD49C77C5AE8D195_H
#ifndef AXISTOUCHBUTTON_T124E142E5813D33A6407C920758816933327BEC4_H
#define AXISTOUCHBUTTON_T124E142E5813D33A6407C920758816933327BEC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct  AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisName
	String_t* ___axisName_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisValue
	float ___axisValue_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::responseSpeed
	float ___responseSpeed_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::returnToCentreSpeed
	float ___returnToCentreSpeed_7;
	// UnityStandardAssets.CrossPlatformInput.AxisTouchButton UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_PairedWith
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4 * ___m_PairedWith_8;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_Axis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_Axis_9;

public:
	inline static int32_t get_offset_of_axisName_4() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___axisName_4)); }
	inline String_t* get_axisName_4() const { return ___axisName_4; }
	inline String_t** get_address_of_axisName_4() { return &___axisName_4; }
	inline void set_axisName_4(String_t* value)
	{
		___axisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_4), value);
	}

	inline static int32_t get_offset_of_axisValue_5() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___axisValue_5)); }
	inline float get_axisValue_5() const { return ___axisValue_5; }
	inline float* get_address_of_axisValue_5() { return &___axisValue_5; }
	inline void set_axisValue_5(float value)
	{
		___axisValue_5 = value;
	}

	inline static int32_t get_offset_of_responseSpeed_6() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___responseSpeed_6)); }
	inline float get_responseSpeed_6() const { return ___responseSpeed_6; }
	inline float* get_address_of_responseSpeed_6() { return &___responseSpeed_6; }
	inline void set_responseSpeed_6(float value)
	{
		___responseSpeed_6 = value;
	}

	inline static int32_t get_offset_of_returnToCentreSpeed_7() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___returnToCentreSpeed_7)); }
	inline float get_returnToCentreSpeed_7() const { return ___returnToCentreSpeed_7; }
	inline float* get_address_of_returnToCentreSpeed_7() { return &___returnToCentreSpeed_7; }
	inline void set_returnToCentreSpeed_7(float value)
	{
		___returnToCentreSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_PairedWith_8() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___m_PairedWith_8)); }
	inline AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4 * get_m_PairedWith_8() const { return ___m_PairedWith_8; }
	inline AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4 ** get_address_of_m_PairedWith_8() { return &___m_PairedWith_8; }
	inline void set_m_PairedWith_8(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4 * value)
	{
		___m_PairedWith_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_PairedWith_8), value);
	}

	inline static int32_t get_offset_of_m_Axis_9() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___m_Axis_9)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_Axis_9() const { return ___m_Axis_9; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_Axis_9() { return &___m_Axis_9; }
	inline void set_m_Axis_9(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_Axis_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Axis_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTOUCHBUTTON_T124E142E5813D33A6407C920758816933327BEC4_H
#ifndef BUTTONHANDLER_TA53C84C0B2F2670630A8B3B02001C0E6F773C631_H
#define BUTTONHANDLER_TA53C84C0B2F2670630A8B3B02001C0E6F773C631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct  ButtonHandler_tA53C84C0B2F2670630A8B3B02001C0E6F773C631  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.ButtonHandler::Name
	String_t* ___Name_4;

public:
	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(ButtonHandler_tA53C84C0B2F2670630A8B3B02001C0E6F773C631, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONHANDLER_TA53C84C0B2F2670630A8B3B02001C0E6F773C631_H
#ifndef INPUTAXISSCROLLBAR_T935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7_H
#define INPUTAXISSCROLLBAR_T935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct  InputAxisScrollbar_t935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::axis
	String_t* ___axis_4;

public:
	inline static int32_t get_offset_of_axis_4() { return static_cast<int32_t>(offsetof(InputAxisScrollbar_t935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7, ___axis_4)); }
	inline String_t* get_axis_4() const { return ___axis_4; }
	inline String_t** get_address_of_axis_4() { return &___axis_4; }
	inline void set_axis_4(String_t* value)
	{
		___axis_4 = value;
		Il2CppCodeGenWriteBarrier((&___axis_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTAXISSCROLLBAR_T935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7_H
#ifndef JOYSTICK_T56ABC51716722ABB9F06ADACA11FE44B502E4300_H
#define JOYSTICK_T56ABC51716722ABB9F06ADACA11FE44B502E4300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick
struct  Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick::MovementRange
	int32_t ___MovementRange_4;
	// UnityStandardAssets.CrossPlatformInput.Joystick_AxisOption UnityStandardAssets.CrossPlatformInput.Joystick::axesToUse
	int32_t ___axesToUse_5;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::horizontalAxisName
	String_t* ___horizontalAxisName_6;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::verticalAxisName
	String_t* ___verticalAxisName_7;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.Joystick::m_StartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_StartPos_8;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseX
	bool ___m_UseX_9;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseY
	bool ___m_UseY_10;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_HorizontalVirtualAxis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_HorizontalVirtualAxis_11;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_VerticalVirtualAxis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_VerticalVirtualAxis_12;

public:
	inline static int32_t get_offset_of_MovementRange_4() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___MovementRange_4)); }
	inline int32_t get_MovementRange_4() const { return ___MovementRange_4; }
	inline int32_t* get_address_of_MovementRange_4() { return &___MovementRange_4; }
	inline void set_MovementRange_4(int32_t value)
	{
		___MovementRange_4 = value;
	}

	inline static int32_t get_offset_of_axesToUse_5() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___axesToUse_5)); }
	inline int32_t get_axesToUse_5() const { return ___axesToUse_5; }
	inline int32_t* get_address_of_axesToUse_5() { return &___axesToUse_5; }
	inline void set_axesToUse_5(int32_t value)
	{
		___axesToUse_5 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_6() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___horizontalAxisName_6)); }
	inline String_t* get_horizontalAxisName_6() const { return ___horizontalAxisName_6; }
	inline String_t** get_address_of_horizontalAxisName_6() { return &___horizontalAxisName_6; }
	inline void set_horizontalAxisName_6(String_t* value)
	{
		___horizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_6), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_7() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___verticalAxisName_7)); }
	inline String_t* get_verticalAxisName_7() const { return ___verticalAxisName_7; }
	inline String_t** get_address_of_verticalAxisName_7() { return &___verticalAxisName_7; }
	inline void set_verticalAxisName_7(String_t* value)
	{
		___verticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_7), value);
	}

	inline static int32_t get_offset_of_m_StartPos_8() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___m_StartPos_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_StartPos_8() const { return ___m_StartPos_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_StartPos_8() { return &___m_StartPos_8; }
	inline void set_m_StartPos_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_StartPos_8 = value;
	}

	inline static int32_t get_offset_of_m_UseX_9() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___m_UseX_9)); }
	inline bool get_m_UseX_9() const { return ___m_UseX_9; }
	inline bool* get_address_of_m_UseX_9() { return &___m_UseX_9; }
	inline void set_m_UseX_9(bool value)
	{
		___m_UseX_9 = value;
	}

	inline static int32_t get_offset_of_m_UseY_10() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___m_UseY_10)); }
	inline bool get_m_UseY_10() const { return ___m_UseY_10; }
	inline bool* get_address_of_m_UseY_10() { return &___m_UseY_10; }
	inline void set_m_UseY_10(bool value)
	{
		___m_UseY_10 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_11() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___m_HorizontalVirtualAxis_11)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_HorizontalVirtualAxis_11() const { return ___m_HorizontalVirtualAxis_11; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_HorizontalVirtualAxis_11() { return &___m_HorizontalVirtualAxis_11; }
	inline void set_m_HorizontalVirtualAxis_11(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_HorizontalVirtualAxis_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_11), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_12() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___m_VerticalVirtualAxis_12)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_VerticalVirtualAxis_12() const { return ___m_VerticalVirtualAxis_12; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_VerticalVirtualAxis_12() { return &___m_VerticalVirtualAxis_12; }
	inline void set_m_VerticalVirtualAxis_12(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_VerticalVirtualAxis_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T56ABC51716722ABB9F06ADACA11FE44B502E4300_H
#ifndef MOBILECONTROLRIG_TD348E6259B1A94E95D09BD393ADAC945B3515418_H
#define MOBILECONTROLRIG_TD348E6259B1A94E95D09BD393ADAC945B3515418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct  MobileControlRig_tD348E6259B1A94E95D09BD393ADAC945B3515418  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLRIG_TD348E6259B1A94E95D09BD393ADAC945B3515418_H
#ifndef TILTINPUT_T51C59191ECAB38039EF5E5E19B0F04983EAC3D53_H
#define TILTINPUT_T51C59191ECAB38039EF5E5E19B0F04983EAC3D53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct  TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput_AxisMapping UnityStandardAssets.CrossPlatformInput.TiltInput::mapping
	AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E * ___mapping_4;
	// UnityStandardAssets.CrossPlatformInput.TiltInput_AxisOptions UnityStandardAssets.CrossPlatformInput.TiltInput::tiltAroundAxis
	int32_t ___tiltAroundAxis_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::fullTiltAngle
	float ___fullTiltAngle_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::centreAngleOffset
	float ___centreAngleOffset_7;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis UnityStandardAssets.CrossPlatformInput.TiltInput::m_SteerAxis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_SteerAxis_8;

public:
	inline static int32_t get_offset_of_mapping_4() { return static_cast<int32_t>(offsetof(TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53, ___mapping_4)); }
	inline AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E * get_mapping_4() const { return ___mapping_4; }
	inline AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E ** get_address_of_mapping_4() { return &___mapping_4; }
	inline void set_mapping_4(AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E * value)
	{
		___mapping_4 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_4), value);
	}

	inline static int32_t get_offset_of_tiltAroundAxis_5() { return static_cast<int32_t>(offsetof(TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53, ___tiltAroundAxis_5)); }
	inline int32_t get_tiltAroundAxis_5() const { return ___tiltAroundAxis_5; }
	inline int32_t* get_address_of_tiltAroundAxis_5() { return &___tiltAroundAxis_5; }
	inline void set_tiltAroundAxis_5(int32_t value)
	{
		___tiltAroundAxis_5 = value;
	}

	inline static int32_t get_offset_of_fullTiltAngle_6() { return static_cast<int32_t>(offsetof(TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53, ___fullTiltAngle_6)); }
	inline float get_fullTiltAngle_6() const { return ___fullTiltAngle_6; }
	inline float* get_address_of_fullTiltAngle_6() { return &___fullTiltAngle_6; }
	inline void set_fullTiltAngle_6(float value)
	{
		___fullTiltAngle_6 = value;
	}

	inline static int32_t get_offset_of_centreAngleOffset_7() { return static_cast<int32_t>(offsetof(TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53, ___centreAngleOffset_7)); }
	inline float get_centreAngleOffset_7() const { return ___centreAngleOffset_7; }
	inline float* get_address_of_centreAngleOffset_7() { return &___centreAngleOffset_7; }
	inline void set_centreAngleOffset_7(float value)
	{
		___centreAngleOffset_7 = value;
	}

	inline static int32_t get_offset_of_m_SteerAxis_8() { return static_cast<int32_t>(offsetof(TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53, ___m_SteerAxis_8)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_SteerAxis_8() const { return ___m_SteerAxis_8; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_SteerAxis_8() { return &___m_SteerAxis_8; }
	inline void set_m_SteerAxis_8(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_SteerAxis_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SteerAxis_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTINPUT_T51C59191ECAB38039EF5E5E19B0F04983EAC3D53_H
#ifndef TOUCHPAD_TAD361C39C471A260BFA37D565706AADF3528BC11_H
#define TOUCHPAD_TAD361C39C471A260BFA37D565706AADF3528BC11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct  TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.CrossPlatformInput.TouchPad_AxisOption UnityStandardAssets.CrossPlatformInput.TouchPad::axesToUse
	int32_t ___axesToUse_4;
	// UnityStandardAssets.CrossPlatformInput.TouchPad_ControlStyle UnityStandardAssets.CrossPlatformInput.TouchPad::controlStyle
	int32_t ___controlStyle_5;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::horizontalAxisName
	String_t* ___horizontalAxisName_6;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::verticalAxisName
	String_t* ___verticalAxisName_7;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Xsensitivity
	float ___Xsensitivity_8;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Ysensitivity
	float ___Ysensitivity_9;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_StartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_StartPos_10;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PreviousDelta_11;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_JoytickOutput
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_JoytickOutput_12;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseX
	bool ___m_UseX_13;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseY
	bool ___m_UseY_14;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_HorizontalVirtualAxis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_HorizontalVirtualAxis_15;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager_VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_VerticalVirtualAxis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_VerticalVirtualAxis_16;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_Dragging
	bool ___m_Dragging_17;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Id
	int32_t ___m_Id_18;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousTouchPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PreviousTouchPos_19;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_20;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.TouchPad::m_Image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_Image_21;

public:
	inline static int32_t get_offset_of_axesToUse_4() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___axesToUse_4)); }
	inline int32_t get_axesToUse_4() const { return ___axesToUse_4; }
	inline int32_t* get_address_of_axesToUse_4() { return &___axesToUse_4; }
	inline void set_axesToUse_4(int32_t value)
	{
		___axesToUse_4 = value;
	}

	inline static int32_t get_offset_of_controlStyle_5() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___controlStyle_5)); }
	inline int32_t get_controlStyle_5() const { return ___controlStyle_5; }
	inline int32_t* get_address_of_controlStyle_5() { return &___controlStyle_5; }
	inline void set_controlStyle_5(int32_t value)
	{
		___controlStyle_5 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_6() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___horizontalAxisName_6)); }
	inline String_t* get_horizontalAxisName_6() const { return ___horizontalAxisName_6; }
	inline String_t** get_address_of_horizontalAxisName_6() { return &___horizontalAxisName_6; }
	inline void set_horizontalAxisName_6(String_t* value)
	{
		___horizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_6), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_7() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___verticalAxisName_7)); }
	inline String_t* get_verticalAxisName_7() const { return ___verticalAxisName_7; }
	inline String_t** get_address_of_verticalAxisName_7() { return &___verticalAxisName_7; }
	inline void set_verticalAxisName_7(String_t* value)
	{
		___verticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_7), value);
	}

	inline static int32_t get_offset_of_Xsensitivity_8() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___Xsensitivity_8)); }
	inline float get_Xsensitivity_8() const { return ___Xsensitivity_8; }
	inline float* get_address_of_Xsensitivity_8() { return &___Xsensitivity_8; }
	inline void set_Xsensitivity_8(float value)
	{
		___Xsensitivity_8 = value;
	}

	inline static int32_t get_offset_of_Ysensitivity_9() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___Ysensitivity_9)); }
	inline float get_Ysensitivity_9() const { return ___Ysensitivity_9; }
	inline float* get_address_of_Ysensitivity_9() { return &___Ysensitivity_9; }
	inline void set_Ysensitivity_9(float value)
	{
		___Ysensitivity_9 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_10() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_StartPos_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_StartPos_10() const { return ___m_StartPos_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_StartPos_10() { return &___m_StartPos_10; }
	inline void set_m_StartPos_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_StartPos_10 = value;
	}

	inline static int32_t get_offset_of_m_PreviousDelta_11() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_PreviousDelta_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PreviousDelta_11() const { return ___m_PreviousDelta_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PreviousDelta_11() { return &___m_PreviousDelta_11; }
	inline void set_m_PreviousDelta_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PreviousDelta_11 = value;
	}

	inline static int32_t get_offset_of_m_JoytickOutput_12() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_JoytickOutput_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_JoytickOutput_12() const { return ___m_JoytickOutput_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_JoytickOutput_12() { return &___m_JoytickOutput_12; }
	inline void set_m_JoytickOutput_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_JoytickOutput_12 = value;
	}

	inline static int32_t get_offset_of_m_UseX_13() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_UseX_13)); }
	inline bool get_m_UseX_13() const { return ___m_UseX_13; }
	inline bool* get_address_of_m_UseX_13() { return &___m_UseX_13; }
	inline void set_m_UseX_13(bool value)
	{
		___m_UseX_13 = value;
	}

	inline static int32_t get_offset_of_m_UseY_14() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_UseY_14)); }
	inline bool get_m_UseY_14() const { return ___m_UseY_14; }
	inline bool* get_address_of_m_UseY_14() { return &___m_UseY_14; }
	inline void set_m_UseY_14(bool value)
	{
		___m_UseY_14 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_15() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_HorizontalVirtualAxis_15)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_HorizontalVirtualAxis_15() const { return ___m_HorizontalVirtualAxis_15; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_HorizontalVirtualAxis_15() { return &___m_HorizontalVirtualAxis_15; }
	inline void set_m_HorizontalVirtualAxis_15(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_HorizontalVirtualAxis_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_15), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_16() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_VerticalVirtualAxis_16)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_VerticalVirtualAxis_16() const { return ___m_VerticalVirtualAxis_16; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_VerticalVirtualAxis_16() { return &___m_VerticalVirtualAxis_16; }
	inline void set_m_VerticalVirtualAxis_16(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_VerticalVirtualAxis_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_16), value);
	}

	inline static int32_t get_offset_of_m_Dragging_17() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_Dragging_17)); }
	inline bool get_m_Dragging_17() const { return ___m_Dragging_17; }
	inline bool* get_address_of_m_Dragging_17() { return &___m_Dragging_17; }
	inline void set_m_Dragging_17(bool value)
	{
		___m_Dragging_17 = value;
	}

	inline static int32_t get_offset_of_m_Id_18() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_Id_18)); }
	inline int32_t get_m_Id_18() const { return ___m_Id_18; }
	inline int32_t* get_address_of_m_Id_18() { return &___m_Id_18; }
	inline void set_m_Id_18(int32_t value)
	{
		___m_Id_18 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTouchPos_19() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_PreviousTouchPos_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PreviousTouchPos_19() const { return ___m_PreviousTouchPos_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PreviousTouchPos_19() { return &___m_PreviousTouchPos_19; }
	inline void set_m_PreviousTouchPos_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PreviousTouchPos_19 = value;
	}

	inline static int32_t get_offset_of_m_Center_20() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_Center_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_20() const { return ___m_Center_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_20() { return &___m_Center_20; }
	inline void set_m_Center_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_20 = value;
	}

	inline static int32_t get_offset_of_m_Image_21() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_Image_21)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_Image_21() const { return ___m_Image_21; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_Image_21() { return &___m_Image_21; }
	inline void set_m_Image_21(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_Image_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_TAD361C39C471A260BFA37D565706AADF3528BC11_H
#ifndef MESHSEQUENCEPLAYER_T3E931E737D23E1AB49097ED6CF0CD5D100336838_H
#define MESHSEQUENCEPLAYER_T3E931E737D23E1AB49097ED6CF0CD5D100336838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MeshSequencePlayer
struct  MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.MeshFilter VertexAnimationTools_30.MeshSequencePlayer::MF
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___MF_4;
	// UnityEngine.MeshCollider VertexAnimationTools_30.MeshSequencePlayer::MC
	MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * ___MC_5;
	// UnityEngine.MeshRenderer VertexAnimationTools_30.MeshSequencePlayer::MR
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___MR_6;
	// VertexAnimationTools_30.PlayerUpdateMode VertexAnimationTools_30.MeshSequencePlayer::UpdateMode
	int32_t ___UpdateMode_7;
	// VertexAnimationTools_30.AutoPlaybackTypeEnum VertexAnimationTools_30.MeshSequencePlayer::PlaybackMode
	int32_t ___PlaybackMode_8;
	// System.Single VertexAnimationTools_30.MeshSequencePlayer::FramesPerSecond
	float ___FramesPerSecond_9;
	// System.Boolean VertexAnimationTools_30.MeshSequencePlayer::UseTimescale
	bool ___UseTimescale_10;
	// System.Single VertexAnimationTools_30.MeshSequencePlayer::NormalizedTime
	float ___NormalizedTime_11;
	// System.Single VertexAnimationTools_30.MeshSequencePlayer::timeDirection
	float ___timeDirection_12;
	// VertexAnimationTools_30.MeshSequence VertexAnimationTools_30.MeshSequencePlayer::prevMeshSequence
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * ___prevMeshSequence_13;
	// VertexAnimationTools_30.MeshSequence VertexAnimationTools_30.MeshSequencePlayer::meshSequence
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * ___meshSequence_14;
	// VertexAnimationTools_30.MeshSequence_Frame VertexAnimationTools_30.MeshSequencePlayer::currentFrame
	Frame_t569CB10118B510EF308215FA5E2BC6354316C27B * ___currentFrame_15;
	// System.Int32 VertexAnimationTools_30.MeshSequencePlayer::TabChoise
	int32_t ___TabChoise_16;

public:
	inline static int32_t get_offset_of_MF_4() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___MF_4)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_MF_4() const { return ___MF_4; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_MF_4() { return &___MF_4; }
	inline void set_MF_4(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___MF_4 = value;
		Il2CppCodeGenWriteBarrier((&___MF_4), value);
	}

	inline static int32_t get_offset_of_MC_5() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___MC_5)); }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * get_MC_5() const { return ___MC_5; }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE ** get_address_of_MC_5() { return &___MC_5; }
	inline void set_MC_5(MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * value)
	{
		___MC_5 = value;
		Il2CppCodeGenWriteBarrier((&___MC_5), value);
	}

	inline static int32_t get_offset_of_MR_6() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___MR_6)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_MR_6() const { return ___MR_6; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_MR_6() { return &___MR_6; }
	inline void set_MR_6(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___MR_6 = value;
		Il2CppCodeGenWriteBarrier((&___MR_6), value);
	}

	inline static int32_t get_offset_of_UpdateMode_7() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___UpdateMode_7)); }
	inline int32_t get_UpdateMode_7() const { return ___UpdateMode_7; }
	inline int32_t* get_address_of_UpdateMode_7() { return &___UpdateMode_7; }
	inline void set_UpdateMode_7(int32_t value)
	{
		___UpdateMode_7 = value;
	}

	inline static int32_t get_offset_of_PlaybackMode_8() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___PlaybackMode_8)); }
	inline int32_t get_PlaybackMode_8() const { return ___PlaybackMode_8; }
	inline int32_t* get_address_of_PlaybackMode_8() { return &___PlaybackMode_8; }
	inline void set_PlaybackMode_8(int32_t value)
	{
		___PlaybackMode_8 = value;
	}

	inline static int32_t get_offset_of_FramesPerSecond_9() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___FramesPerSecond_9)); }
	inline float get_FramesPerSecond_9() const { return ___FramesPerSecond_9; }
	inline float* get_address_of_FramesPerSecond_9() { return &___FramesPerSecond_9; }
	inline void set_FramesPerSecond_9(float value)
	{
		___FramesPerSecond_9 = value;
	}

	inline static int32_t get_offset_of_UseTimescale_10() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___UseTimescale_10)); }
	inline bool get_UseTimescale_10() const { return ___UseTimescale_10; }
	inline bool* get_address_of_UseTimescale_10() { return &___UseTimescale_10; }
	inline void set_UseTimescale_10(bool value)
	{
		___UseTimescale_10 = value;
	}

	inline static int32_t get_offset_of_NormalizedTime_11() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___NormalizedTime_11)); }
	inline float get_NormalizedTime_11() const { return ___NormalizedTime_11; }
	inline float* get_address_of_NormalizedTime_11() { return &___NormalizedTime_11; }
	inline void set_NormalizedTime_11(float value)
	{
		___NormalizedTime_11 = value;
	}

	inline static int32_t get_offset_of_timeDirection_12() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___timeDirection_12)); }
	inline float get_timeDirection_12() const { return ___timeDirection_12; }
	inline float* get_address_of_timeDirection_12() { return &___timeDirection_12; }
	inline void set_timeDirection_12(float value)
	{
		___timeDirection_12 = value;
	}

	inline static int32_t get_offset_of_prevMeshSequence_13() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___prevMeshSequence_13)); }
	inline MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * get_prevMeshSequence_13() const { return ___prevMeshSequence_13; }
	inline MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA ** get_address_of_prevMeshSequence_13() { return &___prevMeshSequence_13; }
	inline void set_prevMeshSequence_13(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * value)
	{
		___prevMeshSequence_13 = value;
		Il2CppCodeGenWriteBarrier((&___prevMeshSequence_13), value);
	}

	inline static int32_t get_offset_of_meshSequence_14() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___meshSequence_14)); }
	inline MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * get_meshSequence_14() const { return ___meshSequence_14; }
	inline MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA ** get_address_of_meshSequence_14() { return &___meshSequence_14; }
	inline void set_meshSequence_14(MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA * value)
	{
		___meshSequence_14 = value;
		Il2CppCodeGenWriteBarrier((&___meshSequence_14), value);
	}

	inline static int32_t get_offset_of_currentFrame_15() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___currentFrame_15)); }
	inline Frame_t569CB10118B510EF308215FA5E2BC6354316C27B * get_currentFrame_15() const { return ___currentFrame_15; }
	inline Frame_t569CB10118B510EF308215FA5E2BC6354316C27B ** get_address_of_currentFrame_15() { return &___currentFrame_15; }
	inline void set_currentFrame_15(Frame_t569CB10118B510EF308215FA5E2BC6354316C27B * value)
	{
		___currentFrame_15 = value;
		Il2CppCodeGenWriteBarrier((&___currentFrame_15), value);
	}

	inline static int32_t get_offset_of_TabChoise_16() { return static_cast<int32_t>(offsetof(MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838, ___TabChoise_16)); }
	inline int32_t get_TabChoise_16() const { return ___TabChoise_16; }
	inline int32_t* get_address_of_TabChoise_16() { return &___TabChoise_16; }
	inline void set_TabChoise_16(int32_t value)
	{
		___TabChoise_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHSEQUENCEPLAYER_T3E931E737D23E1AB49097ED6CF0CD5D100336838_H
#ifndef POINTCACHEPLAYER_T7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_H
#define POINTCACHEPLAYER_T7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCachePlayer
struct  PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean VertexAnimationTools_30.PointCachePlayer::UseTimescale
	bool ___UseTimescale_5;
	// VertexAnimationTools_30.PlayerUpdateMode VertexAnimationTools_30.PointCachePlayer::UpdateMode
	int32_t ___UpdateMode_6;
	// System.Single VertexAnimationTools_30.PointCachePlayer::timeDirection
	float ___timeDirection_7;
	// System.Int32 VertexAnimationTools_30.PointCachePlayer::ActiveMesh
	int32_t ___ActiveMesh_8;
	// UnityEngine.MeshCollider VertexAnimationTools_30.PointCachePlayer::MCollider
	MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * ___MCollider_9;
	// UnityEngine.Mesh VertexAnimationTools_30.PointCachePlayer::ColliderMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___ColliderMesh_10;
	// VertexAnimationTools_30.PointCachePlayer_Clip[] VertexAnimationTools_30.PointCachePlayer::Clips
	ClipU5BU5D_tAA79AB3C6F96CA78E7D4DE8DBB9F3D785C413603* ___Clips_11;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip0NormalizedTime
	float ___Clip0NormalizedTime_12;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip0Weight
	float ___Clip0Weight_13;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip1NormalizedTime
	float ___Clip1NormalizedTime_14;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip1Weight
	float ___Clip1Weight_15;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip2NormalizedTime
	float ___Clip2NormalizedTime_16;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip2Weight
	float ___Clip2Weight_17;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip3NormalizedTime
	float ___Clip3NormalizedTime_18;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip3Weight
	float ___Clip3Weight_19;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip4NormalizedTime
	float ___Clip4NormalizedTime_20;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip4Weight
	float ___Clip4Weight_21;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip5NormalizedTime
	float ___Clip5NormalizedTime_22;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip5Weight
	float ___Clip5Weight_23;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip6NormalizedTime
	float ___Clip6NormalizedTime_24;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip6Weight
	float ___Clip6Weight_25;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip7NormalizedTime
	float ___Clip7NormalizedTime_26;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip7Weight
	float ___Clip7Weight_27;
	// VertexAnimationTools_30.PointCache VertexAnimationTools_30.PointCachePlayer::pointCache
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * ___pointCache_28;
	// UnityEngine.SkinnedMeshRenderer VertexAnimationTools_30.PointCachePlayer::smr
	SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * ___smr_29;
	// VertexAnimationTools_30.PointCachePlayer_Constraint[] VertexAnimationTools_30.PointCachePlayer::Constraints
	ConstraintU5BU5D_tA24CD0C95C240251A402CA5801AEEFBA2A56756A* ___Constraints_30;
	// System.Int32 VertexAnimationTools_30.PointCachePlayer::PreparedMeshCollider
	int32_t ___PreparedMeshCollider_31;
	// System.Boolean VertexAnimationTools_30.PointCachePlayer::DrawMeshGizmo
	bool ___DrawMeshGizmo_32;

public:
	inline static int32_t get_offset_of_UseTimescale_5() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___UseTimescale_5)); }
	inline bool get_UseTimescale_5() const { return ___UseTimescale_5; }
	inline bool* get_address_of_UseTimescale_5() { return &___UseTimescale_5; }
	inline void set_UseTimescale_5(bool value)
	{
		___UseTimescale_5 = value;
	}

	inline static int32_t get_offset_of_UpdateMode_6() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___UpdateMode_6)); }
	inline int32_t get_UpdateMode_6() const { return ___UpdateMode_6; }
	inline int32_t* get_address_of_UpdateMode_6() { return &___UpdateMode_6; }
	inline void set_UpdateMode_6(int32_t value)
	{
		___UpdateMode_6 = value;
	}

	inline static int32_t get_offset_of_timeDirection_7() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___timeDirection_7)); }
	inline float get_timeDirection_7() const { return ___timeDirection_7; }
	inline float* get_address_of_timeDirection_7() { return &___timeDirection_7; }
	inline void set_timeDirection_7(float value)
	{
		___timeDirection_7 = value;
	}

	inline static int32_t get_offset_of_ActiveMesh_8() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___ActiveMesh_8)); }
	inline int32_t get_ActiveMesh_8() const { return ___ActiveMesh_8; }
	inline int32_t* get_address_of_ActiveMesh_8() { return &___ActiveMesh_8; }
	inline void set_ActiveMesh_8(int32_t value)
	{
		___ActiveMesh_8 = value;
	}

	inline static int32_t get_offset_of_MCollider_9() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___MCollider_9)); }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * get_MCollider_9() const { return ___MCollider_9; }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE ** get_address_of_MCollider_9() { return &___MCollider_9; }
	inline void set_MCollider_9(MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * value)
	{
		___MCollider_9 = value;
		Il2CppCodeGenWriteBarrier((&___MCollider_9), value);
	}

	inline static int32_t get_offset_of_ColliderMesh_10() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___ColliderMesh_10)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_ColliderMesh_10() const { return ___ColliderMesh_10; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_ColliderMesh_10() { return &___ColliderMesh_10; }
	inline void set_ColliderMesh_10(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___ColliderMesh_10 = value;
		Il2CppCodeGenWriteBarrier((&___ColliderMesh_10), value);
	}

	inline static int32_t get_offset_of_Clips_11() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clips_11)); }
	inline ClipU5BU5D_tAA79AB3C6F96CA78E7D4DE8DBB9F3D785C413603* get_Clips_11() const { return ___Clips_11; }
	inline ClipU5BU5D_tAA79AB3C6F96CA78E7D4DE8DBB9F3D785C413603** get_address_of_Clips_11() { return &___Clips_11; }
	inline void set_Clips_11(ClipU5BU5D_tAA79AB3C6F96CA78E7D4DE8DBB9F3D785C413603* value)
	{
		___Clips_11 = value;
		Il2CppCodeGenWriteBarrier((&___Clips_11), value);
	}

	inline static int32_t get_offset_of_Clip0NormalizedTime_12() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip0NormalizedTime_12)); }
	inline float get_Clip0NormalizedTime_12() const { return ___Clip0NormalizedTime_12; }
	inline float* get_address_of_Clip0NormalizedTime_12() { return &___Clip0NormalizedTime_12; }
	inline void set_Clip0NormalizedTime_12(float value)
	{
		___Clip0NormalizedTime_12 = value;
	}

	inline static int32_t get_offset_of_Clip0Weight_13() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip0Weight_13)); }
	inline float get_Clip0Weight_13() const { return ___Clip0Weight_13; }
	inline float* get_address_of_Clip0Weight_13() { return &___Clip0Weight_13; }
	inline void set_Clip0Weight_13(float value)
	{
		___Clip0Weight_13 = value;
	}

	inline static int32_t get_offset_of_Clip1NormalizedTime_14() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip1NormalizedTime_14)); }
	inline float get_Clip1NormalizedTime_14() const { return ___Clip1NormalizedTime_14; }
	inline float* get_address_of_Clip1NormalizedTime_14() { return &___Clip1NormalizedTime_14; }
	inline void set_Clip1NormalizedTime_14(float value)
	{
		___Clip1NormalizedTime_14 = value;
	}

	inline static int32_t get_offset_of_Clip1Weight_15() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip1Weight_15)); }
	inline float get_Clip1Weight_15() const { return ___Clip1Weight_15; }
	inline float* get_address_of_Clip1Weight_15() { return &___Clip1Weight_15; }
	inline void set_Clip1Weight_15(float value)
	{
		___Clip1Weight_15 = value;
	}

	inline static int32_t get_offset_of_Clip2NormalizedTime_16() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip2NormalizedTime_16)); }
	inline float get_Clip2NormalizedTime_16() const { return ___Clip2NormalizedTime_16; }
	inline float* get_address_of_Clip2NormalizedTime_16() { return &___Clip2NormalizedTime_16; }
	inline void set_Clip2NormalizedTime_16(float value)
	{
		___Clip2NormalizedTime_16 = value;
	}

	inline static int32_t get_offset_of_Clip2Weight_17() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip2Weight_17)); }
	inline float get_Clip2Weight_17() const { return ___Clip2Weight_17; }
	inline float* get_address_of_Clip2Weight_17() { return &___Clip2Weight_17; }
	inline void set_Clip2Weight_17(float value)
	{
		___Clip2Weight_17 = value;
	}

	inline static int32_t get_offset_of_Clip3NormalizedTime_18() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip3NormalizedTime_18)); }
	inline float get_Clip3NormalizedTime_18() const { return ___Clip3NormalizedTime_18; }
	inline float* get_address_of_Clip3NormalizedTime_18() { return &___Clip3NormalizedTime_18; }
	inline void set_Clip3NormalizedTime_18(float value)
	{
		___Clip3NormalizedTime_18 = value;
	}

	inline static int32_t get_offset_of_Clip3Weight_19() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip3Weight_19)); }
	inline float get_Clip3Weight_19() const { return ___Clip3Weight_19; }
	inline float* get_address_of_Clip3Weight_19() { return &___Clip3Weight_19; }
	inline void set_Clip3Weight_19(float value)
	{
		___Clip3Weight_19 = value;
	}

	inline static int32_t get_offset_of_Clip4NormalizedTime_20() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip4NormalizedTime_20)); }
	inline float get_Clip4NormalizedTime_20() const { return ___Clip4NormalizedTime_20; }
	inline float* get_address_of_Clip4NormalizedTime_20() { return &___Clip4NormalizedTime_20; }
	inline void set_Clip4NormalizedTime_20(float value)
	{
		___Clip4NormalizedTime_20 = value;
	}

	inline static int32_t get_offset_of_Clip4Weight_21() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip4Weight_21)); }
	inline float get_Clip4Weight_21() const { return ___Clip4Weight_21; }
	inline float* get_address_of_Clip4Weight_21() { return &___Clip4Weight_21; }
	inline void set_Clip4Weight_21(float value)
	{
		___Clip4Weight_21 = value;
	}

	inline static int32_t get_offset_of_Clip5NormalizedTime_22() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip5NormalizedTime_22)); }
	inline float get_Clip5NormalizedTime_22() const { return ___Clip5NormalizedTime_22; }
	inline float* get_address_of_Clip5NormalizedTime_22() { return &___Clip5NormalizedTime_22; }
	inline void set_Clip5NormalizedTime_22(float value)
	{
		___Clip5NormalizedTime_22 = value;
	}

	inline static int32_t get_offset_of_Clip5Weight_23() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip5Weight_23)); }
	inline float get_Clip5Weight_23() const { return ___Clip5Weight_23; }
	inline float* get_address_of_Clip5Weight_23() { return &___Clip5Weight_23; }
	inline void set_Clip5Weight_23(float value)
	{
		___Clip5Weight_23 = value;
	}

	inline static int32_t get_offset_of_Clip6NormalizedTime_24() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip6NormalizedTime_24)); }
	inline float get_Clip6NormalizedTime_24() const { return ___Clip6NormalizedTime_24; }
	inline float* get_address_of_Clip6NormalizedTime_24() { return &___Clip6NormalizedTime_24; }
	inline void set_Clip6NormalizedTime_24(float value)
	{
		___Clip6NormalizedTime_24 = value;
	}

	inline static int32_t get_offset_of_Clip6Weight_25() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip6Weight_25)); }
	inline float get_Clip6Weight_25() const { return ___Clip6Weight_25; }
	inline float* get_address_of_Clip6Weight_25() { return &___Clip6Weight_25; }
	inline void set_Clip6Weight_25(float value)
	{
		___Clip6Weight_25 = value;
	}

	inline static int32_t get_offset_of_Clip7NormalizedTime_26() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip7NormalizedTime_26)); }
	inline float get_Clip7NormalizedTime_26() const { return ___Clip7NormalizedTime_26; }
	inline float* get_address_of_Clip7NormalizedTime_26() { return &___Clip7NormalizedTime_26; }
	inline void set_Clip7NormalizedTime_26(float value)
	{
		___Clip7NormalizedTime_26 = value;
	}

	inline static int32_t get_offset_of_Clip7Weight_27() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip7Weight_27)); }
	inline float get_Clip7Weight_27() const { return ___Clip7Weight_27; }
	inline float* get_address_of_Clip7Weight_27() { return &___Clip7Weight_27; }
	inline void set_Clip7Weight_27(float value)
	{
		___Clip7Weight_27 = value;
	}

	inline static int32_t get_offset_of_pointCache_28() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___pointCache_28)); }
	inline PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * get_pointCache_28() const { return ___pointCache_28; }
	inline PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 ** get_address_of_pointCache_28() { return &___pointCache_28; }
	inline void set_pointCache_28(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * value)
	{
		___pointCache_28 = value;
		Il2CppCodeGenWriteBarrier((&___pointCache_28), value);
	}

	inline static int32_t get_offset_of_smr_29() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___smr_29)); }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * get_smr_29() const { return ___smr_29; }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 ** get_address_of_smr_29() { return &___smr_29; }
	inline void set_smr_29(SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * value)
	{
		___smr_29 = value;
		Il2CppCodeGenWriteBarrier((&___smr_29), value);
	}

	inline static int32_t get_offset_of_Constraints_30() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Constraints_30)); }
	inline ConstraintU5BU5D_tA24CD0C95C240251A402CA5801AEEFBA2A56756A* get_Constraints_30() const { return ___Constraints_30; }
	inline ConstraintU5BU5D_tA24CD0C95C240251A402CA5801AEEFBA2A56756A** get_address_of_Constraints_30() { return &___Constraints_30; }
	inline void set_Constraints_30(ConstraintU5BU5D_tA24CD0C95C240251A402CA5801AEEFBA2A56756A* value)
	{
		___Constraints_30 = value;
		Il2CppCodeGenWriteBarrier((&___Constraints_30), value);
	}

	inline static int32_t get_offset_of_PreparedMeshCollider_31() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___PreparedMeshCollider_31)); }
	inline int32_t get_PreparedMeshCollider_31() const { return ___PreparedMeshCollider_31; }
	inline int32_t* get_address_of_PreparedMeshCollider_31() { return &___PreparedMeshCollider_31; }
	inline void set_PreparedMeshCollider_31(int32_t value)
	{
		___PreparedMeshCollider_31 = value;
	}

	inline static int32_t get_offset_of_DrawMeshGizmo_32() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___DrawMeshGizmo_32)); }
	inline bool get_DrawMeshGizmo_32() const { return ___DrawMeshGizmo_32; }
	inline bool* get_address_of_DrawMeshGizmo_32() { return &___DrawMeshGizmo_32; }
	inline void set_DrawMeshGizmo_32(bool value)
	{
		___DrawMeshGizmo_32 = value;
	}
};

struct PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_StaticFields
{
public:
	// UnityEngine.Color[] VertexAnimationTools_30.PointCachePlayer::GizmosClipColors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___GizmosClipColors_4;

public:
	inline static int32_t get_offset_of_GizmosClipColors_4() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_StaticFields, ___GizmosClipColors_4)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_GizmosClipColors_4() const { return ___GizmosClipColors_4; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_GizmosClipColors_4() { return &___GizmosClipColors_4; }
	inline void set_GizmosClipColors_4(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___GizmosClipColors_4 = value;
		Il2CppCodeGenWriteBarrier((&___GizmosClipColors_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCACHEPLAYER_T7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_H
#ifndef FRESNELSCRIPT_TB9629A7C001A3BABEE37CF42E8FCA19C41AA1732_H
#define FRESNELSCRIPT_TB9629A7C001A3BABEE37CF42E8FCA19C41AA1732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// fresnelScript
struct  fresnelScript_tB9629A7C001A3BABEE37CF42E8FCA19C41AA1732  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.MeshRenderer fresnelScript::mesh
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___mesh_4;
	// UnityEngine.Material fresnelScript::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_5;
	// System.Single fresnelScript::alpha
	float ___alpha_6;

public:
	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(fresnelScript_tB9629A7C001A3BABEE37CF42E8FCA19C41AA1732, ___mesh_4)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_mesh_4() const { return ___mesh_4; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(fresnelScript_tB9629A7C001A3BABEE37CF42E8FCA19C41AA1732, ___material_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_5() const { return ___material_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_alpha_6() { return static_cast<int32_t>(offsetof(fresnelScript_tB9629A7C001A3BABEE37CF42E8FCA19C41AA1732, ___alpha_6)); }
	inline float get_alpha_6() const { return ___alpha_6; }
	inline float* get_address_of_alpha_6() { return &___alpha_6; }
	inline void set_alpha_6(float value)
	{
		___alpha_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRESNELSCRIPT_TB9629A7C001A3BABEE37CF42E8FCA19C41AA1732_H
#ifndef MANFRESNELSCRIPT_TCDA12259CDE2EBD210054BE15A17EB52A2723729_H
#define MANFRESNELSCRIPT_TCDA12259CDE2EBD210054BE15A17EB52A2723729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// manfresnelScript
struct  manfresnelScript_tCDA12259CDE2EBD210054BE15A17EB52A2723729  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.MeshRenderer manfresnelScript::mesh
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___mesh_4;
	// UnityEngine.Material manfresnelScript::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_5;
	// System.Single manfresnelScript::alpha
	float ___alpha_6;

public:
	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(manfresnelScript_tCDA12259CDE2EBD210054BE15A17EB52A2723729, ___mesh_4)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_mesh_4() const { return ___mesh_4; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(manfresnelScript_tCDA12259CDE2EBD210054BE15A17EB52A2723729, ___material_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_5() const { return ___material_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_alpha_6() { return static_cast<int32_t>(offsetof(manfresnelScript_tCDA12259CDE2EBD210054BE15A17EB52A2723729, ___alpha_6)); }
	inline float get_alpha_6() const { return ___alpha_6; }
	inline float* get_address_of_alpha_6() { return &___alpha_6; }
	inline void set_alpha_6(float value)
	{
		___alpha_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANFRESNELSCRIPT_TCDA12259CDE2EBD210054BE15A17EB52A2723729_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3700[2] = 
{
	UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4::get_offset_of_BeginFrameRendering_0(),
	UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4::get_offset_of_BeginCameraRendering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (UnityAndroidPermissions_tA4CE60356A3424ECF5E1AB1F39264182BB49D7CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3703[6] = 
{
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_axisName_4(),
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_axisValue_5(),
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_responseSpeed_6(),
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_returnToCentreSpeed_7(),
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_m_PairedWith_8(),
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_m_Axis_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (ButtonHandler_tA53C84C0B2F2670630A8B3B02001C0E6F773C631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3704[1] = 
{
	ButtonHandler_tA53C84C0B2F2670630A8B3B02001C0E6F773C631::get_offset_of_Name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E), -1, sizeof(CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3705[3] = 
{
	CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3706[3] = 
{
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B::get_offset_of_m_Value_1(),
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3707[5] = 
{
	VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (InputAxisScrollbar_t935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3708[1] = 
{
	InputAxisScrollbar_t935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7::get_offset_of_axis_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3709[9] = 
{
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_MovementRange_4(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_axesToUse_5(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_horizontalAxisName_6(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_verticalAxisName_7(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_m_StartPos_8(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_m_UseX_9(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_m_UseY_10(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_m_HorizontalVirtualAxis_11(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_m_VerticalVirtualAxis_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (AxisOption_t2E4FAD7B3F55CE394680B6CEA9C2F01B92849786)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3710[4] = 
{
	AxisOption_t2E4FAD7B3F55CE394680B6CEA9C2F01B92849786::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (MobileControlRig_tD348E6259B1A94E95D09BD393ADAC945B3515418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3712[5] = 
{
	TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53::get_offset_of_mapping_4(),
	TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53::get_offset_of_tiltAroundAxis_5(),
	TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53::get_offset_of_fullTiltAngle_6(),
	TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53::get_offset_of_centreAngleOffset_7(),
	TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53::get_offset_of_m_SteerAxis_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (AxisOptions_t7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3713[3] = 
{
	AxisOptions_t7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3714[2] = 
{
	AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E::get_offset_of_type_0(),
	AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (MappingType_tA5DC189BC7DB7B5AE049E710D502B88A32BA87B3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3715[5] = 
{
	MappingType_tA5DC189BC7DB7B5AE049E710D502B88A32BA87B3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3716[18] = 
{
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_axesToUse_4(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_controlStyle_5(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_horizontalAxisName_6(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_verticalAxisName_7(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_Xsensitivity_8(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_Ysensitivity_9(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_StartPos_10(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_PreviousDelta_11(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_JoytickOutput_12(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_UseX_13(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_UseY_14(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_HorizontalVirtualAxis_15(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_VerticalVirtualAxis_16(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_Dragging_17(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_Id_18(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_PreviousTouchPos_19(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_Center_20(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_Image_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (AxisOption_tA48A0481101792CB4679A6C5D81F14DB1582FF11)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3717[4] = 
{
	AxisOption_tA48A0481101792CB4679A6C5D81F14DB1582FF11::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (ControlStyle_t60A34C5903F112F7E6982BB8161F9F7372669CAC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3718[4] = 
{
	ControlStyle_t60A34C5903F112F7E6982BB8161F9F7372669CAC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { sizeof (VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3719[4] = 
{
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (MobileInput_t219DDF915FF54DBE73004E0163F8BFCB4DA3BA44), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { sizeof (StandaloneInput_t9BFD53669697FC88D0D237110D8D1DF76400AFEB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3722[10] = 
{
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8::get_offset_of_mc_0(),
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8::get_offset_of_imc_1(),
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8::get_offset_of_tris_2(),
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8::get_offset_of_ltw_3(),
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8::get_offset_of_dirs_4(),
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8::get_offset_of_vertices_5(),
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8::get_offset_of_normals_6(),
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8::get_offset_of_tangents_7(),
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8::get_offset_of_edges_8(),
	BindingHelper_t94750971A156A320AB3463254F2A9A6702A772E8::get_offset_of_edgesArr_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (Edge_t82512244422942C9C917667D89F2511A61F288B0)+ sizeof (RuntimeObject), sizeof(Edge_t82512244422942C9C917667D89F2511A61F288B0 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3723[7] = 
{
	Edge_t82512244422942C9C917667D89F2511A61F288B0::get_offset_of_A_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Edge_t82512244422942C9C917667D89F2511A61F288B0::get_offset_of_B_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Edge_t82512244422942C9C917667D89F2511A61F288B0::get_offset_of_pA_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Edge_t82512244422942C9C917667D89F2511A61F288B0::get_offset_of_pB_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Edge_t82512244422942C9C917667D89F2511A61F288B0::get_offset_of_ab_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Edge_t82512244422942C9C917667D89F2511A61F288B0::get_offset_of_length_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Edge_t82512244422942C9C917667D89F2511A61F288B0::get_offset_of_length2_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3724[21] = 
{
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_RemoveLight_4(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_RemoveDark_5(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_AddLight_6(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_AddDark_7(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_RefreshLight_8(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_RefreshDark_9(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_Graph0101BackgroundLight_10(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_Graph0101BackgroundDark_11(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_QuadIconButtonStyle_12(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_TLabel_13(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_ConstraintLabel_14(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_ConstraintIcon_15(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_ConstraintEditLabel_16(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_ConstraintEditIcon_17(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_MeshSequenceIcon_18(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_PointCacheIcon_19(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_RequireReimportLight_20(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_RequireReimportDark_21(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_PressedButton_22(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_FoldoutToggle_23(),
	EditorResourcesHolderSO_tE30C521C4E4E1D9B76961DCE71712ED165B83287::get_offset_of_FoldoutToggleDark_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (InterpolateModeEnum_t4C69237CDE436AC34D5440295EF0FE18E6D18EA8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3725[3] = 
{
	InterpolateModeEnum_t4C69237CDE436AC34D5440295EF0FE18E6D18EA8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (TransitionModeEnum_tB044BF8C2E6D830051A88CC905F5E8BF910ED946)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3726[4] = 
{
	TransitionModeEnum_tB044BF8C2E6D830051A88CC905F5E8BF910ED946::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (PlayerUpdateMode_t69E6A34DE274376292987D8D567B1E7B30220293)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3727[4] = 
{
	PlayerUpdateMode_t69E6A34DE274376292987D8D567B1E7B30220293::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (AutoPlaybackTypeEnum_tF7A9D12E935B6A75A0B4D544954EF4DFF033BC44)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3728[5] = 
{
	AutoPlaybackTypeEnum_tF7A9D12E935B6A75A0B4D544954EF4DFF033BC44::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3729[3] = 
{
	MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0::get_offset_of_Mat_0(),
	MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0::get_offset_of_Used_1(),
	MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1)+ sizeof (RuntimeObject), sizeof(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3730[2] = 
{
	TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1::get_offset_of_Persentage_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (NormalizedSpline_t69AF6108C4A53B141ADA0C9952918B21F43779E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3731[3] = 
{
	NormalizedSpline_t69AF6108C4A53B141ADA0C9952918B21F43779E7::get_offset_of_Vertices_0(),
	NormalizedSpline_t69AF6108C4A53B141ADA0C9952918B21F43779E7::get_offset_of_Distances_1(),
	NormalizedSpline_t69AF6108C4A53B141ADA0C9952918B21F43779E7::get_offset_of_Length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8)+ sizeof (RuntimeObject), sizeof(BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3732[5] = 
{
	BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8::get_offset_of_VidxA_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8::get_offset_of_VidxB_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8::get_offset_of_VidxC_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8::get_offset_of_Bary_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BindInfo_t75FBC726121A66B131BD615CEF61C9064A13AFA8::get_offset_of_TrisSpace_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D)+ sizeof (RuntimeObject), sizeof(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D ), 0, 0 };
extern const int32_t g_FieldOffsetTable3733[3] = 
{
	PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D::get_offset_of_P_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D::get_offset_of_F_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D::get_offset_of_U_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775)+ sizeof (RuntimeObject), sizeof(NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3734[2] = 
{
	NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775::get_offset_of_Min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumbersRange_t8E4C7EFBF23B572F991E58128981D31BAC842775::get_offset_of_Max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (ConstraintClip_tD597EA3660DFD882F13B25065680AABEBDCF91A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3735[1] = 
{
	ConstraintClip_tD597EA3660DFD882F13B25065680AABEBDCF91A9::get_offset_of_Frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3736[3] = 
{
	TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F::get_offset_of_Name_0(),
	TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F::get_offset_of_items_1(),
	TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F::get_offset_of_itemsDict_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3737[5] = 
{
	Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4::get_offset_of_Name_0(),
	Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4::get_offset_of_Iterations_1(),
	Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4::get_offset_of_NormalizedFrom_2(),
	Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4::get_offset_of_NormalizedTo_3(),
	Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4::get_offset_of_Weight_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (Extension_tF6DBF61AFC70AA0040187AEAEDF04F47352C6795), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3739[6] = 
{
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8::get_offset_of_frames_0(),
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8::get_offset_of_IsLoop_1(),
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8::get_offset_of_Interpolation_2(),
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8::get_offset_of_b1_3(),
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8::get_offset_of_b2_4(),
	FramesArray_t3FA26E290063A6BFC3BE95E002191F09335FC4E8::get_offset_of_b3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3740[17] = 
{
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CU3E1__state_0(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CU3E2__current_1(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CU3E4__this_3(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_iterations_4(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CU3E3__iterations_5(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_smoothMin_6(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CU3E3__smoothMin_7(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_smoothMax_8(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CU3E3__smoothMax_9(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_easeOffset_10(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CU3E3__easeOffset_11(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_easeLength_12(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CU3E3__easeLength_13(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CsmoothedU3E5__2_14(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CtempU3E5__3_15(),
	U3CSmoothIEU3Ed__26_tC2C1944834010B560BEA42332BE6BD620C942BE8::get_offset_of_U3CiU3E5__4_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3741[5] = 
{
	MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C::get_offset_of_State_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C::get_offset_of_infos_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C::get_offset_of_SequenceName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C::get_offset_of_DirectoryPath_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshSequenceInfo_tE6A3120CE71A5B79CC9C74CBE459B45F0279342C::get_offset_of_SortMode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (StateEnum_t1A5CA96CCB6E733D9C08173F783ED35D49C3C242)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3742[4] = 
{
	StateEnum_t1A5CA96CCB6E733D9C08173F783ED35D49C3C242::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (SortModeEnum_t00E0F838F904669A96A495F07E911ACEB856FADF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3743[3] = 
{
	SortModeEnum_t00E0F838F904669A96A495F07E911ACEB856FADF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3744[4] = 
{
	ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846::get_offset_of_FileName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846::get_offset_of_Hash_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846::get_offset_of_ParsedNumber_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjFileInfo_t9BE0BE91810D7410D2C388DDAF3BF8F841FF9846::get_offset_of_fi_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3745[37] = 
{
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_Name_0(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_FlipNormals_1(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_CalculateNormals_2(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_ImportUV_3(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_CalculateTangents_4(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_SmoothingGroupsMode_5(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_NormalsRecalculationMode_6(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_UM_vertices_7(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_UM_uv_8(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_UM_normals_9(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_UM_v3tangents_10(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_UM_colors_11(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_UM_v4tangents_12(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_UM_Bounds_13(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_VerticesToSet_14(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_AllFaces_15(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_SubMeshes_16(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_Vertices_17(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_Normals_18(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_mapVertices_19(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_bindVerts_20(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_bindNormals_21(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_bindTangents_22(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_IndexFormat_23(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_UnityVertices_24(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_AllPolygons_25(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_refMesh_26(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_subString_27(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_objFile_28(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_currentSmoothingGroup_29(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_offSGMode_30(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_offSGcounter_31(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_spaceSeparator_32(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_objFileLength_33(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_BuildSW_34(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_ApplySW_35(),
	ObjData_tEFABA5323FBC9AA15B72ADA7B77993F69F66FA62::get_offset_of_fplp_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3746[5] = 
{
	UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC::get_offset_of_ThisIdx_0(),
	UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC::get_offset_of_Position_1(),
	UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC::get_offset_of_Normal_2(),
	UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC::get_offset_of_UV_3(),
	UMeshVertex_tE2BF83A5C98E610175CE747D6657859E6EBD37CC::get_offset_of_Bi_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3747[2] = 
{
	UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997::get_offset_of_itemsDict_0(),
	UMeshVerticesList_tC475FE5EEBD1A9F01789D898112D00E602FFF997::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3748[10] = 
{
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D::get_offset_of_Value_0(),
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D::get_offset_of_AO_1(),
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D::get_offset_of_IsInnerAO_2(),
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D::get_offset_of_SmoothingGroup_3(),
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D::get_offset_of_Position_4(),
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D::get_offset_of_AdjacentPolygons_5(),
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D::get_offset_of_aAdjacentPolygons_6(),
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D::get_offset_of_AdjacentNormals_7(),
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D::get_offset_of_Cavity_8(),
	NormalVertex_t82CB2897B045FD15284797267376441CA5ADAE4D::get_offset_of_AdjacentMult_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { sizeof (NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3749[2] = 
{
	NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001::get_offset_of_items_0(),
	NormalsList_tF0247CD879F6DA7D69B829C5D681042015C2E001::get_offset_of_itemsDict_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3750[6] = 
{
	MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8::get_offset_of_UMV_0(),
	MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8::get_offset_of_Value_1(),
	MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8::get_offset_of_AdjacentPolygons_2(),
	MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8::get_offset_of_aAdjacentPolygons_3(),
	MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8::get_offset_of_Tangent_4(),
	MapVertex_t9B430990F9D13CEABE74D76884A4D730A72CD7D8::get_offset_of_TangentW_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3751[2] = 
{
	MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491::get_offset_of_objmvs_0(),
	MapVerticesList_t5FF455607E356DD44247943103835EBF2183F491::get_offset_of_forgedMvs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3752[6] = 
{
	U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E::get_offset_of_U3CU3E1__state_0(),
	U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E::get_offset_of_U3CU3E2__current_1(),
	U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E::get_offset_of_U3CU3E4__this_3(),
	U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E::get_offset_of_U3CiU3E5__2_4(),
	U3CAllMapVertsU3Ed__5_tC0166927CCBD50DBCAAB5AD8D9743DB95A77EA2E::get_offset_of_U3CU3E7__wrap2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3753[14] = 
{
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_Idx_0(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_Va_1(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_Vb_2(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_Vc_3(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_ParentPolygon_4(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_TangentT_5(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_TangentW_6(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_Sign_7(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_Normal_8(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_Tangent_9(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_Center_10(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_Area_11(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_isParallel_12(),
	Face_t9B11D8A1D1422AA7F0854CFADF0BE45F4A0E5ACB::get_offset_of_tv_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { sizeof (PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3754[5] = 
{
	PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186::get_offset_of_BindPos_0(),
	PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186::get_offset_of_Value_1(),
	PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186::get_offset_of_AdjacentPolygons_2(),
	PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186::get_offset_of_IsInner_3(),
	PositionVertex_t734DA4E056F2DEFDA0D24559A8BEFB2D75837186::get_offset_of_InnerAO_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3755[12] = 
{
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_SmoothingGroup_0(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_Normal_1(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_Tangent_2(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_Center_3(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_Area_4(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_corners_5(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_AO_6(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_InnerAO_7(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_Cavity_8(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_FacesMult_9(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_CornersMult_10(),
	Polygon_t5AD583D973314A72EACF4F8794C2471F93EDE8B2::get_offset_of_Faces_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (AdjacentPolygon_tDFA687AB2A5D03856F53EBFBE3CA16D5B1D7CA17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3756[2] = 
{
	AdjacentPolygon_tDFA687AB2A5D03856F53EBFBE3CA16D5B1D7CA17::get_offset_of_polygon_0(),
	AdjacentPolygon_tDFA687AB2A5D03856F53EBFBE3CA16D5B1D7CA17::get_offset_of_AreaMult_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3757[3] = 
{
	SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330::get_offset_of_MaterialName_0(),
	SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330::get_offset_of_TrisIndeces_1(),
	SubMesh_tADC64FBEFFF2B2DC28F7D212D60807B04FE6F330::get_offset_of_Polygons_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { sizeof (SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3758[2] = 
{
	SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA::get_offset_of_items_0(),
	SubMeshesList_t835A463649E47CD647BC4EBA1975E42CA2A919FA::get_offset_of_Current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (PolygonTriangulator_t3CADF3489835B31F8D09A77FAA00C2DAA5091CFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3759[3] = 
{
	PolygonTriangulator_t3CADF3489835B31F8D09A77FAA00C2DAA5091CFF::get_offset_of_corners_0(),
	PolygonTriangulator_t3CADF3489835B31F8D09A77FAA00C2DAA5091CFF::get_offset_of_trianglesIndeces_1(),
	PolygonTriangulator_t3CADF3489835B31F8D09A77FAA00C2DAA5091CFF::get_offset_of_minAngleCorner_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { sizeof (Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3760[4] = 
{
	Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821::get_offset_of_SourceIdx_0(),
	Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821::get_offset_of_WorldPoint_1(),
	Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821::get_offset_of_PolygonSpacePoint_2(),
	Corner_tB30359CF24D1E97BB2B11F3700FB1CC8768F4821::get_offset_of_Angle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { sizeof (Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D)+ sizeof (RuntimeObject), sizeof(Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D ), 0, 0 };
extern const int32_t g_FieldOffsetTable3761[9] = 
{
	Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D::get_offset_of_a_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D::get_offset_of_b_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D::get_offset_of_c_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D::get_offset_of_v0_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D::get_offset_of_v1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D::get_offset_of_dot00_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D::get_offset_of_dot01_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D::get_offset_of_dot11_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Triangle2d_t62295746A1A9009D2518E16CBEEDDEE489C6BA8D::get_offset_of_invDenom_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { sizeof (SmoothingGroupImportModeEnum_t8ED4A6B144003B5D3F74060965BB1B38FA190261)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3762[4] = 
{
	SmoothingGroupImportModeEnum_t8ED4A6B144003B5D3F74060965BB1B38FA190261::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (NormalsRecalculationModeEnum_t0F9CC811A0AD33784B9A02EA5BB55CD8DC533A41)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3763[3] = 
{
	NormalsRecalculationModeEnum_t0F9CC811A0AD33784B9A02EA5BB55CD8DC533A41::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (ObjLineIdEnum_t7150015CDFE28B7C65B4E06AF07BAD11EC170370)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3764[8] = 
{
	ObjLineIdEnum_t7150015CDFE28B7C65B4E06AF07BAD11EC170370::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { sizeof (FaceLineParser_t3456F4CAF39256EA2ED2F481EA54AAD232B26FC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3765[1] = 
{
	FaceLineParser_t3456F4CAF39256EA2ED2F481EA54AAD232B26FC7::get_offset_of_corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { sizeof (Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3766[5] = 
{
	Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8::get_offset_of_VertIdx_0(),
	Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8::get_offset_of_UvIdx_1(),
	Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8::get_offset_of_NormalIdx_2(),
	Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8::get_offset_of_chars_3(),
	Corner_t8E8024D987474216BA43F7AAB17DCEEBD453CBF8::get_offset_of_ci_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { sizeof (VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3767[17] = 
{
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_go_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_quality_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_Cavity_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_CavityAmount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_CavityAngleMin_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_CavityAngleMax_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_CavityBlur_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_CavityBlurIterations_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_InnerVertexOcclusion_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_InnerVertexOcclusionAmount_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_InnerVertexOcclusionBlur_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_InnerVertexOcclusionBlurIterations_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_AmbientOcclusion_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_AmbientOcclusionAmount_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_AmbientOcclusionRadius_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_AmbientOcclusionBlur_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexColorsSettings_t53884E0DB2B39EAAEAA3C008189AAE64B5BDA84B::get_offset_of_AmbientOcclusionBlurIterations_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { sizeof (U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3768[8] = 
{
	U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A::get_offset_of_U3CU3E1__state_0(),
	U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A::get_offset_of_U3CU3E2__current_1(),
	U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A::get_offset_of_U3CU3E4__this_3(),
	U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A::get_offset_of_bh_4(),
	U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A::get_offset_of_U3CU3E3__bh_5(),
	U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A::get_offset_of_U3CtinameU3E5__2_6(),
	U3CBindIEU3Ed__56_t899FB537CA32CF081270FD9EFC641829CC5A193A::get_offset_of_U3CiU3E5__3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { sizeof (PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3769[8] = 
{
	PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5::get_offset_of_Name_0(),
	PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5::get_offset_of_Frames_1(),
	PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5::get_offset_of_clip_2(),
	PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5::get_offset_of_cis_3(),
	PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5::get_offset_of_pointCache_4(),
	PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5::get_offset_of_geometryVerticesCount_5(),
	PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5::get_offset_of_IsMeshSequence_6(),
	PointCacheData_t5D22CAA1F1B13B97ADA92DA8EAD1B2158635F0F5::get_offset_of_tstack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { sizeof (U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3770[15] = 
{
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CU3E1__state_0(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CU3E2__current_1(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CU3E4__this_3(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CTransitionFramesU3E5__2_4(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CreadFramesCounterU3E5__3_5(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CmsiU3E5__4_6(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CfU3E5__5_7(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CobjVerticesU3E5__6_8(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CfsU3E5__7_9(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CbinReaderU3E5__8_10(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CretimedFramesU3E5__9_11(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CstepU3E5__10_12(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CU3E7__wrap10_13(),
	U3CBuildU3Ed__10_tD0C96D53FA455C6CBB66EFA452911C69D3616E62::get_offset_of_U3CmPathVertListU3E5__12_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { sizeof (ProjectionQualityEnum_t3EF48B93088C7D81A7804EF2F56E48CBB8C4564F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3771[6] = 
{
	ProjectionQualityEnum_t3EF48B93088C7D81A7804EF2F56E48CBB8C4564F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { sizeof (ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3772[2] = 
{
	ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708::get_offset_of_SphereSamples_4(),
	ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708::get_offset_of_DomeSamples_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { sizeof (Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3773[2] = 
{
	Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08::get_offset_of_Name_0(),
	Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08::get_offset_of_Dirs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { sizeof (VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3774[1] = 
{
	VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252::get_offset_of_Materials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { sizeof (MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3775[9] = 
{
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA::get_offset_of_PreImport_5(),
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA::get_offset_of_PostImport_6(),
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA::get_offset_of_Frames_7(),
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA::get_offset_of_SequenceVerticesCount_8(),
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA::get_offset_of_SequenceObjVerticesCount_9(),
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA::get_offset_of_SequenceTrianglesCount_10(),
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA::get_offset_of_SequenceObjPolygonsCount_11(),
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA::get_offset_of_SequenceSubmeshesCount_12(),
	MeshSequence_tCAA5510E0F5678834DCB299C86607D56EC8FC0CA::get_offset_of_AssetFileSize_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (Frame_t569CB10118B510EF308215FA5E2BC6354316C27B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3776[7] = 
{
	Frame_t569CB10118B510EF308215FA5E2BC6354316C27B::get_offset_of_Name_0(),
	Frame_t569CB10118B510EF308215FA5E2BC6354316C27B::get_offset_of_ObjPolygonsCount_1(),
	Frame_t569CB10118B510EF308215FA5E2BC6354316C27B::get_offset_of_ObjVerticesCount_2(),
	Frame_t569CB10118B510EF308215FA5E2BC6354316C27B::get_offset_of_MeshVertsCount_3(),
	Frame_t569CB10118B510EF308215FA5E2BC6354316C27B::get_offset_of_MeshTrisCount_4(),
	Frame_t569CB10118B510EF308215FA5E2BC6354316C27B::get_offset_of_FrameMesh_5(),
	Frame_t569CB10118B510EF308215FA5E2BC6354316C27B::get_offset_of_Materials_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { sizeof (ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3777[22] = 
{
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_PathToObj_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_MSI_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_ImportCustomRange_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_ImportFromFrame_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_ImportToFrame_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_SwapYZAxis_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_PivotOffset_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_ScaleFactor_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_FlipNormals_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_ImportUV_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_CalculateNormals_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_CalculateTangents_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_SmoothingGroupImportMode_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_NormalRecalculationMode_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_MeshCompression_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_OptimizeMesh_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_IndexFormat_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_GenerateMaterials_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_VColorSettings_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_NormalizedPerFrame_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_FilesSortMode_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tD2BA041D78A8DB7465355499EAFEC198F62CBA2D::get_offset_of_ImportDate_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { sizeof (MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3778[13] = 
{
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_MF_4(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_MC_5(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_MR_6(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_UpdateMode_7(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_PlaybackMode_8(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_FramesPerSecond_9(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_UseTimescale_10(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_NormalizedTime_11(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_timeDirection_12(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_prevMeshSequence_13(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_meshSequence_14(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_currentFrame_15(),
	MeshSequencePlayer_t3E931E737D23E1AB49097ED6CF0CD5D100336838::get_offset_of_TabChoise_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { sizeof (U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3779[11] = 
{
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_U3CU3E1__state_0(),
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_U3CU3E2__current_1(),
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_t_3(),
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_U3CU3E3__t_4(),
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_U3CU3E4__this_5(),
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_U3CimportToU3E5__2_6(),
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_U3CtotalFramesCountU3E5__3_7(),
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_U3CnframesU3E5__4_8(),
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_U3CcounterU3E5__5_9(),
	U3CImportIEU3Ed__21_t723562B682F441D32174CAFFCA594171268B053D::get_offset_of_U3CfU3E5__6_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { sizeof (PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3780[13] = 
{
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_PreImport_5(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_PostImport_6(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_Clips_7(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_Meshes_8(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_PreConstraints_9(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_PostConstraints_10(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_ImportSettingsIsDirtyFlag_11(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_ConstraintHandlesSize_12(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_DrawConstraintHandlesName_13(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_SelectedTabIdx_14(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_SelectedImportTabIdx_15(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_AssetFileSize_16(),
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7::get_offset_of_ImportingDate_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { sizeof (PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3781[10] = 
{
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C::get_offset_of_Name_0(),
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C::get_offset_of_Path_1(),
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C::get_offset_of_MeshName_2(),
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C::get_offset_of_VertsCount_3(),
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C::get_offset_of_PolygonsCount_4(),
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C::get_offset_of_SubMeshesCount_5(),
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C::get_offset_of_Info_6(),
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C::get_offset_of_mesh_7(),
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C::get_offset_of_Materials_8(),
	PolygonMesh_tD75E6669E4E51A1B4621D2ACA63EE1B2FEE8EF4C::get_offset_of_od_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { sizeof (Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3782[5] = 
{
	Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553::get_offset_of_PreImport_0(),
	Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553::get_offset_of_PostImport_1(),
	Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553::get_offset_of_FoldoutState_2(),
	Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553::get_offset_of_MotionPathsCount_3(),
	Clip_t60B69E5DD4C63C3D061B39C5EDCECD99F3F36553::get_offset_of_MotionPathVertices_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { sizeof (ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE)+ sizeof (RuntimeObject), sizeof(ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3783[29] = 
{
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_FoldoutIsOpen_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_FilePath_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_FileName_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_FileVertsCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_FileFramesCount_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_FileInfo_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_SwapYZAxis_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_Scale_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_IsLoop_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_EnableCustomRange_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_CustomRangeFrom_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_CustomRangeTo_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_TransitionMode_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_TransitionLength_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_ChangeFramesCount_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_CustomFramesCount_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_SubFrameInterpolation_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_FrameIdxOffset_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_EnableMotionSmoothing_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_MotionSmoothIterations_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_MotionSmoothAmountMin_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_MotionSmoothAmountMax_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_MotionSmoothEaseOffset_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_MotionSmoothEaseLength_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_GenerageMotionPaths_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_MotionPathsIndexStep_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_EnableNormalizeSpeed_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_tDD231CF2C6B9AF0231FB2F9F1439305B4E262AEE::get_offset_of_NormalizeSpeedPercentage_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { sizeof (ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C)+ sizeof (RuntimeObject), sizeof(ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3784[12] = 
{
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_SwapYZAxis_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_ScaleFactor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_FlipNormals_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_SmoothingGroupImportMode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_NormalRecalculationMode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_MeshCompression_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_OptimizeMesh_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_UsedClipsCount_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_UsedMeshesCount_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_GenerateMaterials_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_SavePortableData_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImportSettings_t9869042A81D4717B91E68D4889002EED19751A3C::get_offset_of_IndexFormat_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785 = { sizeof (PreImportConstraint_t15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3785[3] = 
{
	PreImportConstraint_t15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC::get_offset_of_Name_0(),
	PreImportConstraint_t15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC::get_offset_of_ObjSpace_1(),
	PreImportConstraint_t15DECBB232AAE98BEC1A1AB5B7DF868287A9E4AC::get_offset_of_BI_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786 = { sizeof (PostImportConstraint_t224DDBE801C7017C5D90625F743A0A22FDE88131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3786[3] = 
{
	PostImportConstraint_t224DDBE801C7017C5D90625F743A0A22FDE88131::get_offset_of_Name_0(),
	PostImportConstraint_t224DDBE801C7017C5D90625F743A0A22FDE88131::get_offset_of_ObjSpace_1(),
	PostImportConstraint_t224DDBE801C7017C5D90625F743A0A22FDE88131::get_offset_of_Clips_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787 = { sizeof (PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F), -1, sizeof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3787[29] = 
{
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_StaticFields::get_offset_of_GizmosClipColors_4(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_UseTimescale_5(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_UpdateMode_6(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_timeDirection_7(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_ActiveMesh_8(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_MCollider_9(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_ColliderMesh_10(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clips_11(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip0NormalizedTime_12(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip0Weight_13(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip1NormalizedTime_14(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip1Weight_15(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip2NormalizedTime_16(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip2Weight_17(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip3NormalizedTime_18(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip3Weight_19(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip4NormalizedTime_20(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip4Weight_21(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip5NormalizedTime_22(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip5Weight_23(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip6NormalizedTime_24(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip6Weight_25(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip7NormalizedTime_26(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Clip7Weight_27(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_pointCache_28(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_smr_29(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_Constraints_30(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_PreparedMeshCollider_31(),
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F::get_offset_of_DrawMeshGizmo_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788 = { sizeof (Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3788[6] = 
{
	Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77::get_offset_of_Idx_0(),
	Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77::get_offset_of_Player_1(),
	Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77::get_offset_of_AutoPlaybackType_2(),
	Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77::get_offset_of_DurationInSeconds_3(),
	Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77::get_offset_of_DrawMotionPath_4(),
	Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77::get_offset_of_MotionPathIconSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789 = { sizeof (Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3789[3] = 
{
	Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1::get_offset_of_Name_0(),
	Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1::get_offset_of_Tr_1(),
	Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1::get_offset_of_utm_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790 = { sizeof (U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3790[18] = 
{
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CU3E1__state_0(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CU3E2__current_1(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CU3E4__this_3(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CpcU3E5__2_4(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CtsU3E5__3_5(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3Clod0U3E5__4_6(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CbindPoseBoundsU3E5__5_7(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CbhU3E5__6_8(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CboundsIsCreatedU3E5__7_9(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CboundsU3E5__8_10(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CblendshapesCounterU3E5__9_11(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CiU3E5__10_12(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CtaskNameU3E5__11_13(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CU3E7__wrap11_14(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CclipU3E5__13_15(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CpcdataU3E5__14_16(),
	U3CImportIEU3Ed__37_t895AEC7C95EA33E148F5688524AB881236D75C9C::get_offset_of_U3CfU3E5__15_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792 = { sizeof (DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3792[7] = 
{
	DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0::get_offset_of_eff1_4(),
	DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0::get_offset_of_eff2_5(),
	DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0::get_offset_of_eff3_6(),
	DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0::get_offset_of_eff4_7(),
	DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0::get_offset_of_eff5_8(),
	DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0::get_offset_of_eff_name_9(),
	DemoController_t57DF2C408A6E73672A5C44B70C67B2E4E7F78CC0::get_offset_of_count_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793 = { sizeof (ArrowScript_tECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3793[4] = 
{
	ArrowScript_tECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2::get_offset_of_mesh_4(),
	ArrowScript_tECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2::get_offset_of_material_5(),
	ArrowScript_tECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2::get_offset_of_col_6(),
	ArrowScript_tECAD24A20AC5A5C10FD24DEF5F3FDDCF99AF69E2::get_offset_of_alpha_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794 = { sizeof (manfresnelScript_tCDA12259CDE2EBD210054BE15A17EB52A2723729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3794[3] = 
{
	manfresnelScript_tCDA12259CDE2EBD210054BE15A17EB52A2723729::get_offset_of_mesh_4(),
	manfresnelScript_tCDA12259CDE2EBD210054BE15A17EB52A2723729::get_offset_of_material_5(),
	manfresnelScript_tCDA12259CDE2EBD210054BE15A17EB52A2723729::get_offset_of_alpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795 = { sizeof (ManScript_tC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3795[4] = 
{
	ManScript_tC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB::get_offset_of_mesh_4(),
	ManScript_tC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB::get_offset_of_material_5(),
	ManScript_tC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB::get_offset_of_alpha_6(),
	ManScript_tC05F11105593A8C6405CDE3F4FEF2CED4CDAA7AB::get_offset_of_color_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796 = { sizeof (TestTime_t535287267B5B8B1DF2E7AE75DD49C77C5AE8D195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3796[2] = 
{
	TestTime_t535287267B5B8B1DF2E7AE75DD49C77C5AE8D195::get_offset_of_timetext_4(),
	TestTime_t535287267B5B8B1DF2E7AE75DD49C77C5AE8D195::get_offset_of_etime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797 = { sizeof (Count_t78AB461663390D23C5961F760F5F935C29C04BAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3797[2] = 
{
	Count_t78AB461663390D23C5961F760F5F935C29C04BAC::get_offset_of_countText_4(),
	Count_t78AB461663390D23C5961F760F5F935C29C04BAC::get_offset_of_boundcount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798 = { sizeof (fresnelScript_tB9629A7C001A3BABEE37CF42E8FCA19C41AA1732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3798[3] = 
{
	fresnelScript_tB9629A7C001A3BABEE37CF42E8FCA19C41AA1732::get_offset_of_mesh_4(),
	fresnelScript_tB9629A7C001A3BABEE37CF42E8FCA19C41AA1732::get_offset_of_material_5(),
	fresnelScript_tB9629A7C001A3BABEE37CF42E8FCA19C41AA1732::get_offset_of_alpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799 = { sizeof (ObjectClip_t569A88667400491D3CE315544091A25CA3CB08FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3799[4] = 
{
	ObjectClip_t569A88667400491D3CE315544091A25CA3CB08FF::get_offset_of_mesh_4(),
	ObjectClip_t569A88667400491D3CE315544091A25CA3CB08FF::get_offset_of_material_5(),
	ObjectClip_t569A88667400491D3CE315544091A25CA3CB08FF::get_offset_of_anitime_6(),
	ObjectClip_t569A88667400491D3CE315544091A25CA3CB08FF::get_offset_of_alpha_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
