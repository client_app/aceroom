﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct VirtActionInvoker6
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct VirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct VirtActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct GenericVirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericVirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericVirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct GenericVirtActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct GenericVirtActionInvoker6
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};
template <typename R>
struct GenericVirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericVirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericVirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct GenericVirtActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct InterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct InterfaceActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct InterfaceActionInvoker6
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct InterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct InterfaceActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct GenericInterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericInterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericInterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct GenericInterfaceActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct GenericInterfaceActionInvoker6
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};
template <typename R>
struct GenericInterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericInterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericInterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct GenericInterfaceActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};

// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action[]
struct ActionU5BU5D_t7BDDAD9C74678DD3A8A0D9C109E1D5AD65392DE3;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B;
// System.Collections.Generic.Dictionary`2<System.Single,TriLib.MorphChannelKey>
struct Dictionary_2_t27753A6CEA8485F892B2780AD0C8A72CCA018B0E;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25;
// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>
struct List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Globalization.Calendar
struct Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5;
// System.Globalization.CompareInfo
struct CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1;
// System.Globalization.CultureData
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8;
// System.Globalization.TextInfo
struct TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IFormatProvider
struct IFormatProvider_t4247E13AE2D97A079B88D594B7ABABF313259901;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.LocalDataStoreHolder
struct LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304;
// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9;
// System.MulticastDelegate
struct MulticastDelegate_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.InteropServices.GCHandle[]
struct GCHandleU5BU5D_tA6EC6308F1B33AD5233BD26DE6FB431B6CEF1DB7;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Principal.IPrincipal
struct IPrincipal_t63FD7F58FBBE134C8FE4D31710AAEA00B000F0BF;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo>
struct AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A;
// System.Threading.ExecutionContext
struct ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70;
// System.Threading.InternalThread
struct InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;
// System.Threading.Thread
struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7;
// System.Threading.ThreadStart
struct ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TriLib.AssimpInterop/DataCallback
struct DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF;
// TriLib.AssimpInterop/ExistsCallback
struct ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F;
// TriLib.AssimpInterop/ProgressCallback
struct ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6;
// TriLib.AssimpMetadata
struct AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD;
// TriLib.AvatarCreatedHandle
struct AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80;
// TriLib.BlendShapeKeyCreatedHandle
struct BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B;
// TriLib.CameraData
struct CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8;
// TriLib.DataDisposalCallback
struct DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB;
// TriLib.Dispatcher
struct Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7;
// TriLib.EmbeddedTextureData
struct EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915;
// TriLib.EmbeddedTextureLoadCallback
struct EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7;
// TriLib.FileLoadData
struct FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13;
// TriLib.GCFileLoadData
struct GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D;
// TriLib.LoadTextureDataCallback
struct LoadTextureDataCallback_tE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3;
// TriLib.MaterialCreatedHandle
struct MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78;
// TriLib.MaterialData
struct MaterialData_t27489063F272E033321F0C94EF062695C238CADA;
// TriLib.MeshCreatedHandle
struct MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3;
// TriLib.MeshData
struct MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537;
// TriLib.MetadataProcessedHandle
struct MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E;
// TriLib.MorphChannelData
struct MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4;
// TriLib.MorphChannelKey
struct MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73;
// TriLib.MorphData[]
struct MorphDataU5BU5D_tF75ABC4B00D9F56D8CF9E690DD05E653D090600E;
// TriLib.NodeData
struct NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C;
// TriLib.NodeData[]
struct NodeDataU5BU5D_t2903451E388B76A240D86BC410A23B2EF74CF920;
// TriLib.ObjectLoadedHandle
struct ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044;
// TriLib.TextureLoadHandle
struct TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D;
// TriLib.TexturePreLoadHandle
struct TexturePreLoadHandle_tC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23;
// TriLib.ThreadUtils/a
struct a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C;
// TriLib.ThreadUtils/b
struct b_tBB4B7EAE3F249876C28028467342545B6E691DD5;
// TriLib.ZipGCFileLoadData
struct ZipGCFileLoadData_tBAFBC6955F8E973FA87C0DDFD15968EC80491E53;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Avatar
struct Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B;
// UnityEngine.BoneWeight[]
struct BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;

extern RuntimeClass* Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var;
extern RuntimeClass* AssimpMetadataType_tE81F99F30662BCAC75055C486BB4F3F68E7D988B_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var;
extern RuntimeClass* ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var;
extern RuntimeClass* CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
extern RuntimeClass* Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83_il2cpp_TypeInfo_var;
extern RuntimeClass* MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* Path_t0B99A4B924A6FDF08814FFA8DD4CD121ED1A0752_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
extern RuntimeClass* Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var;
extern RuntimeClass* TextureCompression_t25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C_il2cpp_TypeInfo_var;
extern RuntimeClass* TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F_il2cpp_TypeInfo_var;
extern RuntimeClass* ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF_il2cpp_TypeInfo_var;
extern RuntimeClass* Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_il2cpp_TypeInfo_var;
extern RuntimeClass* Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var;
extern RuntimeClass* UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_il2cpp_TypeInfo_var;
extern RuntimeClass* a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C_il2cpp_TypeInfo_var;
extern RuntimeClass* b_tBB4B7EAE3F249876C28028467342545B6E691DD5_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral08534F33C201A45017B502E90A800F1B708EBCB3;
extern String_t* _stringLiteral0E82471DC8B063E6A5EBAD5261B8B0220682D513;
extern String_t* _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8;
extern String_t* _stringLiteral48F91366C839E056A4DCD440512F99E82188C46A;
extern const RuntimeMethod* Enumerator_Dispose_mEF3D4BA77CC66D385732EA73C5FBC441B4EADB2D_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_mF8DDDD6ADA4D6EE84C9D0827FEDB491318A8DF18_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m10833796C91ECE236B89832F9E92B26177E0C5EE_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisDispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_m133548093B1F19680188CF25587863E8A639B5F4_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m4CDF70E93E8FA24EE5A3711F44050893C037D388_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m006ABCBEDE7E5C2254B3D5393230AD09BE5D35B5_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m067BB9ECCE18366950BE75D550F8C62D6B61EFA7_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_Dequeue_mAB1E8E40E4211793A2E12D4592AC98240DD22161_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_Enqueue_m7694F6F1EE0F232575659FA4C6AB608B5250ECF5_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1__ctor_m0627B54FDADD1BC91409C5E9149CCF2CE4068554_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_get_Count_mF7EC2631EF60F0F94E4A081B0CA88DF05CD4B09E_RuntimeMethod_var;
extern const RuntimeMethod* a_c_m86F446BF97B3C113908FC32FF5D261637C66B5BB_RuntimeMethod_var;
extern const RuntimeMethod* b_b_mB1A0EA2F3DA09A7F4ACEF3D2D29904C00EED570D_RuntimeMethod_var;
extern const uint32_t DataCallback_BeginInvoke_m5098DBB791FCA3235E56ED21ECE83168F01832C1_MetadataUsageId;
extern const uint32_t DataDisposalCallback_BeginInvoke_m9AC8587753A08B623740AA0D5278C0225C46A6F2_MetadataUsageId;
extern const uint32_t Dispatcher_CheckInstance_mB6EDA7B5F82FCE4D44371434F75843BF03F4129E_MetadataUsageId;
extern const uint32_t Dispatcher_InvokeAsync_mD181ABE170BA0E1427FAA6252A08B75FD997473A_MetadataUsageId;
extern const uint32_t Dispatcher__cctor_m55952B725107EED38B56DA6FFF871B484400AF3D_MetadataUsageId;
extern const uint32_t Dispatcher_a_m88CB245848768AC761F98A3F5917C69D6BFBF7AF_MetadataUsageId;
extern const uint32_t Dispatcher_b_mC25478C947ACAB9F5C825EB3E2D4CAB1CAF824F0_MetadataUsageId;
extern const uint32_t Dispatcher_c_m8032A8347EB56C5DA6D320E39F334E8E77F5D379_MetadataUsageId;
extern const uint32_t EmbeddedTextureData_Dispose_m2B52F2B9FAFA0FD4A71BC2968FE303D94C90CF3C_MetadataUsageId;
extern const uint32_t ExistsCallback_BeginInvoke_mEA7304C5A8CCB3C5199F935092951D818B39C9F6_MetadataUsageId;
extern const uint32_t FileUtils_GetFileDirectory_m312D7CE2F87FDC9E00DB06C78DF142E881968B7F_MetadataUsageId;
extern const uint32_t FileUtils_GetFilename_m6FDFEEF0E70BFC5CCE4CB8750BA3C4445CCD851F_MetadataUsageId;
extern const uint32_t FileUtils_GetShortFilename_m07A9A02D60BADB44891F00FC1C70D4AE36E95382_MetadataUsageId;
extern const uint32_t FileUtils_LoadFileData_mD283027186E6E7806A9902027C2BB96506AA7572_MetadataUsageId;
extern const uint32_t GCFileLoadData_AddBuffer_m16D165F4DA2EEF481D1B9E72865688A5587B15EC_MetadataUsageId;
extern const uint32_t GCFileLoadData_Dispose_mF465C47F2E315F604EB588FCF4899263EF2D953C_MetadataUsageId;
extern const uint32_t GCFileLoadData__ctor_m5536FE0D9D5D2543DBA887D0EBCC6391307D3D45_MetadataUsageId;
extern const uint32_t MaterialCreatedHandle_BeginInvoke_m717C2033DE0682CCC0CB95417A0725AD4A46A2C1_MetadataUsageId;
extern const uint32_t MatrixExtensions_ExtractPosition_mA131E4F43AE4CB616B92459592831C2A244E8026_MetadataUsageId;
extern const uint32_t MatrixExtensions_ExtractRotation_m447F990451673541F36962C765EBBE40E0811AA1_MetadataUsageId;
extern const uint32_t MeshCreatedHandle_BeginInvoke_m76E416366E306E3AC1193C983B3DD59B7F4C679D_MetadataUsageId;
extern const uint32_t MetadataProcessedHandle_BeginInvoke_mCF31467123B3A6E9EF53626BF19CA38616C8C7C5_MetadataUsageId;
extern const uint32_t ProgressCallback_BeginInvoke_mFCC262B113759CE445869F09BF2EC939CB5A479C_MetadataUsageId;
extern const uint32_t StreamUtils_ReadFullStream_m947FCFB0AAEE7849112FCF323913537CE05AACC6_MetadataUsageId;
extern const uint32_t StringUtils_GenerateUniqueName_m99805772E1AFE9D8F4C32E48C693E157D1739935_MetadataUsageId;
extern const uint32_t Texture2DUtils_ProcessTexture_m49A522EEE0E46D58079DD614972076CCC5C9CF21_MetadataUsageId;
extern const uint32_t Texture2DUtils_a_m0E1656118EAA73C07AD9C6854F037E83131767B1_MetadataUsageId;
extern const uint32_t Texture2DUtils_a_mAFB4665E853FF838B8ED8836C67C43459EBC7DE0_MetadataUsageId;
extern const uint32_t TexturePreLoadHandle_BeginInvoke_mBC2C9551EA67D19142FD94A4CA512DAF1A753F94_MetadataUsageId;
extern const uint32_t ThreadUtils_RunThread_m4924878B8E72A7CA9C5C72E04A020452EB6C1629_MetadataUsageId;
extern const uint32_t TransformExtensions_FindDeepChild_mB87B6DDFE9FBD94736262E668DC9EE0CDFD7459D_MetadataUsageId;
extern const uint32_t a_c_m86F446BF97B3C113908FC32FF5D261637C66B5BB_MetadataUsageId;
extern const uint32_t b_b_mB1A0EA2F3DA09A7F4ACEF3D2D29904C00EED570D_MetadataUsageId;
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_com;
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_pinvoke;
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com;
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T897CFD5A76D916278D41769D4682EAFEFC02CE83_H
#define LIST_1_T897CFD5A76D916278D41769D4682EAFEFC02CE83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>
struct  List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GCHandleU5BU5D_tA6EC6308F1B33AD5233BD26DE6FB431B6CEF1DB7* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83, ____items_1)); }
	inline GCHandleU5BU5D_tA6EC6308F1B33AD5233BD26DE6FB431B6CEF1DB7* get__items_1() const { return ____items_1; }
	inline GCHandleU5BU5D_tA6EC6308F1B33AD5233BD26DE6FB431B6CEF1DB7** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GCHandleU5BU5D_tA6EC6308F1B33AD5233BD26DE6FB431B6CEF1DB7* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GCHandleU5BU5D_tA6EC6308F1B33AD5233BD26DE6FB431B6CEF1DB7* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83_StaticFields, ____emptyArray_5)); }
	inline GCHandleU5BU5D_tA6EC6308F1B33AD5233BD26DE6FB431B6CEF1DB7* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GCHandleU5BU5D_tA6EC6308F1B33AD5233BD26DE6FB431B6CEF1DB7** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GCHandleU5BU5D_tA6EC6308F1B33AD5233BD26DE6FB431B6CEF1DB7* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T897CFD5A76D916278D41769D4682EAFEFC02CE83_H
#ifndef QUEUE_1_TAAB5A24FE87B601E444ABA5CDF24F833AC5C651D_H
#define QUEUE_1_TAAB5A24FE87B601E444ABA5CDF24F833AC5C651D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<System.Action>
struct  Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	ActionU5BU5D_t7BDDAD9C74678DD3A8A0D9C109E1D5AD65392DE3* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;
	// System.Object System.Collections.Generic.Queue`1::_syncRoot
	RuntimeObject * ____syncRoot_5;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D, ____array_0)); }
	inline ActionU5BU5D_t7BDDAD9C74678DD3A8A0D9C109E1D5AD65392DE3* get__array_0() const { return ____array_0; }
	inline ActionU5BU5D_t7BDDAD9C74678DD3A8A0D9C109E1D5AD65392DE3** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ActionU5BU5D_t7BDDAD9C74678DD3A8A0D9C109E1D5AD65392DE3* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}

	inline static int32_t get_offset_of__syncRoot_5() { return static_cast<int32_t>(offsetof(Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D, ____syncRoot_5)); }
	inline RuntimeObject * get__syncRoot_5() const { return ____syncRoot_5; }
	inline RuntimeObject ** get_address_of__syncRoot_5() { return &____syncRoot_5; }
	inline void set__syncRoot_5(RuntimeObject * value)
	{
		____syncRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_1_TAAB5A24FE87B601E444ABA5CDF24F833AC5C651D_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef CULTUREINFO_T345AC6924134F039ED9A11F3E03F8E91B6A3225F_H
#define CULTUREINFO_T345AC6924134F039ED9A11F3E03F8E91B6A3225F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___numInfo_10)); }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_10), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_11), value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___textInfo_12)); }
	inline TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_12), value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_13), value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_14), value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_15), value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_16), value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_17), value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_18), value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((&___territory_19), value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___native_calendar_names_20)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((&___native_calendar_names_20), value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___compareInfo_21)); }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_21), value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___calendar_24)); }
	inline Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_24), value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___parent_culture_25)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_25), value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_27), value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_cultureData_28)); }
	inline CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_cultureData_28), value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_0), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_1), value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((&___default_current_culture_2), value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultThreadCurrentUICulture_33), value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultThreadCurrentCulture_34), value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_35), value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_36), value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	uint8_t* ___cached_serialized_form_27;
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	uint8_t* ___cached_serialized_form_27;
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
#endif // CULTUREINFO_T345AC6924134F039ED9A11F3E03F8E91B6A3225F_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#define CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct  CriticalFinalizerObject_t8B006E1DEE084E781F5C0F3283E9226E28894DD9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRITICALFINALIZEROBJECT_T8B006E1DEE084E781F5C0F3283E9226E28894DD9_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef FILELOADDATA_TE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13_H
#define FILELOADDATA_TE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.FileLoadData
struct  FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13  : public RuntimeObject
{
public:
	// System.String TriLib.FileLoadData::Filename
	String_t* ___Filename_0;
	// System.String TriLib.FileLoadData::BasePath
	String_t* ___BasePath_1;

public:
	inline static int32_t get_offset_of_Filename_0() { return static_cast<int32_t>(offsetof(FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13, ___Filename_0)); }
	inline String_t* get_Filename_0() const { return ___Filename_0; }
	inline String_t** get_address_of_Filename_0() { return &___Filename_0; }
	inline void set_Filename_0(String_t* value)
	{
		___Filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___Filename_0), value);
	}

	inline static int32_t get_offset_of_BasePath_1() { return static_cast<int32_t>(offsetof(FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13, ___BasePath_1)); }
	inline String_t* get_BasePath_1() const { return ___BasePath_1; }
	inline String_t** get_address_of_BasePath_1() { return &___BasePath_1; }
	inline void set_BasePath_1(String_t* value)
	{
		___BasePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___BasePath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILELOADDATA_TE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13_H
#ifndef FILEUTILS_TC978494CEBA8E2DE23341446DB88A4DE75E0E5A1_H
#define FILEUTILS_TC978494CEBA8E2DE23341446DB88A4DE75E0E5A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.FileUtils
struct  FileUtils_tC978494CEBA8E2DE23341446DB88A4DE75E0E5A1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEUTILS_TC978494CEBA8E2DE23341446DB88A4DE75E0E5A1_H
#ifndef MATRIXEXTENSIONS_T83E9B3470D05F4C6F10A66D1542EFD93E3502EEB_H
#define MATRIXEXTENSIONS_T83E9B3470D05F4C6F10A66D1542EFD93E3502EEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MatrixExtensions
struct  MatrixExtensions_t83E9B3470D05F4C6F10A66D1542EFD93E3502EEB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIXEXTENSIONS_T83E9B3470D05F4C6F10A66D1542EFD93E3502EEB_H
#ifndef MESHDATA_TA109105B40CB093BC17F6ED73E4D4BAF626AA537_H
#define MESHDATA_TA109105B40CB093BC17F6ED73E4D4BAF626AA537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MeshData
struct  MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537  : public RuntimeObject
{
public:
	// System.String TriLib.MeshData::Name
	String_t* ___Name_0;
	// System.String TriLib.MeshData::SubMeshName
	String_t* ___SubMeshName_1;
	// UnityEngine.Vector3[] TriLib.MeshData::Vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Vertices_2;
	// UnityEngine.Vector3[] TriLib.MeshData::Normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Normals_3;
	// UnityEngine.Vector4[] TriLib.MeshData::Tangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___Tangents_4;
	// UnityEngine.Vector4[] TriLib.MeshData::BiTangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___BiTangents_5;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv_6;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv1
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv1_7;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv2
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv2_8;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv3
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv3_9;
	// UnityEngine.Color[] TriLib.MeshData::Colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___Colors_10;
	// System.Int32[] TriLib.MeshData::Triangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Triangles_11;
	// System.Boolean TriLib.MeshData::HasBoneInfo
	bool ___HasBoneInfo_12;
	// UnityEngine.Matrix4x4[] TriLib.MeshData::BindPoses
	Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* ___BindPoses_13;
	// System.String[] TriLib.MeshData::BoneNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___BoneNames_14;
	// UnityEngine.BoneWeight[] TriLib.MeshData::BoneWeights
	BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* ___BoneWeights_15;
	// System.UInt32 TriLib.MeshData::MaterialIndex
	uint32_t ___MaterialIndex_16;
	// TriLib.MorphData[] TriLib.MeshData::MorphsData
	MorphDataU5BU5D_tF75ABC4B00D9F56D8CF9E690DD05E653D090600E* ___MorphsData_17;
	// UnityEngine.Mesh TriLib.MeshData::Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___Mesh_18;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_SubMeshName_1() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___SubMeshName_1)); }
	inline String_t* get_SubMeshName_1() const { return ___SubMeshName_1; }
	inline String_t** get_address_of_SubMeshName_1() { return &___SubMeshName_1; }
	inline void set_SubMeshName_1(String_t* value)
	{
		___SubMeshName_1 = value;
		Il2CppCodeGenWriteBarrier((&___SubMeshName_1), value);
	}

	inline static int32_t get_offset_of_Vertices_2() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Vertices_2)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Vertices_2() const { return ___Vertices_2; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Vertices_2() { return &___Vertices_2; }
	inline void set_Vertices_2(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Vertices_2 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_2), value);
	}

	inline static int32_t get_offset_of_Normals_3() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Normals_3)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Normals_3() const { return ___Normals_3; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Normals_3() { return &___Normals_3; }
	inline void set_Normals_3(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Normals_3 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_3), value);
	}

	inline static int32_t get_offset_of_Tangents_4() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Tangents_4)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_Tangents_4() const { return ___Tangents_4; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_Tangents_4() { return &___Tangents_4; }
	inline void set_Tangents_4(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___Tangents_4 = value;
		Il2CppCodeGenWriteBarrier((&___Tangents_4), value);
	}

	inline static int32_t get_offset_of_BiTangents_5() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___BiTangents_5)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_BiTangents_5() const { return ___BiTangents_5; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_BiTangents_5() { return &___BiTangents_5; }
	inline void set_BiTangents_5(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___BiTangents_5 = value;
		Il2CppCodeGenWriteBarrier((&___BiTangents_5), value);
	}

	inline static int32_t get_offset_of_Uv_6() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Uv_6)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv_6() const { return ___Uv_6; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv_6() { return &___Uv_6; }
	inline void set_Uv_6(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv_6 = value;
		Il2CppCodeGenWriteBarrier((&___Uv_6), value);
	}

	inline static int32_t get_offset_of_Uv1_7() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Uv1_7)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv1_7() const { return ___Uv1_7; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv1_7() { return &___Uv1_7; }
	inline void set_Uv1_7(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv1_7 = value;
		Il2CppCodeGenWriteBarrier((&___Uv1_7), value);
	}

	inline static int32_t get_offset_of_Uv2_8() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Uv2_8)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv2_8() const { return ___Uv2_8; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv2_8() { return &___Uv2_8; }
	inline void set_Uv2_8(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv2_8 = value;
		Il2CppCodeGenWriteBarrier((&___Uv2_8), value);
	}

	inline static int32_t get_offset_of_Uv3_9() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Uv3_9)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv3_9() const { return ___Uv3_9; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv3_9() { return &___Uv3_9; }
	inline void set_Uv3_9(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv3_9 = value;
		Il2CppCodeGenWriteBarrier((&___Uv3_9), value);
	}

	inline static int32_t get_offset_of_Colors_10() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Colors_10)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_Colors_10() const { return ___Colors_10; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_Colors_10() { return &___Colors_10; }
	inline void set_Colors_10(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___Colors_10 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_10), value);
	}

	inline static int32_t get_offset_of_Triangles_11() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Triangles_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Triangles_11() const { return ___Triangles_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Triangles_11() { return &___Triangles_11; }
	inline void set_Triangles_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Triangles_11 = value;
		Il2CppCodeGenWriteBarrier((&___Triangles_11), value);
	}

	inline static int32_t get_offset_of_HasBoneInfo_12() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___HasBoneInfo_12)); }
	inline bool get_HasBoneInfo_12() const { return ___HasBoneInfo_12; }
	inline bool* get_address_of_HasBoneInfo_12() { return &___HasBoneInfo_12; }
	inline void set_HasBoneInfo_12(bool value)
	{
		___HasBoneInfo_12 = value;
	}

	inline static int32_t get_offset_of_BindPoses_13() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___BindPoses_13)); }
	inline Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* get_BindPoses_13() const { return ___BindPoses_13; }
	inline Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9** get_address_of_BindPoses_13() { return &___BindPoses_13; }
	inline void set_BindPoses_13(Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* value)
	{
		___BindPoses_13 = value;
		Il2CppCodeGenWriteBarrier((&___BindPoses_13), value);
	}

	inline static int32_t get_offset_of_BoneNames_14() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___BoneNames_14)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_BoneNames_14() const { return ___BoneNames_14; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_BoneNames_14() { return &___BoneNames_14; }
	inline void set_BoneNames_14(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___BoneNames_14 = value;
		Il2CppCodeGenWriteBarrier((&___BoneNames_14), value);
	}

	inline static int32_t get_offset_of_BoneWeights_15() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___BoneWeights_15)); }
	inline BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* get_BoneWeights_15() const { return ___BoneWeights_15; }
	inline BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D** get_address_of_BoneWeights_15() { return &___BoneWeights_15; }
	inline void set_BoneWeights_15(BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* value)
	{
		___BoneWeights_15 = value;
		Il2CppCodeGenWriteBarrier((&___BoneWeights_15), value);
	}

	inline static int32_t get_offset_of_MaterialIndex_16() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___MaterialIndex_16)); }
	inline uint32_t get_MaterialIndex_16() const { return ___MaterialIndex_16; }
	inline uint32_t* get_address_of_MaterialIndex_16() { return &___MaterialIndex_16; }
	inline void set_MaterialIndex_16(uint32_t value)
	{
		___MaterialIndex_16 = value;
	}

	inline static int32_t get_offset_of_MorphsData_17() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___MorphsData_17)); }
	inline MorphDataU5BU5D_tF75ABC4B00D9F56D8CF9E690DD05E653D090600E* get_MorphsData_17() const { return ___MorphsData_17; }
	inline MorphDataU5BU5D_tF75ABC4B00D9F56D8CF9E690DD05E653D090600E** get_address_of_MorphsData_17() { return &___MorphsData_17; }
	inline void set_MorphsData_17(MorphDataU5BU5D_tF75ABC4B00D9F56D8CF9E690DD05E653D090600E* value)
	{
		___MorphsData_17 = value;
		Il2CppCodeGenWriteBarrier((&___MorphsData_17), value);
	}

	inline static int32_t get_offset_of_Mesh_18() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Mesh_18)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_Mesh_18() const { return ___Mesh_18; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_Mesh_18() { return &___Mesh_18; }
	inline void set_Mesh_18(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___Mesh_18 = value;
		Il2CppCodeGenWriteBarrier((&___Mesh_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_TA109105B40CB093BC17F6ED73E4D4BAF626AA537_H
#ifndef MORPHCHANNELDATA_T6629EC80A43769190D8ED48A56093C6DD37C25E4_H
#define MORPHCHANNELDATA_T6629EC80A43769190D8ED48A56093C6DD37C25E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MorphChannelData
struct  MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4  : public RuntimeObject
{
public:
	// System.String TriLib.MorphChannelData::NodeName
	String_t* ___NodeName_0;
	// System.Collections.Generic.Dictionary`2<System.Single,TriLib.MorphChannelKey> TriLib.MorphChannelData::MorphChannelKeys
	Dictionary_2_t27753A6CEA8485F892B2780AD0C8A72CCA018B0E * ___MorphChannelKeys_1;

public:
	inline static int32_t get_offset_of_NodeName_0() { return static_cast<int32_t>(offsetof(MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4, ___NodeName_0)); }
	inline String_t* get_NodeName_0() const { return ___NodeName_0; }
	inline String_t** get_address_of_NodeName_0() { return &___NodeName_0; }
	inline void set_NodeName_0(String_t* value)
	{
		___NodeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___NodeName_0), value);
	}

	inline static int32_t get_offset_of_MorphChannelKeys_1() { return static_cast<int32_t>(offsetof(MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4, ___MorphChannelKeys_1)); }
	inline Dictionary_2_t27753A6CEA8485F892B2780AD0C8A72CCA018B0E * get_MorphChannelKeys_1() const { return ___MorphChannelKeys_1; }
	inline Dictionary_2_t27753A6CEA8485F892B2780AD0C8A72CCA018B0E ** get_address_of_MorphChannelKeys_1() { return &___MorphChannelKeys_1; }
	inline void set_MorphChannelKeys_1(Dictionary_2_t27753A6CEA8485F892B2780AD0C8A72CCA018B0E * value)
	{
		___MorphChannelKeys_1 = value;
		Il2CppCodeGenWriteBarrier((&___MorphChannelKeys_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MORPHCHANNELDATA_T6629EC80A43769190D8ED48A56093C6DD37C25E4_H
#ifndef MORPHCHANNELKEY_TED235489B14590843F8C37E1381749ADCC625E73_H
#define MORPHCHANNELKEY_TED235489B14590843F8C37E1381749ADCC625E73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MorphChannelKey
struct  MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73  : public RuntimeObject
{
public:
	// System.UInt32[] TriLib.MorphChannelKey::Indices
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Indices_0;
	// System.Single[] TriLib.MorphChannelKey::Weights
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___Weights_1;

public:
	inline static int32_t get_offset_of_Indices_0() { return static_cast<int32_t>(offsetof(MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73, ___Indices_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Indices_0() const { return ___Indices_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Indices_0() { return &___Indices_0; }
	inline void set_Indices_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Indices_0 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_0), value);
	}

	inline static int32_t get_offset_of_Weights_1() { return static_cast<int32_t>(offsetof(MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73, ___Weights_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_Weights_1() const { return ___Weights_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_Weights_1() { return &___Weights_1; }
	inline void set_Weights_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___Weights_1 = value;
		Il2CppCodeGenWriteBarrier((&___Weights_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MORPHCHANNELKEY_TED235489B14590843F8C37E1381749ADCC625E73_H
#ifndef MORPHDATA_TE312C946C7103853AF92364F5B0DF1DD06A4AC8B_H
#define MORPHDATA_TE312C946C7103853AF92364F5B0DF1DD06A4AC8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MorphData
struct  MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B  : public RuntimeObject
{
public:
	// System.String TriLib.MorphData::Name
	String_t* ___Name_0;
	// UnityEngine.Vector3[] TriLib.MorphData::Vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Vertices_1;
	// UnityEngine.Vector3[] TriLib.MorphData::Normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Normals_2;
	// UnityEngine.Vector3[] TriLib.MorphData::Tangents
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Tangents_3;
	// System.Single TriLib.MorphData::Weight
	float ___Weight_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Vertices_1() { return static_cast<int32_t>(offsetof(MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B, ___Vertices_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Vertices_1() const { return ___Vertices_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Vertices_1() { return &___Vertices_1; }
	inline void set_Vertices_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_1), value);
	}

	inline static int32_t get_offset_of_Normals_2() { return static_cast<int32_t>(offsetof(MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B, ___Normals_2)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Normals_2() const { return ___Normals_2; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Normals_2() { return &___Normals_2; }
	inline void set_Normals_2(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Normals_2 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_2), value);
	}

	inline static int32_t get_offset_of_Tangents_3() { return static_cast<int32_t>(offsetof(MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B, ___Tangents_3)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Tangents_3() const { return ___Tangents_3; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Tangents_3() { return &___Tangents_3; }
	inline void set_Tangents_3(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Tangents_3 = value;
		Il2CppCodeGenWriteBarrier((&___Tangents_3), value);
	}

	inline static int32_t get_offset_of_Weight_4() { return static_cast<int32_t>(offsetof(MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B, ___Weight_4)); }
	inline float get_Weight_4() const { return ___Weight_4; }
	inline float* get_address_of_Weight_4() { return &___Weight_4; }
	inline void set_Weight_4(float value)
	{
		___Weight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MORPHDATA_TE312C946C7103853AF92364F5B0DF1DD06A4AC8B_H
#ifndef STREAMUTILS_T6D2DC8042ED0F5EF8F5F4A9425CF18E945007FDC_H
#define STREAMUTILS_T6D2DC8042ED0F5EF8F5F4A9425CF18E945007FDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.StreamUtils
struct  StreamUtils_t6D2DC8042ED0F5EF8F5F4A9425CF18E945007FDC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMUTILS_T6D2DC8042ED0F5EF8F5F4A9425CF18E945007FDC_H
#ifndef STRINGUTILS_T138F1BEFB530B5E8D03690A7C3790BD6BD1905AC_H
#define STRINGUTILS_T138F1BEFB530B5E8D03690A7C3790BD6BD1905AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.StringUtils
struct  StringUtils_t138F1BEFB530B5E8D03690A7C3790BD6BD1905AC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T138F1BEFB530B5E8D03690A7C3790BD6BD1905AC_H
#ifndef TEXTURE2DUTILS_T8347444C9FA2D57E80F3EB8ADDF7927DB25CAB6C_H
#define TEXTURE2DUTILS_T8347444C9FA2D57E80F3EB8ADDF7927DB25CAB6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Texture2DUtils
struct  Texture2DUtils_t8347444C9FA2D57E80F3EB8ADDF7927DB25CAB6C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2DUTILS_T8347444C9FA2D57E80F3EB8ADDF7927DB25CAB6C_H
#ifndef THREADUTILS_TB30F54991AC35C1AF20C6917FB1653127F7109FE_H
#define THREADUTILS_TB30F54991AC35C1AF20C6917FB1653127F7109FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils
struct  ThreadUtils_tB30F54991AC35C1AF20C6917FB1653127F7109FE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADUTILS_TB30F54991AC35C1AF20C6917FB1653127F7109FE_H
#ifndef A_T9752C01F959BCB0184461E930AD1BD0C6400DA0C_H
#define A_T9752C01F959BCB0184461E930AD1BD0C6400DA0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils_a
struct  a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C  : public RuntimeObject
{
public:
	// System.Action TriLib.ThreadUtils_a::a
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___a_0;
	// System.Action TriLib.ThreadUtils_a::b
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C, ___a_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_a_0() const { return ___a_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C, ___b_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_b_1() const { return ___b_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T9752C01F959BCB0184461E930AD1BD0C6400DA0C_H
#ifndef B_TBB4B7EAE3F249876C28028467342545B6E691DD5_H
#define B_TBB4B7EAE3F249876C28028467342545B6E691DD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils_b
struct  b_tBB4B7EAE3F249876C28028467342545B6E691DD5  : public RuntimeObject
{
public:
	// System.Exception TriLib.ThreadUtils_b::a
	Exception_t * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(b_tBB4B7EAE3F249876C28028467342545B6E691DD5, ___a_0)); }
	inline Exception_t * get_a_0() const { return ___a_0; }
	inline Exception_t ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Exception_t * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // B_TBB4B7EAE3F249876C28028467342545B6E691DD5_H
#ifndef TRANSFORMEXTENSIONS_T0CA99D8D9FEAB9B39CA87EA9816DD70313658C42_H
#define TRANSFORMEXTENSIONS_T0CA99D8D9FEAB9B39CA87EA9816DD70313658C42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TransformExtensions
struct  TransformExtensions_t0CA99D8D9FEAB9B39CA87EA9816DD70313658C42  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSIONS_T0CA99D8D9FEAB9B39CA87EA9816DD70313658C42_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#define BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_TF87C579059BD4633E6840EBBBEEF899C6E33EF07_H
#ifndef CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#define CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_TBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#define GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef THREAD_TF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_H
#define THREAD_TF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Thread
struct  Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7  : public CriticalFinalizerObject_t8B006E1DEE084E781F5C0F3283E9226E28894DD9
{
public:
	// System.Threading.InternalThread System.Threading.Thread::internal_thread
	InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 * ___internal_thread_6;
	// System.Object System.Threading.Thread::m_ThreadStartArg
	RuntimeObject * ___m_ThreadStartArg_7;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject * ___pending_exception_8;
	// System.Security.Principal.IPrincipal System.Threading.Thread::principal
	RuntimeObject* ___principal_9;
	// System.Int32 System.Threading.Thread::principal_version
	int32_t ___principal_version_10;
	// System.MulticastDelegate System.Threading.Thread::m_Delegate
	MulticastDelegate_t * ___m_Delegate_12;
	// System.Threading.ExecutionContext System.Threading.Thread::m_ExecutionContext
	ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * ___m_ExecutionContext_13;
	// System.Boolean System.Threading.Thread::m_ExecutionContextBelongsToOuterScope
	bool ___m_ExecutionContextBelongsToOuterScope_14;

public:
	inline static int32_t get_offset_of_internal_thread_6() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___internal_thread_6)); }
	inline InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 * get_internal_thread_6() const { return ___internal_thread_6; }
	inline InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 ** get_address_of_internal_thread_6() { return &___internal_thread_6; }
	inline void set_internal_thread_6(InternalThread_tA4C58C2A7D15AF43C3E7507375E6D31DBBE7D192 * value)
	{
		___internal_thread_6 = value;
		Il2CppCodeGenWriteBarrier((&___internal_thread_6), value);
	}

	inline static int32_t get_offset_of_m_ThreadStartArg_7() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_ThreadStartArg_7)); }
	inline RuntimeObject * get_m_ThreadStartArg_7() const { return ___m_ThreadStartArg_7; }
	inline RuntimeObject ** get_address_of_m_ThreadStartArg_7() { return &___m_ThreadStartArg_7; }
	inline void set_m_ThreadStartArg_7(RuntimeObject * value)
	{
		___m_ThreadStartArg_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ThreadStartArg_7), value);
	}

	inline static int32_t get_offset_of_pending_exception_8() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___pending_exception_8)); }
	inline RuntimeObject * get_pending_exception_8() const { return ___pending_exception_8; }
	inline RuntimeObject ** get_address_of_pending_exception_8() { return &___pending_exception_8; }
	inline void set_pending_exception_8(RuntimeObject * value)
	{
		___pending_exception_8 = value;
		Il2CppCodeGenWriteBarrier((&___pending_exception_8), value);
	}

	inline static int32_t get_offset_of_principal_9() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___principal_9)); }
	inline RuntimeObject* get_principal_9() const { return ___principal_9; }
	inline RuntimeObject** get_address_of_principal_9() { return &___principal_9; }
	inline void set_principal_9(RuntimeObject* value)
	{
		___principal_9 = value;
		Il2CppCodeGenWriteBarrier((&___principal_9), value);
	}

	inline static int32_t get_offset_of_principal_version_10() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___principal_version_10)); }
	inline int32_t get_principal_version_10() const { return ___principal_version_10; }
	inline int32_t* get_address_of_principal_version_10() { return &___principal_version_10; }
	inline void set_principal_version_10(int32_t value)
	{
		___principal_version_10 = value;
	}

	inline static int32_t get_offset_of_m_Delegate_12() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_Delegate_12)); }
	inline MulticastDelegate_t * get_m_Delegate_12() const { return ___m_Delegate_12; }
	inline MulticastDelegate_t ** get_address_of_m_Delegate_12() { return &___m_Delegate_12; }
	inline void set_m_Delegate_12(MulticastDelegate_t * value)
	{
		___m_Delegate_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Delegate_12), value);
	}

	inline static int32_t get_offset_of_m_ExecutionContext_13() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_ExecutionContext_13)); }
	inline ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * get_m_ExecutionContext_13() const { return ___m_ExecutionContext_13; }
	inline ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 ** get_address_of_m_ExecutionContext_13() { return &___m_ExecutionContext_13; }
	inline void set_m_ExecutionContext_13(ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * value)
	{
		___m_ExecutionContext_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExecutionContext_13), value);
	}

	inline static int32_t get_offset_of_m_ExecutionContextBelongsToOuterScope_14() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7, ___m_ExecutionContextBelongsToOuterScope_14)); }
	inline bool get_m_ExecutionContextBelongsToOuterScope_14() const { return ___m_ExecutionContextBelongsToOuterScope_14; }
	inline bool* get_address_of_m_ExecutionContextBelongsToOuterScope_14() { return &___m_ExecutionContextBelongsToOuterScope_14; }
	inline void set_m_ExecutionContextBelongsToOuterScope_14(bool value)
	{
		___m_ExecutionContextBelongsToOuterScope_14 = value;
	}
};

struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields
{
public:
	// System.LocalDataStoreMgr System.Threading.Thread::s_LocalDataStoreMgr
	LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 * ___s_LocalDataStoreMgr_0;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentCulture
	AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * ___s_asyncLocalCurrentCulture_4;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentUICulture
	AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * ___s_asyncLocalCurrentUICulture_5;

public:
	inline static int32_t get_offset_of_s_LocalDataStoreMgr_0() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields, ___s_LocalDataStoreMgr_0)); }
	inline LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 * get_s_LocalDataStoreMgr_0() const { return ___s_LocalDataStoreMgr_0; }
	inline LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 ** get_address_of_s_LocalDataStoreMgr_0() { return &___s_LocalDataStoreMgr_0; }
	inline void set_s_LocalDataStoreMgr_0(LocalDataStoreMgr_t1964DDB9F2BE154BE3159A7507D0D0CCBF8FDCA9 * value)
	{
		___s_LocalDataStoreMgr_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalDataStoreMgr_0), value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentCulture_4() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields, ___s_asyncLocalCurrentCulture_4)); }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * get_s_asyncLocalCurrentCulture_4() const { return ___s_asyncLocalCurrentCulture_4; }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A ** get_address_of_s_asyncLocalCurrentCulture_4() { return &___s_asyncLocalCurrentCulture_4; }
	inline void set_s_asyncLocalCurrentCulture_4(AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * value)
	{
		___s_asyncLocalCurrentCulture_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_asyncLocalCurrentCulture_4), value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentUICulture_5() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_StaticFields, ___s_asyncLocalCurrentUICulture_5)); }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * get_s_asyncLocalCurrentUICulture_5() const { return ___s_asyncLocalCurrentUICulture_5; }
	inline AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A ** get_address_of_s_asyncLocalCurrentUICulture_5() { return &___s_asyncLocalCurrentUICulture_5; }
	inline void set_s_asyncLocalCurrentUICulture_5(AsyncLocal_1_tD39651C2EDD14B144FF3D9B9C716F807EB57655A * value)
	{
		___s_asyncLocalCurrentUICulture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_asyncLocalCurrentUICulture_5), value);
	}
};

struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields
{
public:
	// System.LocalDataStoreHolder System.Threading.Thread::s_LocalDataStore
	LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 * ___s_LocalDataStore_1;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___m_CurrentCulture_2;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentUICulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___m_CurrentUICulture_3;
	// System.Threading.Thread System.Threading.Thread::current_thread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___current_thread_11;

public:
	inline static int32_t get_offset_of_s_LocalDataStore_1() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___s_LocalDataStore_1)); }
	inline LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 * get_s_LocalDataStore_1() const { return ___s_LocalDataStore_1; }
	inline LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 ** get_address_of_s_LocalDataStore_1() { return &___s_LocalDataStore_1; }
	inline void set_s_LocalDataStore_1(LocalDataStoreHolder_tE0636E08496405406FD63190AC51EEB2EE51E304 * value)
	{
		___s_LocalDataStore_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalDataStore_1), value);
	}

	inline static int32_t get_offset_of_m_CurrentCulture_2() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___m_CurrentCulture_2)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_m_CurrentCulture_2() const { return ___m_CurrentCulture_2; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_m_CurrentCulture_2() { return &___m_CurrentCulture_2; }
	inline void set_m_CurrentCulture_2(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___m_CurrentCulture_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentCulture_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentUICulture_3() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___m_CurrentUICulture_3)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_m_CurrentUICulture_3() const { return ___m_CurrentUICulture_3; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_m_CurrentUICulture_3() { return &___m_CurrentUICulture_3; }
	inline void set_m_CurrentUICulture_3(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___m_CurrentUICulture_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentUICulture_3), value);
	}

	inline static int32_t get_offset_of_current_thread_11() { return static_cast<int32_t>(offsetof(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_ThreadStaticFields, ___current_thread_11)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_current_thread_11() const { return ___current_thread_11; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_current_thread_11() { return &___current_thread_11; }
	inline void set_current_thread_11(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___current_thread_11 = value;
		Il2CppCodeGenWriteBarrier((&___current_thread_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREAD_TF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_H
#ifndef UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#define UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef GCFILELOADDATA_T7A72633928546FD4AC5348CE9EDDE47AA7C7F62D_H
#define GCFILELOADDATA_T7A72633928546FD4AC5348CE9EDDE47AA7C7F62D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.GCFileLoadData
struct  GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D  : public FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13
{
public:
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle> TriLib.GCFileLoadData::LockedBuffers
	List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * ___LockedBuffers_2;

public:
	inline static int32_t get_offset_of_LockedBuffers_2() { return static_cast<int32_t>(offsetof(GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D, ___LockedBuffers_2)); }
	inline List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * get_LockedBuffers_2() const { return ___LockedBuffers_2; }
	inline List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 ** get_address_of_LockedBuffers_2() { return &___LockedBuffers_2; }
	inline void set_LockedBuffers_2(List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * value)
	{
		___LockedBuffers_2 = value;
		Il2CppCodeGenWriteBarrier((&___LockedBuffers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCFILELOADDATA_T7A72633928546FD4AC5348CE9EDDE47AA7C7F62D_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef A_TF08E05BEBCCB7007505B2FDCCAC98CD9A986586E_H
#define A_TF08E05BEBCCB7007505B2FDCCAC98CD9A986586E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// a_a
struct  a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_TF08E05BEBCCB7007505B2FDCCAC98CD9A986586E_H
#ifndef ENUMERATOR_T21B963B3FB6624FE95B0B4B4DF36F585E45FB16F_H
#define ENUMERATOR_T21B963B3FB6624FE95B0B4B4DF36F585E45FB16F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<System.Runtime.InteropServices.GCHandle>
struct  Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F, ___list_0)); }
	inline List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * get_list_0() const { return ___list_0; }
	inline List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F, ___current_3)); }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  get_current_3() const { return ___current_3; }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T21B963B3FB6624FE95B0B4B4DF36F585E45FB16F_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#define MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_4;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_5;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_6;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_7;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_8;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_9;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_10;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_11;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_12;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ____lastReadTask_13;

public:
	inline static int32_t get_offset_of__buffer_4() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____buffer_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_4() const { return ____buffer_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_4() { return &____buffer_4; }
	inline void set__buffer_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_4 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_4), value);
	}

	inline static int32_t get_offset_of__origin_5() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____origin_5)); }
	inline int32_t get__origin_5() const { return ____origin_5; }
	inline int32_t* get_address_of__origin_5() { return &____origin_5; }
	inline void set__origin_5(int32_t value)
	{
		____origin_5 = value;
	}

	inline static int32_t get_offset_of__position_6() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____position_6)); }
	inline int32_t get__position_6() const { return ____position_6; }
	inline int32_t* get_address_of__position_6() { return &____position_6; }
	inline void set__position_6(int32_t value)
	{
		____position_6 = value;
	}

	inline static int32_t get_offset_of__length_7() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____length_7)); }
	inline int32_t get__length_7() const { return ____length_7; }
	inline int32_t* get_address_of__length_7() { return &____length_7; }
	inline void set__length_7(int32_t value)
	{
		____length_7 = value;
	}

	inline static int32_t get_offset_of__capacity_8() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____capacity_8)); }
	inline int32_t get__capacity_8() const { return ____capacity_8; }
	inline int32_t* get_address_of__capacity_8() { return &____capacity_8; }
	inline void set__capacity_8(int32_t value)
	{
		____capacity_8 = value;
	}

	inline static int32_t get_offset_of__expandable_9() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____expandable_9)); }
	inline bool get__expandable_9() const { return ____expandable_9; }
	inline bool* get_address_of__expandable_9() { return &____expandable_9; }
	inline void set__expandable_9(bool value)
	{
		____expandable_9 = value;
	}

	inline static int32_t get_offset_of__writable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____writable_10)); }
	inline bool get__writable_10() const { return ____writable_10; }
	inline bool* get_address_of__writable_10() { return &____writable_10; }
	inline void set__writable_10(bool value)
	{
		____writable_10 = value;
	}

	inline static int32_t get_offset_of__exposable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____exposable_11)); }
	inline bool get__exposable_11() const { return ____exposable_11; }
	inline bool* get_address_of__exposable_11() { return &____exposable_11; }
	inline void set__exposable_11(bool value)
	{
		____exposable_11 = value;
	}

	inline static int32_t get_offset_of__isOpen_12() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____isOpen_12)); }
	inline bool get__isOpen_12() const { return ____isOpen_12; }
	inline bool* get_address_of__isOpen_12() { return &____isOpen_12; }
	inline void set__isOpen_12(bool value)
	{
		____isOpen_12 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_13() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____lastReadTask_13)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get__lastReadTask_13() const { return ____lastReadTask_13; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of__lastReadTask_13() { return &____lastReadTask_13; }
	inline void set__lastReadTask_13(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		____lastReadTask_13 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifndef ASSIMPMETADATATYPE_TE81F99F30662BCAC75055C486BB4F3F68E7D988B_H
#define ASSIMPMETADATATYPE_TE81F99F30662BCAC75055C486BB4F3F68E7D988B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpMetadataType
struct  AssimpMetadataType_tE81F99F30662BCAC75055C486BB4F3F68E7D988B 
{
public:
	// System.Int32 TriLib.AssimpMetadataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssimpMetadataType_tE81F99F30662BCAC75055C486BB4F3F68E7D988B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPMETADATATYPE_TE81F99F30662BCAC75055C486BB4F3F68E7D988B_H
#ifndef ASSIMPPOSTPROCESSSTEPS_T89C7A137C5A7EEA789392222AFF34DCD11F471AD_H
#define ASSIMPPOSTPROCESSSTEPS_T89C7A137C5A7EEA789392222AFF34DCD11F471AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpPostProcessSteps
struct  AssimpPostProcessSteps_t89C7A137C5A7EEA789392222AFF34DCD11F471AD 
{
public:
	// System.Int32 TriLib.AssimpPostProcessSteps::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssimpPostProcessSteps_t89C7A137C5A7EEA789392222AFF34DCD11F471AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPPOSTPROCESSSTEPS_T89C7A137C5A7EEA789392222AFF34DCD11F471AD_H
#ifndef CAMERADATA_TC06135C8B99D7A922FF22C8E9E138EF9CA426FF8_H
#define CAMERADATA_TC06135C8B99D7A922FF22C8E9E138EF9CA426FF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.CameraData
struct  CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8  : public RuntimeObject
{
public:
	// System.String TriLib.CameraData::Name
	String_t* ___Name_0;
	// System.Single TriLib.CameraData::Aspect
	float ___Aspect_1;
	// System.Single TriLib.CameraData::NearClipPlane
	float ___NearClipPlane_2;
	// System.Single TriLib.CameraData::FarClipPlane
	float ___FarClipPlane_3;
	// System.Single TriLib.CameraData::FieldOfView
	float ___FieldOfView_4;
	// UnityEngine.Vector3 TriLib.CameraData::LocalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___LocalPosition_5;
	// UnityEngine.Vector3 TriLib.CameraData::Forward
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Forward_6;
	// UnityEngine.Vector3 TriLib.CameraData::Up
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Up_7;
	// UnityEngine.Camera TriLib.CameraData::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_8;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Aspect_1() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___Aspect_1)); }
	inline float get_Aspect_1() const { return ___Aspect_1; }
	inline float* get_address_of_Aspect_1() { return &___Aspect_1; }
	inline void set_Aspect_1(float value)
	{
		___Aspect_1 = value;
	}

	inline static int32_t get_offset_of_NearClipPlane_2() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___NearClipPlane_2)); }
	inline float get_NearClipPlane_2() const { return ___NearClipPlane_2; }
	inline float* get_address_of_NearClipPlane_2() { return &___NearClipPlane_2; }
	inline void set_NearClipPlane_2(float value)
	{
		___NearClipPlane_2 = value;
	}

	inline static int32_t get_offset_of_FarClipPlane_3() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___FarClipPlane_3)); }
	inline float get_FarClipPlane_3() const { return ___FarClipPlane_3; }
	inline float* get_address_of_FarClipPlane_3() { return &___FarClipPlane_3; }
	inline void set_FarClipPlane_3(float value)
	{
		___FarClipPlane_3 = value;
	}

	inline static int32_t get_offset_of_FieldOfView_4() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___FieldOfView_4)); }
	inline float get_FieldOfView_4() const { return ___FieldOfView_4; }
	inline float* get_address_of_FieldOfView_4() { return &___FieldOfView_4; }
	inline void set_FieldOfView_4(float value)
	{
		___FieldOfView_4 = value;
	}

	inline static int32_t get_offset_of_LocalPosition_5() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___LocalPosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_LocalPosition_5() const { return ___LocalPosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_LocalPosition_5() { return &___LocalPosition_5; }
	inline void set_LocalPosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___LocalPosition_5 = value;
	}

	inline static int32_t get_offset_of_Forward_6() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___Forward_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Forward_6() const { return ___Forward_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Forward_6() { return &___Forward_6; }
	inline void set_Forward_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Forward_6 = value;
	}

	inline static int32_t get_offset_of_Up_7() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___Up_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Up_7() const { return ___Up_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Up_7() { return &___Up_7; }
	inline void set_Up_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Up_7 = value;
	}

	inline static int32_t get_offset_of_Camera_8() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___Camera_8)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_8() const { return ___Camera_8; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_8() { return &___Camera_8; }
	inline void set_Camera_8(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_8 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADATA_TC06135C8B99D7A922FF22C8E9E138EF9CA426FF8_H
#ifndef EMBEDDEDTEXTUREDATA_T256A14D2298773D48CE5F0225DCABD4ABD703915_H
#define EMBEDDEDTEXTUREDATA_T256A14D2298773D48CE5F0225DCABD4ABD703915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.EmbeddedTextureData
struct  EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915  : public RuntimeObject
{
public:
	// System.Byte[] TriLib.EmbeddedTextureData::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_0;
	// System.IntPtr TriLib.EmbeddedTextureData::DataPointer
	intptr_t ___DataPointer_1;
	// System.Int32 TriLib.EmbeddedTextureData::DataLength
	int32_t ___DataLength_2;
	// TriLib.DataDisposalCallback TriLib.EmbeddedTextureData::OnDataDisposal
	DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * ___OnDataDisposal_3;
	// System.Int32 TriLib.EmbeddedTextureData::Width
	int32_t ___Width_4;
	// System.Int32 TriLib.EmbeddedTextureData::Height
	int32_t ___Height_5;
	// System.Int32 TriLib.EmbeddedTextureData::NumChannels
	int32_t ___NumChannels_6;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___Data_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_0() const { return ___Data_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((&___Data_0), value);
	}

	inline static int32_t get_offset_of_DataPointer_1() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___DataPointer_1)); }
	inline intptr_t get_DataPointer_1() const { return ___DataPointer_1; }
	inline intptr_t* get_address_of_DataPointer_1() { return &___DataPointer_1; }
	inline void set_DataPointer_1(intptr_t value)
	{
		___DataPointer_1 = value;
	}

	inline static int32_t get_offset_of_DataLength_2() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___DataLength_2)); }
	inline int32_t get_DataLength_2() const { return ___DataLength_2; }
	inline int32_t* get_address_of_DataLength_2() { return &___DataLength_2; }
	inline void set_DataLength_2(int32_t value)
	{
		___DataLength_2 = value;
	}

	inline static int32_t get_offset_of_OnDataDisposal_3() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___OnDataDisposal_3)); }
	inline DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * get_OnDataDisposal_3() const { return ___OnDataDisposal_3; }
	inline DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB ** get_address_of_OnDataDisposal_3() { return &___OnDataDisposal_3; }
	inline void set_OnDataDisposal_3(DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * value)
	{
		___OnDataDisposal_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnDataDisposal_3), value);
	}

	inline static int32_t get_offset_of_Width_4() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___Width_4)); }
	inline int32_t get_Width_4() const { return ___Width_4; }
	inline int32_t* get_address_of_Width_4() { return &___Width_4; }
	inline void set_Width_4(int32_t value)
	{
		___Width_4 = value;
	}

	inline static int32_t get_offset_of_Height_5() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___Height_5)); }
	inline int32_t get_Height_5() const { return ___Height_5; }
	inline int32_t* get_address_of_Height_5() { return &___Height_5; }
	inline void set_Height_5(int32_t value)
	{
		___Height_5 = value;
	}

	inline static int32_t get_offset_of_NumChannels_6() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___NumChannels_6)); }
	inline int32_t get_NumChannels_6() const { return ___NumChannels_6; }
	inline int32_t* get_address_of_NumChannels_6() { return &___NumChannels_6; }
	inline void set_NumChannels_6(int32_t value)
	{
		___NumChannels_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMBEDDEDTEXTUREDATA_T256A14D2298773D48CE5F0225DCABD4ABD703915_H
#ifndef NODEDATA_TD2933CCEB90B2F77D752DB697E43E8C616DAC93C_H
#define NODEDATA_TD2933CCEB90B2F77D752DB697E43E8C616DAC93C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.NodeData
struct  NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C  : public RuntimeObject
{
public:
	// System.String TriLib.NodeData::Name
	String_t* ___Name_0;
	// System.String TriLib.NodeData::Path
	String_t* ___Path_1;
	// UnityEngine.Matrix4x4 TriLib.NodeData::Matrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___Matrix_2;
	// System.UInt32[] TriLib.NodeData::Meshes
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Meshes_3;
	// TriLib.NodeData TriLib.NodeData::Parent
	NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C * ___Parent_4;
	// TriLib.NodeData[] TriLib.NodeData::Children
	NodeDataU5BU5D_t2903451E388B76A240D86BC410A23B2EF74CF920* ___Children_5;
	// UnityEngine.GameObject TriLib.NodeData::GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameObject_6;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Path_1() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Path_1)); }
	inline String_t* get_Path_1() const { return ___Path_1; }
	inline String_t** get_address_of_Path_1() { return &___Path_1; }
	inline void set_Path_1(String_t* value)
	{
		___Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___Path_1), value);
	}

	inline static int32_t get_offset_of_Matrix_2() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Matrix_2)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_Matrix_2() const { return ___Matrix_2; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_Matrix_2() { return &___Matrix_2; }
	inline void set_Matrix_2(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___Matrix_2 = value;
	}

	inline static int32_t get_offset_of_Meshes_3() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Meshes_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Meshes_3() const { return ___Meshes_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Meshes_3() { return &___Meshes_3; }
	inline void set_Meshes_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Meshes_3 = value;
		Il2CppCodeGenWriteBarrier((&___Meshes_3), value);
	}

	inline static int32_t get_offset_of_Parent_4() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Parent_4)); }
	inline NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C * get_Parent_4() const { return ___Parent_4; }
	inline NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C ** get_address_of_Parent_4() { return &___Parent_4; }
	inline void set_Parent_4(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C * value)
	{
		___Parent_4 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_4), value);
	}

	inline static int32_t get_offset_of_Children_5() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Children_5)); }
	inline NodeDataU5BU5D_t2903451E388B76A240D86BC410A23B2EF74CF920* get_Children_5() const { return ___Children_5; }
	inline NodeDataU5BU5D_t2903451E388B76A240D86BC410A23B2EF74CF920** get_address_of_Children_5() { return &___Children_5; }
	inline void set_Children_5(NodeDataU5BU5D_t2903451E388B76A240D86BC410A23B2EF74CF920* value)
	{
		___Children_5 = value;
		Il2CppCodeGenWriteBarrier((&___Children_5), value);
	}

	inline static int32_t get_offset_of_GameObject_6() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___GameObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_GameObject_6() const { return ___GameObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_GameObject_6() { return &___GameObject_6; }
	inline void set_GameObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___GameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEDATA_TD2933CCEB90B2F77D752DB697E43E8C616DAC93C_H
#ifndef TEXTURECOMPRESSION_T25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C_H
#define TEXTURECOMPRESSION_T25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureCompression
struct  TextureCompression_t25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C 
{
public:
	// System.Int32 TriLib.TextureCompression::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureCompression_t25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURECOMPRESSION_T25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C_H
#ifndef ZIPGCFILELOADDATA_TBAFBC6955F8E973FA87C0DDFD15968EC80491E53_H
#define ZIPGCFILELOADDATA_TBAFBC6955F8E973FA87C0DDFD15968EC80491E53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ZipGCFileLoadData
struct  ZipGCFileLoadData_tBAFBC6955F8E973FA87C0DDFD15968EC80491E53  : public GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipFile TriLib.ZipGCFileLoadData::ZipFile
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * ___ZipFile_3;

public:
	inline static int32_t get_offset_of_ZipFile_3() { return static_cast<int32_t>(offsetof(ZipGCFileLoadData_tBAFBC6955F8E973FA87C0DDFD15968EC80491E53, ___ZipFile_3)); }
	inline ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * get_ZipFile_3() const { return ___ZipFile_3; }
	inline ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F ** get_address_of_ZipFile_3() { return &___ZipFile_3; }
	inline void set_ZipFile_3(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * value)
	{
		___ZipFile_3 = value;
		Il2CppCodeGenWriteBarrier((&___ZipFile_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPGCFILELOADDATA_TBAFBC6955F8E973FA87C0DDFD15968EC80491E53_H
#ifndef FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#define FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#define TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#ifndef TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#define TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#ifndef A_TB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_H
#define A_TB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// a
struct  a_tB29FDBA317931B7D50F19992FEB1BC2EC7C616FC  : public RuntimeObject
{
public:

public:
};

struct a_tB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_StaticFields
{
public:
	// a_a a::a
	a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E  ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_tB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_StaticFields, ___a_0)); }
	inline a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E  get_a_0() const { return ___a_0; }
	inline a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E * get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(a_tF08E05BEBCCB7007505B2FDCCAC98CD9A986586E  value)
	{
		___a_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_TB29FDBA317931B7D50F19992FEB1BC2EC7C616FC_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ASSIMPMETADATA_T7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD_H
#define ASSIMPMETADATA_T7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpMetadata
struct  AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD  : public RuntimeObject
{
public:
	// TriLib.AssimpMetadataType TriLib.AssimpMetadata::MetadataType
	int32_t ___MetadataType_0;
	// System.UInt32 TriLib.AssimpMetadata::MetadataIndex
	uint32_t ___MetadataIndex_1;
	// System.String TriLib.AssimpMetadata::MetadataKey
	String_t* ___MetadataKey_2;
	// System.Object TriLib.AssimpMetadata::MetadataValue
	RuntimeObject * ___MetadataValue_3;

public:
	inline static int32_t get_offset_of_MetadataType_0() { return static_cast<int32_t>(offsetof(AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD, ___MetadataType_0)); }
	inline int32_t get_MetadataType_0() const { return ___MetadataType_0; }
	inline int32_t* get_address_of_MetadataType_0() { return &___MetadataType_0; }
	inline void set_MetadataType_0(int32_t value)
	{
		___MetadataType_0 = value;
	}

	inline static int32_t get_offset_of_MetadataIndex_1() { return static_cast<int32_t>(offsetof(AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD, ___MetadataIndex_1)); }
	inline uint32_t get_MetadataIndex_1() const { return ___MetadataIndex_1; }
	inline uint32_t* get_address_of_MetadataIndex_1() { return &___MetadataIndex_1; }
	inline void set_MetadataIndex_1(uint32_t value)
	{
		___MetadataIndex_1 = value;
	}

	inline static int32_t get_offset_of_MetadataKey_2() { return static_cast<int32_t>(offsetof(AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD, ___MetadataKey_2)); }
	inline String_t* get_MetadataKey_2() const { return ___MetadataKey_2; }
	inline String_t** get_address_of_MetadataKey_2() { return &___MetadataKey_2; }
	inline void set_MetadataKey_2(String_t* value)
	{
		___MetadataKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___MetadataKey_2), value);
	}

	inline static int32_t get_offset_of_MetadataValue_3() { return static_cast<int32_t>(offsetof(AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD, ___MetadataValue_3)); }
	inline RuntimeObject * get_MetadataValue_3() const { return ___MetadataValue_3; }
	inline RuntimeObject ** get_address_of_MetadataValue_3() { return &___MetadataValue_3; }
	inline void set_MetadataValue_3(RuntimeObject * value)
	{
		___MetadataValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___MetadataValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPMETADATA_T7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD_H
#ifndef MATERIALDATA_T27489063F272E033321F0C94EF062695C238CADA_H
#define MATERIALDATA_T27489063F272E033321F0C94EF062695C238CADA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MaterialData
struct  MaterialData_t27489063F272E033321F0C94EF062695C238CADA  : public RuntimeObject
{
public:
	// System.String TriLib.MaterialData::Name
	String_t* ___Name_0;
	// System.Boolean TriLib.MaterialData::AlphaLoaded
	bool ___AlphaLoaded_1;
	// System.Single TriLib.MaterialData::Alpha
	float ___Alpha_2;
	// System.Boolean TriLib.MaterialData::DiffuseInfoLoaded
	bool ___DiffuseInfoLoaded_3;
	// System.String TriLib.MaterialData::DiffusePath
	String_t* ___DiffusePath_4;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::DiffuseWrapMode
	int32_t ___DiffuseWrapMode_5;
	// System.String TriLib.MaterialData::DiffuseName
	String_t* ___DiffuseName_6;
	// System.Single TriLib.MaterialData::DiffuseBlendMode
	float ___DiffuseBlendMode_7;
	// System.UInt32 TriLib.MaterialData::DiffuseOp
	uint32_t ___DiffuseOp_8;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::DiffuseEmbeddedTextureData
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___DiffuseEmbeddedTextureData_9;
	// System.Boolean TriLib.MaterialData::DiffuseColorLoaded
	bool ___DiffuseColorLoaded_10;
	// UnityEngine.Color TriLib.MaterialData::DiffuseColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___DiffuseColor_11;
	// System.Boolean TriLib.MaterialData::EmissionInfoLoaded
	bool ___EmissionInfoLoaded_12;
	// System.String TriLib.MaterialData::EmissionPath
	String_t* ___EmissionPath_13;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::EmissionWrapMode
	int32_t ___EmissionWrapMode_14;
	// System.String TriLib.MaterialData::EmissionName
	String_t* ___EmissionName_15;
	// System.Single TriLib.MaterialData::EmissionBlendMode
	float ___EmissionBlendMode_16;
	// System.UInt32 TriLib.MaterialData::EmissionOp
	uint32_t ___EmissionOp_17;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::EmissionEmbeddedTextureData
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___EmissionEmbeddedTextureData_18;
	// System.Boolean TriLib.MaterialData::EmissionColorLoaded
	bool ___EmissionColorLoaded_19;
	// UnityEngine.Color TriLib.MaterialData::EmissionColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___EmissionColor_20;
	// System.Boolean TriLib.MaterialData::SpecularInfoLoaded
	bool ___SpecularInfoLoaded_21;
	// System.String TriLib.MaterialData::SpecularPath
	String_t* ___SpecularPath_22;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::SpecularWrapMode
	int32_t ___SpecularWrapMode_23;
	// System.String TriLib.MaterialData::SpecularName
	String_t* ___SpecularName_24;
	// System.Single TriLib.MaterialData::SpecularBlendMode
	float ___SpecularBlendMode_25;
	// System.UInt32 TriLib.MaterialData::SpecularOp
	uint32_t ___SpecularOp_26;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::SpecularEmbeddedTextureData
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___SpecularEmbeddedTextureData_27;
	// System.Boolean TriLib.MaterialData::SpecularColorLoaded
	bool ___SpecularColorLoaded_28;
	// UnityEngine.Color TriLib.MaterialData::SpecularColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SpecularColor_29;
	// System.Boolean TriLib.MaterialData::NormalInfoLoaded
	bool ___NormalInfoLoaded_30;
	// System.String TriLib.MaterialData::NormalPath
	String_t* ___NormalPath_31;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::NormalWrapMode
	int32_t ___NormalWrapMode_32;
	// System.String TriLib.MaterialData::NormalName
	String_t* ___NormalName_33;
	// System.Single TriLib.MaterialData::NormalBlendMode
	float ___NormalBlendMode_34;
	// System.UInt32 TriLib.MaterialData::NormalOp
	uint32_t ___NormalOp_35;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::NormalEmbeddedTextureData
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___NormalEmbeddedTextureData_36;
	// System.Boolean TriLib.MaterialData::HeightInfoLoaded
	bool ___HeightInfoLoaded_37;
	// System.String TriLib.MaterialData::HeightPath
	String_t* ___HeightPath_38;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::HeightWrapMode
	int32_t ___HeightWrapMode_39;
	// System.String TriLib.MaterialData::HeightName
	String_t* ___HeightName_40;
	// System.Single TriLib.MaterialData::HeightBlendMode
	float ___HeightBlendMode_41;
	// System.UInt32 TriLib.MaterialData::HeightOp
	uint32_t ___HeightOp_42;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::HeightEmbeddedTextureData
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___HeightEmbeddedTextureData_43;
	// System.Boolean TriLib.MaterialData::BumpScaleLoaded
	bool ___BumpScaleLoaded_44;
	// System.Single TriLib.MaterialData::BumpScale
	float ___BumpScale_45;
	// System.Boolean TriLib.MaterialData::GlossinessLoaded
	bool ___GlossinessLoaded_46;
	// System.Single TriLib.MaterialData::Glossiness
	float ___Glossiness_47;
	// System.Boolean TriLib.MaterialData::GlossMapScaleLoaded
	bool ___GlossMapScaleLoaded_48;
	// System.Single TriLib.MaterialData::GlossMapScale
	float ___GlossMapScale_49;
	// UnityEngine.Material TriLib.MaterialData::Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___Material_50;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_AlphaLoaded_1() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___AlphaLoaded_1)); }
	inline bool get_AlphaLoaded_1() const { return ___AlphaLoaded_1; }
	inline bool* get_address_of_AlphaLoaded_1() { return &___AlphaLoaded_1; }
	inline void set_AlphaLoaded_1(bool value)
	{
		___AlphaLoaded_1 = value;
	}

	inline static int32_t get_offset_of_Alpha_2() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___Alpha_2)); }
	inline float get_Alpha_2() const { return ___Alpha_2; }
	inline float* get_address_of_Alpha_2() { return &___Alpha_2; }
	inline void set_Alpha_2(float value)
	{
		___Alpha_2 = value;
	}

	inline static int32_t get_offset_of_DiffuseInfoLoaded_3() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseInfoLoaded_3)); }
	inline bool get_DiffuseInfoLoaded_3() const { return ___DiffuseInfoLoaded_3; }
	inline bool* get_address_of_DiffuseInfoLoaded_3() { return &___DiffuseInfoLoaded_3; }
	inline void set_DiffuseInfoLoaded_3(bool value)
	{
		___DiffuseInfoLoaded_3 = value;
	}

	inline static int32_t get_offset_of_DiffusePath_4() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffusePath_4)); }
	inline String_t* get_DiffusePath_4() const { return ___DiffusePath_4; }
	inline String_t** get_address_of_DiffusePath_4() { return &___DiffusePath_4; }
	inline void set_DiffusePath_4(String_t* value)
	{
		___DiffusePath_4 = value;
		Il2CppCodeGenWriteBarrier((&___DiffusePath_4), value);
	}

	inline static int32_t get_offset_of_DiffuseWrapMode_5() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseWrapMode_5)); }
	inline int32_t get_DiffuseWrapMode_5() const { return ___DiffuseWrapMode_5; }
	inline int32_t* get_address_of_DiffuseWrapMode_5() { return &___DiffuseWrapMode_5; }
	inline void set_DiffuseWrapMode_5(int32_t value)
	{
		___DiffuseWrapMode_5 = value;
	}

	inline static int32_t get_offset_of_DiffuseName_6() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseName_6)); }
	inline String_t* get_DiffuseName_6() const { return ___DiffuseName_6; }
	inline String_t** get_address_of_DiffuseName_6() { return &___DiffuseName_6; }
	inline void set_DiffuseName_6(String_t* value)
	{
		___DiffuseName_6 = value;
		Il2CppCodeGenWriteBarrier((&___DiffuseName_6), value);
	}

	inline static int32_t get_offset_of_DiffuseBlendMode_7() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseBlendMode_7)); }
	inline float get_DiffuseBlendMode_7() const { return ___DiffuseBlendMode_7; }
	inline float* get_address_of_DiffuseBlendMode_7() { return &___DiffuseBlendMode_7; }
	inline void set_DiffuseBlendMode_7(float value)
	{
		___DiffuseBlendMode_7 = value;
	}

	inline static int32_t get_offset_of_DiffuseOp_8() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseOp_8)); }
	inline uint32_t get_DiffuseOp_8() const { return ___DiffuseOp_8; }
	inline uint32_t* get_address_of_DiffuseOp_8() { return &___DiffuseOp_8; }
	inline void set_DiffuseOp_8(uint32_t value)
	{
		___DiffuseOp_8 = value;
	}

	inline static int32_t get_offset_of_DiffuseEmbeddedTextureData_9() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseEmbeddedTextureData_9)); }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * get_DiffuseEmbeddedTextureData_9() const { return ___DiffuseEmbeddedTextureData_9; }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 ** get_address_of_DiffuseEmbeddedTextureData_9() { return &___DiffuseEmbeddedTextureData_9; }
	inline void set_DiffuseEmbeddedTextureData_9(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * value)
	{
		___DiffuseEmbeddedTextureData_9 = value;
		Il2CppCodeGenWriteBarrier((&___DiffuseEmbeddedTextureData_9), value);
	}

	inline static int32_t get_offset_of_DiffuseColorLoaded_10() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseColorLoaded_10)); }
	inline bool get_DiffuseColorLoaded_10() const { return ___DiffuseColorLoaded_10; }
	inline bool* get_address_of_DiffuseColorLoaded_10() { return &___DiffuseColorLoaded_10; }
	inline void set_DiffuseColorLoaded_10(bool value)
	{
		___DiffuseColorLoaded_10 = value;
	}

	inline static int32_t get_offset_of_DiffuseColor_11() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseColor_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_DiffuseColor_11() const { return ___DiffuseColor_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_DiffuseColor_11() { return &___DiffuseColor_11; }
	inline void set_DiffuseColor_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___DiffuseColor_11 = value;
	}

	inline static int32_t get_offset_of_EmissionInfoLoaded_12() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionInfoLoaded_12)); }
	inline bool get_EmissionInfoLoaded_12() const { return ___EmissionInfoLoaded_12; }
	inline bool* get_address_of_EmissionInfoLoaded_12() { return &___EmissionInfoLoaded_12; }
	inline void set_EmissionInfoLoaded_12(bool value)
	{
		___EmissionInfoLoaded_12 = value;
	}

	inline static int32_t get_offset_of_EmissionPath_13() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionPath_13)); }
	inline String_t* get_EmissionPath_13() const { return ___EmissionPath_13; }
	inline String_t** get_address_of_EmissionPath_13() { return &___EmissionPath_13; }
	inline void set_EmissionPath_13(String_t* value)
	{
		___EmissionPath_13 = value;
		Il2CppCodeGenWriteBarrier((&___EmissionPath_13), value);
	}

	inline static int32_t get_offset_of_EmissionWrapMode_14() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionWrapMode_14)); }
	inline int32_t get_EmissionWrapMode_14() const { return ___EmissionWrapMode_14; }
	inline int32_t* get_address_of_EmissionWrapMode_14() { return &___EmissionWrapMode_14; }
	inline void set_EmissionWrapMode_14(int32_t value)
	{
		___EmissionWrapMode_14 = value;
	}

	inline static int32_t get_offset_of_EmissionName_15() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionName_15)); }
	inline String_t* get_EmissionName_15() const { return ___EmissionName_15; }
	inline String_t** get_address_of_EmissionName_15() { return &___EmissionName_15; }
	inline void set_EmissionName_15(String_t* value)
	{
		___EmissionName_15 = value;
		Il2CppCodeGenWriteBarrier((&___EmissionName_15), value);
	}

	inline static int32_t get_offset_of_EmissionBlendMode_16() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionBlendMode_16)); }
	inline float get_EmissionBlendMode_16() const { return ___EmissionBlendMode_16; }
	inline float* get_address_of_EmissionBlendMode_16() { return &___EmissionBlendMode_16; }
	inline void set_EmissionBlendMode_16(float value)
	{
		___EmissionBlendMode_16 = value;
	}

	inline static int32_t get_offset_of_EmissionOp_17() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionOp_17)); }
	inline uint32_t get_EmissionOp_17() const { return ___EmissionOp_17; }
	inline uint32_t* get_address_of_EmissionOp_17() { return &___EmissionOp_17; }
	inline void set_EmissionOp_17(uint32_t value)
	{
		___EmissionOp_17 = value;
	}

	inline static int32_t get_offset_of_EmissionEmbeddedTextureData_18() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionEmbeddedTextureData_18)); }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * get_EmissionEmbeddedTextureData_18() const { return ___EmissionEmbeddedTextureData_18; }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 ** get_address_of_EmissionEmbeddedTextureData_18() { return &___EmissionEmbeddedTextureData_18; }
	inline void set_EmissionEmbeddedTextureData_18(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * value)
	{
		___EmissionEmbeddedTextureData_18 = value;
		Il2CppCodeGenWriteBarrier((&___EmissionEmbeddedTextureData_18), value);
	}

	inline static int32_t get_offset_of_EmissionColorLoaded_19() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionColorLoaded_19)); }
	inline bool get_EmissionColorLoaded_19() const { return ___EmissionColorLoaded_19; }
	inline bool* get_address_of_EmissionColorLoaded_19() { return &___EmissionColorLoaded_19; }
	inline void set_EmissionColorLoaded_19(bool value)
	{
		___EmissionColorLoaded_19 = value;
	}

	inline static int32_t get_offset_of_EmissionColor_20() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionColor_20)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_EmissionColor_20() const { return ___EmissionColor_20; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_EmissionColor_20() { return &___EmissionColor_20; }
	inline void set_EmissionColor_20(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___EmissionColor_20 = value;
	}

	inline static int32_t get_offset_of_SpecularInfoLoaded_21() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularInfoLoaded_21)); }
	inline bool get_SpecularInfoLoaded_21() const { return ___SpecularInfoLoaded_21; }
	inline bool* get_address_of_SpecularInfoLoaded_21() { return &___SpecularInfoLoaded_21; }
	inline void set_SpecularInfoLoaded_21(bool value)
	{
		___SpecularInfoLoaded_21 = value;
	}

	inline static int32_t get_offset_of_SpecularPath_22() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularPath_22)); }
	inline String_t* get_SpecularPath_22() const { return ___SpecularPath_22; }
	inline String_t** get_address_of_SpecularPath_22() { return &___SpecularPath_22; }
	inline void set_SpecularPath_22(String_t* value)
	{
		___SpecularPath_22 = value;
		Il2CppCodeGenWriteBarrier((&___SpecularPath_22), value);
	}

	inline static int32_t get_offset_of_SpecularWrapMode_23() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularWrapMode_23)); }
	inline int32_t get_SpecularWrapMode_23() const { return ___SpecularWrapMode_23; }
	inline int32_t* get_address_of_SpecularWrapMode_23() { return &___SpecularWrapMode_23; }
	inline void set_SpecularWrapMode_23(int32_t value)
	{
		___SpecularWrapMode_23 = value;
	}

	inline static int32_t get_offset_of_SpecularName_24() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularName_24)); }
	inline String_t* get_SpecularName_24() const { return ___SpecularName_24; }
	inline String_t** get_address_of_SpecularName_24() { return &___SpecularName_24; }
	inline void set_SpecularName_24(String_t* value)
	{
		___SpecularName_24 = value;
		Il2CppCodeGenWriteBarrier((&___SpecularName_24), value);
	}

	inline static int32_t get_offset_of_SpecularBlendMode_25() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularBlendMode_25)); }
	inline float get_SpecularBlendMode_25() const { return ___SpecularBlendMode_25; }
	inline float* get_address_of_SpecularBlendMode_25() { return &___SpecularBlendMode_25; }
	inline void set_SpecularBlendMode_25(float value)
	{
		___SpecularBlendMode_25 = value;
	}

	inline static int32_t get_offset_of_SpecularOp_26() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularOp_26)); }
	inline uint32_t get_SpecularOp_26() const { return ___SpecularOp_26; }
	inline uint32_t* get_address_of_SpecularOp_26() { return &___SpecularOp_26; }
	inline void set_SpecularOp_26(uint32_t value)
	{
		___SpecularOp_26 = value;
	}

	inline static int32_t get_offset_of_SpecularEmbeddedTextureData_27() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularEmbeddedTextureData_27)); }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * get_SpecularEmbeddedTextureData_27() const { return ___SpecularEmbeddedTextureData_27; }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 ** get_address_of_SpecularEmbeddedTextureData_27() { return &___SpecularEmbeddedTextureData_27; }
	inline void set_SpecularEmbeddedTextureData_27(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * value)
	{
		___SpecularEmbeddedTextureData_27 = value;
		Il2CppCodeGenWriteBarrier((&___SpecularEmbeddedTextureData_27), value);
	}

	inline static int32_t get_offset_of_SpecularColorLoaded_28() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularColorLoaded_28)); }
	inline bool get_SpecularColorLoaded_28() const { return ___SpecularColorLoaded_28; }
	inline bool* get_address_of_SpecularColorLoaded_28() { return &___SpecularColorLoaded_28; }
	inline void set_SpecularColorLoaded_28(bool value)
	{
		___SpecularColorLoaded_28 = value;
	}

	inline static int32_t get_offset_of_SpecularColor_29() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularColor_29)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SpecularColor_29() const { return ___SpecularColor_29; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SpecularColor_29() { return &___SpecularColor_29; }
	inline void set_SpecularColor_29(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SpecularColor_29 = value;
	}

	inline static int32_t get_offset_of_NormalInfoLoaded_30() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalInfoLoaded_30)); }
	inline bool get_NormalInfoLoaded_30() const { return ___NormalInfoLoaded_30; }
	inline bool* get_address_of_NormalInfoLoaded_30() { return &___NormalInfoLoaded_30; }
	inline void set_NormalInfoLoaded_30(bool value)
	{
		___NormalInfoLoaded_30 = value;
	}

	inline static int32_t get_offset_of_NormalPath_31() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalPath_31)); }
	inline String_t* get_NormalPath_31() const { return ___NormalPath_31; }
	inline String_t** get_address_of_NormalPath_31() { return &___NormalPath_31; }
	inline void set_NormalPath_31(String_t* value)
	{
		___NormalPath_31 = value;
		Il2CppCodeGenWriteBarrier((&___NormalPath_31), value);
	}

	inline static int32_t get_offset_of_NormalWrapMode_32() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalWrapMode_32)); }
	inline int32_t get_NormalWrapMode_32() const { return ___NormalWrapMode_32; }
	inline int32_t* get_address_of_NormalWrapMode_32() { return &___NormalWrapMode_32; }
	inline void set_NormalWrapMode_32(int32_t value)
	{
		___NormalWrapMode_32 = value;
	}

	inline static int32_t get_offset_of_NormalName_33() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalName_33)); }
	inline String_t* get_NormalName_33() const { return ___NormalName_33; }
	inline String_t** get_address_of_NormalName_33() { return &___NormalName_33; }
	inline void set_NormalName_33(String_t* value)
	{
		___NormalName_33 = value;
		Il2CppCodeGenWriteBarrier((&___NormalName_33), value);
	}

	inline static int32_t get_offset_of_NormalBlendMode_34() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalBlendMode_34)); }
	inline float get_NormalBlendMode_34() const { return ___NormalBlendMode_34; }
	inline float* get_address_of_NormalBlendMode_34() { return &___NormalBlendMode_34; }
	inline void set_NormalBlendMode_34(float value)
	{
		___NormalBlendMode_34 = value;
	}

	inline static int32_t get_offset_of_NormalOp_35() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalOp_35)); }
	inline uint32_t get_NormalOp_35() const { return ___NormalOp_35; }
	inline uint32_t* get_address_of_NormalOp_35() { return &___NormalOp_35; }
	inline void set_NormalOp_35(uint32_t value)
	{
		___NormalOp_35 = value;
	}

	inline static int32_t get_offset_of_NormalEmbeddedTextureData_36() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalEmbeddedTextureData_36)); }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * get_NormalEmbeddedTextureData_36() const { return ___NormalEmbeddedTextureData_36; }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 ** get_address_of_NormalEmbeddedTextureData_36() { return &___NormalEmbeddedTextureData_36; }
	inline void set_NormalEmbeddedTextureData_36(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * value)
	{
		___NormalEmbeddedTextureData_36 = value;
		Il2CppCodeGenWriteBarrier((&___NormalEmbeddedTextureData_36), value);
	}

	inline static int32_t get_offset_of_HeightInfoLoaded_37() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightInfoLoaded_37)); }
	inline bool get_HeightInfoLoaded_37() const { return ___HeightInfoLoaded_37; }
	inline bool* get_address_of_HeightInfoLoaded_37() { return &___HeightInfoLoaded_37; }
	inline void set_HeightInfoLoaded_37(bool value)
	{
		___HeightInfoLoaded_37 = value;
	}

	inline static int32_t get_offset_of_HeightPath_38() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightPath_38)); }
	inline String_t* get_HeightPath_38() const { return ___HeightPath_38; }
	inline String_t** get_address_of_HeightPath_38() { return &___HeightPath_38; }
	inline void set_HeightPath_38(String_t* value)
	{
		___HeightPath_38 = value;
		Il2CppCodeGenWriteBarrier((&___HeightPath_38), value);
	}

	inline static int32_t get_offset_of_HeightWrapMode_39() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightWrapMode_39)); }
	inline int32_t get_HeightWrapMode_39() const { return ___HeightWrapMode_39; }
	inline int32_t* get_address_of_HeightWrapMode_39() { return &___HeightWrapMode_39; }
	inline void set_HeightWrapMode_39(int32_t value)
	{
		___HeightWrapMode_39 = value;
	}

	inline static int32_t get_offset_of_HeightName_40() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightName_40)); }
	inline String_t* get_HeightName_40() const { return ___HeightName_40; }
	inline String_t** get_address_of_HeightName_40() { return &___HeightName_40; }
	inline void set_HeightName_40(String_t* value)
	{
		___HeightName_40 = value;
		Il2CppCodeGenWriteBarrier((&___HeightName_40), value);
	}

	inline static int32_t get_offset_of_HeightBlendMode_41() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightBlendMode_41)); }
	inline float get_HeightBlendMode_41() const { return ___HeightBlendMode_41; }
	inline float* get_address_of_HeightBlendMode_41() { return &___HeightBlendMode_41; }
	inline void set_HeightBlendMode_41(float value)
	{
		___HeightBlendMode_41 = value;
	}

	inline static int32_t get_offset_of_HeightOp_42() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightOp_42)); }
	inline uint32_t get_HeightOp_42() const { return ___HeightOp_42; }
	inline uint32_t* get_address_of_HeightOp_42() { return &___HeightOp_42; }
	inline void set_HeightOp_42(uint32_t value)
	{
		___HeightOp_42 = value;
	}

	inline static int32_t get_offset_of_HeightEmbeddedTextureData_43() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightEmbeddedTextureData_43)); }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * get_HeightEmbeddedTextureData_43() const { return ___HeightEmbeddedTextureData_43; }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 ** get_address_of_HeightEmbeddedTextureData_43() { return &___HeightEmbeddedTextureData_43; }
	inline void set_HeightEmbeddedTextureData_43(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * value)
	{
		___HeightEmbeddedTextureData_43 = value;
		Il2CppCodeGenWriteBarrier((&___HeightEmbeddedTextureData_43), value);
	}

	inline static int32_t get_offset_of_BumpScaleLoaded_44() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___BumpScaleLoaded_44)); }
	inline bool get_BumpScaleLoaded_44() const { return ___BumpScaleLoaded_44; }
	inline bool* get_address_of_BumpScaleLoaded_44() { return &___BumpScaleLoaded_44; }
	inline void set_BumpScaleLoaded_44(bool value)
	{
		___BumpScaleLoaded_44 = value;
	}

	inline static int32_t get_offset_of_BumpScale_45() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___BumpScale_45)); }
	inline float get_BumpScale_45() const { return ___BumpScale_45; }
	inline float* get_address_of_BumpScale_45() { return &___BumpScale_45; }
	inline void set_BumpScale_45(float value)
	{
		___BumpScale_45 = value;
	}

	inline static int32_t get_offset_of_GlossinessLoaded_46() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___GlossinessLoaded_46)); }
	inline bool get_GlossinessLoaded_46() const { return ___GlossinessLoaded_46; }
	inline bool* get_address_of_GlossinessLoaded_46() { return &___GlossinessLoaded_46; }
	inline void set_GlossinessLoaded_46(bool value)
	{
		___GlossinessLoaded_46 = value;
	}

	inline static int32_t get_offset_of_Glossiness_47() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___Glossiness_47)); }
	inline float get_Glossiness_47() const { return ___Glossiness_47; }
	inline float* get_address_of_Glossiness_47() { return &___Glossiness_47; }
	inline void set_Glossiness_47(float value)
	{
		___Glossiness_47 = value;
	}

	inline static int32_t get_offset_of_GlossMapScaleLoaded_48() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___GlossMapScaleLoaded_48)); }
	inline bool get_GlossMapScaleLoaded_48() const { return ___GlossMapScaleLoaded_48; }
	inline bool* get_address_of_GlossMapScaleLoaded_48() { return &___GlossMapScaleLoaded_48; }
	inline void set_GlossMapScaleLoaded_48(bool value)
	{
		___GlossMapScaleLoaded_48 = value;
	}

	inline static int32_t get_offset_of_GlossMapScale_49() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___GlossMapScale_49)); }
	inline float get_GlossMapScale_49() const { return ___GlossMapScale_49; }
	inline float* get_address_of_GlossMapScale_49() { return &___GlossMapScale_49; }
	inline void set_GlossMapScale_49(float value)
	{
		___GlossMapScale_49 = value;
	}

	inline static int32_t get_offset_of_Material_50() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___Material_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_Material_50() const { return ___Material_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_Material_50() { return &___Material_50; }
	inline void set_Material_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___Material_50 = value;
		Il2CppCodeGenWriteBarrier((&___Material_50), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALDATA_T27489063F272E033321F0C94EF062695C238CADA_H
#ifndef AVATAR_T14B515893D5504566D487FFE046DCB8C8C50D02B_H
#define AVATAR_T14B515893D5504566D487FFE046DCB8C8C50D02B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Avatar
struct  Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATAR_T14B515893D5504566D487FFE046DCB8C8C50D02B_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#define MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#ifndef MESH_T6106B8D8E4C691321581AB0445552EC78B947B8C_H
#define MESH_T6106B8D8E4C691321581AB0445552EC78B947B8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T6106B8D8E4C691321581AB0445552EC78B947B8C_H
#ifndef TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#define TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifndef ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#define ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t591D2A86165F896B4B800BB5C25CE18672A55579  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#ifndef ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#define ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifndef THREADSTART_T09FFA4371E4B2A713F212B157CC9B8B61983B5BF_H
#define THREADSTART_T09FFA4371E4B2A713F212B157CC9B8B61983B5BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadStart
struct  ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSTART_T09FFA4371E4B2A713F212B157CC9B8B61983B5BF_H
#ifndef DATACALLBACK_T0C125601C6F711EE7EBBFFC24B0A519E2243A0AF_H
#define DATACALLBACK_T0C125601C6F711EE7EBBFFC24B0A519E2243A0AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop_DataCallback
struct  DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACALLBACK_T0C125601C6F711EE7EBBFFC24B0A519E2243A0AF_H
#ifndef EXISTSCALLBACK_T8473DF0E166AE4F98314FBD5199893CA67012C2F_H
#define EXISTSCALLBACK_T8473DF0E166AE4F98314FBD5199893CA67012C2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop_ExistsCallback
struct  ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXISTSCALLBACK_T8473DF0E166AE4F98314FBD5199893CA67012C2F_H
#ifndef PROGRESSCALLBACK_T39D480CF4ABBF766B79A16905486E417FE40DBE6_H
#define PROGRESSCALLBACK_T39D480CF4ABBF766B79A16905486E417FE40DBE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop_ProgressCallback
struct  ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSCALLBACK_T39D480CF4ABBF766B79A16905486E417FE40DBE6_H
#ifndef AVATARCREATEDHANDLE_T7B8714B52752D992F191D2D5E77CC71206196B80_H
#define AVATARCREATEDHANDLE_T7B8714B52752D992F191D2D5E77CC71206196B80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AvatarCreatedHandle
struct  AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARCREATEDHANDLE_T7B8714B52752D992F191D2D5E77CC71206196B80_H
#ifndef BLENDSHAPEKEYCREATEDHANDLE_TE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B_H
#define BLENDSHAPEKEYCREATEDHANDLE_TE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.BlendShapeKeyCreatedHandle
struct  BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSHAPEKEYCREATEDHANDLE_TE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B_H
#ifndef DATADISPOSALCALLBACK_T6963DE3A2EE2117382E3DDC8435453FC38426BFB_H
#define DATADISPOSALCALLBACK_T6963DE3A2EE2117382E3DDC8435453FC38426BFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.DataDisposalCallback
struct  DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATADISPOSALCALLBACK_T6963DE3A2EE2117382E3DDC8435453FC38426BFB_H
#ifndef EMBEDDEDTEXTURELOADCALLBACK_TD05D49054BB811D79C7297F03927D06DE9E8F3E7_H
#define EMBEDDEDTEXTURELOADCALLBACK_TD05D49054BB811D79C7297F03927D06DE9E8F3E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.EmbeddedTextureLoadCallback
struct  EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMBEDDEDTEXTURELOADCALLBACK_TD05D49054BB811D79C7297F03927D06DE9E8F3E7_H
#ifndef LOADTEXTUREDATACALLBACK_TE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3_H
#define LOADTEXTUREDATACALLBACK_TE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.LoadTextureDataCallback
struct  LoadTextureDataCallback_tE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADTEXTUREDATACALLBACK_TE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3_H
#ifndef MATERIALCREATEDHANDLE_TCFCF9310D550A45C8C968E6E65FA3254E5395F78_H
#define MATERIALCREATEDHANDLE_TCFCF9310D550A45C8C968E6E65FA3254E5395F78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MaterialCreatedHandle
struct  MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALCREATEDHANDLE_TCFCF9310D550A45C8C968E6E65FA3254E5395F78_H
#ifndef MESHCREATEDHANDLE_T3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3_H
#define MESHCREATEDHANDLE_T3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MeshCreatedHandle
struct  MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCREATEDHANDLE_T3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3_H
#ifndef METADATAPROCESSEDHANDLE_TF83A79A9D6E56AD891A607A5DB625A0E564EE63E_H
#define METADATAPROCESSEDHANDLE_TF83A79A9D6E56AD891A607A5DB625A0E564EE63E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MetadataProcessedHandle
struct  MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROCESSEDHANDLE_TF83A79A9D6E56AD891A607A5DB625A0E564EE63E_H
#ifndef OBJECTLOADEDHANDLE_TE2DB0F8E41A86557CE40122A88339551B399D044_H
#define OBJECTLOADEDHANDLE_TE2DB0F8E41A86557CE40122A88339551B399D044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ObjectLoadedHandle
struct  ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTLOADEDHANDLE_TE2DB0F8E41A86557CE40122A88339551B399D044_H
#ifndef TEXTURELOADHANDLE_T29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D_H
#define TEXTURELOADHANDLE_T29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureLoadHandle
struct  TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURELOADHANDLE_T29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D_H
#ifndef TEXTUREPRELOADHANDLE_TC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23_H
#define TEXTUREPRELOADHANDLE_TC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TexturePreLoadHandle
struct  TexturePreLoadHandle_tC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPRELOADHANDLE_TC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#define TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#define ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef DISPATCHER_TB781C2F44E71A5E1249F3232459FDED4D76B39D7_H
#define DISPATCHER_TB781C2F44E71A5E1249F3232459FDED4D76B39D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Dispatcher
struct  Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields
{
public:
	// TriLib.Dispatcher TriLib.Dispatcher::a
	Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * ___a_4;
	// System.Boolean TriLib.Dispatcher::b
	bool ___b_5;
	// System.Object TriLib.Dispatcher::c
	RuntimeObject * ___c_6;
	// System.Collections.Generic.Queue`1<System.Action> TriLib.Dispatcher::d
	Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * ___d_7;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields, ___a_4)); }
	inline Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * get_a_4() const { return ___a_4; }
	inline Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields, ___b_5)); }
	inline bool get_b_5() const { return ___b_5; }
	inline bool* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(bool value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields, ___c_6)); }
	inline RuntimeObject * get_c_6() const { return ___c_6; }
	inline RuntimeObject ** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(RuntimeObject * value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields, ___d_7)); }
	inline Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * get_d_7() const { return ___d_7; }
	inline Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D ** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHER_TB781C2F44E71A5E1249F3232459FDED4D76B39D7_H
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  m_Items[1];

public:
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  m_Items[1];

public:
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  m_Items[1];

public:
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(!0)
extern "C" IL2CPP_METHOD_ATTR void Queue_1_Enqueue_m12D1C0BBE742C2537335B7E2B71F7E42A421A6FD_gshared (Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<System.Object>::Dequeue()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Queue_1_Dequeue_m6013DB8A542ACA15F662B6832ED389BB061EFEDE_gshared (Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t Queue_1_get_Count_m0CE0B6919A09EFFBB1EBA5B5DFEF50E4F8A89CFA_gshared (Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Queue_1__ctor_m57D20E9B6532A644845C835306D5BCBCD3163964_gshared (Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F  List_1_GetEnumerator_m006ABCBEDE7E5C2254B3D5393230AD09BE5D35B5_gshared (List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Runtime.InteropServices.GCHandle>::get_Current()
extern "C" IL2CPP_METHOD_ATTR GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  Enumerator_get_Current_m10833796C91ECE236B89832F9E92B26177E0C5EE_gshared (Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Runtime.InteropServices.GCHandle>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mF8DDDD6ADA4D6EE84C9D0827FEDB491318A8DF18_gshared (Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Runtime.InteropServices.GCHandle>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_mEF3D4BA77CC66D385732EA73C5FBC441B4EADB2D_gshared (Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m4CDF70E93E8FA24EE5A3711F44050893C037D388_gshared (List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * __this, GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m067BB9ECCE18366950BE75D550F8C62D6B61EFA7_gshared (List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<TriLib.Dispatcher>()
inline Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * GameObject_AddComponent_TisDispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_m133548093B1F19680188CF25587863E8A639B5F4 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared)(__this, method);
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
extern "C" IL2CPP_METHOD_ATTR void Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5 (RuntimeObject * p0, bool* p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<System.Action>::Enqueue(!0)
inline void Queue_1_Enqueue_m7694F6F1EE0F232575659FA4C6AB608B5250ECF5 (Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * __this, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * p0, const RuntimeMethod* method)
{
	((  void (*) (Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D *, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *, const RuntimeMethod*))Queue_1_Enqueue_m12D1C0BBE742C2537335B7E2B71F7E42A421A6FD_gshared)(__this, p0, method);
}
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<System.Action>::Dequeue()
inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * Queue_1_Dequeue_mAB1E8E40E4211793A2E12D4592AC98240DD22161 (Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * __this, const RuntimeMethod* method)
{
	return ((  Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * (*) (Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D *, const RuntimeMethod*))Queue_1_Dequeue_m6013DB8A542ACA15F662B6832ED389BB061EFEDE_gshared)(__this, method);
}
// System.Void System.Action::Invoke()
extern "C" IL2CPP_METHOD_ATTR void Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<System.Action>::get_Count()
inline int32_t Queue_1_get_Count_mF7EC2631EF60F0F94E4A081B0CA88DF05CD4B09E (Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D *, const RuntimeMethod*))Queue_1_get_Count_m0CE0B6919A09EFFBB1EBA5B5DFEF50E4F8A89CFA_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<System.Action>::.ctor()
inline void Queue_1__ctor_m0627B54FDADD1BC91409C5E9149CCF2CE4068554 (Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * __this, const RuntimeMethod* method)
{
	((  void (*) (Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D *, const RuntimeMethod*))Queue_1__ctor_m57D20E9B6532A644845C835306D5BCBCD3163964_gshared)(__this, method);
}
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR bool IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61 (intptr_t p0, intptr_t p1, const RuntimeMethod* method);
// System.Void TriLib.DataDisposalCallback::Invoke(System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void DataDisposalCallback_Invoke_m658949AFA4C423B49F12A7B4BD989E5C3E3F37A3 (DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * __this, intptr_t ___dataPointer0, const RuntimeMethod* method);
// System.Int32 System.String::LastIndexOf(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t String_LastIndexOf_mC924D20DC71F85A7106D9DD09BF41497C6816E20 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE (String_t* __this, int32_t p0, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Int32 System.String::LastIndexOf(System.Char)
extern "C" IL2CPP_METHOD_ATTR int32_t String_LastIndexOf_m76C37E3915E802044761572007B8FB0635995F59 (String_t* __this, Il2CppChar p0, const RuntimeMethod* method);
// System.String System.String::ToLowerInvariant()
extern "C" IL2CPP_METHOD_ATTR String_t* String_ToLowerInvariant_m197BD65B6582DC546FF1BC398161EEFA708F799E (String_t* __this, const RuntimeMethod* method);
// System.String System.IO.Path::GetFileName(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* Path_GetFileName_m2307E8E0B250632002840D9EC27DBABE9C4EB85E (String_t* p0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String System.String::Replace(System.Char,System.Char)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Replace_m276641366A463205C185A9B3DC0E24ECB95122C9 (String_t* __this, Il2CppChar p0, Il2CppChar p1, const RuntimeMethod* method);
// System.Byte[] System.IO.File::ReadAllBytes(System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* File_ReadAllBytes_mF29468CED0B7B3B7C0971ACEBB16A38683718BEC (String_t* p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>::GetEnumerator()
inline Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F  List_1_GetEnumerator_m006ABCBEDE7E5C2254B3D5393230AD09BE5D35B5 (List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F  (*) (List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 *, const RuntimeMethod*))List_1_GetEnumerator_m006ABCBEDE7E5C2254B3D5393230AD09BE5D35B5_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Runtime.InteropServices.GCHandle>::get_Current()
inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  Enumerator_get_Current_m10833796C91ECE236B89832F9E92B26177E0C5EE (Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F * __this, const RuntimeMethod* method)
{
	return ((  GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  (*) (Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F *, const RuntimeMethod*))Enumerator_get_Current_m10833796C91ECE236B89832F9E92B26177E0C5EE_gshared)(__this, method);
}
// System.Void System.Runtime.InteropServices.GCHandle::Free()
extern "C" IL2CPP_METHOD_ATTR void GCHandle_Free_m392ECC9B1058E35A0FD5CF21A65F212873FC26F0 (GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Runtime.InteropServices.GCHandle>::MoveNext()
inline bool Enumerator_MoveNext_mF8DDDD6ADA4D6EE84C9D0827FEDB491318A8DF18 (Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F *, const RuntimeMethod*))Enumerator_MoveNext_mF8DDDD6ADA4D6EE84C9D0827FEDB491318A8DF18_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Runtime.InteropServices.GCHandle>::Dispose()
inline void Enumerator_Dispose_mEF3D4BA77CC66D385732EA73C5FBC441B4EADB2D (Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F *, const RuntimeMethod*))Enumerator_Dispose_mEF3D4BA77CC66D385732EA73C5FBC441B4EADB2D_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>::Add(!0)
inline void List_1_Add_m4CDF70E93E8FA24EE5A3711F44050893C037D388 (List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * __this, GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 *, GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 , const RuntimeMethod*))List_1_Add_m4CDF70E93E8FA24EE5A3711F44050893C037D388_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>::.ctor()
inline void List_1__ctor_m067BB9ECCE18366950BE75D550F8C62D6B61EFA7 (List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 *, const RuntimeMethod*))List_1__ctor_m067BB9ECCE18366950BE75D550F8C62D6B61EFA7_gshared)(__this, method);
}
// System.Void TriLib.FileLoadData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FileLoadData__ctor_m00D7C76BFBF854A54F829100201CD91B73A851B6 (FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13 * __this, const RuntimeMethod* method);
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector4_op_Implicit_mEAB05A77FF8B3EE79C31499F0CF0A0D621A6496C (Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector4::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector4_get_magnitude_mE33CE76438DDE4DDBBAD94178B07D9364674D356 (Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void System.IO.MemoryStream::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MemoryStream__ctor_m9D0F92C76EFEDA651B678A98EB693FD945286DC2 (MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * __this, const RuntimeMethod* method);
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C" IL2CPP_METHOD_ATTR CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72 (const RuntimeMethod* method);
// System.String System.Int32::ToString(System.IFormatProvider)
extern "C" IL2CPP_METHOD_ATTR String_t* Int32_ToString_m1D0AF82BDAB5D4710527DD3FEFA6F01246D128A5 (int32_t* __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR bool IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934 (intptr_t p0, intptr_t p1, const RuntimeMethod* method);
// System.Boolean TriLib.Texture2DUtils::a(TriLib.EmbeddedTextureData,UnityEngine.Texture2D&)
extern "C" IL2CPP_METHOD_ATTR bool Texture2DUtils_a_m0E1656118EAA73C07AD9C6854F037E83131767B1 (EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___A_00, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** ___A_11, const RuntimeMethod* method);
// UnityEngine.Texture2D TriLib.Texture2DUtils::a(UnityEngine.Texture2D,System.String,System.Boolean&,UnityEngine.TextureWrapMode,UnityEngine.FilterMode,TriLib.TextureCompression,System.Boolean,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * Texture2DUtils_a_mAFB4665E853FF838B8ED8836C67C43459EBC7DE0 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___A_00, String_t* ___A_11, bool* ___A_22, int32_t ___A_33, int32_t ___A_44, int32_t ___A_55, bool ___A_66, bool ___A_77, bool ___A_88, const RuntimeMethod* method);
// System.Void TriLib.EmbeddedTextureData::Dispose()
extern "C" IL2CPP_METHOD_ATTR void EmbeddedTextureData_Dispose_m2B52F2B9FAFA0FD4A71BC2968FE303D94C90CF3C (EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::LoadRawTextureData(System.IntPtr,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Texture2D_LoadRawTextureData_m7F70DFFDBFA1E307A79838B94BC1A0438E063DFC (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, intptr_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::LoadRawTextureData(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void Texture2D_LoadRawTextureData_m0331275D060EC3521BCCF84194FA35BBEEF767CB (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229 (String_t* p0, const RuntimeMethod* method);
// System.String TriLib.StringUtils::GenerateUniqueName(System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* StringUtils_GenerateUniqueName_m99805772E1AFE9D8F4C32E48C693E157D1739935 (RuntimeObject * ___id0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" IL2CPP_METHOD_ATTR void Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C" IL2CPP_METHOD_ATTR void Texture_set_wrapMode_m85E9A995D5947B59FE13A7311E891F3DEDEBBCEC (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C" IL2CPP_METHOD_ATTR void Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
extern "C" IL2CPP_METHOD_ATTR Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* Texture2D_GetPixels32_m7CC6EC6AD48D4CD84AF28DFDFBE24750900FA2E6 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
extern "C" IL2CPP_METHOD_ATTR int32_t Texture_get_wrapMode_mC21054C7BC6E958937B7459DAF1D17654284B07A (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, const RuntimeMethod* method);
// UnityEngine.FilterMode UnityEngine.Texture::get_filterMode()
extern "C" IL2CPP_METHOD_ATTR int32_t Texture_get_filterMode_m4A05E0414655866D27811376709B3A8C584A2579 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern "C" IL2CPP_METHOD_ATTR void Texture2D_SetPixels32_m8669AE290D19897A70859BE23D9A438EB7EDA67E (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Apply_m368893ECE2F9659BDA54ED1E4EB00D01CC2D1B16 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C" IL2CPP_METHOD_ATTR bool Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5 (const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Compress(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Compress_m3D0191F151DF6D66F312D35FF76BDDE92A3ED395 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, bool p0, const RuntimeMethod* method);
// System.Void TriLib.ThreadUtils/a::.ctor()
extern "C" IL2CPP_METHOD_ATTR void a__ctor_m5469D1F788AEC8E9C66EEF1E122BDECAEF607054 (a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C * __this, const RuntimeMethod* method);
// System.Void TriLib.Dispatcher::CheckInstance()
extern "C" IL2CPP_METHOD_ATTR void Dispatcher_CheckInstance_mB6EDA7B5F82FCE4D44371434F75843BF03F4129E (const RuntimeMethod* method);
// System.Void System.Threading.ThreadStart::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void ThreadStart__ctor_m692348FEAEBAF381D62984EE95B217CC024A77D5 (ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Threading.Thread::.ctor(System.Threading.ThreadStart)
extern "C" IL2CPP_METHOD_ATTR void Thread__ctor_m36A33B990160C4499E991D288341CA325AE66DDD (Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * __this, ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF * p0, const RuntimeMethod* method);
// System.Void System.Threading.Thread::Start()
extern "C" IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void Thread_Start_mE2AC4744AFD147FDC84E8D9317B4E3AB890BC2D6 (Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * __this, const RuntimeMethod* method);
// System.Void TriLib.Dispatcher::InvokeAsync(System.Action)
extern "C" IL2CPP_METHOD_ATTR void Dispatcher_InvokeAsync_mD181ABE170BA0E1427FAA6252A08B75FD997473A (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action0, const RuntimeMethod* method);
// System.Void TriLib.ThreadUtils/b::.ctor()
extern "C" IL2CPP_METHOD_ATTR void b__ctor_m1C4DB70256844B56744B7B6C960099C82B85BC2B (b_tBB4B7EAE3F249876C28028467342545B6E691DD5 * __this, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// UnityEngine.Vector3 TriLib.MatrixExtensions::ExtractScale(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  MatrixExtensions_ExtractScale_mCE27761DAD1E9A346FA55D16E3042B00140042D5 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion TriLib.MatrixExtensions::ExtractRotation(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  MatrixExtensions_ExtractRotation_m447F990451673541F36962C765EBBE40E0811AA1 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 TriLib.MatrixExtensions::ExtractPosition(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  MatrixExtensions_ExtractPosition_mA131E4F43AE4CB616B92459592831C2A244E8026 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Boolean System.String::EndsWith(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_EndsWith_mE4F039DCC2A9FCB8C1ED2D04B00A35E3CE16DE99 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Transform TriLib.TransformExtensions::FindDeepChild(UnityEngine.Transform,System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * TransformExtensions_FindDeepChild_mB87B6DDFE9FBD94736262E668DC9EE0CDFD7459D (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, String_t* ___name1, bool ___endsWith2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Void TriLib.GCFileLoadData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GCFileLoadData__ctor_m5536FE0D9D5D2543DBA887D0EBCC6391307D3D45 (GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D * __this, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96 (String_t* __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018 (String_t* __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  intptr_t DelegatePInvokeWrapper_DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF (DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF * __this, String_t* ___pFile0, int32_t ___fileId1, int32_t* ___fileSize2, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc)(char*, int32_t, int32_t*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter U27___pFile0U27 to native representation
	char* ____pFile0_marshaled = NULL;
	____pFile0_marshaled = il2cpp_codegen_marshal_string(___pFile0);

	// Native function invocation
	intptr_t returnValue = il2cppPInvokeFunc(____pFile0_marshaled, ___fileId1, ___fileSize2);

	// Marshaling cleanup of parameter U27___pFile0U27 native representation
	il2cpp_codegen_marshal_free(____pFile0_marshaled);
	____pFile0_marshaled = NULL;

	return returnValue;
}
// System.Void TriLib.AssimpInterop_DataCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void DataCallback__ctor_mE84EDA18BCE135639C90B9ECF83192156F3027F2 (DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.IntPtr TriLib.AssimpInterop_DataCallback::Invoke(System.String,System.Int32,System.Int32U26)
extern "C" IL2CPP_METHOD_ATTR intptr_t DataCallback_Invoke_m61EC64D6B507EB678FAEFAF7AA62C38AF4D4786A (DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF * __this, String_t* ___pFile0, int32_t ___fileId1, int32_t* ___fileSize2, const RuntimeMethod* method)
{
	intptr_t result;
	memset(&result, 0, sizeof(result));
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 3)
				{
					// open
					typedef intptr_t (*FunctionPointerType) (String_t*, int32_t, int32_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, ___fileSize2, targetMethod);
				}
				else
				{
					// closed
					typedef intptr_t (*FunctionPointerType) (void*, String_t*, int32_t, int32_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___pFile0, ___fileId1, ___fileSize2, targetMethod);
				}
			}
			else if (___parameterCount != 3)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker2< intptr_t, int32_t, int32_t* >::Invoke(targetMethod, ___pFile0, ___fileId1, ___fileSize2);
							else
								result = GenericVirtFuncInvoker2< intptr_t, int32_t, int32_t* >::Invoke(targetMethod, ___pFile0, ___fileId1, ___fileSize2);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker2< intptr_t, int32_t, int32_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___pFile0, ___fileId1, ___fileSize2);
							else
								result = VirtFuncInvoker2< intptr_t, int32_t, int32_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___pFile0, ___fileId1, ___fileSize2);
						}
					}
				}
				else
				{
					typedef intptr_t (*FunctionPointerType) (String_t*, int32_t, int32_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, ___fileSize2, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef intptr_t (*FunctionPointerType) (String_t*, int32_t, int32_t*, const RuntimeMethod*);
							result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, ___fileSize2, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker3< intptr_t, String_t*, int32_t, int32_t* >::Invoke(targetMethod, targetThis, ___pFile0, ___fileId1, ___fileSize2);
							else
								result = GenericVirtFuncInvoker3< intptr_t, String_t*, int32_t, int32_t* >::Invoke(targetMethod, targetThis, ___pFile0, ___fileId1, ___fileSize2);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker3< intptr_t, String_t*, int32_t, int32_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___pFile0, ___fileId1, ___fileSize2);
							else
								result = VirtFuncInvoker3< intptr_t, String_t*, int32_t, int32_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___pFile0, ___fileId1, ___fileSize2);
						}
					}
				}
				else
				{
					typedef intptr_t (*FunctionPointerType) (void*, String_t*, int32_t, int32_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___pFile0, ___fileId1, ___fileSize2, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 3)
			{
				// open
				typedef intptr_t (*FunctionPointerType) (String_t*, int32_t, int32_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, ___fileSize2, targetMethod);
			}
			else
			{
				// closed
				typedef intptr_t (*FunctionPointerType) (void*, String_t*, int32_t, int32_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___pFile0, ___fileId1, ___fileSize2, targetMethod);
			}
		}
		else if (___parameterCount != 3)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker2< intptr_t, int32_t, int32_t* >::Invoke(targetMethod, ___pFile0, ___fileId1, ___fileSize2);
						else
							result = GenericVirtFuncInvoker2< intptr_t, int32_t, int32_t* >::Invoke(targetMethod, ___pFile0, ___fileId1, ___fileSize2);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker2< intptr_t, int32_t, int32_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___pFile0, ___fileId1, ___fileSize2);
						else
							result = VirtFuncInvoker2< intptr_t, int32_t, int32_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___pFile0, ___fileId1, ___fileSize2);
					}
				}
			}
			else
			{
				typedef intptr_t (*FunctionPointerType) (String_t*, int32_t, int32_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, ___fileSize2, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef intptr_t (*FunctionPointerType) (String_t*, int32_t, int32_t*, const RuntimeMethod*);
						result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, ___fileSize2, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker3< intptr_t, String_t*, int32_t, int32_t* >::Invoke(targetMethod, targetThis, ___pFile0, ___fileId1, ___fileSize2);
						else
							result = GenericVirtFuncInvoker3< intptr_t, String_t*, int32_t, int32_t* >::Invoke(targetMethod, targetThis, ___pFile0, ___fileId1, ___fileSize2);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker3< intptr_t, String_t*, int32_t, int32_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___pFile0, ___fileId1, ___fileSize2);
						else
							result = VirtFuncInvoker3< intptr_t, String_t*, int32_t, int32_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___pFile0, ___fileId1, ___fileSize2);
					}
				}
			}
			else
			{
				typedef intptr_t (*FunctionPointerType) (void*, String_t*, int32_t, int32_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___pFile0, ___fileId1, ___fileSize2, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult TriLib.AssimpInterop_DataCallback::BeginInvoke(System.String,System.Int32,System.Int32U26,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* DataCallback_BeginInvoke_m5098DBB791FCA3235E56ED21ECE83168F01832C1 (DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF * __this, String_t* ___pFile0, int32_t ___fileId1, int32_t* ___fileSize2, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataCallback_BeginInvoke_m5098DBB791FCA3235E56ED21ECE83168F01832C1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___pFile0;
	__d_args[1] = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &___fileId1);
	__d_args[2] = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &*___fileSize2);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.IntPtr TriLib.AssimpInterop_DataCallback::EndInvoke(System.Int32U26,System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR intptr_t DataCallback_EndInvoke_m3BAE2E721BE2B653B208D7B607AE7C75085E7D99 (DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF * __this, int32_t* ___fileSize0, RuntimeObject* ___result1, const RuntimeMethod* method)
{
	void* ___out_args[] = {
	___fileSize0,
	};
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
	return *(intptr_t*)UnBox ((RuntimeObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  bool DelegatePInvokeWrapper_ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F (ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F * __this, String_t* ___pFile0, int32_t ___fileId1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc)(char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter U27___pFile0U27 to native representation
	char* ____pFile0_marshaled = NULL;
	____pFile0_marshaled = il2cpp_codegen_marshal_string(___pFile0);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____pFile0_marshaled, ___fileId1);

	// Marshaling cleanup of parameter U27___pFile0U27 native representation
	il2cpp_codegen_marshal_free(____pFile0_marshaled);
	____pFile0_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.Void TriLib.AssimpInterop_ExistsCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void ExistsCallback__ctor_m0BCF7C78687E99F65BA6BB3E56F504D7ED9BAEE5 (ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean TriLib.AssimpInterop_ExistsCallback::Invoke(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool ExistsCallback_Invoke_m4D47773CCDCB2BA2296CDDB5D43F8BC2A0B3C2C5 (ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F * __this, String_t* ___pFile0, int32_t ___fileId1, const RuntimeMethod* method)
{
	bool result = false;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 2)
				{
					// open
					typedef bool (*FunctionPointerType) (String_t*, int32_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, targetMethod);
				}
				else
				{
					// closed
					typedef bool (*FunctionPointerType) (void*, String_t*, int32_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___pFile0, ___fileId1, targetMethod);
				}
			}
			else if (___parameterCount != 2)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker1< bool, int32_t >::Invoke(targetMethod, ___pFile0, ___fileId1);
							else
								result = GenericVirtFuncInvoker1< bool, int32_t >::Invoke(targetMethod, ___pFile0, ___fileId1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker1< bool, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___pFile0, ___fileId1);
							else
								result = VirtFuncInvoker1< bool, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___pFile0, ___fileId1);
						}
					}
				}
				else
				{
					typedef bool (*FunctionPointerType) (String_t*, int32_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef bool (*FunctionPointerType) (String_t*, int32_t, const RuntimeMethod*);
							result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker2< bool, String_t*, int32_t >::Invoke(targetMethod, targetThis, ___pFile0, ___fileId1);
							else
								result = GenericVirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(targetMethod, targetThis, ___pFile0, ___fileId1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker2< bool, String_t*, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___pFile0, ___fileId1);
							else
								result = VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___pFile0, ___fileId1);
						}
					}
				}
				else
				{
					typedef bool (*FunctionPointerType) (void*, String_t*, int32_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___pFile0, ___fileId1, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef bool (*FunctionPointerType) (String_t*, int32_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, targetMethod);
			}
			else
			{
				// closed
				typedef bool (*FunctionPointerType) (void*, String_t*, int32_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___pFile0, ___fileId1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker1< bool, int32_t >::Invoke(targetMethod, ___pFile0, ___fileId1);
						else
							result = GenericVirtFuncInvoker1< bool, int32_t >::Invoke(targetMethod, ___pFile0, ___fileId1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker1< bool, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___pFile0, ___fileId1);
						else
							result = VirtFuncInvoker1< bool, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___pFile0, ___fileId1);
					}
				}
			}
			else
			{
				typedef bool (*FunctionPointerType) (String_t*, int32_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef bool (*FunctionPointerType) (String_t*, int32_t, const RuntimeMethod*);
						result = ((FunctionPointerType)targetMethodPointer)(___pFile0, ___fileId1, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker2< bool, String_t*, int32_t >::Invoke(targetMethod, targetThis, ___pFile0, ___fileId1);
						else
							result = GenericVirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(targetMethod, targetThis, ___pFile0, ___fileId1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker2< bool, String_t*, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___pFile0, ___fileId1);
						else
							result = VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___pFile0, ___fileId1);
					}
				}
			}
			else
			{
				typedef bool (*FunctionPointerType) (void*, String_t*, int32_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___pFile0, ___fileId1, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult TriLib.AssimpInterop_ExistsCallback::BeginInvoke(System.String,System.Int32,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* ExistsCallback_BeginInvoke_mEA7304C5A8CCB3C5199F935092951D818B39C9F6 (ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F * __this, String_t* ___pFile0, int32_t ___fileId1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExistsCallback_BeginInvoke_mEA7304C5A8CCB3C5199F935092951D818B39C9F6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___pFile0;
	__d_args[1] = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &___fileId1);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Boolean TriLib.AssimpInterop_ExistsCallback::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR bool ExistsCallback_EndInvoke_m451743F819E62FF10F5132577286ECF6099215AD (ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 (ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * __this, float ___progress0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(float);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___progress0);

}
// System.Void TriLib.AssimpInterop_ProgressCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void ProgressCallback__ctor_m6C1F95ADA805C23B197F919A58CE39493D0D09B5 (ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.AssimpInterop_ProgressCallback::Invoke(System.Single)
extern "C" IL2CPP_METHOD_ATTR void ProgressCallback_Invoke_mDF736D8860B472B654A6E35D007EC3EA130C5161 (ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * __this, float ___progress0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 1)
				{
					// open
					typedef void (*FunctionPointerType) (float, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___progress0, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, float, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___progress0, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (float, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___progress0, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< float >::Invoke(targetMethod, targetThis, ___progress0);
							else
								GenericVirtActionInvoker1< float >::Invoke(targetMethod, targetThis, ___progress0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< float >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___progress0);
							else
								VirtActionInvoker1< float >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___progress0);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, float, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___progress0, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (float, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___progress0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, float, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___progress0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (float, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___progress0, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< float >::Invoke(targetMethod, targetThis, ___progress0);
						else
							GenericVirtActionInvoker1< float >::Invoke(targetMethod, targetThis, ___progress0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< float >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___progress0);
						else
							VirtActionInvoker1< float >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___progress0);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, float, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___progress0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.AssimpInterop_ProgressCallback::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* ProgressCallback_BeginInvoke_mFCC262B113759CE445869F09BF2EC939CB5A479C (ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * __this, float ___progress0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ProgressCallback_BeginInvoke_mFCC262B113759CE445869F09BF2EC939CB5A479C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &___progress0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void TriLib.AssimpInterop_ProgressCallback::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void ProgressCallback_EndInvoke_m37BB5C67191813384E1FBF4DA80DE2B817C8CC18 (ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.AssimpMetadata::.ctor(TriLib.AssimpMetadataType,System.UInt32,System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR void AssimpMetadata__ctor_m6443FC5E3132622B5FBDEE8D6ED5DA9F69618400 (AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD * __this, int32_t ___metadataType0, uint32_t ___metadataIndex1, String_t* ___metadataKey2, RuntimeObject * ___metadataValue3, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___metadataType0;
		__this->set_MetadataType_0(L_0);
		uint32_t L_1 = ___metadataIndex1;
		__this->set_MetadataIndex_1(L_1);
		String_t* L_2 = ___metadataKey2;
		__this->set_MetadataKey_2(L_2);
		RuntimeObject * L_3 = ___metadataValue3;
		__this->set_MetadataValue_3(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.AvatarCreatedHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void AvatarCreatedHandle__ctor_m556A562965FD801F6E737DC0C63E8DE7BEF0CE9B (AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.AvatarCreatedHandle::Invoke(UnityEngine.Avatar,UnityEngine.Animator)
extern "C" IL2CPP_METHOD_ATTR void AvatarCreatedHandle_Invoke_m658295491893D0DB798A928B783992ACEDDDEA6A (AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80 * __this, Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B * ___avatar0, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animator1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 2)
				{
					// open
					typedef void (*FunctionPointerType) (Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___avatar0, ___animator1, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___avatar0, ___animator1, targetMethod);
				}
			}
			else if (___parameterCount != 2)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(targetMethod, ___avatar0, ___animator1);
							else
								GenericVirtActionInvoker1< Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(targetMethod, ___avatar0, ___animator1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___avatar0, ___animator1);
							else
								VirtActionInvoker1< Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___avatar0, ___animator1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___avatar0, ___animator1, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A *, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___avatar0, ___animator1, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(targetMethod, targetThis, ___avatar0, ___animator1);
							else
								GenericVirtActionInvoker2< Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(targetMethod, targetThis, ___avatar0, ___animator1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___avatar0, ___animator1);
							else
								VirtActionInvoker2< Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___avatar0, ___animator1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___avatar0, ___animator1, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___avatar0, ___animator1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___avatar0, ___animator1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(targetMethod, ___avatar0, ___animator1);
						else
							GenericVirtActionInvoker1< Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(targetMethod, ___avatar0, ___animator1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___avatar0, ___animator1);
						else
							VirtActionInvoker1< Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___avatar0, ___animator1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___avatar0, ___animator1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___avatar0, ___animator1, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(targetMethod, targetThis, ___avatar0, ___animator1);
						else
							GenericVirtActionInvoker2< Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(targetMethod, targetThis, ___avatar0, ___animator1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___avatar0, ___animator1);
						else
							VirtActionInvoker2< Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___avatar0, ___animator1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B *, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___avatar0, ___animator1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.AvatarCreatedHandle::BeginInvoke(UnityEngine.Avatar,UnityEngine.Animator,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* AvatarCreatedHandle_BeginInvoke_m78E26A6FEC10ECB5AAE78D298AEF864BB39C7915 (AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80 * __this, Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B * ___avatar0, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animator1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___avatar0;
	__d_args[1] = ___animator1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void TriLib.AvatarCreatedHandle::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void AvatarCreatedHandle_EndInvoke_m4EE78767FF3301B6F9125D414E3F1B0DCCA063C1 (AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.BlendShapeKeyCreatedHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void BlendShapeKeyCreatedHandle__ctor_mB8BCF4DF1FA3C18390E46A34E387F5A197AE1721 (BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.BlendShapeKeyCreatedHandle::Invoke(UnityEngine.Mesh,System.String,UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector4[],UnityEngine.Vector4[])
extern "C" IL2CPP_METHOD_ATTR void BlendShapeKeyCreatedHandle_Invoke_m06C0499D7327E5C34A62BBD62C038153517FFFD5 (BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B * __this, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, String_t* ___name1, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___vertices2, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___normals3, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___tangents4, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___biTangents5, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 6)
				{
					// open
					typedef void (*FunctionPointerType) (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5, targetMethod);
				}
			}
			else if (___parameterCount != 6)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker5< String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(targetMethod, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
							else
								GenericVirtActionInvoker5< String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(targetMethod, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker5< String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
							else
								VirtActionInvoker5< String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker6< Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(targetMethod, targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
							else
								GenericVirtActionInvoker6< Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(targetMethod, targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker6< Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
							else
								VirtActionInvoker6< Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 6)
			{
				// open
				typedef void (*FunctionPointerType) (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5, targetMethod);
			}
		}
		else if (___parameterCount != 6)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker5< String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(targetMethod, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
						else
							GenericVirtActionInvoker5< String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(targetMethod, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker5< String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
						else
							VirtActionInvoker5< String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker6< Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(targetMethod, targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
						else
							GenericVirtActionInvoker6< Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(targetMethod, targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker6< Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
						else
							VirtActionInvoker6< Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, String_t*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___mesh0, ___name1, ___vertices2, ___normals3, ___tangents4, ___biTangents5, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.BlendShapeKeyCreatedHandle::BeginInvoke(UnityEngine.Mesh,System.String,UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector4[],UnityEngine.Vector4[],System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* BlendShapeKeyCreatedHandle_BeginInvoke_m56FDE70C8E20CBF766A60E7D3E0C786324C4AF07 (BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B * __this, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, String_t* ___name1, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___vertices2, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___normals3, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___tangents4, Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___biTangents5, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback6, RuntimeObject * ___object7, const RuntimeMethod* method)
{
	void *__d_args[7] = {0};
	__d_args[0] = ___mesh0;
	__d_args[1] = ___name1;
	__d_args[2] = ___vertices2;
	__d_args[3] = ___normals3;
	__d_args[4] = ___tangents4;
	__d_args[5] = ___biTangents5;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback6, (RuntimeObject*)___object7);
}
// System.Void TriLib.BlendShapeKeyCreatedHandle::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void BlendShapeKeyCreatedHandle_EndInvoke_mF911C32A08737ECFE07739E2DB0B7B15EEDC4263 (BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.CameraData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CameraData__ctor_m8ED87459BED0896F5233A4D8BCA9ECFB8236261A (CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB (DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * __this, intptr_t ___dataPointer0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___dataPointer0);

}
// System.Void TriLib.DataDisposalCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void DataDisposalCallback__ctor_m0E84B470853A661698B6227457277406F85551D1 (DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.DataDisposalCallback::Invoke(System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void DataDisposalCallback_Invoke_m658949AFA4C423B49F12A7B4BD989E5C3E3F37A3 (DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * __this, intptr_t ___dataPointer0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 1)
				{
					// open
					typedef void (*FunctionPointerType) (intptr_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___dataPointer0, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, intptr_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___dataPointer0, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (intptr_t, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___dataPointer0, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< intptr_t >::Invoke(targetMethod, targetThis, ___dataPointer0);
							else
								GenericVirtActionInvoker1< intptr_t >::Invoke(targetMethod, targetThis, ___dataPointer0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< intptr_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___dataPointer0);
							else
								VirtActionInvoker1< intptr_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___dataPointer0);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, intptr_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___dataPointer0, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (intptr_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___dataPointer0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, intptr_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___dataPointer0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (intptr_t, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___dataPointer0, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< intptr_t >::Invoke(targetMethod, targetThis, ___dataPointer0);
						else
							GenericVirtActionInvoker1< intptr_t >::Invoke(targetMethod, targetThis, ___dataPointer0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< intptr_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___dataPointer0);
						else
							VirtActionInvoker1< intptr_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___dataPointer0);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, intptr_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___dataPointer0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.DataDisposalCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* DataDisposalCallback_BeginInvoke_m9AC8587753A08B623740AA0D5278C0225C46A6F2 (DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * __this, intptr_t ___dataPointer0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataDisposalCallback_BeginInvoke_m9AC8587753A08B623740AA0D5278C0225C46A6F2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___dataPointer0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void TriLib.DataDisposalCallback::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void DataDisposalCallback_EndInvoke_m9BFD582C7FE5A22985169A3DC489F5B7AF69CCEE (DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.Dispatcher::CheckInstance()
extern "C" IL2CPP_METHOD_ATTR void Dispatcher_CheckInstance_mB6EDA7B5F82FCE4D44371434F75843BF03F4129E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dispatcher_CheckInstance_mB6EDA7B5F82FCE4D44371434F75843BF03F4129E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		bool L_0 = ((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->get_b_5();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_1, _stringLiteral48F91366C839E056A4DCD440512F99E82188C46A, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_AddComponent_TisDispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_m133548093B1F19680188CF25587863E8A639B5F4(L_1, /*hidden argument*/GameObject_AddComponent_TisDispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_m133548093B1F19680188CF25587863E8A639B5F4_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->set_b_5((bool)1);
	}

IL_001d:
	{
		return;
	}
}
// System.Void TriLib.Dispatcher::InvokeAsync(System.Action)
extern "C" IL2CPP_METHOD_ATTR void Dispatcher_InvokeAsync_mD181ABE170BA0E1427FAA6252A08B75FD997473A (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dispatcher_InvokeAsync_mD181ABE170BA0E1427FAA6252A08B75FD997473A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		bool L_0 = ((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->get_b_5();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral0E82471DC8B063E6A5EBAD5261B8B0220682D513, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		RuntimeObject * L_1 = ((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->get_c_6();
		V_0 = L_1;
		V_1 = (bool)0;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_2, (bool*)(&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * L_3 = ((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->get_d_7();
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_4 = ___action0;
		NullCheck(L_3);
		Queue_1_Enqueue_m7694F6F1EE0F232575659FA4C6AB608B5250ECF5(L_3, L_4, /*hidden argument*/Queue_1_Enqueue_m7694F6F1EE0F232575659FA4C6AB608B5250ECF5_RuntimeMethod_var);
		IL2CPP_LEAVE(0x39, FINALLY_002f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		{
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_0038;
			}
		}

IL_0032:
		{
			RuntimeObject * L_6 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_6, /*hidden argument*/NULL);
		}

IL_0038:
		{
			IL2CPP_END_FINALLY(47)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0039:
	{
		return;
	}
}
// System.Void TriLib.Dispatcher::c()
extern "C" IL2CPP_METHOD_ATTR void Dispatcher_c_m8032A8347EB56C5DA6D320E39F334E8E77F5D379 (Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dispatcher_c_m8032A8347EB56C5DA6D320E39F334E8E77F5D379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * L_0 = ((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->get_a_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(__this, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->set_a_4(__this);
		((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->set_b_5((bool)1);
		return;
	}
}
// System.Void TriLib.Dispatcher::b()
extern "C" IL2CPP_METHOD_ATTR void Dispatcher_b_mC25478C947ACAB9F5C825EB3E2D4CAB1CAF824F0 (Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dispatcher_b_mC25478C947ACAB9F5C825EB3E2D4CAB1CAF824F0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * L_0 = ((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->get_a_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->set_a_4((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 *)NULL);
		((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->set_b_5((bool)0);
	}

IL_0019:
	{
		return;
	}
}
// System.Void TriLib.Dispatcher::a()
extern "C" IL2CPP_METHOD_ATTR void Dispatcher_a_m88CB245848768AC761F98A3F5917C69D6BFBF7AF (Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dispatcher_a_m88CB245848768AC761F98A3F5917C69D6BFBF7AF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->get_c_6();
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			goto IL_0021;
		}

IL_0012:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
			Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * L_2 = ((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->get_d_7();
			NullCheck(L_2);
			Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_3 = Queue_1_Dequeue_mAB1E8E40E4211793A2E12D4592AC98240DD22161(L_2, /*hidden argument*/Queue_1_Dequeue_mAB1E8E40E4211793A2E12D4592AC98240DD22161_RuntimeMethod_var);
			NullCheck(L_3);
			Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_3, /*hidden argument*/NULL);
		}

IL_0021:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
			Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * L_4 = ((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->get_d_7();
			NullCheck(L_4);
			int32_t L_5 = Queue_1_get_Count_mF7EC2631EF60F0F94E4A081B0CA88DF05CD4B09E(L_4, /*hidden argument*/Queue_1_get_Count_mF7EC2631EF60F0F94E4A081B0CA88DF05CD4B09E_RuntimeMethod_var);
			if ((((int32_t)L_5) > ((int32_t)0)))
			{
				goto IL_0012;
			}
		}

IL_002e:
		{
			IL2CPP_LEAVE(0x3A, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		{
			bool L_6 = V_1;
			if (!L_6)
			{
				goto IL_0039;
			}
		}

IL_0033:
		{
			RuntimeObject * L_7 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_7, /*hidden argument*/NULL);
		}

IL_0039:
		{
			IL2CPP_END_FINALLY(48)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3A, IL_003a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003a:
	{
		return;
	}
}
// System.Void TriLib.Dispatcher::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dispatcher__ctor_m9FDB7513020D13B55845F5B1EAC9C5515FB4B5C3 (Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TriLib.Dispatcher::.cctor()
extern "C" IL2CPP_METHOD_ATTR void Dispatcher__cctor_m55952B725107EED38B56DA6FFF871B484400AF3D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Dispatcher__cctor_m55952B725107EED38B56DA6FFF871B484400AF3D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(L_0, /*hidden argument*/NULL);
		((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->set_c_6(L_0);
		Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * L_1 = (Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D *)il2cpp_codegen_object_new(Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D_il2cpp_TypeInfo_var);
		Queue_1__ctor_m0627B54FDADD1BC91409C5E9149CCF2CE4068554(L_1, /*hidden argument*/Queue_1__ctor_m0627B54FDADD1BC91409C5E9149CCF2CE4068554_RuntimeMethod_var);
		((Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields*)il2cpp_codegen_static_fields_for(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var))->set_d_7(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.EmbeddedTextureData::Dispose()
extern "C" IL2CPP_METHOD_ATTR void EmbeddedTextureData_Dispose_m2B52F2B9FAFA0FD4A71BC2968FE303D94C90CF3C (EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EmbeddedTextureData_Dispose_m2B52F2B9FAFA0FD4A71BC2968FE303D94C90CF3C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = __this->get_DataPointer_1();
		bool L_1 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * L_2 = __this->get_OnDataDisposal_3();
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * L_3 = __this->get_OnDataDisposal_3();
		intptr_t L_4 = __this->get_DataPointer_1();
		NullCheck(L_3);
		DataDisposalCallback_Invoke_m658949AFA4C423B49F12A7B4BD989E5C3E3F37A3(L_3, (intptr_t)L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		__this->set_DataPointer_1((intptr_t)(0));
	}

IL_0036:
	{
		return;
	}
}
// System.Void TriLib.EmbeddedTextureData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EmbeddedTextureData__ctor_mDE460DA56375B762910F0A5BF8429F8E6EA6BB42 (EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * __this, const RuntimeMethod* method)
{
	{
		__this->set_NumChannels_6(4);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.EmbeddedTextureLoadCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void EmbeddedTextureLoadCallback__ctor_mB3C9D130E3A0F71F9453E6184961668A0A22D716 (EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TriLib.EmbeddedTextureData TriLib.EmbeddedTextureLoadCallback::Invoke(System.String)
extern "C" IL2CPP_METHOD_ATTR EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * EmbeddedTextureLoadCallback_Invoke_mA002D9780C2759713C33DB7EF3940AFDE5414506 (EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7 * __this, String_t* ___path0, const RuntimeMethod* method)
{
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 1)
				{
					// open
					typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___path0, targetMethod);
				}
				else
				{
					// closed
					typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___path0, targetMethod);
				}
			}
			else if (___parameterCount != 1)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker0< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * >::Invoke(targetMethod, ___path0);
							else
								result = GenericVirtFuncInvoker0< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * >::Invoke(targetMethod, ___path0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker0< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___path0);
							else
								result = VirtFuncInvoker0< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___path0);
						}
					}
				}
				else
				{
					typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___path0, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, const RuntimeMethod*);
							result = ((FunctionPointerType)targetMethodPointer)(___path0, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(targetMethod, targetThis, ___path0);
							else
								result = GenericVirtFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(targetMethod, targetThis, ___path0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___path0);
							else
								result = VirtFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___path0);
						}
					}
				}
				else
				{
					typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___path0, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___path0, targetMethod);
			}
			else
			{
				// closed
				typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___path0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker0< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * >::Invoke(targetMethod, ___path0);
						else
							result = GenericVirtFuncInvoker0< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * >::Invoke(targetMethod, ___path0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker0< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___path0);
						else
							result = VirtFuncInvoker0< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___path0);
					}
				}
			}
			else
			{
				typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___path0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, const RuntimeMethod*);
						result = ((FunctionPointerType)targetMethodPointer)(___path0, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(targetMethod, targetThis, ___path0);
						else
							result = GenericVirtFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(targetMethod, targetThis, ___path0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___path0);
						else
							result = VirtFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___path0);
					}
				}
			}
			else
			{
				typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___path0, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult TriLib.EmbeddedTextureLoadCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* EmbeddedTextureLoadCallback_BeginInvoke_mE5CCDD87691C64BAE1D7477C972AFD7E917CA8C7 (EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7 * __this, String_t* ___path0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___path0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// TriLib.EmbeddedTextureData TriLib.EmbeddedTextureLoadCallback::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * EmbeddedTextureLoadCallback_EndInvoke_m8376ECC8CF181A7BE05452E170155091F1E6BCFD (EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.FileLoadData::Dispose()
extern "C" IL2CPP_METHOD_ATTR void FileLoadData_Dispose_mA7C15B8264DA239722BC8EDF3EAEA38076334CC2 (FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void TriLib.FileLoadData::AddBuffer(System.Runtime.InteropServices.GCHandle)
extern "C" IL2CPP_METHOD_ATTR void FileLoadData_AddBuffer_mE932EDD17D9E40E39230D7C68194BD06925E99F0 (FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13 * __this, GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ___bufferHandle0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void TriLib.FileLoadData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FileLoadData__ctor_m00D7C76BFBF854A54F829100201CD91B73A851B6 (FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TriLib.FileUtils::GetShortFilename(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* FileUtils_GetShortFilename_m07A9A02D60BADB44891F00FC1C70D4AE36E95382 (String_t* ___filename0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileUtils_GetShortFilename_m07A9A02D60BADB44891F00FC1C70D4AE36E95382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___filename0;
		NullCheck(L_0);
		int32_t L_1 = String_LastIndexOf_mC924D20DC71F85A7106D9DD09BF41497C6816E20(L_0, _stringLiteral08534F33C201A45017B502E90A800F1B708EBCB3, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_3 = ___filename0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		String_t* L_5 = String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE(L_3, ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)), /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		String_t* L_6 = ___filename0;
		NullCheck(L_6);
		int32_t L_7 = String_LastIndexOf_mC924D20DC71F85A7106D9DD09BF41497C6816E20(L_6, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) < ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_9 = ___filename0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE(L_9, ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)), /*hidden argument*/NULL);
		return L_11;
	}

IL_0034:
	{
		String_t* L_12 = ___filename0;
		return L_12;
	}
}
// System.String TriLib.FileUtils::GetFileDirectory(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* FileUtils_GetFileDirectory_m312D7CE2F87FDC9E00DB06C78DF142E881968B7F (String_t* ___filename0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileUtils_GetFileDirectory_m312D7CE2F87FDC9E00DB06C78DF142E881968B7F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___filename0;
		NullCheck(L_0);
		int32_t L_1 = String_LastIndexOf_mC924D20DC71F85A7106D9DD09BF41497C6816E20(L_0, _stringLiteral08534F33C201A45017B502E90A800F1B708EBCB3, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_3 = ___filename0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		String_t* L_5 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_3, 0, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		String_t* L_6 = ___filename0;
		NullCheck(L_6);
		int32_t L_7 = String_LastIndexOf_mC924D20DC71F85A7106D9DD09BF41497C6816E20(L_6, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) < ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_9 = ___filename0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_9, 0, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0032:
	{
		return (String_t*)NULL;
	}
}
// System.String TriLib.FileUtils::GetFileExtension(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* FileUtils_GetFileExtension_mAA1AC4E5380E6377EE4A38A5F4EEE9C65BE27C51 (String_t* ___filename0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___filename0;
		NullCheck(L_0);
		int32_t L_1 = String_LastIndexOf_m76C37E3915E802044761572007B8FB0635995F59(L_0, ((int32_t)46), /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000f:
	{
		String_t* L_3 = ___filename0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		String_t* L_5 = String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = String_ToLowerInvariant_m197BD65B6582DC546FF1BC398161EEFA708F799E(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String TriLib.FileUtils::GetFilename(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* FileUtils_GetFilename_m6FDFEEF0E70BFC5CCE4CB8750BA3C4445CCD851F (String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileUtils_GetFilename_m6FDFEEF0E70BFC5CCE4CB8750BA3C4445CCD851F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t0B99A4B924A6FDF08814FFA8DD4CD121ED1A0752_il2cpp_TypeInfo_var);
		String_t* L_1 = Path_GetFileName_m2307E8E0B250632002840D9EC27DBABE9C4EB85E(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___path0;
		String_t* L_3 = V_0;
		bool L_4 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		String_t* L_5 = ___path0;
		NullCheck(L_5);
		int32_t L_6 = String_LastIndexOf_mC924D20DC71F85A7106D9DD09BF41497C6816E20(L_5, _stringLiteral08534F33C201A45017B502E90A800F1B708EBCB3, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) < ((int32_t)0)))
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_8 = ___path0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		String_t* L_10 = String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE(L_8, ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)), /*hidden argument*/NULL);
		return L_10;
	}

IL_002a:
	{
		String_t* L_11 = ___path0;
		NullCheck(L_11);
		int32_t L_12 = String_LastIndexOf_mC924D20DC71F85A7106D9DD09BF41497C6816E20(L_11, _stringLiteral42099B4AF021E53FD8FD4E056C2568D7C2E3FFA8, /*hidden argument*/NULL);
		V_2 = L_12;
		int32_t L_13 = V_2;
		if ((((int32_t)L_13) < ((int32_t)0)))
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_14 = ___path0;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		String_t* L_16 = String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE(L_14, ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1)), /*hidden argument*/NULL);
		return L_16;
	}

IL_0044:
	{
		String_t* L_17 = ___path0;
		return L_17;
	}

IL_0046:
	{
		String_t* L_18 = V_0;
		return L_18;
	}
}
// System.Byte[] TriLib.FileUtils::LoadFileData(System.String)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* FileUtils_LoadFileData_mD283027186E6E7806A9902027C2BB96506AA7572 (String_t* ___filename0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FileUtils_LoadFileData_mD283027186E6E7806A9902027C2BB96506AA7572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___filename0;
			if (L_0)
			{
				goto IL_000c;
			}
		}

IL_0003:
		{
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)0);
			V_0 = L_1;
			goto IL_0028;
		}

IL_000c:
		{
			String_t* L_2 = ___filename0;
			NullCheck(L_2);
			String_t* L_3 = String_Replace_m276641366A463205C185A9B3DC0E24ECB95122C9(L_2, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = File_ReadAllBytes_mF29468CED0B7B3B7C0971ACEBB16A38683718BEC(L_3, /*hidden argument*/NULL);
			V_0 = L_4;
			goto IL_0028;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001e;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.Exception)
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)0);
		V_0 = L_5;
		goto IL_0028;
	} // end catch (depth: 1)

IL_0028:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = V_0;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.GCFileLoadData::Dispose()
extern "C" IL2CPP_METHOD_ATTR void GCFileLoadData_Dispose_mF465C47F2E315F604EB588FCF4899263EF2D953C (GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GCFileLoadData_Dispose_mF465C47F2E315F604EB588FCF4899263EF2D953C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * L_0 = __this->get_LockedBuffers_2();
		NullCheck(L_0);
		Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F  L_1 = List_1_GetEnumerator_m006ABCBEDE7E5C2254B3D5393230AD09BE5D35B5(L_0, /*hidden argument*/List_1_GetEnumerator_m006ABCBEDE7E5C2254B3D5393230AD09BE5D35B5_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001d;
		}

IL_000e:
		{
			GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  L_2 = Enumerator_get_Current_m10833796C91ECE236B89832F9E92B26177E0C5EE((Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F *)(&V_0), /*hidden argument*/Enumerator_get_Current_m10833796C91ECE236B89832F9E92B26177E0C5EE_RuntimeMethod_var);
			V_1 = L_2;
			GCHandle_Free_m392ECC9B1058E35A0FD5CF21A65F212873FC26F0((GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 *)(&V_1), /*hidden argument*/NULL);
		}

IL_001d:
		{
			bool L_3 = Enumerator_MoveNext_mF8DDDD6ADA4D6EE84C9D0827FEDB491318A8DF18((Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mF8DDDD6ADA4D6EE84C9D0827FEDB491318A8DF18_RuntimeMethod_var);
			if (L_3)
			{
				goto IL_000e;
			}
		}

IL_0026:
		{
			IL2CPP_LEAVE(0x36, FINALLY_0028);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mEF3D4BA77CC66D385732EA73C5FBC441B4EADB2D((Enumerator_t21B963B3FB6624FE95B0B4B4DF36F585E45FB16F *)(&V_0), /*hidden argument*/Enumerator_Dispose_mEF3D4BA77CC66D385732EA73C5FBC441B4EADB2D_RuntimeMethod_var);
		IL2CPP_END_FINALLY(40)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0036:
	{
		return;
	}
}
// System.Void TriLib.GCFileLoadData::AddBuffer(System.Runtime.InteropServices.GCHandle)
extern "C" IL2CPP_METHOD_ATTR void GCFileLoadData_AddBuffer_m16D165F4DA2EEF481D1B9E72865688A5587B15EC (GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D * __this, GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ___bufferHandle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GCFileLoadData_AddBuffer_m16D165F4DA2EEF481D1B9E72865688A5587B15EC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * L_0 = __this->get_LockedBuffers_2();
		GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  L_1 = ___bufferHandle0;
		NullCheck(L_0);
		List_1_Add_m4CDF70E93E8FA24EE5A3711F44050893C037D388(L_0, L_1, /*hidden argument*/List_1_Add_m4CDF70E93E8FA24EE5A3711F44050893C037D388_RuntimeMethod_var);
		return;
	}
}
// System.Void TriLib.GCFileLoadData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GCFileLoadData__ctor_m5536FE0D9D5D2543DBA887D0EBCC6391307D3D45 (GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GCFileLoadData__ctor_m5536FE0D9D5D2543DBA887D0EBCC6391307D3D45_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * L_0 = (List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 *)il2cpp_codegen_object_new(List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83_il2cpp_TypeInfo_var);
		List_1__ctor_m067BB9ECCE18366950BE75D550F8C62D6B61EFA7(L_0, /*hidden argument*/List_1__ctor_m067BB9ECCE18366950BE75D550F8C62D6B61EFA7_RuntimeMethod_var);
		__this->set_LockedBuffers_2(L_0);
		FileLoadData__ctor_m00D7C76BFBF854A54F829100201CD91B73A851B6(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.LoadTextureDataCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void LoadTextureDataCallback__ctor_mF32E9496AA2CCEFF49969DABCFBF9E8625240C25 (LoadTextureDataCallback_tE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TriLib.EmbeddedTextureData TriLib.LoadTextureDataCallback::Invoke(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * LoadTextureDataCallback_Invoke_m696EB7DD8BE73D9AA0A9F741FC3180ACEF1C4A45 (LoadTextureDataCallback_tE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3 * __this, String_t* ___path0, String_t* ___basePath1, const RuntimeMethod* method)
{
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 2)
				{
					// open
					typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___path0, ___basePath1, targetMethod);
				}
				else
				{
					// closed
					typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (void*, String_t*, String_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___path0, ___basePath1, targetMethod);
				}
			}
			else if (___parameterCount != 2)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(targetMethod, ___path0, ___basePath1);
							else
								result = GenericVirtFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(targetMethod, ___path0, ___basePath1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___path0, ___basePath1);
							else
								result = VirtFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___path0, ___basePath1);
						}
					}
				}
				else
				{
					typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___path0, ___basePath1, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
							result = ((FunctionPointerType)targetMethodPointer)(___path0, ___basePath1, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = GenericInterfaceFuncInvoker2< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t*, String_t* >::Invoke(targetMethod, targetThis, ___path0, ___basePath1);
							else
								result = GenericVirtFuncInvoker2< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t*, String_t* >::Invoke(targetMethod, targetThis, ___path0, ___basePath1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								result = InterfaceFuncInvoker2< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t*, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___path0, ___basePath1);
							else
								result = VirtFuncInvoker2< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t*, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___path0, ___basePath1);
						}
					}
				}
				else
				{
					typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (void*, String_t*, String_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___path0, ___basePath1, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___path0, ___basePath1, targetMethod);
			}
			else
			{
				// closed
				typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (void*, String_t*, String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___path0, ___basePath1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(targetMethod, ___path0, ___basePath1);
						else
							result = GenericVirtFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(targetMethod, ___path0, ___basePath1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___path0, ___basePath1);
						else
							result = VirtFuncInvoker1< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___path0, ___basePath1);
					}
				}
			}
			else
			{
				typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___path0, ___basePath1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
						result = ((FunctionPointerType)targetMethodPointer)(___path0, ___basePath1, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = GenericInterfaceFuncInvoker2< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t*, String_t* >::Invoke(targetMethod, targetThis, ___path0, ___basePath1);
						else
							result = GenericVirtFuncInvoker2< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t*, String_t* >::Invoke(targetMethod, targetThis, ___path0, ___basePath1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							result = InterfaceFuncInvoker2< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t*, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___path0, ___basePath1);
						else
							result = VirtFuncInvoker2< EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *, String_t*, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___path0, ___basePath1);
					}
				}
			}
			else
			{
				typedef EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * (*FunctionPointerType) (void*, String_t*, String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___path0, ___basePath1, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult TriLib.LoadTextureDataCallback::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* LoadTextureDataCallback_BeginInvoke_mFFFCB3E22B07F88EC6CC095640E63C42F8B8E16B (LoadTextureDataCallback_tE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3 * __this, String_t* ___path0, String_t* ___basePath1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___path0;
	__d_args[1] = ___basePath1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// TriLib.EmbeddedTextureData TriLib.LoadTextureDataCallback::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * LoadTextureDataCallback_EndInvoke_mA94264CB7B9C44908701D3F8B69C0FC2945150E5 (LoadTextureDataCallback_tE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.MaterialCreatedHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void MaterialCreatedHandle__ctor_m378A77D6F565A15C40B6161F64BBF3D6C7B0D6B4 (MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.MaterialCreatedHandle::Invoke(System.UInt32,System.Boolean,UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR void MaterialCreatedHandle_Invoke_mC7993BB28F9D87E7C2CC49EF7B2B792A822C4731 (MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78 * __this, uint32_t ___materialIndex0, bool ___isOverriden1, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material2, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 3)
				{
					// open
					typedef void (*FunctionPointerType) (uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___materialIndex0, ___isOverriden1, ___material2, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___materialIndex0, ___isOverriden1, ___material2, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___materialIndex0, ___isOverriden1, ___material2, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker3< uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * >::Invoke(targetMethod, targetThis, ___materialIndex0, ___isOverriden1, ___material2);
							else
								GenericVirtActionInvoker3< uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * >::Invoke(targetMethod, targetThis, ___materialIndex0, ___isOverriden1, ___material2);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker3< uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___materialIndex0, ___isOverriden1, ___material2);
							else
								VirtActionInvoker3< uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___materialIndex0, ___isOverriden1, ___material2);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___materialIndex0, ___isOverriden1, ___material2, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 3)
			{
				// open
				typedef void (*FunctionPointerType) (uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___materialIndex0, ___isOverriden1, ___material2, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___materialIndex0, ___isOverriden1, ___material2, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___materialIndex0, ___isOverriden1, ___material2, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker3< uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * >::Invoke(targetMethod, targetThis, ___materialIndex0, ___isOverriden1, ___material2);
						else
							GenericVirtActionInvoker3< uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * >::Invoke(targetMethod, targetThis, ___materialIndex0, ___isOverriden1, ___material2);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker3< uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___materialIndex0, ___isOverriden1, ___material2);
						else
							VirtActionInvoker3< uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___materialIndex0, ___isOverriden1, ___material2);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, uint32_t, bool, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___materialIndex0, ___isOverriden1, ___material2, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.MaterialCreatedHandle::BeginInvoke(System.UInt32,System.Boolean,UnityEngine.Material,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MaterialCreatedHandle_BeginInvoke_m717C2033DE0682CCC0CB95417A0725AD4A46A2C1 (MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78 * __this, uint32_t ___materialIndex0, bool ___isOverriden1, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material2, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MaterialCreatedHandle_BeginInvoke_m717C2033DE0682CCC0CB95417A0725AD4A46A2C1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &___materialIndex0);
	__d_args[1] = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &___isOverriden1);
	__d_args[2] = ___material2;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Void TriLib.MaterialCreatedHandle::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void MaterialCreatedHandle_EndInvoke_m0BA71F240E49395A7BFD41ED8FA11F35367DA7B5 (MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.MaterialData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MaterialData__ctor_mCE592592C7D6D04E1EC6D8D971D280DDBB3DCC42 (MaterialData_t27489063F272E033321F0C94EF062695C238CADA * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Quaternion TriLib.MatrixExtensions::ExtractRotation(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  MatrixExtensions_ExtractRotation_m447F990451673541F36962C765EBBE40E0811AA1 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MatrixExtensions_ExtractRotation_m447F990451673541F36962C765EBBE40E0811AA1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_0 = Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___matrix0), 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Vector4_op_Implicit_mEAB05A77FF8B3EE79C31499F0CF0A0D621A6496C(L_0, /*hidden argument*/NULL);
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_2 = Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___matrix0), 1, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector4_op_Implicit_mEAB05A77FF8B3EE79C31499F0CF0A0D621A6496C(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_4 = Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 TriLib.MatrixExtensions::ExtractPosition(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  MatrixExtensions_ExtractPosition_mA131E4F43AE4CB616B92459592831C2A244E8026 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MatrixExtensions_ExtractPosition_mA131E4F43AE4CB616B92459592831C2A244E8026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_0 = Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___matrix0), 3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Vector4_op_Implicit_mEAB05A77FF8B3EE79C31499F0CF0A0D621A6496C(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 TriLib.MatrixExtensions::ExtractScale(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  MatrixExtensions_ExtractScale_mCE27761DAD1E9A346FA55D16E3042B00140042D5 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix0, const RuntimeMethod* method)
{
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_0 = Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___matrix0), 0, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Vector4_get_magnitude_mE33CE76438DDE4DDBBAD94178B07D9364674D356((Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E *)(&V_0), /*hidden argument*/NULL);
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_2 = Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___matrix0), 1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector4_get_magnitude_mE33CE76438DDE4DDBBAD94178B07D9364674D356((Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E *)(&V_0), /*hidden argument*/NULL);
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_4 = Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___matrix0), 2, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = Vector4_get_magnitude_mE33CE76438DDE4DDBBAD94178B07D9364674D356((Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_6), L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.MeshCreatedHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void MeshCreatedHandle__ctor_mDC670EF3DC9F7323CA8F6F660D16E7A7912CC5F1 (MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.MeshCreatedHandle::Invoke(System.UInt32,UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void MeshCreatedHandle_Invoke_m96E2B62E5C95C1B338B50E9D93A5B841C7A54A03 (MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3 * __this, uint32_t ___meshIndex0, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 2)
				{
					// open
					typedef void (*FunctionPointerType) (uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___meshIndex0, ___mesh1, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___meshIndex0, ___mesh1, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___meshIndex0, ___mesh1, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * >::Invoke(targetMethod, targetThis, ___meshIndex0, ___mesh1);
							else
								GenericVirtActionInvoker2< uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * >::Invoke(targetMethod, targetThis, ___meshIndex0, ___mesh1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___meshIndex0, ___mesh1);
							else
								VirtActionInvoker2< uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___meshIndex0, ___mesh1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___meshIndex0, ___mesh1, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___meshIndex0, ___mesh1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___meshIndex0, ___mesh1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___meshIndex0, ___mesh1, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * >::Invoke(targetMethod, targetThis, ___meshIndex0, ___mesh1);
						else
							GenericVirtActionInvoker2< uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * >::Invoke(targetMethod, targetThis, ___meshIndex0, ___mesh1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___meshIndex0, ___mesh1);
						else
							VirtActionInvoker2< uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___meshIndex0, ___mesh1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, uint32_t, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___meshIndex0, ___mesh1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.MeshCreatedHandle::BeginInvoke(System.UInt32,UnityEngine.Mesh,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MeshCreatedHandle_BeginInvoke_m76E416366E306E3AC1193C983B3DD59B7F4C679D (MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3 * __this, uint32_t ___meshIndex0, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MeshCreatedHandle_BeginInvoke_m76E416366E306E3AC1193C983B3DD59B7F4C679D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &___meshIndex0);
	__d_args[1] = ___mesh1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void TriLib.MeshCreatedHandle::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void MeshCreatedHandle_EndInvoke_m8ACBFFB6001D32F16B9913B2ED800BA9F4B6DCF2 (MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.MeshData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MeshData__ctor_m3CA82B9AB92EF59C7820587D61F0EFFB0B8EB92C (MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.MetadataProcessedHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void MetadataProcessedHandle__ctor_mFC7F52698B74FD582D0DC2579163CC431EFEE16C (MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.MetadataProcessedHandle::Invoke(TriLib.AssimpMetadataType,System.UInt32,System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR void MetadataProcessedHandle_Invoke_m99F94CC7B9D5CE7F8AD79072AD1A61BE9900DEAB (MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E * __this, int32_t ___metadataType0, uint32_t ___metadataIndex1, String_t* ___metadataKey2, RuntimeObject * ___metadataValue3, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 4)
				{
					// open
					typedef void (*FunctionPointerType) (int32_t, uint32_t, String_t*, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, int32_t, uint32_t, String_t*, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (int32_t, uint32_t, String_t*, RuntimeObject *, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker4< int32_t, uint32_t, String_t*, RuntimeObject * >::Invoke(targetMethod, targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3);
							else
								GenericVirtActionInvoker4< int32_t, uint32_t, String_t*, RuntimeObject * >::Invoke(targetMethod, targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker4< int32_t, uint32_t, String_t*, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3);
							else
								VirtActionInvoker4< int32_t, uint32_t, String_t*, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, int32_t, uint32_t, String_t*, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 4)
			{
				// open
				typedef void (*FunctionPointerType) (int32_t, uint32_t, String_t*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, int32_t, uint32_t, String_t*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (int32_t, uint32_t, String_t*, RuntimeObject *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker4< int32_t, uint32_t, String_t*, RuntimeObject * >::Invoke(targetMethod, targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3);
						else
							GenericVirtActionInvoker4< int32_t, uint32_t, String_t*, RuntimeObject * >::Invoke(targetMethod, targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker4< int32_t, uint32_t, String_t*, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3);
						else
							VirtActionInvoker4< int32_t, uint32_t, String_t*, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, int32_t, uint32_t, String_t*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___metadataType0, ___metadataIndex1, ___metadataKey2, ___metadataValue3, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.MetadataProcessedHandle::BeginInvoke(TriLib.AssimpMetadataType,System.UInt32,System.String,System.Object,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MetadataProcessedHandle_BeginInvoke_mCF31467123B3A6E9EF53626BF19CA38616C8C7C5 (MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E * __this, int32_t ___metadataType0, uint32_t ___metadataIndex1, String_t* ___metadataKey2, RuntimeObject * ___metadataValue3, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback4, RuntimeObject * ___object5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MetadataProcessedHandle_BeginInvoke_mCF31467123B3A6E9EF53626BF19CA38616C8C7C5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(AssimpMetadataType_tE81F99F30662BCAC75055C486BB4F3F68E7D988B_il2cpp_TypeInfo_var, &___metadataType0);
	__d_args[1] = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &___metadataIndex1);
	__d_args[2] = ___metadataKey2;
	__d_args[3] = ___metadataValue3;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback4, (RuntimeObject*)___object5);
}
// System.Void TriLib.MetadataProcessedHandle::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void MetadataProcessedHandle_EndInvoke_m7F4939543AE6457325CE0771AB345BE88CD52801 (MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.MorphChannelData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MorphChannelData__ctor_m8E609455EE8F6349B2B4772DC526CD4FB11B77E2 (MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.MorphChannelKey::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MorphChannelKey__ctor_m9D6E25EBCC25BD71012A95199E3EF0EA6CAE256C (MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.NodeData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NodeData__ctor_mFC15D5A683CBD5931E627EC6F6C717D3A20C6525 (NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.ObjectLoadedHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void ObjectLoadedHandle__ctor_m1D339D8644D21BD841EB49F47A7F42223962C210 (ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.ObjectLoadedHandle::Invoke(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void ObjectLoadedHandle_Invoke_m3B5AFF2C3DF5BC3BA52E6026995640A32E1E7672 (ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * __this, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___loadedGameObject0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 1)
				{
					// open
					typedef void (*FunctionPointerType) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___loadedGameObject0, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___loadedGameObject0, targetMethod);
				}
			}
			else if (___parameterCount != 1)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, ___loadedGameObject0);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, ___loadedGameObject0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___loadedGameObject0);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___loadedGameObject0);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___loadedGameObject0, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___loadedGameObject0, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * >::Invoke(targetMethod, targetThis, ___loadedGameObject0);
							else
								GenericVirtActionInvoker1< GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * >::Invoke(targetMethod, targetThis, ___loadedGameObject0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___loadedGameObject0);
							else
								VirtActionInvoker1< GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___loadedGameObject0);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___loadedGameObject0, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___loadedGameObject0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___loadedGameObject0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, ___loadedGameObject0);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, ___loadedGameObject0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___loadedGameObject0);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___loadedGameObject0);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___loadedGameObject0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___loadedGameObject0, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * >::Invoke(targetMethod, targetThis, ___loadedGameObject0);
						else
							GenericVirtActionInvoker1< GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * >::Invoke(targetMethod, targetThis, ___loadedGameObject0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___loadedGameObject0);
						else
							VirtActionInvoker1< GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___loadedGameObject0);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___loadedGameObject0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.ObjectLoadedHandle::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* ObjectLoadedHandle_BeginInvoke_m1BA9C6D25377AC84BC4E6E3A17A71FCB4D8106D7 (ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * __this, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___loadedGameObject0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___loadedGameObject0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void TriLib.ObjectLoadedHandle::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void ObjectLoadedHandle_EndInvoke_m41DF78C25BB4D66DD860391EEE882EF6B22053D0 (ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] TriLib.StreamUtils::ReadFullStream(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* StreamUtils_ReadFullStream_m947FCFB0AAEE7849112FCF323913537CE05AACC6 (Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StreamUtils_ReadFullStream_m947FCFB0AAEE7849112FCF323913537CE05AACC6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * V_1 = NULL;
	int32_t V_2 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)4096));
		V_0 = L_0;
		MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * L_1 = (MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C *)il2cpp_codegen_object_new(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m9D0F92C76EFEDA651B678A98EB693FD945286DC2(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001c;
		}

IL_0013:
		{
			MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * L_2 = V_1;
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = V_0;
			int32_t L_4 = V_2;
			NullCheck(L_2);
			VirtActionInvoker3< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(25 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, L_4);
		}

IL_001c:
		{
			Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_5 = ___stream0;
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = V_0;
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = V_0;
			NullCheck(L_7);
			NullCheck(L_5);
			int32_t L_8 = VirtFuncInvoker3< int32_t, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(23 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_5, L_6, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))));
			int32_t L_9 = L_8;
			V_2 = L_9;
			if ((((int32_t)L_9) > ((int32_t)0)))
			{
				goto IL_0013;
			}
		}

IL_002c:
		{
			MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * L_10 = V_1;
			NullCheck(L_10);
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = VirtFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(30 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_10);
			V_3 = L_11;
			IL2CPP_LEAVE(0x3F, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * L_12 = V_1;
			if (!L_12)
			{
				goto IL_003e;
			}
		}

IL_0038:
		{
			MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * L_13 = V_1;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_13);
		}

IL_003e:
		{
			IL2CPP_END_FINALLY(53)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003f:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = V_3;
		return L_14;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TriLib.StringUtils::GenerateUniqueName(System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* StringUtils_GenerateUniqueName_m99805772E1AFE9D8F4C32E48C693E157D1739935 (RuntimeObject * ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringUtils_GenerateUniqueName_m99805772E1AFE9D8F4C32E48C693E157D1739935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		RuntimeObject * L_0 = ___id0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var);
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_2 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
		String_t* L_3 = Int32_ToString_m1D0AF82BDAB5D4710527DD3FEFA6F01246D128A5((int32_t*)(&V_0), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Texture2D TriLib.Texture2DUtils::ProcessTexture(TriLib.EmbeddedTextureData,System.String,System.BooleanU26,System.Boolean,UnityEngine.TextureWrapMode,UnityEngine.FilterMode,TriLib.TextureCompression,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * Texture2DUtils_ProcessTexture_m49A522EEE0E46D58079DD614972076CCC5C9CF21 (EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___embeddedTextureData0, String_t* ___name1, bool* ___hasAlphaChannel2, bool ___isNormalMap3, int32_t ___textureWrapMode4, int32_t ___textureFilterMode5, int32_t ___textureCompression6, bool ___checkAlphaChannel7, bool ___generateMipMaps8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2DUtils_ProcessTexture_m49A522EEE0E46D58079DD614972076CCC5C9CF21_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * V_0 = NULL;
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * V_1 = NULL;
	{
		V_0 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)NULL;
		EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_0 = ___embeddedTextureData0;
		NullCheck(L_0);
		intptr_t L_1 = L_0->get_DataPointer_1();
		bool L_2 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_1, (intptr_t)(0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_003b;
		}
	}
	{
		EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_3 = ___embeddedTextureData0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_DataLength_2();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_5 = ___embeddedTextureData0;
		bool L_6 = Texture2DUtils_a_m0E1656118EAA73C07AD9C6854F037E83131767B1(L_5, (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C **)(&V_1), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_7 = V_1;
		String_t* L_8 = ___name1;
		bool* L_9 = ___hasAlphaChannel2;
		int32_t L_10 = ___textureWrapMode4;
		int32_t L_11 = ___textureFilterMode5;
		int32_t L_12 = ___textureCompression6;
		bool L_13 = ___isNormalMap3;
		bool L_14 = ___checkAlphaChannel7;
		bool L_15 = ___generateMipMaps8;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_16 = Texture2DUtils_a_mAFB4665E853FF838B8ED8836C67C43459EBC7DE0(L_7, L_8, (bool*)L_9, L_10, L_11, L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_003b:
	{
		EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_17 = ___embeddedTextureData0;
		NullCheck(L_17);
		EmbeddedTextureData_Dispose_m2B52F2B9FAFA0FD4A71BC2968FE303D94C90CF3C(L_17, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_18 = V_0;
		return L_18;
	}
}
// System.Boolean TriLib.Texture2DUtils::a(TriLib.EmbeddedTextureData,UnityEngine.Texture2DU26)
extern "C" IL2CPP_METHOD_ATTR bool Texture2DUtils_a_m0E1656118EAA73C07AD9C6854F037E83131767B1 (EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___A_00, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** ___A_11, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2DUtils_a_m0E1656118EAA73C07AD9C6854F037E83131767B1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_0 = ___A_00;
		NullCheck(L_0);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = L_0->get_Data_0();
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_2 = ___A_00;
		NullCheck(L_2);
		intptr_t L_3 = L_2->get_DataPointer_1();
		bool L_4 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_3, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** L_5 = ___A_11;
		*((RuntimeObject **)L_5) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_5, (RuntimeObject *)NULL);
		return (bool)0;
	}

IL_001f:
	{
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** L_6 = ___A_11;
			EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_7 = ___A_00;
			NullCheck(L_7);
			int32_t L_8 = L_7->get_Width_4();
			EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_9 = ___A_00;
			NullCheck(L_9);
			int32_t L_10 = L_9->get_Height_5();
			Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_11 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
			Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_11, L_8, L_10, 4, (bool)0, /*hidden argument*/NULL);
			*((RuntimeObject **)L_6) = (RuntimeObject *)L_11;
			Il2CppCodeGenWriteBarrier((RuntimeObject **)L_6, (RuntimeObject *)L_11);
			EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_12 = ___A_00;
			NullCheck(L_12);
			intptr_t L_13 = L_12->get_DataPointer_1();
			bool L_14 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_13, (intptr_t)(0), /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_005c;
			}
		}

IL_0047:
		{
			Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** L_15 = ___A_11;
			Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_16 = *((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C **)L_15);
			EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_17 = ___A_00;
			NullCheck(L_17);
			intptr_t L_18 = L_17->get_DataPointer_1();
			EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_19 = ___A_00;
			NullCheck(L_19);
			int32_t L_20 = L_19->get_DataLength_2();
			NullCheck(L_16);
			Texture2D_LoadRawTextureData_m7F70DFFDBFA1E307A79838B94BC1A0438E063DFC(L_16, (intptr_t)L_18, L_20, /*hidden argument*/NULL);
			goto IL_0069;
		}

IL_005c:
		{
			Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** L_21 = ___A_11;
			Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_22 = *((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C **)L_21);
			EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * L_23 = ___A_00;
			NullCheck(L_23);
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = L_23->get_Data_0();
			NullCheck(L_22);
			Texture2D_LoadRawTextureData_m0331275D060EC3521BCCF84194FA35BBEEF767CB(L_22, L_24, /*hidden argument*/NULL);
		}

IL_0069:
		{
			Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** L_25 = ___A_11;
			Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_26 = *((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C **)L_25);
			NullCheck(L_26);
			Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA(L_26, /*hidden argument*/NULL);
			V_0 = (bool)1;
			goto IL_007c;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0074;
		throw e;
	}

CATCH_0074:
	{ // begin catch(System.Object)
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** L_27 = ___A_11;
		*((RuntimeObject **)L_27) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_27, (RuntimeObject *)NULL);
		V_0 = (bool)0;
		goto IL_007c;
	} // end catch (depth: 1)

IL_007c:
	{
		bool L_28 = V_0;
		return L_28;
	}
}
// UnityEngine.Texture2D TriLib.Texture2DUtils::a(UnityEngine.Texture2D,System.String,System.BooleanU26,UnityEngine.TextureWrapMode,UnityEngine.FilterMode,TriLib.TextureCompression,System.Boolean,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * Texture2DUtils_a_mAFB4665E853FF838B8ED8836C67C43459EBC7DE0 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___A_00, String_t* ___A_11, bool* ___A_22, int32_t ___A_33, int32_t ___A_44, int32_t ___A_55, bool ___A_66, bool ___A_77, bool ___A_88, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2DUtils_a_mAFB4665E853FF838B8ED8836C67C43459EBC7DE0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* V_0 = NULL;
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * V_1 = NULL;
	int32_t V_2 = 0;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  V_3;
	memset(&V_3, 0, sizeof(V_3));
	uint8_t V_4 = 0x0;
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* V_5 = NULL;
	int32_t V_6 = 0;
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * G_B12_0 = NULL;
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * G_B11_0 = NULL;
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * G_B13_0 = NULL;
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_0 = ___A_00;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000b;
		}
	}
	{
		return (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)NULL;
	}

IL_000b:
	{
		String_t* L_2 = ___A_11;
		bool L_3 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_4 = ___A_00;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_5 = ___A_00;
		String_t* L_6 = StringUtils_GenerateUniqueName_m99805772E1AFE9D8F4C32E48C693E157D1739935(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_4, L_6, /*hidden argument*/NULL);
	}

IL_001f:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_7 = ___A_00;
		String_t* L_8 = ___A_11;
		NullCheck(L_7);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_7, L_8, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_9 = ___A_00;
		int32_t L_10 = ___A_33;
		NullCheck(L_9);
		Texture_set_wrapMode_m85E9A995D5947B59FE13A7311E891F3DEDEBBCEC(L_9, L_10, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_11 = ___A_00;
		int32_t L_12 = ___A_44;
		NullCheck(L_11);
		Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01(L_11, L_12, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_13 = ___A_00;
		NullCheck(L_13);
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_14 = Texture2D_GetPixels32_m7CC6EC6AD48D4CD84AF28DFDFBE24750900FA2E6(L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		bool L_15 = ___A_66;
		if (!L_15)
		{
			goto IL_00d0;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_16 = ___A_00;
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_16);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_18 = ___A_00;
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_18);
		bool L_20 = ___A_88;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_21 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_21, L_17, L_19, 4, L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_22 = V_1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_23 = ___A_00;
		NullCheck(L_23);
		String_t* L_24 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_22, L_24, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_25 = V_1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_26 = ___A_00;
		NullCheck(L_26);
		int32_t L_27 = Texture_get_wrapMode_mC21054C7BC6E958937B7459DAF1D17654284B07A(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Texture_set_wrapMode_m85E9A995D5947B59FE13A7311E891F3DEDEBBCEC(L_25, L_27, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_28 = V_1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_29 = ___A_00;
		NullCheck(L_29);
		int32_t L_30 = Texture_get_filterMode_m4A05E0414655866D27811376709B3A8C584A2579(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01(L_28, L_30, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_00b2;
	}

IL_0080:
	{
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_31 = V_0;
		int32_t L_32 = V_2;
		NullCheck(L_31);
		int32_t L_33 = L_32;
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		V_3 = L_34;
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_35 = V_3;
		uint8_t L_36 = L_35.get_r_1();
		V_4 = L_36;
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_37 = V_3;
		uint8_t L_38 = L_37.get_a_4();
		(&V_3)->set_r_1(L_38);
		uint8_t L_39 = V_4;
		(&V_3)->set_a_4(L_39);
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_40 = V_0;
		int32_t L_41 = V_2;
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_42 = V_3;
		NullCheck(L_40);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(L_41), (Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 )L_42);
		int32_t L_43 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_43, (int32_t)1));
	}

IL_00b2:
	{
		int32_t L_44 = V_2;
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_45 = V_0;
		NullCheck(L_45);
		if ((((int32_t)L_44) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_45)->max_length)))))))
		{
			goto IL_0080;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_46 = V_1;
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_47 = V_0;
		NullCheck(L_46);
		Texture2D_SetPixels32_m8669AE290D19897A70859BE23D9A438EB7EDA67E(L_46, L_47, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_48 = V_1;
		bool L_49 = ___A_88;
		NullCheck(L_48);
		Texture2D_Apply_m368893ECE2F9659BDA54ED1E4EB00D01CC2D1B16(L_48, L_49, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_50 = ___A_00;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_50, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_51 = V_1;
		___A_00 = L_51;
	}

IL_00d0:
	{
		bool L_52 = ___A_66;
		bool L_53 = ___A_88;
		if (!((int32_t)((int32_t)((((int32_t)L_52) == ((int32_t)0))? 1 : 0)&(int32_t)L_53)))
		{
			goto IL_0136;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_54 = ___A_00;
		NullCheck(L_54);
		int32_t L_55 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_54);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_56 = ___A_00;
		NullCheck(L_56);
		int32_t L_57 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_56);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_58 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_58, L_55, L_57, 4, (bool)1, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_59 = L_58;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_60 = ___A_00;
		NullCheck(L_60);
		String_t* L_61 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_60, /*hidden argument*/NULL);
		NullCheck(L_59);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_59, L_61, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_62 = L_59;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_63 = ___A_00;
		NullCheck(L_63);
		int32_t L_64 = Texture_get_wrapMode_mC21054C7BC6E958937B7459DAF1D17654284B07A(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		Texture_set_wrapMode_m85E9A995D5947B59FE13A7311E891F3DEDEBBCEC(L_62, L_64, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_65 = L_62;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_66 = ___A_00;
		NullCheck(L_66);
		int32_t L_67 = Texture_get_filterMode_m4A05E0414655866D27811376709B3A8C584A2579(L_66, /*hidden argument*/NULL);
		NullCheck(L_65);
		Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01(L_65, L_67, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_68 = L_65;
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_69 = V_0;
		NullCheck(L_68);
		Texture2D_SetPixels32_m8669AE290D19897A70859BE23D9A438EB7EDA67E(L_68, L_69, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_70 = L_68;
		NullCheck(L_70);
		Texture2D_Apply_m368893ECE2F9659BDA54ED1E4EB00D01CC2D1B16(L_70, (bool)1, /*hidden argument*/NULL);
		bool L_71 = Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5(/*hidden argument*/NULL);
		G_B11_0 = L_70;
		if (!L_71)
		{
			G_B12_0 = L_70;
			goto IL_012e;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_72 = ___A_00;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_72, /*hidden argument*/NULL);
		G_B13_0 = G_B11_0;
		goto IL_0134;
	}

IL_012e:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_73 = ___A_00;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_73, /*hidden argument*/NULL);
		G_B13_0 = G_B12_0;
	}

IL_0134:
	{
		___A_00 = G_B13_0;
	}

IL_0136:
	{
		int32_t L_74 = ___A_55;
		if (!L_74)
		{
			goto IL_0145;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_75 = ___A_00;
		int32_t L_76 = ___A_55;
		NullCheck(L_75);
		Texture2D_Compress_m3D0191F151DF6D66F312D35FF76BDDE92A3ED395(L_75, (bool)((((int32_t)L_76) == ((int32_t)2))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0145:
	{
		bool L_77 = ___A_77;
		if (!L_77)
		{
			goto IL_017c;
		}
	}
	{
		bool* L_78 = ___A_22;
		*((int8_t*)L_78) = (int8_t)0;
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_79 = V_0;
		V_5 = L_79;
		V_6 = 0;
		goto IL_0174;
	}

IL_0154:
	{
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_80 = V_5;
		int32_t L_81 = V_6;
		NullCheck(L_80);
		int32_t L_82 = L_81;
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		uint8_t L_84 = L_83.get_a_4();
		if ((((int32_t)L_84) == ((int32_t)((int32_t)255))))
		{
			goto IL_016e;
		}
	}
	{
		bool* L_85 = ___A_22;
		*((int8_t*)L_85) = (int8_t)1;
		goto IL_017c;
	}

IL_016e:
	{
		int32_t L_86 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_86, (int32_t)1));
	}

IL_0174:
	{
		int32_t L_87 = V_6;
		Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* L_88 = V_5;
		NullCheck(L_88);
		if ((((int32_t)L_87) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_88)->max_length)))))))
		{
			goto IL_0154;
		}
	}

IL_017c:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_89 = ___A_00;
		return L_89;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.TextureLoadHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TextureLoadHandle__ctor_m43F50847857755E35B481590E5DDB83861CC485D (TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.TextureLoadHandle::Invoke(System.String,UnityEngine.Material,System.String,UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR void TextureLoadHandle_Invoke_m107FBAC3631871EC60E05895C3E65D0A72452BDF (TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D * __this, String_t* ___sourcePath0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material1, String_t* ___propertyName2, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture3, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 4)
				{
					// open
					typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
				}
			}
			else if (___parameterCount != 4)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
							else
								GenericVirtActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___sourcePath0, ___material1, ___propertyName2, ___texture3);
							else
								VirtActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
							else
								GenericVirtActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
							else
								VirtActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 4)
			{
				// open
				typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
		}
		else if (___parameterCount != 4)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						else
							GenericVirtActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						else
							VirtActionInvoker3< Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						else
							GenericVirtActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(targetMethod, targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
						else
							VirtActionInvoker4< String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___sourcePath0, ___material1, ___propertyName2, ___texture3, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.TextureLoadHandle::BeginInvoke(System.String,UnityEngine.Material,System.String,UnityEngine.Texture2D,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TextureLoadHandle_BeginInvoke_mB9AF24AF63FDD141B13DF08E2F2FA55FCA10F415 (TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D * __this, String_t* ___sourcePath0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material1, String_t* ___propertyName2, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture3, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback4, RuntimeObject * ___object5, const RuntimeMethod* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___sourcePath0;
	__d_args[1] = ___material1;
	__d_args[2] = ___propertyName2;
	__d_args[3] = ___texture3;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback4, (RuntimeObject*)___object5);
}
// System.Void TriLib.TextureLoadHandle::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void TextureLoadHandle_EndInvoke_m1CCDE8BCD7D72603105A2BF32D4F59A3472C5AC5 (TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.TexturePreLoadHandle::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TexturePreLoadHandle__ctor_m157C5E8DB552E342A318814A9379DD25A166CDE0 (TexturePreLoadHandle_tC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TriLib.TexturePreLoadHandle::Invoke(System.IntPtr,System.String,System.String,UnityEngine.Material,System.String,System.BooleanU26,UnityEngine.TextureWrapMode,System.String,TriLib.TextureLoadHandle,TriLib.TextureCompression,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TexturePreLoadHandle_Invoke_m1DDDB92E2104D002F92920991D956C876417A6DB (TexturePreLoadHandle_tC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23 * __this, intptr_t ___scene0, String_t* ___path1, String_t* ___name2, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material3, String_t* ___propertyName4, bool* ___checkAlphaChannel5, int32_t ___textureWrapMode6, String_t* ___basePath7, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D * ___onTextureLoaded8, int32_t ___textureCompression9, bool ___isNormalMap10, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 11)
				{
					// open
					typedef void (*FunctionPointerType) (intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool >::Invoke(targetMethod, targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
							else
								GenericVirtActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool >::Invoke(targetMethod, targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
							else
								VirtActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 11)
			{
				// open
				typedef void (*FunctionPointerType) (intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool >::Invoke(targetMethod, targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
						else
							GenericVirtActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool >::Invoke(targetMethod, targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
						else
							VirtActionInvoker11< intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, intptr_t, String_t*, String_t*, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *, String_t*, bool*, int32_t, String_t*, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D *, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___scene0, ___path1, ___name2, ___material3, ___propertyName4, ___checkAlphaChannel5, ___textureWrapMode6, ___basePath7, ___onTextureLoaded8, ___textureCompression9, ___isNormalMap10, targetMethod);
			}
		}
	}
}
// System.IAsyncResult TriLib.TexturePreLoadHandle::BeginInvoke(System.IntPtr,System.String,System.String,UnityEngine.Material,System.String,System.BooleanU26,UnityEngine.TextureWrapMode,System.String,TriLib.TextureLoadHandle,TriLib.TextureCompression,System.Boolean,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TexturePreLoadHandle_BeginInvoke_mBC2C9551EA67D19142FD94A4CA512DAF1A753F94 (TexturePreLoadHandle_tC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23 * __this, intptr_t ___scene0, String_t* ___path1, String_t* ___name2, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material3, String_t* ___propertyName4, bool* ___checkAlphaChannel5, int32_t ___textureWrapMode6, String_t* ___basePath7, TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D * ___onTextureLoaded8, int32_t ___textureCompression9, bool ___isNormalMap10, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback11, RuntimeObject * ___object12, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TexturePreLoadHandle_BeginInvoke_mBC2C9551EA67D19142FD94A4CA512DAF1A753F94_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[12] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___scene0);
	__d_args[1] = ___path1;
	__d_args[2] = ___name2;
	__d_args[3] = ___material3;
	__d_args[4] = ___propertyName4;
	__d_args[5] = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &*___checkAlphaChannel5);
	__d_args[6] = Box(TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F_il2cpp_TypeInfo_var, &___textureWrapMode6);
	__d_args[7] = ___basePath7;
	__d_args[8] = ___onTextureLoaded8;
	__d_args[9] = Box(TextureCompression_t25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C_il2cpp_TypeInfo_var, &___textureCompression9);
	__d_args[10] = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &___isNormalMap10);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback11, (RuntimeObject*)___object12);
}
// System.Void TriLib.TexturePreLoadHandle::EndInvoke(System.BooleanU26,System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void TexturePreLoadHandle_EndInvoke_m81705B3D15ECB2A362596B28AFE4D2925E654ACB (TexturePreLoadHandle_tC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23 * __this, bool* ___checkAlphaChannel0, RuntimeObject* ___result1, const RuntimeMethod* method)
{
	void* ___out_args[] = {
	___checkAlphaChannel0,
	};
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result1, ___out_args);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Threading.Thread TriLib.ThreadUtils::RunThread(System.Action,System.Action)
extern "C" IL2CPP_METHOD_ATTR Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ThreadUtils_RunThread_m4924878B8E72A7CA9C5C72E04A020452EB6C1629 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action0, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onComplete1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadUtils_RunThread_m4924878B8E72A7CA9C5C72E04A020452EB6C1629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C * L_0 = (a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C *)il2cpp_codegen_object_new(a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C_il2cpp_TypeInfo_var);
		a__ctor_m5469D1F788AEC8E9C66EEF1E122BDECAEF607054(L_0, /*hidden argument*/NULL);
		a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C * L_1 = L_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_a_0(L_2);
		a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C * L_3 = L_1;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_4 = ___onComplete1;
		NullCheck(L_3);
		L_3->set_b_1(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		Dispatcher_CheckInstance_mB6EDA7B5F82FCE4D44371434F75843BF03F4129E(/*hidden argument*/NULL);
		ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF * L_5 = (ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF *)il2cpp_codegen_object_new(ThreadStart_t09FFA4371E4B2A713F212B157CC9B8B61983B5BF_il2cpp_TypeInfo_var);
		ThreadStart__ctor_m692348FEAEBAF381D62984EE95B217CC024A77D5(L_5, L_3, (intptr_t)((intptr_t)a_c_m86F446BF97B3C113908FC32FF5D261637C66B5BB_RuntimeMethod_var), /*hidden argument*/NULL);
		Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * L_6 = (Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 *)il2cpp_codegen_object_new(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7_il2cpp_TypeInfo_var);
		Thread__ctor_m36A33B990160C4499E991D288341CA325AE66DDD(L_6, L_5, /*hidden argument*/NULL);
		Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * L_7 = L_6;
		NullCheck(L_7);
		Thread_Start_mE2AC4744AFD147FDC84E8D9317B4E3AB890BC2D6(L_7, /*hidden argument*/NULL);
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.ThreadUtils_a::.ctor()
extern "C" IL2CPP_METHOD_ATTR void a__ctor_m5469D1F788AEC8E9C66EEF1E122BDECAEF607054 (a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TriLib.ThreadUtils_a::c()
extern "C" IL2CPP_METHOD_ATTR void a_c_m86F446BF97B3C113908FC32FF5D261637C66B5BB (a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (a_c_m86F446BF97B3C113908FC32FF5D261637C66B5BB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	b_tBB4B7EAE3F249876C28028467342545B6E691DD5 * V_0 = NULL;
	Exception_t * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_0 = __this->get_a_0();
		NullCheck(L_0);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_0, /*hidden argument*/NULL);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_1 = __this->get_b_1();
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		Dispatcher_InvokeAsync_mD181ABE170BA0E1427FAA6252A08B75FD997473A(L_1, /*hidden argument*/NULL);
		goto IL_0039;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0018;
		throw e;
	}

CATCH_0018:
	{ // begin catch(System.Exception)
		b_tBB4B7EAE3F249876C28028467342545B6E691DD5 * L_2 = (b_tBB4B7EAE3F249876C28028467342545B6E691DD5 *)il2cpp_codegen_object_new(b_tBB4B7EAE3F249876C28028467342545B6E691DD5_il2cpp_TypeInfo_var);
		b__ctor_m1C4DB70256844B56744B7B6C960099C82B85BC2B(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = ((Exception_t *)__exception_local);
		b_tBB4B7EAE3F249876C28028467342545B6E691DD5 * L_3 = V_0;
		Exception_t * L_4 = V_1;
		NullCheck(L_3);
		L_3->set_a_0(L_4);
		b_tBB4B7EAE3F249876C28028467342545B6E691DD5 * L_5 = V_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_6 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
		Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_6, L_5, (intptr_t)((intptr_t)b_b_mB1A0EA2F3DA09A7F4ACEF3D2D29904C00EED570D_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_il2cpp_TypeInfo_var);
		Dispatcher_InvokeAsync_mD181ABE170BA0E1427FAA6252A08B75FD997473A(L_6, /*hidden argument*/NULL);
		goto IL_0039;
	} // end catch (depth: 1)

IL_0039:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.ThreadUtils_b::.ctor()
extern "C" IL2CPP_METHOD_ATTR void b__ctor_m1C4DB70256844B56744B7B6C960099C82B85BC2B (b_tBB4B7EAE3F249876C28028467342545B6E691DD5 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TriLib.ThreadUtils_b::b()
extern "C" IL2CPP_METHOD_ATTR void b_b_mB1A0EA2F3DA09A7F4ACEF3D2D29904C00EED570D (b_tBB4B7EAE3F249876C28028467342545B6E691DD5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (b_b_mB1A0EA2F3DA09A7F4ACEF3D2D29904C00EED570D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t * L_0 = __this->get_a_0();
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, b_b_mB1A0EA2F3DA09A7F4ACEF3D2D29904C00EED570D_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.TransformExtensions::LoadMatrix(UnityEngine.Transform,UnityEngine.Matrix4x4,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TransformExtensions_LoadMatrix_m48C72A359D77AF8A3A6F657440DD475C7FC18511 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix1, bool ___local2, const RuntimeMethod* method)
{
	{
		bool L_0 = ___local2;
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = ___transform0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_2 = ___matrix1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = MatrixExtensions_ExtractScale_mCE27761DAD1E9A346FA55D16E3042B00140042D5(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_1, L_3, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = ___transform0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_5 = ___matrix1;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_6 = MatrixExtensions_ExtractRotation_m447F990451673541F36962C765EBBE40E0811AA1(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_4, L_6, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = ___transform0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_8 = ___matrix1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = MatrixExtensions_ExtractPosition_mA131E4F43AE4CB616B92459592831C2A244E8026(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_7, L_9, /*hidden argument*/NULL);
		return;
	}

IL_0028:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = ___transform0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_11 = ___matrix1;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_12 = MatrixExtensions_ExtractRotation_m447F990451673541F36962C765EBBE40E0811AA1(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_10, L_12, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = ___transform0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_14 = ___matrix1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = MatrixExtensions_ExtractPosition_mA131E4F43AE4CB616B92459592831C2A244E8026(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform TriLib.TransformExtensions::FindDeepChild(UnityEngine.Transform,System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * TransformExtensions_FindDeepChild_mB87B6DDFE9FBD94736262E668DC9EE0CDFD7459D (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform0, String_t* ___name1, bool ___endsWith2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformExtensions_FindDeepChild_mB87B6DDFE9FBD94736262E668DC9EE0CDFD7459D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_1 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	bool G_B3_0 = false;
	{
		bool L_0 = ___endsWith2;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = ___transform0;
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___name1;
		NullCheck(L_2);
		bool L_4 = String_EndsWith_mE4F039DCC2A9FCB8C1ED2D04B00A35E3CE16DE99(L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001d;
	}

IL_0011:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = ___transform0;
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_5, /*hidden argument*/NULL);
		String_t* L_7 = ___name1;
		bool L_8 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_6, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_001d:
	{
		if (!G_B3_0)
		{
			goto IL_0021;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = ___transform0;
		return L_9;
	}

IL_0021:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = ___transform0;
		NullCheck(L_10);
		RuntimeObject* L_11 = Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004a;
		}

IL_002a:
		{
			RuntimeObject* L_12 = V_0;
			NullCheck(L_12);
			RuntimeObject * L_13 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_12);
			String_t* L_14 = ___name1;
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = TransformExtensions_FindDeepChild_mB87B6DDFE9FBD94736262E668DC9EE0CDFD7459D(((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)CastclassClass((RuntimeObject*)L_13, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var)), L_14, (bool)0, /*hidden argument*/NULL);
			V_1 = L_15;
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			bool L_17 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_16, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_004a;
			}
		}

IL_0046:
		{
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = V_1;
			V_2 = L_18;
			IL2CPP_LEAVE(0x67, FINALLY_0054);
		}

IL_004a:
		{
			RuntimeObject* L_19 = V_0;
			NullCheck(L_19);
			bool L_20 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_19);
			if (L_20)
			{
				goto IL_002a;
			}
		}

IL_0052:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0054);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0054;
	}

FINALLY_0054:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_21 = V_0;
			V_3 = ((RuntimeObject*)IsInst((RuntimeObject*)L_21, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
			RuntimeObject* L_22 = V_3;
			if (!L_22)
			{
				goto IL_0064;
			}
		}

IL_005e:
		{
			RuntimeObject* L_23 = V_3;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_23);
		}

IL_0064:
		{
			IL2CPP_END_FINALLY(84)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(84)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0065:
	{
		return (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)NULL;
	}

IL_0067:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = V_2;
		return L_24;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLib.ZipGCFileLoadData::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ZipGCFileLoadData__ctor_m7718D93768C1469C36D5AB8FBFADC77CC481DACD (ZipGCFileLoadData_tBAFBC6955F8E973FA87C0DDFD15968EC80491E53 * __this, const RuntimeMethod* method)
{
	{
		GCFileLoadData__ctor_m5536FE0D9D5D2543DBA887D0EBCC6391307D3D45(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32 a::a(System.String)
extern "C" IL2CPP_METHOD_ATTR uint32_t a_a_m73A55868A3468E7F389B5D7AF8E429EB57A48DF9 (String_t* ___A_00, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___A_00;
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		V_0 = ((int32_t)-2128831035);
		V_1 = 0;
		goto IL_0021;
	}

IL_000d:
	{
		String_t* L_1 = ___A_00;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Il2CppChar L_3 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)((int32_t)L_3^(int32_t)L_4)), (int32_t)((int32_t)16777619)));
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_6 = V_1;
		String_t* L_7 = ___A_00;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_000d;
		}
	}

IL_002a:
	{
		uint32_t L_9 = V_0;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
