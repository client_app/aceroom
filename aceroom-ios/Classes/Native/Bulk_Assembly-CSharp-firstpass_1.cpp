﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.String,VertexAnimationTools_30.TasksStack/Task>[]
struct EntryU5BU5D_t41B725507AF4EDB7C42FAEEA3CD988A72EA8CCCA;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,VertexAnimationTools_30.TasksStack/Task>
struct KeyCollection_tF415CB4C8FB0CDC3FB0933A3C61E98126D14444D;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VertexAnimationTools_30.TasksStack/Task>
struct ValueCollection_tD1FFED9FB2EC2E92FEBCD5CA8D869FF45281FA9C;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA;
// System.Collections.Generic.Dictionary`2<System.String,VertexAnimationTools_30.TasksStack/Task>
struct Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<VertexAnimationTools_30.MaterialInfo>
struct List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA;
// System.Collections.Generic.List`1<VertexAnimationTools_30.TasksStack/Task>
struct List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshCollider
struct MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// VertexAnimationTools_30.MaterialInfo
struct MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0;
// VertexAnimationTools_30.MaterialInfo[]
struct MaterialInfoU5BU5D_t94DC06E688DE56C79F735F3FA0144F088D82F80D;
// VertexAnimationTools_30.PointCache
struct PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7;
// VertexAnimationTools_30.PointCachePlayer
struct PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F;
// VertexAnimationTools_30.PointCachePlayer/Clip
struct Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77;
// VertexAnimationTools_30.PointCachePlayer/Clip[]
struct ClipU5BU5D_tAA79AB3C6F96CA78E7D4DE8DBB9F3D785C413603;
// VertexAnimationTools_30.PointCachePlayer/Constraint
struct Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1;
// VertexAnimationTools_30.PointCachePlayer/Constraint[]
struct ConstraintU5BU5D_tA24CD0C95C240251A402CA5801AEEFBA2A56756A;
// VertexAnimationTools_30.ProjectionSamples
struct ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708;
// VertexAnimationTools_30.ProjectionSamples/Samples
struct Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08;
// VertexAnimationTools_30.ProjectionSamples/Samples[]
struct SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44;
// VertexAnimationTools_30.TasksStack
struct TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F;
// VertexAnimationTools_30.TasksStack/Task
struct Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4;
// VertexAnimationTools_30.TasksStack/Task[]
struct TaskU5BU5D_tBB5F48935BC3977997D53AB7BD37AD4226CF6269;
// VertexAnimationTools_30.VertexAnimationToolsAssetBase
struct VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252;

extern RuntimeClass* Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA_il2cpp_TypeInfo_var;
extern RuntimeClass* MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0_il2cpp_TypeInfo_var;
extern RuntimeClass* MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
extern RuntimeClass* Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2A7BC94A06F3221293677515044B0A9DD3960F4E;
extern String_t* _stringLiteral7E7DA0C90224CF487241A587C1FC53AF063CA8AA;
extern String_t* _stringLiteralCFFCEF8C0B9CC5209BA1949CFB2B9D52D8E67F6E;
extern String_t* _stringLiteralF76CF85AB6A67B58748F6C3DDDA4898D6687E9FF;
extern const RuntimeMethod* Dictionary_2_Add_mA7F5F5DAFC9B5AEF2107CB1B9809F1A812BF4D92_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m831F11AC81E0F04B86DB2A62B1CDD21CB47B2FE0_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m59A3A2601326E56C3EE4E802E8F0E9CA09A0C299_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_mB37BDDEB7C29FEC675826E3C374316BFBBB480C9_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mA28790AE5AFF26DBF13EB5B4E1B0B7954E1D3ED1_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mFD28069FE280F9BC204CD8EAB6B2938CB1FC7FA9_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m5768F6A9F78F566E53AB5ECAAF5BCDB414C3B6B3_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m6F104AD5D8FB6EFF1B7453916727EBB430214437_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m75AFAA3C830D844D785A019FE6D4E3A71344B7BD_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m9B7BF65FC57179097215D9CE925549BD2E722E67_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268_RuntimeMethod_var;
extern const RuntimeMethod* Resources_Load_TisProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708_m2A2EAEEE2C850C84F94448E91F0164E0D48AEBEB_RuntimeMethod_var;
extern const uint32_t Constraint_Apply_m96292A52D04762BB78C857D75189C4FB61BB7A29_MetadataUsageId;
extern const uint32_t ProjectionSamples_get_Get_m97A838CD8497590553505AF036C890C1CF47B013_MetadataUsageId;
extern const uint32_t Task_GetInfo_m23A2566AA31B9C73E88294BF26F8C643F6830CDC_MetadataUsageId;
extern const uint32_t Task_GetInfo_m9CABACF1E820BDB7746FCD6B1EC092711791B097_MetadataUsageId;
extern const uint32_t TasksStack_Add_mBFCE89C6026C5FE8F2C52924F26C54914F4988C4_MetadataUsageId;
extern const uint32_t TasksStack_Add_mDB3AF7F713CCA6D7D9B83957F3A70160B5A5590F_MetadataUsageId;
extern const uint32_t TasksStack_Normalize_m86E6117ABBCC03C07B5015447A99C71B9857E127_MetadataUsageId;
extern const uint32_t TasksStack__ctor_m06BB2D082AEC6B7B2F2ED6C6F5DDE952519F6E58_MetadataUsageId;
extern const uint32_t TasksStack_get_Done_mF66017E24AF36ACC7379FD09898D29C35A8558A8_MetadataUsageId;
extern const uint32_t TasksStack_get_Item_mB6EAC86E6B6379FDCA79684C53755A0B56B1E021_MetadataUsageId;
extern const uint32_t VertexAnimationToolsAssetBase_ClearUnusedMaterials_m0BC8523B36E38D380843F02DF85B3BC4C691E088_MetadataUsageId;
extern const uint32_t VertexAnimationToolsAssetBase_GetPolygonalMeshMaterials_m83BBF1EB87A4490009966B24594D13A129A0E093_MetadataUsageId;
extern const uint32_t VertexAnimationToolsAssetBase_SetMaterialsUnused_mAEF7968AD62EDE7FB3203DA91FCC88F2D72B9E64_MetadataUsageId;
extern const uint32_t VertexAnimationToolsAssetBase__ctor_mDB19E63561EF65DDFD687C5692057B4F4CEF4C76_MetadataUsageId;

struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_TD9DD7B2D90FA6A95E876AD7293733ED74CE678D5_H
#define DICTIONARY_2_TD9DD7B2D90FA6A95E876AD7293733ED74CE678D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,VertexAnimationTools_30.TasksStack_Task>
struct  Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t41B725507AF4EDB7C42FAEEA3CD988A72EA8CCCA* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tF415CB4C8FB0CDC3FB0933A3C61E98126D14444D * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tD1FFED9FB2EC2E92FEBCD5CA8D869FF45281FA9C * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5, ___entries_1)); }
	inline EntryU5BU5D_t41B725507AF4EDB7C42FAEEA3CD988A72EA8CCCA* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t41B725507AF4EDB7C42FAEEA3CD988A72EA8CCCA** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t41B725507AF4EDB7C42FAEEA3CD988A72EA8CCCA* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5, ___keys_7)); }
	inline KeyCollection_tF415CB4C8FB0CDC3FB0933A3C61E98126D14444D * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tF415CB4C8FB0CDC3FB0933A3C61E98126D14444D ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tF415CB4C8FB0CDC3FB0933A3C61E98126D14444D * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5, ___values_8)); }
	inline ValueCollection_tD1FFED9FB2EC2E92FEBCD5CA8D869FF45281FA9C * get_values_8() const { return ___values_8; }
	inline ValueCollection_tD1FFED9FB2EC2E92FEBCD5CA8D869FF45281FA9C ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tD1FFED9FB2EC2E92FEBCD5CA8D869FF45281FA9C * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_TD9DD7B2D90FA6A95E876AD7293733ED74CE678D5_H
#ifndef LIST_1_T5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA_H
#define LIST_1_T5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<VertexAnimationTools_30.MaterialInfo>
struct  List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MaterialInfoU5BU5D_t94DC06E688DE56C79F735F3FA0144F088D82F80D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA, ____items_1)); }
	inline MaterialInfoU5BU5D_t94DC06E688DE56C79F735F3FA0144F088D82F80D* get__items_1() const { return ____items_1; }
	inline MaterialInfoU5BU5D_t94DC06E688DE56C79F735F3FA0144F088D82F80D** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MaterialInfoU5BU5D_t94DC06E688DE56C79F735F3FA0144F088D82F80D* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	MaterialInfoU5BU5D_t94DC06E688DE56C79F735F3FA0144F088D82F80D* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA_StaticFields, ____emptyArray_5)); }
	inline MaterialInfoU5BU5D_t94DC06E688DE56C79F735F3FA0144F088D82F80D* get__emptyArray_5() const { return ____emptyArray_5; }
	inline MaterialInfoU5BU5D_t94DC06E688DE56C79F735F3FA0144F088D82F80D** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(MaterialInfoU5BU5D_t94DC06E688DE56C79F735F3FA0144F088D82F80D* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA_H
#ifndef LIST_1_T0B4B303294DA4BE8DA6E974FBB866FFE0C252D23_H
#define LIST_1_T0B4B303294DA4BE8DA6E974FBB866FFE0C252D23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<VertexAnimationTools_30.TasksStack_Task>
struct  List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TaskU5BU5D_tBB5F48935BC3977997D53AB7BD37AD4226CF6269* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23, ____items_1)); }
	inline TaskU5BU5D_tBB5F48935BC3977997D53AB7BD37AD4226CF6269* get__items_1() const { return ____items_1; }
	inline TaskU5BU5D_tBB5F48935BC3977997D53AB7BD37AD4226CF6269** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TaskU5BU5D_tBB5F48935BC3977997D53AB7BD37AD4226CF6269* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TaskU5BU5D_tBB5F48935BC3977997D53AB7BD37AD4226CF6269* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23_StaticFields, ____emptyArray_5)); }
	inline TaskU5BU5D_tBB5F48935BC3977997D53AB7BD37AD4226CF6269* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TaskU5BU5D_tBB5F48935BC3977997D53AB7BD37AD4226CF6269** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TaskU5BU5D_tBB5F48935BC3977997D53AB7BD37AD4226CF6269* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T0B4B303294DA4BE8DA6E974FBB866FFE0C252D23_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef MATERIALINFO_TEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0_H
#define MATERIALINFO_TEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.MaterialInfo
struct  MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0  : public RuntimeObject
{
public:
	// UnityEngine.Material VertexAnimationTools_30.MaterialInfo::Mat
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___Mat_0;
	// System.Boolean VertexAnimationTools_30.MaterialInfo::Used
	bool ___Used_1;
	// System.String VertexAnimationTools_30.MaterialInfo::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_Mat_0() { return static_cast<int32_t>(offsetof(MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0, ___Mat_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_Mat_0() const { return ___Mat_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_Mat_0() { return &___Mat_0; }
	inline void set_Mat_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___Mat_0 = value;
		Il2CppCodeGenWriteBarrier((&___Mat_0), value);
	}

	inline static int32_t get_offset_of_Used_1() { return static_cast<int32_t>(offsetof(MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0, ___Used_1)); }
	inline bool get_Used_1() const { return ___Used_1; }
	inline bool* get_address_of_Used_1() { return &___Used_1; }
	inline void set_Used_1(bool value)
	{
		___Used_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALINFO_TEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0_H
#ifndef SAMPLES_TCBF54D332EEA974DDC15636D1CFC5B13179D0D08_H
#define SAMPLES_TCBF54D332EEA974DDC15636D1CFC5B13179D0D08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ProjectionSamples_Samples
struct  Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.ProjectionSamples_Samples::Name
	String_t* ___Name_0;
	// UnityEngine.Vector3[] VertexAnimationTools_30.ProjectionSamples_Samples::Dirs
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Dirs_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Dirs_1() { return static_cast<int32_t>(offsetof(Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08, ___Dirs_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Dirs_1() const { return ___Dirs_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Dirs_1() { return &___Dirs_1; }
	inline void set_Dirs_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Dirs_1 = value;
		Il2CppCodeGenWriteBarrier((&___Dirs_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLES_TCBF54D332EEA974DDC15636D1CFC5B13179D0D08_H
#ifndef TASKSSTACK_TA7E298F5409030E4A9DB41C8074552889B7B927F_H
#define TASKSSTACK_TA7E298F5409030E4A9DB41C8074552889B7B927F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.TasksStack
struct  TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.TasksStack::Name
	String_t* ___Name_0;
	// System.Collections.Generic.List`1<VertexAnimationTools_30.TasksStack_Task> VertexAnimationTools_30.TasksStack::items
	List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * ___items_1;
	// System.Collections.Generic.Dictionary`2<System.String,VertexAnimationTools_30.TasksStack_Task> VertexAnimationTools_30.TasksStack::itemsDict
	Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * ___itemsDict_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F, ___items_1)); }
	inline List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * get_items_1() const { return ___items_1; }
	inline List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 ** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((&___items_1), value);
	}

	inline static int32_t get_offset_of_itemsDict_2() { return static_cast<int32_t>(offsetof(TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F, ___itemsDict_2)); }
	inline Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * get_itemsDict_2() const { return ___itemsDict_2; }
	inline Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 ** get_address_of_itemsDict_2() { return &___itemsDict_2; }
	inline void set_itemsDict_2(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * value)
	{
		___itemsDict_2 = value;
		Il2CppCodeGenWriteBarrier((&___itemsDict_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKSSTACK_TA7E298F5409030E4A9DB41C8074552889B7B927F_H
#ifndef TASK_TC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4_H
#define TASK_TC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.TasksStack_Task
struct  Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.TasksStack_Task::Name
	String_t* ___Name_0;
	// System.Single VertexAnimationTools_30.TasksStack_Task::Iterations
	float ___Iterations_1;
	// System.Single VertexAnimationTools_30.TasksStack_Task::NormalizedFrom
	float ___NormalizedFrom_2;
	// System.Single VertexAnimationTools_30.TasksStack_Task::NormalizedTo
	float ___NormalizedTo_3;
	// System.Single VertexAnimationTools_30.TasksStack_Task::Weight
	float ___Weight_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Iterations_1() { return static_cast<int32_t>(offsetof(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4, ___Iterations_1)); }
	inline float get_Iterations_1() const { return ___Iterations_1; }
	inline float* get_address_of_Iterations_1() { return &___Iterations_1; }
	inline void set_Iterations_1(float value)
	{
		___Iterations_1 = value;
	}

	inline static int32_t get_offset_of_NormalizedFrom_2() { return static_cast<int32_t>(offsetof(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4, ___NormalizedFrom_2)); }
	inline float get_NormalizedFrom_2() const { return ___NormalizedFrom_2; }
	inline float* get_address_of_NormalizedFrom_2() { return &___NormalizedFrom_2; }
	inline void set_NormalizedFrom_2(float value)
	{
		___NormalizedFrom_2 = value;
	}

	inline static int32_t get_offset_of_NormalizedTo_3() { return static_cast<int32_t>(offsetof(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4, ___NormalizedTo_3)); }
	inline float get_NormalizedTo_3() const { return ___NormalizedTo_3; }
	inline float* get_address_of_NormalizedTo_3() { return &___NormalizedTo_3; }
	inline void set_NormalizedTo_3(float value)
	{
		___NormalizedTo_3 = value;
	}

	inline static int32_t get_offset_of_Weight_4() { return static_cast<int32_t>(offsetof(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4, ___Weight_4)); }
	inline float get_Weight_4() const { return ___Weight_4; }
	inline float* get_address_of_Weight_4() { return &___Weight_4; }
	inline void set_Weight_4(float value)
	{
		___Weight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASK_TC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef TASKINFO_T3FBF9A10362590974F251CE28A2FD4FFB8F778F1_H
#define TASKINFO_T3FBF9A10362590974F251CE28A2FD4FFB8F778F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.TaskInfo
struct  TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 
{
public:
	// System.String VertexAnimationTools_30.TaskInfo::Name
	String_t* ___Name_0;
	// System.Single VertexAnimationTools_30.TaskInfo::Persentage
	float ___Persentage_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Persentage_1() { return static_cast<int32_t>(offsetof(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1, ___Persentage_1)); }
	inline float get_Persentage_1() const { return ___Persentage_1; }
	inline float* get_address_of_Persentage_1() { return &___Persentage_1; }
	inline void set_Persentage_1(float value)
	{
		___Persentage_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of VertexAnimationTools_30.TaskInfo
struct TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_pinvoke
{
	char* ___Name_0;
	float ___Persentage_1;
};
// Native definition for COM marshalling of VertexAnimationTools_30.TaskInfo
struct TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_com
{
	Il2CppChar* ___Name_0;
	float ___Persentage_1;
};
#endif // TASKINFO_T3FBF9A10362590974F251CE28A2FD4FFB8F778F1_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef AUTOPLAYBACKTYPEENUM_TF7A9D12E935B6A75A0B4D544954EF4DFF033BC44_H
#define AUTOPLAYBACKTYPEENUM_TF7A9D12E935B6A75A0B4D544954EF4DFF033BC44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.AutoPlaybackTypeEnum
struct  AutoPlaybackTypeEnum_tF7A9D12E935B6A75A0B4D544954EF4DFF033BC44 
{
public:
	// System.Int32 VertexAnimationTools_30.AutoPlaybackTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoPlaybackTypeEnum_tF7A9D12E935B6A75A0B4D544954EF4DFF033BC44, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAYBACKTYPEENUM_TF7A9D12E935B6A75A0B4D544954EF4DFF033BC44_H
#ifndef PFU_TD897B0E8199F5D54F4DBE9BFA91500B564EDB97D_H
#define PFU_TD897B0E8199F5D54F4DBE9BFA91500B564EDB97D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PFU
struct  PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D 
{
public:
	// UnityEngine.Vector3 VertexAnimationTools_30.PFU::P
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___P_0;
	// UnityEngine.Vector3 VertexAnimationTools_30.PFU::F
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___F_1;
	// UnityEngine.Vector3 VertexAnimationTools_30.PFU::U
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D, ___P_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_P_0() const { return ___P_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___P_0 = value;
	}

	inline static int32_t get_offset_of_F_1() { return static_cast<int32_t>(offsetof(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D, ___F_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_F_1() const { return ___F_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_F_1() { return &___F_1; }
	inline void set_F_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___F_1 = value;
	}

	inline static int32_t get_offset_of_U_2() { return static_cast<int32_t>(offsetof(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D, ___U_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U_2() const { return ___U_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U_2() { return &___U_2; }
	inline void set_U_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PFU_TD897B0E8199F5D54F4DBE9BFA91500B564EDB97D_H
#ifndef PLAYERUPDATEMODE_T69E6A34DE274376292987D8D567B1E7B30220293_H
#define PLAYERUPDATEMODE_T69E6A34DE274376292987D8D567B1E7B30220293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PlayerUpdateMode
struct  PlayerUpdateMode_t69E6A34DE274376292987D8D567B1E7B30220293 
{
public:
	// System.Int32 VertexAnimationTools_30.PlayerUpdateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlayerUpdateMode_t69E6A34DE274376292987D8D567B1E7B30220293, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERUPDATEMODE_T69E6A34DE274376292987D8D567B1E7B30220293_H
#ifndef PROJECTIONQUALITYENUM_T3EF48B93088C7D81A7804EF2F56E48CBB8C4564F_H
#define PROJECTIONQUALITYENUM_T3EF48B93088C7D81A7804EF2F56E48CBB8C4564F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ProjectionQualityEnum
struct  ProjectionQualityEnum_t3EF48B93088C7D81A7804EF2F56E48CBB8C4564F 
{
public:
	// System.Int32 VertexAnimationTools_30.ProjectionQualityEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProjectionQualityEnum_t3EF48B93088C7D81A7804EF2F56E48CBB8C4564F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONQUALITYENUM_T3EF48B93088C7D81A7804EF2F56E48CBB8C4564F_H
#ifndef TRANSITIONMODEENUM_TB044BF8C2E6D830051A88CC905F5E8BF910ED946_H
#define TRANSITIONMODEENUM_TB044BF8C2E6D830051A88CC905F5E8BF910ED946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.TransitionModeEnum
struct  TransitionModeEnum_tB044BF8C2E6D830051A88CC905F5E8BF910ED946 
{
public:
	// System.Int32 VertexAnimationTools_30.TransitionModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransitionModeEnum_tB044BF8C2E6D830051A88CC905F5E8BF910ED946, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITIONMODEENUM_TB044BF8C2E6D830051A88CC905F5E8BF910ED946_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#define MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef CLIP_T7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77_H
#define CLIP_T7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCachePlayer_Clip
struct  Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77  : public RuntimeObject
{
public:
	// System.Int32 VertexAnimationTools_30.PointCachePlayer_Clip::Idx
	int32_t ___Idx_0;
	// VertexAnimationTools_30.PointCachePlayer VertexAnimationTools_30.PointCachePlayer_Clip::Player
	PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * ___Player_1;
	// VertexAnimationTools_30.AutoPlaybackTypeEnum VertexAnimationTools_30.PointCachePlayer_Clip::AutoPlaybackType
	int32_t ___AutoPlaybackType_2;
	// System.Single VertexAnimationTools_30.PointCachePlayer_Clip::DurationInSeconds
	float ___DurationInSeconds_3;
	// System.Boolean VertexAnimationTools_30.PointCachePlayer_Clip::DrawMotionPath
	bool ___DrawMotionPath_4;
	// System.Single VertexAnimationTools_30.PointCachePlayer_Clip::MotionPathIconSize
	float ___MotionPathIconSize_5;

public:
	inline static int32_t get_offset_of_Idx_0() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___Idx_0)); }
	inline int32_t get_Idx_0() const { return ___Idx_0; }
	inline int32_t* get_address_of_Idx_0() { return &___Idx_0; }
	inline void set_Idx_0(int32_t value)
	{
		___Idx_0 = value;
	}

	inline static int32_t get_offset_of_Player_1() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___Player_1)); }
	inline PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * get_Player_1() const { return ___Player_1; }
	inline PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F ** get_address_of_Player_1() { return &___Player_1; }
	inline void set_Player_1(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * value)
	{
		___Player_1 = value;
		Il2CppCodeGenWriteBarrier((&___Player_1), value);
	}

	inline static int32_t get_offset_of_AutoPlaybackType_2() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___AutoPlaybackType_2)); }
	inline int32_t get_AutoPlaybackType_2() const { return ___AutoPlaybackType_2; }
	inline int32_t* get_address_of_AutoPlaybackType_2() { return &___AutoPlaybackType_2; }
	inline void set_AutoPlaybackType_2(int32_t value)
	{
		___AutoPlaybackType_2 = value;
	}

	inline static int32_t get_offset_of_DurationInSeconds_3() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___DurationInSeconds_3)); }
	inline float get_DurationInSeconds_3() const { return ___DurationInSeconds_3; }
	inline float* get_address_of_DurationInSeconds_3() { return &___DurationInSeconds_3; }
	inline void set_DurationInSeconds_3(float value)
	{
		___DurationInSeconds_3 = value;
	}

	inline static int32_t get_offset_of_DrawMotionPath_4() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___DrawMotionPath_4)); }
	inline bool get_DrawMotionPath_4() const { return ___DrawMotionPath_4; }
	inline bool* get_address_of_DrawMotionPath_4() { return &___DrawMotionPath_4; }
	inline void set_DrawMotionPath_4(bool value)
	{
		___DrawMotionPath_4 = value;
	}

	inline static int32_t get_offset_of_MotionPathIconSize_5() { return static_cast<int32_t>(offsetof(Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77, ___MotionPathIconSize_5)); }
	inline float get_MotionPathIconSize_5() const { return ___MotionPathIconSize_5; }
	inline float* get_address_of_MotionPathIconSize_5() { return &___MotionPathIconSize_5; }
	inline void set_MotionPathIconSize_5(float value)
	{
		___MotionPathIconSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIP_T7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77_H
#ifndef CONSTRAINT_T5134256E17C3C59BE6CBE19E96E7369D7A88D6C1_H
#define CONSTRAINT_T5134256E17C3C59BE6CBE19E96E7369D7A88D6C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCachePlayer_Constraint
struct  Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1  : public RuntimeObject
{
public:
	// System.String VertexAnimationTools_30.PointCachePlayer_Constraint::Name
	String_t* ___Name_0;
	// UnityEngine.Transform VertexAnimationTools_30.PointCachePlayer_Constraint::Tr
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Tr_1;
	// VertexAnimationTools_30.PFU VertexAnimationTools_30.PointCachePlayer_Constraint::utm
	PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  ___utm_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Tr_1() { return static_cast<int32_t>(offsetof(Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1, ___Tr_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_Tr_1() const { return ___Tr_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_Tr_1() { return &___Tr_1; }
	inline void set_Tr_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___Tr_1 = value;
		Il2CppCodeGenWriteBarrier((&___Tr_1), value);
	}

	inline static int32_t get_offset_of_utm_2() { return static_cast<int32_t>(offsetof(Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1, ___utm_2)); }
	inline PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  get_utm_2() const { return ___utm_2; }
	inline PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D * get_address_of_utm_2() { return &___utm_2; }
	inline void set_utm_2(PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  value)
	{
		___utm_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T5134256E17C3C59BE6CBE19E96E7369D7A88D6C1_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef PROJECTIONSAMPLES_T3F2EAB34ED2433A74795DB31B8790F3AFAC3F708_H
#define PROJECTIONSAMPLES_T3F2EAB34ED2433A74795DB31B8790F3AFAC3F708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.ProjectionSamples
struct  ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// VertexAnimationTools_30.ProjectionSamples_Samples[] VertexAnimationTools_30.ProjectionSamples::SphereSamples
	SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* ___SphereSamples_4;
	// VertexAnimationTools_30.ProjectionSamples_Samples[] VertexAnimationTools_30.ProjectionSamples::DomeSamples
	SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* ___DomeSamples_5;

public:
	inline static int32_t get_offset_of_SphereSamples_4() { return static_cast<int32_t>(offsetof(ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708, ___SphereSamples_4)); }
	inline SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* get_SphereSamples_4() const { return ___SphereSamples_4; }
	inline SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44** get_address_of_SphereSamples_4() { return &___SphereSamples_4; }
	inline void set_SphereSamples_4(SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* value)
	{
		___SphereSamples_4 = value;
		Il2CppCodeGenWriteBarrier((&___SphereSamples_4), value);
	}

	inline static int32_t get_offset_of_DomeSamples_5() { return static_cast<int32_t>(offsetof(ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708, ___DomeSamples_5)); }
	inline SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* get_DomeSamples_5() const { return ___DomeSamples_5; }
	inline SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44** get_address_of_DomeSamples_5() { return &___DomeSamples_5; }
	inline void set_DomeSamples_5(SamplesU5BU5D_t88AA6FF639429331DBCAAD5BCAD0D7631B546F44* value)
	{
		___DomeSamples_5 = value;
		Il2CppCodeGenWriteBarrier((&___DomeSamples_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONSAMPLES_T3F2EAB34ED2433A74795DB31B8790F3AFAC3F708_H
#ifndef VERTEXANIMATIONTOOLSASSETBASE_T24D12B84EDE085A896DF88C598AEA5C55A648252_H
#define VERTEXANIMATIONTOOLSASSETBASE_T24D12B84EDE085A896DF88C598AEA5C55A648252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.VertexAnimationToolsAssetBase
struct  VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<VertexAnimationTools_30.MaterialInfo> VertexAnimationTools_30.VertexAnimationToolsAssetBase::Materials
	List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * ___Materials_4;

public:
	inline static int32_t get_offset_of_Materials_4() { return static_cast<int32_t>(offsetof(VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252, ___Materials_4)); }
	inline List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * get_Materials_4() const { return ___Materials_4; }
	inline List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA ** get_address_of_Materials_4() { return &___Materials_4; }
	inline void set_Materials_4(List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * value)
	{
		___Materials_4 = value;
		Il2CppCodeGenWriteBarrier((&___Materials_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXANIMATIONTOOLSASSETBASE_T24D12B84EDE085A896DF88C598AEA5C55A648252_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef POINTCACHEPLAYER_T7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_H
#define POINTCACHEPLAYER_T7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexAnimationTools_30.PointCachePlayer
struct  PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean VertexAnimationTools_30.PointCachePlayer::UseTimescale
	bool ___UseTimescale_5;
	// VertexAnimationTools_30.PlayerUpdateMode VertexAnimationTools_30.PointCachePlayer::UpdateMode
	int32_t ___UpdateMode_6;
	// System.Single VertexAnimationTools_30.PointCachePlayer::timeDirection
	float ___timeDirection_7;
	// System.Int32 VertexAnimationTools_30.PointCachePlayer::ActiveMesh
	int32_t ___ActiveMesh_8;
	// UnityEngine.MeshCollider VertexAnimationTools_30.PointCachePlayer::MCollider
	MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * ___MCollider_9;
	// UnityEngine.Mesh VertexAnimationTools_30.PointCachePlayer::ColliderMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___ColliderMesh_10;
	// VertexAnimationTools_30.PointCachePlayer_Clip[] VertexAnimationTools_30.PointCachePlayer::Clips
	ClipU5BU5D_tAA79AB3C6F96CA78E7D4DE8DBB9F3D785C413603* ___Clips_11;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip0NormalizedTime
	float ___Clip0NormalizedTime_12;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip0Weight
	float ___Clip0Weight_13;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip1NormalizedTime
	float ___Clip1NormalizedTime_14;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip1Weight
	float ___Clip1Weight_15;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip2NormalizedTime
	float ___Clip2NormalizedTime_16;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip2Weight
	float ___Clip2Weight_17;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip3NormalizedTime
	float ___Clip3NormalizedTime_18;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip3Weight
	float ___Clip3Weight_19;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip4NormalizedTime
	float ___Clip4NormalizedTime_20;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip4Weight
	float ___Clip4Weight_21;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip5NormalizedTime
	float ___Clip5NormalizedTime_22;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip5Weight
	float ___Clip5Weight_23;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip6NormalizedTime
	float ___Clip6NormalizedTime_24;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip6Weight
	float ___Clip6Weight_25;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip7NormalizedTime
	float ___Clip7NormalizedTime_26;
	// System.Single VertexAnimationTools_30.PointCachePlayer::Clip7Weight
	float ___Clip7Weight_27;
	// VertexAnimationTools_30.PointCache VertexAnimationTools_30.PointCachePlayer::pointCache
	PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * ___pointCache_28;
	// UnityEngine.SkinnedMeshRenderer VertexAnimationTools_30.PointCachePlayer::smr
	SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * ___smr_29;
	// VertexAnimationTools_30.PointCachePlayer_Constraint[] VertexAnimationTools_30.PointCachePlayer::Constraints
	ConstraintU5BU5D_tA24CD0C95C240251A402CA5801AEEFBA2A56756A* ___Constraints_30;
	// System.Int32 VertexAnimationTools_30.PointCachePlayer::PreparedMeshCollider
	int32_t ___PreparedMeshCollider_31;
	// System.Boolean VertexAnimationTools_30.PointCachePlayer::DrawMeshGizmo
	bool ___DrawMeshGizmo_32;

public:
	inline static int32_t get_offset_of_UseTimescale_5() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___UseTimescale_5)); }
	inline bool get_UseTimescale_5() const { return ___UseTimescale_5; }
	inline bool* get_address_of_UseTimescale_5() { return &___UseTimescale_5; }
	inline void set_UseTimescale_5(bool value)
	{
		___UseTimescale_5 = value;
	}

	inline static int32_t get_offset_of_UpdateMode_6() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___UpdateMode_6)); }
	inline int32_t get_UpdateMode_6() const { return ___UpdateMode_6; }
	inline int32_t* get_address_of_UpdateMode_6() { return &___UpdateMode_6; }
	inline void set_UpdateMode_6(int32_t value)
	{
		___UpdateMode_6 = value;
	}

	inline static int32_t get_offset_of_timeDirection_7() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___timeDirection_7)); }
	inline float get_timeDirection_7() const { return ___timeDirection_7; }
	inline float* get_address_of_timeDirection_7() { return &___timeDirection_7; }
	inline void set_timeDirection_7(float value)
	{
		___timeDirection_7 = value;
	}

	inline static int32_t get_offset_of_ActiveMesh_8() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___ActiveMesh_8)); }
	inline int32_t get_ActiveMesh_8() const { return ___ActiveMesh_8; }
	inline int32_t* get_address_of_ActiveMesh_8() { return &___ActiveMesh_8; }
	inline void set_ActiveMesh_8(int32_t value)
	{
		___ActiveMesh_8 = value;
	}

	inline static int32_t get_offset_of_MCollider_9() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___MCollider_9)); }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * get_MCollider_9() const { return ___MCollider_9; }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE ** get_address_of_MCollider_9() { return &___MCollider_9; }
	inline void set_MCollider_9(MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * value)
	{
		___MCollider_9 = value;
		Il2CppCodeGenWriteBarrier((&___MCollider_9), value);
	}

	inline static int32_t get_offset_of_ColliderMesh_10() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___ColliderMesh_10)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_ColliderMesh_10() const { return ___ColliderMesh_10; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_ColliderMesh_10() { return &___ColliderMesh_10; }
	inline void set_ColliderMesh_10(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___ColliderMesh_10 = value;
		Il2CppCodeGenWriteBarrier((&___ColliderMesh_10), value);
	}

	inline static int32_t get_offset_of_Clips_11() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clips_11)); }
	inline ClipU5BU5D_tAA79AB3C6F96CA78E7D4DE8DBB9F3D785C413603* get_Clips_11() const { return ___Clips_11; }
	inline ClipU5BU5D_tAA79AB3C6F96CA78E7D4DE8DBB9F3D785C413603** get_address_of_Clips_11() { return &___Clips_11; }
	inline void set_Clips_11(ClipU5BU5D_tAA79AB3C6F96CA78E7D4DE8DBB9F3D785C413603* value)
	{
		___Clips_11 = value;
		Il2CppCodeGenWriteBarrier((&___Clips_11), value);
	}

	inline static int32_t get_offset_of_Clip0NormalizedTime_12() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip0NormalizedTime_12)); }
	inline float get_Clip0NormalizedTime_12() const { return ___Clip0NormalizedTime_12; }
	inline float* get_address_of_Clip0NormalizedTime_12() { return &___Clip0NormalizedTime_12; }
	inline void set_Clip0NormalizedTime_12(float value)
	{
		___Clip0NormalizedTime_12 = value;
	}

	inline static int32_t get_offset_of_Clip0Weight_13() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip0Weight_13)); }
	inline float get_Clip0Weight_13() const { return ___Clip0Weight_13; }
	inline float* get_address_of_Clip0Weight_13() { return &___Clip0Weight_13; }
	inline void set_Clip0Weight_13(float value)
	{
		___Clip0Weight_13 = value;
	}

	inline static int32_t get_offset_of_Clip1NormalizedTime_14() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip1NormalizedTime_14)); }
	inline float get_Clip1NormalizedTime_14() const { return ___Clip1NormalizedTime_14; }
	inline float* get_address_of_Clip1NormalizedTime_14() { return &___Clip1NormalizedTime_14; }
	inline void set_Clip1NormalizedTime_14(float value)
	{
		___Clip1NormalizedTime_14 = value;
	}

	inline static int32_t get_offset_of_Clip1Weight_15() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip1Weight_15)); }
	inline float get_Clip1Weight_15() const { return ___Clip1Weight_15; }
	inline float* get_address_of_Clip1Weight_15() { return &___Clip1Weight_15; }
	inline void set_Clip1Weight_15(float value)
	{
		___Clip1Weight_15 = value;
	}

	inline static int32_t get_offset_of_Clip2NormalizedTime_16() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip2NormalizedTime_16)); }
	inline float get_Clip2NormalizedTime_16() const { return ___Clip2NormalizedTime_16; }
	inline float* get_address_of_Clip2NormalizedTime_16() { return &___Clip2NormalizedTime_16; }
	inline void set_Clip2NormalizedTime_16(float value)
	{
		___Clip2NormalizedTime_16 = value;
	}

	inline static int32_t get_offset_of_Clip2Weight_17() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip2Weight_17)); }
	inline float get_Clip2Weight_17() const { return ___Clip2Weight_17; }
	inline float* get_address_of_Clip2Weight_17() { return &___Clip2Weight_17; }
	inline void set_Clip2Weight_17(float value)
	{
		___Clip2Weight_17 = value;
	}

	inline static int32_t get_offset_of_Clip3NormalizedTime_18() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip3NormalizedTime_18)); }
	inline float get_Clip3NormalizedTime_18() const { return ___Clip3NormalizedTime_18; }
	inline float* get_address_of_Clip3NormalizedTime_18() { return &___Clip3NormalizedTime_18; }
	inline void set_Clip3NormalizedTime_18(float value)
	{
		___Clip3NormalizedTime_18 = value;
	}

	inline static int32_t get_offset_of_Clip3Weight_19() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip3Weight_19)); }
	inline float get_Clip3Weight_19() const { return ___Clip3Weight_19; }
	inline float* get_address_of_Clip3Weight_19() { return &___Clip3Weight_19; }
	inline void set_Clip3Weight_19(float value)
	{
		___Clip3Weight_19 = value;
	}

	inline static int32_t get_offset_of_Clip4NormalizedTime_20() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip4NormalizedTime_20)); }
	inline float get_Clip4NormalizedTime_20() const { return ___Clip4NormalizedTime_20; }
	inline float* get_address_of_Clip4NormalizedTime_20() { return &___Clip4NormalizedTime_20; }
	inline void set_Clip4NormalizedTime_20(float value)
	{
		___Clip4NormalizedTime_20 = value;
	}

	inline static int32_t get_offset_of_Clip4Weight_21() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip4Weight_21)); }
	inline float get_Clip4Weight_21() const { return ___Clip4Weight_21; }
	inline float* get_address_of_Clip4Weight_21() { return &___Clip4Weight_21; }
	inline void set_Clip4Weight_21(float value)
	{
		___Clip4Weight_21 = value;
	}

	inline static int32_t get_offset_of_Clip5NormalizedTime_22() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip5NormalizedTime_22)); }
	inline float get_Clip5NormalizedTime_22() const { return ___Clip5NormalizedTime_22; }
	inline float* get_address_of_Clip5NormalizedTime_22() { return &___Clip5NormalizedTime_22; }
	inline void set_Clip5NormalizedTime_22(float value)
	{
		___Clip5NormalizedTime_22 = value;
	}

	inline static int32_t get_offset_of_Clip5Weight_23() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip5Weight_23)); }
	inline float get_Clip5Weight_23() const { return ___Clip5Weight_23; }
	inline float* get_address_of_Clip5Weight_23() { return &___Clip5Weight_23; }
	inline void set_Clip5Weight_23(float value)
	{
		___Clip5Weight_23 = value;
	}

	inline static int32_t get_offset_of_Clip6NormalizedTime_24() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip6NormalizedTime_24)); }
	inline float get_Clip6NormalizedTime_24() const { return ___Clip6NormalizedTime_24; }
	inline float* get_address_of_Clip6NormalizedTime_24() { return &___Clip6NormalizedTime_24; }
	inline void set_Clip6NormalizedTime_24(float value)
	{
		___Clip6NormalizedTime_24 = value;
	}

	inline static int32_t get_offset_of_Clip6Weight_25() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip6Weight_25)); }
	inline float get_Clip6Weight_25() const { return ___Clip6Weight_25; }
	inline float* get_address_of_Clip6Weight_25() { return &___Clip6Weight_25; }
	inline void set_Clip6Weight_25(float value)
	{
		___Clip6Weight_25 = value;
	}

	inline static int32_t get_offset_of_Clip7NormalizedTime_26() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip7NormalizedTime_26)); }
	inline float get_Clip7NormalizedTime_26() const { return ___Clip7NormalizedTime_26; }
	inline float* get_address_of_Clip7NormalizedTime_26() { return &___Clip7NormalizedTime_26; }
	inline void set_Clip7NormalizedTime_26(float value)
	{
		___Clip7NormalizedTime_26 = value;
	}

	inline static int32_t get_offset_of_Clip7Weight_27() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Clip7Weight_27)); }
	inline float get_Clip7Weight_27() const { return ___Clip7Weight_27; }
	inline float* get_address_of_Clip7Weight_27() { return &___Clip7Weight_27; }
	inline void set_Clip7Weight_27(float value)
	{
		___Clip7Weight_27 = value;
	}

	inline static int32_t get_offset_of_pointCache_28() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___pointCache_28)); }
	inline PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * get_pointCache_28() const { return ___pointCache_28; }
	inline PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 ** get_address_of_pointCache_28() { return &___pointCache_28; }
	inline void set_pointCache_28(PointCache_t08D0CD88824B54FB717F6B510FF8EA87D1FA49B7 * value)
	{
		___pointCache_28 = value;
		Il2CppCodeGenWriteBarrier((&___pointCache_28), value);
	}

	inline static int32_t get_offset_of_smr_29() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___smr_29)); }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * get_smr_29() const { return ___smr_29; }
	inline SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 ** get_address_of_smr_29() { return &___smr_29; }
	inline void set_smr_29(SkinnedMeshRenderer_tFC8103EE7842F7F8A98BEF0C855D32A9711B7B65 * value)
	{
		___smr_29 = value;
		Il2CppCodeGenWriteBarrier((&___smr_29), value);
	}

	inline static int32_t get_offset_of_Constraints_30() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___Constraints_30)); }
	inline ConstraintU5BU5D_tA24CD0C95C240251A402CA5801AEEFBA2A56756A* get_Constraints_30() const { return ___Constraints_30; }
	inline ConstraintU5BU5D_tA24CD0C95C240251A402CA5801AEEFBA2A56756A** get_address_of_Constraints_30() { return &___Constraints_30; }
	inline void set_Constraints_30(ConstraintU5BU5D_tA24CD0C95C240251A402CA5801AEEFBA2A56756A* value)
	{
		___Constraints_30 = value;
		Il2CppCodeGenWriteBarrier((&___Constraints_30), value);
	}

	inline static int32_t get_offset_of_PreparedMeshCollider_31() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___PreparedMeshCollider_31)); }
	inline int32_t get_PreparedMeshCollider_31() const { return ___PreparedMeshCollider_31; }
	inline int32_t* get_address_of_PreparedMeshCollider_31() { return &___PreparedMeshCollider_31; }
	inline void set_PreparedMeshCollider_31(int32_t value)
	{
		___PreparedMeshCollider_31 = value;
	}

	inline static int32_t get_offset_of_DrawMeshGizmo_32() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F, ___DrawMeshGizmo_32)); }
	inline bool get_DrawMeshGizmo_32() const { return ___DrawMeshGizmo_32; }
	inline bool* get_address_of_DrawMeshGizmo_32() { return &___DrawMeshGizmo_32; }
	inline void set_DrawMeshGizmo_32(bool value)
	{
		___DrawMeshGizmo_32 = value;
	}
};

struct PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_StaticFields
{
public:
	// UnityEngine.Color[] VertexAnimationTools_30.PointCachePlayer::GizmosClipColors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___GizmosClipColors_4;

public:
	inline static int32_t get_offset_of_GizmosClipColors_4() { return static_cast<int32_t>(offsetof(PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_StaticFields, ___GizmosClipColors_4)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_GizmosClipColors_4() const { return ___GizmosClipColors_4; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_GizmosClipColors_4() { return &___GizmosClipColors_4; }
	inline void set_GizmosClipColors_4(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___GizmosClipColors_4 = value;
		Il2CppCodeGenWriteBarrier((&___GizmosClipColors_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCACHEPLAYER_T7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F_H
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * m_Items[1];

public:
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Resources_Load_TisRuntimeObject_mF219AFC44F367143C90364C2DD386AEF6DCDA9A9_gshared (String_t* p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_m6625C3BA931A6EE5D6DB46B9E743B40AAA30010B_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C7E51568033239B506E15E7804A0B8658246498_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C" IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m3455807C552312C60038DF52EF328C3687442DE3_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Add_mC741BBB0A647C814227953DB9B23CB1BDF571C5B_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mF28CCEE6497CBD0D989E2CCC63105F00E66CDCDF_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// VertexAnimationTools_30.PFU VertexAnimationTools_30.PFU::op_Multiply(VertexAnimationTools_30.PFU,UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  PFU_op_Multiply_mB243CBB36BAA2C76BA1AF797CB642DC9756551E2 (PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  ___a0, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<VertexAnimationTools_30.ProjectionSamples>(System.String)
inline ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708 * Resources_Load_TisProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708_m2A2EAEEE2C850C84F94448E91F0164E0D48AEBEB (String_t* p0, const RuntimeMethod* method)
{
	return ((  ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708 * (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_mF219AFC44F367143C90364C2DD386AEF6DCDA9A9_gshared)(p0, method);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B (ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734 * __this, const RuntimeMethod* method);
// System.Void VertexAnimationTools_30.TaskInfo::.ctor(System.String,System.Single)
extern "C" IL2CPP_METHOD_ATTR void TaskInfo__ctor_mB7A07F8C9C02C3B33CF3D47A267FD0FE94F31540 (TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 * __this, String_t* ___name0, float ___persentage1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.String,VertexAnimationTools_30.TasksStack/Task>::get_Item(!0)
inline Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * Dictionary_2_get_Item_mB37BDDEB7C29FEC675826E3C374316BFBBB480C9 (Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * __this, String_t* p0, const RuntimeMethod* method)
{
	return ((  Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * (*) (Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m6625C3BA931A6EE5D6DB46B9E743B40AAA30010B_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<VertexAnimationTools_30.TasksStack/Task>::.ctor()
inline void List_1__ctor_m6F104AD5D8FB6EFF1B7453916727EBB430214437 (List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,VertexAnimationTools_30.TasksStack/Task>::.ctor()
inline void Dictionary_2__ctor_m59A3A2601326E56C3EE4E802E8F0E9CA09A0C299 (Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 *, const RuntimeMethod*))Dictionary_2__ctor_m2C7E51568033239B506E15E7804A0B8658246498_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,VertexAnimationTools_30.TasksStack/Task>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m831F11AC81E0F04B86DB2A62B1CDD21CB47B2FE0 (Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * __this, String_t* p0, Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 ** p1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 *, String_t*, Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3455807C552312C60038DF52EF328C3687442DE3_gshared)(__this, p0, p1, method);
}
// System.Void VertexAnimationTools_30.TasksStack/Task::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void Task__ctor_m8ECD07E86F37B5FFFAD7AC598B6B7457BA0C1AA7 (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,VertexAnimationTools_30.TasksStack/Task>::Add(!0,!1)
inline void Dictionary_2_Add_mA7F5F5DAFC9B5AEF2107CB1B9809F1A812BF4D92 (Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * __this, String_t* p0, Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 *, String_t*, Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 *, const RuntimeMethod*))Dictionary_2_Add_mC741BBB0A647C814227953DB9B23CB1BDF571C5B_gshared)(__this, p0, p1, method);
}
// System.Void System.Collections.Generic.List`1<VertexAnimationTools_30.TasksStack/Task>::Add(!0)
inline void List_1_Add_mFD28069FE280F9BC204CD8EAB6B2938CB1FC7FA9 (List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * __this, Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 *, Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// !0 System.Collections.Generic.List`1<VertexAnimationTools_30.TasksStack/Task>::get_Item(System.Int32)
inline Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80 (List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * (*) (List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<VertexAnimationTools_30.TasksStack/Task>::get_Count()
inline int32_t List_1_get_Count_m9B7BF65FC57179097215D9CE925549BD2E722E67 (List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 *, const RuntimeMethod*))List_1_get_Count_mF28CCEE6497CBD0D989E2CCC63105F00E66CDCDF_gshared)(__this, method);
}
// System.String System.String::Format(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA (String_t* p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364 (float p0, float p1, float p2, const RuntimeMethod* method);
// System.String System.Single::ToString(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE (float* __this, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A (String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<VertexAnimationTools_30.MaterialInfo>::get_Item(System.Int32)
inline MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268 (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * (*) (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<VertexAnimationTools_30.MaterialInfo>::get_Count()
inline int32_t List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00 (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA *, const RuntimeMethod*))List_1_get_Count_mF28CCEE6497CBD0D989E2CCC63105F00E66CDCDF_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<VertexAnimationTools_30.MaterialInfo>::.ctor()
inline void List_1__ctor_m75AFAA3C830D844D785A019FE6D4E3A71344B7BD (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// UnityEngine.Material VertexAnimationTools_30.Extension::GetRandomMaterial(System.String)
extern "C" IL2CPP_METHOD_ATTR Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * Extension_GetRandomMaterial_m207F4452B34C29779CB8E896C4C79EA47A512D22 (String_t* ___name0, const RuntimeMethod* method);
// System.Void VertexAnimationTools_30.MaterialInfo::.ctor(System.String,UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR void MaterialInfo__ctor_mA7FFEB75A85A1C260304A806EC1389A8B018990E (MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * __this, String_t* ___name0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mat1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<VertexAnimationTools_30.MaterialInfo>::Add(!0)
inline void List_1_Add_mA28790AE5AFF26DBF13EB5B4E1B0B7954E1D3ED1 (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * __this, MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA *, MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1<VertexAnimationTools_30.MaterialInfo>::Remove(!0)
inline bool List_1_Remove_m5768F6A9F78F566E53AB5ECAAF5BCDB414C3B6B3 (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * __this, MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA *, MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 *, const RuntimeMethod*))List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared)(__this, p0, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single VertexAnimationTools_30.PointCachePlayer_Clip::get_NormalizedTime()
extern "C" IL2CPP_METHOD_ATTR float Clip_get_NormalizedTime_m5D5ABECB18FB975D3BEBFB051A3F66051C9B0327 (Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_Idx_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_002f;
			}
			case 1:
			{
				goto IL_003b;
			}
			case 2:
			{
				goto IL_0047;
			}
			case 3:
			{
				goto IL_0053;
			}
			case 4:
			{
				goto IL_005f;
			}
			case 5:
			{
				goto IL_006b;
			}
			case 6:
			{
				goto IL_0077;
			}
			case 7:
			{
				goto IL_0083;
			}
		}
	}
	{
		goto IL_008f;
	}

IL_002f:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_2 = __this->get_Player_1();
		NullCheck(L_2);
		float L_3 = L_2->get_Clip0NormalizedTime_12();
		return L_3;
	}

IL_003b:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_4 = __this->get_Player_1();
		NullCheck(L_4);
		float L_5 = L_4->get_Clip1NormalizedTime_14();
		return L_5;
	}

IL_0047:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_6 = __this->get_Player_1();
		NullCheck(L_6);
		float L_7 = L_6->get_Clip2NormalizedTime_16();
		return L_7;
	}

IL_0053:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_8 = __this->get_Player_1();
		NullCheck(L_8);
		float L_9 = L_8->get_Clip3NormalizedTime_18();
		return L_9;
	}

IL_005f:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_10 = __this->get_Player_1();
		NullCheck(L_10);
		float L_11 = L_10->get_Clip4NormalizedTime_20();
		return L_11;
	}

IL_006b:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_12 = __this->get_Player_1();
		NullCheck(L_12);
		float L_13 = L_12->get_Clip5NormalizedTime_22();
		return L_13;
	}

IL_0077:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_14 = __this->get_Player_1();
		NullCheck(L_14);
		float L_15 = L_14->get_Clip6NormalizedTime_24();
		return L_15;
	}

IL_0083:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_16 = __this->get_Player_1();
		NullCheck(L_16);
		float L_17 = L_16->get_Clip7NormalizedTime_26();
		return L_17;
	}

IL_008f:
	{
		return (-1.0f);
	}
}
// System.Void VertexAnimationTools_30.PointCachePlayer_Clip::set_NormalizedTime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Clip_set_NormalizedTime_mFB15DF932C114E9BFA205F16FCD7DE243577329A (Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77 * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_Idx_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_002e;
			}
			case 1:
			{
				goto IL_003b;
			}
			case 2:
			{
				goto IL_0048;
			}
			case 3:
			{
				goto IL_0055;
			}
			case 4:
			{
				goto IL_0062;
			}
			case 5:
			{
				goto IL_006f;
			}
			case 6:
			{
				goto IL_007c;
			}
			case 7:
			{
				goto IL_0089;
			}
		}
	}
	{
		return;
	}

IL_002e:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_2 = __this->get_Player_1();
		float L_3 = ___value0;
		NullCheck(L_2);
		L_2->set_Clip0NormalizedTime_12(L_3);
		return;
	}

IL_003b:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_4 = __this->get_Player_1();
		float L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_Clip1NormalizedTime_14(L_5);
		return;
	}

IL_0048:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_6 = __this->get_Player_1();
		float L_7 = ___value0;
		NullCheck(L_6);
		L_6->set_Clip2NormalizedTime_16(L_7);
		return;
	}

IL_0055:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_8 = __this->get_Player_1();
		float L_9 = ___value0;
		NullCheck(L_8);
		L_8->set_Clip3NormalizedTime_18(L_9);
		return;
	}

IL_0062:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_10 = __this->get_Player_1();
		float L_11 = ___value0;
		NullCheck(L_10);
		L_10->set_Clip4NormalizedTime_20(L_11);
		return;
	}

IL_006f:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_12 = __this->get_Player_1();
		float L_13 = ___value0;
		NullCheck(L_12);
		L_12->set_Clip5NormalizedTime_22(L_13);
		return;
	}

IL_007c:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_14 = __this->get_Player_1();
		float L_15 = ___value0;
		NullCheck(L_14);
		L_14->set_Clip6NormalizedTime_24(L_15);
		return;
	}

IL_0089:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_16 = __this->get_Player_1();
		float L_17 = ___value0;
		NullCheck(L_16);
		L_16->set_Clip7NormalizedTime_26(L_17);
		return;
	}
}
// System.Single VertexAnimationTools_30.PointCachePlayer_Clip::get_Weight()
extern "C" IL2CPP_METHOD_ATTR float Clip_get_Weight_m2A8C7F99008CCD0D209D7699FC203ED4EA9C2FD1 (Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_Idx_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_002f;
			}
			case 1:
			{
				goto IL_003b;
			}
			case 2:
			{
				goto IL_0047;
			}
			case 3:
			{
				goto IL_0053;
			}
			case 4:
			{
				goto IL_005f;
			}
			case 5:
			{
				goto IL_006b;
			}
			case 6:
			{
				goto IL_0077;
			}
			case 7:
			{
				goto IL_0083;
			}
		}
	}
	{
		goto IL_008f;
	}

IL_002f:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_2 = __this->get_Player_1();
		NullCheck(L_2);
		float L_3 = L_2->get_Clip0Weight_13();
		return L_3;
	}

IL_003b:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_4 = __this->get_Player_1();
		NullCheck(L_4);
		float L_5 = L_4->get_Clip1Weight_15();
		return L_5;
	}

IL_0047:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_6 = __this->get_Player_1();
		NullCheck(L_6);
		float L_7 = L_6->get_Clip2Weight_17();
		return L_7;
	}

IL_0053:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_8 = __this->get_Player_1();
		NullCheck(L_8);
		float L_9 = L_8->get_Clip3Weight_19();
		return L_9;
	}

IL_005f:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_10 = __this->get_Player_1();
		NullCheck(L_10);
		float L_11 = L_10->get_Clip4Weight_21();
		return L_11;
	}

IL_006b:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_12 = __this->get_Player_1();
		NullCheck(L_12);
		float L_13 = L_12->get_Clip5Weight_23();
		return L_13;
	}

IL_0077:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_14 = __this->get_Player_1();
		NullCheck(L_14);
		float L_15 = L_14->get_Clip6Weight_25();
		return L_15;
	}

IL_0083:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_16 = __this->get_Player_1();
		NullCheck(L_16);
		float L_17 = L_16->get_Clip7Weight_27();
		return L_17;
	}

IL_008f:
	{
		return (-1.0f);
	}
}
// System.Void VertexAnimationTools_30.PointCachePlayer_Clip::set_Weight(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Clip_set_Weight_m037A12B8013A93E76B6FDEAF8FE0B3E035FD1952 (Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77 * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_Idx_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_002e;
			}
			case 1:
			{
				goto IL_003b;
			}
			case 2:
			{
				goto IL_0048;
			}
			case 3:
			{
				goto IL_0055;
			}
			case 4:
			{
				goto IL_0062;
			}
			case 5:
			{
				goto IL_006f;
			}
			case 6:
			{
				goto IL_007c;
			}
			case 7:
			{
				goto IL_0089;
			}
		}
	}
	{
		return;
	}

IL_002e:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_2 = __this->get_Player_1();
		float L_3 = ___value0;
		NullCheck(L_2);
		L_2->set_Clip0Weight_13(L_3);
		return;
	}

IL_003b:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_4 = __this->get_Player_1();
		float L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_Clip1Weight_15(L_5);
		return;
	}

IL_0048:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_6 = __this->get_Player_1();
		float L_7 = ___value0;
		NullCheck(L_6);
		L_6->set_Clip2Weight_17(L_7);
		return;
	}

IL_0055:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_8 = __this->get_Player_1();
		float L_9 = ___value0;
		NullCheck(L_8);
		L_8->set_Clip3Weight_19(L_9);
		return;
	}

IL_0062:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_10 = __this->get_Player_1();
		float L_11 = ___value0;
		NullCheck(L_10);
		L_10->set_Clip4Weight_21(L_11);
		return;
	}

IL_006f:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_12 = __this->get_Player_1();
		float L_13 = ___value0;
		NullCheck(L_12);
		L_12->set_Clip5Weight_23(L_13);
		return;
	}

IL_007c:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_14 = __this->get_Player_1();
		float L_15 = ___value0;
		NullCheck(L_14);
		L_14->set_Clip6Weight_25(L_15);
		return;
	}

IL_0089:
	{
		PointCachePlayer_t7B87B3882AC5058B4EA9B3B95F52743BFE95FF8F * L_16 = __this->get_Player_1();
		float L_17 = ___value0;
		NullCheck(L_16);
		L_16->set_Clip7Weight_27(L_17);
		return;
	}
}
// System.Void VertexAnimationTools_30.PointCachePlayer_Clip::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Clip__ctor_mC4F008C7214C0DBD2898D0BD82D8B30D208259B1 (Clip_t7BE3BD40802F2CA44AD5DD5155460E3A01E0AD77 * __this, int32_t ___idx0, const RuntimeMethod* method)
{
	{
		__this->set_DurationInSeconds_3((1.0f));
		__this->set_MotionPathIconSize_5((0.1f));
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___idx0;
		__this->set_Idx_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VertexAnimationTools_30.PointCachePlayer_Constraint::Apply(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR void Constraint_Apply_m96292A52D04762BB78C857D75189C4FB61BB7A29 (Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1 * __this, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___tm0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Constraint_Apply_m96292A52D04762BB78C857D75189C4FB61BB7A29_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = __this->get_Tr_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005c;
		}
	}
	{
		PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  L_2 = __this->get_utm_2();
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_3 = ___tm0;
		PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D  L_4 = PFU_op_Multiply_mB243CBB36BAA2C76BA1AF797CB642DC9756551E2(L_2, L_3, /*hidden argument*/NULL);
		__this->set_utm_2(L_4);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = __this->get_Tr_1();
		PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D * L_6 = __this->get_address_of_utm_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = L_6->get_P_0();
		NullCheck(L_5);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_5, L_7, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = __this->get_Tr_1();
		PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D * L_9 = __this->get_address_of_utm_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = L_9->get_F_1();
		PFU_tD897B0E8199F5D54F4DBE9BFA91500B564EDB97D * L_11 = __this->get_address_of_utm_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = L_11->get_U_2();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_13 = Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672(L_10, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_8, L_13, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// System.Void VertexAnimationTools_30.PointCachePlayer_Constraint::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Constraint__ctor_mADD966646C63611E99184035B503EEC16AFF2A6E (Constraint_t5134256E17C3C59BE6CBE19E96E7369D7A88D6C1 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// VertexAnimationTools_30.ProjectionSamples VertexAnimationTools_30.ProjectionSamples::get_Get()
extern "C" IL2CPP_METHOD_ATTR ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708 * ProjectionSamples_get_Get_m97A838CD8497590553505AF036C890C1CF47B013 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ProjectionSamples_get_Get_m97A838CD8497590553505AF036C890C1CF47B013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708 * L_0 = Resources_Load_TisProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708_m2A2EAEEE2C850C84F94448E91F0164E0D48AEBEB(_stringLiteralCFFCEF8C0B9CC5209BA1949CFB2B9D52D8E67F6E, /*hidden argument*/Resources_Load_TisProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708_m2A2EAEEE2C850C84F94448E91F0164E0D48AEBEB_RuntimeMethod_var);
		return L_0;
	}
}
// System.Void VertexAnimationTools_30.ProjectionSamples::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ProjectionSamples__ctor_mDC6C91B55F44F34D1C4B68244A08AE8CCDBDCC81 (ProjectionSamples_t3F2EAB34ED2433A74795DB31B8790F3AFAC3F708 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VertexAnimationTools_30.ProjectionSamples_Samples::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Samples__ctor_m688D7C1E4B832099FA852308019CF438955895FC (Samples_tCBF54D332EEA974DDC15636D1CFC5B13179D0D08 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: VertexAnimationTools_30.TaskInfo
extern "C" void TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshal_pinvoke(const TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1& unmarshaled, TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_pinvoke& marshaled)
{
	marshaled.___Name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_Name_0());
	marshaled.___Persentage_1 = unmarshaled.get_Persentage_1();
}
extern "C" void TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshal_pinvoke_back(const TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_pinvoke& marshaled, TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1& unmarshaled)
{
	unmarshaled.set_Name_0(il2cpp_codegen_marshal_string_result(marshaled.___Name_0));
	float unmarshaled_Persentage_temp_1 = 0.0f;
	unmarshaled_Persentage_temp_1 = marshaled.___Persentage_1;
	unmarshaled.set_Persentage_1(unmarshaled_Persentage_temp_1);
}
// Conversion method for clean up from marshalling of: VertexAnimationTools_30.TaskInfo
extern "C" void TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshal_pinvoke_cleanup(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Name_0);
	marshaled.___Name_0 = NULL;
}
// Conversion methods for marshalling of: VertexAnimationTools_30.TaskInfo
extern "C" void TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshal_com(const TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1& unmarshaled, TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_com& marshaled)
{
	marshaled.___Name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Name_0());
	marshaled.___Persentage_1 = unmarshaled.get_Persentage_1();
}
extern "C" void TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshal_com_back(const TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_com& marshaled, TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1& unmarshaled)
{
	unmarshaled.set_Name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___Name_0));
	float unmarshaled_Persentage_temp_1 = 0.0f;
	unmarshaled_Persentage_temp_1 = marshaled.___Persentage_1;
	unmarshaled.set_Persentage_1(unmarshaled_Persentage_temp_1);
}
// Conversion method for clean up from marshalling of: VertexAnimationTools_30.TaskInfo
extern "C" void TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshal_com_cleanup(TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___Name_0);
	marshaled.___Name_0 = NULL;
}
// System.Void VertexAnimationTools_30.TaskInfo::.ctor(System.String,System.Single)
extern "C" IL2CPP_METHOD_ATTR void TaskInfo__ctor_mB7A07F8C9C02C3B33CF3D47A267FD0FE94F31540 (TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 * __this, String_t* ___name0, float ___persentage1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		__this->set_Name_0(L_0);
		float L_1 = ___persentage1;
		__this->set_Persentage_1(L_1);
		return;
	}
}
extern "C"  void TaskInfo__ctor_mB7A07F8C9C02C3B33CF3D47A267FD0FE94F31540_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, float ___persentage1, const RuntimeMethod* method)
{
	TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 * _thisAdjusted = reinterpret_cast<TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1 *>(__this + 1);
	TaskInfo__ctor_mB7A07F8C9C02C3B33CF3D47A267FD0FE94F31540(_thisAdjusted, ___name0, ___persentage1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// VertexAnimationTools_30.TasksStack_Task VertexAnimationTools_30.TasksStack::get_Item(System.String)
extern "C" IL2CPP_METHOD_ATTR Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * TasksStack_get_Item_mB6EAC86E6B6379FDCA79684C53755A0B56B1E021 (TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TasksStack_get_Item_mB6EAC86E6B6379FDCA79684C53755A0B56B1E021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * L_0 = __this->get_itemsDict_2();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_2 = Dictionary_2_get_Item_mB37BDDEB7C29FEC675826E3C374316BFBBB480C9(L_0, L_1, /*hidden argument*/Dictionary_2_get_Item_mB37BDDEB7C29FEC675826E3C374316BFBBB480C9_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void VertexAnimationTools_30.TasksStack::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void TasksStack__ctor_m06BB2D082AEC6B7B2F2ED6C6F5DDE952519F6E58 (TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TasksStack__ctor_m06BB2D082AEC6B7B2F2ED6C6F5DDE952519F6E58_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * L_0 = (List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 *)il2cpp_codegen_object_new(List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23_il2cpp_TypeInfo_var);
		List_1__ctor_m6F104AD5D8FB6EFF1B7453916727EBB430214437(L_0, /*hidden argument*/List_1__ctor_m6F104AD5D8FB6EFF1B7453916727EBB430214437_RuntimeMethod_var);
		__this->set_items_1(L_0);
		Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * L_1 = (Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 *)il2cpp_codegen_object_new(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m59A3A2601326E56C3EE4E802E8F0E9CA09A0C299(L_1, /*hidden argument*/Dictionary_2__ctor_m59A3A2601326E56C3EE4E802E8F0E9CA09A0C299_RuntimeMethod_var);
		__this->set_itemsDict_2(L_1);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___name0;
		__this->set_Name_0(L_2);
		Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * L_3 = (Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 *)il2cpp_codegen_object_new(Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m59A3A2601326E56C3EE4E802E8F0E9CA09A0C299(L_3, /*hidden argument*/Dictionary_2__ctor_m59A3A2601326E56C3EE4E802E8F0E9CA09A0C299_RuntimeMethod_var);
		__this->set_itemsDict_2(L_3);
		return;
	}
}
// System.Void VertexAnimationTools_30.TasksStack::Add(System.String,System.Single)
extern "C" IL2CPP_METHOD_ATTR void TasksStack_Add_mDB3AF7F713CCA6D7D9B83957F3A70160B5A5590F (TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * __this, String_t* ___name0, float ___iterations1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TasksStack_Add_mDB3AF7F713CCA6D7D9B83957F3A70160B5A5590F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * V_0 = NULL;
	Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * V_1 = NULL;
	{
		V_0 = (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 *)NULL;
		Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * L_0 = __this->get_itemsDict_2();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m831F11AC81E0F04B86DB2A62B1CDD21CB47B2FE0(L_0, L_1, (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m831F11AC81E0F04B86DB2A62B1CDD21CB47B2FE0_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_0039;
		}
	}
	{
		String_t* L_3 = ___name0;
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_4 = (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 *)il2cpp_codegen_object_new(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4_il2cpp_TypeInfo_var);
		Task__ctor_m8ECD07E86F37B5FFFAD7AC598B6B7457BA0C1AA7(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_5 = V_1;
		float L_6 = ___iterations1;
		NullCheck(L_5);
		L_5->set_Iterations_1(L_6);
		Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * L_7 = __this->get_itemsDict_2();
		String_t* L_8 = ___name0;
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_9 = V_1;
		NullCheck(L_7);
		Dictionary_2_Add_mA7F5F5DAFC9B5AEF2107CB1B9809F1A812BF4D92(L_7, L_8, L_9, /*hidden argument*/Dictionary_2_Add_mA7F5F5DAFC9B5AEF2107CB1B9809F1A812BF4D92_RuntimeMethod_var);
		List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * L_10 = __this->get_items_1();
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_11 = V_1;
		NullCheck(L_10);
		List_1_Add_mFD28069FE280F9BC204CD8EAB6B2938CB1FC7FA9(L_10, L_11, /*hidden argument*/List_1_Add_mFD28069FE280F9BC204CD8EAB6B2938CB1FC7FA9_RuntimeMethod_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void VertexAnimationTools_30.TasksStack::Add(System.String,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void TasksStack_Add_mBFCE89C6026C5FE8F2C52924F26C54914F4988C4 (TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * __this, String_t* ___name0, float ___iterations1, float ___weight2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TasksStack_Add_mBFCE89C6026C5FE8F2C52924F26C54914F4988C4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * V_0 = NULL;
	Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * V_1 = NULL;
	{
		V_0 = (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 *)NULL;
		Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * L_0 = __this->get_itemsDict_2();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m831F11AC81E0F04B86DB2A62B1CDD21CB47B2FE0(L_0, L_1, (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m831F11AC81E0F04B86DB2A62B1CDD21CB47B2FE0_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_0040;
		}
	}
	{
		String_t* L_3 = ___name0;
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_4 = (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 *)il2cpp_codegen_object_new(Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4_il2cpp_TypeInfo_var);
		Task__ctor_m8ECD07E86F37B5FFFAD7AC598B6B7457BA0C1AA7(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_5 = V_1;
		float L_6 = ___iterations1;
		NullCheck(L_5);
		L_5->set_Iterations_1(L_6);
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_7 = V_1;
		float L_8 = ___weight2;
		NullCheck(L_7);
		L_7->set_Weight_4(L_8);
		Dictionary_2_tD9DD7B2D90FA6A95E876AD7293733ED74CE678D5 * L_9 = __this->get_itemsDict_2();
		String_t* L_10 = ___name0;
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_11 = V_1;
		NullCheck(L_9);
		Dictionary_2_Add_mA7F5F5DAFC9B5AEF2107CB1B9809F1A812BF4D92(L_9, L_10, L_11, /*hidden argument*/Dictionary_2_Add_mA7F5F5DAFC9B5AEF2107CB1B9809F1A812BF4D92_RuntimeMethod_var);
		List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * L_12 = __this->get_items_1();
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_13 = V_1;
		NullCheck(L_12);
		List_1_Add_mFD28069FE280F9BC204CD8EAB6B2938CB1FC7FA9(L_12, L_13, /*hidden argument*/List_1_Add_mFD28069FE280F9BC204CD8EAB6B2938CB1FC7FA9_RuntimeMethod_var);
	}

IL_0040:
	{
		return;
	}
}
// System.Void VertexAnimationTools_30.TasksStack::Normalize()
extern "C" IL2CPP_METHOD_ATTR void TasksStack_Normalize_m86E6117ABBCC03C07B5015447A99C71B9857E127 (TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TasksStack_Normalize_m86E6117ABBCC03C07B5015447A99C71B9857E127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = (0.0f);
		V_1 = 0;
		goto IL_0046;
	}

IL_000a:
	{
		List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * L_0 = __this->get_items_1();
		int32_t L_1 = V_1;
		NullCheck(L_0);
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_2 = List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80(L_0, L_1, /*hidden argument*/List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80_RuntimeMethod_var);
		float L_3 = V_0;
		NullCheck(L_2);
		L_2->set_NormalizedFrom_2(L_3);
		float L_4 = V_0;
		List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * L_5 = __this->get_items_1();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_7 = List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80(L_5, L_6, /*hidden argument*/List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80_RuntimeMethod_var);
		NullCheck(L_7);
		float L_8 = L_7->get_Weight_4();
		V_0 = ((float)il2cpp_codegen_add((float)L_4, (float)L_8));
		List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * L_9 = __this->get_items_1();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_11 = List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80(L_9, L_10, /*hidden argument*/List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80_RuntimeMethod_var);
		float L_12 = V_0;
		NullCheck(L_11);
		L_11->set_NormalizedTo_3(L_12);
		int32_t L_13 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0046:
	{
		int32_t L_14 = V_1;
		List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * L_15 = __this->get_items_1();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m9B7BF65FC57179097215D9CE925549BD2E722E67(L_15, /*hidden argument*/List_1_get_Count_m9B7BF65FC57179097215D9CE925549BD2E722E67_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_000a;
		}
	}
	{
		V_2 = 0;
		goto IL_008e;
	}

IL_0058:
	{
		List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * L_17 = __this->get_items_1();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_19 = List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80(L_17, L_18, /*hidden argument*/List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80_RuntimeMethod_var);
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_20 = L_19;
		NullCheck(L_20);
		float L_21 = L_20->get_NormalizedFrom_2();
		float L_22 = V_0;
		NullCheck(L_20);
		L_20->set_NormalizedFrom_2(((float)((float)L_21/(float)L_22)));
		List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * L_23 = __this->get_items_1();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_25 = List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80(L_23, L_24, /*hidden argument*/List_1_get_Item_m71B0BE55A21F05C361948AB72624AA17A4F82F80_RuntimeMethod_var);
		Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * L_26 = L_25;
		NullCheck(L_26);
		float L_27 = L_26->get_NormalizedTo_3();
		float L_28 = V_0;
		NullCheck(L_26);
		L_26->set_NormalizedTo_3(((float)((float)L_27/(float)L_28)));
		int32_t L_29 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_008e:
	{
		int32_t L_30 = V_2;
		List_1_t0B4B303294DA4BE8DA6E974FBB866FFE0C252D23 * L_31 = __this->get_items_1();
		NullCheck(L_31);
		int32_t L_32 = List_1_get_Count_m9B7BF65FC57179097215D9CE925549BD2E722E67(L_31, /*hidden argument*/List_1_get_Count_m9B7BF65FC57179097215D9CE925549BD2E722E67_RuntimeMethod_var);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_0058;
		}
	}
	{
		return;
	}
}
// VertexAnimationTools_30.TaskInfo VertexAnimationTools_30.TasksStack::get_Done()
extern "C" IL2CPP_METHOD_ATTR TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  TasksStack_get_Done_mF66017E24AF36ACC7379FD09898D29C35A8558A8 (TasksStack_tA7E298F5409030E4A9DB41C8074552889B7B927F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TasksStack_get_Done_mF66017E24AF36ACC7379FD09898D29C35A8558A8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_Name_0();
		String_t* L_1 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(_stringLiteralF76CF85AB6A67B58748F6C3DDDA4898D6687E9FF, L_0, /*hidden argument*/NULL);
		TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  L_2;
		memset(&L_2, 0, sizeof(L_2));
		TaskInfo__ctor_mB7A07F8C9C02C3B33CF3D47A267FD0FE94F31540((&L_2), L_1, (1.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VertexAnimationTools_30.TasksStack_Task::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void Task__ctor_m8ECD07E86F37B5FFFAD7AC598B6B7457BA0C1AA7 (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		__this->set_Weight_4((1.0f));
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_0(L_0);
		return;
	}
}
// VertexAnimationTools_30.TaskInfo VertexAnimationTools_30.TasksStack_Task::GetInfo(System.Int32)
extern "C" IL2CPP_METHOD_ATTR TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  Task_GetInfo_m9CABACF1E820BDB7746FCD6B1EC092711791B097 (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * __this, int32_t ___iteration0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_GetInfo_m9CABACF1E820BDB7746FCD6B1EC092711791B097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		int32_t L_0 = ___iteration0;
		float L_1 = __this->get_Iterations_1();
		V_0 = ((float)((float)(((float)((float)L_0)))/(float)(((float)((float)L_1)))));
		float L_2 = __this->get_NormalizedFrom_2();
		float L_3 = __this->get_NormalizedTo_3();
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364(L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = __this->get_Name_0();
		float L_7 = V_0;
		V_2 = ((float)il2cpp_codegen_multiply((float)L_7, (float)(100.0f)));
		String_t* L_8 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)(&V_2), _stringLiteral2A7BC94A06F3221293677515044B0A9DD3960F4E, /*hidden argument*/NULL);
		String_t* L_9 = String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A(_stringLiteral7E7DA0C90224CF487241A587C1FC53AF063CA8AA, L_6, L_8, /*hidden argument*/NULL);
		float L_10 = V_1;
		TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  L_11;
		memset(&L_11, 0, sizeof(L_11));
		TaskInfo__ctor_mB7A07F8C9C02C3B33CF3D47A267FD0FE94F31540((&L_11), L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// VertexAnimationTools_30.TaskInfo VertexAnimationTools_30.TasksStack_Task::GetInfo(System.Single)
extern "C" IL2CPP_METHOD_ATTR TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  Task_GetInfo_m23A2566AA31B9C73E88294BF26F8C643F6830CDC (Task_tC8A5CB4A9B28C51BA104671ACA5EEC24D49267E4 * __this, float ___localPersentage0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_GetInfo_m23A2566AA31B9C73E88294BF26F8C643F6830CDC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_NormalizedFrom_2();
		float L_1 = __this->get_NormalizedTo_3();
		float L_2 = ___localPersentage0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = __this->get_Name_0();
		float L_5 = ___localPersentage0;
		V_1 = ((float)il2cpp_codegen_multiply((float)L_5, (float)(100.0f)));
		String_t* L_6 = Single_ToString_m211B5E14B0C60667C56838D5C3788CFA58670DDE((float*)(&V_1), _stringLiteral2A7BC94A06F3221293677515044B0A9DD3960F4E, /*hidden argument*/NULL);
		String_t* L_7 = String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A(_stringLiteral7E7DA0C90224CF487241A587C1FC53AF063CA8AA, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = V_0;
		TaskInfo_t3FBF9A10362590974F251CE28A2FD4FFB8F778F1  L_9;
		memset(&L_9, 0, sizeof(L_9));
		TaskInfo__ctor_mB7A07F8C9C02C3B33CF3D47A267FD0FE94F31540((&L_9), L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VertexAnimationTools_30.VertexAnimationToolsAssetBase::SetMaterialsUnused()
extern "C" IL2CPP_METHOD_ATTR void VertexAnimationToolsAssetBase_SetMaterialsUnused_mAEF7968AD62EDE7FB3203DA91FCC88F2D72B9E64 (VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VertexAnimationToolsAssetBase_SetMaterialsUnused_mAEF7968AD62EDE7FB3203DA91FCC88F2D72B9E64_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001a;
	}

IL_0004:
	{
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_0 = __this->get_Materials_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_2 = List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268(L_0, L_1, /*hidden argument*/List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268_RuntimeMethod_var);
		NullCheck(L_2);
		L_2->set_Used_1((bool)0);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_001a:
	{
		int32_t L_4 = V_0;
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_5 = __this->get_Materials_4();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00(L_5, /*hidden argument*/List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00_RuntimeMethod_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// UnityEngine.Material[] VertexAnimationTools_30.VertexAnimationToolsAssetBase::GetPolygonalMeshMaterials(System.String[])
extern "C" IL2CPP_METHOD_ATTR MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* VertexAnimationToolsAssetBase_GetPolygonalMeshMaterials_m83BBF1EB87A4490009966B24594D13A129A0E093 (VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252 * __this, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___submeshesNames0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VertexAnimationToolsAssetBase_GetPolygonalMeshMaterials_m83BBF1EB87A4490009966B24594D13A129A0E093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * V_0 = NULL;
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* V_1 = NULL;
	int32_t V_2 = 0;
	MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_0 = (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA *)il2cpp_codegen_object_new(List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA_il2cpp_TypeInfo_var);
		List_1__ctor_m75AFAA3C830D844D785A019FE6D4E3A71344B7BD(L_0, /*hidden argument*/List_1__ctor_m75AFAA3C830D844D785A019FE6D4E3A71344B7BD_RuntimeMethod_var);
		V_0 = L_0;
		V_2 = 0;
		goto IL_0082;
	}

IL_000a:
	{
		V_3 = (MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 *)NULL;
		V_4 = 0;
		goto IL_0041;
	}

IL_0011:
	{
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_1 = __this->get_Materials_4();
		int32_t L_2 = V_4;
		NullCheck(L_1);
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_3 = List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268(L_1, L_2, /*hidden argument*/List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268_RuntimeMethod_var);
		NullCheck(L_3);
		String_t* L_4 = L_3->get_Name_2();
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = ___submeshesNames0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		bool L_9 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_4, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003b;
		}
	}
	{
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_10 = __this->get_Materials_4();
		int32_t L_11 = V_4;
		NullCheck(L_10);
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_12 = List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268(L_10, L_11, /*hidden argument*/List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268_RuntimeMethod_var);
		V_3 = L_12;
	}

IL_003b:
	{
		int32_t L_13 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0041:
	{
		int32_t L_14 = V_4;
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_15 = __this->get_Materials_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00(L_15, /*hidden argument*/List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0011;
		}
	}
	{
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_17 = V_3;
		if (L_17)
		{
			goto IL_0070;
		}
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_18 = ___submeshesNames0;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		String_t* L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_22 = ___submeshesNames0;
		int32_t L_23 = V_2;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		String_t* L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_26 = Extension_GetRandomMaterial_m207F4452B34C29779CB8E896C4C79EA47A512D22(L_25, /*hidden argument*/NULL);
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_27 = (MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 *)il2cpp_codegen_object_new(MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0_il2cpp_TypeInfo_var);
		MaterialInfo__ctor_mA7FFEB75A85A1C260304A806EC1389A8B018990E(L_27, L_21, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_28 = __this->get_Materials_4();
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_29 = V_3;
		NullCheck(L_28);
		List_1_Add_mA28790AE5AFF26DBF13EB5B4E1B0B7954E1D3ED1(L_28, L_29, /*hidden argument*/List_1_Add_mA28790AE5AFF26DBF13EB5B4E1B0B7954E1D3ED1_RuntimeMethod_var);
	}

IL_0070:
	{
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_30 = V_3;
		NullCheck(L_30);
		L_30->set_Used_1((bool)1);
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_31 = V_0;
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_32 = V_3;
		NullCheck(L_31);
		List_1_Add_mA28790AE5AFF26DBF13EB5B4E1B0B7954E1D3ED1(L_31, L_32, /*hidden argument*/List_1_Add_mA28790AE5AFF26DBF13EB5B4E1B0B7954E1D3ED1_RuntimeMethod_var);
		int32_t L_33 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_0082:
	{
		int32_t L_34 = V_2;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_35 = ___submeshesNames0;
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length)))))))
		{
			goto IL_000a;
		}
	}
	{
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_36 = V_0;
		NullCheck(L_36);
		int32_t L_37 = List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00(L_36, /*hidden argument*/List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00_RuntimeMethod_var);
		MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* L_38 = (MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398*)SZArrayNew(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398_il2cpp_TypeInfo_var, (uint32_t)L_37);
		V_1 = L_38;
		V_5 = 0;
		goto IL_00b0;
	}

IL_0099:
	{
		MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* L_39 = V_1;
		int32_t L_40 = V_5;
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_41 = V_0;
		int32_t L_42 = V_5;
		NullCheck(L_41);
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_43 = List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268(L_41, L_42, /*hidden argument*/List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268_RuntimeMethod_var);
		NullCheck(L_43);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_44 = L_43->get_Mat_0();
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, L_44);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(L_40), (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)L_44);
		int32_t L_45 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
	}

IL_00b0:
	{
		int32_t L_46 = V_5;
		MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* L_47 = V_1;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_47)->max_length)))))))
		{
			goto IL_0099;
		}
	}
	{
		MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* L_48 = V_1;
		return L_48;
	}
}
// System.Void VertexAnimationTools_30.VertexAnimationToolsAssetBase::ClearUnusedMaterials()
extern "C" IL2CPP_METHOD_ATTR void VertexAnimationToolsAssetBase_ClearUnusedMaterials_m0BC8523B36E38D380843F02DF85B3BC4C691E088 (VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VertexAnimationToolsAssetBase_ClearUnusedMaterials_m0BC8523B36E38D380843F02DF85B3BC4C691E088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_0 = (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA *)il2cpp_codegen_object_new(List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA_il2cpp_TypeInfo_var);
		List_1__ctor_m75AFAA3C830D844D785A019FE6D4E3A71344B7BD(L_0, /*hidden argument*/List_1__ctor_m75AFAA3C830D844D785A019FE6D4E3A71344B7BD_RuntimeMethod_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0033;
	}

IL_000a:
	{
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_1 = __this->get_Materials_4();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_3 = List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268(L_1, L_2, /*hidden argument*/List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268_RuntimeMethod_var);
		NullCheck(L_3);
		bool L_4 = L_3->get_Used_1();
		if (L_4)
		{
			goto IL_002f;
		}
	}
	{
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_5 = V_0;
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_6 = __this->get_Materials_4();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_8 = List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268(L_6, L_7, /*hidden argument*/List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268_RuntimeMethod_var);
		NullCheck(L_5);
		List_1_Add_mA28790AE5AFF26DBF13EB5B4E1B0B7954E1D3ED1(L_5, L_8, /*hidden argument*/List_1_Add_mA28790AE5AFF26DBF13EB5B4E1B0B7954E1D3ED1_RuntimeMethod_var);
	}

IL_002f:
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0033:
	{
		int32_t L_10 = V_1;
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_11 = __this->get_Materials_4();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00(L_11, /*hidden argument*/List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00_RuntimeMethod_var);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_000a;
		}
	}
	{
		V_2 = 0;
		goto IL_005c;
	}

IL_0045:
	{
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_13 = __this->get_Materials_4();
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_14 = V_0;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		MaterialInfo_tEF013480CA76F50AA70D3CD1C0AB40AFBAF2BEE0 * L_16 = List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268(L_14, L_15, /*hidden argument*/List_1_get_Item_m9A8500AC4B06C7B0941F322D7BAD329B17B4B268_RuntimeMethod_var);
		NullCheck(L_13);
		List_1_Remove_m5768F6A9F78F566E53AB5ECAAF5BCDB414C3B6B3(L_13, L_16, /*hidden argument*/List_1_Remove_m5768F6A9F78F566E53AB5ECAAF5BCDB414C3B6B3_RuntimeMethod_var);
		int32_t L_17 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
	}

IL_005c:
	{
		int32_t L_18 = V_2;
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00(L_19, /*hidden argument*/List_1_get_Count_m4B99F65618B5C4BF0DCB6A74C6AE685096DBDB00_RuntimeMethod_var);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_0045;
		}
	}
	{
		return;
	}
}
// System.Void VertexAnimationTools_30.VertexAnimationToolsAssetBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void VertexAnimationToolsAssetBase__ctor_mDB19E63561EF65DDFD687C5692057B4F4CEF4C76 (VertexAnimationToolsAssetBase_t24D12B84EDE085A896DF88C598AEA5C55A648252 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VertexAnimationToolsAssetBase__ctor_mDB19E63561EF65DDFD687C5692057B4F4CEF4C76_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA * L_0 = (List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA *)il2cpp_codegen_object_new(List_1_t5CEAB939FA41C2E3BEF0E6A506DB64576F6617AA_il2cpp_TypeInfo_var);
		List_1__ctor_m75AFAA3C830D844D785A019FE6D4E3A71344B7BD(L_0, /*hidden argument*/List_1__ctor_m75AFAA3C830D844D785A019FE6D4E3A71344B7BD_RuntimeMethod_var);
		__this->set_Materials_4(L_0);
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
