﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.LinkedListNode`1<System.Action>>
struct Dictionary_2_tEEF4B5BCE22F1668633B79E278CE326EA9D28B63;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int64>
struct Dictionary_2_t00E84574188532E4E3DB0BF26562D075ADA48AA4;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_ColorGradient>
struct Dictionary_2_t017C8BD76A9A6FD565D481CAA080853B4FA9DB23;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset>
struct Dictionary_2_t54358ACD47B384266FFF50A73EACF8D8AB65CF8B;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_SpriteAsset>
struct Dictionary_2_t0AE55103C4518DDF10B2F3EE31CAC5B25156DC4B;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material>
struct Dictionary_2_t7931B625094E53EE02F381E1D3390C4C897A927A;
// System.Collections.Generic.Dictionary`2<System.Int64,TMPro.TMP_GlyphPairAdjustmentRecord>
struct Dictionary_2_t736E19399CC4C28863016ECA01A6057F0EABE00D;
// System.Collections.Generic.Dictionary`2<System.Int64,TMPro.TMP_MaterialManager/FallbackMaterial>
struct Dictionary_2_t14BA3821BDA3D5259DC66711E828D3A074A27DA1;
// System.Collections.Generic.Dictionary`2<System.Single,TriLib.MorphChannelKey>
struct Dictionary_2_t27753A6CEA8485F892B2780AD0C8A72CCA018B0E;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,TriLib.AnimationCurveData>
struct Dictionary_2_t9B2B5688BDEECA5CF9151662B25CF2D335714228;
// System.Collections.Generic.Dictionary`2<System.String,TriLib.EmbeddedTextureData>
struct Dictionary_2_t57B3F18F00761BE67616EC3A88E4A26E19DE1B32;
// System.Collections.Generic.Dictionary`2<System.String,TriLib.MeshData>
struct Dictionary_2_t9D6D0891CDB41250259DE1A6829B3762986E4704;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_t722ECF723B462DCDB0A863F3FA84E47F2040B34C;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_t48F9E7887D14BA0E81A2775FD742FAB95CADAF85;
// System.Collections.Generic.Dictionary`2<System.UInt32,TMPro.TMP_Character>
struct Dictionary_2_tE7F15226C09DF54159023A3FC4A77F9997A61F1B;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph>
struct Dictionary_2_t572EE3ED0678E28D8D2E0199F7C33E24756FB72B;
// System.Collections.Generic.Dictionary`2<UnityEngine.SkinnedMeshRenderer,System.Collections.Generic.IList`1<System.String>>
struct Dictionary_2_t09D5586CFD781770994E6AE3F93F97A102BAF885;
// System.Collections.Generic.LinkedList`1<System.Action>
struct LinkedList_1_tDEDBF25BF91255B38D62FD7E168D23843F11D6F7;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>
struct List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E;
// System.Collections.Generic.List`1<TMPro.KerningPair>
struct List_1_tF4974BC7657A5F386511EB2B02DC211FF4039A20;
// System.Collections.Generic.List`1<TMPro.TMP_Character>
struct List_1_tA95ABBEB9024057D7EEFADA755FABA1E3150CBED;
// System.Collections.Generic.List`1<TMPro.TMP_FontAsset>
struct List_1_t746C622521747C7842E2C567F304B446EA74B8BB;
// System.Collections.Generic.List`1<TMPro.TMP_Glyph>
struct List_1_tB2A7609CA52574815578619F788242AB43EC2C82;
// System.Collections.Generic.List`1<TMPro.TMP_GlyphPairAdjustmentRecord>
struct List_1_t549D1117445C77EA93641A5A895FE1034EC82BAA;
// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/FallbackMaterial>
struct List_1_t08C0C72FABB903BE7B01CA5BB25FCC397D7C819F;
// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/MaskingMaterial>
struct List_1_t59BF25C313C761CCDFC872F15471E2BAB18D53A6;
// System.Collections.Generic.List`1<TriLib.AssetAdvancedConfig>
struct List_1_t982F69BB2BFD78219D910A25530B5A9B31ABD5CF;
// System.Collections.Generic.List`1<UnityEngine.TextCore.Glyph>
struct List_1_t8B3AA8D740B2E10383225037638055610A7BE123;
// System.Collections.Generic.List`1<UnityEngine.TextCore.GlyphRect>
struct List_1_tD87292C3DA9A1BCF7BE7A6A63897ABF69A015D65;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Exception
struct Exception_t;
// System.Func`2<TMPro.TMP_Character,System.UInt32>
struct Func_2_t0301B23E6BA8AC0733B9A2ACA6C15C4ECA0B076D;
// System.Func`2<UnityEngine.TextCore.Glyph,System.UInt32>
struct Func_2_t7242E2316E84C21FAC84FE158374AC6EA8DE2287;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.FaceInfo_Legacy
struct FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E;
// TMPro.FontWeight[]
struct FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446;
// TMPro.KerningTable
struct KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B;
// TMPro.RichTextTagAttribute[]
struct RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652;
// TMPro.TMP_Character
struct TMP_Character_t1875AACA978396521498D6A699052C187903553D;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C;
// TMPro.TMP_FontFeatureTable
struct TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756;
// TMPro.TMP_FontWeightPair[]
struct TMP_FontWeightPairU5BU5D_tD4C8F5F8465CC6A30370C93F43B43BE3147DA68D;
// TMPro.TMP_Settings/LineBreakingTable
struct LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487;
// TMPro.TMP_StyleSheet
struct TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04;
// TMPro.TMP_SubMeshUI[]
struct TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E;
// TMPro.TMP_SubMesh[]
struct TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD;
// TMPro.TMP_Text/UnicodeChar[]
struct UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505;
// TMPro.TMP_TextElement
struct TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B;
// TMPro.TextMeshPro
struct TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2;
// TriLib.AnimationChannelData[]
struct AnimationChannelDataU5BU5D_tD32C463829B8FA9E6FA5C7F9CE6ACDD5258961F5;
// TriLib.AnimationClipCreatedHandle
struct AnimationClipCreatedHandle_t13B0579E3C7ABBF0AF164928743D86D52FA5016F;
// TriLib.AnimationData[]
struct AnimationDataU5BU5D_tBFB48E31561CE499886A3C833130A229E1C2D51E;
// TriLib.AssetLoaderAsync
struct AssetLoaderAsync_t03A7B7BBBDC2EC8425B8FEE2D6A695A8156C1870;
// TriLib.AssetLoaderOptions
struct AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531;
// TriLib.AssimpInterop/DataCallback
struct DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF;
// TriLib.AssimpInterop/ExistsCallback
struct ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F;
// TriLib.AssimpInterop/ProgressCallback
struct ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6;
// TriLib.AssimpMetadata[]
struct AssimpMetadataU5BU5D_t43651D4F106B3744AB588280B857BF3E0A53B3FF;
// TriLib.AvatarCreatedHandle
struct AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80;
// TriLib.BlendShapeKeyCreatedHandle
struct BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B;
// TriLib.CameraData[]
struct CameraDataU5BU5D_tE0068A647898485A8BD2861CAC5A1D0589A07EBE;
// TriLib.ConcurrentList`1<TriLib.FileLoadData>
struct ConcurrentList_1_t0A359FE7FD9FC9BFD249E433775F3929A72DEC58;
// TriLib.DataDisposalCallback
struct DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB;
// TriLib.EmbeddedTextureData
struct EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915;
// TriLib.EmbeddedTextureLoadCallback
struct EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7;
// TriLib.MaterialCreatedHandle
struct MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78;
// TriLib.MaterialData[]
struct MaterialDataU5BU5D_t96A25DA908C2869FFE5C2BC523A70E0B0460184F;
// TriLib.MeshCreatedHandle
struct MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3;
// TriLib.MeshData[]
struct MeshDataU5BU5D_tE36D464408234701C2529B85B6C88378A9E03200;
// TriLib.MetadataProcessedHandle
struct MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E;
// TriLib.MorphChannelData[]
struct MorphChannelDataU5BU5D_tAF1809268F138ACD333A34B61857DDD7180374AA;
// TriLib.MorphData[]
struct MorphDataU5BU5D_tF75ABC4B00D9F56D8CF9E690DD05E653D090600E;
// TriLib.NodeData
struct NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C;
// TriLib.NodeData[]
struct NodeDataU5BU5D_t2903451E388B76A240D86BC410A23B2EF74CF920;
// TriLib.ObjectLoadedHandle
struct ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044;
// TriLib.TextureLoadHandle
struct TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D;
// UnityEngine.AnimationClip
struct AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Avatar
struct Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B;
// UnityEngine.BoneWeight[]
struct BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF4DC3E9BD9E6DB77FFF7BDC0B1545B5D6071513D;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD;
// UnityEngine.TextAsset
struct TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E;
// UnityEngine.TextCore.Glyph
struct Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;

struct Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 ;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D ;
struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 ;
struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E ;



#ifndef U3CMODULEU3E_TB181AC380D679257A9DFE61F653774B69E5DFF6D_H
#define U3CMODULEU3E_TB181AC380D679257A9DFE61F653774B69E5DFF6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB181AC380D679257A9DFE61F653774B69E5DFF6D 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TB181AC380D679257A9DFE61F653774B69E5DFF6D_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef FACEINFO_LEGACY_TA5B0942ED5875808552FE732238217F6CF70027E_H
#define FACEINFO_LEGACY_TA5B0942ED5875808552FE732238217F6CF70027E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FaceInfo_Legacy
struct  FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E  : public RuntimeObject
{
public:
	// System.String TMPro.FaceInfo_Legacy::Name
	String_t* ___Name_0;
	// System.Single TMPro.FaceInfo_Legacy::PointSize
	float ___PointSize_1;
	// System.Single TMPro.FaceInfo_Legacy::Scale
	float ___Scale_2;
	// System.Int32 TMPro.FaceInfo_Legacy::CharacterCount
	int32_t ___CharacterCount_3;
	// System.Single TMPro.FaceInfo_Legacy::LineHeight
	float ___LineHeight_4;
	// System.Single TMPro.FaceInfo_Legacy::Baseline
	float ___Baseline_5;
	// System.Single TMPro.FaceInfo_Legacy::Ascender
	float ___Ascender_6;
	// System.Single TMPro.FaceInfo_Legacy::CapHeight
	float ___CapHeight_7;
	// System.Single TMPro.FaceInfo_Legacy::Descender
	float ___Descender_8;
	// System.Single TMPro.FaceInfo_Legacy::CenterLine
	float ___CenterLine_9;
	// System.Single TMPro.FaceInfo_Legacy::SuperscriptOffset
	float ___SuperscriptOffset_10;
	// System.Single TMPro.FaceInfo_Legacy::SubscriptOffset
	float ___SubscriptOffset_11;
	// System.Single TMPro.FaceInfo_Legacy::SubSize
	float ___SubSize_12;
	// System.Single TMPro.FaceInfo_Legacy::Underline
	float ___Underline_13;
	// System.Single TMPro.FaceInfo_Legacy::UnderlineThickness
	float ___UnderlineThickness_14;
	// System.Single TMPro.FaceInfo_Legacy::strikethrough
	float ___strikethrough_15;
	// System.Single TMPro.FaceInfo_Legacy::strikethroughThickness
	float ___strikethroughThickness_16;
	// System.Single TMPro.FaceInfo_Legacy::TabWidth
	float ___TabWidth_17;
	// System.Single TMPro.FaceInfo_Legacy::Padding
	float ___Padding_18;
	// System.Single TMPro.FaceInfo_Legacy::AtlasWidth
	float ___AtlasWidth_19;
	// System.Single TMPro.FaceInfo_Legacy::AtlasHeight
	float ___AtlasHeight_20;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_PointSize_1() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___PointSize_1)); }
	inline float get_PointSize_1() const { return ___PointSize_1; }
	inline float* get_address_of_PointSize_1() { return &___PointSize_1; }
	inline void set_PointSize_1(float value)
	{
		___PointSize_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_CharacterCount_3() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___CharacterCount_3)); }
	inline int32_t get_CharacterCount_3() const { return ___CharacterCount_3; }
	inline int32_t* get_address_of_CharacterCount_3() { return &___CharacterCount_3; }
	inline void set_CharacterCount_3(int32_t value)
	{
		___CharacterCount_3 = value;
	}

	inline static int32_t get_offset_of_LineHeight_4() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___LineHeight_4)); }
	inline float get_LineHeight_4() const { return ___LineHeight_4; }
	inline float* get_address_of_LineHeight_4() { return &___LineHeight_4; }
	inline void set_LineHeight_4(float value)
	{
		___LineHeight_4 = value;
	}

	inline static int32_t get_offset_of_Baseline_5() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___Baseline_5)); }
	inline float get_Baseline_5() const { return ___Baseline_5; }
	inline float* get_address_of_Baseline_5() { return &___Baseline_5; }
	inline void set_Baseline_5(float value)
	{
		___Baseline_5 = value;
	}

	inline static int32_t get_offset_of_Ascender_6() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___Ascender_6)); }
	inline float get_Ascender_6() const { return ___Ascender_6; }
	inline float* get_address_of_Ascender_6() { return &___Ascender_6; }
	inline void set_Ascender_6(float value)
	{
		___Ascender_6 = value;
	}

	inline static int32_t get_offset_of_CapHeight_7() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___CapHeight_7)); }
	inline float get_CapHeight_7() const { return ___CapHeight_7; }
	inline float* get_address_of_CapHeight_7() { return &___CapHeight_7; }
	inline void set_CapHeight_7(float value)
	{
		___CapHeight_7 = value;
	}

	inline static int32_t get_offset_of_Descender_8() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___Descender_8)); }
	inline float get_Descender_8() const { return ___Descender_8; }
	inline float* get_address_of_Descender_8() { return &___Descender_8; }
	inline void set_Descender_8(float value)
	{
		___Descender_8 = value;
	}

	inline static int32_t get_offset_of_CenterLine_9() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___CenterLine_9)); }
	inline float get_CenterLine_9() const { return ___CenterLine_9; }
	inline float* get_address_of_CenterLine_9() { return &___CenterLine_9; }
	inline void set_CenterLine_9(float value)
	{
		___CenterLine_9 = value;
	}

	inline static int32_t get_offset_of_SuperscriptOffset_10() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___SuperscriptOffset_10)); }
	inline float get_SuperscriptOffset_10() const { return ___SuperscriptOffset_10; }
	inline float* get_address_of_SuperscriptOffset_10() { return &___SuperscriptOffset_10; }
	inline void set_SuperscriptOffset_10(float value)
	{
		___SuperscriptOffset_10 = value;
	}

	inline static int32_t get_offset_of_SubscriptOffset_11() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___SubscriptOffset_11)); }
	inline float get_SubscriptOffset_11() const { return ___SubscriptOffset_11; }
	inline float* get_address_of_SubscriptOffset_11() { return &___SubscriptOffset_11; }
	inline void set_SubscriptOffset_11(float value)
	{
		___SubscriptOffset_11 = value;
	}

	inline static int32_t get_offset_of_SubSize_12() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___SubSize_12)); }
	inline float get_SubSize_12() const { return ___SubSize_12; }
	inline float* get_address_of_SubSize_12() { return &___SubSize_12; }
	inline void set_SubSize_12(float value)
	{
		___SubSize_12 = value;
	}

	inline static int32_t get_offset_of_Underline_13() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___Underline_13)); }
	inline float get_Underline_13() const { return ___Underline_13; }
	inline float* get_address_of_Underline_13() { return &___Underline_13; }
	inline void set_Underline_13(float value)
	{
		___Underline_13 = value;
	}

	inline static int32_t get_offset_of_UnderlineThickness_14() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___UnderlineThickness_14)); }
	inline float get_UnderlineThickness_14() const { return ___UnderlineThickness_14; }
	inline float* get_address_of_UnderlineThickness_14() { return &___UnderlineThickness_14; }
	inline void set_UnderlineThickness_14(float value)
	{
		___UnderlineThickness_14 = value;
	}

	inline static int32_t get_offset_of_strikethrough_15() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___strikethrough_15)); }
	inline float get_strikethrough_15() const { return ___strikethrough_15; }
	inline float* get_address_of_strikethrough_15() { return &___strikethrough_15; }
	inline void set_strikethrough_15(float value)
	{
		___strikethrough_15 = value;
	}

	inline static int32_t get_offset_of_strikethroughThickness_16() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___strikethroughThickness_16)); }
	inline float get_strikethroughThickness_16() const { return ___strikethroughThickness_16; }
	inline float* get_address_of_strikethroughThickness_16() { return &___strikethroughThickness_16; }
	inline void set_strikethroughThickness_16(float value)
	{
		___strikethroughThickness_16 = value;
	}

	inline static int32_t get_offset_of_TabWidth_17() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___TabWidth_17)); }
	inline float get_TabWidth_17() const { return ___TabWidth_17; }
	inline float* get_address_of_TabWidth_17() { return &___TabWidth_17; }
	inline void set_TabWidth_17(float value)
	{
		___TabWidth_17 = value;
	}

	inline static int32_t get_offset_of_Padding_18() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___Padding_18)); }
	inline float get_Padding_18() const { return ___Padding_18; }
	inline float* get_address_of_Padding_18() { return &___Padding_18; }
	inline void set_Padding_18(float value)
	{
		___Padding_18 = value;
	}

	inline static int32_t get_offset_of_AtlasWidth_19() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___AtlasWidth_19)); }
	inline float get_AtlasWidth_19() const { return ___AtlasWidth_19; }
	inline float* get_address_of_AtlasWidth_19() { return &___AtlasWidth_19; }
	inline void set_AtlasWidth_19(float value)
	{
		___AtlasWidth_19 = value;
	}

	inline static int32_t get_offset_of_AtlasHeight_20() { return static_cast<int32_t>(offsetof(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E, ___AtlasHeight_20)); }
	inline float get_AtlasHeight_20() const { return ___AtlasHeight_20; }
	inline float* get_address_of_AtlasHeight_20() { return &___AtlasHeight_20; }
	inline void set_AtlasHeight_20(float value)
	{
		___AtlasHeight_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEINFO_LEGACY_TA5B0942ED5875808552FE732238217F6CF70027E_H
#ifndef FASTACTION_T270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB_H
#define FASTACTION_T270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FastAction
struct  FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<System.Action> TMPro.FastAction::delegates
	LinkedList_1_tDEDBF25BF91255B38D62FD7E168D23843F11D6F7 * ___delegates_0;
	// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.LinkedListNode`1<System.Action>> TMPro.FastAction::lookup
	Dictionary_2_tEEF4B5BCE22F1668633B79E278CE326EA9D28B63 * ___lookup_1;

public:
	inline static int32_t get_offset_of_delegates_0() { return static_cast<int32_t>(offsetof(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB, ___delegates_0)); }
	inline LinkedList_1_tDEDBF25BF91255B38D62FD7E168D23843F11D6F7 * get_delegates_0() const { return ___delegates_0; }
	inline LinkedList_1_tDEDBF25BF91255B38D62FD7E168D23843F11D6F7 ** get_address_of_delegates_0() { return &___delegates_0; }
	inline void set_delegates_0(LinkedList_1_tDEDBF25BF91255B38D62FD7E168D23843F11D6F7 * value)
	{
		___delegates_0 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_0), value);
	}

	inline static int32_t get_offset_of_lookup_1() { return static_cast<int32_t>(offsetof(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB, ___lookup_1)); }
	inline Dictionary_2_tEEF4B5BCE22F1668633B79E278CE326EA9D28B63 * get_lookup_1() const { return ___lookup_1; }
	inline Dictionary_2_tEEF4B5BCE22F1668633B79E278CE326EA9D28B63 ** get_address_of_lookup_1() { return &___lookup_1; }
	inline void set_lookup_1(Dictionary_2_tEEF4B5BCE22F1668633B79E278CE326EA9D28B63 * value)
	{
		___lookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTACTION_T270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB_H
#ifndef KERNINGTABLE_TAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_H
#define KERNINGTABLE_TAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable
struct  KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.KerningPair> TMPro.KerningTable::kerningPairs
	List_1_tF4974BC7657A5F386511EB2B02DC211FF4039A20 * ___kerningPairs_0;

public:
	inline static int32_t get_offset_of_kerningPairs_0() { return static_cast<int32_t>(offsetof(KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F, ___kerningPairs_0)); }
	inline List_1_tF4974BC7657A5F386511EB2B02DC211FF4039A20 * get_kerningPairs_0() const { return ___kerningPairs_0; }
	inline List_1_tF4974BC7657A5F386511EB2B02DC211FF4039A20 ** get_address_of_kerningPairs_0() { return &___kerningPairs_0; }
	inline void set_kerningPairs_0(List_1_tF4974BC7657A5F386511EB2B02DC211FF4039A20 * value)
	{
		___kerningPairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___kerningPairs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGTABLE_TAF8D2AABDC878598EFE90D838BAAD285FA8CE05F_H
#ifndef MATERIALREFERENCEMANAGER_TDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_H
#define MATERIALREFERENCEMANAGER_TDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReferenceManager
struct  MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> TMPro.MaterialReferenceManager::m_FontMaterialReferenceLookup
	Dictionary_2_t7931B625094E53EE02F381E1D3390C4C897A927A * ___m_FontMaterialReferenceLookup_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset> TMPro.MaterialReferenceManager::m_FontAssetReferenceLookup
	Dictionary_2_t54358ACD47B384266FFF50A73EACF8D8AB65CF8B * ___m_FontAssetReferenceLookup_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_SpriteAsset> TMPro.MaterialReferenceManager::m_SpriteAssetReferenceLookup
	Dictionary_2_t0AE55103C4518DDF10B2F3EE31CAC5B25156DC4B * ___m_SpriteAssetReferenceLookup_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_ColorGradient> TMPro.MaterialReferenceManager::m_ColorGradientReferenceLookup
	Dictionary_2_t017C8BD76A9A6FD565D481CAA080853B4FA9DB23 * ___m_ColorGradientReferenceLookup_4;

public:
	inline static int32_t get_offset_of_m_FontMaterialReferenceLookup_1() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC, ___m_FontMaterialReferenceLookup_1)); }
	inline Dictionary_2_t7931B625094E53EE02F381E1D3390C4C897A927A * get_m_FontMaterialReferenceLookup_1() const { return ___m_FontMaterialReferenceLookup_1; }
	inline Dictionary_2_t7931B625094E53EE02F381E1D3390C4C897A927A ** get_address_of_m_FontMaterialReferenceLookup_1() { return &___m_FontMaterialReferenceLookup_1; }
	inline void set_m_FontMaterialReferenceLookup_1(Dictionary_2_t7931B625094E53EE02F381E1D3390C4C897A927A * value)
	{
		___m_FontMaterialReferenceLookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontMaterialReferenceLookup_1), value);
	}

	inline static int32_t get_offset_of_m_FontAssetReferenceLookup_2() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC, ___m_FontAssetReferenceLookup_2)); }
	inline Dictionary_2_t54358ACD47B384266FFF50A73EACF8D8AB65CF8B * get_m_FontAssetReferenceLookup_2() const { return ___m_FontAssetReferenceLookup_2; }
	inline Dictionary_2_t54358ACD47B384266FFF50A73EACF8D8AB65CF8B ** get_address_of_m_FontAssetReferenceLookup_2() { return &___m_FontAssetReferenceLookup_2; }
	inline void set_m_FontAssetReferenceLookup_2(Dictionary_2_t54358ACD47B384266FFF50A73EACF8D8AB65CF8B * value)
	{
		___m_FontAssetReferenceLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontAssetReferenceLookup_2), value);
	}

	inline static int32_t get_offset_of_m_SpriteAssetReferenceLookup_3() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC, ___m_SpriteAssetReferenceLookup_3)); }
	inline Dictionary_2_t0AE55103C4518DDF10B2F3EE31CAC5B25156DC4B * get_m_SpriteAssetReferenceLookup_3() const { return ___m_SpriteAssetReferenceLookup_3; }
	inline Dictionary_2_t0AE55103C4518DDF10B2F3EE31CAC5B25156DC4B ** get_address_of_m_SpriteAssetReferenceLookup_3() { return &___m_SpriteAssetReferenceLookup_3; }
	inline void set_m_SpriteAssetReferenceLookup_3(Dictionary_2_t0AE55103C4518DDF10B2F3EE31CAC5B25156DC4B * value)
	{
		___m_SpriteAssetReferenceLookup_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpriteAssetReferenceLookup_3), value);
	}

	inline static int32_t get_offset_of_m_ColorGradientReferenceLookup_4() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC, ___m_ColorGradientReferenceLookup_4)); }
	inline Dictionary_2_t017C8BD76A9A6FD565D481CAA080853B4FA9DB23 * get_m_ColorGradientReferenceLookup_4() const { return ___m_ColorGradientReferenceLookup_4; }
	inline Dictionary_2_t017C8BD76A9A6FD565D481CAA080853B4FA9DB23 ** get_address_of_m_ColorGradientReferenceLookup_4() { return &___m_ColorGradientReferenceLookup_4; }
	inline void set_m_ColorGradientReferenceLookup_4(Dictionary_2_t017C8BD76A9A6FD565D481CAA080853B4FA9DB23 * value)
	{
		___m_ColorGradientReferenceLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorGradientReferenceLookup_4), value);
	}
};

struct MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_StaticFields
{
public:
	// TMPro.MaterialReferenceManager TMPro.MaterialReferenceManager::s_Instance
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_StaticFields, ___s_Instance_0)); }
	inline MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC * get_s_Instance_0() const { return ___s_Instance_0; }
	inline MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALREFERENCEMANAGER_TDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_H
#ifndef U3CU3EC_T94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_H
#define U3CU3EC_T94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontAsset_<>c
struct  U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_StaticFields
{
public:
	// TMPro.TMP_FontAsset_<>c TMPro.TMP_FontAsset_<>c::<>9
	U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF * ___U3CU3E9_0;
	// System.Func`2<TMPro.TMP_Character,System.UInt32> TMPro.TMP_FontAsset_<>c::<>9__100_0
	Func_2_t0301B23E6BA8AC0733B9A2ACA6C15C4ECA0B076D * ___U3CU3E9__100_0_1;
	// System.Func`2<UnityEngine.TextCore.Glyph,System.UInt32> TMPro.TMP_FontAsset_<>c::<>9__101_0
	Func_2_t7242E2316E84C21FAC84FE158374AC6EA8DE2287 * ___U3CU3E9__101_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__100_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_StaticFields, ___U3CU3E9__100_0_1)); }
	inline Func_2_t0301B23E6BA8AC0733B9A2ACA6C15C4ECA0B076D * get_U3CU3E9__100_0_1() const { return ___U3CU3E9__100_0_1; }
	inline Func_2_t0301B23E6BA8AC0733B9A2ACA6C15C4ECA0B076D ** get_address_of_U3CU3E9__100_0_1() { return &___U3CU3E9__100_0_1; }
	inline void set_U3CU3E9__100_0_1(Func_2_t0301B23E6BA8AC0733B9A2ACA6C15C4ECA0B076D * value)
	{
		___U3CU3E9__100_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__100_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__101_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_StaticFields, ___U3CU3E9__101_0_2)); }
	inline Func_2_t7242E2316E84C21FAC84FE158374AC6EA8DE2287 * get_U3CU3E9__101_0_2() const { return ___U3CU3E9__101_0_2; }
	inline Func_2_t7242E2316E84C21FAC84FE158374AC6EA8DE2287 ** get_address_of_U3CU3E9__101_0_2() { return &___U3CU3E9__101_0_2; }
	inline void set_U3CU3E9__101_0_2(Func_2_t7242E2316E84C21FAC84FE158374AC6EA8DE2287 * value)
	{
		___U3CU3E9__101_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__101_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_H
#ifndef TMP_FONTASSETUTILITIES_T5B217899A57BD221ED25A93A8E2CB2039D0EABA0_H
#define TMP_FONTASSETUTILITIES_T5B217899A57BD221ED25A93A8E2CB2039D0EABA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontAssetUtilities
struct  TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0  : public RuntimeObject
{
public:

public:
};

struct TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0_StaticFields
{
public:
	// TMPro.TMP_FontAssetUtilities TMPro.TMP_FontAssetUtilities::s_Instance
	TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0 * ___s_Instance_0;
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_FontAssetUtilities::k_SearchedFontAssets
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___k_SearchedFontAssets_1;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0_StaticFields, ___s_Instance_0)); }
	inline TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}

	inline static int32_t get_offset_of_k_SearchedFontAssets_1() { return static_cast<int32_t>(offsetof(TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0_StaticFields, ___k_SearchedFontAssets_1)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_k_SearchedFontAssets_1() const { return ___k_SearchedFontAssets_1; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_k_SearchedFontAssets_1() { return &___k_SearchedFontAssets_1; }
	inline void set_k_SearchedFontAssets_1(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___k_SearchedFontAssets_1 = value;
		Il2CppCodeGenWriteBarrier((&___k_SearchedFontAssets_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTASSETUTILITIES_T5B217899A57BD221ED25A93A8E2CB2039D0EABA0_H
#ifndef TMP_FONTFEATURETABLE_T6F3402916A5D81F2C4180CA75E04DB7A6F950756_H
#define TMP_FONTFEATURETABLE_T6F3402916A5D81F2C4180CA75E04DB7A6F950756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontFeatureTable
struct  TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_GlyphPairAdjustmentRecord> TMPro.TMP_FontFeatureTable::m_GlyphPairAdjustmentRecords
	List_1_t549D1117445C77EA93641A5A895FE1034EC82BAA * ___m_GlyphPairAdjustmentRecords_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,TMPro.TMP_GlyphPairAdjustmentRecord> TMPro.TMP_FontFeatureTable::m_GlyphPairAdjustmentRecordLookupDictionary
	Dictionary_2_t736E19399CC4C28863016ECA01A6057F0EABE00D * ___m_GlyphPairAdjustmentRecordLookupDictionary_1;

public:
	inline static int32_t get_offset_of_m_GlyphPairAdjustmentRecords_0() { return static_cast<int32_t>(offsetof(TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756, ___m_GlyphPairAdjustmentRecords_0)); }
	inline List_1_t549D1117445C77EA93641A5A895FE1034EC82BAA * get_m_GlyphPairAdjustmentRecords_0() const { return ___m_GlyphPairAdjustmentRecords_0; }
	inline List_1_t549D1117445C77EA93641A5A895FE1034EC82BAA ** get_address_of_m_GlyphPairAdjustmentRecords_0() { return &___m_GlyphPairAdjustmentRecords_0; }
	inline void set_m_GlyphPairAdjustmentRecords_0(List_1_t549D1117445C77EA93641A5A895FE1034EC82BAA * value)
	{
		___m_GlyphPairAdjustmentRecords_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlyphPairAdjustmentRecords_0), value);
	}

	inline static int32_t get_offset_of_m_GlyphPairAdjustmentRecordLookupDictionary_1() { return static_cast<int32_t>(offsetof(TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756, ___m_GlyphPairAdjustmentRecordLookupDictionary_1)); }
	inline Dictionary_2_t736E19399CC4C28863016ECA01A6057F0EABE00D * get_m_GlyphPairAdjustmentRecordLookupDictionary_1() const { return ___m_GlyphPairAdjustmentRecordLookupDictionary_1; }
	inline Dictionary_2_t736E19399CC4C28863016ECA01A6057F0EABE00D ** get_address_of_m_GlyphPairAdjustmentRecordLookupDictionary_1() { return &___m_GlyphPairAdjustmentRecordLookupDictionary_1; }
	inline void set_m_GlyphPairAdjustmentRecordLookupDictionary_1(Dictionary_2_t736E19399CC4C28863016ECA01A6057F0EABE00D * value)
	{
		___m_GlyphPairAdjustmentRecordLookupDictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlyphPairAdjustmentRecordLookupDictionary_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTFEATURETABLE_T6F3402916A5D81F2C4180CA75E04DB7A6F950756_H
#ifndef TMP_MATERIALMANAGER_T7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_H
#define TMP_MATERIALMANAGER_T7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager
struct  TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336  : public RuntimeObject
{
public:

public:
};

struct TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager_MaskingMaterial> TMPro.TMP_MaterialManager::m_materialList
	List_1_t59BF25C313C761CCDFC872F15471E2BAB18D53A6 * ___m_materialList_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,TMPro.TMP_MaterialManager_FallbackMaterial> TMPro.TMP_MaterialManager::m_fallbackMaterials
	Dictionary_2_t14BA3821BDA3D5259DC66711E828D3A074A27DA1 * ___m_fallbackMaterials_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int64> TMPro.TMP_MaterialManager::m_fallbackMaterialLookup
	Dictionary_2_t00E84574188532E4E3DB0BF26562D075ADA48AA4 * ___m_fallbackMaterialLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager_FallbackMaterial> TMPro.TMP_MaterialManager::m_fallbackCleanupList
	List_1_t08C0C72FABB903BE7B01CA5BB25FCC397D7C819F * ___m_fallbackCleanupList_3;
	// System.Boolean TMPro.TMP_MaterialManager::isFallbackListDirty
	bool ___isFallbackListDirty_4;

public:
	inline static int32_t get_offset_of_m_materialList_0() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields, ___m_materialList_0)); }
	inline List_1_t59BF25C313C761CCDFC872F15471E2BAB18D53A6 * get_m_materialList_0() const { return ___m_materialList_0; }
	inline List_1_t59BF25C313C761CCDFC872F15471E2BAB18D53A6 ** get_address_of_m_materialList_0() { return &___m_materialList_0; }
	inline void set_m_materialList_0(List_1_t59BF25C313C761CCDFC872F15471E2BAB18D53A6 * value)
	{
		___m_materialList_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialList_0), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterials_1() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields, ___m_fallbackMaterials_1)); }
	inline Dictionary_2_t14BA3821BDA3D5259DC66711E828D3A074A27DA1 * get_m_fallbackMaterials_1() const { return ___m_fallbackMaterials_1; }
	inline Dictionary_2_t14BA3821BDA3D5259DC66711E828D3A074A27DA1 ** get_address_of_m_fallbackMaterials_1() { return &___m_fallbackMaterials_1; }
	inline void set_m_fallbackMaterials_1(Dictionary_2_t14BA3821BDA3D5259DC66711E828D3A074A27DA1 * value)
	{
		___m_fallbackMaterials_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterials_1), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterialLookup_2() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields, ___m_fallbackMaterialLookup_2)); }
	inline Dictionary_2_t00E84574188532E4E3DB0BF26562D075ADA48AA4 * get_m_fallbackMaterialLookup_2() const { return ___m_fallbackMaterialLookup_2; }
	inline Dictionary_2_t00E84574188532E4E3DB0BF26562D075ADA48AA4 ** get_address_of_m_fallbackMaterialLookup_2() { return &___m_fallbackMaterialLookup_2; }
	inline void set_m_fallbackMaterialLookup_2(Dictionary_2_t00E84574188532E4E3DB0BF26562D075ADA48AA4 * value)
	{
		___m_fallbackMaterialLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterialLookup_2), value);
	}

	inline static int32_t get_offset_of_m_fallbackCleanupList_3() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields, ___m_fallbackCleanupList_3)); }
	inline List_1_t08C0C72FABB903BE7B01CA5BB25FCC397D7C819F * get_m_fallbackCleanupList_3() const { return ___m_fallbackCleanupList_3; }
	inline List_1_t08C0C72FABB903BE7B01CA5BB25FCC397D7C819F ** get_address_of_m_fallbackCleanupList_3() { return &___m_fallbackCleanupList_3; }
	inline void set_m_fallbackCleanupList_3(List_1_t08C0C72FABB903BE7B01CA5BB25FCC397D7C819F * value)
	{
		___m_fallbackCleanupList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackCleanupList_3), value);
	}

	inline static int32_t get_offset_of_isFallbackListDirty_4() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields, ___isFallbackListDirty_4)); }
	inline bool get_isFallbackListDirty_4() const { return ___isFallbackListDirty_4; }
	inline bool* get_address_of_isFallbackListDirty_4() { return &___isFallbackListDirty_4; }
	inline void set_isFallbackListDirty_4(bool value)
	{
		___isFallbackListDirty_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATERIALMANAGER_T7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_H
#ifndef FALLBACKMATERIAL_T538C842FD3863FAF785036939034732F56B2473A_H
#define FALLBACKMATERIAL_T538C842FD3863FAF785036939034732F56B2473A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager_FallbackMaterial
struct  FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_MaterialManager_FallbackMaterial::baseID
	int32_t ___baseID_0;
	// UnityEngine.Material TMPro.TMP_MaterialManager_FallbackMaterial::baseMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___baseMaterial_1;
	// System.Int64 TMPro.TMP_MaterialManager_FallbackMaterial::fallbackID
	int64_t ___fallbackID_2;
	// UnityEngine.Material TMPro.TMP_MaterialManager_FallbackMaterial::fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_3;
	// System.Int32 TMPro.TMP_MaterialManager_FallbackMaterial::count
	int32_t ___count_4;

public:
	inline static int32_t get_offset_of_baseID_0() { return static_cast<int32_t>(offsetof(FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A, ___baseID_0)); }
	inline int32_t get_baseID_0() const { return ___baseID_0; }
	inline int32_t* get_address_of_baseID_0() { return &___baseID_0; }
	inline void set_baseID_0(int32_t value)
	{
		___baseID_0 = value;
	}

	inline static int32_t get_offset_of_baseMaterial_1() { return static_cast<int32_t>(offsetof(FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A, ___baseMaterial_1)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_baseMaterial_1() const { return ___baseMaterial_1; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_baseMaterial_1() { return &___baseMaterial_1; }
	inline void set_baseMaterial_1(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___baseMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_1), value);
	}

	inline static int32_t get_offset_of_fallbackID_2() { return static_cast<int32_t>(offsetof(FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A, ___fallbackID_2)); }
	inline int64_t get_fallbackID_2() const { return ___fallbackID_2; }
	inline int64_t* get_address_of_fallbackID_2() { return &___fallbackID_2; }
	inline void set_fallbackID_2(int64_t value)
	{
		___fallbackID_2 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_3() { return static_cast<int32_t>(offsetof(FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A, ___fallbackMaterial_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fallbackMaterial_3() const { return ___fallbackMaterial_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fallbackMaterial_3() { return &___fallbackMaterial_3; }
	inline void set_fallbackMaterial_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fallbackMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_3), value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FALLBACKMATERIAL_T538C842FD3863FAF785036939034732F56B2473A_H
#ifndef MASKINGMATERIAL_TD567961933B31276005075026B5BA552CF42F30B_H
#define MASKINGMATERIAL_TD567961933B31276005075026B5BA552CF42F30B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager_MaskingMaterial
struct  MaskingMaterial_tD567961933B31276005075026B5BA552CF42F30B  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager_MaskingMaterial::baseMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___baseMaterial_0;
	// UnityEngine.Material TMPro.TMP_MaterialManager_MaskingMaterial::stencilMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___stencilMaterial_1;
	// System.Int32 TMPro.TMP_MaterialManager_MaskingMaterial::count
	int32_t ___count_2;
	// System.Int32 TMPro.TMP_MaterialManager_MaskingMaterial::stencilID
	int32_t ___stencilID_3;

public:
	inline static int32_t get_offset_of_baseMaterial_0() { return static_cast<int32_t>(offsetof(MaskingMaterial_tD567961933B31276005075026B5BA552CF42F30B, ___baseMaterial_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_baseMaterial_0() const { return ___baseMaterial_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_baseMaterial_0() { return &___baseMaterial_0; }
	inline void set_baseMaterial_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___baseMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_0), value);
	}

	inline static int32_t get_offset_of_stencilMaterial_1() { return static_cast<int32_t>(offsetof(MaskingMaterial_tD567961933B31276005075026B5BA552CF42F30B, ___stencilMaterial_1)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_stencilMaterial_1() const { return ___stencilMaterial_1; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_stencilMaterial_1() { return &___stencilMaterial_1; }
	inline void set_stencilMaterial_1(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___stencilMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(MaskingMaterial_tD567961933B31276005075026B5BA552CF42F30B, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_stencilID_3() { return static_cast<int32_t>(offsetof(MaskingMaterial_tD567961933B31276005075026B5BA552CF42F30B, ___stencilID_3)); }
	inline int32_t get_stencilID_3() const { return ___stencilID_3; }
	inline int32_t* get_address_of_stencilID_3() { return &___stencilID_3; }
	inline void set_stencilID_3(int32_t value)
	{
		___stencilID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGMATERIAL_TD567961933B31276005075026B5BA552CF42F30B_H
#ifndef TMP_TEXTELEMENT_LEGACY_T020BAF673D3D29BC2682AEA5717411BFB13C6D05_H
#define TMP_TEXTELEMENT_LEGACY_T020BAF673D3D29BC2682AEA5717411BFB13C6D05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement_Legacy
struct  TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement_Legacy::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement_Legacy::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement_Legacy::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement_Legacy::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement_Legacy::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement_Legacy::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement_Legacy::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement_Legacy::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement_Legacy::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_LEGACY_T020BAF673D3D29BC2682AEA5717411BFB13C6D05_H
#ifndef ANIMATIONCHANNELDATA_TE060663D77BD68F727028B9145CF4BAC631D5DAC_H
#define ANIMATIONCHANNELDATA_TE060663D77BD68F727028B9145CF4BAC631D5DAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AnimationChannelData
struct  AnimationChannelData_tE060663D77BD68F727028B9145CF4BAC631D5DAC  : public RuntimeObject
{
public:
	// System.String TriLib.AnimationChannelData::NodeName
	String_t* ___NodeName_0;
	// System.Collections.Generic.Dictionary`2<System.String,TriLib.AnimationCurveData> TriLib.AnimationChannelData::CurveData
	Dictionary_2_t9B2B5688BDEECA5CF9151662B25CF2D335714228 * ___CurveData_1;

public:
	inline static int32_t get_offset_of_NodeName_0() { return static_cast<int32_t>(offsetof(AnimationChannelData_tE060663D77BD68F727028B9145CF4BAC631D5DAC, ___NodeName_0)); }
	inline String_t* get_NodeName_0() const { return ___NodeName_0; }
	inline String_t** get_address_of_NodeName_0() { return &___NodeName_0; }
	inline void set_NodeName_0(String_t* value)
	{
		___NodeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___NodeName_0), value);
	}

	inline static int32_t get_offset_of_CurveData_1() { return static_cast<int32_t>(offsetof(AnimationChannelData_tE060663D77BD68F727028B9145CF4BAC631D5DAC, ___CurveData_1)); }
	inline Dictionary_2_t9B2B5688BDEECA5CF9151662B25CF2D335714228 * get_CurveData_1() const { return ___CurveData_1; }
	inline Dictionary_2_t9B2B5688BDEECA5CF9151662B25CF2D335714228 ** get_address_of_CurveData_1() { return &___CurveData_1; }
	inline void set_CurveData_1(Dictionary_2_t9B2B5688BDEECA5CF9151662B25CF2D335714228 * value)
	{
		___CurveData_1 = value;
		Il2CppCodeGenWriteBarrier((&___CurveData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCHANNELDATA_TE060663D77BD68F727028B9145CF4BAC631D5DAC_H
#ifndef ANIMATIONCURVEDATA_T5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8_H
#define ANIMATIONCURVEDATA_T5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AnimationCurveData
struct  AnimationCurveData_t5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8  : public RuntimeObject
{
public:
	// UnityEngine.Keyframe[] TriLib.AnimationCurveData::Keyframes
	KeyframeU5BU5D_tF4DC3E9BD9E6DB77FFF7BDC0B1545B5D6071513D* ___Keyframes_0;
	// System.UInt32 TriLib.AnimationCurveData::a
	uint32_t ___a_1;
	// UnityEngine.AnimationCurve TriLib.AnimationCurveData::AnimationCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___AnimationCurve_2;

public:
	inline static int32_t get_offset_of_Keyframes_0() { return static_cast<int32_t>(offsetof(AnimationCurveData_t5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8, ___Keyframes_0)); }
	inline KeyframeU5BU5D_tF4DC3E9BD9E6DB77FFF7BDC0B1545B5D6071513D* get_Keyframes_0() const { return ___Keyframes_0; }
	inline KeyframeU5BU5D_tF4DC3E9BD9E6DB77FFF7BDC0B1545B5D6071513D** get_address_of_Keyframes_0() { return &___Keyframes_0; }
	inline void set_Keyframes_0(KeyframeU5BU5D_tF4DC3E9BD9E6DB77FFF7BDC0B1545B5D6071513D* value)
	{
		___Keyframes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Keyframes_0), value);
	}

	inline static int32_t get_offset_of_a_1() { return static_cast<int32_t>(offsetof(AnimationCurveData_t5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8, ___a_1)); }
	inline uint32_t get_a_1() const { return ___a_1; }
	inline uint32_t* get_address_of_a_1() { return &___a_1; }
	inline void set_a_1(uint32_t value)
	{
		___a_1 = value;
	}

	inline static int32_t get_offset_of_AnimationCurve_2() { return static_cast<int32_t>(offsetof(AnimationCurveData_t5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8, ___AnimationCurve_2)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_AnimationCurve_2() const { return ___AnimationCurve_2; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_AnimationCurve_2() { return &___AnimationCurve_2; }
	inline void set_AnimationCurve_2(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___AnimationCurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationCurve_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCURVEDATA_T5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8_H
#ifndef D_TA4DA584627EF7D094631EDD3B87BA78B22640BF4_H
#define D_TA4DA584627EF7D094631EDD3B87BA78B22640BF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderAsync_d
struct  d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4  : public RuntimeObject
{
public:
	// TriLib.AssetLoaderAsync TriLib.AssetLoaderAsync_d::a
	AssetLoaderAsync_t03A7B7BBBDC2EC8425B8FEE2D6A695A8156C1870 * ___a_0;
	// System.Byte[] TriLib.AssetLoaderAsync_d::b
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___b_1;
	// System.String TriLib.AssetLoaderAsync_d::c
	String_t* ___c_2;
	// System.String TriLib.AssetLoaderAsync_d::d
	String_t* ___d_3;
	// TriLib.AssetLoaderOptions TriLib.AssetLoaderAsync_d::e
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531 * ___e_4;
	// System.Boolean TriLib.AssetLoaderAsync_d::f
	bool ___f_5;
	// TriLib.AssimpInterop_DataCallback TriLib.AssetLoaderAsync_d::g
	DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF * ___g_6;
	// TriLib.AssimpInterop_ExistsCallback TriLib.AssetLoaderAsync_d::h
	ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F * ___h_7;
	// TriLib.AssimpInterop_ProgressCallback TriLib.AssetLoaderAsync_d::i
	ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * ___i_8;
	// UnityEngine.GameObject TriLib.AssetLoaderAsync_d::j
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___j_9;
	// TriLib.ObjectLoadedHandle TriLib.AssetLoaderAsync_d::k
	ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * ___k_10;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___a_0)); }
	inline AssetLoaderAsync_t03A7B7BBBDC2EC8425B8FEE2D6A695A8156C1870 * get_a_0() const { return ___a_0; }
	inline AssetLoaderAsync_t03A7B7BBBDC2EC8425B8FEE2D6A695A8156C1870 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(AssetLoaderAsync_t03A7B7BBBDC2EC8425B8FEE2D6A695A8156C1870 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___b_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_b_1() const { return ___b_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___c_2)); }
	inline String_t* get_c_2() const { return ___c_2; }
	inline String_t** get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(String_t* value)
	{
		___c_2 = value;
		Il2CppCodeGenWriteBarrier((&___c_2), value);
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___d_3)); }
	inline String_t* get_d_3() const { return ___d_3; }
	inline String_t** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(String_t* value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___e_4)); }
	inline AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531 * get_e_4() const { return ___e_4; }
	inline AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531 ** get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531 * value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier((&___e_4), value);
	}

	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___f_5)); }
	inline bool get_f_5() const { return ___f_5; }
	inline bool* get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(bool value)
	{
		___f_5 = value;
	}

	inline static int32_t get_offset_of_g_6() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___g_6)); }
	inline DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF * get_g_6() const { return ___g_6; }
	inline DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF ** get_address_of_g_6() { return &___g_6; }
	inline void set_g_6(DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF * value)
	{
		___g_6 = value;
		Il2CppCodeGenWriteBarrier((&___g_6), value);
	}

	inline static int32_t get_offset_of_h_7() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___h_7)); }
	inline ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F * get_h_7() const { return ___h_7; }
	inline ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F ** get_address_of_h_7() { return &___h_7; }
	inline void set_h_7(ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F * value)
	{
		___h_7 = value;
		Il2CppCodeGenWriteBarrier((&___h_7), value);
	}

	inline static int32_t get_offset_of_i_8() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___i_8)); }
	inline ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * get_i_8() const { return ___i_8; }
	inline ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 ** get_address_of_i_8() { return &___i_8; }
	inline void set_i_8(ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6 * value)
	{
		___i_8 = value;
		Il2CppCodeGenWriteBarrier((&___i_8), value);
	}

	inline static int32_t get_offset_of_j_9() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___j_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_j_9() const { return ___j_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_j_9() { return &___j_9; }
	inline void set_j_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___j_9 = value;
		Il2CppCodeGenWriteBarrier((&___j_9), value);
	}

	inline static int32_t get_offset_of_k_10() { return static_cast<int32_t>(offsetof(d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4, ___k_10)); }
	inline ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * get_k_10() const { return ___k_10; }
	inline ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 ** get_address_of_k_10() { return &___k_10; }
	inline void set_k_10(ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * value)
	{
		___k_10 = value;
		Il2CppCodeGenWriteBarrier((&___k_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // D_TA4DA584627EF7D094631EDD3B87BA78B22640BF4_H
#ifndef A_T750AC741A58D91B95FB211CF345C0A47DD74F8F8_H
#define A_T750AC741A58D91B95FB211CF345C0A47DD74F8F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderBase_a
struct  a_t750AC741A58D91B95FB211CF345C0A47DD74F8F8  : public RuntimeObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipFile TriLib.AssetLoaderBase_a::a
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t750AC741A58D91B95FB211CF345C0A47DD74F8F8, ___a_0)); }
	inline ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * get_a_0() const { return ___a_0; }
	inline ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T750AC741A58D91B95FB211CF345C0A47DD74F8F8_H
#ifndef ASSIMPINTEROP_T355DF5D17B2144BD8587867267B3754AEEBD462C_H
#define ASSIMPINTEROP_T355DF5D17B2144BD8587867267B3754AEEBD462C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop
struct  AssimpInterop_t355DF5D17B2144BD8587867267B3754AEEBD462C  : public RuntimeObject
{
public:

public:
};

struct AssimpInterop_t355DF5D17B2144BD8587867267B3754AEEBD462C_StaticFields
{
public:
	// System.Boolean TriLib.AssimpInterop::d
	bool ___d_0;
	// System.Int32 TriLib.AssimpInterop::e
	int32_t ___e_1;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(AssimpInterop_t355DF5D17B2144BD8587867267B3754AEEBD462C_StaticFields, ___d_0)); }
	inline bool get_d_0() const { return ___d_0; }
	inline bool* get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(bool value)
	{
		___d_0 = value;
	}

	inline static int32_t get_offset_of_e_1() { return static_cast<int32_t>(offsetof(AssimpInterop_t355DF5D17B2144BD8587867267B3754AEEBD462C_StaticFields, ___e_1)); }
	inline int32_t get_e_1() const { return ___e_1; }
	inline int32_t* get_address_of_e_1() { return &___e_1; }
	inline void set_e_1(int32_t value)
	{
		___e_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPINTEROP_T355DF5D17B2144BD8587867267B3754AEEBD462C_H
#ifndef FILELOADDATA_TE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13_H
#define FILELOADDATA_TE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.FileLoadData
struct  FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13  : public RuntimeObject
{
public:
	// System.String TriLib.FileLoadData::Filename
	String_t* ___Filename_0;
	// System.String TriLib.FileLoadData::BasePath
	String_t* ___BasePath_1;

public:
	inline static int32_t get_offset_of_Filename_0() { return static_cast<int32_t>(offsetof(FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13, ___Filename_0)); }
	inline String_t* get_Filename_0() const { return ___Filename_0; }
	inline String_t** get_address_of_Filename_0() { return &___Filename_0; }
	inline void set_Filename_0(String_t* value)
	{
		___Filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___Filename_0), value);
	}

	inline static int32_t get_offset_of_BasePath_1() { return static_cast<int32_t>(offsetof(FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13, ___BasePath_1)); }
	inline String_t* get_BasePath_1() const { return ___BasePath_1; }
	inline String_t** get_address_of_BasePath_1() { return &___BasePath_1; }
	inline void set_BasePath_1(String_t* value)
	{
		___BasePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___BasePath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILELOADDATA_TE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13_H
#ifndef FILEUTILS_TC978494CEBA8E2DE23341446DB88A4DE75E0E5A1_H
#define FILEUTILS_TC978494CEBA8E2DE23341446DB88A4DE75E0E5A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.FileUtils
struct  FileUtils_tC978494CEBA8E2DE23341446DB88A4DE75E0E5A1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEUTILS_TC978494CEBA8E2DE23341446DB88A4DE75E0E5A1_H
#ifndef MATRIXEXTENSIONS_T83E9B3470D05F4C6F10A66D1542EFD93E3502EEB_H
#define MATRIXEXTENSIONS_T83E9B3470D05F4C6F10A66D1542EFD93E3502EEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MatrixExtensions
struct  MatrixExtensions_t83E9B3470D05F4C6F10A66D1542EFD93E3502EEB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIXEXTENSIONS_T83E9B3470D05F4C6F10A66D1542EFD93E3502EEB_H
#ifndef MESHDATA_TA109105B40CB093BC17F6ED73E4D4BAF626AA537_H
#define MESHDATA_TA109105B40CB093BC17F6ED73E4D4BAF626AA537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MeshData
struct  MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537  : public RuntimeObject
{
public:
	// System.String TriLib.MeshData::Name
	String_t* ___Name_0;
	// System.String TriLib.MeshData::SubMeshName
	String_t* ___SubMeshName_1;
	// UnityEngine.Vector3[] TriLib.MeshData::Vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Vertices_2;
	// UnityEngine.Vector3[] TriLib.MeshData::Normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Normals_3;
	// UnityEngine.Vector4[] TriLib.MeshData::Tangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___Tangents_4;
	// UnityEngine.Vector4[] TriLib.MeshData::BiTangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___BiTangents_5;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv_6;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv1
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv1_7;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv2
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv2_8;
	// UnityEngine.Vector2[] TriLib.MeshData::Uv3
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___Uv3_9;
	// UnityEngine.Color[] TriLib.MeshData::Colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___Colors_10;
	// System.Int32[] TriLib.MeshData::Triangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Triangles_11;
	// System.Boolean TriLib.MeshData::HasBoneInfo
	bool ___HasBoneInfo_12;
	// UnityEngine.Matrix4x4[] TriLib.MeshData::BindPoses
	Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* ___BindPoses_13;
	// System.String[] TriLib.MeshData::BoneNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___BoneNames_14;
	// UnityEngine.BoneWeight[] TriLib.MeshData::BoneWeights
	BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* ___BoneWeights_15;
	// System.UInt32 TriLib.MeshData::MaterialIndex
	uint32_t ___MaterialIndex_16;
	// TriLib.MorphData[] TriLib.MeshData::MorphsData
	MorphDataU5BU5D_tF75ABC4B00D9F56D8CF9E690DD05E653D090600E* ___MorphsData_17;
	// UnityEngine.Mesh TriLib.MeshData::Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___Mesh_18;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_SubMeshName_1() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___SubMeshName_1)); }
	inline String_t* get_SubMeshName_1() const { return ___SubMeshName_1; }
	inline String_t** get_address_of_SubMeshName_1() { return &___SubMeshName_1; }
	inline void set_SubMeshName_1(String_t* value)
	{
		___SubMeshName_1 = value;
		Il2CppCodeGenWriteBarrier((&___SubMeshName_1), value);
	}

	inline static int32_t get_offset_of_Vertices_2() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Vertices_2)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Vertices_2() const { return ___Vertices_2; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Vertices_2() { return &___Vertices_2; }
	inline void set_Vertices_2(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Vertices_2 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_2), value);
	}

	inline static int32_t get_offset_of_Normals_3() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Normals_3)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Normals_3() const { return ___Normals_3; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Normals_3() { return &___Normals_3; }
	inline void set_Normals_3(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Normals_3 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_3), value);
	}

	inline static int32_t get_offset_of_Tangents_4() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Tangents_4)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_Tangents_4() const { return ___Tangents_4; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_Tangents_4() { return &___Tangents_4; }
	inline void set_Tangents_4(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___Tangents_4 = value;
		Il2CppCodeGenWriteBarrier((&___Tangents_4), value);
	}

	inline static int32_t get_offset_of_BiTangents_5() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___BiTangents_5)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_BiTangents_5() const { return ___BiTangents_5; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_BiTangents_5() { return &___BiTangents_5; }
	inline void set_BiTangents_5(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___BiTangents_5 = value;
		Il2CppCodeGenWriteBarrier((&___BiTangents_5), value);
	}

	inline static int32_t get_offset_of_Uv_6() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Uv_6)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv_6() const { return ___Uv_6; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv_6() { return &___Uv_6; }
	inline void set_Uv_6(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv_6 = value;
		Il2CppCodeGenWriteBarrier((&___Uv_6), value);
	}

	inline static int32_t get_offset_of_Uv1_7() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Uv1_7)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv1_7() const { return ___Uv1_7; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv1_7() { return &___Uv1_7; }
	inline void set_Uv1_7(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv1_7 = value;
		Il2CppCodeGenWriteBarrier((&___Uv1_7), value);
	}

	inline static int32_t get_offset_of_Uv2_8() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Uv2_8)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv2_8() const { return ___Uv2_8; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv2_8() { return &___Uv2_8; }
	inline void set_Uv2_8(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv2_8 = value;
		Il2CppCodeGenWriteBarrier((&___Uv2_8), value);
	}

	inline static int32_t get_offset_of_Uv3_9() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Uv3_9)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_Uv3_9() const { return ___Uv3_9; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_Uv3_9() { return &___Uv3_9; }
	inline void set_Uv3_9(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___Uv3_9 = value;
		Il2CppCodeGenWriteBarrier((&___Uv3_9), value);
	}

	inline static int32_t get_offset_of_Colors_10() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Colors_10)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_Colors_10() const { return ___Colors_10; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_Colors_10() { return &___Colors_10; }
	inline void set_Colors_10(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___Colors_10 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_10), value);
	}

	inline static int32_t get_offset_of_Triangles_11() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Triangles_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Triangles_11() const { return ___Triangles_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Triangles_11() { return &___Triangles_11; }
	inline void set_Triangles_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Triangles_11 = value;
		Il2CppCodeGenWriteBarrier((&___Triangles_11), value);
	}

	inline static int32_t get_offset_of_HasBoneInfo_12() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___HasBoneInfo_12)); }
	inline bool get_HasBoneInfo_12() const { return ___HasBoneInfo_12; }
	inline bool* get_address_of_HasBoneInfo_12() { return &___HasBoneInfo_12; }
	inline void set_HasBoneInfo_12(bool value)
	{
		___HasBoneInfo_12 = value;
	}

	inline static int32_t get_offset_of_BindPoses_13() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___BindPoses_13)); }
	inline Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* get_BindPoses_13() const { return ___BindPoses_13; }
	inline Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9** get_address_of_BindPoses_13() { return &___BindPoses_13; }
	inline void set_BindPoses_13(Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* value)
	{
		___BindPoses_13 = value;
		Il2CppCodeGenWriteBarrier((&___BindPoses_13), value);
	}

	inline static int32_t get_offset_of_BoneNames_14() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___BoneNames_14)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_BoneNames_14() const { return ___BoneNames_14; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_BoneNames_14() { return &___BoneNames_14; }
	inline void set_BoneNames_14(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___BoneNames_14 = value;
		Il2CppCodeGenWriteBarrier((&___BoneNames_14), value);
	}

	inline static int32_t get_offset_of_BoneWeights_15() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___BoneWeights_15)); }
	inline BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* get_BoneWeights_15() const { return ___BoneWeights_15; }
	inline BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D** get_address_of_BoneWeights_15() { return &___BoneWeights_15; }
	inline void set_BoneWeights_15(BoneWeightU5BU5D_t0CFB75EF43257A99CDCF393A069504F365A13F8D* value)
	{
		___BoneWeights_15 = value;
		Il2CppCodeGenWriteBarrier((&___BoneWeights_15), value);
	}

	inline static int32_t get_offset_of_MaterialIndex_16() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___MaterialIndex_16)); }
	inline uint32_t get_MaterialIndex_16() const { return ___MaterialIndex_16; }
	inline uint32_t* get_address_of_MaterialIndex_16() { return &___MaterialIndex_16; }
	inline void set_MaterialIndex_16(uint32_t value)
	{
		___MaterialIndex_16 = value;
	}

	inline static int32_t get_offset_of_MorphsData_17() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___MorphsData_17)); }
	inline MorphDataU5BU5D_tF75ABC4B00D9F56D8CF9E690DD05E653D090600E* get_MorphsData_17() const { return ___MorphsData_17; }
	inline MorphDataU5BU5D_tF75ABC4B00D9F56D8CF9E690DD05E653D090600E** get_address_of_MorphsData_17() { return &___MorphsData_17; }
	inline void set_MorphsData_17(MorphDataU5BU5D_tF75ABC4B00D9F56D8CF9E690DD05E653D090600E* value)
	{
		___MorphsData_17 = value;
		Il2CppCodeGenWriteBarrier((&___MorphsData_17), value);
	}

	inline static int32_t get_offset_of_Mesh_18() { return static_cast<int32_t>(offsetof(MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537, ___Mesh_18)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_Mesh_18() const { return ___Mesh_18; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_Mesh_18() { return &___Mesh_18; }
	inline void set_Mesh_18(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___Mesh_18 = value;
		Il2CppCodeGenWriteBarrier((&___Mesh_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_TA109105B40CB093BC17F6ED73E4D4BAF626AA537_H
#ifndef MORPHCHANNELDATA_T6629EC80A43769190D8ED48A56093C6DD37C25E4_H
#define MORPHCHANNELDATA_T6629EC80A43769190D8ED48A56093C6DD37C25E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MorphChannelData
struct  MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4  : public RuntimeObject
{
public:
	// System.String TriLib.MorphChannelData::NodeName
	String_t* ___NodeName_0;
	// System.Collections.Generic.Dictionary`2<System.Single,TriLib.MorphChannelKey> TriLib.MorphChannelData::MorphChannelKeys
	Dictionary_2_t27753A6CEA8485F892B2780AD0C8A72CCA018B0E * ___MorphChannelKeys_1;

public:
	inline static int32_t get_offset_of_NodeName_0() { return static_cast<int32_t>(offsetof(MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4, ___NodeName_0)); }
	inline String_t* get_NodeName_0() const { return ___NodeName_0; }
	inline String_t** get_address_of_NodeName_0() { return &___NodeName_0; }
	inline void set_NodeName_0(String_t* value)
	{
		___NodeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___NodeName_0), value);
	}

	inline static int32_t get_offset_of_MorphChannelKeys_1() { return static_cast<int32_t>(offsetof(MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4, ___MorphChannelKeys_1)); }
	inline Dictionary_2_t27753A6CEA8485F892B2780AD0C8A72CCA018B0E * get_MorphChannelKeys_1() const { return ___MorphChannelKeys_1; }
	inline Dictionary_2_t27753A6CEA8485F892B2780AD0C8A72CCA018B0E ** get_address_of_MorphChannelKeys_1() { return &___MorphChannelKeys_1; }
	inline void set_MorphChannelKeys_1(Dictionary_2_t27753A6CEA8485F892B2780AD0C8A72CCA018B0E * value)
	{
		___MorphChannelKeys_1 = value;
		Il2CppCodeGenWriteBarrier((&___MorphChannelKeys_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MORPHCHANNELDATA_T6629EC80A43769190D8ED48A56093C6DD37C25E4_H
#ifndef MORPHCHANNELKEY_TED235489B14590843F8C37E1381749ADCC625E73_H
#define MORPHCHANNELKEY_TED235489B14590843F8C37E1381749ADCC625E73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MorphChannelKey
struct  MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73  : public RuntimeObject
{
public:
	// System.UInt32[] TriLib.MorphChannelKey::Indices
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Indices_0;
	// System.Single[] TriLib.MorphChannelKey::Weights
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___Weights_1;

public:
	inline static int32_t get_offset_of_Indices_0() { return static_cast<int32_t>(offsetof(MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73, ___Indices_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Indices_0() const { return ___Indices_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Indices_0() { return &___Indices_0; }
	inline void set_Indices_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Indices_0 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_0), value);
	}

	inline static int32_t get_offset_of_Weights_1() { return static_cast<int32_t>(offsetof(MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73, ___Weights_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_Weights_1() const { return ___Weights_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_Weights_1() { return &___Weights_1; }
	inline void set_Weights_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___Weights_1 = value;
		Il2CppCodeGenWriteBarrier((&___Weights_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MORPHCHANNELKEY_TED235489B14590843F8C37E1381749ADCC625E73_H
#ifndef MORPHDATA_TE312C946C7103853AF92364F5B0DF1DD06A4AC8B_H
#define MORPHDATA_TE312C946C7103853AF92364F5B0DF1DD06A4AC8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MorphData
struct  MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B  : public RuntimeObject
{
public:
	// System.String TriLib.MorphData::Name
	String_t* ___Name_0;
	// UnityEngine.Vector3[] TriLib.MorphData::Vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Vertices_1;
	// UnityEngine.Vector3[] TriLib.MorphData::Normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Normals_2;
	// UnityEngine.Vector3[] TriLib.MorphData::Tangents
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Tangents_3;
	// System.Single TriLib.MorphData::Weight
	float ___Weight_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Vertices_1() { return static_cast<int32_t>(offsetof(MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B, ___Vertices_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Vertices_1() const { return ___Vertices_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Vertices_1() { return &___Vertices_1; }
	inline void set_Vertices_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___Vertices_1), value);
	}

	inline static int32_t get_offset_of_Normals_2() { return static_cast<int32_t>(offsetof(MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B, ___Normals_2)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Normals_2() const { return ___Normals_2; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Normals_2() { return &___Normals_2; }
	inline void set_Normals_2(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Normals_2 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_2), value);
	}

	inline static int32_t get_offset_of_Tangents_3() { return static_cast<int32_t>(offsetof(MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B, ___Tangents_3)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Tangents_3() const { return ___Tangents_3; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Tangents_3() { return &___Tangents_3; }
	inline void set_Tangents_3(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Tangents_3 = value;
		Il2CppCodeGenWriteBarrier((&___Tangents_3), value);
	}

	inline static int32_t get_offset_of_Weight_4() { return static_cast<int32_t>(offsetof(MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B, ___Weight_4)); }
	inline float get_Weight_4() const { return ___Weight_4; }
	inline float* get_address_of_Weight_4() { return &___Weight_4; }
	inline void set_Weight_4(float value)
	{
		___Weight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MORPHDATA_TE312C946C7103853AF92364F5B0DF1DD06A4AC8B_H
#ifndef STREAMUTILS_T6D2DC8042ED0F5EF8F5F4A9425CF18E945007FDC_H
#define STREAMUTILS_T6D2DC8042ED0F5EF8F5F4A9425CF18E945007FDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.StreamUtils
struct  StreamUtils_t6D2DC8042ED0F5EF8F5F4A9425CF18E945007FDC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMUTILS_T6D2DC8042ED0F5EF8F5F4A9425CF18E945007FDC_H
#ifndef STRINGUTILS_T138F1BEFB530B5E8D03690A7C3790BD6BD1905AC_H
#define STRINGUTILS_T138F1BEFB530B5E8D03690A7C3790BD6BD1905AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.StringUtils
struct  StringUtils_t138F1BEFB530B5E8D03690A7C3790BD6BD1905AC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T138F1BEFB530B5E8D03690A7C3790BD6BD1905AC_H
#ifndef TEXTURE2DUTILS_T8347444C9FA2D57E80F3EB8ADDF7927DB25CAB6C_H
#define TEXTURE2DUTILS_T8347444C9FA2D57E80F3EB8ADDF7927DB25CAB6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Texture2DUtils
struct  Texture2DUtils_t8347444C9FA2D57E80F3EB8ADDF7927DB25CAB6C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2DUTILS_T8347444C9FA2D57E80F3EB8ADDF7927DB25CAB6C_H
#ifndef THREADUTILS_TB30F54991AC35C1AF20C6917FB1653127F7109FE_H
#define THREADUTILS_TB30F54991AC35C1AF20C6917FB1653127F7109FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils
struct  ThreadUtils_tB30F54991AC35C1AF20C6917FB1653127F7109FE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADUTILS_TB30F54991AC35C1AF20C6917FB1653127F7109FE_H
#ifndef A_T9752C01F959BCB0184461E930AD1BD0C6400DA0C_H
#define A_T9752C01F959BCB0184461E930AD1BD0C6400DA0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils_a
struct  a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C  : public RuntimeObject
{
public:
	// System.Action TriLib.ThreadUtils_a::a
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___a_0;
	// System.Action TriLib.ThreadUtils_a::b
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C, ___a_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_a_0() const { return ___a_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C, ___b_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_b_1() const { return ___b_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___b_1 = value;
		Il2CppCodeGenWriteBarrier((&___b_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // A_T9752C01F959BCB0184461E930AD1BD0C6400DA0C_H
#ifndef B_TBB4B7EAE3F249876C28028467342545B6E691DD5_H
#define B_TBB4B7EAE3F249876C28028467342545B6E691DD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ThreadUtils_b
struct  b_tBB4B7EAE3F249876C28028467342545B6E691DD5  : public RuntimeObject
{
public:
	// System.Exception TriLib.ThreadUtils_b::a
	Exception_t * ___a_0;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(b_tBB4B7EAE3F249876C28028467342545B6E691DD5, ___a_0)); }
	inline Exception_t * get_a_0() const { return ___a_0; }
	inline Exception_t ** get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Exception_t * value)
	{
		___a_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // B_TBB4B7EAE3F249876C28028467342545B6E691DD5_H
#ifndef TRANSFORMEXTENSIONS_T0CA99D8D9FEAB9B39CA87EA9816DD70313658C42_H
#define TRANSFORMEXTENSIONS_T0CA99D8D9FEAB9B39CA87EA9816DD70313658C42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TransformExtensions
struct  TransformExtensions_t0CA99D8D9FEAB9B39CA87EA9816DD70313658C42  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSIONS_T0CA99D8D9FEAB9B39CA87EA9816DD70313658C42_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#define UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T4980FA09003AFAAB5A6E361BA2748EA9A005709B_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef FONTASSETCREATIONSETTINGS_TC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_H
#define FONTASSETCREATIONSETTINGS_TC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontAssetCreationSettings
struct  FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4 
{
public:
	// System.String TMPro.FontAssetCreationSettings::sourceFontFileName
	String_t* ___sourceFontFileName_0;
	// System.String TMPro.FontAssetCreationSettings::sourceFontFileGUID
	String_t* ___sourceFontFileGUID_1;
	// System.Int32 TMPro.FontAssetCreationSettings::pointSizeSamplingMode
	int32_t ___pointSizeSamplingMode_2;
	// System.Int32 TMPro.FontAssetCreationSettings::pointSize
	int32_t ___pointSize_3;
	// System.Int32 TMPro.FontAssetCreationSettings::padding
	int32_t ___padding_4;
	// System.Int32 TMPro.FontAssetCreationSettings::packingMode
	int32_t ___packingMode_5;
	// System.Int32 TMPro.FontAssetCreationSettings::atlasWidth
	int32_t ___atlasWidth_6;
	// System.Int32 TMPro.FontAssetCreationSettings::atlasHeight
	int32_t ___atlasHeight_7;
	// System.Int32 TMPro.FontAssetCreationSettings::characterSetSelectionMode
	int32_t ___characterSetSelectionMode_8;
	// System.String TMPro.FontAssetCreationSettings::characterSequence
	String_t* ___characterSequence_9;
	// System.String TMPro.FontAssetCreationSettings::referencedFontAssetGUID
	String_t* ___referencedFontAssetGUID_10;
	// System.String TMPro.FontAssetCreationSettings::referencedTextAssetGUID
	String_t* ___referencedTextAssetGUID_11;
	// System.Int32 TMPro.FontAssetCreationSettings::fontStyle
	int32_t ___fontStyle_12;
	// System.Single TMPro.FontAssetCreationSettings::fontStyleModifier
	float ___fontStyleModifier_13;
	// System.Int32 TMPro.FontAssetCreationSettings::renderMode
	int32_t ___renderMode_14;
	// System.Boolean TMPro.FontAssetCreationSettings::includeFontFeatures
	bool ___includeFontFeatures_15;

public:
	inline static int32_t get_offset_of_sourceFontFileName_0() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___sourceFontFileName_0)); }
	inline String_t* get_sourceFontFileName_0() const { return ___sourceFontFileName_0; }
	inline String_t** get_address_of_sourceFontFileName_0() { return &___sourceFontFileName_0; }
	inline void set_sourceFontFileName_0(String_t* value)
	{
		___sourceFontFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFontFileName_0), value);
	}

	inline static int32_t get_offset_of_sourceFontFileGUID_1() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___sourceFontFileGUID_1)); }
	inline String_t* get_sourceFontFileGUID_1() const { return ___sourceFontFileGUID_1; }
	inline String_t** get_address_of_sourceFontFileGUID_1() { return &___sourceFontFileGUID_1; }
	inline void set_sourceFontFileGUID_1(String_t* value)
	{
		___sourceFontFileGUID_1 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFontFileGUID_1), value);
	}

	inline static int32_t get_offset_of_pointSizeSamplingMode_2() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___pointSizeSamplingMode_2)); }
	inline int32_t get_pointSizeSamplingMode_2() const { return ___pointSizeSamplingMode_2; }
	inline int32_t* get_address_of_pointSizeSamplingMode_2() { return &___pointSizeSamplingMode_2; }
	inline void set_pointSizeSamplingMode_2(int32_t value)
	{
		___pointSizeSamplingMode_2 = value;
	}

	inline static int32_t get_offset_of_pointSize_3() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___pointSize_3)); }
	inline int32_t get_pointSize_3() const { return ___pointSize_3; }
	inline int32_t* get_address_of_pointSize_3() { return &___pointSize_3; }
	inline void set_pointSize_3(int32_t value)
	{
		___pointSize_3 = value;
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___padding_4)); }
	inline int32_t get_padding_4() const { return ___padding_4; }
	inline int32_t* get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(int32_t value)
	{
		___padding_4 = value;
	}

	inline static int32_t get_offset_of_packingMode_5() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___packingMode_5)); }
	inline int32_t get_packingMode_5() const { return ___packingMode_5; }
	inline int32_t* get_address_of_packingMode_5() { return &___packingMode_5; }
	inline void set_packingMode_5(int32_t value)
	{
		___packingMode_5 = value;
	}

	inline static int32_t get_offset_of_atlasWidth_6() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___atlasWidth_6)); }
	inline int32_t get_atlasWidth_6() const { return ___atlasWidth_6; }
	inline int32_t* get_address_of_atlasWidth_6() { return &___atlasWidth_6; }
	inline void set_atlasWidth_6(int32_t value)
	{
		___atlasWidth_6 = value;
	}

	inline static int32_t get_offset_of_atlasHeight_7() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___atlasHeight_7)); }
	inline int32_t get_atlasHeight_7() const { return ___atlasHeight_7; }
	inline int32_t* get_address_of_atlasHeight_7() { return &___atlasHeight_7; }
	inline void set_atlasHeight_7(int32_t value)
	{
		___atlasHeight_7 = value;
	}

	inline static int32_t get_offset_of_characterSetSelectionMode_8() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___characterSetSelectionMode_8)); }
	inline int32_t get_characterSetSelectionMode_8() const { return ___characterSetSelectionMode_8; }
	inline int32_t* get_address_of_characterSetSelectionMode_8() { return &___characterSetSelectionMode_8; }
	inline void set_characterSetSelectionMode_8(int32_t value)
	{
		___characterSetSelectionMode_8 = value;
	}

	inline static int32_t get_offset_of_characterSequence_9() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___characterSequence_9)); }
	inline String_t* get_characterSequence_9() const { return ___characterSequence_9; }
	inline String_t** get_address_of_characterSequence_9() { return &___characterSequence_9; }
	inline void set_characterSequence_9(String_t* value)
	{
		___characterSequence_9 = value;
		Il2CppCodeGenWriteBarrier((&___characterSequence_9), value);
	}

	inline static int32_t get_offset_of_referencedFontAssetGUID_10() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___referencedFontAssetGUID_10)); }
	inline String_t* get_referencedFontAssetGUID_10() const { return ___referencedFontAssetGUID_10; }
	inline String_t** get_address_of_referencedFontAssetGUID_10() { return &___referencedFontAssetGUID_10; }
	inline void set_referencedFontAssetGUID_10(String_t* value)
	{
		___referencedFontAssetGUID_10 = value;
		Il2CppCodeGenWriteBarrier((&___referencedFontAssetGUID_10), value);
	}

	inline static int32_t get_offset_of_referencedTextAssetGUID_11() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___referencedTextAssetGUID_11)); }
	inline String_t* get_referencedTextAssetGUID_11() const { return ___referencedTextAssetGUID_11; }
	inline String_t** get_address_of_referencedTextAssetGUID_11() { return &___referencedTextAssetGUID_11; }
	inline void set_referencedTextAssetGUID_11(String_t* value)
	{
		___referencedTextAssetGUID_11 = value;
		Il2CppCodeGenWriteBarrier((&___referencedTextAssetGUID_11), value);
	}

	inline static int32_t get_offset_of_fontStyle_12() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___fontStyle_12)); }
	inline int32_t get_fontStyle_12() const { return ___fontStyle_12; }
	inline int32_t* get_address_of_fontStyle_12() { return &___fontStyle_12; }
	inline void set_fontStyle_12(int32_t value)
	{
		___fontStyle_12 = value;
	}

	inline static int32_t get_offset_of_fontStyleModifier_13() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___fontStyleModifier_13)); }
	inline float get_fontStyleModifier_13() const { return ___fontStyleModifier_13; }
	inline float* get_address_of_fontStyleModifier_13() { return &___fontStyleModifier_13; }
	inline void set_fontStyleModifier_13(float value)
	{
		___fontStyleModifier_13 = value;
	}

	inline static int32_t get_offset_of_renderMode_14() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___renderMode_14)); }
	inline int32_t get_renderMode_14() const { return ___renderMode_14; }
	inline int32_t* get_address_of_renderMode_14() { return &___renderMode_14; }
	inline void set_renderMode_14(int32_t value)
	{
		___renderMode_14 = value;
	}

	inline static int32_t get_offset_of_includeFontFeatures_15() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4, ___includeFontFeatures_15)); }
	inline bool get_includeFontFeatures_15() const { return ___includeFontFeatures_15; }
	inline bool* get_address_of_includeFontFeatures_15() { return &___includeFontFeatures_15; }
	inline void set_includeFontFeatures_15(bool value)
	{
		___includeFontFeatures_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FontAssetCreationSettings
struct FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_marshaled_pinvoke
{
	char* ___sourceFontFileName_0;
	char* ___sourceFontFileGUID_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	char* ___characterSequence_9;
	char* ___referencedFontAssetGUID_10;
	char* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
// Native definition for COM marshalling of TMPro.FontAssetCreationSettings
struct FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_marshaled_com
{
	Il2CppChar* ___sourceFontFileName_0;
	Il2CppChar* ___sourceFontFileGUID_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	Il2CppChar* ___characterSequence_9;
	Il2CppChar* ___referencedFontAssetGUID_10;
	Il2CppChar* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
#endif // FONTASSETCREATIONSETTINGS_TC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_H
#ifndef GLYPHPAIRKEY_TBED040784D7FD7F268CCCCC24844C2D723D936F6_H
#define GLYPHPAIRKEY_TBED040784D7FD7F268CCCCC24844C2D723D936F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.GlyphPairKey
struct  GlyphPairKey_tBED040784D7FD7F268CCCCC24844C2D723D936F6 
{
public:
	// System.UInt32 TMPro.GlyphPairKey::firstGlyphIndex
	uint32_t ___firstGlyphIndex_0;
	// System.UInt32 TMPro.GlyphPairKey::secondGlyphIndex
	uint32_t ___secondGlyphIndex_1;
	// System.Int64 TMPro.GlyphPairKey::key
	int64_t ___key_2;

public:
	inline static int32_t get_offset_of_firstGlyphIndex_0() { return static_cast<int32_t>(offsetof(GlyphPairKey_tBED040784D7FD7F268CCCCC24844C2D723D936F6, ___firstGlyphIndex_0)); }
	inline uint32_t get_firstGlyphIndex_0() const { return ___firstGlyphIndex_0; }
	inline uint32_t* get_address_of_firstGlyphIndex_0() { return &___firstGlyphIndex_0; }
	inline void set_firstGlyphIndex_0(uint32_t value)
	{
		___firstGlyphIndex_0 = value;
	}

	inline static int32_t get_offset_of_secondGlyphIndex_1() { return static_cast<int32_t>(offsetof(GlyphPairKey_tBED040784D7FD7F268CCCCC24844C2D723D936F6, ___secondGlyphIndex_1)); }
	inline uint32_t get_secondGlyphIndex_1() const { return ___secondGlyphIndex_1; }
	inline uint32_t* get_address_of_secondGlyphIndex_1() { return &___secondGlyphIndex_1; }
	inline void set_secondGlyphIndex_1(uint32_t value)
	{
		___secondGlyphIndex_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(GlyphPairKey_tBED040784D7FD7F268CCCCC24844C2D723D936F6, ___key_2)); }
	inline int64_t get_key_2() const { return ___key_2; }
	inline int64_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(int64_t value)
	{
		___key_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHPAIRKEY_TBED040784D7FD7F268CCCCC24844C2D723D936F6_H
#ifndef GLYPHVALUERECORD_LEGACY_T775B262F8408BFA72A5736B93D15EB6D633BC84A_H
#define GLYPHVALUERECORD_LEGACY_T775B262F8408BFA72A5736B93D15EB6D633BC84A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.GlyphValueRecord_Legacy
struct  GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A 
{
public:
	// System.Single TMPro.GlyphValueRecord_Legacy::xPlacement
	float ___xPlacement_0;
	// System.Single TMPro.GlyphValueRecord_Legacy::yPlacement
	float ___yPlacement_1;
	// System.Single TMPro.GlyphValueRecord_Legacy::xAdvance
	float ___xAdvance_2;
	// System.Single TMPro.GlyphValueRecord_Legacy::yAdvance
	float ___yAdvance_3;

public:
	inline static int32_t get_offset_of_xPlacement_0() { return static_cast<int32_t>(offsetof(GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A, ___xPlacement_0)); }
	inline float get_xPlacement_0() const { return ___xPlacement_0; }
	inline float* get_address_of_xPlacement_0() { return &___xPlacement_0; }
	inline void set_xPlacement_0(float value)
	{
		___xPlacement_0 = value;
	}

	inline static int32_t get_offset_of_yPlacement_1() { return static_cast<int32_t>(offsetof(GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A, ___yPlacement_1)); }
	inline float get_yPlacement_1() const { return ___yPlacement_1; }
	inline float* get_address_of_yPlacement_1() { return &___yPlacement_1; }
	inline void set_yPlacement_1(float value)
	{
		___yPlacement_1 = value;
	}

	inline static int32_t get_offset_of_xAdvance_2() { return static_cast<int32_t>(offsetof(GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A, ___xAdvance_2)); }
	inline float get_xAdvance_2() const { return ___xAdvance_2; }
	inline float* get_address_of_xAdvance_2() { return &___xAdvance_2; }
	inline void set_xAdvance_2(float value)
	{
		___xAdvance_2 = value;
	}

	inline static int32_t get_offset_of_yAdvance_3() { return static_cast<int32_t>(offsetof(GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A, ___yAdvance_3)); }
	inline float get_yAdvance_3() const { return ___yAdvance_3; }
	inline float* get_address_of_yAdvance_3() { return &___yAdvance_3; }
	inline void set_yAdvance_3(float value)
	{
		___yAdvance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHVALUERECORD_LEGACY_T775B262F8408BFA72A5736B93D15EB6D633BC84A_H
#ifndef MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#define MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fontAsset_1)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___material_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_3() const { return ___material_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fallbackMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifndef TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#define TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontStyleStack
struct  TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 
{
public:
	// System.Byte TMPro.TMP_FontStyleStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_FontStyleStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_FontStyleStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_FontStyleStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_FontStyleStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_FontStyleStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_FontStyleStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_FontStyleStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_FontStyleStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_FontStyleStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#ifndef TMP_FONTWEIGHTPAIR_T14BB1EA6F16060838C5465F6BBB20C92ED79AEE3_H
#define TMP_FONTWEIGHTPAIR_T14BB1EA6F16060838C5465F6BBB20C92ED79AEE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontWeightPair
struct  TMP_FontWeightPair_t14BB1EA6F16060838C5465F6BBB20C92ED79AEE3 
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_FontWeightPair::regularTypeface
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___regularTypeface_0;
	// TMPro.TMP_FontAsset TMPro.TMP_FontWeightPair::italicTypeface
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___italicTypeface_1;

public:
	inline static int32_t get_offset_of_regularTypeface_0() { return static_cast<int32_t>(offsetof(TMP_FontWeightPair_t14BB1EA6F16060838C5465F6BBB20C92ED79AEE3, ___regularTypeface_0)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_regularTypeface_0() const { return ___regularTypeface_0; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_regularTypeface_0() { return &___regularTypeface_0; }
	inline void set_regularTypeface_0(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___regularTypeface_0 = value;
		Il2CppCodeGenWriteBarrier((&___regularTypeface_0), value);
	}

	inline static int32_t get_offset_of_italicTypeface_1() { return static_cast<int32_t>(offsetof(TMP_FontWeightPair_t14BB1EA6F16060838C5465F6BBB20C92ED79AEE3, ___italicTypeface_1)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_italicTypeface_1() const { return ___italicTypeface_1; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_italicTypeface_1() { return &___italicTypeface_1; }
	inline void set_italicTypeface_1(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___italicTypeface_1 = value;
		Il2CppCodeGenWriteBarrier((&___italicTypeface_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_FontWeightPair
struct TMP_FontWeightPair_t14BB1EA6F16060838C5465F6BBB20C92ED79AEE3_marshaled_pinvoke
{
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___regularTypeface_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___italicTypeface_1;
};
// Native definition for COM marshalling of TMPro.TMP_FontWeightPair
struct TMP_FontWeightPair_t14BB1EA6F16060838C5465F6BBB20C92ED79AEE3_marshaled_com
{
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___regularTypeface_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___italicTypeface_1;
};
#endif // TMP_FONTWEIGHTPAIR_T14BB1EA6F16060838C5465F6BBB20C92ED79AEE3_H
#ifndef TMP_GLYPH_TCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C_H
#define TMP_GLYPH_TCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Glyph
struct  TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C  : public TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_GLYPH_TCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C_H
#ifndef TMP_GLYPHVALUERECORD_T78C803E430E95C128540C14391CBACF833943BD8_H
#define TMP_GLYPHVALUERECORD_T78C803E430E95C128540C14391CBACF833943BD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_GlyphValueRecord
struct  TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8 
{
public:
	// System.Single TMPro.TMP_GlyphValueRecord::m_XPlacement
	float ___m_XPlacement_0;
	// System.Single TMPro.TMP_GlyphValueRecord::m_YPlacement
	float ___m_YPlacement_1;
	// System.Single TMPro.TMP_GlyphValueRecord::m_XAdvance
	float ___m_XAdvance_2;
	// System.Single TMPro.TMP_GlyphValueRecord::m_YAdvance
	float ___m_YAdvance_3;

public:
	inline static int32_t get_offset_of_m_XPlacement_0() { return static_cast<int32_t>(offsetof(TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8, ___m_XPlacement_0)); }
	inline float get_m_XPlacement_0() const { return ___m_XPlacement_0; }
	inline float* get_address_of_m_XPlacement_0() { return &___m_XPlacement_0; }
	inline void set_m_XPlacement_0(float value)
	{
		___m_XPlacement_0 = value;
	}

	inline static int32_t get_offset_of_m_YPlacement_1() { return static_cast<int32_t>(offsetof(TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8, ___m_YPlacement_1)); }
	inline float get_m_YPlacement_1() const { return ___m_YPlacement_1; }
	inline float* get_address_of_m_YPlacement_1() { return &___m_YPlacement_1; }
	inline void set_m_YPlacement_1(float value)
	{
		___m_YPlacement_1 = value;
	}

	inline static int32_t get_offset_of_m_XAdvance_2() { return static_cast<int32_t>(offsetof(TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8, ___m_XAdvance_2)); }
	inline float get_m_XAdvance_2() const { return ___m_XAdvance_2; }
	inline float* get_address_of_m_XAdvance_2() { return &___m_XAdvance_2; }
	inline void set_m_XAdvance_2(float value)
	{
		___m_XAdvance_2 = value;
	}

	inline static int32_t get_offset_of_m_YAdvance_3() { return static_cast<int32_t>(offsetof(TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8, ___m_YAdvance_3)); }
	inline float get_m_YAdvance_3() const { return ___m_YAdvance_3; }
	inline float* get_address_of_m_YAdvance_3() { return &___m_YAdvance_3; }
	inline void set_m_YAdvance_3(float value)
	{
		___m_YAdvance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_GLYPHVALUERECORD_T78C803E430E95C128540C14391CBACF833943BD8_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T629E00E06021AA51A5AD8607BAFC61DB099F2D7F_H
#define TMP_RICHTEXTTAGSTACK_1_T629E00E06021AA51A5AD8607BAFC61DB099F2D7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<System.Int32>
struct  TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_ItemStack_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T629E00E06021AA51A5AD8607BAFC61DB099F2D7F_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T221674BAE112F99AB4BDB4D127F20A021FF50CA3_H
#define TMP_RICHTEXTTAGSTACK_1_T221674BAE112F99AB4BDB4D127F20A021FF50CA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<System.Single>
struct  TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	float ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_ItemStack_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_DefaultItem_3)); }
	inline float get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline float* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(float value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T221674BAE112F99AB4BDB4D127F20A021FF50CA3_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#define TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_ItemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_DefaultItem_3)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_DefaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#ifndef GCFILELOADDATA_T7A72633928546FD4AC5348CE9EDDE47AA7C7F62D_H
#define GCFILELOADDATA_T7A72633928546FD4AC5348CE9EDDE47AA7C7F62D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.GCFileLoadData
struct  GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D  : public FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13
{
public:
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle> TriLib.GCFileLoadData::LockedBuffers
	List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * ___LockedBuffers_2;

public:
	inline static int32_t get_offset_of_LockedBuffers_2() { return static_cast<int32_t>(offsetof(GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D, ___LockedBuffers_2)); }
	inline List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * get_LockedBuffers_2() const { return ___LockedBuffers_2; }
	inline List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 ** get_address_of_LockedBuffers_2() { return &___LockedBuffers_2; }
	inline void set_LockedBuffers_2(List_1_t897CFD5A76D916278D41769D4682EAFEFC02CE83 * value)
	{
		___LockedBuffers_2 = value;
		Il2CppCodeGenWriteBarrier((&___LockedBuffers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCFILELOADDATA_T7A72633928546FD4AC5348CE9EDDE47AA7C7F62D_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef FACEINFO_T32155CB9E0D125155E829A3D23119FB323F382A8_H
#define FACEINFO_T32155CB9E0D125155E829A3D23119FB323F382A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.FaceInfo
struct  FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8 
{
public:
	// System.String UnityEngine.TextCore.FaceInfo::m_FamilyName
	String_t* ___m_FamilyName_0;
	// System.String UnityEngine.TextCore.FaceInfo::m_StyleName
	String_t* ___m_StyleName_1;
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_PointSize
	int32_t ___m_PointSize_2;
	// System.Single UnityEngine.TextCore.FaceInfo::m_Scale
	float ___m_Scale_3;
	// System.Single UnityEngine.TextCore.FaceInfo::m_LineHeight
	float ___m_LineHeight_4;
	// System.Single UnityEngine.TextCore.FaceInfo::m_AscentLine
	float ___m_AscentLine_5;
	// System.Single UnityEngine.TextCore.FaceInfo::m_CapLine
	float ___m_CapLine_6;
	// System.Single UnityEngine.TextCore.FaceInfo::m_MeanLine
	float ___m_MeanLine_7;
	// System.Single UnityEngine.TextCore.FaceInfo::m_Baseline
	float ___m_Baseline_8;
	// System.Single UnityEngine.TextCore.FaceInfo::m_DescentLine
	float ___m_DescentLine_9;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SuperscriptOffset
	float ___m_SuperscriptOffset_10;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SuperscriptSize
	float ___m_SuperscriptSize_11;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SubscriptOffset
	float ___m_SubscriptOffset_12;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SubscriptSize
	float ___m_SubscriptSize_13;
	// System.Single UnityEngine.TextCore.FaceInfo::m_UnderlineOffset
	float ___m_UnderlineOffset_14;
	// System.Single UnityEngine.TextCore.FaceInfo::m_UnderlineThickness
	float ___m_UnderlineThickness_15;
	// System.Single UnityEngine.TextCore.FaceInfo::m_StrikethroughOffset
	float ___m_StrikethroughOffset_16;
	// System.Single UnityEngine.TextCore.FaceInfo::m_StrikethroughThickness
	float ___m_StrikethroughThickness_17;
	// System.Single UnityEngine.TextCore.FaceInfo::m_TabWidth
	float ___m_TabWidth_18;

public:
	inline static int32_t get_offset_of_m_FamilyName_0() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_FamilyName_0)); }
	inline String_t* get_m_FamilyName_0() const { return ___m_FamilyName_0; }
	inline String_t** get_address_of_m_FamilyName_0() { return &___m_FamilyName_0; }
	inline void set_m_FamilyName_0(String_t* value)
	{
		___m_FamilyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_FamilyName_0), value);
	}

	inline static int32_t get_offset_of_m_StyleName_1() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_StyleName_1)); }
	inline String_t* get_m_StyleName_1() const { return ___m_StyleName_1; }
	inline String_t** get_address_of_m_StyleName_1() { return &___m_StyleName_1; }
	inline void set_m_StyleName_1(String_t* value)
	{
		___m_StyleName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleName_1), value);
	}

	inline static int32_t get_offset_of_m_PointSize_2() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_PointSize_2)); }
	inline int32_t get_m_PointSize_2() const { return ___m_PointSize_2; }
	inline int32_t* get_address_of_m_PointSize_2() { return &___m_PointSize_2; }
	inline void set_m_PointSize_2(int32_t value)
	{
		___m_PointSize_2 = value;
	}

	inline static int32_t get_offset_of_m_Scale_3() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_Scale_3)); }
	inline float get_m_Scale_3() const { return ___m_Scale_3; }
	inline float* get_address_of_m_Scale_3() { return &___m_Scale_3; }
	inline void set_m_Scale_3(float value)
	{
		___m_Scale_3 = value;
	}

	inline static int32_t get_offset_of_m_LineHeight_4() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_LineHeight_4)); }
	inline float get_m_LineHeight_4() const { return ___m_LineHeight_4; }
	inline float* get_address_of_m_LineHeight_4() { return &___m_LineHeight_4; }
	inline void set_m_LineHeight_4(float value)
	{
		___m_LineHeight_4 = value;
	}

	inline static int32_t get_offset_of_m_AscentLine_5() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_AscentLine_5)); }
	inline float get_m_AscentLine_5() const { return ___m_AscentLine_5; }
	inline float* get_address_of_m_AscentLine_5() { return &___m_AscentLine_5; }
	inline void set_m_AscentLine_5(float value)
	{
		___m_AscentLine_5 = value;
	}

	inline static int32_t get_offset_of_m_CapLine_6() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_CapLine_6)); }
	inline float get_m_CapLine_6() const { return ___m_CapLine_6; }
	inline float* get_address_of_m_CapLine_6() { return &___m_CapLine_6; }
	inline void set_m_CapLine_6(float value)
	{
		___m_CapLine_6 = value;
	}

	inline static int32_t get_offset_of_m_MeanLine_7() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_MeanLine_7)); }
	inline float get_m_MeanLine_7() const { return ___m_MeanLine_7; }
	inline float* get_address_of_m_MeanLine_7() { return &___m_MeanLine_7; }
	inline void set_m_MeanLine_7(float value)
	{
		___m_MeanLine_7 = value;
	}

	inline static int32_t get_offset_of_m_Baseline_8() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_Baseline_8)); }
	inline float get_m_Baseline_8() const { return ___m_Baseline_8; }
	inline float* get_address_of_m_Baseline_8() { return &___m_Baseline_8; }
	inline void set_m_Baseline_8(float value)
	{
		___m_Baseline_8 = value;
	}

	inline static int32_t get_offset_of_m_DescentLine_9() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_DescentLine_9)); }
	inline float get_m_DescentLine_9() const { return ___m_DescentLine_9; }
	inline float* get_address_of_m_DescentLine_9() { return &___m_DescentLine_9; }
	inline void set_m_DescentLine_9(float value)
	{
		___m_DescentLine_9 = value;
	}

	inline static int32_t get_offset_of_m_SuperscriptOffset_10() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_SuperscriptOffset_10)); }
	inline float get_m_SuperscriptOffset_10() const { return ___m_SuperscriptOffset_10; }
	inline float* get_address_of_m_SuperscriptOffset_10() { return &___m_SuperscriptOffset_10; }
	inline void set_m_SuperscriptOffset_10(float value)
	{
		___m_SuperscriptOffset_10 = value;
	}

	inline static int32_t get_offset_of_m_SuperscriptSize_11() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_SuperscriptSize_11)); }
	inline float get_m_SuperscriptSize_11() const { return ___m_SuperscriptSize_11; }
	inline float* get_address_of_m_SuperscriptSize_11() { return &___m_SuperscriptSize_11; }
	inline void set_m_SuperscriptSize_11(float value)
	{
		___m_SuperscriptSize_11 = value;
	}

	inline static int32_t get_offset_of_m_SubscriptOffset_12() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_SubscriptOffset_12)); }
	inline float get_m_SubscriptOffset_12() const { return ___m_SubscriptOffset_12; }
	inline float* get_address_of_m_SubscriptOffset_12() { return &___m_SubscriptOffset_12; }
	inline void set_m_SubscriptOffset_12(float value)
	{
		___m_SubscriptOffset_12 = value;
	}

	inline static int32_t get_offset_of_m_SubscriptSize_13() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_SubscriptSize_13)); }
	inline float get_m_SubscriptSize_13() const { return ___m_SubscriptSize_13; }
	inline float* get_address_of_m_SubscriptSize_13() { return &___m_SubscriptSize_13; }
	inline void set_m_SubscriptSize_13(float value)
	{
		___m_SubscriptSize_13 = value;
	}

	inline static int32_t get_offset_of_m_UnderlineOffset_14() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_UnderlineOffset_14)); }
	inline float get_m_UnderlineOffset_14() const { return ___m_UnderlineOffset_14; }
	inline float* get_address_of_m_UnderlineOffset_14() { return &___m_UnderlineOffset_14; }
	inline void set_m_UnderlineOffset_14(float value)
	{
		___m_UnderlineOffset_14 = value;
	}

	inline static int32_t get_offset_of_m_UnderlineThickness_15() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_UnderlineThickness_15)); }
	inline float get_m_UnderlineThickness_15() const { return ___m_UnderlineThickness_15; }
	inline float* get_address_of_m_UnderlineThickness_15() { return &___m_UnderlineThickness_15; }
	inline void set_m_UnderlineThickness_15(float value)
	{
		___m_UnderlineThickness_15 = value;
	}

	inline static int32_t get_offset_of_m_StrikethroughOffset_16() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_StrikethroughOffset_16)); }
	inline float get_m_StrikethroughOffset_16() const { return ___m_StrikethroughOffset_16; }
	inline float* get_address_of_m_StrikethroughOffset_16() { return &___m_StrikethroughOffset_16; }
	inline void set_m_StrikethroughOffset_16(float value)
	{
		___m_StrikethroughOffset_16 = value;
	}

	inline static int32_t get_offset_of_m_StrikethroughThickness_17() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_StrikethroughThickness_17)); }
	inline float get_m_StrikethroughThickness_17() const { return ___m_StrikethroughThickness_17; }
	inline float* get_address_of_m_StrikethroughThickness_17() { return &___m_StrikethroughThickness_17; }
	inline void set_m_StrikethroughThickness_17(float value)
	{
		___m_StrikethroughThickness_17 = value;
	}

	inline static int32_t get_offset_of_m_TabWidth_18() { return static_cast<int32_t>(offsetof(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8, ___m_TabWidth_18)); }
	inline float get_m_TabWidth_18() const { return ___m_TabWidth_18; }
	inline float* get_address_of_m_TabWidth_18() { return &___m_TabWidth_18; }
	inline void set_m_TabWidth_18(float value)
	{
		___m_TabWidth_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.FaceInfo
struct FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8_marshaled_pinvoke
{
	char* ___m_FamilyName_0;
	char* ___m_StyleName_1;
	int32_t ___m_PointSize_2;
	float ___m_Scale_3;
	float ___m_LineHeight_4;
	float ___m_AscentLine_5;
	float ___m_CapLine_6;
	float ___m_MeanLine_7;
	float ___m_Baseline_8;
	float ___m_DescentLine_9;
	float ___m_SuperscriptOffset_10;
	float ___m_SuperscriptSize_11;
	float ___m_SubscriptOffset_12;
	float ___m_SubscriptSize_13;
	float ___m_UnderlineOffset_14;
	float ___m_UnderlineThickness_15;
	float ___m_StrikethroughOffset_16;
	float ___m_StrikethroughThickness_17;
	float ___m_TabWidth_18;
};
// Native definition for COM marshalling of UnityEngine.TextCore.FaceInfo
struct FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8_marshaled_com
{
	Il2CppChar* ___m_FamilyName_0;
	Il2CppChar* ___m_StyleName_1;
	int32_t ___m_PointSize_2;
	float ___m_Scale_3;
	float ___m_LineHeight_4;
	float ___m_AscentLine_5;
	float ___m_CapLine_6;
	float ___m_MeanLine_7;
	float ___m_Baseline_8;
	float ___m_DescentLine_9;
	float ___m_SuperscriptOffset_10;
	float ___m_SuperscriptSize_11;
	float ___m_SubscriptOffset_12;
	float ___m_SubscriptSize_13;
	float ___m_UnderlineOffset_14;
	float ___m_UnderlineThickness_15;
	float ___m_StrikethroughOffset_16;
	float ___m_StrikethroughThickness_17;
	float ___m_TabWidth_18;
};
#endif // FACEINFO_T32155CB9E0D125155E829A3D23119FB323F382A8_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef ATLASPOPULATIONMODE_T719D719A21DA39129F8EA982DF7BC7C344F6BC4D_H
#define ATLASPOPULATIONMODE_T719D719A21DA39129F8EA982DF7BC7C344F6BC4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.AtlasPopulationMode
struct  AtlasPopulationMode_t719D719A21DA39129F8EA982DF7BC7C344F6BC4D 
{
public:
	// System.Int32 TMPro.AtlasPopulationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AtlasPopulationMode_t719D719A21DA39129F8EA982DF7BC7C344F6BC4D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASPOPULATIONMODE_T719D719A21DA39129F8EA982DF7BC7C344F6BC4D_H
#ifndef COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#define COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorMode
struct  ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3 
{
public:
	// System.Int32 TMPro.ColorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#ifndef EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#define EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifndef FONTFEATURELOOKUPFLAGS_T5E2AC8F0E11557FFBDC03A81EA2ECD8B82C17D8D_H
#define FONTFEATURELOOKUPFLAGS_T5E2AC8F0E11557FFBDC03A81EA2ECD8B82C17D8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontFeatureLookupFlags
struct  FontFeatureLookupFlags_t5E2AC8F0E11557FFBDC03A81EA2ECD8B82C17D8D 
{
public:
	// System.Int32 TMPro.FontFeatureLookupFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontFeatureLookupFlags_t5E2AC8F0E11557FFBDC03A81EA2ECD8B82C17D8D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTFEATURELOOKUPFLAGS_T5E2AC8F0E11557FFBDC03A81EA2ECD8B82C17D8D_H
#ifndef FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#define FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifndef FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#define FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeight
struct  FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C 
{
public:
	// System.Int32 TMPro.FontWeight::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#ifndef KERNINGPAIR_T4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD_H
#define KERNINGPAIR_T4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPair
struct  KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningPair::m_FirstGlyph
	uint32_t ___m_FirstGlyph_0;
	// TMPro.GlyphValueRecord_Legacy TMPro.KerningPair::m_FirstGlyphAdjustments
	GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A  ___m_FirstGlyphAdjustments_1;
	// System.UInt32 TMPro.KerningPair::m_SecondGlyph
	uint32_t ___m_SecondGlyph_2;
	// TMPro.GlyphValueRecord_Legacy TMPro.KerningPair::m_SecondGlyphAdjustments
	GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A  ___m_SecondGlyphAdjustments_3;
	// System.Single TMPro.KerningPair::xOffset
	float ___xOffset_4;
	// System.Boolean TMPro.KerningPair::m_IgnoreSpacingAdjustments
	bool ___m_IgnoreSpacingAdjustments_6;

public:
	inline static int32_t get_offset_of_m_FirstGlyph_0() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___m_FirstGlyph_0)); }
	inline uint32_t get_m_FirstGlyph_0() const { return ___m_FirstGlyph_0; }
	inline uint32_t* get_address_of_m_FirstGlyph_0() { return &___m_FirstGlyph_0; }
	inline void set_m_FirstGlyph_0(uint32_t value)
	{
		___m_FirstGlyph_0 = value;
	}

	inline static int32_t get_offset_of_m_FirstGlyphAdjustments_1() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___m_FirstGlyphAdjustments_1)); }
	inline GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A  get_m_FirstGlyphAdjustments_1() const { return ___m_FirstGlyphAdjustments_1; }
	inline GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A * get_address_of_m_FirstGlyphAdjustments_1() { return &___m_FirstGlyphAdjustments_1; }
	inline void set_m_FirstGlyphAdjustments_1(GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A  value)
	{
		___m_FirstGlyphAdjustments_1 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyph_2() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___m_SecondGlyph_2)); }
	inline uint32_t get_m_SecondGlyph_2() const { return ___m_SecondGlyph_2; }
	inline uint32_t* get_address_of_m_SecondGlyph_2() { return &___m_SecondGlyph_2; }
	inline void set_m_SecondGlyph_2(uint32_t value)
	{
		___m_SecondGlyph_2 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyphAdjustments_3() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___m_SecondGlyphAdjustments_3)); }
	inline GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A  get_m_SecondGlyphAdjustments_3() const { return ___m_SecondGlyphAdjustments_3; }
	inline GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A * get_address_of_m_SecondGlyphAdjustments_3() { return &___m_SecondGlyphAdjustments_3; }
	inline void set_m_SecondGlyphAdjustments_3(GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A  value)
	{
		___m_SecondGlyphAdjustments_3 = value;
	}

	inline static int32_t get_offset_of_xOffset_4() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___xOffset_4)); }
	inline float get_xOffset_4() const { return ___xOffset_4; }
	inline float* get_address_of_xOffset_4() { return &___xOffset_4; }
	inline void set_xOffset_4(float value)
	{
		___xOffset_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreSpacingAdjustments_6() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD, ___m_IgnoreSpacingAdjustments_6)); }
	inline bool get_m_IgnoreSpacingAdjustments_6() const { return ___m_IgnoreSpacingAdjustments_6; }
	inline bool* get_address_of_m_IgnoreSpacingAdjustments_6() { return &___m_IgnoreSpacingAdjustments_6; }
	inline void set_m_IgnoreSpacingAdjustments_6(bool value)
	{
		___m_IgnoreSpacingAdjustments_6 = value;
	}
};

struct KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD_StaticFields
{
public:
	// TMPro.KerningPair TMPro.KerningPair::empty
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD * ___empty_5;

public:
	inline static int32_t get_offset_of_empty_5() { return static_cast<int32_t>(offsetof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD_StaticFields, ___empty_5)); }
	inline KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD * get_empty_5() const { return ___empty_5; }
	inline KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD ** get_address_of_empty_5() { return &___empty_5; }
	inline void set_empty_5(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD * value)
	{
		___empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIR_T4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD_H
#ifndef MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#define MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t37B6F292739A890CF34EA024D24A5BFA88579086 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaskingTypes_t37B6F292739A890CF34EA024D24A5BFA88579086, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#ifndef TMP_GLYPHADJUSTMENTRECORD_T515A636DEA8D632C08D0B01A9AC1F962F0291E58_H
#define TMP_GLYPHADJUSTMENTRECORD_T515A636DEA8D632C08D0B01A9AC1F962F0291E58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_GlyphAdjustmentRecord
struct  TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58 
{
public:
	// System.UInt32 TMPro.TMP_GlyphAdjustmentRecord::m_GlyphIndex
	uint32_t ___m_GlyphIndex_0;
	// TMPro.TMP_GlyphValueRecord TMPro.TMP_GlyphAdjustmentRecord::m_GlyphValueRecord
	TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8  ___m_GlyphValueRecord_1;

public:
	inline static int32_t get_offset_of_m_GlyphIndex_0() { return static_cast<int32_t>(offsetof(TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58, ___m_GlyphIndex_0)); }
	inline uint32_t get_m_GlyphIndex_0() const { return ___m_GlyphIndex_0; }
	inline uint32_t* get_address_of_m_GlyphIndex_0() { return &___m_GlyphIndex_0; }
	inline void set_m_GlyphIndex_0(uint32_t value)
	{
		___m_GlyphIndex_0 = value;
	}

	inline static int32_t get_offset_of_m_GlyphValueRecord_1() { return static_cast<int32_t>(offsetof(TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58, ___m_GlyphValueRecord_1)); }
	inline TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8  get_m_GlyphValueRecord_1() const { return ___m_GlyphValueRecord_1; }
	inline TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8 * get_address_of_m_GlyphValueRecord_1() { return &___m_GlyphValueRecord_1; }
	inline void set_m_GlyphValueRecord_1(TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8  value)
	{
		___m_GlyphValueRecord_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_GLYPHADJUSTMENTRECORD_T515A636DEA8D632C08D0B01A9AC1F962F0291E58_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#define TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference>
struct  TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_ItemStack_0)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_DefaultItem_3)); }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F * get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#define TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32>
struct  TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_ItemStack_0)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_DefaultItem_3)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#ifndef TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#define TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text_TextInputSources
struct  TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4 
{
public:
	// System.Int32 TMPro.TMP_Text_TextInputSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#ifndef TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#define TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifndef TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#define TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv_1() const { return ___uv_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv2_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv4_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___color_4)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_color_4() const { return ___color_4; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#ifndef TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#define TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagUnitType
struct  TagUnitType_t5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF 
{
public:
	// System.Int32 TMPro.TagUnitType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagUnitType_t5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#ifndef TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#define TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagValueType
struct  TagValueType_tB0AE4FE83F0293DDD337886C8D5268947F25F5CC 
{
public:
	// System.Int32 TMPro.TagValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagValueType_tB0AE4FE83F0293DDD337886C8D5268947F25F5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#ifndef TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#define TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifndef TEXTCONTAINERANCHORS_T8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D_H
#define TEXTCONTAINERANCHORS_T8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextContainerAnchors
struct  TextContainerAnchors_t8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D 
{
public:
	// System.Int32 TMPro.TextContainerAnchors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextContainerAnchors_t8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONTAINERANCHORS_T8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D_H
#ifndef TEXTELEMENTTYPE_T3C95010E28DAFD09E9C361EEB679937475CEE857_H
#define TEXTELEMENTTYPE_T3C95010E28DAFD09E9C361EEB679937475CEE857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextElementType
struct  TextElementType_t3C95010E28DAFD09E9C361EEB679937475CEE857 
{
public:
	// System.Byte TMPro.TextElementType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextElementType_t3C95010E28DAFD09E9C361EEB679937475CEE857, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTELEMENTTYPE_T3C95010E28DAFD09E9C361EEB679937475CEE857_H
#ifndef TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#define TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#ifndef TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#define TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#ifndef TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#define TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#ifndef VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#define VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topLeft_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topRight_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topRight_1() const { return ___topRight_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomLeft_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomRight_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifndef VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#define VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#ifndef ASSETLOADERBASE_T4276A226F958FD499EE6C37536BB1CFD76B403CD_H
#define ASSETLOADERBASE_T4276A226F958FD499EE6C37536BB1CFD76B403CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderBase
struct  AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD  : public RuntimeObject
{
public:
	// TriLib.NodeData TriLib.AssetLoaderBase::RootNodeData
	NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C * ___RootNodeData_0;
	// TriLib.MaterialData[] TriLib.AssetLoaderBase::MaterialData
	MaterialDataU5BU5D_t96A25DA908C2869FFE5C2BC523A70E0B0460184F* ___MaterialData_1;
	// TriLib.MeshData[] TriLib.AssetLoaderBase::MeshData
	MeshDataU5BU5D_tE36D464408234701C2529B85B6C88378A9E03200* ___MeshData_2;
	// TriLib.AnimationData[] TriLib.AssetLoaderBase::AnimationData
	AnimationDataU5BU5D_tBFB48E31561CE499886A3C833130A229E1C2D51E* ___AnimationData_3;
	// TriLib.CameraData[] TriLib.AssetLoaderBase::CameraData
	CameraDataU5BU5D_tE0068A647898485A8BD2861CAC5A1D0589A07EBE* ___CameraData_4;
	// TriLib.AssimpMetadata[] TriLib.AssetLoaderBase::Metadata
	AssimpMetadataU5BU5D_t43651D4F106B3744AB588280B857BF3E0A53B3FF* ___Metadata_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> TriLib.AssetLoaderBase::NodesPath
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___NodesPath_6;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> TriLib.AssetLoaderBase::LoadedMaterials
	Dictionary_2_t722ECF723B462DCDB0A863F3FA84E47F2040B34C * ___LoadedMaterials_7;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D> TriLib.AssetLoaderBase::LoadedTextures
	Dictionary_2_t48F9E7887D14BA0E81A2775FD742FAB95CADAF85 * ___LoadedTextures_8;
	// System.Collections.Generic.Dictionary`2<UnityEngine.SkinnedMeshRenderer,System.Collections.Generic.IList`1<System.String>> TriLib.AssetLoaderBase::LoadedBoneNames
	Dictionary_2_t09D5586CFD781770994E6AE3F93F97A102BAF885 * ___LoadedBoneNames_9;
	// System.Collections.Generic.Dictionary`2<System.String,TriLib.MeshData> TriLib.AssetLoaderBase::MeshDataConnections
	Dictionary_2_t9D6D0891CDB41250259DE1A6829B3762986E4704 * ___MeshDataConnections_10;
	// System.Collections.Generic.Dictionary`2<System.String,TriLib.EmbeddedTextureData> TriLib.AssetLoaderBase::EmbeddedTextures
	Dictionary_2_t57B3F18F00761BE67616EC3A88E4A26E19DE1B32 * ___EmbeddedTextures_11;
	// System.UInt32 TriLib.AssetLoaderBase::NodeId
	uint32_t ___NodeId_20;
	// System.Boolean TriLib.AssetLoaderBase::HasBoneInfo
	bool ___HasBoneInfo_21;
	// System.Boolean TriLib.AssetLoaderBase::HasBlendShapes
	bool ___HasBlendShapes_22;
	// System.IntPtr TriLib.AssetLoaderBase::Scene
	intptr_t ___Scene_23;
	// TriLib.EmbeddedTextureLoadCallback TriLib.AssetLoaderBase::a
	EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7 * ___a_24;
	// TriLib.MeshCreatedHandle TriLib.AssetLoaderBase::b
	MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3 * ___b_25;
	// TriLib.MaterialCreatedHandle TriLib.AssetLoaderBase::c
	MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78 * ___c_26;
	// TriLib.TextureLoadHandle TriLib.AssetLoaderBase::d
	TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D * ___d_27;
	// TriLib.AvatarCreatedHandle TriLib.AssetLoaderBase::e
	AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80 * ___e_28;
	// TriLib.AnimationClipCreatedHandle TriLib.AssetLoaderBase::f
	AnimationClipCreatedHandle_t13B0579E3C7ABBF0AF164928743D86D52FA5016F * ___f_29;
	// TriLib.ObjectLoadedHandle TriLib.AssetLoaderBase::g
	ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * ___g_30;
	// TriLib.MetadataProcessedHandle TriLib.AssetLoaderBase::h
	MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E * ___h_31;
	// TriLib.BlendShapeKeyCreatedHandle TriLib.AssetLoaderBase::i
	BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B * ___i_32;

public:
	inline static int32_t get_offset_of_RootNodeData_0() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___RootNodeData_0)); }
	inline NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C * get_RootNodeData_0() const { return ___RootNodeData_0; }
	inline NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C ** get_address_of_RootNodeData_0() { return &___RootNodeData_0; }
	inline void set_RootNodeData_0(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C * value)
	{
		___RootNodeData_0 = value;
		Il2CppCodeGenWriteBarrier((&___RootNodeData_0), value);
	}

	inline static int32_t get_offset_of_MaterialData_1() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___MaterialData_1)); }
	inline MaterialDataU5BU5D_t96A25DA908C2869FFE5C2BC523A70E0B0460184F* get_MaterialData_1() const { return ___MaterialData_1; }
	inline MaterialDataU5BU5D_t96A25DA908C2869FFE5C2BC523A70E0B0460184F** get_address_of_MaterialData_1() { return &___MaterialData_1; }
	inline void set_MaterialData_1(MaterialDataU5BU5D_t96A25DA908C2869FFE5C2BC523A70E0B0460184F* value)
	{
		___MaterialData_1 = value;
		Il2CppCodeGenWriteBarrier((&___MaterialData_1), value);
	}

	inline static int32_t get_offset_of_MeshData_2() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___MeshData_2)); }
	inline MeshDataU5BU5D_tE36D464408234701C2529B85B6C88378A9E03200* get_MeshData_2() const { return ___MeshData_2; }
	inline MeshDataU5BU5D_tE36D464408234701C2529B85B6C88378A9E03200** get_address_of_MeshData_2() { return &___MeshData_2; }
	inline void set_MeshData_2(MeshDataU5BU5D_tE36D464408234701C2529B85B6C88378A9E03200* value)
	{
		___MeshData_2 = value;
		Il2CppCodeGenWriteBarrier((&___MeshData_2), value);
	}

	inline static int32_t get_offset_of_AnimationData_3() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___AnimationData_3)); }
	inline AnimationDataU5BU5D_tBFB48E31561CE499886A3C833130A229E1C2D51E* get_AnimationData_3() const { return ___AnimationData_3; }
	inline AnimationDataU5BU5D_tBFB48E31561CE499886A3C833130A229E1C2D51E** get_address_of_AnimationData_3() { return &___AnimationData_3; }
	inline void set_AnimationData_3(AnimationDataU5BU5D_tBFB48E31561CE499886A3C833130A229E1C2D51E* value)
	{
		___AnimationData_3 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationData_3), value);
	}

	inline static int32_t get_offset_of_CameraData_4() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___CameraData_4)); }
	inline CameraDataU5BU5D_tE0068A647898485A8BD2861CAC5A1D0589A07EBE* get_CameraData_4() const { return ___CameraData_4; }
	inline CameraDataU5BU5D_tE0068A647898485A8BD2861CAC5A1D0589A07EBE** get_address_of_CameraData_4() { return &___CameraData_4; }
	inline void set_CameraData_4(CameraDataU5BU5D_tE0068A647898485A8BD2861CAC5A1D0589A07EBE* value)
	{
		___CameraData_4 = value;
		Il2CppCodeGenWriteBarrier((&___CameraData_4), value);
	}

	inline static int32_t get_offset_of_Metadata_5() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___Metadata_5)); }
	inline AssimpMetadataU5BU5D_t43651D4F106B3744AB588280B857BF3E0A53B3FF* get_Metadata_5() const { return ___Metadata_5; }
	inline AssimpMetadataU5BU5D_t43651D4F106B3744AB588280B857BF3E0A53B3FF** get_address_of_Metadata_5() { return &___Metadata_5; }
	inline void set_Metadata_5(AssimpMetadataU5BU5D_t43651D4F106B3744AB588280B857BF3E0A53B3FF* value)
	{
		___Metadata_5 = value;
		Il2CppCodeGenWriteBarrier((&___Metadata_5), value);
	}

	inline static int32_t get_offset_of_NodesPath_6() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___NodesPath_6)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_NodesPath_6() const { return ___NodesPath_6; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_NodesPath_6() { return &___NodesPath_6; }
	inline void set_NodesPath_6(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___NodesPath_6 = value;
		Il2CppCodeGenWriteBarrier((&___NodesPath_6), value);
	}

	inline static int32_t get_offset_of_LoadedMaterials_7() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___LoadedMaterials_7)); }
	inline Dictionary_2_t722ECF723B462DCDB0A863F3FA84E47F2040B34C * get_LoadedMaterials_7() const { return ___LoadedMaterials_7; }
	inline Dictionary_2_t722ECF723B462DCDB0A863F3FA84E47F2040B34C ** get_address_of_LoadedMaterials_7() { return &___LoadedMaterials_7; }
	inline void set_LoadedMaterials_7(Dictionary_2_t722ECF723B462DCDB0A863F3FA84E47F2040B34C * value)
	{
		___LoadedMaterials_7 = value;
		Il2CppCodeGenWriteBarrier((&___LoadedMaterials_7), value);
	}

	inline static int32_t get_offset_of_LoadedTextures_8() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___LoadedTextures_8)); }
	inline Dictionary_2_t48F9E7887D14BA0E81A2775FD742FAB95CADAF85 * get_LoadedTextures_8() const { return ___LoadedTextures_8; }
	inline Dictionary_2_t48F9E7887D14BA0E81A2775FD742FAB95CADAF85 ** get_address_of_LoadedTextures_8() { return &___LoadedTextures_8; }
	inline void set_LoadedTextures_8(Dictionary_2_t48F9E7887D14BA0E81A2775FD742FAB95CADAF85 * value)
	{
		___LoadedTextures_8 = value;
		Il2CppCodeGenWriteBarrier((&___LoadedTextures_8), value);
	}

	inline static int32_t get_offset_of_LoadedBoneNames_9() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___LoadedBoneNames_9)); }
	inline Dictionary_2_t09D5586CFD781770994E6AE3F93F97A102BAF885 * get_LoadedBoneNames_9() const { return ___LoadedBoneNames_9; }
	inline Dictionary_2_t09D5586CFD781770994E6AE3F93F97A102BAF885 ** get_address_of_LoadedBoneNames_9() { return &___LoadedBoneNames_9; }
	inline void set_LoadedBoneNames_9(Dictionary_2_t09D5586CFD781770994E6AE3F93F97A102BAF885 * value)
	{
		___LoadedBoneNames_9 = value;
		Il2CppCodeGenWriteBarrier((&___LoadedBoneNames_9), value);
	}

	inline static int32_t get_offset_of_MeshDataConnections_10() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___MeshDataConnections_10)); }
	inline Dictionary_2_t9D6D0891CDB41250259DE1A6829B3762986E4704 * get_MeshDataConnections_10() const { return ___MeshDataConnections_10; }
	inline Dictionary_2_t9D6D0891CDB41250259DE1A6829B3762986E4704 ** get_address_of_MeshDataConnections_10() { return &___MeshDataConnections_10; }
	inline void set_MeshDataConnections_10(Dictionary_2_t9D6D0891CDB41250259DE1A6829B3762986E4704 * value)
	{
		___MeshDataConnections_10 = value;
		Il2CppCodeGenWriteBarrier((&___MeshDataConnections_10), value);
	}

	inline static int32_t get_offset_of_EmbeddedTextures_11() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___EmbeddedTextures_11)); }
	inline Dictionary_2_t57B3F18F00761BE67616EC3A88E4A26E19DE1B32 * get_EmbeddedTextures_11() const { return ___EmbeddedTextures_11; }
	inline Dictionary_2_t57B3F18F00761BE67616EC3A88E4A26E19DE1B32 ** get_address_of_EmbeddedTextures_11() { return &___EmbeddedTextures_11; }
	inline void set_EmbeddedTextures_11(Dictionary_2_t57B3F18F00761BE67616EC3A88E4A26E19DE1B32 * value)
	{
		___EmbeddedTextures_11 = value;
		Il2CppCodeGenWriteBarrier((&___EmbeddedTextures_11), value);
	}

	inline static int32_t get_offset_of_NodeId_20() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___NodeId_20)); }
	inline uint32_t get_NodeId_20() const { return ___NodeId_20; }
	inline uint32_t* get_address_of_NodeId_20() { return &___NodeId_20; }
	inline void set_NodeId_20(uint32_t value)
	{
		___NodeId_20 = value;
	}

	inline static int32_t get_offset_of_HasBoneInfo_21() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___HasBoneInfo_21)); }
	inline bool get_HasBoneInfo_21() const { return ___HasBoneInfo_21; }
	inline bool* get_address_of_HasBoneInfo_21() { return &___HasBoneInfo_21; }
	inline void set_HasBoneInfo_21(bool value)
	{
		___HasBoneInfo_21 = value;
	}

	inline static int32_t get_offset_of_HasBlendShapes_22() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___HasBlendShapes_22)); }
	inline bool get_HasBlendShapes_22() const { return ___HasBlendShapes_22; }
	inline bool* get_address_of_HasBlendShapes_22() { return &___HasBlendShapes_22; }
	inline void set_HasBlendShapes_22(bool value)
	{
		___HasBlendShapes_22 = value;
	}

	inline static int32_t get_offset_of_Scene_23() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___Scene_23)); }
	inline intptr_t get_Scene_23() const { return ___Scene_23; }
	inline intptr_t* get_address_of_Scene_23() { return &___Scene_23; }
	inline void set_Scene_23(intptr_t value)
	{
		___Scene_23 = value;
	}

	inline static int32_t get_offset_of_a_24() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___a_24)); }
	inline EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7 * get_a_24() const { return ___a_24; }
	inline EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7 ** get_address_of_a_24() { return &___a_24; }
	inline void set_a_24(EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7 * value)
	{
		___a_24 = value;
		Il2CppCodeGenWriteBarrier((&___a_24), value);
	}

	inline static int32_t get_offset_of_b_25() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___b_25)); }
	inline MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3 * get_b_25() const { return ___b_25; }
	inline MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3 ** get_address_of_b_25() { return &___b_25; }
	inline void set_b_25(MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3 * value)
	{
		___b_25 = value;
		Il2CppCodeGenWriteBarrier((&___b_25), value);
	}

	inline static int32_t get_offset_of_c_26() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___c_26)); }
	inline MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78 * get_c_26() const { return ___c_26; }
	inline MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78 ** get_address_of_c_26() { return &___c_26; }
	inline void set_c_26(MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78 * value)
	{
		___c_26 = value;
		Il2CppCodeGenWriteBarrier((&___c_26), value);
	}

	inline static int32_t get_offset_of_d_27() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___d_27)); }
	inline TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D * get_d_27() const { return ___d_27; }
	inline TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D ** get_address_of_d_27() { return &___d_27; }
	inline void set_d_27(TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D * value)
	{
		___d_27 = value;
		Il2CppCodeGenWriteBarrier((&___d_27), value);
	}

	inline static int32_t get_offset_of_e_28() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___e_28)); }
	inline AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80 * get_e_28() const { return ___e_28; }
	inline AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80 ** get_address_of_e_28() { return &___e_28; }
	inline void set_e_28(AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80 * value)
	{
		___e_28 = value;
		Il2CppCodeGenWriteBarrier((&___e_28), value);
	}

	inline static int32_t get_offset_of_f_29() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___f_29)); }
	inline AnimationClipCreatedHandle_t13B0579E3C7ABBF0AF164928743D86D52FA5016F * get_f_29() const { return ___f_29; }
	inline AnimationClipCreatedHandle_t13B0579E3C7ABBF0AF164928743D86D52FA5016F ** get_address_of_f_29() { return &___f_29; }
	inline void set_f_29(AnimationClipCreatedHandle_t13B0579E3C7ABBF0AF164928743D86D52FA5016F * value)
	{
		___f_29 = value;
		Il2CppCodeGenWriteBarrier((&___f_29), value);
	}

	inline static int32_t get_offset_of_g_30() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___g_30)); }
	inline ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * get_g_30() const { return ___g_30; }
	inline ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 ** get_address_of_g_30() { return &___g_30; }
	inline void set_g_30(ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044 * value)
	{
		___g_30 = value;
		Il2CppCodeGenWriteBarrier((&___g_30), value);
	}

	inline static int32_t get_offset_of_h_31() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___h_31)); }
	inline MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E * get_h_31() const { return ___h_31; }
	inline MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E ** get_address_of_h_31() { return &___h_31; }
	inline void set_h_31(MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E * value)
	{
		___h_31 = value;
		Il2CppCodeGenWriteBarrier((&___h_31), value);
	}

	inline static int32_t get_offset_of_i_32() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD, ___i_32)); }
	inline BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B * get_i_32() const { return ___i_32; }
	inline BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B ** get_address_of_i_32() { return &___i_32; }
	inline void set_i_32(BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B * value)
	{
		___i_32 = value;
		Il2CppCodeGenWriteBarrier((&___i_32), value);
	}
};

struct AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields
{
public:
	// TriLib.ConcurrentList`1<TriLib.FileLoadData> TriLib.AssetLoaderBase::FilesLoadData
	ConcurrentList_1_t0A359FE7FD9FC9BFD249E433775F3929A72DEC58 * ___FilesLoadData_12;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardBaseMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardBaseMaterial_13;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardSpecularMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardSpecularMaterial_14;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardBaseAlphaMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardBaseAlphaMaterial_15;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardSpecularAlphaMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardSpecularAlphaMaterial_16;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardBaseCutoutMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardBaseCutoutMaterial_17;
	// UnityEngine.Material TriLib.AssetLoaderBase::StandardSpecularCutoutMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StandardSpecularCutoutMaterial_18;
	// UnityEngine.Texture2D TriLib.AssetLoaderBase::NotFoundTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___NotFoundTexture_19;

public:
	inline static int32_t get_offset_of_FilesLoadData_12() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields, ___FilesLoadData_12)); }
	inline ConcurrentList_1_t0A359FE7FD9FC9BFD249E433775F3929A72DEC58 * get_FilesLoadData_12() const { return ___FilesLoadData_12; }
	inline ConcurrentList_1_t0A359FE7FD9FC9BFD249E433775F3929A72DEC58 ** get_address_of_FilesLoadData_12() { return &___FilesLoadData_12; }
	inline void set_FilesLoadData_12(ConcurrentList_1_t0A359FE7FD9FC9BFD249E433775F3929A72DEC58 * value)
	{
		___FilesLoadData_12 = value;
		Il2CppCodeGenWriteBarrier((&___FilesLoadData_12), value);
	}

	inline static int32_t get_offset_of_StandardBaseMaterial_13() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields, ___StandardBaseMaterial_13)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardBaseMaterial_13() const { return ___StandardBaseMaterial_13; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardBaseMaterial_13() { return &___StandardBaseMaterial_13; }
	inline void set_StandardBaseMaterial_13(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardBaseMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&___StandardBaseMaterial_13), value);
	}

	inline static int32_t get_offset_of_StandardSpecularMaterial_14() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields, ___StandardSpecularMaterial_14)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardSpecularMaterial_14() const { return ___StandardSpecularMaterial_14; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardSpecularMaterial_14() { return &___StandardSpecularMaterial_14; }
	inline void set_StandardSpecularMaterial_14(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardSpecularMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___StandardSpecularMaterial_14), value);
	}

	inline static int32_t get_offset_of_StandardBaseAlphaMaterial_15() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields, ___StandardBaseAlphaMaterial_15)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardBaseAlphaMaterial_15() const { return ___StandardBaseAlphaMaterial_15; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardBaseAlphaMaterial_15() { return &___StandardBaseAlphaMaterial_15; }
	inline void set_StandardBaseAlphaMaterial_15(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardBaseAlphaMaterial_15 = value;
		Il2CppCodeGenWriteBarrier((&___StandardBaseAlphaMaterial_15), value);
	}

	inline static int32_t get_offset_of_StandardSpecularAlphaMaterial_16() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields, ___StandardSpecularAlphaMaterial_16)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardSpecularAlphaMaterial_16() const { return ___StandardSpecularAlphaMaterial_16; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardSpecularAlphaMaterial_16() { return &___StandardSpecularAlphaMaterial_16; }
	inline void set_StandardSpecularAlphaMaterial_16(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardSpecularAlphaMaterial_16 = value;
		Il2CppCodeGenWriteBarrier((&___StandardSpecularAlphaMaterial_16), value);
	}

	inline static int32_t get_offset_of_StandardBaseCutoutMaterial_17() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields, ___StandardBaseCutoutMaterial_17)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardBaseCutoutMaterial_17() const { return ___StandardBaseCutoutMaterial_17; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardBaseCutoutMaterial_17() { return &___StandardBaseCutoutMaterial_17; }
	inline void set_StandardBaseCutoutMaterial_17(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardBaseCutoutMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___StandardBaseCutoutMaterial_17), value);
	}

	inline static int32_t get_offset_of_StandardSpecularCutoutMaterial_18() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields, ___StandardSpecularCutoutMaterial_18)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StandardSpecularCutoutMaterial_18() const { return ___StandardSpecularCutoutMaterial_18; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StandardSpecularCutoutMaterial_18() { return &___StandardSpecularCutoutMaterial_18; }
	inline void set_StandardSpecularCutoutMaterial_18(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StandardSpecularCutoutMaterial_18 = value;
		Il2CppCodeGenWriteBarrier((&___StandardSpecularCutoutMaterial_18), value);
	}

	inline static int32_t get_offset_of_NotFoundTexture_19() { return static_cast<int32_t>(offsetof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields, ___NotFoundTexture_19)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_NotFoundTexture_19() const { return ___NotFoundTexture_19; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_NotFoundTexture_19() { return &___NotFoundTexture_19; }
	inline void set_NotFoundTexture_19(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___NotFoundTexture_19 = value;
		Il2CppCodeGenWriteBarrier((&___NotFoundTexture_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLOADERBASE_T4276A226F958FD499EE6C37536BB1CFD76B403CD_H
#ifndef ASSIMPMETADATATYPE_TE81F99F30662BCAC75055C486BB4F3F68E7D988B_H
#define ASSIMPMETADATATYPE_TE81F99F30662BCAC75055C486BB4F3F68E7D988B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpMetadataType
struct  AssimpMetadataType_tE81F99F30662BCAC75055C486BB4F3F68E7D988B 
{
public:
	// System.Int32 TriLib.AssimpMetadataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssimpMetadataType_tE81F99F30662BCAC75055C486BB4F3F68E7D988B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPMETADATATYPE_TE81F99F30662BCAC75055C486BB4F3F68E7D988B_H
#ifndef ASSIMPPOSTPROCESSSTEPS_T89C7A137C5A7EEA789392222AFF34DCD11F471AD_H
#define ASSIMPPOSTPROCESSSTEPS_T89C7A137C5A7EEA789392222AFF34DCD11F471AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpPostProcessSteps
struct  AssimpPostProcessSteps_t89C7A137C5A7EEA789392222AFF34DCD11F471AD 
{
public:
	// System.Int32 TriLib.AssimpPostProcessSteps::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssimpPostProcessSteps_t89C7A137C5A7EEA789392222AFF34DCD11F471AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPPOSTPROCESSSTEPS_T89C7A137C5A7EEA789392222AFF34DCD11F471AD_H
#ifndef CAMERADATA_TC06135C8B99D7A922FF22C8E9E138EF9CA426FF8_H
#define CAMERADATA_TC06135C8B99D7A922FF22C8E9E138EF9CA426FF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.CameraData
struct  CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8  : public RuntimeObject
{
public:
	// System.String TriLib.CameraData::Name
	String_t* ___Name_0;
	// System.Single TriLib.CameraData::Aspect
	float ___Aspect_1;
	// System.Single TriLib.CameraData::NearClipPlane
	float ___NearClipPlane_2;
	// System.Single TriLib.CameraData::FarClipPlane
	float ___FarClipPlane_3;
	// System.Single TriLib.CameraData::FieldOfView
	float ___FieldOfView_4;
	// UnityEngine.Vector3 TriLib.CameraData::LocalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___LocalPosition_5;
	// UnityEngine.Vector3 TriLib.CameraData::Forward
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Forward_6;
	// UnityEngine.Vector3 TriLib.CameraData::Up
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Up_7;
	// UnityEngine.Camera TriLib.CameraData::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_8;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Aspect_1() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___Aspect_1)); }
	inline float get_Aspect_1() const { return ___Aspect_1; }
	inline float* get_address_of_Aspect_1() { return &___Aspect_1; }
	inline void set_Aspect_1(float value)
	{
		___Aspect_1 = value;
	}

	inline static int32_t get_offset_of_NearClipPlane_2() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___NearClipPlane_2)); }
	inline float get_NearClipPlane_2() const { return ___NearClipPlane_2; }
	inline float* get_address_of_NearClipPlane_2() { return &___NearClipPlane_2; }
	inline void set_NearClipPlane_2(float value)
	{
		___NearClipPlane_2 = value;
	}

	inline static int32_t get_offset_of_FarClipPlane_3() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___FarClipPlane_3)); }
	inline float get_FarClipPlane_3() const { return ___FarClipPlane_3; }
	inline float* get_address_of_FarClipPlane_3() { return &___FarClipPlane_3; }
	inline void set_FarClipPlane_3(float value)
	{
		___FarClipPlane_3 = value;
	}

	inline static int32_t get_offset_of_FieldOfView_4() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___FieldOfView_4)); }
	inline float get_FieldOfView_4() const { return ___FieldOfView_4; }
	inline float* get_address_of_FieldOfView_4() { return &___FieldOfView_4; }
	inline void set_FieldOfView_4(float value)
	{
		___FieldOfView_4 = value;
	}

	inline static int32_t get_offset_of_LocalPosition_5() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___LocalPosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_LocalPosition_5() const { return ___LocalPosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_LocalPosition_5() { return &___LocalPosition_5; }
	inline void set_LocalPosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___LocalPosition_5 = value;
	}

	inline static int32_t get_offset_of_Forward_6() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___Forward_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Forward_6() const { return ___Forward_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Forward_6() { return &___Forward_6; }
	inline void set_Forward_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Forward_6 = value;
	}

	inline static int32_t get_offset_of_Up_7() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___Up_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Up_7() const { return ___Up_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Up_7() { return &___Up_7; }
	inline void set_Up_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Up_7 = value;
	}

	inline static int32_t get_offset_of_Camera_8() { return static_cast<int32_t>(offsetof(CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8, ___Camera_8)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_8() const { return ___Camera_8; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_8() { return &___Camera_8; }
	inline void set_Camera_8(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_8 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADATA_TC06135C8B99D7A922FF22C8E9E138EF9CA426FF8_H
#ifndef EMBEDDEDTEXTUREDATA_T256A14D2298773D48CE5F0225DCABD4ABD703915_H
#define EMBEDDEDTEXTUREDATA_T256A14D2298773D48CE5F0225DCABD4ABD703915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.EmbeddedTextureData
struct  EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915  : public RuntimeObject
{
public:
	// System.Byte[] TriLib.EmbeddedTextureData::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_0;
	// System.IntPtr TriLib.EmbeddedTextureData::DataPointer
	intptr_t ___DataPointer_1;
	// System.Int32 TriLib.EmbeddedTextureData::DataLength
	int32_t ___DataLength_2;
	// TriLib.DataDisposalCallback TriLib.EmbeddedTextureData::OnDataDisposal
	DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * ___OnDataDisposal_3;
	// System.Int32 TriLib.EmbeddedTextureData::Width
	int32_t ___Width_4;
	// System.Int32 TriLib.EmbeddedTextureData::Height
	int32_t ___Height_5;
	// System.Int32 TriLib.EmbeddedTextureData::NumChannels
	int32_t ___NumChannels_6;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___Data_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_0() const { return ___Data_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((&___Data_0), value);
	}

	inline static int32_t get_offset_of_DataPointer_1() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___DataPointer_1)); }
	inline intptr_t get_DataPointer_1() const { return ___DataPointer_1; }
	inline intptr_t* get_address_of_DataPointer_1() { return &___DataPointer_1; }
	inline void set_DataPointer_1(intptr_t value)
	{
		___DataPointer_1 = value;
	}

	inline static int32_t get_offset_of_DataLength_2() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___DataLength_2)); }
	inline int32_t get_DataLength_2() const { return ___DataLength_2; }
	inline int32_t* get_address_of_DataLength_2() { return &___DataLength_2; }
	inline void set_DataLength_2(int32_t value)
	{
		___DataLength_2 = value;
	}

	inline static int32_t get_offset_of_OnDataDisposal_3() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___OnDataDisposal_3)); }
	inline DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * get_OnDataDisposal_3() const { return ___OnDataDisposal_3; }
	inline DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB ** get_address_of_OnDataDisposal_3() { return &___OnDataDisposal_3; }
	inline void set_OnDataDisposal_3(DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB * value)
	{
		___OnDataDisposal_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnDataDisposal_3), value);
	}

	inline static int32_t get_offset_of_Width_4() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___Width_4)); }
	inline int32_t get_Width_4() const { return ___Width_4; }
	inline int32_t* get_address_of_Width_4() { return &___Width_4; }
	inline void set_Width_4(int32_t value)
	{
		___Width_4 = value;
	}

	inline static int32_t get_offset_of_Height_5() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___Height_5)); }
	inline int32_t get_Height_5() const { return ___Height_5; }
	inline int32_t* get_address_of_Height_5() { return &___Height_5; }
	inline void set_Height_5(int32_t value)
	{
		___Height_5 = value;
	}

	inline static int32_t get_offset_of_NumChannels_6() { return static_cast<int32_t>(offsetof(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915, ___NumChannels_6)); }
	inline int32_t get_NumChannels_6() const { return ___NumChannels_6; }
	inline int32_t* get_address_of_NumChannels_6() { return &___NumChannels_6; }
	inline void set_NumChannels_6(int32_t value)
	{
		___NumChannels_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMBEDDEDTEXTUREDATA_T256A14D2298773D48CE5F0225DCABD4ABD703915_H
#ifndef NODEDATA_TD2933CCEB90B2F77D752DB697E43E8C616DAC93C_H
#define NODEDATA_TD2933CCEB90B2F77D752DB697E43E8C616DAC93C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.NodeData
struct  NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C  : public RuntimeObject
{
public:
	// System.String TriLib.NodeData::Name
	String_t* ___Name_0;
	// System.String TriLib.NodeData::Path
	String_t* ___Path_1;
	// UnityEngine.Matrix4x4 TriLib.NodeData::Matrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___Matrix_2;
	// System.UInt32[] TriLib.NodeData::Meshes
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Meshes_3;
	// TriLib.NodeData TriLib.NodeData::Parent
	NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C * ___Parent_4;
	// TriLib.NodeData[] TriLib.NodeData::Children
	NodeDataU5BU5D_t2903451E388B76A240D86BC410A23B2EF74CF920* ___Children_5;
	// UnityEngine.GameObject TriLib.NodeData::GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameObject_6;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Path_1() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Path_1)); }
	inline String_t* get_Path_1() const { return ___Path_1; }
	inline String_t** get_address_of_Path_1() { return &___Path_1; }
	inline void set_Path_1(String_t* value)
	{
		___Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___Path_1), value);
	}

	inline static int32_t get_offset_of_Matrix_2() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Matrix_2)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_Matrix_2() const { return ___Matrix_2; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_Matrix_2() { return &___Matrix_2; }
	inline void set_Matrix_2(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___Matrix_2 = value;
	}

	inline static int32_t get_offset_of_Meshes_3() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Meshes_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Meshes_3() const { return ___Meshes_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Meshes_3() { return &___Meshes_3; }
	inline void set_Meshes_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Meshes_3 = value;
		Il2CppCodeGenWriteBarrier((&___Meshes_3), value);
	}

	inline static int32_t get_offset_of_Parent_4() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Parent_4)); }
	inline NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C * get_Parent_4() const { return ___Parent_4; }
	inline NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C ** get_address_of_Parent_4() { return &___Parent_4; }
	inline void set_Parent_4(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C * value)
	{
		___Parent_4 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_4), value);
	}

	inline static int32_t get_offset_of_Children_5() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___Children_5)); }
	inline NodeDataU5BU5D_t2903451E388B76A240D86BC410A23B2EF74CF920* get_Children_5() const { return ___Children_5; }
	inline NodeDataU5BU5D_t2903451E388B76A240D86BC410A23B2EF74CF920** get_address_of_Children_5() { return &___Children_5; }
	inline void set_Children_5(NodeDataU5BU5D_t2903451E388B76A240D86BC410A23B2EF74CF920* value)
	{
		___Children_5 = value;
		Il2CppCodeGenWriteBarrier((&___Children_5), value);
	}

	inline static int32_t get_offset_of_GameObject_6() { return static_cast<int32_t>(offsetof(NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C, ___GameObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_GameObject_6() const { return ___GameObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_GameObject_6() { return &___GameObject_6; }
	inline void set_GameObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___GameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEDATA_TD2933CCEB90B2F77D752DB697E43E8C616DAC93C_H
#ifndef TEXTURECOMPRESSION_T25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C_H
#define TEXTURECOMPRESSION_T25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureCompression
struct  TextureCompression_t25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C 
{
public:
	// System.Int32 TriLib.TextureCompression::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureCompression_t25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURECOMPRESSION_T25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C_H
#ifndef ZIPGCFILELOADDATA_TBAFBC6955F8E973FA87C0DDFD15968EC80491E53_H
#define ZIPGCFILELOADDATA_TBAFBC6955F8E973FA87C0DDFD15968EC80491E53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ZipGCFileLoadData
struct  ZipGCFileLoadData_tBAFBC6955F8E973FA87C0DDFD15968EC80491E53  : public GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipFile TriLib.ZipGCFileLoadData::ZipFile
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * ___ZipFile_3;

public:
	inline static int32_t get_offset_of_ZipFile_3() { return static_cast<int32_t>(offsetof(ZipGCFileLoadData_tBAFBC6955F8E973FA87C0DDFD15968EC80491E53, ___ZipFile_3)); }
	inline ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * get_ZipFile_3() const { return ___ZipFile_3; }
	inline ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F ** get_address_of_ZipFile_3() { return &___ZipFile_3; }
	inline void set_ZipFile_3(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * value)
	{
		___ZipFile_3 = value;
		Il2CppCodeGenWriteBarrier((&___ZipFile_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPGCFILELOADDATA_TBAFBC6955F8E973FA87C0DDFD15968EC80491E53_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#define FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef GLYPHRENDERMODE_T73887B794BC6100E833D50FB9F5BF86B6D5D4A0D_H
#define GLYPHRENDERMODE_T73887B794BC6100E833D50FB9F5BF86B6D5D4A0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.LowLevel.GlyphRenderMode
struct  GlyphRenderMode_t73887B794BC6100E833D50FB9F5BF86B6D5D4A0D 
{
public:
	// System.Int32 UnityEngine.TextCore.LowLevel.GlyphRenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GlyphRenderMode_t73887B794BC6100E833D50FB9F5BF86B6D5D4A0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHRENDERMODE_T73887B794BC6100E833D50FB9F5BF86B6D5D4A0D_H
#ifndef TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#define TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#ifndef WRAPMODE_T3704F0388A4F801D2F54B1EA1EE8DC014D667AFD_H
#define WRAPMODE_T3704F0388A4F801D2F54B1EA1EE8DC014D667AFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WrapMode
struct  WrapMode_t3704F0388A4F801D2F54B1EA1EE8DC014D667AFD 
{
public:
	// System.Int32 UnityEngine.WrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WrapMode_t3704F0388A4F801D2F54B1EA1EE8DC014D667AFD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPMODE_T3704F0388A4F801D2F54B1EA1EE8DC014D667AFD_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#define TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int32 TMPro.TMP_CharacterInfo::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_CharacterInfo::stringLength
	int32_t ___stringLength_2;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_3;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_4;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_5;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_6;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_7;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_8;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_9;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_10;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_11;
	// System.Int32 TMPro.TMP_CharacterInfo::lineNumber
	int32_t ___lineNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::pageNumber
	int32_t ___pageNumber_13;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_17;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_21;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_22;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_23;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_24;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_25;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_26;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_27;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_28;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_32;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_33;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_34;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_35;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_stringLength_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___stringLength_2)); }
	inline int32_t get_stringLength_2() const { return ___stringLength_2; }
	inline int32_t* get_address_of_stringLength_2() { return &___stringLength_2; }
	inline void set_stringLength_2(int32_t value)
	{
		___stringLength_2 = value;
	}

	inline static int32_t get_offset_of_elementType_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___elementType_3)); }
	inline int32_t get_elementType_3() const { return ___elementType_3; }
	inline int32_t* get_address_of_elementType_3() { return &___elementType_3; }
	inline void set_elementType_3(int32_t value)
	{
		___elementType_3 = value;
	}

	inline static int32_t get_offset_of_textElement_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___textElement_4)); }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * get_textElement_4() const { return ___textElement_4; }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 ** get_address_of_textElement_4() { return &___textElement_4; }
	inline void set_textElement_4(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * value)
	{
		___textElement_4 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_4), value);
	}

	inline static int32_t get_offset_of_fontAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___fontAsset_5)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_5() const { return ___fontAsset_5; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_5() { return &___fontAsset_5; }
	inline void set_fontAsset_5(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteAsset_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___spriteAsset_6)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_6() const { return ___spriteAsset_6; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_6() { return &___spriteAsset_6; }
	inline void set_spriteAsset_6(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_6 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_6), value);
	}

	inline static int32_t get_offset_of_spriteIndex_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___spriteIndex_7)); }
	inline int32_t get_spriteIndex_7() const { return ___spriteIndex_7; }
	inline int32_t* get_address_of_spriteIndex_7() { return &___spriteIndex_7; }
	inline void set_spriteIndex_7(int32_t value)
	{
		___spriteIndex_7 = value;
	}

	inline static int32_t get_offset_of_material_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___material_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_8() const { return ___material_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_8() { return &___material_8; }
	inline void set_material_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_8 = value;
		Il2CppCodeGenWriteBarrier((&___material_8), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___materialReferenceIndex_9)); }
	inline int32_t get_materialReferenceIndex_9() const { return ___materialReferenceIndex_9; }
	inline int32_t* get_address_of_materialReferenceIndex_9() { return &___materialReferenceIndex_9; }
	inline void set_materialReferenceIndex_9(int32_t value)
	{
		___materialReferenceIndex_9 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___isUsingAlternateTypeface_10)); }
	inline bool get_isUsingAlternateTypeface_10() const { return ___isUsingAlternateTypeface_10; }
	inline bool* get_address_of_isUsingAlternateTypeface_10() { return &___isUsingAlternateTypeface_10; }
	inline void set_isUsingAlternateTypeface_10(bool value)
	{
		___isUsingAlternateTypeface_10 = value;
	}

	inline static int32_t get_offset_of_pointSize_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___pointSize_11)); }
	inline float get_pointSize_11() const { return ___pointSize_11; }
	inline float* get_address_of_pointSize_11() { return &___pointSize_11; }
	inline void set_pointSize_11(float value)
	{
		___pointSize_11 = value;
	}

	inline static int32_t get_offset_of_lineNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___lineNumber_12)); }
	inline int32_t get_lineNumber_12() const { return ___lineNumber_12; }
	inline int32_t* get_address_of_lineNumber_12() { return &___lineNumber_12; }
	inline void set_lineNumber_12(int32_t value)
	{
		___lineNumber_12 = value;
	}

	inline static int32_t get_offset_of_pageNumber_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___pageNumber_13)); }
	inline int32_t get_pageNumber_13() const { return ___pageNumber_13; }
	inline int32_t* get_address_of_pageNumber_13() { return &___pageNumber_13; }
	inline void set_pageNumber_13(int32_t value)
	{
		___pageNumber_13 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertexIndex_14)); }
	inline int32_t get_vertexIndex_14() const { return ___vertexIndex_14; }
	inline int32_t* get_address_of_vertexIndex_14() { return &___vertexIndex_14; }
	inline void set_vertexIndex_14(int32_t value)
	{
		___vertexIndex_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_BL_15)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_TL_16)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_TL_16() const { return ___vertex_TL_16; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_TL_16() { return &___vertex_TL_16; }
	inline void set_vertex_TL_16(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_TL_16 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_TR_17)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_TR_17() const { return ___vertex_TR_17; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_TR_17() { return &___vertex_TR_17; }
	inline void set_vertex_TR_17(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_TR_17 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_BR_18)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_BR_18() const { return ___vertex_BR_18; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_BR_18() { return &___vertex_BR_18; }
	inline void set_vertex_BR_18(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_BR_18 = value;
	}

	inline static int32_t get_offset_of_topLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___topLeft_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_topLeft_19() const { return ___topLeft_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_topLeft_19() { return &___topLeft_19; }
	inline void set_topLeft_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___topLeft_19 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___bottomLeft_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomLeft_20() const { return ___bottomLeft_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomLeft_20() { return &___bottomLeft_20; }
	inline void set_bottomLeft_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomLeft_20 = value;
	}

	inline static int32_t get_offset_of_topRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___topRight_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_topRight_21() const { return ___topRight_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_topRight_21() { return &___topRight_21; }
	inline void set_topRight_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___topRight_21 = value;
	}

	inline static int32_t get_offset_of_bottomRight_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___bottomRight_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRight_22() const { return ___bottomRight_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRight_22() { return &___bottomRight_22; }
	inline void set_bottomRight_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRight_22 = value;
	}

	inline static int32_t get_offset_of_origin_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___origin_23)); }
	inline float get_origin_23() const { return ___origin_23; }
	inline float* get_address_of_origin_23() { return &___origin_23; }
	inline void set_origin_23(float value)
	{
		___origin_23 = value;
	}

	inline static int32_t get_offset_of_ascender_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___ascender_24)); }
	inline float get_ascender_24() const { return ___ascender_24; }
	inline float* get_address_of_ascender_24() { return &___ascender_24; }
	inline void set_ascender_24(float value)
	{
		___ascender_24 = value;
	}

	inline static int32_t get_offset_of_baseLine_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___baseLine_25)); }
	inline float get_baseLine_25() const { return ___baseLine_25; }
	inline float* get_address_of_baseLine_25() { return &___baseLine_25; }
	inline void set_baseLine_25(float value)
	{
		___baseLine_25 = value;
	}

	inline static int32_t get_offset_of_descender_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___descender_26)); }
	inline float get_descender_26() const { return ___descender_26; }
	inline float* get_address_of_descender_26() { return &___descender_26; }
	inline void set_descender_26(float value)
	{
		___descender_26 = value;
	}

	inline static int32_t get_offset_of_xAdvance_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___xAdvance_27)); }
	inline float get_xAdvance_27() const { return ___xAdvance_27; }
	inline float* get_address_of_xAdvance_27() { return &___xAdvance_27; }
	inline void set_xAdvance_27(float value)
	{
		___xAdvance_27 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___aspectRatio_28)); }
	inline float get_aspectRatio_28() const { return ___aspectRatio_28; }
	inline float* get_address_of_aspectRatio_28() { return &___aspectRatio_28; }
	inline void set_aspectRatio_28(float value)
	{
		___aspectRatio_28 = value;
	}

	inline static int32_t get_offset_of_scale_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___scale_29)); }
	inline float get_scale_29() const { return ___scale_29; }
	inline float* get_address_of_scale_29() { return &___scale_29; }
	inline void set_scale_29(float value)
	{
		___scale_29 = value;
	}

	inline static int32_t get_offset_of_color_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___color_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_color_30() const { return ___color_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_color_30() { return &___color_30; }
	inline void set_color_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___color_30 = value;
	}

	inline static int32_t get_offset_of_underlineColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___underlineColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_31() const { return ___underlineColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_31() { return &___underlineColor_31; }
	inline void set_underlineColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_31 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___strikethroughColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_32() const { return ___strikethroughColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_32() { return &___strikethroughColor_32; }
	inline void set_strikethroughColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_32 = value;
	}

	inline static int32_t get_offset_of_highlightColor_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___highlightColor_33)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_33() const { return ___highlightColor_33; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_33() { return &___highlightColor_33; }
	inline void set_highlightColor_33(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_33 = value;
	}

	inline static int32_t get_offset_of_style_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___style_34)); }
	inline int32_t get_style_34() const { return ___style_34; }
	inline int32_t* get_address_of_style_34() { return &___style_34; }
	inline void set_style_34(int32_t value)
	{
		___style_34 = value;
	}

	inline static int32_t get_offset_of_isVisible_35() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___isVisible_35)); }
	inline bool get_isVisible_35() const { return ___isVisible_35; }
	inline bool* get_address_of_isVisible_35() { return &___isVisible_35; }
	inline void set_isVisible_35(bool value)
	{
		___isVisible_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___stringLength_2;
	int32_t ___elementType_3;
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_4;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_5;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_6;
	int32_t ___spriteIndex_7;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_8;
	int32_t ___materialReferenceIndex_9;
	int32_t ___isUsingAlternateTypeface_10;
	float ___pointSize_11;
	int32_t ___lineNumber_12;
	int32_t ___pageNumber_13;
	int32_t ___vertexIndex_14;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_16;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_17;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_18;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_19;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_20;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_21;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_22;
	float ___origin_23;
	float ___ascender_24;
	float ___baseLine_25;
	float ___descender_26;
	float ___xAdvance_27;
	float ___aspectRatio_28;
	float ___scale_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_32;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_33;
	int32_t ___style_34;
	int32_t ___isVisible_35;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___stringLength_2;
	int32_t ___elementType_3;
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_4;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_5;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_6;
	int32_t ___spriteIndex_7;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_8;
	int32_t ___materialReferenceIndex_9;
	int32_t ___isUsingAlternateTypeface_10;
	float ___pointSize_11;
	int32_t ___lineNumber_12;
	int32_t ___pageNumber_13;
	int32_t ___vertexIndex_14;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_16;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_17;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_18;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_19;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_20;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_21;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_22;
	float ___origin_23;
	float ___ascender_24;
	float ___baseLine_25;
	float ___descender_26;
	float ___xAdvance_27;
	float ___aspectRatio_28;
	float ___scale_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_32;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_33;
	int32_t ___style_34;
	int32_t ___isVisible_35;
};
#endif // TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#ifndef TMP_GLYPHPAIRADJUSTMENTRECORD_TEF0669284CC50EEFC3EE68A7BC378F285EAD7B76_H
#define TMP_GLYPHPAIRADJUSTMENTRECORD_TEF0669284CC50EEFC3EE68A7BC378F285EAD7B76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_GlyphPairAdjustmentRecord
struct  TMP_GlyphPairAdjustmentRecord_tEF0669284CC50EEFC3EE68A7BC378F285EAD7B76  : public RuntimeObject
{
public:
	// TMPro.TMP_GlyphAdjustmentRecord TMPro.TMP_GlyphPairAdjustmentRecord::m_FirstAdjustmentRecord
	TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58  ___m_FirstAdjustmentRecord_0;
	// TMPro.TMP_GlyphAdjustmentRecord TMPro.TMP_GlyphPairAdjustmentRecord::m_SecondAdjustmentRecord
	TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58  ___m_SecondAdjustmentRecord_1;
	// TMPro.FontFeatureLookupFlags TMPro.TMP_GlyphPairAdjustmentRecord::m_FeatureLookupFlags
	int32_t ___m_FeatureLookupFlags_2;

public:
	inline static int32_t get_offset_of_m_FirstAdjustmentRecord_0() { return static_cast<int32_t>(offsetof(TMP_GlyphPairAdjustmentRecord_tEF0669284CC50EEFC3EE68A7BC378F285EAD7B76, ___m_FirstAdjustmentRecord_0)); }
	inline TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58  get_m_FirstAdjustmentRecord_0() const { return ___m_FirstAdjustmentRecord_0; }
	inline TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58 * get_address_of_m_FirstAdjustmentRecord_0() { return &___m_FirstAdjustmentRecord_0; }
	inline void set_m_FirstAdjustmentRecord_0(TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58  value)
	{
		___m_FirstAdjustmentRecord_0 = value;
	}

	inline static int32_t get_offset_of_m_SecondAdjustmentRecord_1() { return static_cast<int32_t>(offsetof(TMP_GlyphPairAdjustmentRecord_tEF0669284CC50EEFC3EE68A7BC378F285EAD7B76, ___m_SecondAdjustmentRecord_1)); }
	inline TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58  get_m_SecondAdjustmentRecord_1() const { return ___m_SecondAdjustmentRecord_1; }
	inline TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58 * get_address_of_m_SecondAdjustmentRecord_1() { return &___m_SecondAdjustmentRecord_1; }
	inline void set_m_SecondAdjustmentRecord_1(TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58  value)
	{
		___m_SecondAdjustmentRecord_1 = value;
	}

	inline static int32_t get_offset_of_m_FeatureLookupFlags_2() { return static_cast<int32_t>(offsetof(TMP_GlyphPairAdjustmentRecord_tEF0669284CC50EEFC3EE68A7BC378F285EAD7B76, ___m_FeatureLookupFlags_2)); }
	inline int32_t get_m_FeatureLookupFlags_2() const { return ___m_FeatureLookupFlags_2; }
	inline int32_t* get_address_of_m_FeatureLookupFlags_2() { return &___m_FeatureLookupFlags_2; }
	inline void set_m_FeatureLookupFlags_2(int32_t value)
	{
		___m_FeatureLookupFlags_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_GLYPHPAIRADJUSTMENTRECORD_TEF0669284CC50EEFC3EE68A7BC378F285EAD7B76_H
#ifndef TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#define TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineExtents_19)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifndef TMP_MESHINFO_T0140B4A33090360DC5CFB47CD8419369BBE3AD2E_H
#define TMP_MESHINFO_T0140B4A33090360DC5CFB47CD8419369BBE3AD2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MeshInfo
struct  TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E 
{
public:
	// UnityEngine.Mesh TMPro.TMP_MeshInfo::mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_4;
	// System.Int32 TMPro.TMP_MeshInfo::vertexCount
	int32_t ___vertexCount_5;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___vertices_6;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___normals_7;
	// UnityEngine.Vector4[] TMPro.TMP_MeshInfo::tangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___tangents_8;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs0
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___uvs0_9;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs2
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___uvs2_10;
	// UnityEngine.Color32[] TMPro.TMP_MeshInfo::colors32
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___colors32_11;
	// System.Int32[] TMPro.TMP_MeshInfo::triangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___triangles_12;

public:
	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___mesh_4)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_mesh_4() const { return ___mesh_4; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_vertexCount_5() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___vertexCount_5)); }
	inline int32_t get_vertexCount_5() const { return ___vertexCount_5; }
	inline int32_t* get_address_of_vertexCount_5() { return &___vertexCount_5; }
	inline void set_vertexCount_5(int32_t value)
	{
		___vertexCount_5 = value;
	}

	inline static int32_t get_offset_of_vertices_6() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___vertices_6)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_vertices_6() const { return ___vertices_6; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_vertices_6() { return &___vertices_6; }
	inline void set_vertices_6(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___vertices_6 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_6), value);
	}

	inline static int32_t get_offset_of_normals_7() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___normals_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_normals_7() const { return ___normals_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_normals_7() { return &___normals_7; }
	inline void set_normals_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___normals_7 = value;
		Il2CppCodeGenWriteBarrier((&___normals_7), value);
	}

	inline static int32_t get_offset_of_tangents_8() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___tangents_8)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_tangents_8() const { return ___tangents_8; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_tangents_8() { return &___tangents_8; }
	inline void set_tangents_8(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___tangents_8 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_8), value);
	}

	inline static int32_t get_offset_of_uvs0_9() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___uvs0_9)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_uvs0_9() const { return ___uvs0_9; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_uvs0_9() { return &___uvs0_9; }
	inline void set_uvs0_9(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___uvs0_9 = value;
		Il2CppCodeGenWriteBarrier((&___uvs0_9), value);
	}

	inline static int32_t get_offset_of_uvs2_10() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___uvs2_10)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_uvs2_10() const { return ___uvs2_10; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_uvs2_10() { return &___uvs2_10; }
	inline void set_uvs2_10(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___uvs2_10 = value;
		Il2CppCodeGenWriteBarrier((&___uvs2_10), value);
	}

	inline static int32_t get_offset_of_colors32_11() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___colors32_11)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_colors32_11() const { return ___colors32_11; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_colors32_11() { return &___colors32_11; }
	inline void set_colors32_11(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___colors32_11 = value;
		Il2CppCodeGenWriteBarrier((&___colors32_11), value);
	}

	inline static int32_t get_offset_of_triangles_12() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___triangles_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_triangles_12() const { return ___triangles_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_triangles_12() { return &___triangles_12; }
	inline void set_triangles_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___triangles_12 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_12), value);
	}
};

struct TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_MeshInfo::s_DefaultColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___s_DefaultColor_0;
	// UnityEngine.Vector3 TMPro.TMP_MeshInfo::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_1;
	// UnityEngine.Vector4 TMPro.TMP_MeshInfo::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_2;
	// UnityEngine.Bounds TMPro.TMP_MeshInfo::s_DefaultBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___s_DefaultBounds_3;

public:
	inline static int32_t get_offset_of_s_DefaultColor_0() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields, ___s_DefaultColor_0)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_s_DefaultColor_0() const { return ___s_DefaultColor_0; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_s_DefaultColor_0() { return &___s_DefaultColor_0; }
	inline void set_s_DefaultColor_0(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___s_DefaultColor_0 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_1() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields, ___s_DefaultNormal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_1() const { return ___s_DefaultNormal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_1() { return &___s_DefaultNormal_1; }
	inline void set_s_DefaultNormal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_1 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_2() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields, ___s_DefaultTangent_2)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_2() const { return ___s_DefaultTangent_2; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_2() { return &___s_DefaultTangent_2; }
	inline void set_s_DefaultTangent_2(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_2 = value;
	}

	inline static int32_t get_offset_of_s_DefaultBounds_3() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields, ___s_DefaultBounds_3)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_s_DefaultBounds_3() const { return ___s_DefaultBounds_3; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_s_DefaultBounds_3() { return &___s_DefaultBounds_3; }
	inline void set_s_DefaultBounds_3(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___s_DefaultBounds_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_marshaled_pinvoke
{
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_4;
	int32_t ___vertexCount_5;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___vertices_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___normals_7;
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * ___tangents_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___uvs0_9;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___uvs2_10;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * ___colors32_11;
	int32_t* ___triangles_12;
};
// Native definition for COM marshalling of TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_marshaled_com
{
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_4;
	int32_t ___vertexCount_5;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___vertices_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___normals_7;
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * ___tangents_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___uvs0_9;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___uvs2_10;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * ___colors32_11;
	int32_t* ___triangles_12;
};
#endif // TMP_MESHINFO_T0140B4A33090360DC5CFB47CD8419369BBE3AD2E_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#define TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight>
struct  TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_ItemStack_0)); }
	inline FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#define TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_ItemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#ifndef TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#define TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344  : public RuntimeObject
{
public:
	// TMPro.TextElementType TMPro.TMP_TextElement::m_ElementType
	uint8_t ___m_ElementType_0;
	// System.UInt32 TMPro.TMP_TextElement::m_Unicode
	uint32_t ___m_Unicode_1;
	// UnityEngine.TextCore.Glyph TMPro.TMP_TextElement::m_Glyph
	Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 * ___m_Glyph_2;
	// System.UInt32 TMPro.TMP_TextElement::m_GlyphIndex
	uint32_t ___m_GlyphIndex_3;
	// System.Single TMPro.TMP_TextElement::m_Scale
	float ___m_Scale_4;

public:
	inline static int32_t get_offset_of_m_ElementType_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_ElementType_0)); }
	inline uint8_t get_m_ElementType_0() const { return ___m_ElementType_0; }
	inline uint8_t* get_address_of_m_ElementType_0() { return &___m_ElementType_0; }
	inline void set_m_ElementType_0(uint8_t value)
	{
		___m_ElementType_0 = value;
	}

	inline static int32_t get_offset_of_m_Unicode_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_Unicode_1)); }
	inline uint32_t get_m_Unicode_1() const { return ___m_Unicode_1; }
	inline uint32_t* get_address_of_m_Unicode_1() { return &___m_Unicode_1; }
	inline void set_m_Unicode_1(uint32_t value)
	{
		___m_Unicode_1 = value;
	}

	inline static int32_t get_offset_of_m_Glyph_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_Glyph_2)); }
	inline Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 * get_m_Glyph_2() const { return ___m_Glyph_2; }
	inline Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 ** get_address_of_m_Glyph_2() { return &___m_Glyph_2; }
	inline void set_m_Glyph_2(Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 * value)
	{
		___m_Glyph_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Glyph_2), value);
	}

	inline static int32_t get_offset_of_m_GlyphIndex_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_GlyphIndex_3)); }
	inline uint32_t get_m_GlyphIndex_3() const { return ___m_GlyphIndex_3; }
	inline uint32_t* get_address_of_m_GlyphIndex_3() { return &___m_GlyphIndex_3; }
	inline void set_m_GlyphIndex_3(uint32_t value)
	{
		___m_GlyphIndex_3 = value;
	}

	inline static int32_t get_offset_of_m_Scale_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_Scale_4)); }
	inline float get_m_Scale_4() const { return ___m_Scale_4; }
	inline float* get_address_of_m_Scale_4() { return &___m_Scale_4; }
	inline void set_m_Scale_4(float value)
	{
		___m_Scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#ifndef ANIMATIONDATA_T8A65386E26DD73656D34EDAB19E37269F4B36FE0_H
#define ANIMATIONDATA_T8A65386E26DD73656D34EDAB19E37269F4B36FE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AnimationData
struct  AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0  : public RuntimeObject
{
public:
	// System.String TriLib.AnimationData::Name
	String_t* ___Name_0;
	// System.Boolean TriLib.AnimationData::Legacy
	bool ___Legacy_1;
	// System.Single TriLib.AnimationData::Length
	float ___Length_2;
	// System.Single TriLib.AnimationData::FrameRate
	float ___FrameRate_3;
	// UnityEngine.WrapMode TriLib.AnimationData::WrapMode
	int32_t ___WrapMode_4;
	// TriLib.AnimationChannelData[] TriLib.AnimationData::ChannelData
	AnimationChannelDataU5BU5D_tD32C463829B8FA9E6FA5C7F9CE6ACDD5258961F5* ___ChannelData_5;
	// TriLib.MorphChannelData[] TriLib.AnimationData::MorphData
	MorphChannelDataU5BU5D_tAF1809268F138ACD333A34B61857DDD7180374AA* ___MorphData_6;
	// UnityEngine.AnimationClip TriLib.AnimationData::AnimationClip
	AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * ___AnimationClip_7;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Legacy_1() { return static_cast<int32_t>(offsetof(AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0, ___Legacy_1)); }
	inline bool get_Legacy_1() const { return ___Legacy_1; }
	inline bool* get_address_of_Legacy_1() { return &___Legacy_1; }
	inline void set_Legacy_1(bool value)
	{
		___Legacy_1 = value;
	}

	inline static int32_t get_offset_of_Length_2() { return static_cast<int32_t>(offsetof(AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0, ___Length_2)); }
	inline float get_Length_2() const { return ___Length_2; }
	inline float* get_address_of_Length_2() { return &___Length_2; }
	inline void set_Length_2(float value)
	{
		___Length_2 = value;
	}

	inline static int32_t get_offset_of_FrameRate_3() { return static_cast<int32_t>(offsetof(AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0, ___FrameRate_3)); }
	inline float get_FrameRate_3() const { return ___FrameRate_3; }
	inline float* get_address_of_FrameRate_3() { return &___FrameRate_3; }
	inline void set_FrameRate_3(float value)
	{
		___FrameRate_3 = value;
	}

	inline static int32_t get_offset_of_WrapMode_4() { return static_cast<int32_t>(offsetof(AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0, ___WrapMode_4)); }
	inline int32_t get_WrapMode_4() const { return ___WrapMode_4; }
	inline int32_t* get_address_of_WrapMode_4() { return &___WrapMode_4; }
	inline void set_WrapMode_4(int32_t value)
	{
		___WrapMode_4 = value;
	}

	inline static int32_t get_offset_of_ChannelData_5() { return static_cast<int32_t>(offsetof(AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0, ___ChannelData_5)); }
	inline AnimationChannelDataU5BU5D_tD32C463829B8FA9E6FA5C7F9CE6ACDD5258961F5* get_ChannelData_5() const { return ___ChannelData_5; }
	inline AnimationChannelDataU5BU5D_tD32C463829B8FA9E6FA5C7F9CE6ACDD5258961F5** get_address_of_ChannelData_5() { return &___ChannelData_5; }
	inline void set_ChannelData_5(AnimationChannelDataU5BU5D_tD32C463829B8FA9E6FA5C7F9CE6ACDD5258961F5* value)
	{
		___ChannelData_5 = value;
		Il2CppCodeGenWriteBarrier((&___ChannelData_5), value);
	}

	inline static int32_t get_offset_of_MorphData_6() { return static_cast<int32_t>(offsetof(AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0, ___MorphData_6)); }
	inline MorphChannelDataU5BU5D_tAF1809268F138ACD333A34B61857DDD7180374AA* get_MorphData_6() const { return ___MorphData_6; }
	inline MorphChannelDataU5BU5D_tAF1809268F138ACD333A34B61857DDD7180374AA** get_address_of_MorphData_6() { return &___MorphData_6; }
	inline void set_MorphData_6(MorphChannelDataU5BU5D_tAF1809268F138ACD333A34B61857DDD7180374AA* value)
	{
		___MorphData_6 = value;
		Il2CppCodeGenWriteBarrier((&___MorphData_6), value);
	}

	inline static int32_t get_offset_of_AnimationClip_7() { return static_cast<int32_t>(offsetof(AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0, ___AnimationClip_7)); }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * get_AnimationClip_7() const { return ___AnimationClip_7; }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE ** get_address_of_AnimationClip_7() { return &___AnimationClip_7; }
	inline void set_AnimationClip_7(AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * value)
	{
		___AnimationClip_7 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationClip_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONDATA_T8A65386E26DD73656D34EDAB19E37269F4B36FE0_H
#ifndef ASSETLOADER_T5931C7BFC04DB6276B6493399CC257BEAA43F659_H
#define ASSETLOADER_T5931C7BFC04DB6276B6493399CC257BEAA43F659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoader
struct  AssetLoader_t5931C7BFC04DB6276B6493399CC257BEAA43F659  : public AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLOADER_T5931C7BFC04DB6276B6493399CC257BEAA43F659_H
#ifndef ASSETLOADERASYNC_T03A7B7BBBDC2EC8425B8FEE2D6A695A8156C1870_H
#define ASSETLOADERASYNC_T03A7B7BBBDC2EC8425B8FEE2D6A695A8156C1870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderAsync
struct  AssetLoaderAsync_t03A7B7BBBDC2EC8425B8FEE2D6A695A8156C1870  : public AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLOADERASYNC_T03A7B7BBBDC2EC8425B8FEE2D6A695A8156C1870_H
#ifndef ASSIMPMETADATA_T7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD_H
#define ASSIMPMETADATA_T7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpMetadata
struct  AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD  : public RuntimeObject
{
public:
	// TriLib.AssimpMetadataType TriLib.AssimpMetadata::MetadataType
	int32_t ___MetadataType_0;
	// System.UInt32 TriLib.AssimpMetadata::MetadataIndex
	uint32_t ___MetadataIndex_1;
	// System.String TriLib.AssimpMetadata::MetadataKey
	String_t* ___MetadataKey_2;
	// System.Object TriLib.AssimpMetadata::MetadataValue
	RuntimeObject * ___MetadataValue_3;

public:
	inline static int32_t get_offset_of_MetadataType_0() { return static_cast<int32_t>(offsetof(AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD, ___MetadataType_0)); }
	inline int32_t get_MetadataType_0() const { return ___MetadataType_0; }
	inline int32_t* get_address_of_MetadataType_0() { return &___MetadataType_0; }
	inline void set_MetadataType_0(int32_t value)
	{
		___MetadataType_0 = value;
	}

	inline static int32_t get_offset_of_MetadataIndex_1() { return static_cast<int32_t>(offsetof(AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD, ___MetadataIndex_1)); }
	inline uint32_t get_MetadataIndex_1() const { return ___MetadataIndex_1; }
	inline uint32_t* get_address_of_MetadataIndex_1() { return &___MetadataIndex_1; }
	inline void set_MetadataIndex_1(uint32_t value)
	{
		___MetadataIndex_1 = value;
	}

	inline static int32_t get_offset_of_MetadataKey_2() { return static_cast<int32_t>(offsetof(AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD, ___MetadataKey_2)); }
	inline String_t* get_MetadataKey_2() const { return ___MetadataKey_2; }
	inline String_t** get_address_of_MetadataKey_2() { return &___MetadataKey_2; }
	inline void set_MetadataKey_2(String_t* value)
	{
		___MetadataKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___MetadataKey_2), value);
	}

	inline static int32_t get_offset_of_MetadataValue_3() { return static_cast<int32_t>(offsetof(AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD, ___MetadataValue_3)); }
	inline RuntimeObject * get_MetadataValue_3() const { return ___MetadataValue_3; }
	inline RuntimeObject ** get_address_of_MetadataValue_3() { return &___MetadataValue_3; }
	inline void set_MetadataValue_3(RuntimeObject * value)
	{
		___MetadataValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___MetadataValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIMPMETADATA_T7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD_H
#ifndef MATERIALDATA_T27489063F272E033321F0C94EF062695C238CADA_H
#define MATERIALDATA_T27489063F272E033321F0C94EF062695C238CADA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MaterialData
struct  MaterialData_t27489063F272E033321F0C94EF062695C238CADA  : public RuntimeObject
{
public:
	// System.String TriLib.MaterialData::Name
	String_t* ___Name_0;
	// System.Boolean TriLib.MaterialData::AlphaLoaded
	bool ___AlphaLoaded_1;
	// System.Single TriLib.MaterialData::Alpha
	float ___Alpha_2;
	// System.Boolean TriLib.MaterialData::DiffuseInfoLoaded
	bool ___DiffuseInfoLoaded_3;
	// System.String TriLib.MaterialData::DiffusePath
	String_t* ___DiffusePath_4;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::DiffuseWrapMode
	int32_t ___DiffuseWrapMode_5;
	// System.String TriLib.MaterialData::DiffuseName
	String_t* ___DiffuseName_6;
	// System.Single TriLib.MaterialData::DiffuseBlendMode
	float ___DiffuseBlendMode_7;
	// System.UInt32 TriLib.MaterialData::DiffuseOp
	uint32_t ___DiffuseOp_8;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::DiffuseEmbeddedTextureData
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___DiffuseEmbeddedTextureData_9;
	// System.Boolean TriLib.MaterialData::DiffuseColorLoaded
	bool ___DiffuseColorLoaded_10;
	// UnityEngine.Color TriLib.MaterialData::DiffuseColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___DiffuseColor_11;
	// System.Boolean TriLib.MaterialData::EmissionInfoLoaded
	bool ___EmissionInfoLoaded_12;
	// System.String TriLib.MaterialData::EmissionPath
	String_t* ___EmissionPath_13;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::EmissionWrapMode
	int32_t ___EmissionWrapMode_14;
	// System.String TriLib.MaterialData::EmissionName
	String_t* ___EmissionName_15;
	// System.Single TriLib.MaterialData::EmissionBlendMode
	float ___EmissionBlendMode_16;
	// System.UInt32 TriLib.MaterialData::EmissionOp
	uint32_t ___EmissionOp_17;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::EmissionEmbeddedTextureData
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___EmissionEmbeddedTextureData_18;
	// System.Boolean TriLib.MaterialData::EmissionColorLoaded
	bool ___EmissionColorLoaded_19;
	// UnityEngine.Color TriLib.MaterialData::EmissionColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___EmissionColor_20;
	// System.Boolean TriLib.MaterialData::SpecularInfoLoaded
	bool ___SpecularInfoLoaded_21;
	// System.String TriLib.MaterialData::SpecularPath
	String_t* ___SpecularPath_22;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::SpecularWrapMode
	int32_t ___SpecularWrapMode_23;
	// System.String TriLib.MaterialData::SpecularName
	String_t* ___SpecularName_24;
	// System.Single TriLib.MaterialData::SpecularBlendMode
	float ___SpecularBlendMode_25;
	// System.UInt32 TriLib.MaterialData::SpecularOp
	uint32_t ___SpecularOp_26;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::SpecularEmbeddedTextureData
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___SpecularEmbeddedTextureData_27;
	// System.Boolean TriLib.MaterialData::SpecularColorLoaded
	bool ___SpecularColorLoaded_28;
	// UnityEngine.Color TriLib.MaterialData::SpecularColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SpecularColor_29;
	// System.Boolean TriLib.MaterialData::NormalInfoLoaded
	bool ___NormalInfoLoaded_30;
	// System.String TriLib.MaterialData::NormalPath
	String_t* ___NormalPath_31;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::NormalWrapMode
	int32_t ___NormalWrapMode_32;
	// System.String TriLib.MaterialData::NormalName
	String_t* ___NormalName_33;
	// System.Single TriLib.MaterialData::NormalBlendMode
	float ___NormalBlendMode_34;
	// System.UInt32 TriLib.MaterialData::NormalOp
	uint32_t ___NormalOp_35;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::NormalEmbeddedTextureData
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___NormalEmbeddedTextureData_36;
	// System.Boolean TriLib.MaterialData::HeightInfoLoaded
	bool ___HeightInfoLoaded_37;
	// System.String TriLib.MaterialData::HeightPath
	String_t* ___HeightPath_38;
	// UnityEngine.TextureWrapMode TriLib.MaterialData::HeightWrapMode
	int32_t ___HeightWrapMode_39;
	// System.String TriLib.MaterialData::HeightName
	String_t* ___HeightName_40;
	// System.Single TriLib.MaterialData::HeightBlendMode
	float ___HeightBlendMode_41;
	// System.UInt32 TriLib.MaterialData::HeightOp
	uint32_t ___HeightOp_42;
	// TriLib.EmbeddedTextureData TriLib.MaterialData::HeightEmbeddedTextureData
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * ___HeightEmbeddedTextureData_43;
	// System.Boolean TriLib.MaterialData::BumpScaleLoaded
	bool ___BumpScaleLoaded_44;
	// System.Single TriLib.MaterialData::BumpScale
	float ___BumpScale_45;
	// System.Boolean TriLib.MaterialData::GlossinessLoaded
	bool ___GlossinessLoaded_46;
	// System.Single TriLib.MaterialData::Glossiness
	float ___Glossiness_47;
	// System.Boolean TriLib.MaterialData::GlossMapScaleLoaded
	bool ___GlossMapScaleLoaded_48;
	// System.Single TriLib.MaterialData::GlossMapScale
	float ___GlossMapScale_49;
	// UnityEngine.Material TriLib.MaterialData::Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___Material_50;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_AlphaLoaded_1() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___AlphaLoaded_1)); }
	inline bool get_AlphaLoaded_1() const { return ___AlphaLoaded_1; }
	inline bool* get_address_of_AlphaLoaded_1() { return &___AlphaLoaded_1; }
	inline void set_AlphaLoaded_1(bool value)
	{
		___AlphaLoaded_1 = value;
	}

	inline static int32_t get_offset_of_Alpha_2() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___Alpha_2)); }
	inline float get_Alpha_2() const { return ___Alpha_2; }
	inline float* get_address_of_Alpha_2() { return &___Alpha_2; }
	inline void set_Alpha_2(float value)
	{
		___Alpha_2 = value;
	}

	inline static int32_t get_offset_of_DiffuseInfoLoaded_3() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseInfoLoaded_3)); }
	inline bool get_DiffuseInfoLoaded_3() const { return ___DiffuseInfoLoaded_3; }
	inline bool* get_address_of_DiffuseInfoLoaded_3() { return &___DiffuseInfoLoaded_3; }
	inline void set_DiffuseInfoLoaded_3(bool value)
	{
		___DiffuseInfoLoaded_3 = value;
	}

	inline static int32_t get_offset_of_DiffusePath_4() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffusePath_4)); }
	inline String_t* get_DiffusePath_4() const { return ___DiffusePath_4; }
	inline String_t** get_address_of_DiffusePath_4() { return &___DiffusePath_4; }
	inline void set_DiffusePath_4(String_t* value)
	{
		___DiffusePath_4 = value;
		Il2CppCodeGenWriteBarrier((&___DiffusePath_4), value);
	}

	inline static int32_t get_offset_of_DiffuseWrapMode_5() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseWrapMode_5)); }
	inline int32_t get_DiffuseWrapMode_5() const { return ___DiffuseWrapMode_5; }
	inline int32_t* get_address_of_DiffuseWrapMode_5() { return &___DiffuseWrapMode_5; }
	inline void set_DiffuseWrapMode_5(int32_t value)
	{
		___DiffuseWrapMode_5 = value;
	}

	inline static int32_t get_offset_of_DiffuseName_6() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseName_6)); }
	inline String_t* get_DiffuseName_6() const { return ___DiffuseName_6; }
	inline String_t** get_address_of_DiffuseName_6() { return &___DiffuseName_6; }
	inline void set_DiffuseName_6(String_t* value)
	{
		___DiffuseName_6 = value;
		Il2CppCodeGenWriteBarrier((&___DiffuseName_6), value);
	}

	inline static int32_t get_offset_of_DiffuseBlendMode_7() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseBlendMode_7)); }
	inline float get_DiffuseBlendMode_7() const { return ___DiffuseBlendMode_7; }
	inline float* get_address_of_DiffuseBlendMode_7() { return &___DiffuseBlendMode_7; }
	inline void set_DiffuseBlendMode_7(float value)
	{
		___DiffuseBlendMode_7 = value;
	}

	inline static int32_t get_offset_of_DiffuseOp_8() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseOp_8)); }
	inline uint32_t get_DiffuseOp_8() const { return ___DiffuseOp_8; }
	inline uint32_t* get_address_of_DiffuseOp_8() { return &___DiffuseOp_8; }
	inline void set_DiffuseOp_8(uint32_t value)
	{
		___DiffuseOp_8 = value;
	}

	inline static int32_t get_offset_of_DiffuseEmbeddedTextureData_9() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseEmbeddedTextureData_9)); }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * get_DiffuseEmbeddedTextureData_9() const { return ___DiffuseEmbeddedTextureData_9; }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 ** get_address_of_DiffuseEmbeddedTextureData_9() { return &___DiffuseEmbeddedTextureData_9; }
	inline void set_DiffuseEmbeddedTextureData_9(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * value)
	{
		___DiffuseEmbeddedTextureData_9 = value;
		Il2CppCodeGenWriteBarrier((&___DiffuseEmbeddedTextureData_9), value);
	}

	inline static int32_t get_offset_of_DiffuseColorLoaded_10() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseColorLoaded_10)); }
	inline bool get_DiffuseColorLoaded_10() const { return ___DiffuseColorLoaded_10; }
	inline bool* get_address_of_DiffuseColorLoaded_10() { return &___DiffuseColorLoaded_10; }
	inline void set_DiffuseColorLoaded_10(bool value)
	{
		___DiffuseColorLoaded_10 = value;
	}

	inline static int32_t get_offset_of_DiffuseColor_11() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___DiffuseColor_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_DiffuseColor_11() const { return ___DiffuseColor_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_DiffuseColor_11() { return &___DiffuseColor_11; }
	inline void set_DiffuseColor_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___DiffuseColor_11 = value;
	}

	inline static int32_t get_offset_of_EmissionInfoLoaded_12() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionInfoLoaded_12)); }
	inline bool get_EmissionInfoLoaded_12() const { return ___EmissionInfoLoaded_12; }
	inline bool* get_address_of_EmissionInfoLoaded_12() { return &___EmissionInfoLoaded_12; }
	inline void set_EmissionInfoLoaded_12(bool value)
	{
		___EmissionInfoLoaded_12 = value;
	}

	inline static int32_t get_offset_of_EmissionPath_13() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionPath_13)); }
	inline String_t* get_EmissionPath_13() const { return ___EmissionPath_13; }
	inline String_t** get_address_of_EmissionPath_13() { return &___EmissionPath_13; }
	inline void set_EmissionPath_13(String_t* value)
	{
		___EmissionPath_13 = value;
		Il2CppCodeGenWriteBarrier((&___EmissionPath_13), value);
	}

	inline static int32_t get_offset_of_EmissionWrapMode_14() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionWrapMode_14)); }
	inline int32_t get_EmissionWrapMode_14() const { return ___EmissionWrapMode_14; }
	inline int32_t* get_address_of_EmissionWrapMode_14() { return &___EmissionWrapMode_14; }
	inline void set_EmissionWrapMode_14(int32_t value)
	{
		___EmissionWrapMode_14 = value;
	}

	inline static int32_t get_offset_of_EmissionName_15() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionName_15)); }
	inline String_t* get_EmissionName_15() const { return ___EmissionName_15; }
	inline String_t** get_address_of_EmissionName_15() { return &___EmissionName_15; }
	inline void set_EmissionName_15(String_t* value)
	{
		___EmissionName_15 = value;
		Il2CppCodeGenWriteBarrier((&___EmissionName_15), value);
	}

	inline static int32_t get_offset_of_EmissionBlendMode_16() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionBlendMode_16)); }
	inline float get_EmissionBlendMode_16() const { return ___EmissionBlendMode_16; }
	inline float* get_address_of_EmissionBlendMode_16() { return &___EmissionBlendMode_16; }
	inline void set_EmissionBlendMode_16(float value)
	{
		___EmissionBlendMode_16 = value;
	}

	inline static int32_t get_offset_of_EmissionOp_17() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionOp_17)); }
	inline uint32_t get_EmissionOp_17() const { return ___EmissionOp_17; }
	inline uint32_t* get_address_of_EmissionOp_17() { return &___EmissionOp_17; }
	inline void set_EmissionOp_17(uint32_t value)
	{
		___EmissionOp_17 = value;
	}

	inline static int32_t get_offset_of_EmissionEmbeddedTextureData_18() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionEmbeddedTextureData_18)); }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * get_EmissionEmbeddedTextureData_18() const { return ___EmissionEmbeddedTextureData_18; }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 ** get_address_of_EmissionEmbeddedTextureData_18() { return &___EmissionEmbeddedTextureData_18; }
	inline void set_EmissionEmbeddedTextureData_18(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * value)
	{
		___EmissionEmbeddedTextureData_18 = value;
		Il2CppCodeGenWriteBarrier((&___EmissionEmbeddedTextureData_18), value);
	}

	inline static int32_t get_offset_of_EmissionColorLoaded_19() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionColorLoaded_19)); }
	inline bool get_EmissionColorLoaded_19() const { return ___EmissionColorLoaded_19; }
	inline bool* get_address_of_EmissionColorLoaded_19() { return &___EmissionColorLoaded_19; }
	inline void set_EmissionColorLoaded_19(bool value)
	{
		___EmissionColorLoaded_19 = value;
	}

	inline static int32_t get_offset_of_EmissionColor_20() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___EmissionColor_20)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_EmissionColor_20() const { return ___EmissionColor_20; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_EmissionColor_20() { return &___EmissionColor_20; }
	inline void set_EmissionColor_20(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___EmissionColor_20 = value;
	}

	inline static int32_t get_offset_of_SpecularInfoLoaded_21() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularInfoLoaded_21)); }
	inline bool get_SpecularInfoLoaded_21() const { return ___SpecularInfoLoaded_21; }
	inline bool* get_address_of_SpecularInfoLoaded_21() { return &___SpecularInfoLoaded_21; }
	inline void set_SpecularInfoLoaded_21(bool value)
	{
		___SpecularInfoLoaded_21 = value;
	}

	inline static int32_t get_offset_of_SpecularPath_22() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularPath_22)); }
	inline String_t* get_SpecularPath_22() const { return ___SpecularPath_22; }
	inline String_t** get_address_of_SpecularPath_22() { return &___SpecularPath_22; }
	inline void set_SpecularPath_22(String_t* value)
	{
		___SpecularPath_22 = value;
		Il2CppCodeGenWriteBarrier((&___SpecularPath_22), value);
	}

	inline static int32_t get_offset_of_SpecularWrapMode_23() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularWrapMode_23)); }
	inline int32_t get_SpecularWrapMode_23() const { return ___SpecularWrapMode_23; }
	inline int32_t* get_address_of_SpecularWrapMode_23() { return &___SpecularWrapMode_23; }
	inline void set_SpecularWrapMode_23(int32_t value)
	{
		___SpecularWrapMode_23 = value;
	}

	inline static int32_t get_offset_of_SpecularName_24() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularName_24)); }
	inline String_t* get_SpecularName_24() const { return ___SpecularName_24; }
	inline String_t** get_address_of_SpecularName_24() { return &___SpecularName_24; }
	inline void set_SpecularName_24(String_t* value)
	{
		___SpecularName_24 = value;
		Il2CppCodeGenWriteBarrier((&___SpecularName_24), value);
	}

	inline static int32_t get_offset_of_SpecularBlendMode_25() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularBlendMode_25)); }
	inline float get_SpecularBlendMode_25() const { return ___SpecularBlendMode_25; }
	inline float* get_address_of_SpecularBlendMode_25() { return &___SpecularBlendMode_25; }
	inline void set_SpecularBlendMode_25(float value)
	{
		___SpecularBlendMode_25 = value;
	}

	inline static int32_t get_offset_of_SpecularOp_26() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularOp_26)); }
	inline uint32_t get_SpecularOp_26() const { return ___SpecularOp_26; }
	inline uint32_t* get_address_of_SpecularOp_26() { return &___SpecularOp_26; }
	inline void set_SpecularOp_26(uint32_t value)
	{
		___SpecularOp_26 = value;
	}

	inline static int32_t get_offset_of_SpecularEmbeddedTextureData_27() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularEmbeddedTextureData_27)); }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * get_SpecularEmbeddedTextureData_27() const { return ___SpecularEmbeddedTextureData_27; }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 ** get_address_of_SpecularEmbeddedTextureData_27() { return &___SpecularEmbeddedTextureData_27; }
	inline void set_SpecularEmbeddedTextureData_27(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * value)
	{
		___SpecularEmbeddedTextureData_27 = value;
		Il2CppCodeGenWriteBarrier((&___SpecularEmbeddedTextureData_27), value);
	}

	inline static int32_t get_offset_of_SpecularColorLoaded_28() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularColorLoaded_28)); }
	inline bool get_SpecularColorLoaded_28() const { return ___SpecularColorLoaded_28; }
	inline bool* get_address_of_SpecularColorLoaded_28() { return &___SpecularColorLoaded_28; }
	inline void set_SpecularColorLoaded_28(bool value)
	{
		___SpecularColorLoaded_28 = value;
	}

	inline static int32_t get_offset_of_SpecularColor_29() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___SpecularColor_29)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SpecularColor_29() const { return ___SpecularColor_29; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SpecularColor_29() { return &___SpecularColor_29; }
	inline void set_SpecularColor_29(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SpecularColor_29 = value;
	}

	inline static int32_t get_offset_of_NormalInfoLoaded_30() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalInfoLoaded_30)); }
	inline bool get_NormalInfoLoaded_30() const { return ___NormalInfoLoaded_30; }
	inline bool* get_address_of_NormalInfoLoaded_30() { return &___NormalInfoLoaded_30; }
	inline void set_NormalInfoLoaded_30(bool value)
	{
		___NormalInfoLoaded_30 = value;
	}

	inline static int32_t get_offset_of_NormalPath_31() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalPath_31)); }
	inline String_t* get_NormalPath_31() const { return ___NormalPath_31; }
	inline String_t** get_address_of_NormalPath_31() { return &___NormalPath_31; }
	inline void set_NormalPath_31(String_t* value)
	{
		___NormalPath_31 = value;
		Il2CppCodeGenWriteBarrier((&___NormalPath_31), value);
	}

	inline static int32_t get_offset_of_NormalWrapMode_32() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalWrapMode_32)); }
	inline int32_t get_NormalWrapMode_32() const { return ___NormalWrapMode_32; }
	inline int32_t* get_address_of_NormalWrapMode_32() { return &___NormalWrapMode_32; }
	inline void set_NormalWrapMode_32(int32_t value)
	{
		___NormalWrapMode_32 = value;
	}

	inline static int32_t get_offset_of_NormalName_33() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalName_33)); }
	inline String_t* get_NormalName_33() const { return ___NormalName_33; }
	inline String_t** get_address_of_NormalName_33() { return &___NormalName_33; }
	inline void set_NormalName_33(String_t* value)
	{
		___NormalName_33 = value;
		Il2CppCodeGenWriteBarrier((&___NormalName_33), value);
	}

	inline static int32_t get_offset_of_NormalBlendMode_34() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalBlendMode_34)); }
	inline float get_NormalBlendMode_34() const { return ___NormalBlendMode_34; }
	inline float* get_address_of_NormalBlendMode_34() { return &___NormalBlendMode_34; }
	inline void set_NormalBlendMode_34(float value)
	{
		___NormalBlendMode_34 = value;
	}

	inline static int32_t get_offset_of_NormalOp_35() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalOp_35)); }
	inline uint32_t get_NormalOp_35() const { return ___NormalOp_35; }
	inline uint32_t* get_address_of_NormalOp_35() { return &___NormalOp_35; }
	inline void set_NormalOp_35(uint32_t value)
	{
		___NormalOp_35 = value;
	}

	inline static int32_t get_offset_of_NormalEmbeddedTextureData_36() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___NormalEmbeddedTextureData_36)); }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * get_NormalEmbeddedTextureData_36() const { return ___NormalEmbeddedTextureData_36; }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 ** get_address_of_NormalEmbeddedTextureData_36() { return &___NormalEmbeddedTextureData_36; }
	inline void set_NormalEmbeddedTextureData_36(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * value)
	{
		___NormalEmbeddedTextureData_36 = value;
		Il2CppCodeGenWriteBarrier((&___NormalEmbeddedTextureData_36), value);
	}

	inline static int32_t get_offset_of_HeightInfoLoaded_37() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightInfoLoaded_37)); }
	inline bool get_HeightInfoLoaded_37() const { return ___HeightInfoLoaded_37; }
	inline bool* get_address_of_HeightInfoLoaded_37() { return &___HeightInfoLoaded_37; }
	inline void set_HeightInfoLoaded_37(bool value)
	{
		___HeightInfoLoaded_37 = value;
	}

	inline static int32_t get_offset_of_HeightPath_38() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightPath_38)); }
	inline String_t* get_HeightPath_38() const { return ___HeightPath_38; }
	inline String_t** get_address_of_HeightPath_38() { return &___HeightPath_38; }
	inline void set_HeightPath_38(String_t* value)
	{
		___HeightPath_38 = value;
		Il2CppCodeGenWriteBarrier((&___HeightPath_38), value);
	}

	inline static int32_t get_offset_of_HeightWrapMode_39() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightWrapMode_39)); }
	inline int32_t get_HeightWrapMode_39() const { return ___HeightWrapMode_39; }
	inline int32_t* get_address_of_HeightWrapMode_39() { return &___HeightWrapMode_39; }
	inline void set_HeightWrapMode_39(int32_t value)
	{
		___HeightWrapMode_39 = value;
	}

	inline static int32_t get_offset_of_HeightName_40() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightName_40)); }
	inline String_t* get_HeightName_40() const { return ___HeightName_40; }
	inline String_t** get_address_of_HeightName_40() { return &___HeightName_40; }
	inline void set_HeightName_40(String_t* value)
	{
		___HeightName_40 = value;
		Il2CppCodeGenWriteBarrier((&___HeightName_40), value);
	}

	inline static int32_t get_offset_of_HeightBlendMode_41() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightBlendMode_41)); }
	inline float get_HeightBlendMode_41() const { return ___HeightBlendMode_41; }
	inline float* get_address_of_HeightBlendMode_41() { return &___HeightBlendMode_41; }
	inline void set_HeightBlendMode_41(float value)
	{
		___HeightBlendMode_41 = value;
	}

	inline static int32_t get_offset_of_HeightOp_42() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightOp_42)); }
	inline uint32_t get_HeightOp_42() const { return ___HeightOp_42; }
	inline uint32_t* get_address_of_HeightOp_42() { return &___HeightOp_42; }
	inline void set_HeightOp_42(uint32_t value)
	{
		___HeightOp_42 = value;
	}

	inline static int32_t get_offset_of_HeightEmbeddedTextureData_43() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___HeightEmbeddedTextureData_43)); }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * get_HeightEmbeddedTextureData_43() const { return ___HeightEmbeddedTextureData_43; }
	inline EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 ** get_address_of_HeightEmbeddedTextureData_43() { return &___HeightEmbeddedTextureData_43; }
	inline void set_HeightEmbeddedTextureData_43(EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915 * value)
	{
		___HeightEmbeddedTextureData_43 = value;
		Il2CppCodeGenWriteBarrier((&___HeightEmbeddedTextureData_43), value);
	}

	inline static int32_t get_offset_of_BumpScaleLoaded_44() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___BumpScaleLoaded_44)); }
	inline bool get_BumpScaleLoaded_44() const { return ___BumpScaleLoaded_44; }
	inline bool* get_address_of_BumpScaleLoaded_44() { return &___BumpScaleLoaded_44; }
	inline void set_BumpScaleLoaded_44(bool value)
	{
		___BumpScaleLoaded_44 = value;
	}

	inline static int32_t get_offset_of_BumpScale_45() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___BumpScale_45)); }
	inline float get_BumpScale_45() const { return ___BumpScale_45; }
	inline float* get_address_of_BumpScale_45() { return &___BumpScale_45; }
	inline void set_BumpScale_45(float value)
	{
		___BumpScale_45 = value;
	}

	inline static int32_t get_offset_of_GlossinessLoaded_46() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___GlossinessLoaded_46)); }
	inline bool get_GlossinessLoaded_46() const { return ___GlossinessLoaded_46; }
	inline bool* get_address_of_GlossinessLoaded_46() { return &___GlossinessLoaded_46; }
	inline void set_GlossinessLoaded_46(bool value)
	{
		___GlossinessLoaded_46 = value;
	}

	inline static int32_t get_offset_of_Glossiness_47() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___Glossiness_47)); }
	inline float get_Glossiness_47() const { return ___Glossiness_47; }
	inline float* get_address_of_Glossiness_47() { return &___Glossiness_47; }
	inline void set_Glossiness_47(float value)
	{
		___Glossiness_47 = value;
	}

	inline static int32_t get_offset_of_GlossMapScaleLoaded_48() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___GlossMapScaleLoaded_48)); }
	inline bool get_GlossMapScaleLoaded_48() const { return ___GlossMapScaleLoaded_48; }
	inline bool* get_address_of_GlossMapScaleLoaded_48() { return &___GlossMapScaleLoaded_48; }
	inline void set_GlossMapScaleLoaded_48(bool value)
	{
		___GlossMapScaleLoaded_48 = value;
	}

	inline static int32_t get_offset_of_GlossMapScale_49() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___GlossMapScale_49)); }
	inline float get_GlossMapScale_49() const { return ___GlossMapScale_49; }
	inline float* get_address_of_GlossMapScale_49() { return &___GlossMapScale_49; }
	inline void set_GlossMapScale_49(float value)
	{
		___GlossMapScale_49 = value;
	}

	inline static int32_t get_offset_of_Material_50() { return static_cast<int32_t>(offsetof(MaterialData_t27489063F272E033321F0C94EF062695C238CADA, ___Material_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_Material_50() const { return ___Material_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_Material_50() { return &___Material_50; }
	inline void set_Material_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___Material_50 = value;
		Il2CppCodeGenWriteBarrier((&___Material_50), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALDATA_T27489063F272E033321F0C94EF062695C238CADA_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef TMP_ASSET_TE47F21E07C734D11D5DCEA5C0A0264465963CB2D_H
#define TMP_ASSET_TE47F21E07C734D11D5DCEA5C0A0264465963CB2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Asset
struct  TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Int32 TMPro.TMP_Asset::hashCode
	int32_t ___hashCode_4;
	// UnityEngine.Material TMPro.TMP_Asset::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_5;
	// System.Int32 TMPro.TMP_Asset::materialHashCode
	int32_t ___materialHashCode_6;

public:
	inline static int32_t get_offset_of_hashCode_4() { return static_cast<int32_t>(offsetof(TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D, ___hashCode_4)); }
	inline int32_t get_hashCode_4() const { return ___hashCode_4; }
	inline int32_t* get_address_of_hashCode_4() { return &___hashCode_4; }
	inline void set_hashCode_4(int32_t value)
	{
		___hashCode_4 = value;
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D, ___material_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_5() const { return ___material_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_materialHashCode_6() { return static_cast<int32_t>(offsetof(TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D, ___materialHashCode_6)); }
	inline int32_t get_materialHashCode_6() const { return ___materialHashCode_6; }
	inline int32_t* get_address_of_materialHashCode_6() { return &___materialHashCode_6; }
	inline void set_materialHashCode_6(int32_t value)
	{
		___materialHashCode_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_ASSET_TE47F21E07C734D11D5DCEA5C0A0264465963CB2D_H
#ifndef TMP_CHARACTER_T1875AACA978396521498D6A699052C187903553D_H
#define TMP_CHARACTER_T1875AACA978396521498D6A699052C187903553D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Character
struct  TMP_Character_t1875AACA978396521498D6A699052C187903553D  : public TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_CHARACTER_T1875AACA978396521498D6A699052C187903553D_H
#ifndef TMP_COLORGRADIENT_TEA29C4736B1786301A803B6C0FB30107A10D79B7_H
#define TMP_COLORGRADIENT_TEA29C4736B1786301A803B6C0FB30107A10D79B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_ColorGradient
struct  TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// TMPro.ColorMode TMPro.TMP_ColorGradient::colorMode
	int32_t ___colorMode_4;
	// UnityEngine.Color TMPro.TMP_ColorGradient::topLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topLeft_5;
	// UnityEngine.Color TMPro.TMP_ColorGradient::topRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topRight_6;
	// UnityEngine.Color TMPro.TMP_ColorGradient::bottomLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomLeft_7;
	// UnityEngine.Color TMPro.TMP_ColorGradient::bottomRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomRight_8;

public:
	inline static int32_t get_offset_of_colorMode_4() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7, ___colorMode_4)); }
	inline int32_t get_colorMode_4() const { return ___colorMode_4; }
	inline int32_t* get_address_of_colorMode_4() { return &___colorMode_4; }
	inline void set_colorMode_4(int32_t value)
	{
		___colorMode_4 = value;
	}

	inline static int32_t get_offset_of_topLeft_5() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7, ___topLeft_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topLeft_5() const { return ___topLeft_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topLeft_5() { return &___topLeft_5; }
	inline void set_topLeft_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topLeft_5 = value;
	}

	inline static int32_t get_offset_of_topRight_6() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7, ___topRight_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topRight_6() const { return ___topRight_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topRight_6() { return &___topRight_6; }
	inline void set_topRight_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topRight_6 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_7() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7, ___bottomLeft_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomLeft_7() const { return ___bottomLeft_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomLeft_7() { return &___bottomLeft_7; }
	inline void set_bottomLeft_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomLeft_7 = value;
	}

	inline static int32_t get_offset_of_bottomRight_8() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7, ___bottomRight_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomRight_8() const { return ___bottomRight_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomRight_8() { return &___bottomRight_8; }
	inline void set_bottomRight_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomRight_8 = value;
	}
};

struct TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7_StaticFields
{
public:
	// UnityEngine.Color TMPro.TMP_ColorGradient::k_DefaultColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___k_DefaultColor_10;

public:
	inline static int32_t get_offset_of_k_DefaultColor_10() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7_StaticFields, ___k_DefaultColor_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_k_DefaultColor_10() const { return ___k_DefaultColor_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_k_DefaultColor_10() { return &___k_DefaultColor_10; }
	inline void set_k_DefaultColor_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___k_DefaultColor_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_COLORGRADIENT_TEA29C4736B1786301A803B6C0FB30107A10D79B7_H
#ifndef TMP_SETTINGS_T1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A_H
#define TMP_SETTINGS_T1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Settings
struct  TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Boolean TMPro.TMP_Settings::m_enableWordWrapping
	bool ___m_enableWordWrapping_5;
	// System.Boolean TMPro.TMP_Settings::m_enableKerning
	bool ___m_enableKerning_6;
	// System.Boolean TMPro.TMP_Settings::m_enableExtraPadding
	bool ___m_enableExtraPadding_7;
	// System.Boolean TMPro.TMP_Settings::m_enableTintAllSprites
	bool ___m_enableTintAllSprites_8;
	// System.Boolean TMPro.TMP_Settings::m_enableParseEscapeCharacters
	bool ___m_enableParseEscapeCharacters_9;
	// System.Boolean TMPro.TMP_Settings::m_EnableRaycastTarget
	bool ___m_EnableRaycastTarget_10;
	// System.Boolean TMPro.TMP_Settings::m_GetFontFeaturesAtRuntime
	bool ___m_GetFontFeaturesAtRuntime_11;
	// System.Int32 TMPro.TMP_Settings::m_missingGlyphCharacter
	int32_t ___m_missingGlyphCharacter_12;
	// System.Boolean TMPro.TMP_Settings::m_warningsDisabled
	bool ___m_warningsDisabled_13;
	// TMPro.TMP_FontAsset TMPro.TMP_Settings::m_defaultFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_defaultFontAsset_14;
	// System.String TMPro.TMP_Settings::m_defaultFontAssetPath
	String_t* ___m_defaultFontAssetPath_15;
	// System.Single TMPro.TMP_Settings::m_defaultFontSize
	float ___m_defaultFontSize_16;
	// System.Single TMPro.TMP_Settings::m_defaultAutoSizeMinRatio
	float ___m_defaultAutoSizeMinRatio_17;
	// System.Single TMPro.TMP_Settings::m_defaultAutoSizeMaxRatio
	float ___m_defaultAutoSizeMaxRatio_18;
	// UnityEngine.Vector2 TMPro.TMP_Settings::m_defaultTextMeshProTextContainerSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_defaultTextMeshProTextContainerSize_19;
	// UnityEngine.Vector2 TMPro.TMP_Settings::m_defaultTextMeshProUITextContainerSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_defaultTextMeshProUITextContainerSize_20;
	// System.Boolean TMPro.TMP_Settings::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_21;
	// System.Collections.Generic.List`1<TMPro.TMP_FontAsset> TMPro.TMP_Settings::m_fallbackFontAssets
	List_1_t746C622521747C7842E2C567F304B446EA74B8BB * ___m_fallbackFontAssets_22;
	// System.Boolean TMPro.TMP_Settings::m_matchMaterialPreset
	bool ___m_matchMaterialPreset_23;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Settings::m_defaultSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_defaultSpriteAsset_24;
	// System.String TMPro.TMP_Settings::m_defaultSpriteAssetPath
	String_t* ___m_defaultSpriteAssetPath_25;
	// System.String TMPro.TMP_Settings::m_defaultColorGradientPresetsPath
	String_t* ___m_defaultColorGradientPresetsPath_26;
	// System.Boolean TMPro.TMP_Settings::m_enableEmojiSupport
	bool ___m_enableEmojiSupport_27;
	// TMPro.TMP_StyleSheet TMPro.TMP_Settings::m_defaultStyleSheet
	TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04 * ___m_defaultStyleSheet_28;
	// UnityEngine.TextAsset TMPro.TMP_Settings::m_leadingCharacters
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___m_leadingCharacters_29;
	// UnityEngine.TextAsset TMPro.TMP_Settings::m_followingCharacters
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___m_followingCharacters_30;
	// TMPro.TMP_Settings_LineBreakingTable TMPro.TMP_Settings::m_linebreakingRules
	LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25 * ___m_linebreakingRules_31;

public:
	inline static int32_t get_offset_of_m_enableWordWrapping_5() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_enableWordWrapping_5)); }
	inline bool get_m_enableWordWrapping_5() const { return ___m_enableWordWrapping_5; }
	inline bool* get_address_of_m_enableWordWrapping_5() { return &___m_enableWordWrapping_5; }
	inline void set_m_enableWordWrapping_5(bool value)
	{
		___m_enableWordWrapping_5 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_6() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_enableKerning_6)); }
	inline bool get_m_enableKerning_6() const { return ___m_enableKerning_6; }
	inline bool* get_address_of_m_enableKerning_6() { return &___m_enableKerning_6; }
	inline void set_m_enableKerning_6(bool value)
	{
		___m_enableKerning_6 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_7() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_enableExtraPadding_7)); }
	inline bool get_m_enableExtraPadding_7() const { return ___m_enableExtraPadding_7; }
	inline bool* get_address_of_m_enableExtraPadding_7() { return &___m_enableExtraPadding_7; }
	inline void set_m_enableExtraPadding_7(bool value)
	{
		___m_enableExtraPadding_7 = value;
	}

	inline static int32_t get_offset_of_m_enableTintAllSprites_8() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_enableTintAllSprites_8)); }
	inline bool get_m_enableTintAllSprites_8() const { return ___m_enableTintAllSprites_8; }
	inline bool* get_address_of_m_enableTintAllSprites_8() { return &___m_enableTintAllSprites_8; }
	inline void set_m_enableTintAllSprites_8(bool value)
	{
		___m_enableTintAllSprites_8 = value;
	}

	inline static int32_t get_offset_of_m_enableParseEscapeCharacters_9() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_enableParseEscapeCharacters_9)); }
	inline bool get_m_enableParseEscapeCharacters_9() const { return ___m_enableParseEscapeCharacters_9; }
	inline bool* get_address_of_m_enableParseEscapeCharacters_9() { return &___m_enableParseEscapeCharacters_9; }
	inline void set_m_enableParseEscapeCharacters_9(bool value)
	{
		___m_enableParseEscapeCharacters_9 = value;
	}

	inline static int32_t get_offset_of_m_EnableRaycastTarget_10() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_EnableRaycastTarget_10)); }
	inline bool get_m_EnableRaycastTarget_10() const { return ___m_EnableRaycastTarget_10; }
	inline bool* get_address_of_m_EnableRaycastTarget_10() { return &___m_EnableRaycastTarget_10; }
	inline void set_m_EnableRaycastTarget_10(bool value)
	{
		___m_EnableRaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_GetFontFeaturesAtRuntime_11() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_GetFontFeaturesAtRuntime_11)); }
	inline bool get_m_GetFontFeaturesAtRuntime_11() const { return ___m_GetFontFeaturesAtRuntime_11; }
	inline bool* get_address_of_m_GetFontFeaturesAtRuntime_11() { return &___m_GetFontFeaturesAtRuntime_11; }
	inline void set_m_GetFontFeaturesAtRuntime_11(bool value)
	{
		___m_GetFontFeaturesAtRuntime_11 = value;
	}

	inline static int32_t get_offset_of_m_missingGlyphCharacter_12() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_missingGlyphCharacter_12)); }
	inline int32_t get_m_missingGlyphCharacter_12() const { return ___m_missingGlyphCharacter_12; }
	inline int32_t* get_address_of_m_missingGlyphCharacter_12() { return &___m_missingGlyphCharacter_12; }
	inline void set_m_missingGlyphCharacter_12(int32_t value)
	{
		___m_missingGlyphCharacter_12 = value;
	}

	inline static int32_t get_offset_of_m_warningsDisabled_13() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_warningsDisabled_13)); }
	inline bool get_m_warningsDisabled_13() const { return ___m_warningsDisabled_13; }
	inline bool* get_address_of_m_warningsDisabled_13() { return &___m_warningsDisabled_13; }
	inline void set_m_warningsDisabled_13(bool value)
	{
		___m_warningsDisabled_13 = value;
	}

	inline static int32_t get_offset_of_m_defaultFontAsset_14() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultFontAsset_14)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_defaultFontAsset_14() const { return ___m_defaultFontAsset_14; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_defaultFontAsset_14() { return &___m_defaultFontAsset_14; }
	inline void set_m_defaultFontAsset_14(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_defaultFontAsset_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultFontAsset_14), value);
	}

	inline static int32_t get_offset_of_m_defaultFontAssetPath_15() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultFontAssetPath_15)); }
	inline String_t* get_m_defaultFontAssetPath_15() const { return ___m_defaultFontAssetPath_15; }
	inline String_t** get_address_of_m_defaultFontAssetPath_15() { return &___m_defaultFontAssetPath_15; }
	inline void set_m_defaultFontAssetPath_15(String_t* value)
	{
		___m_defaultFontAssetPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultFontAssetPath_15), value);
	}

	inline static int32_t get_offset_of_m_defaultFontSize_16() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultFontSize_16)); }
	inline float get_m_defaultFontSize_16() const { return ___m_defaultFontSize_16; }
	inline float* get_address_of_m_defaultFontSize_16() { return &___m_defaultFontSize_16; }
	inline void set_m_defaultFontSize_16(float value)
	{
		___m_defaultFontSize_16 = value;
	}

	inline static int32_t get_offset_of_m_defaultAutoSizeMinRatio_17() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultAutoSizeMinRatio_17)); }
	inline float get_m_defaultAutoSizeMinRatio_17() const { return ___m_defaultAutoSizeMinRatio_17; }
	inline float* get_address_of_m_defaultAutoSizeMinRatio_17() { return &___m_defaultAutoSizeMinRatio_17; }
	inline void set_m_defaultAutoSizeMinRatio_17(float value)
	{
		___m_defaultAutoSizeMinRatio_17 = value;
	}

	inline static int32_t get_offset_of_m_defaultAutoSizeMaxRatio_18() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultAutoSizeMaxRatio_18)); }
	inline float get_m_defaultAutoSizeMaxRatio_18() const { return ___m_defaultAutoSizeMaxRatio_18; }
	inline float* get_address_of_m_defaultAutoSizeMaxRatio_18() { return &___m_defaultAutoSizeMaxRatio_18; }
	inline void set_m_defaultAutoSizeMaxRatio_18(float value)
	{
		___m_defaultAutoSizeMaxRatio_18 = value;
	}

	inline static int32_t get_offset_of_m_defaultTextMeshProTextContainerSize_19() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultTextMeshProTextContainerSize_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_defaultTextMeshProTextContainerSize_19() const { return ___m_defaultTextMeshProTextContainerSize_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_defaultTextMeshProTextContainerSize_19() { return &___m_defaultTextMeshProTextContainerSize_19; }
	inline void set_m_defaultTextMeshProTextContainerSize_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_defaultTextMeshProTextContainerSize_19 = value;
	}

	inline static int32_t get_offset_of_m_defaultTextMeshProUITextContainerSize_20() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultTextMeshProUITextContainerSize_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_defaultTextMeshProUITextContainerSize_20() const { return ___m_defaultTextMeshProUITextContainerSize_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_defaultTextMeshProUITextContainerSize_20() { return &___m_defaultTextMeshProUITextContainerSize_20; }
	inline void set_m_defaultTextMeshProUITextContainerSize_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_defaultTextMeshProUITextContainerSize_20 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_21() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_autoSizeTextContainer_21)); }
	inline bool get_m_autoSizeTextContainer_21() const { return ___m_autoSizeTextContainer_21; }
	inline bool* get_address_of_m_autoSizeTextContainer_21() { return &___m_autoSizeTextContainer_21; }
	inline void set_m_autoSizeTextContainer_21(bool value)
	{
		___m_autoSizeTextContainer_21 = value;
	}

	inline static int32_t get_offset_of_m_fallbackFontAssets_22() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_fallbackFontAssets_22)); }
	inline List_1_t746C622521747C7842E2C567F304B446EA74B8BB * get_m_fallbackFontAssets_22() const { return ___m_fallbackFontAssets_22; }
	inline List_1_t746C622521747C7842E2C567F304B446EA74B8BB ** get_address_of_m_fallbackFontAssets_22() { return &___m_fallbackFontAssets_22; }
	inline void set_m_fallbackFontAssets_22(List_1_t746C622521747C7842E2C567F304B446EA74B8BB * value)
	{
		___m_fallbackFontAssets_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackFontAssets_22), value);
	}

	inline static int32_t get_offset_of_m_matchMaterialPreset_23() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_matchMaterialPreset_23)); }
	inline bool get_m_matchMaterialPreset_23() const { return ___m_matchMaterialPreset_23; }
	inline bool* get_address_of_m_matchMaterialPreset_23() { return &___m_matchMaterialPreset_23; }
	inline void set_m_matchMaterialPreset_23(bool value)
	{
		___m_matchMaterialPreset_23 = value;
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_24() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultSpriteAsset_24)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_defaultSpriteAsset_24() const { return ___m_defaultSpriteAsset_24; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_defaultSpriteAsset_24() { return &___m_defaultSpriteAsset_24; }
	inline void set_m_defaultSpriteAsset_24(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_defaultSpriteAsset_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_24), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAssetPath_25() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultSpriteAssetPath_25)); }
	inline String_t* get_m_defaultSpriteAssetPath_25() const { return ___m_defaultSpriteAssetPath_25; }
	inline String_t** get_address_of_m_defaultSpriteAssetPath_25() { return &___m_defaultSpriteAssetPath_25; }
	inline void set_m_defaultSpriteAssetPath_25(String_t* value)
	{
		___m_defaultSpriteAssetPath_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAssetPath_25), value);
	}

	inline static int32_t get_offset_of_m_defaultColorGradientPresetsPath_26() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultColorGradientPresetsPath_26)); }
	inline String_t* get_m_defaultColorGradientPresetsPath_26() const { return ___m_defaultColorGradientPresetsPath_26; }
	inline String_t** get_address_of_m_defaultColorGradientPresetsPath_26() { return &___m_defaultColorGradientPresetsPath_26; }
	inline void set_m_defaultColorGradientPresetsPath_26(String_t* value)
	{
		___m_defaultColorGradientPresetsPath_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultColorGradientPresetsPath_26), value);
	}

	inline static int32_t get_offset_of_m_enableEmojiSupport_27() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_enableEmojiSupport_27)); }
	inline bool get_m_enableEmojiSupport_27() const { return ___m_enableEmojiSupport_27; }
	inline bool* get_address_of_m_enableEmojiSupport_27() { return &___m_enableEmojiSupport_27; }
	inline void set_m_enableEmojiSupport_27(bool value)
	{
		___m_enableEmojiSupport_27 = value;
	}

	inline static int32_t get_offset_of_m_defaultStyleSheet_28() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_defaultStyleSheet_28)); }
	inline TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04 * get_m_defaultStyleSheet_28() const { return ___m_defaultStyleSheet_28; }
	inline TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04 ** get_address_of_m_defaultStyleSheet_28() { return &___m_defaultStyleSheet_28; }
	inline void set_m_defaultStyleSheet_28(TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04 * value)
	{
		___m_defaultStyleSheet_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultStyleSheet_28), value);
	}

	inline static int32_t get_offset_of_m_leadingCharacters_29() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_leadingCharacters_29)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_m_leadingCharacters_29() const { return ___m_leadingCharacters_29; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_m_leadingCharacters_29() { return &___m_leadingCharacters_29; }
	inline void set_m_leadingCharacters_29(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___m_leadingCharacters_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_leadingCharacters_29), value);
	}

	inline static int32_t get_offset_of_m_followingCharacters_30() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_followingCharacters_30)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_m_followingCharacters_30() const { return ___m_followingCharacters_30; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_m_followingCharacters_30() { return &___m_followingCharacters_30; }
	inline void set_m_followingCharacters_30(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___m_followingCharacters_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_followingCharacters_30), value);
	}

	inline static int32_t get_offset_of_m_linebreakingRules_31() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A, ___m_linebreakingRules_31)); }
	inline LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25 * get_m_linebreakingRules_31() const { return ___m_linebreakingRules_31; }
	inline LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25 ** get_address_of_m_linebreakingRules_31() { return &___m_linebreakingRules_31; }
	inline void set_m_linebreakingRules_31(LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25 * value)
	{
		___m_linebreakingRules_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_linebreakingRules_31), value);
	}
};

struct TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A_StaticFields
{
public:
	// TMPro.TMP_Settings TMPro.TMP_Settings::s_Instance
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A_StaticFields, ___s_Instance_4)); }
	inline TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A * get_s_Instance_4() const { return ___s_Instance_4; }
	inline TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SETTINGS_T1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A_H
#ifndef WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#define WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	// TMPro.TMP_FontStyleStack TMPro.WordWrapState::basicStyleStack
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___sizeStack_39;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___indentStack_40;
	// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight> TMPro.WordWrapState::fontWeightStack
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___styleStack_42;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___baselineStack_43;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___actionStack_44;
	// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___textInfo_27)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineInfo_28)); }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___vertexColor_29)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColor_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___basicStyleStack_33)); }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorStack_34)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColorStack_35)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColorStack_36)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColorStack_37)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorGradientStack_38)); }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___sizeStack_39)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___indentStack_40)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontWeightStack_41)); }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___styleStack_42)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineStack_43)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___actionStack_44)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___materialReferenceStack_45)); }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineJustificationStack_46)); }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterial_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___meshExtents_52)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___sizeStack_39;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___indentStack_40;
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___styleStack_42;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___baselineStack_43;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___actionStack_44;
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___sizeStack_39;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___indentStack_40;
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___styleStack_42;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___baselineStack_43;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___actionStack_44;
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifndef ANIMATIONCLIPCREATEDHANDLE_T13B0579E3C7ABBF0AF164928743D86D52FA5016F_H
#define ANIMATIONCLIPCREATEDHANDLE_T13B0579E3C7ABBF0AF164928743D86D52FA5016F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AnimationClipCreatedHandle
struct  AnimationClipCreatedHandle_t13B0579E3C7ABBF0AF164928743D86D52FA5016F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIPCREATEDHANDLE_T13B0579E3C7ABBF0AF164928743D86D52FA5016F_H
#ifndef ASSETLOADEROPTIONS_T75FF85F488FF730A40076444651BE4560F65E531_H
#define ASSETLOADEROPTIONS_T75FF85F488FF730A40076444651BE4560F65E531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetLoaderOptions
struct  AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Boolean TriLib.AssetLoaderOptions::AddAssetUnloader
	bool ___AddAssetUnloader_4;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadAnimations
	bool ___DontLoadAnimations_5;
	// System.Boolean TriLib.AssetLoaderOptions::DontApplyAnimations
	bool ___DontApplyAnimations_6;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadLights
	bool ___DontLoadLights_7;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadCameras
	bool ___DontLoadCameras_8;
	// System.Boolean TriLib.AssetLoaderOptions::AutoPlayAnimations
	bool ___AutoPlayAnimations_9;
	// UnityEngine.WrapMode TriLib.AssetLoaderOptions::AnimationWrapMode
	int32_t ___AnimationWrapMode_10;
	// System.Boolean TriLib.AssetLoaderOptions::UseLegacyAnimations
	bool ___UseLegacyAnimations_11;
	// System.Boolean TriLib.AssetLoaderOptions::EnsureQuaternionContinuity
	bool ___EnsureQuaternionContinuity_12;
	// UnityEngine.RuntimeAnimatorController TriLib.AssetLoaderOptions::AnimatorController
	RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * ___AnimatorController_13;
	// UnityEngine.Avatar TriLib.AssetLoaderOptions::Avatar
	Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B * ___Avatar_14;
	// System.Boolean TriLib.AssetLoaderOptions::DontGenerateAvatar
	bool ___DontGenerateAvatar_15;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadMetadata
	bool ___DontLoadMetadata_16;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadMaterials
	bool ___DontLoadMaterials_17;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyColorAlpha
	bool ___ApplyColorAlpha_18;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyDiffuseColor
	bool ___ApplyDiffuseColor_19;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyEmissionColor
	bool ___ApplyEmissionColor_20;
	// System.Boolean TriLib.AssetLoaderOptions::ApplySpecularColor
	bool ___ApplySpecularColor_21;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyDiffuseTexture
	bool ___ApplyDiffuseTexture_22;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyEmissionTexture
	bool ___ApplyEmissionTexture_23;
	// System.Boolean TriLib.AssetLoaderOptions::ApplySpecularTexture
	bool ___ApplySpecularTexture_24;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyNormalTexture
	bool ___ApplyNormalTexture_25;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyDisplacementTexture
	bool ___ApplyDisplacementTexture_26;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyNormalScale
	bool ___ApplyNormalScale_27;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyGlossiness
	bool ___ApplyGlossiness_28;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyGlossinessScale
	bool ___ApplyGlossinessScale_29;
	// System.Boolean TriLib.AssetLoaderOptions::DisableAlphaMaterials
	bool ___DisableAlphaMaterials_30;
	// System.Boolean TriLib.AssetLoaderOptions::ApplyAlphaMaterials
	bool ___ApplyAlphaMaterials_31;
	// System.Boolean TriLib.AssetLoaderOptions::ScanForAlphaMaterials
	bool ___ScanForAlphaMaterials_32;
	// System.Boolean TriLib.AssetLoaderOptions::UseCutoutMaterials
	bool ___UseCutoutMaterials_33;
	// System.Boolean TriLib.AssetLoaderOptions::UseStandardSpecularMaterial
	bool ___UseStandardSpecularMaterial_34;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadMeshes
	bool ___DontLoadMeshes_35;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadBlendShapes
	bool ___DontLoadBlendShapes_36;
	// System.Boolean TriLib.AssetLoaderOptions::DontLoadSkinning
	bool ___DontLoadSkinning_37;
	// System.Boolean TriLib.AssetLoaderOptions::CombineMeshes
	bool ___CombineMeshes_38;
	// System.Boolean TriLib.AssetLoaderOptions::GenerateMeshColliders
	bool ___GenerateMeshColliders_39;
	// System.Boolean TriLib.AssetLoaderOptions::ConvexMeshColliders
	bool ___ConvexMeshColliders_40;
	// UnityEngine.Vector3 TriLib.AssetLoaderOptions::RotationAngles
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RotationAngles_41;
	// System.Single TriLib.AssetLoaderOptions::Scale
	float ___Scale_42;
	// TriLib.AssimpPostProcessSteps TriLib.AssetLoaderOptions::PostProcessSteps
	int32_t ___PostProcessSteps_43;
	// TriLib.TextureCompression TriLib.AssetLoaderOptions::TextureCompression
	int32_t ___TextureCompression_44;
	// UnityEngine.FilterMode TriLib.AssetLoaderOptions::TextureFilterMode
	int32_t ___TextureFilterMode_45;
	// System.Boolean TriLib.AssetLoaderOptions::GenerateMipMaps
	bool ___GenerateMipMaps_46;
	// System.Collections.Generic.List`1<TriLib.AssetAdvancedConfig> TriLib.AssetLoaderOptions::AdvancedConfigs
	List_1_t982F69BB2BFD78219D910A25530B5A9B31ABD5CF * ___AdvancedConfigs_47;

public:
	inline static int32_t get_offset_of_AddAssetUnloader_4() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___AddAssetUnloader_4)); }
	inline bool get_AddAssetUnloader_4() const { return ___AddAssetUnloader_4; }
	inline bool* get_address_of_AddAssetUnloader_4() { return &___AddAssetUnloader_4; }
	inline void set_AddAssetUnloader_4(bool value)
	{
		___AddAssetUnloader_4 = value;
	}

	inline static int32_t get_offset_of_DontLoadAnimations_5() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DontLoadAnimations_5)); }
	inline bool get_DontLoadAnimations_5() const { return ___DontLoadAnimations_5; }
	inline bool* get_address_of_DontLoadAnimations_5() { return &___DontLoadAnimations_5; }
	inline void set_DontLoadAnimations_5(bool value)
	{
		___DontLoadAnimations_5 = value;
	}

	inline static int32_t get_offset_of_DontApplyAnimations_6() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DontApplyAnimations_6)); }
	inline bool get_DontApplyAnimations_6() const { return ___DontApplyAnimations_6; }
	inline bool* get_address_of_DontApplyAnimations_6() { return &___DontApplyAnimations_6; }
	inline void set_DontApplyAnimations_6(bool value)
	{
		___DontApplyAnimations_6 = value;
	}

	inline static int32_t get_offset_of_DontLoadLights_7() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DontLoadLights_7)); }
	inline bool get_DontLoadLights_7() const { return ___DontLoadLights_7; }
	inline bool* get_address_of_DontLoadLights_7() { return &___DontLoadLights_7; }
	inline void set_DontLoadLights_7(bool value)
	{
		___DontLoadLights_7 = value;
	}

	inline static int32_t get_offset_of_DontLoadCameras_8() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DontLoadCameras_8)); }
	inline bool get_DontLoadCameras_8() const { return ___DontLoadCameras_8; }
	inline bool* get_address_of_DontLoadCameras_8() { return &___DontLoadCameras_8; }
	inline void set_DontLoadCameras_8(bool value)
	{
		___DontLoadCameras_8 = value;
	}

	inline static int32_t get_offset_of_AutoPlayAnimations_9() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___AutoPlayAnimations_9)); }
	inline bool get_AutoPlayAnimations_9() const { return ___AutoPlayAnimations_9; }
	inline bool* get_address_of_AutoPlayAnimations_9() { return &___AutoPlayAnimations_9; }
	inline void set_AutoPlayAnimations_9(bool value)
	{
		___AutoPlayAnimations_9 = value;
	}

	inline static int32_t get_offset_of_AnimationWrapMode_10() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___AnimationWrapMode_10)); }
	inline int32_t get_AnimationWrapMode_10() const { return ___AnimationWrapMode_10; }
	inline int32_t* get_address_of_AnimationWrapMode_10() { return &___AnimationWrapMode_10; }
	inline void set_AnimationWrapMode_10(int32_t value)
	{
		___AnimationWrapMode_10 = value;
	}

	inline static int32_t get_offset_of_UseLegacyAnimations_11() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___UseLegacyAnimations_11)); }
	inline bool get_UseLegacyAnimations_11() const { return ___UseLegacyAnimations_11; }
	inline bool* get_address_of_UseLegacyAnimations_11() { return &___UseLegacyAnimations_11; }
	inline void set_UseLegacyAnimations_11(bool value)
	{
		___UseLegacyAnimations_11 = value;
	}

	inline static int32_t get_offset_of_EnsureQuaternionContinuity_12() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___EnsureQuaternionContinuity_12)); }
	inline bool get_EnsureQuaternionContinuity_12() const { return ___EnsureQuaternionContinuity_12; }
	inline bool* get_address_of_EnsureQuaternionContinuity_12() { return &___EnsureQuaternionContinuity_12; }
	inline void set_EnsureQuaternionContinuity_12(bool value)
	{
		___EnsureQuaternionContinuity_12 = value;
	}

	inline static int32_t get_offset_of_AnimatorController_13() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___AnimatorController_13)); }
	inline RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * get_AnimatorController_13() const { return ___AnimatorController_13; }
	inline RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD ** get_address_of_AnimatorController_13() { return &___AnimatorController_13; }
	inline void set_AnimatorController_13(RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * value)
	{
		___AnimatorController_13 = value;
		Il2CppCodeGenWriteBarrier((&___AnimatorController_13), value);
	}

	inline static int32_t get_offset_of_Avatar_14() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___Avatar_14)); }
	inline Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B * get_Avatar_14() const { return ___Avatar_14; }
	inline Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B ** get_address_of_Avatar_14() { return &___Avatar_14; }
	inline void set_Avatar_14(Avatar_t14B515893D5504566D487FFE046DCB8C8C50D02B * value)
	{
		___Avatar_14 = value;
		Il2CppCodeGenWriteBarrier((&___Avatar_14), value);
	}

	inline static int32_t get_offset_of_DontGenerateAvatar_15() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DontGenerateAvatar_15)); }
	inline bool get_DontGenerateAvatar_15() const { return ___DontGenerateAvatar_15; }
	inline bool* get_address_of_DontGenerateAvatar_15() { return &___DontGenerateAvatar_15; }
	inline void set_DontGenerateAvatar_15(bool value)
	{
		___DontGenerateAvatar_15 = value;
	}

	inline static int32_t get_offset_of_DontLoadMetadata_16() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DontLoadMetadata_16)); }
	inline bool get_DontLoadMetadata_16() const { return ___DontLoadMetadata_16; }
	inline bool* get_address_of_DontLoadMetadata_16() { return &___DontLoadMetadata_16; }
	inline void set_DontLoadMetadata_16(bool value)
	{
		___DontLoadMetadata_16 = value;
	}

	inline static int32_t get_offset_of_DontLoadMaterials_17() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DontLoadMaterials_17)); }
	inline bool get_DontLoadMaterials_17() const { return ___DontLoadMaterials_17; }
	inline bool* get_address_of_DontLoadMaterials_17() { return &___DontLoadMaterials_17; }
	inline void set_DontLoadMaterials_17(bool value)
	{
		___DontLoadMaterials_17 = value;
	}

	inline static int32_t get_offset_of_ApplyColorAlpha_18() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyColorAlpha_18)); }
	inline bool get_ApplyColorAlpha_18() const { return ___ApplyColorAlpha_18; }
	inline bool* get_address_of_ApplyColorAlpha_18() { return &___ApplyColorAlpha_18; }
	inline void set_ApplyColorAlpha_18(bool value)
	{
		___ApplyColorAlpha_18 = value;
	}

	inline static int32_t get_offset_of_ApplyDiffuseColor_19() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyDiffuseColor_19)); }
	inline bool get_ApplyDiffuseColor_19() const { return ___ApplyDiffuseColor_19; }
	inline bool* get_address_of_ApplyDiffuseColor_19() { return &___ApplyDiffuseColor_19; }
	inline void set_ApplyDiffuseColor_19(bool value)
	{
		___ApplyDiffuseColor_19 = value;
	}

	inline static int32_t get_offset_of_ApplyEmissionColor_20() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyEmissionColor_20)); }
	inline bool get_ApplyEmissionColor_20() const { return ___ApplyEmissionColor_20; }
	inline bool* get_address_of_ApplyEmissionColor_20() { return &___ApplyEmissionColor_20; }
	inline void set_ApplyEmissionColor_20(bool value)
	{
		___ApplyEmissionColor_20 = value;
	}

	inline static int32_t get_offset_of_ApplySpecularColor_21() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplySpecularColor_21)); }
	inline bool get_ApplySpecularColor_21() const { return ___ApplySpecularColor_21; }
	inline bool* get_address_of_ApplySpecularColor_21() { return &___ApplySpecularColor_21; }
	inline void set_ApplySpecularColor_21(bool value)
	{
		___ApplySpecularColor_21 = value;
	}

	inline static int32_t get_offset_of_ApplyDiffuseTexture_22() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyDiffuseTexture_22)); }
	inline bool get_ApplyDiffuseTexture_22() const { return ___ApplyDiffuseTexture_22; }
	inline bool* get_address_of_ApplyDiffuseTexture_22() { return &___ApplyDiffuseTexture_22; }
	inline void set_ApplyDiffuseTexture_22(bool value)
	{
		___ApplyDiffuseTexture_22 = value;
	}

	inline static int32_t get_offset_of_ApplyEmissionTexture_23() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyEmissionTexture_23)); }
	inline bool get_ApplyEmissionTexture_23() const { return ___ApplyEmissionTexture_23; }
	inline bool* get_address_of_ApplyEmissionTexture_23() { return &___ApplyEmissionTexture_23; }
	inline void set_ApplyEmissionTexture_23(bool value)
	{
		___ApplyEmissionTexture_23 = value;
	}

	inline static int32_t get_offset_of_ApplySpecularTexture_24() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplySpecularTexture_24)); }
	inline bool get_ApplySpecularTexture_24() const { return ___ApplySpecularTexture_24; }
	inline bool* get_address_of_ApplySpecularTexture_24() { return &___ApplySpecularTexture_24; }
	inline void set_ApplySpecularTexture_24(bool value)
	{
		___ApplySpecularTexture_24 = value;
	}

	inline static int32_t get_offset_of_ApplyNormalTexture_25() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyNormalTexture_25)); }
	inline bool get_ApplyNormalTexture_25() const { return ___ApplyNormalTexture_25; }
	inline bool* get_address_of_ApplyNormalTexture_25() { return &___ApplyNormalTexture_25; }
	inline void set_ApplyNormalTexture_25(bool value)
	{
		___ApplyNormalTexture_25 = value;
	}

	inline static int32_t get_offset_of_ApplyDisplacementTexture_26() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyDisplacementTexture_26)); }
	inline bool get_ApplyDisplacementTexture_26() const { return ___ApplyDisplacementTexture_26; }
	inline bool* get_address_of_ApplyDisplacementTexture_26() { return &___ApplyDisplacementTexture_26; }
	inline void set_ApplyDisplacementTexture_26(bool value)
	{
		___ApplyDisplacementTexture_26 = value;
	}

	inline static int32_t get_offset_of_ApplyNormalScale_27() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyNormalScale_27)); }
	inline bool get_ApplyNormalScale_27() const { return ___ApplyNormalScale_27; }
	inline bool* get_address_of_ApplyNormalScale_27() { return &___ApplyNormalScale_27; }
	inline void set_ApplyNormalScale_27(bool value)
	{
		___ApplyNormalScale_27 = value;
	}

	inline static int32_t get_offset_of_ApplyGlossiness_28() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyGlossiness_28)); }
	inline bool get_ApplyGlossiness_28() const { return ___ApplyGlossiness_28; }
	inline bool* get_address_of_ApplyGlossiness_28() { return &___ApplyGlossiness_28; }
	inline void set_ApplyGlossiness_28(bool value)
	{
		___ApplyGlossiness_28 = value;
	}

	inline static int32_t get_offset_of_ApplyGlossinessScale_29() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyGlossinessScale_29)); }
	inline bool get_ApplyGlossinessScale_29() const { return ___ApplyGlossinessScale_29; }
	inline bool* get_address_of_ApplyGlossinessScale_29() { return &___ApplyGlossinessScale_29; }
	inline void set_ApplyGlossinessScale_29(bool value)
	{
		___ApplyGlossinessScale_29 = value;
	}

	inline static int32_t get_offset_of_DisableAlphaMaterials_30() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DisableAlphaMaterials_30)); }
	inline bool get_DisableAlphaMaterials_30() const { return ___DisableAlphaMaterials_30; }
	inline bool* get_address_of_DisableAlphaMaterials_30() { return &___DisableAlphaMaterials_30; }
	inline void set_DisableAlphaMaterials_30(bool value)
	{
		___DisableAlphaMaterials_30 = value;
	}

	inline static int32_t get_offset_of_ApplyAlphaMaterials_31() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ApplyAlphaMaterials_31)); }
	inline bool get_ApplyAlphaMaterials_31() const { return ___ApplyAlphaMaterials_31; }
	inline bool* get_address_of_ApplyAlphaMaterials_31() { return &___ApplyAlphaMaterials_31; }
	inline void set_ApplyAlphaMaterials_31(bool value)
	{
		___ApplyAlphaMaterials_31 = value;
	}

	inline static int32_t get_offset_of_ScanForAlphaMaterials_32() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ScanForAlphaMaterials_32)); }
	inline bool get_ScanForAlphaMaterials_32() const { return ___ScanForAlphaMaterials_32; }
	inline bool* get_address_of_ScanForAlphaMaterials_32() { return &___ScanForAlphaMaterials_32; }
	inline void set_ScanForAlphaMaterials_32(bool value)
	{
		___ScanForAlphaMaterials_32 = value;
	}

	inline static int32_t get_offset_of_UseCutoutMaterials_33() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___UseCutoutMaterials_33)); }
	inline bool get_UseCutoutMaterials_33() const { return ___UseCutoutMaterials_33; }
	inline bool* get_address_of_UseCutoutMaterials_33() { return &___UseCutoutMaterials_33; }
	inline void set_UseCutoutMaterials_33(bool value)
	{
		___UseCutoutMaterials_33 = value;
	}

	inline static int32_t get_offset_of_UseStandardSpecularMaterial_34() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___UseStandardSpecularMaterial_34)); }
	inline bool get_UseStandardSpecularMaterial_34() const { return ___UseStandardSpecularMaterial_34; }
	inline bool* get_address_of_UseStandardSpecularMaterial_34() { return &___UseStandardSpecularMaterial_34; }
	inline void set_UseStandardSpecularMaterial_34(bool value)
	{
		___UseStandardSpecularMaterial_34 = value;
	}

	inline static int32_t get_offset_of_DontLoadMeshes_35() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DontLoadMeshes_35)); }
	inline bool get_DontLoadMeshes_35() const { return ___DontLoadMeshes_35; }
	inline bool* get_address_of_DontLoadMeshes_35() { return &___DontLoadMeshes_35; }
	inline void set_DontLoadMeshes_35(bool value)
	{
		___DontLoadMeshes_35 = value;
	}

	inline static int32_t get_offset_of_DontLoadBlendShapes_36() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DontLoadBlendShapes_36)); }
	inline bool get_DontLoadBlendShapes_36() const { return ___DontLoadBlendShapes_36; }
	inline bool* get_address_of_DontLoadBlendShapes_36() { return &___DontLoadBlendShapes_36; }
	inline void set_DontLoadBlendShapes_36(bool value)
	{
		___DontLoadBlendShapes_36 = value;
	}

	inline static int32_t get_offset_of_DontLoadSkinning_37() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___DontLoadSkinning_37)); }
	inline bool get_DontLoadSkinning_37() const { return ___DontLoadSkinning_37; }
	inline bool* get_address_of_DontLoadSkinning_37() { return &___DontLoadSkinning_37; }
	inline void set_DontLoadSkinning_37(bool value)
	{
		___DontLoadSkinning_37 = value;
	}

	inline static int32_t get_offset_of_CombineMeshes_38() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___CombineMeshes_38)); }
	inline bool get_CombineMeshes_38() const { return ___CombineMeshes_38; }
	inline bool* get_address_of_CombineMeshes_38() { return &___CombineMeshes_38; }
	inline void set_CombineMeshes_38(bool value)
	{
		___CombineMeshes_38 = value;
	}

	inline static int32_t get_offset_of_GenerateMeshColliders_39() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___GenerateMeshColliders_39)); }
	inline bool get_GenerateMeshColliders_39() const { return ___GenerateMeshColliders_39; }
	inline bool* get_address_of_GenerateMeshColliders_39() { return &___GenerateMeshColliders_39; }
	inline void set_GenerateMeshColliders_39(bool value)
	{
		___GenerateMeshColliders_39 = value;
	}

	inline static int32_t get_offset_of_ConvexMeshColliders_40() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___ConvexMeshColliders_40)); }
	inline bool get_ConvexMeshColliders_40() const { return ___ConvexMeshColliders_40; }
	inline bool* get_address_of_ConvexMeshColliders_40() { return &___ConvexMeshColliders_40; }
	inline void set_ConvexMeshColliders_40(bool value)
	{
		___ConvexMeshColliders_40 = value;
	}

	inline static int32_t get_offset_of_RotationAngles_41() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___RotationAngles_41)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RotationAngles_41() const { return ___RotationAngles_41; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RotationAngles_41() { return &___RotationAngles_41; }
	inline void set_RotationAngles_41(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RotationAngles_41 = value;
	}

	inline static int32_t get_offset_of_Scale_42() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___Scale_42)); }
	inline float get_Scale_42() const { return ___Scale_42; }
	inline float* get_address_of_Scale_42() { return &___Scale_42; }
	inline void set_Scale_42(float value)
	{
		___Scale_42 = value;
	}

	inline static int32_t get_offset_of_PostProcessSteps_43() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___PostProcessSteps_43)); }
	inline int32_t get_PostProcessSteps_43() const { return ___PostProcessSteps_43; }
	inline int32_t* get_address_of_PostProcessSteps_43() { return &___PostProcessSteps_43; }
	inline void set_PostProcessSteps_43(int32_t value)
	{
		___PostProcessSteps_43 = value;
	}

	inline static int32_t get_offset_of_TextureCompression_44() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___TextureCompression_44)); }
	inline int32_t get_TextureCompression_44() const { return ___TextureCompression_44; }
	inline int32_t* get_address_of_TextureCompression_44() { return &___TextureCompression_44; }
	inline void set_TextureCompression_44(int32_t value)
	{
		___TextureCompression_44 = value;
	}

	inline static int32_t get_offset_of_TextureFilterMode_45() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___TextureFilterMode_45)); }
	inline int32_t get_TextureFilterMode_45() const { return ___TextureFilterMode_45; }
	inline int32_t* get_address_of_TextureFilterMode_45() { return &___TextureFilterMode_45; }
	inline void set_TextureFilterMode_45(int32_t value)
	{
		___TextureFilterMode_45 = value;
	}

	inline static int32_t get_offset_of_GenerateMipMaps_46() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___GenerateMipMaps_46)); }
	inline bool get_GenerateMipMaps_46() const { return ___GenerateMipMaps_46; }
	inline bool* get_address_of_GenerateMipMaps_46() { return &___GenerateMipMaps_46; }
	inline void set_GenerateMipMaps_46(bool value)
	{
		___GenerateMipMaps_46 = value;
	}

	inline static int32_t get_offset_of_AdvancedConfigs_47() { return static_cast<int32_t>(offsetof(AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531, ___AdvancedConfigs_47)); }
	inline List_1_t982F69BB2BFD78219D910A25530B5A9B31ABD5CF * get_AdvancedConfigs_47() const { return ___AdvancedConfigs_47; }
	inline List_1_t982F69BB2BFD78219D910A25530B5A9B31ABD5CF ** get_address_of_AdvancedConfigs_47() { return &___AdvancedConfigs_47; }
	inline void set_AdvancedConfigs_47(List_1_t982F69BB2BFD78219D910A25530B5A9B31ABD5CF * value)
	{
		___AdvancedConfigs_47 = value;
		Il2CppCodeGenWriteBarrier((&___AdvancedConfigs_47), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETLOADEROPTIONS_T75FF85F488FF730A40076444651BE4560F65E531_H
#ifndef DATACALLBACK_T0C125601C6F711EE7EBBFFC24B0A519E2243A0AF_H
#define DATACALLBACK_T0C125601C6F711EE7EBBFFC24B0A519E2243A0AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop_DataCallback
struct  DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACALLBACK_T0C125601C6F711EE7EBBFFC24B0A519E2243A0AF_H
#ifndef EXISTSCALLBACK_T8473DF0E166AE4F98314FBD5199893CA67012C2F_H
#define EXISTSCALLBACK_T8473DF0E166AE4F98314FBD5199893CA67012C2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop_ExistsCallback
struct  ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXISTSCALLBACK_T8473DF0E166AE4F98314FBD5199893CA67012C2F_H
#ifndef PROGRESSCALLBACK_T39D480CF4ABBF766B79A16905486E417FE40DBE6_H
#define PROGRESSCALLBACK_T39D480CF4ABBF766B79A16905486E417FE40DBE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssimpInterop_ProgressCallback
struct  ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSCALLBACK_T39D480CF4ABBF766B79A16905486E417FE40DBE6_H
#ifndef AVATARCREATEDHANDLE_T7B8714B52752D992F191D2D5E77CC71206196B80_H
#define AVATARCREATEDHANDLE_T7B8714B52752D992F191D2D5E77CC71206196B80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AvatarCreatedHandle
struct  AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARCREATEDHANDLE_T7B8714B52752D992F191D2D5E77CC71206196B80_H
#ifndef BLENDSHAPEKEYCREATEDHANDLE_TE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B_H
#define BLENDSHAPEKEYCREATEDHANDLE_TE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.BlendShapeKeyCreatedHandle
struct  BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSHAPEKEYCREATEDHANDLE_TE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B_H
#ifndef DATADISPOSALCALLBACK_T6963DE3A2EE2117382E3DDC8435453FC38426BFB_H
#define DATADISPOSALCALLBACK_T6963DE3A2EE2117382E3DDC8435453FC38426BFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.DataDisposalCallback
struct  DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATADISPOSALCALLBACK_T6963DE3A2EE2117382E3DDC8435453FC38426BFB_H
#ifndef EMBEDDEDTEXTURELOADCALLBACK_TD05D49054BB811D79C7297F03927D06DE9E8F3E7_H
#define EMBEDDEDTEXTURELOADCALLBACK_TD05D49054BB811D79C7297F03927D06DE9E8F3E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.EmbeddedTextureLoadCallback
struct  EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMBEDDEDTEXTURELOADCALLBACK_TD05D49054BB811D79C7297F03927D06DE9E8F3E7_H
#ifndef LOADTEXTUREDATACALLBACK_TE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3_H
#define LOADTEXTUREDATACALLBACK_TE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.LoadTextureDataCallback
struct  LoadTextureDataCallback_tE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADTEXTUREDATACALLBACK_TE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3_H
#ifndef MATERIALCREATEDHANDLE_TCFCF9310D550A45C8C968E6E65FA3254E5395F78_H
#define MATERIALCREATEDHANDLE_TCFCF9310D550A45C8C968E6E65FA3254E5395F78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MaterialCreatedHandle
struct  MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALCREATEDHANDLE_TCFCF9310D550A45C8C968E6E65FA3254E5395F78_H
#ifndef MESHCREATEDHANDLE_T3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3_H
#define MESHCREATEDHANDLE_T3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MeshCreatedHandle
struct  MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCREATEDHANDLE_T3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3_H
#ifndef METADATAPROCESSEDHANDLE_TF83A79A9D6E56AD891A607A5DB625A0E564EE63E_H
#define METADATAPROCESSEDHANDLE_TF83A79A9D6E56AD891A607A5DB625A0E564EE63E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.MetadataProcessedHandle
struct  MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROCESSEDHANDLE_TF83A79A9D6E56AD891A607A5DB625A0E564EE63E_H
#ifndef OBJECTLOADEDHANDLE_TE2DB0F8E41A86557CE40122A88339551B399D044_H
#define OBJECTLOADEDHANDLE_TE2DB0F8E41A86557CE40122A88339551B399D044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.ObjectLoadedHandle
struct  ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTLOADEDHANDLE_TE2DB0F8E41A86557CE40122A88339551B399D044_H
#ifndef TEXTURELOADHANDLE_T29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D_H
#define TEXTURELOADHANDLE_T29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TextureLoadHandle
struct  TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURELOADHANDLE_T29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D_H
#ifndef TEXTUREPRELOADHANDLE_TC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23_H
#define TEXTUREPRELOADHANDLE_TC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.TexturePreLoadHandle
struct  TexturePreLoadHandle_tC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPRELOADHANDLE_TC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef TMP_FONTASSET_T44D2006105B39FB33AE5A0ADF07A7EF36C72385C_H
#define TMP_FONTASSET_T44D2006105B39FB33AE5A0ADF07A7EF36C72385C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontAsset
struct  TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C  : public TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D
{
public:
	// System.String TMPro.TMP_FontAsset::m_Version
	String_t* ___m_Version_7;
	// System.String TMPro.TMP_FontAsset::m_SourceFontFileGUID
	String_t* ___m_SourceFontFileGUID_8;
	// UnityEngine.Font TMPro.TMP_FontAsset::m_SourceFontFile
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___m_SourceFontFile_9;
	// TMPro.AtlasPopulationMode TMPro.TMP_FontAsset::m_AtlasPopulationMode
	int32_t ___m_AtlasPopulationMode_10;
	// UnityEngine.TextCore.FaceInfo TMPro.TMP_FontAsset::m_FaceInfo
	FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8  ___m_FaceInfo_11;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Glyph> TMPro.TMP_FontAsset::m_GlyphTable
	List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * ___m_GlyphTable_12;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph> TMPro.TMP_FontAsset::m_GlyphLookupDictionary
	Dictionary_2_t572EE3ED0678E28D8D2E0199F7C33E24756FB72B * ___m_GlyphLookupDictionary_13;
	// System.Collections.Generic.List`1<TMPro.TMP_Character> TMPro.TMP_FontAsset::m_CharacterTable
	List_1_tA95ABBEB9024057D7EEFADA755FABA1E3150CBED * ___m_CharacterTable_14;
	// System.Collections.Generic.Dictionary`2<System.UInt32,TMPro.TMP_Character> TMPro.TMP_FontAsset::m_CharacterLookupDictionary
	Dictionary_2_tE7F15226C09DF54159023A3FC4A77F9997A61F1B * ___m_CharacterLookupDictionary_15;
	// UnityEngine.Texture2D TMPro.TMP_FontAsset::m_AtlasTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_AtlasTexture_16;
	// UnityEngine.Texture2D[] TMPro.TMP_FontAsset::m_AtlasTextures
	Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* ___m_AtlasTextures_17;
	// System.Int32 TMPro.TMP_FontAsset::m_AtlasTextureIndex
	int32_t ___m_AtlasTextureIndex_18;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.GlyphRect> TMPro.TMP_FontAsset::m_UsedGlyphRects
	List_1_tD87292C3DA9A1BCF7BE7A6A63897ABF69A015D65 * ___m_UsedGlyphRects_19;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.GlyphRect> TMPro.TMP_FontAsset::m_FreeGlyphRects
	List_1_tD87292C3DA9A1BCF7BE7A6A63897ABF69A015D65 * ___m_FreeGlyphRects_20;
	// TMPro.FaceInfo_Legacy TMPro.TMP_FontAsset::m_fontInfo
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E * ___m_fontInfo_21;
	// UnityEngine.Texture2D TMPro.TMP_FontAsset::atlas
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___atlas_22;
	// System.Int32 TMPro.TMP_FontAsset::m_AtlasWidth
	int32_t ___m_AtlasWidth_23;
	// System.Int32 TMPro.TMP_FontAsset::m_AtlasHeight
	int32_t ___m_AtlasHeight_24;
	// System.Int32 TMPro.TMP_FontAsset::m_AtlasPadding
	int32_t ___m_AtlasPadding_25;
	// UnityEngine.TextCore.LowLevel.GlyphRenderMode TMPro.TMP_FontAsset::m_AtlasRenderMode
	int32_t ___m_AtlasRenderMode_26;
	// System.Collections.Generic.List`1<TMPro.TMP_Glyph> TMPro.TMP_FontAsset::m_glyphInfoList
	List_1_tB2A7609CA52574815578619F788242AB43EC2C82 * ___m_glyphInfoList_27;
	// TMPro.KerningTable TMPro.TMP_FontAsset::m_KerningTable
	KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F * ___m_KerningTable_28;
	// TMPro.TMP_FontFeatureTable TMPro.TMP_FontAsset::m_FontFeatureTable
	TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756 * ___m_FontFeatureTable_29;
	// System.Collections.Generic.List`1<TMPro.TMP_FontAsset> TMPro.TMP_FontAsset::fallbackFontAssets
	List_1_t746C622521747C7842E2C567F304B446EA74B8BB * ___fallbackFontAssets_30;
	// System.Collections.Generic.List`1<TMPro.TMP_FontAsset> TMPro.TMP_FontAsset::m_FallbackFontAssetTable
	List_1_t746C622521747C7842E2C567F304B446EA74B8BB * ___m_FallbackFontAssetTable_31;
	// TMPro.FontAssetCreationSettings TMPro.TMP_FontAsset::m_CreationSettings
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4  ___m_CreationSettings_32;
	// TMPro.TMP_FontWeightPair[] TMPro.TMP_FontAsset::m_FontWeightTable
	TMP_FontWeightPairU5BU5D_tD4C8F5F8465CC6A30370C93F43B43BE3147DA68D* ___m_FontWeightTable_33;
	// TMPro.TMP_FontWeightPair[] TMPro.TMP_FontAsset::fontWeights
	TMP_FontWeightPairU5BU5D_tD4C8F5F8465CC6A30370C93F43B43BE3147DA68D* ___fontWeights_34;
	// System.Single TMPro.TMP_FontAsset::normalStyle
	float ___normalStyle_35;
	// System.Single TMPro.TMP_FontAsset::normalSpacingOffset
	float ___normalSpacingOffset_36;
	// System.Single TMPro.TMP_FontAsset::boldStyle
	float ___boldStyle_37;
	// System.Single TMPro.TMP_FontAsset::boldSpacing
	float ___boldSpacing_38;
	// System.Byte TMPro.TMP_FontAsset::italicStyle
	uint8_t ___italicStyle_39;
	// System.Byte TMPro.TMP_FontAsset::tabSize
	uint8_t ___tabSize_40;
	// System.Byte TMPro.TMP_FontAsset::m_oldTabSize
	uint8_t ___m_oldTabSize_41;
	// System.Boolean TMPro.TMP_FontAsset::m_IsFontAssetLookupTablesDirty
	bool ___m_IsFontAssetLookupTablesDirty_42;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Glyph> TMPro.TMP_FontAsset::m_GlyphsToPack
	List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * ___m_GlyphsToPack_43;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Glyph> TMPro.TMP_FontAsset::m_GlyphsPacked
	List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * ___m_GlyphsPacked_44;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Glyph> TMPro.TMP_FontAsset::m_GlyphsToRender
	List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * ___m_GlyphsToRender_45;
	// System.Collections.Generic.List`1<System.UInt32> TMPro.TMP_FontAsset::m_GlyphIndexList
	List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E * ___m_GlyphIndexList_46;
	// System.Collections.Generic.List`1<TMPro.TMP_Character> TMPro.TMP_FontAsset::m_CharactersToAdd
	List_1_tA95ABBEB9024057D7EEFADA755FABA1E3150CBED * ___m_CharactersToAdd_47;

public:
	inline static int32_t get_offset_of_m_Version_7() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_Version_7)); }
	inline String_t* get_m_Version_7() const { return ___m_Version_7; }
	inline String_t** get_address_of_m_Version_7() { return &___m_Version_7; }
	inline void set_m_Version_7(String_t* value)
	{
		___m_Version_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Version_7), value);
	}

	inline static int32_t get_offset_of_m_SourceFontFileGUID_8() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_SourceFontFileGUID_8)); }
	inline String_t* get_m_SourceFontFileGUID_8() const { return ___m_SourceFontFileGUID_8; }
	inline String_t** get_address_of_m_SourceFontFileGUID_8() { return &___m_SourceFontFileGUID_8; }
	inline void set_m_SourceFontFileGUID_8(String_t* value)
	{
		___m_SourceFontFileGUID_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceFontFileGUID_8), value);
	}

	inline static int32_t get_offset_of_m_SourceFontFile_9() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_SourceFontFile_9)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_m_SourceFontFile_9() const { return ___m_SourceFontFile_9; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_m_SourceFontFile_9() { return &___m_SourceFontFile_9; }
	inline void set_m_SourceFontFile_9(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___m_SourceFontFile_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceFontFile_9), value);
	}

	inline static int32_t get_offset_of_m_AtlasPopulationMode_10() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_AtlasPopulationMode_10)); }
	inline int32_t get_m_AtlasPopulationMode_10() const { return ___m_AtlasPopulationMode_10; }
	inline int32_t* get_address_of_m_AtlasPopulationMode_10() { return &___m_AtlasPopulationMode_10; }
	inline void set_m_AtlasPopulationMode_10(int32_t value)
	{
		___m_AtlasPopulationMode_10 = value;
	}

	inline static int32_t get_offset_of_m_FaceInfo_11() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_FaceInfo_11)); }
	inline FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8  get_m_FaceInfo_11() const { return ___m_FaceInfo_11; }
	inline FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8 * get_address_of_m_FaceInfo_11() { return &___m_FaceInfo_11; }
	inline void set_m_FaceInfo_11(FaceInfo_t32155CB9E0D125155E829A3D23119FB323F382A8  value)
	{
		___m_FaceInfo_11 = value;
	}

	inline static int32_t get_offset_of_m_GlyphTable_12() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_GlyphTable_12)); }
	inline List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * get_m_GlyphTable_12() const { return ___m_GlyphTable_12; }
	inline List_1_t8B3AA8D740B2E10383225037638055610A7BE123 ** get_address_of_m_GlyphTable_12() { return &___m_GlyphTable_12; }
	inline void set_m_GlyphTable_12(List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * value)
	{
		___m_GlyphTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlyphTable_12), value);
	}

	inline static int32_t get_offset_of_m_GlyphLookupDictionary_13() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_GlyphLookupDictionary_13)); }
	inline Dictionary_2_t572EE3ED0678E28D8D2E0199F7C33E24756FB72B * get_m_GlyphLookupDictionary_13() const { return ___m_GlyphLookupDictionary_13; }
	inline Dictionary_2_t572EE3ED0678E28D8D2E0199F7C33E24756FB72B ** get_address_of_m_GlyphLookupDictionary_13() { return &___m_GlyphLookupDictionary_13; }
	inline void set_m_GlyphLookupDictionary_13(Dictionary_2_t572EE3ED0678E28D8D2E0199F7C33E24756FB72B * value)
	{
		___m_GlyphLookupDictionary_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlyphLookupDictionary_13), value);
	}

	inline static int32_t get_offset_of_m_CharacterTable_14() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_CharacterTable_14)); }
	inline List_1_tA95ABBEB9024057D7EEFADA755FABA1E3150CBED * get_m_CharacterTable_14() const { return ___m_CharacterTable_14; }
	inline List_1_tA95ABBEB9024057D7EEFADA755FABA1E3150CBED ** get_address_of_m_CharacterTable_14() { return &___m_CharacterTable_14; }
	inline void set_m_CharacterTable_14(List_1_tA95ABBEB9024057D7EEFADA755FABA1E3150CBED * value)
	{
		___m_CharacterTable_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_CharacterTable_14), value);
	}

	inline static int32_t get_offset_of_m_CharacterLookupDictionary_15() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_CharacterLookupDictionary_15)); }
	inline Dictionary_2_tE7F15226C09DF54159023A3FC4A77F9997A61F1B * get_m_CharacterLookupDictionary_15() const { return ___m_CharacterLookupDictionary_15; }
	inline Dictionary_2_tE7F15226C09DF54159023A3FC4A77F9997A61F1B ** get_address_of_m_CharacterLookupDictionary_15() { return &___m_CharacterLookupDictionary_15; }
	inline void set_m_CharacterLookupDictionary_15(Dictionary_2_tE7F15226C09DF54159023A3FC4A77F9997A61F1B * value)
	{
		___m_CharacterLookupDictionary_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CharacterLookupDictionary_15), value);
	}

	inline static int32_t get_offset_of_m_AtlasTexture_16() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_AtlasTexture_16)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_AtlasTexture_16() const { return ___m_AtlasTexture_16; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_AtlasTexture_16() { return &___m_AtlasTexture_16; }
	inline void set_m_AtlasTexture_16(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_AtlasTexture_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_AtlasTexture_16), value);
	}

	inline static int32_t get_offset_of_m_AtlasTextures_17() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_AtlasTextures_17)); }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* get_m_AtlasTextures_17() const { return ___m_AtlasTextures_17; }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9** get_address_of_m_AtlasTextures_17() { return &___m_AtlasTextures_17; }
	inline void set_m_AtlasTextures_17(Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* value)
	{
		___m_AtlasTextures_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_AtlasTextures_17), value);
	}

	inline static int32_t get_offset_of_m_AtlasTextureIndex_18() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_AtlasTextureIndex_18)); }
	inline int32_t get_m_AtlasTextureIndex_18() const { return ___m_AtlasTextureIndex_18; }
	inline int32_t* get_address_of_m_AtlasTextureIndex_18() { return &___m_AtlasTextureIndex_18; }
	inline void set_m_AtlasTextureIndex_18(int32_t value)
	{
		___m_AtlasTextureIndex_18 = value;
	}

	inline static int32_t get_offset_of_m_UsedGlyphRects_19() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_UsedGlyphRects_19)); }
	inline List_1_tD87292C3DA9A1BCF7BE7A6A63897ABF69A015D65 * get_m_UsedGlyphRects_19() const { return ___m_UsedGlyphRects_19; }
	inline List_1_tD87292C3DA9A1BCF7BE7A6A63897ABF69A015D65 ** get_address_of_m_UsedGlyphRects_19() { return &___m_UsedGlyphRects_19; }
	inline void set_m_UsedGlyphRects_19(List_1_tD87292C3DA9A1BCF7BE7A6A63897ABF69A015D65 * value)
	{
		___m_UsedGlyphRects_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_UsedGlyphRects_19), value);
	}

	inline static int32_t get_offset_of_m_FreeGlyphRects_20() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_FreeGlyphRects_20)); }
	inline List_1_tD87292C3DA9A1BCF7BE7A6A63897ABF69A015D65 * get_m_FreeGlyphRects_20() const { return ___m_FreeGlyphRects_20; }
	inline List_1_tD87292C3DA9A1BCF7BE7A6A63897ABF69A015D65 ** get_address_of_m_FreeGlyphRects_20() { return &___m_FreeGlyphRects_20; }
	inline void set_m_FreeGlyphRects_20(List_1_tD87292C3DA9A1BCF7BE7A6A63897ABF69A015D65 * value)
	{
		___m_FreeGlyphRects_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_FreeGlyphRects_20), value);
	}

	inline static int32_t get_offset_of_m_fontInfo_21() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_fontInfo_21)); }
	inline FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E * get_m_fontInfo_21() const { return ___m_fontInfo_21; }
	inline FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E ** get_address_of_m_fontInfo_21() { return &___m_fontInfo_21; }
	inline void set_m_fontInfo_21(FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E * value)
	{
		___m_fontInfo_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontInfo_21), value);
	}

	inline static int32_t get_offset_of_atlas_22() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___atlas_22)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_atlas_22() const { return ___atlas_22; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_atlas_22() { return &___atlas_22; }
	inline void set_atlas_22(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___atlas_22 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_22), value);
	}

	inline static int32_t get_offset_of_m_AtlasWidth_23() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_AtlasWidth_23)); }
	inline int32_t get_m_AtlasWidth_23() const { return ___m_AtlasWidth_23; }
	inline int32_t* get_address_of_m_AtlasWidth_23() { return &___m_AtlasWidth_23; }
	inline void set_m_AtlasWidth_23(int32_t value)
	{
		___m_AtlasWidth_23 = value;
	}

	inline static int32_t get_offset_of_m_AtlasHeight_24() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_AtlasHeight_24)); }
	inline int32_t get_m_AtlasHeight_24() const { return ___m_AtlasHeight_24; }
	inline int32_t* get_address_of_m_AtlasHeight_24() { return &___m_AtlasHeight_24; }
	inline void set_m_AtlasHeight_24(int32_t value)
	{
		___m_AtlasHeight_24 = value;
	}

	inline static int32_t get_offset_of_m_AtlasPadding_25() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_AtlasPadding_25)); }
	inline int32_t get_m_AtlasPadding_25() const { return ___m_AtlasPadding_25; }
	inline int32_t* get_address_of_m_AtlasPadding_25() { return &___m_AtlasPadding_25; }
	inline void set_m_AtlasPadding_25(int32_t value)
	{
		___m_AtlasPadding_25 = value;
	}

	inline static int32_t get_offset_of_m_AtlasRenderMode_26() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_AtlasRenderMode_26)); }
	inline int32_t get_m_AtlasRenderMode_26() const { return ___m_AtlasRenderMode_26; }
	inline int32_t* get_address_of_m_AtlasRenderMode_26() { return &___m_AtlasRenderMode_26; }
	inline void set_m_AtlasRenderMode_26(int32_t value)
	{
		___m_AtlasRenderMode_26 = value;
	}

	inline static int32_t get_offset_of_m_glyphInfoList_27() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_glyphInfoList_27)); }
	inline List_1_tB2A7609CA52574815578619F788242AB43EC2C82 * get_m_glyphInfoList_27() const { return ___m_glyphInfoList_27; }
	inline List_1_tB2A7609CA52574815578619F788242AB43EC2C82 ** get_address_of_m_glyphInfoList_27() { return &___m_glyphInfoList_27; }
	inline void set_m_glyphInfoList_27(List_1_tB2A7609CA52574815578619F788242AB43EC2C82 * value)
	{
		___m_glyphInfoList_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_glyphInfoList_27), value);
	}

	inline static int32_t get_offset_of_m_KerningTable_28() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_KerningTable_28)); }
	inline KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F * get_m_KerningTable_28() const { return ___m_KerningTable_28; }
	inline KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F ** get_address_of_m_KerningTable_28() { return &___m_KerningTable_28; }
	inline void set_m_KerningTable_28(KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F * value)
	{
		___m_KerningTable_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_KerningTable_28), value);
	}

	inline static int32_t get_offset_of_m_FontFeatureTable_29() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_FontFeatureTable_29)); }
	inline TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756 * get_m_FontFeatureTable_29() const { return ___m_FontFeatureTable_29; }
	inline TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756 ** get_address_of_m_FontFeatureTable_29() { return &___m_FontFeatureTable_29; }
	inline void set_m_FontFeatureTable_29(TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756 * value)
	{
		___m_FontFeatureTable_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontFeatureTable_29), value);
	}

	inline static int32_t get_offset_of_fallbackFontAssets_30() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___fallbackFontAssets_30)); }
	inline List_1_t746C622521747C7842E2C567F304B446EA74B8BB * get_fallbackFontAssets_30() const { return ___fallbackFontAssets_30; }
	inline List_1_t746C622521747C7842E2C567F304B446EA74B8BB ** get_address_of_fallbackFontAssets_30() { return &___fallbackFontAssets_30; }
	inline void set_fallbackFontAssets_30(List_1_t746C622521747C7842E2C567F304B446EA74B8BB * value)
	{
		___fallbackFontAssets_30 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackFontAssets_30), value);
	}

	inline static int32_t get_offset_of_m_FallbackFontAssetTable_31() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_FallbackFontAssetTable_31)); }
	inline List_1_t746C622521747C7842E2C567F304B446EA74B8BB * get_m_FallbackFontAssetTable_31() const { return ___m_FallbackFontAssetTable_31; }
	inline List_1_t746C622521747C7842E2C567F304B446EA74B8BB ** get_address_of_m_FallbackFontAssetTable_31() { return &___m_FallbackFontAssetTable_31; }
	inline void set_m_FallbackFontAssetTable_31(List_1_t746C622521747C7842E2C567F304B446EA74B8BB * value)
	{
		___m_FallbackFontAssetTable_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_FallbackFontAssetTable_31), value);
	}

	inline static int32_t get_offset_of_m_CreationSettings_32() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_CreationSettings_32)); }
	inline FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4  get_m_CreationSettings_32() const { return ___m_CreationSettings_32; }
	inline FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4 * get_address_of_m_CreationSettings_32() { return &___m_CreationSettings_32; }
	inline void set_m_CreationSettings_32(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4  value)
	{
		___m_CreationSettings_32 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightTable_33() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_FontWeightTable_33)); }
	inline TMP_FontWeightPairU5BU5D_tD4C8F5F8465CC6A30370C93F43B43BE3147DA68D* get_m_FontWeightTable_33() const { return ___m_FontWeightTable_33; }
	inline TMP_FontWeightPairU5BU5D_tD4C8F5F8465CC6A30370C93F43B43BE3147DA68D** get_address_of_m_FontWeightTable_33() { return &___m_FontWeightTable_33; }
	inline void set_m_FontWeightTable_33(TMP_FontWeightPairU5BU5D_tD4C8F5F8465CC6A30370C93F43B43BE3147DA68D* value)
	{
		___m_FontWeightTable_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontWeightTable_33), value);
	}

	inline static int32_t get_offset_of_fontWeights_34() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___fontWeights_34)); }
	inline TMP_FontWeightPairU5BU5D_tD4C8F5F8465CC6A30370C93F43B43BE3147DA68D* get_fontWeights_34() const { return ___fontWeights_34; }
	inline TMP_FontWeightPairU5BU5D_tD4C8F5F8465CC6A30370C93F43B43BE3147DA68D** get_address_of_fontWeights_34() { return &___fontWeights_34; }
	inline void set_fontWeights_34(TMP_FontWeightPairU5BU5D_tD4C8F5F8465CC6A30370C93F43B43BE3147DA68D* value)
	{
		___fontWeights_34 = value;
		Il2CppCodeGenWriteBarrier((&___fontWeights_34), value);
	}

	inline static int32_t get_offset_of_normalStyle_35() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___normalStyle_35)); }
	inline float get_normalStyle_35() const { return ___normalStyle_35; }
	inline float* get_address_of_normalStyle_35() { return &___normalStyle_35; }
	inline void set_normalStyle_35(float value)
	{
		___normalStyle_35 = value;
	}

	inline static int32_t get_offset_of_normalSpacingOffset_36() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___normalSpacingOffset_36)); }
	inline float get_normalSpacingOffset_36() const { return ___normalSpacingOffset_36; }
	inline float* get_address_of_normalSpacingOffset_36() { return &___normalSpacingOffset_36; }
	inline void set_normalSpacingOffset_36(float value)
	{
		___normalSpacingOffset_36 = value;
	}

	inline static int32_t get_offset_of_boldStyle_37() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___boldStyle_37)); }
	inline float get_boldStyle_37() const { return ___boldStyle_37; }
	inline float* get_address_of_boldStyle_37() { return &___boldStyle_37; }
	inline void set_boldStyle_37(float value)
	{
		___boldStyle_37 = value;
	}

	inline static int32_t get_offset_of_boldSpacing_38() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___boldSpacing_38)); }
	inline float get_boldSpacing_38() const { return ___boldSpacing_38; }
	inline float* get_address_of_boldSpacing_38() { return &___boldSpacing_38; }
	inline void set_boldSpacing_38(float value)
	{
		___boldSpacing_38 = value;
	}

	inline static int32_t get_offset_of_italicStyle_39() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___italicStyle_39)); }
	inline uint8_t get_italicStyle_39() const { return ___italicStyle_39; }
	inline uint8_t* get_address_of_italicStyle_39() { return &___italicStyle_39; }
	inline void set_italicStyle_39(uint8_t value)
	{
		___italicStyle_39 = value;
	}

	inline static int32_t get_offset_of_tabSize_40() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___tabSize_40)); }
	inline uint8_t get_tabSize_40() const { return ___tabSize_40; }
	inline uint8_t* get_address_of_tabSize_40() { return &___tabSize_40; }
	inline void set_tabSize_40(uint8_t value)
	{
		___tabSize_40 = value;
	}

	inline static int32_t get_offset_of_m_oldTabSize_41() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_oldTabSize_41)); }
	inline uint8_t get_m_oldTabSize_41() const { return ___m_oldTabSize_41; }
	inline uint8_t* get_address_of_m_oldTabSize_41() { return &___m_oldTabSize_41; }
	inline void set_m_oldTabSize_41(uint8_t value)
	{
		___m_oldTabSize_41 = value;
	}

	inline static int32_t get_offset_of_m_IsFontAssetLookupTablesDirty_42() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_IsFontAssetLookupTablesDirty_42)); }
	inline bool get_m_IsFontAssetLookupTablesDirty_42() const { return ___m_IsFontAssetLookupTablesDirty_42; }
	inline bool* get_address_of_m_IsFontAssetLookupTablesDirty_42() { return &___m_IsFontAssetLookupTablesDirty_42; }
	inline void set_m_IsFontAssetLookupTablesDirty_42(bool value)
	{
		___m_IsFontAssetLookupTablesDirty_42 = value;
	}

	inline static int32_t get_offset_of_m_GlyphsToPack_43() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_GlyphsToPack_43)); }
	inline List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * get_m_GlyphsToPack_43() const { return ___m_GlyphsToPack_43; }
	inline List_1_t8B3AA8D740B2E10383225037638055610A7BE123 ** get_address_of_m_GlyphsToPack_43() { return &___m_GlyphsToPack_43; }
	inline void set_m_GlyphsToPack_43(List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * value)
	{
		___m_GlyphsToPack_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlyphsToPack_43), value);
	}

	inline static int32_t get_offset_of_m_GlyphsPacked_44() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_GlyphsPacked_44)); }
	inline List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * get_m_GlyphsPacked_44() const { return ___m_GlyphsPacked_44; }
	inline List_1_t8B3AA8D740B2E10383225037638055610A7BE123 ** get_address_of_m_GlyphsPacked_44() { return &___m_GlyphsPacked_44; }
	inline void set_m_GlyphsPacked_44(List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * value)
	{
		___m_GlyphsPacked_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlyphsPacked_44), value);
	}

	inline static int32_t get_offset_of_m_GlyphsToRender_45() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_GlyphsToRender_45)); }
	inline List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * get_m_GlyphsToRender_45() const { return ___m_GlyphsToRender_45; }
	inline List_1_t8B3AA8D740B2E10383225037638055610A7BE123 ** get_address_of_m_GlyphsToRender_45() { return &___m_GlyphsToRender_45; }
	inline void set_m_GlyphsToRender_45(List_1_t8B3AA8D740B2E10383225037638055610A7BE123 * value)
	{
		___m_GlyphsToRender_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlyphsToRender_45), value);
	}

	inline static int32_t get_offset_of_m_GlyphIndexList_46() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_GlyphIndexList_46)); }
	inline List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E * get_m_GlyphIndexList_46() const { return ___m_GlyphIndexList_46; }
	inline List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E ** get_address_of_m_GlyphIndexList_46() { return &___m_GlyphIndexList_46; }
	inline void set_m_GlyphIndexList_46(List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E * value)
	{
		___m_GlyphIndexList_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlyphIndexList_46), value);
	}

	inline static int32_t get_offset_of_m_CharactersToAdd_47() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C, ___m_CharactersToAdd_47)); }
	inline List_1_tA95ABBEB9024057D7EEFADA755FABA1E3150CBED * get_m_CharactersToAdd_47() const { return ___m_CharactersToAdd_47; }
	inline List_1_tA95ABBEB9024057D7EEFADA755FABA1E3150CBED ** get_address_of_m_CharactersToAdd_47() { return &___m_CharactersToAdd_47; }
	inline void set_m_CharactersToAdd_47(List_1_tA95ABBEB9024057D7EEFADA755FABA1E3150CBED * value)
	{
		___m_CharactersToAdd_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_CharactersToAdd_47), value);
	}
};

struct TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C_StaticFields
{
public:
	// System.UInt32[] TMPro.TMP_FontAsset::s_GlyphIndexArray
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___s_GlyphIndexArray_48;
	// System.Collections.Generic.List`1<System.UInt32> TMPro.TMP_FontAsset::s_MissingCharacterList
	List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E * ___s_MissingCharacterList_49;

public:
	inline static int32_t get_offset_of_s_GlyphIndexArray_48() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C_StaticFields, ___s_GlyphIndexArray_48)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_s_GlyphIndexArray_48() const { return ___s_GlyphIndexArray_48; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_s_GlyphIndexArray_48() { return &___s_GlyphIndexArray_48; }
	inline void set_s_GlyphIndexArray_48(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___s_GlyphIndexArray_48 = value;
		Il2CppCodeGenWriteBarrier((&___s_GlyphIndexArray_48), value);
	}

	inline static int32_t get_offset_of_s_MissingCharacterList_49() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C_StaticFields, ___s_MissingCharacterList_49)); }
	inline List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E * get_s_MissingCharacterList_49() const { return ___s_MissingCharacterList_49; }
	inline List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E ** get_address_of_s_MissingCharacterList_49() { return &___s_MissingCharacterList_49; }
	inline void set_s_MissingCharacterList_49(List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E * value)
	{
		___s_MissingCharacterList_49 = value;
		Il2CppCodeGenWriteBarrier((&___s_MissingCharacterList_49), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTASSET_T44D2006105B39FB33AE5A0ADF07A7EF36C72385C_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ASSETUNLOADER_TCB8D5D17907E58E4E5A84E1AF9AB169FAF711BF7_H
#define ASSETUNLOADER_TCB8D5D17907E58E4E5A84E1AF9AB169FAF711BF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.AssetUnloader
struct  AssetUnloader_tCB8D5D17907E58E4E5A84E1AF9AB169FAF711BF7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETUNLOADER_TCB8D5D17907E58E4E5A84E1AF9AB169FAF711BF7_H
#ifndef DISPATCHER_TB781C2F44E71A5E1249F3232459FDED4D76B39D7_H
#define DISPATCHER_TB781C2F44E71A5E1249F3232459FDED4D76B39D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLib.Dispatcher
struct  Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields
{
public:
	// TriLib.Dispatcher TriLib.Dispatcher::a
	Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * ___a_4;
	// System.Boolean TriLib.Dispatcher::b
	bool ___b_5;
	// System.Object TriLib.Dispatcher::c
	RuntimeObject * ___c_6;
	// System.Collections.Generic.Queue`1<System.Action> TriLib.Dispatcher::d
	Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * ___d_7;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields, ___a_4)); }
	inline Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * get_a_4() const { return ___a_4; }
	inline Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields, ___b_5)); }
	inline bool get_b_5() const { return ___b_5; }
	inline bool* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(bool value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields, ___c_6)); }
	inline RuntimeObject * get_c_6() const { return ___c_6; }
	inline RuntimeObject ** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(RuntimeObject * value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((&___c_6), value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields, ___d_7)); }
	inline Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * get_d_7() const { return ___d_7; }
	inline Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D ** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(Queue_1_tAAB5A24FE87B601E444ABA5CDF24F833AC5C651D * value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((&___d_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHER_TB781C2F44E71A5E1249F3232459FDED4D76B39D7_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef TEXTCONTAINER_TF5E5EB56D152102B19C27607F84847CA594391DE_H
#define TEXTCONTAINER_TF5E5EB56D152102B19C27607F84847CA594391DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextContainer
struct  TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Boolean TMPro.TextContainer::m_hasChanged
	bool ___m_hasChanged_4;
	// UnityEngine.Vector2 TMPro.TextContainer::m_pivot
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_pivot_5;
	// TMPro.TextContainerAnchors TMPro.TextContainer::m_anchorPosition
	int32_t ___m_anchorPosition_6;
	// UnityEngine.Rect TMPro.TextContainer::m_rect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_rect_7;
	// System.Boolean TMPro.TextContainer::m_isDefaultWidth
	bool ___m_isDefaultWidth_8;
	// System.Boolean TMPro.TextContainer::m_isDefaultHeight
	bool ___m_isDefaultHeight_9;
	// System.Boolean TMPro.TextContainer::m_isAutoFitting
	bool ___m_isAutoFitting_10;
	// UnityEngine.Vector3[] TMPro.TextContainer::m_corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_corners_11;
	// UnityEngine.Vector3[] TMPro.TextContainer::m_worldCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_worldCorners_12;
	// UnityEngine.Vector4 TMPro.TextContainer::m_margins
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_margins_13;
	// UnityEngine.RectTransform TMPro.TextContainer::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_14;
	// TMPro.TextMeshPro TMPro.TextContainer::m_textMeshPro
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * ___m_textMeshPro_16;

public:
	inline static int32_t get_offset_of_m_hasChanged_4() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_hasChanged_4)); }
	inline bool get_m_hasChanged_4() const { return ___m_hasChanged_4; }
	inline bool* get_address_of_m_hasChanged_4() { return &___m_hasChanged_4; }
	inline void set_m_hasChanged_4(bool value)
	{
		___m_hasChanged_4 = value;
	}

	inline static int32_t get_offset_of_m_pivot_5() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_pivot_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_pivot_5() const { return ___m_pivot_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_pivot_5() { return &___m_pivot_5; }
	inline void set_m_pivot_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_pivot_5 = value;
	}

	inline static int32_t get_offset_of_m_anchorPosition_6() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_anchorPosition_6)); }
	inline int32_t get_m_anchorPosition_6() const { return ___m_anchorPosition_6; }
	inline int32_t* get_address_of_m_anchorPosition_6() { return &___m_anchorPosition_6; }
	inline void set_m_anchorPosition_6(int32_t value)
	{
		___m_anchorPosition_6 = value;
	}

	inline static int32_t get_offset_of_m_rect_7() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_rect_7)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_rect_7() const { return ___m_rect_7; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_rect_7() { return &___m_rect_7; }
	inline void set_m_rect_7(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_rect_7 = value;
	}

	inline static int32_t get_offset_of_m_isDefaultWidth_8() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_isDefaultWidth_8)); }
	inline bool get_m_isDefaultWidth_8() const { return ___m_isDefaultWidth_8; }
	inline bool* get_address_of_m_isDefaultWidth_8() { return &___m_isDefaultWidth_8; }
	inline void set_m_isDefaultWidth_8(bool value)
	{
		___m_isDefaultWidth_8 = value;
	}

	inline static int32_t get_offset_of_m_isDefaultHeight_9() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_isDefaultHeight_9)); }
	inline bool get_m_isDefaultHeight_9() const { return ___m_isDefaultHeight_9; }
	inline bool* get_address_of_m_isDefaultHeight_9() { return &___m_isDefaultHeight_9; }
	inline void set_m_isDefaultHeight_9(bool value)
	{
		___m_isDefaultHeight_9 = value;
	}

	inline static int32_t get_offset_of_m_isAutoFitting_10() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_isAutoFitting_10)); }
	inline bool get_m_isAutoFitting_10() const { return ___m_isAutoFitting_10; }
	inline bool* get_address_of_m_isAutoFitting_10() { return &___m_isAutoFitting_10; }
	inline void set_m_isAutoFitting_10(bool value)
	{
		___m_isAutoFitting_10 = value;
	}

	inline static int32_t get_offset_of_m_corners_11() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_corners_11)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_corners_11() const { return ___m_corners_11; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_corners_11() { return &___m_corners_11; }
	inline void set_m_corners_11(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_corners_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_corners_11), value);
	}

	inline static int32_t get_offset_of_m_worldCorners_12() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_worldCorners_12)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_worldCorners_12() const { return ___m_worldCorners_12; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_worldCorners_12() { return &___m_worldCorners_12; }
	inline void set_m_worldCorners_12(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_worldCorners_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_worldCorners_12), value);
	}

	inline static int32_t get_offset_of_m_margins_13() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_margins_13)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_margins_13() const { return ___m_margins_13; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_margins_13() { return &___m_margins_13; }
	inline void set_m_margins_13(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_margins_13 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_14() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_rectTransform_14)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_14() const { return ___m_rectTransform_14; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_14() { return &___m_rectTransform_14; }
	inline void set_m_rectTransform_14(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_14), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_16() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_textMeshPro_16)); }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * get_m_textMeshPro_16() const { return ___m_textMeshPro_16; }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 ** get_address_of_m_textMeshPro_16() { return &___m_textMeshPro_16; }
	inline void set_m_textMeshPro_16(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * value)
	{
		___m_textMeshPro_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_16), value);
	}
};

struct TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TextContainer::k_defaultSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_defaultSize_15;

public:
	inline static int32_t get_offset_of_k_defaultSize_15() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE_StaticFields, ___k_defaultSize_15)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_defaultSize_15() const { return ___k_defaultSize_15; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_defaultSize_15() { return &___k_defaultSize_15; }
	inline void set_k_defaultSize_15(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_defaultSize_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONTAINER_TF5E5EB56D152102B19C27607F84847CA594391DE_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#define TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_30;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_31;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_fontAsset_32;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_currentFontAsset_33;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_34;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_sharedMaterial_35;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_currentMaterial_36;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___m_materialReferences_37;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F * ___m_materialReferenceIndexLookup_38;
	// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___m_materialReferenceStack_39;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___m_fontSharedMaterials_41;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_fontMaterial_42;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___m_fontMaterials_43;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_fontColor32_45;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_fontColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_underlineColor_48;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_strikethroughColor_49;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_highlightColor_50;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_highlightPadding
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_highlightPadding_51;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_52;
	// TMPro.ColorMode TMPro.TMP_Text::m_colorMode
	int32_t ___m_colorMode_53;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  ___m_fontColorGradient_54;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_fontColorGradientPreset_55;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_spriteAsset_56;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_57;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_58;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_spriteColor_59;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_60;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_faceColor_61;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_outlineColor_62;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_63;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_64;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_65;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_66;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___m_sizeStack_67;
	// TMPro.FontWeight TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_68;
	// TMPro.FontWeight TMPro.TMP_Text::m_FontWeightInternal
	int32_t ___m_FontWeightInternal_69;
	// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight> TMPro.TMP_Text::m_FontWeightStack
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___m_FontWeightStack_70;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_71;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_72;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_73;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_74;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_75;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_76;
	// TMPro.FontStyles TMPro.TMP_Text::m_FontStyleInternal
	int32_t ___m_FontStyleInternal_77;
	// TMPro.TMP_FontStyleStack TMPro.TMP_Text::m_fontStyleStack
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___m_fontStyleStack_78;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_79;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_80;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_81;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___m_lineJustificationStack_82;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_textContainerLocalCorners_83;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_84;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_85;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_86;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_87;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_88;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_89;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_90;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_91;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_92;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_93;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_94;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_95;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_96;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_97;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_98;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_99;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_100;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_101;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_linkedTextComponent_102;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_103;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_104;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_105;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_106;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_107;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_108;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_109;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_110;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_111;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_112;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_113;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_114;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_115;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_116;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_117;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_118;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_119;
	// System.Boolean TMPro.TMP_Text::m_VertexBufferAutoSizeReduction
	bool ___m_VertexBufferAutoSizeReduction_120;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_121;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_122;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_123;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_124;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_125;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_126;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_127;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_margin_128;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_129;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_130;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_131;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_132;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_133;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___m_textInfo_134;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_135;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_136;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_transform_137;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_138;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_139;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_140;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_141;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_142;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * ___m_spriteAnimator_143;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_144;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_145;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_146;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_147;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_148;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_149;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___m_LayoutElement_150;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_151;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_152;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_153;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_154;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_155;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_156;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_157;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_158;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_159;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_160;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_161;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_162;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_163;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_164;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_165;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_166;
	// TMPro.TMP_Text_TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_167;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_168;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_169;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_170;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_htmlTag_171;
	// TMPro.RichTextTagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* ___m_xmlAttribute_172;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_attributeParameterValues_173;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_174;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_175;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___m_indentStack_176;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_177;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_178;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_FXMatrix_179;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_180;
	// TMPro.TMP_Text_UnicodeChar[] TMPro.TMP_Text::m_TextParsingBuffer
	UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* ___m_TextParsingBuffer_181;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* ___m_internalCharacterInfo_182;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_input_CharArray_183;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_184;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_185;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  ___m_SavedWordWrapState_186;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  ___m_SavedLineState_187;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_188;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_189;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_190;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_191;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_192;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_193;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_194;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_195;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_196;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_197;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_198;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_199;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_200;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_201;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_202;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___m_meshExtents_203;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_htmlColor_204;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_colorStack_205;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_underlineColorStack_206;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_strikethroughColorStack_207;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_highlightColorStack_208;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_colorGradientPreset_209;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___m_colorGradientStack_210;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_211;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_212;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___m_styleStack_213;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___m_actionStack_214;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_215;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_216;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___m_baselineOffsetStack_217;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_218;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_219;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___m_cached_TextElement_220;
	// TMPro.TMP_Character TMPro.TMP_Text::m_cached_Underline_Character
	TMP_Character_t1875AACA978396521498D6A699052C187903553D * ___m_cached_Underline_Character_221;
	// TMPro.TMP_Character TMPro.TMP_Text::m_cached_Ellipsis_Character
	TMP_Character_t1875AACA978396521498D6A699052C187903553D * ___m_cached_Ellipsis_Character_222;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_defaultSpriteAsset_223;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_currentSpriteAsset_224;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_225;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_226;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_227;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_228;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___k_Power_229;

public:
	inline static int32_t get_offset_of_m_text_30() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_text_30)); }
	inline String_t* get_m_text_30() const { return ___m_text_30; }
	inline String_t** get_address_of_m_text_30() { return &___m_text_30; }
	inline void set_m_text_30(String_t* value)
	{
		___m_text_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_30), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_31() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isRightToLeft_31)); }
	inline bool get_m_isRightToLeft_31() const { return ___m_isRightToLeft_31; }
	inline bool* get_address_of_m_isRightToLeft_31() { return &___m_isRightToLeft_31; }
	inline void set_m_isRightToLeft_31(bool value)
	{
		___m_isRightToLeft_31 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_32() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontAsset_32)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_fontAsset_32() const { return ___m_fontAsset_32; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_fontAsset_32() { return &___m_fontAsset_32; }
	inline void set_m_fontAsset_32(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_fontAsset_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_32), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_33() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentFontAsset_33)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_currentFontAsset_33() const { return ___m_currentFontAsset_33; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_currentFontAsset_33() { return &___m_currentFontAsset_33; }
	inline void set_m_currentFontAsset_33(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_currentFontAsset_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_33), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_34() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isSDFShader_34)); }
	inline bool get_m_isSDFShader_34() const { return ___m_isSDFShader_34; }
	inline bool* get_address_of_m_isSDFShader_34() { return &___m_isSDFShader_34; }
	inline void set_m_isSDFShader_34(bool value)
	{
		___m_isSDFShader_34 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_35() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_sharedMaterial_35)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_sharedMaterial_35() const { return ___m_sharedMaterial_35; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_sharedMaterial_35() { return &___m_sharedMaterial_35; }
	inline void set_m_sharedMaterial_35(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_sharedMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_35), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_36() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentMaterial_36)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_currentMaterial_36() const { return ___m_currentMaterial_36; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_currentMaterial_36() { return &___m_currentMaterial_36; }
	inline void set_m_currentMaterial_36(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_currentMaterial_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_37() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferences_37)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_m_materialReferences_37() const { return ___m_materialReferences_37; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_m_materialReferences_37() { return &___m_materialReferences_37; }
	inline void set_m_materialReferences_37(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___m_materialReferences_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_37), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_38() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferenceIndexLookup_38)); }
	inline Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F * get_m_materialReferenceIndexLookup_38() const { return ___m_materialReferenceIndexLookup_38; }
	inline Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F ** get_address_of_m_materialReferenceIndexLookup_38() { return &___m_materialReferenceIndexLookup_38; }
	inline void set_m_materialReferenceIndexLookup_38(Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F * value)
	{
		___m_materialReferenceIndexLookup_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_38), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_39() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferenceStack_39)); }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  get_m_materialReferenceStack_39() const { return ___m_materialReferenceStack_39; }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 * get_address_of_m_materialReferenceStack_39() { return &___m_materialReferenceStack_39; }
	inline void set_m_materialReferenceStack_39(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  value)
	{
		___m_materialReferenceStack_39 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_40() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentMaterialIndex_40)); }
	inline int32_t get_m_currentMaterialIndex_40() const { return ___m_currentMaterialIndex_40; }
	inline int32_t* get_address_of_m_currentMaterialIndex_40() { return &___m_currentMaterialIndex_40; }
	inline void set_m_currentMaterialIndex_40(int32_t value)
	{
		___m_currentMaterialIndex_40 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSharedMaterials_41)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_m_fontSharedMaterials_41() const { return ___m_fontSharedMaterials_41; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_m_fontSharedMaterials_41() { return &___m_fontSharedMaterials_41; }
	inline void set_m_fontSharedMaterials_41(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___m_fontSharedMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_42() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontMaterial_42)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_fontMaterial_42() const { return ___m_fontMaterial_42; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_fontMaterial_42() { return &___m_fontMaterial_42; }
	inline void set_m_fontMaterial_42(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_fontMaterial_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_42), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_43() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontMaterials_43)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_m_fontMaterials_43() const { return ___m_fontMaterials_43; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_m_fontMaterials_43() { return &___m_fontMaterials_43; }
	inline void set_m_fontMaterials_43(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___m_fontMaterials_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_43), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_44() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isMaterialDirty_44)); }
	inline bool get_m_isMaterialDirty_44() const { return ___m_isMaterialDirty_44; }
	inline bool* get_address_of_m_isMaterialDirty_44() { return &___m_isMaterialDirty_44; }
	inline void set_m_isMaterialDirty_44(bool value)
	{
		___m_isMaterialDirty_44 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_45() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColor32_45)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_fontColor32_45() const { return ___m_fontColor32_45; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_fontColor32_45() { return &___m_fontColor32_45; }
	inline void set_m_fontColor32_45(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_fontColor32_45 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColor_46)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_fontColor_46() const { return ___m_fontColor_46; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_fontColor_46() { return &___m_fontColor_46; }
	inline void set_m_fontColor_46(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_fontColor_46 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_underlineColor_48)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_underlineColor_48() const { return ___m_underlineColor_48; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_underlineColor_48() { return &___m_underlineColor_48; }
	inline void set_m_underlineColor_48(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_underlineColor_48 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_49() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_strikethroughColor_49)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_strikethroughColor_49() const { return ___m_strikethroughColor_49; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_strikethroughColor_49() { return &___m_strikethroughColor_49; }
	inline void set_m_strikethroughColor_49(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_strikethroughColor_49 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_50() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightColor_50)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_highlightColor_50() const { return ___m_highlightColor_50; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_highlightColor_50() { return &___m_highlightColor_50; }
	inline void set_m_highlightColor_50(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_highlightColor_50 = value;
	}

	inline static int32_t get_offset_of_m_highlightPadding_51() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightPadding_51)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_highlightPadding_51() const { return ___m_highlightPadding_51; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_highlightPadding_51() { return &___m_highlightPadding_51; }
	inline void set_m_highlightPadding_51(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_highlightPadding_51 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_52() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableVertexGradient_52)); }
	inline bool get_m_enableVertexGradient_52() const { return ___m_enableVertexGradient_52; }
	inline bool* get_address_of_m_enableVertexGradient_52() { return &___m_enableVertexGradient_52; }
	inline void set_m_enableVertexGradient_52(bool value)
	{
		___m_enableVertexGradient_52 = value;
	}

	inline static int32_t get_offset_of_m_colorMode_53() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorMode_53)); }
	inline int32_t get_m_colorMode_53() const { return ___m_colorMode_53; }
	inline int32_t* get_address_of_m_colorMode_53() { return &___m_colorMode_53; }
	inline void set_m_colorMode_53(int32_t value)
	{
		___m_colorMode_53 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_54() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColorGradient_54)); }
	inline VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  get_m_fontColorGradient_54() const { return ___m_fontColorGradient_54; }
	inline VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A * get_address_of_m_fontColorGradient_54() { return &___m_fontColorGradient_54; }
	inline void set_m_fontColorGradient_54(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  value)
	{
		___m_fontColorGradient_54 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_55() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColorGradientPreset_55)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_fontColorGradientPreset_55() const { return ___m_fontColorGradientPreset_55; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_fontColorGradientPreset_55() { return &___m_fontColorGradientPreset_55; }
	inline void set_m_fontColorGradientPreset_55(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_fontColorGradientPreset_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_55), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_56() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAsset_56)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_spriteAsset_56() const { return ___m_spriteAsset_56; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_spriteAsset_56() { return &___m_spriteAsset_56; }
	inline void set_m_spriteAsset_56(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_spriteAsset_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_56), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_57() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tintAllSprites_57)); }
	inline bool get_m_tintAllSprites_57() const { return ___m_tintAllSprites_57; }
	inline bool* get_address_of_m_tintAllSprites_57() { return &___m_tintAllSprites_57; }
	inline void set_m_tintAllSprites_57(bool value)
	{
		___m_tintAllSprites_57 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_58() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tintSprite_58)); }
	inline bool get_m_tintSprite_58() const { return ___m_tintSprite_58; }
	inline bool* get_address_of_m_tintSprite_58() { return &___m_tintSprite_58; }
	inline void set_m_tintSprite_58(bool value)
	{
		___m_tintSprite_58 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_59() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteColor_59)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_spriteColor_59() const { return ___m_spriteColor_59; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_spriteColor_59() { return &___m_spriteColor_59; }
	inline void set_m_spriteColor_59(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_spriteColor_59 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_60() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_overrideHtmlColors_60)); }
	inline bool get_m_overrideHtmlColors_60() const { return ___m_overrideHtmlColors_60; }
	inline bool* get_address_of_m_overrideHtmlColors_60() { return &___m_overrideHtmlColors_60; }
	inline void set_m_overrideHtmlColors_60(bool value)
	{
		___m_overrideHtmlColors_60 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_61() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_faceColor_61)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_faceColor_61() const { return ___m_faceColor_61; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_faceColor_61() { return &___m_faceColor_61; }
	inline void set_m_faceColor_61(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_faceColor_61 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_62() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_outlineColor_62)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_outlineColor_62() const { return ___m_outlineColor_62; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_outlineColor_62() { return &___m_outlineColor_62; }
	inline void set_m_outlineColor_62(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_outlineColor_62 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_63() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_outlineWidth_63)); }
	inline float get_m_outlineWidth_63() const { return ___m_outlineWidth_63; }
	inline float* get_address_of_m_outlineWidth_63() { return &___m_outlineWidth_63; }
	inline void set_m_outlineWidth_63(float value)
	{
		___m_outlineWidth_63 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_64() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSize_64)); }
	inline float get_m_fontSize_64() const { return ___m_fontSize_64; }
	inline float* get_address_of_m_fontSize_64() { return &___m_fontSize_64; }
	inline void set_m_fontSize_64(float value)
	{
		___m_fontSize_64 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_65() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentFontSize_65)); }
	inline float get_m_currentFontSize_65() const { return ___m_currentFontSize_65; }
	inline float* get_address_of_m_currentFontSize_65() { return &___m_currentFontSize_65; }
	inline void set_m_currentFontSize_65(float value)
	{
		___m_currentFontSize_65 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_66() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeBase_66)); }
	inline float get_m_fontSizeBase_66() const { return ___m_fontSizeBase_66; }
	inline float* get_address_of_m_fontSizeBase_66() { return &___m_fontSizeBase_66; }
	inline void set_m_fontSizeBase_66(float value)
	{
		___m_fontSizeBase_66 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_67() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_sizeStack_67)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_m_sizeStack_67() const { return ___m_sizeStack_67; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_m_sizeStack_67() { return &___m_sizeStack_67; }
	inline void set_m_sizeStack_67(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___m_sizeStack_67 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_68() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontWeight_68)); }
	inline int32_t get_m_fontWeight_68() const { return ___m_fontWeight_68; }
	inline int32_t* get_address_of_m_fontWeight_68() { return &___m_fontWeight_68; }
	inline void set_m_fontWeight_68(int32_t value)
	{
		___m_fontWeight_68 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightInternal_69() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontWeightInternal_69)); }
	inline int32_t get_m_FontWeightInternal_69() const { return ___m_FontWeightInternal_69; }
	inline int32_t* get_address_of_m_FontWeightInternal_69() { return &___m_FontWeightInternal_69; }
	inline void set_m_FontWeightInternal_69(int32_t value)
	{
		___m_FontWeightInternal_69 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightStack_70() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontWeightStack_70)); }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  get_m_FontWeightStack_70() const { return ___m_FontWeightStack_70; }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B * get_address_of_m_FontWeightStack_70() { return &___m_FontWeightStack_70; }
	inline void set_m_FontWeightStack_70(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  value)
	{
		___m_FontWeightStack_70 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_71() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableAutoSizing_71)); }
	inline bool get_m_enableAutoSizing_71() const { return ___m_enableAutoSizing_71; }
	inline bool* get_address_of_m_enableAutoSizing_71() { return &___m_enableAutoSizing_71; }
	inline void set_m_enableAutoSizing_71(bool value)
	{
		___m_enableAutoSizing_71 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_72() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxFontSize_72)); }
	inline float get_m_maxFontSize_72() const { return ___m_maxFontSize_72; }
	inline float* get_address_of_m_maxFontSize_72() { return &___m_maxFontSize_72; }
	inline void set_m_maxFontSize_72(float value)
	{
		___m_maxFontSize_72 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_73() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minFontSize_73)); }
	inline float get_m_minFontSize_73() const { return ___m_minFontSize_73; }
	inline float* get_address_of_m_minFontSize_73() { return &___m_minFontSize_73; }
	inline void set_m_minFontSize_73(float value)
	{
		___m_minFontSize_73 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_74() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeMin_74)); }
	inline float get_m_fontSizeMin_74() const { return ___m_fontSizeMin_74; }
	inline float* get_address_of_m_fontSizeMin_74() { return &___m_fontSizeMin_74; }
	inline void set_m_fontSizeMin_74(float value)
	{
		___m_fontSizeMin_74 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_75() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeMax_75)); }
	inline float get_m_fontSizeMax_75() const { return ___m_fontSizeMax_75; }
	inline float* get_address_of_m_fontSizeMax_75() { return &___m_fontSizeMax_75; }
	inline void set_m_fontSizeMax_75(float value)
	{
		___m_fontSizeMax_75 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_76() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontStyle_76)); }
	inline int32_t get_m_fontStyle_76() const { return ___m_fontStyle_76; }
	inline int32_t* get_address_of_m_fontStyle_76() { return &___m_fontStyle_76; }
	inline void set_m_fontStyle_76(int32_t value)
	{
		___m_fontStyle_76 = value;
	}

	inline static int32_t get_offset_of_m_FontStyleInternal_77() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontStyleInternal_77)); }
	inline int32_t get_m_FontStyleInternal_77() const { return ___m_FontStyleInternal_77; }
	inline int32_t* get_address_of_m_FontStyleInternal_77() { return &___m_FontStyleInternal_77; }
	inline void set_m_FontStyleInternal_77(int32_t value)
	{
		___m_FontStyleInternal_77 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_78() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontStyleStack_78)); }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  get_m_fontStyleStack_78() const { return ___m_fontStyleStack_78; }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 * get_address_of_m_fontStyleStack_78() { return &___m_fontStyleStack_78; }
	inline void set_m_fontStyleStack_78(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  value)
	{
		___m_fontStyleStack_78 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_79() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isUsingBold_79)); }
	inline bool get_m_isUsingBold_79() const { return ___m_isUsingBold_79; }
	inline bool* get_address_of_m_isUsingBold_79() { return &___m_isUsingBold_79; }
	inline void set_m_isUsingBold_79(bool value)
	{
		___m_isUsingBold_79 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_80() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textAlignment_80)); }
	inline int32_t get_m_textAlignment_80() const { return ___m_textAlignment_80; }
	inline int32_t* get_address_of_m_textAlignment_80() { return &___m_textAlignment_80; }
	inline void set_m_textAlignment_80(int32_t value)
	{
		___m_textAlignment_80 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_81() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineJustification_81)); }
	inline int32_t get_m_lineJustification_81() const { return ___m_lineJustification_81; }
	inline int32_t* get_address_of_m_lineJustification_81() { return &___m_lineJustification_81; }
	inline void set_m_lineJustification_81(int32_t value)
	{
		___m_lineJustification_81 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_82() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineJustificationStack_82)); }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  get_m_lineJustificationStack_82() const { return ___m_lineJustificationStack_82; }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 * get_address_of_m_lineJustificationStack_82() { return &___m_lineJustificationStack_82; }
	inline void set_m_lineJustificationStack_82(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  value)
	{
		___m_lineJustificationStack_82 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_83() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textContainerLocalCorners_83)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_textContainerLocalCorners_83() const { return ___m_textContainerLocalCorners_83; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_textContainerLocalCorners_83() { return &___m_textContainerLocalCorners_83; }
	inline void set_m_textContainerLocalCorners_83(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_textContainerLocalCorners_83 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_83), value);
	}

	inline static int32_t get_offset_of_m_characterSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_characterSpacing_84)); }
	inline float get_m_characterSpacing_84() const { return ___m_characterSpacing_84; }
	inline float* get_address_of_m_characterSpacing_84() { return &___m_characterSpacing_84; }
	inline void set_m_characterSpacing_84(float value)
	{
		___m_characterSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cSpacing_85)); }
	inline float get_m_cSpacing_85() const { return ___m_cSpacing_85; }
	inline float* get_address_of_m_cSpacing_85() { return &___m_cSpacing_85; }
	inline void set_m_cSpacing_85(float value)
	{
		___m_cSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_86() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_monoSpacing_86)); }
	inline float get_m_monoSpacing_86() const { return ___m_monoSpacing_86; }
	inline float* get_address_of_m_monoSpacing_86() { return &___m_monoSpacing_86; }
	inline void set_m_monoSpacing_86(float value)
	{
		___m_monoSpacing_86 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_87() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_wordSpacing_87)); }
	inline float get_m_wordSpacing_87() const { return ___m_wordSpacing_87; }
	inline float* get_address_of_m_wordSpacing_87() { return &___m_wordSpacing_87; }
	inline void set_m_wordSpacing_87(float value)
	{
		___m_wordSpacing_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_88() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacing_88)); }
	inline float get_m_lineSpacing_88() const { return ___m_lineSpacing_88; }
	inline float* get_address_of_m_lineSpacing_88() { return &___m_lineSpacing_88; }
	inline void set_m_lineSpacing_88(float value)
	{
		___m_lineSpacing_88 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_89() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacingDelta_89)); }
	inline float get_m_lineSpacingDelta_89() const { return ___m_lineSpacingDelta_89; }
	inline float* get_address_of_m_lineSpacingDelta_89() { return &___m_lineSpacingDelta_89; }
	inline void set_m_lineSpacingDelta_89(float value)
	{
		___m_lineSpacingDelta_89 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_90() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineHeight_90)); }
	inline float get_m_lineHeight_90() const { return ___m_lineHeight_90; }
	inline float* get_address_of_m_lineHeight_90() { return &___m_lineHeight_90; }
	inline void set_m_lineHeight_90(float value)
	{
		___m_lineHeight_90 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_91() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacingMax_91)); }
	inline float get_m_lineSpacingMax_91() const { return ___m_lineSpacingMax_91; }
	inline float* get_address_of_m_lineSpacingMax_91() { return &___m_lineSpacingMax_91; }
	inline void set_m_lineSpacingMax_91(float value)
	{
		___m_lineSpacingMax_91 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_92() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_paragraphSpacing_92)); }
	inline float get_m_paragraphSpacing_92() const { return ___m_paragraphSpacing_92; }
	inline float* get_address_of_m_paragraphSpacing_92() { return &___m_paragraphSpacing_92; }
	inline void set_m_paragraphSpacing_92(float value)
	{
		___m_paragraphSpacing_92 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_93() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charWidthMaxAdj_93)); }
	inline float get_m_charWidthMaxAdj_93() const { return ___m_charWidthMaxAdj_93; }
	inline float* get_address_of_m_charWidthMaxAdj_93() { return &___m_charWidthMaxAdj_93; }
	inline void set_m_charWidthMaxAdj_93(float value)
	{
		___m_charWidthMaxAdj_93 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_94() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charWidthAdjDelta_94)); }
	inline float get_m_charWidthAdjDelta_94() const { return ___m_charWidthAdjDelta_94; }
	inline float* get_address_of_m_charWidthAdjDelta_94() { return &___m_charWidthAdjDelta_94; }
	inline void set_m_charWidthAdjDelta_94(float value)
	{
		___m_charWidthAdjDelta_94 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_95() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableWordWrapping_95)); }
	inline bool get_m_enableWordWrapping_95() const { return ___m_enableWordWrapping_95; }
	inline bool* get_address_of_m_enableWordWrapping_95() { return &___m_enableWordWrapping_95; }
	inline void set_m_enableWordWrapping_95(bool value)
	{
		___m_enableWordWrapping_95 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_96() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCharacterWrappingEnabled_96)); }
	inline bool get_m_isCharacterWrappingEnabled_96() const { return ___m_isCharacterWrappingEnabled_96; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_96() { return &___m_isCharacterWrappingEnabled_96; }
	inline void set_m_isCharacterWrappingEnabled_96(bool value)
	{
		___m_isCharacterWrappingEnabled_96 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_97() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isNonBreakingSpace_97)); }
	inline bool get_m_isNonBreakingSpace_97() const { return ___m_isNonBreakingSpace_97; }
	inline bool* get_address_of_m_isNonBreakingSpace_97() { return &___m_isNonBreakingSpace_97; }
	inline void set_m_isNonBreakingSpace_97(bool value)
	{
		___m_isNonBreakingSpace_97 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_98() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isIgnoringAlignment_98)); }
	inline bool get_m_isIgnoringAlignment_98() const { return ___m_isIgnoringAlignment_98; }
	inline bool* get_address_of_m_isIgnoringAlignment_98() { return &___m_isIgnoringAlignment_98; }
	inline void set_m_isIgnoringAlignment_98(bool value)
	{
		___m_isIgnoringAlignment_98 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_99() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_wordWrappingRatios_99)); }
	inline float get_m_wordWrappingRatios_99() const { return ___m_wordWrappingRatios_99; }
	inline float* get_address_of_m_wordWrappingRatios_99() { return &___m_wordWrappingRatios_99; }
	inline void set_m_wordWrappingRatios_99(float value)
	{
		___m_wordWrappingRatios_99 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_100() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_overflowMode_100)); }
	inline int32_t get_m_overflowMode_100() const { return ___m_overflowMode_100; }
	inline int32_t* get_address_of_m_overflowMode_100() { return &___m_overflowMode_100; }
	inline void set_m_overflowMode_100(int32_t value)
	{
		___m_overflowMode_100 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_101() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstOverflowCharacterIndex_101)); }
	inline int32_t get_m_firstOverflowCharacterIndex_101() const { return ___m_firstOverflowCharacterIndex_101; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_101() { return &___m_firstOverflowCharacterIndex_101; }
	inline void set_m_firstOverflowCharacterIndex_101(int32_t value)
	{
		___m_firstOverflowCharacterIndex_101 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_102() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_linkedTextComponent_102)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_linkedTextComponent_102() const { return ___m_linkedTextComponent_102; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_linkedTextComponent_102() { return &___m_linkedTextComponent_102; }
	inline void set_m_linkedTextComponent_102(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_linkedTextComponent_102 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_102), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_103() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isLinkedTextComponent_103)); }
	inline bool get_m_isLinkedTextComponent_103() const { return ___m_isLinkedTextComponent_103; }
	inline bool* get_address_of_m_isLinkedTextComponent_103() { return &___m_isLinkedTextComponent_103; }
	inline void set_m_isLinkedTextComponent_103(bool value)
	{
		___m_isLinkedTextComponent_103 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_104() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isTextTruncated_104)); }
	inline bool get_m_isTextTruncated_104() const { return ___m_isTextTruncated_104; }
	inline bool* get_address_of_m_isTextTruncated_104() { return &___m_isTextTruncated_104; }
	inline void set_m_isTextTruncated_104(bool value)
	{
		___m_isTextTruncated_104 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_105() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableKerning_105)); }
	inline bool get_m_enableKerning_105() const { return ___m_enableKerning_105; }
	inline bool* get_address_of_m_enableKerning_105() { return &___m_enableKerning_105; }
	inline void set_m_enableKerning_105(bool value)
	{
		___m_enableKerning_105 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_106() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableExtraPadding_106)); }
	inline bool get_m_enableExtraPadding_106() const { return ___m_enableExtraPadding_106; }
	inline bool* get_address_of_m_enableExtraPadding_106() { return &___m_enableExtraPadding_106; }
	inline void set_m_enableExtraPadding_106(bool value)
	{
		___m_enableExtraPadding_106 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_107() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___checkPaddingRequired_107)); }
	inline bool get_checkPaddingRequired_107() const { return ___checkPaddingRequired_107; }
	inline bool* get_address_of_checkPaddingRequired_107() { return &___checkPaddingRequired_107; }
	inline void set_checkPaddingRequired_107(bool value)
	{
		___checkPaddingRequired_107 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_108() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isRichText_108)); }
	inline bool get_m_isRichText_108() const { return ___m_isRichText_108; }
	inline bool* get_address_of_m_isRichText_108() { return &___m_isRichText_108; }
	inline void set_m_isRichText_108(bool value)
	{
		___m_isRichText_108 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_109() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_parseCtrlCharacters_109)); }
	inline bool get_m_parseCtrlCharacters_109() const { return ___m_parseCtrlCharacters_109; }
	inline bool* get_address_of_m_parseCtrlCharacters_109() { return &___m_parseCtrlCharacters_109; }
	inline void set_m_parseCtrlCharacters_109(bool value)
	{
		___m_parseCtrlCharacters_109 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_110() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isOverlay_110)); }
	inline bool get_m_isOverlay_110() const { return ___m_isOverlay_110; }
	inline bool* get_address_of_m_isOverlay_110() { return &___m_isOverlay_110; }
	inline void set_m_isOverlay_110(bool value)
	{
		___m_isOverlay_110 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_111() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isOrthographic_111)); }
	inline bool get_m_isOrthographic_111() const { return ___m_isOrthographic_111; }
	inline bool* get_address_of_m_isOrthographic_111() { return &___m_isOrthographic_111; }
	inline void set_m_isOrthographic_111(bool value)
	{
		___m_isOrthographic_111 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_112() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCullingEnabled_112)); }
	inline bool get_m_isCullingEnabled_112() const { return ___m_isCullingEnabled_112; }
	inline bool* get_address_of_m_isCullingEnabled_112() { return &___m_isCullingEnabled_112; }
	inline void set_m_isCullingEnabled_112(bool value)
	{
		___m_isCullingEnabled_112 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_113() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreRectMaskCulling_113)); }
	inline bool get_m_ignoreRectMaskCulling_113() const { return ___m_ignoreRectMaskCulling_113; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_113() { return &___m_ignoreRectMaskCulling_113; }
	inline void set_m_ignoreRectMaskCulling_113(bool value)
	{
		___m_ignoreRectMaskCulling_113 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_114() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreCulling_114)); }
	inline bool get_m_ignoreCulling_114() const { return ___m_ignoreCulling_114; }
	inline bool* get_address_of_m_ignoreCulling_114() { return &___m_ignoreCulling_114; }
	inline void set_m_ignoreCulling_114(bool value)
	{
		___m_ignoreCulling_114 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_115() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_horizontalMapping_115)); }
	inline int32_t get_m_horizontalMapping_115() const { return ___m_horizontalMapping_115; }
	inline int32_t* get_address_of_m_horizontalMapping_115() { return &___m_horizontalMapping_115; }
	inline void set_m_horizontalMapping_115(int32_t value)
	{
		___m_horizontalMapping_115 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_116() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_verticalMapping_116)); }
	inline int32_t get_m_verticalMapping_116() const { return ___m_verticalMapping_116; }
	inline int32_t* get_address_of_m_verticalMapping_116() { return &___m_verticalMapping_116; }
	inline void set_m_verticalMapping_116(int32_t value)
	{
		___m_verticalMapping_116 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_117() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_uvLineOffset_117)); }
	inline float get_m_uvLineOffset_117() const { return ___m_uvLineOffset_117; }
	inline float* get_address_of_m_uvLineOffset_117() { return &___m_uvLineOffset_117; }
	inline void set_m_uvLineOffset_117(float value)
	{
		___m_uvLineOffset_117 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_118() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderMode_118)); }
	inline int32_t get_m_renderMode_118() const { return ___m_renderMode_118; }
	inline int32_t* get_address_of_m_renderMode_118() { return &___m_renderMode_118; }
	inline void set_m_renderMode_118(int32_t value)
	{
		___m_renderMode_118 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_119() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_geometrySortingOrder_119)); }
	inline int32_t get_m_geometrySortingOrder_119() const { return ___m_geometrySortingOrder_119; }
	inline int32_t* get_address_of_m_geometrySortingOrder_119() { return &___m_geometrySortingOrder_119; }
	inline void set_m_geometrySortingOrder_119(int32_t value)
	{
		___m_geometrySortingOrder_119 = value;
	}

	inline static int32_t get_offset_of_m_VertexBufferAutoSizeReduction_120() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_VertexBufferAutoSizeReduction_120)); }
	inline bool get_m_VertexBufferAutoSizeReduction_120() const { return ___m_VertexBufferAutoSizeReduction_120; }
	inline bool* get_address_of_m_VertexBufferAutoSizeReduction_120() { return &___m_VertexBufferAutoSizeReduction_120; }
	inline void set_m_VertexBufferAutoSizeReduction_120(bool value)
	{
		___m_VertexBufferAutoSizeReduction_120 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_121() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstVisibleCharacter_121)); }
	inline int32_t get_m_firstVisibleCharacter_121() const { return ___m_firstVisibleCharacter_121; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_121() { return &___m_firstVisibleCharacter_121; }
	inline void set_m_firstVisibleCharacter_121(int32_t value)
	{
		___m_firstVisibleCharacter_121 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_122() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleCharacters_122)); }
	inline int32_t get_m_maxVisibleCharacters_122() const { return ___m_maxVisibleCharacters_122; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_122() { return &___m_maxVisibleCharacters_122; }
	inline void set_m_maxVisibleCharacters_122(int32_t value)
	{
		___m_maxVisibleCharacters_122 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_123() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleWords_123)); }
	inline int32_t get_m_maxVisibleWords_123() const { return ___m_maxVisibleWords_123; }
	inline int32_t* get_address_of_m_maxVisibleWords_123() { return &___m_maxVisibleWords_123; }
	inline void set_m_maxVisibleWords_123(int32_t value)
	{
		___m_maxVisibleWords_123 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_124() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleLines_124)); }
	inline int32_t get_m_maxVisibleLines_124() const { return ___m_maxVisibleLines_124; }
	inline int32_t* get_address_of_m_maxVisibleLines_124() { return &___m_maxVisibleLines_124; }
	inline void set_m_maxVisibleLines_124(int32_t value)
	{
		___m_maxVisibleLines_124 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_125() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_useMaxVisibleDescender_125)); }
	inline bool get_m_useMaxVisibleDescender_125() const { return ___m_useMaxVisibleDescender_125; }
	inline bool* get_address_of_m_useMaxVisibleDescender_125() { return &___m_useMaxVisibleDescender_125; }
	inline void set_m_useMaxVisibleDescender_125(bool value)
	{
		___m_useMaxVisibleDescender_125 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_126() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_pageToDisplay_126)); }
	inline int32_t get_m_pageToDisplay_126() const { return ___m_pageToDisplay_126; }
	inline int32_t* get_address_of_m_pageToDisplay_126() { return &___m_pageToDisplay_126; }
	inline void set_m_pageToDisplay_126(int32_t value)
	{
		___m_pageToDisplay_126 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_127() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isNewPage_127)); }
	inline bool get_m_isNewPage_127() const { return ___m_isNewPage_127; }
	inline bool* get_address_of_m_isNewPage_127() { return &___m_isNewPage_127; }
	inline void set_m_isNewPage_127(bool value)
	{
		___m_isNewPage_127 = value;
	}

	inline static int32_t get_offset_of_m_margin_128() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_margin_128)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_margin_128() const { return ___m_margin_128; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_margin_128() { return &___m_margin_128; }
	inline void set_m_margin_128(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_margin_128 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_129() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginLeft_129)); }
	inline float get_m_marginLeft_129() const { return ___m_marginLeft_129; }
	inline float* get_address_of_m_marginLeft_129() { return &___m_marginLeft_129; }
	inline void set_m_marginLeft_129(float value)
	{
		___m_marginLeft_129 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_130() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginRight_130)); }
	inline float get_m_marginRight_130() const { return ___m_marginRight_130; }
	inline float* get_address_of_m_marginRight_130() { return &___m_marginRight_130; }
	inline void set_m_marginRight_130(float value)
	{
		___m_marginRight_130 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_131() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginWidth_131)); }
	inline float get_m_marginWidth_131() const { return ___m_marginWidth_131; }
	inline float* get_address_of_m_marginWidth_131() { return &___m_marginWidth_131; }
	inline void set_m_marginWidth_131(float value)
	{
		___m_marginWidth_131 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_132() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginHeight_132)); }
	inline float get_m_marginHeight_132() const { return ___m_marginHeight_132; }
	inline float* get_address_of_m_marginHeight_132() { return &___m_marginHeight_132; }
	inline void set_m_marginHeight_132(float value)
	{
		___m_marginHeight_132 = value;
	}

	inline static int32_t get_offset_of_m_width_133() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_width_133)); }
	inline float get_m_width_133() const { return ___m_width_133; }
	inline float* get_address_of_m_width_133() { return &___m_width_133; }
	inline void set_m_width_133(float value)
	{
		___m_width_133 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_134() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textInfo_134)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_m_textInfo_134() const { return ___m_textInfo_134; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_m_textInfo_134() { return &___m_textInfo_134; }
	inline void set_m_textInfo_134(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___m_textInfo_134 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_134), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_135() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_havePropertiesChanged_135)); }
	inline bool get_m_havePropertiesChanged_135() const { return ___m_havePropertiesChanged_135; }
	inline bool* get_address_of_m_havePropertiesChanged_135() { return &___m_havePropertiesChanged_135; }
	inline void set_m_havePropertiesChanged_135(bool value)
	{
		___m_havePropertiesChanged_135 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_136() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isUsingLegacyAnimationComponent_136)); }
	inline bool get_m_isUsingLegacyAnimationComponent_136() const { return ___m_isUsingLegacyAnimationComponent_136; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_136() { return &___m_isUsingLegacyAnimationComponent_136; }
	inline void set_m_isUsingLegacyAnimationComponent_136(bool value)
	{
		___m_isUsingLegacyAnimationComponent_136 = value;
	}

	inline static int32_t get_offset_of_m_transform_137() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_transform_137)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_transform_137() const { return ___m_transform_137; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_transform_137() { return &___m_transform_137; }
	inline void set_m_transform_137(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_transform_137 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_137), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_138() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_rectTransform_138)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_138() const { return ___m_rectTransform_138; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_138() { return &___m_rectTransform_138; }
	inline void set_m_rectTransform_138(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_138 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_138), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_139() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___U3CautoSizeTextContainerU3Ek__BackingField_139)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_139() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_139; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_139() { return &___U3CautoSizeTextContainerU3Ek__BackingField_139; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_139(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_139 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_140() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_autoSizeTextContainer_140)); }
	inline bool get_m_autoSizeTextContainer_140() const { return ___m_autoSizeTextContainer_140; }
	inline bool* get_address_of_m_autoSizeTextContainer_140() { return &___m_autoSizeTextContainer_140; }
	inline void set_m_autoSizeTextContainer_140(bool value)
	{
		___m_autoSizeTextContainer_140 = value;
	}

	inline static int32_t get_offset_of_m_mesh_141() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_mesh_141)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_141() const { return ___m_mesh_141; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_141() { return &___m_mesh_141; }
	inline void set_m_mesh_141(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_141 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_141), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_142() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isVolumetricText_142)); }
	inline bool get_m_isVolumetricText_142() const { return ___m_isVolumetricText_142; }
	inline bool* get_address_of_m_isVolumetricText_142() { return &___m_isVolumetricText_142; }
	inline void set_m_isVolumetricText_142(bool value)
	{
		___m_isVolumetricText_142 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_143() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAnimator_143)); }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * get_m_spriteAnimator_143() const { return ___m_spriteAnimator_143; }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 ** get_address_of_m_spriteAnimator_143() { return &___m_spriteAnimator_143; }
	inline void set_m_spriteAnimator_143(TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * value)
	{
		___m_spriteAnimator_143 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_143), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_144() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_flexibleHeight_144)); }
	inline float get_m_flexibleHeight_144() const { return ___m_flexibleHeight_144; }
	inline float* get_address_of_m_flexibleHeight_144() { return &___m_flexibleHeight_144; }
	inline void set_m_flexibleHeight_144(float value)
	{
		___m_flexibleHeight_144 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_145() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_flexibleWidth_145)); }
	inline float get_m_flexibleWidth_145() const { return ___m_flexibleWidth_145; }
	inline float* get_address_of_m_flexibleWidth_145() { return &___m_flexibleWidth_145; }
	inline void set_m_flexibleWidth_145(float value)
	{
		___m_flexibleWidth_145 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_146() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minWidth_146)); }
	inline float get_m_minWidth_146() const { return ___m_minWidth_146; }
	inline float* get_address_of_m_minWidth_146() { return &___m_minWidth_146; }
	inline void set_m_minWidth_146(float value)
	{
		___m_minWidth_146 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_147() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minHeight_147)); }
	inline float get_m_minHeight_147() const { return ___m_minHeight_147; }
	inline float* get_address_of_m_minHeight_147() { return &___m_minHeight_147; }
	inline void set_m_minHeight_147(float value)
	{
		___m_minHeight_147 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_148() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxWidth_148)); }
	inline float get_m_maxWidth_148() const { return ___m_maxWidth_148; }
	inline float* get_address_of_m_maxWidth_148() { return &___m_maxWidth_148; }
	inline void set_m_maxWidth_148(float value)
	{
		___m_maxWidth_148 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_149() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxHeight_149)); }
	inline float get_m_maxHeight_149() const { return ___m_maxHeight_149; }
	inline float* get_address_of_m_maxHeight_149() { return &___m_maxHeight_149; }
	inline void set_m_maxHeight_149(float value)
	{
		___m_maxHeight_149 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_150() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_LayoutElement_150)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_m_LayoutElement_150() const { return ___m_LayoutElement_150; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_m_LayoutElement_150() { return &___m_LayoutElement_150; }
	inline void set_m_LayoutElement_150(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___m_LayoutElement_150 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_150), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_151() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_preferredWidth_151)); }
	inline float get_m_preferredWidth_151() const { return ___m_preferredWidth_151; }
	inline float* get_address_of_m_preferredWidth_151() { return &___m_preferredWidth_151; }
	inline void set_m_preferredWidth_151(float value)
	{
		___m_preferredWidth_151 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_152() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderedWidth_152)); }
	inline float get_m_renderedWidth_152() const { return ___m_renderedWidth_152; }
	inline float* get_address_of_m_renderedWidth_152() { return &___m_renderedWidth_152; }
	inline void set_m_renderedWidth_152(float value)
	{
		___m_renderedWidth_152 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_153() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isPreferredWidthDirty_153)); }
	inline bool get_m_isPreferredWidthDirty_153() const { return ___m_isPreferredWidthDirty_153; }
	inline bool* get_address_of_m_isPreferredWidthDirty_153() { return &___m_isPreferredWidthDirty_153; }
	inline void set_m_isPreferredWidthDirty_153(bool value)
	{
		___m_isPreferredWidthDirty_153 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_154() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_preferredHeight_154)); }
	inline float get_m_preferredHeight_154() const { return ___m_preferredHeight_154; }
	inline float* get_address_of_m_preferredHeight_154() { return &___m_preferredHeight_154; }
	inline void set_m_preferredHeight_154(float value)
	{
		___m_preferredHeight_154 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_155() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderedHeight_155)); }
	inline float get_m_renderedHeight_155() const { return ___m_renderedHeight_155; }
	inline float* get_address_of_m_renderedHeight_155() { return &___m_renderedHeight_155; }
	inline void set_m_renderedHeight_155(float value)
	{
		___m_renderedHeight_155 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_156() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isPreferredHeightDirty_156)); }
	inline bool get_m_isPreferredHeightDirty_156() const { return ___m_isPreferredHeightDirty_156; }
	inline bool* get_address_of_m_isPreferredHeightDirty_156() { return &___m_isPreferredHeightDirty_156; }
	inline void set_m_isPreferredHeightDirty_156(bool value)
	{
		___m_isPreferredHeightDirty_156 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_157() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCalculatingPreferredValues_157)); }
	inline bool get_m_isCalculatingPreferredValues_157() const { return ___m_isCalculatingPreferredValues_157; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_157() { return &___m_isCalculatingPreferredValues_157; }
	inline void set_m_isCalculatingPreferredValues_157(bool value)
	{
		___m_isCalculatingPreferredValues_157 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_158() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_recursiveCount_158)); }
	inline int32_t get_m_recursiveCount_158() const { return ___m_recursiveCount_158; }
	inline int32_t* get_address_of_m_recursiveCount_158() { return &___m_recursiveCount_158; }
	inline void set_m_recursiveCount_158(int32_t value)
	{
		___m_recursiveCount_158 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_159() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_layoutPriority_159)); }
	inline int32_t get_m_layoutPriority_159() const { return ___m_layoutPriority_159; }
	inline int32_t* get_address_of_m_layoutPriority_159() { return &___m_layoutPriority_159; }
	inline void set_m_layoutPriority_159(int32_t value)
	{
		___m_layoutPriority_159 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_160() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCalculateSizeRequired_160)); }
	inline bool get_m_isCalculateSizeRequired_160() const { return ___m_isCalculateSizeRequired_160; }
	inline bool* get_address_of_m_isCalculateSizeRequired_160() { return &___m_isCalculateSizeRequired_160; }
	inline void set_m_isCalculateSizeRequired_160(bool value)
	{
		___m_isCalculateSizeRequired_160 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_161() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isLayoutDirty_161)); }
	inline bool get_m_isLayoutDirty_161() const { return ___m_isLayoutDirty_161; }
	inline bool* get_address_of_m_isLayoutDirty_161() { return &___m_isLayoutDirty_161; }
	inline void set_m_isLayoutDirty_161(bool value)
	{
		___m_isLayoutDirty_161 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_162() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_verticesAlreadyDirty_162)); }
	inline bool get_m_verticesAlreadyDirty_162() const { return ___m_verticesAlreadyDirty_162; }
	inline bool* get_address_of_m_verticesAlreadyDirty_162() { return &___m_verticesAlreadyDirty_162; }
	inline void set_m_verticesAlreadyDirty_162(bool value)
	{
		___m_verticesAlreadyDirty_162 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_163() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_layoutAlreadyDirty_163)); }
	inline bool get_m_layoutAlreadyDirty_163() const { return ___m_layoutAlreadyDirty_163; }
	inline bool* get_address_of_m_layoutAlreadyDirty_163() { return &___m_layoutAlreadyDirty_163; }
	inline void set_m_layoutAlreadyDirty_163(bool value)
	{
		___m_layoutAlreadyDirty_163 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_164() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isAwake_164)); }
	inline bool get_m_isAwake_164() const { return ___m_isAwake_164; }
	inline bool* get_address_of_m_isAwake_164() { return &___m_isAwake_164; }
	inline void set_m_isAwake_164(bool value)
	{
		___m_isAwake_164 = value;
	}

	inline static int32_t get_offset_of_m_isWaitingOnResourceLoad_165() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isWaitingOnResourceLoad_165)); }
	inline bool get_m_isWaitingOnResourceLoad_165() const { return ___m_isWaitingOnResourceLoad_165; }
	inline bool* get_address_of_m_isWaitingOnResourceLoad_165() { return &___m_isWaitingOnResourceLoad_165; }
	inline void set_m_isWaitingOnResourceLoad_165(bool value)
	{
		___m_isWaitingOnResourceLoad_165 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_166() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isInputParsingRequired_166)); }
	inline bool get_m_isInputParsingRequired_166() const { return ___m_isInputParsingRequired_166; }
	inline bool* get_address_of_m_isInputParsingRequired_166() { return &___m_isInputParsingRequired_166; }
	inline void set_m_isInputParsingRequired_166(bool value)
	{
		___m_isInputParsingRequired_166 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_167() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_inputSource_167)); }
	inline int32_t get_m_inputSource_167() const { return ___m_inputSource_167; }
	inline int32_t* get_address_of_m_inputSource_167() { return &___m_inputSource_167; }
	inline void set_m_inputSource_167(int32_t value)
	{
		___m_inputSource_167 = value;
	}

	inline static int32_t get_offset_of_old_text_168() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___old_text_168)); }
	inline String_t* get_old_text_168() const { return ___old_text_168; }
	inline String_t** get_address_of_old_text_168() { return &___old_text_168; }
	inline void set_old_text_168(String_t* value)
	{
		___old_text_168 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_168), value);
	}

	inline static int32_t get_offset_of_m_fontScale_169() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontScale_169)); }
	inline float get_m_fontScale_169() const { return ___m_fontScale_169; }
	inline float* get_address_of_m_fontScale_169() { return &___m_fontScale_169; }
	inline void set_m_fontScale_169(float value)
	{
		___m_fontScale_169 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_170() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontScaleMultiplier_170)); }
	inline float get_m_fontScaleMultiplier_170() const { return ___m_fontScaleMultiplier_170; }
	inline float* get_address_of_m_fontScaleMultiplier_170() { return &___m_fontScaleMultiplier_170; }
	inline void set_m_fontScaleMultiplier_170(float value)
	{
		___m_fontScaleMultiplier_170 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_171() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_htmlTag_171)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_htmlTag_171() const { return ___m_htmlTag_171; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_htmlTag_171() { return &___m_htmlTag_171; }
	inline void set_m_htmlTag_171(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_htmlTag_171 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_171), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_172() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_xmlAttribute_172)); }
	inline RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* get_m_xmlAttribute_172() const { return ___m_xmlAttribute_172; }
	inline RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652** get_address_of_m_xmlAttribute_172() { return &___m_xmlAttribute_172; }
	inline void set_m_xmlAttribute_172(RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* value)
	{
		___m_xmlAttribute_172 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_172), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_173() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_attributeParameterValues_173)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_attributeParameterValues_173() const { return ___m_attributeParameterValues_173; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_attributeParameterValues_173() { return &___m_attributeParameterValues_173; }
	inline void set_m_attributeParameterValues_173(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_attributeParameterValues_173 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_173), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_174() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_LineIndent_174)); }
	inline float get_tag_LineIndent_174() const { return ___tag_LineIndent_174; }
	inline float* get_address_of_tag_LineIndent_174() { return &___tag_LineIndent_174; }
	inline void set_tag_LineIndent_174(float value)
	{
		___tag_LineIndent_174 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_175() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_Indent_175)); }
	inline float get_tag_Indent_175() const { return ___tag_Indent_175; }
	inline float* get_address_of_tag_Indent_175() { return &___tag_Indent_175; }
	inline void set_tag_Indent_175(float value)
	{
		___tag_Indent_175 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_176() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_indentStack_176)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_m_indentStack_176() const { return ___m_indentStack_176; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_m_indentStack_176() { return &___m_indentStack_176; }
	inline void set_m_indentStack_176(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___m_indentStack_176 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_177() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_NoParsing_177)); }
	inline bool get_tag_NoParsing_177() const { return ___tag_NoParsing_177; }
	inline bool* get_address_of_tag_NoParsing_177() { return &___tag_NoParsing_177; }
	inline void set_tag_NoParsing_177(bool value)
	{
		___tag_NoParsing_177 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_178() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isParsingText_178)); }
	inline bool get_m_isParsingText_178() const { return ___m_isParsingText_178; }
	inline bool* get_address_of_m_isParsingText_178() { return &___m_isParsingText_178; }
	inline void set_m_isParsingText_178(bool value)
	{
		___m_isParsingText_178 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_179() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FXMatrix_179)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_FXMatrix_179() const { return ___m_FXMatrix_179; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_FXMatrix_179() { return &___m_FXMatrix_179; }
	inline void set_m_FXMatrix_179(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_FXMatrix_179 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_180() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isFXMatrixSet_180)); }
	inline bool get_m_isFXMatrixSet_180() const { return ___m_isFXMatrixSet_180; }
	inline bool* get_address_of_m_isFXMatrixSet_180() { return &___m_isFXMatrixSet_180; }
	inline void set_m_isFXMatrixSet_180(bool value)
	{
		___m_isFXMatrixSet_180 = value;
	}

	inline static int32_t get_offset_of_m_TextParsingBuffer_181() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_TextParsingBuffer_181)); }
	inline UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* get_m_TextParsingBuffer_181() const { return ___m_TextParsingBuffer_181; }
	inline UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505** get_address_of_m_TextParsingBuffer_181() { return &___m_TextParsingBuffer_181; }
	inline void set_m_TextParsingBuffer_181(UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* value)
	{
		___m_TextParsingBuffer_181 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextParsingBuffer_181), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_182() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_internalCharacterInfo_182)); }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* get_m_internalCharacterInfo_182() const { return ___m_internalCharacterInfo_182; }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604** get_address_of_m_internalCharacterInfo_182() { return &___m_internalCharacterInfo_182; }
	inline void set_m_internalCharacterInfo_182(TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* value)
	{
		___m_internalCharacterInfo_182 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_182), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_183() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_input_CharArray_183)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_input_CharArray_183() const { return ___m_input_CharArray_183; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_input_CharArray_183() { return &___m_input_CharArray_183; }
	inline void set_m_input_CharArray_183(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_input_CharArray_183 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_183), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_184() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charArray_Length_184)); }
	inline int32_t get_m_charArray_Length_184() const { return ___m_charArray_Length_184; }
	inline int32_t* get_address_of_m_charArray_Length_184() { return &___m_charArray_Length_184; }
	inline void set_m_charArray_Length_184(int32_t value)
	{
		___m_charArray_Length_184 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_185() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_totalCharacterCount_185)); }
	inline int32_t get_m_totalCharacterCount_185() const { return ___m_totalCharacterCount_185; }
	inline int32_t* get_address_of_m_totalCharacterCount_185() { return &___m_totalCharacterCount_185; }
	inline void set_m_totalCharacterCount_185(int32_t value)
	{
		___m_totalCharacterCount_185 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_186() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_SavedWordWrapState_186)); }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  get_m_SavedWordWrapState_186() const { return ___m_SavedWordWrapState_186; }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 * get_address_of_m_SavedWordWrapState_186() { return &___m_SavedWordWrapState_186; }
	inline void set_m_SavedWordWrapState_186(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  value)
	{
		___m_SavedWordWrapState_186 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_187() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_SavedLineState_187)); }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  get_m_SavedLineState_187() const { return ___m_SavedLineState_187; }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 * get_address_of_m_SavedLineState_187() { return &___m_SavedLineState_187; }
	inline void set_m_SavedLineState_187(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  value)
	{
		___m_SavedLineState_187 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_188() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_characterCount_188)); }
	inline int32_t get_m_characterCount_188() const { return ___m_characterCount_188; }
	inline int32_t* get_address_of_m_characterCount_188() { return &___m_characterCount_188; }
	inline void set_m_characterCount_188(int32_t value)
	{
		___m_characterCount_188 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_189() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstCharacterOfLine_189)); }
	inline int32_t get_m_firstCharacterOfLine_189() const { return ___m_firstCharacterOfLine_189; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_189() { return &___m_firstCharacterOfLine_189; }
	inline void set_m_firstCharacterOfLine_189(int32_t value)
	{
		___m_firstCharacterOfLine_189 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_190() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstVisibleCharacterOfLine_190)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_190() const { return ___m_firstVisibleCharacterOfLine_190; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_190() { return &___m_firstVisibleCharacterOfLine_190; }
	inline void set_m_firstVisibleCharacterOfLine_190(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_190 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_191() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lastCharacterOfLine_191)); }
	inline int32_t get_m_lastCharacterOfLine_191() const { return ___m_lastCharacterOfLine_191; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_191() { return &___m_lastCharacterOfLine_191; }
	inline void set_m_lastCharacterOfLine_191(int32_t value)
	{
		___m_lastCharacterOfLine_191 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_192() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lastVisibleCharacterOfLine_192)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_192() const { return ___m_lastVisibleCharacterOfLine_192; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_192() { return &___m_lastVisibleCharacterOfLine_192; }
	inline void set_m_lastVisibleCharacterOfLine_192(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_192 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_193() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineNumber_193)); }
	inline int32_t get_m_lineNumber_193() const { return ___m_lineNumber_193; }
	inline int32_t* get_address_of_m_lineNumber_193() { return &___m_lineNumber_193; }
	inline void set_m_lineNumber_193(int32_t value)
	{
		___m_lineNumber_193 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_194() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineVisibleCharacterCount_194)); }
	inline int32_t get_m_lineVisibleCharacterCount_194() const { return ___m_lineVisibleCharacterCount_194; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_194() { return &___m_lineVisibleCharacterCount_194; }
	inline void set_m_lineVisibleCharacterCount_194(int32_t value)
	{
		___m_lineVisibleCharacterCount_194 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_195() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_pageNumber_195)); }
	inline int32_t get_m_pageNumber_195() const { return ___m_pageNumber_195; }
	inline int32_t* get_address_of_m_pageNumber_195() { return &___m_pageNumber_195; }
	inline void set_m_pageNumber_195(int32_t value)
	{
		___m_pageNumber_195 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxAscender_196)); }
	inline float get_m_maxAscender_196() const { return ___m_maxAscender_196; }
	inline float* get_address_of_m_maxAscender_196() { return &___m_maxAscender_196; }
	inline void set_m_maxAscender_196(float value)
	{
		___m_maxAscender_196 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_197() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxCapHeight_197)); }
	inline float get_m_maxCapHeight_197() const { return ___m_maxCapHeight_197; }
	inline float* get_address_of_m_maxCapHeight_197() { return &___m_maxCapHeight_197; }
	inline void set_m_maxCapHeight_197(float value)
	{
		___m_maxCapHeight_197 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_198() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxDescender_198)); }
	inline float get_m_maxDescender_198() const { return ___m_maxDescender_198; }
	inline float* get_address_of_m_maxDescender_198() { return &___m_maxDescender_198; }
	inline void set_m_maxDescender_198(float value)
	{
		___m_maxDescender_198 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_199() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxLineAscender_199)); }
	inline float get_m_maxLineAscender_199() const { return ___m_maxLineAscender_199; }
	inline float* get_address_of_m_maxLineAscender_199() { return &___m_maxLineAscender_199; }
	inline void set_m_maxLineAscender_199(float value)
	{
		___m_maxLineAscender_199 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_200() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxLineDescender_200)); }
	inline float get_m_maxLineDescender_200() const { return ___m_maxLineDescender_200; }
	inline float* get_address_of_m_maxLineDescender_200() { return &___m_maxLineDescender_200; }
	inline void set_m_maxLineDescender_200(float value)
	{
		___m_maxLineDescender_200 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_201() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_startOfLineAscender_201)); }
	inline float get_m_startOfLineAscender_201() const { return ___m_startOfLineAscender_201; }
	inline float* get_address_of_m_startOfLineAscender_201() { return &___m_startOfLineAscender_201; }
	inline void set_m_startOfLineAscender_201(float value)
	{
		___m_startOfLineAscender_201 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_202() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineOffset_202)); }
	inline float get_m_lineOffset_202() const { return ___m_lineOffset_202; }
	inline float* get_address_of_m_lineOffset_202() { return &___m_lineOffset_202; }
	inline void set_m_lineOffset_202(float value)
	{
		___m_lineOffset_202 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_203() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_meshExtents_203)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_m_meshExtents_203() const { return ___m_meshExtents_203; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_m_meshExtents_203() { return &___m_meshExtents_203; }
	inline void set_m_meshExtents_203(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___m_meshExtents_203 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_204() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_htmlColor_204)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_htmlColor_204() const { return ___m_htmlColor_204; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_htmlColor_204() { return &___m_htmlColor_204; }
	inline void set_m_htmlColor_204(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_htmlColor_204 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorStack_205)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_colorStack_205() const { return ___m_colorStack_205; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_colorStack_205() { return &___m_colorStack_205; }
	inline void set_m_colorStack_205(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_colorStack_205 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_206() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_underlineColorStack_206)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_underlineColorStack_206() const { return ___m_underlineColorStack_206; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_underlineColorStack_206() { return &___m_underlineColorStack_206; }
	inline void set_m_underlineColorStack_206(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_underlineColorStack_206 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_207() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_strikethroughColorStack_207)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_strikethroughColorStack_207() const { return ___m_strikethroughColorStack_207; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_strikethroughColorStack_207() { return &___m_strikethroughColorStack_207; }
	inline void set_m_strikethroughColorStack_207(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_strikethroughColorStack_207 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightColorStack_208)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_highlightColorStack_208() const { return ___m_highlightColorStack_208; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_highlightColorStack_208() { return &___m_highlightColorStack_208; }
	inline void set_m_highlightColorStack_208(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_highlightColorStack_208 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_209() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorGradientPreset_209)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_colorGradientPreset_209() const { return ___m_colorGradientPreset_209; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_colorGradientPreset_209() { return &___m_colorGradientPreset_209; }
	inline void set_m_colorGradientPreset_209(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_colorGradientPreset_209 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_209), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_210() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorGradientStack_210)); }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  get_m_colorGradientStack_210() const { return ___m_colorGradientStack_210; }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D * get_address_of_m_colorGradientStack_210() { return &___m_colorGradientStack_210; }
	inline void set_m_colorGradientStack_210(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  value)
	{
		___m_colorGradientStack_210 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_211() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tabSpacing_211)); }
	inline float get_m_tabSpacing_211() const { return ___m_tabSpacing_211; }
	inline float* get_address_of_m_tabSpacing_211() { return &___m_tabSpacing_211; }
	inline void set_m_tabSpacing_211(float value)
	{
		___m_tabSpacing_211 = value;
	}

	inline static int32_t get_offset_of_m_spacing_212() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spacing_212)); }
	inline float get_m_spacing_212() const { return ___m_spacing_212; }
	inline float* get_address_of_m_spacing_212() { return &___m_spacing_212; }
	inline void set_m_spacing_212(float value)
	{
		___m_spacing_212 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_213() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_styleStack_213)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_m_styleStack_213() const { return ___m_styleStack_213; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_m_styleStack_213() { return &___m_styleStack_213; }
	inline void set_m_styleStack_213(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___m_styleStack_213 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_214() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_actionStack_214)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_m_actionStack_214() const { return ___m_actionStack_214; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_m_actionStack_214() { return &___m_actionStack_214; }
	inline void set_m_actionStack_214(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___m_actionStack_214 = value;
	}

	inline static int32_t get_offset_of_m_padding_215() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_padding_215)); }
	inline float get_m_padding_215() const { return ___m_padding_215; }
	inline float* get_address_of_m_padding_215() { return &___m_padding_215; }
	inline void set_m_padding_215(float value)
	{
		___m_padding_215 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_216() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_baselineOffset_216)); }
	inline float get_m_baselineOffset_216() const { return ___m_baselineOffset_216; }
	inline float* get_address_of_m_baselineOffset_216() { return &___m_baselineOffset_216; }
	inline void set_m_baselineOffset_216(float value)
	{
		___m_baselineOffset_216 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_217() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_baselineOffsetStack_217)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_m_baselineOffsetStack_217() const { return ___m_baselineOffsetStack_217; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_m_baselineOffsetStack_217() { return &___m_baselineOffsetStack_217; }
	inline void set_m_baselineOffsetStack_217(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___m_baselineOffsetStack_217 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_218() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_xAdvance_218)); }
	inline float get_m_xAdvance_218() const { return ___m_xAdvance_218; }
	inline float* get_address_of_m_xAdvance_218() { return &___m_xAdvance_218; }
	inline void set_m_xAdvance_218(float value)
	{
		___m_xAdvance_218 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_219() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textElementType_219)); }
	inline int32_t get_m_textElementType_219() const { return ___m_textElementType_219; }
	inline int32_t* get_address_of_m_textElementType_219() { return &___m_textElementType_219; }
	inline void set_m_textElementType_219(int32_t value)
	{
		___m_textElementType_219 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_220() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_TextElement_220)); }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * get_m_cached_TextElement_220() const { return ___m_cached_TextElement_220; }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 ** get_address_of_m_cached_TextElement_220() { return &___m_cached_TextElement_220; }
	inline void set_m_cached_TextElement_220(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * value)
	{
		___m_cached_TextElement_220 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_220), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_Character_221() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_Underline_Character_221)); }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D * get_m_cached_Underline_Character_221() const { return ___m_cached_Underline_Character_221; }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D ** get_address_of_m_cached_Underline_Character_221() { return &___m_cached_Underline_Character_221; }
	inline void set_m_cached_Underline_Character_221(TMP_Character_t1875AACA978396521498D6A699052C187903553D * value)
	{
		___m_cached_Underline_Character_221 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_Character_221), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_Character_222() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_Ellipsis_Character_222)); }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D * get_m_cached_Ellipsis_Character_222() const { return ___m_cached_Ellipsis_Character_222; }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D ** get_address_of_m_cached_Ellipsis_Character_222() { return &___m_cached_Ellipsis_Character_222; }
	inline void set_m_cached_Ellipsis_Character_222(TMP_Character_t1875AACA978396521498D6A699052C187903553D * value)
	{
		___m_cached_Ellipsis_Character_222 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_Character_222), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_223() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_defaultSpriteAsset_223)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_defaultSpriteAsset_223() const { return ___m_defaultSpriteAsset_223; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_defaultSpriteAsset_223() { return &___m_defaultSpriteAsset_223; }
	inline void set_m_defaultSpriteAsset_223(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_defaultSpriteAsset_223 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_223), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_224() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentSpriteAsset_224)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_currentSpriteAsset_224() const { return ___m_currentSpriteAsset_224; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_currentSpriteAsset_224() { return &___m_currentSpriteAsset_224; }
	inline void set_m_currentSpriteAsset_224(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_currentSpriteAsset_224 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_224), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_225() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteCount_225)); }
	inline int32_t get_m_spriteCount_225() const { return ___m_spriteCount_225; }
	inline int32_t* get_address_of_m_spriteCount_225() { return &___m_spriteCount_225; }
	inline void set_m_spriteCount_225(int32_t value)
	{
		___m_spriteCount_225 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_226() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteIndex_226)); }
	inline int32_t get_m_spriteIndex_226() const { return ___m_spriteIndex_226; }
	inline int32_t* get_address_of_m_spriteIndex_226() { return &___m_spriteIndex_226; }
	inline void set_m_spriteIndex_226(int32_t value)
	{
		___m_spriteIndex_226 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_227() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAnimationID_227)); }
	inline int32_t get_m_spriteAnimationID_227() const { return ___m_spriteAnimationID_227; }
	inline int32_t* get_address_of_m_spriteAnimationID_227() { return &___m_spriteAnimationID_227; }
	inline void set_m_spriteAnimationID_227(int32_t value)
	{
		___m_spriteAnimationID_227 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_228() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreActiveState_228)); }
	inline bool get_m_ignoreActiveState_228() const { return ___m_ignoreActiveState_228; }
	inline bool* get_address_of_m_ignoreActiveState_228() { return &___m_ignoreActiveState_228; }
	inline void set_m_ignoreActiveState_228(bool value)
	{
		___m_ignoreActiveState_228 = value;
	}

	inline static int32_t get_offset_of_k_Power_229() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___k_Power_229)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_k_Power_229() const { return ___k_Power_229; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_k_Power_229() { return &___k_Power_229; }
	inline void set_k_Power_229(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___k_Power_229 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_229), value);
	}
};

struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___s_colorWhite_47;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_LargePositiveVector2_230;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_LargeNegativeVector2_231;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_232;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_233;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_234;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_235;

public:
	inline static int32_t get_offset_of_s_colorWhite_47() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___s_colorWhite_47)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_s_colorWhite_47() const { return ___s_colorWhite_47; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_s_colorWhite_47() { return &___s_colorWhite_47; }
	inline void set_s_colorWhite_47(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___s_colorWhite_47 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_230() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveVector2_230)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_LargePositiveVector2_230() const { return ___k_LargePositiveVector2_230; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_LargePositiveVector2_230() { return &___k_LargePositiveVector2_230; }
	inline void set_k_LargePositiveVector2_230(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_LargePositiveVector2_230 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_231() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeVector2_231)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_LargeNegativeVector2_231() const { return ___k_LargeNegativeVector2_231; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_LargeNegativeVector2_231() { return &___k_LargeNegativeVector2_231; }
	inline void set_k_LargeNegativeVector2_231(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_LargeNegativeVector2_231 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_232() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveFloat_232)); }
	inline float get_k_LargePositiveFloat_232() const { return ___k_LargePositiveFloat_232; }
	inline float* get_address_of_k_LargePositiveFloat_232() { return &___k_LargePositiveFloat_232; }
	inline void set_k_LargePositiveFloat_232(float value)
	{
		___k_LargePositiveFloat_232 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_233() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeFloat_233)); }
	inline float get_k_LargeNegativeFloat_233() const { return ___k_LargeNegativeFloat_233; }
	inline float* get_address_of_k_LargeNegativeFloat_233() { return &___k_LargeNegativeFloat_233; }
	inline void set_k_LargeNegativeFloat_233(float value)
	{
		___k_LargeNegativeFloat_233 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_234() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveInt_234)); }
	inline int32_t get_k_LargePositiveInt_234() const { return ___k_LargePositiveInt_234; }
	inline int32_t* get_address_of_k_LargePositiveInt_234() { return &___k_LargePositiveInt_234; }
	inline void set_k_LargePositiveInt_234(int32_t value)
	{
		___k_LargePositiveInt_234 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_235() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeInt_235)); }
	inline int32_t get_k_LargeNegativeInt_235() const { return ___k_LargeNegativeInt_235; }
	inline int32_t* get_address_of_k_LargeNegativeInt_235() { return &___k_LargeNegativeInt_235; }
	inline void set_k_LargeNegativeInt_235(int32_t value)
	{
		___k_LargeNegativeInt_235 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#ifndef TEXTMESHPRO_T6FF60D9DCAF295045FE47C014CC855C5784752E2_H
#define TEXTMESHPRO_T6FF60D9DCAF295045FE47C014CC855C5784752E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshPro
struct  TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2  : public TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7
{
public:
	// System.Boolean TMPro.TextMeshPro::m_currentAutoSizeMode
	bool ___m_currentAutoSizeMode_236;
	// System.Boolean TMPro.TextMeshPro::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_237;
	// System.Single TMPro.TextMeshPro::m_previousLossyScaleY
	float ___m_previousLossyScaleY_238;
	// UnityEngine.Renderer TMPro.TextMeshPro::m_renderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___m_renderer_239;
	// UnityEngine.MeshFilter TMPro.TextMeshPro::m_meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___m_meshFilter_240;
	// System.Boolean TMPro.TextMeshPro::m_isFirstAllocation
	bool ___m_isFirstAllocation_241;
	// System.Int32 TMPro.TextMeshPro::m_max_characters
	int32_t ___m_max_characters_242;
	// System.Int32 TMPro.TextMeshPro::m_max_numberOfLines
	int32_t ___m_max_numberOfLines_243;
	// TMPro.TMP_SubMesh[] TMPro.TextMeshPro::m_subTextObjects
	TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD* ___m_subTextObjects_244;
	// System.Boolean TMPro.TextMeshPro::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_245;
	// System.Boolean TMPro.TextMeshPro::isMaskUpdateRequired
	bool ___isMaskUpdateRequired_246;
	// TMPro.MaskingTypes TMPro.TextMeshPro::m_maskType
	int32_t ___m_maskType_247;
	// UnityEngine.Matrix4x4 TMPro.TextMeshPro::m_EnvMapMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_EnvMapMatrix_248;
	// UnityEngine.Vector3[] TMPro.TextMeshPro::m_RectTransformCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_RectTransformCorners_249;
	// System.Boolean TMPro.TextMeshPro::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_250;
	// System.Int32 TMPro.TextMeshPro::loopCountA
	int32_t ___loopCountA_251;

public:
	inline static int32_t get_offset_of_m_currentAutoSizeMode_236() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_currentAutoSizeMode_236)); }
	inline bool get_m_currentAutoSizeMode_236() const { return ___m_currentAutoSizeMode_236; }
	inline bool* get_address_of_m_currentAutoSizeMode_236() { return &___m_currentAutoSizeMode_236; }
	inline void set_m_currentAutoSizeMode_236(bool value)
	{
		___m_currentAutoSizeMode_236 = value;
	}

	inline static int32_t get_offset_of_m_hasFontAssetChanged_237() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_hasFontAssetChanged_237)); }
	inline bool get_m_hasFontAssetChanged_237() const { return ___m_hasFontAssetChanged_237; }
	inline bool* get_address_of_m_hasFontAssetChanged_237() { return &___m_hasFontAssetChanged_237; }
	inline void set_m_hasFontAssetChanged_237(bool value)
	{
		___m_hasFontAssetChanged_237 = value;
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_238() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_previousLossyScaleY_238)); }
	inline float get_m_previousLossyScaleY_238() const { return ___m_previousLossyScaleY_238; }
	inline float* get_address_of_m_previousLossyScaleY_238() { return &___m_previousLossyScaleY_238; }
	inline void set_m_previousLossyScaleY_238(float value)
	{
		___m_previousLossyScaleY_238 = value;
	}

	inline static int32_t get_offset_of_m_renderer_239() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_renderer_239)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_m_renderer_239() const { return ___m_renderer_239; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_m_renderer_239() { return &___m_renderer_239; }
	inline void set_m_renderer_239(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___m_renderer_239 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_239), value);
	}

	inline static int32_t get_offset_of_m_meshFilter_240() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_meshFilter_240)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_m_meshFilter_240() const { return ___m_meshFilter_240; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_m_meshFilter_240() { return &___m_meshFilter_240; }
	inline void set_m_meshFilter_240(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___m_meshFilter_240 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_240), value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_241() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_isFirstAllocation_241)); }
	inline bool get_m_isFirstAllocation_241() const { return ___m_isFirstAllocation_241; }
	inline bool* get_address_of_m_isFirstAllocation_241() { return &___m_isFirstAllocation_241; }
	inline void set_m_isFirstAllocation_241(bool value)
	{
		___m_isFirstAllocation_241 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_242() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_max_characters_242)); }
	inline int32_t get_m_max_characters_242() const { return ___m_max_characters_242; }
	inline int32_t* get_address_of_m_max_characters_242() { return &___m_max_characters_242; }
	inline void set_m_max_characters_242(int32_t value)
	{
		___m_max_characters_242 = value;
	}

	inline static int32_t get_offset_of_m_max_numberOfLines_243() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_max_numberOfLines_243)); }
	inline int32_t get_m_max_numberOfLines_243() const { return ___m_max_numberOfLines_243; }
	inline int32_t* get_address_of_m_max_numberOfLines_243() { return &___m_max_numberOfLines_243; }
	inline void set_m_max_numberOfLines_243(int32_t value)
	{
		___m_max_numberOfLines_243 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_244() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_subTextObjects_244)); }
	inline TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD* get_m_subTextObjects_244() const { return ___m_subTextObjects_244; }
	inline TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD** get_address_of_m_subTextObjects_244() { return &___m_subTextObjects_244; }
	inline void set_m_subTextObjects_244(TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD* value)
	{
		___m_subTextObjects_244 = value;
		Il2CppCodeGenWriteBarrier((&___m_subTextObjects_244), value);
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_245() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_isMaskingEnabled_245)); }
	inline bool get_m_isMaskingEnabled_245() const { return ___m_isMaskingEnabled_245; }
	inline bool* get_address_of_m_isMaskingEnabled_245() { return &___m_isMaskingEnabled_245; }
	inline void set_m_isMaskingEnabled_245(bool value)
	{
		___m_isMaskingEnabled_245 = value;
	}

	inline static int32_t get_offset_of_isMaskUpdateRequired_246() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___isMaskUpdateRequired_246)); }
	inline bool get_isMaskUpdateRequired_246() const { return ___isMaskUpdateRequired_246; }
	inline bool* get_address_of_isMaskUpdateRequired_246() { return &___isMaskUpdateRequired_246; }
	inline void set_isMaskUpdateRequired_246(bool value)
	{
		___isMaskUpdateRequired_246 = value;
	}

	inline static int32_t get_offset_of_m_maskType_247() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_maskType_247)); }
	inline int32_t get_m_maskType_247() const { return ___m_maskType_247; }
	inline int32_t* get_address_of_m_maskType_247() { return &___m_maskType_247; }
	inline void set_m_maskType_247(int32_t value)
	{
		___m_maskType_247 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_248() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_EnvMapMatrix_248)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_EnvMapMatrix_248() const { return ___m_EnvMapMatrix_248; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_EnvMapMatrix_248() { return &___m_EnvMapMatrix_248; }
	inline void set_m_EnvMapMatrix_248(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_EnvMapMatrix_248 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_249() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_RectTransformCorners_249)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_RectTransformCorners_249() const { return ___m_RectTransformCorners_249; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_RectTransformCorners_249() { return &___m_RectTransformCorners_249; }
	inline void set_m_RectTransformCorners_249(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_RectTransformCorners_249 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformCorners_249), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_250() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_isRegisteredForEvents_250)); }
	inline bool get_m_isRegisteredForEvents_250() const { return ___m_isRegisteredForEvents_250; }
	inline bool* get_address_of_m_isRegisteredForEvents_250() { return &___m_isRegisteredForEvents_250; }
	inline void set_m_isRegisteredForEvents_250(bool value)
	{
		___m_isRegisteredForEvents_250 = value;
	}

	inline static int32_t get_offset_of_loopCountA_251() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___loopCountA_251)); }
	inline int32_t get_loopCountA_251() const { return ___loopCountA_251; }
	inline int32_t* get_address_of_loopCountA_251() { return &___loopCountA_251; }
	inline void set_loopCountA_251(int32_t value)
	{
		___loopCountA_251 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPRO_T6FF60D9DCAF295045FE47C014CC855C5784752E2_H
#ifndef TEXTMESHPROUGUI_TBA60B913AB6151F8563F7078AD67EB6458129438_H
#define TEXTMESHPROUGUI_TBA60B913AB6151F8563F7078AD67EB6458129438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshProUGUI
struct  TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438  : public TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7
{
public:
	// System.Boolean TMPro.TextMeshProUGUI::m_isRebuildingLayout
	bool ___m_isRebuildingLayout_236;
	// System.Boolean TMPro.TextMeshProUGUI::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_237;
	// TMPro.TMP_SubMeshUI[] TMPro.TextMeshProUGUI::m_subTextObjects
	TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E* ___m_subTextObjects_238;
	// System.Single TMPro.TextMeshProUGUI::m_previousLossyScaleY
	float ___m_previousLossyScaleY_239;
	// UnityEngine.Vector3[] TMPro.TextMeshProUGUI::m_RectTransformCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_RectTransformCorners_240;
	// UnityEngine.CanvasRenderer TMPro.TextMeshProUGUI::m_canvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_canvasRenderer_241;
	// UnityEngine.Canvas TMPro.TextMeshProUGUI::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_242;
	// System.Boolean TMPro.TextMeshProUGUI::m_isFirstAllocation
	bool ___m_isFirstAllocation_243;
	// System.Int32 TMPro.TextMeshProUGUI::m_max_characters
	int32_t ___m_max_characters_244;
	// System.Boolean TMPro.TextMeshProUGUI::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_245;
	// UnityEngine.Material TMPro.TextMeshProUGUI::m_baseMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_baseMaterial_246;
	// System.Boolean TMPro.TextMeshProUGUI::m_isScrollRegionSet
	bool ___m_isScrollRegionSet_247;
	// System.Int32 TMPro.TextMeshProUGUI::m_stencilID
	int32_t ___m_stencilID_248;
	// UnityEngine.Vector4 TMPro.TextMeshProUGUI::m_maskOffset
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_maskOffset_249;
	// UnityEngine.Matrix4x4 TMPro.TextMeshProUGUI::m_EnvMapMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_EnvMapMatrix_250;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_251;
	// System.Int32 TMPro.TextMeshProUGUI::m_recursiveCountA
	int32_t ___m_recursiveCountA_252;
	// System.Int32 TMPro.TextMeshProUGUI::loopCountA
	int32_t ___loopCountA_253;

public:
	inline static int32_t get_offset_of_m_isRebuildingLayout_236() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isRebuildingLayout_236)); }
	inline bool get_m_isRebuildingLayout_236() const { return ___m_isRebuildingLayout_236; }
	inline bool* get_address_of_m_isRebuildingLayout_236() { return &___m_isRebuildingLayout_236; }
	inline void set_m_isRebuildingLayout_236(bool value)
	{
		___m_isRebuildingLayout_236 = value;
	}

	inline static int32_t get_offset_of_m_hasFontAssetChanged_237() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_hasFontAssetChanged_237)); }
	inline bool get_m_hasFontAssetChanged_237() const { return ___m_hasFontAssetChanged_237; }
	inline bool* get_address_of_m_hasFontAssetChanged_237() { return &___m_hasFontAssetChanged_237; }
	inline void set_m_hasFontAssetChanged_237(bool value)
	{
		___m_hasFontAssetChanged_237 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_238() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_subTextObjects_238)); }
	inline TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E* get_m_subTextObjects_238() const { return ___m_subTextObjects_238; }
	inline TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E** get_address_of_m_subTextObjects_238() { return &___m_subTextObjects_238; }
	inline void set_m_subTextObjects_238(TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E* value)
	{
		___m_subTextObjects_238 = value;
		Il2CppCodeGenWriteBarrier((&___m_subTextObjects_238), value);
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_239() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_previousLossyScaleY_239)); }
	inline float get_m_previousLossyScaleY_239() const { return ___m_previousLossyScaleY_239; }
	inline float* get_address_of_m_previousLossyScaleY_239() { return &___m_previousLossyScaleY_239; }
	inline void set_m_previousLossyScaleY_239(float value)
	{
		___m_previousLossyScaleY_239 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_240() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_RectTransformCorners_240)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_RectTransformCorners_240() const { return ___m_RectTransformCorners_240; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_RectTransformCorners_240() { return &___m_RectTransformCorners_240; }
	inline void set_m_RectTransformCorners_240(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_RectTransformCorners_240 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformCorners_240), value);
	}

	inline static int32_t get_offset_of_m_canvasRenderer_241() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_canvasRenderer_241)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_canvasRenderer_241() const { return ___m_canvasRenderer_241; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_canvasRenderer_241() { return &___m_canvasRenderer_241; }
	inline void set_m_canvasRenderer_241(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_canvasRenderer_241 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRenderer_241), value);
	}

	inline static int32_t get_offset_of_m_canvas_242() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_canvas_242)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_242() const { return ___m_canvas_242; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_242() { return &___m_canvas_242; }
	inline void set_m_canvas_242(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_242 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_242), value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_243() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isFirstAllocation_243)); }
	inline bool get_m_isFirstAllocation_243() const { return ___m_isFirstAllocation_243; }
	inline bool* get_address_of_m_isFirstAllocation_243() { return &___m_isFirstAllocation_243; }
	inline void set_m_isFirstAllocation_243(bool value)
	{
		___m_isFirstAllocation_243 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_244() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_max_characters_244)); }
	inline int32_t get_m_max_characters_244() const { return ___m_max_characters_244; }
	inline int32_t* get_address_of_m_max_characters_244() { return &___m_max_characters_244; }
	inline void set_m_max_characters_244(int32_t value)
	{
		___m_max_characters_244 = value;
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_245() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isMaskingEnabled_245)); }
	inline bool get_m_isMaskingEnabled_245() const { return ___m_isMaskingEnabled_245; }
	inline bool* get_address_of_m_isMaskingEnabled_245() { return &___m_isMaskingEnabled_245; }
	inline void set_m_isMaskingEnabled_245(bool value)
	{
		___m_isMaskingEnabled_245 = value;
	}

	inline static int32_t get_offset_of_m_baseMaterial_246() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_baseMaterial_246)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_baseMaterial_246() const { return ___m_baseMaterial_246; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_baseMaterial_246() { return &___m_baseMaterial_246; }
	inline void set_m_baseMaterial_246(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_baseMaterial_246 = value;
		Il2CppCodeGenWriteBarrier((&___m_baseMaterial_246), value);
	}

	inline static int32_t get_offset_of_m_isScrollRegionSet_247() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isScrollRegionSet_247)); }
	inline bool get_m_isScrollRegionSet_247() const { return ___m_isScrollRegionSet_247; }
	inline bool* get_address_of_m_isScrollRegionSet_247() { return &___m_isScrollRegionSet_247; }
	inline void set_m_isScrollRegionSet_247(bool value)
	{
		___m_isScrollRegionSet_247 = value;
	}

	inline static int32_t get_offset_of_m_stencilID_248() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_stencilID_248)); }
	inline int32_t get_m_stencilID_248() const { return ___m_stencilID_248; }
	inline int32_t* get_address_of_m_stencilID_248() { return &___m_stencilID_248; }
	inline void set_m_stencilID_248(int32_t value)
	{
		___m_stencilID_248 = value;
	}

	inline static int32_t get_offset_of_m_maskOffset_249() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_maskOffset_249)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_maskOffset_249() const { return ___m_maskOffset_249; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_maskOffset_249() { return &___m_maskOffset_249; }
	inline void set_m_maskOffset_249(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_maskOffset_249 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_250() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_EnvMapMatrix_250)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_EnvMapMatrix_250() const { return ___m_EnvMapMatrix_250; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_EnvMapMatrix_250() { return &___m_EnvMapMatrix_250; }
	inline void set_m_EnvMapMatrix_250(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_EnvMapMatrix_250 = value;
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_251() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isRegisteredForEvents_251)); }
	inline bool get_m_isRegisteredForEvents_251() const { return ___m_isRegisteredForEvents_251; }
	inline bool* get_address_of_m_isRegisteredForEvents_251() { return &___m_isRegisteredForEvents_251; }
	inline void set_m_isRegisteredForEvents_251(bool value)
	{
		___m_isRegisteredForEvents_251 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCountA_252() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_recursiveCountA_252)); }
	inline int32_t get_m_recursiveCountA_252() const { return ___m_recursiveCountA_252; }
	inline int32_t* get_address_of_m_recursiveCountA_252() { return &___m_recursiveCountA_252; }
	inline void set_m_recursiveCountA_252(int32_t value)
	{
		___m_recursiveCountA_252 = value;
	}

	inline static int32_t get_offset_of_loopCountA_253() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___loopCountA_253)); }
	inline int32_t get_loopCountA_253() const { return ___loopCountA_253; }
	inline int32_t* get_address_of_loopCountA_253() { return &___loopCountA_253; }
	inline void set_loopCountA_253(int32_t value)
	{
		___loopCountA_253 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROUGUI_TBA60B913AB6151F8563F7078AD67EB6458129438_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (AssetLoader_t5931C7BFC04DB6276B6493399CC257BEAA43F659), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD), -1, sizeof(AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3101[33] = 
{
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_RootNodeData_0(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_MaterialData_1(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_MeshData_2(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_AnimationData_3(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_CameraData_4(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_Metadata_5(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_NodesPath_6(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_LoadedMaterials_7(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_LoadedTextures_8(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_LoadedBoneNames_9(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_MeshDataConnections_10(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_EmbeddedTextures_11(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields::get_offset_of_FilesLoadData_12(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields::get_offset_of_StandardBaseMaterial_13(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields::get_offset_of_StandardSpecularMaterial_14(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields::get_offset_of_StandardBaseAlphaMaterial_15(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields::get_offset_of_StandardSpecularAlphaMaterial_16(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields::get_offset_of_StandardBaseCutoutMaterial_17(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields::get_offset_of_StandardSpecularCutoutMaterial_18(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD_StaticFields::get_offset_of_NotFoundTexture_19(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_NodeId_20(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_HasBoneInfo_21(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_HasBlendShapes_22(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_Scene_23(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_a_24(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_b_25(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_c_26(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_d_27(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_e_28(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_f_29(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_g_30(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_h_31(),
	AssetLoaderBase_t4276A226F958FD499EE6C37536BB1CFD76B403CD::get_offset_of_i_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (a_t750AC741A58D91B95FB211CF345C0A47DD74F8F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3102[1] = 
{
	a_t750AC741A58D91B95FB211CF345C0A47DD74F8F8::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (AssetLoaderAsync_t03A7B7BBBDC2EC8425B8FEE2D6A695A8156C1870), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[11] = 
{
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_a_0(),
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_b_1(),
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_c_2(),
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_d_3(),
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_e_4(),
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_f_5(),
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_g_6(),
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_h_7(),
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_i_8(),
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_j_9(),
	d_tA4DA584627EF7D094631EDD3B87BA78B22640BF4::get_offset_of_k_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (ObjectLoadedHandle_tE2DB0F8E41A86557CE40122A88339551B399D044), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (MeshCreatedHandle_t3353700F7B081BA1A5AF4C3FBA7A439FA0DAE0A3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (MaterialCreatedHandle_tCFCF9310D550A45C8C968E6E65FA3254E5395F78), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (AvatarCreatedHandle_t7B8714B52752D992F191D2D5E77CC71206196B80), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (AnimationClipCreatedHandle_t13B0579E3C7ABBF0AF164928743D86D52FA5016F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (MetadataProcessedHandle_tF83A79A9D6E56AD891A607A5DB625A0E564EE63E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (BlendShapeKeyCreatedHandle_tE42FF8D81D7B2A451D08F4C3BB6C2EF60741EB6B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (EmbeddedTextureLoadCallback_tD05D49054BB811D79C7297F03927D06DE9E8F3E7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (LoadTextureDataCallback_tE1AAEA1202DD3CFD69A6D9DB25200EC97FCAF1B3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[44] = 
{
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_AddAssetUnloader_4(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DontLoadAnimations_5(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DontApplyAnimations_6(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DontLoadLights_7(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DontLoadCameras_8(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_AutoPlayAnimations_9(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_AnimationWrapMode_10(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_UseLegacyAnimations_11(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_EnsureQuaternionContinuity_12(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_AnimatorController_13(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_Avatar_14(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DontGenerateAvatar_15(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DontLoadMetadata_16(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DontLoadMaterials_17(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyColorAlpha_18(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyDiffuseColor_19(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyEmissionColor_20(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplySpecularColor_21(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyDiffuseTexture_22(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyEmissionTexture_23(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplySpecularTexture_24(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyNormalTexture_25(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyDisplacementTexture_26(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyNormalScale_27(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyGlossiness_28(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyGlossinessScale_29(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DisableAlphaMaterials_30(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ApplyAlphaMaterials_31(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ScanForAlphaMaterials_32(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_UseCutoutMaterials_33(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_UseStandardSpecularMaterial_34(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DontLoadMeshes_35(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DontLoadBlendShapes_36(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_DontLoadSkinning_37(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_CombineMeshes_38(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_GenerateMeshColliders_39(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_ConvexMeshColliders_40(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_RotationAngles_41(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_Scale_42(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_PostProcessSteps_43(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_TextureCompression_44(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_TextureFilterMode_45(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_GenerateMipMaps_46(),
	AssetLoaderOptions_t75FF85F488FF730A40076444651BE4560F65E531::get_offset_of_AdvancedConfigs_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (AssetUnloader_tCB8D5D17907E58E4E5A84E1AF9AB169FAF711BF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (AssimpPostProcessSteps_t89C7A137C5A7EEA789392222AFF34DCD11F471AD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3116[28] = 
{
	AssimpPostProcessSteps_t89C7A137C5A7EEA789392222AFF34DCD11F471AD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (AssimpInterop_t355DF5D17B2144BD8587867267B3754AEEBD462C), -1, sizeof(AssimpInterop_t355DF5D17B2144BD8587867267B3754AEEBD462C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3117[2] = 
{
	AssimpInterop_t355DF5D17B2144BD8587867267B3754AEEBD462C_StaticFields::get_offset_of_d_0(),
	AssimpInterop_t355DF5D17B2144BD8587867267B3754AEEBD462C_StaticFields::get_offset_of_e_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (DataCallback_t0C125601C6F711EE7EBBFFC24B0A519E2243A0AF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (ExistsCallback_t8473DF0E166AE4F98314FBD5199893CA67012C2F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (ProgressCallback_t39D480CF4ABBF766B79A16905486E417FE40DBE6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (AnimationChannelData_tE060663D77BD68F727028B9145CF4BAC631D5DAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3121[2] = 
{
	AnimationChannelData_tE060663D77BD68F727028B9145CF4BAC631D5DAC::get_offset_of_NodeName_0(),
	AnimationChannelData_tE060663D77BD68F727028B9145CF4BAC631D5DAC::get_offset_of_CurveData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (AnimationCurveData_t5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[3] = 
{
	AnimationCurveData_t5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8::get_offset_of_Keyframes_0(),
	AnimationCurveData_t5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8::get_offset_of_a_1(),
	AnimationCurveData_t5C0478FE8AAA13514EA9F4F53730B6F5889CF0D8::get_offset_of_AnimationCurve_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[8] = 
{
	AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0::get_offset_of_Name_0(),
	AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0::get_offset_of_Legacy_1(),
	AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0::get_offset_of_Length_2(),
	AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0::get_offset_of_FrameRate_3(),
	AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0::get_offset_of_WrapMode_4(),
	AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0::get_offset_of_ChannelData_5(),
	AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0::get_offset_of_MorphData_6(),
	AnimationData_t8A65386E26DD73656D34EDAB19E37269F4B36FE0::get_offset_of_AnimationClip_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (AssimpMetadataType_tE81F99F30662BCAC75055C486BB4F3F68E7D988B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3124[8] = 
{
	AssimpMetadataType_tE81F99F30662BCAC75055C486BB4F3F68E7D988B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3125[4] = 
{
	AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD::get_offset_of_MetadataType_0(),
	AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD::get_offset_of_MetadataIndex_1(),
	AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD::get_offset_of_MetadataKey_2(),
	AssimpMetadata_t7FB97F81BF8BC57F8ADC1BDD9121D600966A98DD::get_offset_of_MetadataValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3126[9] = 
{
	CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8::get_offset_of_Name_0(),
	CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8::get_offset_of_Aspect_1(),
	CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8::get_offset_of_NearClipPlane_2(),
	CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8::get_offset_of_FarClipPlane_3(),
	CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8::get_offset_of_FieldOfView_4(),
	CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8::get_offset_of_LocalPosition_5(),
	CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8::get_offset_of_Forward_6(),
	CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8::get_offset_of_Up_7(),
	CameraData_tC06135C8B99D7A922FF22C8E9E138EF9CA426FF8::get_offset_of_Camera_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (DataDisposalCallback_t6963DE3A2EE2117382E3DDC8435453FC38426BFB), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3129[7] = 
{
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915::get_offset_of_Data_0(),
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915::get_offset_of_DataPointer_1(),
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915::get_offset_of_DataLength_2(),
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915::get_offset_of_OnDataDisposal_3(),
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915::get_offset_of_Width_4(),
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915::get_offset_of_Height_5(),
	EmbeddedTextureData_t256A14D2298773D48CE5F0225DCABD4ABD703915::get_offset_of_NumChannels_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (ZipGCFileLoadData_tBAFBC6955F8E973FA87C0DDFD15968EC80491E53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3130[1] = 
{
	ZipGCFileLoadData_tBAFBC6955F8E973FA87C0DDFD15968EC80491E53::get_offset_of_ZipFile_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3131[1] = 
{
	GCFileLoadData_t7A72633928546FD4AC5348CE9EDDE47AA7C7F62D::get_offset_of_LockedBuffers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3132[2] = 
{
	FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13::get_offset_of_Filename_0(),
	FileLoadData_tE9E3C8E550522C1F62FEF35FAFD0DE7B6B9DBB13::get_offset_of_BasePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (MaterialData_t27489063F272E033321F0C94EF062695C238CADA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[51] = 
{
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_Name_0(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_AlphaLoaded_1(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_Alpha_2(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_DiffuseInfoLoaded_3(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_DiffusePath_4(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_DiffuseWrapMode_5(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_DiffuseName_6(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_DiffuseBlendMode_7(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_DiffuseOp_8(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_DiffuseEmbeddedTextureData_9(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_DiffuseColorLoaded_10(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_DiffuseColor_11(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_EmissionInfoLoaded_12(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_EmissionPath_13(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_EmissionWrapMode_14(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_EmissionName_15(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_EmissionBlendMode_16(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_EmissionOp_17(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_EmissionEmbeddedTextureData_18(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_EmissionColorLoaded_19(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_EmissionColor_20(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_SpecularInfoLoaded_21(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_SpecularPath_22(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_SpecularWrapMode_23(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_SpecularName_24(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_SpecularBlendMode_25(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_SpecularOp_26(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_SpecularEmbeddedTextureData_27(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_SpecularColorLoaded_28(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_SpecularColor_29(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_NormalInfoLoaded_30(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_NormalPath_31(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_NormalWrapMode_32(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_NormalName_33(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_NormalBlendMode_34(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_NormalOp_35(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_NormalEmbeddedTextureData_36(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_HeightInfoLoaded_37(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_HeightPath_38(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_HeightWrapMode_39(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_HeightName_40(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_HeightBlendMode_41(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_HeightOp_42(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_HeightEmbeddedTextureData_43(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_BumpScaleLoaded_44(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_BumpScale_45(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_GlossinessLoaded_46(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_Glossiness_47(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_GlossMapScaleLoaded_48(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_GlossMapScale_49(),
	MaterialData_t27489063F272E033321F0C94EF062695C238CADA::get_offset_of_Material_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3134[19] = 
{
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Name_0(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_SubMeshName_1(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Vertices_2(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Normals_3(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Tangents_4(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_BiTangents_5(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Uv_6(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Uv1_7(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Uv2_8(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Uv3_9(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Colors_10(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Triangles_11(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_HasBoneInfo_12(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_BindPoses_13(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_BoneNames_14(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_BoneWeights_15(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_MaterialIndex_16(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_MorphsData_17(),
	MeshData_tA109105B40CB093BC17F6ED73E4D4BAF626AA537::get_offset_of_Mesh_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[2] = 
{
	MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4::get_offset_of_NodeName_0(),
	MorphChannelData_t6629EC80A43769190D8ED48A56093C6DD37C25E4::get_offset_of_MorphChannelKeys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[2] = 
{
	MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73::get_offset_of_Indices_0(),
	MorphChannelKey_tED235489B14590843F8C37E1381749ADCC625E73::get_offset_of_Weights_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[5] = 
{
	MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B::get_offset_of_Name_0(),
	MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B::get_offset_of_Vertices_1(),
	MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B::get_offset_of_Normals_2(),
	MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B::get_offset_of_Tangents_3(),
	MorphData_tE312C946C7103853AF92364F5B0DF1DD06A4AC8B::get_offset_of_Weight_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[7] = 
{
	NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C::get_offset_of_Name_0(),
	NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C::get_offset_of_Path_1(),
	NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C::get_offset_of_Matrix_2(),
	NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C::get_offset_of_Meshes_3(),
	NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C::get_offset_of_Parent_4(),
	NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C::get_offset_of_Children_5(),
	NodeData_tD2933CCEB90B2F77D752DB697E43E8C616DAC93C::get_offset_of_GameObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7), -1, sizeof(Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3139[4] = 
{
	Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields::get_offset_of_a_4(),
	Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields::get_offset_of_b_5(),
	Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields::get_offset_of_c_6(),
	Dispatcher_tB781C2F44E71A5E1249F3232459FDED4D76B39D7_StaticFields::get_offset_of_d_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (FileUtils_tC978494CEBA8E2DE23341446DB88A4DE75E0E5A1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (MatrixExtensions_t83E9B3470D05F4C6F10A66D1542EFD93E3502EEB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (StreamUtils_t6D2DC8042ED0F5EF8F5F4A9425CF18E945007FDC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (StringUtils_t138F1BEFB530B5E8D03690A7C3790BD6BD1905AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (TextureCompression_t25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3144[4] = 
{
	TextureCompression_t25667F6F8E09F2A8A8EBE5C93DFCE8C051A1E23C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (TextureLoadHandle_t29566CE04C014A3DB96B9B9DA2FE8DE700C6F09D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (TexturePreLoadHandle_tC0C5FE8AD70DD179BB0EE19495CDCCB7A0E0DD23), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (Texture2DUtils_t8347444C9FA2D57E80F3EB8ADDF7927DB25CAB6C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (ThreadUtils_tB30F54991AC35C1AF20C6917FB1653127F7109FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3149[2] = 
{
	a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C::get_offset_of_a_0(),
	a_t9752C01F959BCB0184461E930AD1BD0C6400DA0C::get_offset_of_b_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (b_tBB4B7EAE3F249876C28028467342545B6E691DD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3150[1] = 
{
	b_tBB4B7EAE3F249876C28028467342545B6E691DD5::get_offset_of_a_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (TransformExtensions_t0CA99D8D9FEAB9B39CA87EA9816DD70313658C42), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (U3CModuleU3E_tB181AC380D679257A9DFE61F653774B69E5DFF6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3153[2] = 
{
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB::get_offset_of_delegates_0(),
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB::get_offset_of_lookup_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3154[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3155[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC), -1, sizeof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3157[5] = 
{
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_StaticFields::get_offset_of_s_Instance_0(),
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC::get_offset_of_m_FontMaterialReferenceLookup_1(),
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC::get_offset_of_m_FontAssetReferenceLookup_2(),
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC::get_offset_of_m_SpriteAssetReferenceLookup_3(),
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC::get_offset_of_m_ColorGradientReferenceLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3158[9] = 
{
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_fontAsset_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_spriteAsset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_material_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_isDefaultMaterial_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_isFallbackMaterial_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_fallbackMaterial_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_padding_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_referenceCount_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (TextContainerAnchors_t8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3159[11] = 
{
	TextContainerAnchors_t8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE), -1, sizeof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3160[13] = 
{
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_hasChanged_4(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_pivot_5(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_anchorPosition_6(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_rect_7(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_isDefaultWidth_8(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_isDefaultHeight_9(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_isAutoFitting_10(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_corners_11(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_worldCorners_12(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_margins_13(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_rectTransform_14(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE_StaticFields::get_offset_of_k_defaultSize_15(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_textMeshPro_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[16] = 
{
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_currentAutoSizeMode_236(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_hasFontAssetChanged_237(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_previousLossyScaleY_238(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_renderer_239(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_meshFilter_240(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_isFirstAllocation_241(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_max_characters_242(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_max_numberOfLines_243(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_subTextObjects_244(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_isMaskingEnabled_245(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_isMaskUpdateRequired_246(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_maskType_247(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_EnvMapMatrix_248(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_RectTransformCorners_249(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_isRegisteredForEvents_250(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_loopCountA_251(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3162[18] = 
{
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_isRebuildingLayout_236(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_hasFontAssetChanged_237(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_subTextObjects_238(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_previousLossyScaleY_239(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_RectTransformCorners_240(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_canvasRenderer_241(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_canvas_242(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_isFirstAllocation_243(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_max_characters_244(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_isMaskingEnabled_245(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_baseMaterial_246(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_isScrollRegionSet_247(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_stencilID_248(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_maskOffset_249(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_EnvMapMatrix_250(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_isRegisteredForEvents_251(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_recursiveCountA_252(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_loopCountA_253(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3163[3] = 
{
	TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D::get_offset_of_hashCode_4(),
	TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D::get_offset_of_material_5(),
	TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D::get_offset_of_materialHashCode_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (TMP_Character_t1875AACA978396521498D6A699052C187903553D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0)+ sizeof (RuntimeObject), sizeof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3165[5] = 
{
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_uv2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_uv4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_color_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3166[36] = 
{
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_character_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_index_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_stringLength_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_elementType_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_textElement_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_fontAsset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_spriteAsset_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_spriteIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_material_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_materialReferenceIndex_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_isUsingAlternateTypeface_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_pointSize_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_lineNumber_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_pageNumber_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertexIndex_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_BL_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_TL_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_TR_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_BR_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_topLeft_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_bottomLeft_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_topRight_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_bottomRight_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_origin_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_ascender_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_baseLine_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_descender_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_xAdvance_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_aspectRatio_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_scale_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_color_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_underlineColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_strikethroughColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_highlightColor_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_style_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_isVisible_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3167[5] = 
{
	ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7), -1, sizeof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3168[7] = 
{
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7::get_offset_of_colorMode_4(),
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7::get_offset_of_topLeft_5(),
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7::get_offset_of_topRight_6(),
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7::get_offset_of_bottomLeft_7(),
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7::get_offset_of_bottomRight_8(),
	0,
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7_StaticFields::get_offset_of_k_DefaultColor_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (AtlasPopulationMode_t719D719A21DA39129F8EA982DF7BC7C344F6BC4D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3169[3] = 
{
	AtlasPopulationMode_t719D719A21DA39129F8EA982DF7BC7C344F6BC4D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C), -1, sizeof(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3170[43] = 
{
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_Version_7(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_SourceFontFileGUID_8(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_SourceFontFile_9(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_AtlasPopulationMode_10(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_FaceInfo_11(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_GlyphTable_12(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_GlyphLookupDictionary_13(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_CharacterTable_14(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_CharacterLookupDictionary_15(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_AtlasTexture_16(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_AtlasTextures_17(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_AtlasTextureIndex_18(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_UsedGlyphRects_19(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_FreeGlyphRects_20(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_fontInfo_21(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_atlas_22(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_AtlasWidth_23(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_AtlasHeight_24(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_AtlasPadding_25(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_AtlasRenderMode_26(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_glyphInfoList_27(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_KerningTable_28(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_FontFeatureTable_29(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_fallbackFontAssets_30(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_FallbackFontAssetTable_31(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_CreationSettings_32(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_FontWeightTable_33(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_fontWeights_34(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_normalStyle_35(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_normalSpacingOffset_36(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_boldStyle_37(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_boldSpacing_38(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_italicStyle_39(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_tabSize_40(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_oldTabSize_41(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_IsFontAssetLookupTablesDirty_42(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_GlyphsToPack_43(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_GlyphsPacked_44(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_GlyphsToRender_45(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_GlyphIndexList_46(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C::get_offset_of_m_CharactersToAdd_47(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C_StaticFields::get_offset_of_s_GlyphIndexArray_48(),
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C_StaticFields::get_offset_of_s_MissingCharacterList_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF), -1, sizeof(U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3171[3] = 
{
	U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_StaticFields::get_offset_of_U3CU3E9__100_0_1(),
	U3CU3Ec_t94D56C97965B637D7B3FBA7D9E4C04B85A013DFF_StaticFields::get_offset_of_U3CU3E9__101_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[21] = 
{
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_Name_0(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_PointSize_1(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_Scale_2(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_CharacterCount_3(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_LineHeight_4(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_Baseline_5(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_Ascender_6(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_CapHeight_7(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_Descender_8(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_CenterLine_9(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_SuperscriptOffset_10(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_SubscriptOffset_11(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_SubSize_12(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_Underline_13(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_UnderlineThickness_14(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_strikethrough_15(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_strikethroughThickness_16(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_TabWidth_17(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_Padding_18(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_AtlasWidth_19(),
	FaceInfo_Legacy_tA5B0942ED5875808552FE732238217F6CF70027E::get_offset_of_AtlasHeight_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4)+ sizeof (RuntimeObject), sizeof(FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3174[16] = 
{
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_sourceFontFileName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_sourceFontFileGUID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_pointSizeSamplingMode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_pointSize_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_padding_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_packingMode_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_atlasWidth_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_atlasHeight_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_characterSetSelectionMode_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_characterSequence_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_referencedFontAssetGUID_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_referencedTextAssetGUID_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_fontStyle_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_fontStyleModifier_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_renderMode_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_tC32D679F14894DDCE48E4C61ACC1D0FA1443E0A4::get_offset_of_includeFontFeatures_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (TMP_FontWeightPair_t14BB1EA6F16060838C5465F6BBB20C92ED79AEE3)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[2] = 
{
	TMP_FontWeightPair_t14BB1EA6F16060838C5465F6BBB20C92ED79AEE3::get_offset_of_regularTypeface_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontWeightPair_t14BB1EA6F16060838C5465F6BBB20C92ED79AEE3::get_offset_of_italicTypeface_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A)+ sizeof (RuntimeObject), sizeof(GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A ), 0, 0 };
extern const int32_t g_FieldOffsetTable3176[4] = 
{
	GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A::get_offset_of_xPlacement_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A::get_offset_of_yPlacement_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A::get_offset_of_xAdvance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_Legacy_t775B262F8408BFA72A5736B93D15EB6D633BC84A::get_offset_of_yAdvance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD), -1, sizeof(KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3177[7] = 
{
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_m_FirstGlyph_0(),
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_m_FirstGlyphAdjustments_1(),
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_m_SecondGlyph_2(),
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_m_SecondGlyphAdjustments_3(),
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_xOffset_4(),
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD_StaticFields::get_offset_of_empty_5(),
	KerningPair_t4FCF2540D9DBE27046ED4A8643B7AA644ED0DAAD::get_offset_of_m_IgnoreSpacingAdjustments_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3178[1] = 
{
	KerningTable_tAF8D2AABDC878598EFE90D838BAAD285FA8CE05F::get_offset_of_kerningPairs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0), -1, sizeof(TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3179[2] = 
{
	TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0_StaticFields::get_offset_of_s_Instance_0(),
	TMP_FontAssetUtilities_t5B217899A57BD221ED25A93A8E2CB2039D0EABA0_StaticFields::get_offset_of_k_SearchedFontAssets_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (FontFeatureLookupFlags_t5E2AC8F0E11557FFBDC03A81EA2ECD8B82C17D8D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3180[3] = 
{
	FontFeatureLookupFlags_t5E2AC8F0E11557FFBDC03A81EA2ECD8B82C17D8D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8)+ sizeof (RuntimeObject), sizeof(TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3181[4] = 
{
	TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8::get_offset_of_m_XPlacement_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8::get_offset_of_m_YPlacement_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8::get_offset_of_m_XAdvance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_GlyphValueRecord_t78C803E430E95C128540C14391CBACF833943BD8::get_offset_of_m_YAdvance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58)+ sizeof (RuntimeObject), sizeof(TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3182[2] = 
{
	TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58::get_offset_of_m_GlyphIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_GlyphAdjustmentRecord_t515A636DEA8D632C08D0B01A9AC1F962F0291E58::get_offset_of_m_GlyphValueRecord_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (TMP_GlyphPairAdjustmentRecord_tEF0669284CC50EEFC3EE68A7BC378F285EAD7B76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[3] = 
{
	TMP_GlyphPairAdjustmentRecord_tEF0669284CC50EEFC3EE68A7BC378F285EAD7B76::get_offset_of_m_FirstAdjustmentRecord_0(),
	TMP_GlyphPairAdjustmentRecord_tEF0669284CC50EEFC3EE68A7BC378F285EAD7B76::get_offset_of_m_SecondAdjustmentRecord_1(),
	TMP_GlyphPairAdjustmentRecord_tEF0669284CC50EEFC3EE68A7BC378F285EAD7B76::get_offset_of_m_FeatureLookupFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (GlyphPairKey_tBED040784D7FD7F268CCCCC24844C2D723D936F6)+ sizeof (RuntimeObject), sizeof(GlyphPairKey_tBED040784D7FD7F268CCCCC24844C2D723D936F6 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3184[3] = 
{
	GlyphPairKey_tBED040784D7FD7F268CCCCC24844C2D723D936F6::get_offset_of_firstGlyphIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphPairKey_tBED040784D7FD7F268CCCCC24844C2D723D936F6::get_offset_of_secondGlyphIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphPairKey_tBED040784D7FD7F268CCCCC24844C2D723D936F6::get_offset_of_key_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3185[2] = 
{
	TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756::get_offset_of_m_GlyphPairAdjustmentRecords_0(),
	TMP_FontFeatureTable_t6F3402916A5D81F2C4180CA75E04DB7A6F950756::get_offset_of_m_GlyphPairAdjustmentRecordLookupDictionary_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442)+ sizeof (RuntimeObject), sizeof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3186[20] = 
{
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_controlCharacterCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_characterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_visibleCharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_spaceCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_wordCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_lastVisibleCharacterIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_length_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_lineHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_ascender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_baseline_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_descender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_maxAdvance_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_width_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_marginLeft_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_marginRight_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_alignment_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442::get_offset_of_lineExtents_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3187[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3188[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336), -1, sizeof(TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3189[5] = 
{
	TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields::get_offset_of_m_materialList_0(),
	TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields::get_offset_of_m_fallbackMaterials_1(),
	TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields::get_offset_of_m_fallbackMaterialLookup_2(),
	TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields::get_offset_of_m_fallbackCleanupList_3(),
	TMP_MaterialManager_t7BAB3C3D85A0B0532A12D1C11F6B28413EE8E336_StaticFields::get_offset_of_isFallbackListDirty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3190[5] = 
{
	FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A::get_offset_of_baseID_0(),
	FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A::get_offset_of_baseMaterial_1(),
	FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A::get_offset_of_fallbackID_2(),
	FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A::get_offset_of_fallbackMaterial_3(),
	FallbackMaterial_t538C842FD3863FAF785036939034732F56B2473A::get_offset_of_count_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (MaskingMaterial_tD567961933B31276005075026B5BA552CF42F30B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3191[4] = 
{
	MaskingMaterial_tD567961933B31276005075026B5BA552CF42F30B::get_offset_of_baseMaterial_0(),
	MaskingMaterial_tD567961933B31276005075026B5BA552CF42F30B::get_offset_of_stencilMaterial_1(),
	MaskingMaterial_tD567961933B31276005075026B5BA552CF42F30B::get_offset_of_count_2(),
	MaskingMaterial_tD567961933B31276005075026B5BA552CF42F30B::get_offset_of_stencilID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3192[3] = 
{
	VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E)+ sizeof (RuntimeObject), -1, sizeof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3193[13] = 
{
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields::get_offset_of_s_DefaultColor_0(),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields::get_offset_of_s_DefaultNormal_1(),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields::get_offset_of_s_DefaultTangent_2(),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields::get_offset_of_s_DefaultBounds_3(),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E::get_offset_of_mesh_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E::get_offset_of_vertexCount_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E::get_offset_of_vertices_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E::get_offset_of_normals_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E::get_offset_of_tangents_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E::get_offset_of_uvs0_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E::get_offset_of_uvs2_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E::get_offset_of_colors32_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E::get_offset_of_triangles_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3194[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (TagValueType_tB0AE4FE83F0293DDD337886C8D5268947F25F5CC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3195[5] = 
{
	TagValueType_tB0AE4FE83F0293DDD337886C8D5268947F25F5CC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (TagUnitType_t5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3196[4] = 
{
	TagUnitType_t5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84)+ sizeof (RuntimeObject), sizeof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3197[10] = 
{
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84::get_offset_of_bold_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84::get_offset_of_italic_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84::get_offset_of_underline_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84::get_offset_of_strikethrough_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84::get_offset_of_highlight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84::get_offset_of_superscript_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84::get_offset_of_subscript_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84::get_offset_of_uppercase_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84::get_offset_of_lowercase_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84::get_offset_of_smallcaps_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3198[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A), -1, sizeof(TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3199[28] = 
{
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A_StaticFields::get_offset_of_s_Instance_4(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_enableWordWrapping_5(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_enableKerning_6(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_enableExtraPadding_7(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_enableTintAllSprites_8(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_enableParseEscapeCharacters_9(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_EnableRaycastTarget_10(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_GetFontFeaturesAtRuntime_11(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_missingGlyphCharacter_12(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_warningsDisabled_13(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultFontAsset_14(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultFontAssetPath_15(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultFontSize_16(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultAutoSizeMinRatio_17(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultAutoSizeMaxRatio_18(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultTextMeshProTextContainerSize_19(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultTextMeshProUITextContainerSize_20(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_autoSizeTextContainer_21(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_fallbackFontAssets_22(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_matchMaterialPreset_23(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultSpriteAsset_24(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultSpriteAssetPath_25(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultColorGradientPresetsPath_26(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_enableEmojiSupport_27(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_defaultStyleSheet_28(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_leadingCharacters_29(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_followingCharacters_30(),
	TMP_Settings_t1CCF2DFCF66223CC1AC404F9AEE3E257BA37255A::get_offset_of_m_linebreakingRules_31(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
