﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessVolume>>
struct Dictionary_2_t072E47B207A6F623AD23618E1104ABFD6DC928FF;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.RenderTextureFormat>
struct Dictionary_2_t1378DEC1682A37F31C0E17122CBF0437EFF78110;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Texture2D>
struct Dictionary_2_t992D19043FB7E6168A78D006336B88ACEDAC2D50;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessAttribute>
struct Dictionary_2_tD9C3253F98C5ECFD23180A7D57335280EDFE50C5;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessBundle>
struct Dictionary_2_t4EC298F4DA29313372D00BECC420F066420CDDC6;
// System.Collections.Generic.Dictionary`2<UnityEngine.Rendering.PostProcessing.MonitorType,UnityEngine.Rendering.PostProcessing.Monitor>
struct Dictionary_2_t6F5E43428F07CBA0DAA38F4FFD419595B52239E4;
// System.Collections.Generic.Dictionary`2<UnityEngine.Rendering.PostProcessing.PostProcessEvent,System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef>>
struct Dictionary_2_t9494DB774A1ACFCD524B3447BB1A97E3D97CFE76;
// System.Collections.Generic.Dictionary`2<UnityEngine.Shader,UnityEngine.Rendering.PostProcessing.PropertySheet>
struct Dictionary_2_tB620A307AE6B4F88DE0FD47BFDCAE45768F3B478;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_tF9225691990EF9799D9F4B64E4063CA0D1DF03CA;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_t34F3153FFE29D834C770DB7FD47483EE5175933C;
// System.Collections.Generic.List`1<UnityEngine.RenderTexture>
struct List_1_tE7D42879B6DB15943660C83DE3C7C11F23A535C6;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessBundle>
struct List_1_tD1CDEEC5721FB71CC260F433093C17D4D3A8377B;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer>
struct List_1_t939524230874DF71A2B82A3F03291BA2261A7180;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings>
struct List_1_t1F47AA13007605005DB7DD4BBCBD45CFCD05BD91;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer/SerializedBundleRef>
struct List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessVolume>
struct List_1_tBD4C46708D829BA47D48F8D990DABBD64D4B540B;
// System.Collections.Generic.List`1<UnityEngine.Rendering.RenderTargetIdentifier>
struct List_1_t3D014466D866220BEE24A92AE5BB33E306B8E2B7;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.PostProcessing.ParameterOverride>
struct ReadOnlyCollection_1_t2D7596D677CC341719EF47DD23F94617FB662DED;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessBundle>,UnityEngine.Rendering.PostProcessing.PostProcessBundle>
struct Func_2_t22847FA433824D82F968976B22D8707ED0A87754;
// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>>
struct Func_2_t8F89995E22C5EB8E236C651EC47EB541468BF297;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t56D7C745FF6EF4A902AAB44EAFC9F43DC7DF0CDD;
// System.Func`2<System.Reflection.FieldInfo,System.Int32>
struct Func_2_t5A22B0C32E2088807584847339810C3893D8EDAE;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Func`3<UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.Matrix4x4>
struct Func_3_t8F15226E3E5D0910B0330699065D10FE301CEA92;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Predicate`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings>
struct Predicate_1_t99D5EC69E65968D9A114AE4952484C236F61E507;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5;
// UnityEngine.ComputeShader
struct ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.RenderTexture[][]
struct RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD;
// UnityEngine.Rendering.PostProcessing.AmbientOcclusion
struct AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B;
// UnityEngine.Rendering.PostProcessing.AutoExposure
struct AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A;
// UnityEngine.Rendering.PostProcessing.BoolParameter
struct BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE;
// UnityEngine.Rendering.PostProcessing.ColorParameter
struct ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B;
// UnityEngine.Rendering.PostProcessing.Dithering
struct Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0;
// UnityEngine.Rendering.PostProcessing.FastApproximateAntialiasing
struct FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D;
// UnityEngine.Rendering.PostProcessing.FloatParameter
struct FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F;
// UnityEngine.Rendering.PostProcessing.Fog
struct Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77;
// UnityEngine.Rendering.PostProcessing.HableCurve
struct HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D;
// UnityEngine.Rendering.PostProcessing.HableCurve/Segment[]
struct SegmentU5BU5D_t8A9F480BCAE31B5C787357C4DD71507ED2DF0A27;
// UnityEngine.Rendering.PostProcessing.HableCurve/Uniforms
struct Uniforms_tA0574F53210B6F8C0A398C19739AFC0FD2A61CE4;
// UnityEngine.Rendering.PostProcessing.HistogramMonitor
struct HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2;
// UnityEngine.Rendering.PostProcessing.IntParameter
struct IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC;
// UnityEngine.Rendering.PostProcessing.LensDistortion
struct LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F;
// UnityEngine.Rendering.PostProcessing.LightMeterMonitor
struct LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075;
// UnityEngine.Rendering.PostProcessing.LogHistogram
struct LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79;
// UnityEngine.Rendering.PostProcessing.MotionBlur
struct MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165;
// UnityEngine.Rendering.PostProcessing.PostProcessAttribute
struct PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748;
// UnityEngine.Rendering.PostProcessing.PostProcessBundle
struct PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225;
// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer
struct PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39;
// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer/OverlaySettings
struct OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6;
// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer
struct PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F;
// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings
struct PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042;
// UnityEngine.Rendering.PostProcessing.PostProcessProfile
struct PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E;
// UnityEngine.Rendering.PostProcessing.PostProcessRenderContext
struct PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB;
// UnityEngine.Rendering.PostProcessing.PostProcessResources
struct PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B;
// UnityEngine.Rendering.PostProcessing.PostProcessResources/ComputeShaders
struct ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457;
// UnityEngine.Rendering.PostProcessing.PostProcessResources/SMAALuts
struct SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090;
// UnityEngine.Rendering.PostProcessing.PostProcessResources/Shaders
struct Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF;
// UnityEngine.Rendering.PostProcessing.PropertySheet
struct PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735;
// UnityEngine.Rendering.PostProcessing.PropertySheetFactory
struct PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4;
// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPresetParameter
struct ScreenSpaceReflectionPresetParameter_tD789E7238F9338AC7EEBC50D75C89648C9FF7778;
// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolutionParameter
struct ScreenSpaceReflectionResolutionParameter_tE0C3A2C876211C74352F5968C45AD1C60C666F50;
// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections
struct ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325;
// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer/QualityPreset[]
struct QualityPresetU5BU5D_t3536A15EBC22E6FE24F834B7CE55A618A0F592A2;
// UnityEngine.Rendering.PostProcessing.Spline
struct Spline_t42213D550262BA4C9B574C062AB52021A970081F;
// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing
struct SubpixelMorphologicalAntialiasing_tDE9497AEBB401059AB2F21890704B15C374B79DD;
// UnityEngine.Rendering.PostProcessing.TargetPool
struct TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949;
// UnityEngine.Rendering.PostProcessing.TemporalAntialiasing
struct TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1;
// UnityEngine.Rendering.PostProcessing.TextureParameter
struct TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87;
// UnityEngine.Rendering.PostProcessing.Vector2Parameter
struct Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2;
// UnityEngine.Rendering.PostProcessing.VectorscopeMonitor
struct VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B;
// UnityEngine.Rendering.PostProcessing.Vignette
struct Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31;
// UnityEngine.Rendering.PostProcessing.VignetteModeParameter
struct VignetteModeParameter_t11A319B818611259008B12060E0263B287D90C14;
// UnityEngine.Rendering.PostProcessing.WaveformMonitor
struct WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6;
// UnityEngine.Rendering.RenderTargetIdentifier[]
struct RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4;
// Vuforia.UnityCompiled.IUnityAndroidPermissions
struct IUnityAndroidPermissions_tC63F0B0237AED0CFC9589B54D40A234FF082562D;
// Vuforia.UnityCompiled.IUnityCompiledFacade
struct IUnityCompiledFacade_t0E3A7AE8F2EFBD9E4E214CB598761DDD267E70BE;
// Vuforia.UnityCompiled.IUnityRenderPipeline
struct IUnityRenderPipeline_tAD5FDC16A735AA177793B835843069369918FC34;




#ifndef U3CMODULEU3E_TBB5D6FDE6CBE0067E5DC774822D5054F01745F8C_H
#define U3CMODULEU3E_TBB5D6FDE6CBE0067E5DC774822D5054F01745F8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tBB5D6FDE6CBE0067E5DC774822D5054F01745F8C 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TBB5D6FDE6CBE0067E5DC774822D5054F01745F8C_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef COLORUTILITIES_T6DCFF299E256E07788F733F928088AD3378F956B_H
#define COLORUTILITIES_T6DCFF299E256E07788F733F928088AD3378F956B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorUtilities
struct  ColorUtilities_t6DCFF299E256E07788F733F928088AD3378F956B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORUTILITIES_T6DCFF299E256E07788F733F928088AD3378F956B_H
#ifndef HABLECURVE_T15B12405420560FC90A4FD852C2C2AF5F6CF491D_H
#define HABLECURVE_T15B12405420560FC90A4FD852C2C2AF5F6CF491D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HableCurve
struct  HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D  : public RuntimeObject
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve::<whitePoint>k__BackingField
	float ___U3CwhitePointU3Ek__BackingField_0;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve::<inverseWhitePoint>k__BackingField
	float ___U3CinverseWhitePointU3Ek__BackingField_1;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve::<x0>k__BackingField
	float ___U3Cx0U3Ek__BackingField_2;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve::<x1>k__BackingField
	float ___U3Cx1U3Ek__BackingField_3;
	// UnityEngine.Rendering.PostProcessing.HableCurve_Segment[] UnityEngine.Rendering.PostProcessing.HableCurve::m_Segments
	SegmentU5BU5D_t8A9F480BCAE31B5C787357C4DD71507ED2DF0A27* ___m_Segments_4;
	// UnityEngine.Rendering.PostProcessing.HableCurve_Uniforms UnityEngine.Rendering.PostProcessing.HableCurve::uniforms
	Uniforms_tA0574F53210B6F8C0A398C19739AFC0FD2A61CE4 * ___uniforms_5;

public:
	inline static int32_t get_offset_of_U3CwhitePointU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D, ___U3CwhitePointU3Ek__BackingField_0)); }
	inline float get_U3CwhitePointU3Ek__BackingField_0() const { return ___U3CwhitePointU3Ek__BackingField_0; }
	inline float* get_address_of_U3CwhitePointU3Ek__BackingField_0() { return &___U3CwhitePointU3Ek__BackingField_0; }
	inline void set_U3CwhitePointU3Ek__BackingField_0(float value)
	{
		___U3CwhitePointU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CinverseWhitePointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D, ___U3CinverseWhitePointU3Ek__BackingField_1)); }
	inline float get_U3CinverseWhitePointU3Ek__BackingField_1() const { return ___U3CinverseWhitePointU3Ek__BackingField_1; }
	inline float* get_address_of_U3CinverseWhitePointU3Ek__BackingField_1() { return &___U3CinverseWhitePointU3Ek__BackingField_1; }
	inline void set_U3CinverseWhitePointU3Ek__BackingField_1(float value)
	{
		___U3CinverseWhitePointU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cx0U3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D, ___U3Cx0U3Ek__BackingField_2)); }
	inline float get_U3Cx0U3Ek__BackingField_2() const { return ___U3Cx0U3Ek__BackingField_2; }
	inline float* get_address_of_U3Cx0U3Ek__BackingField_2() { return &___U3Cx0U3Ek__BackingField_2; }
	inline void set_U3Cx0U3Ek__BackingField_2(float value)
	{
		___U3Cx0U3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Cx1U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D, ___U3Cx1U3Ek__BackingField_3)); }
	inline float get_U3Cx1U3Ek__BackingField_3() const { return ___U3Cx1U3Ek__BackingField_3; }
	inline float* get_address_of_U3Cx1U3Ek__BackingField_3() { return &___U3Cx1U3Ek__BackingField_3; }
	inline void set_U3Cx1U3Ek__BackingField_3(float value)
	{
		___U3Cx1U3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_Segments_4() { return static_cast<int32_t>(offsetof(HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D, ___m_Segments_4)); }
	inline SegmentU5BU5D_t8A9F480BCAE31B5C787357C4DD71507ED2DF0A27* get_m_Segments_4() const { return ___m_Segments_4; }
	inline SegmentU5BU5D_t8A9F480BCAE31B5C787357C4DD71507ED2DF0A27** get_address_of_m_Segments_4() { return &___m_Segments_4; }
	inline void set_m_Segments_4(SegmentU5BU5D_t8A9F480BCAE31B5C787357C4DD71507ED2DF0A27* value)
	{
		___m_Segments_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Segments_4), value);
	}

	inline static int32_t get_offset_of_uniforms_5() { return static_cast<int32_t>(offsetof(HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D, ___uniforms_5)); }
	inline Uniforms_tA0574F53210B6F8C0A398C19739AFC0FD2A61CE4 * get_uniforms_5() const { return ___uniforms_5; }
	inline Uniforms_tA0574F53210B6F8C0A398C19739AFC0FD2A61CE4 ** get_address_of_uniforms_5() { return &___uniforms_5; }
	inline void set_uniforms_5(Uniforms_tA0574F53210B6F8C0A398C19739AFC0FD2A61CE4 * value)
	{
		___uniforms_5 = value;
		Il2CppCodeGenWriteBarrier((&___uniforms_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HABLECURVE_T15B12405420560FC90A4FD852C2C2AF5F6CF491D_H
#ifndef SEGMENT_TA16884985B21D43ECD1DD8792FA789BD0C64411C_H
#define SEGMENT_TA16884985B21D43ECD1DD8792FA789BD0C64411C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HableCurve_Segment
struct  Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C  : public RuntimeObject
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_Segment::offsetX
	float ___offsetX_0;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_Segment::offsetY
	float ___offsetY_1;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_Segment::scaleX
	float ___scaleX_2;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_Segment::scaleY
	float ___scaleY_3;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_Segment::lnA
	float ___lnA_4;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_Segment::B
	float ___B_5;

public:
	inline static int32_t get_offset_of_offsetX_0() { return static_cast<int32_t>(offsetof(Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C, ___offsetX_0)); }
	inline float get_offsetX_0() const { return ___offsetX_0; }
	inline float* get_address_of_offsetX_0() { return &___offsetX_0; }
	inline void set_offsetX_0(float value)
	{
		___offsetX_0 = value;
	}

	inline static int32_t get_offset_of_offsetY_1() { return static_cast<int32_t>(offsetof(Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C, ___offsetY_1)); }
	inline float get_offsetY_1() const { return ___offsetY_1; }
	inline float* get_address_of_offsetY_1() { return &___offsetY_1; }
	inline void set_offsetY_1(float value)
	{
		___offsetY_1 = value;
	}

	inline static int32_t get_offset_of_scaleX_2() { return static_cast<int32_t>(offsetof(Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C, ___scaleX_2)); }
	inline float get_scaleX_2() const { return ___scaleX_2; }
	inline float* get_address_of_scaleX_2() { return &___scaleX_2; }
	inline void set_scaleX_2(float value)
	{
		___scaleX_2 = value;
	}

	inline static int32_t get_offset_of_scaleY_3() { return static_cast<int32_t>(offsetof(Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C, ___scaleY_3)); }
	inline float get_scaleY_3() const { return ___scaleY_3; }
	inline float* get_address_of_scaleY_3() { return &___scaleY_3; }
	inline void set_scaleY_3(float value)
	{
		___scaleY_3 = value;
	}

	inline static int32_t get_offset_of_lnA_4() { return static_cast<int32_t>(offsetof(Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C, ___lnA_4)); }
	inline float get_lnA_4() const { return ___lnA_4; }
	inline float* get_address_of_lnA_4() { return &___lnA_4; }
	inline void set_lnA_4(float value)
	{
		___lnA_4 = value;
	}

	inline static int32_t get_offset_of_B_5() { return static_cast<int32_t>(offsetof(Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C, ___B_5)); }
	inline float get_B_5() const { return ___B_5; }
	inline float* get_address_of_B_5() { return &___B_5; }
	inline void set_B_5(float value)
	{
		___B_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENT_TA16884985B21D43ECD1DD8792FA789BD0C64411C_H
#ifndef UNIFORMS_TA0574F53210B6F8C0A398C19739AFC0FD2A61CE4_H
#define UNIFORMS_TA0574F53210B6F8C0A398C19739AFC0FD2A61CE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HableCurve_Uniforms
struct  Uniforms_tA0574F53210B6F8C0A398C19739AFC0FD2A61CE4  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.HableCurve UnityEngine.Rendering.PostProcessing.HableCurve_Uniforms::parent
	HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Uniforms_tA0574F53210B6F8C0A398C19739AFC0FD2A61CE4, ___parent_0)); }
	inline HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D * get_parent_0() const { return ___parent_0; }
	inline HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_TA0574F53210B6F8C0A398C19739AFC0FD2A61CE4_H
#ifndef HALTONSEQ_T1A3CE446490E66FFA4ED3809DE5C2CB78983EDC6_H
#define HALTONSEQ_T1A3CE446490E66FFA4ED3809DE5C2CB78983EDC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HaltonSeq
struct  HaltonSeq_t1A3CE446490E66FFA4ED3809DE5C2CB78983EDC6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HALTONSEQ_T1A3CE446490E66FFA4ED3809DE5C2CB78983EDC6_H
#ifndef LOGHISTOGRAM_T25F6613321C3909DA2D9A9BB3AC78AC84447DF79_H
#define LOGHISTOGRAM_T25F6613321C3909DA2D9A9BB3AC78AC84447DF79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.LogHistogram
struct  LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79  : public RuntimeObject
{
public:
	// UnityEngine.ComputeBuffer UnityEngine.Rendering.PostProcessing.LogHistogram::<data>k__BackingField
	ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * ___U3CdataU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CdataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79, ___U3CdataU3Ek__BackingField_3)); }
	inline ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * get_U3CdataU3Ek__BackingField_3() const { return ___U3CdataU3Ek__BackingField_3; }
	inline ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 ** get_address_of_U3CdataU3Ek__BackingField_3() { return &___U3CdataU3Ek__BackingField_3; }
	inline void set_U3CdataU3Ek__BackingField_3(ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * value)
	{
		___U3CdataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGHISTOGRAM_T25F6613321C3909DA2D9A9BB3AC78AC84447DF79_H
#ifndef MONITOR_T9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA_H
#define MONITOR_T9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Monitor
struct  Monitor_t9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA  : public RuntimeObject
{
public:
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.Monitor::<output>k__BackingField
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___U3CoutputU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Rendering.PostProcessing.Monitor::requested
	bool ___requested_1;

public:
	inline static int32_t get_offset_of_U3CoutputU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Monitor_t9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA, ___U3CoutputU3Ek__BackingField_0)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_U3CoutputU3Ek__BackingField_0() const { return ___U3CoutputU3Ek__BackingField_0; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_U3CoutputU3Ek__BackingField_0() { return &___U3CoutputU3Ek__BackingField_0; }
	inline void set_U3CoutputU3Ek__BackingField_0(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___U3CoutputU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_requested_1() { return static_cast<int32_t>(offsetof(Monitor_t9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA, ___requested_1)); }
	inline bool get_requested_1() const { return ___requested_1; }
	inline bool* get_address_of_requested_1() { return &___requested_1; }
	inline void set_requested_1(bool value)
	{
		___requested_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONITOR_T9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA_H
#ifndef MULTISCALEVO_T916332C3C084DF662B2DEE0376083133C7BAB2C6_H
#define MULTISCALEVO_T916332C3C084DF662B2DEE0376083133C7BAB2C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MultiScaleVO
struct  MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6  : public RuntimeObject
{
public:
	// System.Single[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_SampleThickness
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_SampleThickness_0;
	// System.Single[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_InvThicknessTable
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_InvThicknessTable_1;
	// System.Single[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_SampleWeightTable
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_SampleWeightTable_2;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_Widths
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_Widths_3;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_Heights
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_Heights_4;
	// UnityEngine.Rendering.PostProcessing.AmbientOcclusion UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_Settings
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B * ___m_Settings_5;
	// UnityEngine.Rendering.PostProcessing.PropertySheet UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_PropertySheet
	PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * ___m_PropertySheet_6;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_Resources
	PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * ___m_Resources_7;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_AmbientOnlyAO
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___m_AmbientOnlyAO_8;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.Rendering.PostProcessing.MultiScaleVO::m_MRT
	RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4* ___m_MRT_9;

public:
	inline static int32_t get_offset_of_m_SampleThickness_0() { return static_cast<int32_t>(offsetof(MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6, ___m_SampleThickness_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_SampleThickness_0() const { return ___m_SampleThickness_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_SampleThickness_0() { return &___m_SampleThickness_0; }
	inline void set_m_SampleThickness_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_SampleThickness_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_SampleThickness_0), value);
	}

	inline static int32_t get_offset_of_m_InvThicknessTable_1() { return static_cast<int32_t>(offsetof(MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6, ___m_InvThicknessTable_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_InvThicknessTable_1() const { return ___m_InvThicknessTable_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_InvThicknessTable_1() { return &___m_InvThicknessTable_1; }
	inline void set_m_InvThicknessTable_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_InvThicknessTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvThicknessTable_1), value);
	}

	inline static int32_t get_offset_of_m_SampleWeightTable_2() { return static_cast<int32_t>(offsetof(MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6, ___m_SampleWeightTable_2)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_SampleWeightTable_2() const { return ___m_SampleWeightTable_2; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_SampleWeightTable_2() { return &___m_SampleWeightTable_2; }
	inline void set_m_SampleWeightTable_2(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_SampleWeightTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SampleWeightTable_2), value);
	}

	inline static int32_t get_offset_of_m_Widths_3() { return static_cast<int32_t>(offsetof(MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6, ___m_Widths_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_Widths_3() const { return ___m_Widths_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_Widths_3() { return &___m_Widths_3; }
	inline void set_m_Widths_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_Widths_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Widths_3), value);
	}

	inline static int32_t get_offset_of_m_Heights_4() { return static_cast<int32_t>(offsetof(MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6, ___m_Heights_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_Heights_4() const { return ___m_Heights_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_Heights_4() { return &___m_Heights_4; }
	inline void set_m_Heights_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_Heights_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Heights_4), value);
	}

	inline static int32_t get_offset_of_m_Settings_5() { return static_cast<int32_t>(offsetof(MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6, ___m_Settings_5)); }
	inline AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B * get_m_Settings_5() const { return ___m_Settings_5; }
	inline AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B ** get_address_of_m_Settings_5() { return &___m_Settings_5; }
	inline void set_m_Settings_5(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B * value)
	{
		___m_Settings_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_5), value);
	}

	inline static int32_t get_offset_of_m_PropertySheet_6() { return static_cast<int32_t>(offsetof(MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6, ___m_PropertySheet_6)); }
	inline PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * get_m_PropertySheet_6() const { return ___m_PropertySheet_6; }
	inline PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 ** get_address_of_m_PropertySheet_6() { return &___m_PropertySheet_6; }
	inline void set_m_PropertySheet_6(PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * value)
	{
		___m_PropertySheet_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropertySheet_6), value);
	}

	inline static int32_t get_offset_of_m_Resources_7() { return static_cast<int32_t>(offsetof(MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6, ___m_Resources_7)); }
	inline PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * get_m_Resources_7() const { return ___m_Resources_7; }
	inline PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B ** get_address_of_m_Resources_7() { return &___m_Resources_7; }
	inline void set_m_Resources_7(PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * value)
	{
		___m_Resources_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Resources_7), value);
	}

	inline static int32_t get_offset_of_m_AmbientOnlyAO_8() { return static_cast<int32_t>(offsetof(MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6, ___m_AmbientOnlyAO_8)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_m_AmbientOnlyAO_8() const { return ___m_AmbientOnlyAO_8; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_m_AmbientOnlyAO_8() { return &___m_AmbientOnlyAO_8; }
	inline void set_m_AmbientOnlyAO_8(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___m_AmbientOnlyAO_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_AmbientOnlyAO_8), value);
	}

	inline static int32_t get_offset_of_m_MRT_9() { return static_cast<int32_t>(offsetof(MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6, ___m_MRT_9)); }
	inline RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4* get_m_MRT_9() const { return ___m_MRT_9; }
	inline RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4** get_address_of_m_MRT_9() { return &___m_MRT_9; }
	inline void set_m_MRT_9(RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4* value)
	{
		___m_MRT_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTISCALEVO_T916332C3C084DF662B2DEE0376083133C7BAB2C6_H
#ifndef PARAMETEROVERRIDE_T2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C_H
#define PARAMETEROVERRIDE_T2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride
struct  ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.ParameterOverride::overrideState
	bool ___overrideState_0;

public:
	inline static int32_t get_offset_of_overrideState_0() { return static_cast<int32_t>(offsetof(ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C, ___overrideState_0)); }
	inline bool get_overrideState_0() const { return ___overrideState_0; }
	inline bool* get_address_of_overrideState_0() { return &___overrideState_0; }
	inline void set_overrideState_0(bool value)
	{
		___overrideState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_T2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C_H
#ifndef POSTPROCESSBUNDLE_TE29C05526676DF2148A37681C517E85FD98BE225_H
#define POSTPROCESSBUNDLE_TE29C05526676DF2148A37681C517E85FD98BE225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessBundle
struct  PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessAttribute UnityEngine.Rendering.PostProcessing.PostProcessBundle::<attribute>k__BackingField
	PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748 * ___U3CattributeU3Ek__BackingField_0;
	// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings UnityEngine.Rendering.PostProcessing.PostProcessBundle::<settings>k__BackingField
	PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042 * ___U3CsettingsU3Ek__BackingField_1;
	// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer UnityEngine.Rendering.PostProcessing.PostProcessBundle::m_Renderer
	PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F * ___m_Renderer_2;

public:
	inline static int32_t get_offset_of_U3CattributeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225, ___U3CattributeU3Ek__BackingField_0)); }
	inline PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748 * get_U3CattributeU3Ek__BackingField_0() const { return ___U3CattributeU3Ek__BackingField_0; }
	inline PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748 ** get_address_of_U3CattributeU3Ek__BackingField_0() { return &___U3CattributeU3Ek__BackingField_0; }
	inline void set_U3CattributeU3Ek__BackingField_0(PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748 * value)
	{
		___U3CattributeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CattributeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225, ___U3CsettingsU3Ek__BackingField_1)); }
	inline PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_m_Renderer_2() { return static_cast<int32_t>(offsetof(PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225, ___m_Renderer_2)); }
	inline PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F * get_m_Renderer_2() const { return ___m_Renderer_2; }
	inline PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F ** get_address_of_m_Renderer_2() { return &___m_Renderer_2; }
	inline void set_m_Renderer_2(PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F * value)
	{
		___m_Renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSBUNDLE_TE29C05526676DF2148A37681C517E85FD98BE225_H
#ifndef POSTPROCESSEFFECTRENDERER_T4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F_H
#define POSTPROCESSEFFECTRENDERER_T4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer
struct  PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer::m_ResetHistory
	bool ___m_ResetHistory_0;

public:
	inline static int32_t get_offset_of_m_ResetHistory_0() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F, ___m_ResetHistory_0)); }
	inline bool get_m_ResetHistory_0() const { return ___m_ResetHistory_0; }
	inline bool* get_address_of_m_ResetHistory_0() { return &___m_ResetHistory_0; }
	inline void set_m_ResetHistory_0(bool value)
	{
		___m_ResetHistory_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_T4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F_H
#ifndef U3CU3EC_T554709697877216F0BA6F6ADC8DF28A35C362EF2_H
#define U3CU3EC_T554709697877216F0BA6F6ADC8DF28A35C362EF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings_<>c
struct  U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2_StaticFields
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings_<>c UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings_<>c::<>9
	U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings_<>c::<>9__3_0
	Func_2_t56D7C745FF6EF4A902AAB44EAFC9F43DC7DF0CDD * ___U3CU3E9__3_0_1;
	// System.Func`2<System.Reflection.FieldInfo,System.Int32> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings_<>c::<>9__3_1
	Func_2_t5A22B0C32E2088807584847339810C3893D8EDAE * ___U3CU3E9__3_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_t56D7C745FF6EF4A902AAB44EAFC9F43DC7DF0CDD * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_t56D7C745FF6EF4A902AAB44EAFC9F43DC7DF0CDD ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_t56D7C745FF6EF4A902AAB44EAFC9F43DC7DF0CDD * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2_StaticFields, ___U3CU3E9__3_1_2)); }
	inline Func_2_t5A22B0C32E2088807584847339810C3893D8EDAE * get_U3CU3E9__3_1_2() const { return ___U3CU3E9__3_1_2; }
	inline Func_2_t5A22B0C32E2088807584847339810C3893D8EDAE ** get_address_of_U3CU3E9__3_1_2() { return &___U3CU3E9__3_1_2; }
	inline void set_U3CU3E9__3_1_2(Func_2_t5A22B0C32E2088807584847339810C3893D8EDAE * value)
	{
		___U3CU3E9__3_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T554709697877216F0BA6F6ADC8DF28A35C362EF2_H
#ifndef U3CU3EC_TD342126B7CDEF358CBE105AB5E085F7EC496615E_H
#define U3CU3EC_TD342126B7CDEF358CBE105AB5E085F7EC496615E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c
struct  U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E_StaticFields
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c::<>9
	U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessBundle>,UnityEngine.Rendering.PostProcessing.PostProcessBundle> UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c::<>9__51_1
	Func_2_t22847FA433824D82F968976B22D8707ED0A87754 * ___U3CU3E9__51_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__51_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E_StaticFields, ___U3CU3E9__51_1_1)); }
	inline Func_2_t22847FA433824D82F968976B22D8707ED0A87754 * get_U3CU3E9__51_1_1() const { return ___U3CU3E9__51_1_1; }
	inline Func_2_t22847FA433824D82F968976B22D8707ED0A87754 ** get_address_of_U3CU3E9__51_1_1() { return &___U3CU3E9__51_1_1; }
	inline void set_U3CU3E9__51_1_1(Func_2_t22847FA433824D82F968976B22D8707ED0A87754 * value)
	{
		___U3CU3E9__51_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__51_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD342126B7CDEF358CBE105AB5E085F7EC496615E_H
#ifndef U3CU3EC__DISPLAYCLASS51_1_T9E5857DEB9F2265096E31C0B60D02137153F6562_H
#define U3CU3EC__DISPLAYCLASS51_1_T9E5857DEB9F2265096E31C0B60D02137153F6562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c__DisplayClass51_1
struct  U3CU3Ec__DisplayClass51_1_t9E5857DEB9F2265096E31C0B60D02137153F6562  : public RuntimeObject
{
public:
	// System.String UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c__DisplayClass51_1::searchStr
	String_t* ___searchStr_0;

public:
	inline static int32_t get_offset_of_searchStr_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass51_1_t9E5857DEB9F2265096E31C0B60D02137153F6562, ___searchStr_0)); }
	inline String_t* get_searchStr_0() const { return ___searchStr_0; }
	inline String_t** get_address_of_searchStr_0() { return &___searchStr_0; }
	inline void set_searchStr_0(String_t* value)
	{
		___searchStr_0 = value;
		Il2CppCodeGenWriteBarrier((&___searchStr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS51_1_T9E5857DEB9F2265096E31C0B60D02137153F6562_H
#ifndef U3CU3EC__DISPLAYCLASS51_2_T7F30F5295B2ECD71E50DBF73A1B2D0D87445767B_H
#define U3CU3EC__DISPLAYCLASS51_2_T7F30F5295B2ECD71E50DBF73A1B2D0D87445767B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c__DisplayClass51_2
struct  U3CU3Ec__DisplayClass51_2_t7F30F5295B2ECD71E50DBF73A1B2D0D87445767B  : public RuntimeObject
{
public:
	// System.String UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c__DisplayClass51_2::typeName
	String_t* ___typeName_0;

public:
	inline static int32_t get_offset_of_typeName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass51_2_t7F30F5295B2ECD71E50DBF73A1B2D0D87445767B, ___typeName_0)); }
	inline String_t* get_typeName_0() const { return ___typeName_0; }
	inline String_t** get_address_of_typeName_0() { return &___typeName_0; }
	inline void set_typeName_0(String_t* value)
	{
		___typeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS51_2_T7F30F5295B2ECD71E50DBF73A1B2D0D87445767B_H
#ifndef U3CU3EC__DISPLAYCLASS51_3_T9A6B0E881515DF4145E88A6630A204668F3DABF0_H
#define U3CU3EC__DISPLAYCLASS51_3_T9A6B0E881515DF4145E88A6630A204668F3DABF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c__DisplayClass51_3
struct  U3CU3Ec__DisplayClass51_3_t9A6B0E881515DF4145E88A6630A204668F3DABF0  : public RuntimeObject
{
public:
	// System.String UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c__DisplayClass51_3::typeName
	String_t* ___typeName_0;

public:
	inline static int32_t get_offset_of_typeName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass51_3_t9A6B0E881515DF4145E88A6630A204668F3DABF0, ___typeName_0)); }
	inline String_t* get_typeName_0() const { return ___typeName_0; }
	inline String_t** get_address_of_typeName_0() { return &___typeName_0; }
	inline void set_typeName_0(String_t* value)
	{
		___typeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS51_3_T9A6B0E881515DF4145E88A6630A204668F3DABF0_H
#ifndef SERIALIZEDBUNDLEREF_TFD6F759A8A0049AFDCA3E2380674C206707FE90E_H
#define SERIALIZEDBUNDLEREF_TFD6F759A8A0049AFDCA3E2380674C206707FE90E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer_SerializedBundleRef
struct  SerializedBundleRef_tFD6F759A8A0049AFDCA3E2380674C206707FE90E  : public RuntimeObject
{
public:
	// System.String UnityEngine.Rendering.PostProcessing.PostProcessLayer_SerializedBundleRef::assemblyQualifiedName
	String_t* ___assemblyQualifiedName_0;
	// UnityEngine.Rendering.PostProcessing.PostProcessBundle UnityEngine.Rendering.PostProcessing.PostProcessLayer_SerializedBundleRef::bundle
	PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225 * ___bundle_1;

public:
	inline static int32_t get_offset_of_assemblyQualifiedName_0() { return static_cast<int32_t>(offsetof(SerializedBundleRef_tFD6F759A8A0049AFDCA3E2380674C206707FE90E, ___assemblyQualifiedName_0)); }
	inline String_t* get_assemblyQualifiedName_0() const { return ___assemblyQualifiedName_0; }
	inline String_t** get_address_of_assemblyQualifiedName_0() { return &___assemblyQualifiedName_0; }
	inline void set_assemblyQualifiedName_0(String_t* value)
	{
		___assemblyQualifiedName_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyQualifiedName_0), value);
	}

	inline static int32_t get_offset_of_bundle_1() { return static_cast<int32_t>(offsetof(SerializedBundleRef_tFD6F759A8A0049AFDCA3E2380674C206707FE90E, ___bundle_1)); }
	inline PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225 * get_bundle_1() const { return ___bundle_1; }
	inline PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225 ** get_address_of_bundle_1() { return &___bundle_1; }
	inline void set_bundle_1(PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225 * value)
	{
		___bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___bundle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDBUNDLEREF_TFD6F759A8A0049AFDCA3E2380674C206707FE90E_H
#ifndef POSTPROCESSMANAGER_T4AE593295C402F7C792D8D4DF15443DBC2831189_H
#define POSTPROCESSMANAGER_T4AE593295C402F7C792D8D4DF15443DBC2831189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessManager
struct  PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessVolume>> UnityEngine.Rendering.PostProcessing.PostProcessManager::m_SortedVolumes
	Dictionary_2_t072E47B207A6F623AD23618E1104ABFD6DC928FF * ___m_SortedVolumes_1;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessVolume> UnityEngine.Rendering.PostProcessing.PostProcessManager::m_Volumes
	List_1_tBD4C46708D829BA47D48F8D990DABBD64D4B540B * ___m_Volumes_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> UnityEngine.Rendering.PostProcessing.PostProcessManager::m_SortNeeded
	Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * ___m_SortNeeded_3;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings> UnityEngine.Rendering.PostProcessing.PostProcessManager::m_BaseSettings
	List_1_t1F47AA13007605005DB7DD4BBCBD45CFCD05BD91 * ___m_BaseSettings_4;
	// System.Collections.Generic.List`1<UnityEngine.Collider> UnityEngine.Rendering.PostProcessing.PostProcessManager::m_TempColliders
	List_1_t34F3153FFE29D834C770DB7FD47483EE5175933C * ___m_TempColliders_5;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessAttribute> UnityEngine.Rendering.PostProcessing.PostProcessManager::settingsTypes
	Dictionary_2_tD9C3253F98C5ECFD23180A7D57335280EDFE50C5 * ___settingsTypes_6;

public:
	inline static int32_t get_offset_of_m_SortedVolumes_1() { return static_cast<int32_t>(offsetof(PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189, ___m_SortedVolumes_1)); }
	inline Dictionary_2_t072E47B207A6F623AD23618E1104ABFD6DC928FF * get_m_SortedVolumes_1() const { return ___m_SortedVolumes_1; }
	inline Dictionary_2_t072E47B207A6F623AD23618E1104ABFD6DC928FF ** get_address_of_m_SortedVolumes_1() { return &___m_SortedVolumes_1; }
	inline void set_m_SortedVolumes_1(Dictionary_2_t072E47B207A6F623AD23618E1104ABFD6DC928FF * value)
	{
		___m_SortedVolumes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SortedVolumes_1), value);
	}

	inline static int32_t get_offset_of_m_Volumes_2() { return static_cast<int32_t>(offsetof(PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189, ___m_Volumes_2)); }
	inline List_1_tBD4C46708D829BA47D48F8D990DABBD64D4B540B * get_m_Volumes_2() const { return ___m_Volumes_2; }
	inline List_1_tBD4C46708D829BA47D48F8D990DABBD64D4B540B ** get_address_of_m_Volumes_2() { return &___m_Volumes_2; }
	inline void set_m_Volumes_2(List_1_tBD4C46708D829BA47D48F8D990DABBD64D4B540B * value)
	{
		___m_Volumes_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Volumes_2), value);
	}

	inline static int32_t get_offset_of_m_SortNeeded_3() { return static_cast<int32_t>(offsetof(PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189, ___m_SortNeeded_3)); }
	inline Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * get_m_SortNeeded_3() const { return ___m_SortNeeded_3; }
	inline Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 ** get_address_of_m_SortNeeded_3() { return &___m_SortNeeded_3; }
	inline void set_m_SortNeeded_3(Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * value)
	{
		___m_SortNeeded_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SortNeeded_3), value);
	}

	inline static int32_t get_offset_of_m_BaseSettings_4() { return static_cast<int32_t>(offsetof(PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189, ___m_BaseSettings_4)); }
	inline List_1_t1F47AA13007605005DB7DD4BBCBD45CFCD05BD91 * get_m_BaseSettings_4() const { return ___m_BaseSettings_4; }
	inline List_1_t1F47AA13007605005DB7DD4BBCBD45CFCD05BD91 ** get_address_of_m_BaseSettings_4() { return &___m_BaseSettings_4; }
	inline void set_m_BaseSettings_4(List_1_t1F47AA13007605005DB7DD4BBCBD45CFCD05BD91 * value)
	{
		___m_BaseSettings_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseSettings_4), value);
	}

	inline static int32_t get_offset_of_m_TempColliders_5() { return static_cast<int32_t>(offsetof(PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189, ___m_TempColliders_5)); }
	inline List_1_t34F3153FFE29D834C770DB7FD47483EE5175933C * get_m_TempColliders_5() const { return ___m_TempColliders_5; }
	inline List_1_t34F3153FFE29D834C770DB7FD47483EE5175933C ** get_address_of_m_TempColliders_5() { return &___m_TempColliders_5; }
	inline void set_m_TempColliders_5(List_1_t34F3153FFE29D834C770DB7FD47483EE5175933C * value)
	{
		___m_TempColliders_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempColliders_5), value);
	}

	inline static int32_t get_offset_of_settingsTypes_6() { return static_cast<int32_t>(offsetof(PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189, ___settingsTypes_6)); }
	inline Dictionary_2_tD9C3253F98C5ECFD23180A7D57335280EDFE50C5 * get_settingsTypes_6() const { return ___settingsTypes_6; }
	inline Dictionary_2_tD9C3253F98C5ECFD23180A7D57335280EDFE50C5 ** get_address_of_settingsTypes_6() { return &___settingsTypes_6; }
	inline void set_settingsTypes_6(Dictionary_2_tD9C3253F98C5ECFD23180A7D57335280EDFE50C5 * value)
	{
		___settingsTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___settingsTypes_6), value);
	}
};

struct PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189_StaticFields
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessManager UnityEngine.Rendering.PostProcessing.PostProcessManager::s_Instance
	PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189_StaticFields, ___s_Instance_0)); }
	inline PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSMANAGER_T4AE593295C402F7C792D8D4DF15443DBC2831189_H
#ifndef U3CU3EC_T1BC90B0B2626B0720C768575D339258F724B5165_H
#define U3CU3EC_T1BC90B0B2626B0720C768575D339258F724B5165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessManager_<>c
struct  U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165_StaticFields
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessManager_<>c UnityEngine.Rendering.PostProcessing.PostProcessManager_<>c::<>9
	U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165 * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Boolean> UnityEngine.Rendering.PostProcessing.PostProcessManager_<>c::<>9__12_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__12_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1BC90B0B2626B0720C768575D339258F724B5165_H
#ifndef U3CU3EC_TBFB16AE2FACC328F1110602BF577A3CF3BEED14A_H
#define U3CU3EC_TBFB16AE2FACC328F1110602BF577A3CF3BEED14A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessProfile_<>c
struct  U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A_StaticFields
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessProfile_<>c UnityEngine.Rendering.PostProcessing.PostProcessProfile_<>c::<>9
	U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A * ___U3CU3E9_0;
	// System.Predicate`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings> UnityEngine.Rendering.PostProcessing.PostProcessProfile_<>c::<>9__2_0
	Predicate_1_t99D5EC69E65968D9A114AE4952484C236F61E507 * ___U3CU3E9__2_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Predicate_1_t99D5EC69E65968D9A114AE4952484C236F61E507 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Predicate_1_t99D5EC69E65968D9A114AE4952484C236F61E507 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Predicate_1_t99D5EC69E65968D9A114AE4952484C236F61E507 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TBFB16AE2FACC328F1110602BF577A3CF3BEED14A_H
#ifndef COMPUTESHADERS_T698DD1B303C404D771872D30E978C4F4237CA457_H
#define COMPUTESHADERS_T698DD1B303C404D771872D30E978C4F4237CA457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders
struct  ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457  : public RuntimeObject
{
public:
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::autoExposure
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___autoExposure_0;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::exposureHistogram
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___exposureHistogram_1;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::lut3DBaker
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___lut3DBaker_2;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::texture3dLerp
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___texture3dLerp_3;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::gammaHistogram
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___gammaHistogram_4;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::waveform
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___waveform_5;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::vectorscope
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___vectorscope_6;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::multiScaleAODownsample1
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___multiScaleAODownsample1_7;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::multiScaleAODownsample2
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___multiScaleAODownsample2_8;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::multiScaleAORender
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___multiScaleAORender_9;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::multiScaleAOUpsample
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___multiScaleAOUpsample_10;
	// UnityEngine.ComputeShader UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders::gaussianDownsample
	ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * ___gaussianDownsample_11;

public:
	inline static int32_t get_offset_of_autoExposure_0() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___autoExposure_0)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_autoExposure_0() const { return ___autoExposure_0; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_autoExposure_0() { return &___autoExposure_0; }
	inline void set_autoExposure_0(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___autoExposure_0 = value;
		Il2CppCodeGenWriteBarrier((&___autoExposure_0), value);
	}

	inline static int32_t get_offset_of_exposureHistogram_1() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___exposureHistogram_1)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_exposureHistogram_1() const { return ___exposureHistogram_1; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_exposureHistogram_1() { return &___exposureHistogram_1; }
	inline void set_exposureHistogram_1(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___exposureHistogram_1 = value;
		Il2CppCodeGenWriteBarrier((&___exposureHistogram_1), value);
	}

	inline static int32_t get_offset_of_lut3DBaker_2() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___lut3DBaker_2)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_lut3DBaker_2() const { return ___lut3DBaker_2; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_lut3DBaker_2() { return &___lut3DBaker_2; }
	inline void set_lut3DBaker_2(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___lut3DBaker_2 = value;
		Il2CppCodeGenWriteBarrier((&___lut3DBaker_2), value);
	}

	inline static int32_t get_offset_of_texture3dLerp_3() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___texture3dLerp_3)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_texture3dLerp_3() const { return ___texture3dLerp_3; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_texture3dLerp_3() { return &___texture3dLerp_3; }
	inline void set_texture3dLerp_3(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___texture3dLerp_3 = value;
		Il2CppCodeGenWriteBarrier((&___texture3dLerp_3), value);
	}

	inline static int32_t get_offset_of_gammaHistogram_4() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___gammaHistogram_4)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_gammaHistogram_4() const { return ___gammaHistogram_4; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_gammaHistogram_4() { return &___gammaHistogram_4; }
	inline void set_gammaHistogram_4(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___gammaHistogram_4 = value;
		Il2CppCodeGenWriteBarrier((&___gammaHistogram_4), value);
	}

	inline static int32_t get_offset_of_waveform_5() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___waveform_5)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_waveform_5() const { return ___waveform_5; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_waveform_5() { return &___waveform_5; }
	inline void set_waveform_5(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___waveform_5 = value;
		Il2CppCodeGenWriteBarrier((&___waveform_5), value);
	}

	inline static int32_t get_offset_of_vectorscope_6() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___vectorscope_6)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_vectorscope_6() const { return ___vectorscope_6; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_vectorscope_6() { return &___vectorscope_6; }
	inline void set_vectorscope_6(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___vectorscope_6 = value;
		Il2CppCodeGenWriteBarrier((&___vectorscope_6), value);
	}

	inline static int32_t get_offset_of_multiScaleAODownsample1_7() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___multiScaleAODownsample1_7)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_multiScaleAODownsample1_7() const { return ___multiScaleAODownsample1_7; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_multiScaleAODownsample1_7() { return &___multiScaleAODownsample1_7; }
	inline void set_multiScaleAODownsample1_7(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___multiScaleAODownsample1_7 = value;
		Il2CppCodeGenWriteBarrier((&___multiScaleAODownsample1_7), value);
	}

	inline static int32_t get_offset_of_multiScaleAODownsample2_8() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___multiScaleAODownsample2_8)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_multiScaleAODownsample2_8() const { return ___multiScaleAODownsample2_8; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_multiScaleAODownsample2_8() { return &___multiScaleAODownsample2_8; }
	inline void set_multiScaleAODownsample2_8(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___multiScaleAODownsample2_8 = value;
		Il2CppCodeGenWriteBarrier((&___multiScaleAODownsample2_8), value);
	}

	inline static int32_t get_offset_of_multiScaleAORender_9() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___multiScaleAORender_9)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_multiScaleAORender_9() const { return ___multiScaleAORender_9; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_multiScaleAORender_9() { return &___multiScaleAORender_9; }
	inline void set_multiScaleAORender_9(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___multiScaleAORender_9 = value;
		Il2CppCodeGenWriteBarrier((&___multiScaleAORender_9), value);
	}

	inline static int32_t get_offset_of_multiScaleAOUpsample_10() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___multiScaleAOUpsample_10)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_multiScaleAOUpsample_10() const { return ___multiScaleAOUpsample_10; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_multiScaleAOUpsample_10() { return &___multiScaleAOUpsample_10; }
	inline void set_multiScaleAOUpsample_10(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___multiScaleAOUpsample_10 = value;
		Il2CppCodeGenWriteBarrier((&___multiScaleAOUpsample_10), value);
	}

	inline static int32_t get_offset_of_gaussianDownsample_11() { return static_cast<int32_t>(offsetof(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457, ___gaussianDownsample_11)); }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * get_gaussianDownsample_11() const { return ___gaussianDownsample_11; }
	inline ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A ** get_address_of_gaussianDownsample_11() { return &___gaussianDownsample_11; }
	inline void set_gaussianDownsample_11(ComputeShader_tF8B65214DC8C7C124C01984EB5CCD28F55C85B2A * value)
	{
		___gaussianDownsample_11 = value;
		Il2CppCodeGenWriteBarrier((&___gaussianDownsample_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTESHADERS_T698DD1B303C404D771872D30E978C4F4237CA457_H
#ifndef SMAALUTS_T7C73EA4D782E325A2FF0795B7FAF5DAA343A4090_H
#define SMAALUTS_T7C73EA4D782E325A2FF0795B7FAF5DAA343A4090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessResources_SMAALuts
struct  SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.PostProcessResources_SMAALuts::area
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___area_0;
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.PostProcessResources_SMAALuts::search
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___search_1;

public:
	inline static int32_t get_offset_of_area_0() { return static_cast<int32_t>(offsetof(SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090, ___area_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_area_0() const { return ___area_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_area_0() { return &___area_0; }
	inline void set_area_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___area_0 = value;
		Il2CppCodeGenWriteBarrier((&___area_0), value);
	}

	inline static int32_t get_offset_of_search_1() { return static_cast<int32_t>(offsetof(SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090, ___search_1)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_search_1() const { return ___search_1; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_search_1() { return &___search_1; }
	inline void set_search_1(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___search_1 = value;
		Il2CppCodeGenWriteBarrier((&___search_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMAALUTS_T7C73EA4D782E325A2FF0795B7FAF5DAA343A4090_H
#ifndef SHADERS_T8171DAB81DB3F380BB5F42114476BE06467B34DF_H
#define SHADERS_T8171DAB81DB3F380BB5F42114476BE06467B34DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders
struct  Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF  : public RuntimeObject
{
public:
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::bloom
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___bloom_0;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::copy
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___copy_1;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::copyStd
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___copyStd_2;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::copyStdFromTexArray
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___copyStdFromTexArray_3;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::copyStdFromDoubleWide
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___copyStdFromDoubleWide_4;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::discardAlpha
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___discardAlpha_5;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::depthOfField
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___depthOfField_6;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::finalPass
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___finalPass_7;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::grainBaker
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___grainBaker_8;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::motionBlur
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___motionBlur_9;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::temporalAntialiasing
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___temporalAntialiasing_10;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::subpixelMorphologicalAntialiasing
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___subpixelMorphologicalAntialiasing_11;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::texture2dLerp
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___texture2dLerp_12;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::uber
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___uber_13;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::lut2DBaker
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___lut2DBaker_14;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::lightMeter
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___lightMeter_15;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::gammaHistogram
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___gammaHistogram_16;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::waveform
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___waveform_17;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::vectorscope
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___vectorscope_18;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::debugOverlays
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___debugOverlays_19;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::deferredFog
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___deferredFog_20;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::scalableAO
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___scalableAO_21;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::multiScaleAO
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___multiScaleAO_22;
	// UnityEngine.Shader UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders::screenSpaceReflections
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___screenSpaceReflections_23;

public:
	inline static int32_t get_offset_of_bloom_0() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___bloom_0)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_bloom_0() const { return ___bloom_0; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_bloom_0() { return &___bloom_0; }
	inline void set_bloom_0(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___bloom_0 = value;
		Il2CppCodeGenWriteBarrier((&___bloom_0), value);
	}

	inline static int32_t get_offset_of_copy_1() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___copy_1)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_copy_1() const { return ___copy_1; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_copy_1() { return &___copy_1; }
	inline void set_copy_1(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___copy_1 = value;
		Il2CppCodeGenWriteBarrier((&___copy_1), value);
	}

	inline static int32_t get_offset_of_copyStd_2() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___copyStd_2)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_copyStd_2() const { return ___copyStd_2; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_copyStd_2() { return &___copyStd_2; }
	inline void set_copyStd_2(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___copyStd_2 = value;
		Il2CppCodeGenWriteBarrier((&___copyStd_2), value);
	}

	inline static int32_t get_offset_of_copyStdFromTexArray_3() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___copyStdFromTexArray_3)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_copyStdFromTexArray_3() const { return ___copyStdFromTexArray_3; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_copyStdFromTexArray_3() { return &___copyStdFromTexArray_3; }
	inline void set_copyStdFromTexArray_3(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___copyStdFromTexArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___copyStdFromTexArray_3), value);
	}

	inline static int32_t get_offset_of_copyStdFromDoubleWide_4() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___copyStdFromDoubleWide_4)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_copyStdFromDoubleWide_4() const { return ___copyStdFromDoubleWide_4; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_copyStdFromDoubleWide_4() { return &___copyStdFromDoubleWide_4; }
	inline void set_copyStdFromDoubleWide_4(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___copyStdFromDoubleWide_4 = value;
		Il2CppCodeGenWriteBarrier((&___copyStdFromDoubleWide_4), value);
	}

	inline static int32_t get_offset_of_discardAlpha_5() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___discardAlpha_5)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_discardAlpha_5() const { return ___discardAlpha_5; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_discardAlpha_5() { return &___discardAlpha_5; }
	inline void set_discardAlpha_5(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___discardAlpha_5 = value;
		Il2CppCodeGenWriteBarrier((&___discardAlpha_5), value);
	}

	inline static int32_t get_offset_of_depthOfField_6() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___depthOfField_6)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_depthOfField_6() const { return ___depthOfField_6; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_depthOfField_6() { return &___depthOfField_6; }
	inline void set_depthOfField_6(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___depthOfField_6 = value;
		Il2CppCodeGenWriteBarrier((&___depthOfField_6), value);
	}

	inline static int32_t get_offset_of_finalPass_7() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___finalPass_7)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_finalPass_7() const { return ___finalPass_7; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_finalPass_7() { return &___finalPass_7; }
	inline void set_finalPass_7(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___finalPass_7 = value;
		Il2CppCodeGenWriteBarrier((&___finalPass_7), value);
	}

	inline static int32_t get_offset_of_grainBaker_8() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___grainBaker_8)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_grainBaker_8() const { return ___grainBaker_8; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_grainBaker_8() { return &___grainBaker_8; }
	inline void set_grainBaker_8(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___grainBaker_8 = value;
		Il2CppCodeGenWriteBarrier((&___grainBaker_8), value);
	}

	inline static int32_t get_offset_of_motionBlur_9() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___motionBlur_9)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_motionBlur_9() const { return ___motionBlur_9; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_motionBlur_9() { return &___motionBlur_9; }
	inline void set_motionBlur_9(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___motionBlur_9 = value;
		Il2CppCodeGenWriteBarrier((&___motionBlur_9), value);
	}

	inline static int32_t get_offset_of_temporalAntialiasing_10() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___temporalAntialiasing_10)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_temporalAntialiasing_10() const { return ___temporalAntialiasing_10; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_temporalAntialiasing_10() { return &___temporalAntialiasing_10; }
	inline void set_temporalAntialiasing_10(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___temporalAntialiasing_10 = value;
		Il2CppCodeGenWriteBarrier((&___temporalAntialiasing_10), value);
	}

	inline static int32_t get_offset_of_subpixelMorphologicalAntialiasing_11() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___subpixelMorphologicalAntialiasing_11)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_subpixelMorphologicalAntialiasing_11() const { return ___subpixelMorphologicalAntialiasing_11; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_subpixelMorphologicalAntialiasing_11() { return &___subpixelMorphologicalAntialiasing_11; }
	inline void set_subpixelMorphologicalAntialiasing_11(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___subpixelMorphologicalAntialiasing_11 = value;
		Il2CppCodeGenWriteBarrier((&___subpixelMorphologicalAntialiasing_11), value);
	}

	inline static int32_t get_offset_of_texture2dLerp_12() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___texture2dLerp_12)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_texture2dLerp_12() const { return ___texture2dLerp_12; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_texture2dLerp_12() { return &___texture2dLerp_12; }
	inline void set_texture2dLerp_12(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___texture2dLerp_12 = value;
		Il2CppCodeGenWriteBarrier((&___texture2dLerp_12), value);
	}

	inline static int32_t get_offset_of_uber_13() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___uber_13)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_uber_13() const { return ___uber_13; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_uber_13() { return &___uber_13; }
	inline void set_uber_13(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___uber_13 = value;
		Il2CppCodeGenWriteBarrier((&___uber_13), value);
	}

	inline static int32_t get_offset_of_lut2DBaker_14() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___lut2DBaker_14)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_lut2DBaker_14() const { return ___lut2DBaker_14; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_lut2DBaker_14() { return &___lut2DBaker_14; }
	inline void set_lut2DBaker_14(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___lut2DBaker_14 = value;
		Il2CppCodeGenWriteBarrier((&___lut2DBaker_14), value);
	}

	inline static int32_t get_offset_of_lightMeter_15() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___lightMeter_15)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_lightMeter_15() const { return ___lightMeter_15; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_lightMeter_15() { return &___lightMeter_15; }
	inline void set_lightMeter_15(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___lightMeter_15 = value;
		Il2CppCodeGenWriteBarrier((&___lightMeter_15), value);
	}

	inline static int32_t get_offset_of_gammaHistogram_16() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___gammaHistogram_16)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_gammaHistogram_16() const { return ___gammaHistogram_16; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_gammaHistogram_16() { return &___gammaHistogram_16; }
	inline void set_gammaHistogram_16(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___gammaHistogram_16 = value;
		Il2CppCodeGenWriteBarrier((&___gammaHistogram_16), value);
	}

	inline static int32_t get_offset_of_waveform_17() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___waveform_17)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_waveform_17() const { return ___waveform_17; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_waveform_17() { return &___waveform_17; }
	inline void set_waveform_17(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___waveform_17 = value;
		Il2CppCodeGenWriteBarrier((&___waveform_17), value);
	}

	inline static int32_t get_offset_of_vectorscope_18() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___vectorscope_18)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_vectorscope_18() const { return ___vectorscope_18; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_vectorscope_18() { return &___vectorscope_18; }
	inline void set_vectorscope_18(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___vectorscope_18 = value;
		Il2CppCodeGenWriteBarrier((&___vectorscope_18), value);
	}

	inline static int32_t get_offset_of_debugOverlays_19() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___debugOverlays_19)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_debugOverlays_19() const { return ___debugOverlays_19; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_debugOverlays_19() { return &___debugOverlays_19; }
	inline void set_debugOverlays_19(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___debugOverlays_19 = value;
		Il2CppCodeGenWriteBarrier((&___debugOverlays_19), value);
	}

	inline static int32_t get_offset_of_deferredFog_20() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___deferredFog_20)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_deferredFog_20() const { return ___deferredFog_20; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_deferredFog_20() { return &___deferredFog_20; }
	inline void set_deferredFog_20(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___deferredFog_20 = value;
		Il2CppCodeGenWriteBarrier((&___deferredFog_20), value);
	}

	inline static int32_t get_offset_of_scalableAO_21() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___scalableAO_21)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_scalableAO_21() const { return ___scalableAO_21; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_scalableAO_21() { return &___scalableAO_21; }
	inline void set_scalableAO_21(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___scalableAO_21 = value;
		Il2CppCodeGenWriteBarrier((&___scalableAO_21), value);
	}

	inline static int32_t get_offset_of_multiScaleAO_22() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___multiScaleAO_22)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_multiScaleAO_22() const { return ___multiScaleAO_22; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_multiScaleAO_22() { return &___multiScaleAO_22; }
	inline void set_multiScaleAO_22(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___multiScaleAO_22 = value;
		Il2CppCodeGenWriteBarrier((&___multiScaleAO_22), value);
	}

	inline static int32_t get_offset_of_screenSpaceReflections_23() { return static_cast<int32_t>(offsetof(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF, ___screenSpaceReflections_23)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_screenSpaceReflections_23() const { return ___screenSpaceReflections_23; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_screenSpaceReflections_23() { return &___screenSpaceReflections_23; }
	inline void set_screenSpaceReflections_23(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___screenSpaceReflections_23 = value;
		Il2CppCodeGenWriteBarrier((&___screenSpaceReflections_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERS_T8171DAB81DB3F380BB5F42114476BE06467B34DF_H
#ifndef PROPERTYSHEET_TA49E657A6C015EADD4DD1F14FD41F022D5DE3735_H
#define PROPERTYSHEET_TA49E657A6C015EADD4DD1F14FD41F022D5DE3735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PropertySheet
struct  PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735  : public RuntimeObject
{
public:
	// UnityEngine.MaterialPropertyBlock UnityEngine.Rendering.PostProcessing.PropertySheet::<properties>k__BackingField
	MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * ___U3CpropertiesU3Ek__BackingField_0;
	// UnityEngine.Material UnityEngine.Rendering.PostProcessing.PropertySheet::<material>k__BackingField
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___U3CmaterialU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CpropertiesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735, ___U3CpropertiesU3Ek__BackingField_0)); }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * get_U3CpropertiesU3Ek__BackingField_0() const { return ___U3CpropertiesU3Ek__BackingField_0; }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 ** get_address_of_U3CpropertiesU3Ek__BackingField_0() { return &___U3CpropertiesU3Ek__BackingField_0; }
	inline void set_U3CpropertiesU3Ek__BackingField_0(MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * value)
	{
		___U3CpropertiesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpropertiesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmaterialU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735, ___U3CmaterialU3Ek__BackingField_1)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_U3CmaterialU3Ek__BackingField_1() const { return ___U3CmaterialU3Ek__BackingField_1; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_U3CmaterialU3Ek__BackingField_1() { return &___U3CmaterialU3Ek__BackingField_1; }
	inline void set_U3CmaterialU3Ek__BackingField_1(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___U3CmaterialU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmaterialU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYSHEET_TA49E657A6C015EADD4DD1F14FD41F022D5DE3735_H
#ifndef PROPERTYSHEETFACTORY_T3E86AEE66A7E970A0861A8B698F21F4135BA0EE4_H
#define PROPERTYSHEETFACTORY_T3E86AEE66A7E970A0861A8B698F21F4135BA0EE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PropertySheetFactory
struct  PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.Shader,UnityEngine.Rendering.PostProcessing.PropertySheet> UnityEngine.Rendering.PostProcessing.PropertySheetFactory::m_Sheets
	Dictionary_2_tB620A307AE6B4F88DE0FD47BFDCAE45768F3B478 * ___m_Sheets_0;

public:
	inline static int32_t get_offset_of_m_Sheets_0() { return static_cast<int32_t>(offsetof(PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4, ___m_Sheets_0)); }
	inline Dictionary_2_tB620A307AE6B4F88DE0FD47BFDCAE45768F3B478 * get_m_Sheets_0() const { return ___m_Sheets_0; }
	inline Dictionary_2_tB620A307AE6B4F88DE0FD47BFDCAE45768F3B478 ** get_address_of_m_Sheets_0() { return &___m_Sheets_0; }
	inline void set_m_Sheets_0(Dictionary_2_tB620A307AE6B4F88DE0FD47BFDCAE45768F3B478 * value)
	{
		___m_Sheets_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sheets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYSHEETFACTORY_T3E86AEE66A7E970A0861A8B698F21F4135BA0EE4_H
#ifndef RUNTIMEUTILITIES_T26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_H
#define RUNTIMEUTILITIES_T26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.RuntimeUtilities
struct  RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95  : public RuntimeObject
{
public:

public:
};

struct RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields
{
public:
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.RuntimeUtilities::m_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_WhiteTexture_0;
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.RuntimeUtilities::m_BlackTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_BlackTexture_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Texture2D> UnityEngine.Rendering.PostProcessing.RuntimeUtilities::m_LutStrips
	Dictionary_2_t992D19043FB7E6168A78D006336B88ACEDAC2D50 * ___m_LutStrips_2;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_Resources
	PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * ___s_Resources_3;
	// UnityEngine.Mesh UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_FullscreenTriangle
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_FullscreenTriangle_4;
	// UnityEngine.Material UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_CopyStdMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_CopyStdMaterial_5;
	// UnityEngine.Material UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_CopyStdFromDoubleWideMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_CopyStdFromDoubleWideMaterial_6;
	// UnityEngine.Material UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_CopyMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_CopyMaterial_7;
	// UnityEngine.Material UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_CopyFromTexArrayMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_CopyFromTexArrayMaterial_8;
	// UnityEngine.Rendering.PostProcessing.PropertySheet UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_CopySheet
	PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * ___s_CopySheet_9;
	// UnityEngine.Rendering.PostProcessing.PropertySheet UnityEngine.Rendering.PostProcessing.RuntimeUtilities::s_CopyFromTexArraySheet
	PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * ___s_CopyFromTexArraySheet_10;
	// System.Collections.Generic.IEnumerable`1<System.Type> UnityEngine.Rendering.PostProcessing.RuntimeUtilities::m_AssemblyTypes
	RuntimeObject* ___m_AssemblyTypes_11;

public:
	inline static int32_t get_offset_of_m_WhiteTexture_0() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___m_WhiteTexture_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_WhiteTexture_0() const { return ___m_WhiteTexture_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_WhiteTexture_0() { return &___m_WhiteTexture_0; }
	inline void set_m_WhiteTexture_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_WhiteTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_WhiteTexture_0), value);
	}

	inline static int32_t get_offset_of_m_BlackTexture_1() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___m_BlackTexture_1)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_BlackTexture_1() const { return ___m_BlackTexture_1; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_BlackTexture_1() { return &___m_BlackTexture_1; }
	inline void set_m_BlackTexture_1(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_BlackTexture_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlackTexture_1), value);
	}

	inline static int32_t get_offset_of_m_LutStrips_2() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___m_LutStrips_2)); }
	inline Dictionary_2_t992D19043FB7E6168A78D006336B88ACEDAC2D50 * get_m_LutStrips_2() const { return ___m_LutStrips_2; }
	inline Dictionary_2_t992D19043FB7E6168A78D006336B88ACEDAC2D50 ** get_address_of_m_LutStrips_2() { return &___m_LutStrips_2; }
	inline void set_m_LutStrips_2(Dictionary_2_t992D19043FB7E6168A78D006336B88ACEDAC2D50 * value)
	{
		___m_LutStrips_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LutStrips_2), value);
	}

	inline static int32_t get_offset_of_s_Resources_3() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___s_Resources_3)); }
	inline PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * get_s_Resources_3() const { return ___s_Resources_3; }
	inline PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B ** get_address_of_s_Resources_3() { return &___s_Resources_3; }
	inline void set_s_Resources_3(PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * value)
	{
		___s_Resources_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Resources_3), value);
	}

	inline static int32_t get_offset_of_s_FullscreenTriangle_4() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___s_FullscreenTriangle_4)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_FullscreenTriangle_4() const { return ___s_FullscreenTriangle_4; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_FullscreenTriangle_4() { return &___s_FullscreenTriangle_4; }
	inline void set_s_FullscreenTriangle_4(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_FullscreenTriangle_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_FullscreenTriangle_4), value);
	}

	inline static int32_t get_offset_of_s_CopyStdMaterial_5() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___s_CopyStdMaterial_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_CopyStdMaterial_5() const { return ___s_CopyStdMaterial_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_CopyStdMaterial_5() { return &___s_CopyStdMaterial_5; }
	inline void set_s_CopyStdMaterial_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_CopyStdMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_CopyStdMaterial_5), value);
	}

	inline static int32_t get_offset_of_s_CopyStdFromDoubleWideMaterial_6() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___s_CopyStdFromDoubleWideMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_CopyStdFromDoubleWideMaterial_6() const { return ___s_CopyStdFromDoubleWideMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_CopyStdFromDoubleWideMaterial_6() { return &___s_CopyStdFromDoubleWideMaterial_6; }
	inline void set_s_CopyStdFromDoubleWideMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_CopyStdFromDoubleWideMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_CopyStdFromDoubleWideMaterial_6), value);
	}

	inline static int32_t get_offset_of_s_CopyMaterial_7() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___s_CopyMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_CopyMaterial_7() const { return ___s_CopyMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_CopyMaterial_7() { return &___s_CopyMaterial_7; }
	inline void set_s_CopyMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_CopyMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_CopyMaterial_7), value);
	}

	inline static int32_t get_offset_of_s_CopyFromTexArrayMaterial_8() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___s_CopyFromTexArrayMaterial_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_CopyFromTexArrayMaterial_8() const { return ___s_CopyFromTexArrayMaterial_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_CopyFromTexArrayMaterial_8() { return &___s_CopyFromTexArrayMaterial_8; }
	inline void set_s_CopyFromTexArrayMaterial_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_CopyFromTexArrayMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_CopyFromTexArrayMaterial_8), value);
	}

	inline static int32_t get_offset_of_s_CopySheet_9() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___s_CopySheet_9)); }
	inline PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * get_s_CopySheet_9() const { return ___s_CopySheet_9; }
	inline PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 ** get_address_of_s_CopySheet_9() { return &___s_CopySheet_9; }
	inline void set_s_CopySheet_9(PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * value)
	{
		___s_CopySheet_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_CopySheet_9), value);
	}

	inline static int32_t get_offset_of_s_CopyFromTexArraySheet_10() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___s_CopyFromTexArraySheet_10)); }
	inline PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * get_s_CopyFromTexArraySheet_10() const { return ___s_CopyFromTexArraySheet_10; }
	inline PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 ** get_address_of_s_CopyFromTexArraySheet_10() { return &___s_CopyFromTexArraySheet_10; }
	inline void set_s_CopyFromTexArraySheet_10(PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * value)
	{
		___s_CopyFromTexArraySheet_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_CopyFromTexArraySheet_10), value);
	}

	inline static int32_t get_offset_of_m_AssemblyTypes_11() { return static_cast<int32_t>(offsetof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields, ___m_AssemblyTypes_11)); }
	inline RuntimeObject* get_m_AssemblyTypes_11() const { return ___m_AssemblyTypes_11; }
	inline RuntimeObject** get_address_of_m_AssemblyTypes_11() { return &___m_AssemblyTypes_11; }
	inline void set_m_AssemblyTypes_11(RuntimeObject* value)
	{
		___m_AssemblyTypes_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_AssemblyTypes_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEUTILITIES_T26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_H
#ifndef U3CU3EC_TD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F_H
#define U3CU3EC_TD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.RuntimeUtilities_<>c
struct  U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F_StaticFields
{
public:
	// UnityEngine.Rendering.PostProcessing.RuntimeUtilities_<>c UnityEngine.Rendering.PostProcessing.RuntimeUtilities_<>c::<>9
	U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>> UnityEngine.Rendering.PostProcessing.RuntimeUtilities_<>c::<>9__87_0
	Func_2_t8F89995E22C5EB8E236C651EC47EB541468BF297 * ___U3CU3E9__87_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__87_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F_StaticFields, ___U3CU3E9__87_0_1)); }
	inline Func_2_t8F89995E22C5EB8E236C651EC47EB541468BF297 * get_U3CU3E9__87_0_1() const { return ___U3CU3E9__87_0_1; }
	inline Func_2_t8F89995E22C5EB8E236C651EC47EB541468BF297 ** get_address_of_U3CU3E9__87_0_1() { return &___U3CU3E9__87_0_1; }
	inline void set_U3CU3E9__87_0_1(Func_2_t8F89995E22C5EB8E236C651EC47EB541468BF297 * value)
	{
		___U3CU3E9__87_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__87_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F_H
#ifndef SCALABLEAO_TE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33_H
#define SCALABLEAO_TE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScalableAO
struct  ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33  : public RuntimeObject
{
public:
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.ScalableAO::m_Result
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___m_Result_0;
	// UnityEngine.Rendering.PostProcessing.PropertySheet UnityEngine.Rendering.PostProcessing.ScalableAO::m_PropertySheet
	PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * ___m_PropertySheet_1;
	// UnityEngine.Rendering.PostProcessing.AmbientOcclusion UnityEngine.Rendering.PostProcessing.ScalableAO::m_Settings
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B * ___m_Settings_2;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.Rendering.PostProcessing.ScalableAO::m_MRT
	RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4* ___m_MRT_3;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.ScalableAO::m_SampleCount
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_SampleCount_4;

public:
	inline static int32_t get_offset_of_m_Result_0() { return static_cast<int32_t>(offsetof(ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33, ___m_Result_0)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_m_Result_0() const { return ___m_Result_0; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_m_Result_0() { return &___m_Result_0; }
	inline void set_m_Result_0(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___m_Result_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_0), value);
	}

	inline static int32_t get_offset_of_m_PropertySheet_1() { return static_cast<int32_t>(offsetof(ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33, ___m_PropertySheet_1)); }
	inline PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * get_m_PropertySheet_1() const { return ___m_PropertySheet_1; }
	inline PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 ** get_address_of_m_PropertySheet_1() { return &___m_PropertySheet_1; }
	inline void set_m_PropertySheet_1(PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * value)
	{
		___m_PropertySheet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropertySheet_1), value);
	}

	inline static int32_t get_offset_of_m_Settings_2() { return static_cast<int32_t>(offsetof(ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33, ___m_Settings_2)); }
	inline AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B * get_m_Settings_2() const { return ___m_Settings_2; }
	inline AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B ** get_address_of_m_Settings_2() { return &___m_Settings_2; }
	inline void set_m_Settings_2(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B * value)
	{
		___m_Settings_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_2), value);
	}

	inline static int32_t get_offset_of_m_MRT_3() { return static_cast<int32_t>(offsetof(ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33, ___m_MRT_3)); }
	inline RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4* get_m_MRT_3() const { return ___m_MRT_3; }
	inline RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4** get_address_of_m_MRT_3() { return &___m_MRT_3; }
	inline void set_m_MRT_3(RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4* value)
	{
		___m_MRT_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_3), value);
	}

	inline static int32_t get_offset_of_m_SampleCount_4() { return static_cast<int32_t>(offsetof(ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33, ___m_SampleCount_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_SampleCount_4() const { return ___m_SampleCount_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_SampleCount_4() { return &___m_SampleCount_4; }
	inline void set_m_SampleCount_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_SampleCount_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SampleCount_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALABLEAO_TE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33_H
#ifndef SHADERIDS_TF2DD07006369DE1FD0D337C7CF4D2F6849F81721_H
#define SHADERIDS_TF2DD07006369DE1FD0D337C7CF4D2F6849F81721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ShaderIDs
struct  ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721  : public RuntimeObject
{
public:

public:
};

struct ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MainTex
	int32_t ___MainTex_0;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Jitter
	int32_t ___Jitter_1;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Sharpness
	int32_t ___Sharpness_2;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::FinalBlendParameters
	int32_t ___FinalBlendParameters_3;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::HistoryTex
	int32_t ___HistoryTex_4;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::SMAA_Flip
	int32_t ___SMAA_Flip_5;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::SMAA_Flop
	int32_t ___SMAA_Flop_6;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::AOParams
	int32_t ___AOParams_7;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::AOColor
	int32_t ___AOColor_8;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::OcclusionTexture1
	int32_t ___OcclusionTexture1_9;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::OcclusionTexture2
	int32_t ___OcclusionTexture2_10;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::SAOcclusionTexture
	int32_t ___SAOcclusionTexture_11;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MSVOcclusionTexture
	int32_t ___MSVOcclusionTexture_12;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::DepthCopy
	int32_t ___DepthCopy_13;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LinearDepth
	int32_t ___LinearDepth_14;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LowDepth1
	int32_t ___LowDepth1_15;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LowDepth2
	int32_t ___LowDepth2_16;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LowDepth3
	int32_t ___LowDepth3_17;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LowDepth4
	int32_t ___LowDepth4_18;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TiledDepth1
	int32_t ___TiledDepth1_19;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TiledDepth2
	int32_t ___TiledDepth2_20;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TiledDepth3
	int32_t ___TiledDepth3_21;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TiledDepth4
	int32_t ___TiledDepth4_22;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Occlusion1
	int32_t ___Occlusion1_23;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Occlusion2
	int32_t ___Occlusion2_24;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Occlusion3
	int32_t ___Occlusion3_25;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Occlusion4
	int32_t ___Occlusion4_26;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Combined1
	int32_t ___Combined1_27;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Combined2
	int32_t ___Combined2_28;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Combined3
	int32_t ___Combined3_29;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::SSRResolveTemp
	int32_t ___SSRResolveTemp_30;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Noise
	int32_t ___Noise_31;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Test
	int32_t ___Test_32;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Resolve
	int32_t ___Resolve_33;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::History
	int32_t ___History_34;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ViewMatrix
	int32_t ___ViewMatrix_35;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::InverseViewMatrix
	int32_t ___InverseViewMatrix_36;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::InverseProjectionMatrix
	int32_t ___InverseProjectionMatrix_37;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ScreenSpaceProjectionMatrix
	int32_t ___ScreenSpaceProjectionMatrix_38;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Params2
	int32_t ___Params2_39;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::FogColor
	int32_t ___FogColor_40;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::FogParams
	int32_t ___FogParams_41;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::VelocityScale
	int32_t ___VelocityScale_42;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MaxBlurRadius
	int32_t ___MaxBlurRadius_43;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::RcpMaxBlurRadius
	int32_t ___RcpMaxBlurRadius_44;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::VelocityTex
	int32_t ___VelocityTex_45;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Tile2RT
	int32_t ___Tile2RT_46;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Tile4RT
	int32_t ___Tile4RT_47;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Tile8RT
	int32_t ___Tile8RT_48;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TileMaxOffs
	int32_t ___TileMaxOffs_49;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TileMaxLoop
	int32_t ___TileMaxLoop_50;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TileVRT
	int32_t ___TileVRT_51;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::NeighborMaxTex
	int32_t ___NeighborMaxTex_52;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LoopCount
	int32_t ___LoopCount_53;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::DepthOfFieldTemp
	int32_t ___DepthOfFieldTemp_54;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::DepthOfFieldTex
	int32_t ___DepthOfFieldTex_55;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Distance
	int32_t ___Distance_56;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LensCoeff
	int32_t ___LensCoeff_57;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MaxCoC
	int32_t ___MaxCoC_58;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::RcpMaxCoC
	int32_t ___RcpMaxCoC_59;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::RcpAspect
	int32_t ___RcpAspect_60;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::CoCTex
	int32_t ___CoCTex_61;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TaaParams
	int32_t ___TaaParams_62;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::AutoExposureTex
	int32_t ___AutoExposureTex_63;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::HistogramBuffer
	int32_t ___HistogramBuffer_64;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Params
	int32_t ___Params_65;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ScaleOffsetRes
	int32_t ___ScaleOffsetRes_66;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::BloomTex
	int32_t ___BloomTex_67;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::SampleScale
	int32_t ___SampleScale_68;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Threshold
	int32_t ___Threshold_69;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ColorIntensity
	int32_t ___ColorIntensity_70;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Bloom_DirtTex
	int32_t ___Bloom_DirtTex_71;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Bloom_Settings
	int32_t ___Bloom_Settings_72;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Bloom_Color
	int32_t ___Bloom_Color_73;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Bloom_DirtTileOffset
	int32_t ___Bloom_DirtTileOffset_74;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ChromaticAberration_Amount
	int32_t ___ChromaticAberration_Amount_75;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ChromaticAberration_SpectralLut
	int32_t ___ChromaticAberration_SpectralLut_76;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Distortion_CenterScale
	int32_t ___Distortion_CenterScale_77;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Distortion_Amount
	int32_t ___Distortion_Amount_78;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Lut2D
	int32_t ___Lut2D_79;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Lut3D
	int32_t ___Lut3D_80;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Lut3D_Params
	int32_t ___Lut3D_Params_81;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Lut2D_Params
	int32_t ___Lut2D_Params_82;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::UserLut2D_Params
	int32_t ___UserLut2D_Params_83;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::PostExposure
	int32_t ___PostExposure_84;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ColorBalance
	int32_t ___ColorBalance_85;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ColorFilter
	int32_t ___ColorFilter_86;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::HueSatCon
	int32_t ___HueSatCon_87;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Brightness
	int32_t ___Brightness_88;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ChannelMixerRed
	int32_t ___ChannelMixerRed_89;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ChannelMixerGreen
	int32_t ___ChannelMixerGreen_90;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ChannelMixerBlue
	int32_t ___ChannelMixerBlue_91;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Lift
	int32_t ___Lift_92;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::InvGamma
	int32_t ___InvGamma_93;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Gain
	int32_t ___Gain_94;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Curves
	int32_t ___Curves_95;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::CustomToneCurve
	int32_t ___CustomToneCurve_96;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ToeSegmentA
	int32_t ___ToeSegmentA_97;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ToeSegmentB
	int32_t ___ToeSegmentB_98;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MidSegmentA
	int32_t ___MidSegmentA_99;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::MidSegmentB
	int32_t ___MidSegmentB_100;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ShoSegmentA
	int32_t ___ShoSegmentA_101;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::ShoSegmentB
	int32_t ___ShoSegmentB_102;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Color
	int32_t ___Vignette_Color_103;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Center
	int32_t ___Vignette_Center_104;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Settings
	int32_t ___Vignette_Settings_105;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Mask
	int32_t ___Vignette_Mask_106;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Opacity
	int32_t ___Vignette_Opacity_107;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Vignette_Mode
	int32_t ___Vignette_Mode_108;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Grain_Params1
	int32_t ___Grain_Params1_109;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Grain_Params2
	int32_t ___Grain_Params2_110;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::GrainTex
	int32_t ___GrainTex_111;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Phase
	int32_t ___Phase_112;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::GrainNoiseParameters
	int32_t ___GrainNoiseParameters_113;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::LumaInAlpha
	int32_t ___LumaInAlpha_114;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::DitheringTex
	int32_t ___DitheringTex_115;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Dithering_Coords
	int32_t ___Dithering_Coords_116;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::From
	int32_t ___From_117;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::To
	int32_t ___To_118;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::Interp
	int32_t ___Interp_119;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::TargetColor
	int32_t ___TargetColor_120;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::HalfResFinalCopy
	int32_t ___HalfResFinalCopy_121;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::WaveformSource
	int32_t ___WaveformSource_122;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::WaveformBuffer
	int32_t ___WaveformBuffer_123;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::VectorscopeBuffer
	int32_t ___VectorscopeBuffer_124;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::RenderViewportScaleFactor
	int32_t ___RenderViewportScaleFactor_125;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::UVTransform
	int32_t ___UVTransform_126;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::DepthSlice
	int32_t ___DepthSlice_127;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::UVScaleOffset
	int32_t ___UVScaleOffset_128;
	// System.Int32 UnityEngine.Rendering.PostProcessing.ShaderIDs::PosScaleOffset
	int32_t ___PosScaleOffset_129;

public:
	inline static int32_t get_offset_of_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___MainTex_0)); }
	inline int32_t get_MainTex_0() const { return ___MainTex_0; }
	inline int32_t* get_address_of_MainTex_0() { return &___MainTex_0; }
	inline void set_MainTex_0(int32_t value)
	{
		___MainTex_0 = value;
	}

	inline static int32_t get_offset_of_Jitter_1() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Jitter_1)); }
	inline int32_t get_Jitter_1() const { return ___Jitter_1; }
	inline int32_t* get_address_of_Jitter_1() { return &___Jitter_1; }
	inline void set_Jitter_1(int32_t value)
	{
		___Jitter_1 = value;
	}

	inline static int32_t get_offset_of_Sharpness_2() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Sharpness_2)); }
	inline int32_t get_Sharpness_2() const { return ___Sharpness_2; }
	inline int32_t* get_address_of_Sharpness_2() { return &___Sharpness_2; }
	inline void set_Sharpness_2(int32_t value)
	{
		___Sharpness_2 = value;
	}

	inline static int32_t get_offset_of_FinalBlendParameters_3() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___FinalBlendParameters_3)); }
	inline int32_t get_FinalBlendParameters_3() const { return ___FinalBlendParameters_3; }
	inline int32_t* get_address_of_FinalBlendParameters_3() { return &___FinalBlendParameters_3; }
	inline void set_FinalBlendParameters_3(int32_t value)
	{
		___FinalBlendParameters_3 = value;
	}

	inline static int32_t get_offset_of_HistoryTex_4() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___HistoryTex_4)); }
	inline int32_t get_HistoryTex_4() const { return ___HistoryTex_4; }
	inline int32_t* get_address_of_HistoryTex_4() { return &___HistoryTex_4; }
	inline void set_HistoryTex_4(int32_t value)
	{
		___HistoryTex_4 = value;
	}

	inline static int32_t get_offset_of_SMAA_Flip_5() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___SMAA_Flip_5)); }
	inline int32_t get_SMAA_Flip_5() const { return ___SMAA_Flip_5; }
	inline int32_t* get_address_of_SMAA_Flip_5() { return &___SMAA_Flip_5; }
	inline void set_SMAA_Flip_5(int32_t value)
	{
		___SMAA_Flip_5 = value;
	}

	inline static int32_t get_offset_of_SMAA_Flop_6() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___SMAA_Flop_6)); }
	inline int32_t get_SMAA_Flop_6() const { return ___SMAA_Flop_6; }
	inline int32_t* get_address_of_SMAA_Flop_6() { return &___SMAA_Flop_6; }
	inline void set_SMAA_Flop_6(int32_t value)
	{
		___SMAA_Flop_6 = value;
	}

	inline static int32_t get_offset_of_AOParams_7() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___AOParams_7)); }
	inline int32_t get_AOParams_7() const { return ___AOParams_7; }
	inline int32_t* get_address_of_AOParams_7() { return &___AOParams_7; }
	inline void set_AOParams_7(int32_t value)
	{
		___AOParams_7 = value;
	}

	inline static int32_t get_offset_of_AOColor_8() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___AOColor_8)); }
	inline int32_t get_AOColor_8() const { return ___AOColor_8; }
	inline int32_t* get_address_of_AOColor_8() { return &___AOColor_8; }
	inline void set_AOColor_8(int32_t value)
	{
		___AOColor_8 = value;
	}

	inline static int32_t get_offset_of_OcclusionTexture1_9() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___OcclusionTexture1_9)); }
	inline int32_t get_OcclusionTexture1_9() const { return ___OcclusionTexture1_9; }
	inline int32_t* get_address_of_OcclusionTexture1_9() { return &___OcclusionTexture1_9; }
	inline void set_OcclusionTexture1_9(int32_t value)
	{
		___OcclusionTexture1_9 = value;
	}

	inline static int32_t get_offset_of_OcclusionTexture2_10() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___OcclusionTexture2_10)); }
	inline int32_t get_OcclusionTexture2_10() const { return ___OcclusionTexture2_10; }
	inline int32_t* get_address_of_OcclusionTexture2_10() { return &___OcclusionTexture2_10; }
	inline void set_OcclusionTexture2_10(int32_t value)
	{
		___OcclusionTexture2_10 = value;
	}

	inline static int32_t get_offset_of_SAOcclusionTexture_11() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___SAOcclusionTexture_11)); }
	inline int32_t get_SAOcclusionTexture_11() const { return ___SAOcclusionTexture_11; }
	inline int32_t* get_address_of_SAOcclusionTexture_11() { return &___SAOcclusionTexture_11; }
	inline void set_SAOcclusionTexture_11(int32_t value)
	{
		___SAOcclusionTexture_11 = value;
	}

	inline static int32_t get_offset_of_MSVOcclusionTexture_12() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___MSVOcclusionTexture_12)); }
	inline int32_t get_MSVOcclusionTexture_12() const { return ___MSVOcclusionTexture_12; }
	inline int32_t* get_address_of_MSVOcclusionTexture_12() { return &___MSVOcclusionTexture_12; }
	inline void set_MSVOcclusionTexture_12(int32_t value)
	{
		___MSVOcclusionTexture_12 = value;
	}

	inline static int32_t get_offset_of_DepthCopy_13() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___DepthCopy_13)); }
	inline int32_t get_DepthCopy_13() const { return ___DepthCopy_13; }
	inline int32_t* get_address_of_DepthCopy_13() { return &___DepthCopy_13; }
	inline void set_DepthCopy_13(int32_t value)
	{
		___DepthCopy_13 = value;
	}

	inline static int32_t get_offset_of_LinearDepth_14() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___LinearDepth_14)); }
	inline int32_t get_LinearDepth_14() const { return ___LinearDepth_14; }
	inline int32_t* get_address_of_LinearDepth_14() { return &___LinearDepth_14; }
	inline void set_LinearDepth_14(int32_t value)
	{
		___LinearDepth_14 = value;
	}

	inline static int32_t get_offset_of_LowDepth1_15() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___LowDepth1_15)); }
	inline int32_t get_LowDepth1_15() const { return ___LowDepth1_15; }
	inline int32_t* get_address_of_LowDepth1_15() { return &___LowDepth1_15; }
	inline void set_LowDepth1_15(int32_t value)
	{
		___LowDepth1_15 = value;
	}

	inline static int32_t get_offset_of_LowDepth2_16() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___LowDepth2_16)); }
	inline int32_t get_LowDepth2_16() const { return ___LowDepth2_16; }
	inline int32_t* get_address_of_LowDepth2_16() { return &___LowDepth2_16; }
	inline void set_LowDepth2_16(int32_t value)
	{
		___LowDepth2_16 = value;
	}

	inline static int32_t get_offset_of_LowDepth3_17() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___LowDepth3_17)); }
	inline int32_t get_LowDepth3_17() const { return ___LowDepth3_17; }
	inline int32_t* get_address_of_LowDepth3_17() { return &___LowDepth3_17; }
	inline void set_LowDepth3_17(int32_t value)
	{
		___LowDepth3_17 = value;
	}

	inline static int32_t get_offset_of_LowDepth4_18() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___LowDepth4_18)); }
	inline int32_t get_LowDepth4_18() const { return ___LowDepth4_18; }
	inline int32_t* get_address_of_LowDepth4_18() { return &___LowDepth4_18; }
	inline void set_LowDepth4_18(int32_t value)
	{
		___LowDepth4_18 = value;
	}

	inline static int32_t get_offset_of_TiledDepth1_19() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___TiledDepth1_19)); }
	inline int32_t get_TiledDepth1_19() const { return ___TiledDepth1_19; }
	inline int32_t* get_address_of_TiledDepth1_19() { return &___TiledDepth1_19; }
	inline void set_TiledDepth1_19(int32_t value)
	{
		___TiledDepth1_19 = value;
	}

	inline static int32_t get_offset_of_TiledDepth2_20() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___TiledDepth2_20)); }
	inline int32_t get_TiledDepth2_20() const { return ___TiledDepth2_20; }
	inline int32_t* get_address_of_TiledDepth2_20() { return &___TiledDepth2_20; }
	inline void set_TiledDepth2_20(int32_t value)
	{
		___TiledDepth2_20 = value;
	}

	inline static int32_t get_offset_of_TiledDepth3_21() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___TiledDepth3_21)); }
	inline int32_t get_TiledDepth3_21() const { return ___TiledDepth3_21; }
	inline int32_t* get_address_of_TiledDepth3_21() { return &___TiledDepth3_21; }
	inline void set_TiledDepth3_21(int32_t value)
	{
		___TiledDepth3_21 = value;
	}

	inline static int32_t get_offset_of_TiledDepth4_22() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___TiledDepth4_22)); }
	inline int32_t get_TiledDepth4_22() const { return ___TiledDepth4_22; }
	inline int32_t* get_address_of_TiledDepth4_22() { return &___TiledDepth4_22; }
	inline void set_TiledDepth4_22(int32_t value)
	{
		___TiledDepth4_22 = value;
	}

	inline static int32_t get_offset_of_Occlusion1_23() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Occlusion1_23)); }
	inline int32_t get_Occlusion1_23() const { return ___Occlusion1_23; }
	inline int32_t* get_address_of_Occlusion1_23() { return &___Occlusion1_23; }
	inline void set_Occlusion1_23(int32_t value)
	{
		___Occlusion1_23 = value;
	}

	inline static int32_t get_offset_of_Occlusion2_24() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Occlusion2_24)); }
	inline int32_t get_Occlusion2_24() const { return ___Occlusion2_24; }
	inline int32_t* get_address_of_Occlusion2_24() { return &___Occlusion2_24; }
	inline void set_Occlusion2_24(int32_t value)
	{
		___Occlusion2_24 = value;
	}

	inline static int32_t get_offset_of_Occlusion3_25() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Occlusion3_25)); }
	inline int32_t get_Occlusion3_25() const { return ___Occlusion3_25; }
	inline int32_t* get_address_of_Occlusion3_25() { return &___Occlusion3_25; }
	inline void set_Occlusion3_25(int32_t value)
	{
		___Occlusion3_25 = value;
	}

	inline static int32_t get_offset_of_Occlusion4_26() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Occlusion4_26)); }
	inline int32_t get_Occlusion4_26() const { return ___Occlusion4_26; }
	inline int32_t* get_address_of_Occlusion4_26() { return &___Occlusion4_26; }
	inline void set_Occlusion4_26(int32_t value)
	{
		___Occlusion4_26 = value;
	}

	inline static int32_t get_offset_of_Combined1_27() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Combined1_27)); }
	inline int32_t get_Combined1_27() const { return ___Combined1_27; }
	inline int32_t* get_address_of_Combined1_27() { return &___Combined1_27; }
	inline void set_Combined1_27(int32_t value)
	{
		___Combined1_27 = value;
	}

	inline static int32_t get_offset_of_Combined2_28() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Combined2_28)); }
	inline int32_t get_Combined2_28() const { return ___Combined2_28; }
	inline int32_t* get_address_of_Combined2_28() { return &___Combined2_28; }
	inline void set_Combined2_28(int32_t value)
	{
		___Combined2_28 = value;
	}

	inline static int32_t get_offset_of_Combined3_29() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Combined3_29)); }
	inline int32_t get_Combined3_29() const { return ___Combined3_29; }
	inline int32_t* get_address_of_Combined3_29() { return &___Combined3_29; }
	inline void set_Combined3_29(int32_t value)
	{
		___Combined3_29 = value;
	}

	inline static int32_t get_offset_of_SSRResolveTemp_30() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___SSRResolveTemp_30)); }
	inline int32_t get_SSRResolveTemp_30() const { return ___SSRResolveTemp_30; }
	inline int32_t* get_address_of_SSRResolveTemp_30() { return &___SSRResolveTemp_30; }
	inline void set_SSRResolveTemp_30(int32_t value)
	{
		___SSRResolveTemp_30 = value;
	}

	inline static int32_t get_offset_of_Noise_31() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Noise_31)); }
	inline int32_t get_Noise_31() const { return ___Noise_31; }
	inline int32_t* get_address_of_Noise_31() { return &___Noise_31; }
	inline void set_Noise_31(int32_t value)
	{
		___Noise_31 = value;
	}

	inline static int32_t get_offset_of_Test_32() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Test_32)); }
	inline int32_t get_Test_32() const { return ___Test_32; }
	inline int32_t* get_address_of_Test_32() { return &___Test_32; }
	inline void set_Test_32(int32_t value)
	{
		___Test_32 = value;
	}

	inline static int32_t get_offset_of_Resolve_33() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Resolve_33)); }
	inline int32_t get_Resolve_33() const { return ___Resolve_33; }
	inline int32_t* get_address_of_Resolve_33() { return &___Resolve_33; }
	inline void set_Resolve_33(int32_t value)
	{
		___Resolve_33 = value;
	}

	inline static int32_t get_offset_of_History_34() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___History_34)); }
	inline int32_t get_History_34() const { return ___History_34; }
	inline int32_t* get_address_of_History_34() { return &___History_34; }
	inline void set_History_34(int32_t value)
	{
		___History_34 = value;
	}

	inline static int32_t get_offset_of_ViewMatrix_35() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ViewMatrix_35)); }
	inline int32_t get_ViewMatrix_35() const { return ___ViewMatrix_35; }
	inline int32_t* get_address_of_ViewMatrix_35() { return &___ViewMatrix_35; }
	inline void set_ViewMatrix_35(int32_t value)
	{
		___ViewMatrix_35 = value;
	}

	inline static int32_t get_offset_of_InverseViewMatrix_36() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___InverseViewMatrix_36)); }
	inline int32_t get_InverseViewMatrix_36() const { return ___InverseViewMatrix_36; }
	inline int32_t* get_address_of_InverseViewMatrix_36() { return &___InverseViewMatrix_36; }
	inline void set_InverseViewMatrix_36(int32_t value)
	{
		___InverseViewMatrix_36 = value;
	}

	inline static int32_t get_offset_of_InverseProjectionMatrix_37() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___InverseProjectionMatrix_37)); }
	inline int32_t get_InverseProjectionMatrix_37() const { return ___InverseProjectionMatrix_37; }
	inline int32_t* get_address_of_InverseProjectionMatrix_37() { return &___InverseProjectionMatrix_37; }
	inline void set_InverseProjectionMatrix_37(int32_t value)
	{
		___InverseProjectionMatrix_37 = value;
	}

	inline static int32_t get_offset_of_ScreenSpaceProjectionMatrix_38() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ScreenSpaceProjectionMatrix_38)); }
	inline int32_t get_ScreenSpaceProjectionMatrix_38() const { return ___ScreenSpaceProjectionMatrix_38; }
	inline int32_t* get_address_of_ScreenSpaceProjectionMatrix_38() { return &___ScreenSpaceProjectionMatrix_38; }
	inline void set_ScreenSpaceProjectionMatrix_38(int32_t value)
	{
		___ScreenSpaceProjectionMatrix_38 = value;
	}

	inline static int32_t get_offset_of_Params2_39() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Params2_39)); }
	inline int32_t get_Params2_39() const { return ___Params2_39; }
	inline int32_t* get_address_of_Params2_39() { return &___Params2_39; }
	inline void set_Params2_39(int32_t value)
	{
		___Params2_39 = value;
	}

	inline static int32_t get_offset_of_FogColor_40() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___FogColor_40)); }
	inline int32_t get_FogColor_40() const { return ___FogColor_40; }
	inline int32_t* get_address_of_FogColor_40() { return &___FogColor_40; }
	inline void set_FogColor_40(int32_t value)
	{
		___FogColor_40 = value;
	}

	inline static int32_t get_offset_of_FogParams_41() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___FogParams_41)); }
	inline int32_t get_FogParams_41() const { return ___FogParams_41; }
	inline int32_t* get_address_of_FogParams_41() { return &___FogParams_41; }
	inline void set_FogParams_41(int32_t value)
	{
		___FogParams_41 = value;
	}

	inline static int32_t get_offset_of_VelocityScale_42() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___VelocityScale_42)); }
	inline int32_t get_VelocityScale_42() const { return ___VelocityScale_42; }
	inline int32_t* get_address_of_VelocityScale_42() { return &___VelocityScale_42; }
	inline void set_VelocityScale_42(int32_t value)
	{
		___VelocityScale_42 = value;
	}

	inline static int32_t get_offset_of_MaxBlurRadius_43() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___MaxBlurRadius_43)); }
	inline int32_t get_MaxBlurRadius_43() const { return ___MaxBlurRadius_43; }
	inline int32_t* get_address_of_MaxBlurRadius_43() { return &___MaxBlurRadius_43; }
	inline void set_MaxBlurRadius_43(int32_t value)
	{
		___MaxBlurRadius_43 = value;
	}

	inline static int32_t get_offset_of_RcpMaxBlurRadius_44() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___RcpMaxBlurRadius_44)); }
	inline int32_t get_RcpMaxBlurRadius_44() const { return ___RcpMaxBlurRadius_44; }
	inline int32_t* get_address_of_RcpMaxBlurRadius_44() { return &___RcpMaxBlurRadius_44; }
	inline void set_RcpMaxBlurRadius_44(int32_t value)
	{
		___RcpMaxBlurRadius_44 = value;
	}

	inline static int32_t get_offset_of_VelocityTex_45() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___VelocityTex_45)); }
	inline int32_t get_VelocityTex_45() const { return ___VelocityTex_45; }
	inline int32_t* get_address_of_VelocityTex_45() { return &___VelocityTex_45; }
	inline void set_VelocityTex_45(int32_t value)
	{
		___VelocityTex_45 = value;
	}

	inline static int32_t get_offset_of_Tile2RT_46() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Tile2RT_46)); }
	inline int32_t get_Tile2RT_46() const { return ___Tile2RT_46; }
	inline int32_t* get_address_of_Tile2RT_46() { return &___Tile2RT_46; }
	inline void set_Tile2RT_46(int32_t value)
	{
		___Tile2RT_46 = value;
	}

	inline static int32_t get_offset_of_Tile4RT_47() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Tile4RT_47)); }
	inline int32_t get_Tile4RT_47() const { return ___Tile4RT_47; }
	inline int32_t* get_address_of_Tile4RT_47() { return &___Tile4RT_47; }
	inline void set_Tile4RT_47(int32_t value)
	{
		___Tile4RT_47 = value;
	}

	inline static int32_t get_offset_of_Tile8RT_48() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Tile8RT_48)); }
	inline int32_t get_Tile8RT_48() const { return ___Tile8RT_48; }
	inline int32_t* get_address_of_Tile8RT_48() { return &___Tile8RT_48; }
	inline void set_Tile8RT_48(int32_t value)
	{
		___Tile8RT_48 = value;
	}

	inline static int32_t get_offset_of_TileMaxOffs_49() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___TileMaxOffs_49)); }
	inline int32_t get_TileMaxOffs_49() const { return ___TileMaxOffs_49; }
	inline int32_t* get_address_of_TileMaxOffs_49() { return &___TileMaxOffs_49; }
	inline void set_TileMaxOffs_49(int32_t value)
	{
		___TileMaxOffs_49 = value;
	}

	inline static int32_t get_offset_of_TileMaxLoop_50() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___TileMaxLoop_50)); }
	inline int32_t get_TileMaxLoop_50() const { return ___TileMaxLoop_50; }
	inline int32_t* get_address_of_TileMaxLoop_50() { return &___TileMaxLoop_50; }
	inline void set_TileMaxLoop_50(int32_t value)
	{
		___TileMaxLoop_50 = value;
	}

	inline static int32_t get_offset_of_TileVRT_51() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___TileVRT_51)); }
	inline int32_t get_TileVRT_51() const { return ___TileVRT_51; }
	inline int32_t* get_address_of_TileVRT_51() { return &___TileVRT_51; }
	inline void set_TileVRT_51(int32_t value)
	{
		___TileVRT_51 = value;
	}

	inline static int32_t get_offset_of_NeighborMaxTex_52() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___NeighborMaxTex_52)); }
	inline int32_t get_NeighborMaxTex_52() const { return ___NeighborMaxTex_52; }
	inline int32_t* get_address_of_NeighborMaxTex_52() { return &___NeighborMaxTex_52; }
	inline void set_NeighborMaxTex_52(int32_t value)
	{
		___NeighborMaxTex_52 = value;
	}

	inline static int32_t get_offset_of_LoopCount_53() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___LoopCount_53)); }
	inline int32_t get_LoopCount_53() const { return ___LoopCount_53; }
	inline int32_t* get_address_of_LoopCount_53() { return &___LoopCount_53; }
	inline void set_LoopCount_53(int32_t value)
	{
		___LoopCount_53 = value;
	}

	inline static int32_t get_offset_of_DepthOfFieldTemp_54() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___DepthOfFieldTemp_54)); }
	inline int32_t get_DepthOfFieldTemp_54() const { return ___DepthOfFieldTemp_54; }
	inline int32_t* get_address_of_DepthOfFieldTemp_54() { return &___DepthOfFieldTemp_54; }
	inline void set_DepthOfFieldTemp_54(int32_t value)
	{
		___DepthOfFieldTemp_54 = value;
	}

	inline static int32_t get_offset_of_DepthOfFieldTex_55() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___DepthOfFieldTex_55)); }
	inline int32_t get_DepthOfFieldTex_55() const { return ___DepthOfFieldTex_55; }
	inline int32_t* get_address_of_DepthOfFieldTex_55() { return &___DepthOfFieldTex_55; }
	inline void set_DepthOfFieldTex_55(int32_t value)
	{
		___DepthOfFieldTex_55 = value;
	}

	inline static int32_t get_offset_of_Distance_56() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Distance_56)); }
	inline int32_t get_Distance_56() const { return ___Distance_56; }
	inline int32_t* get_address_of_Distance_56() { return &___Distance_56; }
	inline void set_Distance_56(int32_t value)
	{
		___Distance_56 = value;
	}

	inline static int32_t get_offset_of_LensCoeff_57() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___LensCoeff_57)); }
	inline int32_t get_LensCoeff_57() const { return ___LensCoeff_57; }
	inline int32_t* get_address_of_LensCoeff_57() { return &___LensCoeff_57; }
	inline void set_LensCoeff_57(int32_t value)
	{
		___LensCoeff_57 = value;
	}

	inline static int32_t get_offset_of_MaxCoC_58() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___MaxCoC_58)); }
	inline int32_t get_MaxCoC_58() const { return ___MaxCoC_58; }
	inline int32_t* get_address_of_MaxCoC_58() { return &___MaxCoC_58; }
	inline void set_MaxCoC_58(int32_t value)
	{
		___MaxCoC_58 = value;
	}

	inline static int32_t get_offset_of_RcpMaxCoC_59() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___RcpMaxCoC_59)); }
	inline int32_t get_RcpMaxCoC_59() const { return ___RcpMaxCoC_59; }
	inline int32_t* get_address_of_RcpMaxCoC_59() { return &___RcpMaxCoC_59; }
	inline void set_RcpMaxCoC_59(int32_t value)
	{
		___RcpMaxCoC_59 = value;
	}

	inline static int32_t get_offset_of_RcpAspect_60() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___RcpAspect_60)); }
	inline int32_t get_RcpAspect_60() const { return ___RcpAspect_60; }
	inline int32_t* get_address_of_RcpAspect_60() { return &___RcpAspect_60; }
	inline void set_RcpAspect_60(int32_t value)
	{
		___RcpAspect_60 = value;
	}

	inline static int32_t get_offset_of_CoCTex_61() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___CoCTex_61)); }
	inline int32_t get_CoCTex_61() const { return ___CoCTex_61; }
	inline int32_t* get_address_of_CoCTex_61() { return &___CoCTex_61; }
	inline void set_CoCTex_61(int32_t value)
	{
		___CoCTex_61 = value;
	}

	inline static int32_t get_offset_of_TaaParams_62() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___TaaParams_62)); }
	inline int32_t get_TaaParams_62() const { return ___TaaParams_62; }
	inline int32_t* get_address_of_TaaParams_62() { return &___TaaParams_62; }
	inline void set_TaaParams_62(int32_t value)
	{
		___TaaParams_62 = value;
	}

	inline static int32_t get_offset_of_AutoExposureTex_63() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___AutoExposureTex_63)); }
	inline int32_t get_AutoExposureTex_63() const { return ___AutoExposureTex_63; }
	inline int32_t* get_address_of_AutoExposureTex_63() { return &___AutoExposureTex_63; }
	inline void set_AutoExposureTex_63(int32_t value)
	{
		___AutoExposureTex_63 = value;
	}

	inline static int32_t get_offset_of_HistogramBuffer_64() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___HistogramBuffer_64)); }
	inline int32_t get_HistogramBuffer_64() const { return ___HistogramBuffer_64; }
	inline int32_t* get_address_of_HistogramBuffer_64() { return &___HistogramBuffer_64; }
	inline void set_HistogramBuffer_64(int32_t value)
	{
		___HistogramBuffer_64 = value;
	}

	inline static int32_t get_offset_of_Params_65() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Params_65)); }
	inline int32_t get_Params_65() const { return ___Params_65; }
	inline int32_t* get_address_of_Params_65() { return &___Params_65; }
	inline void set_Params_65(int32_t value)
	{
		___Params_65 = value;
	}

	inline static int32_t get_offset_of_ScaleOffsetRes_66() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ScaleOffsetRes_66)); }
	inline int32_t get_ScaleOffsetRes_66() const { return ___ScaleOffsetRes_66; }
	inline int32_t* get_address_of_ScaleOffsetRes_66() { return &___ScaleOffsetRes_66; }
	inline void set_ScaleOffsetRes_66(int32_t value)
	{
		___ScaleOffsetRes_66 = value;
	}

	inline static int32_t get_offset_of_BloomTex_67() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___BloomTex_67)); }
	inline int32_t get_BloomTex_67() const { return ___BloomTex_67; }
	inline int32_t* get_address_of_BloomTex_67() { return &___BloomTex_67; }
	inline void set_BloomTex_67(int32_t value)
	{
		___BloomTex_67 = value;
	}

	inline static int32_t get_offset_of_SampleScale_68() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___SampleScale_68)); }
	inline int32_t get_SampleScale_68() const { return ___SampleScale_68; }
	inline int32_t* get_address_of_SampleScale_68() { return &___SampleScale_68; }
	inline void set_SampleScale_68(int32_t value)
	{
		___SampleScale_68 = value;
	}

	inline static int32_t get_offset_of_Threshold_69() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Threshold_69)); }
	inline int32_t get_Threshold_69() const { return ___Threshold_69; }
	inline int32_t* get_address_of_Threshold_69() { return &___Threshold_69; }
	inline void set_Threshold_69(int32_t value)
	{
		___Threshold_69 = value;
	}

	inline static int32_t get_offset_of_ColorIntensity_70() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ColorIntensity_70)); }
	inline int32_t get_ColorIntensity_70() const { return ___ColorIntensity_70; }
	inline int32_t* get_address_of_ColorIntensity_70() { return &___ColorIntensity_70; }
	inline void set_ColorIntensity_70(int32_t value)
	{
		___ColorIntensity_70 = value;
	}

	inline static int32_t get_offset_of_Bloom_DirtTex_71() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Bloom_DirtTex_71)); }
	inline int32_t get_Bloom_DirtTex_71() const { return ___Bloom_DirtTex_71; }
	inline int32_t* get_address_of_Bloom_DirtTex_71() { return &___Bloom_DirtTex_71; }
	inline void set_Bloom_DirtTex_71(int32_t value)
	{
		___Bloom_DirtTex_71 = value;
	}

	inline static int32_t get_offset_of_Bloom_Settings_72() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Bloom_Settings_72)); }
	inline int32_t get_Bloom_Settings_72() const { return ___Bloom_Settings_72; }
	inline int32_t* get_address_of_Bloom_Settings_72() { return &___Bloom_Settings_72; }
	inline void set_Bloom_Settings_72(int32_t value)
	{
		___Bloom_Settings_72 = value;
	}

	inline static int32_t get_offset_of_Bloom_Color_73() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Bloom_Color_73)); }
	inline int32_t get_Bloom_Color_73() const { return ___Bloom_Color_73; }
	inline int32_t* get_address_of_Bloom_Color_73() { return &___Bloom_Color_73; }
	inline void set_Bloom_Color_73(int32_t value)
	{
		___Bloom_Color_73 = value;
	}

	inline static int32_t get_offset_of_Bloom_DirtTileOffset_74() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Bloom_DirtTileOffset_74)); }
	inline int32_t get_Bloom_DirtTileOffset_74() const { return ___Bloom_DirtTileOffset_74; }
	inline int32_t* get_address_of_Bloom_DirtTileOffset_74() { return &___Bloom_DirtTileOffset_74; }
	inline void set_Bloom_DirtTileOffset_74(int32_t value)
	{
		___Bloom_DirtTileOffset_74 = value;
	}

	inline static int32_t get_offset_of_ChromaticAberration_Amount_75() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ChromaticAberration_Amount_75)); }
	inline int32_t get_ChromaticAberration_Amount_75() const { return ___ChromaticAberration_Amount_75; }
	inline int32_t* get_address_of_ChromaticAberration_Amount_75() { return &___ChromaticAberration_Amount_75; }
	inline void set_ChromaticAberration_Amount_75(int32_t value)
	{
		___ChromaticAberration_Amount_75 = value;
	}

	inline static int32_t get_offset_of_ChromaticAberration_SpectralLut_76() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ChromaticAberration_SpectralLut_76)); }
	inline int32_t get_ChromaticAberration_SpectralLut_76() const { return ___ChromaticAberration_SpectralLut_76; }
	inline int32_t* get_address_of_ChromaticAberration_SpectralLut_76() { return &___ChromaticAberration_SpectralLut_76; }
	inline void set_ChromaticAberration_SpectralLut_76(int32_t value)
	{
		___ChromaticAberration_SpectralLut_76 = value;
	}

	inline static int32_t get_offset_of_Distortion_CenterScale_77() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Distortion_CenterScale_77)); }
	inline int32_t get_Distortion_CenterScale_77() const { return ___Distortion_CenterScale_77; }
	inline int32_t* get_address_of_Distortion_CenterScale_77() { return &___Distortion_CenterScale_77; }
	inline void set_Distortion_CenterScale_77(int32_t value)
	{
		___Distortion_CenterScale_77 = value;
	}

	inline static int32_t get_offset_of_Distortion_Amount_78() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Distortion_Amount_78)); }
	inline int32_t get_Distortion_Amount_78() const { return ___Distortion_Amount_78; }
	inline int32_t* get_address_of_Distortion_Amount_78() { return &___Distortion_Amount_78; }
	inline void set_Distortion_Amount_78(int32_t value)
	{
		___Distortion_Amount_78 = value;
	}

	inline static int32_t get_offset_of_Lut2D_79() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Lut2D_79)); }
	inline int32_t get_Lut2D_79() const { return ___Lut2D_79; }
	inline int32_t* get_address_of_Lut2D_79() { return &___Lut2D_79; }
	inline void set_Lut2D_79(int32_t value)
	{
		___Lut2D_79 = value;
	}

	inline static int32_t get_offset_of_Lut3D_80() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Lut3D_80)); }
	inline int32_t get_Lut3D_80() const { return ___Lut3D_80; }
	inline int32_t* get_address_of_Lut3D_80() { return &___Lut3D_80; }
	inline void set_Lut3D_80(int32_t value)
	{
		___Lut3D_80 = value;
	}

	inline static int32_t get_offset_of_Lut3D_Params_81() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Lut3D_Params_81)); }
	inline int32_t get_Lut3D_Params_81() const { return ___Lut3D_Params_81; }
	inline int32_t* get_address_of_Lut3D_Params_81() { return &___Lut3D_Params_81; }
	inline void set_Lut3D_Params_81(int32_t value)
	{
		___Lut3D_Params_81 = value;
	}

	inline static int32_t get_offset_of_Lut2D_Params_82() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Lut2D_Params_82)); }
	inline int32_t get_Lut2D_Params_82() const { return ___Lut2D_Params_82; }
	inline int32_t* get_address_of_Lut2D_Params_82() { return &___Lut2D_Params_82; }
	inline void set_Lut2D_Params_82(int32_t value)
	{
		___Lut2D_Params_82 = value;
	}

	inline static int32_t get_offset_of_UserLut2D_Params_83() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___UserLut2D_Params_83)); }
	inline int32_t get_UserLut2D_Params_83() const { return ___UserLut2D_Params_83; }
	inline int32_t* get_address_of_UserLut2D_Params_83() { return &___UserLut2D_Params_83; }
	inline void set_UserLut2D_Params_83(int32_t value)
	{
		___UserLut2D_Params_83 = value;
	}

	inline static int32_t get_offset_of_PostExposure_84() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___PostExposure_84)); }
	inline int32_t get_PostExposure_84() const { return ___PostExposure_84; }
	inline int32_t* get_address_of_PostExposure_84() { return &___PostExposure_84; }
	inline void set_PostExposure_84(int32_t value)
	{
		___PostExposure_84 = value;
	}

	inline static int32_t get_offset_of_ColorBalance_85() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ColorBalance_85)); }
	inline int32_t get_ColorBalance_85() const { return ___ColorBalance_85; }
	inline int32_t* get_address_of_ColorBalance_85() { return &___ColorBalance_85; }
	inline void set_ColorBalance_85(int32_t value)
	{
		___ColorBalance_85 = value;
	}

	inline static int32_t get_offset_of_ColorFilter_86() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ColorFilter_86)); }
	inline int32_t get_ColorFilter_86() const { return ___ColorFilter_86; }
	inline int32_t* get_address_of_ColorFilter_86() { return &___ColorFilter_86; }
	inline void set_ColorFilter_86(int32_t value)
	{
		___ColorFilter_86 = value;
	}

	inline static int32_t get_offset_of_HueSatCon_87() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___HueSatCon_87)); }
	inline int32_t get_HueSatCon_87() const { return ___HueSatCon_87; }
	inline int32_t* get_address_of_HueSatCon_87() { return &___HueSatCon_87; }
	inline void set_HueSatCon_87(int32_t value)
	{
		___HueSatCon_87 = value;
	}

	inline static int32_t get_offset_of_Brightness_88() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Brightness_88)); }
	inline int32_t get_Brightness_88() const { return ___Brightness_88; }
	inline int32_t* get_address_of_Brightness_88() { return &___Brightness_88; }
	inline void set_Brightness_88(int32_t value)
	{
		___Brightness_88 = value;
	}

	inline static int32_t get_offset_of_ChannelMixerRed_89() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ChannelMixerRed_89)); }
	inline int32_t get_ChannelMixerRed_89() const { return ___ChannelMixerRed_89; }
	inline int32_t* get_address_of_ChannelMixerRed_89() { return &___ChannelMixerRed_89; }
	inline void set_ChannelMixerRed_89(int32_t value)
	{
		___ChannelMixerRed_89 = value;
	}

	inline static int32_t get_offset_of_ChannelMixerGreen_90() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ChannelMixerGreen_90)); }
	inline int32_t get_ChannelMixerGreen_90() const { return ___ChannelMixerGreen_90; }
	inline int32_t* get_address_of_ChannelMixerGreen_90() { return &___ChannelMixerGreen_90; }
	inline void set_ChannelMixerGreen_90(int32_t value)
	{
		___ChannelMixerGreen_90 = value;
	}

	inline static int32_t get_offset_of_ChannelMixerBlue_91() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ChannelMixerBlue_91)); }
	inline int32_t get_ChannelMixerBlue_91() const { return ___ChannelMixerBlue_91; }
	inline int32_t* get_address_of_ChannelMixerBlue_91() { return &___ChannelMixerBlue_91; }
	inline void set_ChannelMixerBlue_91(int32_t value)
	{
		___ChannelMixerBlue_91 = value;
	}

	inline static int32_t get_offset_of_Lift_92() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Lift_92)); }
	inline int32_t get_Lift_92() const { return ___Lift_92; }
	inline int32_t* get_address_of_Lift_92() { return &___Lift_92; }
	inline void set_Lift_92(int32_t value)
	{
		___Lift_92 = value;
	}

	inline static int32_t get_offset_of_InvGamma_93() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___InvGamma_93)); }
	inline int32_t get_InvGamma_93() const { return ___InvGamma_93; }
	inline int32_t* get_address_of_InvGamma_93() { return &___InvGamma_93; }
	inline void set_InvGamma_93(int32_t value)
	{
		___InvGamma_93 = value;
	}

	inline static int32_t get_offset_of_Gain_94() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Gain_94)); }
	inline int32_t get_Gain_94() const { return ___Gain_94; }
	inline int32_t* get_address_of_Gain_94() { return &___Gain_94; }
	inline void set_Gain_94(int32_t value)
	{
		___Gain_94 = value;
	}

	inline static int32_t get_offset_of_Curves_95() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Curves_95)); }
	inline int32_t get_Curves_95() const { return ___Curves_95; }
	inline int32_t* get_address_of_Curves_95() { return &___Curves_95; }
	inline void set_Curves_95(int32_t value)
	{
		___Curves_95 = value;
	}

	inline static int32_t get_offset_of_CustomToneCurve_96() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___CustomToneCurve_96)); }
	inline int32_t get_CustomToneCurve_96() const { return ___CustomToneCurve_96; }
	inline int32_t* get_address_of_CustomToneCurve_96() { return &___CustomToneCurve_96; }
	inline void set_CustomToneCurve_96(int32_t value)
	{
		___CustomToneCurve_96 = value;
	}

	inline static int32_t get_offset_of_ToeSegmentA_97() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ToeSegmentA_97)); }
	inline int32_t get_ToeSegmentA_97() const { return ___ToeSegmentA_97; }
	inline int32_t* get_address_of_ToeSegmentA_97() { return &___ToeSegmentA_97; }
	inline void set_ToeSegmentA_97(int32_t value)
	{
		___ToeSegmentA_97 = value;
	}

	inline static int32_t get_offset_of_ToeSegmentB_98() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ToeSegmentB_98)); }
	inline int32_t get_ToeSegmentB_98() const { return ___ToeSegmentB_98; }
	inline int32_t* get_address_of_ToeSegmentB_98() { return &___ToeSegmentB_98; }
	inline void set_ToeSegmentB_98(int32_t value)
	{
		___ToeSegmentB_98 = value;
	}

	inline static int32_t get_offset_of_MidSegmentA_99() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___MidSegmentA_99)); }
	inline int32_t get_MidSegmentA_99() const { return ___MidSegmentA_99; }
	inline int32_t* get_address_of_MidSegmentA_99() { return &___MidSegmentA_99; }
	inline void set_MidSegmentA_99(int32_t value)
	{
		___MidSegmentA_99 = value;
	}

	inline static int32_t get_offset_of_MidSegmentB_100() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___MidSegmentB_100)); }
	inline int32_t get_MidSegmentB_100() const { return ___MidSegmentB_100; }
	inline int32_t* get_address_of_MidSegmentB_100() { return &___MidSegmentB_100; }
	inline void set_MidSegmentB_100(int32_t value)
	{
		___MidSegmentB_100 = value;
	}

	inline static int32_t get_offset_of_ShoSegmentA_101() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ShoSegmentA_101)); }
	inline int32_t get_ShoSegmentA_101() const { return ___ShoSegmentA_101; }
	inline int32_t* get_address_of_ShoSegmentA_101() { return &___ShoSegmentA_101; }
	inline void set_ShoSegmentA_101(int32_t value)
	{
		___ShoSegmentA_101 = value;
	}

	inline static int32_t get_offset_of_ShoSegmentB_102() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___ShoSegmentB_102)); }
	inline int32_t get_ShoSegmentB_102() const { return ___ShoSegmentB_102; }
	inline int32_t* get_address_of_ShoSegmentB_102() { return &___ShoSegmentB_102; }
	inline void set_ShoSegmentB_102(int32_t value)
	{
		___ShoSegmentB_102 = value;
	}

	inline static int32_t get_offset_of_Vignette_Color_103() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Vignette_Color_103)); }
	inline int32_t get_Vignette_Color_103() const { return ___Vignette_Color_103; }
	inline int32_t* get_address_of_Vignette_Color_103() { return &___Vignette_Color_103; }
	inline void set_Vignette_Color_103(int32_t value)
	{
		___Vignette_Color_103 = value;
	}

	inline static int32_t get_offset_of_Vignette_Center_104() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Vignette_Center_104)); }
	inline int32_t get_Vignette_Center_104() const { return ___Vignette_Center_104; }
	inline int32_t* get_address_of_Vignette_Center_104() { return &___Vignette_Center_104; }
	inline void set_Vignette_Center_104(int32_t value)
	{
		___Vignette_Center_104 = value;
	}

	inline static int32_t get_offset_of_Vignette_Settings_105() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Vignette_Settings_105)); }
	inline int32_t get_Vignette_Settings_105() const { return ___Vignette_Settings_105; }
	inline int32_t* get_address_of_Vignette_Settings_105() { return &___Vignette_Settings_105; }
	inline void set_Vignette_Settings_105(int32_t value)
	{
		___Vignette_Settings_105 = value;
	}

	inline static int32_t get_offset_of_Vignette_Mask_106() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Vignette_Mask_106)); }
	inline int32_t get_Vignette_Mask_106() const { return ___Vignette_Mask_106; }
	inline int32_t* get_address_of_Vignette_Mask_106() { return &___Vignette_Mask_106; }
	inline void set_Vignette_Mask_106(int32_t value)
	{
		___Vignette_Mask_106 = value;
	}

	inline static int32_t get_offset_of_Vignette_Opacity_107() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Vignette_Opacity_107)); }
	inline int32_t get_Vignette_Opacity_107() const { return ___Vignette_Opacity_107; }
	inline int32_t* get_address_of_Vignette_Opacity_107() { return &___Vignette_Opacity_107; }
	inline void set_Vignette_Opacity_107(int32_t value)
	{
		___Vignette_Opacity_107 = value;
	}

	inline static int32_t get_offset_of_Vignette_Mode_108() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Vignette_Mode_108)); }
	inline int32_t get_Vignette_Mode_108() const { return ___Vignette_Mode_108; }
	inline int32_t* get_address_of_Vignette_Mode_108() { return &___Vignette_Mode_108; }
	inline void set_Vignette_Mode_108(int32_t value)
	{
		___Vignette_Mode_108 = value;
	}

	inline static int32_t get_offset_of_Grain_Params1_109() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Grain_Params1_109)); }
	inline int32_t get_Grain_Params1_109() const { return ___Grain_Params1_109; }
	inline int32_t* get_address_of_Grain_Params1_109() { return &___Grain_Params1_109; }
	inline void set_Grain_Params1_109(int32_t value)
	{
		___Grain_Params1_109 = value;
	}

	inline static int32_t get_offset_of_Grain_Params2_110() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Grain_Params2_110)); }
	inline int32_t get_Grain_Params2_110() const { return ___Grain_Params2_110; }
	inline int32_t* get_address_of_Grain_Params2_110() { return &___Grain_Params2_110; }
	inline void set_Grain_Params2_110(int32_t value)
	{
		___Grain_Params2_110 = value;
	}

	inline static int32_t get_offset_of_GrainTex_111() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___GrainTex_111)); }
	inline int32_t get_GrainTex_111() const { return ___GrainTex_111; }
	inline int32_t* get_address_of_GrainTex_111() { return &___GrainTex_111; }
	inline void set_GrainTex_111(int32_t value)
	{
		___GrainTex_111 = value;
	}

	inline static int32_t get_offset_of_Phase_112() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Phase_112)); }
	inline int32_t get_Phase_112() const { return ___Phase_112; }
	inline int32_t* get_address_of_Phase_112() { return &___Phase_112; }
	inline void set_Phase_112(int32_t value)
	{
		___Phase_112 = value;
	}

	inline static int32_t get_offset_of_GrainNoiseParameters_113() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___GrainNoiseParameters_113)); }
	inline int32_t get_GrainNoiseParameters_113() const { return ___GrainNoiseParameters_113; }
	inline int32_t* get_address_of_GrainNoiseParameters_113() { return &___GrainNoiseParameters_113; }
	inline void set_GrainNoiseParameters_113(int32_t value)
	{
		___GrainNoiseParameters_113 = value;
	}

	inline static int32_t get_offset_of_LumaInAlpha_114() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___LumaInAlpha_114)); }
	inline int32_t get_LumaInAlpha_114() const { return ___LumaInAlpha_114; }
	inline int32_t* get_address_of_LumaInAlpha_114() { return &___LumaInAlpha_114; }
	inline void set_LumaInAlpha_114(int32_t value)
	{
		___LumaInAlpha_114 = value;
	}

	inline static int32_t get_offset_of_DitheringTex_115() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___DitheringTex_115)); }
	inline int32_t get_DitheringTex_115() const { return ___DitheringTex_115; }
	inline int32_t* get_address_of_DitheringTex_115() { return &___DitheringTex_115; }
	inline void set_DitheringTex_115(int32_t value)
	{
		___DitheringTex_115 = value;
	}

	inline static int32_t get_offset_of_Dithering_Coords_116() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Dithering_Coords_116)); }
	inline int32_t get_Dithering_Coords_116() const { return ___Dithering_Coords_116; }
	inline int32_t* get_address_of_Dithering_Coords_116() { return &___Dithering_Coords_116; }
	inline void set_Dithering_Coords_116(int32_t value)
	{
		___Dithering_Coords_116 = value;
	}

	inline static int32_t get_offset_of_From_117() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___From_117)); }
	inline int32_t get_From_117() const { return ___From_117; }
	inline int32_t* get_address_of_From_117() { return &___From_117; }
	inline void set_From_117(int32_t value)
	{
		___From_117 = value;
	}

	inline static int32_t get_offset_of_To_118() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___To_118)); }
	inline int32_t get_To_118() const { return ___To_118; }
	inline int32_t* get_address_of_To_118() { return &___To_118; }
	inline void set_To_118(int32_t value)
	{
		___To_118 = value;
	}

	inline static int32_t get_offset_of_Interp_119() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___Interp_119)); }
	inline int32_t get_Interp_119() const { return ___Interp_119; }
	inline int32_t* get_address_of_Interp_119() { return &___Interp_119; }
	inline void set_Interp_119(int32_t value)
	{
		___Interp_119 = value;
	}

	inline static int32_t get_offset_of_TargetColor_120() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___TargetColor_120)); }
	inline int32_t get_TargetColor_120() const { return ___TargetColor_120; }
	inline int32_t* get_address_of_TargetColor_120() { return &___TargetColor_120; }
	inline void set_TargetColor_120(int32_t value)
	{
		___TargetColor_120 = value;
	}

	inline static int32_t get_offset_of_HalfResFinalCopy_121() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___HalfResFinalCopy_121)); }
	inline int32_t get_HalfResFinalCopy_121() const { return ___HalfResFinalCopy_121; }
	inline int32_t* get_address_of_HalfResFinalCopy_121() { return &___HalfResFinalCopy_121; }
	inline void set_HalfResFinalCopy_121(int32_t value)
	{
		___HalfResFinalCopy_121 = value;
	}

	inline static int32_t get_offset_of_WaveformSource_122() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___WaveformSource_122)); }
	inline int32_t get_WaveformSource_122() const { return ___WaveformSource_122; }
	inline int32_t* get_address_of_WaveformSource_122() { return &___WaveformSource_122; }
	inline void set_WaveformSource_122(int32_t value)
	{
		___WaveformSource_122 = value;
	}

	inline static int32_t get_offset_of_WaveformBuffer_123() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___WaveformBuffer_123)); }
	inline int32_t get_WaveformBuffer_123() const { return ___WaveformBuffer_123; }
	inline int32_t* get_address_of_WaveformBuffer_123() { return &___WaveformBuffer_123; }
	inline void set_WaveformBuffer_123(int32_t value)
	{
		___WaveformBuffer_123 = value;
	}

	inline static int32_t get_offset_of_VectorscopeBuffer_124() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___VectorscopeBuffer_124)); }
	inline int32_t get_VectorscopeBuffer_124() const { return ___VectorscopeBuffer_124; }
	inline int32_t* get_address_of_VectorscopeBuffer_124() { return &___VectorscopeBuffer_124; }
	inline void set_VectorscopeBuffer_124(int32_t value)
	{
		___VectorscopeBuffer_124 = value;
	}

	inline static int32_t get_offset_of_RenderViewportScaleFactor_125() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___RenderViewportScaleFactor_125)); }
	inline int32_t get_RenderViewportScaleFactor_125() const { return ___RenderViewportScaleFactor_125; }
	inline int32_t* get_address_of_RenderViewportScaleFactor_125() { return &___RenderViewportScaleFactor_125; }
	inline void set_RenderViewportScaleFactor_125(int32_t value)
	{
		___RenderViewportScaleFactor_125 = value;
	}

	inline static int32_t get_offset_of_UVTransform_126() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___UVTransform_126)); }
	inline int32_t get_UVTransform_126() const { return ___UVTransform_126; }
	inline int32_t* get_address_of_UVTransform_126() { return &___UVTransform_126; }
	inline void set_UVTransform_126(int32_t value)
	{
		___UVTransform_126 = value;
	}

	inline static int32_t get_offset_of_DepthSlice_127() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___DepthSlice_127)); }
	inline int32_t get_DepthSlice_127() const { return ___DepthSlice_127; }
	inline int32_t* get_address_of_DepthSlice_127() { return &___DepthSlice_127; }
	inline void set_DepthSlice_127(int32_t value)
	{
		___DepthSlice_127 = value;
	}

	inline static int32_t get_offset_of_UVScaleOffset_128() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___UVScaleOffset_128)); }
	inline int32_t get_UVScaleOffset_128() const { return ___UVScaleOffset_128; }
	inline int32_t* get_address_of_UVScaleOffset_128() { return &___UVScaleOffset_128; }
	inline void set_UVScaleOffset_128(int32_t value)
	{
		___UVScaleOffset_128 = value;
	}

	inline static int32_t get_offset_of_PosScaleOffset_129() { return static_cast<int32_t>(offsetof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields, ___PosScaleOffset_129)); }
	inline int32_t get_PosScaleOffset_129() const { return ___PosScaleOffset_129; }
	inline int32_t* get_address_of_PosScaleOffset_129() { return &___PosScaleOffset_129; }
	inline void set_PosScaleOffset_129(int32_t value)
	{
		___PosScaleOffset_129 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERIDS_TF2DD07006369DE1FD0D337C7CF4D2F6849F81721_H
#ifndef SPLINE_T42213D550262BA4C9B574C062AB52021A970081F_H
#define SPLINE_T42213D550262BA4C9B574C062AB52021A970081F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Spline
struct  Spline_t42213D550262BA4C9B574C062AB52021A970081F  : public RuntimeObject
{
public:
	// UnityEngine.AnimationCurve UnityEngine.Rendering.PostProcessing.Spline::curve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___curve_0;
	// System.Boolean UnityEngine.Rendering.PostProcessing.Spline::m_Loop
	bool ___m_Loop_1;
	// System.Single UnityEngine.Rendering.PostProcessing.Spline::m_ZeroValue
	float ___m_ZeroValue_2;
	// System.Single UnityEngine.Rendering.PostProcessing.Spline::m_Range
	float ___m_Range_3;
	// UnityEngine.AnimationCurve UnityEngine.Rendering.PostProcessing.Spline::m_InternalLoopingCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_InternalLoopingCurve_4;
	// System.Int32 UnityEngine.Rendering.PostProcessing.Spline::frameCount
	int32_t ___frameCount_5;
	// System.Single[] UnityEngine.Rendering.PostProcessing.Spline::cachedData
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___cachedData_6;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(Spline_t42213D550262BA4C9B574C062AB52021A970081F, ___curve_0)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_curve_0() const { return ___curve_0; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}

	inline static int32_t get_offset_of_m_Loop_1() { return static_cast<int32_t>(offsetof(Spline_t42213D550262BA4C9B574C062AB52021A970081F, ___m_Loop_1)); }
	inline bool get_m_Loop_1() const { return ___m_Loop_1; }
	inline bool* get_address_of_m_Loop_1() { return &___m_Loop_1; }
	inline void set_m_Loop_1(bool value)
	{
		___m_Loop_1 = value;
	}

	inline static int32_t get_offset_of_m_ZeroValue_2() { return static_cast<int32_t>(offsetof(Spline_t42213D550262BA4C9B574C062AB52021A970081F, ___m_ZeroValue_2)); }
	inline float get_m_ZeroValue_2() const { return ___m_ZeroValue_2; }
	inline float* get_address_of_m_ZeroValue_2() { return &___m_ZeroValue_2; }
	inline void set_m_ZeroValue_2(float value)
	{
		___m_ZeroValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Range_3() { return static_cast<int32_t>(offsetof(Spline_t42213D550262BA4C9B574C062AB52021A970081F, ___m_Range_3)); }
	inline float get_m_Range_3() const { return ___m_Range_3; }
	inline float* get_address_of_m_Range_3() { return &___m_Range_3; }
	inline void set_m_Range_3(float value)
	{
		___m_Range_3 = value;
	}

	inline static int32_t get_offset_of_m_InternalLoopingCurve_4() { return static_cast<int32_t>(offsetof(Spline_t42213D550262BA4C9B574C062AB52021A970081F, ___m_InternalLoopingCurve_4)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_InternalLoopingCurve_4() const { return ___m_InternalLoopingCurve_4; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_InternalLoopingCurve_4() { return &___m_InternalLoopingCurve_4; }
	inline void set_m_InternalLoopingCurve_4(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_InternalLoopingCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalLoopingCurve_4), value);
	}

	inline static int32_t get_offset_of_frameCount_5() { return static_cast<int32_t>(offsetof(Spline_t42213D550262BA4C9B574C062AB52021A970081F, ___frameCount_5)); }
	inline int32_t get_frameCount_5() const { return ___frameCount_5; }
	inline int32_t* get_address_of_frameCount_5() { return &___frameCount_5; }
	inline void set_frameCount_5(int32_t value)
	{
		___frameCount_5 = value;
	}

	inline static int32_t get_offset_of_cachedData_6() { return static_cast<int32_t>(offsetof(Spline_t42213D550262BA4C9B574C062AB52021A970081F, ___cachedData_6)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_cachedData_6() const { return ___cachedData_6; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_cachedData_6() { return &___cachedData_6; }
	inline void set_cachedData_6(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___cachedData_6 = value;
		Il2CppCodeGenWriteBarrier((&___cachedData_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINE_T42213D550262BA4C9B574C062AB52021A970081F_H
#ifndef TARGETPOOL_T974506C6CF46650F99CDC2EC4E34EC6B5E119949_H
#define TARGETPOOL_T974506C6CF46650F99CDC2EC4E34EC6B5E119949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TargetPool
struct  TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Rendering.PostProcessing.TargetPool::m_Pool
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_Pool_0;
	// System.Int32 UnityEngine.Rendering.PostProcessing.TargetPool::m_Current
	int32_t ___m_Current_1;

public:
	inline static int32_t get_offset_of_m_Pool_0() { return static_cast<int32_t>(offsetof(TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949, ___m_Pool_0)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_Pool_0() const { return ___m_Pool_0; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_Pool_0() { return &___m_Pool_0; }
	inline void set_m_Pool_0(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_Pool_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pool_0), value);
	}

	inline static int32_t get_offset_of_m_Current_1() { return static_cast<int32_t>(offsetof(TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949, ___m_Current_1)); }
	inline int32_t get_m_Current_1() const { return ___m_Current_1; }
	inline int32_t* get_address_of_m_Current_1() { return &___m_Current_1; }
	inline void set_m_Current_1(int32_t value)
	{
		___m_Current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETPOOL_T974506C6CF46650F99CDC2EC4E34EC6B5E119949_H
#ifndef TEXTUREFORMATUTILITIES_TFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_H
#define TEXTUREFORMATUTILITIES_TFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TextureFormatUtilities
struct  TextureFormatUtilities_tFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2  : public RuntimeObject
{
public:

public:
};

struct TextureFormatUtilities_tFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.RenderTextureFormat> UnityEngine.Rendering.PostProcessing.TextureFormatUtilities::s_FormatAliasMap
	Dictionary_2_t1378DEC1682A37F31C0E17122CBF0437EFF78110 * ___s_FormatAliasMap_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> UnityEngine.Rendering.PostProcessing.TextureFormatUtilities::s_SupportedRenderTextureFormats
	Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * ___s_SupportedRenderTextureFormats_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> UnityEngine.Rendering.PostProcessing.TextureFormatUtilities::s_SupportedTextureFormats
	Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * ___s_SupportedTextureFormats_2;

public:
	inline static int32_t get_offset_of_s_FormatAliasMap_0() { return static_cast<int32_t>(offsetof(TextureFormatUtilities_tFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_StaticFields, ___s_FormatAliasMap_0)); }
	inline Dictionary_2_t1378DEC1682A37F31C0E17122CBF0437EFF78110 * get_s_FormatAliasMap_0() const { return ___s_FormatAliasMap_0; }
	inline Dictionary_2_t1378DEC1682A37F31C0E17122CBF0437EFF78110 ** get_address_of_s_FormatAliasMap_0() { return &___s_FormatAliasMap_0; }
	inline void set_s_FormatAliasMap_0(Dictionary_2_t1378DEC1682A37F31C0E17122CBF0437EFF78110 * value)
	{
		___s_FormatAliasMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_FormatAliasMap_0), value);
	}

	inline static int32_t get_offset_of_s_SupportedRenderTextureFormats_1() { return static_cast<int32_t>(offsetof(TextureFormatUtilities_tFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_StaticFields, ___s_SupportedRenderTextureFormats_1)); }
	inline Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * get_s_SupportedRenderTextureFormats_1() const { return ___s_SupportedRenderTextureFormats_1; }
	inline Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 ** get_address_of_s_SupportedRenderTextureFormats_1() { return &___s_SupportedRenderTextureFormats_1; }
	inline void set_s_SupportedRenderTextureFormats_1(Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * value)
	{
		___s_SupportedRenderTextureFormats_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_SupportedRenderTextureFormats_1), value);
	}

	inline static int32_t get_offset_of_s_SupportedTextureFormats_2() { return static_cast<int32_t>(offsetof(TextureFormatUtilities_tFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_StaticFields, ___s_SupportedTextureFormats_2)); }
	inline Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * get_s_SupportedTextureFormats_2() const { return ___s_SupportedTextureFormats_2; }
	inline Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 ** get_address_of_s_SupportedTextureFormats_2() { return &___s_SupportedTextureFormats_2; }
	inline void set_s_SupportedTextureFormats_2(Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * value)
	{
		___s_SupportedTextureFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_SupportedTextureFormats_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMATUTILITIES_TFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_H
#ifndef TEXTURELERPER_T959B5D2D8F95C3C99FA908490054E5019A61AA83_H
#define TEXTURELERPER_T959B5D2D8F95C3C99FA908490054E5019A61AA83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TextureLerper
struct  TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.TextureLerper::m_Command
	CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * ___m_Command_1;
	// UnityEngine.Rendering.PostProcessing.PropertySheetFactory UnityEngine.Rendering.PostProcessing.TextureLerper::m_PropertySheets
	PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 * ___m_PropertySheets_2;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources UnityEngine.Rendering.PostProcessing.TextureLerper::m_Resources
	PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * ___m_Resources_3;
	// System.Collections.Generic.List`1<UnityEngine.RenderTexture> UnityEngine.Rendering.PostProcessing.TextureLerper::m_Recycled
	List_1_tE7D42879B6DB15943660C83DE3C7C11F23A535C6 * ___m_Recycled_4;
	// System.Collections.Generic.List`1<UnityEngine.RenderTexture> UnityEngine.Rendering.PostProcessing.TextureLerper::m_Actives
	List_1_tE7D42879B6DB15943660C83DE3C7C11F23A535C6 * ___m_Actives_5;

public:
	inline static int32_t get_offset_of_m_Command_1() { return static_cast<int32_t>(offsetof(TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83, ___m_Command_1)); }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * get_m_Command_1() const { return ___m_Command_1; }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD ** get_address_of_m_Command_1() { return &___m_Command_1; }
	inline void set_m_Command_1(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * value)
	{
		___m_Command_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Command_1), value);
	}

	inline static int32_t get_offset_of_m_PropertySheets_2() { return static_cast<int32_t>(offsetof(TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83, ___m_PropertySheets_2)); }
	inline PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 * get_m_PropertySheets_2() const { return ___m_PropertySheets_2; }
	inline PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 ** get_address_of_m_PropertySheets_2() { return &___m_PropertySheets_2; }
	inline void set_m_PropertySheets_2(PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 * value)
	{
		___m_PropertySheets_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropertySheets_2), value);
	}

	inline static int32_t get_offset_of_m_Resources_3() { return static_cast<int32_t>(offsetof(TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83, ___m_Resources_3)); }
	inline PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * get_m_Resources_3() const { return ___m_Resources_3; }
	inline PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B ** get_address_of_m_Resources_3() { return &___m_Resources_3; }
	inline void set_m_Resources_3(PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * value)
	{
		___m_Resources_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Resources_3), value);
	}

	inline static int32_t get_offset_of_m_Recycled_4() { return static_cast<int32_t>(offsetof(TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83, ___m_Recycled_4)); }
	inline List_1_tE7D42879B6DB15943660C83DE3C7C11F23A535C6 * get_m_Recycled_4() const { return ___m_Recycled_4; }
	inline List_1_tE7D42879B6DB15943660C83DE3C7C11F23A535C6 ** get_address_of_m_Recycled_4() { return &___m_Recycled_4; }
	inline void set_m_Recycled_4(List_1_tE7D42879B6DB15943660C83DE3C7C11F23A535C6 * value)
	{
		___m_Recycled_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Recycled_4), value);
	}

	inline static int32_t get_offset_of_m_Actives_5() { return static_cast<int32_t>(offsetof(TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83, ___m_Actives_5)); }
	inline List_1_tE7D42879B6DB15943660C83DE3C7C11F23A535C6 * get_m_Actives_5() const { return ___m_Actives_5; }
	inline List_1_tE7D42879B6DB15943660C83DE3C7C11F23A535C6 ** get_address_of_m_Actives_5() { return &___m_Actives_5; }
	inline void set_m_Actives_5(List_1_tE7D42879B6DB15943660C83DE3C7C11F23A535C6 * value)
	{
		___m_Actives_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Actives_5), value);
	}
};

struct TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83_StaticFields
{
public:
	// UnityEngine.Rendering.PostProcessing.TextureLerper UnityEngine.Rendering.PostProcessing.TextureLerper::m_Instance
	TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83 * ___m_Instance_0;

public:
	inline static int32_t get_offset_of_m_Instance_0() { return static_cast<int32_t>(offsetof(TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83_StaticFields, ___m_Instance_0)); }
	inline TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83 * get_m_Instance_0() const { return ___m_Instance_0; }
	inline TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83 ** get_address_of_m_Instance_0() { return &___m_Instance_0; }
	inline void set_m_Instance_0(TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83 * value)
	{
		___m_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURELERPER_T959B5D2D8F95C3C99FA908490054E5019A61AA83_H
#ifndef RUNTIMEOPENSOURCEINITIALIZER_T81785D24983BCE4C95906F20541AD7A3D3D11862_H
#define RUNTIMEOPENSOURCEINITIALIZER_T81785D24983BCE4C95906F20541AD7A3D3D11862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.RuntimeOpenSourceInitializer
struct  RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862  : public RuntimeObject
{
public:

public:
};

struct RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862_StaticFields
{
public:
	// Vuforia.UnityCompiled.IUnityCompiledFacade Vuforia.UnityCompiled.RuntimeOpenSourceInitializer::sFacade
	RuntimeObject* ___sFacade_0;

public:
	inline static int32_t get_offset_of_sFacade_0() { return static_cast<int32_t>(offsetof(RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862_StaticFields, ___sFacade_0)); }
	inline RuntimeObject* get_sFacade_0() const { return ___sFacade_0; }
	inline RuntimeObject** get_address_of_sFacade_0() { return &___sFacade_0; }
	inline void set_sFacade_0(RuntimeObject* value)
	{
		___sFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&___sFacade_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOPENSOURCEINITIALIZER_T81785D24983BCE4C95906F20541AD7A3D3D11862_H
#ifndef OPENSOURCEUNITYCOMPILEDFACADE_T8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5_H
#define OPENSOURCEUNITYCOMPILEDFACADE_T8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_OpenSourceUnityCompiledFacade
struct  OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5  : public RuntimeObject
{
public:
	// Vuforia.UnityCompiled.IUnityRenderPipeline Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_OpenSourceUnityCompiledFacade::mUnityRenderPipeline
	RuntimeObject* ___mUnityRenderPipeline_0;
	// Vuforia.UnityCompiled.IUnityAndroidPermissions Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_OpenSourceUnityCompiledFacade::mUnityAndroidPermissions
	RuntimeObject* ___mUnityAndroidPermissions_1;

public:
	inline static int32_t get_offset_of_mUnityRenderPipeline_0() { return static_cast<int32_t>(offsetof(OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5, ___mUnityRenderPipeline_0)); }
	inline RuntimeObject* get_mUnityRenderPipeline_0() const { return ___mUnityRenderPipeline_0; }
	inline RuntimeObject** get_address_of_mUnityRenderPipeline_0() { return &___mUnityRenderPipeline_0; }
	inline void set_mUnityRenderPipeline_0(RuntimeObject* value)
	{
		___mUnityRenderPipeline_0 = value;
		Il2CppCodeGenWriteBarrier((&___mUnityRenderPipeline_0), value);
	}

	inline static int32_t get_offset_of_mUnityAndroidPermissions_1() { return static_cast<int32_t>(offsetof(OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5, ___mUnityAndroidPermissions_1)); }
	inline RuntimeObject* get_mUnityAndroidPermissions_1() const { return ___mUnityAndroidPermissions_1; }
	inline RuntimeObject** get_address_of_mUnityAndroidPermissions_1() { return &___mUnityAndroidPermissions_1; }
	inline void set_mUnityAndroidPermissions_1(RuntimeObject* value)
	{
		___mUnityAndroidPermissions_1 = value;
		Il2CppCodeGenWriteBarrier((&___mUnityAndroidPermissions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENSOURCEUNITYCOMPILEDFACADE_T8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_T5985CE017087B57462C0BFD1B938A2521A7C364C_H
#define __STATICARRAYINITTYPESIZEU3D20_T5985CE017087B57462C0BFD1B938A2521A7C364C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20
struct  __StaticArrayInitTypeSizeU3D20_t5985CE017087B57462C0BFD1B938A2521A7C364C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_t5985CE017087B57462C0BFD1B938A2521A7C364C__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_T5985CE017087B57462C0BFD1B938A2521A7C364C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef DIRECTPARAMS_TADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6_H
#define DIRECTPARAMS_TADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HableCurve_DirectParams
struct  DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6 
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_DirectParams::x0
	float ___x0_0;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_DirectParams::y0
	float ___y0_1;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_DirectParams::x1
	float ___x1_2;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_DirectParams::y1
	float ___y1_3;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_DirectParams::W
	float ___W_4;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_DirectParams::overshootX
	float ___overshootX_5;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_DirectParams::overshootY
	float ___overshootY_6;
	// System.Single UnityEngine.Rendering.PostProcessing.HableCurve_DirectParams::gamma
	float ___gamma_7;

public:
	inline static int32_t get_offset_of_x0_0() { return static_cast<int32_t>(offsetof(DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6, ___x0_0)); }
	inline float get_x0_0() const { return ___x0_0; }
	inline float* get_address_of_x0_0() { return &___x0_0; }
	inline void set_x0_0(float value)
	{
		___x0_0 = value;
	}

	inline static int32_t get_offset_of_y0_1() { return static_cast<int32_t>(offsetof(DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6, ___y0_1)); }
	inline float get_y0_1() const { return ___y0_1; }
	inline float* get_address_of_y0_1() { return &___y0_1; }
	inline void set_y0_1(float value)
	{
		___y0_1 = value;
	}

	inline static int32_t get_offset_of_x1_2() { return static_cast<int32_t>(offsetof(DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6, ___x1_2)); }
	inline float get_x1_2() const { return ___x1_2; }
	inline float* get_address_of_x1_2() { return &___x1_2; }
	inline void set_x1_2(float value)
	{
		___x1_2 = value;
	}

	inline static int32_t get_offset_of_y1_3() { return static_cast<int32_t>(offsetof(DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6, ___y1_3)); }
	inline float get_y1_3() const { return ___y1_3; }
	inline float* get_address_of_y1_3() { return &___y1_3; }
	inline void set_y1_3(float value)
	{
		___y1_3 = value;
	}

	inline static int32_t get_offset_of_W_4() { return static_cast<int32_t>(offsetof(DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6, ___W_4)); }
	inline float get_W_4() const { return ___W_4; }
	inline float* get_address_of_W_4() { return &___W_4; }
	inline void set_W_4(float value)
	{
		___W_4 = value;
	}

	inline static int32_t get_offset_of_overshootX_5() { return static_cast<int32_t>(offsetof(DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6, ___overshootX_5)); }
	inline float get_overshootX_5() const { return ___overshootX_5; }
	inline float* get_address_of_overshootX_5() { return &___overshootX_5; }
	inline void set_overshootX_5(float value)
	{
		___overshootX_5 = value;
	}

	inline static int32_t get_offset_of_overshootY_6() { return static_cast<int32_t>(offsetof(DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6, ___overshootY_6)); }
	inline float get_overshootY_6() const { return ___overshootY_6; }
	inline float* get_address_of_overshootY_6() { return &___overshootY_6; }
	inline void set_overshootY_6(float value)
	{
		___overshootY_6 = value;
	}

	inline static int32_t get_offset_of_gamma_7() { return static_cast<int32_t>(offsetof(DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6, ___gamma_7)); }
	inline float get_gamma_7() const { return ___gamma_7; }
	inline float* get_address_of_gamma_7() { return &___gamma_7; }
	inline void set_gamma_7(float value)
	{
		___gamma_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTPARAMS_TADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6_H
#ifndef LIGHTMETERMONITOR_T170AF7815A8487BD85CBF0EEC68865453951A075_H
#define LIGHTMETERMONITOR_T170AF7815A8487BD85CBF0EEC68865453951A075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.LightMeterMonitor
struct  LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075  : public Monitor_t9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.LightMeterMonitor::width
	int32_t ___width_2;
	// System.Int32 UnityEngine.Rendering.PostProcessing.LightMeterMonitor::height
	int32_t ___height_3;
	// System.Boolean UnityEngine.Rendering.PostProcessing.LightMeterMonitor::showCurves
	bool ___showCurves_4;

public:
	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_showCurves_4() { return static_cast<int32_t>(offsetof(LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075, ___showCurves_4)); }
	inline bool get_showCurves_4() const { return ___showCurves_4; }
	inline bool* get_address_of_showCurves_4() { return &___showCurves_4; }
	inline void set_showCurves_4(bool value)
	{
		___showCurves_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTMETERMONITOR_T170AF7815A8487BD85CBF0EEC68865453951A075_H
#ifndef PARAMETEROVERRIDE_1_T83886941E1BBB097188273A5DE03B9E77CAAA543_H
#define PARAMETEROVERRIDE_1_T83886941E1BBB097188273A5DE03B9E77CAAA543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Boolean>
struct  ParameterOverride_1_t83886941E1BBB097188273A5DE03B9E77CAAA543  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t83886941E1BBB097188273A5DE03B9E77CAAA543, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T83886941E1BBB097188273A5DE03B9E77CAAA543_H
#ifndef PARAMETEROVERRIDE_1_TE8195EADA360EF9FC5A279B93FC1F301925F73F7_H
#define PARAMETEROVERRIDE_1_TE8195EADA360EF9FC5A279B93FC1F301925F73F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Int32>
struct  ParameterOverride_1_tE8195EADA360EF9FC5A279B93FC1F301925F73F7  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_tE8195EADA360EF9FC5A279B93FC1F301925F73F7, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_TE8195EADA360EF9FC5A279B93FC1F301925F73F7_H
#ifndef PARAMETEROVERRIDE_1_TA5639774BC970236B9AFC291589A597150CBA71B_H
#define PARAMETEROVERRIDE_1_TA5639774BC970236B9AFC291589A597150CBA71B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Single>
struct  ParameterOverride_1_tA5639774BC970236B9AFC291589A597150CBA71B  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	float ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_tA5639774BC970236B9AFC291589A597150CBA71B, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_TA5639774BC970236B9AFC291589A597150CBA71B_H
#ifndef PARAMETEROVERRIDE_1_T72C8D750446A063D6AB02DE6E747319FF04F91AC_H
#define PARAMETEROVERRIDE_1_T72C8D750446A063D6AB02DE6E747319FF04F91AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.Spline>
struct  ParameterOverride_1_t72C8D750446A063D6AB02DE6E747319FF04F91AC  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	Spline_t42213D550262BA4C9B574C062AB52021A970081F * ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t72C8D750446A063D6AB02DE6E747319FF04F91AC, ___value_1)); }
	inline Spline_t42213D550262BA4C9B574C062AB52021A970081F * get_value_1() const { return ___value_1; }
	inline Spline_t42213D550262BA4C9B574C062AB52021A970081F ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Spline_t42213D550262BA4C9B574C062AB52021A970081F * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T72C8D750446A063D6AB02DE6E747319FF04F91AC_H
#ifndef PARAMETEROVERRIDE_1_T2ACDAC034DCBB9DE5BD78EFD31136BC66F595DFA_H
#define PARAMETEROVERRIDE_1_T2ACDAC034DCBB9DE5BD78EFD31136BC66F595DFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Texture>
struct  ParameterOverride_1_t2ACDAC034DCBB9DE5BD78EFD31136BC66F595DFA  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t2ACDAC034DCBB9DE5BD78EFD31136BC66F595DFA, ___value_1)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_value_1() const { return ___value_1; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T2ACDAC034DCBB9DE5BD78EFD31136BC66F595DFA_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T26518661FC6C7566CBE8E5BC1629CC800906EC10_H
#define POSTPROCESSEFFECTRENDERER_1_T26518661FC6C7566CBE8E5BC1629CC800906EC10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.LensDistortion>
struct  PostProcessEffectRenderer_1_t26518661FC6C7566CBE8E5BC1629CC800906EC10  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t26518661FC6C7566CBE8E5BC1629CC800906EC10, ___U3CsettingsU3Ek__BackingField_1)); }
	inline LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T26518661FC6C7566CBE8E5BC1629CC800906EC10_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T0E09C55D34A7796C3F4A917AD1B5147C324E5C4C_H
#define POSTPROCESSEFFECTRENDERER_1_T0E09C55D34A7796C3F4A917AD1B5147C324E5C4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.MotionBlur>
struct  PostProcessEffectRenderer_1_t0E09C55D34A7796C3F4A917AD1B5147C324E5C4C  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t0E09C55D34A7796C3F4A917AD1B5147C324E5C4C, ___U3CsettingsU3Ek__BackingField_1)); }
	inline MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T0E09C55D34A7796C3F4A917AD1B5147C324E5C4C_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T33EE31F207A9D1862F4D3F2808C06C47244E9B53_H
#define POSTPROCESSEFFECTRENDERER_1_T33EE31F207A9D1862F4D3F2808C06C47244E9B53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections>
struct  PostProcessEffectRenderer_1_t33EE31F207A9D1862F4D3F2808C06C47244E9B53  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t33EE31F207A9D1862F4D3F2808C06C47244E9B53, ___U3CsettingsU3Ek__BackingField_1)); }
	inline ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T33EE31F207A9D1862F4D3F2808C06C47244E9B53_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T0B3E4C2EF13125CDBE3816D19617291297AE38DE_H
#define POSTPROCESSEFFECTRENDERER_1_T0B3E4C2EF13125CDBE3816D19617291297AE38DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.Vignette>
struct  PostProcessEffectRenderer_1_t0B3E4C2EF13125CDBE3816D19617291297AE38DE  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t0B3E4C2EF13125CDBE3816D19617291297AE38DE, ___U3CsettingsU3Ek__BackingField_1)); }
	inline Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T0B3E4C2EF13125CDBE3816D19617291297AE38DE_H
#ifndef POSTPROCESSEVENTCOMPARER_T69E93AE9BD81722DFAFAD51564C3C555AE0975E4_H
#define POSTPROCESSEVENTCOMPARER_T69E93AE9BD81722DFAFAD51564C3C555AE0975E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEventComparer
struct  PostProcessEventComparer_t69E93AE9BD81722DFAFAD51564C3C555AE0975E4 
{
public:
	union
	{
		struct
		{
		};
		uint8_t PostProcessEventComparer_t69E93AE9BD81722DFAFAD51564C3C555AE0975E4__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEVENTCOMPARER_T69E93AE9BD81722DFAFAD51564C3C555AE0975E4_H
#ifndef VECTORSCOPEMONITOR_TACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B_H
#define VECTORSCOPEMONITOR_TACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.VectorscopeMonitor
struct  VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B  : public Monitor_t9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.VectorscopeMonitor::size
	int32_t ___size_2;
	// System.Single UnityEngine.Rendering.PostProcessing.VectorscopeMonitor::exposure
	float ___exposure_3;
	// UnityEngine.ComputeBuffer UnityEngine.Rendering.PostProcessing.VectorscopeMonitor::m_Data
	ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * ___m_Data_4;

public:
	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_exposure_3() { return static_cast<int32_t>(offsetof(VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B, ___exposure_3)); }
	inline float get_exposure_3() const { return ___exposure_3; }
	inline float* get_address_of_exposure_3() { return &___exposure_3; }
	inline void set_exposure_3(float value)
	{
		___exposure_3 = value;
	}

	inline static int32_t get_offset_of_m_Data_4() { return static_cast<int32_t>(offsetof(VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B, ___m_Data_4)); }
	inline ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * get_m_Data_4() const { return ___m_Data_4; }
	inline ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 ** get_address_of_m_Data_4() { return &___m_Data_4; }
	inline void set_m_Data_4(ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * value)
	{
		___m_Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORSCOPEMONITOR_TACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B_H
#ifndef WAVEFORMMONITOR_T3ACB5C1E48425044C04B01099C7D99B57DE859A6_H
#define WAVEFORMMONITOR_T3ACB5C1E48425044C04B01099C7D99B57DE859A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.WaveformMonitor
struct  WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6  : public Monitor_t9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.WaveformMonitor::exposure
	float ___exposure_2;
	// System.Int32 UnityEngine.Rendering.PostProcessing.WaveformMonitor::height
	int32_t ___height_3;
	// UnityEngine.ComputeBuffer UnityEngine.Rendering.PostProcessing.WaveformMonitor::m_Data
	ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * ___m_Data_4;

public:
	inline static int32_t get_offset_of_exposure_2() { return static_cast<int32_t>(offsetof(WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6, ___exposure_2)); }
	inline float get_exposure_2() const { return ___exposure_2; }
	inline float* get_address_of_exposure_2() { return &___exposure_2; }
	inline void set_exposure_2(float value)
	{
		___exposure_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_m_Data_4() { return static_cast<int32_t>(offsetof(WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6, ___m_Data_4)); }
	inline ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * get_m_Data_4() const { return ___m_Data_4; }
	inline ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 ** get_address_of_m_Data_4() { return &___m_Data_4; }
	inline void set_m_Data_4(ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * value)
	{
		___m_Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVEFORMMONITOR_T3ACB5C1E48425044C04B01099C7D99B57DE859A6_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T205DFFBD2DB592A47BC91DB97BB3717FA0393C48_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T205DFFBD2DB592A47BC91DB97BB3717FA0393C48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t205DFFBD2DB592A47BC91DB97BB3717FA0393C48  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t205DFFBD2DB592A47BC91DB97BB3717FA0393C48_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20 <PrivateImplementationDetails>::0ED907628EE272F93737B500A23D77C9B1C88368
	__StaticArrayInitTypeSizeU3D20_t5985CE017087B57462C0BFD1B938A2521A7C364C  ___0ED907628EE272F93737B500A23D77C9B1C88368_0;

public:
	inline static int32_t get_offset_of_U30ED907628EE272F93737B500A23D77C9B1C88368_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t205DFFBD2DB592A47BC91DB97BB3717FA0393C48_StaticFields, ___0ED907628EE272F93737B500A23D77C9B1C88368_0)); }
	inline __StaticArrayInitTypeSizeU3D20_t5985CE017087B57462C0BFD1B938A2521A7C364C  get_U30ED907628EE272F93737B500A23D77C9B1C88368_0() const { return ___0ED907628EE272F93737B500A23D77C9B1C88368_0; }
	inline __StaticArrayInitTypeSizeU3D20_t5985CE017087B57462C0BFD1B938A2521A7C364C * get_address_of_U30ED907628EE272F93737B500A23D77C9B1C88368_0() { return &___0ED907628EE272F93737B500A23D77C9B1C88368_0; }
	inline void set_U30ED907628EE272F93737B500A23D77C9B1C88368_0(__StaticArrayInitTypeSizeU3D20_t5985CE017087B57462C0BFD1B938A2521A7C364C  value)
	{
		___0ED907628EE272F93737B500A23D77C9B1C88368_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T205DFFBD2DB592A47BC91DB97BB3717FA0393C48_H
#ifndef CUBEMAPFACE_T74DD9C86D8A5E5F782F136F8753580668F96FFB9_H
#define CUBEMAPFACE_T74DD9C86D8A5E5F782F136F8753580668F96FFB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CubemapFace
struct  CubemapFace_t74DD9C86D8A5E5F782F136F8753580668F96FFB9 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CubemapFace_t74DD9C86D8A5E5F782F136F8753580668F96FFB9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPFACE_T74DD9C86D8A5E5F782F136F8753580668F96FFB9_H
#ifndef DEPTHTEXTUREMODE_T284833A8AB245ACA7E27BE611BE03B18B0249F01_H
#define DEPTHTEXTUREMODE_T284833A8AB245ACA7E27BE611BE03B18B0249F01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DepthTextureMode
struct  DepthTextureMode_t284833A8AB245ACA7E27BE611BE03B18B0249F01 
{
public:
	// System.Int32 UnityEngine.DepthTextureMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DepthTextureMode_t284833A8AB245ACA7E27BE611BE03B18B0249F01, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHTEXTUREMODE_T284833A8AB245ACA7E27BE611BE03B18B0249F01_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RENDERTEXTURECREATIONFLAGS_TF63E06301E4BB4746F7E07759B359872BD4BFB1E_H
#define RENDERTEXTURECREATIONFLAGS_TF63E06301E4BB4746F7E07759B359872BD4BFB1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureCreationFlags
struct  RenderTextureCreationFlags_tF63E06301E4BB4746F7E07759B359872BD4BFB1E 
{
public:
	// System.Int32 UnityEngine.RenderTextureCreationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureCreationFlags_tF63E06301E4BB4746F7E07759B359872BD4BFB1E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURECREATIONFLAGS_TF63E06301E4BB4746F7E07759B359872BD4BFB1E_H
#ifndef RENDERTEXTUREFORMAT_T2AB1B77FBD247648292FBBE1182F12B5FC47AF85_H
#define RENDERTEXTUREFORMAT_T2AB1B77FBD247648292FBBE1182F12B5FC47AF85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t2AB1B77FBD247648292FBBE1182F12B5FC47AF85 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t2AB1B77FBD247648292FBBE1182F12B5FC47AF85, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T2AB1B77FBD247648292FBBE1182F12B5FC47AF85_H
#ifndef RENDERTEXTUREMEMORYLESS_T19E37ADD57C1F00D67146A2BB4521D06F370D2E9_H
#define RENDERTEXTUREMEMORYLESS_T19E37ADD57C1F00D67146A2BB4521D06F370D2E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureMemoryless
struct  RenderTextureMemoryless_t19E37ADD57C1F00D67146A2BB4521D06F370D2E9 
{
public:
	// System.Int32 UnityEngine.RenderTextureMemoryless::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureMemoryless_t19E37ADD57C1F00D67146A2BB4521D06F370D2E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREMEMORYLESS_T19E37ADD57C1F00D67146A2BB4521D06F370D2E9_H
#ifndef BUILTINRENDERTEXTURETYPE_T6ECEE9FF62E815D7ED640D057EEA84C0FD145D01_H
#define BUILTINRENDERTEXTURETYPE_T6ECEE9FF62E815D7ED640D057EEA84C0FD145D01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.BuiltinRenderTextureType
struct  BuiltinRenderTextureType_t6ECEE9FF62E815D7ED640D057EEA84C0FD145D01 
{
public:
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BuiltinRenderTextureType_t6ECEE9FF62E815D7ED640D057EEA84C0FD145D01, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINRENDERTEXTURETYPE_T6ECEE9FF62E815D7ED640D057EEA84C0FD145D01_H
#ifndef BOOLPARAMETER_TEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE_H
#define BOOLPARAMETER_TEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.BoolParameter
struct  BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE  : public ParameterOverride_1_t83886941E1BBB097188273A5DE03B9E77CAAA543
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLPARAMETER_TEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE_H
#ifndef COLORBLINDNESSTYPE_TF783AE5750B515662C15C92D8F428BACE786E7F6_H
#define COLORBLINDNESSTYPE_TF783AE5750B515662C15C92D8F428BACE786E7F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorBlindnessType
struct  ColorBlindnessType_tF783AE5750B515662C15C92D8F428BACE786E7F6 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ColorBlindnessType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorBlindnessType_tF783AE5750B515662C15C92D8F428BACE786E7F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLINDNESSTYPE_TF783AE5750B515662C15C92D8F428BACE786E7F6_H
#ifndef DEBUGOVERLAY_T7F24134A21C1E2972B7BA13F89C2756CF7A7FF2E_H
#define DEBUGOVERLAY_T7F24134A21C1E2972B7BA13F89C2756CF7A7FF2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.DebugOverlay
struct  DebugOverlay_t7F24134A21C1E2972B7BA13F89C2756CF7A7FF2E 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.DebugOverlay::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugOverlay_t7F24134A21C1E2972B7BA13F89C2756CF7A7FF2E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGOVERLAY_T7F24134A21C1E2972B7BA13F89C2756CF7A7FF2E_H
#ifndef FLOATPARAMETER_T55E57BA8E6C776667673B2C973ADB64B15E2AB7F_H
#define FLOATPARAMETER_T55E57BA8E6C776667673B2C973ADB64B15E2AB7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.FloatParameter
struct  FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F  : public ParameterOverride_1_tA5639774BC970236B9AFC291589A597150CBA71B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARAMETER_T55E57BA8E6C776667673B2C973ADB64B15E2AB7F_H
#ifndef CHANNEL_T7B171541952948F851D8DFD5BC7A16AEE4E0D02E_H
#define CHANNEL_T7B171541952948F851D8DFD5BC7A16AEE4E0D02E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HistogramMonitor_Channel
struct  Channel_t7B171541952948F851D8DFD5BC7A16AEE4E0D02E 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.HistogramMonitor_Channel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Channel_t7B171541952948F851D8DFD5BC7A16AEE4E0D02E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNEL_T7B171541952948F851D8DFD5BC7A16AEE4E0D02E_H
#ifndef INTPARAMETER_T268BB9E59D69C34B45EB3174B23702F6C91760CC_H
#define INTPARAMETER_T268BB9E59D69C34B45EB3174B23702F6C91760CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.IntParameter
struct  IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC  : public ParameterOverride_1_tE8195EADA360EF9FC5A279B93FC1F301925F73F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPARAMETER_T268BB9E59D69C34B45EB3174B23702F6C91760CC_H
#ifndef LENSDISTORTIONRENDERER_TD0992AD8A28D4048994B736C7E62A7A3C54B3E34_H
#define LENSDISTORTIONRENDERER_TD0992AD8A28D4048994B736C7E62A7A3C54B3E34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.LensDistortionRenderer
struct  LensDistortionRenderer_tD0992AD8A28D4048994B736C7E62A7A3C54B3E34  : public PostProcessEffectRenderer_1_t26518661FC6C7566CBE8E5BC1629CC800906EC10
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSDISTORTIONRENDERER_TD0992AD8A28D4048994B736C7E62A7A3C54B3E34_H
#ifndef MONITORTYPE_TAFB1F1FED2AB2C4704DC197BB2CEF4A96267AE8C_H
#define MONITORTYPE_TAFB1F1FED2AB2C4704DC197BB2CEF4A96267AE8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MonitorType
struct  MonitorType_tAFB1F1FED2AB2C4704DC197BB2CEF4A96267AE8C 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.MonitorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MonitorType_tAFB1F1FED2AB2C4704DC197BB2CEF4A96267AE8C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONITORTYPE_TAFB1F1FED2AB2C4704DC197BB2CEF4A96267AE8C_H
#ifndef MOTIONBLURRENDERER_T004254D33561D01A03920F2548308516C85A272F_H
#define MOTIONBLURRENDERER_T004254D33561D01A03920F2548308516C85A272F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MotionBlurRenderer
struct  MotionBlurRenderer_t004254D33561D01A03920F2548308516C85A272F  : public PostProcessEffectRenderer_1_t0E09C55D34A7796C3F4A917AD1B5147C324E5C4C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLURRENDERER_T004254D33561D01A03920F2548308516C85A272F_H
#ifndef PASS_T264F66F4CAA144E9EC147CBF990804B7492A263C_H
#define PASS_T264F66F4CAA144E9EC147CBF990804B7492A263C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MotionBlurRenderer_Pass
struct  Pass_t264F66F4CAA144E9EC147CBF990804B7492A263C 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.MotionBlurRenderer_Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_t264F66F4CAA144E9EC147CBF990804B7492A263C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T264F66F4CAA144E9EC147CBF990804B7492A263C_H
#ifndef MIPLEVEL_TCBD464EE7356E35A1364CBE9B0C5C6B913050EB9_H
#define MIPLEVEL_TCBD464EE7356E35A1364CBE9B0C5C6B913050EB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MultiScaleVO_MipLevel
struct  MipLevel_tCBD464EE7356E35A1364CBE9B0C5C6B913050EB9 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.MultiScaleVO_MipLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MipLevel_tCBD464EE7356E35A1364CBE9B0C5C6B913050EB9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIPLEVEL_TCBD464EE7356E35A1364CBE9B0C5C6B913050EB9_H
#ifndef PASS_T76CE8D717150416754BF0DA8BFBD76168BC8B887_H
#define PASS_T76CE8D717150416754BF0DA8BFBD76168BC8B887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MultiScaleVO_Pass
struct  Pass_t76CE8D717150416754BF0DA8BFBD76168BC8B887 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.MultiScaleVO_Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_t76CE8D717150416754BF0DA8BFBD76168BC8B887, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T76CE8D717150416754BF0DA8BFBD76168BC8B887_H
#ifndef PARAMETEROVERRIDE_1_T23F21A3C4BDF3D75D33F5E24B8462A2A7376387E_H
#define PARAMETEROVERRIDE_1_T23F21A3C4BDF3D75D33F5E24B8462A2A7376387E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Color>
struct  ParameterOverride_1_t23F21A3C4BDF3D75D33F5E24B8462A2A7376387E  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t23F21A3C4BDF3D75D33F5E24B8462A2A7376387E, ___value_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_value_1() const { return ___value_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T23F21A3C4BDF3D75D33F5E24B8462A2A7376387E_H
#ifndef PARAMETEROVERRIDE_1_TED4B89A13ADA28A3E8CFEDF351B518B2F91E1004_H
#define PARAMETEROVERRIDE_1_TED4B89A13ADA28A3E8CFEDF351B518B2F91E1004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Vector2>
struct  ParameterOverride_1_tED4B89A13ADA28A3E8CFEDF351B518B2F91E1004  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_tED4B89A13ADA28A3E8CFEDF351B518B2F91E1004, ___value_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_value_1() const { return ___value_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_TED4B89A13ADA28A3E8CFEDF351B518B2F91E1004_H
#ifndef PARAMETEROVERRIDE_1_TA725C89A0A7CBF68BEB3E1B19D91328AFAF9710F_H
#define PARAMETEROVERRIDE_1_TA725C89A0A7CBF68BEB3E1B19D91328AFAF9710F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Vector4>
struct  ParameterOverride_1_tA725C89A0A7CBF68BEB3E1B19D91328AFAF9710F  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_tA725C89A0A7CBF68BEB3E1B19D91328AFAF9710F, ___value_1)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_value_1() const { return ___value_1; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_TA725C89A0A7CBF68BEB3E1B19D91328AFAF9710F_H
#ifndef POSTPROCESSEVENT_TA94B9311FE47676FC4B04E6F7935F3DE20E42BDA_H
#define POSTPROCESSEVENT_TA94B9311FE47676FC4B04E6F7935F3DE20E42BDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEvent
struct  PostProcessEvent_tA94B9311FE47676FC4B04E6F7935F3DE20E42BDA 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PostProcessEvent_tA94B9311FE47676FC4B04E6F7935F3DE20E42BDA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEVENT_TA94B9311FE47676FC4B04E6F7935F3DE20E42BDA_H
#ifndef ANTIALIASING_T4B83F9C9FA7E42D5B3EB30CE9B5B4F870C3780FA_H
#define ANTIALIASING_T4B83F9C9FA7E42D5B3EB30CE9B5B4F870C3780FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer_Antialiasing
struct  Antialiasing_t4B83F9C9FA7E42D5B3EB30CE9B5B4F870C3780FA 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessLayer_Antialiasing::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Antialiasing_t4B83F9C9FA7E42D5B3EB30CE9B5B4F870C3780FA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANTIALIASING_T4B83F9C9FA7E42D5B3EB30CE9B5B4F870C3780FA_H
#ifndef STEREORENDERINGMODE_T3EFE9CED6CDCB21D9519A8F922090969360DFF66_H
#define STEREORENDERINGMODE_T3EFE9CED6CDCB21D9519A8F922090969360DFF66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessRenderContext_StereoRenderingMode
struct  StereoRenderingMode_t3EFE9CED6CDCB21D9519A8F922090969360DFF66 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext_StereoRenderingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StereoRenderingMode_t3EFE9CED6CDCB21D9519A8F922090969360DFF66, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREORENDERINGMODE_T3EFE9CED6CDCB21D9519A8F922090969360DFF66_H
#ifndef PASS_T4E5F6D23D9B3602519552218DD227CE731A4D241_H
#define PASS_T4E5F6D23D9B3602519552218DD227CE731A4D241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScalableAO_Pass
struct  Pass_t4E5F6D23D9B3602519552218DD227CE731A4D241 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ScalableAO_Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_t4E5F6D23D9B3602519552218DD227CE731A4D241, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T4E5F6D23D9B3602519552218DD227CE731A4D241_H
#ifndef SCREENSPACEREFLECTIONPRESET_TD5E24AD52047183FBEF36FF6ACD36EA3098DC4DF_H
#define SCREENSPACEREFLECTIONPRESET_TD5E24AD52047183FBEF36FF6ACD36EA3098DC4DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPreset
struct  ScreenSpaceReflectionPreset_tD5E24AD52047183FBEF36FF6ACD36EA3098DC4DF 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPreset::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionPreset_tD5E24AD52047183FBEF36FF6ACD36EA3098DC4DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONPRESET_TD5E24AD52047183FBEF36FF6ACD36EA3098DC4DF_H
#ifndef SCREENSPACEREFLECTIONRESOLUTION_T7FD9ADDC1B93D2A141F30A114785DF26A72544C5_H
#define SCREENSPACEREFLECTIONRESOLUTION_T7FD9ADDC1B93D2A141F30A114785DF26A72544C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolution
struct  ScreenSpaceReflectionResolution_t7FD9ADDC1B93D2A141F30A114785DF26A72544C5 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionResolution_t7FD9ADDC1B93D2A141F30A114785DF26A72544C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONRESOLUTION_T7FD9ADDC1B93D2A141F30A114785DF26A72544C5_H
#ifndef SCREENSPACEREFLECTIONSRENDERER_TA1AD5222A4989034AEC0C2B297CF8D781D67AC0D_H
#define SCREENSPACEREFLECTIONSRENDERER_TA1AD5222A4989034AEC0C2B297CF8D781D67AC0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer
struct  ScreenSpaceReflectionsRenderer_tA1AD5222A4989034AEC0C2B297CF8D781D67AC0D  : public PostProcessEffectRenderer_1_t33EE31F207A9D1862F4D3F2808C06C47244E9B53
{
public:
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer::m_Resolve
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___m_Resolve_2;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer::m_History
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___m_History_3;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer::m_MipIDs
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_MipIDs_4;
	// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer_QualityPreset[] UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer::m_Presets
	QualityPresetU5BU5D_t3536A15EBC22E6FE24F834B7CE55A618A0F592A2* ___m_Presets_5;

public:
	inline static int32_t get_offset_of_m_Resolve_2() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionsRenderer_tA1AD5222A4989034AEC0C2B297CF8D781D67AC0D, ___m_Resolve_2)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_m_Resolve_2() const { return ___m_Resolve_2; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_m_Resolve_2() { return &___m_Resolve_2; }
	inline void set_m_Resolve_2(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___m_Resolve_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Resolve_2), value);
	}

	inline static int32_t get_offset_of_m_History_3() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionsRenderer_tA1AD5222A4989034AEC0C2B297CF8D781D67AC0D, ___m_History_3)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_m_History_3() const { return ___m_History_3; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_m_History_3() { return &___m_History_3; }
	inline void set_m_History_3(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___m_History_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_History_3), value);
	}

	inline static int32_t get_offset_of_m_MipIDs_4() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionsRenderer_tA1AD5222A4989034AEC0C2B297CF8D781D67AC0D, ___m_MipIDs_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_MipIDs_4() const { return ___m_MipIDs_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_MipIDs_4() { return &___m_MipIDs_4; }
	inline void set_m_MipIDs_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_MipIDs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MipIDs_4), value);
	}

	inline static int32_t get_offset_of_m_Presets_5() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionsRenderer_tA1AD5222A4989034AEC0C2B297CF8D781D67AC0D, ___m_Presets_5)); }
	inline QualityPresetU5BU5D_t3536A15EBC22E6FE24F834B7CE55A618A0F592A2* get_m_Presets_5() const { return ___m_Presets_5; }
	inline QualityPresetU5BU5D_t3536A15EBC22E6FE24F834B7CE55A618A0F592A2** get_address_of_m_Presets_5() { return &___m_Presets_5; }
	inline void set_m_Presets_5(QualityPresetU5BU5D_t3536A15EBC22E6FE24F834B7CE55A618A0F592A2* value)
	{
		___m_Presets_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Presets_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONSRENDERER_TA1AD5222A4989034AEC0C2B297CF8D781D67AC0D_H
#ifndef PASS_T4C474550C72E2AE28170430C78280B09F09F8A01_H
#define PASS_T4C474550C72E2AE28170430C78280B09F09F8A01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer_Pass
struct  Pass_t4C474550C72E2AE28170430C78280B09F09F8A01 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer_Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_t4C474550C72E2AE28170430C78280B09F09F8A01, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T4C474550C72E2AE28170430C78280B09F09F8A01_H
#ifndef SPLINEPARAMETER_T0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0_H
#define SPLINEPARAMETER_T0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.SplineParameter
struct  SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0  : public ParameterOverride_1_t72C8D750446A063D6AB02DE6E747319FF04F91AC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEPARAMETER_T0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0_H
#ifndef PASS_TE937511E9D3F1E227E0FEB8456DD5DF0A28819B8_H
#define PASS_TE937511E9D3F1E227E0FEB8456DD5DF0A28819B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing_Pass
struct  Pass_tE937511E9D3F1E227E0FEB8456DD5DF0A28819B8 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing_Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_tE937511E9D3F1E227E0FEB8456DD5DF0A28819B8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_TE937511E9D3F1E227E0FEB8456DD5DF0A28819B8_H
#ifndef QUALITY_TF55D5DEED13EABAFEB6A2AE4B4CE32603D758710_H
#define QUALITY_TF55D5DEED13EABAFEB6A2AE4B4CE32603D758710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing_Quality
struct  Quality_tF55D5DEED13EABAFEB6A2AE4B4CE32603D758710 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing_Quality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Quality_tF55D5DEED13EABAFEB6A2AE4B4CE32603D758710, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITY_TF55D5DEED13EABAFEB6A2AE4B4CE32603D758710_H
#ifndef TEMPORALANTIALIASING_T708477A918872723B0F89A467FFAFC7EA27B97D1_H
#define TEMPORALANTIALIASING_T708477A918872723B0F89A467FFAFC7EA27B97D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TemporalAntialiasing
struct  TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1  : public RuntimeObject
{
public:
	// System.Single UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::jitterSpread
	float ___jitterSpread_0;
	// System.Single UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::sharpness
	float ___sharpness_1;
	// System.Single UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::stationaryBlending
	float ___stationaryBlending_2;
	// System.Single UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::motionBlending
	float ___motionBlending_3;
	// System.Func`3<UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.Matrix4x4> UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::jitteredMatrixFunc
	Func_3_t8F15226E3E5D0910B0330699065D10FE301CEA92 * ___jitteredMatrixFunc_4;
	// UnityEngine.Vector2 UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::<jitter>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CjitterU3Ek__BackingField_5;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::m_Mrt
	RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4* ___m_Mrt_6;
	// System.Boolean UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::m_ResetHistory
	bool ___m_ResetHistory_7;
	// System.Int32 UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::<sampleIndex>k__BackingField
	int32_t ___U3CsampleIndexU3Ek__BackingField_9;
	// UnityEngine.RenderTexture[][] UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::m_HistoryTextures
	RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF* ___m_HistoryTextures_12;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.TemporalAntialiasing::m_HistoryPingPong
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_HistoryPingPong_13;

public:
	inline static int32_t get_offset_of_jitterSpread_0() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___jitterSpread_0)); }
	inline float get_jitterSpread_0() const { return ___jitterSpread_0; }
	inline float* get_address_of_jitterSpread_0() { return &___jitterSpread_0; }
	inline void set_jitterSpread_0(float value)
	{
		___jitterSpread_0 = value;
	}

	inline static int32_t get_offset_of_sharpness_1() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___sharpness_1)); }
	inline float get_sharpness_1() const { return ___sharpness_1; }
	inline float* get_address_of_sharpness_1() { return &___sharpness_1; }
	inline void set_sharpness_1(float value)
	{
		___sharpness_1 = value;
	}

	inline static int32_t get_offset_of_stationaryBlending_2() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___stationaryBlending_2)); }
	inline float get_stationaryBlending_2() const { return ___stationaryBlending_2; }
	inline float* get_address_of_stationaryBlending_2() { return &___stationaryBlending_2; }
	inline void set_stationaryBlending_2(float value)
	{
		___stationaryBlending_2 = value;
	}

	inline static int32_t get_offset_of_motionBlending_3() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___motionBlending_3)); }
	inline float get_motionBlending_3() const { return ___motionBlending_3; }
	inline float* get_address_of_motionBlending_3() { return &___motionBlending_3; }
	inline void set_motionBlending_3(float value)
	{
		___motionBlending_3 = value;
	}

	inline static int32_t get_offset_of_jitteredMatrixFunc_4() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___jitteredMatrixFunc_4)); }
	inline Func_3_t8F15226E3E5D0910B0330699065D10FE301CEA92 * get_jitteredMatrixFunc_4() const { return ___jitteredMatrixFunc_4; }
	inline Func_3_t8F15226E3E5D0910B0330699065D10FE301CEA92 ** get_address_of_jitteredMatrixFunc_4() { return &___jitteredMatrixFunc_4; }
	inline void set_jitteredMatrixFunc_4(Func_3_t8F15226E3E5D0910B0330699065D10FE301CEA92 * value)
	{
		___jitteredMatrixFunc_4 = value;
		Il2CppCodeGenWriteBarrier((&___jitteredMatrixFunc_4), value);
	}

	inline static int32_t get_offset_of_U3CjitterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___U3CjitterU3Ek__BackingField_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CjitterU3Ek__BackingField_5() const { return ___U3CjitterU3Ek__BackingField_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CjitterU3Ek__BackingField_5() { return &___U3CjitterU3Ek__BackingField_5; }
	inline void set_U3CjitterU3Ek__BackingField_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CjitterU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_m_Mrt_6() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___m_Mrt_6)); }
	inline RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4* get_m_Mrt_6() const { return ___m_Mrt_6; }
	inline RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4** get_address_of_m_Mrt_6() { return &___m_Mrt_6; }
	inline void set_m_Mrt_6(RenderTargetIdentifierU5BU5D_t57069CA3F97B36638E39044168D1BEB956436DD4* value)
	{
		___m_Mrt_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mrt_6), value);
	}

	inline static int32_t get_offset_of_m_ResetHistory_7() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___m_ResetHistory_7)); }
	inline bool get_m_ResetHistory_7() const { return ___m_ResetHistory_7; }
	inline bool* get_address_of_m_ResetHistory_7() { return &___m_ResetHistory_7; }
	inline void set_m_ResetHistory_7(bool value)
	{
		___m_ResetHistory_7 = value;
	}

	inline static int32_t get_offset_of_U3CsampleIndexU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___U3CsampleIndexU3Ek__BackingField_9)); }
	inline int32_t get_U3CsampleIndexU3Ek__BackingField_9() const { return ___U3CsampleIndexU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CsampleIndexU3Ek__BackingField_9() { return &___U3CsampleIndexU3Ek__BackingField_9; }
	inline void set_U3CsampleIndexU3Ek__BackingField_9(int32_t value)
	{
		___U3CsampleIndexU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_m_HistoryTextures_12() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___m_HistoryTextures_12)); }
	inline RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF* get_m_HistoryTextures_12() const { return ___m_HistoryTextures_12; }
	inline RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF** get_address_of_m_HistoryTextures_12() { return &___m_HistoryTextures_12; }
	inline void set_m_HistoryTextures_12(RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF* value)
	{
		___m_HistoryTextures_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistoryTextures_12), value);
	}

	inline static int32_t get_offset_of_m_HistoryPingPong_13() { return static_cast<int32_t>(offsetof(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1, ___m_HistoryPingPong_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_HistoryPingPong_13() const { return ___m_HistoryPingPong_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_HistoryPingPong_13() { return &___m_HistoryPingPong_13; }
	inline void set_m_HistoryPingPong_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_HistoryPingPong_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistoryPingPong_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEMPORALANTIALIASING_T708477A918872723B0F89A467FFAFC7EA27B97D1_H
#ifndef PASS_TC06140916640021B01BB1C7F7E1CF85042282FE3_H
#define PASS_TC06140916640021B01BB1C7F7E1CF85042282FE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TemporalAntialiasing_Pass
struct  Pass_tC06140916640021B01BB1C7F7E1CF85042282FE3 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.TemporalAntialiasing_Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_tC06140916640021B01BB1C7F7E1CF85042282FE3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_TC06140916640021B01BB1C7F7E1CF85042282FE3_H
#ifndef TEXTUREPARAMETERDEFAULT_T774773D69629DCC7D41D000FCE712DD1A6307FFB_H
#define TEXTUREPARAMETERDEFAULT_T774773D69629DCC7D41D000FCE712DD1A6307FFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TextureParameterDefault
struct  TextureParameterDefault_t774773D69629DCC7D41D000FCE712DD1A6307FFB 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.TextureParameterDefault::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureParameterDefault_t774773D69629DCC7D41D000FCE712DD1A6307FFB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPARAMETERDEFAULT_T774773D69629DCC7D41D000FCE712DD1A6307FFB_H
#ifndef VIGNETTEMODE_T0FD0B75231561D28DAA858F7739527CFD8696046_H
#define VIGNETTEMODE_T0FD0B75231561D28DAA858F7739527CFD8696046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.VignetteMode
struct  VignetteMode_t0FD0B75231561D28DAA858F7739527CFD8696046 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.VignetteMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VignetteMode_t0FD0B75231561D28DAA858F7739527CFD8696046, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTEMODE_T0FD0B75231561D28DAA858F7739527CFD8696046_H
#ifndef VIGNETTERENDERER_T37D1882E6AC06E8599CE7D277AA09CA1FE6EFB79_H
#define VIGNETTERENDERER_T37D1882E6AC06E8599CE7D277AA09CA1FE6EFB79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.VignetteRenderer
struct  VignetteRenderer_t37D1882E6AC06E8599CE7D277AA09CA1FE6EFB79  : public PostProcessEffectRenderer_1_t0B3E4C2EF13125CDBE3816D19617291297AE38DE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTERENDERER_T37D1882E6AC06E8599CE7D277AA09CA1FE6EFB79_H
#ifndef SHADOWSAMPLINGMODE_T585A9BDECAC505FF19FF785F55CDD403A2E5DA73_H
#define SHADOWSAMPLINGMODE_T585A9BDECAC505FF19FF785F55CDD403A2E5DA73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.ShadowSamplingMode
struct  ShadowSamplingMode_t585A9BDECAC505FF19FF785F55CDD403A2E5DA73 
{
public:
	// System.Int32 UnityEngine.Rendering.ShadowSamplingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShadowSamplingMode_t585A9BDECAC505FF19FF785F55CDD403A2E5DA73, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWSAMPLINGMODE_T585A9BDECAC505FF19FF785F55CDD403A2E5DA73_H
#ifndef TEXTUREDIMENSION_T90D0E4110D3F4D062F3E8C0F69809BFBBDF8E19C_H
#define TEXTUREDIMENSION_T90D0E4110D3F4D062F3E8C0F69809BFBBDF8E19C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.TextureDimension
struct  TextureDimension_t90D0E4110D3F4D062F3E8C0F69809BFBBDF8E19C 
{
public:
	// System.Int32 UnityEngine.Rendering.TextureDimension::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureDimension_t90D0E4110D3F4D062F3E8C0F69809BFBBDF8E19C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREDIMENSION_T90D0E4110D3F4D062F3E8C0F69809BFBBDF8E19C_H
#ifndef VRTEXTUREUSAGE_T2D7C2397ABF03DD28086B969100F7D91DDD978A0_H
#define VRTEXTUREUSAGE_T2D7C2397ABF03DD28086B969100F7D91DDD978A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VRTextureUsage
struct  VRTextureUsage_t2D7C2397ABF03DD28086B969100F7D91DDD978A0 
{
public:
	// System.Int32 UnityEngine.VRTextureUsage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VRTextureUsage_t2D7C2397ABF03DD28086B969100F7D91DDD978A0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRTEXTUREUSAGE_T2D7C2397ABF03DD28086B969100F7D91DDD978A0_H
#ifndef STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#define STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour_Status
struct  Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef RENDERTEXTUREDESCRIPTOR_T74FEC57A54F89E11748E1865F7DCA3565BFAF58E_H
#define RENDERTEXTUREDESCRIPTOR_T74FEC57A54F89E11748E1865F7DCA3565BFAF58E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureDescriptor
struct  RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E 
{
public:
	// System.Int32 UnityEngine.RenderTextureDescriptor::<width>k__BackingField
	int32_t ___U3CwidthU3Ek__BackingField_0;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<height>k__BackingField
	int32_t ___U3CheightU3Ek__BackingField_1;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<msaaSamples>k__BackingField
	int32_t ___U3CmsaaSamplesU3Ek__BackingField_2;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<volumeDepth>k__BackingField
	int32_t ___U3CvolumeDepthU3Ek__BackingField_3;
	// UnityEngine.RenderTextureFormat UnityEngine.RenderTextureDescriptor::<colorFormat>k__BackingField
	int32_t ___U3CcolorFormatU3Ek__BackingField_4;
	// System.Int32 UnityEngine.RenderTextureDescriptor::_depthBufferBits
	int32_t ____depthBufferBits_5;
	// UnityEngine.Rendering.TextureDimension UnityEngine.RenderTextureDescriptor::<dimension>k__BackingField
	int32_t ___U3CdimensionU3Ek__BackingField_7;
	// UnityEngine.Rendering.ShadowSamplingMode UnityEngine.RenderTextureDescriptor::<shadowSamplingMode>k__BackingField
	int32_t ___U3CshadowSamplingModeU3Ek__BackingField_8;
	// UnityEngine.VRTextureUsage UnityEngine.RenderTextureDescriptor::<vrUsage>k__BackingField
	int32_t ___U3CvrUsageU3Ek__BackingField_9;
	// UnityEngine.RenderTextureCreationFlags UnityEngine.RenderTextureDescriptor::_flags
	int32_t ____flags_10;
	// UnityEngine.RenderTextureMemoryless UnityEngine.RenderTextureDescriptor::<memoryless>k__BackingField
	int32_t ___U3CmemorylessU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CwidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CwidthU3Ek__BackingField_0)); }
	inline int32_t get_U3CwidthU3Ek__BackingField_0() const { return ___U3CwidthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CwidthU3Ek__BackingField_0() { return &___U3CwidthU3Ek__BackingField_0; }
	inline void set_U3CwidthU3Ek__BackingField_0(int32_t value)
	{
		___U3CwidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CheightU3Ek__BackingField_1)); }
	inline int32_t get_U3CheightU3Ek__BackingField_1() const { return ___U3CheightU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CheightU3Ek__BackingField_1() { return &___U3CheightU3Ek__BackingField_1; }
	inline void set_U3CheightU3Ek__BackingField_1(int32_t value)
	{
		___U3CheightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmsaaSamplesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CmsaaSamplesU3Ek__BackingField_2)); }
	inline int32_t get_U3CmsaaSamplesU3Ek__BackingField_2() const { return ___U3CmsaaSamplesU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CmsaaSamplesU3Ek__BackingField_2() { return &___U3CmsaaSamplesU3Ek__BackingField_2; }
	inline void set_U3CmsaaSamplesU3Ek__BackingField_2(int32_t value)
	{
		___U3CmsaaSamplesU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CvolumeDepthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CvolumeDepthU3Ek__BackingField_3)); }
	inline int32_t get_U3CvolumeDepthU3Ek__BackingField_3() const { return ___U3CvolumeDepthU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CvolumeDepthU3Ek__BackingField_3() { return &___U3CvolumeDepthU3Ek__BackingField_3; }
	inline void set_U3CvolumeDepthU3Ek__BackingField_3(int32_t value)
	{
		___U3CvolumeDepthU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CcolorFormatU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CcolorFormatU3Ek__BackingField_4)); }
	inline int32_t get_U3CcolorFormatU3Ek__BackingField_4() const { return ___U3CcolorFormatU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CcolorFormatU3Ek__BackingField_4() { return &___U3CcolorFormatU3Ek__BackingField_4; }
	inline void set_U3CcolorFormatU3Ek__BackingField_4(int32_t value)
	{
		___U3CcolorFormatU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of__depthBufferBits_5() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ____depthBufferBits_5)); }
	inline int32_t get__depthBufferBits_5() const { return ____depthBufferBits_5; }
	inline int32_t* get_address_of__depthBufferBits_5() { return &____depthBufferBits_5; }
	inline void set__depthBufferBits_5(int32_t value)
	{
		____depthBufferBits_5 = value;
	}

	inline static int32_t get_offset_of_U3CdimensionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CdimensionU3Ek__BackingField_7)); }
	inline int32_t get_U3CdimensionU3Ek__BackingField_7() const { return ___U3CdimensionU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CdimensionU3Ek__BackingField_7() { return &___U3CdimensionU3Ek__BackingField_7; }
	inline void set_U3CdimensionU3Ek__BackingField_7(int32_t value)
	{
		___U3CdimensionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CshadowSamplingModeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CshadowSamplingModeU3Ek__BackingField_8)); }
	inline int32_t get_U3CshadowSamplingModeU3Ek__BackingField_8() const { return ___U3CshadowSamplingModeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CshadowSamplingModeU3Ek__BackingField_8() { return &___U3CshadowSamplingModeU3Ek__BackingField_8; }
	inline void set_U3CshadowSamplingModeU3Ek__BackingField_8(int32_t value)
	{
		___U3CshadowSamplingModeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CvrUsageU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CvrUsageU3Ek__BackingField_9)); }
	inline int32_t get_U3CvrUsageU3Ek__BackingField_9() const { return ___U3CvrUsageU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CvrUsageU3Ek__BackingField_9() { return &___U3CvrUsageU3Ek__BackingField_9; }
	inline void set_U3CvrUsageU3Ek__BackingField_9(int32_t value)
	{
		___U3CvrUsageU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of__flags_10() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ____flags_10)); }
	inline int32_t get__flags_10() const { return ____flags_10; }
	inline int32_t* get_address_of__flags_10() { return &____flags_10; }
	inline void set__flags_10(int32_t value)
	{
		____flags_10 = value;
	}

	inline static int32_t get_offset_of_U3CmemorylessU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E, ___U3CmemorylessU3Ek__BackingField_11)); }
	inline int32_t get_U3CmemorylessU3Ek__BackingField_11() const { return ___U3CmemorylessU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CmemorylessU3Ek__BackingField_11() { return &___U3CmemorylessU3Ek__BackingField_11; }
	inline void set_U3CmemorylessU3Ek__BackingField_11(int32_t value)
	{
		___U3CmemorylessU3Ek__BackingField_11 = value;
	}
};

struct RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E_StaticFields
{
public:
	// System.Int32[] UnityEngine.RenderTextureDescriptor::depthFormatBits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___depthFormatBits_6;

public:
	inline static int32_t get_offset_of_depthFormatBits_6() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E_StaticFields, ___depthFormatBits_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_depthFormatBits_6() const { return ___depthFormatBits_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_depthFormatBits_6() { return &___depthFormatBits_6; }
	inline void set_depthFormatBits_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___depthFormatBits_6 = value;
		Il2CppCodeGenWriteBarrier((&___depthFormatBits_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREDESCRIPTOR_T74FEC57A54F89E11748E1865F7DCA3565BFAF58E_H
#ifndef COLORPARAMETER_TB218BF7292D70153A62E291FDF6C7534D5CA084B_H
#define COLORPARAMETER_TB218BF7292D70153A62E291FDF6C7534D5CA084B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorParameter
struct  ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B  : public ParameterOverride_1_t23F21A3C4BDF3D75D33F5E24B8462A2A7376387E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPARAMETER_TB218BF7292D70153A62E291FDF6C7534D5CA084B_H
#ifndef HISTOGRAMMONITOR_T20EDD407C13FD6F9B96233D0925863ACD10D7FA2_H
#define HISTOGRAMMONITOR_T20EDD407C13FD6F9B96233D0925863ACD10D7FA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.HistogramMonitor
struct  HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2  : public Monitor_t9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.HistogramMonitor::width
	int32_t ___width_2;
	// System.Int32 UnityEngine.Rendering.PostProcessing.HistogramMonitor::height
	int32_t ___height_3;
	// UnityEngine.Rendering.PostProcessing.HistogramMonitor_Channel UnityEngine.Rendering.PostProcessing.HistogramMonitor::channel
	int32_t ___channel_4;
	// UnityEngine.ComputeBuffer UnityEngine.Rendering.PostProcessing.HistogramMonitor::m_Data
	ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * ___m_Data_5;

public:
	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_channel_4() { return static_cast<int32_t>(offsetof(HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2, ___channel_4)); }
	inline int32_t get_channel_4() const { return ___channel_4; }
	inline int32_t* get_address_of_channel_4() { return &___channel_4; }
	inline void set_channel_4(int32_t value)
	{
		___channel_4 = value;
	}

	inline static int32_t get_offset_of_m_Data_5() { return static_cast<int32_t>(offsetof(HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2, ___m_Data_5)); }
	inline ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * get_m_Data_5() const { return ___m_Data_5; }
	inline ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 ** get_address_of_m_Data_5() { return &___m_Data_5; }
	inline void set_m_Data_5(ComputeBuffer_t52D8926E1D54293AD28F4C29FE3F5363749B0FE5 * value)
	{
		___m_Data_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Data_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HISTOGRAMMONITOR_T20EDD407C13FD6F9B96233D0925863ACD10D7FA2_H
#ifndef PARAMETEROVERRIDE_1_T58C4C7E6C294A51111B04879C5E26C015499F38E_H
#define PARAMETEROVERRIDE_1_T58C4C7E6C294A51111B04879C5E26C015499F38E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPreset>
struct  ParameterOverride_1_t58C4C7E6C294A51111B04879C5E26C015499F38E  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t58C4C7E6C294A51111B04879C5E26C015499F38E, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T58C4C7E6C294A51111B04879C5E26C015499F38E_H
#ifndef PARAMETEROVERRIDE_1_T67889465519D37D4AD6A29F9DE63AA17E0419BDF_H
#define PARAMETEROVERRIDE_1_T67889465519D37D4AD6A29F9DE63AA17E0419BDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolution>
struct  ParameterOverride_1_t67889465519D37D4AD6A29F9DE63AA17E0419BDF  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t67889465519D37D4AD6A29F9DE63AA17E0419BDF, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T67889465519D37D4AD6A29F9DE63AA17E0419BDF_H
#ifndef PARAMETEROVERRIDE_1_T6BB8846771999798B8AF83A70C7DBBAE1A4670A0_H
#define PARAMETEROVERRIDE_1_T6BB8846771999798B8AF83A70C7DBBAE1A4670A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.VignetteMode>
struct  ParameterOverride_1_t6BB8846771999798B8AF83A70C7DBBAE1A4670A0  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t6BB8846771999798B8AF83A70C7DBBAE1A4670A0, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T6BB8846771999798B8AF83A70C7DBBAE1A4670A0_H
#ifndef POSTPROCESSDEBUGLAYER_T4E9A4AB3CC038EC6A976F756D455A89AD156AC39_H
#define POSTPROCESSDEBUGLAYER_T4E9A4AB3CC038EC6A976F756D455A89AD156AC39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer
struct  PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.LightMeterMonitor UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::lightMeter
	LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075 * ___lightMeter_0;
	// UnityEngine.Rendering.PostProcessing.HistogramMonitor UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::histogram
	HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2 * ___histogram_1;
	// UnityEngine.Rendering.PostProcessing.WaveformMonitor UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::waveform
	WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6 * ___waveform_2;
	// UnityEngine.Rendering.PostProcessing.VectorscopeMonitor UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::vectorscope
	VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B * ___vectorscope_3;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Rendering.PostProcessing.MonitorType,UnityEngine.Rendering.PostProcessing.Monitor> UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::m_Monitors
	Dictionary_2_t6F5E43428F07CBA0DAA38F4FFD419595B52239E4 * ___m_Monitors_4;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::frameWidth
	int32_t ___frameWidth_5;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::frameHeight
	int32_t ___frameHeight_6;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::<debugOverlayTarget>k__BackingField
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___U3CdebugOverlayTargetU3Ek__BackingField_7;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::<debugOverlayActive>k__BackingField
	bool ___U3CdebugOverlayActiveU3Ek__BackingField_8;
	// UnityEngine.Rendering.PostProcessing.DebugOverlay UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::<debugOverlay>k__BackingField
	int32_t ___U3CdebugOverlayU3Ek__BackingField_9;
	// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer_OverlaySettings UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer::overlaySettings
	OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6 * ___overlaySettings_10;

public:
	inline static int32_t get_offset_of_lightMeter_0() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___lightMeter_0)); }
	inline LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075 * get_lightMeter_0() const { return ___lightMeter_0; }
	inline LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075 ** get_address_of_lightMeter_0() { return &___lightMeter_0; }
	inline void set_lightMeter_0(LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075 * value)
	{
		___lightMeter_0 = value;
		Il2CppCodeGenWriteBarrier((&___lightMeter_0), value);
	}

	inline static int32_t get_offset_of_histogram_1() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___histogram_1)); }
	inline HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2 * get_histogram_1() const { return ___histogram_1; }
	inline HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2 ** get_address_of_histogram_1() { return &___histogram_1; }
	inline void set_histogram_1(HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2 * value)
	{
		___histogram_1 = value;
		Il2CppCodeGenWriteBarrier((&___histogram_1), value);
	}

	inline static int32_t get_offset_of_waveform_2() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___waveform_2)); }
	inline WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6 * get_waveform_2() const { return ___waveform_2; }
	inline WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6 ** get_address_of_waveform_2() { return &___waveform_2; }
	inline void set_waveform_2(WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6 * value)
	{
		___waveform_2 = value;
		Il2CppCodeGenWriteBarrier((&___waveform_2), value);
	}

	inline static int32_t get_offset_of_vectorscope_3() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___vectorscope_3)); }
	inline VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B * get_vectorscope_3() const { return ___vectorscope_3; }
	inline VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B ** get_address_of_vectorscope_3() { return &___vectorscope_3; }
	inline void set_vectorscope_3(VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B * value)
	{
		___vectorscope_3 = value;
		Il2CppCodeGenWriteBarrier((&___vectorscope_3), value);
	}

	inline static int32_t get_offset_of_m_Monitors_4() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___m_Monitors_4)); }
	inline Dictionary_2_t6F5E43428F07CBA0DAA38F4FFD419595B52239E4 * get_m_Monitors_4() const { return ___m_Monitors_4; }
	inline Dictionary_2_t6F5E43428F07CBA0DAA38F4FFD419595B52239E4 ** get_address_of_m_Monitors_4() { return &___m_Monitors_4; }
	inline void set_m_Monitors_4(Dictionary_2_t6F5E43428F07CBA0DAA38F4FFD419595B52239E4 * value)
	{
		___m_Monitors_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Monitors_4), value);
	}

	inline static int32_t get_offset_of_frameWidth_5() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___frameWidth_5)); }
	inline int32_t get_frameWidth_5() const { return ___frameWidth_5; }
	inline int32_t* get_address_of_frameWidth_5() { return &___frameWidth_5; }
	inline void set_frameWidth_5(int32_t value)
	{
		___frameWidth_5 = value;
	}

	inline static int32_t get_offset_of_frameHeight_6() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___frameHeight_6)); }
	inline int32_t get_frameHeight_6() const { return ___frameHeight_6; }
	inline int32_t* get_address_of_frameHeight_6() { return &___frameHeight_6; }
	inline void set_frameHeight_6(int32_t value)
	{
		___frameHeight_6 = value;
	}

	inline static int32_t get_offset_of_U3CdebugOverlayTargetU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___U3CdebugOverlayTargetU3Ek__BackingField_7)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_U3CdebugOverlayTargetU3Ek__BackingField_7() const { return ___U3CdebugOverlayTargetU3Ek__BackingField_7; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_U3CdebugOverlayTargetU3Ek__BackingField_7() { return &___U3CdebugOverlayTargetU3Ek__BackingField_7; }
	inline void set_U3CdebugOverlayTargetU3Ek__BackingField_7(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___U3CdebugOverlayTargetU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdebugOverlayTargetU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CdebugOverlayActiveU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___U3CdebugOverlayActiveU3Ek__BackingField_8)); }
	inline bool get_U3CdebugOverlayActiveU3Ek__BackingField_8() const { return ___U3CdebugOverlayActiveU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CdebugOverlayActiveU3Ek__BackingField_8() { return &___U3CdebugOverlayActiveU3Ek__BackingField_8; }
	inline void set_U3CdebugOverlayActiveU3Ek__BackingField_8(bool value)
	{
		___U3CdebugOverlayActiveU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CdebugOverlayU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___U3CdebugOverlayU3Ek__BackingField_9)); }
	inline int32_t get_U3CdebugOverlayU3Ek__BackingField_9() const { return ___U3CdebugOverlayU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CdebugOverlayU3Ek__BackingField_9() { return &___U3CdebugOverlayU3Ek__BackingField_9; }
	inline void set_U3CdebugOverlayU3Ek__BackingField_9(int32_t value)
	{
		___U3CdebugOverlayU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_overlaySettings_10() { return static_cast<int32_t>(offsetof(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39, ___overlaySettings_10)); }
	inline OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6 * get_overlaySettings_10() const { return ___overlaySettings_10; }
	inline OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6 ** get_address_of_overlaySettings_10() { return &___overlaySettings_10; }
	inline void set_overlaySettings_10(OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6 * value)
	{
		___overlaySettings_10 = value;
		Il2CppCodeGenWriteBarrier((&___overlaySettings_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSDEBUGLAYER_T4E9A4AB3CC038EC6A976F756D455A89AD156AC39_H
#ifndef OVERLAYSETTINGS_T386F6AF515101C43472A020FF0F0B03C0F3607B6_H
#define OVERLAYSETTINGS_T386F6AF515101C43472A020FF0F0B03C0F3607B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer_OverlaySettings
struct  OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer_OverlaySettings::linearDepth
	bool ___linearDepth_0;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer_OverlaySettings::motionColorIntensity
	float ___motionColorIntensity_1;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer_OverlaySettings::motionGridSize
	int32_t ___motionGridSize_2;
	// UnityEngine.Rendering.PostProcessing.ColorBlindnessType UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer_OverlaySettings::colorBlindnessType
	int32_t ___colorBlindnessType_3;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer_OverlaySettings::colorBlindnessStrength
	float ___colorBlindnessStrength_4;

public:
	inline static int32_t get_offset_of_linearDepth_0() { return static_cast<int32_t>(offsetof(OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6, ___linearDepth_0)); }
	inline bool get_linearDepth_0() const { return ___linearDepth_0; }
	inline bool* get_address_of_linearDepth_0() { return &___linearDepth_0; }
	inline void set_linearDepth_0(bool value)
	{
		___linearDepth_0 = value;
	}

	inline static int32_t get_offset_of_motionColorIntensity_1() { return static_cast<int32_t>(offsetof(OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6, ___motionColorIntensity_1)); }
	inline float get_motionColorIntensity_1() const { return ___motionColorIntensity_1; }
	inline float* get_address_of_motionColorIntensity_1() { return &___motionColorIntensity_1; }
	inline void set_motionColorIntensity_1(float value)
	{
		___motionColorIntensity_1 = value;
	}

	inline static int32_t get_offset_of_motionGridSize_2() { return static_cast<int32_t>(offsetof(OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6, ___motionGridSize_2)); }
	inline int32_t get_motionGridSize_2() const { return ___motionGridSize_2; }
	inline int32_t* get_address_of_motionGridSize_2() { return &___motionGridSize_2; }
	inline void set_motionGridSize_2(int32_t value)
	{
		___motionGridSize_2 = value;
	}

	inline static int32_t get_offset_of_colorBlindnessType_3() { return static_cast<int32_t>(offsetof(OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6, ___colorBlindnessType_3)); }
	inline int32_t get_colorBlindnessType_3() const { return ___colorBlindnessType_3; }
	inline int32_t* get_address_of_colorBlindnessType_3() { return &___colorBlindnessType_3; }
	inline void set_colorBlindnessType_3(int32_t value)
	{
		___colorBlindnessType_3 = value;
	}

	inline static int32_t get_offset_of_colorBlindnessStrength_4() { return static_cast<int32_t>(offsetof(OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6, ___colorBlindnessStrength_4)); }
	inline float get_colorBlindnessStrength_4() const { return ___colorBlindnessStrength_4; }
	inline float* get_address_of_colorBlindnessStrength_4() { return &___colorBlindnessStrength_4; }
	inline void set_colorBlindnessStrength_4(float value)
	{
		___colorBlindnessStrength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERLAYSETTINGS_T386F6AF515101C43472A020FF0F0B03C0F3607B6_H
#ifndef U3CU3EC__DISPLAYCLASS51_0_T5A29CC1C56D8C5C3B797C8CEBB7C6195C64F5EBF_H
#define U3CU3EC__DISPLAYCLASS51_0_T5A29CC1C56D8C5C3B797C8CEBB7C6195C64F5EBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c__DisplayClass51_0
struct  U3CU3Ec__DisplayClass51_0_t5A29CC1C56D8C5C3B797C8CEBB7C6195C64F5EBF  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessEvent UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c__DisplayClass51_0::evt
	int32_t ___evt_0;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessBundle> UnityEngine.Rendering.PostProcessing.PostProcessLayer_<>c__DisplayClass51_0::effects
	List_1_tD1CDEEC5721FB71CC260F433093C17D4D3A8377B * ___effects_1;

public:
	inline static int32_t get_offset_of_evt_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass51_0_t5A29CC1C56D8C5C3B797C8CEBB7C6195C64F5EBF, ___evt_0)); }
	inline int32_t get_evt_0() const { return ___evt_0; }
	inline int32_t* get_address_of_evt_0() { return &___evt_0; }
	inline void set_evt_0(int32_t value)
	{
		___evt_0 = value;
	}

	inline static int32_t get_offset_of_effects_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass51_0_t5A29CC1C56D8C5C3B797C8CEBB7C6195C64F5EBF, ___effects_1)); }
	inline List_1_tD1CDEEC5721FB71CC260F433093C17D4D3A8377B * get_effects_1() const { return ___effects_1; }
	inline List_1_tD1CDEEC5721FB71CC260F433093C17D4D3A8377B ** get_address_of_effects_1() { return &___effects_1; }
	inline void set_effects_1(List_1_tD1CDEEC5721FB71CC260F433093C17D4D3A8377B * value)
	{
		___effects_1 = value;
		Il2CppCodeGenWriteBarrier((&___effects_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS51_0_T5A29CC1C56D8C5C3B797C8CEBB7C6195C64F5EBF_H
#ifndef QUALITYPRESET_TDC55FF6F041F963732F2EAAE3AF685BCED564EF9_H
#define QUALITYPRESET_TDC55FF6F041F963732F2EAAE3AF685BCED564EF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer_QualityPreset
struct  QualityPreset_tDC55FF6F041F963732F2EAAE3AF685BCED564EF9  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer_QualityPreset::maximumIterationCount
	int32_t ___maximumIterationCount_0;
	// System.Single UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer_QualityPreset::thickness
	float ___thickness_1;
	// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolution UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionsRenderer_QualityPreset::downsampling
	int32_t ___downsampling_2;

public:
	inline static int32_t get_offset_of_maximumIterationCount_0() { return static_cast<int32_t>(offsetof(QualityPreset_tDC55FF6F041F963732F2EAAE3AF685BCED564EF9, ___maximumIterationCount_0)); }
	inline int32_t get_maximumIterationCount_0() const { return ___maximumIterationCount_0; }
	inline int32_t* get_address_of_maximumIterationCount_0() { return &___maximumIterationCount_0; }
	inline void set_maximumIterationCount_0(int32_t value)
	{
		___maximumIterationCount_0 = value;
	}

	inline static int32_t get_offset_of_thickness_1() { return static_cast<int32_t>(offsetof(QualityPreset_tDC55FF6F041F963732F2EAAE3AF685BCED564EF9, ___thickness_1)); }
	inline float get_thickness_1() const { return ___thickness_1; }
	inline float* get_address_of_thickness_1() { return &___thickness_1; }
	inline void set_thickness_1(float value)
	{
		___thickness_1 = value;
	}

	inline static int32_t get_offset_of_downsampling_2() { return static_cast<int32_t>(offsetof(QualityPreset_tDC55FF6F041F963732F2EAAE3AF685BCED564EF9, ___downsampling_2)); }
	inline int32_t get_downsampling_2() const { return ___downsampling_2; }
	inline int32_t* get_address_of_downsampling_2() { return &___downsampling_2; }
	inline void set_downsampling_2(int32_t value)
	{
		___downsampling_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYPRESET_TDC55FF6F041F963732F2EAAE3AF685BCED564EF9_H
#ifndef SUBPIXELMORPHOLOGICALANTIALIASING_TDE9497AEBB401059AB2F21890704B15C374B79DD_H
#define SUBPIXELMORPHOLOGICALANTIALIASING_TDE9497AEBB401059AB2F21890704B15C374B79DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing
struct  SubpixelMorphologicalAntialiasing_tDE9497AEBB401059AB2F21890704B15C374B79DD  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing_Quality UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing::quality
	int32_t ___quality_0;

public:
	inline static int32_t get_offset_of_quality_0() { return static_cast<int32_t>(offsetof(SubpixelMorphologicalAntialiasing_tDE9497AEBB401059AB2F21890704B15C374B79DD, ___quality_0)); }
	inline int32_t get_quality_0() const { return ___quality_0; }
	inline int32_t* get_address_of_quality_0() { return &___quality_0; }
	inline void set_quality_0(int32_t value)
	{
		___quality_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBPIXELMORPHOLOGICALANTIALIASING_TDE9497AEBB401059AB2F21890704B15C374B79DD_H
#ifndef TEXTUREPARAMETER_T585968DEF0992D732C2826195CA4F7D721462A87_H
#define TEXTUREPARAMETER_T585968DEF0992D732C2826195CA4F7D721462A87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TextureParameter
struct  TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87  : public ParameterOverride_1_t2ACDAC034DCBB9DE5BD78EFD31136BC66F595DFA
{
public:
	// UnityEngine.Rendering.PostProcessing.TextureParameterDefault UnityEngine.Rendering.PostProcessing.TextureParameter::defaultState
	int32_t ___defaultState_2;

public:
	inline static int32_t get_offset_of_defaultState_2() { return static_cast<int32_t>(offsetof(TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87, ___defaultState_2)); }
	inline int32_t get_defaultState_2() const { return ___defaultState_2; }
	inline int32_t* get_address_of_defaultState_2() { return &___defaultState_2; }
	inline void set_defaultState_2(int32_t value)
	{
		___defaultState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPARAMETER_T585968DEF0992D732C2826195CA4F7D721462A87_H
#ifndef VECTOR2PARAMETER_TBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2_H
#define VECTOR2PARAMETER_TBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Vector2Parameter
struct  Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2  : public ParameterOverride_1_tED4B89A13ADA28A3E8CFEDF351B518B2F91E1004
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2PARAMETER_TBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2_H
#ifndef VECTOR4PARAMETER_T9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888_H
#define VECTOR4PARAMETER_T9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Vector4Parameter
struct  Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888  : public ParameterOverride_1_tA725C89A0A7CBF68BEB3E1B19D91328AFAF9710F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4PARAMETER_T9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888_H
#ifndef RENDERTARGETIDENTIFIER_TB7480EE944FC70E0AB7D499DB17D119EB65B0F5B_H
#define RENDERTARGETIDENTIFIER_TB7480EE944FC70E0AB7D499DB17D119EB65B0F5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.RenderTargetIdentifier
struct  RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B 
{
public:
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_3;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_4;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_5;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_6;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_NameID_1() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B, ___m_NameID_1)); }
	inline int32_t get_m_NameID_1() const { return ___m_NameID_1; }
	inline int32_t* get_address_of_m_NameID_1() { return &___m_NameID_1; }
	inline void set_m_NameID_1(int32_t value)
	{
		___m_NameID_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_BufferPointer_3() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B, ___m_BufferPointer_3)); }
	inline intptr_t get_m_BufferPointer_3() const { return ___m_BufferPointer_3; }
	inline intptr_t* get_address_of_m_BufferPointer_3() { return &___m_BufferPointer_3; }
	inline void set_m_BufferPointer_3(intptr_t value)
	{
		___m_BufferPointer_3 = value;
	}

	inline static int32_t get_offset_of_m_MipLevel_4() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B, ___m_MipLevel_4)); }
	inline int32_t get_m_MipLevel_4() const { return ___m_MipLevel_4; }
	inline int32_t* get_address_of_m_MipLevel_4() { return &___m_MipLevel_4; }
	inline void set_m_MipLevel_4(int32_t value)
	{
		___m_MipLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_CubeFace_5() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B, ___m_CubeFace_5)); }
	inline int32_t get_m_CubeFace_5() const { return ___m_CubeFace_5; }
	inline int32_t* get_address_of_m_CubeFace_5() { return &___m_CubeFace_5; }
	inline void set_m_CubeFace_5(int32_t value)
	{
		___m_CubeFace_5 = value;
	}

	inline static int32_t get_offset_of_m_DepthSlice_6() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B, ___m_DepthSlice_6)); }
	inline int32_t get_m_DepthSlice_6() const { return ___m_DepthSlice_6; }
	inline int32_t* get_address_of_m_DepthSlice_6() { return &___m_DepthSlice_6; }
	inline void set_m_DepthSlice_6(int32_t value)
	{
		___m_DepthSlice_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTARGETIDENTIFIER_TB7480EE944FC70E0AB7D499DB17D119EB65B0F5B_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef POSTPROCESSEFFECTSETTINGS_TF399FB819B12B3398DDDEF1AAA1FB0685C524042_H
#define POSTPROCESSEFFECTSETTINGS_TF399FB819B12B3398DDDEF1AAA1FB0685C524042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings
struct  PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::active
	bool ___active_4;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::enabled
	BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * ___enabled_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.PostProcessing.ParameterOverride> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::parameters
	ReadOnlyCollection_1_t2D7596D677CC341719EF47DD23F94617FB662DED * ___parameters_6;

public:
	inline static int32_t get_offset_of_active_4() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042, ___active_4)); }
	inline bool get_active_4() const { return ___active_4; }
	inline bool* get_address_of_active_4() { return &___active_4; }
	inline void set_active_4(bool value)
	{
		___active_4 = value;
	}

	inline static int32_t get_offset_of_enabled_5() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042, ___enabled_5)); }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * get_enabled_5() const { return ___enabled_5; }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE ** get_address_of_enabled_5() { return &___enabled_5; }
	inline void set_enabled_5(BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * value)
	{
		___enabled_5 = value;
		Il2CppCodeGenWriteBarrier((&___enabled_5), value);
	}

	inline static int32_t get_offset_of_parameters_6() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042, ___parameters_6)); }
	inline ReadOnlyCollection_1_t2D7596D677CC341719EF47DD23F94617FB662DED * get_parameters_6() const { return ___parameters_6; }
	inline ReadOnlyCollection_1_t2D7596D677CC341719EF47DD23F94617FB662DED ** get_address_of_parameters_6() { return &___parameters_6; }
	inline void set_parameters_6(ReadOnlyCollection_1_t2D7596D677CC341719EF47DD23F94617FB662DED * value)
	{
		___parameters_6 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTSETTINGS_TF399FB819B12B3398DDDEF1AAA1FB0685C524042_H
#ifndef POSTPROCESSPROFILE_TEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E_H
#define POSTPROCESSPROFILE_TEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessProfile
struct  PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings> UnityEngine.Rendering.PostProcessing.PostProcessProfile::settings
	List_1_t1F47AA13007605005DB7DD4BBCBD45CFCD05BD91 * ___settings_4;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessProfile::isDirty
	bool ___isDirty_5;

public:
	inline static int32_t get_offset_of_settings_4() { return static_cast<int32_t>(offsetof(PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E, ___settings_4)); }
	inline List_1_t1F47AA13007605005DB7DD4BBCBD45CFCD05BD91 * get_settings_4() const { return ___settings_4; }
	inline List_1_t1F47AA13007605005DB7DD4BBCBD45CFCD05BD91 ** get_address_of_settings_4() { return &___settings_4; }
	inline void set_settings_4(List_1_t1F47AA13007605005DB7DD4BBCBD45CFCD05BD91 * value)
	{
		___settings_4 = value;
		Il2CppCodeGenWriteBarrier((&___settings_4), value);
	}

	inline static int32_t get_offset_of_isDirty_5() { return static_cast<int32_t>(offsetof(PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E, ___isDirty_5)); }
	inline bool get_isDirty_5() const { return ___isDirty_5; }
	inline bool* get_address_of_isDirty_5() { return &___isDirty_5; }
	inline void set_isDirty_5(bool value)
	{
		___isDirty_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSPROFILE_TEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E_H
#ifndef POSTPROCESSRENDERCONTEXT_TF3C9D7123D8B250A5E5FE6743B2319890F0168BB_H
#define POSTPROCESSRENDERCONTEXT_TF3C9D7123D8B250A5E5FE6743B2319890F0168BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessRenderContext
struct  PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::m_Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_Camera_0;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<command>k__BackingField
	CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * ___U3CcommandU3Ek__BackingField_1;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<source>k__BackingField
	RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B  ___U3CsourceU3Ek__BackingField_2;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<destination>k__BackingField
	RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B  ___U3CdestinationU3Ek__BackingField_3;
	// UnityEngine.RenderTextureFormat UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<sourceFormat>k__BackingField
	int32_t ___U3CsourceFormatU3Ek__BackingField_4;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<flip>k__BackingField
	bool ___U3CflipU3Ek__BackingField_5;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<resources>k__BackingField
	PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * ___U3CresourcesU3Ek__BackingField_6;
	// UnityEngine.Rendering.PostProcessing.PropertySheetFactory UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<propertySheets>k__BackingField
	PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 * ___U3CpropertySheetsU3Ek__BackingField_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<userData>k__BackingField
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___U3CuserDataU3Ek__BackingField_8;
	// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<debugLayer>k__BackingField
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39 * ___U3CdebugLayerU3Ek__BackingField_9;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<width>k__BackingField
	int32_t ___U3CwidthU3Ek__BackingField_10;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<height>k__BackingField
	int32_t ___U3CheightU3Ek__BackingField_11;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<stereoActive>k__BackingField
	bool ___U3CstereoActiveU3Ek__BackingField_12;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<xrActiveEye>k__BackingField
	int32_t ___U3CxrActiveEyeU3Ek__BackingField_13;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<numberOfEyes>k__BackingField
	int32_t ___U3CnumberOfEyesU3Ek__BackingField_14;
	// UnityEngine.Rendering.PostProcessing.PostProcessRenderContext_StereoRenderingMode UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<stereoRenderingMode>k__BackingField
	int32_t ___U3CstereoRenderingModeU3Ek__BackingField_15;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<screenWidth>k__BackingField
	int32_t ___U3CscreenWidthU3Ek__BackingField_16;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<screenHeight>k__BackingField
	int32_t ___U3CscreenHeightU3Ek__BackingField_17;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<isSceneView>k__BackingField
	bool ___U3CisSceneViewU3Ek__BackingField_18;
	// UnityEngine.Rendering.PostProcessing.PostProcessLayer_Antialiasing UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<antialiasing>k__BackingField
	int32_t ___U3CantialiasingU3Ek__BackingField_19;
	// UnityEngine.Rendering.PostProcessing.TemporalAntialiasing UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::<temporalAntialiasing>k__BackingField
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1 * ___U3CtemporalAntialiasingU3Ek__BackingField_20;
	// UnityEngine.Rendering.PostProcessing.PropertySheet UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::uberSheet
	PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * ___uberSheet_21;
	// UnityEngine.Texture UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::autoExposureTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___autoExposureTexture_22;
	// UnityEngine.Rendering.PostProcessing.LogHistogram UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::logHistogram
	LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79 * ___logHistogram_23;
	// UnityEngine.Texture UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::logLut
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___logLut_24;
	// UnityEngine.Rendering.PostProcessing.AutoExposure UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::autoExposure
	AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A * ___autoExposure_25;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::bloomBufferNameID
	int32_t ___bloomBufferNameID_26;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::physicalCamera
	bool ___physicalCamera_27;
	// UnityEngine.RenderTextureDescriptor UnityEngine.Rendering.PostProcessing.PostProcessRenderContext::m_sourceDescriptor
	RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E  ___m_sourceDescriptor_28;

public:
	inline static int32_t get_offset_of_m_Camera_0() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___m_Camera_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_Camera_0() const { return ___m_Camera_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_Camera_0() { return &___m_Camera_0; }
	inline void set_m_Camera_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_0), value);
	}

	inline static int32_t get_offset_of_U3CcommandU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CcommandU3Ek__BackingField_1)); }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * get_U3CcommandU3Ek__BackingField_1() const { return ___U3CcommandU3Ek__BackingField_1; }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD ** get_address_of_U3CcommandU3Ek__BackingField_1() { return &___U3CcommandU3Ek__BackingField_1; }
	inline void set_U3CcommandU3Ek__BackingField_1(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * value)
	{
		___U3CcommandU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcommandU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CsourceU3Ek__BackingField_2)); }
	inline RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B  get_U3CsourceU3Ek__BackingField_2() const { return ___U3CsourceU3Ek__BackingField_2; }
	inline RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B * get_address_of_U3CsourceU3Ek__BackingField_2() { return &___U3CsourceU3Ek__BackingField_2; }
	inline void set_U3CsourceU3Ek__BackingField_2(RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B  value)
	{
		___U3CsourceU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CdestinationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CdestinationU3Ek__BackingField_3)); }
	inline RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B  get_U3CdestinationU3Ek__BackingField_3() const { return ___U3CdestinationU3Ek__BackingField_3; }
	inline RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B * get_address_of_U3CdestinationU3Ek__BackingField_3() { return &___U3CdestinationU3Ek__BackingField_3; }
	inline void set_U3CdestinationU3Ek__BackingField_3(RenderTargetIdentifier_tB7480EE944FC70E0AB7D499DB17D119EB65B0F5B  value)
	{
		___U3CdestinationU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsourceFormatU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CsourceFormatU3Ek__BackingField_4)); }
	inline int32_t get_U3CsourceFormatU3Ek__BackingField_4() const { return ___U3CsourceFormatU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CsourceFormatU3Ek__BackingField_4() { return &___U3CsourceFormatU3Ek__BackingField_4; }
	inline void set_U3CsourceFormatU3Ek__BackingField_4(int32_t value)
	{
		___U3CsourceFormatU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CflipU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CflipU3Ek__BackingField_5)); }
	inline bool get_U3CflipU3Ek__BackingField_5() const { return ___U3CflipU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CflipU3Ek__BackingField_5() { return &___U3CflipU3Ek__BackingField_5; }
	inline void set_U3CflipU3Ek__BackingField_5(bool value)
	{
		___U3CflipU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CresourcesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CresourcesU3Ek__BackingField_6)); }
	inline PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * get_U3CresourcesU3Ek__BackingField_6() const { return ___U3CresourcesU3Ek__BackingField_6; }
	inline PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B ** get_address_of_U3CresourcesU3Ek__BackingField_6() { return &___U3CresourcesU3Ek__BackingField_6; }
	inline void set_U3CresourcesU3Ek__BackingField_6(PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * value)
	{
		___U3CresourcesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresourcesU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpropertySheetsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CpropertySheetsU3Ek__BackingField_7)); }
	inline PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 * get_U3CpropertySheetsU3Ek__BackingField_7() const { return ___U3CpropertySheetsU3Ek__BackingField_7; }
	inline PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 ** get_address_of_U3CpropertySheetsU3Ek__BackingField_7() { return &___U3CpropertySheetsU3Ek__BackingField_7; }
	inline void set_U3CpropertySheetsU3Ek__BackingField_7(PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 * value)
	{
		___U3CpropertySheetsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpropertySheetsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CuserDataU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CuserDataU3Ek__BackingField_8)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_U3CuserDataU3Ek__BackingField_8() const { return ___U3CuserDataU3Ek__BackingField_8; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_U3CuserDataU3Ek__BackingField_8() { return &___U3CuserDataU3Ek__BackingField_8; }
	inline void set_U3CuserDataU3Ek__BackingField_8(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___U3CuserDataU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserDataU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CdebugLayerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CdebugLayerU3Ek__BackingField_9)); }
	inline PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39 * get_U3CdebugLayerU3Ek__BackingField_9() const { return ___U3CdebugLayerU3Ek__BackingField_9; }
	inline PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39 ** get_address_of_U3CdebugLayerU3Ek__BackingField_9() { return &___U3CdebugLayerU3Ek__BackingField_9; }
	inline void set_U3CdebugLayerU3Ek__BackingField_9(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39 * value)
	{
		___U3CdebugLayerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdebugLayerU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CwidthU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CwidthU3Ek__BackingField_10)); }
	inline int32_t get_U3CwidthU3Ek__BackingField_10() const { return ___U3CwidthU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CwidthU3Ek__BackingField_10() { return &___U3CwidthU3Ek__BackingField_10; }
	inline void set_U3CwidthU3Ek__BackingField_10(int32_t value)
	{
		___U3CwidthU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CheightU3Ek__BackingField_11)); }
	inline int32_t get_U3CheightU3Ek__BackingField_11() const { return ___U3CheightU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CheightU3Ek__BackingField_11() { return &___U3CheightU3Ek__BackingField_11; }
	inline void set_U3CheightU3Ek__BackingField_11(int32_t value)
	{
		___U3CheightU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CstereoActiveU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CstereoActiveU3Ek__BackingField_12)); }
	inline bool get_U3CstereoActiveU3Ek__BackingField_12() const { return ___U3CstereoActiveU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CstereoActiveU3Ek__BackingField_12() { return &___U3CstereoActiveU3Ek__BackingField_12; }
	inline void set_U3CstereoActiveU3Ek__BackingField_12(bool value)
	{
		___U3CstereoActiveU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CxrActiveEyeU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CxrActiveEyeU3Ek__BackingField_13)); }
	inline int32_t get_U3CxrActiveEyeU3Ek__BackingField_13() const { return ___U3CxrActiveEyeU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CxrActiveEyeU3Ek__BackingField_13() { return &___U3CxrActiveEyeU3Ek__BackingField_13; }
	inline void set_U3CxrActiveEyeU3Ek__BackingField_13(int32_t value)
	{
		___U3CxrActiveEyeU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CnumberOfEyesU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CnumberOfEyesU3Ek__BackingField_14)); }
	inline int32_t get_U3CnumberOfEyesU3Ek__BackingField_14() const { return ___U3CnumberOfEyesU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CnumberOfEyesU3Ek__BackingField_14() { return &___U3CnumberOfEyesU3Ek__BackingField_14; }
	inline void set_U3CnumberOfEyesU3Ek__BackingField_14(int32_t value)
	{
		___U3CnumberOfEyesU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CstereoRenderingModeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CstereoRenderingModeU3Ek__BackingField_15)); }
	inline int32_t get_U3CstereoRenderingModeU3Ek__BackingField_15() const { return ___U3CstereoRenderingModeU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CstereoRenderingModeU3Ek__BackingField_15() { return &___U3CstereoRenderingModeU3Ek__BackingField_15; }
	inline void set_U3CstereoRenderingModeU3Ek__BackingField_15(int32_t value)
	{
		___U3CstereoRenderingModeU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CscreenWidthU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CscreenWidthU3Ek__BackingField_16)); }
	inline int32_t get_U3CscreenWidthU3Ek__BackingField_16() const { return ___U3CscreenWidthU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CscreenWidthU3Ek__BackingField_16() { return &___U3CscreenWidthU3Ek__BackingField_16; }
	inline void set_U3CscreenWidthU3Ek__BackingField_16(int32_t value)
	{
		___U3CscreenWidthU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CscreenHeightU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CscreenHeightU3Ek__BackingField_17)); }
	inline int32_t get_U3CscreenHeightU3Ek__BackingField_17() const { return ___U3CscreenHeightU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CscreenHeightU3Ek__BackingField_17() { return &___U3CscreenHeightU3Ek__BackingField_17; }
	inline void set_U3CscreenHeightU3Ek__BackingField_17(int32_t value)
	{
		___U3CscreenHeightU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CisSceneViewU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CisSceneViewU3Ek__BackingField_18)); }
	inline bool get_U3CisSceneViewU3Ek__BackingField_18() const { return ___U3CisSceneViewU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CisSceneViewU3Ek__BackingField_18() { return &___U3CisSceneViewU3Ek__BackingField_18; }
	inline void set_U3CisSceneViewU3Ek__BackingField_18(bool value)
	{
		___U3CisSceneViewU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CantialiasingU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CantialiasingU3Ek__BackingField_19)); }
	inline int32_t get_U3CantialiasingU3Ek__BackingField_19() const { return ___U3CantialiasingU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CantialiasingU3Ek__BackingField_19() { return &___U3CantialiasingU3Ek__BackingField_19; }
	inline void set_U3CantialiasingU3Ek__BackingField_19(int32_t value)
	{
		___U3CantialiasingU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CtemporalAntialiasingU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___U3CtemporalAntialiasingU3Ek__BackingField_20)); }
	inline TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1 * get_U3CtemporalAntialiasingU3Ek__BackingField_20() const { return ___U3CtemporalAntialiasingU3Ek__BackingField_20; }
	inline TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1 ** get_address_of_U3CtemporalAntialiasingU3Ek__BackingField_20() { return &___U3CtemporalAntialiasingU3Ek__BackingField_20; }
	inline void set_U3CtemporalAntialiasingU3Ek__BackingField_20(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1 * value)
	{
		___U3CtemporalAntialiasingU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtemporalAntialiasingU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_uberSheet_21() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___uberSheet_21)); }
	inline PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * get_uberSheet_21() const { return ___uberSheet_21; }
	inline PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 ** get_address_of_uberSheet_21() { return &___uberSheet_21; }
	inline void set_uberSheet_21(PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735 * value)
	{
		___uberSheet_21 = value;
		Il2CppCodeGenWriteBarrier((&___uberSheet_21), value);
	}

	inline static int32_t get_offset_of_autoExposureTexture_22() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___autoExposureTexture_22)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_autoExposureTexture_22() const { return ___autoExposureTexture_22; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_autoExposureTexture_22() { return &___autoExposureTexture_22; }
	inline void set_autoExposureTexture_22(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___autoExposureTexture_22 = value;
		Il2CppCodeGenWriteBarrier((&___autoExposureTexture_22), value);
	}

	inline static int32_t get_offset_of_logHistogram_23() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___logHistogram_23)); }
	inline LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79 * get_logHistogram_23() const { return ___logHistogram_23; }
	inline LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79 ** get_address_of_logHistogram_23() { return &___logHistogram_23; }
	inline void set_logHistogram_23(LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79 * value)
	{
		___logHistogram_23 = value;
		Il2CppCodeGenWriteBarrier((&___logHistogram_23), value);
	}

	inline static int32_t get_offset_of_logLut_24() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___logLut_24)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_logLut_24() const { return ___logLut_24; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_logLut_24() { return &___logLut_24; }
	inline void set_logLut_24(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___logLut_24 = value;
		Il2CppCodeGenWriteBarrier((&___logLut_24), value);
	}

	inline static int32_t get_offset_of_autoExposure_25() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___autoExposure_25)); }
	inline AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A * get_autoExposure_25() const { return ___autoExposure_25; }
	inline AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A ** get_address_of_autoExposure_25() { return &___autoExposure_25; }
	inline void set_autoExposure_25(AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A * value)
	{
		___autoExposure_25 = value;
		Il2CppCodeGenWriteBarrier((&___autoExposure_25), value);
	}

	inline static int32_t get_offset_of_bloomBufferNameID_26() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___bloomBufferNameID_26)); }
	inline int32_t get_bloomBufferNameID_26() const { return ___bloomBufferNameID_26; }
	inline int32_t* get_address_of_bloomBufferNameID_26() { return &___bloomBufferNameID_26; }
	inline void set_bloomBufferNameID_26(int32_t value)
	{
		___bloomBufferNameID_26 = value;
	}

	inline static int32_t get_offset_of_physicalCamera_27() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___physicalCamera_27)); }
	inline bool get_physicalCamera_27() const { return ___physicalCamera_27; }
	inline bool* get_address_of_physicalCamera_27() { return &___physicalCamera_27; }
	inline void set_physicalCamera_27(bool value)
	{
		___physicalCamera_27 = value;
	}

	inline static int32_t get_offset_of_m_sourceDescriptor_28() { return static_cast<int32_t>(offsetof(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB, ___m_sourceDescriptor_28)); }
	inline RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E  get_m_sourceDescriptor_28() const { return ___m_sourceDescriptor_28; }
	inline RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E * get_address_of_m_sourceDescriptor_28() { return &___m_sourceDescriptor_28; }
	inline void set_m_sourceDescriptor_28(RenderTextureDescriptor_t74FEC57A54F89E11748E1865F7DCA3565BFAF58E  value)
	{
		___m_sourceDescriptor_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSRENDERCONTEXT_TF3C9D7123D8B250A5E5FE6743B2319890F0168BB_H
#ifndef POSTPROCESSRESOURCES_T70D45255B03138A85C22CE4619CF88EB1F0C2D6B_H
#define POSTPROCESSRESOURCES_T70D45255B03138A85C22CE4619CF88EB1F0C2D6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessResources
struct  PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.Texture2D[] UnityEngine.Rendering.PostProcessing.PostProcessResources::blueNoise64
	Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* ___blueNoise64_4;
	// UnityEngine.Texture2D[] UnityEngine.Rendering.PostProcessing.PostProcessResources::blueNoise256
	Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* ___blueNoise256_5;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources_SMAALuts UnityEngine.Rendering.PostProcessing.PostProcessResources::smaaLuts
	SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090 * ___smaaLuts_6;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources_Shaders UnityEngine.Rendering.PostProcessing.PostProcessResources::shaders
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF * ___shaders_7;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources_ComputeShaders UnityEngine.Rendering.PostProcessing.PostProcessResources::computeShaders
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457 * ___computeShaders_8;

public:
	inline static int32_t get_offset_of_blueNoise64_4() { return static_cast<int32_t>(offsetof(PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B, ___blueNoise64_4)); }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* get_blueNoise64_4() const { return ___blueNoise64_4; }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9** get_address_of_blueNoise64_4() { return &___blueNoise64_4; }
	inline void set_blueNoise64_4(Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* value)
	{
		___blueNoise64_4 = value;
		Il2CppCodeGenWriteBarrier((&___blueNoise64_4), value);
	}

	inline static int32_t get_offset_of_blueNoise256_5() { return static_cast<int32_t>(offsetof(PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B, ___blueNoise256_5)); }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* get_blueNoise256_5() const { return ___blueNoise256_5; }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9** get_address_of_blueNoise256_5() { return &___blueNoise256_5; }
	inline void set_blueNoise256_5(Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* value)
	{
		___blueNoise256_5 = value;
		Il2CppCodeGenWriteBarrier((&___blueNoise256_5), value);
	}

	inline static int32_t get_offset_of_smaaLuts_6() { return static_cast<int32_t>(offsetof(PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B, ___smaaLuts_6)); }
	inline SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090 * get_smaaLuts_6() const { return ___smaaLuts_6; }
	inline SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090 ** get_address_of_smaaLuts_6() { return &___smaaLuts_6; }
	inline void set_smaaLuts_6(SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090 * value)
	{
		___smaaLuts_6 = value;
		Il2CppCodeGenWriteBarrier((&___smaaLuts_6), value);
	}

	inline static int32_t get_offset_of_shaders_7() { return static_cast<int32_t>(offsetof(PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B, ___shaders_7)); }
	inline Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF * get_shaders_7() const { return ___shaders_7; }
	inline Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF ** get_address_of_shaders_7() { return &___shaders_7; }
	inline void set_shaders_7(Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF * value)
	{
		___shaders_7 = value;
		Il2CppCodeGenWriteBarrier((&___shaders_7), value);
	}

	inline static int32_t get_offset_of_computeShaders_8() { return static_cast<int32_t>(offsetof(PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B, ___computeShaders_8)); }
	inline ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457 * get_computeShaders_8() const { return ___computeShaders_8; }
	inline ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457 ** get_address_of_computeShaders_8() { return &___computeShaders_8; }
	inline void set_computeShaders_8(ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457 * value)
	{
		___computeShaders_8 = value;
		Il2CppCodeGenWriteBarrier((&___computeShaders_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSRESOURCES_T70D45255B03138A85C22CE4619CF88EB1F0C2D6B_H
#ifndef SCREENSPACEREFLECTIONPRESETPARAMETER_TD789E7238F9338AC7EEBC50D75C89648C9FF7778_H
#define SCREENSPACEREFLECTIONPRESETPARAMETER_TD789E7238F9338AC7EEBC50D75C89648C9FF7778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPresetParameter
struct  ScreenSpaceReflectionPresetParameter_tD789E7238F9338AC7EEBC50D75C89648C9FF7778  : public ParameterOverride_1_t58C4C7E6C294A51111B04879C5E26C015499F38E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONPRESETPARAMETER_TD789E7238F9338AC7EEBC50D75C89648C9FF7778_H
#ifndef SCREENSPACEREFLECTIONRESOLUTIONPARAMETER_TE0C3A2C876211C74352F5968C45AD1C60C666F50_H
#define SCREENSPACEREFLECTIONRESOLUTIONPARAMETER_TE0C3A2C876211C74352F5968C45AD1C60C666F50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolutionParameter
struct  ScreenSpaceReflectionResolutionParameter_tE0C3A2C876211C74352F5968C45AD1C60C666F50  : public ParameterOverride_1_t67889465519D37D4AD6A29F9DE63AA17E0419BDF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONRESOLUTIONPARAMETER_TE0C3A2C876211C74352F5968C45AD1C60C666F50_H
#ifndef VIGNETTEMODEPARAMETER_T11A319B818611259008B12060E0263B287D90C14_H
#define VIGNETTEMODEPARAMETER_T11A319B818611259008B12060E0263B287D90C14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.VignetteModeParameter
struct  VignetteModeParameter_t11A319B818611259008B12060E0263B287D90C14  : public ParameterOverride_1_t6BB8846771999798B8AF83A70C7DBBAE1A4670A0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTEMODEPARAMETER_T11A319B818611259008B12060E0263B287D90C14_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef LENSDISTORTION_TD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F_H
#define LENSDISTORTION_TD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.LensDistortion
struct  LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::intensity
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___intensity_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::intensityX
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___intensityX_8;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::intensityY
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___intensityY_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::centerX
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___centerX_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::centerY
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___centerY_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::scale
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___scale_12;

public:
	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F, ___intensity_7)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_intensity_7() const { return ___intensity_7; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___intensity_7 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_7), value);
	}

	inline static int32_t get_offset_of_intensityX_8() { return static_cast<int32_t>(offsetof(LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F, ___intensityX_8)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_intensityX_8() const { return ___intensityX_8; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_intensityX_8() { return &___intensityX_8; }
	inline void set_intensityX_8(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___intensityX_8 = value;
		Il2CppCodeGenWriteBarrier((&___intensityX_8), value);
	}

	inline static int32_t get_offset_of_intensityY_9() { return static_cast<int32_t>(offsetof(LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F, ___intensityY_9)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_intensityY_9() const { return ___intensityY_9; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_intensityY_9() { return &___intensityY_9; }
	inline void set_intensityY_9(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___intensityY_9 = value;
		Il2CppCodeGenWriteBarrier((&___intensityY_9), value);
	}

	inline static int32_t get_offset_of_centerX_10() { return static_cast<int32_t>(offsetof(LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F, ___centerX_10)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_centerX_10() const { return ___centerX_10; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_centerX_10() { return &___centerX_10; }
	inline void set_centerX_10(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___centerX_10 = value;
		Il2CppCodeGenWriteBarrier((&___centerX_10), value);
	}

	inline static int32_t get_offset_of_centerY_11() { return static_cast<int32_t>(offsetof(LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F, ___centerY_11)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_centerY_11() const { return ___centerY_11; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_centerY_11() { return &___centerY_11; }
	inline void set_centerY_11(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___centerY_11 = value;
		Il2CppCodeGenWriteBarrier((&___centerY_11), value);
	}

	inline static int32_t get_offset_of_scale_12() { return static_cast<int32_t>(offsetof(LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F, ___scale_12)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_scale_12() const { return ___scale_12; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_scale_12() { return &___scale_12; }
	inline void set_scale_12(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___scale_12 = value;
		Il2CppCodeGenWriteBarrier((&___scale_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSDISTORTION_TD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F_H
#ifndef MOTIONBLUR_T222E0B381BC4028B97DADE6A484812479E54E165_H
#define MOTIONBLUR_T222E0B381BC4028B97DADE6A484812479E54E165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.MotionBlur
struct  MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.MotionBlur::shutterAngle
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___shutterAngle_7;
	// UnityEngine.Rendering.PostProcessing.IntParameter UnityEngine.Rendering.PostProcessing.MotionBlur::sampleCount
	IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC * ___sampleCount_8;

public:
	inline static int32_t get_offset_of_shutterAngle_7() { return static_cast<int32_t>(offsetof(MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165, ___shutterAngle_7)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_shutterAngle_7() const { return ___shutterAngle_7; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_shutterAngle_7() { return &___shutterAngle_7; }
	inline void set_shutterAngle_7(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___shutterAngle_7 = value;
		Il2CppCodeGenWriteBarrier((&___shutterAngle_7), value);
	}

	inline static int32_t get_offset_of_sampleCount_8() { return static_cast<int32_t>(offsetof(MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165, ___sampleCount_8)); }
	inline IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC * get_sampleCount_8() const { return ___sampleCount_8; }
	inline IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC ** get_address_of_sampleCount_8() { return &___sampleCount_8; }
	inline void set_sampleCount_8(IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC * value)
	{
		___sampleCount_8 = value;
		Il2CppCodeGenWriteBarrier((&___sampleCount_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLUR_T222E0B381BC4028B97DADE6A484812479E54E165_H
#ifndef SCREENSPACEREFLECTIONS_TD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325_H
#define SCREENSPACEREFLECTIONS_TD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections
struct  ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionPresetParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::preset
	ScreenSpaceReflectionPresetParameter_tD789E7238F9338AC7EEBC50D75C89648C9FF7778 * ___preset_7;
	// UnityEngine.Rendering.PostProcessing.IntParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::maximumIterationCount
	IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC * ___maximumIterationCount_8;
	// UnityEngine.Rendering.PostProcessing.ScreenSpaceReflectionResolutionParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::resolution
	ScreenSpaceReflectionResolutionParameter_tE0C3A2C876211C74352F5968C45AD1C60C666F50 * ___resolution_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::thickness
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___thickness_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::maximumMarchDistance
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___maximumMarchDistance_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::distanceFade
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___distanceFade_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ScreenSpaceReflections::vignette
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___vignette_13;

public:
	inline static int32_t get_offset_of_preset_7() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325, ___preset_7)); }
	inline ScreenSpaceReflectionPresetParameter_tD789E7238F9338AC7EEBC50D75C89648C9FF7778 * get_preset_7() const { return ___preset_7; }
	inline ScreenSpaceReflectionPresetParameter_tD789E7238F9338AC7EEBC50D75C89648C9FF7778 ** get_address_of_preset_7() { return &___preset_7; }
	inline void set_preset_7(ScreenSpaceReflectionPresetParameter_tD789E7238F9338AC7EEBC50D75C89648C9FF7778 * value)
	{
		___preset_7 = value;
		Il2CppCodeGenWriteBarrier((&___preset_7), value);
	}

	inline static int32_t get_offset_of_maximumIterationCount_8() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325, ___maximumIterationCount_8)); }
	inline IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC * get_maximumIterationCount_8() const { return ___maximumIterationCount_8; }
	inline IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC ** get_address_of_maximumIterationCount_8() { return &___maximumIterationCount_8; }
	inline void set_maximumIterationCount_8(IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC * value)
	{
		___maximumIterationCount_8 = value;
		Il2CppCodeGenWriteBarrier((&___maximumIterationCount_8), value);
	}

	inline static int32_t get_offset_of_resolution_9() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325, ___resolution_9)); }
	inline ScreenSpaceReflectionResolutionParameter_tE0C3A2C876211C74352F5968C45AD1C60C666F50 * get_resolution_9() const { return ___resolution_9; }
	inline ScreenSpaceReflectionResolutionParameter_tE0C3A2C876211C74352F5968C45AD1C60C666F50 ** get_address_of_resolution_9() { return &___resolution_9; }
	inline void set_resolution_9(ScreenSpaceReflectionResolutionParameter_tE0C3A2C876211C74352F5968C45AD1C60C666F50 * value)
	{
		___resolution_9 = value;
		Il2CppCodeGenWriteBarrier((&___resolution_9), value);
	}

	inline static int32_t get_offset_of_thickness_10() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325, ___thickness_10)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_thickness_10() const { return ___thickness_10; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_thickness_10() { return &___thickness_10; }
	inline void set_thickness_10(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___thickness_10 = value;
		Il2CppCodeGenWriteBarrier((&___thickness_10), value);
	}

	inline static int32_t get_offset_of_maximumMarchDistance_11() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325, ___maximumMarchDistance_11)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_maximumMarchDistance_11() const { return ___maximumMarchDistance_11; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_maximumMarchDistance_11() { return &___maximumMarchDistance_11; }
	inline void set_maximumMarchDistance_11(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___maximumMarchDistance_11 = value;
		Il2CppCodeGenWriteBarrier((&___maximumMarchDistance_11), value);
	}

	inline static int32_t get_offset_of_distanceFade_12() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325, ___distanceFade_12)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_distanceFade_12() const { return ___distanceFade_12; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_distanceFade_12() { return &___distanceFade_12; }
	inline void set_distanceFade_12(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___distanceFade_12 = value;
		Il2CppCodeGenWriteBarrier((&___distanceFade_12), value);
	}

	inline static int32_t get_offset_of_vignette_13() { return static_cast<int32_t>(offsetof(ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325, ___vignette_13)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_vignette_13() const { return ___vignette_13; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_vignette_13() { return &___vignette_13; }
	inline void set_vignette_13(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___vignette_13 = value;
		Il2CppCodeGenWriteBarrier((&___vignette_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONS_TD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325_H
#ifndef VIGNETTE_TCF8492DA1D89F968A7CDB3857FA9252B61C53D31_H
#define VIGNETTE_TCF8492DA1D89F968A7CDB3857FA9252B61C53D31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Vignette
struct  Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.VignetteModeParameter UnityEngine.Rendering.PostProcessing.Vignette::mode
	VignetteModeParameter_t11A319B818611259008B12060E0263B287D90C14 * ___mode_7;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.Vignette::color
	ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * ___color_8;
	// UnityEngine.Rendering.PostProcessing.Vector2Parameter UnityEngine.Rendering.PostProcessing.Vignette::center
	Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2 * ___center_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::intensity
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___intensity_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::smoothness
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___smoothness_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::roundness
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___roundness_12;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.Vignette::rounded
	BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * ___rounded_13;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.Vignette::mask
	TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * ___mask_14;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::opacity
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___opacity_15;

public:
	inline static int32_t get_offset_of_mode_7() { return static_cast<int32_t>(offsetof(Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31, ___mode_7)); }
	inline VignetteModeParameter_t11A319B818611259008B12060E0263B287D90C14 * get_mode_7() const { return ___mode_7; }
	inline VignetteModeParameter_t11A319B818611259008B12060E0263B287D90C14 ** get_address_of_mode_7() { return &___mode_7; }
	inline void set_mode_7(VignetteModeParameter_t11A319B818611259008B12060E0263B287D90C14 * value)
	{
		___mode_7 = value;
		Il2CppCodeGenWriteBarrier((&___mode_7), value);
	}

	inline static int32_t get_offset_of_color_8() { return static_cast<int32_t>(offsetof(Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31, ___color_8)); }
	inline ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * get_color_8() const { return ___color_8; }
	inline ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B ** get_address_of_color_8() { return &___color_8; }
	inline void set_color_8(ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * value)
	{
		___color_8 = value;
		Il2CppCodeGenWriteBarrier((&___color_8), value);
	}

	inline static int32_t get_offset_of_center_9() { return static_cast<int32_t>(offsetof(Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31, ___center_9)); }
	inline Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2 * get_center_9() const { return ___center_9; }
	inline Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2 ** get_address_of_center_9() { return &___center_9; }
	inline void set_center_9(Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2 * value)
	{
		___center_9 = value;
		Il2CppCodeGenWriteBarrier((&___center_9), value);
	}

	inline static int32_t get_offset_of_intensity_10() { return static_cast<int32_t>(offsetof(Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31, ___intensity_10)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_intensity_10() const { return ___intensity_10; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_intensity_10() { return &___intensity_10; }
	inline void set_intensity_10(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___intensity_10 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_10), value);
	}

	inline static int32_t get_offset_of_smoothness_11() { return static_cast<int32_t>(offsetof(Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31, ___smoothness_11)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_smoothness_11() const { return ___smoothness_11; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_smoothness_11() { return &___smoothness_11; }
	inline void set_smoothness_11(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___smoothness_11 = value;
		Il2CppCodeGenWriteBarrier((&___smoothness_11), value);
	}

	inline static int32_t get_offset_of_roundness_12() { return static_cast<int32_t>(offsetof(Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31, ___roundness_12)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_roundness_12() const { return ___roundness_12; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_roundness_12() { return &___roundness_12; }
	inline void set_roundness_12(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___roundness_12 = value;
		Il2CppCodeGenWriteBarrier((&___roundness_12), value);
	}

	inline static int32_t get_offset_of_rounded_13() { return static_cast<int32_t>(offsetof(Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31, ___rounded_13)); }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * get_rounded_13() const { return ___rounded_13; }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE ** get_address_of_rounded_13() { return &___rounded_13; }
	inline void set_rounded_13(BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * value)
	{
		___rounded_13 = value;
		Il2CppCodeGenWriteBarrier((&___rounded_13), value);
	}

	inline static int32_t get_offset_of_mask_14() { return static_cast<int32_t>(offsetof(Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31, ___mask_14)); }
	inline TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * get_mask_14() const { return ___mask_14; }
	inline TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 ** get_address_of_mask_14() { return &___mask_14; }
	inline void set_mask_14(TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * value)
	{
		___mask_14 = value;
		Il2CppCodeGenWriteBarrier((&___mask_14), value);
	}

	inline static int32_t get_offset_of_opacity_15() { return static_cast<int32_t>(offsetof(Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31, ___opacity_15)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_opacity_15() const { return ___opacity_15; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_opacity_15() { return &___opacity_15; }
	inline void set_opacity_15(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___opacity_15 = value;
		Il2CppCodeGenWriteBarrier((&___opacity_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTE_TCF8492DA1D89F968A7CDB3857FA9252B61C53D31_H
#ifndef BOUNDINGBOXRENDERER_T687A36FDD509AC21A29D0CF70359619EFD251AFB_H
#define BOUNDINGBOXRENDERER_T687A36FDD509AC21A29D0CF70359619EFD251AFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoundingBoxRenderer
struct  BoundingBoxRenderer_t687A36FDD509AC21A29D0CF70359619EFD251AFB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Material BoundingBoxRenderer::mLineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mLineMaterial_4;

public:
	inline static int32_t get_offset_of_mLineMaterial_4() { return static_cast<int32_t>(offsetof(BoundingBoxRenderer_t687A36FDD509AC21A29D0CF70359619EFD251AFB, ___mLineMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_mLineMaterial_4() const { return ___mLineMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_mLineMaterial_4() { return &___mLineMaterial_4; }
	inline void set_mLineMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___mLineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLineMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXRENDERER_T687A36FDD509AC21A29D0CF70359619EFD251AFB_H
#ifndef DEFAULTMODELRECOEVENTHANDLER_TD8D52485390B4887E690E2CE623D20236D7E82CA_H
#define DEFAULTMODELRECOEVENTHANDLER_TD8D52485390B4887E690E2CE623D20236D7E82CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultModelRecoEventHandler
struct  DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text DefaultModelRecoEventHandler::ModelRecoErrorText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___ModelRecoErrorText_4;
	// System.Boolean DefaultModelRecoEventHandler::StopSearchWhenModelFound
	bool ___StopSearchWhenModelFound_5;
	// System.Boolean DefaultModelRecoEventHandler::StopSearchWhileTracking
	bool ___StopSearchWhileTracking_6;

public:
	inline static int32_t get_offset_of_ModelRecoErrorText_4() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA, ___ModelRecoErrorText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_ModelRecoErrorText_4() const { return ___ModelRecoErrorText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_ModelRecoErrorText_4() { return &___ModelRecoErrorText_4; }
	inline void set_ModelRecoErrorText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___ModelRecoErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___ModelRecoErrorText_4), value);
	}

	inline static int32_t get_offset_of_StopSearchWhenModelFound_5() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA, ___StopSearchWhenModelFound_5)); }
	inline bool get_StopSearchWhenModelFound_5() const { return ___StopSearchWhenModelFound_5; }
	inline bool* get_address_of_StopSearchWhenModelFound_5() { return &___StopSearchWhenModelFound_5; }
	inline void set_StopSearchWhenModelFound_5(bool value)
	{
		___StopSearchWhenModelFound_5 = value;
	}

	inline static int32_t get_offset_of_StopSearchWhileTracking_6() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA, ___StopSearchWhileTracking_6)); }
	inline bool get_StopSearchWhileTracking_6() const { return ___StopSearchWhileTracking_6; }
	inline bool* get_address_of_StopSearchWhileTracking_6() { return &___StopSearchWhileTracking_6; }
	inline void set_StopSearchWhileTracking_6(bool value)
	{
		___StopSearchWhileTracking_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTMODELRECOEVENTHANDLER_TD8D52485390B4887E690E2CE623D20236D7E82CA_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T6997E0A19AC0FABC165FB7264F57DF2EDF4E8022_H
#define DEFAULTTRACKABLEEVENTHANDLER_T6997E0A19AC0FABC165FB7264F57DF2EDF4E8022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour_Status DefaultTrackableEventHandler::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour_Status DefaultTrackableEventHandler::m_NewStatus
	int32_t ___m_NewStatus_6;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T6997E0A19AC0FABC165FB7264F57DF2EDF4E8022_H
#ifndef POSTPROCESSLAYER_T23DFD96D31A91809B519CBD47DA4F63BC73D85B2_H
#define POSTPROCESSLAYER_T23DFD96D31A91809B519CBD47DA4F63BC73D85B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessLayer
struct  PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform UnityEngine.Rendering.PostProcessing.PostProcessLayer::volumeTrigger
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___volumeTrigger_4;
	// UnityEngine.LayerMask UnityEngine.Rendering.PostProcessing.PostProcessLayer::volumeLayer
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___volumeLayer_5;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::stopNaNPropagation
	bool ___stopNaNPropagation_6;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::finalBlitToCameraTarget
	bool ___finalBlitToCameraTarget_7;
	// UnityEngine.Rendering.PostProcessing.PostProcessLayer_Antialiasing UnityEngine.Rendering.PostProcessing.PostProcessLayer::antialiasingMode
	int32_t ___antialiasingMode_8;
	// UnityEngine.Rendering.PostProcessing.TemporalAntialiasing UnityEngine.Rendering.PostProcessing.PostProcessLayer::temporalAntialiasing
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1 * ___temporalAntialiasing_9;
	// UnityEngine.Rendering.PostProcessing.SubpixelMorphologicalAntialiasing UnityEngine.Rendering.PostProcessing.PostProcessLayer::subpixelMorphologicalAntialiasing
	SubpixelMorphologicalAntialiasing_tDE9497AEBB401059AB2F21890704B15C374B79DD * ___subpixelMorphologicalAntialiasing_10;
	// UnityEngine.Rendering.PostProcessing.FastApproximateAntialiasing UnityEngine.Rendering.PostProcessing.PostProcessLayer::fastApproximateAntialiasing
	FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D * ___fastApproximateAntialiasing_11;
	// UnityEngine.Rendering.PostProcessing.Fog UnityEngine.Rendering.PostProcessing.PostProcessLayer::fog
	Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77 * ___fog_12;
	// UnityEngine.Rendering.PostProcessing.Dithering UnityEngine.Rendering.PostProcessing.PostProcessLayer::dithering
	Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0 * ___dithering_13;
	// UnityEngine.Rendering.PostProcessing.PostProcessDebugLayer UnityEngine.Rendering.PostProcessing.PostProcessLayer::debugLayer
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39 * ___debugLayer_14;
	// UnityEngine.Rendering.PostProcessing.PostProcessResources UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_Resources
	PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * ___m_Resources_15;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_ShowToolkit
	bool ___m_ShowToolkit_16;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_ShowCustomSorter
	bool ___m_ShowCustomSorter_17;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::breakBeforeColorGrading
	bool ___breakBeforeColorGrading_18;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer_SerializedBundleRef> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_BeforeTransparentBundles
	List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E * ___m_BeforeTransparentBundles_19;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer_SerializedBundleRef> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_BeforeStackBundles
	List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E * ___m_BeforeStackBundles_20;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer_SerializedBundleRef> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_AfterStackBundles
	List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E * ___m_AfterStackBundles_21;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Rendering.PostProcessing.PostProcessEvent,System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessLayer_SerializedBundleRef>> UnityEngine.Rendering.PostProcessing.PostProcessLayer::<sortedBundles>k__BackingField
	Dictionary_2_t9494DB774A1ACFCD524B3447BB1A97E3D97CFE76 * ___U3CsortedBundlesU3Ek__BackingField_22;
	// UnityEngine.DepthTextureMode UnityEngine.Rendering.PostProcessing.PostProcessLayer::<cameraDepthFlags>k__BackingField
	int32_t ___U3CcameraDepthFlagsU3Ek__BackingField_23;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::<haveBundlesBeenInited>k__BackingField
	bool ___U3ChaveBundlesBeenInitedU3Ek__BackingField_24;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Rendering.PostProcessing.PostProcessBundle> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_Bundles
	Dictionary_2_t4EC298F4DA29313372D00BECC420F066420CDDC6 * ___m_Bundles_25;
	// UnityEngine.Rendering.PostProcessing.PropertySheetFactory UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_PropertySheetFactory
	PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 * ___m_PropertySheetFactory_26;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_LegacyCmdBufferBeforeReflections
	CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * ___m_LegacyCmdBufferBeforeReflections_27;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_LegacyCmdBufferBeforeLighting
	CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * ___m_LegacyCmdBufferBeforeLighting_28;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_LegacyCmdBufferOpaque
	CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * ___m_LegacyCmdBufferOpaque_29;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_LegacyCmdBuffer
	CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * ___m_LegacyCmdBuffer_30;
	// UnityEngine.Camera UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_Camera_31;
	// UnityEngine.Rendering.PostProcessing.PostProcessRenderContext UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_CurrentContext
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB * ___m_CurrentContext_32;
	// UnityEngine.Rendering.PostProcessing.LogHistogram UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_LogHistogram
	LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79 * ___m_LogHistogram_33;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_SettingsUpdateNeeded
	bool ___m_SettingsUpdateNeeded_34;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_IsRenderingInSceneView
	bool ___m_IsRenderingInSceneView_35;
	// UnityEngine.Rendering.PostProcessing.TargetPool UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_TargetPool
	TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949 * ___m_TargetPool_36;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_NaNKilled
	bool ___m_NaNKilled_37;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_ActiveEffects
	List_1_t939524230874DF71A2B82A3F03291BA2261A7180 * ___m_ActiveEffects_38;
	// System.Collections.Generic.List`1<UnityEngine.Rendering.RenderTargetIdentifier> UnityEngine.Rendering.PostProcessing.PostProcessLayer::m_Targets
	List_1_t3D014466D866220BEE24A92AE5BB33E306B8E2B7 * ___m_Targets_39;

public:
	inline static int32_t get_offset_of_volumeTrigger_4() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___volumeTrigger_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_volumeTrigger_4() const { return ___volumeTrigger_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_volumeTrigger_4() { return &___volumeTrigger_4; }
	inline void set_volumeTrigger_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___volumeTrigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___volumeTrigger_4), value);
	}

	inline static int32_t get_offset_of_volumeLayer_5() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___volumeLayer_5)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_volumeLayer_5() const { return ___volumeLayer_5; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_volumeLayer_5() { return &___volumeLayer_5; }
	inline void set_volumeLayer_5(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___volumeLayer_5 = value;
	}

	inline static int32_t get_offset_of_stopNaNPropagation_6() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___stopNaNPropagation_6)); }
	inline bool get_stopNaNPropagation_6() const { return ___stopNaNPropagation_6; }
	inline bool* get_address_of_stopNaNPropagation_6() { return &___stopNaNPropagation_6; }
	inline void set_stopNaNPropagation_6(bool value)
	{
		___stopNaNPropagation_6 = value;
	}

	inline static int32_t get_offset_of_finalBlitToCameraTarget_7() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___finalBlitToCameraTarget_7)); }
	inline bool get_finalBlitToCameraTarget_7() const { return ___finalBlitToCameraTarget_7; }
	inline bool* get_address_of_finalBlitToCameraTarget_7() { return &___finalBlitToCameraTarget_7; }
	inline void set_finalBlitToCameraTarget_7(bool value)
	{
		___finalBlitToCameraTarget_7 = value;
	}

	inline static int32_t get_offset_of_antialiasingMode_8() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___antialiasingMode_8)); }
	inline int32_t get_antialiasingMode_8() const { return ___antialiasingMode_8; }
	inline int32_t* get_address_of_antialiasingMode_8() { return &___antialiasingMode_8; }
	inline void set_antialiasingMode_8(int32_t value)
	{
		___antialiasingMode_8 = value;
	}

	inline static int32_t get_offset_of_temporalAntialiasing_9() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___temporalAntialiasing_9)); }
	inline TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1 * get_temporalAntialiasing_9() const { return ___temporalAntialiasing_9; }
	inline TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1 ** get_address_of_temporalAntialiasing_9() { return &___temporalAntialiasing_9; }
	inline void set_temporalAntialiasing_9(TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1 * value)
	{
		___temporalAntialiasing_9 = value;
		Il2CppCodeGenWriteBarrier((&___temporalAntialiasing_9), value);
	}

	inline static int32_t get_offset_of_subpixelMorphologicalAntialiasing_10() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___subpixelMorphologicalAntialiasing_10)); }
	inline SubpixelMorphologicalAntialiasing_tDE9497AEBB401059AB2F21890704B15C374B79DD * get_subpixelMorphologicalAntialiasing_10() const { return ___subpixelMorphologicalAntialiasing_10; }
	inline SubpixelMorphologicalAntialiasing_tDE9497AEBB401059AB2F21890704B15C374B79DD ** get_address_of_subpixelMorphologicalAntialiasing_10() { return &___subpixelMorphologicalAntialiasing_10; }
	inline void set_subpixelMorphologicalAntialiasing_10(SubpixelMorphologicalAntialiasing_tDE9497AEBB401059AB2F21890704B15C374B79DD * value)
	{
		___subpixelMorphologicalAntialiasing_10 = value;
		Il2CppCodeGenWriteBarrier((&___subpixelMorphologicalAntialiasing_10), value);
	}

	inline static int32_t get_offset_of_fastApproximateAntialiasing_11() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___fastApproximateAntialiasing_11)); }
	inline FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D * get_fastApproximateAntialiasing_11() const { return ___fastApproximateAntialiasing_11; }
	inline FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D ** get_address_of_fastApproximateAntialiasing_11() { return &___fastApproximateAntialiasing_11; }
	inline void set_fastApproximateAntialiasing_11(FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D * value)
	{
		___fastApproximateAntialiasing_11 = value;
		Il2CppCodeGenWriteBarrier((&___fastApproximateAntialiasing_11), value);
	}

	inline static int32_t get_offset_of_fog_12() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___fog_12)); }
	inline Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77 * get_fog_12() const { return ___fog_12; }
	inline Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77 ** get_address_of_fog_12() { return &___fog_12; }
	inline void set_fog_12(Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77 * value)
	{
		___fog_12 = value;
		Il2CppCodeGenWriteBarrier((&___fog_12), value);
	}

	inline static int32_t get_offset_of_dithering_13() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___dithering_13)); }
	inline Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0 * get_dithering_13() const { return ___dithering_13; }
	inline Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0 ** get_address_of_dithering_13() { return &___dithering_13; }
	inline void set_dithering_13(Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0 * value)
	{
		___dithering_13 = value;
		Il2CppCodeGenWriteBarrier((&___dithering_13), value);
	}

	inline static int32_t get_offset_of_debugLayer_14() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___debugLayer_14)); }
	inline PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39 * get_debugLayer_14() const { return ___debugLayer_14; }
	inline PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39 ** get_address_of_debugLayer_14() { return &___debugLayer_14; }
	inline void set_debugLayer_14(PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39 * value)
	{
		___debugLayer_14 = value;
		Il2CppCodeGenWriteBarrier((&___debugLayer_14), value);
	}

	inline static int32_t get_offset_of_m_Resources_15() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_Resources_15)); }
	inline PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * get_m_Resources_15() const { return ___m_Resources_15; }
	inline PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B ** get_address_of_m_Resources_15() { return &___m_Resources_15; }
	inline void set_m_Resources_15(PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B * value)
	{
		___m_Resources_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Resources_15), value);
	}

	inline static int32_t get_offset_of_m_ShowToolkit_16() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_ShowToolkit_16)); }
	inline bool get_m_ShowToolkit_16() const { return ___m_ShowToolkit_16; }
	inline bool* get_address_of_m_ShowToolkit_16() { return &___m_ShowToolkit_16; }
	inline void set_m_ShowToolkit_16(bool value)
	{
		___m_ShowToolkit_16 = value;
	}

	inline static int32_t get_offset_of_m_ShowCustomSorter_17() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_ShowCustomSorter_17)); }
	inline bool get_m_ShowCustomSorter_17() const { return ___m_ShowCustomSorter_17; }
	inline bool* get_address_of_m_ShowCustomSorter_17() { return &___m_ShowCustomSorter_17; }
	inline void set_m_ShowCustomSorter_17(bool value)
	{
		___m_ShowCustomSorter_17 = value;
	}

	inline static int32_t get_offset_of_breakBeforeColorGrading_18() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___breakBeforeColorGrading_18)); }
	inline bool get_breakBeforeColorGrading_18() const { return ___breakBeforeColorGrading_18; }
	inline bool* get_address_of_breakBeforeColorGrading_18() { return &___breakBeforeColorGrading_18; }
	inline void set_breakBeforeColorGrading_18(bool value)
	{
		___breakBeforeColorGrading_18 = value;
	}

	inline static int32_t get_offset_of_m_BeforeTransparentBundles_19() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_BeforeTransparentBundles_19)); }
	inline List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E * get_m_BeforeTransparentBundles_19() const { return ___m_BeforeTransparentBundles_19; }
	inline List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E ** get_address_of_m_BeforeTransparentBundles_19() { return &___m_BeforeTransparentBundles_19; }
	inline void set_m_BeforeTransparentBundles_19(List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E * value)
	{
		___m_BeforeTransparentBundles_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_BeforeTransparentBundles_19), value);
	}

	inline static int32_t get_offset_of_m_BeforeStackBundles_20() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_BeforeStackBundles_20)); }
	inline List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E * get_m_BeforeStackBundles_20() const { return ___m_BeforeStackBundles_20; }
	inline List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E ** get_address_of_m_BeforeStackBundles_20() { return &___m_BeforeStackBundles_20; }
	inline void set_m_BeforeStackBundles_20(List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E * value)
	{
		___m_BeforeStackBundles_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_BeforeStackBundles_20), value);
	}

	inline static int32_t get_offset_of_m_AfterStackBundles_21() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_AfterStackBundles_21)); }
	inline List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E * get_m_AfterStackBundles_21() const { return ___m_AfterStackBundles_21; }
	inline List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E ** get_address_of_m_AfterStackBundles_21() { return &___m_AfterStackBundles_21; }
	inline void set_m_AfterStackBundles_21(List_1_t341E93DBF39ACB4B238DD631C5B95E518F44360E * value)
	{
		___m_AfterStackBundles_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_AfterStackBundles_21), value);
	}

	inline static int32_t get_offset_of_U3CsortedBundlesU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___U3CsortedBundlesU3Ek__BackingField_22)); }
	inline Dictionary_2_t9494DB774A1ACFCD524B3447BB1A97E3D97CFE76 * get_U3CsortedBundlesU3Ek__BackingField_22() const { return ___U3CsortedBundlesU3Ek__BackingField_22; }
	inline Dictionary_2_t9494DB774A1ACFCD524B3447BB1A97E3D97CFE76 ** get_address_of_U3CsortedBundlesU3Ek__BackingField_22() { return &___U3CsortedBundlesU3Ek__BackingField_22; }
	inline void set_U3CsortedBundlesU3Ek__BackingField_22(Dictionary_2_t9494DB774A1ACFCD524B3447BB1A97E3D97CFE76 * value)
	{
		___U3CsortedBundlesU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsortedBundlesU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CcameraDepthFlagsU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___U3CcameraDepthFlagsU3Ek__BackingField_23)); }
	inline int32_t get_U3CcameraDepthFlagsU3Ek__BackingField_23() const { return ___U3CcameraDepthFlagsU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CcameraDepthFlagsU3Ek__BackingField_23() { return &___U3CcameraDepthFlagsU3Ek__BackingField_23; }
	inline void set_U3CcameraDepthFlagsU3Ek__BackingField_23(int32_t value)
	{
		___U3CcameraDepthFlagsU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3ChaveBundlesBeenInitedU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___U3ChaveBundlesBeenInitedU3Ek__BackingField_24)); }
	inline bool get_U3ChaveBundlesBeenInitedU3Ek__BackingField_24() const { return ___U3ChaveBundlesBeenInitedU3Ek__BackingField_24; }
	inline bool* get_address_of_U3ChaveBundlesBeenInitedU3Ek__BackingField_24() { return &___U3ChaveBundlesBeenInitedU3Ek__BackingField_24; }
	inline void set_U3ChaveBundlesBeenInitedU3Ek__BackingField_24(bool value)
	{
		___U3ChaveBundlesBeenInitedU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_m_Bundles_25() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_Bundles_25)); }
	inline Dictionary_2_t4EC298F4DA29313372D00BECC420F066420CDDC6 * get_m_Bundles_25() const { return ___m_Bundles_25; }
	inline Dictionary_2_t4EC298F4DA29313372D00BECC420F066420CDDC6 ** get_address_of_m_Bundles_25() { return &___m_Bundles_25; }
	inline void set_m_Bundles_25(Dictionary_2_t4EC298F4DA29313372D00BECC420F066420CDDC6 * value)
	{
		___m_Bundles_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Bundles_25), value);
	}

	inline static int32_t get_offset_of_m_PropertySheetFactory_26() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_PropertySheetFactory_26)); }
	inline PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 * get_m_PropertySheetFactory_26() const { return ___m_PropertySheetFactory_26; }
	inline PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 ** get_address_of_m_PropertySheetFactory_26() { return &___m_PropertySheetFactory_26; }
	inline void set_m_PropertySheetFactory_26(PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4 * value)
	{
		___m_PropertySheetFactory_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_PropertySheetFactory_26), value);
	}

	inline static int32_t get_offset_of_m_LegacyCmdBufferBeforeReflections_27() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_LegacyCmdBufferBeforeReflections_27)); }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * get_m_LegacyCmdBufferBeforeReflections_27() const { return ___m_LegacyCmdBufferBeforeReflections_27; }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD ** get_address_of_m_LegacyCmdBufferBeforeReflections_27() { return &___m_LegacyCmdBufferBeforeReflections_27; }
	inline void set_m_LegacyCmdBufferBeforeReflections_27(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * value)
	{
		___m_LegacyCmdBufferBeforeReflections_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyCmdBufferBeforeReflections_27), value);
	}

	inline static int32_t get_offset_of_m_LegacyCmdBufferBeforeLighting_28() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_LegacyCmdBufferBeforeLighting_28)); }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * get_m_LegacyCmdBufferBeforeLighting_28() const { return ___m_LegacyCmdBufferBeforeLighting_28; }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD ** get_address_of_m_LegacyCmdBufferBeforeLighting_28() { return &___m_LegacyCmdBufferBeforeLighting_28; }
	inline void set_m_LegacyCmdBufferBeforeLighting_28(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * value)
	{
		___m_LegacyCmdBufferBeforeLighting_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyCmdBufferBeforeLighting_28), value);
	}

	inline static int32_t get_offset_of_m_LegacyCmdBufferOpaque_29() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_LegacyCmdBufferOpaque_29)); }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * get_m_LegacyCmdBufferOpaque_29() const { return ___m_LegacyCmdBufferOpaque_29; }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD ** get_address_of_m_LegacyCmdBufferOpaque_29() { return &___m_LegacyCmdBufferOpaque_29; }
	inline void set_m_LegacyCmdBufferOpaque_29(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * value)
	{
		___m_LegacyCmdBufferOpaque_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyCmdBufferOpaque_29), value);
	}

	inline static int32_t get_offset_of_m_LegacyCmdBuffer_30() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_LegacyCmdBuffer_30)); }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * get_m_LegacyCmdBuffer_30() const { return ___m_LegacyCmdBuffer_30; }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD ** get_address_of_m_LegacyCmdBuffer_30() { return &___m_LegacyCmdBuffer_30; }
	inline void set_m_LegacyCmdBuffer_30(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * value)
	{
		___m_LegacyCmdBuffer_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyCmdBuffer_30), value);
	}

	inline static int32_t get_offset_of_m_Camera_31() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_Camera_31)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_Camera_31() const { return ___m_Camera_31; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_Camera_31() { return &___m_Camera_31; }
	inline void set_m_Camera_31(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_Camera_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_31), value);
	}

	inline static int32_t get_offset_of_m_CurrentContext_32() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_CurrentContext_32)); }
	inline PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB * get_m_CurrentContext_32() const { return ___m_CurrentContext_32; }
	inline PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB ** get_address_of_m_CurrentContext_32() { return &___m_CurrentContext_32; }
	inline void set_m_CurrentContext_32(PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB * value)
	{
		___m_CurrentContext_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentContext_32), value);
	}

	inline static int32_t get_offset_of_m_LogHistogram_33() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_LogHistogram_33)); }
	inline LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79 * get_m_LogHistogram_33() const { return ___m_LogHistogram_33; }
	inline LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79 ** get_address_of_m_LogHistogram_33() { return &___m_LogHistogram_33; }
	inline void set_m_LogHistogram_33(LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79 * value)
	{
		___m_LogHistogram_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogHistogram_33), value);
	}

	inline static int32_t get_offset_of_m_SettingsUpdateNeeded_34() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_SettingsUpdateNeeded_34)); }
	inline bool get_m_SettingsUpdateNeeded_34() const { return ___m_SettingsUpdateNeeded_34; }
	inline bool* get_address_of_m_SettingsUpdateNeeded_34() { return &___m_SettingsUpdateNeeded_34; }
	inline void set_m_SettingsUpdateNeeded_34(bool value)
	{
		___m_SettingsUpdateNeeded_34 = value;
	}

	inline static int32_t get_offset_of_m_IsRenderingInSceneView_35() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_IsRenderingInSceneView_35)); }
	inline bool get_m_IsRenderingInSceneView_35() const { return ___m_IsRenderingInSceneView_35; }
	inline bool* get_address_of_m_IsRenderingInSceneView_35() { return &___m_IsRenderingInSceneView_35; }
	inline void set_m_IsRenderingInSceneView_35(bool value)
	{
		___m_IsRenderingInSceneView_35 = value;
	}

	inline static int32_t get_offset_of_m_TargetPool_36() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_TargetPool_36)); }
	inline TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949 * get_m_TargetPool_36() const { return ___m_TargetPool_36; }
	inline TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949 ** get_address_of_m_TargetPool_36() { return &___m_TargetPool_36; }
	inline void set_m_TargetPool_36(TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949 * value)
	{
		___m_TargetPool_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetPool_36), value);
	}

	inline static int32_t get_offset_of_m_NaNKilled_37() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_NaNKilled_37)); }
	inline bool get_m_NaNKilled_37() const { return ___m_NaNKilled_37; }
	inline bool* get_address_of_m_NaNKilled_37() { return &___m_NaNKilled_37; }
	inline void set_m_NaNKilled_37(bool value)
	{
		___m_NaNKilled_37 = value;
	}

	inline static int32_t get_offset_of_m_ActiveEffects_38() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_ActiveEffects_38)); }
	inline List_1_t939524230874DF71A2B82A3F03291BA2261A7180 * get_m_ActiveEffects_38() const { return ___m_ActiveEffects_38; }
	inline List_1_t939524230874DF71A2B82A3F03291BA2261A7180 ** get_address_of_m_ActiveEffects_38() { return &___m_ActiveEffects_38; }
	inline void set_m_ActiveEffects_38(List_1_t939524230874DF71A2B82A3F03291BA2261A7180 * value)
	{
		___m_ActiveEffects_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveEffects_38), value);
	}

	inline static int32_t get_offset_of_m_Targets_39() { return static_cast<int32_t>(offsetof(PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2, ___m_Targets_39)); }
	inline List_1_t3D014466D866220BEE24A92AE5BB33E306B8E2B7 * get_m_Targets_39() const { return ___m_Targets_39; }
	inline List_1_t3D014466D866220BEE24A92AE5BB33E306B8E2B7 ** get_address_of_m_Targets_39() { return &___m_Targets_39; }
	inline void set_m_Targets_39(List_1_t3D014466D866220BEE24A92AE5BB33E306B8E2B7 * value)
	{
		___m_Targets_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_Targets_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSLAYER_T23DFD96D31A91809B519CBD47DA4F63BC73D85B2_H
#ifndef POSTPROCESSVOLUME_TA4BF247DA34512CB303F8DEEAFBD60A378FE303D_H
#define POSTPROCESSVOLUME_TA4BF247DA34512CB303F8DEEAFBD60A378FE303D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessVolume
struct  PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessProfile UnityEngine.Rendering.PostProcessing.PostProcessVolume::sharedProfile
	PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E * ___sharedProfile_4;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessVolume::isGlobal
	bool ___isGlobal_5;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::blendDistance
	float ___blendDistance_6;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::weight
	float ___weight_7;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::priority
	float ___priority_8;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_PreviousLayer
	int32_t ___m_PreviousLayer_9;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_PreviousPriority
	float ___m_PreviousPriority_10;
	// System.Collections.Generic.List`1<UnityEngine.Collider> UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_TempColliders
	List_1_t34F3153FFE29D834C770DB7FD47483EE5175933C * ___m_TempColliders_11;
	// UnityEngine.Rendering.PostProcessing.PostProcessProfile UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_InternalProfile
	PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E * ___m_InternalProfile_12;

public:
	inline static int32_t get_offset_of_sharedProfile_4() { return static_cast<int32_t>(offsetof(PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D, ___sharedProfile_4)); }
	inline PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E * get_sharedProfile_4() const { return ___sharedProfile_4; }
	inline PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E ** get_address_of_sharedProfile_4() { return &___sharedProfile_4; }
	inline void set_sharedProfile_4(PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E * value)
	{
		___sharedProfile_4 = value;
		Il2CppCodeGenWriteBarrier((&___sharedProfile_4), value);
	}

	inline static int32_t get_offset_of_isGlobal_5() { return static_cast<int32_t>(offsetof(PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D, ___isGlobal_5)); }
	inline bool get_isGlobal_5() const { return ___isGlobal_5; }
	inline bool* get_address_of_isGlobal_5() { return &___isGlobal_5; }
	inline void set_isGlobal_5(bool value)
	{
		___isGlobal_5 = value;
	}

	inline static int32_t get_offset_of_blendDistance_6() { return static_cast<int32_t>(offsetof(PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D, ___blendDistance_6)); }
	inline float get_blendDistance_6() const { return ___blendDistance_6; }
	inline float* get_address_of_blendDistance_6() { return &___blendDistance_6; }
	inline void set_blendDistance_6(float value)
	{
		___blendDistance_6 = value;
	}

	inline static int32_t get_offset_of_weight_7() { return static_cast<int32_t>(offsetof(PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D, ___weight_7)); }
	inline float get_weight_7() const { return ___weight_7; }
	inline float* get_address_of_weight_7() { return &___weight_7; }
	inline void set_weight_7(float value)
	{
		___weight_7 = value;
	}

	inline static int32_t get_offset_of_priority_8() { return static_cast<int32_t>(offsetof(PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D, ___priority_8)); }
	inline float get_priority_8() const { return ___priority_8; }
	inline float* get_address_of_priority_8() { return &___priority_8; }
	inline void set_priority_8(float value)
	{
		___priority_8 = value;
	}

	inline static int32_t get_offset_of_m_PreviousLayer_9() { return static_cast<int32_t>(offsetof(PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D, ___m_PreviousLayer_9)); }
	inline int32_t get_m_PreviousLayer_9() const { return ___m_PreviousLayer_9; }
	inline int32_t* get_address_of_m_PreviousLayer_9() { return &___m_PreviousLayer_9; }
	inline void set_m_PreviousLayer_9(int32_t value)
	{
		___m_PreviousLayer_9 = value;
	}

	inline static int32_t get_offset_of_m_PreviousPriority_10() { return static_cast<int32_t>(offsetof(PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D, ___m_PreviousPriority_10)); }
	inline float get_m_PreviousPriority_10() const { return ___m_PreviousPriority_10; }
	inline float* get_address_of_m_PreviousPriority_10() { return &___m_PreviousPriority_10; }
	inline void set_m_PreviousPriority_10(float value)
	{
		___m_PreviousPriority_10 = value;
	}

	inline static int32_t get_offset_of_m_TempColliders_11() { return static_cast<int32_t>(offsetof(PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D, ___m_TempColliders_11)); }
	inline List_1_t34F3153FFE29D834C770DB7FD47483EE5175933C * get_m_TempColliders_11() const { return ___m_TempColliders_11; }
	inline List_1_t34F3153FFE29D834C770DB7FD47483EE5175933C ** get_address_of_m_TempColliders_11() { return &___m_TempColliders_11; }
	inline void set_m_TempColliders_11(List_1_t34F3153FFE29D834C770DB7FD47483EE5175933C * value)
	{
		___m_TempColliders_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempColliders_11), value);
	}

	inline static int32_t get_offset_of_m_InternalProfile_12() { return static_cast<int32_t>(offsetof(PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D, ___m_InternalProfile_12)); }
	inline PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E * get_m_InternalProfile_12() const { return ___m_InternalProfile_12; }
	inline PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E ** get_address_of_m_InternalProfile_12() { return &___m_InternalProfile_12; }
	inline void set_m_InternalProfile_12(PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E * value)
	{
		___m_InternalProfile_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalProfile_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSVOLUME_TA4BF247DA34512CB303F8DEEAFBD60A378FE303D_H
#ifndef VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#define VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_4;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___bodyStyle_7;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___headerStyle_8;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___footerStyle_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___bodyTexture_10;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___headerTexture_11;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___footerTexture_12;

public:
	inline static int32_t get_offset_of_mErrorText_4() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___mErrorText_4)); }
	inline String_t* get_mErrorText_4() const { return ___mErrorText_4; }
	inline String_t** get_address_of_mErrorText_4() { return &___mErrorText_4; }
	inline void set_mErrorText_4(String_t* value)
	{
		___mErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_4), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___mErrorOccurred_5)); }
	inline bool get_mErrorOccurred_5() const { return ___mErrorOccurred_5; }
	inline bool* get_address_of_mErrorOccurred_5() { return &___mErrorOccurred_5; }
	inline void set_mErrorOccurred_5(bool value)
	{
		___mErrorOccurred_5 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___bodyStyle_7)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_bodyStyle_7() const { return ___bodyStyle_7; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_bodyStyle_7() { return &___bodyStyle_7; }
	inline void set_bodyStyle_7(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___bodyStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_7), value);
	}

	inline static int32_t get_offset_of_headerStyle_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___headerStyle_8)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_headerStyle_8() const { return ___headerStyle_8; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_headerStyle_8() { return &___headerStyle_8; }
	inline void set_headerStyle_8(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___headerStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_8), value);
	}

	inline static int32_t get_offset_of_footerStyle_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___footerStyle_9)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_footerStyle_9() const { return ___footerStyle_9; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_footerStyle_9() { return &___footerStyle_9; }
	inline void set_footerStyle_9(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___footerStyle_9 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_9), value);
	}

	inline static int32_t get_offset_of_bodyTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___bodyTexture_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_bodyTexture_10() const { return ___bodyTexture_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_bodyTexture_10() { return &___bodyTexture_10; }
	inline void set_bodyTexture_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___bodyTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_10), value);
	}

	inline static int32_t get_offset_of_headerTexture_11() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___headerTexture_11)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_headerTexture_11() const { return ___headerTexture_11; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_headerTexture_11() { return &___headerTexture_11; }
	inline void set_headerTexture_11(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___headerTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_11), value);
	}

	inline static int32_t get_offset_of_footerTexture_12() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___footerTexture_12)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_footerTexture_12() const { return ___footerTexture_12; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_footerTexture_12() { return &___footerTexture_12; }
	inline void set_footerTexture_12(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___footerTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3600[6] = 
{
	LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F::get_offset_of_intensity_7(),
	LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F::get_offset_of_intensityX_8(),
	LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F::get_offset_of_intensityY_9(),
	LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F::get_offset_of_centerX_10(),
	LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F::get_offset_of_centerY_11(),
	LensDistortion_tD1D078C7FCCFBF10294B9C92DB7FCB768D0D586F::get_offset_of_scale_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (LensDistortionRenderer_tD0992AD8A28D4048994B736C7E62A7A3C54B3E34), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3602[2] = 
{
	MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165::get_offset_of_shutterAngle_7(),
	MotionBlur_t222E0B381BC4028B97DADE6A484812479E54E165::get_offset_of_sampleCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (MotionBlurRenderer_t004254D33561D01A03920F2548308516C85A272F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (Pass_t264F66F4CAA144E9EC147CBF990804B7492A263C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3604[7] = 
{
	Pass_t264F66F4CAA144E9EC147CBF990804B7492A263C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3605[10] = 
{
	MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6::get_offset_of_m_SampleThickness_0(),
	MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6::get_offset_of_m_InvThicknessTable_1(),
	MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6::get_offset_of_m_SampleWeightTable_2(),
	MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6::get_offset_of_m_Widths_3(),
	MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6::get_offset_of_m_Heights_4(),
	MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6::get_offset_of_m_Settings_5(),
	MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6::get_offset_of_m_PropertySheet_6(),
	MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6::get_offset_of_m_Resources_7(),
	MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6::get_offset_of_m_AmbientOnlyAO_8(),
	MultiScaleVO_t916332C3C084DF662B2DEE0376083133C7BAB2C6::get_offset_of_m_MRT_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (MipLevel_tCBD464EE7356E35A1364CBE9B0C5C6B913050EB9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3606[8] = 
{
	MipLevel_tCBD464EE7356E35A1364CBE9B0C5C6B913050EB9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (Pass_t76CE8D717150416754BF0DA8BFBD76168BC8B887)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3607[5] = 
{
	Pass_t76CE8D717150416754BF0DA8BFBD76168BC8B887::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3608[5] = 
{
	ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33::get_offset_of_m_Result_0(),
	ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33::get_offset_of_m_PropertySheet_1(),
	ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33::get_offset_of_m_Settings_2(),
	ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33::get_offset_of_m_MRT_3(),
	ScalableAO_tE5626773DE5D0B7895B64D8B8DA64D1FEFD2AE33::get_offset_of_m_SampleCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (Pass_t4E5F6D23D9B3602519552218DD227CE731A4D241)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3609[9] = 
{
	Pass_t4E5F6D23D9B3602519552218DD227CE731A4D241::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (ScreenSpaceReflectionPreset_tD5E24AD52047183FBEF36FF6ACD36EA3098DC4DF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3610[9] = 
{
	ScreenSpaceReflectionPreset_tD5E24AD52047183FBEF36FF6ACD36EA3098DC4DF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (ScreenSpaceReflectionResolution_t7FD9ADDC1B93D2A141F30A114785DF26A72544C5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3611[4] = 
{
	ScreenSpaceReflectionResolution_t7FD9ADDC1B93D2A141F30A114785DF26A72544C5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (ScreenSpaceReflectionPresetParameter_tD789E7238F9338AC7EEBC50D75C89648C9FF7778), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (ScreenSpaceReflectionResolutionParameter_tE0C3A2C876211C74352F5968C45AD1C60C666F50), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3614[7] = 
{
	ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325::get_offset_of_preset_7(),
	ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325::get_offset_of_maximumIterationCount_8(),
	ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325::get_offset_of_resolution_9(),
	ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325::get_offset_of_thickness_10(),
	ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325::get_offset_of_maximumMarchDistance_11(),
	ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325::get_offset_of_distanceFade_12(),
	ScreenSpaceReflections_tD41B4F6E2A57DC6DBA38B7E0AE4B0C0E4735C325::get_offset_of_vignette_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (ScreenSpaceReflectionsRenderer_tA1AD5222A4989034AEC0C2B297CF8D781D67AC0D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3615[4] = 
{
	ScreenSpaceReflectionsRenderer_tA1AD5222A4989034AEC0C2B297CF8D781D67AC0D::get_offset_of_m_Resolve_2(),
	ScreenSpaceReflectionsRenderer_tA1AD5222A4989034AEC0C2B297CF8D781D67AC0D::get_offset_of_m_History_3(),
	ScreenSpaceReflectionsRenderer_tA1AD5222A4989034AEC0C2B297CF8D781D67AC0D::get_offset_of_m_MipIDs_4(),
	ScreenSpaceReflectionsRenderer_tA1AD5222A4989034AEC0C2B297CF8D781D67AC0D::get_offset_of_m_Presets_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (QualityPreset_tDC55FF6F041F963732F2EAAE3AF685BCED564EF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3616[3] = 
{
	QualityPreset_tDC55FF6F041F963732F2EAAE3AF685BCED564EF9::get_offset_of_maximumIterationCount_0(),
	QualityPreset_tDC55FF6F041F963732F2EAAE3AF685BCED564EF9::get_offset_of_thickness_1(),
	QualityPreset_tDC55FF6F041F963732F2EAAE3AF685BCED564EF9::get_offset_of_downsampling_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (Pass_t4C474550C72E2AE28170430C78280B09F09F8A01)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3617[5] = 
{
	Pass_t4C474550C72E2AE28170430C78280B09F09F8A01::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (SubpixelMorphologicalAntialiasing_tDE9497AEBB401059AB2F21890704B15C374B79DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3618[1] = 
{
	SubpixelMorphologicalAntialiasing_tDE9497AEBB401059AB2F21890704B15C374B79DD::get_offset_of_quality_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (Pass_tE937511E9D3F1E227E0FEB8456DD5DF0A28819B8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3619[4] = 
{
	Pass_tE937511E9D3F1E227E0FEB8456DD5DF0A28819B8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (Quality_tF55D5DEED13EABAFEB6A2AE4B4CE32603D758710)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3620[4] = 
{
	Quality_tF55D5DEED13EABAFEB6A2AE4B4CE32603D758710::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3621[14] = 
{
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_jitterSpread_0(),
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_sharpness_1(),
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_stationaryBlending_2(),
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_motionBlending_3(),
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_jitteredMatrixFunc_4(),
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_U3CjitterU3Ek__BackingField_5(),
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_m_Mrt_6(),
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_m_ResetHistory_7(),
	0,
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_U3CsampleIndexU3Ek__BackingField_9(),
	0,
	0,
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_m_HistoryTextures_12(),
	TemporalAntialiasing_t708477A918872723B0F89A467FFAFC7EA27B97D1::get_offset_of_m_HistoryPingPong_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (Pass_tC06140916640021B01BB1C7F7E1CF85042282FE3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3622[3] = 
{
	Pass_tC06140916640021B01BB1C7F7E1CF85042282FE3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (VignetteMode_t0FD0B75231561D28DAA858F7739527CFD8696046)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3623[3] = 
{
	VignetteMode_t0FD0B75231561D28DAA858F7739527CFD8696046::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (VignetteModeParameter_t11A319B818611259008B12060E0263B287D90C14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3625[9] = 
{
	Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31::get_offset_of_mode_7(),
	Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31::get_offset_of_color_8(),
	Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31::get_offset_of_center_9(),
	Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31::get_offset_of_intensity_10(),
	Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31::get_offset_of_smoothness_11(),
	Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31::get_offset_of_roundness_12(),
	Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31::get_offset_of_rounded_13(),
	Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31::get_offset_of_mask_14(),
	Vignette_tCF8492DA1D89F968A7CDB3857FA9252B61C53D31::get_offset_of_opacity_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (VignetteRenderer_t37D1882E6AC06E8599CE7D277AA09CA1FE6EFB79), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3627[7] = 
{
	HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2::get_offset_of_width_2(),
	HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2::get_offset_of_height_3(),
	HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2::get_offset_of_channel_4(),
	HistogramMonitor_t20EDD407C13FD6F9B96233D0925863ACD10D7FA2::get_offset_of_m_Data_5(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (Channel_t7B171541952948F851D8DFD5BC7A16AEE4E0D02E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3628[5] = 
{
	Channel_t7B171541952948F851D8DFD5BC7A16AEE4E0D02E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[3] = 
{
	LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075::get_offset_of_width_2(),
	LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075::get_offset_of_height_3(),
	LightMeterMonitor_t170AF7815A8487BD85CBF0EEC68865453951A075::get_offset_of_showCurves_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (MonitorType_tAFB1F1FED2AB2C4704DC197BB2CEF4A96267AE8C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3630[5] = 
{
	MonitorType_tAFB1F1FED2AB2C4704DC197BB2CEF4A96267AE8C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (Monitor_t9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3631[2] = 
{
	Monitor_t9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA::get_offset_of_U3CoutputU3Ek__BackingField_0(),
	Monitor_t9E2B2FB4C7BC386F8EB5CDDD8FE5B35D8BB012AA::get_offset_of_requested_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3632[5] = 
{
	VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B::get_offset_of_size_2(),
	VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B::get_offset_of_exposure_3(),
	VectorscopeMonitor_tACC68DFCDB9632D0F2C4577DCDCC22EF7D77D36B::get_offset_of_m_Data_4(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3633[6] = 
{
	WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6::get_offset_of_exposure_2(),
	WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6::get_offset_of_height_3(),
	WaveformMonitor_t3ACB5C1E48425044C04B01099C7D99B57DE859A6::get_offset_of_m_Data_4(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3634[1] = 
{
	ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C::get_offset_of_overrideState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3635[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (IntParameter_t268BB9E59D69C34B45EB3174B23702F6C91760CC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (TextureParameterDefault_t774773D69629DCC7D41D000FCE712DD1A6307FFB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3643[6] = 
{
	TextureParameterDefault_t774773D69629DCC7D41D000FCE712DD1A6307FFB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3644[1] = 
{
	TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87::get_offset_of_defaultState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3645[3] = 
{
	PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225::get_offset_of_U3CattributeU3Ek__BackingField_0(),
	PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225::get_offset_of_U3CsettingsU3Ek__BackingField_1(),
	PostProcessBundle_tE29C05526676DF2148A37681C517E85FD98BE225::get_offset_of_m_Renderer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (DebugOverlay_t7F24134A21C1E2972B7BA13F89C2756CF7A7FF2E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3646[12] = 
{
	DebugOverlay_t7F24134A21C1E2972B7BA13F89C2756CF7A7FF2E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (ColorBlindnessType_tF783AE5750B515662C15C92D8F428BACE786E7F6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3647[4] = 
{
	ColorBlindnessType_tF783AE5750B515662C15C92D8F428BACE786E7F6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3648[11] = 
{
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_lightMeter_0(),
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_histogram_1(),
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_waveform_2(),
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_vectorscope_3(),
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_m_Monitors_4(),
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_frameWidth_5(),
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_frameHeight_6(),
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_U3CdebugOverlayTargetU3Ek__BackingField_7(),
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_U3CdebugOverlayActiveU3Ek__BackingField_8(),
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_U3CdebugOverlayU3Ek__BackingField_9(),
	PostProcessDebugLayer_t4E9A4AB3CC038EC6A976F756D455A89AD156AC39::get_offset_of_overlaySettings_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3649[5] = 
{
	OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6::get_offset_of_linearDepth_0(),
	OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6::get_offset_of_motionColorIntensity_1(),
	OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6::get_offset_of_motionGridSize_2(),
	OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6::get_offset_of_colorBlindnessType_3(),
	OverlaySettings_t386F6AF515101C43472A020FF0F0B03C0F3607B6::get_offset_of_colorBlindnessStrength_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3650[1] = 
{
	PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F::get_offset_of_m_ResetHistory_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[3] = 
{
	PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042::get_offset_of_active_4(),
	PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042::get_offset_of_enabled_5(),
	PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042::get_offset_of_parameters_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2), -1, sizeof(U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3653[3] = 
{
	U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
	U3CU3Ec_t554709697877216F0BA6F6ADC8DF28A35C362EF2_StaticFields::get_offset_of_U3CU3E9__3_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (PostProcessEvent_tA94B9311FE47676FC4B04E6F7935F3DE20E42BDA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3654[4] = 
{
	PostProcessEvent_tA94B9311FE47676FC4B04E6F7935F3DE20E42BDA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (PostProcessEventComparer_t69E93AE9BD81722DFAFAD51564C3C555AE0975E4)+ sizeof (RuntimeObject), sizeof(PostProcessEventComparer_t69E93AE9BD81722DFAFAD51564C3C555AE0975E4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3656[36] = 
{
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_volumeTrigger_4(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_volumeLayer_5(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_stopNaNPropagation_6(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_finalBlitToCameraTarget_7(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_antialiasingMode_8(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_temporalAntialiasing_9(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_subpixelMorphologicalAntialiasing_10(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_fastApproximateAntialiasing_11(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_fog_12(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_dithering_13(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_debugLayer_14(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_Resources_15(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_ShowToolkit_16(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_ShowCustomSorter_17(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_breakBeforeColorGrading_18(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_BeforeTransparentBundles_19(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_BeforeStackBundles_20(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_AfterStackBundles_21(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_U3CsortedBundlesU3Ek__BackingField_22(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_U3CcameraDepthFlagsU3Ek__BackingField_23(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_U3ChaveBundlesBeenInitedU3Ek__BackingField_24(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_Bundles_25(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_PropertySheetFactory_26(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_LegacyCmdBufferBeforeReflections_27(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_LegacyCmdBufferBeforeLighting_28(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_LegacyCmdBufferOpaque_29(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_LegacyCmdBuffer_30(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_Camera_31(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_CurrentContext_32(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_LogHistogram_33(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_SettingsUpdateNeeded_34(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_IsRenderingInSceneView_35(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_TargetPool_36(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_NaNKilled_37(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_ActiveEffects_38(),
	PostProcessLayer_t23DFD96D31A91809B519CBD47DA4F63BC73D85B2::get_offset_of_m_Targets_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (Antialiasing_t4B83F9C9FA7E42D5B3EB30CE9B5B4F870C3780FA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3657[5] = 
{
	Antialiasing_t4B83F9C9FA7E42D5B3EB30CE9B5B4F870C3780FA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (SerializedBundleRef_tFD6F759A8A0049AFDCA3E2380674C206707FE90E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3658[2] = 
{
	SerializedBundleRef_tFD6F759A8A0049AFDCA3E2380674C206707FE90E::get_offset_of_assemblyQualifiedName_0(),
	SerializedBundleRef_tFD6F759A8A0049AFDCA3E2380674C206707FE90E::get_offset_of_bundle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (U3CU3Ec__DisplayClass51_0_t5A29CC1C56D8C5C3B797C8CEBB7C6195C64F5EBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3659[2] = 
{
	U3CU3Ec__DisplayClass51_0_t5A29CC1C56D8C5C3B797C8CEBB7C6195C64F5EBF::get_offset_of_evt_0(),
	U3CU3Ec__DisplayClass51_0_t5A29CC1C56D8C5C3B797C8CEBB7C6195C64F5EBF::get_offset_of_effects_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (U3CU3Ec__DisplayClass51_1_t9E5857DEB9F2265096E31C0B60D02137153F6562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3660[1] = 
{
	U3CU3Ec__DisplayClass51_1_t9E5857DEB9F2265096E31C0B60D02137153F6562::get_offset_of_searchStr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (U3CU3Ec__DisplayClass51_2_t7F30F5295B2ECD71E50DBF73A1B2D0D87445767B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3661[1] = 
{
	U3CU3Ec__DisplayClass51_2_t7F30F5295B2ECD71E50DBF73A1B2D0D87445767B::get_offset_of_typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (U3CU3Ec__DisplayClass51_3_t9A6B0E881515DF4145E88A6630A204668F3DABF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3662[1] = 
{
	U3CU3Ec__DisplayClass51_3_t9A6B0E881515DF4145E88A6630A204668F3DABF0::get_offset_of_typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E), -1, sizeof(U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3663[2] = 
{
	U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD342126B7CDEF358CBE105AB5E085F7EC496615E_StaticFields::get_offset_of_U3CU3E9__51_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189), -1, sizeof(PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3664[7] = 
{
	PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189_StaticFields::get_offset_of_s_Instance_0(),
	PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189::get_offset_of_m_SortedVolumes_1(),
	PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189::get_offset_of_m_Volumes_2(),
	PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189::get_offset_of_m_SortNeeded_3(),
	PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189::get_offset_of_m_BaseSettings_4(),
	PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189::get_offset_of_m_TempColliders_5(),
	PostProcessManager_t4AE593295C402F7C792D8D4DF15443DBC2831189::get_offset_of_settingsTypes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165), -1, sizeof(U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3665[2] = 
{
	U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1BC90B0B2626B0720C768575D339258F724B5165_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[2] = 
{
	PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E::get_offset_of_settings_4(),
	PostProcessProfile_tEDCC9540DA0B4BBB006AF5858E8A96D0E528DC5E::get_offset_of_isDirty_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A), -1, sizeof(U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3667[2] = 
{
	U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tBFB16AE2FACC328F1110602BF577A3CF3BEED14A_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3668[29] = 
{
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_m_Camera_0(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CcommandU3Ek__BackingField_1(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CsourceU3Ek__BackingField_2(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CdestinationU3Ek__BackingField_3(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CsourceFormatU3Ek__BackingField_4(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CflipU3Ek__BackingField_5(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CresourcesU3Ek__BackingField_6(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CpropertySheetsU3Ek__BackingField_7(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CuserDataU3Ek__BackingField_8(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CdebugLayerU3Ek__BackingField_9(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CwidthU3Ek__BackingField_10(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CheightU3Ek__BackingField_11(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CstereoActiveU3Ek__BackingField_12(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CxrActiveEyeU3Ek__BackingField_13(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CnumberOfEyesU3Ek__BackingField_14(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CstereoRenderingModeU3Ek__BackingField_15(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CscreenWidthU3Ek__BackingField_16(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CscreenHeightU3Ek__BackingField_17(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CisSceneViewU3Ek__BackingField_18(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CantialiasingU3Ek__BackingField_19(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_U3CtemporalAntialiasingU3Ek__BackingField_20(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_uberSheet_21(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_autoExposureTexture_22(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_logHistogram_23(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_logLut_24(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_autoExposure_25(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_bloomBufferNameID_26(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_physicalCamera_27(),
	PostProcessRenderContext_tF3C9D7123D8B250A5E5FE6743B2319890F0168BB::get_offset_of_m_sourceDescriptor_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (StereoRenderingMode_t3EFE9CED6CDCB21D9519A8F922090969360DFF66)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3669[5] = 
{
	StereoRenderingMode_t3EFE9CED6CDCB21D9519A8F922090969360DFF66::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3670[5] = 
{
	PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B::get_offset_of_blueNoise64_4(),
	PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B::get_offset_of_blueNoise256_5(),
	PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B::get_offset_of_smaaLuts_6(),
	PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B::get_offset_of_shaders_7(),
	PostProcessResources_t70D45255B03138A85C22CE4619CF88EB1F0C2D6B::get_offset_of_computeShaders_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[24] = 
{
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_bloom_0(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_copy_1(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_copyStd_2(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_copyStdFromTexArray_3(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_copyStdFromDoubleWide_4(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_discardAlpha_5(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_depthOfField_6(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_finalPass_7(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_grainBaker_8(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_motionBlur_9(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_temporalAntialiasing_10(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_subpixelMorphologicalAntialiasing_11(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_texture2dLerp_12(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_uber_13(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_lut2DBaker_14(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_lightMeter_15(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_gammaHistogram_16(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_waveform_17(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_vectorscope_18(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_debugOverlays_19(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_deferredFog_20(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_scalableAO_21(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_multiScaleAO_22(),
	Shaders_t8171DAB81DB3F380BB5F42114476BE06467B34DF::get_offset_of_screenSpaceReflections_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3672[12] = 
{
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_autoExposure_0(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_exposureHistogram_1(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_lut3DBaker_2(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_texture3dLerp_3(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_gammaHistogram_4(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_waveform_5(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_vectorscope_6(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_multiScaleAODownsample1_7(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_multiScaleAODownsample2_8(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_multiScaleAORender_9(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_multiScaleAOUpsample_10(),
	ComputeShaders_t698DD1B303C404D771872D30E978C4F4237CA457::get_offset_of_gaussianDownsample_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3673[2] = 
{
	SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090::get_offset_of_area_0(),
	SMAALuts_t7C73EA4D782E325A2FF0795B7FAF5DAA343A4090::get_offset_of_search_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3674[9] = 
{
	PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D::get_offset_of_sharedProfile_4(),
	PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D::get_offset_of_isGlobal_5(),
	PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D::get_offset_of_blendDistance_6(),
	PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D::get_offset_of_weight_7(),
	PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D::get_offset_of_priority_8(),
	PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D::get_offset_of_m_PreviousLayer_9(),
	PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D::get_offset_of_m_PreviousPriority_10(),
	PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D::get_offset_of_m_TempColliders_11(),
	PostProcessVolume_tA4BF247DA34512CB303F8DEEAFBD60A378FE303D::get_offset_of_m_InternalProfile_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (ColorUtilities_t6DCFF299E256E07788F733F928088AD3378F956B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3676[6] = 
{
	HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D::get_offset_of_U3CwhitePointU3Ek__BackingField_0(),
	HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D::get_offset_of_U3CinverseWhitePointU3Ek__BackingField_1(),
	HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D::get_offset_of_U3Cx0U3Ek__BackingField_2(),
	HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D::get_offset_of_U3Cx1U3Ek__BackingField_3(),
	HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D::get_offset_of_m_Segments_4(),
	HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D::get_offset_of_uniforms_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3677[6] = 
{
	Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C::get_offset_of_offsetX_0(),
	Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C::get_offset_of_offsetY_1(),
	Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C::get_offset_of_scaleX_2(),
	Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C::get_offset_of_scaleY_3(),
	Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C::get_offset_of_lnA_4(),
	Segment_tA16884985B21D43ECD1DD8792FA789BD0C64411C::get_offset_of_B_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6)+ sizeof (RuntimeObject), sizeof(DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3678[8] = 
{
	DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6::get_offset_of_x0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6::get_offset_of_y0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6::get_offset_of_x1_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6::get_offset_of_y1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6::get_offset_of_W_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6::get_offset_of_overshootX_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6::get_offset_of_overshootY_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DirectParams_tADDEE8C7C4C655132C07B0993B1D6F115BCAC2B6::get_offset_of_gamma_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (Uniforms_tA0574F53210B6F8C0A398C19739AFC0FD2A61CE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[1] = 
{
	Uniforms_tA0574F53210B6F8C0A398C19739AFC0FD2A61CE4::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (HaltonSeq_t1A3CE446490E66FFA4ED3809DE5C2CB78983EDC6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3681[4] = 
{
	0,
	0,
	0,
	LogHistogram_t25F6613321C3909DA2D9A9BB3AC78AC84447DF79::get_offset_of_U3CdataU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3682[2] = 
{
	PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735::get_offset_of_U3CpropertiesU3Ek__BackingField_0(),
	PropertySheet_tA49E657A6C015EADD4DD1F14FD41F022D5DE3735::get_offset_of_U3CmaterialU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3683[1] = 
{
	PropertySheetFactory_t3E86AEE66A7E970A0861A8B698F21F4135BA0EE4::get_offset_of_m_Sheets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95), -1, sizeof(RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3684[12] = 
{
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_m_WhiteTexture_0(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_m_BlackTexture_1(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_m_LutStrips_2(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_s_Resources_3(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_s_FullscreenTriangle_4(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_s_CopyStdMaterial_5(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_s_CopyStdFromDoubleWideMaterial_6(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_s_CopyMaterial_7(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_s_CopyFromTexArrayMaterial_8(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_s_CopySheet_9(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_s_CopyFromTexArraySheet_10(),
	RuntimeUtilities_t26ECA2041AD9AA47E8B5C4BF74D6CFCED7A7FE95_StaticFields::get_offset_of_m_AssemblyTypes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F), -1, sizeof(U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3685[2] = 
{
	U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD4C77A39D977A00B1BDD0A71D94D6A153CF85C7F_StaticFields::get_offset_of_U3CU3E9__87_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721), -1, sizeof(ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3686[130] = 
{
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_MainTex_0(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Jitter_1(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Sharpness_2(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_FinalBlendParameters_3(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_HistoryTex_4(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_SMAA_Flip_5(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_SMAA_Flop_6(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_AOParams_7(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_AOColor_8(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_OcclusionTexture1_9(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_OcclusionTexture2_10(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_SAOcclusionTexture_11(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_MSVOcclusionTexture_12(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_DepthCopy_13(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_LinearDepth_14(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_LowDepth1_15(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_LowDepth2_16(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_LowDepth3_17(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_LowDepth4_18(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_TiledDepth1_19(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_TiledDepth2_20(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_TiledDepth3_21(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_TiledDepth4_22(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Occlusion1_23(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Occlusion2_24(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Occlusion3_25(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Occlusion4_26(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Combined1_27(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Combined2_28(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Combined3_29(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_SSRResolveTemp_30(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Noise_31(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Test_32(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Resolve_33(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_History_34(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ViewMatrix_35(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_InverseViewMatrix_36(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_InverseProjectionMatrix_37(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ScreenSpaceProjectionMatrix_38(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Params2_39(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_FogColor_40(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_FogParams_41(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_VelocityScale_42(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_MaxBlurRadius_43(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_RcpMaxBlurRadius_44(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_VelocityTex_45(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Tile2RT_46(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Tile4RT_47(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Tile8RT_48(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_TileMaxOffs_49(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_TileMaxLoop_50(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_TileVRT_51(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_NeighborMaxTex_52(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_LoopCount_53(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_DepthOfFieldTemp_54(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_DepthOfFieldTex_55(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Distance_56(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_LensCoeff_57(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_MaxCoC_58(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_RcpMaxCoC_59(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_RcpAspect_60(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_CoCTex_61(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_TaaParams_62(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_AutoExposureTex_63(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_HistogramBuffer_64(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Params_65(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ScaleOffsetRes_66(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_BloomTex_67(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_SampleScale_68(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Threshold_69(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ColorIntensity_70(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Bloom_DirtTex_71(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Bloom_Settings_72(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Bloom_Color_73(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Bloom_DirtTileOffset_74(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ChromaticAberration_Amount_75(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ChromaticAberration_SpectralLut_76(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Distortion_CenterScale_77(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Distortion_Amount_78(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Lut2D_79(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Lut3D_80(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Lut3D_Params_81(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Lut2D_Params_82(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_UserLut2D_Params_83(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_PostExposure_84(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ColorBalance_85(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ColorFilter_86(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_HueSatCon_87(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Brightness_88(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ChannelMixerRed_89(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ChannelMixerGreen_90(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ChannelMixerBlue_91(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Lift_92(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_InvGamma_93(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Gain_94(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Curves_95(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_CustomToneCurve_96(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ToeSegmentA_97(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ToeSegmentB_98(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_MidSegmentA_99(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_MidSegmentB_100(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ShoSegmentA_101(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_ShoSegmentB_102(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Vignette_Color_103(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Vignette_Center_104(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Vignette_Settings_105(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Vignette_Mask_106(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Vignette_Opacity_107(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Vignette_Mode_108(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Grain_Params1_109(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Grain_Params2_110(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_GrainTex_111(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Phase_112(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_GrainNoiseParameters_113(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_LumaInAlpha_114(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_DitheringTex_115(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Dithering_Coords_116(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_From_117(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_To_118(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_Interp_119(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_TargetColor_120(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_HalfResFinalCopy_121(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_WaveformSource_122(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_WaveformBuffer_123(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_VectorscopeBuffer_124(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_RenderViewportScaleFactor_125(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_UVTransform_126(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_DepthSlice_127(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_UVScaleOffset_128(),
	ShaderIDs_tF2DD07006369DE1FD0D337C7CF4D2F6849F81721_StaticFields::get_offset_of_PosScaleOffset_129(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (Spline_t42213D550262BA4C9B574C062AB52021A970081F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3687[7] = 
{
	Spline_t42213D550262BA4C9B574C062AB52021A970081F::get_offset_of_curve_0(),
	Spline_t42213D550262BA4C9B574C062AB52021A970081F::get_offset_of_m_Loop_1(),
	Spline_t42213D550262BA4C9B574C062AB52021A970081F::get_offset_of_m_ZeroValue_2(),
	Spline_t42213D550262BA4C9B574C062AB52021A970081F::get_offset_of_m_Range_3(),
	Spline_t42213D550262BA4C9B574C062AB52021A970081F::get_offset_of_m_InternalLoopingCurve_4(),
	Spline_t42213D550262BA4C9B574C062AB52021A970081F::get_offset_of_frameCount_5(),
	Spline_t42213D550262BA4C9B574C062AB52021A970081F::get_offset_of_cachedData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3688[2] = 
{
	TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949::get_offset_of_m_Pool_0(),
	TargetPool_t974506C6CF46650F99CDC2EC4E34EC6B5E119949::get_offset_of_m_Current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (TextureFormatUtilities_tFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2), -1, sizeof(TextureFormatUtilities_tFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3689[3] = 
{
	TextureFormatUtilities_tFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_StaticFields::get_offset_of_s_FormatAliasMap_0(),
	TextureFormatUtilities_tFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_StaticFields::get_offset_of_s_SupportedRenderTextureFormats_1(),
	TextureFormatUtilities_tFE9FE4FE5EEE20A9F6E04B098C43C42A1AED70E2_StaticFields::get_offset_of_s_SupportedTextureFormats_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83), -1, sizeof(TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3690[6] = 
{
	TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83_StaticFields::get_offset_of_m_Instance_0(),
	TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83::get_offset_of_m_Command_1(),
	TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83::get_offset_of_m_PropertySheets_2(),
	TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83::get_offset_of_m_Resources_3(),
	TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83::get_offset_of_m_Recycled_4(),
	TextureLerper_t959B5D2D8F95C3C99FA908490054E5019A61AA83::get_offset_of_m_Actives_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (U3CPrivateImplementationDetailsU3E_t205DFFBD2DB592A47BC91DB97BB3717FA0393C48), -1, sizeof(U3CPrivateImplementationDetailsU3E_t205DFFBD2DB592A47BC91DB97BB3717FA0393C48_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3691[1] = 
{
	U3CPrivateImplementationDetailsU3E_t205DFFBD2DB592A47BC91DB97BB3717FA0393C48_StaticFields::get_offset_of_U30ED907628EE272F93737B500A23D77C9B1C88368_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (__StaticArrayInitTypeSizeU3D20_t5985CE017087B57462C0BFD1B938A2521A7C364C)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_t5985CE017087B57462C0BFD1B938A2521A7C364C ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (U3CModuleU3E_tBB5D6FDE6CBE0067E5DC774822D5054F01745F8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (BoundingBoxRenderer_t687A36FDD509AC21A29D0CF70359619EFD251AFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3694[1] = 
{
	BoundingBoxRenderer_t687A36FDD509AC21A29D0CF70359619EFD251AFB::get_offset_of_mLineMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3695[9] = 
{
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_mErrorText_4(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_mErrorOccurred_5(),
	0,
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_bodyStyle_7(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_headerStyle_8(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_footerStyle_9(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_bodyTexture_10(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_headerTexture_11(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_footerTexture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3696[3] = 
{
	DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA::get_offset_of_ModelRecoErrorText_4(),
	DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA::get_offset_of_StopSearchWhenModelFound_5(),
	DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA::get_offset_of_StopSearchWhileTracking_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3697[3] = 
{
	DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022::get_offset_of_mTrackableBehaviour_4(),
	DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022::get_offset_of_m_PreviousStatus_5(),
	DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022::get_offset_of_m_NewStatus_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862), -1, sizeof(RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3698[1] = 
{
	RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862_StaticFields::get_offset_of_sFacade_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3699[2] = 
{
	OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5::get_offset_of_mUnityRenderPipeline_0(),
	OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5::get_offset_of_mUnityAndroidPermissions_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
