﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ICSharpCode.SharpZipLib.Checksums.Adler32
struct Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033;
// ICSharpCode.SharpZipLib.Core.INameTransform
struct INameTransform_t3427F87035EE4BD38B68C3015D253A1A08FB7F29;
// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195;
// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader
struct InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5;
// ICSharpCode.SharpZipLib.Zip.IArchiveStorage
struct IArchiveStorage_tA26244F967901B1B7831DF183DDBAEB6EE787B5C;
// ICSharpCode.SharpZipLib.Zip.IDynamicDataSource
struct IDynamicDataSource_tDF2EF6F650885419EC0DE3BC16B51ED5CE1E2AE8;
// ICSharpCode.SharpZipLib.Zip.IEntryFactory
struct IEntryFactory_t9FAA37B862F9CF44DD8AF1FB148406E1A10C3BEF;
// ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs
struct KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148;
// ICSharpCode.SharpZipLib.Zip.ZipEntry[]
struct ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917;
// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F;
// ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler
struct KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`3<System.Boolean,System.Boolean,System.Int32>
struct Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t1B3F60982C3189AF70B204EF3F19940A645EA02E;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_tE4E9EE9F348ABAD1007C663DD77A14907CCD9A79;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct List_1_t17E826BD8EFE34027ADF1493A584383128BCC213;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t4E10C259BBC525A8E0ABEAE9EF01C8589F51FEE5;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t43C29A7F3A8C2DDAC9F3BF9BF739B03E4D5DE9A9;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E;
// System.String
struct String_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966;
// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t7521247C87411935E8A2CA38683533083459473F;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_tA46EC5C3029914B5C6BC43C2337CBB8067BB19FC;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#define U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t76DD45B11E728799BA16B6E93B81827DD86E5AEE 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#ifndef U3CMODULEU3E_TAA90CDF50306A16DF6C29043AB49497B8E0C9DA6_H
#define U3CMODULEU3E_TAA90CDF50306A16DF6C29043AB49497B8E0C9DA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tAA90CDF50306A16DF6C29043AB49497B8E0C9DA6 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TAA90CDF50306A16DF6C29043AB49497B8E0C9DA6_H
#ifndef U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#define U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifndef U3CMODULEU3E_TCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0_H
#define U3CMODULEU3E_TCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0_H
#ifndef U3CMODULEU3E_TB054F17A779AC945E3659AF119A96DB806541AF9_H
#define U3CMODULEU3E_TB054F17A779AC945E3659AF119A96DB806541AF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB054F17A779AC945E3659AF119A96DB806541AF9 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TB054F17A779AC945E3659AF119A96DB806541AF9_H
#ifndef U3CMODULEU3E_T1559CC8A802937295A65A1DCA2F81D3D5CEDAA58_H
#define U3CMODULEU3E_T1559CC8A802937295A65A1DCA2F81D3D5CEDAA58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1559CC8A802937295A65A1DCA2F81D3D5CEDAA58 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1559CC8A802937295A65A1DCA2F81D3D5CEDAA58_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ADLER32_T1764266FFA97C6DE074ACBE8444B9BC93C9F2033_H
#define ADLER32_T1764266FFA97C6DE074ACBE8444B9BC93C9F2033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Checksums.Adler32
struct  Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033  : public RuntimeObject
{
public:
	// System.UInt32 ICSharpCode.SharpZipLib.Checksums.Adler32::checksum
	uint32_t ___checksum_0;

public:
	inline static int32_t get_offset_of_checksum_0() { return static_cast<int32_t>(offsetof(Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033, ___checksum_0)); }
	inline uint32_t get_checksum_0() const { return ___checksum_0; }
	inline uint32_t* get_address_of_checksum_0() { return &___checksum_0; }
	inline void set_checksum_0(uint32_t value)
	{
		___checksum_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLER32_T1764266FFA97C6DE074ACBE8444B9BC93C9F2033_H
#ifndef CRC32_T92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_H
#define CRC32_T92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Checksums.Crc32
struct  Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F  : public RuntimeObject
{
public:

public:
};

struct Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_StaticFields
{
public:
	// System.UInt32[] ICSharpCode.SharpZipLib.Checksums.Crc32::CrcTable
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___CrcTable_0;

public:
	inline static int32_t get_offset_of_CrcTable_0() { return static_cast<int32_t>(offsetof(Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_StaticFields, ___CrcTable_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_CrcTable_0() const { return ___CrcTable_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_CrcTable_0() { return &___CrcTable_0; }
	inline void set_CrcTable_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___CrcTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___CrcTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRC32_T92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_H
#ifndef STREAMUTILS_TC8B2EC2CC286B4E36E2D78C3D96D64770DC3D6F4_H
#define STREAMUTILS_TC8B2EC2CC286B4E36E2D78C3D96D64770DC3D6F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.StreamUtils
struct  StreamUtils_tC8B2EC2CC286B4E36E2D78C3D96D64770DC3D6F4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMUTILS_TC8B2EC2CC286B4E36E2D78C3D96D64770DC3D6F4_H
#ifndef PKZIPCLASSICCRYPTOBASE_T82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B_H
#define PKZIPCLASSICCRYPTOBASE_T82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase
struct  PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B  : public RuntimeObject
{
public:
	// System.UInt32[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::keys
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___keys_0;

public:
	inline static int32_t get_offset_of_keys_0() { return static_cast<int32_t>(offsetof(PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B, ___keys_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_keys_0() const { return ___keys_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_keys_0() { return &___keys_0; }
	inline void set_keys_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___keys_0 = value;
		Il2CppCodeGenWriteBarrier((&___keys_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKZIPCLASSICCRYPTOBASE_T82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B_H
#ifndef ZIPAESTRANSFORM_TD2F237B71B8532EC36AAF6E45E4FFAB185301195_H
#define ZIPAESTRANSFORM_TD2F237B71B8532EC36AAF6E45E4FFAB185301195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct  ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195  : public RuntimeObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_blockSize
	int32_t ____blockSize_0;
	// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encryptor
	RuntimeObject* ____encryptor_1;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_counterNonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____counterNonce_2;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encryptBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____encryptBuffer_3;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encrPos
	int32_t ____encrPos_4;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_pwdVerifier
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____pwdVerifier_5;
	// System.Security.Cryptography.HMACSHA1 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_hmacsha1
	HMACSHA1_t4E10C259BBC525A8E0ABEAE9EF01C8589F51FEE5 * ____hmacsha1_6;
	// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_finalised
	bool ____finalised_7;
	// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_writeMode
	bool ____writeMode_8;

public:
	inline static int32_t get_offset_of__blockSize_0() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____blockSize_0)); }
	inline int32_t get__blockSize_0() const { return ____blockSize_0; }
	inline int32_t* get_address_of__blockSize_0() { return &____blockSize_0; }
	inline void set__blockSize_0(int32_t value)
	{
		____blockSize_0 = value;
	}

	inline static int32_t get_offset_of__encryptor_1() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____encryptor_1)); }
	inline RuntimeObject* get__encryptor_1() const { return ____encryptor_1; }
	inline RuntimeObject** get_address_of__encryptor_1() { return &____encryptor_1; }
	inline void set__encryptor_1(RuntimeObject* value)
	{
		____encryptor_1 = value;
		Il2CppCodeGenWriteBarrier((&____encryptor_1), value);
	}

	inline static int32_t get_offset_of__counterNonce_2() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____counterNonce_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__counterNonce_2() const { return ____counterNonce_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__counterNonce_2() { return &____counterNonce_2; }
	inline void set__counterNonce_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____counterNonce_2 = value;
		Il2CppCodeGenWriteBarrier((&____counterNonce_2), value);
	}

	inline static int32_t get_offset_of__encryptBuffer_3() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____encryptBuffer_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__encryptBuffer_3() const { return ____encryptBuffer_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__encryptBuffer_3() { return &____encryptBuffer_3; }
	inline void set__encryptBuffer_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____encryptBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&____encryptBuffer_3), value);
	}

	inline static int32_t get_offset_of__encrPos_4() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____encrPos_4)); }
	inline int32_t get__encrPos_4() const { return ____encrPos_4; }
	inline int32_t* get_address_of__encrPos_4() { return &____encrPos_4; }
	inline void set__encrPos_4(int32_t value)
	{
		____encrPos_4 = value;
	}

	inline static int32_t get_offset_of__pwdVerifier_5() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____pwdVerifier_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__pwdVerifier_5() const { return ____pwdVerifier_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__pwdVerifier_5() { return &____pwdVerifier_5; }
	inline void set__pwdVerifier_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____pwdVerifier_5 = value;
		Il2CppCodeGenWriteBarrier((&____pwdVerifier_5), value);
	}

	inline static int32_t get_offset_of__hmacsha1_6() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____hmacsha1_6)); }
	inline HMACSHA1_t4E10C259BBC525A8E0ABEAE9EF01C8589F51FEE5 * get__hmacsha1_6() const { return ____hmacsha1_6; }
	inline HMACSHA1_t4E10C259BBC525A8E0ABEAE9EF01C8589F51FEE5 ** get_address_of__hmacsha1_6() { return &____hmacsha1_6; }
	inline void set__hmacsha1_6(HMACSHA1_t4E10C259BBC525A8E0ABEAE9EF01C8589F51FEE5 * value)
	{
		____hmacsha1_6 = value;
		Il2CppCodeGenWriteBarrier((&____hmacsha1_6), value);
	}

	inline static int32_t get_offset_of__finalised_7() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____finalised_7)); }
	inline bool get__finalised_7() const { return ____finalised_7; }
	inline bool* get_address_of__finalised_7() { return &____finalised_7; }
	inline void set__finalised_7(bool value)
	{
		____finalised_7 = value;
	}

	inline static int32_t get_offset_of__writeMode_8() { return static_cast<int32_t>(offsetof(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195, ____writeMode_8)); }
	inline bool get__writeMode_8() const { return ____writeMode_8; }
	inline bool* get_address_of__writeMode_8() { return &____writeMode_8; }
	inline void set__writeMode_8(bool value)
	{
		____writeMode_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPAESTRANSFORM_TD2F237B71B8532EC36AAF6E45E4FFAB185301195_H
#ifndef DEFLATERHUFFMAN_T07ABC2467D43109DA82829D4D0476554059A12BD_H
#define DEFLATERHUFFMAN_T07ABC2467D43109DA82829D4D0476554059A12BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman
struct  DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD  : public RuntimeObject
{
public:

public:
};

struct DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields
{
public:
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::BL_ORDER
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___BL_ORDER_0;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::bit4Reverse
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bit4Reverse_1;
	// System.Int16[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::staticLCodes
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___staticLCodes_2;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::staticLLength
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___staticLLength_3;
	// System.Int16[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::staticDCodes
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___staticDCodes_4;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::staticDLength
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___staticDLength_5;

public:
	inline static int32_t get_offset_of_BL_ORDER_0() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___BL_ORDER_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_BL_ORDER_0() const { return ___BL_ORDER_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_BL_ORDER_0() { return &___BL_ORDER_0; }
	inline void set_BL_ORDER_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___BL_ORDER_0 = value;
		Il2CppCodeGenWriteBarrier((&___BL_ORDER_0), value);
	}

	inline static int32_t get_offset_of_bit4Reverse_1() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___bit4Reverse_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bit4Reverse_1() const { return ___bit4Reverse_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bit4Reverse_1() { return &___bit4Reverse_1; }
	inline void set_bit4Reverse_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bit4Reverse_1 = value;
		Il2CppCodeGenWriteBarrier((&___bit4Reverse_1), value);
	}

	inline static int32_t get_offset_of_staticLCodes_2() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___staticLCodes_2)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_staticLCodes_2() const { return ___staticLCodes_2; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_staticLCodes_2() { return &___staticLCodes_2; }
	inline void set_staticLCodes_2(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___staticLCodes_2 = value;
		Il2CppCodeGenWriteBarrier((&___staticLCodes_2), value);
	}

	inline static int32_t get_offset_of_staticLLength_3() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___staticLLength_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_staticLLength_3() const { return ___staticLLength_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_staticLLength_3() { return &___staticLLength_3; }
	inline void set_staticLLength_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___staticLLength_3 = value;
		Il2CppCodeGenWriteBarrier((&___staticLLength_3), value);
	}

	inline static int32_t get_offset_of_staticDCodes_4() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___staticDCodes_4)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_staticDCodes_4() const { return ___staticDCodes_4; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_staticDCodes_4() { return &___staticDCodes_4; }
	inline void set_staticDCodes_4(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___staticDCodes_4 = value;
		Il2CppCodeGenWriteBarrier((&___staticDCodes_4), value);
	}

	inline static int32_t get_offset_of_staticDLength_5() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields, ___staticDLength_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_staticDLength_5() const { return ___staticDLength_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_staticDLength_5() { return &___staticDLength_5; }
	inline void set_staticDLength_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___staticDLength_5 = value;
		Il2CppCodeGenWriteBarrier((&___staticDLength_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATERHUFFMAN_T07ABC2467D43109DA82829D4D0476554059A12BD_H
#ifndef INFLATER_T69AF9807C58C78BD7FCF1A0372F416961C1C73CB_H
#define INFLATER_T69AF9807C58C78BD7FCF1A0372F416961C1C73CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct  Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB  : public RuntimeObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::mode
	int32_t ___mode_4;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::readAdler
	int32_t ___readAdler_5;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::neededBits
	int32_t ___neededBits_6;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::repLength
	int32_t ___repLength_7;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::repDist
	int32_t ___repDist_8;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::uncomprLen
	int32_t ___uncomprLen_9;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::isLastBlock
	bool ___isLastBlock_10;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::totalOut
	int64_t ___totalOut_11;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::totalIn
	int64_t ___totalIn_12;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::noHeader
	bool ___noHeader_13;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator ICSharpCode.SharpZipLib.Zip.Compression.Inflater::input
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5 * ___input_14;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow ICSharpCode.SharpZipLib.Zip.Compression.Inflater::outputWindow
	OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581 * ___outputWindow_15;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader ICSharpCode.SharpZipLib.Zip.Compression.Inflater::dynHeader
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3 * ___dynHeader_16;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.Inflater::litlenTree
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * ___litlenTree_17;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.Inflater::distTree
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * ___distTree_18;
	// ICSharpCode.SharpZipLib.Checksums.Adler32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::adler
	Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033 * ___adler_19;

public:
	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_readAdler_5() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___readAdler_5)); }
	inline int32_t get_readAdler_5() const { return ___readAdler_5; }
	inline int32_t* get_address_of_readAdler_5() { return &___readAdler_5; }
	inline void set_readAdler_5(int32_t value)
	{
		___readAdler_5 = value;
	}

	inline static int32_t get_offset_of_neededBits_6() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___neededBits_6)); }
	inline int32_t get_neededBits_6() const { return ___neededBits_6; }
	inline int32_t* get_address_of_neededBits_6() { return &___neededBits_6; }
	inline void set_neededBits_6(int32_t value)
	{
		___neededBits_6 = value;
	}

	inline static int32_t get_offset_of_repLength_7() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___repLength_7)); }
	inline int32_t get_repLength_7() const { return ___repLength_7; }
	inline int32_t* get_address_of_repLength_7() { return &___repLength_7; }
	inline void set_repLength_7(int32_t value)
	{
		___repLength_7 = value;
	}

	inline static int32_t get_offset_of_repDist_8() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___repDist_8)); }
	inline int32_t get_repDist_8() const { return ___repDist_8; }
	inline int32_t* get_address_of_repDist_8() { return &___repDist_8; }
	inline void set_repDist_8(int32_t value)
	{
		___repDist_8 = value;
	}

	inline static int32_t get_offset_of_uncomprLen_9() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___uncomprLen_9)); }
	inline int32_t get_uncomprLen_9() const { return ___uncomprLen_9; }
	inline int32_t* get_address_of_uncomprLen_9() { return &___uncomprLen_9; }
	inline void set_uncomprLen_9(int32_t value)
	{
		___uncomprLen_9 = value;
	}

	inline static int32_t get_offset_of_isLastBlock_10() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___isLastBlock_10)); }
	inline bool get_isLastBlock_10() const { return ___isLastBlock_10; }
	inline bool* get_address_of_isLastBlock_10() { return &___isLastBlock_10; }
	inline void set_isLastBlock_10(bool value)
	{
		___isLastBlock_10 = value;
	}

	inline static int32_t get_offset_of_totalOut_11() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___totalOut_11)); }
	inline int64_t get_totalOut_11() const { return ___totalOut_11; }
	inline int64_t* get_address_of_totalOut_11() { return &___totalOut_11; }
	inline void set_totalOut_11(int64_t value)
	{
		___totalOut_11 = value;
	}

	inline static int32_t get_offset_of_totalIn_12() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___totalIn_12)); }
	inline int64_t get_totalIn_12() const { return ___totalIn_12; }
	inline int64_t* get_address_of_totalIn_12() { return &___totalIn_12; }
	inline void set_totalIn_12(int64_t value)
	{
		___totalIn_12 = value;
	}

	inline static int32_t get_offset_of_noHeader_13() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___noHeader_13)); }
	inline bool get_noHeader_13() const { return ___noHeader_13; }
	inline bool* get_address_of_noHeader_13() { return &___noHeader_13; }
	inline void set_noHeader_13(bool value)
	{
		___noHeader_13 = value;
	}

	inline static int32_t get_offset_of_input_14() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___input_14)); }
	inline StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5 * get_input_14() const { return ___input_14; }
	inline StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5 ** get_address_of_input_14() { return &___input_14; }
	inline void set_input_14(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5 * value)
	{
		___input_14 = value;
		Il2CppCodeGenWriteBarrier((&___input_14), value);
	}

	inline static int32_t get_offset_of_outputWindow_15() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___outputWindow_15)); }
	inline OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581 * get_outputWindow_15() const { return ___outputWindow_15; }
	inline OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581 ** get_address_of_outputWindow_15() { return &___outputWindow_15; }
	inline void set_outputWindow_15(OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581 * value)
	{
		___outputWindow_15 = value;
		Il2CppCodeGenWriteBarrier((&___outputWindow_15), value);
	}

	inline static int32_t get_offset_of_dynHeader_16() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___dynHeader_16)); }
	inline InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3 * get_dynHeader_16() const { return ___dynHeader_16; }
	inline InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3 ** get_address_of_dynHeader_16() { return &___dynHeader_16; }
	inline void set_dynHeader_16(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3 * value)
	{
		___dynHeader_16 = value;
		Il2CppCodeGenWriteBarrier((&___dynHeader_16), value);
	}

	inline static int32_t get_offset_of_litlenTree_17() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___litlenTree_17)); }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * get_litlenTree_17() const { return ___litlenTree_17; }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B ** get_address_of_litlenTree_17() { return &___litlenTree_17; }
	inline void set_litlenTree_17(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * value)
	{
		___litlenTree_17 = value;
		Il2CppCodeGenWriteBarrier((&___litlenTree_17), value);
	}

	inline static int32_t get_offset_of_distTree_18() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___distTree_18)); }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * get_distTree_18() const { return ___distTree_18; }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B ** get_address_of_distTree_18() { return &___distTree_18; }
	inline void set_distTree_18(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * value)
	{
		___distTree_18 = value;
		Il2CppCodeGenWriteBarrier((&___distTree_18), value);
	}

	inline static int32_t get_offset_of_adler_19() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB, ___adler_19)); }
	inline Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033 * get_adler_19() const { return ___adler_19; }
	inline Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033 ** get_address_of_adler_19() { return &___adler_19; }
	inline void set_adler_19(Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033 * value)
	{
		___adler_19 = value;
		Il2CppCodeGenWriteBarrier((&___adler_19), value);
	}
};

struct Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields
{
public:
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPLENS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CPLENS_0;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPLEXT
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CPLEXT_1;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPDIST
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CPDIST_2;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPDEXT
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CPDEXT_3;

public:
	inline static int32_t get_offset_of_CPLENS_0() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields, ___CPLENS_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CPLENS_0() const { return ___CPLENS_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CPLENS_0() { return &___CPLENS_0; }
	inline void set_CPLENS_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CPLENS_0 = value;
		Il2CppCodeGenWriteBarrier((&___CPLENS_0), value);
	}

	inline static int32_t get_offset_of_CPLEXT_1() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields, ___CPLEXT_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CPLEXT_1() const { return ___CPLEXT_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CPLEXT_1() { return &___CPLEXT_1; }
	inline void set_CPLEXT_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CPLEXT_1 = value;
		Il2CppCodeGenWriteBarrier((&___CPLEXT_1), value);
	}

	inline static int32_t get_offset_of_CPDIST_2() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields, ___CPDIST_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CPDIST_2() const { return ___CPDIST_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CPDIST_2() { return &___CPDIST_2; }
	inline void set_CPDIST_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CPDIST_2 = value;
		Il2CppCodeGenWriteBarrier((&___CPDIST_2), value);
	}

	inline static int32_t get_offset_of_CPDEXT_3() { return static_cast<int32_t>(offsetof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields, ___CPDEXT_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CPDEXT_3() const { return ___CPDEXT_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CPDEXT_3() { return &___CPDEXT_3; }
	inline void set_CPDEXT_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CPDEXT_3 = value;
		Il2CppCodeGenWriteBarrier((&___CPDEXT_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATER_T69AF9807C58C78BD7FCF1A0372F416961C1C73CB_H
#ifndef INFLATERDYNHEADER_T4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_H
#define INFLATERDYNHEADER_T4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader
struct  InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3  : public RuntimeObject
{
public:
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::blLens
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___blLens_3;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::litdistLens
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___litdistLens_4;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::blTree
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * ___blTree_5;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::mode
	int32_t ___mode_6;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::lnum
	int32_t ___lnum_7;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::dnum
	int32_t ___dnum_8;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::blnum
	int32_t ___blnum_9;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::num
	int32_t ___num_10;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::repSymbol
	int32_t ___repSymbol_11;
	// System.Byte ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::lastLen
	uint8_t ___lastLen_12;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::ptr
	int32_t ___ptr_13;

public:
	inline static int32_t get_offset_of_blLens_3() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___blLens_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_blLens_3() const { return ___blLens_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_blLens_3() { return &___blLens_3; }
	inline void set_blLens_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___blLens_3 = value;
		Il2CppCodeGenWriteBarrier((&___blLens_3), value);
	}

	inline static int32_t get_offset_of_litdistLens_4() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___litdistLens_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_litdistLens_4() const { return ___litdistLens_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_litdistLens_4() { return &___litdistLens_4; }
	inline void set_litdistLens_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___litdistLens_4 = value;
		Il2CppCodeGenWriteBarrier((&___litdistLens_4), value);
	}

	inline static int32_t get_offset_of_blTree_5() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___blTree_5)); }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * get_blTree_5() const { return ___blTree_5; }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B ** get_address_of_blTree_5() { return &___blTree_5; }
	inline void set_blTree_5(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * value)
	{
		___blTree_5 = value;
		Il2CppCodeGenWriteBarrier((&___blTree_5), value);
	}

	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_lnum_7() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___lnum_7)); }
	inline int32_t get_lnum_7() const { return ___lnum_7; }
	inline int32_t* get_address_of_lnum_7() { return &___lnum_7; }
	inline void set_lnum_7(int32_t value)
	{
		___lnum_7 = value;
	}

	inline static int32_t get_offset_of_dnum_8() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___dnum_8)); }
	inline int32_t get_dnum_8() const { return ___dnum_8; }
	inline int32_t* get_address_of_dnum_8() { return &___dnum_8; }
	inline void set_dnum_8(int32_t value)
	{
		___dnum_8 = value;
	}

	inline static int32_t get_offset_of_blnum_9() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___blnum_9)); }
	inline int32_t get_blnum_9() const { return ___blnum_9; }
	inline int32_t* get_address_of_blnum_9() { return &___blnum_9; }
	inline void set_blnum_9(int32_t value)
	{
		___blnum_9 = value;
	}

	inline static int32_t get_offset_of_num_10() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___num_10)); }
	inline int32_t get_num_10() const { return ___num_10; }
	inline int32_t* get_address_of_num_10() { return &___num_10; }
	inline void set_num_10(int32_t value)
	{
		___num_10 = value;
	}

	inline static int32_t get_offset_of_repSymbol_11() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___repSymbol_11)); }
	inline int32_t get_repSymbol_11() const { return ___repSymbol_11; }
	inline int32_t* get_address_of_repSymbol_11() { return &___repSymbol_11; }
	inline void set_repSymbol_11(int32_t value)
	{
		___repSymbol_11 = value;
	}

	inline static int32_t get_offset_of_lastLen_12() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___lastLen_12)); }
	inline uint8_t get_lastLen_12() const { return ___lastLen_12; }
	inline uint8_t* get_address_of_lastLen_12() { return &___lastLen_12; }
	inline void set_lastLen_12(uint8_t value)
	{
		___lastLen_12 = value;
	}

	inline static int32_t get_offset_of_ptr_13() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3, ___ptr_13)); }
	inline int32_t get_ptr_13() const { return ___ptr_13; }
	inline int32_t* get_address_of_ptr_13() { return &___ptr_13; }
	inline void set_ptr_13(int32_t value)
	{
		___ptr_13 = value;
	}
};

struct InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields
{
public:
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::repMin
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___repMin_0;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::repBits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___repBits_1;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BL_ORDER
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___BL_ORDER_2;

public:
	inline static int32_t get_offset_of_repMin_0() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields, ___repMin_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_repMin_0() const { return ___repMin_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_repMin_0() { return &___repMin_0; }
	inline void set_repMin_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___repMin_0 = value;
		Il2CppCodeGenWriteBarrier((&___repMin_0), value);
	}

	inline static int32_t get_offset_of_repBits_1() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields, ___repBits_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_repBits_1() const { return ___repBits_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_repBits_1() { return &___repBits_1; }
	inline void set_repBits_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___repBits_1 = value;
		Il2CppCodeGenWriteBarrier((&___repBits_1), value);
	}

	inline static int32_t get_offset_of_BL_ORDER_2() { return static_cast<int32_t>(offsetof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields, ___BL_ORDER_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_BL_ORDER_2() const { return ___BL_ORDER_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_BL_ORDER_2() { return &___BL_ORDER_2; }
	inline void set_BL_ORDER_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___BL_ORDER_2 = value;
		Il2CppCodeGenWriteBarrier((&___BL_ORDER_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATERDYNHEADER_T4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_H
#ifndef INFLATERHUFFMANTREE_T3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_H
#define INFLATERHUFFMANTREE_T3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct  InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B  : public RuntimeObject
{
public:
	// System.Int16[] ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::tree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___tree_0;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B, ___tree_0)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_tree_0() const { return ___tree_0; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}
};

struct InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields
{
public:
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::defLitLenTree
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * ___defLitLenTree_1;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::defDistTree
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * ___defDistTree_2;

public:
	inline static int32_t get_offset_of_defLitLenTree_1() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields, ___defLitLenTree_1)); }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * get_defLitLenTree_1() const { return ___defLitLenTree_1; }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B ** get_address_of_defLitLenTree_1() { return &___defLitLenTree_1; }
	inline void set_defLitLenTree_1(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * value)
	{
		___defLitLenTree_1 = value;
		Il2CppCodeGenWriteBarrier((&___defLitLenTree_1), value);
	}

	inline static int32_t get_offset_of_defDistTree_2() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields, ___defDistTree_2)); }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * get_defDistTree_2() const { return ___defDistTree_2; }
	inline InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B ** get_address_of_defDistTree_2() { return &___defDistTree_2; }
	inline void set_defDistTree_2(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B * value)
	{
		___defDistTree_2 = value;
		Il2CppCodeGenWriteBarrier((&___defDistTree_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATERHUFFMANTREE_T3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_H
#ifndef INFLATERINPUTBUFFER_T7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3_H
#define INFLATERINPUTBUFFER_T7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct  InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3  : public RuntimeObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::rawLength
	int32_t ___rawLength_0;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::rawData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___rawData_1;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::clearTextLength
	int32_t ___clearTextLength_2;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::clearText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___clearText_3;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::available
	int32_t ___available_4;
	// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::cryptoTransform
	RuntimeObject* ___cryptoTransform_5;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::inputStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___inputStream_6;

public:
	inline static int32_t get_offset_of_rawLength_0() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___rawLength_0)); }
	inline int32_t get_rawLength_0() const { return ___rawLength_0; }
	inline int32_t* get_address_of_rawLength_0() { return &___rawLength_0; }
	inline void set_rawLength_0(int32_t value)
	{
		___rawLength_0 = value;
	}

	inline static int32_t get_offset_of_rawData_1() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___rawData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_rawData_1() const { return ___rawData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_rawData_1() { return &___rawData_1; }
	inline void set_rawData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___rawData_1 = value;
		Il2CppCodeGenWriteBarrier((&___rawData_1), value);
	}

	inline static int32_t get_offset_of_clearTextLength_2() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___clearTextLength_2)); }
	inline int32_t get_clearTextLength_2() const { return ___clearTextLength_2; }
	inline int32_t* get_address_of_clearTextLength_2() { return &___clearTextLength_2; }
	inline void set_clearTextLength_2(int32_t value)
	{
		___clearTextLength_2 = value;
	}

	inline static int32_t get_offset_of_clearText_3() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___clearText_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_clearText_3() const { return ___clearText_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_clearText_3() { return &___clearText_3; }
	inline void set_clearText_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___clearText_3 = value;
		Il2CppCodeGenWriteBarrier((&___clearText_3), value);
	}

	inline static int32_t get_offset_of_available_4() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___available_4)); }
	inline int32_t get_available_4() const { return ___available_4; }
	inline int32_t* get_address_of_available_4() { return &___available_4; }
	inline void set_available_4(int32_t value)
	{
		___available_4 = value;
	}

	inline static int32_t get_offset_of_cryptoTransform_5() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___cryptoTransform_5)); }
	inline RuntimeObject* get_cryptoTransform_5() const { return ___cryptoTransform_5; }
	inline RuntimeObject** get_address_of_cryptoTransform_5() { return &___cryptoTransform_5; }
	inline void set_cryptoTransform_5(RuntimeObject* value)
	{
		___cryptoTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoTransform_5), value);
	}

	inline static int32_t get_offset_of_inputStream_6() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3, ___inputStream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_inputStream_6() const { return ___inputStream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_inputStream_6() { return &___inputStream_6; }
	inline void set_inputStream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___inputStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___inputStream_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATERINPUTBUFFER_T7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3_H
#ifndef OUTPUTWINDOW_T7CB804720F02D63ECB8018477FF479C596859581_H
#define OUTPUTWINDOW_T7CB804720F02D63ECB8018477FF479C596859581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct  OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581  : public RuntimeObject
{
public:
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::window
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window_0;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::windowEnd
	int32_t ___windowEnd_1;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::windowFilled
	int32_t ___windowFilled_2;

public:
	inline static int32_t get_offset_of_window_0() { return static_cast<int32_t>(offsetof(OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581, ___window_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window_0() const { return ___window_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window_0() { return &___window_0; }
	inline void set_window_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window_0 = value;
		Il2CppCodeGenWriteBarrier((&___window_0), value);
	}

	inline static int32_t get_offset_of_windowEnd_1() { return static_cast<int32_t>(offsetof(OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581, ___windowEnd_1)); }
	inline int32_t get_windowEnd_1() const { return ___windowEnd_1; }
	inline int32_t* get_address_of_windowEnd_1() { return &___windowEnd_1; }
	inline void set_windowEnd_1(int32_t value)
	{
		___windowEnd_1 = value;
	}

	inline static int32_t get_offset_of_windowFilled_2() { return static_cast<int32_t>(offsetof(OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581, ___windowFilled_2)); }
	inline int32_t get_windowFilled_2() const { return ___windowFilled_2; }
	inline int32_t* get_address_of_windowFilled_2() { return &___windowFilled_2; }
	inline void set_windowFilled_2(int32_t value)
	{
		___windowFilled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTWINDOW_T7CB804720F02D63ECB8018477FF479C596859581_H
#ifndef STREAMMANIPULATOR_TBAD267162B2327BE4350A4407E38A40C613CFAB5_H
#define STREAMMANIPULATOR_TBAD267162B2327BE4350A4407E38A40C613CFAB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct  StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5  : public RuntimeObject
{
public:
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::window_
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window__0;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::windowStart_
	int32_t ___windowStart__1;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::windowEnd_
	int32_t ___windowEnd__2;
	// System.UInt32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::buffer_
	uint32_t ___buffer__3;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::bitsInBuffer_
	int32_t ___bitsInBuffer__4;

public:
	inline static int32_t get_offset_of_window__0() { return static_cast<int32_t>(offsetof(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5, ___window__0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window__0() const { return ___window__0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window__0() { return &___window__0; }
	inline void set_window__0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window__0 = value;
		Il2CppCodeGenWriteBarrier((&___window__0), value);
	}

	inline static int32_t get_offset_of_windowStart__1() { return static_cast<int32_t>(offsetof(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5, ___windowStart__1)); }
	inline int32_t get_windowStart__1() const { return ___windowStart__1; }
	inline int32_t* get_address_of_windowStart__1() { return &___windowStart__1; }
	inline void set_windowStart__1(int32_t value)
	{
		___windowStart__1 = value;
	}

	inline static int32_t get_offset_of_windowEnd__2() { return static_cast<int32_t>(offsetof(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5, ___windowEnd__2)); }
	inline int32_t get_windowEnd__2() const { return ___windowEnd__2; }
	inline int32_t* get_address_of_windowEnd__2() { return &___windowEnd__2; }
	inline void set_windowEnd__2(int32_t value)
	{
		___windowEnd__2 = value;
	}

	inline static int32_t get_offset_of_buffer__3() { return static_cast<int32_t>(offsetof(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5, ___buffer__3)); }
	inline uint32_t get_buffer__3() const { return ___buffer__3; }
	inline uint32_t* get_address_of_buffer__3() { return &___buffer__3; }
	inline void set_buffer__3(uint32_t value)
	{
		___buffer__3 = value;
	}

	inline static int32_t get_offset_of_bitsInBuffer__4() { return static_cast<int32_t>(offsetof(StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5, ___bitsInBuffer__4)); }
	inline int32_t get_bitsInBuffer__4() const { return ___bitsInBuffer__4; }
	inline int32_t* get_address_of_bitsInBuffer__4() { return &___bitsInBuffer__4; }
	inline void set_bitsInBuffer__4(int32_t value)
	{
		___bitsInBuffer__4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMMANIPULATOR_TBAD267162B2327BE4350A4407E38A40C613CFAB5_H
#ifndef ZIPCONSTANTS_T346D2EB237FE5E5068BED97F246531E08A363863_H
#define ZIPCONSTANTS_T346D2EB237FE5E5068BED97F246531E08A363863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipConstants
struct  ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863  : public RuntimeObject
{
public:

public:
};

struct ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863_StaticFields
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipConstants::defaultCodePage
	int32_t ___defaultCodePage_0;

public:
	inline static int32_t get_offset_of_defaultCodePage_0() { return static_cast<int32_t>(offsetof(ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863_StaticFields, ___defaultCodePage_0)); }
	inline int32_t get_defaultCodePage_0() const { return ___defaultCodePage_0; }
	inline int32_t* get_address_of_defaultCodePage_0() { return &___defaultCodePage_0; }
	inline void set_defaultCodePage_0(int32_t value)
	{
		___defaultCodePage_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPCONSTANTS_T346D2EB237FE5E5068BED97F246531E08A363863_H
#ifndef ZIPEXTRADATA_T2BE422C50624DC89A6D09B5207221310DC3C82A0_H
#define ZIPEXTRADATA_T2BE422C50624DC89A6D09B5207221310DC3C82A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipExtraData
struct  ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0  : public RuntimeObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::_index
	int32_t ____index_0;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::_readValueStart
	int32_t ____readValueStart_1;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::_readValueLength
	int32_t ____readValueLength_2;
	// System.IO.MemoryStream ICSharpCode.SharpZipLib.Zip.ZipExtraData::_newEntry
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ____newEntry_3;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipExtraData::_data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____data_4;

public:
	inline static int32_t get_offset_of__index_0() { return static_cast<int32_t>(offsetof(ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0, ____index_0)); }
	inline int32_t get__index_0() const { return ____index_0; }
	inline int32_t* get_address_of__index_0() { return &____index_0; }
	inline void set__index_0(int32_t value)
	{
		____index_0 = value;
	}

	inline static int32_t get_offset_of__readValueStart_1() { return static_cast<int32_t>(offsetof(ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0, ____readValueStart_1)); }
	inline int32_t get__readValueStart_1() const { return ____readValueStart_1; }
	inline int32_t* get_address_of__readValueStart_1() { return &____readValueStart_1; }
	inline void set__readValueStart_1(int32_t value)
	{
		____readValueStart_1 = value;
	}

	inline static int32_t get_offset_of__readValueLength_2() { return static_cast<int32_t>(offsetof(ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0, ____readValueLength_2)); }
	inline int32_t get__readValueLength_2() const { return ____readValueLength_2; }
	inline int32_t* get_address_of__readValueLength_2() { return &____readValueLength_2; }
	inline void set__readValueLength_2(int32_t value)
	{
		____readValueLength_2 = value;
	}

	inline static int32_t get_offset_of__newEntry_3() { return static_cast<int32_t>(offsetof(ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0, ____newEntry_3)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get__newEntry_3() const { return ____newEntry_3; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of__newEntry_3() { return &____newEntry_3; }
	inline void set__newEntry_3(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		____newEntry_3 = value;
		Il2CppCodeGenWriteBarrier((&____newEntry_3), value);
	}

	inline static int32_t get_offset_of__data_4() { return static_cast<int32_t>(offsetof(ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0, ____data_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__data_4() const { return ____data_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__data_4() { return &____data_4; }
	inline void set__data_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____data_4 = value;
		Il2CppCodeGenWriteBarrier((&____data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPEXTRADATA_T2BE422C50624DC89A6D09B5207221310DC3C82A0_H
#ifndef ZIPENTRYENUMERATOR_TD1C0F596AD8074627C711424B3F64891F3B2037F_H
#define ZIPENTRYENUMERATOR_TD1C0F596AD8074627C711424B3F64891F3B2037F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile_ZipEntryEnumerator
struct  ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F  : public RuntimeObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipEntry[] ICSharpCode.SharpZipLib.Zip.ZipFile_ZipEntryEnumerator::array
	ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* ___array_0;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile_ZipEntryEnumerator::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_array_0() { return static_cast<int32_t>(offsetof(ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F, ___array_0)); }
	inline ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* get_array_0() const { return ___array_0; }
	inline ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917** get_address_of_array_0() { return &___array_0; }
	inline void set_array_0(ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* value)
	{
		___array_0 = value;
		Il2CppCodeGenWriteBarrier((&___array_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPENTRYENUMERATOR_TD1C0F596AD8074627C711424B3F64891F3B2037F_H
#ifndef ZIPNAMETRANSFORM_T8016B5EE6B0807E685F47F38135D7F000D18BAEF_H
#define ZIPNAMETRANSFORM_T8016B5EE6B0807E685F47F38135D7F000D18BAEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipNameTransform
struct  ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF  : public RuntimeObject
{
public:

public:
};

struct ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields
{
public:
	// System.Char[] ICSharpCode.SharpZipLib.Zip.ZipNameTransform::InvalidEntryChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___InvalidEntryChars_0;
	// System.Char[] ICSharpCode.SharpZipLib.Zip.ZipNameTransform::InvalidEntryCharsRelaxed
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___InvalidEntryCharsRelaxed_1;

public:
	inline static int32_t get_offset_of_InvalidEntryChars_0() { return static_cast<int32_t>(offsetof(ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields, ___InvalidEntryChars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_InvalidEntryChars_0() const { return ___InvalidEntryChars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_InvalidEntryChars_0() { return &___InvalidEntryChars_0; }
	inline void set_InvalidEntryChars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___InvalidEntryChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidEntryChars_0), value);
	}

	inline static int32_t get_offset_of_InvalidEntryCharsRelaxed_1() { return static_cast<int32_t>(offsetof(ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields, ___InvalidEntryCharsRelaxed_1)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_InvalidEntryCharsRelaxed_1() const { return ___InvalidEntryCharsRelaxed_1; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_InvalidEntryCharsRelaxed_1() { return &___InvalidEntryCharsRelaxed_1; }
	inline void set_InvalidEntryCharsRelaxed_1(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___InvalidEntryCharsRelaxed_1 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidEntryCharsRelaxed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPNAMETRANSFORM_T8016B5EE6B0807E685F47F38135D7F000D18BAEF_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANALYTICSSESSIONINFO_TE075F764A74D2B095CFD57F3B179397F504B7D8C_H
#define ANALYTICSSESSIONINFO_TE075F764A74D2B095CFD57F3B179397F504B7D8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo
struct  AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C  : public RuntimeObject
{
public:

public:
};

struct AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields
{
public:
	// UnityEngine.Analytics.AnalyticsSessionInfo_SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F * ___sessionStateChanged_0;

public:
	inline static int32_t get_offset_of_sessionStateChanged_0() { return static_cast<int32_t>(offsetof(AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields, ___sessionStateChanged_0)); }
	inline SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F * get_sessionStateChanged_0() const { return ___sessionStateChanged_0; }
	inline SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F ** get_address_of_sessionStateChanged_0() { return &___sessionStateChanged_0; }
	inline void set_sessionStateChanged_0(SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F * value)
	{
		___sessionStateChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionStateChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONINFO_TE075F764A74D2B095CFD57F3B179397F504B7D8C_H
#ifndef CONTINUOUSEVENT_TBAB6336255F3FC327CBA03CE368CD4D8D027107A_H
#define CONTINUOUSEVENT_TBAB6336255F3FC327CBA03CE368CD4D8D027107A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ContinuousEvent
struct  ContinuousEvent_tBAB6336255F3FC327CBA03CE368CD4D8D027107A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTINUOUSEVENT_TBAB6336255F3FC327CBA03CE368CD4D8D027107A_H
#ifndef EXECUTEEVENTS_T622B95FF46A568C8205B76C1D4111049FC265985_H
#define EXECUTEEVENTS_T622B95FF46A568C8205B76C1D4111049FC265985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents
struct  ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985  : public RuntimeObject
{
public:

public:
};

struct ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerEnterHandler
	EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * ___s_PointerEnterHandler_0;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerExitHandler
	EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * ___s_PointerExitHandler_1;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerDownHandler
	EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * ___s_PointerDownHandler_2;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerUpHandler
	EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * ___s_PointerUpHandler_3;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerClickHandler
	EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * ___s_PointerClickHandler_4;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_InitializePotentialDragHandler
	EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * ___s_InitializePotentialDragHandler_5;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_BeginDragHandler
	EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * ___s_BeginDragHandler_6;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_DragHandler
	EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * ___s_DragHandler_7;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_EndDragHandler
	EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * ___s_EndDragHandler_8;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::s_DropHandler
	EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * ___s_DropHandler_9;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::s_ScrollHandler
	EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * ___s_ScrollHandler_10;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::s_UpdateSelectedHandler
	EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * ___s_UpdateSelectedHandler_11;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::s_SelectHandler
	EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * ___s_SelectHandler_12;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::s_DeselectHandler
	EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * ___s_DeselectHandler_13;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::s_MoveHandler
	EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * ___s_MoveHandler_14;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::s_SubmitHandler
	EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * ___s_SubmitHandler_15;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::s_CancelHandler
	EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * ___s_CancelHandler_16;
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>> UnityEngine.EventSystems.ExecuteEvents::s_HandlerListPool
	ObjectPool_1_tA46EC5C3029914B5C6BC43C2337CBB8067BB19FC * ___s_HandlerListPool_17;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.EventSystems.ExecuteEvents::s_InternalTransformList
	List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * ___s_InternalTransformList_18;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache0
	EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * ___U3CU3Ef__mgU24cache0_19;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache1
	EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * ___U3CU3Ef__mgU24cache1_20;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache2
	EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * ___U3CU3Ef__mgU24cache2_21;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache3
	EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * ___U3CU3Ef__mgU24cache3_22;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache4
	EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * ___U3CU3Ef__mgU24cache4_23;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache5
	EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * ___U3CU3Ef__mgU24cache5_24;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache6
	EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * ___U3CU3Ef__mgU24cache6_25;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache7
	EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * ___U3CU3Ef__mgU24cache7_26;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache8
	EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * ___U3CU3Ef__mgU24cache8_27;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache9
	EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * ___U3CU3Ef__mgU24cache9_28;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheA
	EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * ___U3CU3Ef__mgU24cacheA_29;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheB
	EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * ___U3CU3Ef__mgU24cacheB_30;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheC
	EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * ___U3CU3Ef__mgU24cacheC_31;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheD
	EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * ___U3CU3Ef__mgU24cacheD_32;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheE
	EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * ___U3CU3Ef__mgU24cacheE_33;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheF
	EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * ___U3CU3Ef__mgU24cacheF_34;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache10
	EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * ___U3CU3Ef__mgU24cache10_35;

public:
	inline static int32_t get_offset_of_s_PointerEnterHandler_0() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerEnterHandler_0)); }
	inline EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * get_s_PointerEnterHandler_0() const { return ___s_PointerEnterHandler_0; }
	inline EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 ** get_address_of_s_PointerEnterHandler_0() { return &___s_PointerEnterHandler_0; }
	inline void set_s_PointerEnterHandler_0(EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * value)
	{
		___s_PointerEnterHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerEnterHandler_0), value);
	}

	inline static int32_t get_offset_of_s_PointerExitHandler_1() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerExitHandler_1)); }
	inline EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * get_s_PointerExitHandler_1() const { return ___s_PointerExitHandler_1; }
	inline EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA ** get_address_of_s_PointerExitHandler_1() { return &___s_PointerExitHandler_1; }
	inline void set_s_PointerExitHandler_1(EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * value)
	{
		___s_PointerExitHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerExitHandler_1), value);
	}

	inline static int32_t get_offset_of_s_PointerDownHandler_2() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerDownHandler_2)); }
	inline EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * get_s_PointerDownHandler_2() const { return ___s_PointerDownHandler_2; }
	inline EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E ** get_address_of_s_PointerDownHandler_2() { return &___s_PointerDownHandler_2; }
	inline void set_s_PointerDownHandler_2(EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * value)
	{
		___s_PointerDownHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerDownHandler_2), value);
	}

	inline static int32_t get_offset_of_s_PointerUpHandler_3() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerUpHandler_3)); }
	inline EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * get_s_PointerUpHandler_3() const { return ___s_PointerUpHandler_3; }
	inline EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 ** get_address_of_s_PointerUpHandler_3() { return &___s_PointerUpHandler_3; }
	inline void set_s_PointerUpHandler_3(EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * value)
	{
		___s_PointerUpHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerUpHandler_3), value);
	}

	inline static int32_t get_offset_of_s_PointerClickHandler_4() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerClickHandler_4)); }
	inline EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * get_s_PointerClickHandler_4() const { return ___s_PointerClickHandler_4; }
	inline EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E ** get_address_of_s_PointerClickHandler_4() { return &___s_PointerClickHandler_4; }
	inline void set_s_PointerClickHandler_4(EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * value)
	{
		___s_PointerClickHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerClickHandler_4), value);
	}

	inline static int32_t get_offset_of_s_InitializePotentialDragHandler_5() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_InitializePotentialDragHandler_5)); }
	inline EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * get_s_InitializePotentialDragHandler_5() const { return ___s_InitializePotentialDragHandler_5; }
	inline EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 ** get_address_of_s_InitializePotentialDragHandler_5() { return &___s_InitializePotentialDragHandler_5; }
	inline void set_s_InitializePotentialDragHandler_5(EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * value)
	{
		___s_InitializePotentialDragHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_InitializePotentialDragHandler_5), value);
	}

	inline static int32_t get_offset_of_s_BeginDragHandler_6() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_BeginDragHandler_6)); }
	inline EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * get_s_BeginDragHandler_6() const { return ___s_BeginDragHandler_6; }
	inline EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 ** get_address_of_s_BeginDragHandler_6() { return &___s_BeginDragHandler_6; }
	inline void set_s_BeginDragHandler_6(EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * value)
	{
		___s_BeginDragHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_BeginDragHandler_6), value);
	}

	inline static int32_t get_offset_of_s_DragHandler_7() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_DragHandler_7)); }
	inline EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * get_s_DragHandler_7() const { return ___s_DragHandler_7; }
	inline EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 ** get_address_of_s_DragHandler_7() { return &___s_DragHandler_7; }
	inline void set_s_DragHandler_7(EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * value)
	{
		___s_DragHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_DragHandler_7), value);
	}

	inline static int32_t get_offset_of_s_EndDragHandler_8() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_EndDragHandler_8)); }
	inline EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * get_s_EndDragHandler_8() const { return ___s_EndDragHandler_8; }
	inline EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 ** get_address_of_s_EndDragHandler_8() { return &___s_EndDragHandler_8; }
	inline void set_s_EndDragHandler_8(EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * value)
	{
		___s_EndDragHandler_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EndDragHandler_8), value);
	}

	inline static int32_t get_offset_of_s_DropHandler_9() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_DropHandler_9)); }
	inline EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * get_s_DropHandler_9() const { return ___s_DropHandler_9; }
	inline EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 ** get_address_of_s_DropHandler_9() { return &___s_DropHandler_9; }
	inline void set_s_DropHandler_9(EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * value)
	{
		___s_DropHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_DropHandler_9), value);
	}

	inline static int32_t get_offset_of_s_ScrollHandler_10() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_ScrollHandler_10)); }
	inline EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * get_s_ScrollHandler_10() const { return ___s_ScrollHandler_10; }
	inline EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A ** get_address_of_s_ScrollHandler_10() { return &___s_ScrollHandler_10; }
	inline void set_s_ScrollHandler_10(EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * value)
	{
		___s_ScrollHandler_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_ScrollHandler_10), value);
	}

	inline static int32_t get_offset_of_s_UpdateSelectedHandler_11() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_UpdateSelectedHandler_11)); }
	inline EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * get_s_UpdateSelectedHandler_11() const { return ___s_UpdateSelectedHandler_11; }
	inline EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 ** get_address_of_s_UpdateSelectedHandler_11() { return &___s_UpdateSelectedHandler_11; }
	inline void set_s_UpdateSelectedHandler_11(EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * value)
	{
		___s_UpdateSelectedHandler_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_UpdateSelectedHandler_11), value);
	}

	inline static int32_t get_offset_of_s_SelectHandler_12() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_SelectHandler_12)); }
	inline EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * get_s_SelectHandler_12() const { return ___s_SelectHandler_12; }
	inline EventFunction_1_t7521247C87411935E8A2CA38683533083459473F ** get_address_of_s_SelectHandler_12() { return &___s_SelectHandler_12; }
	inline void set_s_SelectHandler_12(EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * value)
	{
		___s_SelectHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_SelectHandler_12), value);
	}

	inline static int32_t get_offset_of_s_DeselectHandler_13() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_DeselectHandler_13)); }
	inline EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * get_s_DeselectHandler_13() const { return ___s_DeselectHandler_13; }
	inline EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 ** get_address_of_s_DeselectHandler_13() { return &___s_DeselectHandler_13; }
	inline void set_s_DeselectHandler_13(EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * value)
	{
		___s_DeselectHandler_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_DeselectHandler_13), value);
	}

	inline static int32_t get_offset_of_s_MoveHandler_14() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_MoveHandler_14)); }
	inline EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * get_s_MoveHandler_14() const { return ___s_MoveHandler_14; }
	inline EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB ** get_address_of_s_MoveHandler_14() { return &___s_MoveHandler_14; }
	inline void set_s_MoveHandler_14(EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * value)
	{
		___s_MoveHandler_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_MoveHandler_14), value);
	}

	inline static int32_t get_offset_of_s_SubmitHandler_15() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_SubmitHandler_15)); }
	inline EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * get_s_SubmitHandler_15() const { return ___s_SubmitHandler_15; }
	inline EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B ** get_address_of_s_SubmitHandler_15() { return &___s_SubmitHandler_15; }
	inline void set_s_SubmitHandler_15(EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * value)
	{
		___s_SubmitHandler_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_SubmitHandler_15), value);
	}

	inline static int32_t get_offset_of_s_CancelHandler_16() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_CancelHandler_16)); }
	inline EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * get_s_CancelHandler_16() const { return ___s_CancelHandler_16; }
	inline EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 ** get_address_of_s_CancelHandler_16() { return &___s_CancelHandler_16; }
	inline void set_s_CancelHandler_16(EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * value)
	{
		___s_CancelHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_CancelHandler_16), value);
	}

	inline static int32_t get_offset_of_s_HandlerListPool_17() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_HandlerListPool_17)); }
	inline ObjectPool_1_tA46EC5C3029914B5C6BC43C2337CBB8067BB19FC * get_s_HandlerListPool_17() const { return ___s_HandlerListPool_17; }
	inline ObjectPool_1_tA46EC5C3029914B5C6BC43C2337CBB8067BB19FC ** get_address_of_s_HandlerListPool_17() { return &___s_HandlerListPool_17; }
	inline void set_s_HandlerListPool_17(ObjectPool_1_tA46EC5C3029914B5C6BC43C2337CBB8067BB19FC * value)
	{
		___s_HandlerListPool_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_HandlerListPool_17), value);
	}

	inline static int32_t get_offset_of_s_InternalTransformList_18() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_InternalTransformList_18)); }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * get_s_InternalTransformList_18() const { return ___s_InternalTransformList_18; }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D ** get_address_of_s_InternalTransformList_18() { return &___s_InternalTransformList_18; }
	inline void set_s_InternalTransformList_18(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * value)
	{
		___s_InternalTransformList_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalTransformList_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_19() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache0_19)); }
	inline EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * get_U3CU3Ef__mgU24cache0_19() const { return ___U3CU3Ef__mgU24cache0_19; }
	inline EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 ** get_address_of_U3CU3Ef__mgU24cache0_19() { return &___U3CU3Ef__mgU24cache0_19; }
	inline void set_U3CU3Ef__mgU24cache0_19(EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * value)
	{
		___U3CU3Ef__mgU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_20() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache1_20)); }
	inline EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * get_U3CU3Ef__mgU24cache1_20() const { return ___U3CU3Ef__mgU24cache1_20; }
	inline EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA ** get_address_of_U3CU3Ef__mgU24cache1_20() { return &___U3CU3Ef__mgU24cache1_20; }
	inline void set_U3CU3Ef__mgU24cache1_20(EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * value)
	{
		___U3CU3Ef__mgU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_21() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache2_21)); }
	inline EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * get_U3CU3Ef__mgU24cache2_21() const { return ___U3CU3Ef__mgU24cache2_21; }
	inline EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E ** get_address_of_U3CU3Ef__mgU24cache2_21() { return &___U3CU3Ef__mgU24cache2_21; }
	inline void set_U3CU3Ef__mgU24cache2_21(EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * value)
	{
		___U3CU3Ef__mgU24cache2_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_22() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache3_22)); }
	inline EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * get_U3CU3Ef__mgU24cache3_22() const { return ___U3CU3Ef__mgU24cache3_22; }
	inline EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 ** get_address_of_U3CU3Ef__mgU24cache3_22() { return &___U3CU3Ef__mgU24cache3_22; }
	inline void set_U3CU3Ef__mgU24cache3_22(EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * value)
	{
		___U3CU3Ef__mgU24cache3_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_23() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache4_23)); }
	inline EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * get_U3CU3Ef__mgU24cache4_23() const { return ___U3CU3Ef__mgU24cache4_23; }
	inline EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E ** get_address_of_U3CU3Ef__mgU24cache4_23() { return &___U3CU3Ef__mgU24cache4_23; }
	inline void set_U3CU3Ef__mgU24cache4_23(EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * value)
	{
		___U3CU3Ef__mgU24cache4_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_24() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache5_24)); }
	inline EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * get_U3CU3Ef__mgU24cache5_24() const { return ___U3CU3Ef__mgU24cache5_24; }
	inline EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 ** get_address_of_U3CU3Ef__mgU24cache5_24() { return &___U3CU3Ef__mgU24cache5_24; }
	inline void set_U3CU3Ef__mgU24cache5_24(EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * value)
	{
		___U3CU3Ef__mgU24cache5_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_25() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache6_25)); }
	inline EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * get_U3CU3Ef__mgU24cache6_25() const { return ___U3CU3Ef__mgU24cache6_25; }
	inline EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 ** get_address_of_U3CU3Ef__mgU24cache6_25() { return &___U3CU3Ef__mgU24cache6_25; }
	inline void set_U3CU3Ef__mgU24cache6_25(EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * value)
	{
		___U3CU3Ef__mgU24cache6_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_26() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache7_26)); }
	inline EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * get_U3CU3Ef__mgU24cache7_26() const { return ___U3CU3Ef__mgU24cache7_26; }
	inline EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 ** get_address_of_U3CU3Ef__mgU24cache7_26() { return &___U3CU3Ef__mgU24cache7_26; }
	inline void set_U3CU3Ef__mgU24cache7_26(EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * value)
	{
		___U3CU3Ef__mgU24cache7_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_27() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache8_27)); }
	inline EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * get_U3CU3Ef__mgU24cache8_27() const { return ___U3CU3Ef__mgU24cache8_27; }
	inline EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 ** get_address_of_U3CU3Ef__mgU24cache8_27() { return &___U3CU3Ef__mgU24cache8_27; }
	inline void set_U3CU3Ef__mgU24cache8_27(EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * value)
	{
		___U3CU3Ef__mgU24cache8_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_28() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache9_28)); }
	inline EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * get_U3CU3Ef__mgU24cache9_28() const { return ___U3CU3Ef__mgU24cache9_28; }
	inline EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 ** get_address_of_U3CU3Ef__mgU24cache9_28() { return &___U3CU3Ef__mgU24cache9_28; }
	inline void set_U3CU3Ef__mgU24cache9_28(EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * value)
	{
		___U3CU3Ef__mgU24cache9_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_29() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheA_29)); }
	inline EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * get_U3CU3Ef__mgU24cacheA_29() const { return ___U3CU3Ef__mgU24cacheA_29; }
	inline EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A ** get_address_of_U3CU3Ef__mgU24cacheA_29() { return &___U3CU3Ef__mgU24cacheA_29; }
	inline void set_U3CU3Ef__mgU24cacheA_29(EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * value)
	{
		___U3CU3Ef__mgU24cacheA_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_30() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheB_30)); }
	inline EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * get_U3CU3Ef__mgU24cacheB_30() const { return ___U3CU3Ef__mgU24cacheB_30; }
	inline EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 ** get_address_of_U3CU3Ef__mgU24cacheB_30() { return &___U3CU3Ef__mgU24cacheB_30; }
	inline void set_U3CU3Ef__mgU24cacheB_30(EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * value)
	{
		___U3CU3Ef__mgU24cacheB_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_31() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheC_31)); }
	inline EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * get_U3CU3Ef__mgU24cacheC_31() const { return ___U3CU3Ef__mgU24cacheC_31; }
	inline EventFunction_1_t7521247C87411935E8A2CA38683533083459473F ** get_address_of_U3CU3Ef__mgU24cacheC_31() { return &___U3CU3Ef__mgU24cacheC_31; }
	inline void set_U3CU3Ef__mgU24cacheC_31(EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * value)
	{
		___U3CU3Ef__mgU24cacheC_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_32() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheD_32)); }
	inline EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * get_U3CU3Ef__mgU24cacheD_32() const { return ___U3CU3Ef__mgU24cacheD_32; }
	inline EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 ** get_address_of_U3CU3Ef__mgU24cacheD_32() { return &___U3CU3Ef__mgU24cacheD_32; }
	inline void set_U3CU3Ef__mgU24cacheD_32(EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * value)
	{
		___U3CU3Ef__mgU24cacheD_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_33() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheE_33)); }
	inline EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * get_U3CU3Ef__mgU24cacheE_33() const { return ___U3CU3Ef__mgU24cacheE_33; }
	inline EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB ** get_address_of_U3CU3Ef__mgU24cacheE_33() { return &___U3CU3Ef__mgU24cacheE_33; }
	inline void set_U3CU3Ef__mgU24cacheE_33(EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * value)
	{
		___U3CU3Ef__mgU24cacheE_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_34() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheF_34)); }
	inline EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * get_U3CU3Ef__mgU24cacheF_34() const { return ___U3CU3Ef__mgU24cacheF_34; }
	inline EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B ** get_address_of_U3CU3Ef__mgU24cacheF_34() { return &___U3CU3Ef__mgU24cacheF_34; }
	inline void set_U3CU3Ef__mgU24cacheF_34(EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * value)
	{
		___U3CU3Ef__mgU24cacheF_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_35() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache10_35)); }
	inline EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * get_U3CU3Ef__mgU24cache10_35() const { return ___U3CU3Ef__mgU24cache10_35; }
	inline EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 ** get_address_of_U3CU3Ef__mgU24cache10_35() { return &___U3CU3Ef__mgU24cache10_35; }
	inline void set_U3CU3Ef__mgU24cache10_35(EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * value)
	{
		___U3CU3Ef__mgU24cache10_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTEEVENTS_T622B95FF46A568C8205B76C1D4111049FC265985_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef UNITYWEBREQUESTTEXTURE_T2F2C62D3509DDAF6615855FA3428F527BF166F09_H
#define UNITYWEBREQUESTTEXTURE_T2F2C62D3509DDAF6615855FA3428F527BF166F09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequestTexture
struct  UnityWebRequestTexture_t2F2C62D3509DDAF6615855FA3428F527BF166F09  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWEBREQUESTTEXTURE_T2F2C62D3509DDAF6615855FA3428F527BF166F09_H
#ifndef RECTTRANSFORMUTILITY_T9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_H
#define RECTTRANSFORMUTILITY_T9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransformUtility
struct  RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA  : public RuntimeObject
{
public:

public:
};

struct RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields
{
public:
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Corners_0;

public:
	inline static int32_t get_offset_of_s_Corners_0() { return static_cast<int32_t>(offsetof(RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields, ___s_Corners_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Corners_0() const { return ___s_Corners_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Corners_0() { return &___s_Corners_0; }
	inline void set_s_Corners_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Corners_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Corners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMUTILITY_T9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_H
#ifndef REMOTESETTINGS_T3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_H
#define REMOTESETTINGS_T3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings
struct  RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED  : public RuntimeObject
{
public:

public:
};

struct RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields
{
public:
	// UnityEngine.RemoteSettings_UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F * ___Updated_0;
	// System.Action UnityEngine.RemoteSettings::BeforeFetchFromServer
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___BeforeFetchFromServer_1;
	// System.Action`3<System.Boolean,System.Boolean,System.Int32> UnityEngine.RemoteSettings::Completed
	Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E * ___Completed_2;

public:
	inline static int32_t get_offset_of_Updated_0() { return static_cast<int32_t>(offsetof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields, ___Updated_0)); }
	inline UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F * get_Updated_0() const { return ___Updated_0; }
	inline UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F ** get_address_of_Updated_0() { return &___Updated_0; }
	inline void set_Updated_0(UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F * value)
	{
		___Updated_0 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_0), value);
	}

	inline static int32_t get_offset_of_BeforeFetchFromServer_1() { return static_cast<int32_t>(offsetof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields, ___BeforeFetchFromServer_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_BeforeFetchFromServer_1() const { return ___BeforeFetchFromServer_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_BeforeFetchFromServer_1() { return &___BeforeFetchFromServer_1; }
	inline void set_BeforeFetchFromServer_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___BeforeFetchFromServer_1 = value;
		Il2CppCodeGenWriteBarrier((&___BeforeFetchFromServer_1), value);
	}

	inline static int32_t get_offset_of_Completed_2() { return static_cast<int32_t>(offsetof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields, ___Completed_2)); }
	inline Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E * get_Completed_2() const { return ___Completed_2; }
	inline Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E ** get_address_of_Completed_2() { return &___Completed_2; }
	inline void set_Completed_2(Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E * value)
	{
		___Completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESETTINGS_T3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_H
#ifndef UISYSTEMPROFILERAPI_TD33E558B2D0176096E5DB375956ACA9F03678F1B_H
#define UISYSTEMPROFILERAPI_TD33E558B2D0176096E5DB375956ACA9F03678F1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi
struct  UISystemProfilerApi_tD33E558B2D0176096E5DB375956ACA9F03678F1B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISYSTEMPROFILERAPI_TD33E558B2D0176096E5DB375956ACA9F03678F1B_H
#ifndef XRDEVICE_T392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_H
#define XRDEVICE_T392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.XRDevice
struct  XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A  : public RuntimeObject
{
public:

public:
};

struct XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_StaticFields
{
public:
	// System.Action`1<System.String> UnityEngine.XR.XRDevice::deviceLoaded
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___deviceLoaded_0;

public:
	inline static int32_t get_offset_of_deviceLoaded_0() { return static_cast<int32_t>(offsetof(XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_StaticFields, ___deviceLoaded_0)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_deviceLoaded_0() const { return ___deviceLoaded_0; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_deviceLoaded_0() { return &___deviceLoaded_0; }
	inline void set_deviceLoaded_0(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___deviceLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&___deviceLoaded_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRDEVICE_T392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_H
#ifndef XRSETTINGS_TB57FCBA5B804996700C097CC13B658E7BD43D874_H
#define XRSETTINGS_TB57FCBA5B804996700C097CC13B658E7BD43D874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.XRSettings
struct  XRSettings_tB57FCBA5B804996700C097CC13B658E7BD43D874  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRSETTINGS_TB57FCBA5B804996700C097CC13B658E7BD43D874_H
#ifndef __STATICARRAYINITTYPESIZEU3D1024_TA2012153EE60F5C9CA38878077580795956DBCD1_H
#define __STATICARRAYINITTYPESIZEU3D1024_TA2012153EE60F5C9CA38878077580795956DBCD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D1024
struct  __StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1__padding[1024];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D1024_TA2012153EE60F5C9CA38878077580795956DBCD1_H
#ifndef __STATICARRAYINITTYPESIZEU3D116_TDF00214C4AC7272026E840470B881841013B7244_H
#define __STATICARRAYINITTYPESIZEU3D116_TDF00214C4AC7272026E840470B881841013B7244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D116
struct  __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244__padding[116];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D116_TDF00214C4AC7272026E840470B881841013B7244_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1_H
#define __STATICARRAYINITTYPESIZEU3D12_T3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1_H
#ifndef __STATICARRAYINITTYPESIZEU3D120_T8DDD6C9514870A994B892ACC8E5E40366D37C155_H
#define __STATICARRAYINITTYPESIZEU3D120_T8DDD6C9514870A994B892ACC8E5E40366D37C155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D120
struct  __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D120_T8DDD6C9514870A994B892ACC8E5E40366D37C155_H
#ifndef __STATICARRAYINITTYPESIZEU3D16_TA74794CC2DB67F8B4B9BCE862B22BC6D2497E777_H
#define __STATICARRAYINITTYPESIZEU3D16_TA74794CC2DB67F8B4B9BCE862B22BC6D2497E777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D16
struct  __StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16_TA74794CC2DB67F8B4B9BCE862B22BC6D2497E777_H
#ifndef __STATICARRAYINITTYPESIZEU3D76_T06243B400D517E4BAA79958C008A9C655F63F936_H
#define __STATICARRAYINITTYPESIZEU3D76_T06243B400D517E4BAA79958C008A9C655F63F936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D76
struct  __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936__padding[76];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D76_T06243B400D517E4BAA79958C008A9C655F63F936_H
#ifndef PKZIPCLASSICDECRYPTCRYPTOTRANSFORM_TBB3B4209A63C60CB64240640E0CBA21626ACA93E_H
#define PKZIPCLASSICDECRYPTCRYPTOTRANSFORM_TBB3B4209A63C60CB64240640E0CBA21626ACA93E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform
struct  PkzipClassicDecryptCryptoTransform_tBB3B4209A63C60CB64240640E0CBA21626ACA93E  : public PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKZIPCLASSICDECRYPTCRYPTOTRANSFORM_TBB3B4209A63C60CB64240640E0CBA21626ACA93E_H
#ifndef PKZIPCLASSICENCRYPTCRYPTOTRANSFORM_T050C6231405F6F498F285DF8D3FCB2DCD6EB2EE0_H
#define PKZIPCLASSICENCRYPTCRYPTOTRANSFORM_T050C6231405F6F498F285DF8D3FCB2DCD6EB2EE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform
struct  PkzipClassicEncryptCryptoTransform_t050C6231405F6F498F285DF8D3FCB2DCD6EB2EE0  : public PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKZIPCLASSICENCRYPTCRYPTOTRANSFORM_T050C6231405F6F498F285DF8D3FCB2DCD6EB2EE0_H
#ifndef KEYSREQUIREDEVENTARGS_T333068E27532BA6462A1C84D7D3CCE8904FC7148_H
#define KEYSREQUIREDEVENTARGS_T333068E27532BA6462A1C84D7D3CCE8904FC7148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs
struct  KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::fileName
	String_t* ___fileName_1;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_2;

public:
	inline static int32_t get_offset_of_fileName_1() { return static_cast<int32_t>(offsetof(KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148, ___fileName_1)); }
	inline String_t* get_fileName_1() const { return ___fileName_1; }
	inline String_t** get_address_of_fileName_1() { return &___fileName_1; }
	inline void set_fileName_1(String_t* value)
	{
		___fileName_1 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_1), value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148, ___key_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_2() const { return ___key_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYSREQUIREDEVENTARGS_T333068E27532BA6462A1C84D7D3CCE8904FC7148_H
#ifndef APPLICATIONEXCEPTION_T664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_H
#define APPLICATIONEXCEPTION_T664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ApplicationException
struct  ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONEXCEPTION_T664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef UNITYEVENT_1_T796EE0CEE20D595E6DACBBADB076540F92D6648C_H
#define UNITYEVENT_1_T796EE0CEE20D595E6DACBBADB076540F92D6648C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>
struct  UnityEvent_1_t796EE0CEE20D595E6DACBBADB076540F92D6648C  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t796EE0CEE20D595E6DACBBADB076540F92D6648C, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T796EE0CEE20D595E6DACBBADB076540F92D6648C_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_TF556D54546BC2FC0638B99AF7BAF448A985AB3EC_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_TF556D54546BC2FC0638B99AF7BAF448A985AB3EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D
struct  U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields
{
public:
	// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D::U24U24method0x60004f9U2D1
	__StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1  ___U24U24method0x60004f9U2D1_0;
	// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D76 <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D::U24U24method0x600029dU2D1
	__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  ___U24U24method0x600029dU2D1_1;
	// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D::U24U24method0x600029dU2D2
	__StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777  ___U24U24method0x600029dU2D2_2;
	// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D116 <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D::U24U24method0x60004fdU2D1
	__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  ___U24U24method0x60004fdU2D1_3;
	// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D116 <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D::U24U24method0x60004fdU2D2
	__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  ___U24U24method0x60004fdU2D2_4;
	// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D120 <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D::U24U24method0x60004fdU2D3
	__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  ___U24U24method0x60004fdU2D3_5;
	// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D120 <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D::U24U24method0x60004fdU2D4
	__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  ___U24U24method0x60004fdU2D4_6;
	// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D::U24U24method0x60004feU2D1
	__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  ___U24U24method0x60004feU2D1_7;
	// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D::U24U24method0x60004feU2D2
	__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  ___U24U24method0x60004feU2D2_8;
	// <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D___StaticArrayInitTypeSizeU3D76 <PrivateImplementationDetails>U7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D::U24U24method0x60004feU2D3
	__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  ___U24U24method0x60004feU2D3_9;

public:
	inline static int32_t get_offset_of_U24U24method0x60004f9U2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004f9U2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1  get_U24U24method0x60004f9U2D1_0() const { return ___U24U24method0x60004f9U2D1_0; }
	inline __StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1 * get_address_of_U24U24method0x60004f9U2D1_0() { return &___U24U24method0x60004f9U2D1_0; }
	inline void set_U24U24method0x60004f9U2D1_0(__StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1  value)
	{
		___U24U24method0x60004f9U2D1_0 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x600029dU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x600029dU2D1_1)); }
	inline __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  get_U24U24method0x600029dU2D1_1() const { return ___U24U24method0x600029dU2D1_1; }
	inline __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936 * get_address_of_U24U24method0x600029dU2D1_1() { return &___U24U24method0x600029dU2D1_1; }
	inline void set_U24U24method0x600029dU2D1_1(__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  value)
	{
		___U24U24method0x600029dU2D1_1 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x600029dU2D2_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x600029dU2D2_2)); }
	inline __StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777  get_U24U24method0x600029dU2D2_2() const { return ___U24U24method0x600029dU2D2_2; }
	inline __StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777 * get_address_of_U24U24method0x600029dU2D2_2() { return &___U24U24method0x600029dU2D2_2; }
	inline void set_U24U24method0x600029dU2D2_2(__StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777  value)
	{
		___U24U24method0x600029dU2D2_2 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004fdU2D1_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004fdU2D1_3)); }
	inline __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  get_U24U24method0x60004fdU2D1_3() const { return ___U24U24method0x60004fdU2D1_3; }
	inline __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244 * get_address_of_U24U24method0x60004fdU2D1_3() { return &___U24U24method0x60004fdU2D1_3; }
	inline void set_U24U24method0x60004fdU2D1_3(__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  value)
	{
		___U24U24method0x60004fdU2D1_3 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004fdU2D2_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004fdU2D2_4)); }
	inline __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  get_U24U24method0x60004fdU2D2_4() const { return ___U24U24method0x60004fdU2D2_4; }
	inline __StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244 * get_address_of_U24U24method0x60004fdU2D2_4() { return &___U24U24method0x60004fdU2D2_4; }
	inline void set_U24U24method0x60004fdU2D2_4(__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244  value)
	{
		___U24U24method0x60004fdU2D2_4 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004fdU2D3_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004fdU2D3_5)); }
	inline __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  get_U24U24method0x60004fdU2D3_5() const { return ___U24U24method0x60004fdU2D3_5; }
	inline __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155 * get_address_of_U24U24method0x60004fdU2D3_5() { return &___U24U24method0x60004fdU2D3_5; }
	inline void set_U24U24method0x60004fdU2D3_5(__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  value)
	{
		___U24U24method0x60004fdU2D3_5 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004fdU2D4_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004fdU2D4_6)); }
	inline __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  get_U24U24method0x60004fdU2D4_6() const { return ___U24U24method0x60004fdU2D4_6; }
	inline __StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155 * get_address_of_U24U24method0x60004fdU2D4_6() { return &___U24U24method0x60004fdU2D4_6; }
	inline void set_U24U24method0x60004fdU2D4_6(__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155  value)
	{
		___U24U24method0x60004fdU2D4_6 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004feU2D1_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004feU2D1_7)); }
	inline __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  get_U24U24method0x60004feU2D1_7() const { return ___U24U24method0x60004feU2D1_7; }
	inline __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1 * get_address_of_U24U24method0x60004feU2D1_7() { return &___U24U24method0x60004feU2D1_7; }
	inline void set_U24U24method0x60004feU2D1_7(__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  value)
	{
		___U24U24method0x60004feU2D1_7 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004feU2D2_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004feU2D2_8)); }
	inline __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  get_U24U24method0x60004feU2D2_8() const { return ___U24U24method0x60004feU2D2_8; }
	inline __StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1 * get_address_of_U24U24method0x60004feU2D2_8() { return &___U24U24method0x60004feU2D2_8; }
	inline void set_U24U24method0x60004feU2D2_8(__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1  value)
	{
		___U24U24method0x60004feU2D2_8 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x60004feU2D3_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields, ___U24U24method0x60004feU2D3_9)); }
	inline __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  get_U24U24method0x60004feU2D3_9() const { return ___U24U24method0x60004feU2D3_9; }
	inline __StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936 * get_address_of_U24U24method0x60004feU2D3_9() { return &___U24U24method0x60004feU2D3_9; }
	inline void set_U24U24method0x60004feU2D3_9(__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936  value)
	{
		___U24U24method0x60004feU2D3_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_TF556D54546BC2FC0638B99AF7BAF448A985AB3EC_H
#ifndef SHARPZIPBASEEXCEPTION_T822541D5F0EC9FCA3550F00DCF0883E987E5045F_H
#define SHARPZIPBASEEXCEPTION_T822541D5F0EC9FCA3550F00DCF0883E987E5045F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.SharpZipBaseException
struct  SharpZipBaseException_t822541D5F0EC9FCA3550F00DCF0883E987E5045F  : public ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARPZIPBASEEXCEPTION_T822541D5F0EC9FCA3550F00DCF0883E987E5045F_H
#ifndef INFLATERINPUTSTREAM_T73956344292D849DFE9A530C35B00DBE415C7DF6_H
#define INFLATERINPUTSTREAM_T73956344292D849DFE9A530C35B00DBE415C7DF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream
struct  InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// ICSharpCode.SharpZipLib.Zip.Compression.Inflater ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::inf
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB * ___inf_4;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::inputBuffer
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3 * ___inputBuffer_5;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::baseInputStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___baseInputStream_6;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::isClosed
	bool ___isClosed_7;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::isStreamOwner
	bool ___isStreamOwner_8;

public:
	inline static int32_t get_offset_of_inf_4() { return static_cast<int32_t>(offsetof(InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6, ___inf_4)); }
	inline Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB * get_inf_4() const { return ___inf_4; }
	inline Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB ** get_address_of_inf_4() { return &___inf_4; }
	inline void set_inf_4(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB * value)
	{
		___inf_4 = value;
		Il2CppCodeGenWriteBarrier((&___inf_4), value);
	}

	inline static int32_t get_offset_of_inputBuffer_5() { return static_cast<int32_t>(offsetof(InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6, ___inputBuffer_5)); }
	inline InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3 * get_inputBuffer_5() const { return ___inputBuffer_5; }
	inline InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3 ** get_address_of_inputBuffer_5() { return &___inputBuffer_5; }
	inline void set_inputBuffer_5(InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3 * value)
	{
		___inputBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___inputBuffer_5), value);
	}

	inline static int32_t get_offset_of_baseInputStream_6() { return static_cast<int32_t>(offsetof(InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6, ___baseInputStream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_baseInputStream_6() const { return ___baseInputStream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_baseInputStream_6() { return &___baseInputStream_6; }
	inline void set_baseInputStream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___baseInputStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseInputStream_6), value);
	}

	inline static int32_t get_offset_of_isClosed_7() { return static_cast<int32_t>(offsetof(InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6, ___isClosed_7)); }
	inline bool get_isClosed_7() const { return ___isClosed_7; }
	inline bool* get_address_of_isClosed_7() { return &___isClosed_7; }
	inline void set_isClosed_7(bool value)
	{
		___isClosed_7 = value;
	}

	inline static int32_t get_offset_of_isStreamOwner_8() { return static_cast<int32_t>(offsetof(InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6, ___isStreamOwner_8)); }
	inline bool get_isStreamOwner_8() const { return ___isStreamOwner_8; }
	inline bool* get_address_of_isStreamOwner_8() { return &___isStreamOwner_8; }
	inline void set_isStreamOwner_8(bool value)
	{
		___isStreamOwner_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATERINPUTSTREAM_T73956344292D849DFE9A530C35B00DBE415C7DF6_H
#ifndef COMPRESSIONMETHOD_T1F31EE4DB3B1958C77654ED6CDA048854A028ECD_H
#define COMPRESSIONMETHOD_T1F31EE4DB3B1958C77654ED6CDA048854A028ECD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.CompressionMethod
struct  CompressionMethod_t1F31EE4DB3B1958C77654ED6CDA048854A028ECD 
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.CompressionMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionMethod_t1F31EE4DB3B1958C77654ED6CDA048854A028ECD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMETHOD_T1F31EE4DB3B1958C77654ED6CDA048854A028ECD_H
#ifndef USEZIP64_T64A23EE068A11F6879053B3C4EFBF67F75D3B884_H
#define USEZIP64_T64A23EE068A11F6879053B3C4EFBF67F75D3B884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.UseZip64
struct  UseZip64_t64A23EE068A11F6879053B3C4EFBF67F75D3B884 
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.UseZip64::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UseZip64_t64A23EE068A11F6879053B3C4EFBF67F75D3B884, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEZIP64_T64A23EE068A11F6879053B3C4EFBF67F75D3B884_H
#ifndef KNOWN_TC640ED74F2497140C21D74C29A9E194AC89EF2FB_H
#define KNOWN_TC640ED74F2497140C21D74C29A9E194AC89EF2FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipEntry_Known
struct  Known_tC640ED74F2497140C21D74C29A9E194AC89EF2FB 
{
public:
	// System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry_Known::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Known_tC640ED74F2497140C21D74C29A9E194AC89EF2FB, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNOWN_TC640ED74F2497140C21D74C29A9E194AC89EF2FB_H
#ifndef ZIPENTRYFACTORY_TD953786FCD19BFC64F0A014807EB7B46B0BEF929_H
#define ZIPENTRYFACTORY_TD953786FCD19BFC64F0A014807EB7B46B0BEF929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipEntryFactory
struct  ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929  : public RuntimeObject
{
public:
	// ICSharpCode.SharpZipLib.Core.INameTransform ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::nameTransform_
	RuntimeObject* ___nameTransform__0;
	// System.DateTime ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::fixedDateTime_
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___fixedDateTime__1;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::getAttributes_
	int32_t ___getAttributes__2;

public:
	inline static int32_t get_offset_of_nameTransform__0() { return static_cast<int32_t>(offsetof(ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929, ___nameTransform__0)); }
	inline RuntimeObject* get_nameTransform__0() const { return ___nameTransform__0; }
	inline RuntimeObject** get_address_of_nameTransform__0() { return &___nameTransform__0; }
	inline void set_nameTransform__0(RuntimeObject* value)
	{
		___nameTransform__0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTransform__0), value);
	}

	inline static int32_t get_offset_of_fixedDateTime__1() { return static_cast<int32_t>(offsetof(ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929, ___fixedDateTime__1)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_fixedDateTime__1() const { return ___fixedDateTime__1; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_fixedDateTime__1() { return &___fixedDateTime__1; }
	inline void set_fixedDateTime__1(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___fixedDateTime__1 = value;
	}

	inline static int32_t get_offset_of_getAttributes__2() { return static_cast<int32_t>(offsetof(ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929, ___getAttributes__2)); }
	inline int32_t get_getAttributes__2() const { return ___getAttributes__2; }
	inline int32_t* get_address_of_getAttributes__2() { return &___getAttributes__2; }
	inline void set_getAttributes__2(int32_t value)
	{
		___getAttributes__2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPENTRYFACTORY_TD953786FCD19BFC64F0A014807EB7B46B0BEF929_H
#ifndef HEADERTEST_T98D72CE7316D175D3364E46D294E73256D1532D8_H
#define HEADERTEST_T98D72CE7316D175D3364E46D294E73256D1532D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile_HeaderTest
struct  HeaderTest_t98D72CE7316D175D3364E46D294E73256D1532D8 
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile_HeaderTest::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HeaderTest_t98D72CE7316D175D3364E46D294E73256D1532D8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERTEST_T98D72CE7316D175D3364E46D294E73256D1532D8_H
#ifndef PARTIALINPUTSTREAM_T5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195_H
#define PARTIALINPUTSTREAM_T5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile_PartialInputStream
struct  PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipFile ICSharpCode.SharpZipLib.Zip.ZipFile_PartialInputStream::zipFile_
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * ___zipFile__4;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile_PartialInputStream::baseStream_
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___baseStream__5;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile_PartialInputStream::start_
	int64_t ___start__6;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile_PartialInputStream::length_
	int64_t ___length__7;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile_PartialInputStream::readPos_
	int64_t ___readPos__8;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile_PartialInputStream::end_
	int64_t ___end__9;

public:
	inline static int32_t get_offset_of_zipFile__4() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___zipFile__4)); }
	inline ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * get_zipFile__4() const { return ___zipFile__4; }
	inline ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F ** get_address_of_zipFile__4() { return &___zipFile__4; }
	inline void set_zipFile__4(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F * value)
	{
		___zipFile__4 = value;
		Il2CppCodeGenWriteBarrier((&___zipFile__4), value);
	}

	inline static int32_t get_offset_of_baseStream__5() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___baseStream__5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_baseStream__5() const { return ___baseStream__5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_baseStream__5() { return &___baseStream__5; }
	inline void set_baseStream__5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___baseStream__5 = value;
		Il2CppCodeGenWriteBarrier((&___baseStream__5), value);
	}

	inline static int32_t get_offset_of_start__6() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___start__6)); }
	inline int64_t get_start__6() const { return ___start__6; }
	inline int64_t* get_address_of_start__6() { return &___start__6; }
	inline void set_start__6(int64_t value)
	{
		___start__6 = value;
	}

	inline static int32_t get_offset_of_length__7() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___length__7)); }
	inline int64_t get_length__7() const { return ___length__7; }
	inline int64_t* get_address_of_length__7() { return &___length__7; }
	inline void set_length__7(int64_t value)
	{
		___length__7 = value;
	}

	inline static int32_t get_offset_of_readPos__8() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___readPos__8)); }
	inline int64_t get_readPos__8() const { return ___readPos__8; }
	inline int64_t* get_address_of_readPos__8() { return &___readPos__8; }
	inline void set_readPos__8(int64_t value)
	{
		___readPos__8 = value;
	}

	inline static int32_t get_offset_of_end__9() { return static_cast<int32_t>(offsetof(PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195, ___end__9)); }
	inline int64_t get_end__9() const { return ___end__9; }
	inline int64_t* get_address_of_end__9() { return &___end__9; }
	inline void set_end__9(int64_t value)
	{
		___end__9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTIALINPUTSTREAM_T5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195_H
#ifndef ZIPHELPERSTREAM_T5EBF734E826C8FAB30C920728A06E3291AB40B12_H
#define ZIPHELPERSTREAM_T5EBF734E826C8FAB30C920728A06E3291AB40B12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipHelperStream
struct  ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::isOwner_
	bool ___isOwner__4;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipHelperStream::stream_
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream__5;

public:
	inline static int32_t get_offset_of_isOwner__4() { return static_cast<int32_t>(offsetof(ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12, ___isOwner__4)); }
	inline bool get_isOwner__4() const { return ___isOwner__4; }
	inline bool* get_address_of_isOwner__4() { return &___isOwner__4; }
	inline void set_isOwner__4(bool value)
	{
		___isOwner__4 = value;
	}

	inline static int32_t get_offset_of_stream__5() { return static_cast<int32_t>(offsetof(ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12, ___stream__5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream__5() const { return ___stream__5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream__5() { return &___stream__5; }
	inline void set_stream__5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream__5 = value;
		Il2CppCodeGenWriteBarrier((&___stream__5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPHELPERSTREAM_T5EBF734E826C8FAB30C920728A06E3291AB40B12_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#define CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t1DC3069D617AC3D17A2608F5BB36C0F115D229DF 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherMode_t1DC3069D617AC3D17A2608F5BB36C0F115D229DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T1DC3069D617AC3D17A2608F5BB36C0F115D229DF_H
#ifndef CRYPTOSTREAMMODE_TDA87D3C6D96B2FCC35184B3B4A00923266ADB2D5_H
#define CRYPTOSTREAMMODE_TDA87D3C6D96B2FCC35184B3B4A00923266ADB2D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CryptoStreamMode
struct  CryptoStreamMode_tDA87D3C6D96B2FCC35184B3B4A00923266ADB2D5 
{
public:
	// System.Int32 System.Security.Cryptography.CryptoStreamMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CryptoStreamMode_tDA87D3C6D96B2FCC35184B3B4A00923266ADB2D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOSTREAMMODE_TDA87D3C6D96B2FCC35184B3B4A00923266ADB2D5_H
#ifndef PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#define PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_tA6F228B2795D29C9554F2D6824DB9FF67519A0E0 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PaddingMode_tA6F228B2795D29C9554F2D6824DB9FF67519A0E0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_TA6F228B2795D29C9554F2D6824DB9FF67519A0E0_H
#ifndef ADDITIONALCANVASSHADERCHANNELS_T2576909EF4884007D0786E0FEEB894B54C61107E_H
#define ADDITIONALCANVASSHADERCHANNELS_T2576909EF4884007D0786E0FEEB894B54C61107E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AdditionalCanvasShaderChannels
struct  AdditionalCanvasShaderChannels_t2576909EF4884007D0786E0FEEB894B54C61107E 
{
public:
	// System.Int32 UnityEngine.AdditionalCanvasShaderChannels::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdditionalCanvasShaderChannels_t2576909EF4884007D0786E0FEEB894B54C61107E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDITIONALCANVASSHADERCHANNELS_T2576909EF4884007D0786E0FEEB894B54C61107E_H
#ifndef ANALYTICSSESSIONSTATE_T61CA873937E9A3B881B71B32F518A954A4C8F267_H
#define ANALYTICSSESSIONSTATE_T61CA873937E9A3B881B71B32F518A954A4C8F267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionState
struct  AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsSessionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONSTATE_T61CA873937E9A3B881B71B32F518A954A4C8F267_H
#ifndef TRIGGEREVENT_TF73252408C49CDE2F1A05AA75FE09086C53A9793_H
#define TRIGGEREVENT_TF73252408C49CDE2F1A05AA75FE09086C53A9793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger_TriggerEvent
struct  TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793  : public UnityEvent_1_t796EE0CEE20D595E6DACBBADB076540F92D6648C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_TF73252408C49CDE2F1A05AA75FE09086C53A9793_H
#ifndef EVENTTRIGGERTYPE_T1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE_H
#define EVENTTRIGGERTYPE_T1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTriggerType
struct  EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventTriggerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGERTYPE_T1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE_H
#ifndef MOVEDIRECTION_T82C25470C79BBE899C5E27B312A983D7FF457E1B_H
#define MOVEDIRECTION_T82C25470C79BBE899C5E27B312A983D7FF457E1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MoveDirection
struct  MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDIRECTION_T82C25470C79BBE899C5E27B312A983D7FF457E1B_H
#ifndef RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#define RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___m_GameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___module_1)); }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldNormal_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___screenPosition_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_pinvoke
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_com
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#ifndef DOWNLOADHANDLER_T4A7802ADC97024B469C87FA454B6973951980EE9_H
#define DOWNLOADHANDLER_T4A7802ADC97024B469C87FA454B6973951980EE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandler
struct  DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // DOWNLOADHANDLER_T4A7802ADC97024B469C87FA454B6973951980EE9_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef REMOTECONFIGSETTINGS_T97154F5546B47CE72257CC2F0B677BDF696AEC4A_H
#define REMOTECONFIGSETTINGS_T97154F5546B47CE72257CC2F0B677BDF696AEC4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteConfigSettings
struct  RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RemoteConfigSettings::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<System.Boolean> UnityEngine.RemoteConfigSettings::Updated
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___Updated_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_Updated_1() { return static_cast<int32_t>(offsetof(RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A, ___Updated_1)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_Updated_1() const { return ___Updated_1; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_Updated_1() { return &___Updated_1; }
	inline void set_Updated_1(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___Updated_1 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
// Native definition for COM marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
#endif // REMOTECONFIGSETTINGS_T97154F5546B47CE72257CC2F0B677BDF696AEC4A_H
#ifndef SAMPLETYPE_T2144AEAF3447ACAFCE1C13AF669F63192F8E75EC_H
#define SAMPLETYPE_T2144AEAF3447ACAFCE1C13AF669F63192F8E75EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi_SampleType
struct  SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC 
{
public:
	// System.Int32 UnityEngine.UISystemProfilerApi_SampleType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLETYPE_T2144AEAF3447ACAFCE1C13AF669F63192F8E75EC_H
#ifndef ZIPENTRY_T0C490E8147650DD0F764C17C5D545E633E38E9E5_H
#define ZIPENTRY_T0C490E8147650DD0F764C17C5D545E633E38E9E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct  ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5  : public RuntimeObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipEntry_Known ICSharpCode.SharpZipLib.Zip.ZipEntry::known
	uint8_t ___known_0;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::externalFileAttributes
	int32_t ___externalFileAttributes_1;
	// System.UInt16 ICSharpCode.SharpZipLib.Zip.ZipEntry::versionMadeBy
	uint16_t ___versionMadeBy_2;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::name
	String_t* ___name_3;
	// System.UInt64 ICSharpCode.SharpZipLib.Zip.ZipEntry::size
	uint64_t ___size_4;
	// System.UInt64 ICSharpCode.SharpZipLib.Zip.ZipEntry::compressedSize
	uint64_t ___compressedSize_5;
	// System.UInt16 ICSharpCode.SharpZipLib.Zip.ZipEntry::versionToExtract
	uint16_t ___versionToExtract_6;
	// System.UInt32 ICSharpCode.SharpZipLib.Zip.ZipEntry::crc
	uint32_t ___crc_7;
	// System.UInt32 ICSharpCode.SharpZipLib.Zip.ZipEntry::dosTime
	uint32_t ___dosTime_8;
	// ICSharpCode.SharpZipLib.Zip.CompressionMethod ICSharpCode.SharpZipLib.Zip.ZipEntry::method
	int32_t ___method_9;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipEntry::extra
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___extra_10;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::comment
	String_t* ___comment_11;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::flags
	int32_t ___flags_12;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::zipFileIndex
	int64_t ___zipFileIndex_13;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::offset
	int64_t ___offset_14;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::forceZip64_
	bool ___forceZip64__15;
	// System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry::cryptoCheckValue_
	uint8_t ___cryptoCheckValue__16;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::_aesVer
	int32_t ____aesVer_17;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::_aesEncryptionStrength
	int32_t ____aesEncryptionStrength_18;

public:
	inline static int32_t get_offset_of_known_0() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___known_0)); }
	inline uint8_t get_known_0() const { return ___known_0; }
	inline uint8_t* get_address_of_known_0() { return &___known_0; }
	inline void set_known_0(uint8_t value)
	{
		___known_0 = value;
	}

	inline static int32_t get_offset_of_externalFileAttributes_1() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___externalFileAttributes_1)); }
	inline int32_t get_externalFileAttributes_1() const { return ___externalFileAttributes_1; }
	inline int32_t* get_address_of_externalFileAttributes_1() { return &___externalFileAttributes_1; }
	inline void set_externalFileAttributes_1(int32_t value)
	{
		___externalFileAttributes_1 = value;
	}

	inline static int32_t get_offset_of_versionMadeBy_2() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___versionMadeBy_2)); }
	inline uint16_t get_versionMadeBy_2() const { return ___versionMadeBy_2; }
	inline uint16_t* get_address_of_versionMadeBy_2() { return &___versionMadeBy_2; }
	inline void set_versionMadeBy_2(uint16_t value)
	{
		___versionMadeBy_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___size_4)); }
	inline uint64_t get_size_4() const { return ___size_4; }
	inline uint64_t* get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(uint64_t value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_compressedSize_5() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___compressedSize_5)); }
	inline uint64_t get_compressedSize_5() const { return ___compressedSize_5; }
	inline uint64_t* get_address_of_compressedSize_5() { return &___compressedSize_5; }
	inline void set_compressedSize_5(uint64_t value)
	{
		___compressedSize_5 = value;
	}

	inline static int32_t get_offset_of_versionToExtract_6() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___versionToExtract_6)); }
	inline uint16_t get_versionToExtract_6() const { return ___versionToExtract_6; }
	inline uint16_t* get_address_of_versionToExtract_6() { return &___versionToExtract_6; }
	inline void set_versionToExtract_6(uint16_t value)
	{
		___versionToExtract_6 = value;
	}

	inline static int32_t get_offset_of_crc_7() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___crc_7)); }
	inline uint32_t get_crc_7() const { return ___crc_7; }
	inline uint32_t* get_address_of_crc_7() { return &___crc_7; }
	inline void set_crc_7(uint32_t value)
	{
		___crc_7 = value;
	}

	inline static int32_t get_offset_of_dosTime_8() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___dosTime_8)); }
	inline uint32_t get_dosTime_8() const { return ___dosTime_8; }
	inline uint32_t* get_address_of_dosTime_8() { return &___dosTime_8; }
	inline void set_dosTime_8(uint32_t value)
	{
		___dosTime_8 = value;
	}

	inline static int32_t get_offset_of_method_9() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___method_9)); }
	inline int32_t get_method_9() const { return ___method_9; }
	inline int32_t* get_address_of_method_9() { return &___method_9; }
	inline void set_method_9(int32_t value)
	{
		___method_9 = value;
	}

	inline static int32_t get_offset_of_extra_10() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___extra_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_extra_10() const { return ___extra_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_extra_10() { return &___extra_10; }
	inline void set_extra_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___extra_10 = value;
		Il2CppCodeGenWriteBarrier((&___extra_10), value);
	}

	inline static int32_t get_offset_of_comment_11() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___comment_11)); }
	inline String_t* get_comment_11() const { return ___comment_11; }
	inline String_t** get_address_of_comment_11() { return &___comment_11; }
	inline void set_comment_11(String_t* value)
	{
		___comment_11 = value;
		Il2CppCodeGenWriteBarrier((&___comment_11), value);
	}

	inline static int32_t get_offset_of_flags_12() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___flags_12)); }
	inline int32_t get_flags_12() const { return ___flags_12; }
	inline int32_t* get_address_of_flags_12() { return &___flags_12; }
	inline void set_flags_12(int32_t value)
	{
		___flags_12 = value;
	}

	inline static int32_t get_offset_of_zipFileIndex_13() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___zipFileIndex_13)); }
	inline int64_t get_zipFileIndex_13() const { return ___zipFileIndex_13; }
	inline int64_t* get_address_of_zipFileIndex_13() { return &___zipFileIndex_13; }
	inline void set_zipFileIndex_13(int64_t value)
	{
		___zipFileIndex_13 = value;
	}

	inline static int32_t get_offset_of_offset_14() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___offset_14)); }
	inline int64_t get_offset_14() const { return ___offset_14; }
	inline int64_t* get_address_of_offset_14() { return &___offset_14; }
	inline void set_offset_14(int64_t value)
	{
		___offset_14 = value;
	}

	inline static int32_t get_offset_of_forceZip64__15() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___forceZip64__15)); }
	inline bool get_forceZip64__15() const { return ___forceZip64__15; }
	inline bool* get_address_of_forceZip64__15() { return &___forceZip64__15; }
	inline void set_forceZip64__15(bool value)
	{
		___forceZip64__15 = value;
	}

	inline static int32_t get_offset_of_cryptoCheckValue__16() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ___cryptoCheckValue__16)); }
	inline uint8_t get_cryptoCheckValue__16() const { return ___cryptoCheckValue__16; }
	inline uint8_t* get_address_of_cryptoCheckValue__16() { return &___cryptoCheckValue__16; }
	inline void set_cryptoCheckValue__16(uint8_t value)
	{
		___cryptoCheckValue__16 = value;
	}

	inline static int32_t get_offset_of__aesVer_17() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ____aesVer_17)); }
	inline int32_t get__aesVer_17() const { return ____aesVer_17; }
	inline int32_t* get_address_of__aesVer_17() { return &____aesVer_17; }
	inline void set__aesVer_17(int32_t value)
	{
		____aesVer_17 = value;
	}

	inline static int32_t get_offset_of__aesEncryptionStrength_18() { return static_cast<int32_t>(offsetof(ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5, ____aesEncryptionStrength_18)); }
	inline int32_t get__aesEncryptionStrength_18() const { return ____aesEncryptionStrength_18; }
	inline int32_t* get_address_of__aesEncryptionStrength_18() { return &____aesEncryptionStrength_18; }
	inline void set__aesEncryptionStrength_18(int32_t value)
	{
		____aesEncryptionStrength_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPENTRY_T0C490E8147650DD0F764C17C5D545E633E38E9E5_H
#ifndef ZIPEXCEPTION_T555EB5B9EA2BB8DF941C19C19F3BBA466EE7D944_H
#define ZIPEXCEPTION_T555EB5B9EA2BB8DF941C19C19F3BBA466EE7D944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipException
struct  ZipException_t555EB5B9EA2BB8DF941C19C19F3BBA466EE7D944  : public SharpZipBaseException_t822541D5F0EC9FCA3550F00DCF0883E987E5045F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPEXCEPTION_T555EB5B9EA2BB8DF941C19C19F3BBA466EE7D944_H
#ifndef ZIPFILE_TEFDC6C532094F9A5F6185F4E69E340602984E28F_H
#define ZIPFILE_TEFDC6C532094F9A5F6185F4E69E340602984E28F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile
struct  ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F  : public RuntimeObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipFile_KeysRequiredEventHandler ICSharpCode.SharpZipLib.Zip.ZipFile::KeysRequired
	KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9 * ___KeysRequired_0;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::isDisposed_
	bool ___isDisposed__1;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::comment_
	String_t* ___comment__2;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::rawPassword_
	String_t* ___rawPassword__3;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::baseStream_
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___baseStream__4;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::isStreamOwner
	bool ___isStreamOwner_5;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::offsetOfFirstEntry
	int64_t ___offsetOfFirstEntry_6;
	// ICSharpCode.SharpZipLib.Zip.ZipEntry[] ICSharpCode.SharpZipLib.Zip.ZipFile::entries_
	ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* ___entries__7;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipFile::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_8;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::isNewArchive_
	bool ___isNewArchive__9;
	// ICSharpCode.SharpZipLib.Zip.UseZip64 ICSharpCode.SharpZipLib.Zip.ZipFile::useZip64_
	int32_t ___useZip64__10;
	// System.Collections.ArrayList ICSharpCode.SharpZipLib.Zip.ZipFile::updates_
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___updates__11;
	// System.Collections.Hashtable ICSharpCode.SharpZipLib.Zip.ZipFile::updateIndex_
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___updateIndex__12;
	// ICSharpCode.SharpZipLib.Zip.IArchiveStorage ICSharpCode.SharpZipLib.Zip.ZipFile::archiveStorage_
	RuntimeObject* ___archiveStorage__13;
	// ICSharpCode.SharpZipLib.Zip.IDynamicDataSource ICSharpCode.SharpZipLib.Zip.ZipFile::updateDataSource_
	RuntimeObject* ___updateDataSource__14;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::bufferSize_
	int32_t ___bufferSize__15;
	// ICSharpCode.SharpZipLib.Zip.IEntryFactory ICSharpCode.SharpZipLib.Zip.ZipFile::updateEntryFactory_
	RuntimeObject* ___updateEntryFactory__16;

public:
	inline static int32_t get_offset_of_KeysRequired_0() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___KeysRequired_0)); }
	inline KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9 * get_KeysRequired_0() const { return ___KeysRequired_0; }
	inline KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9 ** get_address_of_KeysRequired_0() { return &___KeysRequired_0; }
	inline void set_KeysRequired_0(KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9 * value)
	{
		___KeysRequired_0 = value;
		Il2CppCodeGenWriteBarrier((&___KeysRequired_0), value);
	}

	inline static int32_t get_offset_of_isDisposed__1() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___isDisposed__1)); }
	inline bool get_isDisposed__1() const { return ___isDisposed__1; }
	inline bool* get_address_of_isDisposed__1() { return &___isDisposed__1; }
	inline void set_isDisposed__1(bool value)
	{
		___isDisposed__1 = value;
	}

	inline static int32_t get_offset_of_comment__2() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___comment__2)); }
	inline String_t* get_comment__2() const { return ___comment__2; }
	inline String_t** get_address_of_comment__2() { return &___comment__2; }
	inline void set_comment__2(String_t* value)
	{
		___comment__2 = value;
		Il2CppCodeGenWriteBarrier((&___comment__2), value);
	}

	inline static int32_t get_offset_of_rawPassword__3() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___rawPassword__3)); }
	inline String_t* get_rawPassword__3() const { return ___rawPassword__3; }
	inline String_t** get_address_of_rawPassword__3() { return &___rawPassword__3; }
	inline void set_rawPassword__3(String_t* value)
	{
		___rawPassword__3 = value;
		Il2CppCodeGenWriteBarrier((&___rawPassword__3), value);
	}

	inline static int32_t get_offset_of_baseStream__4() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___baseStream__4)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_baseStream__4() const { return ___baseStream__4; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_baseStream__4() { return &___baseStream__4; }
	inline void set_baseStream__4(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___baseStream__4 = value;
		Il2CppCodeGenWriteBarrier((&___baseStream__4), value);
	}

	inline static int32_t get_offset_of_isStreamOwner_5() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___isStreamOwner_5)); }
	inline bool get_isStreamOwner_5() const { return ___isStreamOwner_5; }
	inline bool* get_address_of_isStreamOwner_5() { return &___isStreamOwner_5; }
	inline void set_isStreamOwner_5(bool value)
	{
		___isStreamOwner_5 = value;
	}

	inline static int32_t get_offset_of_offsetOfFirstEntry_6() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___offsetOfFirstEntry_6)); }
	inline int64_t get_offsetOfFirstEntry_6() const { return ___offsetOfFirstEntry_6; }
	inline int64_t* get_address_of_offsetOfFirstEntry_6() { return &___offsetOfFirstEntry_6; }
	inline void set_offsetOfFirstEntry_6(int64_t value)
	{
		___offsetOfFirstEntry_6 = value;
	}

	inline static int32_t get_offset_of_entries__7() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___entries__7)); }
	inline ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* get_entries__7() const { return ___entries__7; }
	inline ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917** get_address_of_entries__7() { return &___entries__7; }
	inline void set_entries__7(ZipEntryU5BU5D_t5CD99058EAD4D532F900C264520CD7BBA4C2A917* value)
	{
		___entries__7 = value;
		Il2CppCodeGenWriteBarrier((&___entries__7), value);
	}

	inline static int32_t get_offset_of_key_8() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___key_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_8() const { return ___key_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_8() { return &___key_8; }
	inline void set_key_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_8 = value;
		Il2CppCodeGenWriteBarrier((&___key_8), value);
	}

	inline static int32_t get_offset_of_isNewArchive__9() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___isNewArchive__9)); }
	inline bool get_isNewArchive__9() const { return ___isNewArchive__9; }
	inline bool* get_address_of_isNewArchive__9() { return &___isNewArchive__9; }
	inline void set_isNewArchive__9(bool value)
	{
		___isNewArchive__9 = value;
	}

	inline static int32_t get_offset_of_useZip64__10() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___useZip64__10)); }
	inline int32_t get_useZip64__10() const { return ___useZip64__10; }
	inline int32_t* get_address_of_useZip64__10() { return &___useZip64__10; }
	inline void set_useZip64__10(int32_t value)
	{
		___useZip64__10 = value;
	}

	inline static int32_t get_offset_of_updates__11() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___updates__11)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_updates__11() const { return ___updates__11; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_updates__11() { return &___updates__11; }
	inline void set_updates__11(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___updates__11 = value;
		Il2CppCodeGenWriteBarrier((&___updates__11), value);
	}

	inline static int32_t get_offset_of_updateIndex__12() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___updateIndex__12)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_updateIndex__12() const { return ___updateIndex__12; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_updateIndex__12() { return &___updateIndex__12; }
	inline void set_updateIndex__12(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___updateIndex__12 = value;
		Il2CppCodeGenWriteBarrier((&___updateIndex__12), value);
	}

	inline static int32_t get_offset_of_archiveStorage__13() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___archiveStorage__13)); }
	inline RuntimeObject* get_archiveStorage__13() const { return ___archiveStorage__13; }
	inline RuntimeObject** get_address_of_archiveStorage__13() { return &___archiveStorage__13; }
	inline void set_archiveStorage__13(RuntimeObject* value)
	{
		___archiveStorage__13 = value;
		Il2CppCodeGenWriteBarrier((&___archiveStorage__13), value);
	}

	inline static int32_t get_offset_of_updateDataSource__14() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___updateDataSource__14)); }
	inline RuntimeObject* get_updateDataSource__14() const { return ___updateDataSource__14; }
	inline RuntimeObject** get_address_of_updateDataSource__14() { return &___updateDataSource__14; }
	inline void set_updateDataSource__14(RuntimeObject* value)
	{
		___updateDataSource__14 = value;
		Il2CppCodeGenWriteBarrier((&___updateDataSource__14), value);
	}

	inline static int32_t get_offset_of_bufferSize__15() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___bufferSize__15)); }
	inline int32_t get_bufferSize__15() const { return ___bufferSize__15; }
	inline int32_t* get_address_of_bufferSize__15() { return &___bufferSize__15; }
	inline void set_bufferSize__15(int32_t value)
	{
		___bufferSize__15 = value;
	}

	inline static int32_t get_offset_of_updateEntryFactory__16() { return static_cast<int32_t>(offsetof(ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F, ___updateEntryFactory__16)); }
	inline RuntimeObject* get_updateEntryFactory__16() const { return ___updateEntryFactory__16; }
	inline RuntimeObject** get_address_of_updateEntryFactory__16() { return &___updateEntryFactory__16; }
	inline void set_updateEntryFactory__16(RuntimeObject* value)
	{
		___updateEntryFactory__16 = value;
		Il2CppCodeGenWriteBarrier((&___updateEntryFactory__16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPFILE_TEFDC6C532094F9A5F6185F4E69E340602984E28F_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef CRYPTOSTREAM_T02F19D004439DD03FD7FA81169B58FC3AA05C9FC_H
#define CRYPTOSTREAM_T02F19D004439DD03FD7FA81169B58FC3AA05C9FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CryptoStream
struct  CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream System.Security.Cryptography.CryptoStream::_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____stream_4;
	// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.CryptoStream::_Transform
	RuntimeObject* ____Transform_5;
	// System.Byte[] System.Security.Cryptography.CryptoStream::_InputBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____InputBuffer_6;
	// System.Int32 System.Security.Cryptography.CryptoStream::_InputBufferIndex
	int32_t ____InputBufferIndex_7;
	// System.Int32 System.Security.Cryptography.CryptoStream::_InputBlockSize
	int32_t ____InputBlockSize_8;
	// System.Byte[] System.Security.Cryptography.CryptoStream::_OutputBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____OutputBuffer_9;
	// System.Int32 System.Security.Cryptography.CryptoStream::_OutputBufferIndex
	int32_t ____OutputBufferIndex_10;
	// System.Int32 System.Security.Cryptography.CryptoStream::_OutputBlockSize
	int32_t ____OutputBlockSize_11;
	// System.Security.Cryptography.CryptoStreamMode System.Security.Cryptography.CryptoStream::_transformMode
	int32_t ____transformMode_12;
	// System.Boolean System.Security.Cryptography.CryptoStream::_canRead
	bool ____canRead_13;
	// System.Boolean System.Security.Cryptography.CryptoStream::_canWrite
	bool ____canWrite_14;
	// System.Boolean System.Security.Cryptography.CryptoStream::_finalBlockTransformed
	bool ____finalBlockTransformed_15;

public:
	inline static int32_t get_offset_of__stream_4() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____stream_4)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__stream_4() const { return ____stream_4; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__stream_4() { return &____stream_4; }
	inline void set__stream_4(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____stream_4 = value;
		Il2CppCodeGenWriteBarrier((&____stream_4), value);
	}

	inline static int32_t get_offset_of__Transform_5() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____Transform_5)); }
	inline RuntimeObject* get__Transform_5() const { return ____Transform_5; }
	inline RuntimeObject** get_address_of__Transform_5() { return &____Transform_5; }
	inline void set__Transform_5(RuntimeObject* value)
	{
		____Transform_5 = value;
		Il2CppCodeGenWriteBarrier((&____Transform_5), value);
	}

	inline static int32_t get_offset_of__InputBuffer_6() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____InputBuffer_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__InputBuffer_6() const { return ____InputBuffer_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__InputBuffer_6() { return &____InputBuffer_6; }
	inline void set__InputBuffer_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____InputBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&____InputBuffer_6), value);
	}

	inline static int32_t get_offset_of__InputBufferIndex_7() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____InputBufferIndex_7)); }
	inline int32_t get__InputBufferIndex_7() const { return ____InputBufferIndex_7; }
	inline int32_t* get_address_of__InputBufferIndex_7() { return &____InputBufferIndex_7; }
	inline void set__InputBufferIndex_7(int32_t value)
	{
		____InputBufferIndex_7 = value;
	}

	inline static int32_t get_offset_of__InputBlockSize_8() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____InputBlockSize_8)); }
	inline int32_t get__InputBlockSize_8() const { return ____InputBlockSize_8; }
	inline int32_t* get_address_of__InputBlockSize_8() { return &____InputBlockSize_8; }
	inline void set__InputBlockSize_8(int32_t value)
	{
		____InputBlockSize_8 = value;
	}

	inline static int32_t get_offset_of__OutputBuffer_9() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____OutputBuffer_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__OutputBuffer_9() const { return ____OutputBuffer_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__OutputBuffer_9() { return &____OutputBuffer_9; }
	inline void set__OutputBuffer_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____OutputBuffer_9 = value;
		Il2CppCodeGenWriteBarrier((&____OutputBuffer_9), value);
	}

	inline static int32_t get_offset_of__OutputBufferIndex_10() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____OutputBufferIndex_10)); }
	inline int32_t get__OutputBufferIndex_10() const { return ____OutputBufferIndex_10; }
	inline int32_t* get_address_of__OutputBufferIndex_10() { return &____OutputBufferIndex_10; }
	inline void set__OutputBufferIndex_10(int32_t value)
	{
		____OutputBufferIndex_10 = value;
	}

	inline static int32_t get_offset_of__OutputBlockSize_11() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____OutputBlockSize_11)); }
	inline int32_t get__OutputBlockSize_11() const { return ____OutputBlockSize_11; }
	inline int32_t* get_address_of__OutputBlockSize_11() { return &____OutputBlockSize_11; }
	inline void set__OutputBlockSize_11(int32_t value)
	{
		____OutputBlockSize_11 = value;
	}

	inline static int32_t get_offset_of__transformMode_12() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____transformMode_12)); }
	inline int32_t get__transformMode_12() const { return ____transformMode_12; }
	inline int32_t* get_address_of__transformMode_12() { return &____transformMode_12; }
	inline void set__transformMode_12(int32_t value)
	{
		____transformMode_12 = value;
	}

	inline static int32_t get_offset_of__canRead_13() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____canRead_13)); }
	inline bool get__canRead_13() const { return ____canRead_13; }
	inline bool* get_address_of__canRead_13() { return &____canRead_13; }
	inline void set__canRead_13(bool value)
	{
		____canRead_13 = value;
	}

	inline static int32_t get_offset_of__canWrite_14() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____canWrite_14)); }
	inline bool get__canWrite_14() const { return ____canWrite_14; }
	inline bool* get_address_of__canWrite_14() { return &____canWrite_14; }
	inline void set__canWrite_14(bool value)
	{
		____canWrite_14 = value;
	}

	inline static int32_t get_offset_of__finalBlockTransformed_15() { return static_cast<int32_t>(offsetof(CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC, ____finalBlockTransformed_15)); }
	inline bool get__finalBlockTransformed_15() const { return ____finalBlockTransformed_15; }
	inline bool* get_address_of__finalBlockTransformed_15() { return &____finalBlockTransformed_15; }
	inline void set__finalBlockTransformed_15(bool value)
	{
		____finalBlockTransformed_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOSTREAM_T02F19D004439DD03FD7FA81169B58FC3AA05C9FC_H
#ifndef SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#define SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SymmetricAlgorithm
struct  SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::BlockSizeValue
	int32_t ___BlockSizeValue_0;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::FeedbackSizeValue
	int32_t ___FeedbackSizeValue_1;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::IVValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IVValue_2;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::KeyValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___KeyValue_3;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalBlockSizesValue
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___LegalBlockSizesValue_4;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* ___LegalKeySizesValue_5;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_6;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::ModeValue
	int32_t ___ModeValue_7;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::PaddingValue
	int32_t ___PaddingValue_8;

public:
	inline static int32_t get_offset_of_BlockSizeValue_0() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___BlockSizeValue_0)); }
	inline int32_t get_BlockSizeValue_0() const { return ___BlockSizeValue_0; }
	inline int32_t* get_address_of_BlockSizeValue_0() { return &___BlockSizeValue_0; }
	inline void set_BlockSizeValue_0(int32_t value)
	{
		___BlockSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_FeedbackSizeValue_1() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___FeedbackSizeValue_1)); }
	inline int32_t get_FeedbackSizeValue_1() const { return ___FeedbackSizeValue_1; }
	inline int32_t* get_address_of_FeedbackSizeValue_1() { return &___FeedbackSizeValue_1; }
	inline void set_FeedbackSizeValue_1(int32_t value)
	{
		___FeedbackSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_IVValue_2() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___IVValue_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IVValue_2() const { return ___IVValue_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IVValue_2() { return &___IVValue_2; }
	inline void set_IVValue_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IVValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___IVValue_2), value);
	}

	inline static int32_t get_offset_of_KeyValue_3() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___KeyValue_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_KeyValue_3() const { return ___KeyValue_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_KeyValue_3() { return &___KeyValue_3; }
	inline void set_KeyValue_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___KeyValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_3), value);
	}

	inline static int32_t get_offset_of_LegalBlockSizesValue_4() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___LegalBlockSizesValue_4)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_LegalBlockSizesValue_4() const { return ___LegalBlockSizesValue_4; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_LegalBlockSizesValue_4() { return &___LegalBlockSizesValue_4; }
	inline void set_LegalBlockSizesValue_4(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___LegalBlockSizesValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___LegalBlockSizesValue_4), value);
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_5() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___LegalKeySizesValue_5)); }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* get_LegalKeySizesValue_5() const { return ___LegalKeySizesValue_5; }
	inline KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E** get_address_of_LegalKeySizesValue_5() { return &___LegalKeySizesValue_5; }
	inline void set_LegalKeySizesValue_5(KeySizesU5BU5D_t934CCA482596402177BAF86727F169872D74934E* value)
	{
		___LegalKeySizesValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_5), value);
	}

	inline static int32_t get_offset_of_KeySizeValue_6() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___KeySizeValue_6)); }
	inline int32_t get_KeySizeValue_6() const { return ___KeySizeValue_6; }
	inline int32_t* get_address_of_KeySizeValue_6() { return &___KeySizeValue_6; }
	inline void set_KeySizeValue_6(int32_t value)
	{
		___KeySizeValue_6 = value;
	}

	inline static int32_t get_offset_of_ModeValue_7() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___ModeValue_7)); }
	inline int32_t get_ModeValue_7() const { return ___ModeValue_7; }
	inline int32_t* get_address_of_ModeValue_7() { return &___ModeValue_7; }
	inline void set_ModeValue_7(int32_t value)
	{
		___ModeValue_7 = value;
	}

	inline static int32_t get_offset_of_PaddingValue_8() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789, ___PaddingValue_8)); }
	inline int32_t get_PaddingValue_8() const { return ___PaddingValue_8; }
	inline int32_t* get_address_of_PaddingValue_8() { return &___PaddingValue_8; }
	inline void set_PaddingValue_8(int32_t value)
	{
		___PaddingValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICALGORITHM_T0A2EC7E7AD8B8976832B4F0AC432B691F862E789_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ENTRY_T58989269D924DCD15F196DDEDAB84B85ED4D734E_H
#define ENTRY_T58989269D924DCD15F196DDEDAB84B85ED4D734E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger_Entry
struct  Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.EventTriggerType UnityEngine.EventSystems.EventTrigger_Entry::eventID
	int32_t ___eventID_0;
	// UnityEngine.EventSystems.EventTrigger_TriggerEvent UnityEngine.EventSystems.EventTrigger_Entry::callback
	TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 * ___callback_1;

public:
	inline static int32_t get_offset_of_eventID_0() { return static_cast<int32_t>(offsetof(Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E, ___eventID_0)); }
	inline int32_t get_eventID_0() const { return ___eventID_0; }
	inline int32_t* get_address_of_eventID_0() { return &___eventID_0; }
	inline void set_eventID_0(int32_t value)
	{
		___eventID_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E, ___callback_1)); }
	inline TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 * get_callback_1() const { return ___callback_1; }
	inline TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T58989269D924DCD15F196DDEDAB84B85ED4D734E_H
#ifndef DOWNLOADHANDLERTEXTURE_TDD365789CD15EDFB8582612507C8EE34C62A3DE4_H
#define DOWNLOADHANDLERTEXTURE_TDD365789CD15EDFB8582612507C8EE34C62A3DE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandlerTexture
struct  DownloadHandlerTexture_tDD365789CD15EDFB8582612507C8EE34C62A3DE4  : public DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9
{
public:
	// UnityEngine.Texture2D UnityEngine.Networking.DownloadHandlerTexture::mTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___mTexture_1;
	// System.Boolean UnityEngine.Networking.DownloadHandlerTexture::mHasTexture
	bool ___mHasTexture_2;
	// System.Boolean UnityEngine.Networking.DownloadHandlerTexture::mNonReadable
	bool ___mNonReadable_3;

public:
	inline static int32_t get_offset_of_mTexture_1() { return static_cast<int32_t>(offsetof(DownloadHandlerTexture_tDD365789CD15EDFB8582612507C8EE34C62A3DE4, ___mTexture_1)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_mTexture_1() const { return ___mTexture_1; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_mTexture_1() { return &___mTexture_1; }
	inline void set_mTexture_1(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___mTexture_1 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_1), value);
	}

	inline static int32_t get_offset_of_mHasTexture_2() { return static_cast<int32_t>(offsetof(DownloadHandlerTexture_tDD365789CD15EDFB8582612507C8EE34C62A3DE4, ___mHasTexture_2)); }
	inline bool get_mHasTexture_2() const { return ___mHasTexture_2; }
	inline bool* get_address_of_mHasTexture_2() { return &___mHasTexture_2; }
	inline void set_mHasTexture_2(bool value)
	{
		___mHasTexture_2 = value;
	}

	inline static int32_t get_offset_of_mNonReadable_3() { return static_cast<int32_t>(offsetof(DownloadHandlerTexture_tDD365789CD15EDFB8582612507C8EE34C62A3DE4, ___mNonReadable_3)); }
	inline bool get_mNonReadable_3() const { return ___mNonReadable_3; }
	inline bool* get_address_of_mNonReadable_3() { return &___mNonReadable_3; }
	inline void set_mNonReadable_3(bool value)
	{
		___mNonReadable_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerTexture
struct DownloadHandlerTexture_tDD365789CD15EDFB8582612507C8EE34C62A3DE4_marshaled_pinvoke : public DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_pinvoke
{
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___mTexture_1;
	int32_t ___mHasTexture_2;
	int32_t ___mNonReadable_3;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerTexture
struct DownloadHandlerTexture_tDD365789CD15EDFB8582612507C8EE34C62A3DE4_marshaled_com : public DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_com
{
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___mTexture_1;
	int32_t ___mHasTexture_2;
	int32_t ___mNonReadable_3;
};
#endif // DOWNLOADHANDLERTEXTURE_TDD365789CD15EDFB8582612507C8EE34C62A3DE4_H
#ifndef PKZIPCLASSIC_TDAD4BF22079888AFE3944F718304C54DD2EEDF7E_H
#define PKZIPCLASSIC_TDAD4BF22079888AFE3944F718304C54DD2EEDF7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassic
struct  PkzipClassic_tDAD4BF22079888AFE3944F718304C54DD2EEDF7E  : public SymmetricAlgorithm_t0A2EC7E7AD8B8976832B4F0AC432B691F862E789
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKZIPCLASSIC_TDAD4BF22079888AFE3944F718304C54DD2EEDF7E_H
#ifndef ZIPAESSTREAM_TCD59113BEAB7E3B63907D92AD46F7503A03B2CB2_H
#define ZIPAESSTREAM_TCD59113BEAB7E3B63907D92AD46F7503A03B2CB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.ZipAESStream
struct  ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2  : public CryptoStream_t02F19D004439DD03FD7FA81169B58FC3AA05C9FC
{
public:
	// System.IO.Stream ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____stream_16;
	// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_transform
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195 * ____transform_17;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____slideBuffer_18;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBufStartPos
	int32_t ____slideBufStartPos_19;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_slideBufFreePos
	int32_t ____slideBufFreePos_20;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::_blockAndAuth
	int32_t ____blockAndAuth_21;

public:
	inline static int32_t get_offset_of__stream_16() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____stream_16)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__stream_16() const { return ____stream_16; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__stream_16() { return &____stream_16; }
	inline void set__stream_16(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____stream_16 = value;
		Il2CppCodeGenWriteBarrier((&____stream_16), value);
	}

	inline static int32_t get_offset_of__transform_17() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____transform_17)); }
	inline ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195 * get__transform_17() const { return ____transform_17; }
	inline ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195 ** get_address_of__transform_17() { return &____transform_17; }
	inline void set__transform_17(ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195 * value)
	{
		____transform_17 = value;
		Il2CppCodeGenWriteBarrier((&____transform_17), value);
	}

	inline static int32_t get_offset_of__slideBuffer_18() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____slideBuffer_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__slideBuffer_18() const { return ____slideBuffer_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__slideBuffer_18() { return &____slideBuffer_18; }
	inline void set__slideBuffer_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____slideBuffer_18 = value;
		Il2CppCodeGenWriteBarrier((&____slideBuffer_18), value);
	}

	inline static int32_t get_offset_of__slideBufStartPos_19() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____slideBufStartPos_19)); }
	inline int32_t get__slideBufStartPos_19() const { return ____slideBufStartPos_19; }
	inline int32_t* get_address_of__slideBufStartPos_19() { return &____slideBufStartPos_19; }
	inline void set__slideBufStartPos_19(int32_t value)
	{
		____slideBufStartPos_19 = value;
	}

	inline static int32_t get_offset_of__slideBufFreePos_20() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____slideBufFreePos_20)); }
	inline int32_t get__slideBufFreePos_20() const { return ____slideBufFreePos_20; }
	inline int32_t* get_address_of__slideBufFreePos_20() { return &____slideBufFreePos_20; }
	inline void set__slideBufFreePos_20(int32_t value)
	{
		____slideBufFreePos_20 = value;
	}

	inline static int32_t get_offset_of__blockAndAuth_21() { return static_cast<int32_t>(offsetof(ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2, ____blockAndAuth_21)); }
	inline int32_t get__blockAndAuth_21() const { return ____blockAndAuth_21; }
	inline int32_t* get_address_of__blockAndAuth_21() { return &____blockAndAuth_21; }
	inline void set__blockAndAuth_21(int32_t value)
	{
		____blockAndAuth_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZIPAESSTREAM_TCD59113BEAB7E3B63907D92AD46F7503A03B2CB2_H
#ifndef KEYSREQUIREDEVENTHANDLER_T01BA6F773E29C468C7FD85BE6B93EE5043A521D9_H
#define KEYSREQUIREDEVENTHANDLER_T01BA6F773E29C468C7FD85BE6B93EE5043A521D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile_KeysRequiredEventHandler
struct  KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYSREQUIREDEVENTHANDLER_T01BA6F773E29C468C7FD85BE6B93EE5043A521D9_H
#ifndef SESSIONSTATECHANGED_T9084549A636BD45086D66CC6765DA8C3DD31066F_H
#define SESSIONSTATECHANGED_T9084549A636BD45086D66CC6765DA8C3DD31066F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo_SessionStateChanged
struct  SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATECHANGED_T9084549A636BD45086D66CC6765DA8C3DD31066F_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef WILLRENDERCANVASES_TBD5AD090B5938021DEAA679A5AEEA790F60A8BEE_H
#define WILLRENDERCANVASES_TBD5AD090B5938021DEAA679A5AEEA790F60A8BEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas_WillRenderCanvases
struct  WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WILLRENDERCANVASES_TBD5AD090B5938021DEAA679A5AEEA790F60A8BEE_H
#ifndef CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#define CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasRenderer
struct  CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:
	// System.Boolean UnityEngine.CanvasRenderer::<isMask>k__BackingField
	bool ___U3CisMaskU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CisMaskU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72, ___U3CisMaskU3Ek__BackingField_4)); }
	inline bool get_U3CisMaskU3Ek__BackingField_4() const { return ___U3CisMaskU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CisMaskU3Ek__BackingField_4() { return &___U3CisMaskU3Ek__BackingField_4; }
	inline void set_U3CisMaskU3Ek__BackingField_4(bool value)
	{
		___U3CisMaskU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#ifndef UPDATEDEVENTHANDLER_TB0230BC83686D7126AB4D3800A66351028CA514F_H
#define UPDATEDEVENTHANDLER_TB0230BC83686D7126AB4D3800A66351028CA514F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings_UpdatedEventHandler
struct  UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDEVENTHANDLER_TB0230BC83686D7126AB4D3800A66351028CA514F_H
#ifndef PKZIPCLASSICMANAGED_TE9CA988C413530D0AE504C4B2E14A9543E505DA4_H
#define PKZIPCLASSICMANAGED_TE9CA988C413530D0AE504C4B2E14A9543E505DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged
struct  PkzipClassicManaged_tE9CA988C413530D0AE504C4B2E14A9543E505DA4  : public PkzipClassic_tDAD4BF22079888AFE3944F718304C54DD2EEDF7E
{
public:
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::key_
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key__9;

public:
	inline static int32_t get_offset_of_key__9() { return static_cast<int32_t>(offsetof(PkzipClassicManaged_tE9CA988C413530D0AE504C4B2E14A9543E505DA4, ___key__9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key__9() const { return ___key__9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key__9() { return &___key__9; }
	inline void set_key__9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key__9 = value;
		Il2CppCodeGenWriteBarrier((&___key__9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKZIPCLASSICMANAGED_TE9CA988C413530D0AE504C4B2E14A9543E505DA4_H
#ifndef CANVAS_TBC28BF1DD8D8499A89B5781505833D3728CF8591_H
#define CANVAS_TBC28BF1DD8D8499A89B5781505833D3728CF8591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields
{
public:
	// UnityEngine.Canvas_WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * ___willRenderCanvases_4;

public:
	inline static int32_t get_offset_of_willRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields, ___willRenderCanvases_4)); }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * get_willRenderCanvases_4() const { return ___willRenderCanvases_4; }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE ** get_address_of_willRenderCanvases_4() { return &___willRenderCanvases_4; }
	inline void set_willRenderCanvases_4(WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * value)
	{
		___willRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_TBC28BF1DD8D8499A89B5781505833D3728CF8591_H
#ifndef CANVASGROUP_TE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_H
#define CANVASGROUP_TE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_TE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef EVENTTRIGGER_T594B0A2EC0E92150FF56250E207ECB7A90BB6298_H
#define EVENTTRIGGER_T594B0A2EC0E92150FF56250E207ECB7A90BB6298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger
struct  EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger_Entry> UnityEngine.EventSystems.EventTrigger::m_Delegates
	List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * ___m_Delegates_4;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger_Entry> UnityEngine.EventSystems.EventTrigger::delegates
	List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * ___delegates_5;

public:
	inline static int32_t get_offset_of_m_Delegates_4() { return static_cast<int32_t>(offsetof(EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298, ___m_Delegates_4)); }
	inline List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * get_m_Delegates_4() const { return ___m_Delegates_4; }
	inline List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 ** get_address_of_m_Delegates_4() { return &___m_Delegates_4; }
	inline void set_m_Delegates_4(List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * value)
	{
		___m_Delegates_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Delegates_4), value);
	}

	inline static int32_t get_offset_of_delegates_5() { return static_cast<int32_t>(offsetof(EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298, ___delegates_5)); }
	inline List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * get_delegates_5() const { return ___delegates_5; }
	inline List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 ** get_address_of_delegates_5() { return &___delegates_5; }
	inline void set_delegates_5(List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * value)
	{
		___delegates_5 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T594B0A2EC0E92150FF56250E207ECB7A90BB6298_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef EVENTSYSTEM_T06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_H
#define EVENTSYSTEM_T06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t1B3F60982C3189AF70B204EF3F19940A645EA02E * ___m_SystemInputModules_4;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 * ___m_CurrentInputModule_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_FirstSelected_7;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_8;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_9;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_CurrentSelected_10;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_11;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_12;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * ___m_DummyData_13;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_4() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_SystemInputModules_4)); }
	inline List_1_t1B3F60982C3189AF70B204EF3F19940A645EA02E * get_m_SystemInputModules_4() const { return ___m_SystemInputModules_4; }
	inline List_1_t1B3F60982C3189AF70B204EF3F19940A645EA02E ** get_address_of_m_SystemInputModules_4() { return &___m_SystemInputModules_4; }
	inline void set_m_SystemInputModules_4(List_1_t1B3F60982C3189AF70B204EF3F19940A645EA02E * value)
	{
		___m_SystemInputModules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInputModules_4), value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_5() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_CurrentInputModule_5)); }
	inline BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 * get_m_CurrentInputModule_5() const { return ___m_CurrentInputModule_5; }
	inline BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 ** get_address_of_m_CurrentInputModule_5() { return &___m_CurrentInputModule_5; }
	inline void set_m_CurrentInputModule_5(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 * value)
	{
		___m_CurrentInputModule_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentInputModule_5), value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_7() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_FirstSelected_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_FirstSelected_7() const { return ___m_FirstSelected_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_FirstSelected_7() { return &___m_FirstSelected_7; }
	inline void set_m_FirstSelected_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_FirstSelected_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_FirstSelected_7), value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_8() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_sendNavigationEvents_8)); }
	inline bool get_m_sendNavigationEvents_8() const { return ___m_sendNavigationEvents_8; }
	inline bool* get_address_of_m_sendNavigationEvents_8() { return &___m_sendNavigationEvents_8; }
	inline void set_m_sendNavigationEvents_8(bool value)
	{
		___m_sendNavigationEvents_8 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_9() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_DragThreshold_9)); }
	inline int32_t get_m_DragThreshold_9() const { return ___m_DragThreshold_9; }
	inline int32_t* get_address_of_m_DragThreshold_9() { return &___m_DragThreshold_9; }
	inline void set_m_DragThreshold_9(int32_t value)
	{
		___m_DragThreshold_9 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_10() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_CurrentSelected_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_CurrentSelected_10() const { return ___m_CurrentSelected_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_CurrentSelected_10() { return &___m_CurrentSelected_10; }
	inline void set_m_CurrentSelected_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_CurrentSelected_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentSelected_10), value);
	}

	inline static int32_t get_offset_of_m_HasFocus_11() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_HasFocus_11)); }
	inline bool get_m_HasFocus_11() const { return ___m_HasFocus_11; }
	inline bool* get_address_of_m_HasFocus_11() { return &___m_HasFocus_11; }
	inline void set_m_HasFocus_11(bool value)
	{
		___m_HasFocus_11 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_12() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_SelectionGuard_12)); }
	inline bool get_m_SelectionGuard_12() const { return ___m_SelectionGuard_12; }
	inline bool* get_address_of_m_SelectionGuard_12() { return &___m_SelectionGuard_12; }
	inline void set_m_SelectionGuard_12(bool value)
	{
		___m_SelectionGuard_12 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_13() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_DummyData_13)); }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * get_m_DummyData_13() const { return ___m_DummyData_13; }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 ** get_address_of_m_DummyData_13() { return &___m_DummyData_13; }
	inline void set_m_DummyData_13(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * value)
	{
		___m_DummyData_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_DummyData_13), value);
	}
};

struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_tE4E9EE9F348ABAD1007C663DD77A14907CCD9A79 * ___m_EventSystems_6;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * ___s_RaycastComparer_14;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::<>f__mgU24cache0
	Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * ___U3CU3Ef__mgU24cache0_15;

public:
	inline static int32_t get_offset_of_m_EventSystems_6() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields, ___m_EventSystems_6)); }
	inline List_1_tE4E9EE9F348ABAD1007C663DD77A14907CCD9A79 * get_m_EventSystems_6() const { return ___m_EventSystems_6; }
	inline List_1_tE4E9EE9F348ABAD1007C663DD77A14907CCD9A79 ** get_address_of_m_EventSystems_6() { return &___m_EventSystems_6; }
	inline void set_m_EventSystems_6(List_1_tE4E9EE9F348ABAD1007C663DD77A14907CCD9A79 * value)
	{
		___m_EventSystems_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystems_6), value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_14() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields, ___s_RaycastComparer_14)); }
	inline Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * get_s_RaycastComparer_14() const { return ___s_RaycastComparer_14; }
	inline Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 ** get_address_of_s_RaycastComparer_14() { return &___s_RaycastComparer_14; }
	inline void set_s_RaycastComparer_14(Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * value)
	{
		___s_RaycastComparer_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_RaycastComparer_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_15() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields, ___U3CU3Ef__mgU24cache0_15)); }
	inline Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * get_U3CU3Ef__mgU24cache0_15() const { return ___U3CU3Ef__mgU24cache0_15; }
	inline Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 ** get_address_of_U3CU3Ef__mgU24cache0_15() { return &___U3CU3Ef__mgU24cache0_15; }
	inline void set_U3CU3Ef__mgU24cache0_15(Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * value)
	{
		___U3CU3Ef__mgU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEM_T06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (AdditionalCanvasShaderChannels_t2576909EF4884007D0786E0FEEB894B54C61107E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2700[7] = 
{
	AdditionalCanvasShaderChannels_t2576909EF4884007D0786E0FEEB894B54C61107E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591), -1, sizeof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2701[1] = 
{
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields::get_offset_of_willRenderCanvases_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (UISystemProfilerApi_tD33E558B2D0176096E5DB375956ACA9F03678F1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2704[3] = 
{
	SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[1] = 
{
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72::get_offset_of_U3CisMaskU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA), -1, sizeof(RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2708[1] = 
{
	RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (U3CModuleU3E_tCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (ContinuousEvent_tBAB6336255F3FC327CBA03CE368CD4D8D027107A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2711[5] = 
{
	AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C), -1, sizeof(AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2712[1] = 
{
	AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields::get_offset_of_sessionStateChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED), -1, sizeof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2714[3] = 
{
	RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields::get_offset_of_Updated_0(),
	RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields::get_offset_of_BeforeFetchFromServer_1(),
	RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields::get_offset_of_Completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A), sizeof(RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2716[2] = 
{
	RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A::get_offset_of_m_Ptr_0(),
	RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A::get_offset_of_Updated_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (U3CModuleU3E_t1559CC8A802937295A65A1DCA2F81D3D5CEDAA58), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (UnityWebRequestTexture_t2F2C62D3509DDAF6615855FA3428F527BF166F09), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (DownloadHandlerTexture_tDD365789CD15EDFB8582612507C8EE34C62A3DE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[3] = 
{
	DownloadHandlerTexture_tDD365789CD15EDFB8582612507C8EE34C62A3DE4::get_offset_of_mTexture_1(),
	DownloadHandlerTexture_tDD365789CD15EDFB8582612507C8EE34C62A3DE4::get_offset_of_mHasTexture_2(),
	DownloadHandlerTexture_tDD365789CD15EDFB8582612507C8EE34C62A3DE4::get_offset_of_mNonReadable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (U3CModuleU3E_tB054F17A779AC945E3659AF119A96DB806541AF9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (XRSettings_tB57FCBA5B804996700C097CC13B658E7BD43D874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A), -1, sizeof(XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2722[1] = 
{
	XRDevice_t392FCA3D1DCEB95FF500C8F374C88B034C31DF4A_StaticFields::get_offset_of_deviceLoaded_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (U3CModuleU3E_tAA90CDF50306A16DF6C29043AB49497B8E0C9DA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (SharpZipBaseException_t822541D5F0EC9FCA3550F00DCF0883E987E5045F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[1] = 
{
	Adler32_t1764266FFA97C6DE074ACBE8444B9BC93C9F2033::get_offset_of_checksum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F), -1, sizeof(Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2728[1] = 
{
	Crc32_t92BAEC6D06DD30E8D7190D8C772C5079C6FEEB0F_StaticFields::get_offset_of_CrcTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (StreamUtils_tC8B2EC2CC286B4E36E2D78C3D96D64770DC3D6F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (PkzipClassic_tDAD4BF22079888AFE3944F718304C54DD2EEDF7E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[1] = 
{
	PkzipClassicCryptoBase_t82E06A5F31748B8C6AE52F3C9863E2DACDCE4C2B::get_offset_of_keys_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (PkzipClassicEncryptCryptoTransform_t050C6231405F6F498F285DF8D3FCB2DCD6EB2EE0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (PkzipClassicDecryptCryptoTransform_tBB3B4209A63C60CB64240640E0CBA21626ACA93E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (PkzipClassicManaged_tE9CA988C413530D0AE504C4B2E14A9543E505DA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[1] = 
{
	PkzipClassicManaged_tE9CA988C413530D0AE504C4B2E14A9543E505DA4::get_offset_of_key__9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[6] = 
{
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__stream_16(),
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__transform_17(),
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__slideBuffer_18(),
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__slideBufStartPos_19(),
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__slideBufFreePos_20(),
	ZipAESStream_tCD59113BEAB7E3B63907D92AD46F7503A03B2CB2::get_offset_of__blockAndAuth_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[9] = 
{
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__blockSize_0(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__encryptor_1(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__counterNonce_2(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__encryptBuffer_3(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__encrPos_4(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__pwdVerifier_5(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__hmacsha1_6(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__finalised_7(),
	ZipAESTransform_tD2F237B71B8532EC36AAF6E45E4FFAB185301195::get_offset_of__writeMode_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[5] = 
{
	InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6::get_offset_of_inf_4(),
	InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6::get_offset_of_inputBuffer_5(),
	InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6::get_offset_of_baseInputStream_6(),
	InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6::get_offset_of_isClosed_7(),
	InflaterInputStream_t73956344292D849DFE9A530C35B00DBE415C7DF6::get_offset_of_isStreamOwner_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[7] = 
{
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_rawLength_0(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_rawData_1(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_clearTextLength_2(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_clearText_3(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_available_4(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_cryptoTransform_5(),
	InflaterInputBuffer_t7B343D95793E5A8D51C29D0625C8F4A2AFEAE6A3::get_offset_of_inputStream_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[3] = 
{
	OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581::get_offset_of_window_0(),
	OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581::get_offset_of_windowEnd_1(),
	OutputWindow_t7CB804720F02D63ECB8018477FF479C596859581::get_offset_of_windowFilled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[5] = 
{
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5::get_offset_of_window__0(),
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5::get_offset_of_windowStart__1(),
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5::get_offset_of_windowEnd__2(),
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5::get_offset_of_buffer__3(),
	StreamManipulator_tBAD267162B2327BE4350A4407E38A40C613CFAB5::get_offset_of_bitsInBuffer__4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD), -1, sizeof(DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2742[6] = 
{
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_BL_ORDER_0(),
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_bit4Reverse_1(),
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_staticLCodes_2(),
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_staticLLength_3(),
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_staticDCodes_4(),
	DeflaterHuffman_t07ABC2467D43109DA82829D4D0476554059A12BD_StaticFields::get_offset_of_staticDLength_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB), -1, sizeof(Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2743[20] = 
{
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields::get_offset_of_CPLENS_0(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields::get_offset_of_CPLEXT_1(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields::get_offset_of_CPDIST_2(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB_StaticFields::get_offset_of_CPDEXT_3(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_mode_4(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_readAdler_5(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_neededBits_6(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_repLength_7(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_repDist_8(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_uncomprLen_9(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_isLastBlock_10(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_totalOut_11(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_totalIn_12(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_noHeader_13(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_input_14(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_outputWindow_15(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_dynHeader_16(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_litlenTree_17(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_distTree_18(),
	Inflater_t69AF9807C58C78BD7FCF1A0372F416961C1C73CB::get_offset_of_adler_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3), -1, sizeof(InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2744[14] = 
{
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields::get_offset_of_repMin_0(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields::get_offset_of_repBits_1(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3_StaticFields::get_offset_of_BL_ORDER_2(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_blLens_3(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_litdistLens_4(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_blTree_5(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_mode_6(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_lnum_7(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_dnum_8(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_blnum_9(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_num_10(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_repSymbol_11(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_lastLen_12(),
	InflaterDynHeader_t4EEFC2F61C40AFF82DF7ED1723CD03E8D9EE73A3::get_offset_of_ptr_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B), -1, sizeof(InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2745[3] = 
{
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B::get_offset_of_tree_0(),
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields::get_offset_of_defLitLenTree_1(),
	InflaterHuffmanTree_t3AA390BD9908A7F2AECCD7C72A0D011A05D0386B_StaticFields::get_offset_of_defDistTree_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (UseZip64_t64A23EE068A11F6879053B3C4EFBF67F75D3B884)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2747[4] = 
{
	UseZip64_t64A23EE068A11F6879053B3C4EFBF67F75D3B884::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (CompressionMethod_t1F31EE4DB3B1958C77654ED6CDA048854A028ECD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2748[6] = 
{
	CompressionMethod_t1F31EE4DB3B1958C77654ED6CDA048854A028ECD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863), -1, sizeof(ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2749[1] = 
{
	ZipConstants_t346D2EB237FE5E5068BED97F246531E08A363863_StaticFields::get_offset_of_defaultCodePage_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[19] = 
{
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_known_0(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_externalFileAttributes_1(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_versionMadeBy_2(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_name_3(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_size_4(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_compressedSize_5(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_versionToExtract_6(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_crc_7(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_dosTime_8(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_method_9(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_extra_10(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_comment_11(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_flags_12(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_zipFileIndex_13(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_offset_14(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_forceZip64__15(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of_cryptoCheckValue__16(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of__aesVer_17(),
	ZipEntry_t0C490E8147650DD0F764C17C5D545E633E38E9E5::get_offset_of__aesEncryptionStrength_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (Known_tC640ED74F2497140C21D74C29A9E194AC89EF2FB)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2751[7] = 
{
	Known_tC640ED74F2497140C21D74C29A9E194AC89EF2FB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[3] = 
{
	ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929::get_offset_of_nameTransform__0(),
	ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929::get_offset_of_fixedDateTime__1(),
	ZipEntryFactory_tD953786FCD19BFC64F0A014807EB7B46B0BEF929::get_offset_of_getAttributes__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (ZipException_t555EB5B9EA2BB8DF941C19C19F3BBA466EE7D944), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[5] = 
{
	ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0::get_offset_of__index_0(),
	ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0::get_offset_of__readValueStart_1(),
	ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0::get_offset_of__readValueLength_2(),
	ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0::get_offset_of__newEntry_3(),
	ZipExtraData_t2BE422C50624DC89A6D09B5207221310DC3C82A0::get_offset_of__data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[2] = 
{
	KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148::get_offset_of_fileName_1(),
	KeysRequiredEventArgs_t333068E27532BA6462A1C84D7D3CCE8904FC7148::get_offset_of_key_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[17] = 
{
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_KeysRequired_0(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_isDisposed__1(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_comment__2(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_rawPassword__3(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_baseStream__4(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_isStreamOwner_5(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_offsetOfFirstEntry_6(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_entries__7(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_key_8(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_isNewArchive__9(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_useZip64__10(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_updates__11(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_updateIndex__12(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_archiveStorage__13(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_updateDataSource__14(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_bufferSize__15(),
	ZipFile_tEFDC6C532094F9A5F6185F4E69E340602984E28F::get_offset_of_updateEntryFactory__16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (KeysRequiredEventHandler_t01BA6F773E29C468C7FD85BE6B93EE5043A521D9), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (HeaderTest_t98D72CE7316D175D3364E46D294E73256D1532D8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2758[3] = 
{
	HeaderTest_t98D72CE7316D175D3364E46D294E73256D1532D8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[2] = 
{
	ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F::get_offset_of_array_0(),
	ZipEntryEnumerator_tD1C0F596AD8074627C711424B3F64891F3B2037F::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[6] = 
{
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_zipFile__4(),
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_baseStream__5(),
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_start__6(),
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_length__7(),
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_readPos__8(),
	PartialInputStream_t5B3105C371367E3A43ACB6C9D41A7B3C1BAC8195::get_offset_of_end__9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[2] = 
{
	ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12::get_offset_of_isOwner__4(),
	ZipHelperStream_t5EBF734E826C8FAB30C920728A06E3291AB40B12::get_offset_of_stream__5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF), -1, sizeof(ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2764[2] = 
{
	ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields::get_offset_of_InvalidEntryChars_0(),
	ZipNameTransform_t8016B5EE6B0807E685F47F38135D7F000D18BAEF_StaticFields::get_offset_of_InvalidEntryCharsRelaxed_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC), -1, sizeof(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2765[10] = 
{
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004f9U2D1_0(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x600029dU2D1_1(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x600029dU2D2_2(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004fdU2D1_3(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004fdU2D2_4(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004fdU2D3_5(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004fdU2D4_6(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004feU2D1_7(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004feU2D2_8(),
	U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_tF556D54546BC2FC0638B99AF7BAF448A985AB3EC_StaticFields::get_offset_of_U24U24method0x60004feU2D3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (__StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D1024_tA2012153EE60F5C9CA38878077580795956DBCD1 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t3A8D17C4EB5C5ADF5E16653E215E3D6FBC3D0DB1 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D76_t06243B400D517E4BAA79958C008A9C655F63F936 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (__StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D16_tA74794CC2DB67F8B4B9BCE862B22BC6D2497E777 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D116_tDF00214C4AC7272026E840470B881841013B7244 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_t8DDD6C9514870A994B892ACC8E5E40366D37C155 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (U3CModuleU3E_t76DD45B11E728799BA16B6E93B81827DD86E5AEE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77), -1, sizeof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2791[12] = 
{
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_SystemInputModules_4(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_CurrentInputModule_5(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields::get_offset_of_m_EventSystems_6(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_FirstSelected_7(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_sendNavigationEvents_8(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_DragThreshold_9(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_CurrentSelected_10(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_HasFocus_11(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_SelectionGuard_12(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_DummyData_13(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields::get_offset_of_s_RaycastComparer_14(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[2] = 
{
	EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298::get_offset_of_m_Delegates_4(),
	EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298::get_offset_of_delegates_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[2] = 
{
	Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E::get_offset_of_eventID_0(),
	Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2795[18] = 
{
	EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985), -1, sizeof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2796[36] = 
{
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2798[6] = 
{
	MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[10] = 
{
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_module_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_index_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
