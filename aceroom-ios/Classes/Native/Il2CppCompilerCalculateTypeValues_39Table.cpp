﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Boomlagoon.TextFx.JSON.tfxJSONArray
struct tfxJSONArray_t44B4042249796AB3367557BDC64EC7A6B1DA13CC;
// Boomlagoon.TextFx.JSON.tfxJSONObject
struct tfxJSONObject_tB6F915240278BF8031DC3EF165A5F66CF0122A8B;
// HSVPickerDemo.HSVDragger
struct HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5;
// HSVPickerDemo.HSVPicker
struct HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565;
// HSVPickerDemo.HSVSliderEvent
struct HSVSliderEvent_tE0150BE444ACECCCD174D0CA99B45FF973AC8318;
// HSVPickerDemo.HexRGB
struct HexRGB_t3457F98D908B302FF235748F6729A0240D21680B;
// HsvSliderPicker
struct HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.Int32>
struct Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B;
// System.Action`1<System.Single>
struct Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B;
// System.Action`1<UnityEngine.Color>
struct Action_1_tB42F900CEA2D8B71F6167E641B4822FF8FB34159;
// System.Action`1<UnityEngine.Font>
struct Action_1_t795662E553415ECF2DD0F8EEB9BA170C3670F37C;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_tE2F19E30ECDF39669C80D0E7DA21064D10C1EE2F;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F;
// System.Collections.Generic.Dictionary`2<System.Int32,TextFx.CustomCharacterInfo>
struct Dictionary_2_t07E58DC3F1C5121D53C052238BC76D426688437D;
// System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.TextFx.JSON.tfxJSONValue>
struct IDictionary_2_t3FB95CAE27E5E0D04A224E31835FE80AE57C66DD;
// System.Collections.Generic.List`1<Boomlagoon.TextFx.JSON.tfxJSONValue>
struct List_1_tA2C7EEEB4A8891D3F3EC6D2C46F0AB4B71DE5F47;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37;
// System.Collections.Generic.List`1<TextFx.ActionLoopCycle>
struct List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3;
// System.Collections.Generic.List`1<TextFx.AudioEffectSetup>
struct List_1_tA015857CE716DE3A369F3779EFF10F8EC7ACDE7F;
// System.Collections.Generic.List`1<TextFx.LetterAction>
struct List_1_t92B02A7B4F386D14AA96EE6BA827436C9306F58F;
// System.Collections.Generic.List`1<TextFx.LetterAnimation>
struct List_1_t3A6363802189E0E5D037C5C09A83E1220B0F1828;
// System.Collections.Generic.List`1<TextFx.ParticleEffectInstanceManager>
struct List_1_tAC55CA1D84E616FB5D8E8CB8B51335E6CC083B3F;
// System.Collections.Generic.List`1<TextFx.ParticleEffectSetup>
struct List_1_tAD371B0AE32A79BD133FF2968C76AC48994D8026;
// System.Collections.Generic.List`1<TextFx.PresetEffectSetting>
struct List_1_t766AD6FD03C5F589C64C80F20B2A1D155B842B13;
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t2A1444170C4D0EF0D4F6CC690367E1792E8C0992;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t64BA96BFC713F221050385E91C868CE455C245D6;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.FontWeight[]
struct FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B;
// TMPro.RichTextTagAttribute[]
struct RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652;
// TMPro.TMP_Character
struct TMP_Character_t1875AACA978396521498D6A699052C187903553D;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487;
// TMPro.TMP_SubMeshUI[]
struct TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E;
// TMPro.TMP_SubMesh[]
struct TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD;
// TMPro.TMP_Text/UnicodeChar[]
struct UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505;
// TMPro.TMP_TextElement
struct TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B;
// TextFx.ActionColorProgression
struct ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA;
// TextFx.ActionFloatProgression
struct ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3;
// TextFx.ActionPositionVector3Progression
struct ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147;
// TextFx.ActionVector3Progression
struct ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C;
// TextFx.AnimationProgressionVariables
struct AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311;
// TextFx.AudioEffectSetup
struct AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F;
// TextFx.AxisEasingOverrideData
struct AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B;
// TextFx.CustomCharacterInfo
struct CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3;
// TextFx.CustomFontCharacterData
struct CustomFontCharacterData_t1391FAABB7293C043AAF8845B64651AF4ED69E99;
// TextFx.LetterAction
struct LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67;
// TextFx.LetterAnimation
struct LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9;
// TextFx.LetterSetup
struct LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E;
// TextFx.LetterSetup[]
struct LetterSetupU5BU5D_t64E53E2965343DF3FC6FB2209365EDCE19A74C07;
// TextFx.ParticleEffectSetup
struct ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F;
// TextFx.TextFxAnimationInterface
struct TextFxAnimationInterface_tA435B89D25E3BBBB678C76327D398D70140E9FEC;
// TextFx.TextFxAnimationManager
struct TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399;
// TextFx.TextFxAnimationManager/PresetAnimationSection
struct PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E;
// TextFx.TextFxNative
struct TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.CombineInstance[]
struct CombineInstanceU5BU5D_t3865A4038A85A764FCE1EE0C134B77C42F4610DA;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextAsset
struct TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.BoxSlider
struct BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A;
// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct BoxSliderEvent_t2E59E1AD1FFFC4D521C2865063F16C634BF59706;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// VertexColour
struct VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333;
// VertexColour[]
struct VertexColourU5BU5D_t1555A77080D0FBDA0DAC227CE369D5BDB2821C0C;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BOOMLAGOONEXTENSIONS_T6B8597A20A88CCA986B73F4B759D7D20CC230AE6_H
#define BOOMLAGOONEXTENSIONS_T6B8597A20A88CCA986B73F4B759D7D20CC230AE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.TextFx.JSON.BoomlagoonExtensions
struct  BoomlagoonExtensions_t6B8597A20A88CCA986B73F4B759D7D20CC230AE6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOMLAGOONEXTENSIONS_T6B8597A20A88CCA986B73F4B759D7D20CC230AE6_H
#ifndef JSONLOGGER_T4DFCB3FA6AEAFA66AFC4775CD95012040815E1EF_H
#define JSONLOGGER_T4DFCB3FA6AEAFA66AFC4775CD95012040815E1EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.TextFx.JSON.JSONLogger
struct  JSONLogger_t4DFCB3FA6AEAFA66AFC4775CD95012040815E1EF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLOGGER_T4DFCB3FA6AEAFA66AFC4775CD95012040815E1EF_H
#ifndef TFXJSONARRAY_T44B4042249796AB3367557BDC64EC7A6B1DA13CC_H
#define TFXJSONARRAY_T44B4042249796AB3367557BDC64EC7A6B1DA13CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.TextFx.JSON.tfxJSONArray
struct  tfxJSONArray_t44B4042249796AB3367557BDC64EC7A6B1DA13CC  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Boomlagoon.TextFx.JSON.tfxJSONValue> Boomlagoon.TextFx.JSON.tfxJSONArray::values
	List_1_tA2C7EEEB4A8891D3F3EC6D2C46F0AB4B71DE5F47 * ___values_0;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(tfxJSONArray_t44B4042249796AB3367557BDC64EC7A6B1DA13CC, ___values_0)); }
	inline List_1_tA2C7EEEB4A8891D3F3EC6D2C46F0AB4B71DE5F47 * get_values_0() const { return ___values_0; }
	inline List_1_tA2C7EEEB4A8891D3F3EC6D2C46F0AB4B71DE5F47 ** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(List_1_tA2C7EEEB4A8891D3F3EC6D2C46F0AB4B71DE5F47 * value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TFXJSONARRAY_T44B4042249796AB3367557BDC64EC7A6B1DA13CC_H
#ifndef TFXJSONOBJECT_TB6F915240278BF8031DC3EF165A5F66CF0122A8B_H
#define TFXJSONOBJECT_TB6F915240278BF8031DC3EF165A5F66CF0122A8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.TextFx.JSON.tfxJSONObject
struct  tfxJSONObject_tB6F915240278BF8031DC3EF165A5F66CF0122A8B  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.TextFx.JSON.tfxJSONValue> Boomlagoon.TextFx.JSON.tfxJSONObject::values
	RuntimeObject* ___values_0;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(tfxJSONObject_tB6F915240278BF8031DC3EF165A5F66CF0122A8B, ___values_0)); }
	inline RuntimeObject* get_values_0() const { return ___values_0; }
	inline RuntimeObject** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(RuntimeObject* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TFXJSONOBJECT_TB6F915240278BF8031DC3EF165A5F66CF0122A8B_H
#ifndef HSVUTIL_T0E0EA69C0A10A5CD024A61DDBB332C988626CB20_H
#define HSVUTIL_T0E0EA69C0A10A5CD024A61DDBB332C988626CB20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVPickerDemo.HSVUtil
struct  HSVUtil_t0E0EA69C0A10A5CD024A61DDBB332C988626CB20  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVUTIL_T0E0EA69C0A10A5CD024A61DDBB332C988626CB20_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANIMATIONPROGRESSIONVARIABLES_TACC3BB5B42E1198FB274379EE8A8BD2667149311_H
#define ANIMATIONPROGRESSIONVARIABLES_TACC3BB5B42E1198FB274379EE8A8BD2667149311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.AnimationProgressionVariables
struct  AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311  : public RuntimeObject
{
public:
	// System.Int32 TextFx.AnimationProgressionVariables::m_letter_value
	int32_t ___m_letter_value_0;
	// System.Int32 TextFx.AnimationProgressionVariables::m_letter_value_inc_white_space
	int32_t ___m_letter_value_inc_white_space_1;
	// System.Int32 TextFx.AnimationProgressionVariables::m_word_value
	int32_t ___m_word_value_2;
	// System.Int32 TextFx.AnimationProgressionVariables::m_line_value
	int32_t ___m_line_value_3;

public:
	inline static int32_t get_offset_of_m_letter_value_0() { return static_cast<int32_t>(offsetof(AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311, ___m_letter_value_0)); }
	inline int32_t get_m_letter_value_0() const { return ___m_letter_value_0; }
	inline int32_t* get_address_of_m_letter_value_0() { return &___m_letter_value_0; }
	inline void set_m_letter_value_0(int32_t value)
	{
		___m_letter_value_0 = value;
	}

	inline static int32_t get_offset_of_m_letter_value_inc_white_space_1() { return static_cast<int32_t>(offsetof(AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311, ___m_letter_value_inc_white_space_1)); }
	inline int32_t get_m_letter_value_inc_white_space_1() const { return ___m_letter_value_inc_white_space_1; }
	inline int32_t* get_address_of_m_letter_value_inc_white_space_1() { return &___m_letter_value_inc_white_space_1; }
	inline void set_m_letter_value_inc_white_space_1(int32_t value)
	{
		___m_letter_value_inc_white_space_1 = value;
	}

	inline static int32_t get_offset_of_m_word_value_2() { return static_cast<int32_t>(offsetof(AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311, ___m_word_value_2)); }
	inline int32_t get_m_word_value_2() const { return ___m_word_value_2; }
	inline int32_t* get_address_of_m_word_value_2() { return &___m_word_value_2; }
	inline void set_m_word_value_2(int32_t value)
	{
		___m_word_value_2 = value;
	}

	inline static int32_t get_offset_of_m_line_value_3() { return static_cast<int32_t>(offsetof(AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311, ___m_line_value_3)); }
	inline int32_t get_m_line_value_3() const { return ___m_line_value_3; }
	inline int32_t* get_address_of_m_line_value_3() { return &___m_line_value_3; }
	inline void set_m_line_value_3(int32_t value)
	{
		___m_line_value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPROGRESSIONVARIABLES_TACC3BB5B42E1198FB274379EE8A8BD2667149311_H
#ifndef CUSTOMFONTCHARACTERDATA_T1391FAABB7293C043AAF8845B64651AF4ED69E99_H
#define CUSTOMFONTCHARACTERDATA_T1391FAABB7293C043AAF8845B64651AF4ED69E99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.CustomFontCharacterData
struct  CustomFontCharacterData_t1391FAABB7293C043AAF8845B64651AF4ED69E99  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,TextFx.CustomCharacterInfo> TextFx.CustomFontCharacterData::m_character_infos
	Dictionary_2_t07E58DC3F1C5121D53C052238BC76D426688437D * ___m_character_infos_0;

public:
	inline static int32_t get_offset_of_m_character_infos_0() { return static_cast<int32_t>(offsetof(CustomFontCharacterData_t1391FAABB7293C043AAF8845B64651AF4ED69E99, ___m_character_infos_0)); }
	inline Dictionary_2_t07E58DC3F1C5121D53C052238BC76D426688437D * get_m_character_infos_0() const { return ___m_character_infos_0; }
	inline Dictionary_2_t07E58DC3F1C5121D53C052238BC76D426688437D ** get_address_of_m_character_infos_0() { return &___m_character_infos_0; }
	inline void set_m_character_infos_0(Dictionary_2_t07E58DC3F1C5121D53C052238BC76D426688437D * value)
	{
		___m_character_infos_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_character_infos_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMFONTCHARACTERDATA_T1391FAABB7293C043AAF8845B64651AF4ED69E99_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T38A971FAC0C1A86373E6D4F7734D3A2873622454_H
#define U3CU3EC__DISPLAYCLASS9_0_T38A971FAC0C1A86373E6D4F7734D3A2873622454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.PresetEffectSetting_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t38A971FAC0C1A86373E6D4F7734D3A2873622454  : public RuntimeObject
{
public:
	// TextFx.LetterAction TextFx.PresetEffectSetting_<>c__DisplayClass9_0::letterAction
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67 * ___letterAction_0;
	// TextFx.TextFxAnimationManager TextFx.PresetEffectSetting_<>c__DisplayClass9_0::animation_manager
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * ___animation_manager_1;

public:
	inline static int32_t get_offset_of_letterAction_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t38A971FAC0C1A86373E6D4F7734D3A2873622454, ___letterAction_0)); }
	inline LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67 * get_letterAction_0() const { return ___letterAction_0; }
	inline LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67 ** get_address_of_letterAction_0() { return &___letterAction_0; }
	inline void set_letterAction_0(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67 * value)
	{
		___letterAction_0 = value;
		Il2CppCodeGenWriteBarrier((&___letterAction_0), value);
	}

	inline static int32_t get_offset_of_animation_manager_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t38A971FAC0C1A86373E6D4F7734D3A2873622454, ___animation_manager_1)); }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * get_animation_manager_1() const { return ___animation_manager_1; }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 ** get_address_of_animation_manager_1() { return &___animation_manager_1; }
	inline void set_animation_manager_1(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * value)
	{
		___animation_manager_1 = value;
		Il2CppCodeGenWriteBarrier((&___animation_manager_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T38A971FAC0C1A86373E6D4F7734D3A2873622454_H
#ifndef TMPTEXTDATAHANDLER_T3A3363A412456C1498F99659F551A17631C19B95_H
#define TMPTEXTDATAHANDLER_T3A3363A412456C1498F99659F551A17631C19B95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TMPTextDataHandler
struct  TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] TextFx.TMPTextDataHandler::m_posData
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_posData_0;
	// UnityEngine.Color32[] TextFx.TMPTextDataHandler::m_colData
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_colData_1;
	// System.Int32 TextFx.TMPTextDataHandler::m_numBaseLetters
	int32_t ___m_numBaseLetters_2;
	// System.Int32 TextFx.TMPTextDataHandler::m_extraVertsPerLetter
	int32_t ___m_extraVertsPerLetter_3;
	// System.Int32 TextFx.TMPTextDataHandler::m_totalVertsPerLetter
	int32_t ___m_totalVertsPerLetter_4;
	// System.Int32 TextFx.TMPTextDataHandler::m_numExtraQuads
	int32_t ___m_numExtraQuads_5;

public:
	inline static int32_t get_offset_of_m_posData_0() { return static_cast<int32_t>(offsetof(TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95, ___m_posData_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_posData_0() const { return ___m_posData_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_posData_0() { return &___m_posData_0; }
	inline void set_m_posData_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_posData_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_posData_0), value);
	}

	inline static int32_t get_offset_of_m_colData_1() { return static_cast<int32_t>(offsetof(TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95, ___m_colData_1)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_colData_1() const { return ___m_colData_1; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_colData_1() { return &___m_colData_1; }
	inline void set_m_colData_1(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_colData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_colData_1), value);
	}

	inline static int32_t get_offset_of_m_numBaseLetters_2() { return static_cast<int32_t>(offsetof(TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95, ___m_numBaseLetters_2)); }
	inline int32_t get_m_numBaseLetters_2() const { return ___m_numBaseLetters_2; }
	inline int32_t* get_address_of_m_numBaseLetters_2() { return &___m_numBaseLetters_2; }
	inline void set_m_numBaseLetters_2(int32_t value)
	{
		___m_numBaseLetters_2 = value;
	}

	inline static int32_t get_offset_of_m_extraVertsPerLetter_3() { return static_cast<int32_t>(offsetof(TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95, ___m_extraVertsPerLetter_3)); }
	inline int32_t get_m_extraVertsPerLetter_3() const { return ___m_extraVertsPerLetter_3; }
	inline int32_t* get_address_of_m_extraVertsPerLetter_3() { return &___m_extraVertsPerLetter_3; }
	inline void set_m_extraVertsPerLetter_3(int32_t value)
	{
		___m_extraVertsPerLetter_3 = value;
	}

	inline static int32_t get_offset_of_m_totalVertsPerLetter_4() { return static_cast<int32_t>(offsetof(TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95, ___m_totalVertsPerLetter_4)); }
	inline int32_t get_m_totalVertsPerLetter_4() const { return ___m_totalVertsPerLetter_4; }
	inline int32_t* get_address_of_m_totalVertsPerLetter_4() { return &___m_totalVertsPerLetter_4; }
	inline void set_m_totalVertsPerLetter_4(int32_t value)
	{
		___m_totalVertsPerLetter_4 = value;
	}

	inline static int32_t get_offset_of_m_numExtraQuads_5() { return static_cast<int32_t>(offsetof(TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95, ___m_numExtraQuads_5)); }
	inline int32_t get_m_numExtraQuads_5() const { return ___m_numExtraQuads_5; }
	inline int32_t* get_address_of_m_numExtraQuads_5() { return &___m_numExtraQuads_5; }
	inline void set_m_numExtraQuads_5(int32_t value)
	{
		___m_numExtraQuads_5 = value;
	}
};

struct TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95_StaticFields
{
public:
	// System.Int32[] TextFx.TMPTextDataHandler::baseColVertIndexes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___baseColVertIndexes_6;

public:
	inline static int32_t get_offset_of_baseColVertIndexes_6() { return static_cast<int32_t>(offsetof(TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95_StaticFields, ___baseColVertIndexes_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_baseColVertIndexes_6() const { return ___baseColVertIndexes_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_baseColVertIndexes_6() { return &___baseColVertIndexes_6; }
	inline void set_baseColVertIndexes_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___baseColVertIndexes_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseColVertIndexes_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPTEXTDATAHANDLER_T3A3363A412456C1498F99659F551A17631C19B95_H
#ifndef U3CPLAYANIMATIONAFTERDELAYU3ED__122_T9BAF09729A5CE37CEDA32859771F52DBC04D95C0_H
#define U3CPLAYANIMATIONAFTERDELAYU3ED__122_T9BAF09729A5CE37CEDA32859771F52DBC04D95C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxAnimationManager_<PlayAnimationAfterDelay>d__122
struct  U3CPlayAnimationAfterDelayU3Ed__122_t9BAF09729A5CE37CEDA32859771F52DBC04D95C0  : public RuntimeObject
{
public:
	// System.Int32 TextFx.TextFxAnimationManager_<PlayAnimationAfterDelay>d__122::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TextFx.TextFxAnimationManager_<PlayAnimationAfterDelay>d__122::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TextFx.TextFxAnimationManager TextFx.TextFxAnimationManager_<PlayAnimationAfterDelay>d__122::<>4__this
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * ___U3CU3E4__this_2;
	// System.Single TextFx.TextFxAnimationManager_<PlayAnimationAfterDelay>d__122::delay
	float ___delay_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPlayAnimationAfterDelayU3Ed__122_t9BAF09729A5CE37CEDA32859771F52DBC04D95C0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPlayAnimationAfterDelayU3Ed__122_t9BAF09729A5CE37CEDA32859771F52DBC04D95C0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPlayAnimationAfterDelayU3Ed__122_t9BAF09729A5CE37CEDA32859771F52DBC04D95C0, ___U3CU3E4__this_2)); }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(U3CPlayAnimationAfterDelayU3Ed__122_t9BAF09729A5CE37CEDA32859771F52DBC04D95C0, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYANIMATIONAFTERDELAYU3ED__122_T9BAF09729A5CE37CEDA32859771F52DBC04D95C0_H
#ifndef PRESETANIMATIONSECTION_T7775917B7A5DA6084D75532BD1D812D5F5357A2E_H
#define PRESETANIMATIONSECTION_T7775917B7A5DA6084D75532BD1D812D5F5357A2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxAnimationManager_PresetAnimationSection
struct  PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TextFx.PresetEffectSetting> TextFx.TextFxAnimationManager_PresetAnimationSection::m_preset_effect_settings
	List_1_t766AD6FD03C5F589C64C80F20B2A1D155B842B13 * ___m_preset_effect_settings_0;
	// System.Boolean TextFx.TextFxAnimationManager_PresetAnimationSection::m_active
	bool ___m_active_1;
	// System.Int32 TextFx.TextFxAnimationManager_PresetAnimationSection::m_start_action
	int32_t ___m_start_action_2;
	// System.Int32 TextFx.TextFxAnimationManager_PresetAnimationSection::m_num_actions
	int32_t ___m_num_actions_3;
	// System.Int32 TextFx.TextFxAnimationManager_PresetAnimationSection::m_start_loop
	int32_t ___m_start_loop_4;
	// System.Int32 TextFx.TextFxAnimationManager_PresetAnimationSection::m_num_loops
	int32_t ___m_num_loops_5;
	// System.Boolean TextFx.TextFxAnimationManager_PresetAnimationSection::m_exit_pause
	bool ___m_exit_pause_6;
	// System.Single TextFx.TextFxAnimationManager_PresetAnimationSection::m_exit_pause_duration
	float ___m_exit_pause_duration_7;
	// System.Boolean TextFx.TextFxAnimationManager_PresetAnimationSection::m_repeat
	bool ___m_repeat_8;
	// System.Int32 TextFx.TextFxAnimationManager_PresetAnimationSection::m_repeat_count
	int32_t ___m_repeat_count_9;

public:
	inline static int32_t get_offset_of_m_preset_effect_settings_0() { return static_cast<int32_t>(offsetof(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E, ___m_preset_effect_settings_0)); }
	inline List_1_t766AD6FD03C5F589C64C80F20B2A1D155B842B13 * get_m_preset_effect_settings_0() const { return ___m_preset_effect_settings_0; }
	inline List_1_t766AD6FD03C5F589C64C80F20B2A1D155B842B13 ** get_address_of_m_preset_effect_settings_0() { return &___m_preset_effect_settings_0; }
	inline void set_m_preset_effect_settings_0(List_1_t766AD6FD03C5F589C64C80F20B2A1D155B842B13 * value)
	{
		___m_preset_effect_settings_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_preset_effect_settings_0), value);
	}

	inline static int32_t get_offset_of_m_active_1() { return static_cast<int32_t>(offsetof(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E, ___m_active_1)); }
	inline bool get_m_active_1() const { return ___m_active_1; }
	inline bool* get_address_of_m_active_1() { return &___m_active_1; }
	inline void set_m_active_1(bool value)
	{
		___m_active_1 = value;
	}

	inline static int32_t get_offset_of_m_start_action_2() { return static_cast<int32_t>(offsetof(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E, ___m_start_action_2)); }
	inline int32_t get_m_start_action_2() const { return ___m_start_action_2; }
	inline int32_t* get_address_of_m_start_action_2() { return &___m_start_action_2; }
	inline void set_m_start_action_2(int32_t value)
	{
		___m_start_action_2 = value;
	}

	inline static int32_t get_offset_of_m_num_actions_3() { return static_cast<int32_t>(offsetof(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E, ___m_num_actions_3)); }
	inline int32_t get_m_num_actions_3() const { return ___m_num_actions_3; }
	inline int32_t* get_address_of_m_num_actions_3() { return &___m_num_actions_3; }
	inline void set_m_num_actions_3(int32_t value)
	{
		___m_num_actions_3 = value;
	}

	inline static int32_t get_offset_of_m_start_loop_4() { return static_cast<int32_t>(offsetof(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E, ___m_start_loop_4)); }
	inline int32_t get_m_start_loop_4() const { return ___m_start_loop_4; }
	inline int32_t* get_address_of_m_start_loop_4() { return &___m_start_loop_4; }
	inline void set_m_start_loop_4(int32_t value)
	{
		___m_start_loop_4 = value;
	}

	inline static int32_t get_offset_of_m_num_loops_5() { return static_cast<int32_t>(offsetof(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E, ___m_num_loops_5)); }
	inline int32_t get_m_num_loops_5() const { return ___m_num_loops_5; }
	inline int32_t* get_address_of_m_num_loops_5() { return &___m_num_loops_5; }
	inline void set_m_num_loops_5(int32_t value)
	{
		___m_num_loops_5 = value;
	}

	inline static int32_t get_offset_of_m_exit_pause_6() { return static_cast<int32_t>(offsetof(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E, ___m_exit_pause_6)); }
	inline bool get_m_exit_pause_6() const { return ___m_exit_pause_6; }
	inline bool* get_address_of_m_exit_pause_6() { return &___m_exit_pause_6; }
	inline void set_m_exit_pause_6(bool value)
	{
		___m_exit_pause_6 = value;
	}

	inline static int32_t get_offset_of_m_exit_pause_duration_7() { return static_cast<int32_t>(offsetof(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E, ___m_exit_pause_duration_7)); }
	inline float get_m_exit_pause_duration_7() const { return ___m_exit_pause_duration_7; }
	inline float* get_address_of_m_exit_pause_duration_7() { return &___m_exit_pause_duration_7; }
	inline void set_m_exit_pause_duration_7(float value)
	{
		___m_exit_pause_duration_7 = value;
	}

	inline static int32_t get_offset_of_m_repeat_8() { return static_cast<int32_t>(offsetof(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E, ___m_repeat_8)); }
	inline bool get_m_repeat_8() const { return ___m_repeat_8; }
	inline bool* get_address_of_m_repeat_8() { return &___m_repeat_8; }
	inline void set_m_repeat_8(bool value)
	{
		___m_repeat_8 = value;
	}

	inline static int32_t get_offset_of_m_repeat_count_9() { return static_cast<int32_t>(offsetof(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E, ___m_repeat_count_9)); }
	inline int32_t get_m_repeat_count_9() const { return ___m_repeat_count_9; }
	inline int32_t* get_address_of_m_repeat_count_9() { return &___m_repeat_count_9; }
	inline void set_m_repeat_count_9(int32_t value)
	{
		___m_repeat_count_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESETANIMATIONSECTION_T7775917B7A5DA6084D75532BD1D812D5F5357A2E_H
#ifndef TEXTFXHELPERMETHODS_TE9A8DBB919531EF5F6FC51CFABE06CC71C1C4967_H
#define TEXTFXHELPERMETHODS_TE9A8DBB919531EF5F6FC51CFABE06CC71C1C4967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxHelperMethods
struct  TextFxHelperMethods_tE9A8DBB919531EF5F6FC51CFABE06CC71C1C4967  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXHELPERMETHODS_TE9A8DBB919531EF5F6FC51CFABE06CC71C1C4967_H
#ifndef U3CU3EC__DISPLAYCLASS113_0_T7824309C125A014D06940D95EEC463038F73C345_H
#define U3CU3EC__DISPLAYCLASS113_0_T7824309C125A014D06940D95EEC463038F73C345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxNative_<>c__DisplayClass113_0
struct  U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345  : public RuntimeObject
{
public:
	// TextFx.TextFxNative TextFx.TextFxNative_<>c__DisplayClass113_0::<>4__this
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E * ___U3CU3E4__this_0;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::y_max
	float ___y_max_1;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::y_min
	float ___y_min_2;
	// TextFx.CustomCharacterInfo TextFx.TextFxNative_<>c__DisplayClass113_0::last_char_info
	CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3 * ___last_char_info_3;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::text_width
	float ___text_width_4;
	// System.Collections.Generic.List`1<System.Single> TextFx.TextFxNative_<>c__DisplayClass113_0::line_widths
	List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * ___line_widths_5;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::line_height_offset
	float ___line_height_offset_6;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::total_text_width
	float ___total_text_width_7;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::total_text_height
	float ___total_text_height_8;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::x_max
	float ___x_max_9;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::x_min
	float ___x_min_10;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::text_height
	float ___text_height_11;
	// System.Int32 TextFx.TextFxNative_<>c__DisplayClass113_0::line_letter_idx
	int32_t ___line_letter_idx_12;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::line_width_at_last_space
	float ___line_width_at_last_space_13;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::space_char_offset
	float ___space_char_offset_14;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::last_space_y_max
	float ___last_space_y_max_15;
	// System.Single TextFx.TextFxNative_<>c__DisplayClass113_0::last_space_y_min
	float ___last_space_y_min_16;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___U3CU3E4__this_0)); }
	inline TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_y_max_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___y_max_1)); }
	inline float get_y_max_1() const { return ___y_max_1; }
	inline float* get_address_of_y_max_1() { return &___y_max_1; }
	inline void set_y_max_1(float value)
	{
		___y_max_1 = value;
	}

	inline static int32_t get_offset_of_y_min_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___y_min_2)); }
	inline float get_y_min_2() const { return ___y_min_2; }
	inline float* get_address_of_y_min_2() { return &___y_min_2; }
	inline void set_y_min_2(float value)
	{
		___y_min_2 = value;
	}

	inline static int32_t get_offset_of_last_char_info_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___last_char_info_3)); }
	inline CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3 * get_last_char_info_3() const { return ___last_char_info_3; }
	inline CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3 ** get_address_of_last_char_info_3() { return &___last_char_info_3; }
	inline void set_last_char_info_3(CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3 * value)
	{
		___last_char_info_3 = value;
		Il2CppCodeGenWriteBarrier((&___last_char_info_3), value);
	}

	inline static int32_t get_offset_of_text_width_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___text_width_4)); }
	inline float get_text_width_4() const { return ___text_width_4; }
	inline float* get_address_of_text_width_4() { return &___text_width_4; }
	inline void set_text_width_4(float value)
	{
		___text_width_4 = value;
	}

	inline static int32_t get_offset_of_line_widths_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___line_widths_5)); }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * get_line_widths_5() const { return ___line_widths_5; }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 ** get_address_of_line_widths_5() { return &___line_widths_5; }
	inline void set_line_widths_5(List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * value)
	{
		___line_widths_5 = value;
		Il2CppCodeGenWriteBarrier((&___line_widths_5), value);
	}

	inline static int32_t get_offset_of_line_height_offset_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___line_height_offset_6)); }
	inline float get_line_height_offset_6() const { return ___line_height_offset_6; }
	inline float* get_address_of_line_height_offset_6() { return &___line_height_offset_6; }
	inline void set_line_height_offset_6(float value)
	{
		___line_height_offset_6 = value;
	}

	inline static int32_t get_offset_of_total_text_width_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___total_text_width_7)); }
	inline float get_total_text_width_7() const { return ___total_text_width_7; }
	inline float* get_address_of_total_text_width_7() { return &___total_text_width_7; }
	inline void set_total_text_width_7(float value)
	{
		___total_text_width_7 = value;
	}

	inline static int32_t get_offset_of_total_text_height_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___total_text_height_8)); }
	inline float get_total_text_height_8() const { return ___total_text_height_8; }
	inline float* get_address_of_total_text_height_8() { return &___total_text_height_8; }
	inline void set_total_text_height_8(float value)
	{
		___total_text_height_8 = value;
	}

	inline static int32_t get_offset_of_x_max_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___x_max_9)); }
	inline float get_x_max_9() const { return ___x_max_9; }
	inline float* get_address_of_x_max_9() { return &___x_max_9; }
	inline void set_x_max_9(float value)
	{
		___x_max_9 = value;
	}

	inline static int32_t get_offset_of_x_min_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___x_min_10)); }
	inline float get_x_min_10() const { return ___x_min_10; }
	inline float* get_address_of_x_min_10() { return &___x_min_10; }
	inline void set_x_min_10(float value)
	{
		___x_min_10 = value;
	}

	inline static int32_t get_offset_of_text_height_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___text_height_11)); }
	inline float get_text_height_11() const { return ___text_height_11; }
	inline float* get_address_of_text_height_11() { return &___text_height_11; }
	inline void set_text_height_11(float value)
	{
		___text_height_11 = value;
	}

	inline static int32_t get_offset_of_line_letter_idx_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___line_letter_idx_12)); }
	inline int32_t get_line_letter_idx_12() const { return ___line_letter_idx_12; }
	inline int32_t* get_address_of_line_letter_idx_12() { return &___line_letter_idx_12; }
	inline void set_line_letter_idx_12(int32_t value)
	{
		___line_letter_idx_12 = value;
	}

	inline static int32_t get_offset_of_line_width_at_last_space_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___line_width_at_last_space_13)); }
	inline float get_line_width_at_last_space_13() const { return ___line_width_at_last_space_13; }
	inline float* get_address_of_line_width_at_last_space_13() { return &___line_width_at_last_space_13; }
	inline void set_line_width_at_last_space_13(float value)
	{
		___line_width_at_last_space_13 = value;
	}

	inline static int32_t get_offset_of_space_char_offset_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___space_char_offset_14)); }
	inline float get_space_char_offset_14() const { return ___space_char_offset_14; }
	inline float* get_address_of_space_char_offset_14() { return &___space_char_offset_14; }
	inline void set_space_char_offset_14(float value)
	{
		___space_char_offset_14 = value;
	}

	inline static int32_t get_offset_of_last_space_y_max_15() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___last_space_y_max_15)); }
	inline float get_last_space_y_max_15() const { return ___last_space_y_max_15; }
	inline float* get_address_of_last_space_y_max_15() { return &___last_space_y_max_15; }
	inline void set_last_space_y_max_15(float value)
	{
		___last_space_y_max_15 = value;
	}

	inline static int32_t get_offset_of_last_space_y_min_16() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345, ___last_space_y_min_16)); }
	inline float get_last_space_y_min_16() const { return ___last_space_y_min_16; }
	inline float* get_address_of_last_space_y_min_16() { return &___last_space_y_min_16; }
	inline void set_last_space_y_min_16(float value)
	{
		___last_space_y_min_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS113_0_T7824309C125A014D06940D95EEC463038F73C345_H
#ifndef TEXTFXTEXTDATAHANDLER_TD66D5E028C0AC7F85A41F5C99EE17318888B105C_H
#define TEXTFXTEXTDATAHANDLER_TD66D5E028C0AC7F85A41F5C99EE17318888B105C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxNative_TextFxTextDataHandler
struct  TextFxTextDataHandler_tD66D5E028C0AC7F85A41F5C99EE17318888B105C  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] TextFx.TextFxNative_TextFxTextDataHandler::m_posData
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_posData_0;
	// UnityEngine.Color[] TextFx.TextFxNative_TextFxTextDataHandler::m_colData
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_colData_1;

public:
	inline static int32_t get_offset_of_m_posData_0() { return static_cast<int32_t>(offsetof(TextFxTextDataHandler_tD66D5E028C0AC7F85A41F5C99EE17318888B105C, ___m_posData_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_posData_0() const { return ___m_posData_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_posData_0() { return &___m_posData_0; }
	inline void set_m_posData_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_posData_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_posData_0), value);
	}

	inline static int32_t get_offset_of_m_colData_1() { return static_cast<int32_t>(offsetof(TextFxTextDataHandler_tD66D5E028C0AC7F85A41F5C99EE17318888B105C, ___m_colData_1)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_colData_1() const { return ___m_colData_1; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_colData_1() { return &___m_colData_1; }
	inline void set_m_colData_1(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_colData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_colData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXTEXTDATAHANDLER_TD66D5E028C0AC7F85A41F5C99EE17318888B105C_H
#ifndef UGUITEXTDATAHANDLER_T39FD0317DB9504E99E62E08013FEABB8F62A6BE6_H
#define UGUITEXTDATAHANDLER_T39FD0317DB9504E99E62E08013FEABB8F62A6BE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxUGUI_UGUITextDataHandler
struct  UGUITextDataHandler_t39FD0317DB9504E99E62E08013FEABB8F62A6BE6  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> TextFx.TextFxUGUI_UGUITextDataHandler::m_vertData
	List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ___m_vertData_0;
	// System.Int32 TextFx.TextFxUGUI_UGUITextDataHandler::m_numBaseLetters
	int32_t ___m_numBaseLetters_1;
	// System.Int32 TextFx.TextFxUGUI_UGUITextDataHandler::m_extraVertsPerLetter
	int32_t ___m_extraVertsPerLetter_2;
	// System.Int32 TextFx.TextFxUGUI_UGUITextDataHandler::m_totalVertsPerLetter
	int32_t ___m_totalVertsPerLetter_3;

public:
	inline static int32_t get_offset_of_m_vertData_0() { return static_cast<int32_t>(offsetof(UGUITextDataHandler_t39FD0317DB9504E99E62E08013FEABB8F62A6BE6, ___m_vertData_0)); }
	inline List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * get_m_vertData_0() const { return ___m_vertData_0; }
	inline List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 ** get_address_of_m_vertData_0() { return &___m_vertData_0; }
	inline void set_m_vertData_0(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * value)
	{
		___m_vertData_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_vertData_0), value);
	}

	inline static int32_t get_offset_of_m_numBaseLetters_1() { return static_cast<int32_t>(offsetof(UGUITextDataHandler_t39FD0317DB9504E99E62E08013FEABB8F62A6BE6, ___m_numBaseLetters_1)); }
	inline int32_t get_m_numBaseLetters_1() const { return ___m_numBaseLetters_1; }
	inline int32_t* get_address_of_m_numBaseLetters_1() { return &___m_numBaseLetters_1; }
	inline void set_m_numBaseLetters_1(int32_t value)
	{
		___m_numBaseLetters_1 = value;
	}

	inline static int32_t get_offset_of_m_extraVertsPerLetter_2() { return static_cast<int32_t>(offsetof(UGUITextDataHandler_t39FD0317DB9504E99E62E08013FEABB8F62A6BE6, ___m_extraVertsPerLetter_2)); }
	inline int32_t get_m_extraVertsPerLetter_2() const { return ___m_extraVertsPerLetter_2; }
	inline int32_t* get_address_of_m_extraVertsPerLetter_2() { return &___m_extraVertsPerLetter_2; }
	inline void set_m_extraVertsPerLetter_2(int32_t value)
	{
		___m_extraVertsPerLetter_2 = value;
	}

	inline static int32_t get_offset_of_m_totalVertsPerLetter_3() { return static_cast<int32_t>(offsetof(UGUITextDataHandler_t39FD0317DB9504E99E62E08013FEABB8F62A6BE6, ___m_totalVertsPerLetter_3)); }
	inline int32_t get_m_totalVertsPerLetter_3() const { return ___m_totalVertsPerLetter_3; }
	inline int32_t* get_address_of_m_totalVertsPerLetter_3() { return &___m_totalVertsPerLetter_3; }
	inline void set_m_totalVertsPerLetter_3(int32_t value)
	{
		___m_totalVertsPerLetter_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUITEXTDATAHANDLER_T39FD0317DB9504E99E62E08013FEABB8F62A6BE6_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef __STATICARRAYINITTYPESIZEU3D16_T6EB025C40E80FDD74132718092D59E92446BD13D_H
#define __STATICARRAYINITTYPESIZEU3D16_T6EB025C40E80FDD74132718092D59E92446BD13D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16
struct  __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16_T6EB025C40E80FDD74132718092D59E92446BD13D_H
#ifndef HSVCOLOR_T5C320A5176C61413E49BE88D50894A89DE00F9FF_H
#define HSVCOLOR_T5C320A5176C61413E49BE88D50894A89DE00F9FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVPickerDemo.HsvColor
struct  HsvColor_t5C320A5176C61413E49BE88D50894A89DE00F9FF 
{
public:
	// System.Double HSVPickerDemo.HsvColor::H
	double ___H_0;
	// System.Double HSVPickerDemo.HsvColor::S
	double ___S_1;
	// System.Double HSVPickerDemo.HsvColor::V
	double ___V_2;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(HsvColor_t5C320A5176C61413E49BE88D50894A89DE00F9FF, ___H_0)); }
	inline double get_H_0() const { return ___H_0; }
	inline double* get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(double value)
	{
		___H_0 = value;
	}

	inline static int32_t get_offset_of_S_1() { return static_cast<int32_t>(offsetof(HsvColor_t5C320A5176C61413E49BE88D50894A89DE00F9FF, ___S_1)); }
	inline double get_S_1() const { return ___S_1; }
	inline double* get_address_of_S_1() { return &___S_1; }
	inline void set_S_1(double value)
	{
		___S_1 = value;
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HsvColor_t5C320A5176C61413E49BE88D50894A89DE00F9FF, ___V_2)); }
	inline double get_V_2() const { return ___V_2; }
	inline double* get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(double value)
	{
		___V_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCOLOR_T5C320A5176C61413E49BE88D50894A89DE00F9FF_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#define MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fontAsset_1)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___material_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_3() const { return ___material_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fallbackMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifndef TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#define TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontStyleStack
struct  TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 
{
public:
	// System.Byte TMPro.TMP_FontStyleStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_FontStyleStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_FontStyleStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_FontStyleStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_FontStyleStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_FontStyleStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_FontStyleStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_FontStyleStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_FontStyleStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_FontStyleStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T629E00E06021AA51A5AD8607BAFC61DB099F2D7F_H
#define TMP_RICHTEXTTAGSTACK_1_T629E00E06021AA51A5AD8607BAFC61DB099F2D7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<System.Int32>
struct  TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_ItemStack_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T629E00E06021AA51A5AD8607BAFC61DB099F2D7F_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T221674BAE112F99AB4BDB4D127F20A021FF50CA3_H
#define TMP_RICHTEXTTAGSTACK_1_T221674BAE112F99AB4BDB4D127F20A021FF50CA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<System.Single>
struct  TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	float ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_ItemStack_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_DefaultItem_3)); }
	inline float get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline float* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(float value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T221674BAE112F99AB4BDB4D127F20A021FF50CA3_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#define TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_ItemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_DefaultItem_3)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_DefaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#ifndef ANIMATIONSTATEVARIABLES_T8A45C679D855E8FB434F54D91FA44A18423E1D12_H
#define ANIMATIONSTATEVARIABLES_T8A45C679D855E8FB434F54D91FA44A18423E1D12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.AnimationStateVariables
struct  AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12 
{
public:
	// System.Boolean TextFx.AnimationStateVariables::m_active
	bool ___m_active_0;
	// System.Boolean TextFx.AnimationStateVariables::m_waiting_to_sync
	bool ___m_waiting_to_sync_1;
	// System.Boolean TextFx.AnimationStateVariables::m_started_action
	bool ___m_started_action_2;
	// System.Single TextFx.AnimationStateVariables::m_break_delay
	float ___m_break_delay_3;
	// System.Single TextFx.AnimationStateVariables::m_timer_offset
	float ___m_timer_offset_4;
	// System.Int32 TextFx.AnimationStateVariables::m_action_index
	int32_t ___m_action_index_5;
	// System.Boolean TextFx.AnimationStateVariables::m_reverse
	bool ___m_reverse_6;
	// System.Int32 TextFx.AnimationStateVariables::m_action_index_progress
	int32_t ___m_action_index_progress_7;
	// System.Int32 TextFx.AnimationStateVariables::m_prev_action_index
	int32_t ___m_prev_action_index_8;
	// System.Single TextFx.AnimationStateVariables::m_linear_progress
	float ___m_linear_progress_9;
	// System.Single TextFx.AnimationStateVariables::m_action_progress
	float ___m_action_progress_10;
	// System.Collections.Generic.List`1<TextFx.ActionLoopCycle> TextFx.AnimationStateVariables::m_active_loop_cycles
	List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3 * ___m_active_loop_cycles_11;

public:
	inline static int32_t get_offset_of_m_active_0() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_active_0)); }
	inline bool get_m_active_0() const { return ___m_active_0; }
	inline bool* get_address_of_m_active_0() { return &___m_active_0; }
	inline void set_m_active_0(bool value)
	{
		___m_active_0 = value;
	}

	inline static int32_t get_offset_of_m_waiting_to_sync_1() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_waiting_to_sync_1)); }
	inline bool get_m_waiting_to_sync_1() const { return ___m_waiting_to_sync_1; }
	inline bool* get_address_of_m_waiting_to_sync_1() { return &___m_waiting_to_sync_1; }
	inline void set_m_waiting_to_sync_1(bool value)
	{
		___m_waiting_to_sync_1 = value;
	}

	inline static int32_t get_offset_of_m_started_action_2() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_started_action_2)); }
	inline bool get_m_started_action_2() const { return ___m_started_action_2; }
	inline bool* get_address_of_m_started_action_2() { return &___m_started_action_2; }
	inline void set_m_started_action_2(bool value)
	{
		___m_started_action_2 = value;
	}

	inline static int32_t get_offset_of_m_break_delay_3() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_break_delay_3)); }
	inline float get_m_break_delay_3() const { return ___m_break_delay_3; }
	inline float* get_address_of_m_break_delay_3() { return &___m_break_delay_3; }
	inline void set_m_break_delay_3(float value)
	{
		___m_break_delay_3 = value;
	}

	inline static int32_t get_offset_of_m_timer_offset_4() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_timer_offset_4)); }
	inline float get_m_timer_offset_4() const { return ___m_timer_offset_4; }
	inline float* get_address_of_m_timer_offset_4() { return &___m_timer_offset_4; }
	inline void set_m_timer_offset_4(float value)
	{
		___m_timer_offset_4 = value;
	}

	inline static int32_t get_offset_of_m_action_index_5() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_action_index_5)); }
	inline int32_t get_m_action_index_5() const { return ___m_action_index_5; }
	inline int32_t* get_address_of_m_action_index_5() { return &___m_action_index_5; }
	inline void set_m_action_index_5(int32_t value)
	{
		___m_action_index_5 = value;
	}

	inline static int32_t get_offset_of_m_reverse_6() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_reverse_6)); }
	inline bool get_m_reverse_6() const { return ___m_reverse_6; }
	inline bool* get_address_of_m_reverse_6() { return &___m_reverse_6; }
	inline void set_m_reverse_6(bool value)
	{
		___m_reverse_6 = value;
	}

	inline static int32_t get_offset_of_m_action_index_progress_7() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_action_index_progress_7)); }
	inline int32_t get_m_action_index_progress_7() const { return ___m_action_index_progress_7; }
	inline int32_t* get_address_of_m_action_index_progress_7() { return &___m_action_index_progress_7; }
	inline void set_m_action_index_progress_7(int32_t value)
	{
		___m_action_index_progress_7 = value;
	}

	inline static int32_t get_offset_of_m_prev_action_index_8() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_prev_action_index_8)); }
	inline int32_t get_m_prev_action_index_8() const { return ___m_prev_action_index_8; }
	inline int32_t* get_address_of_m_prev_action_index_8() { return &___m_prev_action_index_8; }
	inline void set_m_prev_action_index_8(int32_t value)
	{
		___m_prev_action_index_8 = value;
	}

	inline static int32_t get_offset_of_m_linear_progress_9() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_linear_progress_9)); }
	inline float get_m_linear_progress_9() const { return ___m_linear_progress_9; }
	inline float* get_address_of_m_linear_progress_9() { return &___m_linear_progress_9; }
	inline void set_m_linear_progress_9(float value)
	{
		___m_linear_progress_9 = value;
	}

	inline static int32_t get_offset_of_m_action_progress_10() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_action_progress_10)); }
	inline float get_m_action_progress_10() const { return ___m_action_progress_10; }
	inline float* get_address_of_m_action_progress_10() { return &___m_action_progress_10; }
	inline void set_m_action_progress_10(float value)
	{
		___m_action_progress_10 = value;
	}

	inline static int32_t get_offset_of_m_active_loop_cycles_11() { return static_cast<int32_t>(offsetof(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12, ___m_active_loop_cycles_11)); }
	inline List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3 * get_m_active_loop_cycles_11() const { return ___m_active_loop_cycles_11; }
	inline List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3 ** get_address_of_m_active_loop_cycles_11() { return &___m_active_loop_cycles_11; }
	inline void set_m_active_loop_cycles_11(List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3 * value)
	{
		___m_active_loop_cycles_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_active_loop_cycles_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TextFx.AnimationStateVariables
struct AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12_marshaled_pinvoke
{
	int32_t ___m_active_0;
	int32_t ___m_waiting_to_sync_1;
	int32_t ___m_started_action_2;
	float ___m_break_delay_3;
	float ___m_timer_offset_4;
	int32_t ___m_action_index_5;
	int32_t ___m_reverse_6;
	int32_t ___m_action_index_progress_7;
	int32_t ___m_prev_action_index_8;
	float ___m_linear_progress_9;
	float ___m_action_progress_10;
	List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3 * ___m_active_loop_cycles_11;
};
// Native definition for COM marshalling of TextFx.AnimationStateVariables
struct AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12_marshaled_com
{
	int32_t ___m_active_0;
	int32_t ___m_waiting_to_sync_1;
	int32_t ___m_started_action_2;
	float ___m_break_delay_3;
	float ___m_timer_offset_4;
	int32_t ___m_action_index_5;
	int32_t ___m_reverse_6;
	int32_t ___m_action_index_progress_7;
	int32_t ___m_prev_action_index_8;
	float ___m_linear_progress_9;
	float ___m_action_progress_10;
	List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3 * ___m_active_loop_cycles_11;
};
#endif // ANIMATIONSTATEVARIABLES_T8A45C679D855E8FB434F54D91FA44A18423E1D12_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#define DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifndef UNITYEVENT_1_TE6445E714E33AD9505BBB6206934FA5A572188E7_H
#define UNITYEVENT_1_TE6445E714E33AD9505BBB6206934FA5A572188E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_tE6445E714E33AD9505BBB6206934FA5A572188E7  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_tE6445E714E33AD9505BBB6206934FA5A572188E7, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TE6445E714E33AD9505BBB6206934FA5A572188E7_H
#ifndef UNITYEVENT_2_T971D254E20245994FC982476B56DFA8D1AD5D369_H
#define UNITYEVENT_2_T971D254E20245994FC982476B56DFA8D1AD5D369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Single,System.Single>
struct  UnityEvent_2_t971D254E20245994FC982476B56DFA8D1AD5D369  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t971D254E20245994FC982476B56DFA8D1AD5D369, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T971D254E20245994FC982476B56DFA8D1AD5D369_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#define SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::43F2C22CB295B073A57F99AC31686B98E380A612
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___43F2C22CB295B073A57F99AC31686B98E380A612_0;

public:
	inline static int32_t get_offset_of_U343F2C22CB295B073A57F99AC31686B98E380A612_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___43F2C22CB295B073A57F99AC31686B98E380A612_0)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U343F2C22CB295B073A57F99AC31686B98E380A612_0() const { return ___43F2C22CB295B073A57F99AC31686B98E380A612_0; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U343F2C22CB295B073A57F99AC31686B98E380A612_0() { return &___43F2C22CB295B073A57F99AC31686B98E380A612_0; }
	inline void set_U343F2C22CB295B073A57F99AC31686B98E380A612_0(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___43F2C22CB295B073A57F99AC31686B98E380A612_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifndef JSONPARSINGSTATE_TBEE5A9A1C06096370A2A1E47ED64E1F0F22BA0E8_H
#define JSONPARSINGSTATE_TBEE5A9A1C06096370A2A1E47ED64E1F0F22BA0E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.TextFx.JSON.tfxJSONObject_JSONParsingState
struct  JSONParsingState_tBEE5A9A1C06096370A2A1E47ED64E1F0F22BA0E8 
{
public:
	// System.Int32 Boomlagoon.TextFx.JSON.tfxJSONObject_JSONParsingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JSONParsingState_tBEE5A9A1C06096370A2A1E47ED64E1F0F22BA0E8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPARSINGSTATE_TBEE5A9A1C06096370A2A1E47ED64E1F0F22BA0E8_H
#ifndef TFXJSONVALUETYPE_T93BF04101D0879A70E1A3D044A72F23D4E791FA5_H
#define TFXJSONVALUETYPE_T93BF04101D0879A70E1A3D044A72F23D4E791FA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.TextFx.JSON.tfxJSONValueType
struct  tfxJSONValueType_t93BF04101D0879A70E1A3D044A72F23D4E791FA5 
{
public:
	// System.Int32 Boomlagoon.TextFx.JSON.tfxJSONValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(tfxJSONValueType_t93BF04101D0879A70E1A3D044A72F23D4E791FA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TFXJSONVALUETYPE_T93BF04101D0879A70E1A3D044A72F23D4E791FA5_H
#ifndef EASINGEQUATION_TCA0CBD0561A15202778424F90084F965713F1C82_H
#define EASINGEQUATION_TCA0CBD0561A15202778424F90084F965713F1C82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasingEquation
struct  EasingEquation_tCA0CBD0561A15202778424F90084F965713F1C82 
{
public:
	// System.Int32 EasingEquation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EasingEquation_tCA0CBD0561A15202778424F90084F965713F1C82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASINGEQUATION_TCA0CBD0561A15202778424F90084F965713F1C82_H
#ifndef HSVSLIDEREVENT_TE0150BE444ACECCCD174D0CA99B45FF973AC8318_H
#define HSVSLIDEREVENT_TE0150BE444ACECCCD174D0CA99B45FF973AC8318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVPickerDemo.HSVSliderEvent
struct  HSVSliderEvent_tE0150BE444ACECCCD174D0CA99B45FF973AC8318  : public UnityEvent_1_tE6445E714E33AD9505BBB6206934FA5A572188E7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVSLIDEREVENT_TE0150BE444ACECCCD174D0CA99B45FF973AC8318_H
#ifndef COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#define COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorMode
struct  ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3 
{
public:
	// System.Int32 TMPro.ColorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#ifndef EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#define EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifndef FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#define FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifndef FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#define FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeight
struct  FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C 
{
public:
	// System.Int32 TMPro.FontWeight::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#ifndef MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#define MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t37B6F292739A890CF34EA024D24A5BFA88579086 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaskingTypes_t37B6F292739A890CF34EA024D24A5BFA88579086, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#define TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference>
struct  TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_ItemStack_0)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_DefaultItem_3)); }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F * get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#define TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32>
struct  TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_ItemStack_0)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_DefaultItem_3)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#ifndef TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#define TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text_TextInputSources
struct  TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4 
{
public:
	// System.Int32 TMPro.TMP_Text_TextInputSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#ifndef TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#define TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifndef TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#define TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifndef TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#define TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#ifndef TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#define TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#ifndef TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#define TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#ifndef VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#define VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topLeft_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topRight_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topRight_1() const { return ___topRight_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomLeft_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomRight_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifndef VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#define VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#ifndef ACTION_TYPE_TF762AC1ECF82E50019320D4B6FE385F3ACACFCE6_H
#define ACTION_TYPE_TF762AC1ECF82E50019320D4B6FE385F3ACACFCE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ACTION_TYPE
struct  ACTION_TYPE_tF762AC1ECF82E50019320D4B6FE385F3ACACFCE6 
{
public:
	// System.Int32 TextFx.ACTION_TYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ACTION_TYPE_tF762AC1ECF82E50019320D4B6FE385F3ACACFCE6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_TYPE_TF762AC1ECF82E50019320D4B6FE385F3ACACFCE6_H
#ifndef ANIMATION_DATA_TYPE_TDD2F3FCCB7592775968BB2ECF653025B830D831C_H
#define ANIMATION_DATA_TYPE_TDD2F3FCCB7592775968BB2ECF653025B830D831C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ANIMATION_DATA_TYPE
struct  ANIMATION_DATA_TYPE_tDD2F3FCCB7592775968BB2ECF653025B830D831C 
{
public:
	// System.Int32 TextFx.ANIMATION_DATA_TYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ANIMATION_DATA_TYPE_tDD2F3FCCB7592775968BB2ECF653025B830D831C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_DATA_TYPE_TDD2F3FCCB7592775968BB2ECF653025B830D831C_H
#ifndef ANIMATEPEROPTIONS_T8F02C09D4D265D1C7F163364349907973CD6FE55_H
#define ANIMATEPEROPTIONS_T8F02C09D4D265D1C7F163364349907973CD6FE55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.AnimatePerOptions
struct  AnimatePerOptions_t8F02C09D4D265D1C7F163364349907973CD6FE55 
{
public:
	// System.Int32 TextFx.AnimatePerOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimatePerOptions_t8F02C09D4D265D1C7F163364349907973CD6FE55, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEPEROPTIONS_T8F02C09D4D265D1C7F163364349907973CD6FE55_H
#ifndef ANIMATIONTIME_T2F508B3A8A9CE9AEF70C6B4062FF98E6848C9CB2_H
#define ANIMATIONTIME_T2F508B3A8A9CE9AEF70C6B4062FF98E6848C9CB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.AnimationTime
struct  AnimationTime_t2F508B3A8A9CE9AEF70C6B4062FF98E6848C9CB2 
{
public:
	// System.Int32 TextFx.AnimationTime::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimationTime_t2F508B3A8A9CE9AEF70C6B4062FF98E6848C9CB2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTIME_T2F508B3A8A9CE9AEF70C6B4062FF98E6848C9CB2_H
#ifndef BEZIERCURVEPOINT_TE162B9039A9C45D49361BB9B26E8346D1C80B770_H
#define BEZIERCURVEPOINT_TE162B9039A9C45D49361BB9B26E8346D1C80B770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.BezierCurvePoint
struct  BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770 
{
public:
	// UnityEngine.Vector3 TextFx.BezierCurvePoint::m_anchor_point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_anchor_point_0;
	// UnityEngine.Vector3 TextFx.BezierCurvePoint::m_handle_point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_handle_point_1;

public:
	inline static int32_t get_offset_of_m_anchor_point_0() { return static_cast<int32_t>(offsetof(BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770, ___m_anchor_point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_anchor_point_0() const { return ___m_anchor_point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_anchor_point_0() { return &___m_anchor_point_0; }
	inline void set_m_anchor_point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_anchor_point_0 = value;
	}

	inline static int32_t get_offset_of_m_handle_point_1() { return static_cast<int32_t>(offsetof(BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770, ___m_handle_point_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_handle_point_1() const { return ___m_handle_point_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_handle_point_1() { return &___m_handle_point_1; }
	inline void set_m_handle_point_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_handle_point_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERCURVEPOINT_TE162B9039A9C45D49361BB9B26E8346D1C80B770_H
#ifndef CONTINUETYPE_T71EF955D26397FEEC364B9A0A5ED6628671D934C_H
#define CONTINUETYPE_T71EF955D26397FEEC364B9A0A5ED6628671D934C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ContinueType
struct  ContinueType_t71EF955D26397FEEC364B9A0A5ED6628671D934C 
{
public:
	// System.Int32 TextFx.ContinueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ContinueType_t71EF955D26397FEEC364B9A0A5ED6628671D934C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTINUETYPE_T71EF955D26397FEEC364B9A0A5ED6628671D934C_H
#ifndef CUSTOMCHARACTERINFO_T5EB0325CD6C49590781C553EB09837AE1FE17CB3_H
#define CUSTOMCHARACTERINFO_T5EB0325CD6C49590781C553EB09837AE1FE17CB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.CustomCharacterInfo
struct  CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3  : public RuntimeObject
{
public:
	// System.Boolean TextFx.CustomCharacterInfo::flipped
	bool ___flipped_0;
	// UnityEngine.Rect TextFx.CustomCharacterInfo::uv
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___uv_1;
	// UnityEngine.Rect TextFx.CustomCharacterInfo::vert
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___vert_2;
	// System.Single TextFx.CustomCharacterInfo::width
	float ___width_3;

public:
	inline static int32_t get_offset_of_flipped_0() { return static_cast<int32_t>(offsetof(CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3, ___flipped_0)); }
	inline bool get_flipped_0() const { return ___flipped_0; }
	inline bool* get_address_of_flipped_0() { return &___flipped_0; }
	inline void set_flipped_0(bool value)
	{
		___flipped_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3, ___uv_1)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_uv_1() const { return ___uv_1; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_vert_2() { return static_cast<int32_t>(offsetof(CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3, ___vert_2)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_vert_2() const { return ___vert_2; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_vert_2() { return &___vert_2; }
	inline void set_vert_2(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___vert_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMCHARACTERINFO_T5EB0325CD6C49590781C553EB09837AE1FE17CB3_H
#ifndef LETTERS_TO_ANIMATE_T250343972219C53F06394DB3A3798EF25DCAE8F9_H
#define LETTERS_TO_ANIMATE_T250343972219C53F06394DB3A3798EF25DCAE8F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.LETTERS_TO_ANIMATE
struct  LETTERS_TO_ANIMATE_t250343972219C53F06394DB3A3798EF25DCAE8F9 
{
public:
	// System.Int32 TextFx.LETTERS_TO_ANIMATE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LETTERS_TO_ANIMATE_t250343972219C53F06394DB3A3798EF25DCAE8F9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERS_TO_ANIMATE_T250343972219C53F06394DB3A3798EF25DCAE8F9_H
#ifndef LETTER_ANIMATION_STATE_T4BA25C9DCB599CE2FCBE7F37433DF50C45A1AA0E_H
#define LETTER_ANIMATION_STATE_T4BA25C9DCB599CE2FCBE7F37433DF50C45A1AA0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.LETTER_ANIMATION_STATE
struct  LETTER_ANIMATION_STATE_t4BA25C9DCB599CE2FCBE7F37433DF50C45A1AA0E 
{
public:
	// System.Int32 TextFx.LETTER_ANIMATION_STATE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LETTER_ANIMATION_STATE_t4BA25C9DCB599CE2FCBE7F37433DF50C45A1AA0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTER_ANIMATION_STATE_T4BA25C9DCB599CE2FCBE7F37433DF50C45A1AA0E_H
#ifndef LOOP_TYPE_TC1D2ADE5F4AC54FD8FBAA782496D0F9943BEE513_H
#define LOOP_TYPE_TC1D2ADE5F4AC54FD8FBAA782496D0F9943BEE513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.LOOP_TYPE
struct  LOOP_TYPE_tC1D2ADE5F4AC54FD8FBAA782496D0F9943BEE513 
{
public:
	// System.Int32 TextFx.LOOP_TYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LOOP_TYPE_tC1D2ADE5F4AC54FD8FBAA782496D0F9943BEE513, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOP_TYPE_TC1D2ADE5F4AC54FD8FBAA782496D0F9943BEE513_H
#ifndef VERTEXPOSITION_T91B44DB8195E7418053D268C4AE708A5E475DD8C_H
#define VERTEXPOSITION_T91B44DB8195E7418053D268C4AE708A5E475DD8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.LetterSetup_VertexPosition
struct  VertexPosition_t91B44DB8195E7418053D268C4AE708A5E475DD8C 
{
public:
	// System.Int32 TextFx.LetterSetup_VertexPosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VertexPosition_t91B44DB8195E7418053D268C4AE708A5E475DD8C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXPOSITION_T91B44DB8195E7418053D268C4AE708A5E475DD8C_H
#ifndef ON_FINISH_ACTION_T40524EC048B30CCF656E94D2C2E80755DE8840C5_H
#define ON_FINISH_ACTION_T40524EC048B30CCF656E94D2C2E80755DE8840C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ON_FINISH_ACTION
struct  ON_FINISH_ACTION_t40524EC048B30CCF656E94D2C2E80755DE8840C5 
{
public:
	// System.Int32 TextFx.ON_FINISH_ACTION::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ON_FINISH_ACTION_t40524EC048B30CCF656E94D2C2E80755DE8840C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ON_FINISH_ACTION_T40524EC048B30CCF656E94D2C2E80755DE8840C5_H
#ifndef PARTICLE_EFFECT_TYPE_TC57A0B7D976966C6ED25198827D28D7BC8AE7E74_H
#define PARTICLE_EFFECT_TYPE_TC57A0B7D976966C6ED25198827D28D7BC8AE7E74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.PARTICLE_EFFECT_TYPE
struct  PARTICLE_EFFECT_TYPE_tC57A0B7D976966C6ED25198827D28D7BC8AE7E74 
{
public:
	// System.Int32 TextFx.PARTICLE_EFFECT_TYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PARTICLE_EFFECT_TYPE_tC57A0B7D976966C6ED25198827D28D7BC8AE7E74, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLE_EFFECT_TYPE_TC57A0B7D976966C6ED25198827D28D7BC8AE7E74_H
#ifndef PLAY_ITEM_ASSIGNMENT_TAE9BDBA820BE5E0FFD3BB8712C0A315D3B7E84F7_H
#define PLAY_ITEM_ASSIGNMENT_TAE9BDBA820BE5E0FFD3BB8712C0A315D3B7E84F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.PLAY_ITEM_ASSIGNMENT
struct  PLAY_ITEM_ASSIGNMENT_tAE9BDBA820BE5E0FFD3BB8712C0A315D3B7E84F7 
{
public:
	// System.Int32 TextFx.PLAY_ITEM_ASSIGNMENT::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PLAY_ITEM_ASSIGNMENT_tAE9BDBA820BE5E0FFD3BB8712C0A315D3B7E84F7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAY_ITEM_ASSIGNMENT_TAE9BDBA820BE5E0FFD3BB8712C0A315D3B7E84F7_H
#ifndef PLAY_ITEM_EVENTS_T239FC77660F81C3153256885424DCF47BAF0624F_H
#define PLAY_ITEM_EVENTS_T239FC77660F81C3153256885424DCF47BAF0624F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.PLAY_ITEM_EVENTS
struct  PLAY_ITEM_EVENTS_t239FC77660F81C3153256885424DCF47BAF0624F 
{
public:
	// System.Int32 TextFx.PLAY_ITEM_EVENTS::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PLAY_ITEM_EVENTS_t239FC77660F81C3153256885424DCF47BAF0624F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAY_ITEM_EVENTS_T239FC77660F81C3153256885424DCF47BAF0624F_H
#ifndef PROGRESSION_VALUE_STATE_T9CB8ADE494C1451DCB82E57F3B642FBA133FF5CE_H
#define PROGRESSION_VALUE_STATE_T9CB8ADE494C1451DCB82E57F3B642FBA133FF5CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.PROGRESSION_VALUE_STATE
struct  PROGRESSION_VALUE_STATE_t9CB8ADE494C1451DCB82E57F3B642FBA133FF5CE 
{
public:
	// System.Int32 TextFx.PROGRESSION_VALUE_STATE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PROGRESSION_VALUE_STATE_t9CB8ADE494C1451DCB82E57F3B642FBA133FF5CE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSION_VALUE_STATE_T9CB8ADE494C1451DCB82E57F3B642FBA133FF5CE_H
#ifndef PARTICLEEFFECTINSTANCEMANAGER_TE79765CA051F0BE5F91AC26C52E77A7FC478C24C_H
#define PARTICLEEFFECTINSTANCEMANAGER_TE79765CA051F0BE5F91AC26C52E77A7FC478C24C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ParticleEffectInstanceManager
struct  ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C  : public RuntimeObject
{
public:
	// TextFx.TextFxAnimationManager TextFx.ParticleEffectInstanceManager::m_animation_manager
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * ___m_animation_manager_0;
	// TextFx.LetterSetup TextFx.ParticleEffectInstanceManager::m_letter_setup_ref
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * ___m_letter_setup_ref_1;
	// UnityEngine.ParticleSystem TextFx.ParticleEffectInstanceManager::m_particle_system
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_particle_system_2;
	// System.Single TextFx.ParticleEffectInstanceManager::m_duration
	float ___m_duration_3;
	// System.Single TextFx.ParticleEffectInstanceManager::m_delay
	float ___m_delay_4;
	// UnityEngine.Vector3 TextFx.ParticleEffectInstanceManager::m_position_offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_position_offset_5;
	// UnityEngine.Quaternion TextFx.ParticleEffectInstanceManager::m_rotation_offset
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_rotation_offset_6;
	// System.Boolean TextFx.ParticleEffectInstanceManager::m_rotate_with_letter
	bool ___m_rotate_with_letter_7;
	// System.Boolean TextFx.ParticleEffectInstanceManager::m_follow_mesh
	bool ___m_follow_mesh_8;
	// System.Boolean TextFx.ParticleEffectInstanceManager::m_active
	bool ___m_active_9;
	// UnityEngine.Transform TextFx.ParticleEffectInstanceManager::m_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_transform_10;
	// UnityEngine.Quaternion TextFx.ParticleEffectInstanceManager::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_11;

public:
	inline static int32_t get_offset_of_m_animation_manager_0() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_animation_manager_0)); }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * get_m_animation_manager_0() const { return ___m_animation_manager_0; }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 ** get_address_of_m_animation_manager_0() { return &___m_animation_manager_0; }
	inline void set_m_animation_manager_0(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * value)
	{
		___m_animation_manager_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_manager_0), value);
	}

	inline static int32_t get_offset_of_m_letter_setup_ref_1() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_letter_setup_ref_1)); }
	inline LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * get_m_letter_setup_ref_1() const { return ___m_letter_setup_ref_1; }
	inline LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E ** get_address_of_m_letter_setup_ref_1() { return &___m_letter_setup_ref_1; }
	inline void set_m_letter_setup_ref_1(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * value)
	{
		___m_letter_setup_ref_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_letter_setup_ref_1), value);
	}

	inline static int32_t get_offset_of_m_particle_system_2() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_particle_system_2)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_m_particle_system_2() const { return ___m_particle_system_2; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_m_particle_system_2() { return &___m_particle_system_2; }
	inline void set_m_particle_system_2(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___m_particle_system_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_particle_system_2), value);
	}

	inline static int32_t get_offset_of_m_duration_3() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_duration_3)); }
	inline float get_m_duration_3() const { return ___m_duration_3; }
	inline float* get_address_of_m_duration_3() { return &___m_duration_3; }
	inline void set_m_duration_3(float value)
	{
		___m_duration_3 = value;
	}

	inline static int32_t get_offset_of_m_delay_4() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_delay_4)); }
	inline float get_m_delay_4() const { return ___m_delay_4; }
	inline float* get_address_of_m_delay_4() { return &___m_delay_4; }
	inline void set_m_delay_4(float value)
	{
		___m_delay_4 = value;
	}

	inline static int32_t get_offset_of_m_position_offset_5() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_position_offset_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_position_offset_5() const { return ___m_position_offset_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_position_offset_5() { return &___m_position_offset_5; }
	inline void set_m_position_offset_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_position_offset_5 = value;
	}

	inline static int32_t get_offset_of_m_rotation_offset_6() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_rotation_offset_6)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_rotation_offset_6() const { return ___m_rotation_offset_6; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_rotation_offset_6() { return &___m_rotation_offset_6; }
	inline void set_m_rotation_offset_6(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_rotation_offset_6 = value;
	}

	inline static int32_t get_offset_of_m_rotate_with_letter_7() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_rotate_with_letter_7)); }
	inline bool get_m_rotate_with_letter_7() const { return ___m_rotate_with_letter_7; }
	inline bool* get_address_of_m_rotate_with_letter_7() { return &___m_rotate_with_letter_7; }
	inline void set_m_rotate_with_letter_7(bool value)
	{
		___m_rotate_with_letter_7 = value;
	}

	inline static int32_t get_offset_of_m_follow_mesh_8() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_follow_mesh_8)); }
	inline bool get_m_follow_mesh_8() const { return ___m_follow_mesh_8; }
	inline bool* get_address_of_m_follow_mesh_8() { return &___m_follow_mesh_8; }
	inline void set_m_follow_mesh_8(bool value)
	{
		___m_follow_mesh_8 = value;
	}

	inline static int32_t get_offset_of_m_active_9() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_active_9)); }
	inline bool get_m_active_9() const { return ___m_active_9; }
	inline bool* get_address_of_m_active_9() { return &___m_active_9; }
	inline void set_m_active_9(bool value)
	{
		___m_active_9 = value;
	}

	inline static int32_t get_offset_of_m_transform_10() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___m_transform_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_transform_10() const { return ___m_transform_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_transform_10() { return &___m_transform_10; }
	inline void set_m_transform_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_transform_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_10), value);
	}

	inline static int32_t get_offset_of_rotation_11() { return static_cast<int32_t>(offsetof(ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C, ___rotation_11)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_11() const { return ___rotation_11; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_11() { return &___rotation_11; }
	inline void set_rotation_11(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEEFFECTINSTANCEMANAGER_TE79765CA051F0BE5F91AC26C52E77A7FC478C24C_H
#ifndef TYPE_T51271A948E2B2A89B4CD430CF90DF185D4685549_H
#define TYPE_T51271A948E2B2A89B4CD430CF90DF185D4685549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.PresetEffectSetting_VariableStateListener_TYPE
struct  TYPE_t51271A948E2B2A89B4CD430CF90DF185D4685549 
{
public:
	// System.Int32 TextFx.PresetEffectSetting_VariableStateListener_TYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TYPE_t51271A948E2B2A89B4CD430CF90DF185D4685549, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T51271A948E2B2A89B4CD430CF90DF185D4685549_H
#ifndef TEXTFX_IMPLEMENTATION_TD7913D147631D070B01A9AD90002E432F3EA7C7F_H
#define TEXTFX_IMPLEMENTATION_TD7913D147631D070B01A9AD90002E432F3EA7C7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TEXTFX_IMPLEMENTATION
struct  TEXTFX_IMPLEMENTATION_tD7913D147631D070B01A9AD90002E432F3EA7C7F 
{
public:
	// System.Int32 TextFx.TEXTFX_IMPLEMENTATION::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TEXTFX_IMPLEMENTATION_tD7913D147631D070B01A9AD90002E432F3EA7C7F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFX_IMPLEMENTATION_TD7913D147631D070B01A9AD90002E432F3EA7C7F_H
#ifndef TEXTDISPLAYAXIS_TC609C7C79F19A29042BF57CD2DFCA6B6A6605AD3_H
#define TEXTDISPLAYAXIS_TC609C7C79F19A29042BF57CD2DFCA6B6A6605AD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextDisplayAxis
struct  TextDisplayAxis_tC609C7C79F19A29042BF57CD2DFCA6B6A6605AD3 
{
public:
	// System.Int32 TextFx.TextDisplayAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextDisplayAxis_tC609C7C79F19A29042BF57CD2DFCA6B6A6605AD3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTDISPLAYAXIS_TC609C7C79F19A29042BF57CD2DFCA6B6A6605AD3_H
#ifndef PRESET_ANIMATION_SECTION_TBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D_H
#define PRESET_ANIMATION_SECTION_TBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxAnimationManager_PRESET_ANIMATION_SECTION
struct  PRESET_ANIMATION_SECTION_tBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D 
{
public:
	// System.Int32 TextFx.TextFxAnimationManager_PRESET_ANIMATION_SECTION::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PRESET_ANIMATION_SECTION_tBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESET_ANIMATION_SECTION_TBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D_H
#ifndef UGUI_MESH_EFFECT_TYPE_T5D20CD8E3154DA28B8B0AD1125B801840C5DB200_H
#define UGUI_MESH_EFFECT_TYPE_T5D20CD8E3154DA28B8B0AD1125B801840C5DB200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxUGUI_UGUI_MESH_EFFECT_TYPE
struct  UGUI_MESH_EFFECT_TYPE_t5D20CD8E3154DA28B8B0AD1125B801840C5DB200 
{
public:
	// System.Int32 TextFx.TextFxUGUI_UGUI_MESH_EFFECT_TYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UGUI_MESH_EFFECT_TYPE_t5D20CD8E3154DA28B8B0AD1125B801840C5DB200, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUI_MESH_EFFECT_TYPE_T5D20CD8E3154DA28B8B0AD1125B801840C5DB200_H
#ifndef VALUEPROGRESSION_T018C382D88BDD1607CC92130B776A6E130938FC6_H
#define VALUEPROGRESSION_T018C382D88BDD1607CC92130B776A6E130938FC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ValueProgression
struct  ValueProgression_t018C382D88BDD1607CC92130B776A6E130938FC6 
{
public:
	// System.Int32 TextFx.ValueProgression::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ValueProgression_t018C382D88BDD1607CC92130B776A6E130938FC6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROGRESSION_T018C382D88BDD1607CC92130B776A6E130938FC6_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TEXTALIGNMENT_TA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD_H
#define TEXTALIGNMENT_TA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAlignment
struct  TextAlignment_tA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD 
{
public:
	// System.Int32 UnityEngine.TextAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignment_tA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENT_TA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD_H
#ifndef TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#define TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifndef BOXSLIDEREVENT_T2E59E1AD1FFFC4D521C2865063F16C634BF59706_H
#define BOXSLIDEREVENT_T2E59E1AD1FFFC4D521C2865063F16C634BF59706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider_BoxSliderEvent
struct  BoxSliderEvent_t2E59E1AD1FFFC4D521C2865063F16C634BF59706  : public UnityEvent_2_t971D254E20245994FC982476B56DFA8D1AD5D369
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDEREVENT_T2E59E1AD1FFFC4D521C2865063F16C634BF59706_H
#ifndef COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#define COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifndef MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#define MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation_Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifndef SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#define SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable_SelectionState
struct  SelectionState_tF089B96B46A592693753CBF23C52A3887632D210 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_tF089B96B46A592693753CBF23C52A3887632D210, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifndef TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#define TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable_Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifndef UIVERTEX_T0583C35B730B218B542E80203F5F4BC6F1E9E577_H
#define UIVERTEX_T0583C35B730B218B542E80203F5F4BC6F1E9E577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UIVertex
struct  UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577 
{
public:
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___normal_1;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___tangent_2;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_3;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv0
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv0_4;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv1
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv1_5;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv2_6;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv3
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv3_7;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577, ___normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_normal_1() const { return ___normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___normal_1 = value;
	}

	inline static int32_t get_offset_of_tangent_2() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577, ___tangent_2)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_tangent_2() const { return ___tangent_2; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_tangent_2() { return &___tangent_2; }
	inline void set_tangent_2(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___tangent_2 = value;
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577, ___color_3)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_color_3() const { return ___color_3; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_uv0_4() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577, ___uv0_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv0_4() const { return ___uv0_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv0_4() { return &___uv0_4; }
	inline void set_uv0_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv0_4 = value;
	}

	inline static int32_t get_offset_of_uv1_5() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577, ___uv1_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv1_5() const { return ___uv1_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv1_5() { return &___uv1_5; }
	inline void set_uv1_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv1_5 = value;
	}

	inline static int32_t get_offset_of_uv2_6() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577, ___uv2_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv2_6() const { return ___uv2_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv2_6() { return &___uv2_6; }
	inline void set_uv2_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv2_6 = value;
	}

	inline static int32_t get_offset_of_uv3_7() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577, ___uv3_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv3_7() const { return ___uv3_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv3_7() { return &___uv3_7; }
	inline void set_uv3_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv3_7 = value;
	}
};

struct UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577_StaticFields
{
public:
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577  ___simpleVert_10;

public:
	inline static int32_t get_offset_of_s_DefaultColor_8() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577_StaticFields, ___s_DefaultColor_8)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_s_DefaultColor_8() const { return ___s_DefaultColor_8; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_s_DefaultColor_8() { return &___s_DefaultColor_8; }
	inline void set_s_DefaultColor_8(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___s_DefaultColor_8 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_simpleVert_10() { return static_cast<int32_t>(offsetof(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577_StaticFields, ___simpleVert_10)); }
	inline UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577  get_simpleVert_10() const { return ___simpleVert_10; }
	inline UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577 * get_address_of_simpleVert_10() { return &___simpleVert_10; }
	inline void set_simpleVert_10(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577  value)
	{
		___simpleVert_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVERTEX_T0583C35B730B218B542E80203F5F4BC6F1E9E577_H
#ifndef VERTEXCOLOUR_T2416BFEEC43DBB3D91622E111A15CF5928BAA333_H
#define VERTEXCOLOUR_T2416BFEEC43DBB3D91622E111A15CF5928BAA333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VertexColour
struct  VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333  : public RuntimeObject
{
public:
	// UnityEngine.Color VertexColour::top_left
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___top_left_0;
	// UnityEngine.Color VertexColour::top_right
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___top_right_1;
	// UnityEngine.Color VertexColour::bottom_right
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottom_right_2;
	// UnityEngine.Color VertexColour::bottom_left
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottom_left_3;

public:
	inline static int32_t get_offset_of_top_left_0() { return static_cast<int32_t>(offsetof(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333, ___top_left_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_top_left_0() const { return ___top_left_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_top_left_0() { return &___top_left_0; }
	inline void set_top_left_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___top_left_0 = value;
	}

	inline static int32_t get_offset_of_top_right_1() { return static_cast<int32_t>(offsetof(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333, ___top_right_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_top_right_1() const { return ___top_right_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_top_right_1() { return &___top_right_1; }
	inline void set_top_right_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___top_right_1 = value;
	}

	inline static int32_t get_offset_of_bottom_right_2() { return static_cast<int32_t>(offsetof(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333, ___bottom_right_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottom_right_2() const { return ___bottom_right_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottom_right_2() { return &___bottom_right_2; }
	inline void set_bottom_right_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottom_right_2 = value;
	}

	inline static int32_t get_offset_of_bottom_left_3() { return static_cast<int32_t>(offsetof(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333, ___bottom_left_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottom_left_3() const { return ___bottom_left_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottom_left_3() { return &___bottom_left_3; }
	inline void set_bottom_left_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottom_left_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXCOLOUR_T2416BFEEC43DBB3D91622E111A15CF5928BAA333_H
#ifndef TFXJSONVALUE_TC3CD526F934193004DD36549716083628729E799_H
#define TFXJSONVALUE_TC3CD526F934193004DD36549716083628729E799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boomlagoon.TextFx.JSON.tfxJSONValue
struct  tfxJSONValue_tC3CD526F934193004DD36549716083628729E799  : public RuntimeObject
{
public:
	// Boomlagoon.TextFx.JSON.tfxJSONValueType Boomlagoon.TextFx.JSON.tfxJSONValue::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_0;
	// System.String Boomlagoon.TextFx.JSON.tfxJSONValue::<Str>k__BackingField
	String_t* ___U3CStrU3Ek__BackingField_1;
	// System.Double Boomlagoon.TextFx.JSON.tfxJSONValue::<Number>k__BackingField
	double ___U3CNumberU3Ek__BackingField_2;
	// Boomlagoon.TextFx.JSON.tfxJSONObject Boomlagoon.TextFx.JSON.tfxJSONValue::<Obj>k__BackingField
	tfxJSONObject_tB6F915240278BF8031DC3EF165A5F66CF0122A8B * ___U3CObjU3Ek__BackingField_3;
	// Boomlagoon.TextFx.JSON.tfxJSONArray Boomlagoon.TextFx.JSON.tfxJSONValue::<Array>k__BackingField
	tfxJSONArray_t44B4042249796AB3367557BDC64EC7A6B1DA13CC * ___U3CArrayU3Ek__BackingField_4;
	// System.Boolean Boomlagoon.TextFx.JSON.tfxJSONValue::<Boolean>k__BackingField
	bool ___U3CBooleanU3Ek__BackingField_5;
	// Boomlagoon.TextFx.JSON.tfxJSONValue Boomlagoon.TextFx.JSON.tfxJSONValue::<Parent>k__BackingField
	tfxJSONValue_tC3CD526F934193004DD36549716083628729E799 * ___U3CParentU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(tfxJSONValue_tC3CD526F934193004DD36549716083628729E799, ___U3CTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStrU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(tfxJSONValue_tC3CD526F934193004DD36549716083628729E799, ___U3CStrU3Ek__BackingField_1)); }
	inline String_t* get_U3CStrU3Ek__BackingField_1() const { return ___U3CStrU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CStrU3Ek__BackingField_1() { return &___U3CStrU3Ek__BackingField_1; }
	inline void set_U3CStrU3Ek__BackingField_1(String_t* value)
	{
		___U3CStrU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStrU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CNumberU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(tfxJSONValue_tC3CD526F934193004DD36549716083628729E799, ___U3CNumberU3Ek__BackingField_2)); }
	inline double get_U3CNumberU3Ek__BackingField_2() const { return ___U3CNumberU3Ek__BackingField_2; }
	inline double* get_address_of_U3CNumberU3Ek__BackingField_2() { return &___U3CNumberU3Ek__BackingField_2; }
	inline void set_U3CNumberU3Ek__BackingField_2(double value)
	{
		___U3CNumberU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CObjU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(tfxJSONValue_tC3CD526F934193004DD36549716083628729E799, ___U3CObjU3Ek__BackingField_3)); }
	inline tfxJSONObject_tB6F915240278BF8031DC3EF165A5F66CF0122A8B * get_U3CObjU3Ek__BackingField_3() const { return ___U3CObjU3Ek__BackingField_3; }
	inline tfxJSONObject_tB6F915240278BF8031DC3EF165A5F66CF0122A8B ** get_address_of_U3CObjU3Ek__BackingField_3() { return &___U3CObjU3Ek__BackingField_3; }
	inline void set_U3CObjU3Ek__BackingField_3(tfxJSONObject_tB6F915240278BF8031DC3EF165A5F66CF0122A8B * value)
	{
		___U3CObjU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CArrayU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(tfxJSONValue_tC3CD526F934193004DD36549716083628729E799, ___U3CArrayU3Ek__BackingField_4)); }
	inline tfxJSONArray_t44B4042249796AB3367557BDC64EC7A6B1DA13CC * get_U3CArrayU3Ek__BackingField_4() const { return ___U3CArrayU3Ek__BackingField_4; }
	inline tfxJSONArray_t44B4042249796AB3367557BDC64EC7A6B1DA13CC ** get_address_of_U3CArrayU3Ek__BackingField_4() { return &___U3CArrayU3Ek__BackingField_4; }
	inline void set_U3CArrayU3Ek__BackingField_4(tfxJSONArray_t44B4042249796AB3367557BDC64EC7A6B1DA13CC * value)
	{
		___U3CArrayU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArrayU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CBooleanU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(tfxJSONValue_tC3CD526F934193004DD36549716083628729E799, ___U3CBooleanU3Ek__BackingField_5)); }
	inline bool get_U3CBooleanU3Ek__BackingField_5() const { return ___U3CBooleanU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CBooleanU3Ek__BackingField_5() { return &___U3CBooleanU3Ek__BackingField_5; }
	inline void set_U3CBooleanU3Ek__BackingField_5(bool value)
	{
		___U3CBooleanU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(tfxJSONValue_tC3CD526F934193004DD36549716083628729E799, ___U3CParentU3Ek__BackingField_6)); }
	inline tfxJSONValue_tC3CD526F934193004DD36549716083628729E799 * get_U3CParentU3Ek__BackingField_6() const { return ___U3CParentU3Ek__BackingField_6; }
	inline tfxJSONValue_tC3CD526F934193004DD36549716083628729E799 ** get_address_of_U3CParentU3Ek__BackingField_6() { return &___U3CParentU3Ek__BackingField_6; }
	inline void set_U3CParentU3Ek__BackingField_6(tfxJSONValue_tC3CD526F934193004DD36549716083628729E799 * value)
	{
		___U3CParentU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TFXJSONVALUE_TC3CD526F934193004DD36549716083628729E799_H
#ifndef TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#define TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineExtents_19)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#define TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight>
struct  TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_ItemStack_0)); }
	inline FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#define TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_ItemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#ifndef ACTIONLOOPCYCLE_TB01A044F8B7003818FB2657B84BB5A6CD441BE9E_H
#define ACTIONLOOPCYCLE_TB01A044F8B7003818FB2657B84BB5A6CD441BE9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ActionLoopCycle
struct  ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E  : public RuntimeObject
{
public:
	// System.Int32 TextFx.ActionLoopCycle::m_start_action_idx
	int32_t ___m_start_action_idx_0;
	// System.Int32 TextFx.ActionLoopCycle::m_end_action_idx
	int32_t ___m_end_action_idx_1;
	// System.Int32 TextFx.ActionLoopCycle::m_number_of_loops
	int32_t ___m_number_of_loops_2;
	// TextFx.LOOP_TYPE TextFx.ActionLoopCycle::m_loop_type
	int32_t ___m_loop_type_3;
	// System.Boolean TextFx.ActionLoopCycle::m_delay_first_only
	bool ___m_delay_first_only_4;
	// System.Boolean TextFx.ActionLoopCycle::m_finish_at_end
	bool ___m_finish_at_end_5;
	// System.Int32 TextFx.ActionLoopCycle::m_active_loop_index
	int32_t ___m_active_loop_index_6;
	// System.Boolean TextFx.ActionLoopCycle::m_first_pass
	bool ___m_first_pass_7;

public:
	inline static int32_t get_offset_of_m_start_action_idx_0() { return static_cast<int32_t>(offsetof(ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E, ___m_start_action_idx_0)); }
	inline int32_t get_m_start_action_idx_0() const { return ___m_start_action_idx_0; }
	inline int32_t* get_address_of_m_start_action_idx_0() { return &___m_start_action_idx_0; }
	inline void set_m_start_action_idx_0(int32_t value)
	{
		___m_start_action_idx_0 = value;
	}

	inline static int32_t get_offset_of_m_end_action_idx_1() { return static_cast<int32_t>(offsetof(ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E, ___m_end_action_idx_1)); }
	inline int32_t get_m_end_action_idx_1() const { return ___m_end_action_idx_1; }
	inline int32_t* get_address_of_m_end_action_idx_1() { return &___m_end_action_idx_1; }
	inline void set_m_end_action_idx_1(int32_t value)
	{
		___m_end_action_idx_1 = value;
	}

	inline static int32_t get_offset_of_m_number_of_loops_2() { return static_cast<int32_t>(offsetof(ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E, ___m_number_of_loops_2)); }
	inline int32_t get_m_number_of_loops_2() const { return ___m_number_of_loops_2; }
	inline int32_t* get_address_of_m_number_of_loops_2() { return &___m_number_of_loops_2; }
	inline void set_m_number_of_loops_2(int32_t value)
	{
		___m_number_of_loops_2 = value;
	}

	inline static int32_t get_offset_of_m_loop_type_3() { return static_cast<int32_t>(offsetof(ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E, ___m_loop_type_3)); }
	inline int32_t get_m_loop_type_3() const { return ___m_loop_type_3; }
	inline int32_t* get_address_of_m_loop_type_3() { return &___m_loop_type_3; }
	inline void set_m_loop_type_3(int32_t value)
	{
		___m_loop_type_3 = value;
	}

	inline static int32_t get_offset_of_m_delay_first_only_4() { return static_cast<int32_t>(offsetof(ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E, ___m_delay_first_only_4)); }
	inline bool get_m_delay_first_only_4() const { return ___m_delay_first_only_4; }
	inline bool* get_address_of_m_delay_first_only_4() { return &___m_delay_first_only_4; }
	inline void set_m_delay_first_only_4(bool value)
	{
		___m_delay_first_only_4 = value;
	}

	inline static int32_t get_offset_of_m_finish_at_end_5() { return static_cast<int32_t>(offsetof(ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E, ___m_finish_at_end_5)); }
	inline bool get_m_finish_at_end_5() const { return ___m_finish_at_end_5; }
	inline bool* get_address_of_m_finish_at_end_5() { return &___m_finish_at_end_5; }
	inline void set_m_finish_at_end_5(bool value)
	{
		___m_finish_at_end_5 = value;
	}

	inline static int32_t get_offset_of_m_active_loop_index_6() { return static_cast<int32_t>(offsetof(ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E, ___m_active_loop_index_6)); }
	inline int32_t get_m_active_loop_index_6() const { return ___m_active_loop_index_6; }
	inline int32_t* get_address_of_m_active_loop_index_6() { return &___m_active_loop_index_6; }
	inline void set_m_active_loop_index_6(int32_t value)
	{
		___m_active_loop_index_6 = value;
	}

	inline static int32_t get_offset_of_m_first_pass_7() { return static_cast<int32_t>(offsetof(ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E, ___m_first_pass_7)); }
	inline bool get_m_first_pass_7() const { return ___m_first_pass_7; }
	inline bool* get_address_of_m_first_pass_7() { return &___m_first_pass_7; }
	inline void set_m_first_pass_7(bool value)
	{
		___m_first_pass_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONLOOPCYCLE_TB01A044F8B7003818FB2657B84BB5A6CD441BE9E_H
#ifndef ACTIONVARIABLEPROGRESSIONREFERENCEDATA_TF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7_H
#define ACTIONVARIABLEPROGRESSIONREFERENCEDATA_TF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ActionVariableProgressionReferenceData
struct  ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7 
{
public:
	// TextFx.ANIMATION_DATA_TYPE TextFx.ActionVariableProgressionReferenceData::m_data_type
	int32_t ___m_data_type_0;
	// System.Boolean TextFx.ActionVariableProgressionReferenceData::m_start_state
	bool ___m_start_state_1;
	// System.Int32 TextFx.ActionVariableProgressionReferenceData::m_action_index
	int32_t ___m_action_index_2;

public:
	inline static int32_t get_offset_of_m_data_type_0() { return static_cast<int32_t>(offsetof(ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7, ___m_data_type_0)); }
	inline int32_t get_m_data_type_0() const { return ___m_data_type_0; }
	inline int32_t* get_address_of_m_data_type_0() { return &___m_data_type_0; }
	inline void set_m_data_type_0(int32_t value)
	{
		___m_data_type_0 = value;
	}

	inline static int32_t get_offset_of_m_start_state_1() { return static_cast<int32_t>(offsetof(ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7, ___m_start_state_1)); }
	inline bool get_m_start_state_1() const { return ___m_start_state_1; }
	inline bool* get_address_of_m_start_state_1() { return &___m_start_state_1; }
	inline void set_m_start_state_1(bool value)
	{
		___m_start_state_1 = value;
	}

	inline static int32_t get_offset_of_m_action_index_2() { return static_cast<int32_t>(offsetof(ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7, ___m_action_index_2)); }
	inline int32_t get_m_action_index_2() const { return ___m_action_index_2; }
	inline int32_t* get_address_of_m_action_index_2() { return &___m_action_index_2; }
	inline void set_m_action_index_2(int32_t value)
	{
		___m_action_index_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TextFx.ActionVariableProgressionReferenceData
struct ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7_marshaled_pinvoke
{
	int32_t ___m_data_type_0;
	int32_t ___m_start_state_1;
	int32_t ___m_action_index_2;
};
// Native definition for COM marshalling of TextFx.ActionVariableProgressionReferenceData
struct ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7_marshaled_com
{
	int32_t ___m_data_type_0;
	int32_t ___m_start_state_1;
	int32_t ___m_action_index_2;
};
#endif // ACTIONVARIABLEPROGRESSIONREFERENCEDATA_TF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7_H
#ifndef AXISEASINGOVERRIDEDATA_TD979C81A44FE08E1065B1C9F1CE0E4300564C47B_H
#define AXISEASINGOVERRIDEDATA_TD979C81A44FE08E1065B1C9F1CE0E4300564C47B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.AxisEasingOverrideData
struct  AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B  : public RuntimeObject
{
public:
	// System.Boolean TextFx.AxisEasingOverrideData::m_override_default
	bool ___m_override_default_0;
	// EasingEquation TextFx.AxisEasingOverrideData::m_x_ease
	int32_t ___m_x_ease_1;
	// EasingEquation TextFx.AxisEasingOverrideData::m_y_ease
	int32_t ___m_y_ease_2;
	// EasingEquation TextFx.AxisEasingOverrideData::m_z_ease
	int32_t ___m_z_ease_3;

public:
	inline static int32_t get_offset_of_m_override_default_0() { return static_cast<int32_t>(offsetof(AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B, ___m_override_default_0)); }
	inline bool get_m_override_default_0() const { return ___m_override_default_0; }
	inline bool* get_address_of_m_override_default_0() { return &___m_override_default_0; }
	inline void set_m_override_default_0(bool value)
	{
		___m_override_default_0 = value;
	}

	inline static int32_t get_offset_of_m_x_ease_1() { return static_cast<int32_t>(offsetof(AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B, ___m_x_ease_1)); }
	inline int32_t get_m_x_ease_1() const { return ___m_x_ease_1; }
	inline int32_t* get_address_of_m_x_ease_1() { return &___m_x_ease_1; }
	inline void set_m_x_ease_1(int32_t value)
	{
		___m_x_ease_1 = value;
	}

	inline static int32_t get_offset_of_m_y_ease_2() { return static_cast<int32_t>(offsetof(AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B, ___m_y_ease_2)); }
	inline int32_t get_m_y_ease_2() const { return ___m_y_ease_2; }
	inline int32_t* get_address_of_m_y_ease_2() { return &___m_y_ease_2; }
	inline void set_m_y_ease_2(int32_t value)
	{
		___m_y_ease_2 = value;
	}

	inline static int32_t get_offset_of_m_z_ease_3() { return static_cast<int32_t>(offsetof(AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B, ___m_z_ease_3)); }
	inline int32_t get_m_z_ease_3() const { return ___m_z_ease_3; }
	inline int32_t* get_address_of_m_z_ease_3() { return &___m_z_ease_3; }
	inline void set_m_z_ease_3(int32_t value)
	{
		___m_z_ease_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISEASINGOVERRIDEDATA_TD979C81A44FE08E1065B1C9F1CE0E4300564C47B_H
#ifndef BEZIERCURVEPOINTDATA_TD8FCDB79B1459B17D382755F250A0CA9518A9298_H
#define BEZIERCURVEPOINTDATA_TD8FCDB79B1459B17D382755F250A0CA9518A9298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.BezierCurvePointData
struct  BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298 
{
public:
	// TextFx.BezierCurvePoint TextFx.BezierCurvePointData::m_anchor_point_1
	BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  ___m_anchor_point_1_0;
	// TextFx.BezierCurvePoint TextFx.BezierCurvePointData::m_anchor_point_2
	BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  ___m_anchor_point_2_1;
	// TextFx.BezierCurvePoint TextFx.BezierCurvePointData::m_anchor_point_3
	BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  ___m_anchor_point_3_2;
	// TextFx.BezierCurvePoint TextFx.BezierCurvePointData::m_anchor_point_4
	BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  ___m_anchor_point_4_3;
	// TextFx.BezierCurvePoint TextFx.BezierCurvePointData::m_anchor_point_5
	BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  ___m_anchor_point_5_4;
	// System.Int32 TextFx.BezierCurvePointData::m_numActiveCurvePoints
	int32_t ___m_numActiveCurvePoints_5;

public:
	inline static int32_t get_offset_of_m_anchor_point_1_0() { return static_cast<int32_t>(offsetof(BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298, ___m_anchor_point_1_0)); }
	inline BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  get_m_anchor_point_1_0() const { return ___m_anchor_point_1_0; }
	inline BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770 * get_address_of_m_anchor_point_1_0() { return &___m_anchor_point_1_0; }
	inline void set_m_anchor_point_1_0(BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  value)
	{
		___m_anchor_point_1_0 = value;
	}

	inline static int32_t get_offset_of_m_anchor_point_2_1() { return static_cast<int32_t>(offsetof(BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298, ___m_anchor_point_2_1)); }
	inline BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  get_m_anchor_point_2_1() const { return ___m_anchor_point_2_1; }
	inline BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770 * get_address_of_m_anchor_point_2_1() { return &___m_anchor_point_2_1; }
	inline void set_m_anchor_point_2_1(BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  value)
	{
		___m_anchor_point_2_1 = value;
	}

	inline static int32_t get_offset_of_m_anchor_point_3_2() { return static_cast<int32_t>(offsetof(BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298, ___m_anchor_point_3_2)); }
	inline BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  get_m_anchor_point_3_2() const { return ___m_anchor_point_3_2; }
	inline BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770 * get_address_of_m_anchor_point_3_2() { return &___m_anchor_point_3_2; }
	inline void set_m_anchor_point_3_2(BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  value)
	{
		___m_anchor_point_3_2 = value;
	}

	inline static int32_t get_offset_of_m_anchor_point_4_3() { return static_cast<int32_t>(offsetof(BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298, ___m_anchor_point_4_3)); }
	inline BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  get_m_anchor_point_4_3() const { return ___m_anchor_point_4_3; }
	inline BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770 * get_address_of_m_anchor_point_4_3() { return &___m_anchor_point_4_3; }
	inline void set_m_anchor_point_4_3(BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  value)
	{
		___m_anchor_point_4_3 = value;
	}

	inline static int32_t get_offset_of_m_anchor_point_5_4() { return static_cast<int32_t>(offsetof(BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298, ___m_anchor_point_5_4)); }
	inline BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  get_m_anchor_point_5_4() const { return ___m_anchor_point_5_4; }
	inline BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770 * get_address_of_m_anchor_point_5_4() { return &___m_anchor_point_5_4; }
	inline void set_m_anchor_point_5_4(BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770  value)
	{
		___m_anchor_point_5_4 = value;
	}

	inline static int32_t get_offset_of_m_numActiveCurvePoints_5() { return static_cast<int32_t>(offsetof(BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298, ___m_numActiveCurvePoints_5)); }
	inline int32_t get_m_numActiveCurvePoints_5() const { return ___m_numActiveCurvePoints_5; }
	inline int32_t* get_address_of_m_numActiveCurvePoints_5() { return &___m_numActiveCurvePoints_5; }
	inline void set_m_numActiveCurvePoints_5(int32_t value)
	{
		___m_numActiveCurvePoints_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERCURVEPOINTDATA_TD8FCDB79B1459B17D382755F250A0CA9518A9298_H
#ifndef EFFECTITEMSETUP_T27955B657D57A80354ED74453BF074681E6EB835_H
#define EFFECTITEMSETUP_T27955B657D57A80354ED74453BF074681E6EB835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.EffectItemSetup
struct  EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835  : public RuntimeObject
{
public:
	// System.Boolean TextFx.EffectItemSetup::m_editor_display
	bool ___m_editor_display_0;
	// TextFx.PLAY_ITEM_EVENTS TextFx.EffectItemSetup::m_play_when
	int32_t ___m_play_when_1;
	// TextFx.PLAY_ITEM_ASSIGNMENT TextFx.EffectItemSetup::m_effect_assignment
	int32_t ___m_effect_assignment_2;
	// System.Boolean TextFx.EffectItemSetup::m_loop_play_once
	bool ___m_loop_play_once_3;
	// UnityEngine.Vector2 TextFx.EffectItemSetup::CUSTOM_LETTERS_LIST_POS
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___CUSTOM_LETTERS_LIST_POS_4;
	// System.Collections.Generic.List`1<System.Int32> TextFx.EffectItemSetup::m_effect_assignment_custom_letters
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_effect_assignment_custom_letters_5;
	// TextFx.ActionFloatProgression TextFx.EffectItemSetup::m_delay
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * ___m_delay_6;

public:
	inline static int32_t get_offset_of_m_editor_display_0() { return static_cast<int32_t>(offsetof(EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835, ___m_editor_display_0)); }
	inline bool get_m_editor_display_0() const { return ___m_editor_display_0; }
	inline bool* get_address_of_m_editor_display_0() { return &___m_editor_display_0; }
	inline void set_m_editor_display_0(bool value)
	{
		___m_editor_display_0 = value;
	}

	inline static int32_t get_offset_of_m_play_when_1() { return static_cast<int32_t>(offsetof(EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835, ___m_play_when_1)); }
	inline int32_t get_m_play_when_1() const { return ___m_play_when_1; }
	inline int32_t* get_address_of_m_play_when_1() { return &___m_play_when_1; }
	inline void set_m_play_when_1(int32_t value)
	{
		___m_play_when_1 = value;
	}

	inline static int32_t get_offset_of_m_effect_assignment_2() { return static_cast<int32_t>(offsetof(EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835, ___m_effect_assignment_2)); }
	inline int32_t get_m_effect_assignment_2() const { return ___m_effect_assignment_2; }
	inline int32_t* get_address_of_m_effect_assignment_2() { return &___m_effect_assignment_2; }
	inline void set_m_effect_assignment_2(int32_t value)
	{
		___m_effect_assignment_2 = value;
	}

	inline static int32_t get_offset_of_m_loop_play_once_3() { return static_cast<int32_t>(offsetof(EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835, ___m_loop_play_once_3)); }
	inline bool get_m_loop_play_once_3() const { return ___m_loop_play_once_3; }
	inline bool* get_address_of_m_loop_play_once_3() { return &___m_loop_play_once_3; }
	inline void set_m_loop_play_once_3(bool value)
	{
		___m_loop_play_once_3 = value;
	}

	inline static int32_t get_offset_of_CUSTOM_LETTERS_LIST_POS_4() { return static_cast<int32_t>(offsetof(EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835, ___CUSTOM_LETTERS_LIST_POS_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_CUSTOM_LETTERS_LIST_POS_4() const { return ___CUSTOM_LETTERS_LIST_POS_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_CUSTOM_LETTERS_LIST_POS_4() { return &___CUSTOM_LETTERS_LIST_POS_4; }
	inline void set_CUSTOM_LETTERS_LIST_POS_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___CUSTOM_LETTERS_LIST_POS_4 = value;
	}

	inline static int32_t get_offset_of_m_effect_assignment_custom_letters_5() { return static_cast<int32_t>(offsetof(EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835, ___m_effect_assignment_custom_letters_5)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_effect_assignment_custom_letters_5() const { return ___m_effect_assignment_custom_letters_5; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_effect_assignment_custom_letters_5() { return &___m_effect_assignment_custom_letters_5; }
	inline void set_m_effect_assignment_custom_letters_5(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_effect_assignment_custom_letters_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_effect_assignment_custom_letters_5), value);
	}

	inline static int32_t get_offset_of_m_delay_6() { return static_cast<int32_t>(offsetof(EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835, ___m_delay_6)); }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * get_m_delay_6() const { return ___m_delay_6; }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 ** get_address_of_m_delay_6() { return &___m_delay_6; }
	inline void set_m_delay_6(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * value)
	{
		___m_delay_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_delay_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTITEMSETUP_T27955B657D57A80354ED74453BF074681E6EB835_H
#ifndef LETTERACTION_TC94703CF8A8258D7D87A358EAED44E3D57890D67_H
#define LETTERACTION_TC94703CF8A8258D7D87A358EAED44E3D57890D67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.LetterAction
struct  LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67  : public RuntimeObject
{
public:
	// System.Boolean TextFx.LetterAction::m_editor_folded
	bool ___m_editor_folded_0;
	// System.Boolean TextFx.LetterAction::m_offset_from_last
	bool ___m_offset_from_last_1;
	// TextFx.ACTION_TYPE TextFx.LetterAction::m_action_type
	int32_t ___m_action_type_2;
	// System.Boolean TextFx.LetterAction::m_colour_transition_active
	bool ___m_colour_transition_active_3;
	// TextFx.ActionColorProgression TextFx.LetterAction::m_start_colour
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * ___m_start_colour_4;
	// TextFx.ActionColorProgression TextFx.LetterAction::m_end_colour
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * ___m_end_colour_5;
	// System.Boolean TextFx.LetterAction::m_position_transition_active
	bool ___m_position_transition_active_6;
	// TextFx.AxisEasingOverrideData TextFx.LetterAction::m_position_axis_ease_data
	AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * ___m_position_axis_ease_data_7;
	// TextFx.ActionPositionVector3Progression TextFx.LetterAction::m_start_pos
	ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147 * ___m_start_pos_8;
	// TextFx.ActionPositionVector3Progression TextFx.LetterAction::m_end_pos
	ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147 * ___m_end_pos_9;
	// System.Boolean TextFx.LetterAction::m_global_rotation_transition_active
	bool ___m_global_rotation_transition_active_10;
	// TextFx.AxisEasingOverrideData TextFx.LetterAction::m_global_rotation_axis_ease_data
	AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * ___m_global_rotation_axis_ease_data_11;
	// TextFx.ActionVector3Progression TextFx.LetterAction::m_global_start_euler_rotation
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * ___m_global_start_euler_rotation_12;
	// TextFx.ActionVector3Progression TextFx.LetterAction::m_global_end_euler_rotation
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * ___m_global_end_euler_rotation_13;
	// System.Boolean TextFx.LetterAction::m_local_rotation_transition_active
	bool ___m_local_rotation_transition_active_14;
	// TextFx.AxisEasingOverrideData TextFx.LetterAction::m_rotation_axis_ease_data
	AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * ___m_rotation_axis_ease_data_15;
	// TextFx.ActionVector3Progression TextFx.LetterAction::m_start_euler_rotation
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * ___m_start_euler_rotation_16;
	// TextFx.ActionVector3Progression TextFx.LetterAction::m_end_euler_rotation
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * ___m_end_euler_rotation_17;
	// System.Boolean TextFx.LetterAction::m_global_scale_transition_active
	bool ___m_global_scale_transition_active_18;
	// TextFx.AxisEasingOverrideData TextFx.LetterAction::m_global_scale_axis_ease_data
	AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * ___m_global_scale_axis_ease_data_19;
	// TextFx.ActionVector3Progression TextFx.LetterAction::m_global_start_scale
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * ___m_global_start_scale_20;
	// TextFx.ActionVector3Progression TextFx.LetterAction::m_global_end_scale
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * ___m_global_end_scale_21;
	// System.Boolean TextFx.LetterAction::m_local_scale_transition_active
	bool ___m_local_scale_transition_active_22;
	// TextFx.AxisEasingOverrideData TextFx.LetterAction::m_scale_axis_ease_data
	AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * ___m_scale_axis_ease_data_23;
	// TextFx.ActionVector3Progression TextFx.LetterAction::m_start_scale
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * ___m_start_scale_24;
	// TextFx.ActionVector3Progression TextFx.LetterAction::m_end_scale
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * ___m_end_scale_25;
	// System.Boolean TextFx.LetterAction::m_force_same_start_time
	bool ___m_force_same_start_time_26;
	// System.Boolean TextFx.LetterAction::m_delay_with_white_space_influence
	bool ___m_delay_with_white_space_influence_27;
	// TextFx.ActionFloatProgression TextFx.LetterAction::m_delay_progression
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * ___m_delay_progression_28;
	// TextFx.ActionFloatProgression TextFx.LetterAction::m_duration_progression
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * ___m_duration_progression_29;
	// EasingEquation TextFx.LetterAction::m_ease_type
	int32_t ___m_ease_type_30;
	// System.Int32 TextFx.LetterAction::m_letter_anchor_start
	int32_t ___m_letter_anchor_start_31;
	// System.Int32 TextFx.LetterAction::m_letter_anchor_end
	int32_t ___m_letter_anchor_end_32;
	// System.Boolean TextFx.LetterAction::m_letter_anchor_2_way
	bool ___m_letter_anchor_2_way_33;
	// System.Collections.Generic.List`1<TextFx.ParticleEffectSetup> TextFx.LetterAction::m_particle_effects
	List_1_tAD371B0AE32A79BD133FF2968C76AC48994D8026 * ___m_particle_effects_34;
	// System.Collections.Generic.List`1<TextFx.AudioEffectSetup> TextFx.LetterAction::m_audio_effects
	List_1_tA015857CE716DE3A369F3779EFF10F8EC7ACDE7F * ___m_audio_effects_35;
	// UnityEngine.Vector3 TextFx.LetterAction::m_anchor_offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_anchor_offset_36;
	// UnityEngine.Vector3 TextFx.LetterAction::m_anchor_offset_end
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_anchor_offset_end_37;
	// System.Boolean TextFx.LetterAction::<ParticleEffectsEditorDisplay>k__BackingField
	bool ___U3CParticleEffectsEditorDisplayU3Ek__BackingField_38;
	// System.Boolean TextFx.LetterAction::<AudioEffectsEditorDisplay>k__BackingField
	bool ___U3CAudioEffectsEditorDisplayU3Ek__BackingField_39;
	// TextFx.AudioEffectSetup TextFx.LetterAction::_audio_effect_setup
	AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F * ____audio_effect_setup_40;
	// TextFx.ParticleEffectSetup TextFx.LetterAction::_particle_effect_setup
	ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F * ____particle_effect_setup_41;

public:
	inline static int32_t get_offset_of_m_editor_folded_0() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_editor_folded_0)); }
	inline bool get_m_editor_folded_0() const { return ___m_editor_folded_0; }
	inline bool* get_address_of_m_editor_folded_0() { return &___m_editor_folded_0; }
	inline void set_m_editor_folded_0(bool value)
	{
		___m_editor_folded_0 = value;
	}

	inline static int32_t get_offset_of_m_offset_from_last_1() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_offset_from_last_1)); }
	inline bool get_m_offset_from_last_1() const { return ___m_offset_from_last_1; }
	inline bool* get_address_of_m_offset_from_last_1() { return &___m_offset_from_last_1; }
	inline void set_m_offset_from_last_1(bool value)
	{
		___m_offset_from_last_1 = value;
	}

	inline static int32_t get_offset_of_m_action_type_2() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_action_type_2)); }
	inline int32_t get_m_action_type_2() const { return ___m_action_type_2; }
	inline int32_t* get_address_of_m_action_type_2() { return &___m_action_type_2; }
	inline void set_m_action_type_2(int32_t value)
	{
		___m_action_type_2 = value;
	}

	inline static int32_t get_offset_of_m_colour_transition_active_3() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_colour_transition_active_3)); }
	inline bool get_m_colour_transition_active_3() const { return ___m_colour_transition_active_3; }
	inline bool* get_address_of_m_colour_transition_active_3() { return &___m_colour_transition_active_3; }
	inline void set_m_colour_transition_active_3(bool value)
	{
		___m_colour_transition_active_3 = value;
	}

	inline static int32_t get_offset_of_m_start_colour_4() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_start_colour_4)); }
	inline ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * get_m_start_colour_4() const { return ___m_start_colour_4; }
	inline ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA ** get_address_of_m_start_colour_4() { return &___m_start_colour_4; }
	inline void set_m_start_colour_4(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * value)
	{
		___m_start_colour_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_start_colour_4), value);
	}

	inline static int32_t get_offset_of_m_end_colour_5() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_end_colour_5)); }
	inline ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * get_m_end_colour_5() const { return ___m_end_colour_5; }
	inline ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA ** get_address_of_m_end_colour_5() { return &___m_end_colour_5; }
	inline void set_m_end_colour_5(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * value)
	{
		___m_end_colour_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_end_colour_5), value);
	}

	inline static int32_t get_offset_of_m_position_transition_active_6() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_position_transition_active_6)); }
	inline bool get_m_position_transition_active_6() const { return ___m_position_transition_active_6; }
	inline bool* get_address_of_m_position_transition_active_6() { return &___m_position_transition_active_6; }
	inline void set_m_position_transition_active_6(bool value)
	{
		___m_position_transition_active_6 = value;
	}

	inline static int32_t get_offset_of_m_position_axis_ease_data_7() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_position_axis_ease_data_7)); }
	inline AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * get_m_position_axis_ease_data_7() const { return ___m_position_axis_ease_data_7; }
	inline AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B ** get_address_of_m_position_axis_ease_data_7() { return &___m_position_axis_ease_data_7; }
	inline void set_m_position_axis_ease_data_7(AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * value)
	{
		___m_position_axis_ease_data_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_position_axis_ease_data_7), value);
	}

	inline static int32_t get_offset_of_m_start_pos_8() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_start_pos_8)); }
	inline ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147 * get_m_start_pos_8() const { return ___m_start_pos_8; }
	inline ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147 ** get_address_of_m_start_pos_8() { return &___m_start_pos_8; }
	inline void set_m_start_pos_8(ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147 * value)
	{
		___m_start_pos_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_start_pos_8), value);
	}

	inline static int32_t get_offset_of_m_end_pos_9() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_end_pos_9)); }
	inline ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147 * get_m_end_pos_9() const { return ___m_end_pos_9; }
	inline ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147 ** get_address_of_m_end_pos_9() { return &___m_end_pos_9; }
	inline void set_m_end_pos_9(ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147 * value)
	{
		___m_end_pos_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_end_pos_9), value);
	}

	inline static int32_t get_offset_of_m_global_rotation_transition_active_10() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_global_rotation_transition_active_10)); }
	inline bool get_m_global_rotation_transition_active_10() const { return ___m_global_rotation_transition_active_10; }
	inline bool* get_address_of_m_global_rotation_transition_active_10() { return &___m_global_rotation_transition_active_10; }
	inline void set_m_global_rotation_transition_active_10(bool value)
	{
		___m_global_rotation_transition_active_10 = value;
	}

	inline static int32_t get_offset_of_m_global_rotation_axis_ease_data_11() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_global_rotation_axis_ease_data_11)); }
	inline AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * get_m_global_rotation_axis_ease_data_11() const { return ___m_global_rotation_axis_ease_data_11; }
	inline AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B ** get_address_of_m_global_rotation_axis_ease_data_11() { return &___m_global_rotation_axis_ease_data_11; }
	inline void set_m_global_rotation_axis_ease_data_11(AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * value)
	{
		___m_global_rotation_axis_ease_data_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_global_rotation_axis_ease_data_11), value);
	}

	inline static int32_t get_offset_of_m_global_start_euler_rotation_12() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_global_start_euler_rotation_12)); }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * get_m_global_start_euler_rotation_12() const { return ___m_global_start_euler_rotation_12; }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C ** get_address_of_m_global_start_euler_rotation_12() { return &___m_global_start_euler_rotation_12; }
	inline void set_m_global_start_euler_rotation_12(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * value)
	{
		___m_global_start_euler_rotation_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_global_start_euler_rotation_12), value);
	}

	inline static int32_t get_offset_of_m_global_end_euler_rotation_13() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_global_end_euler_rotation_13)); }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * get_m_global_end_euler_rotation_13() const { return ___m_global_end_euler_rotation_13; }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C ** get_address_of_m_global_end_euler_rotation_13() { return &___m_global_end_euler_rotation_13; }
	inline void set_m_global_end_euler_rotation_13(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * value)
	{
		___m_global_end_euler_rotation_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_global_end_euler_rotation_13), value);
	}

	inline static int32_t get_offset_of_m_local_rotation_transition_active_14() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_local_rotation_transition_active_14)); }
	inline bool get_m_local_rotation_transition_active_14() const { return ___m_local_rotation_transition_active_14; }
	inline bool* get_address_of_m_local_rotation_transition_active_14() { return &___m_local_rotation_transition_active_14; }
	inline void set_m_local_rotation_transition_active_14(bool value)
	{
		___m_local_rotation_transition_active_14 = value;
	}

	inline static int32_t get_offset_of_m_rotation_axis_ease_data_15() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_rotation_axis_ease_data_15)); }
	inline AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * get_m_rotation_axis_ease_data_15() const { return ___m_rotation_axis_ease_data_15; }
	inline AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B ** get_address_of_m_rotation_axis_ease_data_15() { return &___m_rotation_axis_ease_data_15; }
	inline void set_m_rotation_axis_ease_data_15(AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * value)
	{
		___m_rotation_axis_ease_data_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_rotation_axis_ease_data_15), value);
	}

	inline static int32_t get_offset_of_m_start_euler_rotation_16() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_start_euler_rotation_16)); }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * get_m_start_euler_rotation_16() const { return ___m_start_euler_rotation_16; }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C ** get_address_of_m_start_euler_rotation_16() { return &___m_start_euler_rotation_16; }
	inline void set_m_start_euler_rotation_16(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * value)
	{
		___m_start_euler_rotation_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_start_euler_rotation_16), value);
	}

	inline static int32_t get_offset_of_m_end_euler_rotation_17() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_end_euler_rotation_17)); }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * get_m_end_euler_rotation_17() const { return ___m_end_euler_rotation_17; }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C ** get_address_of_m_end_euler_rotation_17() { return &___m_end_euler_rotation_17; }
	inline void set_m_end_euler_rotation_17(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * value)
	{
		___m_end_euler_rotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_end_euler_rotation_17), value);
	}

	inline static int32_t get_offset_of_m_global_scale_transition_active_18() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_global_scale_transition_active_18)); }
	inline bool get_m_global_scale_transition_active_18() const { return ___m_global_scale_transition_active_18; }
	inline bool* get_address_of_m_global_scale_transition_active_18() { return &___m_global_scale_transition_active_18; }
	inline void set_m_global_scale_transition_active_18(bool value)
	{
		___m_global_scale_transition_active_18 = value;
	}

	inline static int32_t get_offset_of_m_global_scale_axis_ease_data_19() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_global_scale_axis_ease_data_19)); }
	inline AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * get_m_global_scale_axis_ease_data_19() const { return ___m_global_scale_axis_ease_data_19; }
	inline AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B ** get_address_of_m_global_scale_axis_ease_data_19() { return &___m_global_scale_axis_ease_data_19; }
	inline void set_m_global_scale_axis_ease_data_19(AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * value)
	{
		___m_global_scale_axis_ease_data_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_global_scale_axis_ease_data_19), value);
	}

	inline static int32_t get_offset_of_m_global_start_scale_20() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_global_start_scale_20)); }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * get_m_global_start_scale_20() const { return ___m_global_start_scale_20; }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C ** get_address_of_m_global_start_scale_20() { return &___m_global_start_scale_20; }
	inline void set_m_global_start_scale_20(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * value)
	{
		___m_global_start_scale_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_global_start_scale_20), value);
	}

	inline static int32_t get_offset_of_m_global_end_scale_21() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_global_end_scale_21)); }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * get_m_global_end_scale_21() const { return ___m_global_end_scale_21; }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C ** get_address_of_m_global_end_scale_21() { return &___m_global_end_scale_21; }
	inline void set_m_global_end_scale_21(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * value)
	{
		___m_global_end_scale_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_global_end_scale_21), value);
	}

	inline static int32_t get_offset_of_m_local_scale_transition_active_22() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_local_scale_transition_active_22)); }
	inline bool get_m_local_scale_transition_active_22() const { return ___m_local_scale_transition_active_22; }
	inline bool* get_address_of_m_local_scale_transition_active_22() { return &___m_local_scale_transition_active_22; }
	inline void set_m_local_scale_transition_active_22(bool value)
	{
		___m_local_scale_transition_active_22 = value;
	}

	inline static int32_t get_offset_of_m_scale_axis_ease_data_23() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_scale_axis_ease_data_23)); }
	inline AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * get_m_scale_axis_ease_data_23() const { return ___m_scale_axis_ease_data_23; }
	inline AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B ** get_address_of_m_scale_axis_ease_data_23() { return &___m_scale_axis_ease_data_23; }
	inline void set_m_scale_axis_ease_data_23(AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B * value)
	{
		___m_scale_axis_ease_data_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_scale_axis_ease_data_23), value);
	}

	inline static int32_t get_offset_of_m_start_scale_24() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_start_scale_24)); }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * get_m_start_scale_24() const { return ___m_start_scale_24; }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C ** get_address_of_m_start_scale_24() { return &___m_start_scale_24; }
	inline void set_m_start_scale_24(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * value)
	{
		___m_start_scale_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_start_scale_24), value);
	}

	inline static int32_t get_offset_of_m_end_scale_25() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_end_scale_25)); }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * get_m_end_scale_25() const { return ___m_end_scale_25; }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C ** get_address_of_m_end_scale_25() { return &___m_end_scale_25; }
	inline void set_m_end_scale_25(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * value)
	{
		___m_end_scale_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_end_scale_25), value);
	}

	inline static int32_t get_offset_of_m_force_same_start_time_26() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_force_same_start_time_26)); }
	inline bool get_m_force_same_start_time_26() const { return ___m_force_same_start_time_26; }
	inline bool* get_address_of_m_force_same_start_time_26() { return &___m_force_same_start_time_26; }
	inline void set_m_force_same_start_time_26(bool value)
	{
		___m_force_same_start_time_26 = value;
	}

	inline static int32_t get_offset_of_m_delay_with_white_space_influence_27() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_delay_with_white_space_influence_27)); }
	inline bool get_m_delay_with_white_space_influence_27() const { return ___m_delay_with_white_space_influence_27; }
	inline bool* get_address_of_m_delay_with_white_space_influence_27() { return &___m_delay_with_white_space_influence_27; }
	inline void set_m_delay_with_white_space_influence_27(bool value)
	{
		___m_delay_with_white_space_influence_27 = value;
	}

	inline static int32_t get_offset_of_m_delay_progression_28() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_delay_progression_28)); }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * get_m_delay_progression_28() const { return ___m_delay_progression_28; }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 ** get_address_of_m_delay_progression_28() { return &___m_delay_progression_28; }
	inline void set_m_delay_progression_28(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * value)
	{
		___m_delay_progression_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_delay_progression_28), value);
	}

	inline static int32_t get_offset_of_m_duration_progression_29() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_duration_progression_29)); }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * get_m_duration_progression_29() const { return ___m_duration_progression_29; }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 ** get_address_of_m_duration_progression_29() { return &___m_duration_progression_29; }
	inline void set_m_duration_progression_29(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * value)
	{
		___m_duration_progression_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_duration_progression_29), value);
	}

	inline static int32_t get_offset_of_m_ease_type_30() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_ease_type_30)); }
	inline int32_t get_m_ease_type_30() const { return ___m_ease_type_30; }
	inline int32_t* get_address_of_m_ease_type_30() { return &___m_ease_type_30; }
	inline void set_m_ease_type_30(int32_t value)
	{
		___m_ease_type_30 = value;
	}

	inline static int32_t get_offset_of_m_letter_anchor_start_31() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_letter_anchor_start_31)); }
	inline int32_t get_m_letter_anchor_start_31() const { return ___m_letter_anchor_start_31; }
	inline int32_t* get_address_of_m_letter_anchor_start_31() { return &___m_letter_anchor_start_31; }
	inline void set_m_letter_anchor_start_31(int32_t value)
	{
		___m_letter_anchor_start_31 = value;
	}

	inline static int32_t get_offset_of_m_letter_anchor_end_32() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_letter_anchor_end_32)); }
	inline int32_t get_m_letter_anchor_end_32() const { return ___m_letter_anchor_end_32; }
	inline int32_t* get_address_of_m_letter_anchor_end_32() { return &___m_letter_anchor_end_32; }
	inline void set_m_letter_anchor_end_32(int32_t value)
	{
		___m_letter_anchor_end_32 = value;
	}

	inline static int32_t get_offset_of_m_letter_anchor_2_way_33() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_letter_anchor_2_way_33)); }
	inline bool get_m_letter_anchor_2_way_33() const { return ___m_letter_anchor_2_way_33; }
	inline bool* get_address_of_m_letter_anchor_2_way_33() { return &___m_letter_anchor_2_way_33; }
	inline void set_m_letter_anchor_2_way_33(bool value)
	{
		___m_letter_anchor_2_way_33 = value;
	}

	inline static int32_t get_offset_of_m_particle_effects_34() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_particle_effects_34)); }
	inline List_1_tAD371B0AE32A79BD133FF2968C76AC48994D8026 * get_m_particle_effects_34() const { return ___m_particle_effects_34; }
	inline List_1_tAD371B0AE32A79BD133FF2968C76AC48994D8026 ** get_address_of_m_particle_effects_34() { return &___m_particle_effects_34; }
	inline void set_m_particle_effects_34(List_1_tAD371B0AE32A79BD133FF2968C76AC48994D8026 * value)
	{
		___m_particle_effects_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_particle_effects_34), value);
	}

	inline static int32_t get_offset_of_m_audio_effects_35() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_audio_effects_35)); }
	inline List_1_tA015857CE716DE3A369F3779EFF10F8EC7ACDE7F * get_m_audio_effects_35() const { return ___m_audio_effects_35; }
	inline List_1_tA015857CE716DE3A369F3779EFF10F8EC7ACDE7F ** get_address_of_m_audio_effects_35() { return &___m_audio_effects_35; }
	inline void set_m_audio_effects_35(List_1_tA015857CE716DE3A369F3779EFF10F8EC7ACDE7F * value)
	{
		___m_audio_effects_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_audio_effects_35), value);
	}

	inline static int32_t get_offset_of_m_anchor_offset_36() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_anchor_offset_36)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_anchor_offset_36() const { return ___m_anchor_offset_36; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_anchor_offset_36() { return &___m_anchor_offset_36; }
	inline void set_m_anchor_offset_36(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_anchor_offset_36 = value;
	}

	inline static int32_t get_offset_of_m_anchor_offset_end_37() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___m_anchor_offset_end_37)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_anchor_offset_end_37() const { return ___m_anchor_offset_end_37; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_anchor_offset_end_37() { return &___m_anchor_offset_end_37; }
	inline void set_m_anchor_offset_end_37(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_anchor_offset_end_37 = value;
	}

	inline static int32_t get_offset_of_U3CParticleEffectsEditorDisplayU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___U3CParticleEffectsEditorDisplayU3Ek__BackingField_38)); }
	inline bool get_U3CParticleEffectsEditorDisplayU3Ek__BackingField_38() const { return ___U3CParticleEffectsEditorDisplayU3Ek__BackingField_38; }
	inline bool* get_address_of_U3CParticleEffectsEditorDisplayU3Ek__BackingField_38() { return &___U3CParticleEffectsEditorDisplayU3Ek__BackingField_38; }
	inline void set_U3CParticleEffectsEditorDisplayU3Ek__BackingField_38(bool value)
	{
		___U3CParticleEffectsEditorDisplayU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CAudioEffectsEditorDisplayU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ___U3CAudioEffectsEditorDisplayU3Ek__BackingField_39)); }
	inline bool get_U3CAudioEffectsEditorDisplayU3Ek__BackingField_39() const { return ___U3CAudioEffectsEditorDisplayU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CAudioEffectsEditorDisplayU3Ek__BackingField_39() { return &___U3CAudioEffectsEditorDisplayU3Ek__BackingField_39; }
	inline void set_U3CAudioEffectsEditorDisplayU3Ek__BackingField_39(bool value)
	{
		___U3CAudioEffectsEditorDisplayU3Ek__BackingField_39 = value;
	}

	inline static int32_t get_offset_of__audio_effect_setup_40() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ____audio_effect_setup_40)); }
	inline AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F * get__audio_effect_setup_40() const { return ____audio_effect_setup_40; }
	inline AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F ** get_address_of__audio_effect_setup_40() { return &____audio_effect_setup_40; }
	inline void set__audio_effect_setup_40(AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F * value)
	{
		____audio_effect_setup_40 = value;
		Il2CppCodeGenWriteBarrier((&____audio_effect_setup_40), value);
	}

	inline static int32_t get_offset_of__particle_effect_setup_41() { return static_cast<int32_t>(offsetof(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67, ____particle_effect_setup_41)); }
	inline ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F * get__particle_effect_setup_41() const { return ____particle_effect_setup_41; }
	inline ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F ** get_address_of__particle_effect_setup_41() { return &____particle_effect_setup_41; }
	inline void set__particle_effect_setup_41(ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F * value)
	{
		____particle_effect_setup_41 = value;
		Il2CppCodeGenWriteBarrier((&____particle_effect_setup_41), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERACTION_TC94703CF8A8258D7D87A358EAED44E3D57890D67_H
#ifndef LETTERANIMATION_TA04230F0A57961D82FA0ED8CA398B340ED77B4A9_H
#define LETTERANIMATION_TA04230F0A57961D82FA0ED8CA398B340ED77B4A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.LetterAnimation
struct  LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TextFx.LetterAction> TextFx.LetterAnimation::m_letter_actions
	List_1_t92B02A7B4F386D14AA96EE6BA827436C9306F58F * ___m_letter_actions_1;
	// System.Collections.Generic.List`1<TextFx.ActionLoopCycle> TextFx.LetterAnimation::m_loop_cycles
	List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3 * ___m_loop_cycles_2;
	// TextFx.LETTERS_TO_ANIMATE TextFx.LetterAnimation::m_letters_to_animate_option
	int32_t ___m_letters_to_animate_option_3;
	// System.Collections.Generic.List`1<System.Int32> TextFx.LetterAnimation::m_letters_to_animate
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_letters_to_animate_4;
	// System.Int32 TextFx.LetterAnimation::m_letters_to_animate_custom_idx
	int32_t ___m_letters_to_animate_custom_idx_5;
	// System.Int32 TextFx.LetterAnimation::m_num_white_space_chars_to_include
	int32_t ___m_num_white_space_chars_to_include_6;
	// TextFx.ActionColorProgression TextFx.LetterAnimation::m_defaultTextColourProgression
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * ___m_defaultTextColourProgression_7;
	// TextFx.LETTER_ANIMATION_STATE TextFx.LetterAnimation::m_animation_state
	int32_t ___m_animation_state_8;

public:
	inline static int32_t get_offset_of_m_letter_actions_1() { return static_cast<int32_t>(offsetof(LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9, ___m_letter_actions_1)); }
	inline List_1_t92B02A7B4F386D14AA96EE6BA827436C9306F58F * get_m_letter_actions_1() const { return ___m_letter_actions_1; }
	inline List_1_t92B02A7B4F386D14AA96EE6BA827436C9306F58F ** get_address_of_m_letter_actions_1() { return &___m_letter_actions_1; }
	inline void set_m_letter_actions_1(List_1_t92B02A7B4F386D14AA96EE6BA827436C9306F58F * value)
	{
		___m_letter_actions_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_letter_actions_1), value);
	}

	inline static int32_t get_offset_of_m_loop_cycles_2() { return static_cast<int32_t>(offsetof(LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9, ___m_loop_cycles_2)); }
	inline List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3 * get_m_loop_cycles_2() const { return ___m_loop_cycles_2; }
	inline List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3 ** get_address_of_m_loop_cycles_2() { return &___m_loop_cycles_2; }
	inline void set_m_loop_cycles_2(List_1_t4553F32FB9DF1AFCEBD31A42B40D40AA028477A3 * value)
	{
		___m_loop_cycles_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_loop_cycles_2), value);
	}

	inline static int32_t get_offset_of_m_letters_to_animate_option_3() { return static_cast<int32_t>(offsetof(LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9, ___m_letters_to_animate_option_3)); }
	inline int32_t get_m_letters_to_animate_option_3() const { return ___m_letters_to_animate_option_3; }
	inline int32_t* get_address_of_m_letters_to_animate_option_3() { return &___m_letters_to_animate_option_3; }
	inline void set_m_letters_to_animate_option_3(int32_t value)
	{
		___m_letters_to_animate_option_3 = value;
	}

	inline static int32_t get_offset_of_m_letters_to_animate_4() { return static_cast<int32_t>(offsetof(LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9, ___m_letters_to_animate_4)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_letters_to_animate_4() const { return ___m_letters_to_animate_4; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_letters_to_animate_4() { return &___m_letters_to_animate_4; }
	inline void set_m_letters_to_animate_4(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_letters_to_animate_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_letters_to_animate_4), value);
	}

	inline static int32_t get_offset_of_m_letters_to_animate_custom_idx_5() { return static_cast<int32_t>(offsetof(LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9, ___m_letters_to_animate_custom_idx_5)); }
	inline int32_t get_m_letters_to_animate_custom_idx_5() const { return ___m_letters_to_animate_custom_idx_5; }
	inline int32_t* get_address_of_m_letters_to_animate_custom_idx_5() { return &___m_letters_to_animate_custom_idx_5; }
	inline void set_m_letters_to_animate_custom_idx_5(int32_t value)
	{
		___m_letters_to_animate_custom_idx_5 = value;
	}

	inline static int32_t get_offset_of_m_num_white_space_chars_to_include_6() { return static_cast<int32_t>(offsetof(LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9, ___m_num_white_space_chars_to_include_6)); }
	inline int32_t get_m_num_white_space_chars_to_include_6() const { return ___m_num_white_space_chars_to_include_6; }
	inline int32_t* get_address_of_m_num_white_space_chars_to_include_6() { return &___m_num_white_space_chars_to_include_6; }
	inline void set_m_num_white_space_chars_to_include_6(int32_t value)
	{
		___m_num_white_space_chars_to_include_6 = value;
	}

	inline static int32_t get_offset_of_m_defaultTextColourProgression_7() { return static_cast<int32_t>(offsetof(LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9, ___m_defaultTextColourProgression_7)); }
	inline ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * get_m_defaultTextColourProgression_7() const { return ___m_defaultTextColourProgression_7; }
	inline ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA ** get_address_of_m_defaultTextColourProgression_7() { return &___m_defaultTextColourProgression_7; }
	inline void set_m_defaultTextColourProgression_7(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * value)
	{
		___m_defaultTextColourProgression_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultTextColourProgression_7), value);
	}

	inline static int32_t get_offset_of_m_animation_state_8() { return static_cast<int32_t>(offsetof(LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9, ___m_animation_state_8)); }
	inline int32_t get_m_animation_state_8() const { return ___m_animation_state_8; }
	inline int32_t* get_address_of_m_animation_state_8() { return &___m_animation_state_8; }
	inline void set_m_animation_state_8(int32_t value)
	{
		___m_animation_state_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERANIMATION_TA04230F0A57961D82FA0ED8CA398B340ED77B4A9_H
#ifndef LETTERSETUP_TFF1F1C00E610201F6E3C9D01843D8866C7F6369E_H
#define LETTERSETUP_TFF1F1C00E610201F6E3C9D01843D8866C7F6369E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.LetterSetup
struct  LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E  : public RuntimeObject
{
public:
	// TextFx.TextFxAnimationManager TextFx.LetterSetup::m_animation_manager_ref
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * ___m_animation_manager_ref_0;
	// TextFx.LetterAction TextFx.LetterSetup::m_current_letter_action
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67 * ___m_current_letter_action_1;
	// UnityEngine.Vector3[] TextFx.LetterSetup::m_base_vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_base_vertices_2;
	// UnityEngine.Vector3[] TextFx.LetterSetup::m_base_extra_vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_base_extra_vertices_3;
	// System.Boolean TextFx.LetterSetup::m_using_curved_data
	bool ___m_using_curved_data_4;
	// UnityEngine.Vector3[] TextFx.LetterSetup::m_curve_base_vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_curve_base_vertices_5;
	// UnityEngine.Vector3[] TextFx.LetterSetup::m_curve_base_extra_vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_curve_base_extra_vertices_6;
	// VertexColour TextFx.LetterSetup::m_base_colours
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * ___m_base_colours_7;
	// UnityEngine.Color[] TextFx.LetterSetup::m_base_extra_colours
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_base_extra_colours_8;
	// System.Int32[] TextFx.LetterSetup::m_base_indexes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_base_indexes_9;
	// System.Boolean TextFx.LetterSetup::m_visible_character
	bool ___m_visible_character_10;
	// System.Boolean TextFx.LetterSetup::m_stub_instance
	bool ___m_stub_instance_11;
	// System.Int32 TextFx.LetterSetup::m_mesh_index
	int32_t ___m_mesh_index_12;
	// System.Single TextFx.LetterSetup::m_width
	float ___m_width_13;
	// System.Single TextFx.LetterSetup::m_height
	float ___m_height_14;
	// TextFx.AnimationProgressionVariables TextFx.LetterSetup::m_progression_variables
	AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311 * ___m_progression_variables_15;
	// TextFx.AnimationStateVariables TextFx.LetterSetup::m_anim_state_vars
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12  ___m_anim_state_vars_16;
	// System.Single TextFx.LetterSetup::m_action_timer
	float ___m_action_timer_17;
	// System.Single TextFx.LetterSetup::m_action_delay
	float ___m_action_delay_18;
	// System.Boolean TextFx.LetterSetup::m_ignore_action_delay
	bool ___m_ignore_action_delay_19;
	// System.Single TextFx.LetterSetup::m_action_duration
	float ___m_action_duration_20;
	// TextFx.AnimatePerOptions TextFx.LetterSetup::m_last_animate_per
	int32_t ___m_last_animate_per_21;
	// UnityEngine.Vector3[] TextFx.LetterSetup::m_current_animated_vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_current_animated_vertices_22;
	// UnityEngine.Color[] TextFx.LetterSetup::m_current_animated_colours
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_current_animated_colours_23;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_local_scale_from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_local_scale_from_24;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_local_scale_to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_local_scale_to_25;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_global_scale_from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_global_scale_from_26;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_global_scale_to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_global_scale_to_27;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_local_rotation_from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_local_rotation_from_28;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_local_rotation_to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_local_rotation_to_29;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_global_rotation_from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_global_rotation_from_30;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_global_rotation_to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_global_rotation_to_31;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_position_from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_position_from_32;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_position_to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_position_to_33;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_anchor_offset_from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_anchor_offset_from_34;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_anchor_offset_to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_anchor_offset_to_35;
	// VertexColour TextFx.LetterSetup::m_colour_from
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * ___m_colour_from_36;
	// VertexColour TextFx.LetterSetup::m_colour_to
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * ___m_colour_to_37;
	// VertexColour TextFx.LetterSetup::m_letter_colour
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * ___m_letter_colour_38;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_letter_position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_letter_position_39;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_letter_scale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_letter_scale_40;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_letter_global_scale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_letter_global_scale_41;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_anchor_offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_anchor_offset_42;
	// UnityEngine.Quaternion TextFx.LetterSetup::m_letter_rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_letter_rotation_43;
	// UnityEngine.Quaternion TextFx.LetterSetup::m_letter_global_rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_letter_global_rotation_44;
	// TextFx.LETTER_ANIMATION_STATE TextFx.LetterSetup::m_current_state
	int32_t ___m_current_state_45;
	// System.Boolean TextFx.LetterSetup::m_flippedVerts
	bool ___m_flippedVerts_46;
	// TextFx.ContinueType TextFx.LetterSetup::m_continueType
	int32_t ___m_continueType_47;
	// System.Action`1<System.Single> TextFx.LetterSetup::m_continueLetterCallback
	Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * ___m_continueLetterCallback_48;
	// System.Int32 TextFx.LetterSetup::m_continueActionIndexTrigger
	int32_t ___m_continueActionIndexTrigger_49;
	// System.Int32 TextFx.LetterSetup::m_continuedLoopStartIndex
	int32_t ___m_continuedLoopStartIndex_50;
	// System.Boolean TextFx.LetterSetup::m_fastTrackLoops
	bool ___m_fastTrackLoops_51;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_anchor_offset_middle_center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_anchor_offset_middle_center_52;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_active_anchor_offset_upper_left
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_active_anchor_offset_upper_left_53;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_active_anchor_offset_upper_center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_active_anchor_offset_upper_center_54;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_active_anchor_offset_upper_right
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_active_anchor_offset_upper_right_55;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_active_anchor_offset_middle_left
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_active_anchor_offset_middle_left_56;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_active_anchor_offset_middle_center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_active_anchor_offset_middle_center_57;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_active_anchor_offset_middle_right
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_active_anchor_offset_middle_right_58;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_active_anchor_offset_lower_left
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_active_anchor_offset_lower_left_59;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_active_anchor_offset_lower_center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_active_anchor_offset_lower_center_60;
	// UnityEngine.Vector3 TextFx.LetterSetup::m_active_anchor_offset_lower_right
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_active_anchor_offset_lower_right_61;
	// UnityEngine.Quaternion TextFx.LetterSetup::m_rotationOffsetQuat
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_rotationOffsetQuat_62;
	// UnityEngine.Quaternion TextFx.LetterSetup::m_rotationOffsetQuatInverse
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_rotationOffsetQuatInverse_63;
	// System.Int32 TextFx.LetterSetup::prev_action_idx
	int32_t ___prev_action_idx_64;
	// System.Single TextFx.LetterSetup::prev_delay
	float ___prev_delay_65;
	// System.Single TextFx.LetterSetup::current_action_delay
	float ___current_action_delay_66;
	// System.Boolean TextFx.LetterSetup::altered_delay
	bool ___altered_delay_67;
	// System.Single TextFx.LetterSetup::old_action_delay
	float ___old_action_delay_68;

public:
	inline static int32_t get_offset_of_m_animation_manager_ref_0() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_animation_manager_ref_0)); }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * get_m_animation_manager_ref_0() const { return ___m_animation_manager_ref_0; }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 ** get_address_of_m_animation_manager_ref_0() { return &___m_animation_manager_ref_0; }
	inline void set_m_animation_manager_ref_0(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * value)
	{
		___m_animation_manager_ref_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_manager_ref_0), value);
	}

	inline static int32_t get_offset_of_m_current_letter_action_1() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_current_letter_action_1)); }
	inline LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67 * get_m_current_letter_action_1() const { return ___m_current_letter_action_1; }
	inline LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67 ** get_address_of_m_current_letter_action_1() { return &___m_current_letter_action_1; }
	inline void set_m_current_letter_action_1(LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67 * value)
	{
		___m_current_letter_action_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_current_letter_action_1), value);
	}

	inline static int32_t get_offset_of_m_base_vertices_2() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_base_vertices_2)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_base_vertices_2() const { return ___m_base_vertices_2; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_base_vertices_2() { return &___m_base_vertices_2; }
	inline void set_m_base_vertices_2(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_base_vertices_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_base_vertices_2), value);
	}

	inline static int32_t get_offset_of_m_base_extra_vertices_3() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_base_extra_vertices_3)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_base_extra_vertices_3() const { return ___m_base_extra_vertices_3; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_base_extra_vertices_3() { return &___m_base_extra_vertices_3; }
	inline void set_m_base_extra_vertices_3(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_base_extra_vertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_base_extra_vertices_3), value);
	}

	inline static int32_t get_offset_of_m_using_curved_data_4() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_using_curved_data_4)); }
	inline bool get_m_using_curved_data_4() const { return ___m_using_curved_data_4; }
	inline bool* get_address_of_m_using_curved_data_4() { return &___m_using_curved_data_4; }
	inline void set_m_using_curved_data_4(bool value)
	{
		___m_using_curved_data_4 = value;
	}

	inline static int32_t get_offset_of_m_curve_base_vertices_5() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_curve_base_vertices_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_curve_base_vertices_5() const { return ___m_curve_base_vertices_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_curve_base_vertices_5() { return &___m_curve_base_vertices_5; }
	inline void set_m_curve_base_vertices_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_curve_base_vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_curve_base_vertices_5), value);
	}

	inline static int32_t get_offset_of_m_curve_base_extra_vertices_6() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_curve_base_extra_vertices_6)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_curve_base_extra_vertices_6() const { return ___m_curve_base_extra_vertices_6; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_curve_base_extra_vertices_6() { return &___m_curve_base_extra_vertices_6; }
	inline void set_m_curve_base_extra_vertices_6(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_curve_base_extra_vertices_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_curve_base_extra_vertices_6), value);
	}

	inline static int32_t get_offset_of_m_base_colours_7() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_base_colours_7)); }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * get_m_base_colours_7() const { return ___m_base_colours_7; }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 ** get_address_of_m_base_colours_7() { return &___m_base_colours_7; }
	inline void set_m_base_colours_7(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * value)
	{
		___m_base_colours_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_base_colours_7), value);
	}

	inline static int32_t get_offset_of_m_base_extra_colours_8() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_base_extra_colours_8)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_base_extra_colours_8() const { return ___m_base_extra_colours_8; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_base_extra_colours_8() { return &___m_base_extra_colours_8; }
	inline void set_m_base_extra_colours_8(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_base_extra_colours_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_base_extra_colours_8), value);
	}

	inline static int32_t get_offset_of_m_base_indexes_9() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_base_indexes_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_base_indexes_9() const { return ___m_base_indexes_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_base_indexes_9() { return &___m_base_indexes_9; }
	inline void set_m_base_indexes_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_base_indexes_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_base_indexes_9), value);
	}

	inline static int32_t get_offset_of_m_visible_character_10() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_visible_character_10)); }
	inline bool get_m_visible_character_10() const { return ___m_visible_character_10; }
	inline bool* get_address_of_m_visible_character_10() { return &___m_visible_character_10; }
	inline void set_m_visible_character_10(bool value)
	{
		___m_visible_character_10 = value;
	}

	inline static int32_t get_offset_of_m_stub_instance_11() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_stub_instance_11)); }
	inline bool get_m_stub_instance_11() const { return ___m_stub_instance_11; }
	inline bool* get_address_of_m_stub_instance_11() { return &___m_stub_instance_11; }
	inline void set_m_stub_instance_11(bool value)
	{
		___m_stub_instance_11 = value;
	}

	inline static int32_t get_offset_of_m_mesh_index_12() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_mesh_index_12)); }
	inline int32_t get_m_mesh_index_12() const { return ___m_mesh_index_12; }
	inline int32_t* get_address_of_m_mesh_index_12() { return &___m_mesh_index_12; }
	inline void set_m_mesh_index_12(int32_t value)
	{
		___m_mesh_index_12 = value;
	}

	inline static int32_t get_offset_of_m_width_13() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_width_13)); }
	inline float get_m_width_13() const { return ___m_width_13; }
	inline float* get_address_of_m_width_13() { return &___m_width_13; }
	inline void set_m_width_13(float value)
	{
		___m_width_13 = value;
	}

	inline static int32_t get_offset_of_m_height_14() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_height_14)); }
	inline float get_m_height_14() const { return ___m_height_14; }
	inline float* get_address_of_m_height_14() { return &___m_height_14; }
	inline void set_m_height_14(float value)
	{
		___m_height_14 = value;
	}

	inline static int32_t get_offset_of_m_progression_variables_15() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_progression_variables_15)); }
	inline AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311 * get_m_progression_variables_15() const { return ___m_progression_variables_15; }
	inline AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311 ** get_address_of_m_progression_variables_15() { return &___m_progression_variables_15; }
	inline void set_m_progression_variables_15(AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311 * value)
	{
		___m_progression_variables_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_progression_variables_15), value);
	}

	inline static int32_t get_offset_of_m_anim_state_vars_16() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_anim_state_vars_16)); }
	inline AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12  get_m_anim_state_vars_16() const { return ___m_anim_state_vars_16; }
	inline AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12 * get_address_of_m_anim_state_vars_16() { return &___m_anim_state_vars_16; }
	inline void set_m_anim_state_vars_16(AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12  value)
	{
		___m_anim_state_vars_16 = value;
	}

	inline static int32_t get_offset_of_m_action_timer_17() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_action_timer_17)); }
	inline float get_m_action_timer_17() const { return ___m_action_timer_17; }
	inline float* get_address_of_m_action_timer_17() { return &___m_action_timer_17; }
	inline void set_m_action_timer_17(float value)
	{
		___m_action_timer_17 = value;
	}

	inline static int32_t get_offset_of_m_action_delay_18() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_action_delay_18)); }
	inline float get_m_action_delay_18() const { return ___m_action_delay_18; }
	inline float* get_address_of_m_action_delay_18() { return &___m_action_delay_18; }
	inline void set_m_action_delay_18(float value)
	{
		___m_action_delay_18 = value;
	}

	inline static int32_t get_offset_of_m_ignore_action_delay_19() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_ignore_action_delay_19)); }
	inline bool get_m_ignore_action_delay_19() const { return ___m_ignore_action_delay_19; }
	inline bool* get_address_of_m_ignore_action_delay_19() { return &___m_ignore_action_delay_19; }
	inline void set_m_ignore_action_delay_19(bool value)
	{
		___m_ignore_action_delay_19 = value;
	}

	inline static int32_t get_offset_of_m_action_duration_20() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_action_duration_20)); }
	inline float get_m_action_duration_20() const { return ___m_action_duration_20; }
	inline float* get_address_of_m_action_duration_20() { return &___m_action_duration_20; }
	inline void set_m_action_duration_20(float value)
	{
		___m_action_duration_20 = value;
	}

	inline static int32_t get_offset_of_m_last_animate_per_21() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_last_animate_per_21)); }
	inline int32_t get_m_last_animate_per_21() const { return ___m_last_animate_per_21; }
	inline int32_t* get_address_of_m_last_animate_per_21() { return &___m_last_animate_per_21; }
	inline void set_m_last_animate_per_21(int32_t value)
	{
		___m_last_animate_per_21 = value;
	}

	inline static int32_t get_offset_of_m_current_animated_vertices_22() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_current_animated_vertices_22)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_current_animated_vertices_22() const { return ___m_current_animated_vertices_22; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_current_animated_vertices_22() { return &___m_current_animated_vertices_22; }
	inline void set_m_current_animated_vertices_22(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_current_animated_vertices_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_current_animated_vertices_22), value);
	}

	inline static int32_t get_offset_of_m_current_animated_colours_23() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_current_animated_colours_23)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_current_animated_colours_23() const { return ___m_current_animated_colours_23; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_current_animated_colours_23() { return &___m_current_animated_colours_23; }
	inline void set_m_current_animated_colours_23(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_current_animated_colours_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_current_animated_colours_23), value);
	}

	inline static int32_t get_offset_of_m_local_scale_from_24() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_local_scale_from_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_local_scale_from_24() const { return ___m_local_scale_from_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_local_scale_from_24() { return &___m_local_scale_from_24; }
	inline void set_m_local_scale_from_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_local_scale_from_24 = value;
	}

	inline static int32_t get_offset_of_m_local_scale_to_25() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_local_scale_to_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_local_scale_to_25() const { return ___m_local_scale_to_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_local_scale_to_25() { return &___m_local_scale_to_25; }
	inline void set_m_local_scale_to_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_local_scale_to_25 = value;
	}

	inline static int32_t get_offset_of_m_global_scale_from_26() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_global_scale_from_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_global_scale_from_26() const { return ___m_global_scale_from_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_global_scale_from_26() { return &___m_global_scale_from_26; }
	inline void set_m_global_scale_from_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_global_scale_from_26 = value;
	}

	inline static int32_t get_offset_of_m_global_scale_to_27() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_global_scale_to_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_global_scale_to_27() const { return ___m_global_scale_to_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_global_scale_to_27() { return &___m_global_scale_to_27; }
	inline void set_m_global_scale_to_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_global_scale_to_27 = value;
	}

	inline static int32_t get_offset_of_m_local_rotation_from_28() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_local_rotation_from_28)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_local_rotation_from_28() const { return ___m_local_rotation_from_28; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_local_rotation_from_28() { return &___m_local_rotation_from_28; }
	inline void set_m_local_rotation_from_28(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_local_rotation_from_28 = value;
	}

	inline static int32_t get_offset_of_m_local_rotation_to_29() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_local_rotation_to_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_local_rotation_to_29() const { return ___m_local_rotation_to_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_local_rotation_to_29() { return &___m_local_rotation_to_29; }
	inline void set_m_local_rotation_to_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_local_rotation_to_29 = value;
	}

	inline static int32_t get_offset_of_m_global_rotation_from_30() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_global_rotation_from_30)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_global_rotation_from_30() const { return ___m_global_rotation_from_30; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_global_rotation_from_30() { return &___m_global_rotation_from_30; }
	inline void set_m_global_rotation_from_30(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_global_rotation_from_30 = value;
	}

	inline static int32_t get_offset_of_m_global_rotation_to_31() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_global_rotation_to_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_global_rotation_to_31() const { return ___m_global_rotation_to_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_global_rotation_to_31() { return &___m_global_rotation_to_31; }
	inline void set_m_global_rotation_to_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_global_rotation_to_31 = value;
	}

	inline static int32_t get_offset_of_m_position_from_32() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_position_from_32)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_position_from_32() const { return ___m_position_from_32; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_position_from_32() { return &___m_position_from_32; }
	inline void set_m_position_from_32(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_position_from_32 = value;
	}

	inline static int32_t get_offset_of_m_position_to_33() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_position_to_33)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_position_to_33() const { return ___m_position_to_33; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_position_to_33() { return &___m_position_to_33; }
	inline void set_m_position_to_33(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_position_to_33 = value;
	}

	inline static int32_t get_offset_of_m_anchor_offset_from_34() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_anchor_offset_from_34)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_anchor_offset_from_34() const { return ___m_anchor_offset_from_34; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_anchor_offset_from_34() { return &___m_anchor_offset_from_34; }
	inline void set_m_anchor_offset_from_34(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_anchor_offset_from_34 = value;
	}

	inline static int32_t get_offset_of_m_anchor_offset_to_35() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_anchor_offset_to_35)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_anchor_offset_to_35() const { return ___m_anchor_offset_to_35; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_anchor_offset_to_35() { return &___m_anchor_offset_to_35; }
	inline void set_m_anchor_offset_to_35(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_anchor_offset_to_35 = value;
	}

	inline static int32_t get_offset_of_m_colour_from_36() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_colour_from_36)); }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * get_m_colour_from_36() const { return ___m_colour_from_36; }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 ** get_address_of_m_colour_from_36() { return &___m_colour_from_36; }
	inline void set_m_colour_from_36(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * value)
	{
		___m_colour_from_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_colour_from_36), value);
	}

	inline static int32_t get_offset_of_m_colour_to_37() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_colour_to_37)); }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * get_m_colour_to_37() const { return ___m_colour_to_37; }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 ** get_address_of_m_colour_to_37() { return &___m_colour_to_37; }
	inline void set_m_colour_to_37(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * value)
	{
		___m_colour_to_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_colour_to_37), value);
	}

	inline static int32_t get_offset_of_m_letter_colour_38() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_letter_colour_38)); }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * get_m_letter_colour_38() const { return ___m_letter_colour_38; }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 ** get_address_of_m_letter_colour_38() { return &___m_letter_colour_38; }
	inline void set_m_letter_colour_38(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * value)
	{
		___m_letter_colour_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_letter_colour_38), value);
	}

	inline static int32_t get_offset_of_m_letter_position_39() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_letter_position_39)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_letter_position_39() const { return ___m_letter_position_39; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_letter_position_39() { return &___m_letter_position_39; }
	inline void set_m_letter_position_39(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_letter_position_39 = value;
	}

	inline static int32_t get_offset_of_m_letter_scale_40() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_letter_scale_40)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_letter_scale_40() const { return ___m_letter_scale_40; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_letter_scale_40() { return &___m_letter_scale_40; }
	inline void set_m_letter_scale_40(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_letter_scale_40 = value;
	}

	inline static int32_t get_offset_of_m_letter_global_scale_41() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_letter_global_scale_41)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_letter_global_scale_41() const { return ___m_letter_global_scale_41; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_letter_global_scale_41() { return &___m_letter_global_scale_41; }
	inline void set_m_letter_global_scale_41(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_letter_global_scale_41 = value;
	}

	inline static int32_t get_offset_of_m_anchor_offset_42() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_anchor_offset_42)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_anchor_offset_42() const { return ___m_anchor_offset_42; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_anchor_offset_42() { return &___m_anchor_offset_42; }
	inline void set_m_anchor_offset_42(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_anchor_offset_42 = value;
	}

	inline static int32_t get_offset_of_m_letter_rotation_43() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_letter_rotation_43)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_letter_rotation_43() const { return ___m_letter_rotation_43; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_letter_rotation_43() { return &___m_letter_rotation_43; }
	inline void set_m_letter_rotation_43(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_letter_rotation_43 = value;
	}

	inline static int32_t get_offset_of_m_letter_global_rotation_44() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_letter_global_rotation_44)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_letter_global_rotation_44() const { return ___m_letter_global_rotation_44; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_letter_global_rotation_44() { return &___m_letter_global_rotation_44; }
	inline void set_m_letter_global_rotation_44(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_letter_global_rotation_44 = value;
	}

	inline static int32_t get_offset_of_m_current_state_45() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_current_state_45)); }
	inline int32_t get_m_current_state_45() const { return ___m_current_state_45; }
	inline int32_t* get_address_of_m_current_state_45() { return &___m_current_state_45; }
	inline void set_m_current_state_45(int32_t value)
	{
		___m_current_state_45 = value;
	}

	inline static int32_t get_offset_of_m_flippedVerts_46() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_flippedVerts_46)); }
	inline bool get_m_flippedVerts_46() const { return ___m_flippedVerts_46; }
	inline bool* get_address_of_m_flippedVerts_46() { return &___m_flippedVerts_46; }
	inline void set_m_flippedVerts_46(bool value)
	{
		___m_flippedVerts_46 = value;
	}

	inline static int32_t get_offset_of_m_continueType_47() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_continueType_47)); }
	inline int32_t get_m_continueType_47() const { return ___m_continueType_47; }
	inline int32_t* get_address_of_m_continueType_47() { return &___m_continueType_47; }
	inline void set_m_continueType_47(int32_t value)
	{
		___m_continueType_47 = value;
	}

	inline static int32_t get_offset_of_m_continueLetterCallback_48() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_continueLetterCallback_48)); }
	inline Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * get_m_continueLetterCallback_48() const { return ___m_continueLetterCallback_48; }
	inline Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B ** get_address_of_m_continueLetterCallback_48() { return &___m_continueLetterCallback_48; }
	inline void set_m_continueLetterCallback_48(Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * value)
	{
		___m_continueLetterCallback_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_continueLetterCallback_48), value);
	}

	inline static int32_t get_offset_of_m_continueActionIndexTrigger_49() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_continueActionIndexTrigger_49)); }
	inline int32_t get_m_continueActionIndexTrigger_49() const { return ___m_continueActionIndexTrigger_49; }
	inline int32_t* get_address_of_m_continueActionIndexTrigger_49() { return &___m_continueActionIndexTrigger_49; }
	inline void set_m_continueActionIndexTrigger_49(int32_t value)
	{
		___m_continueActionIndexTrigger_49 = value;
	}

	inline static int32_t get_offset_of_m_continuedLoopStartIndex_50() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_continuedLoopStartIndex_50)); }
	inline int32_t get_m_continuedLoopStartIndex_50() const { return ___m_continuedLoopStartIndex_50; }
	inline int32_t* get_address_of_m_continuedLoopStartIndex_50() { return &___m_continuedLoopStartIndex_50; }
	inline void set_m_continuedLoopStartIndex_50(int32_t value)
	{
		___m_continuedLoopStartIndex_50 = value;
	}

	inline static int32_t get_offset_of_m_fastTrackLoops_51() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_fastTrackLoops_51)); }
	inline bool get_m_fastTrackLoops_51() const { return ___m_fastTrackLoops_51; }
	inline bool* get_address_of_m_fastTrackLoops_51() { return &___m_fastTrackLoops_51; }
	inline void set_m_fastTrackLoops_51(bool value)
	{
		___m_fastTrackLoops_51 = value;
	}

	inline static int32_t get_offset_of_m_anchor_offset_middle_center_52() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_anchor_offset_middle_center_52)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_anchor_offset_middle_center_52() const { return ___m_anchor_offset_middle_center_52; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_anchor_offset_middle_center_52() { return &___m_anchor_offset_middle_center_52; }
	inline void set_m_anchor_offset_middle_center_52(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_anchor_offset_middle_center_52 = value;
	}

	inline static int32_t get_offset_of_m_active_anchor_offset_upper_left_53() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_active_anchor_offset_upper_left_53)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_active_anchor_offset_upper_left_53() const { return ___m_active_anchor_offset_upper_left_53; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_active_anchor_offset_upper_left_53() { return &___m_active_anchor_offset_upper_left_53; }
	inline void set_m_active_anchor_offset_upper_left_53(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_active_anchor_offset_upper_left_53 = value;
	}

	inline static int32_t get_offset_of_m_active_anchor_offset_upper_center_54() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_active_anchor_offset_upper_center_54)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_active_anchor_offset_upper_center_54() const { return ___m_active_anchor_offset_upper_center_54; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_active_anchor_offset_upper_center_54() { return &___m_active_anchor_offset_upper_center_54; }
	inline void set_m_active_anchor_offset_upper_center_54(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_active_anchor_offset_upper_center_54 = value;
	}

	inline static int32_t get_offset_of_m_active_anchor_offset_upper_right_55() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_active_anchor_offset_upper_right_55)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_active_anchor_offset_upper_right_55() const { return ___m_active_anchor_offset_upper_right_55; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_active_anchor_offset_upper_right_55() { return &___m_active_anchor_offset_upper_right_55; }
	inline void set_m_active_anchor_offset_upper_right_55(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_active_anchor_offset_upper_right_55 = value;
	}

	inline static int32_t get_offset_of_m_active_anchor_offset_middle_left_56() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_active_anchor_offset_middle_left_56)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_active_anchor_offset_middle_left_56() const { return ___m_active_anchor_offset_middle_left_56; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_active_anchor_offset_middle_left_56() { return &___m_active_anchor_offset_middle_left_56; }
	inline void set_m_active_anchor_offset_middle_left_56(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_active_anchor_offset_middle_left_56 = value;
	}

	inline static int32_t get_offset_of_m_active_anchor_offset_middle_center_57() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_active_anchor_offset_middle_center_57)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_active_anchor_offset_middle_center_57() const { return ___m_active_anchor_offset_middle_center_57; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_active_anchor_offset_middle_center_57() { return &___m_active_anchor_offset_middle_center_57; }
	inline void set_m_active_anchor_offset_middle_center_57(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_active_anchor_offset_middle_center_57 = value;
	}

	inline static int32_t get_offset_of_m_active_anchor_offset_middle_right_58() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_active_anchor_offset_middle_right_58)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_active_anchor_offset_middle_right_58() const { return ___m_active_anchor_offset_middle_right_58; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_active_anchor_offset_middle_right_58() { return &___m_active_anchor_offset_middle_right_58; }
	inline void set_m_active_anchor_offset_middle_right_58(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_active_anchor_offset_middle_right_58 = value;
	}

	inline static int32_t get_offset_of_m_active_anchor_offset_lower_left_59() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_active_anchor_offset_lower_left_59)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_active_anchor_offset_lower_left_59() const { return ___m_active_anchor_offset_lower_left_59; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_active_anchor_offset_lower_left_59() { return &___m_active_anchor_offset_lower_left_59; }
	inline void set_m_active_anchor_offset_lower_left_59(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_active_anchor_offset_lower_left_59 = value;
	}

	inline static int32_t get_offset_of_m_active_anchor_offset_lower_center_60() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_active_anchor_offset_lower_center_60)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_active_anchor_offset_lower_center_60() const { return ___m_active_anchor_offset_lower_center_60; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_active_anchor_offset_lower_center_60() { return &___m_active_anchor_offset_lower_center_60; }
	inline void set_m_active_anchor_offset_lower_center_60(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_active_anchor_offset_lower_center_60 = value;
	}

	inline static int32_t get_offset_of_m_active_anchor_offset_lower_right_61() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_active_anchor_offset_lower_right_61)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_active_anchor_offset_lower_right_61() const { return ___m_active_anchor_offset_lower_right_61; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_active_anchor_offset_lower_right_61() { return &___m_active_anchor_offset_lower_right_61; }
	inline void set_m_active_anchor_offset_lower_right_61(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_active_anchor_offset_lower_right_61 = value;
	}

	inline static int32_t get_offset_of_m_rotationOffsetQuat_62() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_rotationOffsetQuat_62)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_rotationOffsetQuat_62() const { return ___m_rotationOffsetQuat_62; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_rotationOffsetQuat_62() { return &___m_rotationOffsetQuat_62; }
	inline void set_m_rotationOffsetQuat_62(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_rotationOffsetQuat_62 = value;
	}

	inline static int32_t get_offset_of_m_rotationOffsetQuatInverse_63() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___m_rotationOffsetQuatInverse_63)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_rotationOffsetQuatInverse_63() const { return ___m_rotationOffsetQuatInverse_63; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_rotationOffsetQuatInverse_63() { return &___m_rotationOffsetQuatInverse_63; }
	inline void set_m_rotationOffsetQuatInverse_63(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_rotationOffsetQuatInverse_63 = value;
	}

	inline static int32_t get_offset_of_prev_action_idx_64() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___prev_action_idx_64)); }
	inline int32_t get_prev_action_idx_64() const { return ___prev_action_idx_64; }
	inline int32_t* get_address_of_prev_action_idx_64() { return &___prev_action_idx_64; }
	inline void set_prev_action_idx_64(int32_t value)
	{
		___prev_action_idx_64 = value;
	}

	inline static int32_t get_offset_of_prev_delay_65() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___prev_delay_65)); }
	inline float get_prev_delay_65() const { return ___prev_delay_65; }
	inline float* get_address_of_prev_delay_65() { return &___prev_delay_65; }
	inline void set_prev_delay_65(float value)
	{
		___prev_delay_65 = value;
	}

	inline static int32_t get_offset_of_current_action_delay_66() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___current_action_delay_66)); }
	inline float get_current_action_delay_66() const { return ___current_action_delay_66; }
	inline float* get_address_of_current_action_delay_66() { return &___current_action_delay_66; }
	inline void set_current_action_delay_66(float value)
	{
		___current_action_delay_66 = value;
	}

	inline static int32_t get_offset_of_altered_delay_67() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___altered_delay_67)); }
	inline bool get_altered_delay_67() const { return ___altered_delay_67; }
	inline bool* get_address_of_altered_delay_67() { return &___altered_delay_67; }
	inline void set_altered_delay_67(bool value)
	{
		___altered_delay_67 = value;
	}

	inline static int32_t get_offset_of_old_action_delay_68() { return static_cast<int32_t>(offsetof(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E, ___old_action_delay_68)); }
	inline float get_old_action_delay_68() const { return ___old_action_delay_68; }
	inline float* get_address_of_old_action_delay_68() { return &___old_action_delay_68; }
	inline void set_old_action_delay_68(float value)
	{
		___old_action_delay_68 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERSETUP_TFF1F1C00E610201F6E3C9D01843D8866C7F6369E_H
#ifndef U3CU3EC__DISPLAYCLASS190_0_T88298C1A6D971506ED0B072A62638705AC6711C3_H
#define U3CU3EC__DISPLAYCLASS190_0_T88298C1A6D971506ED0B072A62638705AC6711C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.LetterSetup_<>c__DisplayClass190_0
struct  U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3  : public RuntimeObject
{
public:
	// TextFx.LetterAnimation TextFx.LetterSetup_<>c__DisplayClass190_0::animation
	LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9 * ___animation_0;
	// System.Int32 TextFx.LetterSetup_<>c__DisplayClass190_0::action_state_to_continue_to
	int32_t ___action_state_to_continue_to_1;
	// TextFx.LetterSetup TextFx.LetterSetup_<>c__DisplayClass190_0::<>4__this
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * ___U3CU3E4__this_2;
	// TextFx.ContinueType TextFx.LetterSetup_<>c__DisplayClass190_0::continueType
	int32_t ___continueType_3;
	// System.Single TextFx.LetterSetup_<>c__DisplayClass190_0::continueDuration
	float ___continueDuration_4;
	// System.Int32 TextFx.LetterSetup_<>c__DisplayClass190_0::action_index_to_continue_with
	int32_t ___action_index_to_continue_with_5;
	// System.Int32 TextFx.LetterSetup_<>c__DisplayClass190_0::action_index_progress
	int32_t ___action_index_progress_6;
	// System.Boolean TextFx.LetterSetup_<>c__DisplayClass190_0::use_start_state
	bool ___use_start_state_7;
	// TextFx.AnimatePerOptions TextFx.LetterSetup_<>c__DisplayClass190_0::animate_per
	int32_t ___animate_per_8;
	// System.Int32 TextFx.LetterSetup_<>c__DisplayClass190_0::deepestLoopDepth
	int32_t ___deepestLoopDepth_9;

public:
	inline static int32_t get_offset_of_animation_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3, ___animation_0)); }
	inline LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9 * get_animation_0() const { return ___animation_0; }
	inline LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9 ** get_address_of_animation_0() { return &___animation_0; }
	inline void set_animation_0(LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9 * value)
	{
		___animation_0 = value;
		Il2CppCodeGenWriteBarrier((&___animation_0), value);
	}

	inline static int32_t get_offset_of_action_state_to_continue_to_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3, ___action_state_to_continue_to_1)); }
	inline int32_t get_action_state_to_continue_to_1() const { return ___action_state_to_continue_to_1; }
	inline int32_t* get_address_of_action_state_to_continue_to_1() { return &___action_state_to_continue_to_1; }
	inline void set_action_state_to_continue_to_1(int32_t value)
	{
		___action_state_to_continue_to_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3, ___U3CU3E4__this_2)); }
	inline LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_continueType_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3, ___continueType_3)); }
	inline int32_t get_continueType_3() const { return ___continueType_3; }
	inline int32_t* get_address_of_continueType_3() { return &___continueType_3; }
	inline void set_continueType_3(int32_t value)
	{
		___continueType_3 = value;
	}

	inline static int32_t get_offset_of_continueDuration_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3, ___continueDuration_4)); }
	inline float get_continueDuration_4() const { return ___continueDuration_4; }
	inline float* get_address_of_continueDuration_4() { return &___continueDuration_4; }
	inline void set_continueDuration_4(float value)
	{
		___continueDuration_4 = value;
	}

	inline static int32_t get_offset_of_action_index_to_continue_with_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3, ___action_index_to_continue_with_5)); }
	inline int32_t get_action_index_to_continue_with_5() const { return ___action_index_to_continue_with_5; }
	inline int32_t* get_address_of_action_index_to_continue_with_5() { return &___action_index_to_continue_with_5; }
	inline void set_action_index_to_continue_with_5(int32_t value)
	{
		___action_index_to_continue_with_5 = value;
	}

	inline static int32_t get_offset_of_action_index_progress_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3, ___action_index_progress_6)); }
	inline int32_t get_action_index_progress_6() const { return ___action_index_progress_6; }
	inline int32_t* get_address_of_action_index_progress_6() { return &___action_index_progress_6; }
	inline void set_action_index_progress_6(int32_t value)
	{
		___action_index_progress_6 = value;
	}

	inline static int32_t get_offset_of_use_start_state_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3, ___use_start_state_7)); }
	inline bool get_use_start_state_7() const { return ___use_start_state_7; }
	inline bool* get_address_of_use_start_state_7() { return &___use_start_state_7; }
	inline void set_use_start_state_7(bool value)
	{
		___use_start_state_7 = value;
	}

	inline static int32_t get_offset_of_animate_per_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3, ___animate_per_8)); }
	inline int32_t get_animate_per_8() const { return ___animate_per_8; }
	inline int32_t* get_address_of_animate_per_8() { return &___animate_per_8; }
	inline void set_animate_per_8(int32_t value)
	{
		___animate_per_8 = value;
	}

	inline static int32_t get_offset_of_deepestLoopDepth_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3, ___deepestLoopDepth_9)); }
	inline int32_t get_deepestLoopDepth_9() const { return ___deepestLoopDepth_9; }
	inline int32_t* get_address_of_deepestLoopDepth_9() { return &___deepestLoopDepth_9; }
	inline void set_deepestLoopDepth_9(int32_t value)
	{
		___deepestLoopDepth_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS190_0_T88298C1A6D971506ED0B072A62638705AC6711C3_H
#ifndef PRESETEFFECTSETTING_T7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C_H
#define PRESETEFFECTSETTING_T7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.PresetEffectSetting
struct  PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C  : public RuntimeObject
{
public:
	// TextFx.ANIMATION_DATA_TYPE TextFx.PresetEffectSetting::m_data_type
	int32_t ___m_data_type_0;
	// System.Boolean TextFx.PresetEffectSetting::m_startState
	bool ___m_startState_1;
	// System.String TextFx.PresetEffectSetting::m_setting_name
	String_t* ___m_setting_name_2;
	// System.Int32 TextFx.PresetEffectSetting::m_animation_idx
	int32_t ___m_animation_idx_3;
	// System.Int32 TextFx.PresetEffectSetting::m_action_idx
	int32_t ___m_action_idx_4;
	// System.Single TextFx.PresetEffectSetting::m_action_progress_state_override
	float ___m_action_progress_state_override_5;

public:
	inline static int32_t get_offset_of_m_data_type_0() { return static_cast<int32_t>(offsetof(PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C, ___m_data_type_0)); }
	inline int32_t get_m_data_type_0() const { return ___m_data_type_0; }
	inline int32_t* get_address_of_m_data_type_0() { return &___m_data_type_0; }
	inline void set_m_data_type_0(int32_t value)
	{
		___m_data_type_0 = value;
	}

	inline static int32_t get_offset_of_m_startState_1() { return static_cast<int32_t>(offsetof(PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C, ___m_startState_1)); }
	inline bool get_m_startState_1() const { return ___m_startState_1; }
	inline bool* get_address_of_m_startState_1() { return &___m_startState_1; }
	inline void set_m_startState_1(bool value)
	{
		___m_startState_1 = value;
	}

	inline static int32_t get_offset_of_m_setting_name_2() { return static_cast<int32_t>(offsetof(PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C, ___m_setting_name_2)); }
	inline String_t* get_m_setting_name_2() const { return ___m_setting_name_2; }
	inline String_t** get_address_of_m_setting_name_2() { return &___m_setting_name_2; }
	inline void set_m_setting_name_2(String_t* value)
	{
		___m_setting_name_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_setting_name_2), value);
	}

	inline static int32_t get_offset_of_m_animation_idx_3() { return static_cast<int32_t>(offsetof(PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C, ___m_animation_idx_3)); }
	inline int32_t get_m_animation_idx_3() const { return ___m_animation_idx_3; }
	inline int32_t* get_address_of_m_animation_idx_3() { return &___m_animation_idx_3; }
	inline void set_m_animation_idx_3(int32_t value)
	{
		___m_animation_idx_3 = value;
	}

	inline static int32_t get_offset_of_m_action_idx_4() { return static_cast<int32_t>(offsetof(PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C, ___m_action_idx_4)); }
	inline int32_t get_m_action_idx_4() const { return ___m_action_idx_4; }
	inline int32_t* get_address_of_m_action_idx_4() { return &___m_action_idx_4; }
	inline void set_m_action_idx_4(int32_t value)
	{
		___m_action_idx_4 = value;
	}

	inline static int32_t get_offset_of_m_action_progress_state_override_5() { return static_cast<int32_t>(offsetof(PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C, ___m_action_progress_state_override_5)); }
	inline float get_m_action_progress_state_override_5() const { return ___m_action_progress_state_override_5; }
	inline float* get_address_of_m_action_progress_state_override_5() { return &___m_action_progress_state_override_5; }
	inline void set_m_action_progress_state_override_5(float value)
	{
		___m_action_progress_state_override_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESETEFFECTSETTING_T7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C_H
#ifndef VARIABLESTATELISTENER_T6B8087CE0DE67C734909A0C385FEF2861B2294DB_H
#define VARIABLESTATELISTENER_T6B8087CE0DE67C734909A0C385FEF2861B2294DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.PresetEffectSetting_VariableStateListener
struct  VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB  : public RuntimeObject
{
public:
	// TextFx.PresetEffectSetting_VariableStateListener_TYPE TextFx.PresetEffectSetting_VariableStateListener::m_type
	int32_t ___m_type_0;
	// System.Action`1<System.Single> TextFx.PresetEffectSetting_VariableStateListener::m_onFloatStateChangeCallback
	Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * ___m_onFloatStateChangeCallback_1;
	// System.Action`1<UnityEngine.Vector3> TextFx.PresetEffectSetting_VariableStateListener::m_onVector3StateChangeCallback
	Action_1_tE2F19E30ECDF39669C80D0E7DA21064D10C1EE2F * ___m_onVector3StateChangeCallback_2;
	// System.Action`1<UnityEngine.Color> TextFx.PresetEffectSetting_VariableStateListener::m_onColorStateChangeCallback
	Action_1_tB42F900CEA2D8B71F6167E641B4822FF8FB34159 * ___m_onColorStateChangeCallback_3;
	// System.Action`1<System.Boolean> TextFx.PresetEffectSetting_VariableStateListener::m_onToggleStateChangeCallback
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___m_onToggleStateChangeCallback_4;
	// System.Single TextFx.PresetEffectSetting_VariableStateListener::m_startFloatValue
	float ___m_startFloatValue_5;
	// UnityEngine.Vector3 TextFx.PresetEffectSetting_VariableStateListener::m_startVector3Value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_startVector3Value_6;
	// UnityEngine.Color TextFx.PresetEffectSetting_VariableStateListener::m_startColorValue
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_startColorValue_7;
	// System.Boolean TextFx.PresetEffectSetting_VariableStateListener::m_startToggleValue
	bool ___m_startToggleValue_8;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB, ___m_type_0)); }
	inline int32_t get_m_type_0() const { return ___m_type_0; }
	inline int32_t* get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(int32_t value)
	{
		___m_type_0 = value;
	}

	inline static int32_t get_offset_of_m_onFloatStateChangeCallback_1() { return static_cast<int32_t>(offsetof(VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB, ___m_onFloatStateChangeCallback_1)); }
	inline Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * get_m_onFloatStateChangeCallback_1() const { return ___m_onFloatStateChangeCallback_1; }
	inline Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B ** get_address_of_m_onFloatStateChangeCallback_1() { return &___m_onFloatStateChangeCallback_1; }
	inline void set_m_onFloatStateChangeCallback_1(Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * value)
	{
		___m_onFloatStateChangeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_onFloatStateChangeCallback_1), value);
	}

	inline static int32_t get_offset_of_m_onVector3StateChangeCallback_2() { return static_cast<int32_t>(offsetof(VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB, ___m_onVector3StateChangeCallback_2)); }
	inline Action_1_tE2F19E30ECDF39669C80D0E7DA21064D10C1EE2F * get_m_onVector3StateChangeCallback_2() const { return ___m_onVector3StateChangeCallback_2; }
	inline Action_1_tE2F19E30ECDF39669C80D0E7DA21064D10C1EE2F ** get_address_of_m_onVector3StateChangeCallback_2() { return &___m_onVector3StateChangeCallback_2; }
	inline void set_m_onVector3StateChangeCallback_2(Action_1_tE2F19E30ECDF39669C80D0E7DA21064D10C1EE2F * value)
	{
		___m_onVector3StateChangeCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_onVector3StateChangeCallback_2), value);
	}

	inline static int32_t get_offset_of_m_onColorStateChangeCallback_3() { return static_cast<int32_t>(offsetof(VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB, ___m_onColorStateChangeCallback_3)); }
	inline Action_1_tB42F900CEA2D8B71F6167E641B4822FF8FB34159 * get_m_onColorStateChangeCallback_3() const { return ___m_onColorStateChangeCallback_3; }
	inline Action_1_tB42F900CEA2D8B71F6167E641B4822FF8FB34159 ** get_address_of_m_onColorStateChangeCallback_3() { return &___m_onColorStateChangeCallback_3; }
	inline void set_m_onColorStateChangeCallback_3(Action_1_tB42F900CEA2D8B71F6167E641B4822FF8FB34159 * value)
	{
		___m_onColorStateChangeCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_onColorStateChangeCallback_3), value);
	}

	inline static int32_t get_offset_of_m_onToggleStateChangeCallback_4() { return static_cast<int32_t>(offsetof(VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB, ___m_onToggleStateChangeCallback_4)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_m_onToggleStateChangeCallback_4() const { return ___m_onToggleStateChangeCallback_4; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_m_onToggleStateChangeCallback_4() { return &___m_onToggleStateChangeCallback_4; }
	inline void set_m_onToggleStateChangeCallback_4(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___m_onToggleStateChangeCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_onToggleStateChangeCallback_4), value);
	}

	inline static int32_t get_offset_of_m_startFloatValue_5() { return static_cast<int32_t>(offsetof(VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB, ___m_startFloatValue_5)); }
	inline float get_m_startFloatValue_5() const { return ___m_startFloatValue_5; }
	inline float* get_address_of_m_startFloatValue_5() { return &___m_startFloatValue_5; }
	inline void set_m_startFloatValue_5(float value)
	{
		___m_startFloatValue_5 = value;
	}

	inline static int32_t get_offset_of_m_startVector3Value_6() { return static_cast<int32_t>(offsetof(VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB, ___m_startVector3Value_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_startVector3Value_6() const { return ___m_startVector3Value_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_startVector3Value_6() { return &___m_startVector3Value_6; }
	inline void set_m_startVector3Value_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_startVector3Value_6 = value;
	}

	inline static int32_t get_offset_of_m_startColorValue_7() { return static_cast<int32_t>(offsetof(VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB, ___m_startColorValue_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_startColorValue_7() const { return ___m_startColorValue_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_startColorValue_7() { return &___m_startColorValue_7; }
	inline void set_m_startColorValue_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_startColorValue_7 = value;
	}

	inline static int32_t get_offset_of_m_startToggleValue_8() { return static_cast<int32_t>(offsetof(VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB, ___m_startToggleValue_8)); }
	inline bool get_m_startToggleValue_8() const { return ___m_startToggleValue_8; }
	inline bool* get_address_of_m_startToggleValue_8() { return &___m_startToggleValue_8; }
	inline void set_m_startToggleValue_8(bool value)
	{
		___m_startToggleValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLESTATELISTENER_T6B8087CE0DE67C734909A0C385FEF2861B2294DB_H
#ifndef TEXTFXANIMATIONMANAGER_TC9986D5A9B84FE61F49FB56B7514A7E7F3016399_H
#define TEXTFXANIMATIONMANAGER_TC9986D5A9B84FE61F49FB56B7514A7E7F3016399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxAnimationManager
struct  TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TextFx.LetterAnimation> TextFx.TextFxAnimationManager::m_master_animations
	List_1_t3A6363802189E0E5D037C5C09A83E1220B0F1828 * ___m_master_animations_7;
	// System.Boolean TextFx.TextFxAnimationManager::m_begin_on_start
	bool ___m_begin_on_start_8;
	// TextFx.ON_FINISH_ACTION TextFx.TextFxAnimationManager::m_on_finish_action
	int32_t ___m_on_finish_action_9;
	// System.Single TextFx.TextFxAnimationManager::m_animation_speed_factor
	float ___m_animation_speed_factor_10;
	// System.Single TextFx.TextFxAnimationManager::m_begin_delay
	float ___m_begin_delay_11;
	// TextFx.AnimatePerOptions TextFx.TextFxAnimationManager::m_animate_per
	int32_t ___m_animate_per_12;
	// TextFx.AnimationTime TextFx.TextFxAnimationManager::m_time_type
	int32_t ___m_time_type_13;
	// TextFx.LetterSetup[] TextFx.TextFxAnimationManager::m_letters
	LetterSetupU5BU5D_t64E53E2965343DF3FC6FB2209365EDCE19A74C07* ___m_letters_14;
	// System.Collections.Generic.List`1<UnityEngine.AudioSource> TextFx.TextFxAnimationManager::m_audio_sources
	List_1_t2A1444170C4D0EF0D4F6CC690367E1792E8C0992 * ___m_audio_sources_15;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> TextFx.TextFxAnimationManager::m_particle_systems
	List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE * ___m_particle_systems_16;
	// System.Collections.Generic.List`1<TextFx.ParticleEffectInstanceManager> TextFx.TextFxAnimationManager::m_particle_effect_managers
	List_1_tAC55CA1D84E616FB5D8E8CB8B51335E6CC083B3F * ___m_particle_effect_managers_17;
	// UnityEngine.GameObject TextFx.TextFxAnimationManager::m_gameObect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_gameObect_18;
	// UnityEngine.Transform TextFx.TextFxAnimationManager::m_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_transform_19;
	// TextFx.TextFxAnimationInterface TextFx.TextFxAnimationManager::m_animation_interface_reference
	RuntimeObject* ___m_animation_interface_reference_20;
	// UnityEngine.MonoBehaviour TextFx.TextFxAnimationManager::m_monobehaviour
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___m_monobehaviour_21;
	// System.Int32 TextFx.TextFxAnimationManager::m_num_letters
	int32_t ___m_num_letters_22;
	// System.Int32 TextFx.TextFxAnimationManager::m_num_extra_verts_per_letter
	int32_t ___m_num_extra_verts_per_letter_23;
	// System.Collections.Generic.List`1<System.Single> TextFx.TextFxAnimationManager::m_textWidths
	List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * ___m_textWidths_24;
	// System.Collections.Generic.List`1<System.Single> TextFx.TextFxAnimationManager::m_textHeights
	List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * ___m_textHeights_25;
	// UnityEngine.Vector3[] TextFx.TextFxAnimationManager::m_current_mesh_verts
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_current_mesh_verts_26;
	// UnityEngine.Color[] TextFx.TextFxAnimationManager::m_current_mesh_colours
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_current_mesh_colours_27;
	// System.String TextFx.TextFxAnimationManager::m_current_text
	String_t* ___m_current_text_28;
	// System.Int32 TextFx.TextFxAnimationManager::m_num_words
	int32_t ___m_num_words_29;
	// System.Int32 TextFx.TextFxAnimationManager::m_num_lines
	int32_t ___m_num_lines_30;
	// System.Collections.Generic.List`1<System.Int32> TextFx.TextFxAnimationManager::m_letters_not_animated
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_letters_not_animated_31;
	// System.Single TextFx.TextFxAnimationManager::m_last_time
	float ___m_last_time_32;
	// System.Single TextFx.TextFxAnimationManager::m_animation_timer
	float ___m_animation_timer_33;
	// System.Int32 TextFx.TextFxAnimationManager::m_lowest_action_progress
	int32_t ___m_lowest_action_progress_34;
	// System.Single TextFx.TextFxAnimationManager::m_runtime_animation_speed_factor
	float ___m_runtime_animation_speed_factor_35;
	// System.Boolean TextFx.TextFxAnimationManager::m_running
	bool ___m_running_36;
	// System.Boolean TextFx.TextFxAnimationManager::m_paused
	bool ___m_paused_37;
	// System.Action TextFx.TextFxAnimationManager::m_animation_callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_animation_callback_38;
	// TextFx.ANIMATION_DATA_TYPE TextFx.TextFxAnimationManager::m_what_just_changed
	int32_t ___m_what_just_changed_39;
	// System.Action`1<System.Int32> TextFx.TextFxAnimationManager::m_animation_continue_callback
	Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B * ___m_animation_continue_callback_40;
	// System.Int32 TextFx.TextFxAnimationManager::m_dataRebuildCallFrame
	int32_t ___m_dataRebuildCallFrame_41;
	// TextFx.TextFxAnimationManager_PresetAnimationSection TextFx.TextFxAnimationManager::m_preset_intro
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * ___m_preset_intro_42;
	// TextFx.TextFxAnimationManager_PresetAnimationSection TextFx.TextFxAnimationManager::m_preset_main
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * ___m_preset_main_43;
	// TextFx.TextFxAnimationManager_PresetAnimationSection TextFx.TextFxAnimationManager::m_preset_outro
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * ___m_preset_outro_44;
	// System.Boolean TextFx.TextFxAnimationManager::m_repeat_all_sections
	bool ___m_repeat_all_sections_45;
	// System.Int32 TextFx.TextFxAnimationManager::m_repeat_all_sections_count
	int32_t ___m_repeat_all_sections_count_46;
	// System.Boolean TextFx.TextFxAnimationManager::m_curveDataApplied
	bool ___m_curveDataApplied_47;
	// System.Boolean TextFx.TextFxAnimationManager::all_letter_anims_finished
	bool ___all_letter_anims_finished_48;
	// System.Boolean TextFx.TextFxAnimationManager::all_letter_anims_waiting
	bool ___all_letter_anims_waiting_49;
	// System.Boolean TextFx.TextFxAnimationManager::all_letter_anims_waiting_infinitely
	bool ___all_letter_anims_waiting_infinitely_50;
	// System.Boolean TextFx.TextFxAnimationManager::all_letter_anims_continuing_finished
	bool ___all_letter_anims_continuing_finished_51;
	// System.Int32 TextFx.TextFxAnimationManager::lowest_action_progress
	int32_t ___lowest_action_progress_52;
	// System.Int32 TextFx.TextFxAnimationManager::last_letter_idx
	int32_t ___last_letter_idx_53;
	// TextFx.LetterSetup TextFx.TextFxAnimationManager::letter_setup
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * ___letter_setup_54;
	// TextFx.LetterAnimation TextFx.TextFxAnimationManager::letterAnimation
	LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9 * ___letterAnimation_55;
	// UnityEngine.Vector3[] TextFx.TextFxAnimationManager::letter_verts
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___letter_verts_56;
	// UnityEngine.Color[] TextFx.TextFxAnimationManager::letter_colours
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___letter_colours_57;

public:
	inline static int32_t get_offset_of_m_master_animations_7() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_master_animations_7)); }
	inline List_1_t3A6363802189E0E5D037C5C09A83E1220B0F1828 * get_m_master_animations_7() const { return ___m_master_animations_7; }
	inline List_1_t3A6363802189E0E5D037C5C09A83E1220B0F1828 ** get_address_of_m_master_animations_7() { return &___m_master_animations_7; }
	inline void set_m_master_animations_7(List_1_t3A6363802189E0E5D037C5C09A83E1220B0F1828 * value)
	{
		___m_master_animations_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_master_animations_7), value);
	}

	inline static int32_t get_offset_of_m_begin_on_start_8() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_begin_on_start_8)); }
	inline bool get_m_begin_on_start_8() const { return ___m_begin_on_start_8; }
	inline bool* get_address_of_m_begin_on_start_8() { return &___m_begin_on_start_8; }
	inline void set_m_begin_on_start_8(bool value)
	{
		___m_begin_on_start_8 = value;
	}

	inline static int32_t get_offset_of_m_on_finish_action_9() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_on_finish_action_9)); }
	inline int32_t get_m_on_finish_action_9() const { return ___m_on_finish_action_9; }
	inline int32_t* get_address_of_m_on_finish_action_9() { return &___m_on_finish_action_9; }
	inline void set_m_on_finish_action_9(int32_t value)
	{
		___m_on_finish_action_9 = value;
	}

	inline static int32_t get_offset_of_m_animation_speed_factor_10() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_animation_speed_factor_10)); }
	inline float get_m_animation_speed_factor_10() const { return ___m_animation_speed_factor_10; }
	inline float* get_address_of_m_animation_speed_factor_10() { return &___m_animation_speed_factor_10; }
	inline void set_m_animation_speed_factor_10(float value)
	{
		___m_animation_speed_factor_10 = value;
	}

	inline static int32_t get_offset_of_m_begin_delay_11() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_begin_delay_11)); }
	inline float get_m_begin_delay_11() const { return ___m_begin_delay_11; }
	inline float* get_address_of_m_begin_delay_11() { return &___m_begin_delay_11; }
	inline void set_m_begin_delay_11(float value)
	{
		___m_begin_delay_11 = value;
	}

	inline static int32_t get_offset_of_m_animate_per_12() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_animate_per_12)); }
	inline int32_t get_m_animate_per_12() const { return ___m_animate_per_12; }
	inline int32_t* get_address_of_m_animate_per_12() { return &___m_animate_per_12; }
	inline void set_m_animate_per_12(int32_t value)
	{
		___m_animate_per_12 = value;
	}

	inline static int32_t get_offset_of_m_time_type_13() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_time_type_13)); }
	inline int32_t get_m_time_type_13() const { return ___m_time_type_13; }
	inline int32_t* get_address_of_m_time_type_13() { return &___m_time_type_13; }
	inline void set_m_time_type_13(int32_t value)
	{
		___m_time_type_13 = value;
	}

	inline static int32_t get_offset_of_m_letters_14() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_letters_14)); }
	inline LetterSetupU5BU5D_t64E53E2965343DF3FC6FB2209365EDCE19A74C07* get_m_letters_14() const { return ___m_letters_14; }
	inline LetterSetupU5BU5D_t64E53E2965343DF3FC6FB2209365EDCE19A74C07** get_address_of_m_letters_14() { return &___m_letters_14; }
	inline void set_m_letters_14(LetterSetupU5BU5D_t64E53E2965343DF3FC6FB2209365EDCE19A74C07* value)
	{
		___m_letters_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_letters_14), value);
	}

	inline static int32_t get_offset_of_m_audio_sources_15() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_audio_sources_15)); }
	inline List_1_t2A1444170C4D0EF0D4F6CC690367E1792E8C0992 * get_m_audio_sources_15() const { return ___m_audio_sources_15; }
	inline List_1_t2A1444170C4D0EF0D4F6CC690367E1792E8C0992 ** get_address_of_m_audio_sources_15() { return &___m_audio_sources_15; }
	inline void set_m_audio_sources_15(List_1_t2A1444170C4D0EF0D4F6CC690367E1792E8C0992 * value)
	{
		___m_audio_sources_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_audio_sources_15), value);
	}

	inline static int32_t get_offset_of_m_particle_systems_16() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_particle_systems_16)); }
	inline List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE * get_m_particle_systems_16() const { return ___m_particle_systems_16; }
	inline List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE ** get_address_of_m_particle_systems_16() { return &___m_particle_systems_16; }
	inline void set_m_particle_systems_16(List_1_tFA660ACCBB75B7DE99651E0CED2A469BEEEF76CE * value)
	{
		___m_particle_systems_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_particle_systems_16), value);
	}

	inline static int32_t get_offset_of_m_particle_effect_managers_17() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_particle_effect_managers_17)); }
	inline List_1_tAC55CA1D84E616FB5D8E8CB8B51335E6CC083B3F * get_m_particle_effect_managers_17() const { return ___m_particle_effect_managers_17; }
	inline List_1_tAC55CA1D84E616FB5D8E8CB8B51335E6CC083B3F ** get_address_of_m_particle_effect_managers_17() { return &___m_particle_effect_managers_17; }
	inline void set_m_particle_effect_managers_17(List_1_tAC55CA1D84E616FB5D8E8CB8B51335E6CC083B3F * value)
	{
		___m_particle_effect_managers_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_particle_effect_managers_17), value);
	}

	inline static int32_t get_offset_of_m_gameObect_18() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_gameObect_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_gameObect_18() const { return ___m_gameObect_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_gameObect_18() { return &___m_gameObect_18; }
	inline void set_m_gameObect_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_gameObect_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameObect_18), value);
	}

	inline static int32_t get_offset_of_m_transform_19() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_transform_19)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_transform_19() const { return ___m_transform_19; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_transform_19() { return &___m_transform_19; }
	inline void set_m_transform_19(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_transform_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_19), value);
	}

	inline static int32_t get_offset_of_m_animation_interface_reference_20() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_animation_interface_reference_20)); }
	inline RuntimeObject* get_m_animation_interface_reference_20() const { return ___m_animation_interface_reference_20; }
	inline RuntimeObject** get_address_of_m_animation_interface_reference_20() { return &___m_animation_interface_reference_20; }
	inline void set_m_animation_interface_reference_20(RuntimeObject* value)
	{
		___m_animation_interface_reference_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_interface_reference_20), value);
	}

	inline static int32_t get_offset_of_m_monobehaviour_21() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_monobehaviour_21)); }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * get_m_monobehaviour_21() const { return ___m_monobehaviour_21; }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** get_address_of_m_monobehaviour_21() { return &___m_monobehaviour_21; }
	inline void set_m_monobehaviour_21(MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		___m_monobehaviour_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_monobehaviour_21), value);
	}

	inline static int32_t get_offset_of_m_num_letters_22() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_num_letters_22)); }
	inline int32_t get_m_num_letters_22() const { return ___m_num_letters_22; }
	inline int32_t* get_address_of_m_num_letters_22() { return &___m_num_letters_22; }
	inline void set_m_num_letters_22(int32_t value)
	{
		___m_num_letters_22 = value;
	}

	inline static int32_t get_offset_of_m_num_extra_verts_per_letter_23() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_num_extra_verts_per_letter_23)); }
	inline int32_t get_m_num_extra_verts_per_letter_23() const { return ___m_num_extra_verts_per_letter_23; }
	inline int32_t* get_address_of_m_num_extra_verts_per_letter_23() { return &___m_num_extra_verts_per_letter_23; }
	inline void set_m_num_extra_verts_per_letter_23(int32_t value)
	{
		___m_num_extra_verts_per_letter_23 = value;
	}

	inline static int32_t get_offset_of_m_textWidths_24() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_textWidths_24)); }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * get_m_textWidths_24() const { return ___m_textWidths_24; }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 ** get_address_of_m_textWidths_24() { return &___m_textWidths_24; }
	inline void set_m_textWidths_24(List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * value)
	{
		___m_textWidths_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_textWidths_24), value);
	}

	inline static int32_t get_offset_of_m_textHeights_25() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_textHeights_25)); }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * get_m_textHeights_25() const { return ___m_textHeights_25; }
	inline List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 ** get_address_of_m_textHeights_25() { return &___m_textHeights_25; }
	inline void set_m_textHeights_25(List_1_t8980FA0E6CB3848F706C43D859930435C34BCC37 * value)
	{
		___m_textHeights_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_textHeights_25), value);
	}

	inline static int32_t get_offset_of_m_current_mesh_verts_26() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_current_mesh_verts_26)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_current_mesh_verts_26() const { return ___m_current_mesh_verts_26; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_current_mesh_verts_26() { return &___m_current_mesh_verts_26; }
	inline void set_m_current_mesh_verts_26(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_current_mesh_verts_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_current_mesh_verts_26), value);
	}

	inline static int32_t get_offset_of_m_current_mesh_colours_27() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_current_mesh_colours_27)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_current_mesh_colours_27() const { return ___m_current_mesh_colours_27; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_current_mesh_colours_27() { return &___m_current_mesh_colours_27; }
	inline void set_m_current_mesh_colours_27(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_current_mesh_colours_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_current_mesh_colours_27), value);
	}

	inline static int32_t get_offset_of_m_current_text_28() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_current_text_28)); }
	inline String_t* get_m_current_text_28() const { return ___m_current_text_28; }
	inline String_t** get_address_of_m_current_text_28() { return &___m_current_text_28; }
	inline void set_m_current_text_28(String_t* value)
	{
		___m_current_text_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_current_text_28), value);
	}

	inline static int32_t get_offset_of_m_num_words_29() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_num_words_29)); }
	inline int32_t get_m_num_words_29() const { return ___m_num_words_29; }
	inline int32_t* get_address_of_m_num_words_29() { return &___m_num_words_29; }
	inline void set_m_num_words_29(int32_t value)
	{
		___m_num_words_29 = value;
	}

	inline static int32_t get_offset_of_m_num_lines_30() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_num_lines_30)); }
	inline int32_t get_m_num_lines_30() const { return ___m_num_lines_30; }
	inline int32_t* get_address_of_m_num_lines_30() { return &___m_num_lines_30; }
	inline void set_m_num_lines_30(int32_t value)
	{
		___m_num_lines_30 = value;
	}

	inline static int32_t get_offset_of_m_letters_not_animated_31() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_letters_not_animated_31)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_letters_not_animated_31() const { return ___m_letters_not_animated_31; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_letters_not_animated_31() { return &___m_letters_not_animated_31; }
	inline void set_m_letters_not_animated_31(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_letters_not_animated_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_letters_not_animated_31), value);
	}

	inline static int32_t get_offset_of_m_last_time_32() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_last_time_32)); }
	inline float get_m_last_time_32() const { return ___m_last_time_32; }
	inline float* get_address_of_m_last_time_32() { return &___m_last_time_32; }
	inline void set_m_last_time_32(float value)
	{
		___m_last_time_32 = value;
	}

	inline static int32_t get_offset_of_m_animation_timer_33() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_animation_timer_33)); }
	inline float get_m_animation_timer_33() const { return ___m_animation_timer_33; }
	inline float* get_address_of_m_animation_timer_33() { return &___m_animation_timer_33; }
	inline void set_m_animation_timer_33(float value)
	{
		___m_animation_timer_33 = value;
	}

	inline static int32_t get_offset_of_m_lowest_action_progress_34() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_lowest_action_progress_34)); }
	inline int32_t get_m_lowest_action_progress_34() const { return ___m_lowest_action_progress_34; }
	inline int32_t* get_address_of_m_lowest_action_progress_34() { return &___m_lowest_action_progress_34; }
	inline void set_m_lowest_action_progress_34(int32_t value)
	{
		___m_lowest_action_progress_34 = value;
	}

	inline static int32_t get_offset_of_m_runtime_animation_speed_factor_35() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_runtime_animation_speed_factor_35)); }
	inline float get_m_runtime_animation_speed_factor_35() const { return ___m_runtime_animation_speed_factor_35; }
	inline float* get_address_of_m_runtime_animation_speed_factor_35() { return &___m_runtime_animation_speed_factor_35; }
	inline void set_m_runtime_animation_speed_factor_35(float value)
	{
		___m_runtime_animation_speed_factor_35 = value;
	}

	inline static int32_t get_offset_of_m_running_36() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_running_36)); }
	inline bool get_m_running_36() const { return ___m_running_36; }
	inline bool* get_address_of_m_running_36() { return &___m_running_36; }
	inline void set_m_running_36(bool value)
	{
		___m_running_36 = value;
	}

	inline static int32_t get_offset_of_m_paused_37() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_paused_37)); }
	inline bool get_m_paused_37() const { return ___m_paused_37; }
	inline bool* get_address_of_m_paused_37() { return &___m_paused_37; }
	inline void set_m_paused_37(bool value)
	{
		___m_paused_37 = value;
	}

	inline static int32_t get_offset_of_m_animation_callback_38() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_animation_callback_38)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_animation_callback_38() const { return ___m_animation_callback_38; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_animation_callback_38() { return &___m_animation_callback_38; }
	inline void set_m_animation_callback_38(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_animation_callback_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_callback_38), value);
	}

	inline static int32_t get_offset_of_m_what_just_changed_39() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_what_just_changed_39)); }
	inline int32_t get_m_what_just_changed_39() const { return ___m_what_just_changed_39; }
	inline int32_t* get_address_of_m_what_just_changed_39() { return &___m_what_just_changed_39; }
	inline void set_m_what_just_changed_39(int32_t value)
	{
		___m_what_just_changed_39 = value;
	}

	inline static int32_t get_offset_of_m_animation_continue_callback_40() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_animation_continue_callback_40)); }
	inline Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B * get_m_animation_continue_callback_40() const { return ___m_animation_continue_callback_40; }
	inline Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B ** get_address_of_m_animation_continue_callback_40() { return &___m_animation_continue_callback_40; }
	inline void set_m_animation_continue_callback_40(Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B * value)
	{
		___m_animation_continue_callback_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_continue_callback_40), value);
	}

	inline static int32_t get_offset_of_m_dataRebuildCallFrame_41() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_dataRebuildCallFrame_41)); }
	inline int32_t get_m_dataRebuildCallFrame_41() const { return ___m_dataRebuildCallFrame_41; }
	inline int32_t* get_address_of_m_dataRebuildCallFrame_41() { return &___m_dataRebuildCallFrame_41; }
	inline void set_m_dataRebuildCallFrame_41(int32_t value)
	{
		___m_dataRebuildCallFrame_41 = value;
	}

	inline static int32_t get_offset_of_m_preset_intro_42() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_preset_intro_42)); }
	inline PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * get_m_preset_intro_42() const { return ___m_preset_intro_42; }
	inline PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E ** get_address_of_m_preset_intro_42() { return &___m_preset_intro_42; }
	inline void set_m_preset_intro_42(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * value)
	{
		___m_preset_intro_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_preset_intro_42), value);
	}

	inline static int32_t get_offset_of_m_preset_main_43() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_preset_main_43)); }
	inline PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * get_m_preset_main_43() const { return ___m_preset_main_43; }
	inline PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E ** get_address_of_m_preset_main_43() { return &___m_preset_main_43; }
	inline void set_m_preset_main_43(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * value)
	{
		___m_preset_main_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_preset_main_43), value);
	}

	inline static int32_t get_offset_of_m_preset_outro_44() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_preset_outro_44)); }
	inline PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * get_m_preset_outro_44() const { return ___m_preset_outro_44; }
	inline PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E ** get_address_of_m_preset_outro_44() { return &___m_preset_outro_44; }
	inline void set_m_preset_outro_44(PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E * value)
	{
		___m_preset_outro_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_preset_outro_44), value);
	}

	inline static int32_t get_offset_of_m_repeat_all_sections_45() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_repeat_all_sections_45)); }
	inline bool get_m_repeat_all_sections_45() const { return ___m_repeat_all_sections_45; }
	inline bool* get_address_of_m_repeat_all_sections_45() { return &___m_repeat_all_sections_45; }
	inline void set_m_repeat_all_sections_45(bool value)
	{
		___m_repeat_all_sections_45 = value;
	}

	inline static int32_t get_offset_of_m_repeat_all_sections_count_46() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_repeat_all_sections_count_46)); }
	inline int32_t get_m_repeat_all_sections_count_46() const { return ___m_repeat_all_sections_count_46; }
	inline int32_t* get_address_of_m_repeat_all_sections_count_46() { return &___m_repeat_all_sections_count_46; }
	inline void set_m_repeat_all_sections_count_46(int32_t value)
	{
		___m_repeat_all_sections_count_46 = value;
	}

	inline static int32_t get_offset_of_m_curveDataApplied_47() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___m_curveDataApplied_47)); }
	inline bool get_m_curveDataApplied_47() const { return ___m_curveDataApplied_47; }
	inline bool* get_address_of_m_curveDataApplied_47() { return &___m_curveDataApplied_47; }
	inline void set_m_curveDataApplied_47(bool value)
	{
		___m_curveDataApplied_47 = value;
	}

	inline static int32_t get_offset_of_all_letter_anims_finished_48() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___all_letter_anims_finished_48)); }
	inline bool get_all_letter_anims_finished_48() const { return ___all_letter_anims_finished_48; }
	inline bool* get_address_of_all_letter_anims_finished_48() { return &___all_letter_anims_finished_48; }
	inline void set_all_letter_anims_finished_48(bool value)
	{
		___all_letter_anims_finished_48 = value;
	}

	inline static int32_t get_offset_of_all_letter_anims_waiting_49() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___all_letter_anims_waiting_49)); }
	inline bool get_all_letter_anims_waiting_49() const { return ___all_letter_anims_waiting_49; }
	inline bool* get_address_of_all_letter_anims_waiting_49() { return &___all_letter_anims_waiting_49; }
	inline void set_all_letter_anims_waiting_49(bool value)
	{
		___all_letter_anims_waiting_49 = value;
	}

	inline static int32_t get_offset_of_all_letter_anims_waiting_infinitely_50() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___all_letter_anims_waiting_infinitely_50)); }
	inline bool get_all_letter_anims_waiting_infinitely_50() const { return ___all_letter_anims_waiting_infinitely_50; }
	inline bool* get_address_of_all_letter_anims_waiting_infinitely_50() { return &___all_letter_anims_waiting_infinitely_50; }
	inline void set_all_letter_anims_waiting_infinitely_50(bool value)
	{
		___all_letter_anims_waiting_infinitely_50 = value;
	}

	inline static int32_t get_offset_of_all_letter_anims_continuing_finished_51() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___all_letter_anims_continuing_finished_51)); }
	inline bool get_all_letter_anims_continuing_finished_51() const { return ___all_letter_anims_continuing_finished_51; }
	inline bool* get_address_of_all_letter_anims_continuing_finished_51() { return &___all_letter_anims_continuing_finished_51; }
	inline void set_all_letter_anims_continuing_finished_51(bool value)
	{
		___all_letter_anims_continuing_finished_51 = value;
	}

	inline static int32_t get_offset_of_lowest_action_progress_52() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___lowest_action_progress_52)); }
	inline int32_t get_lowest_action_progress_52() const { return ___lowest_action_progress_52; }
	inline int32_t* get_address_of_lowest_action_progress_52() { return &___lowest_action_progress_52; }
	inline void set_lowest_action_progress_52(int32_t value)
	{
		___lowest_action_progress_52 = value;
	}

	inline static int32_t get_offset_of_last_letter_idx_53() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___last_letter_idx_53)); }
	inline int32_t get_last_letter_idx_53() const { return ___last_letter_idx_53; }
	inline int32_t* get_address_of_last_letter_idx_53() { return &___last_letter_idx_53; }
	inline void set_last_letter_idx_53(int32_t value)
	{
		___last_letter_idx_53 = value;
	}

	inline static int32_t get_offset_of_letter_setup_54() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___letter_setup_54)); }
	inline LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * get_letter_setup_54() const { return ___letter_setup_54; }
	inline LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E ** get_address_of_letter_setup_54() { return &___letter_setup_54; }
	inline void set_letter_setup_54(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * value)
	{
		___letter_setup_54 = value;
		Il2CppCodeGenWriteBarrier((&___letter_setup_54), value);
	}

	inline static int32_t get_offset_of_letterAnimation_55() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___letterAnimation_55)); }
	inline LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9 * get_letterAnimation_55() const { return ___letterAnimation_55; }
	inline LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9 ** get_address_of_letterAnimation_55() { return &___letterAnimation_55; }
	inline void set_letterAnimation_55(LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9 * value)
	{
		___letterAnimation_55 = value;
		Il2CppCodeGenWriteBarrier((&___letterAnimation_55), value);
	}

	inline static int32_t get_offset_of_letter_verts_56() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___letter_verts_56)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_letter_verts_56() const { return ___letter_verts_56; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_letter_verts_56() { return &___letter_verts_56; }
	inline void set_letter_verts_56(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___letter_verts_56 = value;
		Il2CppCodeGenWriteBarrier((&___letter_verts_56), value);
	}

	inline static int32_t get_offset_of_letter_colours_57() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399, ___letter_colours_57)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_letter_colours_57() const { return ___letter_colours_57; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_letter_colours_57() { return &___letter_colours_57; }
	inline void set_letter_colours_57(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___letter_colours_57 = value;
		Il2CppCodeGenWriteBarrier((&___letter_colours_57), value);
	}
};

struct TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399_StaticFields
{
public:
	// System.String[] TextFx.TextFxAnimationManager::m_animation_section_names
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_animation_section_names_0;
	// System.String[] TextFx.TextFxAnimationManager::m_animation_section_folders
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_animation_section_folders_1;

public:
	inline static int32_t get_offset_of_m_animation_section_names_0() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399_StaticFields, ___m_animation_section_names_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_animation_section_names_0() const { return ___m_animation_section_names_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_animation_section_names_0() { return &___m_animation_section_names_0; }
	inline void set_m_animation_section_names_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_animation_section_names_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_section_names_0), value);
	}

	inline static int32_t get_offset_of_m_animation_section_folders_1() { return static_cast<int32_t>(offsetof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399_StaticFields, ___m_animation_section_folders_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_animation_section_folders_1() const { return ___m_animation_section_folders_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_animation_section_folders_1() { return &___m_animation_section_folders_1; }
	inline void set_m_animation_section_folders_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_animation_section_folders_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_section_folders_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXANIMATIONMANAGER_TC9986D5A9B84FE61F49FB56B7514A7E7F3016399_H
#ifndef U3CTIMEDELAYU3ED__157_T4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101_H
#define U3CTIMEDELAYU3ED__157_T4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxAnimationManager_<TimeDelay>d__157
struct  U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101  : public RuntimeObject
{
public:
	// System.Int32 TextFx.TextFxAnimationManager_<TimeDelay>d__157::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TextFx.TextFxAnimationManager_<TimeDelay>d__157::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TextFx.AnimationTime TextFx.TextFxAnimationManager_<TimeDelay>d__157::time_type
	int32_t ___time_type_2;
	// System.Single TextFx.TextFxAnimationManager_<TimeDelay>d__157::delay
	float ___delay_3;
	// System.Single TextFx.TextFxAnimationManager_<TimeDelay>d__157::<timer>5__2
	float ___U3CtimerU3E5__2_4;
	// System.Single TextFx.TextFxAnimationManager_<TimeDelay>d__157::<last_time>5__3
	float ___U3Clast_timeU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_time_type_2() { return static_cast<int32_t>(offsetof(U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101, ___time_type_2)); }
	inline int32_t get_time_type_2() const { return ___time_type_2; }
	inline int32_t* get_address_of_time_type_2() { return &___time_type_2; }
	inline void set_time_type_2(int32_t value)
	{
		___time_type_2 = value;
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_U3CtimerU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101, ___U3CtimerU3E5__2_4)); }
	inline float get_U3CtimerU3E5__2_4() const { return ___U3CtimerU3E5__2_4; }
	inline float* get_address_of_U3CtimerU3E5__2_4() { return &___U3CtimerU3E5__2_4; }
	inline void set_U3CtimerU3E5__2_4(float value)
	{
		___U3CtimerU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3Clast_timeU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101, ___U3Clast_timeU3E5__3_5)); }
	inline float get_U3Clast_timeU3E5__3_5() const { return ___U3Clast_timeU3E5__3_5; }
	inline float* get_address_of_U3Clast_timeU3E5__3_5() { return &___U3Clast_timeU3E5__3_5; }
	inline void set_U3Clast_timeU3E5__3_5(float value)
	{
		___U3Clast_timeU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDELAYU3ED__157_T4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_TA1E408646D4814FEEDB47A631771A175249DD7C8_H
#define U3CU3EC__DISPLAYCLASS11_0_TA1E408646D4814FEEDB47A631771A175249DD7C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxBezierCurve_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8  : public RuntimeObject
{
public:
	// UnityEngine.TextAlignment TextFx.TextFxBezierCurve_<>c__DisplayClass11_0::alignment
	int32_t ___alignment_0;
	// System.Single TextFx.TextFxBezierCurve_<>c__DisplayClass11_0::renderedTextWidth
	float ___renderedTextWidth_1;
	// TextFx.TextFxAnimationManager TextFx.TextFxBezierCurve_<>c__DisplayClass11_0::anim_manager
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * ___anim_manager_2;
	// TextFx.LetterSetup TextFx.TextFxBezierCurve_<>c__DisplayClass11_0::letter
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * ___letter_3;
	// System.Single TextFx.TextFxBezierCurve_<>c__DisplayClass11_0::curve_length
	float ___curve_length_4;
	// System.Single TextFx.TextFxBezierCurve_<>c__DisplayClass11_0::letters_offset
	float ___letters_offset_5;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_renderedTextWidth_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8, ___renderedTextWidth_1)); }
	inline float get_renderedTextWidth_1() const { return ___renderedTextWidth_1; }
	inline float* get_address_of_renderedTextWidth_1() { return &___renderedTextWidth_1; }
	inline void set_renderedTextWidth_1(float value)
	{
		___renderedTextWidth_1 = value;
	}

	inline static int32_t get_offset_of_anim_manager_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8, ___anim_manager_2)); }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * get_anim_manager_2() const { return ___anim_manager_2; }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 ** get_address_of_anim_manager_2() { return &___anim_manager_2; }
	inline void set_anim_manager_2(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * value)
	{
		___anim_manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___anim_manager_2), value);
	}

	inline static int32_t get_offset_of_letter_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8, ___letter_3)); }
	inline LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * get_letter_3() const { return ___letter_3; }
	inline LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E ** get_address_of_letter_3() { return &___letter_3; }
	inline void set_letter_3(LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E * value)
	{
		___letter_3 = value;
		Il2CppCodeGenWriteBarrier((&___letter_3), value);
	}

	inline static int32_t get_offset_of_curve_length_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8, ___curve_length_4)); }
	inline float get_curve_length_4() const { return ___curve_length_4; }
	inline float* get_address_of_curve_length_4() { return &___curve_length_4; }
	inline void set_curve_length_4(float value)
	{
		___curve_length_4 = value;
	}

	inline static int32_t get_offset_of_letters_offset_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8, ___letters_offset_5)); }
	inline float get_letters_offset_5() const { return ___letters_offset_5; }
	inline float* get_address_of_letters_offset_5() { return &___letters_offset_5; }
	inline void set_letters_offset_5(float value)
	{
		___letters_offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_TA1E408646D4814FEEDB47A631771A175249DD7C8_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#define NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation_Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifndef WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#define WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	// TMPro.TMP_FontStyleStack TMPro.WordWrapState::basicStyleStack
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___sizeStack_39;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___indentStack_40;
	// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight> TMPro.WordWrapState::fontWeightStack
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___styleStack_42;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___baselineStack_43;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___actionStack_44;
	// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___textInfo_27)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineInfo_28)); }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___vertexColor_29)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColor_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___basicStyleStack_33)); }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorStack_34)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColorStack_35)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColorStack_36)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColorStack_37)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorGradientStack_38)); }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___sizeStack_39)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___indentStack_40)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontWeightStack_41)); }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___styleStack_42)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineStack_43)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___actionStack_44)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___materialReferenceStack_45)); }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineJustificationStack_46)); }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterial_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___meshExtents_52)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___sizeStack_39;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___indentStack_40;
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___styleStack_42;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___baselineStack_43;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___actionStack_44;
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___sizeStack_39;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___indentStack_40;
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___styleStack_42;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___baselineStack_43;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___actionStack_44;
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifndef ACTIONVARIABLEPROGRESSION_TF36AFCFFC89DF7318F20B441D78CB48B2AA8A372_H
#define ACTIONVARIABLEPROGRESSION_TF36AFCFFC89DF7318F20B441D78CB48B2AA8A372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ActionVariableProgression
struct  ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372  : public RuntimeObject
{
public:
	// TextFx.ActionVariableProgressionReferenceData TextFx.ActionVariableProgression::m_reference_data
	ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7  ___m_reference_data_0;
	// TextFx.ValueProgression TextFx.ActionVariableProgression::m_progression
	int32_t ___m_progression_1;
	// System.Int32 TextFx.ActionVariableProgression::m_progression_idx
	int32_t ___m_progression_idx_2;
	// EasingEquation TextFx.ActionVariableProgression::m_ease_type
	int32_t ___m_ease_type_3;
	// System.Boolean TextFx.ActionVariableProgression::m_is_offset_from_last
	bool ___m_is_offset_from_last_4;
	// System.Boolean TextFx.ActionVariableProgression::m_to_to_bool
	bool ___m_to_to_bool_5;
	// System.Boolean TextFx.ActionVariableProgression::m_unique_randoms
	bool ___m_unique_randoms_6;
	// TextFx.AnimatePerOptions TextFx.ActionVariableProgression::m_animate_per
	int32_t ___m_animate_per_7;
	// System.Boolean TextFx.ActionVariableProgression::m_override_animate_per_option
	bool ___m_override_animate_per_option_8;
	// UnityEngine.AnimationCurve TextFx.ActionVariableProgression::m_custom_ease_curve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_custom_ease_curve_9;

public:
	inline static int32_t get_offset_of_m_reference_data_0() { return static_cast<int32_t>(offsetof(ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372, ___m_reference_data_0)); }
	inline ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7  get_m_reference_data_0() const { return ___m_reference_data_0; }
	inline ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7 * get_address_of_m_reference_data_0() { return &___m_reference_data_0; }
	inline void set_m_reference_data_0(ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7  value)
	{
		___m_reference_data_0 = value;
	}

	inline static int32_t get_offset_of_m_progression_1() { return static_cast<int32_t>(offsetof(ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372, ___m_progression_1)); }
	inline int32_t get_m_progression_1() const { return ___m_progression_1; }
	inline int32_t* get_address_of_m_progression_1() { return &___m_progression_1; }
	inline void set_m_progression_1(int32_t value)
	{
		___m_progression_1 = value;
	}

	inline static int32_t get_offset_of_m_progression_idx_2() { return static_cast<int32_t>(offsetof(ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372, ___m_progression_idx_2)); }
	inline int32_t get_m_progression_idx_2() const { return ___m_progression_idx_2; }
	inline int32_t* get_address_of_m_progression_idx_2() { return &___m_progression_idx_2; }
	inline void set_m_progression_idx_2(int32_t value)
	{
		___m_progression_idx_2 = value;
	}

	inline static int32_t get_offset_of_m_ease_type_3() { return static_cast<int32_t>(offsetof(ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372, ___m_ease_type_3)); }
	inline int32_t get_m_ease_type_3() const { return ___m_ease_type_3; }
	inline int32_t* get_address_of_m_ease_type_3() { return &___m_ease_type_3; }
	inline void set_m_ease_type_3(int32_t value)
	{
		___m_ease_type_3 = value;
	}

	inline static int32_t get_offset_of_m_is_offset_from_last_4() { return static_cast<int32_t>(offsetof(ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372, ___m_is_offset_from_last_4)); }
	inline bool get_m_is_offset_from_last_4() const { return ___m_is_offset_from_last_4; }
	inline bool* get_address_of_m_is_offset_from_last_4() { return &___m_is_offset_from_last_4; }
	inline void set_m_is_offset_from_last_4(bool value)
	{
		___m_is_offset_from_last_4 = value;
	}

	inline static int32_t get_offset_of_m_to_to_bool_5() { return static_cast<int32_t>(offsetof(ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372, ___m_to_to_bool_5)); }
	inline bool get_m_to_to_bool_5() const { return ___m_to_to_bool_5; }
	inline bool* get_address_of_m_to_to_bool_5() { return &___m_to_to_bool_5; }
	inline void set_m_to_to_bool_5(bool value)
	{
		___m_to_to_bool_5 = value;
	}

	inline static int32_t get_offset_of_m_unique_randoms_6() { return static_cast<int32_t>(offsetof(ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372, ___m_unique_randoms_6)); }
	inline bool get_m_unique_randoms_6() const { return ___m_unique_randoms_6; }
	inline bool* get_address_of_m_unique_randoms_6() { return &___m_unique_randoms_6; }
	inline void set_m_unique_randoms_6(bool value)
	{
		___m_unique_randoms_6 = value;
	}

	inline static int32_t get_offset_of_m_animate_per_7() { return static_cast<int32_t>(offsetof(ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372, ___m_animate_per_7)); }
	inline int32_t get_m_animate_per_7() const { return ___m_animate_per_7; }
	inline int32_t* get_address_of_m_animate_per_7() { return &___m_animate_per_7; }
	inline void set_m_animate_per_7(int32_t value)
	{
		___m_animate_per_7 = value;
	}

	inline static int32_t get_offset_of_m_override_animate_per_option_8() { return static_cast<int32_t>(offsetof(ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372, ___m_override_animate_per_option_8)); }
	inline bool get_m_override_animate_per_option_8() const { return ___m_override_animate_per_option_8; }
	inline bool* get_address_of_m_override_animate_per_option_8() { return &___m_override_animate_per_option_8; }
	inline void set_m_override_animate_per_option_8(bool value)
	{
		___m_override_animate_per_option_8 = value;
	}

	inline static int32_t get_offset_of_m_custom_ease_curve_9() { return static_cast<int32_t>(offsetof(ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372, ___m_custom_ease_curve_9)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_custom_ease_curve_9() const { return ___m_custom_ease_curve_9; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_custom_ease_curve_9() { return &___m_custom_ease_curve_9; }
	inline void set_m_custom_ease_curve_9(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_custom_ease_curve_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_custom_ease_curve_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONVARIABLEPROGRESSION_TF36AFCFFC89DF7318F20B441D78CB48B2AA8A372_H
#ifndef AUDIOEFFECTSETUP_T825C0C272933908E8B86A8FD78EA73614B10BB8F_H
#define AUDIOEFFECTSETUP_T825C0C272933908E8B86A8FD78EA73614B10BB8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.AudioEffectSetup
struct  AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F  : public EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835
{
public:
	// UnityEngine.AudioClip TextFx.AudioEffectSetup::m_audio_clip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___m_audio_clip_7;
	// TextFx.ActionFloatProgression TextFx.AudioEffectSetup::m_offset_time
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * ___m_offset_time_8;
	// TextFx.ActionFloatProgression TextFx.AudioEffectSetup::m_volume
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * ___m_volume_9;
	// TextFx.ActionFloatProgression TextFx.AudioEffectSetup::m_pitch
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * ___m_pitch_10;

public:
	inline static int32_t get_offset_of_m_audio_clip_7() { return static_cast<int32_t>(offsetof(AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F, ___m_audio_clip_7)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_m_audio_clip_7() const { return ___m_audio_clip_7; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_m_audio_clip_7() { return &___m_audio_clip_7; }
	inline void set_m_audio_clip_7(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___m_audio_clip_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_audio_clip_7), value);
	}

	inline static int32_t get_offset_of_m_offset_time_8() { return static_cast<int32_t>(offsetof(AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F, ___m_offset_time_8)); }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * get_m_offset_time_8() const { return ___m_offset_time_8; }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 ** get_address_of_m_offset_time_8() { return &___m_offset_time_8; }
	inline void set_m_offset_time_8(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * value)
	{
		___m_offset_time_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_offset_time_8), value);
	}

	inline static int32_t get_offset_of_m_volume_9() { return static_cast<int32_t>(offsetof(AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F, ___m_volume_9)); }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * get_m_volume_9() const { return ___m_volume_9; }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 ** get_address_of_m_volume_9() { return &___m_volume_9; }
	inline void set_m_volume_9(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * value)
	{
		___m_volume_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_volume_9), value);
	}

	inline static int32_t get_offset_of_m_pitch_10() { return static_cast<int32_t>(offsetof(AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F, ___m_pitch_10)); }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * get_m_pitch_10() const { return ___m_pitch_10; }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 ** get_address_of_m_pitch_10() { return &___m_pitch_10; }
	inline void set_m_pitch_10(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * value)
	{
		___m_pitch_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_pitch_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEFFECTSETUP_T825C0C272933908E8B86A8FD78EA73614B10BB8F_H
#ifndef PARTICLEEFFECTSETUP_T95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F_H
#define PARTICLEEFFECTSETUP_T95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ParticleEffectSetup
struct  ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F  : public EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835
{
public:
	// TextFx.PARTICLE_EFFECT_TYPE TextFx.ParticleEffectSetup::m_effect_type
	int32_t ___m_effect_type_7;
	// UnityEngine.ParticleSystem TextFx.ParticleEffectSetup::m_shuriken_particle_effect
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_shuriken_particle_effect_8;
	// TextFx.ActionFloatProgression TextFx.ParticleEffectSetup::m_duration
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * ___m_duration_9;
	// System.Boolean TextFx.ParticleEffectSetup::m_follow_mesh
	bool ___m_follow_mesh_10;
	// TextFx.ActionVector3Progression TextFx.ParticleEffectSetup::m_position_offset
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * ___m_position_offset_11;
	// TextFx.ActionVector3Progression TextFx.ParticleEffectSetup::m_rotation_offset
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * ___m_rotation_offset_12;
	// System.Boolean TextFx.ParticleEffectSetup::m_rotate_relative_to_letter
	bool ___m_rotate_relative_to_letter_13;

public:
	inline static int32_t get_offset_of_m_effect_type_7() { return static_cast<int32_t>(offsetof(ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F, ___m_effect_type_7)); }
	inline int32_t get_m_effect_type_7() const { return ___m_effect_type_7; }
	inline int32_t* get_address_of_m_effect_type_7() { return &___m_effect_type_7; }
	inline void set_m_effect_type_7(int32_t value)
	{
		___m_effect_type_7 = value;
	}

	inline static int32_t get_offset_of_m_shuriken_particle_effect_8() { return static_cast<int32_t>(offsetof(ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F, ___m_shuriken_particle_effect_8)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_m_shuriken_particle_effect_8() const { return ___m_shuriken_particle_effect_8; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_m_shuriken_particle_effect_8() { return &___m_shuriken_particle_effect_8; }
	inline void set_m_shuriken_particle_effect_8(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___m_shuriken_particle_effect_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_shuriken_particle_effect_8), value);
	}

	inline static int32_t get_offset_of_m_duration_9() { return static_cast<int32_t>(offsetof(ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F, ___m_duration_9)); }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * get_m_duration_9() const { return ___m_duration_9; }
	inline ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 ** get_address_of_m_duration_9() { return &___m_duration_9; }
	inline void set_m_duration_9(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3 * value)
	{
		___m_duration_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_duration_9), value);
	}

	inline static int32_t get_offset_of_m_follow_mesh_10() { return static_cast<int32_t>(offsetof(ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F, ___m_follow_mesh_10)); }
	inline bool get_m_follow_mesh_10() const { return ___m_follow_mesh_10; }
	inline bool* get_address_of_m_follow_mesh_10() { return &___m_follow_mesh_10; }
	inline void set_m_follow_mesh_10(bool value)
	{
		___m_follow_mesh_10 = value;
	}

	inline static int32_t get_offset_of_m_position_offset_11() { return static_cast<int32_t>(offsetof(ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F, ___m_position_offset_11)); }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * get_m_position_offset_11() const { return ___m_position_offset_11; }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C ** get_address_of_m_position_offset_11() { return &___m_position_offset_11; }
	inline void set_m_position_offset_11(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * value)
	{
		___m_position_offset_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_position_offset_11), value);
	}

	inline static int32_t get_offset_of_m_rotation_offset_12() { return static_cast<int32_t>(offsetof(ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F, ___m_rotation_offset_12)); }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * get_m_rotation_offset_12() const { return ___m_rotation_offset_12; }
	inline ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C ** get_address_of_m_rotation_offset_12() { return &___m_rotation_offset_12; }
	inline void set_m_rotation_offset_12(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C * value)
	{
		___m_rotation_offset_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_rotation_offset_12), value);
	}

	inline static int32_t get_offset_of_m_rotate_relative_to_letter_13() { return static_cast<int32_t>(offsetof(ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F, ___m_rotate_relative_to_letter_13)); }
	inline bool get_m_rotate_relative_to_letter_13() const { return ___m_rotate_relative_to_letter_13; }
	inline bool* get_address_of_m_rotate_relative_to_letter_13() { return &___m_rotate_relative_to_letter_13; }
	inline void set_m_rotate_relative_to_letter_13(bool value)
	{
		___m_rotate_relative_to_letter_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEEFFECTSETUP_T95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F_H
#ifndef TEXTFXBEZIERCURVE_T64713478F22A36E9B349DE2116BECD570F8F5AB7_H
#define TEXTFXBEZIERCURVE_T64713478F22A36E9B349DE2116BECD570F8F5AB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxBezierCurve
struct  TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7 
{
public:
	// TextFx.BezierCurvePointData TextFx.TextFxBezierCurve::m_pointData
	BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298  ___m_pointData_3;
	// System.Single TextFx.TextFxBezierCurve::m_baselineOffset
	float ___m_baselineOffset_4;
	// UnityEngine.Vector3[] TextFx.TextFxBezierCurve::m_temp_anchor_points
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_temp_anchor_points_5;
	// UnityEngine.Vector3 TextFx.TextFxBezierCurve::rot
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rot_6;

public:
	inline static int32_t get_offset_of_m_pointData_3() { return static_cast<int32_t>(offsetof(TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7, ___m_pointData_3)); }
	inline BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298  get_m_pointData_3() const { return ___m_pointData_3; }
	inline BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298 * get_address_of_m_pointData_3() { return &___m_pointData_3; }
	inline void set_m_pointData_3(BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298  value)
	{
		___m_pointData_3 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_4() { return static_cast<int32_t>(offsetof(TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7, ___m_baselineOffset_4)); }
	inline float get_m_baselineOffset_4() const { return ___m_baselineOffset_4; }
	inline float* get_address_of_m_baselineOffset_4() { return &___m_baselineOffset_4; }
	inline void set_m_baselineOffset_4(float value)
	{
		___m_baselineOffset_4 = value;
	}

	inline static int32_t get_offset_of_m_temp_anchor_points_5() { return static_cast<int32_t>(offsetof(TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7, ___m_temp_anchor_points_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_temp_anchor_points_5() const { return ___m_temp_anchor_points_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_temp_anchor_points_5() { return &___m_temp_anchor_points_5; }
	inline void set_m_temp_anchor_points_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_temp_anchor_points_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_temp_anchor_points_5), value);
	}

	inline static int32_t get_offset_of_rot_6() { return static_cast<int32_t>(offsetof(TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7, ___rot_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rot_6() const { return ___rot_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rot_6() { return &___rot_6; }
	inline void set_rot_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXBEZIERCURVE_T64713478F22A36E9B349DE2116BECD570F8F5AB7_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef ACTIONCOLORPROGRESSION_T806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA_H
#define ACTIONCOLORPROGRESSION_T806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ActionColorProgression
struct  ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA  : public ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372
{
public:
	// VertexColour[] TextFx.ActionColorProgression::m_values
	VertexColourU5BU5D_t1555A77080D0FBDA0DAC227CE369D5BDB2821C0C* ___m_values_10;
	// VertexColour TextFx.ActionColorProgression::m_from
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * ___m_from_11;
	// VertexColour TextFx.ActionColorProgression::m_to
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * ___m_to_12;
	// VertexColour TextFx.ActionColorProgression::m_to_to
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * ___m_to_to_13;
	// System.Boolean TextFx.ActionColorProgression::m_override_alpha
	bool ___m_override_alpha_14;
	// System.Boolean TextFx.ActionColorProgression::m_use_colour_gradients
	bool ___m_use_colour_gradients_15;
	// TextFx.PROGRESSION_VALUE_STATE TextFx.ActionColorProgression::m_value_state
	int32_t ___m_value_state_16;
	// TextFx.ActionVariableProgressionReferenceData TextFx.ActionColorProgression::m_offset_progression
	ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7  ___m_offset_progression_17;
	// TextFx.ActionColorProgression TextFx.ActionColorProgression::cachedColourProgression
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * ___cachedColourProgression_18;

public:
	inline static int32_t get_offset_of_m_values_10() { return static_cast<int32_t>(offsetof(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA, ___m_values_10)); }
	inline VertexColourU5BU5D_t1555A77080D0FBDA0DAC227CE369D5BDB2821C0C* get_m_values_10() const { return ___m_values_10; }
	inline VertexColourU5BU5D_t1555A77080D0FBDA0DAC227CE369D5BDB2821C0C** get_address_of_m_values_10() { return &___m_values_10; }
	inline void set_m_values_10(VertexColourU5BU5D_t1555A77080D0FBDA0DAC227CE369D5BDB2821C0C* value)
	{
		___m_values_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_values_10), value);
	}

	inline static int32_t get_offset_of_m_from_11() { return static_cast<int32_t>(offsetof(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA, ___m_from_11)); }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * get_m_from_11() const { return ___m_from_11; }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 ** get_address_of_m_from_11() { return &___m_from_11; }
	inline void set_m_from_11(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * value)
	{
		___m_from_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_from_11), value);
	}

	inline static int32_t get_offset_of_m_to_12() { return static_cast<int32_t>(offsetof(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA, ___m_to_12)); }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * get_m_to_12() const { return ___m_to_12; }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 ** get_address_of_m_to_12() { return &___m_to_12; }
	inline void set_m_to_12(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * value)
	{
		___m_to_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_to_12), value);
	}

	inline static int32_t get_offset_of_m_to_to_13() { return static_cast<int32_t>(offsetof(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA, ___m_to_to_13)); }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * get_m_to_to_13() const { return ___m_to_to_13; }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 ** get_address_of_m_to_to_13() { return &___m_to_to_13; }
	inline void set_m_to_to_13(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * value)
	{
		___m_to_to_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_to_to_13), value);
	}

	inline static int32_t get_offset_of_m_override_alpha_14() { return static_cast<int32_t>(offsetof(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA, ___m_override_alpha_14)); }
	inline bool get_m_override_alpha_14() const { return ___m_override_alpha_14; }
	inline bool* get_address_of_m_override_alpha_14() { return &___m_override_alpha_14; }
	inline void set_m_override_alpha_14(bool value)
	{
		___m_override_alpha_14 = value;
	}

	inline static int32_t get_offset_of_m_use_colour_gradients_15() { return static_cast<int32_t>(offsetof(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA, ___m_use_colour_gradients_15)); }
	inline bool get_m_use_colour_gradients_15() const { return ___m_use_colour_gradients_15; }
	inline bool* get_address_of_m_use_colour_gradients_15() { return &___m_use_colour_gradients_15; }
	inline void set_m_use_colour_gradients_15(bool value)
	{
		___m_use_colour_gradients_15 = value;
	}

	inline static int32_t get_offset_of_m_value_state_16() { return static_cast<int32_t>(offsetof(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA, ___m_value_state_16)); }
	inline int32_t get_m_value_state_16() const { return ___m_value_state_16; }
	inline int32_t* get_address_of_m_value_state_16() { return &___m_value_state_16; }
	inline void set_m_value_state_16(int32_t value)
	{
		___m_value_state_16 = value;
	}

	inline static int32_t get_offset_of_m_offset_progression_17() { return static_cast<int32_t>(offsetof(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA, ___m_offset_progression_17)); }
	inline ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7  get_m_offset_progression_17() const { return ___m_offset_progression_17; }
	inline ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7 * get_address_of_m_offset_progression_17() { return &___m_offset_progression_17; }
	inline void set_m_offset_progression_17(ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7  value)
	{
		___m_offset_progression_17 = value;
	}

	inline static int32_t get_offset_of_cachedColourProgression_18() { return static_cast<int32_t>(offsetof(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA, ___cachedColourProgression_18)); }
	inline ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * get_cachedColourProgression_18() const { return ___cachedColourProgression_18; }
	inline ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA ** get_address_of_cachedColourProgression_18() { return &___cachedColourProgression_18; }
	inline void set_cachedColourProgression_18(ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA * value)
	{
		___cachedColourProgression_18 = value;
		Il2CppCodeGenWriteBarrier((&___cachedColourProgression_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONCOLORPROGRESSION_T806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA_H
#ifndef ACTIONFLOATPROGRESSION_T8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3_H
#define ACTIONFLOATPROGRESSION_T8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ActionFloatProgression
struct  ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3  : public ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372
{
public:
	// System.Single[] TextFx.ActionFloatProgression::m_values
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_values_10;
	// System.Single TextFx.ActionFloatProgression::m_from
	float ___m_from_11;
	// System.Single TextFx.ActionFloatProgression::m_to
	float ___m_to_12;
	// System.Single TextFx.ActionFloatProgression::m_to_to
	float ___m_to_to_13;

public:
	inline static int32_t get_offset_of_m_values_10() { return static_cast<int32_t>(offsetof(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3, ___m_values_10)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_values_10() const { return ___m_values_10; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_values_10() { return &___m_values_10; }
	inline void set_m_values_10(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_values_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_values_10), value);
	}

	inline static int32_t get_offset_of_m_from_11() { return static_cast<int32_t>(offsetof(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3, ___m_from_11)); }
	inline float get_m_from_11() const { return ___m_from_11; }
	inline float* get_address_of_m_from_11() { return &___m_from_11; }
	inline void set_m_from_11(float value)
	{
		___m_from_11 = value;
	}

	inline static int32_t get_offset_of_m_to_12() { return static_cast<int32_t>(offsetof(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3, ___m_to_12)); }
	inline float get_m_to_12() const { return ___m_to_12; }
	inline float* get_address_of_m_to_12() { return &___m_to_12; }
	inline void set_m_to_12(float value)
	{
		___m_to_12 = value;
	}

	inline static int32_t get_offset_of_m_to_to_13() { return static_cast<int32_t>(offsetof(ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3, ___m_to_to_13)); }
	inline float get_m_to_to_13() const { return ___m_to_to_13; }
	inline float* get_address_of_m_to_to_13() { return &___m_to_to_13; }
	inline void set_m_to_to_13(float value)
	{
		___m_to_to_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONFLOATPROGRESSION_T8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3_H
#ifndef ACTIONVECTOR3PROGRESSION_TFACD68ACDC1AEF873186F79689940091436CE99C_H
#define ACTIONVECTOR3PROGRESSION_TFACD68ACDC1AEF873186F79689940091436CE99C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ActionVector3Progression
struct  ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C  : public ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372
{
public:
	// UnityEngine.Vector3[] TextFx.ActionVector3Progression::m_values
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_values_10;
	// UnityEngine.Vector3 TextFx.ActionVector3Progression::m_from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_from_11;
	// UnityEngine.Vector3 TextFx.ActionVector3Progression::m_to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_to_12;
	// UnityEngine.Vector3 TextFx.ActionVector3Progression::m_to_to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_to_to_13;
	// System.Boolean TextFx.ActionVector3Progression::m_ease_curve_per_axis
	bool ___m_ease_curve_per_axis_14;
	// UnityEngine.AnimationCurve TextFx.ActionVector3Progression::m_custom_ease_curve_y
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_custom_ease_curve_y_15;
	// UnityEngine.AnimationCurve TextFx.ActionVector3Progression::m_custom_ease_curve_z
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_custom_ease_curve_z_16;
	// TextFx.PROGRESSION_VALUE_STATE TextFx.ActionVector3Progression::m_value_state
	int32_t ___m_value_state_17;
	// TextFx.ActionVariableProgressionReferenceData TextFx.ActionVector3Progression::m_offset_progression
	ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7  ___m_offset_progression_18;

public:
	inline static int32_t get_offset_of_m_values_10() { return static_cast<int32_t>(offsetof(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C, ___m_values_10)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_values_10() const { return ___m_values_10; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_values_10() { return &___m_values_10; }
	inline void set_m_values_10(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_values_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_values_10), value);
	}

	inline static int32_t get_offset_of_m_from_11() { return static_cast<int32_t>(offsetof(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C, ___m_from_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_from_11() const { return ___m_from_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_from_11() { return &___m_from_11; }
	inline void set_m_from_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_from_11 = value;
	}

	inline static int32_t get_offset_of_m_to_12() { return static_cast<int32_t>(offsetof(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C, ___m_to_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_to_12() const { return ___m_to_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_to_12() { return &___m_to_12; }
	inline void set_m_to_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_to_12 = value;
	}

	inline static int32_t get_offset_of_m_to_to_13() { return static_cast<int32_t>(offsetof(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C, ___m_to_to_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_to_to_13() const { return ___m_to_to_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_to_to_13() { return &___m_to_to_13; }
	inline void set_m_to_to_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_to_to_13 = value;
	}

	inline static int32_t get_offset_of_m_ease_curve_per_axis_14() { return static_cast<int32_t>(offsetof(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C, ___m_ease_curve_per_axis_14)); }
	inline bool get_m_ease_curve_per_axis_14() const { return ___m_ease_curve_per_axis_14; }
	inline bool* get_address_of_m_ease_curve_per_axis_14() { return &___m_ease_curve_per_axis_14; }
	inline void set_m_ease_curve_per_axis_14(bool value)
	{
		___m_ease_curve_per_axis_14 = value;
	}

	inline static int32_t get_offset_of_m_custom_ease_curve_y_15() { return static_cast<int32_t>(offsetof(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C, ___m_custom_ease_curve_y_15)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_custom_ease_curve_y_15() const { return ___m_custom_ease_curve_y_15; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_custom_ease_curve_y_15() { return &___m_custom_ease_curve_y_15; }
	inline void set_m_custom_ease_curve_y_15(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_custom_ease_curve_y_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_custom_ease_curve_y_15), value);
	}

	inline static int32_t get_offset_of_m_custom_ease_curve_z_16() { return static_cast<int32_t>(offsetof(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C, ___m_custom_ease_curve_z_16)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_custom_ease_curve_z_16() const { return ___m_custom_ease_curve_z_16; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_custom_ease_curve_z_16() { return &___m_custom_ease_curve_z_16; }
	inline void set_m_custom_ease_curve_z_16(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_custom_ease_curve_z_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_custom_ease_curve_z_16), value);
	}

	inline static int32_t get_offset_of_m_value_state_17() { return static_cast<int32_t>(offsetof(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C, ___m_value_state_17)); }
	inline int32_t get_m_value_state_17() const { return ___m_value_state_17; }
	inline int32_t* get_address_of_m_value_state_17() { return &___m_value_state_17; }
	inline void set_m_value_state_17(int32_t value)
	{
		___m_value_state_17 = value;
	}

	inline static int32_t get_offset_of_m_offset_progression_18() { return static_cast<int32_t>(offsetof(ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C, ___m_offset_progression_18)); }
	inline ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7  get_m_offset_progression_18() const { return ___m_offset_progression_18; }
	inline ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7 * get_address_of_m_offset_progression_18() { return &___m_offset_progression_18; }
	inline void set_m_offset_progression_18(ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7  value)
	{
		___m_offset_progression_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONVECTOR3PROGRESSION_TFACD68ACDC1AEF873186F79689940091436CE99C_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef SCENEMANAGER_T3F01A7DF4D0C1E7E9688344DD2A09C7B79116172_H
#define SCENEMANAGER_T3F01A7DF4D0C1E7E9688344DD2A09C7B79116172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DLLCore.SceneManager
struct  SceneManager_t3F01A7DF4D0C1E7E9688344DD2A09C7B79116172  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button DLLCore.SceneManager::toAR
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___toAR_4;
	// UnityEngine.UI.Button DLLCore.SceneManager::toHomeDesign
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___toHomeDesign_5;

public:
	inline static int32_t get_offset_of_toAR_4() { return static_cast<int32_t>(offsetof(SceneManager_t3F01A7DF4D0C1E7E9688344DD2A09C7B79116172, ___toAR_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_toAR_4() const { return ___toAR_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_toAR_4() { return &___toAR_4; }
	inline void set_toAR_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___toAR_4 = value;
		Il2CppCodeGenWriteBarrier((&___toAR_4), value);
	}

	inline static int32_t get_offset_of_toHomeDesign_5() { return static_cast<int32_t>(offsetof(SceneManager_t3F01A7DF4D0C1E7E9688344DD2A09C7B79116172, ___toHomeDesign_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_toHomeDesign_5() const { return ___toHomeDesign_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_toHomeDesign_5() { return &___toHomeDesign_5; }
	inline void set_toHomeDesign_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___toHomeDesign_5 = value;
		Il2CppCodeGenWriteBarrier((&___toHomeDesign_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEMANAGER_T3F01A7DF4D0C1E7E9688344DD2A09C7B79116172_H
#ifndef HSVDRAGGER_T4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5_H
#define HSVDRAGGER_T4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVPickerDemo.HSVDragger
struct  HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform HSVPickerDemo.HSVDragger::parentPanel
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___parentPanel_4;
	// UnityEngine.RectTransform HSVPickerDemo.HSVDragger::rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rectTransform_5;
	// UnityEngine.UI.ScrollRect HSVPickerDemo.HSVDragger::scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___scrollRect_6;
	// HSVPickerDemo.HSVPicker HSVPickerDemo.HSVDragger::picker
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * ___picker_7;

public:
	inline static int32_t get_offset_of_parentPanel_4() { return static_cast<int32_t>(offsetof(HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5, ___parentPanel_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_parentPanel_4() const { return ___parentPanel_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_parentPanel_4() { return &___parentPanel_4; }
	inline void set_parentPanel_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___parentPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___parentPanel_4), value);
	}

	inline static int32_t get_offset_of_rectTransform_5() { return static_cast<int32_t>(offsetof(HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5, ___rectTransform_5)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rectTransform_5() const { return ___rectTransform_5; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rectTransform_5() { return &___rectTransform_5; }
	inline void set_rectTransform_5(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_5), value);
	}

	inline static int32_t get_offset_of_scrollRect_6() { return static_cast<int32_t>(offsetof(HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5, ___scrollRect_6)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_scrollRect_6() const { return ___scrollRect_6; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_scrollRect_6() { return &___scrollRect_6; }
	inline void set_scrollRect_6(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___scrollRect_6 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_6), value);
	}

	inline static int32_t get_offset_of_picker_7() { return static_cast<int32_t>(offsetof(HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5, ___picker_7)); }
	inline HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * get_picker_7() const { return ___picker_7; }
	inline HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 ** get_address_of_picker_7() { return &___picker_7; }
	inline void set_picker_7(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * value)
	{
		___picker_7 = value;
		Il2CppCodeGenWriteBarrier((&___picker_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVDRAGGER_T4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5_H
#ifndef HSVPICKER_TABA452C1C512F3C5A07C4581B07C9823224CA565_H
#define HSVPICKER_TABA452C1C512F3C5A07C4581B07C9823224CA565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVPickerDemo.HSVPicker
struct  HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HSVPickerDemo.HexRGB HSVPickerDemo.HSVPicker::hexrgb
	HexRGB_t3457F98D908B302FF235748F6729A0240D21680B * ___hexrgb_4;
	// UnityEngine.Color32 HSVPickerDemo.HSVPicker::currentColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___currentColor_5;
	// System.Byte HSVPickerDemo.HSVPicker::r
	uint8_t ___r_6;
	// System.Byte HSVPickerDemo.HSVPicker::g
	uint8_t ___g_7;
	// System.Byte HSVPickerDemo.HSVPicker::b
	uint8_t ___b_8;
	// UnityEngine.UI.Image HSVPickerDemo.HSVPicker::colorImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___colorImage_9;
	// UnityEngine.UI.RawImage HSVPickerDemo.HSVPicker::hsvSlider
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___hsvSlider_10;
	// UnityEngine.UI.RawImage HSVPickerDemo.HSVPicker::hsvImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___hsvImage_11;
	// HsvSliderPicker HSVPickerDemo.HSVPicker::sliderPicker
	HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB * ___sliderPicker_12;
	// UnityEngine.UI.BoxSlider HSVPickerDemo.HSVPicker::boxSlider
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A * ___boxSlider_13;
	// UnityEngine.UI.Slider HSVPickerDemo.HSVPicker::sliderR
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___sliderR_14;
	// UnityEngine.UI.Slider HSVPickerDemo.HSVPicker::sliderG
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___sliderG_15;
	// UnityEngine.UI.Slider HSVPickerDemo.HSVPicker::sliderB
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___sliderB_16;
	// UnityEngine.UI.Text HSVPickerDemo.HSVPicker::sliderRText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___sliderRText_17;
	// UnityEngine.UI.Text HSVPickerDemo.HSVPicker::sliderGText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___sliderGText_18;
	// UnityEngine.UI.Text HSVPickerDemo.HSVPicker::sliderBText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___sliderBText_19;
	// System.Single HSVPickerDemo.HSVPicker::pointerPos
	float ___pointerPos_20;
	// System.Int32 HSVPickerDemo.HSVPicker::minHue
	int32_t ___minHue_21;
	// System.Int32 HSVPickerDemo.HSVPicker::maxHue
	int32_t ___maxHue_22;
	// System.Single HSVPickerDemo.HSVPicker::minSat
	float ___minSat_23;
	// System.Single HSVPickerDemo.HSVPicker::maxSat
	float ___maxSat_24;
	// System.Single HSVPickerDemo.HSVPicker::minV
	float ___minV_25;
	// System.Single HSVPickerDemo.HSVPicker::maxV
	float ___maxV_26;
	// System.Single HSVPickerDemo.HSVPicker::cursorX
	float ___cursorX_27;
	// System.Single HSVPickerDemo.HSVPicker::cursorY
	float ___cursorY_28;
	// HSVPickerDemo.HSVSliderEvent HSVPickerDemo.HSVPicker::onValueChanged
	HSVSliderEvent_tE0150BE444ACECCCD174D0CA99B45FF973AC8318 * ___onValueChanged_29;
	// System.Boolean HSVPickerDemo.HSVPicker::isChanging
	bool ___isChanging_30;

public:
	inline static int32_t get_offset_of_hexrgb_4() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___hexrgb_4)); }
	inline HexRGB_t3457F98D908B302FF235748F6729A0240D21680B * get_hexrgb_4() const { return ___hexrgb_4; }
	inline HexRGB_t3457F98D908B302FF235748F6729A0240D21680B ** get_address_of_hexrgb_4() { return &___hexrgb_4; }
	inline void set_hexrgb_4(HexRGB_t3457F98D908B302FF235748F6729A0240D21680B * value)
	{
		___hexrgb_4 = value;
		Il2CppCodeGenWriteBarrier((&___hexrgb_4), value);
	}

	inline static int32_t get_offset_of_currentColor_5() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___currentColor_5)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_currentColor_5() const { return ___currentColor_5; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_currentColor_5() { return &___currentColor_5; }
	inline void set_currentColor_5(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___currentColor_5 = value;
	}

	inline static int32_t get_offset_of_r_6() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___r_6)); }
	inline uint8_t get_r_6() const { return ___r_6; }
	inline uint8_t* get_address_of_r_6() { return &___r_6; }
	inline void set_r_6(uint8_t value)
	{
		___r_6 = value;
	}

	inline static int32_t get_offset_of_g_7() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___g_7)); }
	inline uint8_t get_g_7() const { return ___g_7; }
	inline uint8_t* get_address_of_g_7() { return &___g_7; }
	inline void set_g_7(uint8_t value)
	{
		___g_7 = value;
	}

	inline static int32_t get_offset_of_b_8() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___b_8)); }
	inline uint8_t get_b_8() const { return ___b_8; }
	inline uint8_t* get_address_of_b_8() { return &___b_8; }
	inline void set_b_8(uint8_t value)
	{
		___b_8 = value;
	}

	inline static int32_t get_offset_of_colorImage_9() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___colorImage_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_colorImage_9() const { return ___colorImage_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_colorImage_9() { return &___colorImage_9; }
	inline void set_colorImage_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___colorImage_9 = value;
		Il2CppCodeGenWriteBarrier((&___colorImage_9), value);
	}

	inline static int32_t get_offset_of_hsvSlider_10() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___hsvSlider_10)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_hsvSlider_10() const { return ___hsvSlider_10; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_hsvSlider_10() { return &___hsvSlider_10; }
	inline void set_hsvSlider_10(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___hsvSlider_10 = value;
		Il2CppCodeGenWriteBarrier((&___hsvSlider_10), value);
	}

	inline static int32_t get_offset_of_hsvImage_11() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___hsvImage_11)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_hsvImage_11() const { return ___hsvImage_11; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_hsvImage_11() { return &___hsvImage_11; }
	inline void set_hsvImage_11(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___hsvImage_11 = value;
		Il2CppCodeGenWriteBarrier((&___hsvImage_11), value);
	}

	inline static int32_t get_offset_of_sliderPicker_12() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___sliderPicker_12)); }
	inline HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB * get_sliderPicker_12() const { return ___sliderPicker_12; }
	inline HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB ** get_address_of_sliderPicker_12() { return &___sliderPicker_12; }
	inline void set_sliderPicker_12(HsvSliderPicker_tC67F8542E7009A8F93936EC8468CE1934AE24DEB * value)
	{
		___sliderPicker_12 = value;
		Il2CppCodeGenWriteBarrier((&___sliderPicker_12), value);
	}

	inline static int32_t get_offset_of_boxSlider_13() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___boxSlider_13)); }
	inline BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A * get_boxSlider_13() const { return ___boxSlider_13; }
	inline BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A ** get_address_of_boxSlider_13() { return &___boxSlider_13; }
	inline void set_boxSlider_13(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A * value)
	{
		___boxSlider_13 = value;
		Il2CppCodeGenWriteBarrier((&___boxSlider_13), value);
	}

	inline static int32_t get_offset_of_sliderR_14() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___sliderR_14)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_sliderR_14() const { return ___sliderR_14; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_sliderR_14() { return &___sliderR_14; }
	inline void set_sliderR_14(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___sliderR_14 = value;
		Il2CppCodeGenWriteBarrier((&___sliderR_14), value);
	}

	inline static int32_t get_offset_of_sliderG_15() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___sliderG_15)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_sliderG_15() const { return ___sliderG_15; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_sliderG_15() { return &___sliderG_15; }
	inline void set_sliderG_15(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___sliderG_15 = value;
		Il2CppCodeGenWriteBarrier((&___sliderG_15), value);
	}

	inline static int32_t get_offset_of_sliderB_16() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___sliderB_16)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_sliderB_16() const { return ___sliderB_16; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_sliderB_16() { return &___sliderB_16; }
	inline void set_sliderB_16(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___sliderB_16 = value;
		Il2CppCodeGenWriteBarrier((&___sliderB_16), value);
	}

	inline static int32_t get_offset_of_sliderRText_17() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___sliderRText_17)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_sliderRText_17() const { return ___sliderRText_17; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_sliderRText_17() { return &___sliderRText_17; }
	inline void set_sliderRText_17(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___sliderRText_17 = value;
		Il2CppCodeGenWriteBarrier((&___sliderRText_17), value);
	}

	inline static int32_t get_offset_of_sliderGText_18() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___sliderGText_18)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_sliderGText_18() const { return ___sliderGText_18; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_sliderGText_18() { return &___sliderGText_18; }
	inline void set_sliderGText_18(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___sliderGText_18 = value;
		Il2CppCodeGenWriteBarrier((&___sliderGText_18), value);
	}

	inline static int32_t get_offset_of_sliderBText_19() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___sliderBText_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_sliderBText_19() const { return ___sliderBText_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_sliderBText_19() { return &___sliderBText_19; }
	inline void set_sliderBText_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___sliderBText_19 = value;
		Il2CppCodeGenWriteBarrier((&___sliderBText_19), value);
	}

	inline static int32_t get_offset_of_pointerPos_20() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___pointerPos_20)); }
	inline float get_pointerPos_20() const { return ___pointerPos_20; }
	inline float* get_address_of_pointerPos_20() { return &___pointerPos_20; }
	inline void set_pointerPos_20(float value)
	{
		___pointerPos_20 = value;
	}

	inline static int32_t get_offset_of_minHue_21() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___minHue_21)); }
	inline int32_t get_minHue_21() const { return ___minHue_21; }
	inline int32_t* get_address_of_minHue_21() { return &___minHue_21; }
	inline void set_minHue_21(int32_t value)
	{
		___minHue_21 = value;
	}

	inline static int32_t get_offset_of_maxHue_22() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___maxHue_22)); }
	inline int32_t get_maxHue_22() const { return ___maxHue_22; }
	inline int32_t* get_address_of_maxHue_22() { return &___maxHue_22; }
	inline void set_maxHue_22(int32_t value)
	{
		___maxHue_22 = value;
	}

	inline static int32_t get_offset_of_minSat_23() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___minSat_23)); }
	inline float get_minSat_23() const { return ___minSat_23; }
	inline float* get_address_of_minSat_23() { return &___minSat_23; }
	inline void set_minSat_23(float value)
	{
		___minSat_23 = value;
	}

	inline static int32_t get_offset_of_maxSat_24() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___maxSat_24)); }
	inline float get_maxSat_24() const { return ___maxSat_24; }
	inline float* get_address_of_maxSat_24() { return &___maxSat_24; }
	inline void set_maxSat_24(float value)
	{
		___maxSat_24 = value;
	}

	inline static int32_t get_offset_of_minV_25() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___minV_25)); }
	inline float get_minV_25() const { return ___minV_25; }
	inline float* get_address_of_minV_25() { return &___minV_25; }
	inline void set_minV_25(float value)
	{
		___minV_25 = value;
	}

	inline static int32_t get_offset_of_maxV_26() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___maxV_26)); }
	inline float get_maxV_26() const { return ___maxV_26; }
	inline float* get_address_of_maxV_26() { return &___maxV_26; }
	inline void set_maxV_26(float value)
	{
		___maxV_26 = value;
	}

	inline static int32_t get_offset_of_cursorX_27() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___cursorX_27)); }
	inline float get_cursorX_27() const { return ___cursorX_27; }
	inline float* get_address_of_cursorX_27() { return &___cursorX_27; }
	inline void set_cursorX_27(float value)
	{
		___cursorX_27 = value;
	}

	inline static int32_t get_offset_of_cursorY_28() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___cursorY_28)); }
	inline float get_cursorY_28() const { return ___cursorY_28; }
	inline float* get_address_of_cursorY_28() { return &___cursorY_28; }
	inline void set_cursorY_28(float value)
	{
		___cursorY_28 = value;
	}

	inline static int32_t get_offset_of_onValueChanged_29() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___onValueChanged_29)); }
	inline HSVSliderEvent_tE0150BE444ACECCCD174D0CA99B45FF973AC8318 * get_onValueChanged_29() const { return ___onValueChanged_29; }
	inline HSVSliderEvent_tE0150BE444ACECCCD174D0CA99B45FF973AC8318 ** get_address_of_onValueChanged_29() { return &___onValueChanged_29; }
	inline void set_onValueChanged_29(HSVSliderEvent_tE0150BE444ACECCCD174D0CA99B45FF973AC8318 * value)
	{
		___onValueChanged_29 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_29), value);
	}

	inline static int32_t get_offset_of_isChanging_30() { return static_cast<int32_t>(offsetof(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565, ___isChanging_30)); }
	inline bool get_isChanging_30() const { return ___isChanging_30; }
	inline bool* get_address_of_isChanging_30() { return &___isChanging_30; }
	inline void set_isChanging_30(bool value)
	{
		___isChanging_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVPICKER_TABA452C1C512F3C5A07C4581B07C9823224CA565_H
#ifndef HEXRGB_T3457F98D908B302FF235748F6729A0240D21680B_H
#define HEXRGB_T3457F98D908B302FF235748F6729A0240D21680B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVPickerDemo.HexRGB
struct  HexRGB_t3457F98D908B302FF235748F6729A0240D21680B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.InputField HSVPickerDemo.HexRGB::textColor
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___textColor_4;
	// HSVPickerDemo.HSVPicker HSVPickerDemo.HexRGB::hsvpicker
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * ___hsvpicker_5;

public:
	inline static int32_t get_offset_of_textColor_4() { return static_cast<int32_t>(offsetof(HexRGB_t3457F98D908B302FF235748F6729A0240D21680B, ___textColor_4)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_textColor_4() const { return ___textColor_4; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_textColor_4() { return &___textColor_4; }
	inline void set_textColor_4(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___textColor_4 = value;
		Il2CppCodeGenWriteBarrier((&___textColor_4), value);
	}

	inline static int32_t get_offset_of_hsvpicker_5() { return static_cast<int32_t>(offsetof(HexRGB_t3457F98D908B302FF235748F6729A0240D21680B, ___hsvpicker_5)); }
	inline HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * get_hsvpicker_5() const { return ___hsvpicker_5; }
	inline HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 ** get_address_of_hsvpicker_5() { return &___hsvpicker_5; }
	inline void set_hsvpicker_5(HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565 * value)
	{
		___hsvpicker_5 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXRGB_T3457F98D908B302FF235748F6729A0240D21680B_H
#ifndef HSVBOXSELECTOR_T75BDC2D0CC267992A0E7127BD48FFC42F699796C_H
#define HSVBOXSELECTOR_T75BDC2D0CC267992A0E7127BD48FFC42F699796C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVPickerDemo.HsvBoxSelector
struct  HsvBoxSelector_t75BDC2D0CC267992A0E7127BD48FFC42F699796C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HSVPickerDemo.HSVDragger HSVPickerDemo.HsvBoxSelector::dragger
	HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5 * ___dragger_4;
	// UnityEngine.RectTransform HSVPickerDemo.HsvBoxSelector::rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rectTransform_5;

public:
	inline static int32_t get_offset_of_dragger_4() { return static_cast<int32_t>(offsetof(HsvBoxSelector_t75BDC2D0CC267992A0E7127BD48FFC42F699796C, ___dragger_4)); }
	inline HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5 * get_dragger_4() const { return ___dragger_4; }
	inline HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5 ** get_address_of_dragger_4() { return &___dragger_4; }
	inline void set_dragger_4(HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5 * value)
	{
		___dragger_4 = value;
		Il2CppCodeGenWriteBarrier((&___dragger_4), value);
	}

	inline static int32_t get_offset_of_rectTransform_5() { return static_cast<int32_t>(offsetof(HsvBoxSelector_t75BDC2D0CC267992A0E7127BD48FFC42F699796C, ___rectTransform_5)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rectTransform_5() const { return ___rectTransform_5; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rectTransform_5() { return &___rectTransform_5; }
	inline void set_rectTransform_5(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVBOXSELECTOR_T75BDC2D0CC267992A0E7127BD48FFC42F699796C_H
#ifndef TILTWINDOW_T0BEC1307ECC8AE2090E015B81C172F87A1E6AE84_H
#define TILTWINDOW_T0BEC1307ECC8AE2090E015B81C172F87A1E6AE84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVPickerDemo.TiltWindow
struct  TiltWindow_t0BEC1307ECC8AE2090E015B81C172F87A1E6AE84  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector2 HSVPickerDemo.TiltWindow::range
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___range_4;
	// UnityEngine.Transform HSVPickerDemo.TiltWindow::mTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mTrans_5;
	// UnityEngine.Quaternion HSVPickerDemo.TiltWindow::mStart
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___mStart_6;
	// UnityEngine.Vector2 HSVPickerDemo.TiltWindow::mRot
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mRot_7;

public:
	inline static int32_t get_offset_of_range_4() { return static_cast<int32_t>(offsetof(TiltWindow_t0BEC1307ECC8AE2090E015B81C172F87A1E6AE84, ___range_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_range_4() const { return ___range_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_range_4() { return &___range_4; }
	inline void set_range_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___range_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(TiltWindow_t0BEC1307ECC8AE2090E015B81C172F87A1E6AE84, ___mTrans_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}

	inline static int32_t get_offset_of_mStart_6() { return static_cast<int32_t>(offsetof(TiltWindow_t0BEC1307ECC8AE2090E015B81C172F87A1E6AE84, ___mStart_6)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_mStart_6() const { return ___mStart_6; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_mStart_6() { return &___mStart_6; }
	inline void set_mStart_6(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___mStart_6 = value;
	}

	inline static int32_t get_offset_of_mRot_7() { return static_cast<int32_t>(offsetof(TiltWindow_t0BEC1307ECC8AE2090E015B81C172F87A1E6AE84, ___mRot_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mRot_7() const { return ___mRot_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mRot_7() { return &___mRot_7; }
	inline void set_mRot_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mRot_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTWINDOW_T0BEC1307ECC8AE2090E015B81C172F87A1E6AE84_H
#ifndef ACTIONPOSITIONVECTOR3PROGRESSION_T142669E514EAC93C1916ECEBB639C9CC481F4147_H
#define ACTIONPOSITIONVECTOR3PROGRESSION_T142669E514EAC93C1916ECEBB639C9CC481F4147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.ActionPositionVector3Progression
struct  ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147  : public ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C
{
public:
	// System.Boolean TextFx.ActionPositionVector3Progression::m_force_position_override
	bool ___m_force_position_override_19;

public:
	inline static int32_t get_offset_of_m_force_position_override_19() { return static_cast<int32_t>(offsetof(ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147, ___m_force_position_override_19)); }
	inline bool get_m_force_position_override_19() const { return ___m_force_position_override_19; }
	inline bool* get_address_of_m_force_position_override_19() { return &___m_force_position_override_19; }
	inline void set_m_force_position_override_19(bool value)
	{
		___m_force_position_override_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONPOSITIONVECTOR3PROGRESSION_T142669E514EAC93C1916ECEBB639C9CC481F4147_H
#ifndef TEXTFXNATIVE_T097594515F97FCCF174099736286557AE5B9FA9E_H
#define TEXTFXNATIVE_T097594515F97FCCF174099736286557AE5B9FA9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxNative
struct  TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String TextFx.TextFxNative::m_text
	String_t* ___m_text_6;
	// UnityEngine.Font TextFx.TextFxNative::m_font
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___m_font_7;
	// System.Int32 TextFx.TextFxNative::m_font_texture_width
	int32_t ___m_font_texture_width_8;
	// System.Int32 TextFx.TextFxNative::m_font_texture_height
	int32_t ___m_font_texture_height_9;
	// UnityEngine.TextAsset TextFx.TextFxNative::m_font_data_file
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___m_font_data_file_10;
	// UnityEngine.Material TextFx.TextFxNative::m_font_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_font_material_11;
	// UnityEngine.Vector2 TextFx.TextFxNative::m_px_offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_px_offset_12;
	// System.Single TextFx.TextFxNative::m_character_size
	float ___m_character_size_13;
	// System.Boolean TextFx.TextFxNative::m_use_colour_gradient
	bool ___m_use_colour_gradient_14;
	// UnityEngine.Color TextFx.TextFxNative::m_textColour
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_textColour_15;
	// VertexColour TextFx.TextFxNative::m_textColourGradient
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * ___m_textColourGradient_16;
	// TextFx.TextDisplayAxis TextFx.TextFxNative::m_display_axis
	int32_t ___m_display_axis_17;
	// UnityEngine.TextAnchor TextFx.TextFxNative::m_text_anchor
	int32_t ___m_text_anchor_18;
	// UnityEngine.TextAlignment TextFx.TextFxNative::m_text_alignment
	int32_t ___m_text_alignment_19;
	// System.Single TextFx.TextFxNative::m_line_height_factor
	float ___m_line_height_factor_20;
	// System.Single TextFx.TextFxNative::m_max_width
	float ___m_max_width_21;
	// System.Boolean TextFx.TextFxNative::m_override_font_baseline
	bool ___m_override_font_baseline_22;
	// System.Single TextFx.TextFxNative::m_font_baseline_override
	float ___m_font_baseline_override_23;
	// TextFx.TextFxAnimationManager TextFx.TextFxNative::m_animation_manager
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * ___m_animation_manager_24;
	// UnityEngine.GameObject TextFx.TextFxNative::m_gameobject_reference
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_gameobject_reference_25;
	// System.Boolean TextFx.TextFxNative::m_renderToCurve
	bool ___m_renderToCurve_26;
	// TextFx.TextFxBezierCurve TextFx.TextFxNative::m_bezierCurve
	TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  ___m_bezierCurve_27;
	// System.Action TextFx.TextFxNative::<OnMeshUpdateCall>k__BackingField
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3COnMeshUpdateCallU3Ek__BackingField_28;
	// UnityEngine.Vector3[] TextFx.TextFxNative::m_mesh_verts
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_mesh_verts_29;
	// UnityEngine.Vector2[] TextFx.TextFxNative::m_mesh_uvs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_mesh_uvs_30;
	// UnityEngine.Color[] TextFx.TextFxNative::m_mesh_cols
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_mesh_cols_31;
	// UnityEngine.Vector3[] TextFx.TextFxNative::m_mesh_normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_mesh_normals_32;
	// System.Int32[] TextFx.TextFxNative::m_mesh_triangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_mesh_triangles_33;
	// System.Single TextFx.TextFxNative::m_font_baseline
	float ___m_font_baseline_34;
	// TextFx.CustomFontCharacterData TextFx.TextFxNative::m_custom_font_data
	CustomFontCharacterData_t1391FAABB7293C043AAF8845B64651AF4ED69E99 * ___m_custom_font_data_35;
	// System.String TextFx.TextFxNative::m_current_font_data_file_name
	String_t* ___m_current_font_data_file_name_36;
	// System.String TextFx.TextFxNative::m_current_font_name
	String_t* ___m_current_font_name_37;
	// System.Action`1<UnityEngine.Font> TextFx.TextFxNative::m_fontRebuildCallback
	Action_1_t795662E553415ECF2DD0F8EEB9BA170C3670F37C * ___m_fontRebuildCallback_38;
	// UnityEngine.CombineInstance[] TextFx.TextFxNative::m_mesh_combine_instance
	CombineInstanceU5BU5D_t3865A4038A85A764FCE1EE0C134B77C42F4610DA* ___m_mesh_combine_instance_39;
	// UnityEngine.Transform TextFx.TextFxNative::m_transform_reference
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_transform_reference_40;
	// UnityEngine.Renderer TextFx.TextFxNative::m_renderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___m_renderer_41;
	// UnityEngine.MeshFilter TextFx.TextFxNative::m_mesh_filter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___m_mesh_filter_42;
	// UnityEngine.Mesh TextFx.TextFxNative::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_43;
	// System.Single TextFx.TextFxNative::m_total_text_width
	float ___m_total_text_width_44;
	// System.Single TextFx.TextFxNative::m_total_text_height
	float ___m_total_text_height_45;
	// System.Single TextFx.TextFxNative::m_line_height
	float ___m_line_height_46;

public:
	inline static int32_t get_offset_of_m_text_6() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_text_6)); }
	inline String_t* get_m_text_6() const { return ___m_text_6; }
	inline String_t** get_address_of_m_text_6() { return &___m_text_6; }
	inline void set_m_text_6(String_t* value)
	{
		___m_text_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_6), value);
	}

	inline static int32_t get_offset_of_m_font_7() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_font_7)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_m_font_7() const { return ___m_font_7; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_m_font_7() { return &___m_font_7; }
	inline void set_m_font_7(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___m_font_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_font_7), value);
	}

	inline static int32_t get_offset_of_m_font_texture_width_8() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_font_texture_width_8)); }
	inline int32_t get_m_font_texture_width_8() const { return ___m_font_texture_width_8; }
	inline int32_t* get_address_of_m_font_texture_width_8() { return &___m_font_texture_width_8; }
	inline void set_m_font_texture_width_8(int32_t value)
	{
		___m_font_texture_width_8 = value;
	}

	inline static int32_t get_offset_of_m_font_texture_height_9() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_font_texture_height_9)); }
	inline int32_t get_m_font_texture_height_9() const { return ___m_font_texture_height_9; }
	inline int32_t* get_address_of_m_font_texture_height_9() { return &___m_font_texture_height_9; }
	inline void set_m_font_texture_height_9(int32_t value)
	{
		___m_font_texture_height_9 = value;
	}

	inline static int32_t get_offset_of_m_font_data_file_10() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_font_data_file_10)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_m_font_data_file_10() const { return ___m_font_data_file_10; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_m_font_data_file_10() { return &___m_font_data_file_10; }
	inline void set_m_font_data_file_10(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___m_font_data_file_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_font_data_file_10), value);
	}

	inline static int32_t get_offset_of_m_font_material_11() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_font_material_11)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_font_material_11() const { return ___m_font_material_11; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_font_material_11() { return &___m_font_material_11; }
	inline void set_m_font_material_11(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_font_material_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_font_material_11), value);
	}

	inline static int32_t get_offset_of_m_px_offset_12() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_px_offset_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_px_offset_12() const { return ___m_px_offset_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_px_offset_12() { return &___m_px_offset_12; }
	inline void set_m_px_offset_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_px_offset_12 = value;
	}

	inline static int32_t get_offset_of_m_character_size_13() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_character_size_13)); }
	inline float get_m_character_size_13() const { return ___m_character_size_13; }
	inline float* get_address_of_m_character_size_13() { return &___m_character_size_13; }
	inline void set_m_character_size_13(float value)
	{
		___m_character_size_13 = value;
	}

	inline static int32_t get_offset_of_m_use_colour_gradient_14() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_use_colour_gradient_14)); }
	inline bool get_m_use_colour_gradient_14() const { return ___m_use_colour_gradient_14; }
	inline bool* get_address_of_m_use_colour_gradient_14() { return &___m_use_colour_gradient_14; }
	inline void set_m_use_colour_gradient_14(bool value)
	{
		___m_use_colour_gradient_14 = value;
	}

	inline static int32_t get_offset_of_m_textColour_15() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_textColour_15)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_textColour_15() const { return ___m_textColour_15; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_textColour_15() { return &___m_textColour_15; }
	inline void set_m_textColour_15(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_textColour_15 = value;
	}

	inline static int32_t get_offset_of_m_textColourGradient_16() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_textColourGradient_16)); }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * get_m_textColourGradient_16() const { return ___m_textColourGradient_16; }
	inline VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 ** get_address_of_m_textColourGradient_16() { return &___m_textColourGradient_16; }
	inline void set_m_textColourGradient_16(VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333 * value)
	{
		___m_textColourGradient_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_textColourGradient_16), value);
	}

	inline static int32_t get_offset_of_m_display_axis_17() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_display_axis_17)); }
	inline int32_t get_m_display_axis_17() const { return ___m_display_axis_17; }
	inline int32_t* get_address_of_m_display_axis_17() { return &___m_display_axis_17; }
	inline void set_m_display_axis_17(int32_t value)
	{
		___m_display_axis_17 = value;
	}

	inline static int32_t get_offset_of_m_text_anchor_18() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_text_anchor_18)); }
	inline int32_t get_m_text_anchor_18() const { return ___m_text_anchor_18; }
	inline int32_t* get_address_of_m_text_anchor_18() { return &___m_text_anchor_18; }
	inline void set_m_text_anchor_18(int32_t value)
	{
		___m_text_anchor_18 = value;
	}

	inline static int32_t get_offset_of_m_text_alignment_19() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_text_alignment_19)); }
	inline int32_t get_m_text_alignment_19() const { return ___m_text_alignment_19; }
	inline int32_t* get_address_of_m_text_alignment_19() { return &___m_text_alignment_19; }
	inline void set_m_text_alignment_19(int32_t value)
	{
		___m_text_alignment_19 = value;
	}

	inline static int32_t get_offset_of_m_line_height_factor_20() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_line_height_factor_20)); }
	inline float get_m_line_height_factor_20() const { return ___m_line_height_factor_20; }
	inline float* get_address_of_m_line_height_factor_20() { return &___m_line_height_factor_20; }
	inline void set_m_line_height_factor_20(float value)
	{
		___m_line_height_factor_20 = value;
	}

	inline static int32_t get_offset_of_m_max_width_21() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_max_width_21)); }
	inline float get_m_max_width_21() const { return ___m_max_width_21; }
	inline float* get_address_of_m_max_width_21() { return &___m_max_width_21; }
	inline void set_m_max_width_21(float value)
	{
		___m_max_width_21 = value;
	}

	inline static int32_t get_offset_of_m_override_font_baseline_22() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_override_font_baseline_22)); }
	inline bool get_m_override_font_baseline_22() const { return ___m_override_font_baseline_22; }
	inline bool* get_address_of_m_override_font_baseline_22() { return &___m_override_font_baseline_22; }
	inline void set_m_override_font_baseline_22(bool value)
	{
		___m_override_font_baseline_22 = value;
	}

	inline static int32_t get_offset_of_m_font_baseline_override_23() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_font_baseline_override_23)); }
	inline float get_m_font_baseline_override_23() const { return ___m_font_baseline_override_23; }
	inline float* get_address_of_m_font_baseline_override_23() { return &___m_font_baseline_override_23; }
	inline void set_m_font_baseline_override_23(float value)
	{
		___m_font_baseline_override_23 = value;
	}

	inline static int32_t get_offset_of_m_animation_manager_24() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_animation_manager_24)); }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * get_m_animation_manager_24() const { return ___m_animation_manager_24; }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 ** get_address_of_m_animation_manager_24() { return &___m_animation_manager_24; }
	inline void set_m_animation_manager_24(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * value)
	{
		___m_animation_manager_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_manager_24), value);
	}

	inline static int32_t get_offset_of_m_gameobject_reference_25() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_gameobject_reference_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_gameobject_reference_25() const { return ___m_gameobject_reference_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_gameobject_reference_25() { return &___m_gameobject_reference_25; }
	inline void set_m_gameobject_reference_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_gameobject_reference_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameobject_reference_25), value);
	}

	inline static int32_t get_offset_of_m_renderToCurve_26() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_renderToCurve_26)); }
	inline bool get_m_renderToCurve_26() const { return ___m_renderToCurve_26; }
	inline bool* get_address_of_m_renderToCurve_26() { return &___m_renderToCurve_26; }
	inline void set_m_renderToCurve_26(bool value)
	{
		___m_renderToCurve_26 = value;
	}

	inline static int32_t get_offset_of_m_bezierCurve_27() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_bezierCurve_27)); }
	inline TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  get_m_bezierCurve_27() const { return ___m_bezierCurve_27; }
	inline TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7 * get_address_of_m_bezierCurve_27() { return &___m_bezierCurve_27; }
	inline void set_m_bezierCurve_27(TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  value)
	{
		___m_bezierCurve_27 = value;
	}

	inline static int32_t get_offset_of_U3COnMeshUpdateCallU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___U3COnMeshUpdateCallU3Ek__BackingField_28)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3COnMeshUpdateCallU3Ek__BackingField_28() const { return ___U3COnMeshUpdateCallU3Ek__BackingField_28; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3COnMeshUpdateCallU3Ek__BackingField_28() { return &___U3COnMeshUpdateCallU3Ek__BackingField_28; }
	inline void set_U3COnMeshUpdateCallU3Ek__BackingField_28(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3COnMeshUpdateCallU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnMeshUpdateCallU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_m_mesh_verts_29() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_mesh_verts_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_mesh_verts_29() const { return ___m_mesh_verts_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_mesh_verts_29() { return &___m_mesh_verts_29; }
	inline void set_m_mesh_verts_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_mesh_verts_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_verts_29), value);
	}

	inline static int32_t get_offset_of_m_mesh_uvs_30() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_mesh_uvs_30)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_mesh_uvs_30() const { return ___m_mesh_uvs_30; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_mesh_uvs_30() { return &___m_mesh_uvs_30; }
	inline void set_m_mesh_uvs_30(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_mesh_uvs_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_uvs_30), value);
	}

	inline static int32_t get_offset_of_m_mesh_cols_31() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_mesh_cols_31)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_mesh_cols_31() const { return ___m_mesh_cols_31; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_mesh_cols_31() { return &___m_mesh_cols_31; }
	inline void set_m_mesh_cols_31(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_mesh_cols_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_cols_31), value);
	}

	inline static int32_t get_offset_of_m_mesh_normals_32() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_mesh_normals_32)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_mesh_normals_32() const { return ___m_mesh_normals_32; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_mesh_normals_32() { return &___m_mesh_normals_32; }
	inline void set_m_mesh_normals_32(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_mesh_normals_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_normals_32), value);
	}

	inline static int32_t get_offset_of_m_mesh_triangles_33() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_mesh_triangles_33)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_mesh_triangles_33() const { return ___m_mesh_triangles_33; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_mesh_triangles_33() { return &___m_mesh_triangles_33; }
	inline void set_m_mesh_triangles_33(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_mesh_triangles_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_triangles_33), value);
	}

	inline static int32_t get_offset_of_m_font_baseline_34() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_font_baseline_34)); }
	inline float get_m_font_baseline_34() const { return ___m_font_baseline_34; }
	inline float* get_address_of_m_font_baseline_34() { return &___m_font_baseline_34; }
	inline void set_m_font_baseline_34(float value)
	{
		___m_font_baseline_34 = value;
	}

	inline static int32_t get_offset_of_m_custom_font_data_35() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_custom_font_data_35)); }
	inline CustomFontCharacterData_t1391FAABB7293C043AAF8845B64651AF4ED69E99 * get_m_custom_font_data_35() const { return ___m_custom_font_data_35; }
	inline CustomFontCharacterData_t1391FAABB7293C043AAF8845B64651AF4ED69E99 ** get_address_of_m_custom_font_data_35() { return &___m_custom_font_data_35; }
	inline void set_m_custom_font_data_35(CustomFontCharacterData_t1391FAABB7293C043AAF8845B64651AF4ED69E99 * value)
	{
		___m_custom_font_data_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_custom_font_data_35), value);
	}

	inline static int32_t get_offset_of_m_current_font_data_file_name_36() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_current_font_data_file_name_36)); }
	inline String_t* get_m_current_font_data_file_name_36() const { return ___m_current_font_data_file_name_36; }
	inline String_t** get_address_of_m_current_font_data_file_name_36() { return &___m_current_font_data_file_name_36; }
	inline void set_m_current_font_data_file_name_36(String_t* value)
	{
		___m_current_font_data_file_name_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_current_font_data_file_name_36), value);
	}

	inline static int32_t get_offset_of_m_current_font_name_37() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_current_font_name_37)); }
	inline String_t* get_m_current_font_name_37() const { return ___m_current_font_name_37; }
	inline String_t** get_address_of_m_current_font_name_37() { return &___m_current_font_name_37; }
	inline void set_m_current_font_name_37(String_t* value)
	{
		___m_current_font_name_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_current_font_name_37), value);
	}

	inline static int32_t get_offset_of_m_fontRebuildCallback_38() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_fontRebuildCallback_38)); }
	inline Action_1_t795662E553415ECF2DD0F8EEB9BA170C3670F37C * get_m_fontRebuildCallback_38() const { return ___m_fontRebuildCallback_38; }
	inline Action_1_t795662E553415ECF2DD0F8EEB9BA170C3670F37C ** get_address_of_m_fontRebuildCallback_38() { return &___m_fontRebuildCallback_38; }
	inline void set_m_fontRebuildCallback_38(Action_1_t795662E553415ECF2DD0F8EEB9BA170C3670F37C * value)
	{
		___m_fontRebuildCallback_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontRebuildCallback_38), value);
	}

	inline static int32_t get_offset_of_m_mesh_combine_instance_39() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_mesh_combine_instance_39)); }
	inline CombineInstanceU5BU5D_t3865A4038A85A764FCE1EE0C134B77C42F4610DA* get_m_mesh_combine_instance_39() const { return ___m_mesh_combine_instance_39; }
	inline CombineInstanceU5BU5D_t3865A4038A85A764FCE1EE0C134B77C42F4610DA** get_address_of_m_mesh_combine_instance_39() { return &___m_mesh_combine_instance_39; }
	inline void set_m_mesh_combine_instance_39(CombineInstanceU5BU5D_t3865A4038A85A764FCE1EE0C134B77C42F4610DA* value)
	{
		___m_mesh_combine_instance_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_combine_instance_39), value);
	}

	inline static int32_t get_offset_of_m_transform_reference_40() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_transform_reference_40)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_transform_reference_40() const { return ___m_transform_reference_40; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_transform_reference_40() { return &___m_transform_reference_40; }
	inline void set_m_transform_reference_40(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_transform_reference_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_reference_40), value);
	}

	inline static int32_t get_offset_of_m_renderer_41() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_renderer_41)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_m_renderer_41() const { return ___m_renderer_41; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_m_renderer_41() { return &___m_renderer_41; }
	inline void set_m_renderer_41(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___m_renderer_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_41), value);
	}

	inline static int32_t get_offset_of_m_mesh_filter_42() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_mesh_filter_42)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_m_mesh_filter_42() const { return ___m_mesh_filter_42; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_m_mesh_filter_42() { return &___m_mesh_filter_42; }
	inline void set_m_mesh_filter_42(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___m_mesh_filter_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_filter_42), value);
	}

	inline static int32_t get_offset_of_m_mesh_43() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_mesh_43)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_43() const { return ___m_mesh_43; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_43() { return &___m_mesh_43; }
	inline void set_m_mesh_43(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_43), value);
	}

	inline static int32_t get_offset_of_m_total_text_width_44() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_total_text_width_44)); }
	inline float get_m_total_text_width_44() const { return ___m_total_text_width_44; }
	inline float* get_address_of_m_total_text_width_44() { return &___m_total_text_width_44; }
	inline void set_m_total_text_width_44(float value)
	{
		___m_total_text_width_44 = value;
	}

	inline static int32_t get_offset_of_m_total_text_height_45() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_total_text_height_45)); }
	inline float get_m_total_text_height_45() const { return ___m_total_text_height_45; }
	inline float* get_address_of_m_total_text_height_45() { return &___m_total_text_height_45; }
	inline void set_m_total_text_height_45(float value)
	{
		___m_total_text_height_45 = value;
	}

	inline static int32_t get_offset_of_m_line_height_46() { return static_cast<int32_t>(offsetof(TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E, ___m_line_height_46)); }
	inline float get_m_line_height_46() const { return ___m_line_height_46; }
	inline float* get_address_of_m_line_height_46() { return &___m_line_height_46; }
	inline void set_m_line_height_46(float value)
	{
		___m_line_height_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXNATIVE_T097594515F97FCCF174099736286557AE5B9FA9E_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#define SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_5;
	// UnityEngine.UI.Selectable_Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable_SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_5)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_7)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_8)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_11)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_17)); }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_List_4)); }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifndef BOXSLIDER_TA8560E79728DA37948F4B2D37D7CC790511E407A_H
#define BOXSLIDER_TA8560E79728DA37948F4B2D37D7CC790511E407A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider
struct  BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HandleRect_18;
	// System.Single UnityEngine.UI.BoxSlider::m_MinValue
	float ___m_MinValue_19;
	// System.Single UnityEngine.UI.BoxSlider::m_MaxValue
	float ___m_MaxValue_20;
	// System.Boolean UnityEngine.UI.BoxSlider::m_WholeNumbers
	bool ___m_WholeNumbers_21;
	// System.Single UnityEngine.UI.BoxSlider::m_Value
	float ___m_Value_22;
	// System.Single UnityEngine.UI.BoxSlider::m_ValueY
	float ___m_ValueY_23;
	// UnityEngine.UI.BoxSlider_BoxSliderEvent UnityEngine.UI.BoxSlider::m_OnValueChanged
	BoxSliderEvent_t2E59E1AD1FFFC4D521C2865063F16C634BF59706 * ___m_OnValueChanged_24;
	// UnityEngine.Transform UnityEngine.UI.BoxSlider::m_HandleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_HandleTransform_25;
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleContainerRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_HandleContainerRect_26;
	// UnityEngine.Vector2 UnityEngine.UI.BoxSlider::m_Offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Offset_27;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.BoxSlider::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_28;

public:
	inline static int32_t get_offset_of_m_HandleRect_18() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_HandleRect_18)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HandleRect_18() const { return ___m_HandleRect_18; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HandleRect_18() { return &___m_HandleRect_18; }
	inline void set_m_HandleRect_18(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HandleRect_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_18), value);
	}

	inline static int32_t get_offset_of_m_MinValue_19() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_MinValue_19)); }
	inline float get_m_MinValue_19() const { return ___m_MinValue_19; }
	inline float* get_address_of_m_MinValue_19() { return &___m_MinValue_19; }
	inline void set_m_MinValue_19(float value)
	{
		___m_MinValue_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_20() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_MaxValue_20)); }
	inline float get_m_MaxValue_20() const { return ___m_MaxValue_20; }
	inline float* get_address_of_m_MaxValue_20() { return &___m_MaxValue_20; }
	inline void set_m_MaxValue_20(float value)
	{
		___m_MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_21() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_WholeNumbers_21)); }
	inline bool get_m_WholeNumbers_21() const { return ___m_WholeNumbers_21; }
	inline bool* get_address_of_m_WholeNumbers_21() { return &___m_WholeNumbers_21; }
	inline void set_m_WholeNumbers_21(bool value)
	{
		___m_WholeNumbers_21 = value;
	}

	inline static int32_t get_offset_of_m_Value_22() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_Value_22)); }
	inline float get_m_Value_22() const { return ___m_Value_22; }
	inline float* get_address_of_m_Value_22() { return &___m_Value_22; }
	inline void set_m_Value_22(float value)
	{
		___m_Value_22 = value;
	}

	inline static int32_t get_offset_of_m_ValueY_23() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_ValueY_23)); }
	inline float get_m_ValueY_23() const { return ___m_ValueY_23; }
	inline float* get_address_of_m_ValueY_23() { return &___m_ValueY_23; }
	inline void set_m_ValueY_23(float value)
	{
		___m_ValueY_23 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_24() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_OnValueChanged_24)); }
	inline BoxSliderEvent_t2E59E1AD1FFFC4D521C2865063F16C634BF59706 * get_m_OnValueChanged_24() const { return ___m_OnValueChanged_24; }
	inline BoxSliderEvent_t2E59E1AD1FFFC4D521C2865063F16C634BF59706 ** get_address_of_m_OnValueChanged_24() { return &___m_OnValueChanged_24; }
	inline void set_m_OnValueChanged_24(BoxSliderEvent_t2E59E1AD1FFFC4D521C2865063F16C634BF59706 * value)
	{
		___m_OnValueChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_24), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_25() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_HandleTransform_25)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_HandleTransform_25() const { return ___m_HandleTransform_25; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_HandleTransform_25() { return &___m_HandleTransform_25; }
	inline void set_m_HandleTransform_25(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_HandleTransform_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_25), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_26() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_HandleContainerRect_26)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_HandleContainerRect_26() const { return ___m_HandleContainerRect_26; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_HandleContainerRect_26() { return &___m_HandleContainerRect_26; }
	inline void set_m_HandleContainerRect_26(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_HandleContainerRect_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_26), value);
	}

	inline static int32_t get_offset_of_m_Offset_27() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_Offset_27)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Offset_27() const { return ___m_Offset_27; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Offset_27() { return &___m_Offset_27; }
	inline void set_m_Offset_27(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Offset_27 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_28() { return static_cast<int32_t>(offsetof(BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A, ___m_Tracker_28)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_28() const { return ___m_Tracker_28; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_28() { return &___m_Tracker_28; }
	inline void set_m_Tracker_28(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDER_TA8560E79728DA37948F4B2D37D7CC790511E407A_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#define TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_30;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_31;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_fontAsset_32;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_currentFontAsset_33;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_34;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_sharedMaterial_35;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_currentMaterial_36;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___m_materialReferences_37;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F * ___m_materialReferenceIndexLookup_38;
	// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___m_materialReferenceStack_39;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___m_fontSharedMaterials_41;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_fontMaterial_42;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___m_fontMaterials_43;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_fontColor32_45;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_fontColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_underlineColor_48;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_strikethroughColor_49;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_highlightColor_50;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_highlightPadding
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_highlightPadding_51;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_52;
	// TMPro.ColorMode TMPro.TMP_Text::m_colorMode
	int32_t ___m_colorMode_53;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  ___m_fontColorGradient_54;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_fontColorGradientPreset_55;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_spriteAsset_56;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_57;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_58;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_spriteColor_59;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_60;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_faceColor_61;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_outlineColor_62;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_63;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_64;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_65;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_66;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___m_sizeStack_67;
	// TMPro.FontWeight TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_68;
	// TMPro.FontWeight TMPro.TMP_Text::m_FontWeightInternal
	int32_t ___m_FontWeightInternal_69;
	// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight> TMPro.TMP_Text::m_FontWeightStack
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___m_FontWeightStack_70;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_71;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_72;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_73;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_74;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_75;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_76;
	// TMPro.FontStyles TMPro.TMP_Text::m_FontStyleInternal
	int32_t ___m_FontStyleInternal_77;
	// TMPro.TMP_FontStyleStack TMPro.TMP_Text::m_fontStyleStack
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___m_fontStyleStack_78;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_79;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_80;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_81;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___m_lineJustificationStack_82;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_textContainerLocalCorners_83;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_84;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_85;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_86;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_87;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_88;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_89;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_90;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_91;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_92;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_93;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_94;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_95;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_96;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_97;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_98;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_99;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_100;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_101;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_linkedTextComponent_102;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_103;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_104;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_105;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_106;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_107;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_108;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_109;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_110;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_111;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_112;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_113;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_114;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_115;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_116;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_117;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_118;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_119;
	// System.Boolean TMPro.TMP_Text::m_VertexBufferAutoSizeReduction
	bool ___m_VertexBufferAutoSizeReduction_120;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_121;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_122;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_123;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_124;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_125;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_126;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_127;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_margin_128;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_129;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_130;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_131;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_132;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_133;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___m_textInfo_134;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_135;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_136;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_transform_137;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_138;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_139;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_140;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_141;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_142;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * ___m_spriteAnimator_143;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_144;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_145;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_146;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_147;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_148;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_149;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___m_LayoutElement_150;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_151;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_152;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_153;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_154;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_155;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_156;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_157;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_158;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_159;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_160;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_161;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_162;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_163;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_164;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_165;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_166;
	// TMPro.TMP_Text_TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_167;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_168;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_169;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_170;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_htmlTag_171;
	// TMPro.RichTextTagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* ___m_xmlAttribute_172;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_attributeParameterValues_173;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_174;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_175;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___m_indentStack_176;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_177;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_178;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_FXMatrix_179;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_180;
	// TMPro.TMP_Text_UnicodeChar[] TMPro.TMP_Text::m_TextParsingBuffer
	UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* ___m_TextParsingBuffer_181;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* ___m_internalCharacterInfo_182;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_input_CharArray_183;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_184;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_185;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  ___m_SavedWordWrapState_186;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  ___m_SavedLineState_187;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_188;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_189;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_190;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_191;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_192;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_193;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_194;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_195;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_196;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_197;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_198;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_199;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_200;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_201;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_202;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___m_meshExtents_203;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_htmlColor_204;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_colorStack_205;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_underlineColorStack_206;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_strikethroughColorStack_207;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_highlightColorStack_208;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_colorGradientPreset_209;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___m_colorGradientStack_210;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_211;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_212;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___m_styleStack_213;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___m_actionStack_214;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_215;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_216;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___m_baselineOffsetStack_217;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_218;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_219;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___m_cached_TextElement_220;
	// TMPro.TMP_Character TMPro.TMP_Text::m_cached_Underline_Character
	TMP_Character_t1875AACA978396521498D6A699052C187903553D * ___m_cached_Underline_Character_221;
	// TMPro.TMP_Character TMPro.TMP_Text::m_cached_Ellipsis_Character
	TMP_Character_t1875AACA978396521498D6A699052C187903553D * ___m_cached_Ellipsis_Character_222;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_defaultSpriteAsset_223;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_currentSpriteAsset_224;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_225;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_226;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_227;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_228;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___k_Power_229;

public:
	inline static int32_t get_offset_of_m_text_30() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_text_30)); }
	inline String_t* get_m_text_30() const { return ___m_text_30; }
	inline String_t** get_address_of_m_text_30() { return &___m_text_30; }
	inline void set_m_text_30(String_t* value)
	{
		___m_text_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_30), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_31() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isRightToLeft_31)); }
	inline bool get_m_isRightToLeft_31() const { return ___m_isRightToLeft_31; }
	inline bool* get_address_of_m_isRightToLeft_31() { return &___m_isRightToLeft_31; }
	inline void set_m_isRightToLeft_31(bool value)
	{
		___m_isRightToLeft_31 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_32() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontAsset_32)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_fontAsset_32() const { return ___m_fontAsset_32; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_fontAsset_32() { return &___m_fontAsset_32; }
	inline void set_m_fontAsset_32(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_fontAsset_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_32), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_33() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentFontAsset_33)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_currentFontAsset_33() const { return ___m_currentFontAsset_33; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_currentFontAsset_33() { return &___m_currentFontAsset_33; }
	inline void set_m_currentFontAsset_33(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_currentFontAsset_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_33), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_34() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isSDFShader_34)); }
	inline bool get_m_isSDFShader_34() const { return ___m_isSDFShader_34; }
	inline bool* get_address_of_m_isSDFShader_34() { return &___m_isSDFShader_34; }
	inline void set_m_isSDFShader_34(bool value)
	{
		___m_isSDFShader_34 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_35() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_sharedMaterial_35)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_sharedMaterial_35() const { return ___m_sharedMaterial_35; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_sharedMaterial_35() { return &___m_sharedMaterial_35; }
	inline void set_m_sharedMaterial_35(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_sharedMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_35), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_36() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentMaterial_36)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_currentMaterial_36() const { return ___m_currentMaterial_36; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_currentMaterial_36() { return &___m_currentMaterial_36; }
	inline void set_m_currentMaterial_36(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_currentMaterial_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_37() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferences_37)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_m_materialReferences_37() const { return ___m_materialReferences_37; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_m_materialReferences_37() { return &___m_materialReferences_37; }
	inline void set_m_materialReferences_37(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___m_materialReferences_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_37), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_38() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferenceIndexLookup_38)); }
	inline Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F * get_m_materialReferenceIndexLookup_38() const { return ___m_materialReferenceIndexLookup_38; }
	inline Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F ** get_address_of_m_materialReferenceIndexLookup_38() { return &___m_materialReferenceIndexLookup_38; }
	inline void set_m_materialReferenceIndexLookup_38(Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F * value)
	{
		___m_materialReferenceIndexLookup_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_38), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_39() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferenceStack_39)); }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  get_m_materialReferenceStack_39() const { return ___m_materialReferenceStack_39; }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 * get_address_of_m_materialReferenceStack_39() { return &___m_materialReferenceStack_39; }
	inline void set_m_materialReferenceStack_39(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  value)
	{
		___m_materialReferenceStack_39 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_40() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentMaterialIndex_40)); }
	inline int32_t get_m_currentMaterialIndex_40() const { return ___m_currentMaterialIndex_40; }
	inline int32_t* get_address_of_m_currentMaterialIndex_40() { return &___m_currentMaterialIndex_40; }
	inline void set_m_currentMaterialIndex_40(int32_t value)
	{
		___m_currentMaterialIndex_40 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSharedMaterials_41)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_m_fontSharedMaterials_41() const { return ___m_fontSharedMaterials_41; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_m_fontSharedMaterials_41() { return &___m_fontSharedMaterials_41; }
	inline void set_m_fontSharedMaterials_41(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___m_fontSharedMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_42() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontMaterial_42)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_fontMaterial_42() const { return ___m_fontMaterial_42; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_fontMaterial_42() { return &___m_fontMaterial_42; }
	inline void set_m_fontMaterial_42(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_fontMaterial_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_42), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_43() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontMaterials_43)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_m_fontMaterials_43() const { return ___m_fontMaterials_43; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_m_fontMaterials_43() { return &___m_fontMaterials_43; }
	inline void set_m_fontMaterials_43(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___m_fontMaterials_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_43), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_44() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isMaterialDirty_44)); }
	inline bool get_m_isMaterialDirty_44() const { return ___m_isMaterialDirty_44; }
	inline bool* get_address_of_m_isMaterialDirty_44() { return &___m_isMaterialDirty_44; }
	inline void set_m_isMaterialDirty_44(bool value)
	{
		___m_isMaterialDirty_44 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_45() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColor32_45)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_fontColor32_45() const { return ___m_fontColor32_45; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_fontColor32_45() { return &___m_fontColor32_45; }
	inline void set_m_fontColor32_45(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_fontColor32_45 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColor_46)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_fontColor_46() const { return ___m_fontColor_46; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_fontColor_46() { return &___m_fontColor_46; }
	inline void set_m_fontColor_46(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_fontColor_46 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_underlineColor_48)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_underlineColor_48() const { return ___m_underlineColor_48; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_underlineColor_48() { return &___m_underlineColor_48; }
	inline void set_m_underlineColor_48(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_underlineColor_48 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_49() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_strikethroughColor_49)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_strikethroughColor_49() const { return ___m_strikethroughColor_49; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_strikethroughColor_49() { return &___m_strikethroughColor_49; }
	inline void set_m_strikethroughColor_49(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_strikethroughColor_49 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_50() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightColor_50)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_highlightColor_50() const { return ___m_highlightColor_50; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_highlightColor_50() { return &___m_highlightColor_50; }
	inline void set_m_highlightColor_50(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_highlightColor_50 = value;
	}

	inline static int32_t get_offset_of_m_highlightPadding_51() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightPadding_51)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_highlightPadding_51() const { return ___m_highlightPadding_51; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_highlightPadding_51() { return &___m_highlightPadding_51; }
	inline void set_m_highlightPadding_51(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_highlightPadding_51 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_52() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableVertexGradient_52)); }
	inline bool get_m_enableVertexGradient_52() const { return ___m_enableVertexGradient_52; }
	inline bool* get_address_of_m_enableVertexGradient_52() { return &___m_enableVertexGradient_52; }
	inline void set_m_enableVertexGradient_52(bool value)
	{
		___m_enableVertexGradient_52 = value;
	}

	inline static int32_t get_offset_of_m_colorMode_53() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorMode_53)); }
	inline int32_t get_m_colorMode_53() const { return ___m_colorMode_53; }
	inline int32_t* get_address_of_m_colorMode_53() { return &___m_colorMode_53; }
	inline void set_m_colorMode_53(int32_t value)
	{
		___m_colorMode_53 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_54() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColorGradient_54)); }
	inline VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  get_m_fontColorGradient_54() const { return ___m_fontColorGradient_54; }
	inline VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A * get_address_of_m_fontColorGradient_54() { return &___m_fontColorGradient_54; }
	inline void set_m_fontColorGradient_54(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  value)
	{
		___m_fontColorGradient_54 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_55() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColorGradientPreset_55)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_fontColorGradientPreset_55() const { return ___m_fontColorGradientPreset_55; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_fontColorGradientPreset_55() { return &___m_fontColorGradientPreset_55; }
	inline void set_m_fontColorGradientPreset_55(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_fontColorGradientPreset_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_55), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_56() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAsset_56)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_spriteAsset_56() const { return ___m_spriteAsset_56; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_spriteAsset_56() { return &___m_spriteAsset_56; }
	inline void set_m_spriteAsset_56(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_spriteAsset_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_56), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_57() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tintAllSprites_57)); }
	inline bool get_m_tintAllSprites_57() const { return ___m_tintAllSprites_57; }
	inline bool* get_address_of_m_tintAllSprites_57() { return &___m_tintAllSprites_57; }
	inline void set_m_tintAllSprites_57(bool value)
	{
		___m_tintAllSprites_57 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_58() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tintSprite_58)); }
	inline bool get_m_tintSprite_58() const { return ___m_tintSprite_58; }
	inline bool* get_address_of_m_tintSprite_58() { return &___m_tintSprite_58; }
	inline void set_m_tintSprite_58(bool value)
	{
		___m_tintSprite_58 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_59() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteColor_59)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_spriteColor_59() const { return ___m_spriteColor_59; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_spriteColor_59() { return &___m_spriteColor_59; }
	inline void set_m_spriteColor_59(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_spriteColor_59 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_60() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_overrideHtmlColors_60)); }
	inline bool get_m_overrideHtmlColors_60() const { return ___m_overrideHtmlColors_60; }
	inline bool* get_address_of_m_overrideHtmlColors_60() { return &___m_overrideHtmlColors_60; }
	inline void set_m_overrideHtmlColors_60(bool value)
	{
		___m_overrideHtmlColors_60 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_61() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_faceColor_61)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_faceColor_61() const { return ___m_faceColor_61; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_faceColor_61() { return &___m_faceColor_61; }
	inline void set_m_faceColor_61(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_faceColor_61 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_62() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_outlineColor_62)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_outlineColor_62() const { return ___m_outlineColor_62; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_outlineColor_62() { return &___m_outlineColor_62; }
	inline void set_m_outlineColor_62(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_outlineColor_62 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_63() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_outlineWidth_63)); }
	inline float get_m_outlineWidth_63() const { return ___m_outlineWidth_63; }
	inline float* get_address_of_m_outlineWidth_63() { return &___m_outlineWidth_63; }
	inline void set_m_outlineWidth_63(float value)
	{
		___m_outlineWidth_63 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_64() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSize_64)); }
	inline float get_m_fontSize_64() const { return ___m_fontSize_64; }
	inline float* get_address_of_m_fontSize_64() { return &___m_fontSize_64; }
	inline void set_m_fontSize_64(float value)
	{
		___m_fontSize_64 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_65() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentFontSize_65)); }
	inline float get_m_currentFontSize_65() const { return ___m_currentFontSize_65; }
	inline float* get_address_of_m_currentFontSize_65() { return &___m_currentFontSize_65; }
	inline void set_m_currentFontSize_65(float value)
	{
		___m_currentFontSize_65 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_66() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeBase_66)); }
	inline float get_m_fontSizeBase_66() const { return ___m_fontSizeBase_66; }
	inline float* get_address_of_m_fontSizeBase_66() { return &___m_fontSizeBase_66; }
	inline void set_m_fontSizeBase_66(float value)
	{
		___m_fontSizeBase_66 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_67() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_sizeStack_67)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_m_sizeStack_67() const { return ___m_sizeStack_67; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_m_sizeStack_67() { return &___m_sizeStack_67; }
	inline void set_m_sizeStack_67(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___m_sizeStack_67 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_68() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontWeight_68)); }
	inline int32_t get_m_fontWeight_68() const { return ___m_fontWeight_68; }
	inline int32_t* get_address_of_m_fontWeight_68() { return &___m_fontWeight_68; }
	inline void set_m_fontWeight_68(int32_t value)
	{
		___m_fontWeight_68 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightInternal_69() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontWeightInternal_69)); }
	inline int32_t get_m_FontWeightInternal_69() const { return ___m_FontWeightInternal_69; }
	inline int32_t* get_address_of_m_FontWeightInternal_69() { return &___m_FontWeightInternal_69; }
	inline void set_m_FontWeightInternal_69(int32_t value)
	{
		___m_FontWeightInternal_69 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightStack_70() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontWeightStack_70)); }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  get_m_FontWeightStack_70() const { return ___m_FontWeightStack_70; }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B * get_address_of_m_FontWeightStack_70() { return &___m_FontWeightStack_70; }
	inline void set_m_FontWeightStack_70(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  value)
	{
		___m_FontWeightStack_70 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_71() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableAutoSizing_71)); }
	inline bool get_m_enableAutoSizing_71() const { return ___m_enableAutoSizing_71; }
	inline bool* get_address_of_m_enableAutoSizing_71() { return &___m_enableAutoSizing_71; }
	inline void set_m_enableAutoSizing_71(bool value)
	{
		___m_enableAutoSizing_71 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_72() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxFontSize_72)); }
	inline float get_m_maxFontSize_72() const { return ___m_maxFontSize_72; }
	inline float* get_address_of_m_maxFontSize_72() { return &___m_maxFontSize_72; }
	inline void set_m_maxFontSize_72(float value)
	{
		___m_maxFontSize_72 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_73() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minFontSize_73)); }
	inline float get_m_minFontSize_73() const { return ___m_minFontSize_73; }
	inline float* get_address_of_m_minFontSize_73() { return &___m_minFontSize_73; }
	inline void set_m_minFontSize_73(float value)
	{
		___m_minFontSize_73 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_74() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeMin_74)); }
	inline float get_m_fontSizeMin_74() const { return ___m_fontSizeMin_74; }
	inline float* get_address_of_m_fontSizeMin_74() { return &___m_fontSizeMin_74; }
	inline void set_m_fontSizeMin_74(float value)
	{
		___m_fontSizeMin_74 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_75() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeMax_75)); }
	inline float get_m_fontSizeMax_75() const { return ___m_fontSizeMax_75; }
	inline float* get_address_of_m_fontSizeMax_75() { return &___m_fontSizeMax_75; }
	inline void set_m_fontSizeMax_75(float value)
	{
		___m_fontSizeMax_75 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_76() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontStyle_76)); }
	inline int32_t get_m_fontStyle_76() const { return ___m_fontStyle_76; }
	inline int32_t* get_address_of_m_fontStyle_76() { return &___m_fontStyle_76; }
	inline void set_m_fontStyle_76(int32_t value)
	{
		___m_fontStyle_76 = value;
	}

	inline static int32_t get_offset_of_m_FontStyleInternal_77() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontStyleInternal_77)); }
	inline int32_t get_m_FontStyleInternal_77() const { return ___m_FontStyleInternal_77; }
	inline int32_t* get_address_of_m_FontStyleInternal_77() { return &___m_FontStyleInternal_77; }
	inline void set_m_FontStyleInternal_77(int32_t value)
	{
		___m_FontStyleInternal_77 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_78() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontStyleStack_78)); }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  get_m_fontStyleStack_78() const { return ___m_fontStyleStack_78; }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 * get_address_of_m_fontStyleStack_78() { return &___m_fontStyleStack_78; }
	inline void set_m_fontStyleStack_78(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  value)
	{
		___m_fontStyleStack_78 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_79() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isUsingBold_79)); }
	inline bool get_m_isUsingBold_79() const { return ___m_isUsingBold_79; }
	inline bool* get_address_of_m_isUsingBold_79() { return &___m_isUsingBold_79; }
	inline void set_m_isUsingBold_79(bool value)
	{
		___m_isUsingBold_79 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_80() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textAlignment_80)); }
	inline int32_t get_m_textAlignment_80() const { return ___m_textAlignment_80; }
	inline int32_t* get_address_of_m_textAlignment_80() { return &___m_textAlignment_80; }
	inline void set_m_textAlignment_80(int32_t value)
	{
		___m_textAlignment_80 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_81() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineJustification_81)); }
	inline int32_t get_m_lineJustification_81() const { return ___m_lineJustification_81; }
	inline int32_t* get_address_of_m_lineJustification_81() { return &___m_lineJustification_81; }
	inline void set_m_lineJustification_81(int32_t value)
	{
		___m_lineJustification_81 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_82() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineJustificationStack_82)); }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  get_m_lineJustificationStack_82() const { return ___m_lineJustificationStack_82; }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 * get_address_of_m_lineJustificationStack_82() { return &___m_lineJustificationStack_82; }
	inline void set_m_lineJustificationStack_82(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  value)
	{
		___m_lineJustificationStack_82 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_83() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textContainerLocalCorners_83)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_textContainerLocalCorners_83() const { return ___m_textContainerLocalCorners_83; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_textContainerLocalCorners_83() { return &___m_textContainerLocalCorners_83; }
	inline void set_m_textContainerLocalCorners_83(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_textContainerLocalCorners_83 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_83), value);
	}

	inline static int32_t get_offset_of_m_characterSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_characterSpacing_84)); }
	inline float get_m_characterSpacing_84() const { return ___m_characterSpacing_84; }
	inline float* get_address_of_m_characterSpacing_84() { return &___m_characterSpacing_84; }
	inline void set_m_characterSpacing_84(float value)
	{
		___m_characterSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cSpacing_85)); }
	inline float get_m_cSpacing_85() const { return ___m_cSpacing_85; }
	inline float* get_address_of_m_cSpacing_85() { return &___m_cSpacing_85; }
	inline void set_m_cSpacing_85(float value)
	{
		___m_cSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_86() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_monoSpacing_86)); }
	inline float get_m_monoSpacing_86() const { return ___m_monoSpacing_86; }
	inline float* get_address_of_m_monoSpacing_86() { return &___m_monoSpacing_86; }
	inline void set_m_monoSpacing_86(float value)
	{
		___m_monoSpacing_86 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_87() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_wordSpacing_87)); }
	inline float get_m_wordSpacing_87() const { return ___m_wordSpacing_87; }
	inline float* get_address_of_m_wordSpacing_87() { return &___m_wordSpacing_87; }
	inline void set_m_wordSpacing_87(float value)
	{
		___m_wordSpacing_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_88() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacing_88)); }
	inline float get_m_lineSpacing_88() const { return ___m_lineSpacing_88; }
	inline float* get_address_of_m_lineSpacing_88() { return &___m_lineSpacing_88; }
	inline void set_m_lineSpacing_88(float value)
	{
		___m_lineSpacing_88 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_89() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacingDelta_89)); }
	inline float get_m_lineSpacingDelta_89() const { return ___m_lineSpacingDelta_89; }
	inline float* get_address_of_m_lineSpacingDelta_89() { return &___m_lineSpacingDelta_89; }
	inline void set_m_lineSpacingDelta_89(float value)
	{
		___m_lineSpacingDelta_89 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_90() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineHeight_90)); }
	inline float get_m_lineHeight_90() const { return ___m_lineHeight_90; }
	inline float* get_address_of_m_lineHeight_90() { return &___m_lineHeight_90; }
	inline void set_m_lineHeight_90(float value)
	{
		___m_lineHeight_90 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_91() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacingMax_91)); }
	inline float get_m_lineSpacingMax_91() const { return ___m_lineSpacingMax_91; }
	inline float* get_address_of_m_lineSpacingMax_91() { return &___m_lineSpacingMax_91; }
	inline void set_m_lineSpacingMax_91(float value)
	{
		___m_lineSpacingMax_91 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_92() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_paragraphSpacing_92)); }
	inline float get_m_paragraphSpacing_92() const { return ___m_paragraphSpacing_92; }
	inline float* get_address_of_m_paragraphSpacing_92() { return &___m_paragraphSpacing_92; }
	inline void set_m_paragraphSpacing_92(float value)
	{
		___m_paragraphSpacing_92 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_93() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charWidthMaxAdj_93)); }
	inline float get_m_charWidthMaxAdj_93() const { return ___m_charWidthMaxAdj_93; }
	inline float* get_address_of_m_charWidthMaxAdj_93() { return &___m_charWidthMaxAdj_93; }
	inline void set_m_charWidthMaxAdj_93(float value)
	{
		___m_charWidthMaxAdj_93 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_94() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charWidthAdjDelta_94)); }
	inline float get_m_charWidthAdjDelta_94() const { return ___m_charWidthAdjDelta_94; }
	inline float* get_address_of_m_charWidthAdjDelta_94() { return &___m_charWidthAdjDelta_94; }
	inline void set_m_charWidthAdjDelta_94(float value)
	{
		___m_charWidthAdjDelta_94 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_95() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableWordWrapping_95)); }
	inline bool get_m_enableWordWrapping_95() const { return ___m_enableWordWrapping_95; }
	inline bool* get_address_of_m_enableWordWrapping_95() { return &___m_enableWordWrapping_95; }
	inline void set_m_enableWordWrapping_95(bool value)
	{
		___m_enableWordWrapping_95 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_96() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCharacterWrappingEnabled_96)); }
	inline bool get_m_isCharacterWrappingEnabled_96() const { return ___m_isCharacterWrappingEnabled_96; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_96() { return &___m_isCharacterWrappingEnabled_96; }
	inline void set_m_isCharacterWrappingEnabled_96(bool value)
	{
		___m_isCharacterWrappingEnabled_96 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_97() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isNonBreakingSpace_97)); }
	inline bool get_m_isNonBreakingSpace_97() const { return ___m_isNonBreakingSpace_97; }
	inline bool* get_address_of_m_isNonBreakingSpace_97() { return &___m_isNonBreakingSpace_97; }
	inline void set_m_isNonBreakingSpace_97(bool value)
	{
		___m_isNonBreakingSpace_97 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_98() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isIgnoringAlignment_98)); }
	inline bool get_m_isIgnoringAlignment_98() const { return ___m_isIgnoringAlignment_98; }
	inline bool* get_address_of_m_isIgnoringAlignment_98() { return &___m_isIgnoringAlignment_98; }
	inline void set_m_isIgnoringAlignment_98(bool value)
	{
		___m_isIgnoringAlignment_98 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_99() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_wordWrappingRatios_99)); }
	inline float get_m_wordWrappingRatios_99() const { return ___m_wordWrappingRatios_99; }
	inline float* get_address_of_m_wordWrappingRatios_99() { return &___m_wordWrappingRatios_99; }
	inline void set_m_wordWrappingRatios_99(float value)
	{
		___m_wordWrappingRatios_99 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_100() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_overflowMode_100)); }
	inline int32_t get_m_overflowMode_100() const { return ___m_overflowMode_100; }
	inline int32_t* get_address_of_m_overflowMode_100() { return &___m_overflowMode_100; }
	inline void set_m_overflowMode_100(int32_t value)
	{
		___m_overflowMode_100 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_101() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstOverflowCharacterIndex_101)); }
	inline int32_t get_m_firstOverflowCharacterIndex_101() const { return ___m_firstOverflowCharacterIndex_101; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_101() { return &___m_firstOverflowCharacterIndex_101; }
	inline void set_m_firstOverflowCharacterIndex_101(int32_t value)
	{
		___m_firstOverflowCharacterIndex_101 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_102() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_linkedTextComponent_102)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_linkedTextComponent_102() const { return ___m_linkedTextComponent_102; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_linkedTextComponent_102() { return &___m_linkedTextComponent_102; }
	inline void set_m_linkedTextComponent_102(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_linkedTextComponent_102 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_102), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_103() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isLinkedTextComponent_103)); }
	inline bool get_m_isLinkedTextComponent_103() const { return ___m_isLinkedTextComponent_103; }
	inline bool* get_address_of_m_isLinkedTextComponent_103() { return &___m_isLinkedTextComponent_103; }
	inline void set_m_isLinkedTextComponent_103(bool value)
	{
		___m_isLinkedTextComponent_103 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_104() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isTextTruncated_104)); }
	inline bool get_m_isTextTruncated_104() const { return ___m_isTextTruncated_104; }
	inline bool* get_address_of_m_isTextTruncated_104() { return &___m_isTextTruncated_104; }
	inline void set_m_isTextTruncated_104(bool value)
	{
		___m_isTextTruncated_104 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_105() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableKerning_105)); }
	inline bool get_m_enableKerning_105() const { return ___m_enableKerning_105; }
	inline bool* get_address_of_m_enableKerning_105() { return &___m_enableKerning_105; }
	inline void set_m_enableKerning_105(bool value)
	{
		___m_enableKerning_105 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_106() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableExtraPadding_106)); }
	inline bool get_m_enableExtraPadding_106() const { return ___m_enableExtraPadding_106; }
	inline bool* get_address_of_m_enableExtraPadding_106() { return &___m_enableExtraPadding_106; }
	inline void set_m_enableExtraPadding_106(bool value)
	{
		___m_enableExtraPadding_106 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_107() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___checkPaddingRequired_107)); }
	inline bool get_checkPaddingRequired_107() const { return ___checkPaddingRequired_107; }
	inline bool* get_address_of_checkPaddingRequired_107() { return &___checkPaddingRequired_107; }
	inline void set_checkPaddingRequired_107(bool value)
	{
		___checkPaddingRequired_107 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_108() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isRichText_108)); }
	inline bool get_m_isRichText_108() const { return ___m_isRichText_108; }
	inline bool* get_address_of_m_isRichText_108() { return &___m_isRichText_108; }
	inline void set_m_isRichText_108(bool value)
	{
		___m_isRichText_108 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_109() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_parseCtrlCharacters_109)); }
	inline bool get_m_parseCtrlCharacters_109() const { return ___m_parseCtrlCharacters_109; }
	inline bool* get_address_of_m_parseCtrlCharacters_109() { return &___m_parseCtrlCharacters_109; }
	inline void set_m_parseCtrlCharacters_109(bool value)
	{
		___m_parseCtrlCharacters_109 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_110() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isOverlay_110)); }
	inline bool get_m_isOverlay_110() const { return ___m_isOverlay_110; }
	inline bool* get_address_of_m_isOverlay_110() { return &___m_isOverlay_110; }
	inline void set_m_isOverlay_110(bool value)
	{
		___m_isOverlay_110 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_111() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isOrthographic_111)); }
	inline bool get_m_isOrthographic_111() const { return ___m_isOrthographic_111; }
	inline bool* get_address_of_m_isOrthographic_111() { return &___m_isOrthographic_111; }
	inline void set_m_isOrthographic_111(bool value)
	{
		___m_isOrthographic_111 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_112() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCullingEnabled_112)); }
	inline bool get_m_isCullingEnabled_112() const { return ___m_isCullingEnabled_112; }
	inline bool* get_address_of_m_isCullingEnabled_112() { return &___m_isCullingEnabled_112; }
	inline void set_m_isCullingEnabled_112(bool value)
	{
		___m_isCullingEnabled_112 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_113() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreRectMaskCulling_113)); }
	inline bool get_m_ignoreRectMaskCulling_113() const { return ___m_ignoreRectMaskCulling_113; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_113() { return &___m_ignoreRectMaskCulling_113; }
	inline void set_m_ignoreRectMaskCulling_113(bool value)
	{
		___m_ignoreRectMaskCulling_113 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_114() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreCulling_114)); }
	inline bool get_m_ignoreCulling_114() const { return ___m_ignoreCulling_114; }
	inline bool* get_address_of_m_ignoreCulling_114() { return &___m_ignoreCulling_114; }
	inline void set_m_ignoreCulling_114(bool value)
	{
		___m_ignoreCulling_114 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_115() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_horizontalMapping_115)); }
	inline int32_t get_m_horizontalMapping_115() const { return ___m_horizontalMapping_115; }
	inline int32_t* get_address_of_m_horizontalMapping_115() { return &___m_horizontalMapping_115; }
	inline void set_m_horizontalMapping_115(int32_t value)
	{
		___m_horizontalMapping_115 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_116() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_verticalMapping_116)); }
	inline int32_t get_m_verticalMapping_116() const { return ___m_verticalMapping_116; }
	inline int32_t* get_address_of_m_verticalMapping_116() { return &___m_verticalMapping_116; }
	inline void set_m_verticalMapping_116(int32_t value)
	{
		___m_verticalMapping_116 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_117() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_uvLineOffset_117)); }
	inline float get_m_uvLineOffset_117() const { return ___m_uvLineOffset_117; }
	inline float* get_address_of_m_uvLineOffset_117() { return &___m_uvLineOffset_117; }
	inline void set_m_uvLineOffset_117(float value)
	{
		___m_uvLineOffset_117 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_118() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderMode_118)); }
	inline int32_t get_m_renderMode_118() const { return ___m_renderMode_118; }
	inline int32_t* get_address_of_m_renderMode_118() { return &___m_renderMode_118; }
	inline void set_m_renderMode_118(int32_t value)
	{
		___m_renderMode_118 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_119() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_geometrySortingOrder_119)); }
	inline int32_t get_m_geometrySortingOrder_119() const { return ___m_geometrySortingOrder_119; }
	inline int32_t* get_address_of_m_geometrySortingOrder_119() { return &___m_geometrySortingOrder_119; }
	inline void set_m_geometrySortingOrder_119(int32_t value)
	{
		___m_geometrySortingOrder_119 = value;
	}

	inline static int32_t get_offset_of_m_VertexBufferAutoSizeReduction_120() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_VertexBufferAutoSizeReduction_120)); }
	inline bool get_m_VertexBufferAutoSizeReduction_120() const { return ___m_VertexBufferAutoSizeReduction_120; }
	inline bool* get_address_of_m_VertexBufferAutoSizeReduction_120() { return &___m_VertexBufferAutoSizeReduction_120; }
	inline void set_m_VertexBufferAutoSizeReduction_120(bool value)
	{
		___m_VertexBufferAutoSizeReduction_120 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_121() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstVisibleCharacter_121)); }
	inline int32_t get_m_firstVisibleCharacter_121() const { return ___m_firstVisibleCharacter_121; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_121() { return &___m_firstVisibleCharacter_121; }
	inline void set_m_firstVisibleCharacter_121(int32_t value)
	{
		___m_firstVisibleCharacter_121 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_122() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleCharacters_122)); }
	inline int32_t get_m_maxVisibleCharacters_122() const { return ___m_maxVisibleCharacters_122; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_122() { return &___m_maxVisibleCharacters_122; }
	inline void set_m_maxVisibleCharacters_122(int32_t value)
	{
		___m_maxVisibleCharacters_122 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_123() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleWords_123)); }
	inline int32_t get_m_maxVisibleWords_123() const { return ___m_maxVisibleWords_123; }
	inline int32_t* get_address_of_m_maxVisibleWords_123() { return &___m_maxVisibleWords_123; }
	inline void set_m_maxVisibleWords_123(int32_t value)
	{
		___m_maxVisibleWords_123 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_124() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleLines_124)); }
	inline int32_t get_m_maxVisibleLines_124() const { return ___m_maxVisibleLines_124; }
	inline int32_t* get_address_of_m_maxVisibleLines_124() { return &___m_maxVisibleLines_124; }
	inline void set_m_maxVisibleLines_124(int32_t value)
	{
		___m_maxVisibleLines_124 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_125() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_useMaxVisibleDescender_125)); }
	inline bool get_m_useMaxVisibleDescender_125() const { return ___m_useMaxVisibleDescender_125; }
	inline bool* get_address_of_m_useMaxVisibleDescender_125() { return &___m_useMaxVisibleDescender_125; }
	inline void set_m_useMaxVisibleDescender_125(bool value)
	{
		___m_useMaxVisibleDescender_125 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_126() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_pageToDisplay_126)); }
	inline int32_t get_m_pageToDisplay_126() const { return ___m_pageToDisplay_126; }
	inline int32_t* get_address_of_m_pageToDisplay_126() { return &___m_pageToDisplay_126; }
	inline void set_m_pageToDisplay_126(int32_t value)
	{
		___m_pageToDisplay_126 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_127() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isNewPage_127)); }
	inline bool get_m_isNewPage_127() const { return ___m_isNewPage_127; }
	inline bool* get_address_of_m_isNewPage_127() { return &___m_isNewPage_127; }
	inline void set_m_isNewPage_127(bool value)
	{
		___m_isNewPage_127 = value;
	}

	inline static int32_t get_offset_of_m_margin_128() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_margin_128)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_margin_128() const { return ___m_margin_128; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_margin_128() { return &___m_margin_128; }
	inline void set_m_margin_128(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_margin_128 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_129() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginLeft_129)); }
	inline float get_m_marginLeft_129() const { return ___m_marginLeft_129; }
	inline float* get_address_of_m_marginLeft_129() { return &___m_marginLeft_129; }
	inline void set_m_marginLeft_129(float value)
	{
		___m_marginLeft_129 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_130() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginRight_130)); }
	inline float get_m_marginRight_130() const { return ___m_marginRight_130; }
	inline float* get_address_of_m_marginRight_130() { return &___m_marginRight_130; }
	inline void set_m_marginRight_130(float value)
	{
		___m_marginRight_130 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_131() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginWidth_131)); }
	inline float get_m_marginWidth_131() const { return ___m_marginWidth_131; }
	inline float* get_address_of_m_marginWidth_131() { return &___m_marginWidth_131; }
	inline void set_m_marginWidth_131(float value)
	{
		___m_marginWidth_131 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_132() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginHeight_132)); }
	inline float get_m_marginHeight_132() const { return ___m_marginHeight_132; }
	inline float* get_address_of_m_marginHeight_132() { return &___m_marginHeight_132; }
	inline void set_m_marginHeight_132(float value)
	{
		___m_marginHeight_132 = value;
	}

	inline static int32_t get_offset_of_m_width_133() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_width_133)); }
	inline float get_m_width_133() const { return ___m_width_133; }
	inline float* get_address_of_m_width_133() { return &___m_width_133; }
	inline void set_m_width_133(float value)
	{
		___m_width_133 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_134() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textInfo_134)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_m_textInfo_134() const { return ___m_textInfo_134; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_m_textInfo_134() { return &___m_textInfo_134; }
	inline void set_m_textInfo_134(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___m_textInfo_134 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_134), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_135() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_havePropertiesChanged_135)); }
	inline bool get_m_havePropertiesChanged_135() const { return ___m_havePropertiesChanged_135; }
	inline bool* get_address_of_m_havePropertiesChanged_135() { return &___m_havePropertiesChanged_135; }
	inline void set_m_havePropertiesChanged_135(bool value)
	{
		___m_havePropertiesChanged_135 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_136() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isUsingLegacyAnimationComponent_136)); }
	inline bool get_m_isUsingLegacyAnimationComponent_136() const { return ___m_isUsingLegacyAnimationComponent_136; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_136() { return &___m_isUsingLegacyAnimationComponent_136; }
	inline void set_m_isUsingLegacyAnimationComponent_136(bool value)
	{
		___m_isUsingLegacyAnimationComponent_136 = value;
	}

	inline static int32_t get_offset_of_m_transform_137() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_transform_137)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_transform_137() const { return ___m_transform_137; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_transform_137() { return &___m_transform_137; }
	inline void set_m_transform_137(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_transform_137 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_137), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_138() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_rectTransform_138)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_138() const { return ___m_rectTransform_138; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_138() { return &___m_rectTransform_138; }
	inline void set_m_rectTransform_138(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_138 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_138), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_139() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___U3CautoSizeTextContainerU3Ek__BackingField_139)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_139() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_139; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_139() { return &___U3CautoSizeTextContainerU3Ek__BackingField_139; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_139(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_139 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_140() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_autoSizeTextContainer_140)); }
	inline bool get_m_autoSizeTextContainer_140() const { return ___m_autoSizeTextContainer_140; }
	inline bool* get_address_of_m_autoSizeTextContainer_140() { return &___m_autoSizeTextContainer_140; }
	inline void set_m_autoSizeTextContainer_140(bool value)
	{
		___m_autoSizeTextContainer_140 = value;
	}

	inline static int32_t get_offset_of_m_mesh_141() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_mesh_141)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_141() const { return ___m_mesh_141; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_141() { return &___m_mesh_141; }
	inline void set_m_mesh_141(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_141 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_141), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_142() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isVolumetricText_142)); }
	inline bool get_m_isVolumetricText_142() const { return ___m_isVolumetricText_142; }
	inline bool* get_address_of_m_isVolumetricText_142() { return &___m_isVolumetricText_142; }
	inline void set_m_isVolumetricText_142(bool value)
	{
		___m_isVolumetricText_142 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_143() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAnimator_143)); }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * get_m_spriteAnimator_143() const { return ___m_spriteAnimator_143; }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 ** get_address_of_m_spriteAnimator_143() { return &___m_spriteAnimator_143; }
	inline void set_m_spriteAnimator_143(TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * value)
	{
		___m_spriteAnimator_143 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_143), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_144() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_flexibleHeight_144)); }
	inline float get_m_flexibleHeight_144() const { return ___m_flexibleHeight_144; }
	inline float* get_address_of_m_flexibleHeight_144() { return &___m_flexibleHeight_144; }
	inline void set_m_flexibleHeight_144(float value)
	{
		___m_flexibleHeight_144 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_145() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_flexibleWidth_145)); }
	inline float get_m_flexibleWidth_145() const { return ___m_flexibleWidth_145; }
	inline float* get_address_of_m_flexibleWidth_145() { return &___m_flexibleWidth_145; }
	inline void set_m_flexibleWidth_145(float value)
	{
		___m_flexibleWidth_145 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_146() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minWidth_146)); }
	inline float get_m_minWidth_146() const { return ___m_minWidth_146; }
	inline float* get_address_of_m_minWidth_146() { return &___m_minWidth_146; }
	inline void set_m_minWidth_146(float value)
	{
		___m_minWidth_146 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_147() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minHeight_147)); }
	inline float get_m_minHeight_147() const { return ___m_minHeight_147; }
	inline float* get_address_of_m_minHeight_147() { return &___m_minHeight_147; }
	inline void set_m_minHeight_147(float value)
	{
		___m_minHeight_147 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_148() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxWidth_148)); }
	inline float get_m_maxWidth_148() const { return ___m_maxWidth_148; }
	inline float* get_address_of_m_maxWidth_148() { return &___m_maxWidth_148; }
	inline void set_m_maxWidth_148(float value)
	{
		___m_maxWidth_148 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_149() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxHeight_149)); }
	inline float get_m_maxHeight_149() const { return ___m_maxHeight_149; }
	inline float* get_address_of_m_maxHeight_149() { return &___m_maxHeight_149; }
	inline void set_m_maxHeight_149(float value)
	{
		___m_maxHeight_149 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_150() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_LayoutElement_150)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_m_LayoutElement_150() const { return ___m_LayoutElement_150; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_m_LayoutElement_150() { return &___m_LayoutElement_150; }
	inline void set_m_LayoutElement_150(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___m_LayoutElement_150 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_150), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_151() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_preferredWidth_151)); }
	inline float get_m_preferredWidth_151() const { return ___m_preferredWidth_151; }
	inline float* get_address_of_m_preferredWidth_151() { return &___m_preferredWidth_151; }
	inline void set_m_preferredWidth_151(float value)
	{
		___m_preferredWidth_151 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_152() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderedWidth_152)); }
	inline float get_m_renderedWidth_152() const { return ___m_renderedWidth_152; }
	inline float* get_address_of_m_renderedWidth_152() { return &___m_renderedWidth_152; }
	inline void set_m_renderedWidth_152(float value)
	{
		___m_renderedWidth_152 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_153() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isPreferredWidthDirty_153)); }
	inline bool get_m_isPreferredWidthDirty_153() const { return ___m_isPreferredWidthDirty_153; }
	inline bool* get_address_of_m_isPreferredWidthDirty_153() { return &___m_isPreferredWidthDirty_153; }
	inline void set_m_isPreferredWidthDirty_153(bool value)
	{
		___m_isPreferredWidthDirty_153 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_154() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_preferredHeight_154)); }
	inline float get_m_preferredHeight_154() const { return ___m_preferredHeight_154; }
	inline float* get_address_of_m_preferredHeight_154() { return &___m_preferredHeight_154; }
	inline void set_m_preferredHeight_154(float value)
	{
		___m_preferredHeight_154 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_155() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderedHeight_155)); }
	inline float get_m_renderedHeight_155() const { return ___m_renderedHeight_155; }
	inline float* get_address_of_m_renderedHeight_155() { return &___m_renderedHeight_155; }
	inline void set_m_renderedHeight_155(float value)
	{
		___m_renderedHeight_155 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_156() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isPreferredHeightDirty_156)); }
	inline bool get_m_isPreferredHeightDirty_156() const { return ___m_isPreferredHeightDirty_156; }
	inline bool* get_address_of_m_isPreferredHeightDirty_156() { return &___m_isPreferredHeightDirty_156; }
	inline void set_m_isPreferredHeightDirty_156(bool value)
	{
		___m_isPreferredHeightDirty_156 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_157() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCalculatingPreferredValues_157)); }
	inline bool get_m_isCalculatingPreferredValues_157() const { return ___m_isCalculatingPreferredValues_157; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_157() { return &___m_isCalculatingPreferredValues_157; }
	inline void set_m_isCalculatingPreferredValues_157(bool value)
	{
		___m_isCalculatingPreferredValues_157 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_158() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_recursiveCount_158)); }
	inline int32_t get_m_recursiveCount_158() const { return ___m_recursiveCount_158; }
	inline int32_t* get_address_of_m_recursiveCount_158() { return &___m_recursiveCount_158; }
	inline void set_m_recursiveCount_158(int32_t value)
	{
		___m_recursiveCount_158 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_159() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_layoutPriority_159)); }
	inline int32_t get_m_layoutPriority_159() const { return ___m_layoutPriority_159; }
	inline int32_t* get_address_of_m_layoutPriority_159() { return &___m_layoutPriority_159; }
	inline void set_m_layoutPriority_159(int32_t value)
	{
		___m_layoutPriority_159 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_160() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCalculateSizeRequired_160)); }
	inline bool get_m_isCalculateSizeRequired_160() const { return ___m_isCalculateSizeRequired_160; }
	inline bool* get_address_of_m_isCalculateSizeRequired_160() { return &___m_isCalculateSizeRequired_160; }
	inline void set_m_isCalculateSizeRequired_160(bool value)
	{
		___m_isCalculateSizeRequired_160 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_161() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isLayoutDirty_161)); }
	inline bool get_m_isLayoutDirty_161() const { return ___m_isLayoutDirty_161; }
	inline bool* get_address_of_m_isLayoutDirty_161() { return &___m_isLayoutDirty_161; }
	inline void set_m_isLayoutDirty_161(bool value)
	{
		___m_isLayoutDirty_161 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_162() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_verticesAlreadyDirty_162)); }
	inline bool get_m_verticesAlreadyDirty_162() const { return ___m_verticesAlreadyDirty_162; }
	inline bool* get_address_of_m_verticesAlreadyDirty_162() { return &___m_verticesAlreadyDirty_162; }
	inline void set_m_verticesAlreadyDirty_162(bool value)
	{
		___m_verticesAlreadyDirty_162 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_163() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_layoutAlreadyDirty_163)); }
	inline bool get_m_layoutAlreadyDirty_163() const { return ___m_layoutAlreadyDirty_163; }
	inline bool* get_address_of_m_layoutAlreadyDirty_163() { return &___m_layoutAlreadyDirty_163; }
	inline void set_m_layoutAlreadyDirty_163(bool value)
	{
		___m_layoutAlreadyDirty_163 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_164() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isAwake_164)); }
	inline bool get_m_isAwake_164() const { return ___m_isAwake_164; }
	inline bool* get_address_of_m_isAwake_164() { return &___m_isAwake_164; }
	inline void set_m_isAwake_164(bool value)
	{
		___m_isAwake_164 = value;
	}

	inline static int32_t get_offset_of_m_isWaitingOnResourceLoad_165() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isWaitingOnResourceLoad_165)); }
	inline bool get_m_isWaitingOnResourceLoad_165() const { return ___m_isWaitingOnResourceLoad_165; }
	inline bool* get_address_of_m_isWaitingOnResourceLoad_165() { return &___m_isWaitingOnResourceLoad_165; }
	inline void set_m_isWaitingOnResourceLoad_165(bool value)
	{
		___m_isWaitingOnResourceLoad_165 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_166() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isInputParsingRequired_166)); }
	inline bool get_m_isInputParsingRequired_166() const { return ___m_isInputParsingRequired_166; }
	inline bool* get_address_of_m_isInputParsingRequired_166() { return &___m_isInputParsingRequired_166; }
	inline void set_m_isInputParsingRequired_166(bool value)
	{
		___m_isInputParsingRequired_166 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_167() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_inputSource_167)); }
	inline int32_t get_m_inputSource_167() const { return ___m_inputSource_167; }
	inline int32_t* get_address_of_m_inputSource_167() { return &___m_inputSource_167; }
	inline void set_m_inputSource_167(int32_t value)
	{
		___m_inputSource_167 = value;
	}

	inline static int32_t get_offset_of_old_text_168() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___old_text_168)); }
	inline String_t* get_old_text_168() const { return ___old_text_168; }
	inline String_t** get_address_of_old_text_168() { return &___old_text_168; }
	inline void set_old_text_168(String_t* value)
	{
		___old_text_168 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_168), value);
	}

	inline static int32_t get_offset_of_m_fontScale_169() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontScale_169)); }
	inline float get_m_fontScale_169() const { return ___m_fontScale_169; }
	inline float* get_address_of_m_fontScale_169() { return &___m_fontScale_169; }
	inline void set_m_fontScale_169(float value)
	{
		___m_fontScale_169 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_170() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontScaleMultiplier_170)); }
	inline float get_m_fontScaleMultiplier_170() const { return ___m_fontScaleMultiplier_170; }
	inline float* get_address_of_m_fontScaleMultiplier_170() { return &___m_fontScaleMultiplier_170; }
	inline void set_m_fontScaleMultiplier_170(float value)
	{
		___m_fontScaleMultiplier_170 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_171() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_htmlTag_171)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_htmlTag_171() const { return ___m_htmlTag_171; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_htmlTag_171() { return &___m_htmlTag_171; }
	inline void set_m_htmlTag_171(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_htmlTag_171 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_171), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_172() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_xmlAttribute_172)); }
	inline RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* get_m_xmlAttribute_172() const { return ___m_xmlAttribute_172; }
	inline RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652** get_address_of_m_xmlAttribute_172() { return &___m_xmlAttribute_172; }
	inline void set_m_xmlAttribute_172(RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* value)
	{
		___m_xmlAttribute_172 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_172), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_173() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_attributeParameterValues_173)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_attributeParameterValues_173() const { return ___m_attributeParameterValues_173; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_attributeParameterValues_173() { return &___m_attributeParameterValues_173; }
	inline void set_m_attributeParameterValues_173(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_attributeParameterValues_173 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_173), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_174() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_LineIndent_174)); }
	inline float get_tag_LineIndent_174() const { return ___tag_LineIndent_174; }
	inline float* get_address_of_tag_LineIndent_174() { return &___tag_LineIndent_174; }
	inline void set_tag_LineIndent_174(float value)
	{
		___tag_LineIndent_174 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_175() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_Indent_175)); }
	inline float get_tag_Indent_175() const { return ___tag_Indent_175; }
	inline float* get_address_of_tag_Indent_175() { return &___tag_Indent_175; }
	inline void set_tag_Indent_175(float value)
	{
		___tag_Indent_175 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_176() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_indentStack_176)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_m_indentStack_176() const { return ___m_indentStack_176; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_m_indentStack_176() { return &___m_indentStack_176; }
	inline void set_m_indentStack_176(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___m_indentStack_176 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_177() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_NoParsing_177)); }
	inline bool get_tag_NoParsing_177() const { return ___tag_NoParsing_177; }
	inline bool* get_address_of_tag_NoParsing_177() { return &___tag_NoParsing_177; }
	inline void set_tag_NoParsing_177(bool value)
	{
		___tag_NoParsing_177 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_178() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isParsingText_178)); }
	inline bool get_m_isParsingText_178() const { return ___m_isParsingText_178; }
	inline bool* get_address_of_m_isParsingText_178() { return &___m_isParsingText_178; }
	inline void set_m_isParsingText_178(bool value)
	{
		___m_isParsingText_178 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_179() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FXMatrix_179)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_FXMatrix_179() const { return ___m_FXMatrix_179; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_FXMatrix_179() { return &___m_FXMatrix_179; }
	inline void set_m_FXMatrix_179(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_FXMatrix_179 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_180() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isFXMatrixSet_180)); }
	inline bool get_m_isFXMatrixSet_180() const { return ___m_isFXMatrixSet_180; }
	inline bool* get_address_of_m_isFXMatrixSet_180() { return &___m_isFXMatrixSet_180; }
	inline void set_m_isFXMatrixSet_180(bool value)
	{
		___m_isFXMatrixSet_180 = value;
	}

	inline static int32_t get_offset_of_m_TextParsingBuffer_181() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_TextParsingBuffer_181)); }
	inline UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* get_m_TextParsingBuffer_181() const { return ___m_TextParsingBuffer_181; }
	inline UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505** get_address_of_m_TextParsingBuffer_181() { return &___m_TextParsingBuffer_181; }
	inline void set_m_TextParsingBuffer_181(UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* value)
	{
		___m_TextParsingBuffer_181 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextParsingBuffer_181), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_182() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_internalCharacterInfo_182)); }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* get_m_internalCharacterInfo_182() const { return ___m_internalCharacterInfo_182; }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604** get_address_of_m_internalCharacterInfo_182() { return &___m_internalCharacterInfo_182; }
	inline void set_m_internalCharacterInfo_182(TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* value)
	{
		___m_internalCharacterInfo_182 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_182), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_183() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_input_CharArray_183)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_input_CharArray_183() const { return ___m_input_CharArray_183; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_input_CharArray_183() { return &___m_input_CharArray_183; }
	inline void set_m_input_CharArray_183(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_input_CharArray_183 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_183), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_184() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charArray_Length_184)); }
	inline int32_t get_m_charArray_Length_184() const { return ___m_charArray_Length_184; }
	inline int32_t* get_address_of_m_charArray_Length_184() { return &___m_charArray_Length_184; }
	inline void set_m_charArray_Length_184(int32_t value)
	{
		___m_charArray_Length_184 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_185() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_totalCharacterCount_185)); }
	inline int32_t get_m_totalCharacterCount_185() const { return ___m_totalCharacterCount_185; }
	inline int32_t* get_address_of_m_totalCharacterCount_185() { return &___m_totalCharacterCount_185; }
	inline void set_m_totalCharacterCount_185(int32_t value)
	{
		___m_totalCharacterCount_185 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_186() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_SavedWordWrapState_186)); }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  get_m_SavedWordWrapState_186() const { return ___m_SavedWordWrapState_186; }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 * get_address_of_m_SavedWordWrapState_186() { return &___m_SavedWordWrapState_186; }
	inline void set_m_SavedWordWrapState_186(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  value)
	{
		___m_SavedWordWrapState_186 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_187() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_SavedLineState_187)); }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  get_m_SavedLineState_187() const { return ___m_SavedLineState_187; }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 * get_address_of_m_SavedLineState_187() { return &___m_SavedLineState_187; }
	inline void set_m_SavedLineState_187(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  value)
	{
		___m_SavedLineState_187 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_188() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_characterCount_188)); }
	inline int32_t get_m_characterCount_188() const { return ___m_characterCount_188; }
	inline int32_t* get_address_of_m_characterCount_188() { return &___m_characterCount_188; }
	inline void set_m_characterCount_188(int32_t value)
	{
		___m_characterCount_188 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_189() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstCharacterOfLine_189)); }
	inline int32_t get_m_firstCharacterOfLine_189() const { return ___m_firstCharacterOfLine_189; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_189() { return &___m_firstCharacterOfLine_189; }
	inline void set_m_firstCharacterOfLine_189(int32_t value)
	{
		___m_firstCharacterOfLine_189 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_190() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstVisibleCharacterOfLine_190)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_190() const { return ___m_firstVisibleCharacterOfLine_190; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_190() { return &___m_firstVisibleCharacterOfLine_190; }
	inline void set_m_firstVisibleCharacterOfLine_190(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_190 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_191() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lastCharacterOfLine_191)); }
	inline int32_t get_m_lastCharacterOfLine_191() const { return ___m_lastCharacterOfLine_191; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_191() { return &___m_lastCharacterOfLine_191; }
	inline void set_m_lastCharacterOfLine_191(int32_t value)
	{
		___m_lastCharacterOfLine_191 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_192() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lastVisibleCharacterOfLine_192)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_192() const { return ___m_lastVisibleCharacterOfLine_192; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_192() { return &___m_lastVisibleCharacterOfLine_192; }
	inline void set_m_lastVisibleCharacterOfLine_192(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_192 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_193() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineNumber_193)); }
	inline int32_t get_m_lineNumber_193() const { return ___m_lineNumber_193; }
	inline int32_t* get_address_of_m_lineNumber_193() { return &___m_lineNumber_193; }
	inline void set_m_lineNumber_193(int32_t value)
	{
		___m_lineNumber_193 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_194() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineVisibleCharacterCount_194)); }
	inline int32_t get_m_lineVisibleCharacterCount_194() const { return ___m_lineVisibleCharacterCount_194; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_194() { return &___m_lineVisibleCharacterCount_194; }
	inline void set_m_lineVisibleCharacterCount_194(int32_t value)
	{
		___m_lineVisibleCharacterCount_194 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_195() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_pageNumber_195)); }
	inline int32_t get_m_pageNumber_195() const { return ___m_pageNumber_195; }
	inline int32_t* get_address_of_m_pageNumber_195() { return &___m_pageNumber_195; }
	inline void set_m_pageNumber_195(int32_t value)
	{
		___m_pageNumber_195 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxAscender_196)); }
	inline float get_m_maxAscender_196() const { return ___m_maxAscender_196; }
	inline float* get_address_of_m_maxAscender_196() { return &___m_maxAscender_196; }
	inline void set_m_maxAscender_196(float value)
	{
		___m_maxAscender_196 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_197() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxCapHeight_197)); }
	inline float get_m_maxCapHeight_197() const { return ___m_maxCapHeight_197; }
	inline float* get_address_of_m_maxCapHeight_197() { return &___m_maxCapHeight_197; }
	inline void set_m_maxCapHeight_197(float value)
	{
		___m_maxCapHeight_197 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_198() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxDescender_198)); }
	inline float get_m_maxDescender_198() const { return ___m_maxDescender_198; }
	inline float* get_address_of_m_maxDescender_198() { return &___m_maxDescender_198; }
	inline void set_m_maxDescender_198(float value)
	{
		___m_maxDescender_198 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_199() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxLineAscender_199)); }
	inline float get_m_maxLineAscender_199() const { return ___m_maxLineAscender_199; }
	inline float* get_address_of_m_maxLineAscender_199() { return &___m_maxLineAscender_199; }
	inline void set_m_maxLineAscender_199(float value)
	{
		___m_maxLineAscender_199 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_200() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxLineDescender_200)); }
	inline float get_m_maxLineDescender_200() const { return ___m_maxLineDescender_200; }
	inline float* get_address_of_m_maxLineDescender_200() { return &___m_maxLineDescender_200; }
	inline void set_m_maxLineDescender_200(float value)
	{
		___m_maxLineDescender_200 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_201() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_startOfLineAscender_201)); }
	inline float get_m_startOfLineAscender_201() const { return ___m_startOfLineAscender_201; }
	inline float* get_address_of_m_startOfLineAscender_201() { return &___m_startOfLineAscender_201; }
	inline void set_m_startOfLineAscender_201(float value)
	{
		___m_startOfLineAscender_201 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_202() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineOffset_202)); }
	inline float get_m_lineOffset_202() const { return ___m_lineOffset_202; }
	inline float* get_address_of_m_lineOffset_202() { return &___m_lineOffset_202; }
	inline void set_m_lineOffset_202(float value)
	{
		___m_lineOffset_202 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_203() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_meshExtents_203)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_m_meshExtents_203() const { return ___m_meshExtents_203; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_m_meshExtents_203() { return &___m_meshExtents_203; }
	inline void set_m_meshExtents_203(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___m_meshExtents_203 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_204() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_htmlColor_204)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_htmlColor_204() const { return ___m_htmlColor_204; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_htmlColor_204() { return &___m_htmlColor_204; }
	inline void set_m_htmlColor_204(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_htmlColor_204 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorStack_205)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_colorStack_205() const { return ___m_colorStack_205; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_colorStack_205() { return &___m_colorStack_205; }
	inline void set_m_colorStack_205(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_colorStack_205 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_206() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_underlineColorStack_206)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_underlineColorStack_206() const { return ___m_underlineColorStack_206; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_underlineColorStack_206() { return &___m_underlineColorStack_206; }
	inline void set_m_underlineColorStack_206(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_underlineColorStack_206 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_207() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_strikethroughColorStack_207)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_strikethroughColorStack_207() const { return ___m_strikethroughColorStack_207; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_strikethroughColorStack_207() { return &___m_strikethroughColorStack_207; }
	inline void set_m_strikethroughColorStack_207(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_strikethroughColorStack_207 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightColorStack_208)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_highlightColorStack_208() const { return ___m_highlightColorStack_208; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_highlightColorStack_208() { return &___m_highlightColorStack_208; }
	inline void set_m_highlightColorStack_208(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_highlightColorStack_208 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_209() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorGradientPreset_209)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_colorGradientPreset_209() const { return ___m_colorGradientPreset_209; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_colorGradientPreset_209() { return &___m_colorGradientPreset_209; }
	inline void set_m_colorGradientPreset_209(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_colorGradientPreset_209 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_209), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_210() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorGradientStack_210)); }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  get_m_colorGradientStack_210() const { return ___m_colorGradientStack_210; }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D * get_address_of_m_colorGradientStack_210() { return &___m_colorGradientStack_210; }
	inline void set_m_colorGradientStack_210(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  value)
	{
		___m_colorGradientStack_210 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_211() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tabSpacing_211)); }
	inline float get_m_tabSpacing_211() const { return ___m_tabSpacing_211; }
	inline float* get_address_of_m_tabSpacing_211() { return &___m_tabSpacing_211; }
	inline void set_m_tabSpacing_211(float value)
	{
		___m_tabSpacing_211 = value;
	}

	inline static int32_t get_offset_of_m_spacing_212() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spacing_212)); }
	inline float get_m_spacing_212() const { return ___m_spacing_212; }
	inline float* get_address_of_m_spacing_212() { return &___m_spacing_212; }
	inline void set_m_spacing_212(float value)
	{
		___m_spacing_212 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_213() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_styleStack_213)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_m_styleStack_213() const { return ___m_styleStack_213; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_m_styleStack_213() { return &___m_styleStack_213; }
	inline void set_m_styleStack_213(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___m_styleStack_213 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_214() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_actionStack_214)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_m_actionStack_214() const { return ___m_actionStack_214; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_m_actionStack_214() { return &___m_actionStack_214; }
	inline void set_m_actionStack_214(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___m_actionStack_214 = value;
	}

	inline static int32_t get_offset_of_m_padding_215() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_padding_215)); }
	inline float get_m_padding_215() const { return ___m_padding_215; }
	inline float* get_address_of_m_padding_215() { return &___m_padding_215; }
	inline void set_m_padding_215(float value)
	{
		___m_padding_215 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_216() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_baselineOffset_216)); }
	inline float get_m_baselineOffset_216() const { return ___m_baselineOffset_216; }
	inline float* get_address_of_m_baselineOffset_216() { return &___m_baselineOffset_216; }
	inline void set_m_baselineOffset_216(float value)
	{
		___m_baselineOffset_216 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_217() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_baselineOffsetStack_217)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_m_baselineOffsetStack_217() const { return ___m_baselineOffsetStack_217; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_m_baselineOffsetStack_217() { return &___m_baselineOffsetStack_217; }
	inline void set_m_baselineOffsetStack_217(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___m_baselineOffsetStack_217 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_218() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_xAdvance_218)); }
	inline float get_m_xAdvance_218() const { return ___m_xAdvance_218; }
	inline float* get_address_of_m_xAdvance_218() { return &___m_xAdvance_218; }
	inline void set_m_xAdvance_218(float value)
	{
		___m_xAdvance_218 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_219() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textElementType_219)); }
	inline int32_t get_m_textElementType_219() const { return ___m_textElementType_219; }
	inline int32_t* get_address_of_m_textElementType_219() { return &___m_textElementType_219; }
	inline void set_m_textElementType_219(int32_t value)
	{
		___m_textElementType_219 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_220() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_TextElement_220)); }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * get_m_cached_TextElement_220() const { return ___m_cached_TextElement_220; }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 ** get_address_of_m_cached_TextElement_220() { return &___m_cached_TextElement_220; }
	inline void set_m_cached_TextElement_220(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * value)
	{
		___m_cached_TextElement_220 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_220), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_Character_221() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_Underline_Character_221)); }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D * get_m_cached_Underline_Character_221() const { return ___m_cached_Underline_Character_221; }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D ** get_address_of_m_cached_Underline_Character_221() { return &___m_cached_Underline_Character_221; }
	inline void set_m_cached_Underline_Character_221(TMP_Character_t1875AACA978396521498D6A699052C187903553D * value)
	{
		___m_cached_Underline_Character_221 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_Character_221), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_Character_222() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_Ellipsis_Character_222)); }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D * get_m_cached_Ellipsis_Character_222() const { return ___m_cached_Ellipsis_Character_222; }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D ** get_address_of_m_cached_Ellipsis_Character_222() { return &___m_cached_Ellipsis_Character_222; }
	inline void set_m_cached_Ellipsis_Character_222(TMP_Character_t1875AACA978396521498D6A699052C187903553D * value)
	{
		___m_cached_Ellipsis_Character_222 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_Character_222), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_223() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_defaultSpriteAsset_223)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_defaultSpriteAsset_223() const { return ___m_defaultSpriteAsset_223; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_defaultSpriteAsset_223() { return &___m_defaultSpriteAsset_223; }
	inline void set_m_defaultSpriteAsset_223(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_defaultSpriteAsset_223 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_223), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_224() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentSpriteAsset_224)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_currentSpriteAsset_224() const { return ___m_currentSpriteAsset_224; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_currentSpriteAsset_224() { return &___m_currentSpriteAsset_224; }
	inline void set_m_currentSpriteAsset_224(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_currentSpriteAsset_224 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_224), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_225() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteCount_225)); }
	inline int32_t get_m_spriteCount_225() const { return ___m_spriteCount_225; }
	inline int32_t* get_address_of_m_spriteCount_225() { return &___m_spriteCount_225; }
	inline void set_m_spriteCount_225(int32_t value)
	{
		___m_spriteCount_225 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_226() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteIndex_226)); }
	inline int32_t get_m_spriteIndex_226() const { return ___m_spriteIndex_226; }
	inline int32_t* get_address_of_m_spriteIndex_226() { return &___m_spriteIndex_226; }
	inline void set_m_spriteIndex_226(int32_t value)
	{
		___m_spriteIndex_226 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_227() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAnimationID_227)); }
	inline int32_t get_m_spriteAnimationID_227() const { return ___m_spriteAnimationID_227; }
	inline int32_t* get_address_of_m_spriteAnimationID_227() { return &___m_spriteAnimationID_227; }
	inline void set_m_spriteAnimationID_227(int32_t value)
	{
		___m_spriteAnimationID_227 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_228() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreActiveState_228)); }
	inline bool get_m_ignoreActiveState_228() const { return ___m_ignoreActiveState_228; }
	inline bool* get_address_of_m_ignoreActiveState_228() { return &___m_ignoreActiveState_228; }
	inline void set_m_ignoreActiveState_228(bool value)
	{
		___m_ignoreActiveState_228 = value;
	}

	inline static int32_t get_offset_of_k_Power_229() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___k_Power_229)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_k_Power_229() const { return ___k_Power_229; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_k_Power_229() { return &___k_Power_229; }
	inline void set_k_Power_229(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___k_Power_229 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_229), value);
	}
};

struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___s_colorWhite_47;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_LargePositiveVector2_230;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_LargeNegativeVector2_231;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_232;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_233;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_234;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_235;

public:
	inline static int32_t get_offset_of_s_colorWhite_47() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___s_colorWhite_47)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_s_colorWhite_47() const { return ___s_colorWhite_47; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_s_colorWhite_47() { return &___s_colorWhite_47; }
	inline void set_s_colorWhite_47(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___s_colorWhite_47 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_230() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveVector2_230)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_LargePositiveVector2_230() const { return ___k_LargePositiveVector2_230; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_LargePositiveVector2_230() { return &___k_LargePositiveVector2_230; }
	inline void set_k_LargePositiveVector2_230(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_LargePositiveVector2_230 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_231() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeVector2_231)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_LargeNegativeVector2_231() const { return ___k_LargeNegativeVector2_231; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_LargeNegativeVector2_231() { return &___k_LargeNegativeVector2_231; }
	inline void set_k_LargeNegativeVector2_231(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_LargeNegativeVector2_231 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_232() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveFloat_232)); }
	inline float get_k_LargePositiveFloat_232() const { return ___k_LargePositiveFloat_232; }
	inline float* get_address_of_k_LargePositiveFloat_232() { return &___k_LargePositiveFloat_232; }
	inline void set_k_LargePositiveFloat_232(float value)
	{
		___k_LargePositiveFloat_232 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_233() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeFloat_233)); }
	inline float get_k_LargeNegativeFloat_233() const { return ___k_LargeNegativeFloat_233; }
	inline float* get_address_of_k_LargeNegativeFloat_233() { return &___k_LargeNegativeFloat_233; }
	inline void set_k_LargeNegativeFloat_233(float value)
	{
		___k_LargeNegativeFloat_233 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_234() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveInt_234)); }
	inline int32_t get_k_LargePositiveInt_234() const { return ___k_LargePositiveInt_234; }
	inline int32_t* get_address_of_k_LargePositiveInt_234() { return &___k_LargePositiveInt_234; }
	inline void set_k_LargePositiveInt_234(int32_t value)
	{
		___k_LargePositiveInt_234 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_235() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeInt_235)); }
	inline int32_t get_k_LargeNegativeInt_235() const { return ___k_LargeNegativeInt_235; }
	inline int32_t* get_address_of_k_LargeNegativeInt_235() { return &___k_LargeNegativeInt_235; }
	inline void set_k_LargeNegativeInt_235(int32_t value)
	{
		___k_LargeNegativeInt_235 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#ifndef TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#define TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_30)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_32)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_34)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#ifndef TEXTMESHPRO_T6FF60D9DCAF295045FE47C014CC855C5784752E2_H
#define TEXTMESHPRO_T6FF60D9DCAF295045FE47C014CC855C5784752E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshPro
struct  TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2  : public TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7
{
public:
	// System.Boolean TMPro.TextMeshPro::m_currentAutoSizeMode
	bool ___m_currentAutoSizeMode_236;
	// System.Boolean TMPro.TextMeshPro::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_237;
	// System.Single TMPro.TextMeshPro::m_previousLossyScaleY
	float ___m_previousLossyScaleY_238;
	// UnityEngine.Renderer TMPro.TextMeshPro::m_renderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___m_renderer_239;
	// UnityEngine.MeshFilter TMPro.TextMeshPro::m_meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___m_meshFilter_240;
	// System.Boolean TMPro.TextMeshPro::m_isFirstAllocation
	bool ___m_isFirstAllocation_241;
	// System.Int32 TMPro.TextMeshPro::m_max_characters
	int32_t ___m_max_characters_242;
	// System.Int32 TMPro.TextMeshPro::m_max_numberOfLines
	int32_t ___m_max_numberOfLines_243;
	// TMPro.TMP_SubMesh[] TMPro.TextMeshPro::m_subTextObjects
	TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD* ___m_subTextObjects_244;
	// System.Boolean TMPro.TextMeshPro::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_245;
	// System.Boolean TMPro.TextMeshPro::isMaskUpdateRequired
	bool ___isMaskUpdateRequired_246;
	// TMPro.MaskingTypes TMPro.TextMeshPro::m_maskType
	int32_t ___m_maskType_247;
	// UnityEngine.Matrix4x4 TMPro.TextMeshPro::m_EnvMapMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_EnvMapMatrix_248;
	// UnityEngine.Vector3[] TMPro.TextMeshPro::m_RectTransformCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_RectTransformCorners_249;
	// System.Boolean TMPro.TextMeshPro::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_250;
	// System.Int32 TMPro.TextMeshPro::loopCountA
	int32_t ___loopCountA_251;

public:
	inline static int32_t get_offset_of_m_currentAutoSizeMode_236() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_currentAutoSizeMode_236)); }
	inline bool get_m_currentAutoSizeMode_236() const { return ___m_currentAutoSizeMode_236; }
	inline bool* get_address_of_m_currentAutoSizeMode_236() { return &___m_currentAutoSizeMode_236; }
	inline void set_m_currentAutoSizeMode_236(bool value)
	{
		___m_currentAutoSizeMode_236 = value;
	}

	inline static int32_t get_offset_of_m_hasFontAssetChanged_237() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_hasFontAssetChanged_237)); }
	inline bool get_m_hasFontAssetChanged_237() const { return ___m_hasFontAssetChanged_237; }
	inline bool* get_address_of_m_hasFontAssetChanged_237() { return &___m_hasFontAssetChanged_237; }
	inline void set_m_hasFontAssetChanged_237(bool value)
	{
		___m_hasFontAssetChanged_237 = value;
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_238() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_previousLossyScaleY_238)); }
	inline float get_m_previousLossyScaleY_238() const { return ___m_previousLossyScaleY_238; }
	inline float* get_address_of_m_previousLossyScaleY_238() { return &___m_previousLossyScaleY_238; }
	inline void set_m_previousLossyScaleY_238(float value)
	{
		___m_previousLossyScaleY_238 = value;
	}

	inline static int32_t get_offset_of_m_renderer_239() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_renderer_239)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_m_renderer_239() const { return ___m_renderer_239; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_m_renderer_239() { return &___m_renderer_239; }
	inline void set_m_renderer_239(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___m_renderer_239 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_239), value);
	}

	inline static int32_t get_offset_of_m_meshFilter_240() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_meshFilter_240)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_m_meshFilter_240() const { return ___m_meshFilter_240; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_m_meshFilter_240() { return &___m_meshFilter_240; }
	inline void set_m_meshFilter_240(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___m_meshFilter_240 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_240), value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_241() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_isFirstAllocation_241)); }
	inline bool get_m_isFirstAllocation_241() const { return ___m_isFirstAllocation_241; }
	inline bool* get_address_of_m_isFirstAllocation_241() { return &___m_isFirstAllocation_241; }
	inline void set_m_isFirstAllocation_241(bool value)
	{
		___m_isFirstAllocation_241 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_242() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_max_characters_242)); }
	inline int32_t get_m_max_characters_242() const { return ___m_max_characters_242; }
	inline int32_t* get_address_of_m_max_characters_242() { return &___m_max_characters_242; }
	inline void set_m_max_characters_242(int32_t value)
	{
		___m_max_characters_242 = value;
	}

	inline static int32_t get_offset_of_m_max_numberOfLines_243() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_max_numberOfLines_243)); }
	inline int32_t get_m_max_numberOfLines_243() const { return ___m_max_numberOfLines_243; }
	inline int32_t* get_address_of_m_max_numberOfLines_243() { return &___m_max_numberOfLines_243; }
	inline void set_m_max_numberOfLines_243(int32_t value)
	{
		___m_max_numberOfLines_243 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_244() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_subTextObjects_244)); }
	inline TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD* get_m_subTextObjects_244() const { return ___m_subTextObjects_244; }
	inline TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD** get_address_of_m_subTextObjects_244() { return &___m_subTextObjects_244; }
	inline void set_m_subTextObjects_244(TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD* value)
	{
		___m_subTextObjects_244 = value;
		Il2CppCodeGenWriteBarrier((&___m_subTextObjects_244), value);
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_245() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_isMaskingEnabled_245)); }
	inline bool get_m_isMaskingEnabled_245() const { return ___m_isMaskingEnabled_245; }
	inline bool* get_address_of_m_isMaskingEnabled_245() { return &___m_isMaskingEnabled_245; }
	inline void set_m_isMaskingEnabled_245(bool value)
	{
		___m_isMaskingEnabled_245 = value;
	}

	inline static int32_t get_offset_of_isMaskUpdateRequired_246() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___isMaskUpdateRequired_246)); }
	inline bool get_isMaskUpdateRequired_246() const { return ___isMaskUpdateRequired_246; }
	inline bool* get_address_of_isMaskUpdateRequired_246() { return &___isMaskUpdateRequired_246; }
	inline void set_isMaskUpdateRequired_246(bool value)
	{
		___isMaskUpdateRequired_246 = value;
	}

	inline static int32_t get_offset_of_m_maskType_247() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_maskType_247)); }
	inline int32_t get_m_maskType_247() const { return ___m_maskType_247; }
	inline int32_t* get_address_of_m_maskType_247() { return &___m_maskType_247; }
	inline void set_m_maskType_247(int32_t value)
	{
		___m_maskType_247 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_248() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_EnvMapMatrix_248)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_EnvMapMatrix_248() const { return ___m_EnvMapMatrix_248; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_EnvMapMatrix_248() { return &___m_EnvMapMatrix_248; }
	inline void set_m_EnvMapMatrix_248(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_EnvMapMatrix_248 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_249() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_RectTransformCorners_249)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_RectTransformCorners_249() const { return ___m_RectTransformCorners_249; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_RectTransformCorners_249() { return &___m_RectTransformCorners_249; }
	inline void set_m_RectTransformCorners_249(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_RectTransformCorners_249 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformCorners_249), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_250() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_isRegisteredForEvents_250)); }
	inline bool get_m_isRegisteredForEvents_250() const { return ___m_isRegisteredForEvents_250; }
	inline bool* get_address_of_m_isRegisteredForEvents_250() { return &___m_isRegisteredForEvents_250; }
	inline void set_m_isRegisteredForEvents_250(bool value)
	{
		___m_isRegisteredForEvents_250 = value;
	}

	inline static int32_t get_offset_of_loopCountA_251() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___loopCountA_251)); }
	inline int32_t get_loopCountA_251() const { return ___loopCountA_251; }
	inline int32_t* get_address_of_loopCountA_251() { return &___loopCountA_251; }
	inline void set_loopCountA_251(int32_t value)
	{
		___loopCountA_251 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPRO_T6FF60D9DCAF295045FE47C014CC855C5784752E2_H
#ifndef TEXTMESHPROUGUI_TBA60B913AB6151F8563F7078AD67EB6458129438_H
#define TEXTMESHPROUGUI_TBA60B913AB6151F8563F7078AD67EB6458129438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshProUGUI
struct  TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438  : public TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7
{
public:
	// System.Boolean TMPro.TextMeshProUGUI::m_isRebuildingLayout
	bool ___m_isRebuildingLayout_236;
	// System.Boolean TMPro.TextMeshProUGUI::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_237;
	// TMPro.TMP_SubMeshUI[] TMPro.TextMeshProUGUI::m_subTextObjects
	TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E* ___m_subTextObjects_238;
	// System.Single TMPro.TextMeshProUGUI::m_previousLossyScaleY
	float ___m_previousLossyScaleY_239;
	// UnityEngine.Vector3[] TMPro.TextMeshProUGUI::m_RectTransformCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_RectTransformCorners_240;
	// UnityEngine.CanvasRenderer TMPro.TextMeshProUGUI::m_canvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_canvasRenderer_241;
	// UnityEngine.Canvas TMPro.TextMeshProUGUI::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_242;
	// System.Boolean TMPro.TextMeshProUGUI::m_isFirstAllocation
	bool ___m_isFirstAllocation_243;
	// System.Int32 TMPro.TextMeshProUGUI::m_max_characters
	int32_t ___m_max_characters_244;
	// System.Boolean TMPro.TextMeshProUGUI::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_245;
	// UnityEngine.Material TMPro.TextMeshProUGUI::m_baseMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_baseMaterial_246;
	// System.Boolean TMPro.TextMeshProUGUI::m_isScrollRegionSet
	bool ___m_isScrollRegionSet_247;
	// System.Int32 TMPro.TextMeshProUGUI::m_stencilID
	int32_t ___m_stencilID_248;
	// UnityEngine.Vector4 TMPro.TextMeshProUGUI::m_maskOffset
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_maskOffset_249;
	// UnityEngine.Matrix4x4 TMPro.TextMeshProUGUI::m_EnvMapMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_EnvMapMatrix_250;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_251;
	// System.Int32 TMPro.TextMeshProUGUI::m_recursiveCountA
	int32_t ___m_recursiveCountA_252;
	// System.Int32 TMPro.TextMeshProUGUI::loopCountA
	int32_t ___loopCountA_253;

public:
	inline static int32_t get_offset_of_m_isRebuildingLayout_236() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isRebuildingLayout_236)); }
	inline bool get_m_isRebuildingLayout_236() const { return ___m_isRebuildingLayout_236; }
	inline bool* get_address_of_m_isRebuildingLayout_236() { return &___m_isRebuildingLayout_236; }
	inline void set_m_isRebuildingLayout_236(bool value)
	{
		___m_isRebuildingLayout_236 = value;
	}

	inline static int32_t get_offset_of_m_hasFontAssetChanged_237() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_hasFontAssetChanged_237)); }
	inline bool get_m_hasFontAssetChanged_237() const { return ___m_hasFontAssetChanged_237; }
	inline bool* get_address_of_m_hasFontAssetChanged_237() { return &___m_hasFontAssetChanged_237; }
	inline void set_m_hasFontAssetChanged_237(bool value)
	{
		___m_hasFontAssetChanged_237 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_238() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_subTextObjects_238)); }
	inline TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E* get_m_subTextObjects_238() const { return ___m_subTextObjects_238; }
	inline TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E** get_address_of_m_subTextObjects_238() { return &___m_subTextObjects_238; }
	inline void set_m_subTextObjects_238(TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E* value)
	{
		___m_subTextObjects_238 = value;
		Il2CppCodeGenWriteBarrier((&___m_subTextObjects_238), value);
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_239() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_previousLossyScaleY_239)); }
	inline float get_m_previousLossyScaleY_239() const { return ___m_previousLossyScaleY_239; }
	inline float* get_address_of_m_previousLossyScaleY_239() { return &___m_previousLossyScaleY_239; }
	inline void set_m_previousLossyScaleY_239(float value)
	{
		___m_previousLossyScaleY_239 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_240() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_RectTransformCorners_240)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_RectTransformCorners_240() const { return ___m_RectTransformCorners_240; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_RectTransformCorners_240() { return &___m_RectTransformCorners_240; }
	inline void set_m_RectTransformCorners_240(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_RectTransformCorners_240 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformCorners_240), value);
	}

	inline static int32_t get_offset_of_m_canvasRenderer_241() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_canvasRenderer_241)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_canvasRenderer_241() const { return ___m_canvasRenderer_241; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_canvasRenderer_241() { return &___m_canvasRenderer_241; }
	inline void set_m_canvasRenderer_241(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_canvasRenderer_241 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRenderer_241), value);
	}

	inline static int32_t get_offset_of_m_canvas_242() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_canvas_242)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_242() const { return ___m_canvas_242; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_242() { return &___m_canvas_242; }
	inline void set_m_canvas_242(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_242 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_242), value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_243() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isFirstAllocation_243)); }
	inline bool get_m_isFirstAllocation_243() const { return ___m_isFirstAllocation_243; }
	inline bool* get_address_of_m_isFirstAllocation_243() { return &___m_isFirstAllocation_243; }
	inline void set_m_isFirstAllocation_243(bool value)
	{
		___m_isFirstAllocation_243 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_244() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_max_characters_244)); }
	inline int32_t get_m_max_characters_244() const { return ___m_max_characters_244; }
	inline int32_t* get_address_of_m_max_characters_244() { return &___m_max_characters_244; }
	inline void set_m_max_characters_244(int32_t value)
	{
		___m_max_characters_244 = value;
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_245() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isMaskingEnabled_245)); }
	inline bool get_m_isMaskingEnabled_245() const { return ___m_isMaskingEnabled_245; }
	inline bool* get_address_of_m_isMaskingEnabled_245() { return &___m_isMaskingEnabled_245; }
	inline void set_m_isMaskingEnabled_245(bool value)
	{
		___m_isMaskingEnabled_245 = value;
	}

	inline static int32_t get_offset_of_m_baseMaterial_246() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_baseMaterial_246)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_baseMaterial_246() const { return ___m_baseMaterial_246; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_baseMaterial_246() { return &___m_baseMaterial_246; }
	inline void set_m_baseMaterial_246(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_baseMaterial_246 = value;
		Il2CppCodeGenWriteBarrier((&___m_baseMaterial_246), value);
	}

	inline static int32_t get_offset_of_m_isScrollRegionSet_247() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isScrollRegionSet_247)); }
	inline bool get_m_isScrollRegionSet_247() const { return ___m_isScrollRegionSet_247; }
	inline bool* get_address_of_m_isScrollRegionSet_247() { return &___m_isScrollRegionSet_247; }
	inline void set_m_isScrollRegionSet_247(bool value)
	{
		___m_isScrollRegionSet_247 = value;
	}

	inline static int32_t get_offset_of_m_stencilID_248() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_stencilID_248)); }
	inline int32_t get_m_stencilID_248() const { return ___m_stencilID_248; }
	inline int32_t* get_address_of_m_stencilID_248() { return &___m_stencilID_248; }
	inline void set_m_stencilID_248(int32_t value)
	{
		___m_stencilID_248 = value;
	}

	inline static int32_t get_offset_of_m_maskOffset_249() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_maskOffset_249)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_maskOffset_249() const { return ___m_maskOffset_249; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_maskOffset_249() { return &___m_maskOffset_249; }
	inline void set_m_maskOffset_249(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_maskOffset_249 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_250() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_EnvMapMatrix_250)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_EnvMapMatrix_250() const { return ___m_EnvMapMatrix_250; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_EnvMapMatrix_250() { return &___m_EnvMapMatrix_250; }
	inline void set_m_EnvMapMatrix_250(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_EnvMapMatrix_250 = value;
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_251() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isRegisteredForEvents_251)); }
	inline bool get_m_isRegisteredForEvents_251() const { return ___m_isRegisteredForEvents_251; }
	inline bool* get_address_of_m_isRegisteredForEvents_251() { return &___m_isRegisteredForEvents_251; }
	inline void set_m_isRegisteredForEvents_251(bool value)
	{
		___m_isRegisteredForEvents_251 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCountA_252() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_recursiveCountA_252)); }
	inline int32_t get_m_recursiveCountA_252() const { return ___m_recursiveCountA_252; }
	inline int32_t* get_address_of_m_recursiveCountA_252() { return &___m_recursiveCountA_252; }
	inline void set_m_recursiveCountA_252(int32_t value)
	{
		___m_recursiveCountA_252 = value;
	}

	inline static int32_t get_offset_of_loopCountA_253() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___loopCountA_253)); }
	inline int32_t get_loopCountA_253() const { return ___loopCountA_253; }
	inline int32_t* get_address_of_loopCountA_253() { return &___loopCountA_253; }
	inline void set_loopCountA_253(int32_t value)
	{
		___loopCountA_253 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROUGUI_TBA60B913AB6151F8563F7078AD67EB6458129438_H
#ifndef TEXTFXUGUI_TC359CF51C5D5602DFC59E0FEEB18192F97EF4F81_H
#define TEXTFXUGUI_TC359CF51C5D5602DFC59E0FEEB18192F97EF4F81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxUGUI
struct  TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81  : public Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030
{
public:
	// TextFx.TextFxAnimationManager TextFx.TextFxUGUI::m_animation_manager
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * ___m_animation_manager_37;
	// UnityEngine.GameObject TextFx.TextFxUGUI::m_gameobject_reference
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_gameobject_reference_38;
	// System.Boolean TextFx.TextFxUGUI::m_renderToCurve
	bool ___m_renderToCurve_39;
	// TextFx.TextFxBezierCurve TextFx.TextFxUGUI::m_bezierCurve
	TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  ___m_bezierCurve_40;
	// System.Action TextFx.TextFxUGUI::<OnMeshUpdateCall>k__BackingField
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3COnMeshUpdateCallU3Ek__BackingField_41;
	// TextFx.TextFxUGUI_UGUI_MESH_EFFECT_TYPE TextFx.TextFxUGUI::m_effect_type
	int32_t ___m_effect_type_42;
	// UnityEngine.Vector2 TextFx.TextFxUGUI::m_effect_offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_effect_offset_43;
	// UnityEngine.Color TextFx.TextFxUGUI::m_effect_colour
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_effect_colour_44;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> TextFx.TextFxUGUI::m_cachedVerts
	List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ___m_cachedVerts_45;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> TextFx.TextFxUGUI::m_currentMeshVerts
	List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ___m_currentMeshVerts_46;
	// System.Boolean TextFx.TextFxUGUI::m_textFxUpdateGeometryCall
	bool ___m_textFxUpdateGeometryCall_47;
	// System.Boolean TextFx.TextFxUGUI::m_textFxAnimDrawCall
	bool ___m_textFxAnimDrawCall_48;
	// System.Int32 TextFx.TextFxUGUI::_numLetterMeshes
	int32_t ____numLetterMeshes_49;
	// UnityEngine.UIVertex TextFx.TextFxUGUI::_temp_vert
	UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577  ____temp_vert_50;
	// UnityEngine.UIVertex[] TextFx.TextFxUGUI::_uiVertexQuad
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ____uiVertexQuad_51;

public:
	inline static int32_t get_offset_of_m_animation_manager_37() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_animation_manager_37)); }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * get_m_animation_manager_37() const { return ___m_animation_manager_37; }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 ** get_address_of_m_animation_manager_37() { return &___m_animation_manager_37; }
	inline void set_m_animation_manager_37(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * value)
	{
		___m_animation_manager_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_manager_37), value);
	}

	inline static int32_t get_offset_of_m_gameobject_reference_38() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_gameobject_reference_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_gameobject_reference_38() const { return ___m_gameobject_reference_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_gameobject_reference_38() { return &___m_gameobject_reference_38; }
	inline void set_m_gameobject_reference_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_gameobject_reference_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameobject_reference_38), value);
	}

	inline static int32_t get_offset_of_m_renderToCurve_39() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_renderToCurve_39)); }
	inline bool get_m_renderToCurve_39() const { return ___m_renderToCurve_39; }
	inline bool* get_address_of_m_renderToCurve_39() { return &___m_renderToCurve_39; }
	inline void set_m_renderToCurve_39(bool value)
	{
		___m_renderToCurve_39 = value;
	}

	inline static int32_t get_offset_of_m_bezierCurve_40() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_bezierCurve_40)); }
	inline TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  get_m_bezierCurve_40() const { return ___m_bezierCurve_40; }
	inline TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7 * get_address_of_m_bezierCurve_40() { return &___m_bezierCurve_40; }
	inline void set_m_bezierCurve_40(TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  value)
	{
		___m_bezierCurve_40 = value;
	}

	inline static int32_t get_offset_of_U3COnMeshUpdateCallU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___U3COnMeshUpdateCallU3Ek__BackingField_41)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3COnMeshUpdateCallU3Ek__BackingField_41() const { return ___U3COnMeshUpdateCallU3Ek__BackingField_41; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3COnMeshUpdateCallU3Ek__BackingField_41() { return &___U3COnMeshUpdateCallU3Ek__BackingField_41; }
	inline void set_U3COnMeshUpdateCallU3Ek__BackingField_41(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3COnMeshUpdateCallU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnMeshUpdateCallU3Ek__BackingField_41), value);
	}

	inline static int32_t get_offset_of_m_effect_type_42() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_effect_type_42)); }
	inline int32_t get_m_effect_type_42() const { return ___m_effect_type_42; }
	inline int32_t* get_address_of_m_effect_type_42() { return &___m_effect_type_42; }
	inline void set_m_effect_type_42(int32_t value)
	{
		___m_effect_type_42 = value;
	}

	inline static int32_t get_offset_of_m_effect_offset_43() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_effect_offset_43)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_effect_offset_43() const { return ___m_effect_offset_43; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_effect_offset_43() { return &___m_effect_offset_43; }
	inline void set_m_effect_offset_43(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_effect_offset_43 = value;
	}

	inline static int32_t get_offset_of_m_effect_colour_44() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_effect_colour_44)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_effect_colour_44() const { return ___m_effect_colour_44; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_effect_colour_44() { return &___m_effect_colour_44; }
	inline void set_m_effect_colour_44(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_effect_colour_44 = value;
	}

	inline static int32_t get_offset_of_m_cachedVerts_45() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_cachedVerts_45)); }
	inline List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * get_m_cachedVerts_45() const { return ___m_cachedVerts_45; }
	inline List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 ** get_address_of_m_cachedVerts_45() { return &___m_cachedVerts_45; }
	inline void set_m_cachedVerts_45(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * value)
	{
		___m_cachedVerts_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_cachedVerts_45), value);
	}

	inline static int32_t get_offset_of_m_currentMeshVerts_46() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_currentMeshVerts_46)); }
	inline List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * get_m_currentMeshVerts_46() const { return ___m_currentMeshVerts_46; }
	inline List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 ** get_address_of_m_currentMeshVerts_46() { return &___m_currentMeshVerts_46; }
	inline void set_m_currentMeshVerts_46(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * value)
	{
		___m_currentMeshVerts_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMeshVerts_46), value);
	}

	inline static int32_t get_offset_of_m_textFxUpdateGeometryCall_47() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_textFxUpdateGeometryCall_47)); }
	inline bool get_m_textFxUpdateGeometryCall_47() const { return ___m_textFxUpdateGeometryCall_47; }
	inline bool* get_address_of_m_textFxUpdateGeometryCall_47() { return &___m_textFxUpdateGeometryCall_47; }
	inline void set_m_textFxUpdateGeometryCall_47(bool value)
	{
		___m_textFxUpdateGeometryCall_47 = value;
	}

	inline static int32_t get_offset_of_m_textFxAnimDrawCall_48() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ___m_textFxAnimDrawCall_48)); }
	inline bool get_m_textFxAnimDrawCall_48() const { return ___m_textFxAnimDrawCall_48; }
	inline bool* get_address_of_m_textFxAnimDrawCall_48() { return &___m_textFxAnimDrawCall_48; }
	inline void set_m_textFxAnimDrawCall_48(bool value)
	{
		___m_textFxAnimDrawCall_48 = value;
	}

	inline static int32_t get_offset_of__numLetterMeshes_49() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ____numLetterMeshes_49)); }
	inline int32_t get__numLetterMeshes_49() const { return ____numLetterMeshes_49; }
	inline int32_t* get_address_of__numLetterMeshes_49() { return &____numLetterMeshes_49; }
	inline void set__numLetterMeshes_49(int32_t value)
	{
		____numLetterMeshes_49 = value;
	}

	inline static int32_t get_offset_of__temp_vert_50() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ____temp_vert_50)); }
	inline UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577  get__temp_vert_50() const { return ____temp_vert_50; }
	inline UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577 * get_address_of__temp_vert_50() { return &____temp_vert_50; }
	inline void set__temp_vert_50(UIVertex_t0583C35B730B218B542E80203F5F4BC6F1E9E577  value)
	{
		____temp_vert_50 = value;
	}

	inline static int32_t get_offset_of__uiVertexQuad_51() { return static_cast<int32_t>(offsetof(TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81, ____uiVertexQuad_51)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get__uiVertexQuad_51() const { return ____uiVertexQuad_51; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of__uiVertexQuad_51() { return &____uiVertexQuad_51; }
	inline void set__uiVertexQuad_51(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		____uiVertexQuad_51 = value;
		Il2CppCodeGenWriteBarrier((&____uiVertexQuad_51), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXUGUI_TC359CF51C5D5602DFC59E0FEEB18192F97EF4F81_H
#ifndef TEXTFXTEXTMESHPRO_T6F5C0CB616E6AEA5789460FA162A1E9A6268C94C_H
#define TEXTFXTEXTMESHPRO_T6F5C0CB616E6AEA5789460FA162A1E9A6268C94C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxTextMeshPro
struct  TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C  : public TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2
{
public:
	// TextFx.TextFxAnimationManager TextFx.TextFxTextMeshPro::m_animation_manager
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * ___m_animation_manager_252;
	// UnityEngine.GameObject TextFx.TextFxTextMeshPro::m_gameobject_reference
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_gameobject_reference_253;
	// System.Boolean TextFx.TextFxTextMeshPro::m_renderToCurve
	bool ___m_renderToCurve_254;
	// TextFx.TextFxBezierCurve TextFx.TextFxTextMeshPro::m_bezierCurve
	TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  ___m_bezierCurve_255;
	// System.Action TextFx.TextFxTextMeshPro::<OnMeshUpdateCall>k__BackingField
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3COnMeshUpdateCallU3Ek__BackingField_256;
	// UnityEngine.Vector3[] TextFx.TextFxTextMeshPro::m_currentVerts
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_currentVerts_257;
	// UnityEngine.Color32[] TextFx.TextFxTextMeshPro::m_currentColours
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_currentColours_258;
	// System.Boolean TextFx.TextFxTextMeshPro::m_textFxUpdateGeometryCall
	bool ___m_textFxUpdateGeometryCall_259;
	// System.Boolean TextFx.TextFxTextMeshPro::m_textFxAnimDrawCall
	bool ___m_textFxAnimDrawCall_260;
	// System.Boolean TextFx.TextFxTextMeshPro::_forceNativeDrawCall
	bool ____forceNativeDrawCall_261;
	// System.String TextFx.TextFxTextMeshPro::_strippedText
	String_t* ____strippedText_262;
	// System.Int32 TextFx.TextFxTextMeshPro::_numRenderedLetters
	int32_t ____numRenderedLetters_263;
	// UnityEngine.Vector3[] TextFx.TextFxTextMeshPro::_vertsToUse
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____vertsToUse_264;
	// UnityEngine.Color32[] TextFx.TextFxTextMeshPro::_colsToUse
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ____colsToUse_265;

public:
	inline static int32_t get_offset_of_m_animation_manager_252() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ___m_animation_manager_252)); }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * get_m_animation_manager_252() const { return ___m_animation_manager_252; }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 ** get_address_of_m_animation_manager_252() { return &___m_animation_manager_252; }
	inline void set_m_animation_manager_252(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * value)
	{
		___m_animation_manager_252 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_manager_252), value);
	}

	inline static int32_t get_offset_of_m_gameobject_reference_253() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ___m_gameobject_reference_253)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_gameobject_reference_253() const { return ___m_gameobject_reference_253; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_gameobject_reference_253() { return &___m_gameobject_reference_253; }
	inline void set_m_gameobject_reference_253(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_gameobject_reference_253 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameobject_reference_253), value);
	}

	inline static int32_t get_offset_of_m_renderToCurve_254() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ___m_renderToCurve_254)); }
	inline bool get_m_renderToCurve_254() const { return ___m_renderToCurve_254; }
	inline bool* get_address_of_m_renderToCurve_254() { return &___m_renderToCurve_254; }
	inline void set_m_renderToCurve_254(bool value)
	{
		___m_renderToCurve_254 = value;
	}

	inline static int32_t get_offset_of_m_bezierCurve_255() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ___m_bezierCurve_255)); }
	inline TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  get_m_bezierCurve_255() const { return ___m_bezierCurve_255; }
	inline TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7 * get_address_of_m_bezierCurve_255() { return &___m_bezierCurve_255; }
	inline void set_m_bezierCurve_255(TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  value)
	{
		___m_bezierCurve_255 = value;
	}

	inline static int32_t get_offset_of_U3COnMeshUpdateCallU3Ek__BackingField_256() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ___U3COnMeshUpdateCallU3Ek__BackingField_256)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3COnMeshUpdateCallU3Ek__BackingField_256() const { return ___U3COnMeshUpdateCallU3Ek__BackingField_256; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3COnMeshUpdateCallU3Ek__BackingField_256() { return &___U3COnMeshUpdateCallU3Ek__BackingField_256; }
	inline void set_U3COnMeshUpdateCallU3Ek__BackingField_256(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3COnMeshUpdateCallU3Ek__BackingField_256 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnMeshUpdateCallU3Ek__BackingField_256), value);
	}

	inline static int32_t get_offset_of_m_currentVerts_257() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ___m_currentVerts_257)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_currentVerts_257() const { return ___m_currentVerts_257; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_currentVerts_257() { return &___m_currentVerts_257; }
	inline void set_m_currentVerts_257(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_currentVerts_257 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentVerts_257), value);
	}

	inline static int32_t get_offset_of_m_currentColours_258() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ___m_currentColours_258)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_currentColours_258() const { return ___m_currentColours_258; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_currentColours_258() { return &___m_currentColours_258; }
	inline void set_m_currentColours_258(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_currentColours_258 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentColours_258), value);
	}

	inline static int32_t get_offset_of_m_textFxUpdateGeometryCall_259() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ___m_textFxUpdateGeometryCall_259)); }
	inline bool get_m_textFxUpdateGeometryCall_259() const { return ___m_textFxUpdateGeometryCall_259; }
	inline bool* get_address_of_m_textFxUpdateGeometryCall_259() { return &___m_textFxUpdateGeometryCall_259; }
	inline void set_m_textFxUpdateGeometryCall_259(bool value)
	{
		___m_textFxUpdateGeometryCall_259 = value;
	}

	inline static int32_t get_offset_of_m_textFxAnimDrawCall_260() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ___m_textFxAnimDrawCall_260)); }
	inline bool get_m_textFxAnimDrawCall_260() const { return ___m_textFxAnimDrawCall_260; }
	inline bool* get_address_of_m_textFxAnimDrawCall_260() { return &___m_textFxAnimDrawCall_260; }
	inline void set_m_textFxAnimDrawCall_260(bool value)
	{
		___m_textFxAnimDrawCall_260 = value;
	}

	inline static int32_t get_offset_of__forceNativeDrawCall_261() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ____forceNativeDrawCall_261)); }
	inline bool get__forceNativeDrawCall_261() const { return ____forceNativeDrawCall_261; }
	inline bool* get_address_of__forceNativeDrawCall_261() { return &____forceNativeDrawCall_261; }
	inline void set__forceNativeDrawCall_261(bool value)
	{
		____forceNativeDrawCall_261 = value;
	}

	inline static int32_t get_offset_of__strippedText_262() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ____strippedText_262)); }
	inline String_t* get__strippedText_262() const { return ____strippedText_262; }
	inline String_t** get_address_of__strippedText_262() { return &____strippedText_262; }
	inline void set__strippedText_262(String_t* value)
	{
		____strippedText_262 = value;
		Il2CppCodeGenWriteBarrier((&____strippedText_262), value);
	}

	inline static int32_t get_offset_of__numRenderedLetters_263() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ____numRenderedLetters_263)); }
	inline int32_t get__numRenderedLetters_263() const { return ____numRenderedLetters_263; }
	inline int32_t* get_address_of__numRenderedLetters_263() { return &____numRenderedLetters_263; }
	inline void set__numRenderedLetters_263(int32_t value)
	{
		____numRenderedLetters_263 = value;
	}

	inline static int32_t get_offset_of__vertsToUse_264() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ____vertsToUse_264)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__vertsToUse_264() const { return ____vertsToUse_264; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__vertsToUse_264() { return &____vertsToUse_264; }
	inline void set__vertsToUse_264(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____vertsToUse_264 = value;
		Il2CppCodeGenWriteBarrier((&____vertsToUse_264), value);
	}

	inline static int32_t get_offset_of__colsToUse_265() { return static_cast<int32_t>(offsetof(TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C, ____colsToUse_265)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get__colsToUse_265() const { return ____colsToUse_265; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of__colsToUse_265() { return &____colsToUse_265; }
	inline void set__colsToUse_265(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		____colsToUse_265 = value;
		Il2CppCodeGenWriteBarrier((&____colsToUse_265), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXTEXTMESHPRO_T6F5C0CB616E6AEA5789460FA162A1E9A6268C94C_H
#ifndef TEXTFXTEXTMESHPROUGUI_T8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC_H
#define TEXTFXTEXTMESHPROUGUI_T8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextFx.TextFxTextMeshProUGUI
struct  TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC  : public TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438
{
public:
	// TextFx.TextFxAnimationManager TextFx.TextFxTextMeshProUGUI::m_animation_manager
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * ___m_animation_manager_254;
	// UnityEngine.GameObject TextFx.TextFxTextMeshProUGUI::m_gameobject_reference
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_gameobject_reference_255;
	// System.Boolean TextFx.TextFxTextMeshProUGUI::m_renderToCurve
	bool ___m_renderToCurve_256;
	// TextFx.TextFxBezierCurve TextFx.TextFxTextMeshProUGUI::m_bezierCurve
	TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  ___m_bezierCurve_257;
	// System.Action TextFx.TextFxTextMeshProUGUI::<OnMeshUpdateCall>k__BackingField
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3COnMeshUpdateCallU3Ek__BackingField_258;
	// UnityEngine.CanvasRenderer TextFx.TextFxTextMeshProUGUI::m_canvasRendererComp
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_canvasRendererComp_259;
	// UnityEngine.Vector3[] TextFx.TextFxTextMeshProUGUI::m_currentVerts
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_currentVerts_260;
	// UnityEngine.Color32[] TextFx.TextFxTextMeshProUGUI::m_currentColours
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_currentColours_261;
	// System.Boolean TextFx.TextFxTextMeshProUGUI::m_textFxUpdateGeometryCall
	bool ___m_textFxUpdateGeometryCall_262;
	// System.Boolean TextFx.TextFxTextMeshProUGUI::m_textFxAnimDrawCall
	bool ___m_textFxAnimDrawCall_263;
	// System.Boolean TextFx.TextFxTextMeshProUGUI::_forceNativeDrawCall
	bool ____forceNativeDrawCall_264;
	// System.String TextFx.TextFxTextMeshProUGUI::_strippedText
	String_t* ____strippedText_265;
	// System.Int32 TextFx.TextFxTextMeshProUGUI::_numRenderedLetters
	int32_t ____numRenderedLetters_266;
	// UnityEngine.Vector3[] TextFx.TextFxTextMeshProUGUI::_vertsToUse
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____vertsToUse_267;
	// UnityEngine.Color32[] TextFx.TextFxTextMeshProUGUI::_colsToUse
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ____colsToUse_268;

public:
	inline static int32_t get_offset_of_m_animation_manager_254() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ___m_animation_manager_254)); }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * get_m_animation_manager_254() const { return ___m_animation_manager_254; }
	inline TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 ** get_address_of_m_animation_manager_254() { return &___m_animation_manager_254; }
	inline void set_m_animation_manager_254(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399 * value)
	{
		___m_animation_manager_254 = value;
		Il2CppCodeGenWriteBarrier((&___m_animation_manager_254), value);
	}

	inline static int32_t get_offset_of_m_gameobject_reference_255() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ___m_gameobject_reference_255)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_gameobject_reference_255() const { return ___m_gameobject_reference_255; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_gameobject_reference_255() { return &___m_gameobject_reference_255; }
	inline void set_m_gameobject_reference_255(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_gameobject_reference_255 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameobject_reference_255), value);
	}

	inline static int32_t get_offset_of_m_renderToCurve_256() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ___m_renderToCurve_256)); }
	inline bool get_m_renderToCurve_256() const { return ___m_renderToCurve_256; }
	inline bool* get_address_of_m_renderToCurve_256() { return &___m_renderToCurve_256; }
	inline void set_m_renderToCurve_256(bool value)
	{
		___m_renderToCurve_256 = value;
	}

	inline static int32_t get_offset_of_m_bezierCurve_257() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ___m_bezierCurve_257)); }
	inline TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  get_m_bezierCurve_257() const { return ___m_bezierCurve_257; }
	inline TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7 * get_address_of_m_bezierCurve_257() { return &___m_bezierCurve_257; }
	inline void set_m_bezierCurve_257(TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7  value)
	{
		___m_bezierCurve_257 = value;
	}

	inline static int32_t get_offset_of_U3COnMeshUpdateCallU3Ek__BackingField_258() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ___U3COnMeshUpdateCallU3Ek__BackingField_258)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3COnMeshUpdateCallU3Ek__BackingField_258() const { return ___U3COnMeshUpdateCallU3Ek__BackingField_258; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3COnMeshUpdateCallU3Ek__BackingField_258() { return &___U3COnMeshUpdateCallU3Ek__BackingField_258; }
	inline void set_U3COnMeshUpdateCallU3Ek__BackingField_258(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3COnMeshUpdateCallU3Ek__BackingField_258 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnMeshUpdateCallU3Ek__BackingField_258), value);
	}

	inline static int32_t get_offset_of_m_canvasRendererComp_259() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ___m_canvasRendererComp_259)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_canvasRendererComp_259() const { return ___m_canvasRendererComp_259; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_canvasRendererComp_259() { return &___m_canvasRendererComp_259; }
	inline void set_m_canvasRendererComp_259(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_canvasRendererComp_259 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRendererComp_259), value);
	}

	inline static int32_t get_offset_of_m_currentVerts_260() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ___m_currentVerts_260)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_currentVerts_260() const { return ___m_currentVerts_260; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_currentVerts_260() { return &___m_currentVerts_260; }
	inline void set_m_currentVerts_260(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_currentVerts_260 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentVerts_260), value);
	}

	inline static int32_t get_offset_of_m_currentColours_261() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ___m_currentColours_261)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_currentColours_261() const { return ___m_currentColours_261; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_currentColours_261() { return &___m_currentColours_261; }
	inline void set_m_currentColours_261(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_currentColours_261 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentColours_261), value);
	}

	inline static int32_t get_offset_of_m_textFxUpdateGeometryCall_262() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ___m_textFxUpdateGeometryCall_262)); }
	inline bool get_m_textFxUpdateGeometryCall_262() const { return ___m_textFxUpdateGeometryCall_262; }
	inline bool* get_address_of_m_textFxUpdateGeometryCall_262() { return &___m_textFxUpdateGeometryCall_262; }
	inline void set_m_textFxUpdateGeometryCall_262(bool value)
	{
		___m_textFxUpdateGeometryCall_262 = value;
	}

	inline static int32_t get_offset_of_m_textFxAnimDrawCall_263() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ___m_textFxAnimDrawCall_263)); }
	inline bool get_m_textFxAnimDrawCall_263() const { return ___m_textFxAnimDrawCall_263; }
	inline bool* get_address_of_m_textFxAnimDrawCall_263() { return &___m_textFxAnimDrawCall_263; }
	inline void set_m_textFxAnimDrawCall_263(bool value)
	{
		___m_textFxAnimDrawCall_263 = value;
	}

	inline static int32_t get_offset_of__forceNativeDrawCall_264() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ____forceNativeDrawCall_264)); }
	inline bool get__forceNativeDrawCall_264() const { return ____forceNativeDrawCall_264; }
	inline bool* get_address_of__forceNativeDrawCall_264() { return &____forceNativeDrawCall_264; }
	inline void set__forceNativeDrawCall_264(bool value)
	{
		____forceNativeDrawCall_264 = value;
	}

	inline static int32_t get_offset_of__strippedText_265() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ____strippedText_265)); }
	inline String_t* get__strippedText_265() const { return ____strippedText_265; }
	inline String_t** get_address_of__strippedText_265() { return &____strippedText_265; }
	inline void set__strippedText_265(String_t* value)
	{
		____strippedText_265 = value;
		Il2CppCodeGenWriteBarrier((&____strippedText_265), value);
	}

	inline static int32_t get_offset_of__numRenderedLetters_266() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ____numRenderedLetters_266)); }
	inline int32_t get__numRenderedLetters_266() const { return ____numRenderedLetters_266; }
	inline int32_t* get_address_of__numRenderedLetters_266() { return &____numRenderedLetters_266; }
	inline void set__numRenderedLetters_266(int32_t value)
	{
		____numRenderedLetters_266 = value;
	}

	inline static int32_t get_offset_of__vertsToUse_267() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ____vertsToUse_267)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__vertsToUse_267() const { return ____vertsToUse_267; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__vertsToUse_267() { return &____vertsToUse_267; }
	inline void set__vertsToUse_267(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____vertsToUse_267 = value;
		Il2CppCodeGenWriteBarrier((&____vertsToUse_267), value);
	}

	inline static int32_t get_offset_of__colsToUse_268() { return static_cast<int32_t>(offsetof(TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC, ____colsToUse_268)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get__colsToUse_268() const { return ____colsToUse_268; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of__colsToUse_268() { return &____colsToUse_268; }
	inline void set__colsToUse_268(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		____colsToUse_268 = value;
		Il2CppCodeGenWriteBarrier((&____colsToUse_268), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFXTEXTMESHPROUGUI_T8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3900[4] = 
{
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333::get_offset_of_top_left_0(),
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333::get_offset_of_top_right_1(),
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333::get_offset_of_bottom_right_2(),
	VertexColour_t2416BFEEC43DBB3D91622E111A15CF5928BAA333::get_offset_of_bottom_left_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (BoomlagoonExtensions_t6B8597A20A88CCA986B73F4B759D7D20CC230AE6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (JSONLogger_t4DFCB3FA6AEAFA66AFC4775CD95012040815E1EF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (tfxJSONValueType_t93BF04101D0879A70E1A3D044A72F23D4E791FA5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3903[7] = 
{
	tfxJSONValueType_t93BF04101D0879A70E1A3D044A72F23D4E791FA5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (tfxJSONValue_tC3CD526F934193004DD36549716083628729E799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3904[7] = 
{
	tfxJSONValue_tC3CD526F934193004DD36549716083628729E799::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	tfxJSONValue_tC3CD526F934193004DD36549716083628729E799::get_offset_of_U3CStrU3Ek__BackingField_1(),
	tfxJSONValue_tC3CD526F934193004DD36549716083628729E799::get_offset_of_U3CNumberU3Ek__BackingField_2(),
	tfxJSONValue_tC3CD526F934193004DD36549716083628729E799::get_offset_of_U3CObjU3Ek__BackingField_3(),
	tfxJSONValue_tC3CD526F934193004DD36549716083628729E799::get_offset_of_U3CArrayU3Ek__BackingField_4(),
	tfxJSONValue_tC3CD526F934193004DD36549716083628729E799::get_offset_of_U3CBooleanU3Ek__BackingField_5(),
	tfxJSONValue_tC3CD526F934193004DD36549716083628729E799::get_offset_of_U3CParentU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (tfxJSONArray_t44B4042249796AB3367557BDC64EC7A6B1DA13CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3905[1] = 
{
	tfxJSONArray_t44B4042249796AB3367557BDC64EC7A6B1DA13CC::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (tfxJSONObject_tB6F915240278BF8031DC3EF165A5F66CF0122A8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3906[1] = 
{
	tfxJSONObject_tB6F915240278BF8031DC3EF165A5F66CF0122A8B::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (JSONParsingState_tBEE5A9A1C06096370A2A1E47ED64E1F0F22BA0E8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3907[13] = 
{
	JSONParsingState_tBEE5A9A1C06096370A2A1E47ED64E1F0F22BA0E8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3908[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3909[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (ValueProgression_t018C382D88BDD1607CC92130B776A6E130938FC6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3910[5] = 
{
	ValueProgression_t018C382D88BDD1607CC92130B776A6E130938FC6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (PROGRESSION_VALUE_STATE_t9CB8ADE494C1451DCB82E57F3B642FBA133FF5CE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3911[4] = 
{
	PROGRESSION_VALUE_STATE_t9CB8ADE494C1451DCB82E57F3B642FBA133FF5CE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7)+ sizeof (RuntimeObject), sizeof(ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3912[3] = 
{
	ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7::get_offset_of_m_data_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7::get_offset_of_m_start_state_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionVariableProgressionReferenceData_tF27EA91CEAA5D11D1827F24E5ABB77E7CA1AE4E7::get_offset_of_m_action_index_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3913[10] = 
{
	ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372::get_offset_of_m_reference_data_0(),
	ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372::get_offset_of_m_progression_1(),
	ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372::get_offset_of_m_progression_idx_2(),
	ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372::get_offset_of_m_ease_type_3(),
	ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372::get_offset_of_m_is_offset_from_last_4(),
	ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372::get_offset_of_m_to_to_bool_5(),
	ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372::get_offset_of_m_unique_randoms_6(),
	ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372::get_offset_of_m_animate_per_7(),
	ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372::get_offset_of_m_override_animate_per_option_8(),
	ActionVariableProgression_tF36AFCFFC89DF7318F20B441D78CB48B2AA8A372::get_offset_of_m_custom_ease_curve_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3914[4] = 
{
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3::get_offset_of_m_values_10(),
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3::get_offset_of_m_from_11(),
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3::get_offset_of_m_to_12(),
	ActionFloatProgression_t8D1E8D5BBDC57C7217A5CAE4FCFDEB3FD7B054A3::get_offset_of_m_to_to_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3915[1] = 
{
	ActionPositionVector3Progression_t142669E514EAC93C1916ECEBB639C9CC481F4147::get_offset_of_m_force_position_override_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3916[9] = 
{
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C::get_offset_of_m_values_10(),
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C::get_offset_of_m_from_11(),
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C::get_offset_of_m_to_12(),
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C::get_offset_of_m_to_to_13(),
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C::get_offset_of_m_ease_curve_per_axis_14(),
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C::get_offset_of_m_custom_ease_curve_y_15(),
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C::get_offset_of_m_custom_ease_curve_z_16(),
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C::get_offset_of_m_value_state_17(),
	ActionVector3Progression_tFACD68ACDC1AEF873186F79689940091436CE99C::get_offset_of_m_offset_progression_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3917[9] = 
{
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA::get_offset_of_m_values_10(),
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA::get_offset_of_m_from_11(),
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA::get_offset_of_m_to_12(),
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA::get_offset_of_m_to_to_13(),
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA::get_offset_of_m_override_alpha_14(),
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA::get_offset_of_m_use_colour_gradients_15(),
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA::get_offset_of_m_value_state_16(),
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA::get_offset_of_m_offset_progression_17(),
	ActionColorProgression_t806CA33B6B9DEEEE9D3E090DF4F4EE2DC0583FDA::get_offset_of_cachedColourProgression_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (ACTION_TYPE_tF762AC1ECF82E50019320D4B6FE385F3ACACFCE6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3918[3] = 
{
	ACTION_TYPE_tF762AC1ECF82E50019320D4B6FE385F3ACACFCE6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (PLAY_ITEM_EVENTS_t239FC77660F81C3153256885424DCF47BAF0624F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3919[3] = 
{
	PLAY_ITEM_EVENTS_t239FC77660F81C3153256885424DCF47BAF0624F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (PARTICLE_EFFECT_TYPE_tC57A0B7D976966C6ED25198827D28D7BC8AE7E74)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3920[3] = 
{
	PARTICLE_EFFECT_TYPE_tC57A0B7D976966C6ED25198827D28D7BC8AE7E74::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (PLAY_ITEM_ASSIGNMENT_tAE9BDBA820BE5E0FFD3BB8712C0A315D3B7E84F7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3921[3] = 
{
	PLAY_ITEM_ASSIGNMENT_tAE9BDBA820BE5E0FFD3BB8712C0A315D3B7E84F7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3922[7] = 
{
	ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F::get_offset_of_m_effect_type_7(),
	ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F::get_offset_of_m_shuriken_particle_effect_8(),
	ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F::get_offset_of_m_duration_9(),
	ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F::get_offset_of_m_follow_mesh_10(),
	ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F::get_offset_of_m_position_offset_11(),
	ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F::get_offset_of_m_rotation_offset_12(),
	ParticleEffectSetup_t95FFCBC74D2EC8AABC9964452ED2BF7F26755C5F::get_offset_of_m_rotate_relative_to_letter_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3923[4] = 
{
	AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F::get_offset_of_m_audio_clip_7(),
	AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F::get_offset_of_m_offset_time_8(),
	AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F::get_offset_of_m_volume_9(),
	AudioEffectSetup_t825C0C272933908E8B86A8FD78EA73614B10BB8F::get_offset_of_m_pitch_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3924[7] = 
{
	EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835::get_offset_of_m_editor_display_0(),
	EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835::get_offset_of_m_play_when_1(),
	EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835::get_offset_of_m_effect_assignment_2(),
	EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835::get_offset_of_m_loop_play_once_3(),
	EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835::get_offset_of_CUSTOM_LETTERS_LIST_POS_4(),
	EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835::get_offset_of_m_effect_assignment_custom_letters_5(),
	EffectItemSetup_t27955B657D57A80354ED74453BF074681E6EB835::get_offset_of_m_delay_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3925[42] = 
{
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_editor_folded_0(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_offset_from_last_1(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_action_type_2(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_colour_transition_active_3(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_start_colour_4(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_end_colour_5(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_position_transition_active_6(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_position_axis_ease_data_7(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_start_pos_8(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_end_pos_9(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_global_rotation_transition_active_10(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_global_rotation_axis_ease_data_11(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_global_start_euler_rotation_12(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_global_end_euler_rotation_13(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_local_rotation_transition_active_14(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_rotation_axis_ease_data_15(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_start_euler_rotation_16(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_end_euler_rotation_17(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_global_scale_transition_active_18(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_global_scale_axis_ease_data_19(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_global_start_scale_20(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_global_end_scale_21(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_local_scale_transition_active_22(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_scale_axis_ease_data_23(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_start_scale_24(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_end_scale_25(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_force_same_start_time_26(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_delay_with_white_space_influence_27(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_delay_progression_28(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_duration_progression_29(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_ease_type_30(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_letter_anchor_start_31(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_letter_anchor_end_32(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_letter_anchor_2_way_33(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_particle_effects_34(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_audio_effects_35(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_anchor_offset_36(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_m_anchor_offset_end_37(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_U3CParticleEffectsEditorDisplayU3Ek__BackingField_38(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of_U3CAudioEffectsEditorDisplayU3Ek__BackingField_39(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of__audio_effect_setup_40(),
	LetterAction_tC94703CF8A8258D7D87A358EAED44E3D57890D67::get_offset_of__particle_effect_setup_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (LOOP_TYPE_tC1D2ADE5F4AC54FD8FBAA782496D0F9943BEE513)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3926[3] = 
{
	LOOP_TYPE_tC1D2ADE5F4AC54FD8FBAA782496D0F9943BEE513::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3927[8] = 
{
	ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E::get_offset_of_m_start_action_idx_0(),
	ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E::get_offset_of_m_end_action_idx_1(),
	ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E::get_offset_of_m_number_of_loops_2(),
	ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E::get_offset_of_m_loop_type_3(),
	ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E::get_offset_of_m_delay_first_only_4(),
	ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E::get_offset_of_m_finish_at_end_5(),
	ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E::get_offset_of_m_active_loop_index_6(),
	ActionLoopCycle_tB01A044F8B7003818FB2657B84BB5A6CD441BE9E::get_offset_of_m_first_pass_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3928[9] = 
{
	0,
	LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9::get_offset_of_m_letter_actions_1(),
	LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9::get_offset_of_m_loop_cycles_2(),
	LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9::get_offset_of_m_letters_to_animate_option_3(),
	LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9::get_offset_of_m_letters_to_animate_4(),
	LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9::get_offset_of_m_letters_to_animate_custom_idx_5(),
	LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9::get_offset_of_m_num_white_space_chars_to_include_6(),
	LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9::get_offset_of_m_defaultTextColourProgression_7(),
	LetterAnimation_tA04230F0A57961D82FA0ED8CA398B340ED77B4A9::get_offset_of_m_animation_state_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3929[12] = 
{
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_active_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_waiting_to_sync_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_started_action_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_break_delay_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_timer_offset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_action_index_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_reverse_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_action_index_progress_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_prev_action_index_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_linear_progress_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_action_progress_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationStateVariables_t8A45C679D855E8FB434F54D91FA44A18423E1D12::get_offset_of_m_active_loop_cycles_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3930[69] = 
{
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_animation_manager_ref_0(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_current_letter_action_1(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_base_vertices_2(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_base_extra_vertices_3(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_using_curved_data_4(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_curve_base_vertices_5(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_curve_base_extra_vertices_6(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_base_colours_7(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_base_extra_colours_8(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_base_indexes_9(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_visible_character_10(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_stub_instance_11(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_mesh_index_12(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_width_13(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_height_14(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_progression_variables_15(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_anim_state_vars_16(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_action_timer_17(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_action_delay_18(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_ignore_action_delay_19(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_action_duration_20(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_last_animate_per_21(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_current_animated_vertices_22(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_current_animated_colours_23(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_local_scale_from_24(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_local_scale_to_25(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_global_scale_from_26(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_global_scale_to_27(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_local_rotation_from_28(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_local_rotation_to_29(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_global_rotation_from_30(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_global_rotation_to_31(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_position_from_32(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_position_to_33(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_anchor_offset_from_34(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_anchor_offset_to_35(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_colour_from_36(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_colour_to_37(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_letter_colour_38(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_letter_position_39(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_letter_scale_40(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_letter_global_scale_41(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_anchor_offset_42(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_letter_rotation_43(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_letter_global_rotation_44(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_current_state_45(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_flippedVerts_46(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_continueType_47(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_continueLetterCallback_48(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_continueActionIndexTrigger_49(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_continuedLoopStartIndex_50(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_fastTrackLoops_51(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_anchor_offset_middle_center_52(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_active_anchor_offset_upper_left_53(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_active_anchor_offset_upper_center_54(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_active_anchor_offset_upper_right_55(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_active_anchor_offset_middle_left_56(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_active_anchor_offset_middle_center_57(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_active_anchor_offset_middle_right_58(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_active_anchor_offset_lower_left_59(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_active_anchor_offset_lower_center_60(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_active_anchor_offset_lower_right_61(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_rotationOffsetQuat_62(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_m_rotationOffsetQuatInverse_63(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_prev_action_idx_64(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_prev_delay_65(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_current_action_delay_66(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_altered_delay_67(),
	LetterSetup_tFF1F1C00E610201F6E3C9D01843D8866C7F6369E::get_offset_of_old_action_delay_68(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { sizeof (VertexPosition_t91B44DB8195E7418053D268C4AE708A5E475DD8C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3931[5] = 
{
	VertexPosition_t91B44DB8195E7418053D268C4AE708A5E475DD8C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3932[10] = 
{
	U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3::get_offset_of_animation_0(),
	U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3::get_offset_of_action_state_to_continue_to_1(),
	U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3::get_offset_of_U3CU3E4__this_2(),
	U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3::get_offset_of_continueType_3(),
	U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3::get_offset_of_continueDuration_4(),
	U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3::get_offset_of_action_index_to_continue_with_5(),
	U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3::get_offset_of_action_index_progress_6(),
	U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3::get_offset_of_use_start_state_7(),
	U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3::get_offset_of_animate_per_8(),
	U3CU3Ec__DisplayClass190_0_t88298C1A6D971506ED0B072A62638705AC6711C3::get_offset_of_deepestLoopDepth_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3933[12] = 
{
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_animation_manager_0(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_letter_setup_ref_1(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_particle_system_2(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_duration_3(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_delay_4(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_position_offset_5(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_rotation_offset_6(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_rotate_with_letter_7(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_follow_mesh_8(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_active_9(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_m_transform_10(),
	ParticleEffectInstanceManager_tE79765CA051F0BE5F91AC26C52E77A7FC478C24C::get_offset_of_rotation_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3934[7] = 
{
	PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C::get_offset_of_m_data_type_0(),
	PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C::get_offset_of_m_startState_1(),
	PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C::get_offset_of_m_setting_name_2(),
	PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C::get_offset_of_m_animation_idx_3(),
	PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C::get_offset_of_m_action_idx_4(),
	PresetEffectSetting_t7C2FA9ADF38B13BDC4FE1E6BE2BF78E896F6F05C::get_offset_of_m_action_progress_state_override_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3935[9] = 
{
	VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB::get_offset_of_m_type_0(),
	VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB::get_offset_of_m_onFloatStateChangeCallback_1(),
	VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB::get_offset_of_m_onVector3StateChangeCallback_2(),
	VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB::get_offset_of_m_onColorStateChangeCallback_3(),
	VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB::get_offset_of_m_onToggleStateChangeCallback_4(),
	VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB::get_offset_of_m_startFloatValue_5(),
	VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB::get_offset_of_m_startVector3Value_6(),
	VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB::get_offset_of_m_startColorValue_7(),
	VariableStateListener_t6B8087CE0DE67C734909A0C385FEF2861B2294DB::get_offset_of_m_startToggleValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (TYPE_t51271A948E2B2A89B4CD430CF90DF185D4685549)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3936[5] = 
{
	TYPE_t51271A948E2B2A89B4CD430CF90DF185D4685549::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (U3CU3Ec__DisplayClass9_0_t38A971FAC0C1A86373E6D4F7734D3A2873622454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3937[2] = 
{
	U3CU3Ec__DisplayClass9_0_t38A971FAC0C1A86373E6D4F7734D3A2873622454::get_offset_of_letterAction_0(),
	U3CU3Ec__DisplayClass9_0_t38A971FAC0C1A86373E6D4F7734D3A2873622454::get_offset_of_animation_manager_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (TEXTFX_IMPLEMENTATION_tD7913D147631D070B01A9AD90002E432F3EA7C7F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3939[8] = 
{
	TEXTFX_IMPLEMENTATION_tD7913D147631D070B01A9AD90002E432F3EA7C7F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { sizeof (TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399), -1, sizeof(TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3940[58] = 
{
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399_StaticFields::get_offset_of_m_animation_section_names_0(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399_StaticFields::get_offset_of_m_animation_section_folders_1(),
	0,
	0,
	0,
	0,
	0,
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_master_animations_7(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_begin_on_start_8(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_on_finish_action_9(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_animation_speed_factor_10(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_begin_delay_11(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_animate_per_12(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_time_type_13(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_letters_14(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_audio_sources_15(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_particle_systems_16(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_particle_effect_managers_17(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_gameObect_18(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_transform_19(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_animation_interface_reference_20(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_monobehaviour_21(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_num_letters_22(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_num_extra_verts_per_letter_23(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_textWidths_24(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_textHeights_25(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_current_mesh_verts_26(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_current_mesh_colours_27(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_current_text_28(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_num_words_29(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_num_lines_30(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_letters_not_animated_31(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_last_time_32(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_animation_timer_33(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_lowest_action_progress_34(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_runtime_animation_speed_factor_35(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_running_36(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_paused_37(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_animation_callback_38(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_what_just_changed_39(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_animation_continue_callback_40(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_dataRebuildCallFrame_41(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_preset_intro_42(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_preset_main_43(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_preset_outro_44(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_repeat_all_sections_45(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_repeat_all_sections_count_46(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_m_curveDataApplied_47(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_all_letter_anims_finished_48(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_all_letter_anims_waiting_49(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_all_letter_anims_waiting_infinitely_50(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_all_letter_anims_continuing_finished_51(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_lowest_action_progress_52(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_last_letter_idx_53(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_letter_setup_54(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_letterAnimation_55(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_letter_verts_56(),
	TextFxAnimationManager_tC9986D5A9B84FE61F49FB56B7514A7E7F3016399::get_offset_of_letter_colours_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { sizeof (PRESET_ANIMATION_SECTION_tBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3941[5] = 
{
	PRESET_ANIMATION_SECTION_tBFB51B6FF6E904EA4297BC6FAD05655DD1318F0D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3942[10] = 
{
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E::get_offset_of_m_preset_effect_settings_0(),
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E::get_offset_of_m_active_1(),
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E::get_offset_of_m_start_action_2(),
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E::get_offset_of_m_num_actions_3(),
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E::get_offset_of_m_start_loop_4(),
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E::get_offset_of_m_num_loops_5(),
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E::get_offset_of_m_exit_pause_6(),
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E::get_offset_of_m_exit_pause_duration_7(),
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E::get_offset_of_m_repeat_8(),
	PresetAnimationSection_t7775917B7A5DA6084D75532BD1D812D5F5357A2E::get_offset_of_m_repeat_count_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (U3CPlayAnimationAfterDelayU3Ed__122_t9BAF09729A5CE37CEDA32859771F52DBC04D95C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3944[4] = 
{
	U3CPlayAnimationAfterDelayU3Ed__122_t9BAF09729A5CE37CEDA32859771F52DBC04D95C0::get_offset_of_U3CU3E1__state_0(),
	U3CPlayAnimationAfterDelayU3Ed__122_t9BAF09729A5CE37CEDA32859771F52DBC04D95C0::get_offset_of_U3CU3E2__current_1(),
	U3CPlayAnimationAfterDelayU3Ed__122_t9BAF09729A5CE37CEDA32859771F52DBC04D95C0::get_offset_of_U3CU3E4__this_2(),
	U3CPlayAnimationAfterDelayU3Ed__122_t9BAF09729A5CE37CEDA32859771F52DBC04D95C0::get_offset_of_delay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3945[6] = 
{
	U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101::get_offset_of_U3CU3E1__state_0(),
	U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101::get_offset_of_U3CU3E2__current_1(),
	U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101::get_offset_of_time_type_2(),
	U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101::get_offset_of_delay_3(),
	U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101::get_offset_of_U3CtimerU3E5__2_4(),
	U3CTimeDelayU3Ed__157_t4A1D7F691D4CE59C4B95D3C94B41EEFB3D7BC101::get_offset_of_U3Clast_timeU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770)+ sizeof (RuntimeObject), sizeof(BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3946[2] = 
{
	BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770::get_offset_of_m_anchor_point_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BezierCurvePoint_tE162B9039A9C45D49361BB9B26E8346D1C80B770::get_offset_of_m_handle_point_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298)+ sizeof (RuntimeObject), sizeof(BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3947[6] = 
{
	BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298::get_offset_of_m_anchor_point_1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298::get_offset_of_m_anchor_point_2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298::get_offset_of_m_anchor_point_3_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298::get_offset_of_m_anchor_point_4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298::get_offset_of_m_anchor_point_5_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BezierCurvePointData_tD8FCDB79B1459B17D382755F250A0CA9518A9298::get_offset_of_m_numActiveCurvePoints_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7)+ sizeof (RuntimeObject), sizeof(TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3948[7] = 
{
	0,
	0,
	0,
	TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7::get_offset_of_m_pointData_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7::get_offset_of_m_baselineOffset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7::get_offset_of_m_temp_anchor_points_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextFxBezierCurve_t64713478F22A36E9B349DE2116BECD570F8F5AB7::get_offset_of_rot_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3949[6] = 
{
	U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8::get_offset_of_alignment_0(),
	U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8::get_offset_of_renderedTextWidth_1(),
	U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8::get_offset_of_anim_manager_2(),
	U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8::get_offset_of_letter_3(),
	U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8::get_offset_of_curve_length_4(),
	U3CU3Ec__DisplayClass11_0_tA1E408646D4814FEEDB47A631771A175249DD7C8::get_offset_of_letters_offset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (LETTERS_TO_ANIMATE_t250343972219C53F06394DB3A3798EF25DCAE8F9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3950[15] = 
{
	LETTERS_TO_ANIMATE_t250343972219C53F06394DB3A3798EF25DCAE8F9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (TextDisplayAxis_tC609C7C79F19A29042BF57CD2DFCA6B6A6605AD3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3951[3] = 
{
	TextDisplayAxis_tC609C7C79F19A29042BF57CD2DFCA6B6A6605AD3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (AnimationTime_t2F508B3A8A9CE9AEF70C6B4062FF98E6848C9CB2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3952[3] = 
{
	AnimationTime_t2F508B3A8A9CE9AEF70C6B4062FF98E6848C9CB2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { sizeof (AnimatePerOptions_t8F02C09D4D265D1C7F163364349907973CD6FE55)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3953[4] = 
{
	AnimatePerOptions_t8F02C09D4D265D1C7F163364349907973CD6FE55::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (LETTER_ANIMATION_STATE_t4BA25C9DCB599CE2FCBE7F37433DF50C45A1AA0E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3954[7] = 
{
	LETTER_ANIMATION_STATE_t4BA25C9DCB599CE2FCBE7F37433DF50C45A1AA0E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (ON_FINISH_ACTION_t40524EC048B30CCF656E94D2C2E80755DE8840C5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3955[5] = 
{
	ON_FINISH_ACTION_t40524EC048B30CCF656E94D2C2E80755DE8840C5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (ANIMATION_DATA_TYPE_tDD2F3FCCB7592775968BB2ECF653025B830D831C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3956[22] = 
{
	ANIMATION_DATA_TYPE_tDD2F3FCCB7592775968BB2ECF653025B830D831C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (ContinueType_t71EF955D26397FEEC364B9A0A5ED6628671D934C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3957[4] = 
{
	ContinueType_t71EF955D26397FEEC364B9A0A5ED6628671D934C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3958[4] = 
{
	AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B::get_offset_of_m_override_default_0(),
	AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B::get_offset_of_m_x_ease_1(),
	AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B::get_offset_of_m_y_ease_2(),
	AxisEasingOverrideData_tD979C81A44FE08E1065B1C9F1CE0E4300564C47B::get_offset_of_m_z_ease_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3959[4] = 
{
	AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311::get_offset_of_m_letter_value_0(),
	AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311::get_offset_of_m_letter_value_inc_white_space_1(),
	AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311::get_offset_of_m_word_value_2(),
	AnimationProgressionVariables_tACC3BB5B42E1198FB274379EE8A8BD2667149311::get_offset_of_m_line_value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3960[4] = 
{
	CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3::get_offset_of_flipped_0(),
	CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3::get_offset_of_uv_1(),
	CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3::get_offset_of_vert_2(),
	CustomCharacterInfo_t5EB0325CD6C49590781C553EB09837AE1FE17CB3::get_offset_of_width_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (CustomFontCharacterData_t1391FAABB7293C043AAF8845B64651AF4ED69E99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3961[1] = 
{
	CustomFontCharacterData_t1391FAABB7293C043AAF8845B64651AF4ED69E99::get_offset_of_m_character_infos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (TextFxHelperMethods_tE9A8DBB919531EF5F6FC51CFABE06CC71C1C4967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3963[43] = 
{
	0,
	0,
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_text_6(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_font_7(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_font_texture_width_8(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_font_texture_height_9(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_font_data_file_10(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_font_material_11(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_px_offset_12(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_character_size_13(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_use_colour_gradient_14(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_textColour_15(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_textColourGradient_16(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_display_axis_17(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_text_anchor_18(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_text_alignment_19(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_line_height_factor_20(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_max_width_21(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_override_font_baseline_22(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_font_baseline_override_23(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_animation_manager_24(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_gameobject_reference_25(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_renderToCurve_26(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_bezierCurve_27(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_U3COnMeshUpdateCallU3Ek__BackingField_28(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_mesh_verts_29(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_mesh_uvs_30(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_mesh_cols_31(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_mesh_normals_32(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_mesh_triangles_33(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_font_baseline_34(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_custom_font_data_35(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_current_font_data_file_name_36(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_current_font_name_37(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_fontRebuildCallback_38(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_mesh_combine_instance_39(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_transform_reference_40(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_renderer_41(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_mesh_filter_42(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_mesh_43(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_total_text_width_44(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_total_text_height_45(),
	TextFxNative_t097594515F97FCCF174099736286557AE5B9FA9E::get_offset_of_m_line_height_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (TextFxTextDataHandler_tD66D5E028C0AC7F85A41F5C99EE17318888B105C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3964[2] = 
{
	TextFxTextDataHandler_tD66D5E028C0AC7F85A41F5C99EE17318888B105C::get_offset_of_m_posData_0(),
	TextFxTextDataHandler_tD66D5E028C0AC7F85A41F5C99EE17318888B105C::get_offset_of_m_colData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3965[17] = 
{
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_y_max_1(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_y_min_2(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_last_char_info_3(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_text_width_4(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_line_widths_5(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_line_height_offset_6(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_total_text_width_7(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_total_text_height_8(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_x_max_9(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_x_min_10(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_text_height_11(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_line_letter_idx_12(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_line_width_at_last_space_13(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_space_char_offset_14(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_last_space_y_max_15(),
	U3CU3Ec__DisplayClass113_0_t7824309C125A014D06940D95EEC463038F73C345::get_offset_of_last_space_y_min_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95), -1, sizeof(TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3966[7] = 
{
	TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95::get_offset_of_m_posData_0(),
	TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95::get_offset_of_m_colData_1(),
	TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95::get_offset_of_m_numBaseLetters_2(),
	TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95::get_offset_of_m_extraVertsPerLetter_3(),
	TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95::get_offset_of_m_totalVertsPerLetter_4(),
	TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95::get_offset_of_m_numExtraQuads_5(),
	TMPTextDataHandler_t3A3363A412456C1498F99659F551A17631C19B95_StaticFields::get_offset_of_baseColVertIndexes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3967[14] = 
{
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of_m_animation_manager_252(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of_m_gameobject_reference_253(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of_m_renderToCurve_254(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of_m_bezierCurve_255(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of_U3COnMeshUpdateCallU3Ek__BackingField_256(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of_m_currentVerts_257(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of_m_currentColours_258(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of_m_textFxUpdateGeometryCall_259(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of_m_textFxAnimDrawCall_260(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of__forceNativeDrawCall_261(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of__strippedText_262(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of__numRenderedLetters_263(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of__vertsToUse_264(),
	TextFxTextMeshPro_t6F5C0CB616E6AEA5789460FA162A1E9A6268C94C::get_offset_of__colsToUse_265(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3968[15] = 
{
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of_m_animation_manager_254(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of_m_gameobject_reference_255(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of_m_renderToCurve_256(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of_m_bezierCurve_257(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of_U3COnMeshUpdateCallU3Ek__BackingField_258(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of_m_canvasRendererComp_259(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of_m_currentVerts_260(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of_m_currentColours_261(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of_m_textFxUpdateGeometryCall_262(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of_m_textFxAnimDrawCall_263(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of__forceNativeDrawCall_264(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of__strippedText_265(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of__numRenderedLetters_266(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of__vertsToUse_267(),
	TextFxTextMeshProUGUI_t8A8D5CBB695C71DC5BD7729B38C01FAC627AA9DC::get_offset_of__colsToUse_268(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3969[15] = 
{
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_animation_manager_37(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_gameobject_reference_38(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_renderToCurve_39(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_bezierCurve_40(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_U3COnMeshUpdateCallU3Ek__BackingField_41(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_effect_type_42(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_effect_offset_43(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_effect_colour_44(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_cachedVerts_45(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_currentMeshVerts_46(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_textFxUpdateGeometryCall_47(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of_m_textFxAnimDrawCall_48(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of__numLetterMeshes_49(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of__temp_vert_50(),
	TextFxUGUI_tC359CF51C5D5602DFC59E0FEEB18192F97EF4F81::get_offset_of__uiVertexQuad_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (UGUI_MESH_EFFECT_TYPE_t5D20CD8E3154DA28B8B0AD1125B801840C5DB200)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3970[5] = 
{
	UGUI_MESH_EFFECT_TYPE_t5D20CD8E3154DA28B8B0AD1125B801840C5DB200::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (UGUITextDataHandler_t39FD0317DB9504E99E62E08013FEABB8F62A6BE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3971[4] = 
{
	UGUITextDataHandler_t39FD0317DB9504E99E62E08013FEABB8F62A6BE6::get_offset_of_m_vertData_0(),
	UGUITextDataHandler_t39FD0317DB9504E99E62E08013FEABB8F62A6BE6::get_offset_of_m_numBaseLetters_1(),
	UGUITextDataHandler_t39FD0317DB9504E99E62E08013FEABB8F62A6BE6::get_offset_of_m_extraVertsPerLetter_2(),
	UGUITextDataHandler_t39FD0317DB9504E99E62E08013FEABB8F62A6BE6::get_offset_of_m_totalVertsPerLetter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (HexRGB_t3457F98D908B302FF235748F6729A0240D21680B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3972[2] = 
{
	HexRGB_t3457F98D908B302FF235748F6729A0240D21680B::get_offset_of_textColor_4(),
	HexRGB_t3457F98D908B302FF235748F6729A0240D21680B::get_offset_of_hsvpicker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (HsvBoxSelector_t75BDC2D0CC267992A0E7127BD48FFC42F699796C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3973[2] = 
{
	HsvBoxSelector_t75BDC2D0CC267992A0E7127BD48FFC42F699796C::get_offset_of_dragger_4(),
	HsvBoxSelector_t75BDC2D0CC267992A0E7127BD48FFC42F699796C::get_offset_of_rectTransform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3974[4] = 
{
	HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5::get_offset_of_parentPanel_4(),
	HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5::get_offset_of_rectTransform_5(),
	HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5::get_offset_of_scrollRect_6(),
	HSVDragger_t4BA183EBB29750BA76D8B112C70FBB6A3DF8EEE5::get_offset_of_picker_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3975[27] = 
{
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_hexrgb_4(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_currentColor_5(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_r_6(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_g_7(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_b_8(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_colorImage_9(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_hsvSlider_10(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_hsvImage_11(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_sliderPicker_12(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_boxSlider_13(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_sliderR_14(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_sliderG_15(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_sliderB_16(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_sliderRText_17(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_sliderGText_18(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_sliderBText_19(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_pointerPos_20(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_minHue_21(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_maxHue_22(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_minSat_23(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_maxSat_24(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_minV_25(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_maxV_26(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_cursorX_27(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_cursorY_28(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_onValueChanged_29(),
	HSVPicker_tABA452C1C512F3C5A07C4581B07C9823224CA565::get_offset_of_isChanging_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (HSVSliderEvent_tE0150BE444ACECCCD174D0CA99B45FF973AC8318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (HSVUtil_t0E0EA69C0A10A5CD024A61DDBB332C988626CB20), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (HsvColor_t5C320A5176C61413E49BE88D50894A89DE00F9FF)+ sizeof (RuntimeObject), sizeof(HsvColor_t5C320A5176C61413E49BE88D50894A89DE00F9FF ), 0, 0 };
extern const int32_t g_FieldOffsetTable3978[3] = 
{
	HsvColor_t5C320A5176C61413E49BE88D50894A89DE00F9FF::get_offset_of_H_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t5C320A5176C61413E49BE88D50894A89DE00F9FF::get_offset_of_S_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t5C320A5176C61413E49BE88D50894A89DE00F9FF::get_offset_of_V_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (TiltWindow_t0BEC1307ECC8AE2090E015B81C172F87A1E6AE84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3979[4] = 
{
	TiltWindow_t0BEC1307ECC8AE2090E015B81C172F87A1E6AE84::get_offset_of_range_4(),
	TiltWindow_t0BEC1307ECC8AE2090E015B81C172F87A1E6AE84::get_offset_of_mTrans_5(),
	TiltWindow_t0BEC1307ECC8AE2090E015B81C172F87A1E6AE84::get_offset_of_mStart_6(),
	TiltWindow_t0BEC1307ECC8AE2090E015B81C172F87A1E6AE84::get_offset_of_mRot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3980[11] = 
{
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_HandleRect_18(),
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_MinValue_19(),
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_MaxValue_20(),
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_WholeNumbers_21(),
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_Value_22(),
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_ValueY_23(),
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_OnValueChanged_24(),
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_HandleTransform_25(),
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_HandleContainerRect_26(),
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_Offset_27(),
	BoxSlider_tA8560E79728DA37948F4B2D37D7CC790511E407A::get_offset_of_m_Tracker_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (BoxSliderEvent_t2E59E1AD1FFFC4D521C2865063F16C634BF59706), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (SceneManager_t3F01A7DF4D0C1E7E9688344DD2A09C7B79116172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3982[2] = 
{
	SceneManager_t3F01A7DF4D0C1E7E9688344DD2A09C7B79116172::get_offset_of_toAR_4(),
	SceneManager_t3F01A7DF4D0C1E7E9688344DD2A09C7B79116172::get_offset_of_toHomeDesign_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3983[1] = 
{
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U343F2C22CB295B073A57F99AC31686B98E380A612_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
