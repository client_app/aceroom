﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<UnityEngine.Camera>
struct Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C;
// System.Action`1<UnityEngine.Camera[]>
struct Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ObjectTarget>
struct Dictionary_2_t38214185C816F7C6D37D4A288F828447EFA5BF63;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonBehaviour>
struct Dictionary_2_t49D49E0D72539C160B6F736C64DBC729238B3481;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Type,Vuforia.Tracker>>
struct Dictionary_2_t633AA66B66AB19AA7FDC21111B1C74DCE1079775;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<Vuforia.Tracker,System.Boolean>>
struct Dictionary_2_t7534CF3FD31278CAF9684333AD1265CDF6DE26C9;
// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.TargetFinder>
struct Dictionary_2_t97145FAA8A62E3A6895B2E7A97B7BB62B2F6B972;
// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.Tracker>
struct Dictionary_2_tB17ABD9DDE1EDA398BB0CCCFED6756493B866D03;
// System.Collections.Generic.Dictionary`2<Vuforia.PIXEL_FORMAT,Vuforia.UnmanagedImage>
struct Dictionary_2_tD36561317F15A4463B66EFAA17CF49A315A67587;
// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>
struct LinkedList_1_tE86603FF5BEA21BD62849C673324AEE74A31E58A;
// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t0748E6C49D36B6B078F5DBEDEAEDDE881580AD0B;
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t1314A3DDAE2D8AD81F9403BD961C3CC0530B04A9;
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_tE1500C473509EDCE2ED838547DF41114B3785932;
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t9F47B63E854EC1B276E04B3C907EA6E9B9A2C619;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.PostProcessing.ParameterOverride>
struct ReadOnlyCollection_1_t2D7596D677CC341719EF47DD23F94617FB662DED;
// System.Func`1<System.Boolean>
struct Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1;
// System.Func`2<System.Type,Vuforia.Tracker>
struct Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580;
// System.Func`2<Vuforia.TargetFinder,System.Boolean>
struct Func_2_tC443A4E5D2EA3B2E46141E8CFA7F0F891B9851C2;
// System.Func`2<Vuforia.Tracker,System.Boolean>
struct Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608;
// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor>
struct Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.RenderTexture[][]
struct RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF;
// UnityEngine.Rendering.PostProcessing.AmbientOcclusion
struct AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B;
// UnityEngine.Rendering.PostProcessing.AmbientOcclusionModeParameter
struct AmbientOcclusionModeParameter_t4C91527D21B57CFBD2DA5A8AAAB48B89555ED8B6;
// UnityEngine.Rendering.PostProcessing.AmbientOcclusionQualityParameter
struct AmbientOcclusionQualityParameter_t293F9DA77655C53F24A5AFC38014DBCEB25D8ECC;
// UnityEngine.Rendering.PostProcessing.AutoExposure
struct AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A;
// UnityEngine.Rendering.PostProcessing.Bloom
struct Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0;
// UnityEngine.Rendering.PostProcessing.BloomRenderer/Level[]
struct LevelU5BU5D_tDFD4CD93E3C076B6A1726E0A34995770431AF9D0;
// UnityEngine.Rendering.PostProcessing.BoolParameter
struct BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE;
// UnityEngine.Rendering.PostProcessing.ChromaticAberration
struct ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D;
// UnityEngine.Rendering.PostProcessing.ColorGrading
struct ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035;
// UnityEngine.Rendering.PostProcessing.ColorParameter
struct ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B;
// UnityEngine.Rendering.PostProcessing.DepthOfField
struct DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC;
// UnityEngine.Rendering.PostProcessing.EyeAdaptationParameter
struct EyeAdaptationParameter_tAECAA41037553C4EAAA628E6840DD6EEC8C84036;
// UnityEngine.Rendering.PostProcessing.FloatParameter
struct FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F;
// UnityEngine.Rendering.PostProcessing.GradingModeParameter
struct GradingModeParameter_tBC84E2E66719BACCD847B301C1F156E4A340362A;
// UnityEngine.Rendering.PostProcessing.Grain
struct Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6;
// UnityEngine.Rendering.PostProcessing.HableCurve
struct HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D;
// UnityEngine.Rendering.PostProcessing.IAmbientOcclusionMethod[]
struct IAmbientOcclusionMethodU5BU5D_t0CB2EB32F63F29733C83C7ABEF672EFB2AA58A01;
// UnityEngine.Rendering.PostProcessing.KernelSizeParameter
struct KernelSizeParameter_t75F71C05A913F8456F68E98BD412A8B9990E2140;
// UnityEngine.Rendering.PostProcessing.SplineParameter
struct SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0;
// UnityEngine.Rendering.PostProcessing.TextureParameter
struct TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87;
// UnityEngine.Rendering.PostProcessing.TonemapperParameter
struct TonemapperParameter_tD2E82FA78543CD6AAA7C34BCF1BFD5F3C1CE65EF;
// UnityEngine.Rendering.PostProcessing.Vector2Parameter
struct Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2;
// UnityEngine.Rendering.PostProcessing.Vector4Parameter
struct Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338;
// Vuforia.CameraCalibrationComparer
struct CameraCalibrationComparer_t7CF72E6611106D56BB040F32DB6D7BFF0898F7EA;
// Vuforia.DataSet
struct DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644;
// Vuforia.DigitalEyewearARController
struct DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4;
// Vuforia.ICameraConfiguration
struct ICameraConfiguration_tB7509A17C94E12154FA84F00D5E0A03EE3F4AF3F;
// Vuforia.IHoloLensApiAbstraction
struct IHoloLensApiAbstraction_t12E3611D625B9E76CD31BE15058EE9D4A058AE24;
// Vuforia.ITrackerManager
struct ITrackerManager_tF9B9BD66F3A783EE6406F6E14B874385FE5E156B;
// Vuforia.IVideoTextureUpdater
struct IVideoTextureUpdater_t7836E664DE377781E54A23DB917456E0CA77966C;
// Vuforia.ImageTarget
struct ImageTarget_t916FA6EEDF77BFCE0C8490D8E534D871354B7E1C;
// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755;
// Vuforia.LateLatchingManager
struct LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C;
// Vuforia.MultiTarget
struct MultiTarget_tE5E82497457AA47E0DEDBBE9AB5B7773B16C528D;
// Vuforia.ObjectTracker
struct ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E;
// Vuforia.StateManager
struct StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1;
// Vuforia.Trackable
struct Trackable_t2A23C572321E7D4FEAC9A1019DFA0AA144FC9B8F;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4;
// Vuforia.TrackerData/TrackableResultData[]
struct TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D;
// Vuforia.TrackerData/VuMarkTargetData[]
struct VuMarkTargetDataU5BU5D_tDEF295778F1EBA00260FE5F7CF00E0E91F5E3605;
// Vuforia.TrackerData/VuMarkTargetResultData[]
struct VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A;
// Vuforia.UnityCompiled.IUnityAndroidPermissions
struct IUnityAndroidPermissions_tC63F0B0237AED0CFC9589B54D40A234FF082562D;
// Vuforia.UnityCompiled.IUnityCompiledFacade
struct IUnityCompiledFacade_t0E3A7AE8F2EFBD9E4E214CB598761DDD267E70BE;
// Vuforia.UnityCompiled.IUnityRenderPipeline
struct IUnityRenderPipeline_tAD5FDC16A735AA177793B835843069369918FC34;
// Vuforia.VirtualButton
struct VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654;
// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2;
// Vuforia.WebCam
struct WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2;
// Vuforia.WorldCenterTrackableBehaviour
struct WorldCenterTrackableBehaviour_tA305098FD468B65DE0B5CC6BDED91127D1306A88;




#ifndef U3CMODULEU3E_T261AC3CE6934CC046947474E3B28E1E19C5641A6_H
#define U3CMODULEU3E_T261AC3CE6934CC046947474E3B28E1E19C5641A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t261AC3CE6934CC046947474E3B28E1E19C5641A6 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T261AC3CE6934CC046947474E3B28E1E19C5641A6_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef DITHERING_T89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0_H
#define DITHERING_T89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Dithering
struct  Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.Dithering::m_NoiseTextureIndex
	int32_t ___m_NoiseTextureIndex_0;
	// System.Random UnityEngine.Rendering.PostProcessing.Dithering::m_Random
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___m_Random_1;

public:
	inline static int32_t get_offset_of_m_NoiseTextureIndex_0() { return static_cast<int32_t>(offsetof(Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0, ___m_NoiseTextureIndex_0)); }
	inline int32_t get_m_NoiseTextureIndex_0() const { return ___m_NoiseTextureIndex_0; }
	inline int32_t* get_address_of_m_NoiseTextureIndex_0() { return &___m_NoiseTextureIndex_0; }
	inline void set_m_NoiseTextureIndex_0(int32_t value)
	{
		___m_NoiseTextureIndex_0 = value;
	}

	inline static int32_t get_offset_of_m_Random_1() { return static_cast<int32_t>(offsetof(Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0, ___m_Random_1)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_m_Random_1() const { return ___m_Random_1; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_m_Random_1() { return &___m_Random_1; }
	inline void set_m_Random_1(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___m_Random_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Random_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERING_T89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0_H
#ifndef FASTAPPROXIMATEANTIALIASING_T8677D2B4EDDE97C4D6C747227D40395AD1DA134D_H
#define FASTAPPROXIMATEANTIALIASING_T8677D2B4EDDE97C4D6C747227D40395AD1DA134D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.FastApproximateAntialiasing
struct  FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.FastApproximateAntialiasing::fastMode
	bool ___fastMode_0;
	// System.Boolean UnityEngine.Rendering.PostProcessing.FastApproximateAntialiasing::keepAlpha
	bool ___keepAlpha_1;

public:
	inline static int32_t get_offset_of_fastMode_0() { return static_cast<int32_t>(offsetof(FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D, ___fastMode_0)); }
	inline bool get_fastMode_0() const { return ___fastMode_0; }
	inline bool* get_address_of_fastMode_0() { return &___fastMode_0; }
	inline void set_fastMode_0(bool value)
	{
		___fastMode_0 = value;
	}

	inline static int32_t get_offset_of_keepAlpha_1() { return static_cast<int32_t>(offsetof(FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D, ___keepAlpha_1)); }
	inline bool get_keepAlpha_1() const { return ___keepAlpha_1; }
	inline bool* get_address_of_keepAlpha_1() { return &___keepAlpha_1; }
	inline void set_keepAlpha_1(bool value)
	{
		___keepAlpha_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTAPPROXIMATEANTIALIASING_T8677D2B4EDDE97C4D6C747227D40395AD1DA134D_H
#ifndef FOG_TCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77_H
#define FOG_TCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Fog
struct  Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.Fog::enabled
	bool ___enabled_0;
	// System.Boolean UnityEngine.Rendering.PostProcessing.Fog::excludeSkybox
	bool ___excludeSkybox_1;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_excludeSkybox_1() { return static_cast<int32_t>(offsetof(Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77, ___excludeSkybox_1)); }
	inline bool get_excludeSkybox_1() const { return ___excludeSkybox_1; }
	inline bool* get_address_of_excludeSkybox_1() { return &___excludeSkybox_1; }
	inline void set_excludeSkybox_1(bool value)
	{
		___excludeSkybox_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOG_TCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77_H
#ifndef PARAMETEROVERRIDE_T2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C_H
#define PARAMETEROVERRIDE_T2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride
struct  ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.ParameterOverride::overrideState
	bool ___overrideState_0;

public:
	inline static int32_t get_offset_of_overrideState_0() { return static_cast<int32_t>(offsetof(ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C, ___overrideState_0)); }
	inline bool get_overrideState_0() const { return ___overrideState_0; }
	inline bool* get_address_of_overrideState_0() { return &___overrideState_0; }
	inline void set_overrideState_0(bool value)
	{
		___overrideState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_T2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C_H
#ifndef POSTPROCESSEFFECTRENDERER_T4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F_H
#define POSTPROCESSEFFECTRENDERER_T4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer
struct  PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer::m_ResetHistory
	bool ___m_ResetHistory_0;

public:
	inline static int32_t get_offset_of_m_ResetHistory_0() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F, ___m_ResetHistory_0)); }
	inline bool get_m_ResetHistory_0() const { return ___m_ResetHistory_0; }
	inline bool* get_address_of_m_ResetHistory_0() { return &___m_ResetHistory_0; }
	inline void set_m_ResetHistory_0(bool value)
	{
		___m_ResetHistory_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_T4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F_H
#ifndef ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#define ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293  : public RuntimeObject
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293, ___mVuforiaBehaviour_0)); }
	inline VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#ifndef TRACKER_T11C8E7B84615512E8125186CDC5DF90D9D7B58F1_H
#define TRACKER_T11C8E7B84615512E8125186CDC5DF90D9D7B58F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T11C8E7B84615512E8125186CDC5DF90D9D7B58F1_H
#ifndef TRACKERMANAGER_T4E97F9E410AEF74F864668111D4E1AE7B8EA7530_H
#define TRACKERMANAGER_T4E97F9E410AEF74F864668111D4E1AE7B8EA7530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager
struct  TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530  : public RuntimeObject
{
public:
	// Vuforia.StateManager Vuforia.TrackerManager::mStateManager
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1 * ___mStateManager_1;
	// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager::mTrackers
	Dictionary_2_tB17ABD9DDE1EDA398BB0CCCFED6756493B866D03 * ___mTrackers_2;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Type,Vuforia.Tracker>> Vuforia.TrackerManager::mTrackerCreators
	Dictionary_2_t633AA66B66AB19AA7FDC21111B1C74DCE1079775 * ___mTrackerCreators_3;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<Vuforia.Tracker,System.Boolean>> Vuforia.TrackerManager::mTrackerNativeDeinitializers
	Dictionary_2_t7534CF3FD31278CAF9684333AD1265CDF6DE26C9 * ___mTrackerNativeDeinitializers_4;

public:
	inline static int32_t get_offset_of_mStateManager_1() { return static_cast<int32_t>(offsetof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530, ___mStateManager_1)); }
	inline StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1 * get_mStateManager_1() const { return ___mStateManager_1; }
	inline StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1 ** get_address_of_mStateManager_1() { return &___mStateManager_1; }
	inline void set_mStateManager_1(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1 * value)
	{
		___mStateManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___mStateManager_1), value);
	}

	inline static int32_t get_offset_of_mTrackers_2() { return static_cast<int32_t>(offsetof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530, ___mTrackers_2)); }
	inline Dictionary_2_tB17ABD9DDE1EDA398BB0CCCFED6756493B866D03 * get_mTrackers_2() const { return ___mTrackers_2; }
	inline Dictionary_2_tB17ABD9DDE1EDA398BB0CCCFED6756493B866D03 ** get_address_of_mTrackers_2() { return &___mTrackers_2; }
	inline void set_mTrackers_2(Dictionary_2_tB17ABD9DDE1EDA398BB0CCCFED6756493B866D03 * value)
	{
		___mTrackers_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackers_2), value);
	}

	inline static int32_t get_offset_of_mTrackerCreators_3() { return static_cast<int32_t>(offsetof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530, ___mTrackerCreators_3)); }
	inline Dictionary_2_t633AA66B66AB19AA7FDC21111B1C74DCE1079775 * get_mTrackerCreators_3() const { return ___mTrackerCreators_3; }
	inline Dictionary_2_t633AA66B66AB19AA7FDC21111B1C74DCE1079775 ** get_address_of_mTrackerCreators_3() { return &___mTrackerCreators_3; }
	inline void set_mTrackerCreators_3(Dictionary_2_t633AA66B66AB19AA7FDC21111B1C74DCE1079775 * value)
	{
		___mTrackerCreators_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerCreators_3), value);
	}

	inline static int32_t get_offset_of_mTrackerNativeDeinitializers_4() { return static_cast<int32_t>(offsetof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530, ___mTrackerNativeDeinitializers_4)); }
	inline Dictionary_2_t7534CF3FD31278CAF9684333AD1265CDF6DE26C9 * get_mTrackerNativeDeinitializers_4() const { return ___mTrackerNativeDeinitializers_4; }
	inline Dictionary_2_t7534CF3FD31278CAF9684333AD1265CDF6DE26C9 ** get_address_of_mTrackerNativeDeinitializers_4() { return &___mTrackerNativeDeinitializers_4; }
	inline void set_mTrackerNativeDeinitializers_4(Dictionary_2_t7534CF3FD31278CAF9684333AD1265CDF6DE26C9 * value)
	{
		___mTrackerNativeDeinitializers_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerNativeDeinitializers_4), value);
	}
};

struct TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530_StaticFields
{
public:
	// Vuforia.ITrackerManager Vuforia.TrackerManager::mInstance
	RuntimeObject* ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530_StaticFields, ___mInstance_0)); }
	inline RuntimeObject* get_mInstance_0() const { return ___mInstance_0; }
	inline RuntimeObject** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(RuntimeObject* value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERMANAGER_T4E97F9E410AEF74F864668111D4E1AE7B8EA7530_H
#ifndef U3CU3EC_TB15984925A821B29BE48F5B2F3E523834E5E9370_H
#define U3CU3EC_TB15984925A821B29BE48F5B2F3E523834E5E9370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager_<>c
struct  U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields
{
public:
	// Vuforia.TrackerManager_<>c Vuforia.TrackerManager_<>c::<>9
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370 * ___U3CU3E9_0;
	// System.Func`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager_<>c::<>9__8_0
	Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * ___U3CU3E9__8_0_1;
	// System.Func`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager_<>c::<>9__8_1
	Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * ___U3CU3E9__8_1_2;
	// System.Func`2<Vuforia.Tracker,System.Boolean> Vuforia.TrackerManager_<>c::<>9__8_2
	Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * ___U3CU3E9__8_2_3;
	// System.Func`2<Vuforia.Tracker,System.Boolean> Vuforia.TrackerManager_<>c::<>9__8_3
	Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * ___U3CU3E9__8_3_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields, ___U3CU3E9__8_1_2)); }
	inline Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * get_U3CU3E9__8_1_2() const { return ___U3CU3E9__8_1_2; }
	inline Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 ** get_address_of_U3CU3E9__8_1_2() { return &___U3CU3E9__8_1_2; }
	inline void set_U3CU3E9__8_1_2(Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * value)
	{
		___U3CU3E9__8_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields, ___U3CU3E9__8_2_3)); }
	inline Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * get_U3CU3E9__8_2_3() const { return ___U3CU3E9__8_2_3; }
	inline Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 ** get_address_of_U3CU3E9__8_2_3() { return &___U3CU3E9__8_2_3; }
	inline void set_U3CU3E9__8_2_3(Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * value)
	{
		___U3CU3E9__8_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields, ___U3CU3E9__8_3_4)); }
	inline Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * get_U3CU3E9__8_3_4() const { return ___U3CU3E9__8_3_4; }
	inline Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 ** get_address_of_U3CU3E9__8_3_4() { return &___U3CU3E9__8_3_4; }
	inline void set_U3CU3E9__8_3_4(Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * value)
	{
		___U3CU3E9__8_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_3_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB15984925A821B29BE48F5B2F3E523834E5E9370_H
#ifndef NULLUNITYCOMPILEDFACADE_T2C8910980052D89707A62705CBE0E40E17CB149A_H
#define NULLUNITYCOMPILEDFACADE_T2C8910980052D89707A62705CBE0E40E17CB149A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.NullUnityCompiledFacade
struct  NullUnityCompiledFacade_t2C8910980052D89707A62705CBE0E40E17CB149A  : public RuntimeObject
{
public:
	// Vuforia.UnityCompiled.IUnityRenderPipeline Vuforia.UnityCompiled.NullUnityCompiledFacade::mUnityRenderPipeline
	RuntimeObject* ___mUnityRenderPipeline_0;
	// Vuforia.UnityCompiled.IUnityAndroidPermissions Vuforia.UnityCompiled.NullUnityCompiledFacade::mUnityAndroidPermissions
	RuntimeObject* ___mUnityAndroidPermissions_1;

public:
	inline static int32_t get_offset_of_mUnityRenderPipeline_0() { return static_cast<int32_t>(offsetof(NullUnityCompiledFacade_t2C8910980052D89707A62705CBE0E40E17CB149A, ___mUnityRenderPipeline_0)); }
	inline RuntimeObject* get_mUnityRenderPipeline_0() const { return ___mUnityRenderPipeline_0; }
	inline RuntimeObject** get_address_of_mUnityRenderPipeline_0() { return &___mUnityRenderPipeline_0; }
	inline void set_mUnityRenderPipeline_0(RuntimeObject* value)
	{
		___mUnityRenderPipeline_0 = value;
		Il2CppCodeGenWriteBarrier((&___mUnityRenderPipeline_0), value);
	}

	inline static int32_t get_offset_of_mUnityAndroidPermissions_1() { return static_cast<int32_t>(offsetof(NullUnityCompiledFacade_t2C8910980052D89707A62705CBE0E40E17CB149A, ___mUnityAndroidPermissions_1)); }
	inline RuntimeObject* get_mUnityAndroidPermissions_1() const { return ___mUnityAndroidPermissions_1; }
	inline RuntimeObject** get_address_of_mUnityAndroidPermissions_1() { return &___mUnityAndroidPermissions_1; }
	inline void set_mUnityAndroidPermissions_1(RuntimeObject* value)
	{
		___mUnityAndroidPermissions_1 = value;
		Il2CppCodeGenWriteBarrier((&___mUnityAndroidPermissions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLUNITYCOMPILEDFACADE_T2C8910980052D89707A62705CBE0E40E17CB149A_H
#ifndef NULLUNITYANDROIDPERMISSIONS_T34F6B3D453B2B55CFF0BE5DD87DAA32229C7CBBB_H
#define NULLUNITYANDROIDPERMISSIONS_T34F6B3D453B2B55CFF0BE5DD87DAA32229C7CBBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.NullUnityCompiledFacade_NullUnityAndroidPermissions
struct  NullUnityAndroidPermissions_t34F6B3D453B2B55CFF0BE5DD87DAA32229C7CBBB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLUNITYANDROIDPERMISSIONS_T34F6B3D453B2B55CFF0BE5DD87DAA32229C7CBBB_H
#ifndef NULLUNITYRENDERPIPELINE_TA8A5F0FD651D711616AABF06C73715DBB8E569D5_H
#define NULLUNITYRENDERPIPELINE_TA8A5F0FD651D711616AABF06C73715DBB8E569D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.NullUnityCompiledFacade_NullUnityRenderPipeline
struct  NullUnityRenderPipeline_tA8A5F0FD651D711616AABF06C73715DBB8E569D5  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.Camera[]> Vuforia.UnityCompiled.NullUnityCompiledFacade_NullUnityRenderPipeline::BeginFrameRendering
	Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A * ___BeginFrameRendering_0;
	// System.Action`1<UnityEngine.Camera> Vuforia.UnityCompiled.NullUnityCompiledFacade_NullUnityRenderPipeline::BeginCameraRendering
	Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C * ___BeginCameraRendering_1;

public:
	inline static int32_t get_offset_of_BeginFrameRendering_0() { return static_cast<int32_t>(offsetof(NullUnityRenderPipeline_tA8A5F0FD651D711616AABF06C73715DBB8E569D5, ___BeginFrameRendering_0)); }
	inline Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A * get_BeginFrameRendering_0() const { return ___BeginFrameRendering_0; }
	inline Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A ** get_address_of_BeginFrameRendering_0() { return &___BeginFrameRendering_0; }
	inline void set_BeginFrameRendering_0(Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A * value)
	{
		___BeginFrameRendering_0 = value;
		Il2CppCodeGenWriteBarrier((&___BeginFrameRendering_0), value);
	}

	inline static int32_t get_offset_of_BeginCameraRendering_1() { return static_cast<int32_t>(offsetof(NullUnityRenderPipeline_tA8A5F0FD651D711616AABF06C73715DBB8E569D5, ___BeginCameraRendering_1)); }
	inline Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C * get_BeginCameraRendering_1() const { return ___BeginCameraRendering_1; }
	inline Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C ** get_address_of_BeginCameraRendering_1() { return &___BeginCameraRendering_1; }
	inline void set_BeginCameraRendering_1(Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C * value)
	{
		___BeginCameraRendering_1 = value;
		Il2CppCodeGenWriteBarrier((&___BeginCameraRendering_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLUNITYRENDERPIPELINE_TA8A5F0FD651D711616AABF06C73715DBB8E569D5_H
#ifndef UNITYCOMPILEDFACADE_TF06053FC3EC0D6850A60CCF314ED1A2A820E7CBD_H
#define UNITYCOMPILEDFACADE_TF06053FC3EC0D6850A60CCF314ED1A2A820E7CBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.UnityCompiledFacade
struct  UnityCompiledFacade_tF06053FC3EC0D6850A60CCF314ED1A2A820E7CBD  : public RuntimeObject
{
public:

public:
};

struct UnityCompiledFacade_tF06053FC3EC0D6850A60CCF314ED1A2A820E7CBD_StaticFields
{
public:
	// Vuforia.UnityCompiled.IUnityCompiledFacade Vuforia.UnityCompiled.UnityCompiledFacade::sInstance
	RuntimeObject* ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(UnityCompiledFacade_tF06053FC3EC0D6850A60CCF314ED1A2A820E7CBD_StaticFields, ___sInstance_0)); }
	inline RuntimeObject* get_sInstance_0() const { return ___sInstance_0; }
	inline RuntimeObject** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(RuntimeObject* value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCOMPILEDFACADE_TF06053FC3EC0D6850A60CCF314ED1A2A820E7CBD_H
#ifndef U3CU3EC_T0D9CA9C8F7A76A99A794019E01671BDCA16F9609_H
#define U3CU3EC_T0D9CA9C8F7A76A99A794019E01671BDCA16F9609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_<>c
struct  U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields
{
public:
	// Vuforia.VuforiaRenderer_<>c Vuforia.VuforiaRenderer_<>c::<>9
	U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609 * ___U3CU3E9_0;
	// System.Func`1<System.Boolean> Vuforia.VuforiaRenderer_<>c::<>9__18_0
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0D9CA9C8F7A76A99A794019E01671BDCA16F9609_H
#ifndef U3CU3EC_T6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_H
#define U3CU3EC_T6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities_<>c
struct  U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_StaticFields
{
public:
	// Vuforia.VuforiaRuntimeUtilities_<>c Vuforia.VuforiaRuntimeUtilities_<>c::<>9
	U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427 * ___U3CU3E9_0;
	// System.Func`2<Vuforia.TargetFinder,System.Boolean> Vuforia.VuforiaRuntimeUtilities_<>c::<>9__31_0
	Func_2_tC443A4E5D2EA3B2E46141E8CFA7F0F891B9851C2 * ___U3CU3E9__31_0_1;
	// System.Func`2<Vuforia.TargetFinder,System.Boolean> Vuforia.VuforiaRuntimeUtilities_<>c::<>9__31_1
	Func_2_tC443A4E5D2EA3B2E46141E8CFA7F0F891B9851C2 * ___U3CU3E9__31_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_StaticFields, ___U3CU3E9__31_0_1)); }
	inline Func_2_tC443A4E5D2EA3B2E46141E8CFA7F0F891B9851C2 * get_U3CU3E9__31_0_1() const { return ___U3CU3E9__31_0_1; }
	inline Func_2_tC443A4E5D2EA3B2E46141E8CFA7F0F891B9851C2 ** get_address_of_U3CU3E9__31_0_1() { return &___U3CU3E9__31_0_1; }
	inline void set_U3CU3E9__31_0_1(Func_2_tC443A4E5D2EA3B2E46141E8CFA7F0F891B9851C2 * value)
	{
		___U3CU3E9__31_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_StaticFields, ___U3CU3E9__31_1_2)); }
	inline Func_2_tC443A4E5D2EA3B2E46141E8CFA7F0F891B9851C2 * get_U3CU3E9__31_1_2() const { return ___U3CU3E9__31_1_2; }
	inline Func_2_tC443A4E5D2EA3B2E46141E8CFA7F0F891B9851C2 ** get_address_of_U3CU3E9__31_1_2() { return &___U3CU3E9__31_1_2; }
	inline void set_U3CU3E9__31_1_2(Func_2_tC443A4E5D2EA3B2E46141E8CFA7F0F891B9851C2 * value)
	{
		___U3CU3E9__31_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_H
#ifndef GLOBALVARS_TEB729934896F3AE63A63EBE07D7FBD183A0B22E6_H
#define GLOBALVARS_TEB729934896F3AE63A63EBE07D7FBD183A0B22E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities_GlobalVars
struct  GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6  : public RuntimeObject
{
public:

public:
};

struct GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6_StaticFields
{
public:
	// System.String Vuforia.VuforiaRuntimeUtilities_GlobalVars::GLTF_ASSET_LOCATION
	String_t* ___GLTF_ASSET_LOCATION_0;

public:
	inline static int32_t get_offset_of_GLTF_ASSET_LOCATION_0() { return static_cast<int32_t>(offsetof(GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6_StaticFields, ___GLTF_ASSET_LOCATION_0)); }
	inline String_t* get_GLTF_ASSET_LOCATION_0() const { return ___GLTF_ASSET_LOCATION_0; }
	inline String_t** get_address_of_GLTF_ASSET_LOCATION_0() { return &___GLTF_ASSET_LOCATION_0; }
	inline void set_GLTF_ASSET_LOCATION_0(String_t* value)
	{
		___GLTF_ASSET_LOCATION_0 = value;
		Il2CppCodeGenWriteBarrier((&___GLTF_ASSET_LOCATION_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALVARS_TEB729934896F3AE63A63EBE07D7FBD183A0B22E6_H
#ifndef VUFORIAUNITY_T0642B0D82DCCD1F058C8133A227319530273E77F_H
#define VUFORIAUNITY_T0642B0D82DCCD1F058C8133A227319530273E77F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity
struct  VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F  : public RuntimeObject
{
public:

public:
};

struct VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields
{
public:
	// Vuforia.IHoloLensApiAbstraction Vuforia.VuforiaUnity::mHoloLensApiAbstraction
	RuntimeObject* ___mHoloLensApiAbstraction_0;
	// System.Boolean Vuforia.VuforiaUnity::mRendererDirty
	bool ___mRendererDirty_1;
	// System.Int32 Vuforia.VuforiaUnity::mWrapperType
	int32_t ___mWrapperType_2;

public:
	inline static int32_t get_offset_of_mHoloLensApiAbstraction_0() { return static_cast<int32_t>(offsetof(VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields, ___mHoloLensApiAbstraction_0)); }
	inline RuntimeObject* get_mHoloLensApiAbstraction_0() const { return ___mHoloLensApiAbstraction_0; }
	inline RuntimeObject** get_address_of_mHoloLensApiAbstraction_0() { return &___mHoloLensApiAbstraction_0; }
	inline void set_mHoloLensApiAbstraction_0(RuntimeObject* value)
	{
		___mHoloLensApiAbstraction_0 = value;
		Il2CppCodeGenWriteBarrier((&___mHoloLensApiAbstraction_0), value);
	}

	inline static int32_t get_offset_of_mRendererDirty_1() { return static_cast<int32_t>(offsetof(VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields, ___mRendererDirty_1)); }
	inline bool get_mRendererDirty_1() const { return ___mRendererDirty_1; }
	inline bool* get_address_of_mRendererDirty_1() { return &___mRendererDirty_1; }
	inline void set_mRendererDirty_1(bool value)
	{
		___mRendererDirty_1 = value;
	}

	inline static int32_t get_offset_of_mWrapperType_2() { return static_cast<int32_t>(offsetof(VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields, ___mWrapperType_2)); }
	inline int32_t get_mWrapperType_2() const { return ___mWrapperType_2; }
	inline int32_t* get_address_of_mWrapperType_2() { return &___mWrapperType_2; }
	inline void set_mWrapperType_2(int32_t value)
	{
		___mWrapperType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAUNITY_T0642B0D82DCCD1F058C8133A227319530273E77F_H
#ifndef U3CU3EC_T7F5A80A4ED3D4E5058582D2682AC998995E82FDB_H
#define U3CU3EC_T7F5A80A4ED3D4E5058582D2682AC998995E82FDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController_<>c
struct  U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields
{
public:
	// Vuforia.WebCamARController_<>c Vuforia.WebCamARController_<>c::<>9
	U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB * ___U3CU3E9_0;
	// System.Func`3<System.String,Vuforia.WebCamProfile_ProfileData,Vuforia.IWebCamTexAdaptor> Vuforia.WebCamARController_<>c::<>9__6_0
	Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T7F5A80A4ED3D4E5058582D2682AC998995E82FDB_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_TD7B241978857C817DC5F90FD7E79EBA548903A7E_H
#define __STATICARRAYINITTYPESIZEU3D24_TD7B241978857C817DC5F90FD7E79EBA548903A7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24
struct  __StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_TD7B241978857C817DC5F90FD7E79EBA548903A7E_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef LEVEL_T21B0259BF8468CA4457E5880ADBEF78D8CAFEC7C_H
#define LEVEL_T21B0259BF8468CA4457E5880ADBEF78D8CAFEC7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.BloomRenderer_Level
struct  Level_t21B0259BF8468CA4457E5880ADBEF78D8CAFEC7C 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.BloomRenderer_Level::down
	int32_t ___down_0;
	// System.Int32 UnityEngine.Rendering.PostProcessing.BloomRenderer_Level::up
	int32_t ___up_1;

public:
	inline static int32_t get_offset_of_down_0() { return static_cast<int32_t>(offsetof(Level_t21B0259BF8468CA4457E5880ADBEF78D8CAFEC7C, ___down_0)); }
	inline int32_t get_down_0() const { return ___down_0; }
	inline int32_t* get_address_of_down_0() { return &___down_0; }
	inline void set_down_0(int32_t value)
	{
		___down_0 = value;
	}

	inline static int32_t get_offset_of_up_1() { return static_cast<int32_t>(offsetof(Level_t21B0259BF8468CA4457E5880ADBEF78D8CAFEC7C, ___up_1)); }
	inline int32_t get_up_1() const { return ___up_1; }
	inline int32_t* get_address_of_up_1() { return &___up_1; }
	inline void set_up_1(int32_t value)
	{
		___up_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T21B0259BF8468CA4457E5880ADBEF78D8CAFEC7C_H
#ifndef POSTPROCESSEFFECTRENDERER_1_TA4B4F4D142ABA9F7DA9626D6D032ADEF372B7F6F_H
#define POSTPROCESSEFFECTRENDERER_1_TA4B4F4D142ABA9F7DA9626D6D032ADEF372B7F6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.AmbientOcclusion>
struct  PostProcessEffectRenderer_1_tA4B4F4D142ABA9F7DA9626D6D032ADEF372B7F6F  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_tA4B4F4D142ABA9F7DA9626D6D032ADEF372B7F6F, ___U3CsettingsU3Ek__BackingField_1)); }
	inline AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_TA4B4F4D142ABA9F7DA9626D6D032ADEF372B7F6F_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T421FE74B3C731B2EFCC650D63668F64472921F90_H
#define POSTPROCESSEFFECTRENDERER_1_T421FE74B3C731B2EFCC650D63668F64472921F90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.AutoExposure>
struct  PostProcessEffectRenderer_1_t421FE74B3C731B2EFCC650D63668F64472921F90  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t421FE74B3C731B2EFCC650D63668F64472921F90, ___U3CsettingsU3Ek__BackingField_1)); }
	inline AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T421FE74B3C731B2EFCC650D63668F64472921F90_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T8AFAC0C6FAEB7D84422403D0C918A3E309182094_H
#define POSTPROCESSEFFECTRENDERER_1_T8AFAC0C6FAEB7D84422403D0C918A3E309182094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.Bloom>
struct  PostProcessEffectRenderer_1_t8AFAC0C6FAEB7D84422403D0C918A3E309182094  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t8AFAC0C6FAEB7D84422403D0C918A3E309182094, ___U3CsettingsU3Ek__BackingField_1)); }
	inline Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T8AFAC0C6FAEB7D84422403D0C918A3E309182094_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T07251E442BB573DB59ACA6E8F4BBBD59A896FEA6_H
#define POSTPROCESSEFFECTRENDERER_1_T07251E442BB573DB59ACA6E8F4BBBD59A896FEA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.ChromaticAberration>
struct  PostProcessEffectRenderer_1_t07251E442BB573DB59ACA6E8F4BBBD59A896FEA6  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t07251E442BB573DB59ACA6E8F4BBBD59A896FEA6, ___U3CsettingsU3Ek__BackingField_1)); }
	inline ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T07251E442BB573DB59ACA6E8F4BBBD59A896FEA6_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T6188E3DAECC7E578F868303F8E7C3A54A9951877_H
#define POSTPROCESSEFFECTRENDERER_1_T6188E3DAECC7E578F868303F8E7C3A54A9951877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.ColorGrading>
struct  PostProcessEffectRenderer_1_t6188E3DAECC7E578F868303F8E7C3A54A9951877  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t6188E3DAECC7E578F868303F8E7C3A54A9951877, ___U3CsettingsU3Ek__BackingField_1)); }
	inline ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T6188E3DAECC7E578F868303F8E7C3A54A9951877_H
#ifndef POSTPROCESSEFFECTRENDERER_1_T2596D39CFE7ECFD7706255E2D37D2A640FCBE43C_H
#define POSTPROCESSEFFECTRENDERER_1_T2596D39CFE7ECFD7706255E2D37D2A640FCBE43C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.DepthOfField>
struct  PostProcessEffectRenderer_1_t2596D39CFE7ECFD7706255E2D37D2A640FCBE43C  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_t2596D39CFE7ECFD7706255E2D37D2A640FCBE43C, ___U3CsettingsU3Ek__BackingField_1)); }
	inline DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_T2596D39CFE7ECFD7706255E2D37D2A640FCBE43C_H
#ifndef POSTPROCESSEFFECTRENDERER_1_TC90D663958D767936D33668435B04BBA68147B5F_H
#define POSTPROCESSEFFECTRENDERER_1_TC90D663958D767936D33668435B04BBA68147B5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1<UnityEngine.Rendering.PostProcessing.Grain>
struct  PostProcessEffectRenderer_1_tC90D663958D767936D33668435B04BBA68147B5F  : public PostProcessEffectRenderer_t4989C38AAABA92A01D597B3B6F25E8BDB63C7A2F
{
public:
	// T UnityEngine.Rendering.PostProcessing.PostProcessEffectRenderer`1::<settings>k__BackingField
	Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6 * ___U3CsettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessEffectRenderer_1_tC90D663958D767936D33668435B04BBA68147B5F, ___U3CsettingsU3Ek__BackingField_1)); }
	inline Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6 * get_U3CsettingsU3Ek__BackingField_1() const { return ___U3CsettingsU3Ek__BackingField_1; }
	inline Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6 ** get_address_of_U3CsettingsU3Ek__BackingField_1() { return &___U3CsettingsU3Ek__BackingField_1; }
	inline void set_U3CsettingsU3Ek__BackingField_1(Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6 * value)
	{
		___U3CsettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsettingsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTRENDERER_1_TC90D663958D767936D33668435B04BBA68147B5F_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef OBJECTTRACKER_TC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E_H
#define OBJECTTRACKER_TC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTracker
struct  ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E  : public Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1
{
public:
	// System.Collections.Generic.List`1<Vuforia.DataSet> Vuforia.ObjectTracker::mActiveDataSets
	List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * ___mActiveDataSets_1;
	// System.Collections.Generic.List`1<Vuforia.DataSet> Vuforia.ObjectTracker::mDataSets
	List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * ___mDataSets_2;
	// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.TargetFinder> Vuforia.ObjectTracker::mTargetFinders
	Dictionary_2_t97145FAA8A62E3A6895B2E7A97B7BB62B2F6B972 * ___mTargetFinders_3;
	// Vuforia.ImageTargetBuilder Vuforia.ObjectTracker::mImageTargetBuilder
	ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755 * ___mImageTargetBuilder_4;

public:
	inline static int32_t get_offset_of_mActiveDataSets_1() { return static_cast<int32_t>(offsetof(ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E, ___mActiveDataSets_1)); }
	inline List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * get_mActiveDataSets_1() const { return ___mActiveDataSets_1; }
	inline List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 ** get_address_of_mActiveDataSets_1() { return &___mActiveDataSets_1; }
	inline void set_mActiveDataSets_1(List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * value)
	{
		___mActiveDataSets_1 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveDataSets_1), value);
	}

	inline static int32_t get_offset_of_mDataSets_2() { return static_cast<int32_t>(offsetof(ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E, ___mDataSets_2)); }
	inline List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * get_mDataSets_2() const { return ___mDataSets_2; }
	inline List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 ** get_address_of_mDataSets_2() { return &___mDataSets_2; }
	inline void set_mDataSets_2(List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * value)
	{
		___mDataSets_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSets_2), value);
	}

	inline static int32_t get_offset_of_mTargetFinders_3() { return static_cast<int32_t>(offsetof(ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E, ___mTargetFinders_3)); }
	inline Dictionary_2_t97145FAA8A62E3A6895B2E7A97B7BB62B2F6B972 * get_mTargetFinders_3() const { return ___mTargetFinders_3; }
	inline Dictionary_2_t97145FAA8A62E3A6895B2E7A97B7BB62B2F6B972 ** get_address_of_mTargetFinders_3() { return &___mTargetFinders_3; }
	inline void set_mTargetFinders_3(Dictionary_2_t97145FAA8A62E3A6895B2E7A97B7BB62B2F6B972 * value)
	{
		___mTargetFinders_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetFinders_3), value);
	}

	inline static int32_t get_offset_of_mImageTargetBuilder_4() { return static_cast<int32_t>(offsetof(ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E, ___mImageTargetBuilder_4)); }
	inline ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755 * get_mImageTargetBuilder_4() const { return ___mImageTargetBuilder_4; }
	inline ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755 ** get_address_of_mImageTargetBuilder_4() { return &___mImageTargetBuilder_4; }
	inline void set_mImageTargetBuilder_4(ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755 * value)
	{
		___mImageTargetBuilder_4 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTargetBuilder_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTRACKER_TC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E_H
#ifndef RECTANGLEDATA_TA9B66ACDEB48962ADACFB8D18129ED016D732BE9_H
#define RECTANGLEDATA_TA9B66ACDEB48962ADACFB8D18129ED016D732BE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RectangleData
#pragma pack(push, tp, 1)
struct  RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9 
{
public:
	// System.Single Vuforia.RectangleData::leftTopX
	float ___leftTopX_0;
	// System.Single Vuforia.RectangleData::leftTopY
	float ___leftTopY_1;
	// System.Single Vuforia.RectangleData::rightBottomX
	float ___rightBottomX_2;
	// System.Single Vuforia.RectangleData::rightBottomY
	float ___rightBottomY_3;

public:
	inline static int32_t get_offset_of_leftTopX_0() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___leftTopX_0)); }
	inline float get_leftTopX_0() const { return ___leftTopX_0; }
	inline float* get_address_of_leftTopX_0() { return &___leftTopX_0; }
	inline void set_leftTopX_0(float value)
	{
		___leftTopX_0 = value;
	}

	inline static int32_t get_offset_of_leftTopY_1() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___leftTopY_1)); }
	inline float get_leftTopY_1() const { return ___leftTopY_1; }
	inline float* get_address_of_leftTopY_1() { return &___leftTopY_1; }
	inline void set_leftTopY_1(float value)
	{
		___leftTopY_1 = value;
	}

	inline static int32_t get_offset_of_rightBottomX_2() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___rightBottomX_2)); }
	inline float get_rightBottomX_2() const { return ___rightBottomX_2; }
	inline float* get_address_of_rightBottomX_2() { return &___rightBottomX_2; }
	inline void set_rightBottomX_2(float value)
	{
		___rightBottomX_2 = value;
	}

	inline static int32_t get_offset_of_rightBottomY_3() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___rightBottomY_3)); }
	inline float get_rightBottomY_3() const { return ___rightBottomY_3; }
	inline float* get_address_of_rightBottomY_3() { return &___rightBottomY_3; }
	inline void set_rightBottomY_3(float value)
	{
		___rightBottomY_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLEDATA_TA9B66ACDEB48962ADACFB8D18129ED016D732BE9_H
#ifndef SIMPLETARGETDATA_T1D59320853F5EB4E7773DE8AF4E146D4B384CF3D_H
#define SIMPLETARGETDATA_T1D59320853F5EB4E7773DE8AF4E146D4B384CF3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SimpleTargetData
#pragma pack(push, tp, 1)
struct  SimpleTargetData_t1D59320853F5EB4E7773DE8AF4E146D4B384CF3D 
{
public:
	// System.Int32 Vuforia.SimpleTargetData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.SimpleTargetData::unused
	int32_t ___unused_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SimpleTargetData_t1D59320853F5EB4E7773DE8AF4E146D4B384CF3D, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_unused_1() { return static_cast<int32_t>(offsetof(SimpleTargetData_t1D59320853F5EB4E7773DE8AF4E146D4B384CF3D, ___unused_1)); }
	inline int32_t get_unused_1() const { return ___unused_1; }
	inline int32_t* get_address_of_unused_1() { return &___unused_1; }
	inline void set_unused_1(int32_t value)
	{
		___unused_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETARGETDATA_T1D59320853F5EB4E7773DE8AF4E146D4B384CF3D_H
#ifndef TARGETFINDERSTATE_TFED6EAB7D0324D88E902EC9988067C11E8B8C11D_H
#define TARGETFINDERSTATE_TFED6EAB7D0324D88E902EC9988067C11E8B8C11D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder_TargetFinderState
#pragma pack(push, tp, 1)
struct  TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D 
{
public:
	// System.Int32 Vuforia.TargetFinder_TargetFinderState::IsRequesting
	int32_t ___IsRequesting_0;
	// System.Int32 Vuforia.TargetFinder_TargetFinderState::UpdateState
	int32_t ___UpdateState_1;

public:
	inline static int32_t get_offset_of_IsRequesting_0() { return static_cast<int32_t>(offsetof(TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D, ___IsRequesting_0)); }
	inline int32_t get_IsRequesting_0() const { return ___IsRequesting_0; }
	inline int32_t* get_address_of_IsRequesting_0() { return &___IsRequesting_0; }
	inline void set_IsRequesting_0(int32_t value)
	{
		___IsRequesting_0 = value;
	}

	inline static int32_t get_offset_of_UpdateState_1() { return static_cast<int32_t>(offsetof(TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D, ___UpdateState_1)); }
	inline int32_t get_UpdateState_1() const { return ___UpdateState_1; }
	inline int32_t* get_address_of_UpdateState_1() { return &___UpdateState_1; }
	inline void set_UpdateState_1(int32_t value)
	{
		___UpdateState_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFINDERSTATE_TFED6EAB7D0324D88E902EC9988067C11E8B8C11D_H
#ifndef TRACKABLEIDPAIR_T0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F_H
#define TRACKABLEIDPAIR_T0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager_TrackableIdPair
struct  TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F 
{
public:
	// System.Int32 Vuforia.VuforiaManager_TrackableIdPair::TrackableId
	int32_t ___TrackableId_0;
	// System.Int32 Vuforia.VuforiaManager_TrackableIdPair::ResultId
	int32_t ___ResultId_1;

public:
	inline static int32_t get_offset_of_TrackableId_0() { return static_cast<int32_t>(offsetof(TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F, ___TrackableId_0)); }
	inline int32_t get_TrackableId_0() const { return ___TrackableId_0; }
	inline int32_t* get_address_of_TrackableId_0() { return &___TrackableId_0; }
	inline void set_TrackableId_0(int32_t value)
	{
		___TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_ResultId_1() { return static_cast<int32_t>(offsetof(TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F, ___ResultId_1)); }
	inline int32_t get_ResultId_1() const { return ___ResultId_1; }
	inline int32_t* get_address_of_ResultId_1() { return &___ResultId_1; }
	inline void set_ResultId_1(int32_t value)
	{
		___ResultId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIDPAIR_T0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F_H
#ifndef VEC2I_T42F341F398E3117176C37CAAAF2D29943EA9FCF5_H
#define VEC2I_T42F341F398E3117176C37CAAAF2D29943EA9FCF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer_Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer_Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T42F341F398E3117176C37CAAAF2D29943EA9FCF5_H
#ifndef WEBCAMARCONTROLLER_T927205B62F0EDCE72569FA4CA0D034E6BC804D1A_H
#define WEBCAMARCONTROLLER_T927205B62F0EDCE72569FA4CA0D034E6BC804D1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController
struct  WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A  : public ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293
{
public:
	// System.Int32 Vuforia.WebCamARController::RenderTextureLayer
	int32_t ___RenderTextureLayer_1;
	// System.String Vuforia.WebCamARController::mDeviceNameSetInEditor
	String_t* ___mDeviceNameSetInEditor_2;
	// Vuforia.WebCam Vuforia.WebCamARController::mWebCamImpl
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2 * ___mWebCamImpl_3;
	// System.Func`3<System.String,Vuforia.WebCamProfile_ProfileData,Vuforia.IWebCamTexAdaptor> Vuforia.WebCamARController::mWebCamTexAdaptorProvider
	Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * ___mWebCamTexAdaptorProvider_4;

public:
	inline static int32_t get_offset_of_RenderTextureLayer_1() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A, ___RenderTextureLayer_1)); }
	inline int32_t get_RenderTextureLayer_1() const { return ___RenderTextureLayer_1; }
	inline int32_t* get_address_of_RenderTextureLayer_1() { return &___RenderTextureLayer_1; }
	inline void set_RenderTextureLayer_1(int32_t value)
	{
		___RenderTextureLayer_1 = value;
	}

	inline static int32_t get_offset_of_mDeviceNameSetInEditor_2() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A, ___mDeviceNameSetInEditor_2)); }
	inline String_t* get_mDeviceNameSetInEditor_2() const { return ___mDeviceNameSetInEditor_2; }
	inline String_t** get_address_of_mDeviceNameSetInEditor_2() { return &___mDeviceNameSetInEditor_2; }
	inline void set_mDeviceNameSetInEditor_2(String_t* value)
	{
		___mDeviceNameSetInEditor_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceNameSetInEditor_2), value);
	}

	inline static int32_t get_offset_of_mWebCamImpl_3() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A, ___mWebCamImpl_3)); }
	inline WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2 * get_mWebCamImpl_3() const { return ___mWebCamImpl_3; }
	inline WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2 ** get_address_of_mWebCamImpl_3() { return &___mWebCamImpl_3; }
	inline void set_mWebCamImpl_3(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2 * value)
	{
		___mWebCamImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamImpl_3), value);
	}

	inline static int32_t get_offset_of_mWebCamTexAdaptorProvider_4() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A, ___mWebCamTexAdaptorProvider_4)); }
	inline Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * get_mWebCamTexAdaptorProvider_4() const { return ___mWebCamTexAdaptorProvider_4; }
	inline Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC ** get_address_of_mWebCamTexAdaptorProvider_4() { return &___mWebCamTexAdaptorProvider_4; }
	inline void set_mWebCamTexAdaptorProvider_4(Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * value)
	{
		___mWebCamTexAdaptorProvider_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamTexAdaptorProvider_4), value);
	}
};

struct WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields
{
public:
	// Vuforia.WebCamARController Vuforia.WebCamARController::mInstance
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A * ___mInstance_5;
	// System.Object Vuforia.WebCamARController::mPadlock
	RuntimeObject * ___mPadlock_6;

public:
	inline static int32_t get_offset_of_mInstance_5() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields, ___mInstance_5)); }
	inline WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A * get_mInstance_5() const { return ___mInstance_5; }
	inline WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A ** get_address_of_mInstance_5() { return &___mInstance_5; }
	inline void set_mInstance_5(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A * value)
	{
		___mInstance_5 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_5), value);
	}

	inline static int32_t get_offset_of_mPadlock_6() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields, ___mPadlock_6)); }
	inline RuntimeObject * get_mPadlock_6() const { return ___mPadlock_6; }
	inline RuntimeObject ** get_address_of_mPadlock_6() { return &___mPadlock_6; }
	inline void set_mPadlock_6(RuntimeObject * value)
	{
		___mPadlock_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMARCONTROLLER_T927205B62F0EDCE72569FA4CA0D034E6BC804D1A_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T6817CFECEB78EAB6A13B460969C0CEAD9F899C47_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T6817CFECEB78EAB6A13B460969C0CEAD9F899C47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::898C2022A0C02FCE602BF05E1C09BD48301606E5
	__StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E  ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0;

public:
	inline static int32_t get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47_StaticFields, ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0)); }
	inline __StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E  get_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() const { return ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline __StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E * get_address_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return &___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline void set_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(__StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E  value)
	{
		___898C2022A0C02FCE602BF05E1C09BD48301606E5_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T6817CFECEB78EAB6A13B460969C0CEAD9F899C47_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef AMBIENTOCCLUSIONMODE_TC03A1F9EA851520397D48B79A7853C2E3E15E0B1_H
#define AMBIENTOCCLUSIONMODE_TC03A1F9EA851520397D48B79A7853C2E3E15E0B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusionMode
struct  AmbientOcclusionMode_tC03A1F9EA851520397D48B79A7853C2E3E15E0B1 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.AmbientOcclusionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AmbientOcclusionMode_tC03A1F9EA851520397D48B79A7853C2E3E15E0B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONMODE_TC03A1F9EA851520397D48B79A7853C2E3E15E0B1_H
#ifndef AMBIENTOCCLUSIONQUALITY_T9A7185FB3F2CEA669147614CF1B96FAAD38A7443_H
#define AMBIENTOCCLUSIONQUALITY_T9A7185FB3F2CEA669147614CF1B96FAAD38A7443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusionQuality
struct  AmbientOcclusionQuality_t9A7185FB3F2CEA669147614CF1B96FAAD38A7443 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.AmbientOcclusionQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AmbientOcclusionQuality_t9A7185FB3F2CEA669147614CF1B96FAAD38A7443, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONQUALITY_T9A7185FB3F2CEA669147614CF1B96FAAD38A7443_H
#ifndef AMBIENTOCCLUSIONRENDERER_T0F9B973F0C092035B89AA216AE3F4539113C98E1_H
#define AMBIENTOCCLUSIONRENDERER_T0F9B973F0C092035B89AA216AE3F4539113C98E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusionRenderer
struct  AmbientOcclusionRenderer_t0F9B973F0C092035B89AA216AE3F4539113C98E1  : public PostProcessEffectRenderer_1_tA4B4F4D142ABA9F7DA9626D6D032ADEF372B7F6F
{
public:
	// UnityEngine.Rendering.PostProcessing.IAmbientOcclusionMethod[] UnityEngine.Rendering.PostProcessing.AmbientOcclusionRenderer::m_Methods
	IAmbientOcclusionMethodU5BU5D_t0CB2EB32F63F29733C83C7ABEF672EFB2AA58A01* ___m_Methods_2;

public:
	inline static int32_t get_offset_of_m_Methods_2() { return static_cast<int32_t>(offsetof(AmbientOcclusionRenderer_t0F9B973F0C092035B89AA216AE3F4539113C98E1, ___m_Methods_2)); }
	inline IAmbientOcclusionMethodU5BU5D_t0CB2EB32F63F29733C83C7ABEF672EFB2AA58A01* get_m_Methods_2() const { return ___m_Methods_2; }
	inline IAmbientOcclusionMethodU5BU5D_t0CB2EB32F63F29733C83C7ABEF672EFB2AA58A01** get_address_of_m_Methods_2() { return &___m_Methods_2; }
	inline void set_m_Methods_2(IAmbientOcclusionMethodU5BU5D_t0CB2EB32F63F29733C83C7ABEF672EFB2AA58A01* value)
	{
		___m_Methods_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Methods_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONRENDERER_T0F9B973F0C092035B89AA216AE3F4539113C98E1_H
#ifndef AUTOEXPOSURERENDERER_TA09D22C0C405C77FE356AAC93097410A181CF5FF_H
#define AUTOEXPOSURERENDERER_TA09D22C0C405C77FE356AAC93097410A181CF5FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AutoExposureRenderer
struct  AutoExposureRenderer_tA09D22C0C405C77FE356AAC93097410A181CF5FF  : public PostProcessEffectRenderer_1_t421FE74B3C731B2EFCC650D63668F64472921F90
{
public:
	// UnityEngine.RenderTexture[][] UnityEngine.Rendering.PostProcessing.AutoExposureRenderer::m_AutoExposurePool
	RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF* ___m_AutoExposurePool_2;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.AutoExposureRenderer::m_AutoExposurePingPong
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_AutoExposurePingPong_3;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.AutoExposureRenderer::m_CurrentAutoExposure
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___m_CurrentAutoExposure_4;

public:
	inline static int32_t get_offset_of_m_AutoExposurePool_2() { return static_cast<int32_t>(offsetof(AutoExposureRenderer_tA09D22C0C405C77FE356AAC93097410A181CF5FF, ___m_AutoExposurePool_2)); }
	inline RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF* get_m_AutoExposurePool_2() const { return ___m_AutoExposurePool_2; }
	inline RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF** get_address_of_m_AutoExposurePool_2() { return &___m_AutoExposurePool_2; }
	inline void set_m_AutoExposurePool_2(RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF* value)
	{
		___m_AutoExposurePool_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AutoExposurePool_2), value);
	}

	inline static int32_t get_offset_of_m_AutoExposurePingPong_3() { return static_cast<int32_t>(offsetof(AutoExposureRenderer_tA09D22C0C405C77FE356AAC93097410A181CF5FF, ___m_AutoExposurePingPong_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_AutoExposurePingPong_3() const { return ___m_AutoExposurePingPong_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_AutoExposurePingPong_3() { return &___m_AutoExposurePingPong_3; }
	inline void set_m_AutoExposurePingPong_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_AutoExposurePingPong_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AutoExposurePingPong_3), value);
	}

	inline static int32_t get_offset_of_m_CurrentAutoExposure_4() { return static_cast<int32_t>(offsetof(AutoExposureRenderer_tA09D22C0C405C77FE356AAC93097410A181CF5FF, ___m_CurrentAutoExposure_4)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_m_CurrentAutoExposure_4() const { return ___m_CurrentAutoExposure_4; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_m_CurrentAutoExposure_4() { return &___m_CurrentAutoExposure_4; }
	inline void set_m_CurrentAutoExposure_4(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___m_CurrentAutoExposure_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentAutoExposure_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOEXPOSURERENDERER_TA09D22C0C405C77FE356AAC93097410A181CF5FF_H
#ifndef BLOOMRENDERER_TBB0EB5B9D5AF88501666D2F5B96C74AE4D6BD491_H
#define BLOOMRENDERER_TBB0EB5B9D5AF88501666D2F5B96C74AE4D6BD491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.BloomRenderer
struct  BloomRenderer_tBB0EB5B9D5AF88501666D2F5B96C74AE4D6BD491  : public PostProcessEffectRenderer_1_t8AFAC0C6FAEB7D84422403D0C918A3E309182094
{
public:
	// UnityEngine.Rendering.PostProcessing.BloomRenderer_Level[] UnityEngine.Rendering.PostProcessing.BloomRenderer::m_Pyramid
	LevelU5BU5D_tDFD4CD93E3C076B6A1726E0A34995770431AF9D0* ___m_Pyramid_2;

public:
	inline static int32_t get_offset_of_m_Pyramid_2() { return static_cast<int32_t>(offsetof(BloomRenderer_tBB0EB5B9D5AF88501666D2F5B96C74AE4D6BD491, ___m_Pyramid_2)); }
	inline LevelU5BU5D_tDFD4CD93E3C076B6A1726E0A34995770431AF9D0* get_m_Pyramid_2() const { return ___m_Pyramid_2; }
	inline LevelU5BU5D_tDFD4CD93E3C076B6A1726E0A34995770431AF9D0** get_address_of_m_Pyramid_2() { return &___m_Pyramid_2; }
	inline void set_m_Pyramid_2(LevelU5BU5D_tDFD4CD93E3C076B6A1726E0A34995770431AF9D0* value)
	{
		___m_Pyramid_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pyramid_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMRENDERER_TBB0EB5B9D5AF88501666D2F5B96C74AE4D6BD491_H
#ifndef PASS_T11A8ABEA4D0C022520585CD85A89C18846D0F9F8_H
#define PASS_T11A8ABEA4D0C022520585CD85A89C18846D0F9F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.BloomRenderer_Pass
struct  Pass_t11A8ABEA4D0C022520585CD85A89C18846D0F9F8 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.BloomRenderer_Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_t11A8ABEA4D0C022520585CD85A89C18846D0F9F8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T11A8ABEA4D0C022520585CD85A89C18846D0F9F8_H
#ifndef CHROMATICABERRATIONRENDERER_TC87F0F11DA11CC60BEEDDA6637242EC319919C91_H
#define CHROMATICABERRATIONRENDERER_TC87F0F11DA11CC60BEEDDA6637242EC319919C91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ChromaticAberrationRenderer
struct  ChromaticAberrationRenderer_tC87F0F11DA11CC60BEEDDA6637242EC319919C91  : public PostProcessEffectRenderer_1_t07251E442BB573DB59ACA6E8F4BBBD59A896FEA6
{
public:
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.ChromaticAberrationRenderer::m_InternalSpectralLut
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_InternalSpectralLut_2;

public:
	inline static int32_t get_offset_of_m_InternalSpectralLut_2() { return static_cast<int32_t>(offsetof(ChromaticAberrationRenderer_tC87F0F11DA11CC60BEEDDA6637242EC319919C91, ___m_InternalSpectralLut_2)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_InternalSpectralLut_2() const { return ___m_InternalSpectralLut_2; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_InternalSpectralLut_2() { return &___m_InternalSpectralLut_2; }
	inline void set_m_InternalSpectralLut_2(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_InternalSpectralLut_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalSpectralLut_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATIONRENDERER_TC87F0F11DA11CC60BEEDDA6637242EC319919C91_H
#ifndef COLORGRADINGRENDERER_T5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3_H
#define COLORGRADINGRENDERER_T5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorGradingRenderer
struct  ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3  : public PostProcessEffectRenderer_1_t6188E3DAECC7E578F868303F8E7C3A54A9951877
{
public:
	// UnityEngine.Texture2D UnityEngine.Rendering.PostProcessing.ColorGradingRenderer::m_GradingCurves
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_GradingCurves_2;
	// UnityEngine.Color[] UnityEngine.Rendering.PostProcessing.ColorGradingRenderer::m_Pixels
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_Pixels_3;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.ColorGradingRenderer::m_InternalLdrLut
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___m_InternalLdrLut_4;
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.ColorGradingRenderer::m_InternalLogLut
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___m_InternalLogLut_5;
	// UnityEngine.Rendering.PostProcessing.HableCurve UnityEngine.Rendering.PostProcessing.ColorGradingRenderer::m_HableCurve
	HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D * ___m_HableCurve_6;

public:
	inline static int32_t get_offset_of_m_GradingCurves_2() { return static_cast<int32_t>(offsetof(ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3, ___m_GradingCurves_2)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_GradingCurves_2() const { return ___m_GradingCurves_2; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_GradingCurves_2() { return &___m_GradingCurves_2; }
	inline void set_m_GradingCurves_2(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_GradingCurves_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradingCurves_2), value);
	}

	inline static int32_t get_offset_of_m_Pixels_3() { return static_cast<int32_t>(offsetof(ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3, ___m_Pixels_3)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_Pixels_3() const { return ___m_Pixels_3; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_Pixels_3() { return &___m_Pixels_3; }
	inline void set_m_Pixels_3(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_Pixels_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pixels_3), value);
	}

	inline static int32_t get_offset_of_m_InternalLdrLut_4() { return static_cast<int32_t>(offsetof(ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3, ___m_InternalLdrLut_4)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_m_InternalLdrLut_4() const { return ___m_InternalLdrLut_4; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_m_InternalLdrLut_4() { return &___m_InternalLdrLut_4; }
	inline void set_m_InternalLdrLut_4(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___m_InternalLdrLut_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalLdrLut_4), value);
	}

	inline static int32_t get_offset_of_m_InternalLogLut_5() { return static_cast<int32_t>(offsetof(ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3, ___m_InternalLogLut_5)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_m_InternalLogLut_5() const { return ___m_InternalLogLut_5; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_m_InternalLogLut_5() { return &___m_InternalLogLut_5; }
	inline void set_m_InternalLogLut_5(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___m_InternalLogLut_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalLogLut_5), value);
	}

	inline static int32_t get_offset_of_m_HableCurve_6() { return static_cast<int32_t>(offsetof(ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3, ___m_HableCurve_6)); }
	inline HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D * get_m_HableCurve_6() const { return ___m_HableCurve_6; }
	inline HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D ** get_address_of_m_HableCurve_6() { return &___m_HableCurve_6; }
	inline void set_m_HableCurve_6(HableCurve_t15B12405420560FC90A4FD852C2C2AF5F6CF491D * value)
	{
		___m_HableCurve_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_HableCurve_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGRENDERER_T5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3_H
#ifndef PASS_T181A7D496ADD2D55DBFF34DCA58045535626646D_H
#define PASS_T181A7D496ADD2D55DBFF34DCA58045535626646D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorGradingRenderer_Pass
struct  Pass_t181A7D496ADD2D55DBFF34DCA58045535626646D 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.ColorGradingRenderer_Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_t181A7D496ADD2D55DBFF34DCA58045535626646D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T181A7D496ADD2D55DBFF34DCA58045535626646D_H
#ifndef DEPTHOFFIELDRENDERER_T71088ABDEC239433D83BDD62EFE80B50DE928340_H
#define DEPTHOFFIELDRENDERER_T71088ABDEC239433D83BDD62EFE80B50DE928340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.DepthOfFieldRenderer
struct  DepthOfFieldRenderer_t71088ABDEC239433D83BDD62EFE80B50DE928340  : public PostProcessEffectRenderer_1_t2596D39CFE7ECFD7706255E2D37D2A640FCBE43C
{
public:
	// UnityEngine.RenderTexture[][] UnityEngine.Rendering.PostProcessing.DepthOfFieldRenderer::m_CoCHistoryTextures
	RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF* ___m_CoCHistoryTextures_2;
	// System.Int32[] UnityEngine.Rendering.PostProcessing.DepthOfFieldRenderer::m_HistoryPingPong
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_HistoryPingPong_3;

public:
	inline static int32_t get_offset_of_m_CoCHistoryTextures_2() { return static_cast<int32_t>(offsetof(DepthOfFieldRenderer_t71088ABDEC239433D83BDD62EFE80B50DE928340, ___m_CoCHistoryTextures_2)); }
	inline RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF* get_m_CoCHistoryTextures_2() const { return ___m_CoCHistoryTextures_2; }
	inline RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF** get_address_of_m_CoCHistoryTextures_2() { return &___m_CoCHistoryTextures_2; }
	inline void set_m_CoCHistoryTextures_2(RenderTextureU5BU5DU5BU5D_t0481781C86014C56DE3E8711ADD0C18B122453EF* value)
	{
		___m_CoCHistoryTextures_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CoCHistoryTextures_2), value);
	}

	inline static int32_t get_offset_of_m_HistoryPingPong_3() { return static_cast<int32_t>(offsetof(DepthOfFieldRenderer_t71088ABDEC239433D83BDD62EFE80B50DE928340, ___m_HistoryPingPong_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_HistoryPingPong_3() const { return ___m_HistoryPingPong_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_HistoryPingPong_3() { return &___m_HistoryPingPong_3; }
	inline void set_m_HistoryPingPong_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_HistoryPingPong_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistoryPingPong_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDRENDERER_T71088ABDEC239433D83BDD62EFE80B50DE928340_H
#ifndef PASS_TE33D40B930CCB914A8ADC1F369099F2C1995B954_H
#define PASS_TE33D40B930CCB914A8ADC1F369099F2C1995B954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.DepthOfFieldRenderer_Pass
struct  Pass_tE33D40B930CCB914A8ADC1F369099F2C1995B954 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.DepthOfFieldRenderer_Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_tE33D40B930CCB914A8ADC1F369099F2C1995B954, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_TE33D40B930CCB914A8ADC1F369099F2C1995B954_H
#ifndef EYEADAPTATION_T73FC6AA5F6081EDF0D54602E044501847E82AF9E_H
#define EYEADAPTATION_T73FC6AA5F6081EDF0D54602E044501847E82AF9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.EyeAdaptation
struct  EyeAdaptation_t73FC6AA5F6081EDF0D54602E044501847E82AF9E 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.EyeAdaptation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EyeAdaptation_t73FC6AA5F6081EDF0D54602E044501847E82AF9E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATION_T73FC6AA5F6081EDF0D54602E044501847E82AF9E_H
#ifndef GRADINGMODE_T3DC1002334E76F80BCBDA10A0D5A0D908062D489_H
#define GRADINGMODE_T3DC1002334E76F80BCBDA10A0D5A0D908062D489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.GradingMode
struct  GradingMode_t3DC1002334E76F80BCBDA10A0D5A0D908062D489 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.GradingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GradingMode_t3DC1002334E76F80BCBDA10A0D5A0D908062D489, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADINGMODE_T3DC1002334E76F80BCBDA10A0D5A0D908062D489_H
#ifndef GRAINRENDERER_T8745359451C44DC431A99B61BD2F7584625F7F25_H
#define GRAINRENDERER_T8745359451C44DC431A99B61BD2F7584625F7F25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.GrainRenderer
struct  GrainRenderer_t8745359451C44DC431A99B61BD2F7584625F7F25  : public PostProcessEffectRenderer_1_tC90D663958D767936D33668435B04BBA68147B5F
{
public:
	// UnityEngine.RenderTexture UnityEngine.Rendering.PostProcessing.GrainRenderer::m_GrainLookupRT
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___m_GrainLookupRT_2;
	// System.Int32 UnityEngine.Rendering.PostProcessing.GrainRenderer::m_SampleIndex
	int32_t ___m_SampleIndex_3;

public:
	inline static int32_t get_offset_of_m_GrainLookupRT_2() { return static_cast<int32_t>(offsetof(GrainRenderer_t8745359451C44DC431A99B61BD2F7584625F7F25, ___m_GrainLookupRT_2)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_m_GrainLookupRT_2() const { return ___m_GrainLookupRT_2; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_m_GrainLookupRT_2() { return &___m_GrainLookupRT_2; }
	inline void set_m_GrainLookupRT_2(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___m_GrainLookupRT_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GrainLookupRT_2), value);
	}

	inline static int32_t get_offset_of_m_SampleIndex_3() { return static_cast<int32_t>(offsetof(GrainRenderer_t8745359451C44DC431A99B61BD2F7584625F7F25, ___m_SampleIndex_3)); }
	inline int32_t get_m_SampleIndex_3() const { return ___m_SampleIndex_3; }
	inline int32_t* get_address_of_m_SampleIndex_3() { return &___m_SampleIndex_3; }
	inline void set_m_SampleIndex_3(int32_t value)
	{
		___m_SampleIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINRENDERER_T8745359451C44DC431A99B61BD2F7584625F7F25_H
#ifndef KERNELSIZE_T9FB5AA957F2E8C257D6E05A4573CB05753B6BB4F_H
#define KERNELSIZE_T9FB5AA957F2E8C257D6E05A4573CB05753B6BB4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.KernelSize
struct  KernelSize_t9FB5AA957F2E8C257D6E05A4573CB05753B6BB4F 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.KernelSize::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KernelSize_t9FB5AA957F2E8C257D6E05A4573CB05753B6BB4F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNELSIZE_T9FB5AA957F2E8C257D6E05A4573CB05753B6BB4F_H
#ifndef POSTPROCESSEVENT_TA94B9311FE47676FC4B04E6F7935F3DE20E42BDA_H
#define POSTPROCESSEVENT_TA94B9311FE47676FC4B04E6F7935F3DE20E42BDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEvent
struct  PostProcessEvent_tA94B9311FE47676FC4B04E6F7935F3DE20E42BDA 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PostProcessEvent_tA94B9311FE47676FC4B04E6F7935F3DE20E42BDA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEVENT_TA94B9311FE47676FC4B04E6F7935F3DE20E42BDA_H
#ifndef TONEMAPPER_TB4DF48591BDC83824592387E9CA0332F0E0F4106_H
#define TONEMAPPER_TB4DF48591BDC83824592387E9CA0332F0E0F4106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Tonemapper
struct  Tonemapper_tB4DF48591BDC83824592387E9CA0332F0E0F4106 
{
public:
	// System.Int32 UnityEngine.Rendering.PostProcessing.Tonemapper::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Tonemapper_tB4DF48591BDC83824592387E9CA0332F0E0F4106, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPER_TB4DF48591BDC83824592387E9CA0332F0E0F4106_H
#ifndef SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#define SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenOrientation_t4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#ifndef CAMERADEVICEMODE_T31CE15C1D60CED5FC63DF3962D53D5DAADD40589_H
#define CAMERADEVICEMODE_T31CE15C1D60CED5FC63DF3962D53D5DAADD40589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice_CameraDeviceMode
struct  CameraDeviceMode_t31CE15C1D60CED5FC63DF3962D53D5DAADD40589 
{
public:
	// System.Int32 Vuforia.CameraDevice_CameraDeviceMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t31CE15C1D60CED5FC63DF3962D53D5DAADD40589, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T31CE15C1D60CED5FC63DF3962D53D5DAADD40589_H
#ifndef CLIPPING_MODE_T74517F84F38E21AC75F4DB3A1D76698EA0D2580D_H
#define CLIPPING_MODE_T74517F84F38E21AC75F4DB3A1D76698EA0D2580D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaUtility_CLIPPING_MODE
struct  CLIPPING_MODE_t74517F84F38E21AC75F4DB3A1D76698EA0D2580D 
{
public:
	// System.Int32 Vuforia.HideExcessAreaUtility_CLIPPING_MODE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t74517F84F38E21AC75F4DB3A1D76698EA0D2580D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T74517F84F38E21AC75F4DB3A1D76698EA0D2580D_H
#ifndef FRAMEQUALITY_TEA002835E0F464BB9506076B691826DAAFEB87FB_H
#define FRAMEQUALITY_TEA002835E0F464BB9506076B691826DAAFEB87FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder_FrameQuality
struct  FrameQuality_tEA002835E0F464BB9506076B691826DAAFEB87FB 
{
public:
	// System.Int32 Vuforia.ImageTargetBuilder_FrameQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FrameQuality_tEA002835E0F464BB9506076B691826DAAFEB87FB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEQUALITY_TEA002835E0F464BB9506076B691826DAAFEB87FB_H
#ifndef IMAGETARGETTYPE_T3A1F68B5507799B9B73A9806FA199BF2F3A819D6_H
#define IMAGETARGETTYPE_T3A1F68B5507799B9B73A9806FA199BF2F3A819D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetType
struct  ImageTargetType_t3A1F68B5507799B9B73A9806FA199BF2F3A819D6 
{
public:
	// System.Int32 Vuforia.ImageTargetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImageTargetType_t3A1F68B5507799B9B73A9806FA199BF2F3A819D6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETTYPE_T3A1F68B5507799B9B73A9806FA199BF2F3A819D6_H
#ifndef TARGETFINDER_T2AFA3E4A66C461FA522FE35048DB093003A7B1AC_H
#define TARGETFINDER_T2AFA3E4A66C461FA522FE35048DB093003A7B1AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder
struct  TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.TargetFinder::mTargetFinderPtr
	intptr_t ___mTargetFinderPtr_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ObjectTarget> Vuforia.TargetFinder::mTargets
	Dictionary_2_t38214185C816F7C6D37D4A288F828447EFA5BF63 * ___mTargets_1;
	// System.IntPtr Vuforia.TargetFinder::mTargetFinderStatePtr
	intptr_t ___mTargetFinderStatePtr_2;
	// Vuforia.TargetFinder_TargetFinderState Vuforia.TargetFinder::mTargetFinderState
	TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D  ___mTargetFinderState_3;
	// System.Collections.Generic.List`1<Vuforia.TargetFinder_TargetSearchResult> Vuforia.TargetFinder::mNewResults
	List_1_t9F47B63E854EC1B276E04B3C907EA6E9B9A2C619 * ___mNewResults_4;

public:
	inline static int32_t get_offset_of_mTargetFinderPtr_0() { return static_cast<int32_t>(offsetof(TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC, ___mTargetFinderPtr_0)); }
	inline intptr_t get_mTargetFinderPtr_0() const { return ___mTargetFinderPtr_0; }
	inline intptr_t* get_address_of_mTargetFinderPtr_0() { return &___mTargetFinderPtr_0; }
	inline void set_mTargetFinderPtr_0(intptr_t value)
	{
		___mTargetFinderPtr_0 = value;
	}

	inline static int32_t get_offset_of_mTargets_1() { return static_cast<int32_t>(offsetof(TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC, ___mTargets_1)); }
	inline Dictionary_2_t38214185C816F7C6D37D4A288F828447EFA5BF63 * get_mTargets_1() const { return ___mTargets_1; }
	inline Dictionary_2_t38214185C816F7C6D37D4A288F828447EFA5BF63 ** get_address_of_mTargets_1() { return &___mTargets_1; }
	inline void set_mTargets_1(Dictionary_2_t38214185C816F7C6D37D4A288F828447EFA5BF63 * value)
	{
		___mTargets_1 = value;
		Il2CppCodeGenWriteBarrier((&___mTargets_1), value);
	}

	inline static int32_t get_offset_of_mTargetFinderStatePtr_2() { return static_cast<int32_t>(offsetof(TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC, ___mTargetFinderStatePtr_2)); }
	inline intptr_t get_mTargetFinderStatePtr_2() const { return ___mTargetFinderStatePtr_2; }
	inline intptr_t* get_address_of_mTargetFinderStatePtr_2() { return &___mTargetFinderStatePtr_2; }
	inline void set_mTargetFinderStatePtr_2(intptr_t value)
	{
		___mTargetFinderStatePtr_2 = value;
	}

	inline static int32_t get_offset_of_mTargetFinderState_3() { return static_cast<int32_t>(offsetof(TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC, ___mTargetFinderState_3)); }
	inline TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D  get_mTargetFinderState_3() const { return ___mTargetFinderState_3; }
	inline TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D * get_address_of_mTargetFinderState_3() { return &___mTargetFinderState_3; }
	inline void set_mTargetFinderState_3(TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D  value)
	{
		___mTargetFinderState_3 = value;
	}

	inline static int32_t get_offset_of_mNewResults_4() { return static_cast<int32_t>(offsetof(TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC, ___mNewResults_4)); }
	inline List_1_t9F47B63E854EC1B276E04B3C907EA6E9B9A2C619 * get_mNewResults_4() const { return ___mNewResults_4; }
	inline List_1_t9F47B63E854EC1B276E04B3C907EA6E9B9A2C619 ** get_address_of_mNewResults_4() { return &___mNewResults_4; }
	inline void set_mNewResults_4(List_1_t9F47B63E854EC1B276E04B3C907EA6E9B9A2C619 * value)
	{
		___mNewResults_4 = value;
		Il2CppCodeGenWriteBarrier((&___mNewResults_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFINDER_T2AFA3E4A66C461FA522FE35048DB093003A7B1AC_H
#ifndef FILTERMODE_T3699B6BDA0391CF764A709D079B6AB7B81943779_H
#define FILTERMODE_T3699B6BDA0391CF764A709D079B6AB7B81943779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder_FilterMode
struct  FilterMode_t3699B6BDA0391CF764A709D079B6AB7B81943779 
{
public:
	// System.Int32 Vuforia.TargetFinder_FilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterMode_t3699B6BDA0391CF764A709D079B6AB7B81943779, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T3699B6BDA0391CF764A709D079B6AB7B81943779_H
#ifndef INITSTATE_TF8DCB9A690DFC7C81A7543FB4E5D6097B27A4925_H
#define INITSTATE_TF8DCB9A690DFC7C81A7543FB4E5D6097B27A4925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder_InitState
struct  InitState_tF8DCB9A690DFC7C81A7543FB4E5D6097B27A4925 
{
public:
	// System.Int32 Vuforia.TargetFinder_InitState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitState_tF8DCB9A690DFC7C81A7543FB4E5D6097B27A4925, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITSTATE_TF8DCB9A690DFC7C81A7543FB4E5D6097B27A4925_H
#ifndef TARGETSEARCHRESULT_TC958489A5AB66221259FCDFD35F79CA426DAC6F5_H
#define TARGETSEARCHRESULT_TC958489A5AB66221259FCDFD35F79CA426DAC6F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder_TargetSearchResult
struct  TargetSearchResult_tC958489A5AB66221259FCDFD35F79CA426DAC6F5  : public RuntimeObject
{
public:
	// System.String Vuforia.TargetFinder_TargetSearchResult::TargetName
	String_t* ___TargetName_0;
	// System.String Vuforia.TargetFinder_TargetSearchResult::UniqueTargetId
	String_t* ___UniqueTargetId_1;
	// System.IntPtr Vuforia.TargetFinder_TargetSearchResult::TargetSearchResultPtr
	intptr_t ___TargetSearchResultPtr_2;

public:
	inline static int32_t get_offset_of_TargetName_0() { return static_cast<int32_t>(offsetof(TargetSearchResult_tC958489A5AB66221259FCDFD35F79CA426DAC6F5, ___TargetName_0)); }
	inline String_t* get_TargetName_0() const { return ___TargetName_0; }
	inline String_t** get_address_of_TargetName_0() { return &___TargetName_0; }
	inline void set_TargetName_0(String_t* value)
	{
		___TargetName_0 = value;
		Il2CppCodeGenWriteBarrier((&___TargetName_0), value);
	}

	inline static int32_t get_offset_of_UniqueTargetId_1() { return static_cast<int32_t>(offsetof(TargetSearchResult_tC958489A5AB66221259FCDFD35F79CA426DAC6F5, ___UniqueTargetId_1)); }
	inline String_t* get_UniqueTargetId_1() const { return ___UniqueTargetId_1; }
	inline String_t** get_address_of_UniqueTargetId_1() { return &___UniqueTargetId_1; }
	inline void set_UniqueTargetId_1(String_t* value)
	{
		___UniqueTargetId_1 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueTargetId_1), value);
	}

	inline static int32_t get_offset_of_TargetSearchResultPtr_2() { return static_cast<int32_t>(offsetof(TargetSearchResult_tC958489A5AB66221259FCDFD35F79CA426DAC6F5, ___TargetSearchResultPtr_2)); }
	inline intptr_t get_TargetSearchResultPtr_2() const { return ___TargetSearchResultPtr_2; }
	inline intptr_t* get_address_of_TargetSearchResultPtr_2() { return &___TargetSearchResultPtr_2; }
	inline void set_TargetSearchResultPtr_2(intptr_t value)
	{
		___TargetSearchResultPtr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETSEARCHRESULT_TC958489A5AB66221259FCDFD35F79CA426DAC6F5_H
#ifndef UPDATESTATE_T9780BF40F4F57951DF918E1522D9A863D587EF8A_H
#define UPDATESTATE_T9780BF40F4F57951DF918E1522D9A863D587EF8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder_UpdateState
struct  UpdateState_t9780BF40F4F57951DF918E1522D9A863D587EF8A 
{
public:
	// System.Int32 Vuforia.TargetFinder_UpdateState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateState_t9780BF40F4F57951DF918E1522D9A863D587EF8A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESTATE_T9780BF40F4F57951DF918E1522D9A863D587EF8A_H
#ifndef STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#define STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour_Status
struct  Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifndef STATUSINFO_T5507FB8CC09640E7771385EBE27221431A2FEB4E_H
#define STATUSINFO_T5507FB8CC09640E7771385EBE27221431A2FEB4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour_StatusInfo
struct  StatusInfo_t5507FB8CC09640E7771385EBE27221431A2FEB4E 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_StatusInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StatusInfo_t5507FB8CC09640E7771385EBE27221431A2FEB4E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSINFO_T5507FB8CC09640E7771385EBE27221431A2FEB4E_H
#ifndef TRACKABLESOURCE_TD34E68F40275ADD2D4284E6CF1DA1800C8194E39_H
#define TRACKABLESOURCE_TD34E68F40275ADD2D4284E6CF1DA1800C8194E39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableSource
struct  TrackableSource_tD34E68F40275ADD2D4284E6CF1DA1800C8194E39  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.TrackableSource::<TrackableSourcePtr>k__BackingField
	intptr_t ___U3CTrackableSourcePtrU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableSource_tD34E68F40275ADD2D4284E6CF1DA1800C8194E39, ___U3CTrackableSourcePtrU3Ek__BackingField_0)); }
	inline intptr_t get_U3CTrackableSourcePtrU3Ek__BackingField_0() const { return ___U3CTrackableSourcePtrU3Ek__BackingField_0; }
	inline intptr_t* get_address_of_U3CTrackableSourcePtrU3Ek__BackingField_0() { return &___U3CTrackableSourcePtrU3Ek__BackingField_0; }
	inline void set_U3CTrackableSourcePtrU3Ek__BackingField_0(intptr_t value)
	{
		___U3CTrackableSourcePtrU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLESOURCE_TD34E68F40275ADD2D4284E6CF1DA1800C8194E39_H
#ifndef FRAMESTATE_T40C5EF3D259514DAB23DEDCD72218A934C5645E0_H
#define FRAMESTATE_T40C5EF3D259514DAB23DEDCD72218A934C5645E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_FrameState
#pragma pack(push, tp, 1)
struct  FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0 
{
public:
	// System.IntPtr Vuforia.TrackerData_FrameState::trackableDataArray
	intptr_t ___trackableDataArray_0;
	// System.IntPtr Vuforia.TrackerData_FrameState::vbDataArray
	intptr_t ___vbDataArray_1;
	// System.IntPtr Vuforia.TrackerData_FrameState::vuMarkResultArray
	intptr_t ___vuMarkResultArray_2;
	// System.IntPtr Vuforia.TrackerData_FrameState::newVuMarkDataArray
	intptr_t ___newVuMarkDataArray_3;
	// System.IntPtr Vuforia.TrackerData_FrameState::illuminationData
	intptr_t ___illuminationData_4;
	// System.Int32 Vuforia.TrackerData_FrameState::numTrackableResults
	int32_t ___numTrackableResults_5;
	// System.Int32 Vuforia.TrackerData_FrameState::numVirtualButtonResults
	int32_t ___numVirtualButtonResults_6;
	// System.Int32 Vuforia.TrackerData_FrameState::frameIndex
	int32_t ___frameIndex_7;
	// System.Int32 Vuforia.TrackerData_FrameState::numVuMarkResults
	int32_t ___numVuMarkResults_8;
	// System.Int32 Vuforia.TrackerData_FrameState::numNewVuMarks
	int32_t ___numNewVuMarks_9;
	// System.Int32 Vuforia.TrackerData_FrameState::deviceTrackableId
	int32_t ___deviceTrackableId_10;
	// System.Int32 Vuforia.TrackerData_FrameState::deviceTrackableStatusInfo
	int32_t ___deviceTrackableStatusInfo_11;
	// UnityEngine.Vector4 Vuforia.TrackerData_FrameState::minCameraCalibration
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___minCameraCalibration_12;

public:
	inline static int32_t get_offset_of_trackableDataArray_0() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___trackableDataArray_0)); }
	inline intptr_t get_trackableDataArray_0() const { return ___trackableDataArray_0; }
	inline intptr_t* get_address_of_trackableDataArray_0() { return &___trackableDataArray_0; }
	inline void set_trackableDataArray_0(intptr_t value)
	{
		___trackableDataArray_0 = value;
	}

	inline static int32_t get_offset_of_vbDataArray_1() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___vbDataArray_1)); }
	inline intptr_t get_vbDataArray_1() const { return ___vbDataArray_1; }
	inline intptr_t* get_address_of_vbDataArray_1() { return &___vbDataArray_1; }
	inline void set_vbDataArray_1(intptr_t value)
	{
		___vbDataArray_1 = value;
	}

	inline static int32_t get_offset_of_vuMarkResultArray_2() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___vuMarkResultArray_2)); }
	inline intptr_t get_vuMarkResultArray_2() const { return ___vuMarkResultArray_2; }
	inline intptr_t* get_address_of_vuMarkResultArray_2() { return &___vuMarkResultArray_2; }
	inline void set_vuMarkResultArray_2(intptr_t value)
	{
		___vuMarkResultArray_2 = value;
	}

	inline static int32_t get_offset_of_newVuMarkDataArray_3() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___newVuMarkDataArray_3)); }
	inline intptr_t get_newVuMarkDataArray_3() const { return ___newVuMarkDataArray_3; }
	inline intptr_t* get_address_of_newVuMarkDataArray_3() { return &___newVuMarkDataArray_3; }
	inline void set_newVuMarkDataArray_3(intptr_t value)
	{
		___newVuMarkDataArray_3 = value;
	}

	inline static int32_t get_offset_of_illuminationData_4() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___illuminationData_4)); }
	inline intptr_t get_illuminationData_4() const { return ___illuminationData_4; }
	inline intptr_t* get_address_of_illuminationData_4() { return &___illuminationData_4; }
	inline void set_illuminationData_4(intptr_t value)
	{
		___illuminationData_4 = value;
	}

	inline static int32_t get_offset_of_numTrackableResults_5() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numTrackableResults_5)); }
	inline int32_t get_numTrackableResults_5() const { return ___numTrackableResults_5; }
	inline int32_t* get_address_of_numTrackableResults_5() { return &___numTrackableResults_5; }
	inline void set_numTrackableResults_5(int32_t value)
	{
		___numTrackableResults_5 = value;
	}

	inline static int32_t get_offset_of_numVirtualButtonResults_6() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numVirtualButtonResults_6)); }
	inline int32_t get_numVirtualButtonResults_6() const { return ___numVirtualButtonResults_6; }
	inline int32_t* get_address_of_numVirtualButtonResults_6() { return &___numVirtualButtonResults_6; }
	inline void set_numVirtualButtonResults_6(int32_t value)
	{
		___numVirtualButtonResults_6 = value;
	}

	inline static int32_t get_offset_of_frameIndex_7() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___frameIndex_7)); }
	inline int32_t get_frameIndex_7() const { return ___frameIndex_7; }
	inline int32_t* get_address_of_frameIndex_7() { return &___frameIndex_7; }
	inline void set_frameIndex_7(int32_t value)
	{
		___frameIndex_7 = value;
	}

	inline static int32_t get_offset_of_numVuMarkResults_8() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numVuMarkResults_8)); }
	inline int32_t get_numVuMarkResults_8() const { return ___numVuMarkResults_8; }
	inline int32_t* get_address_of_numVuMarkResults_8() { return &___numVuMarkResults_8; }
	inline void set_numVuMarkResults_8(int32_t value)
	{
		___numVuMarkResults_8 = value;
	}

	inline static int32_t get_offset_of_numNewVuMarks_9() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numNewVuMarks_9)); }
	inline int32_t get_numNewVuMarks_9() const { return ___numNewVuMarks_9; }
	inline int32_t* get_address_of_numNewVuMarks_9() { return &___numNewVuMarks_9; }
	inline void set_numNewVuMarks_9(int32_t value)
	{
		___numNewVuMarks_9 = value;
	}

	inline static int32_t get_offset_of_deviceTrackableId_10() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___deviceTrackableId_10)); }
	inline int32_t get_deviceTrackableId_10() const { return ___deviceTrackableId_10; }
	inline int32_t* get_address_of_deviceTrackableId_10() { return &___deviceTrackableId_10; }
	inline void set_deviceTrackableId_10(int32_t value)
	{
		___deviceTrackableId_10 = value;
	}

	inline static int32_t get_offset_of_deviceTrackableStatusInfo_11() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___deviceTrackableStatusInfo_11)); }
	inline int32_t get_deviceTrackableStatusInfo_11() const { return ___deviceTrackableStatusInfo_11; }
	inline int32_t* get_address_of_deviceTrackableStatusInfo_11() { return &___deviceTrackableStatusInfo_11; }
	inline void set_deviceTrackableStatusInfo_11(int32_t value)
	{
		___deviceTrackableStatusInfo_11 = value;
	}

	inline static int32_t get_offset_of_minCameraCalibration_12() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___minCameraCalibration_12)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_minCameraCalibration_12() const { return ___minCameraCalibration_12; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_minCameraCalibration_12() { return &___minCameraCalibration_12; }
	inline void set_minCameraCalibration_12(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___minCameraCalibration_12 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMESTATE_T40C5EF3D259514DAB23DEDCD72218A934C5645E0_H
#ifndef SENSITIVITY_T7654EFB20C36491C60EAA2D010FAA6A0D9D14B01_H
#define SENSITIVITY_T7654EFB20C36491C60EAA2D010FAA6A0D9D14B01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton_Sensitivity
struct  Sensitivity_t7654EFB20C36491C60EAA2D010FAA6A0D9D14B01 
{
public:
	// System.Int32 Vuforia.VirtualButton_Sensitivity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Sensitivity_t7654EFB20C36491C60EAA2D010FAA6A0D9D14B01, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENSITIVITY_T7654EFB20C36491C60EAA2D010FAA6A0D9D14B01_H
#ifndef WORLDCENTERMODE_T53E1430BD989A54F75332A2DA9D61C93545897E6_H
#define WORLDCENTERMODE_T53E1430BD989A54F75332A2DA9D61C93545897E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController_WorldCenterMode
struct  WorldCenterMode_t53E1430BD989A54F75332A2DA9D61C93545897E6 
{
public:
	// System.Int32 Vuforia.VuforiaARController_WorldCenterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WorldCenterMode_t53E1430BD989A54F75332A2DA9D61C93545897E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCENTERMODE_T53E1430BD989A54F75332A2DA9D61C93545897E6_H
#ifndef U3CU3EC__DISPLAYCLASS71_0_TF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174_H
#define U3CU3EC__DISPLAYCLASS71_0_TF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager_<>c__DisplayClass71_0
struct  U3CU3Ec__DisplayClass71_0_tF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174  : public RuntimeObject
{
public:
	// Vuforia.VuforiaManager_TrackableIdPair Vuforia.VuforiaManager_<>c__DisplayClass71_0::id
	TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F  ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass71_0_tF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174, ___id_0)); }
	inline TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F  get_id_0() const { return ___id_0; }
	inline TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F * get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F  value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS71_0_TF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174_H
#ifndef RENDEREVENT_T855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7_H
#define RENDEREVENT_T855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_RenderEvent
struct  RenderEvent_t855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer_RenderEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderEvent_t855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDEREVENT_T855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7_H
#ifndef RENDERERAPI_TC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1_H
#define RENDERERAPI_TC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_RendererAPI
struct  RendererAPI_tC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer_RendererAPI::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RendererAPI_tC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERERAPI_TC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1_H
#ifndef VIDEOBGCFGDATA_T4FD9E3E825BD3AF119DFAE696F50C478336B1A9A_H
#define VIDEOBGCFGDATA_T4FD9E3E825BD3AF119DFAE696F50C478336B1A9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_VideoBGCfgData
#pragma pack(push, tp, 1)
struct  VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A 
{
public:
	// Vuforia.VuforiaRenderer_Vec2I Vuforia.VuforiaRenderer_VideoBGCfgData::position
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  ___position_0;
	// Vuforia.VuforiaRenderer_Vec2I Vuforia.VuforiaRenderer_VideoBGCfgData::size
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  ___size_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A, ___position_0)); }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  get_position_0() const { return ___position_0; }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A, ___size_1)); }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  get_size_1() const { return ___size_1; }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  value)
	{
		___size_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBGCFGDATA_T4FD9E3E825BD3AF119DFAE696F50C478336B1A9A_H
#ifndef VIDEOTEXTUREINFO_TE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D_H
#define VIDEOTEXTUREINFO_TE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_VideoTextureInfo
#pragma pack(push, tp, 1)
struct  VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D 
{
public:
	// Vuforia.VuforiaRenderer_Vec2I Vuforia.VuforiaRenderer_VideoTextureInfo::textureSize
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  ___textureSize_0;
	// Vuforia.VuforiaRenderer_Vec2I Vuforia.VuforiaRenderer_VideoTextureInfo::imageSize
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  ___imageSize_1;

public:
	inline static int32_t get_offset_of_textureSize_0() { return static_cast<int32_t>(offsetof(VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D, ___textureSize_0)); }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  get_textureSize_0() const { return ___textureSize_0; }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 * get_address_of_textureSize_0() { return &___textureSize_0; }
	inline void set_textureSize_0(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  value)
	{
		___textureSize_0 = value;
	}

	inline static int32_t get_offset_of_imageSize_1() { return static_cast<int32_t>(offsetof(VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D, ___imageSize_1)); }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  get_imageSize_1() const { return ___imageSize_1; }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 * get_address_of_imageSize_1() { return &___imageSize_1; }
	inline void set_imageSize_1(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  value)
	{
		___imageSize_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTEXTUREINFO_TE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D_H
#ifndef INITIALIZABLEBOOL_T6C711D5999687FC6E8CAF38208EC8E185BF1FC22_H
#define INITIALIZABLEBOOL_T6C711D5999687FC6E8CAF38208EC8E185BF1FC22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities_InitializableBool
struct  InitializableBool_t6C711D5999687FC6E8CAF38208EC8E185BF1FC22 
{
public:
	// System.Int32 Vuforia.VuforiaRuntimeUtilities_InitializableBool::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitializableBool_t6C711D5999687FC6E8CAF38208EC8E185BF1FC22, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZABLEBOOL_T6C711D5999687FC6E8CAF38208EC8E185BF1FC22_H
#ifndef INITERROR_T486F7D53F5B0B7943D4E22BE2D32F4913D4A0431_H
#define INITERROR_T486F7D53F5B0B7943D4E22BE2D32F4913D4A0431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity_InitError
struct  InitError_t486F7D53F5B0B7943D4E22BE2D32F4913D4A0431 
{
public:
	// System.Int32 Vuforia.VuforiaUnity_InitError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitError_t486F7D53F5B0B7943D4E22BE2D32F4913D4A0431, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITERROR_T486F7D53F5B0B7943D4E22BE2D32F4913D4A0431_H
#ifndef STORAGETYPE_T6847D699566F42A8C42777B30537BA0C0521A4D3_H
#define STORAGETYPE_T6847D699566F42A8C42777B30537BA0C0521A4D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity_StorageType
struct  StorageType_t6847D699566F42A8C42777B30537BA0C0521A4D3 
{
public:
	// System.Int32 Vuforia.VuforiaUnity_StorageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StorageType_t6847D699566F42A8C42777B30537BA0C0521A4D3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGETYPE_T6847D699566F42A8C42777B30537BA0C0521A4D3_H
#ifndef VUFORIAHINT_T5E4E8E78687FAEB9311A944CD4051169EF4A5A6D_H
#define VUFORIAHINT_T5E4E8E78687FAEB9311A944CD4051169EF4A5A6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity_VuforiaHint
struct  VuforiaHint_t5E4E8E78687FAEB9311A944CD4051169EF4A5A6D 
{
public:
	// System.Int32 Vuforia.VuforiaUnity_VuforiaHint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VuforiaHint_t5E4E8E78687FAEB9311A944CD4051169EF4A5A6D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAHINT_T5E4E8E78687FAEB9311A944CD4051169EF4A5A6D_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef PARAMETEROVERRIDE_1_TB4B18E969640B6C93068EF667D413C1DF741F342_H
#define PARAMETEROVERRIDE_1_TB4B18E969640B6C93068EF667D413C1DF741F342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.AmbientOcclusionMode>
struct  ParameterOverride_1_tB4B18E969640B6C93068EF667D413C1DF741F342  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_tB4B18E969640B6C93068EF667D413C1DF741F342, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_TB4B18E969640B6C93068EF667D413C1DF741F342_H
#ifndef PARAMETEROVERRIDE_1_T6A5DF2D1C2BF88EA35C618A18492895C0F54A473_H
#define PARAMETEROVERRIDE_1_T6A5DF2D1C2BF88EA35C618A18492895C0F54A473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.AmbientOcclusionQuality>
struct  ParameterOverride_1_t6A5DF2D1C2BF88EA35C618A18492895C0F54A473  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t6A5DF2D1C2BF88EA35C618A18492895C0F54A473, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T6A5DF2D1C2BF88EA35C618A18492895C0F54A473_H
#ifndef PARAMETEROVERRIDE_1_T757461DE48A2FBE0DB6B12C864033CCD202AF941_H
#define PARAMETEROVERRIDE_1_T757461DE48A2FBE0DB6B12C864033CCD202AF941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.EyeAdaptation>
struct  ParameterOverride_1_t757461DE48A2FBE0DB6B12C864033CCD202AF941  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t757461DE48A2FBE0DB6B12C864033CCD202AF941, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T757461DE48A2FBE0DB6B12C864033CCD202AF941_H
#ifndef PARAMETEROVERRIDE_1_T231434EB436B677A1218432A1AF3D6CF5D057EAE_H
#define PARAMETEROVERRIDE_1_T231434EB436B677A1218432A1AF3D6CF5D057EAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.GradingMode>
struct  ParameterOverride_1_t231434EB436B677A1218432A1AF3D6CF5D057EAE  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t231434EB436B677A1218432A1AF3D6CF5D057EAE, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T231434EB436B677A1218432A1AF3D6CF5D057EAE_H
#ifndef PARAMETEROVERRIDE_1_TE8E6764045AEC904A3C2879ABCC08C9B801DAC48_H
#define PARAMETEROVERRIDE_1_TE8E6764045AEC904A3C2879ABCC08C9B801DAC48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.KernelSize>
struct  ParameterOverride_1_tE8E6764045AEC904A3C2879ABCC08C9B801DAC48  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_tE8E6764045AEC904A3C2879ABCC08C9B801DAC48, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_TE8E6764045AEC904A3C2879ABCC08C9B801DAC48_H
#ifndef PARAMETEROVERRIDE_1_T9F3AFB32918ADB96160F6179BBD83B2105DCFFF0_H
#define PARAMETEROVERRIDE_1_T9F3AFB32918ADB96160F6179BBD83B2105DCFFF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<UnityEngine.Rendering.PostProcessing.Tonemapper>
struct  ParameterOverride_1_t9F3AFB32918ADB96160F6179BBD83B2105DCFFF0  : public ParameterOverride_t2CDE93BD17EF3E2BF3D449A8360002FB60D4F25C
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_t9F3AFB32918ADB96160F6179BBD83B2105DCFFF0, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETEROVERRIDE_1_T9F3AFB32918ADB96160F6179BBD83B2105DCFFF0_H
#ifndef POSTPROCESSATTRIBUTE_T9E50FBFEC7C3457951445E73AD60F252DAB70748_H
#define POSTPROCESSATTRIBUTE_T9E50FBFEC7C3457951445E73AD60F252DAB70748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessAttribute
struct  PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type UnityEngine.Rendering.PostProcessing.PostProcessAttribute::renderer
	Type_t * ___renderer_0;
	// UnityEngine.Rendering.PostProcessing.PostProcessEvent UnityEngine.Rendering.PostProcessing.PostProcessAttribute::eventType
	int32_t ___eventType_1;
	// System.String UnityEngine.Rendering.PostProcessing.PostProcessAttribute::menuItem
	String_t* ___menuItem_2;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessAttribute::allowInSceneView
	bool ___allowInSceneView_3;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessAttribute::builtinEffect
	bool ___builtinEffect_4;

public:
	inline static int32_t get_offset_of_renderer_0() { return static_cast<int32_t>(offsetof(PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748, ___renderer_0)); }
	inline Type_t * get_renderer_0() const { return ___renderer_0; }
	inline Type_t ** get_address_of_renderer_0() { return &___renderer_0; }
	inline void set_renderer_0(Type_t * value)
	{
		___renderer_0 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_0), value);
	}

	inline static int32_t get_offset_of_eventType_1() { return static_cast<int32_t>(offsetof(PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748, ___eventType_1)); }
	inline int32_t get_eventType_1() const { return ___eventType_1; }
	inline int32_t* get_address_of_eventType_1() { return &___eventType_1; }
	inline void set_eventType_1(int32_t value)
	{
		___eventType_1 = value;
	}

	inline static int32_t get_offset_of_menuItem_2() { return static_cast<int32_t>(offsetof(PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748, ___menuItem_2)); }
	inline String_t* get_menuItem_2() const { return ___menuItem_2; }
	inline String_t** get_address_of_menuItem_2() { return &___menuItem_2; }
	inline void set_menuItem_2(String_t* value)
	{
		___menuItem_2 = value;
		Il2CppCodeGenWriteBarrier((&___menuItem_2), value);
	}

	inline static int32_t get_offset_of_allowInSceneView_3() { return static_cast<int32_t>(offsetof(PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748, ___allowInSceneView_3)); }
	inline bool get_allowInSceneView_3() const { return ___allowInSceneView_3; }
	inline bool* get_address_of_allowInSceneView_3() { return &___allowInSceneView_3; }
	inline void set_allowInSceneView_3(bool value)
	{
		___allowInSceneView_3 = value;
	}

	inline static int32_t get_offset_of_builtinEffect_4() { return static_cast<int32_t>(offsetof(PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748, ___builtinEffect_4)); }
	inline bool get_builtinEffect_4() const { return ___builtinEffect_4; }
	inline bool* get_address_of_builtinEffect_4() { return &___builtinEffect_4; }
	inline void set_builtinEffect_4(bool value)
	{
		___builtinEffect_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSATTRIBUTE_T9E50FBFEC7C3457951445E73AD60F252DAB70748_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef SURFACEUTILITIES_T904664FFE194582F491FD18913B0751AAC957680_H
#define SURFACEUTILITIES_T904664FFE194582F491FD18913B0751AAC957680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SurfaceUtilities
struct  SurfaceUtilities_t904664FFE194582F491FD18913B0751AAC957680  : public RuntimeObject
{
public:

public:
};

struct SurfaceUtilities_t904664FFE194582F491FD18913B0751AAC957680_StaticFields
{
public:
	// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::mScreenOrientation
	int32_t ___mScreenOrientation_0;

public:
	inline static int32_t get_offset_of_mScreenOrientation_0() { return static_cast<int32_t>(offsetof(SurfaceUtilities_t904664FFE194582F491FD18913B0751AAC957680_StaticFields, ___mScreenOrientation_0)); }
	inline int32_t get_mScreenOrientation_0() const { return ___mScreenOrientation_0; }
	inline int32_t* get_address_of_mScreenOrientation_0() { return &___mScreenOrientation_0; }
	inline void set_mScreenOrientation_0(int32_t value)
	{
		___mScreenOrientation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEUTILITIES_T904664FFE194582F491FD18913B0751AAC957680_H
#ifndef CLOUDRECOSEARCHRESULT_T2D45E5EE3E8F270F399A928879500AE5A8EFA02C_H
#define CLOUDRECOSEARCHRESULT_T2D45E5EE3E8F270F399A928879500AE5A8EFA02C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder_CloudRecoSearchResult
struct  CloudRecoSearchResult_t2D45E5EE3E8F270F399A928879500AE5A8EFA02C  : public TargetSearchResult_tC958489A5AB66221259FCDFD35F79CA426DAC6F5
{
public:
	// System.String Vuforia.TargetFinder_CloudRecoSearchResult::MetaData
	String_t* ___MetaData_3;
	// System.Single Vuforia.TargetFinder_CloudRecoSearchResult::TargetSize
	float ___TargetSize_4;
	// System.Byte Vuforia.TargetFinder_CloudRecoSearchResult::TrackingRating
	uint8_t ___TrackingRating_5;

public:
	inline static int32_t get_offset_of_MetaData_3() { return static_cast<int32_t>(offsetof(CloudRecoSearchResult_t2D45E5EE3E8F270F399A928879500AE5A8EFA02C, ___MetaData_3)); }
	inline String_t* get_MetaData_3() const { return ___MetaData_3; }
	inline String_t** get_address_of_MetaData_3() { return &___MetaData_3; }
	inline void set_MetaData_3(String_t* value)
	{
		___MetaData_3 = value;
		Il2CppCodeGenWriteBarrier((&___MetaData_3), value);
	}

	inline static int32_t get_offset_of_TargetSize_4() { return static_cast<int32_t>(offsetof(CloudRecoSearchResult_t2D45E5EE3E8F270F399A928879500AE5A8EFA02C, ___TargetSize_4)); }
	inline float get_TargetSize_4() const { return ___TargetSize_4; }
	inline float* get_address_of_TargetSize_4() { return &___TargetSize_4; }
	inline void set_TargetSize_4(float value)
	{
		___TargetSize_4 = value;
	}

	inline static int32_t get_offset_of_TrackingRating_5() { return static_cast<int32_t>(offsetof(CloudRecoSearchResult_t2D45E5EE3E8F270F399A928879500AE5A8EFA02C, ___TrackingRating_5)); }
	inline uint8_t get_TrackingRating_5() const { return ___TrackingRating_5; }
	inline uint8_t* get_address_of_TrackingRating_5() { return &___TrackingRating_5; }
	inline void set_TrackingRating_5(uint8_t value)
	{
		___TrackingRating_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOSEARCHRESULT_T2D45E5EE3E8F270F399A928879500AE5A8EFA02C_H
#ifndef VIDEOBACKGROUNDMANAGER_TDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_H
#define VIDEOBACKGROUNDMANAGER_TDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundManager
struct  VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50  : public ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293
{
public:
	// Vuforia.HideExcessAreaUtility_CLIPPING_MODE Vuforia.VideoBackgroundManager::mClippingMode
	int32_t ___mClippingMode_1;
	// UnityEngine.Shader Vuforia.VideoBackgroundManager::mMatteShader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___mMatteShader_2;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBackgroundEnabled
	bool ___mVideoBackgroundEnabled_3;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBackgroundNeedsRedrawing
	bool ___mVideoBackgroundNeedsRedrawing_4;
	// System.Int32 Vuforia.VideoBackgroundManager::mDiscardStatesForRendering
	int32_t ___mDiscardStatesForRendering_5;
	// System.Boolean Vuforia.VideoBackgroundManager::mIsSeeThroughDevice
	bool ___mIsSeeThroughDevice_6;
	// System.Boolean Vuforia.VideoBackgroundManager::<VideoBackgroundTextureSet>k__BackingField
	bool ___U3CVideoBackgroundTextureSetU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_mClippingMode_1() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mClippingMode_1)); }
	inline int32_t get_mClippingMode_1() const { return ___mClippingMode_1; }
	inline int32_t* get_address_of_mClippingMode_1() { return &___mClippingMode_1; }
	inline void set_mClippingMode_1(int32_t value)
	{
		___mClippingMode_1 = value;
	}

	inline static int32_t get_offset_of_mMatteShader_2() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mMatteShader_2)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_mMatteShader_2() const { return ___mMatteShader_2; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_mMatteShader_2() { return &___mMatteShader_2; }
	inline void set_mMatteShader_2(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___mMatteShader_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMatteShader_2), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundEnabled_3() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mVideoBackgroundEnabled_3)); }
	inline bool get_mVideoBackgroundEnabled_3() const { return ___mVideoBackgroundEnabled_3; }
	inline bool* get_address_of_mVideoBackgroundEnabled_3() { return &___mVideoBackgroundEnabled_3; }
	inline void set_mVideoBackgroundEnabled_3(bool value)
	{
		___mVideoBackgroundEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mVideoBackgroundNeedsRedrawing_4() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mVideoBackgroundNeedsRedrawing_4)); }
	inline bool get_mVideoBackgroundNeedsRedrawing_4() const { return ___mVideoBackgroundNeedsRedrawing_4; }
	inline bool* get_address_of_mVideoBackgroundNeedsRedrawing_4() { return &___mVideoBackgroundNeedsRedrawing_4; }
	inline void set_mVideoBackgroundNeedsRedrawing_4(bool value)
	{
		___mVideoBackgroundNeedsRedrawing_4 = value;
	}

	inline static int32_t get_offset_of_mDiscardStatesForRendering_5() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mDiscardStatesForRendering_5)); }
	inline int32_t get_mDiscardStatesForRendering_5() const { return ___mDiscardStatesForRendering_5; }
	inline int32_t* get_address_of_mDiscardStatesForRendering_5() { return &___mDiscardStatesForRendering_5; }
	inline void set_mDiscardStatesForRendering_5(int32_t value)
	{
		___mDiscardStatesForRendering_5 = value;
	}

	inline static int32_t get_offset_of_mIsSeeThroughDevice_6() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mIsSeeThroughDevice_6)); }
	inline bool get_mIsSeeThroughDevice_6() const { return ___mIsSeeThroughDevice_6; }
	inline bool* get_address_of_mIsSeeThroughDevice_6() { return &___mIsSeeThroughDevice_6; }
	inline void set_mIsSeeThroughDevice_6(bool value)
	{
		___mIsSeeThroughDevice_6 = value;
	}

	inline static int32_t get_offset_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___U3CVideoBackgroundTextureSetU3Ek__BackingField_9)); }
	inline bool get_U3CVideoBackgroundTextureSetU3Ek__BackingField_9() const { return ___U3CVideoBackgroundTextureSetU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_9() { return &___U3CVideoBackgroundTextureSetU3Ek__BackingField_9; }
	inline void set_U3CVideoBackgroundTextureSetU3Ek__BackingField_9(bool value)
	{
		___U3CVideoBackgroundTextureSetU3Ek__BackingField_9 = value;
	}
};

struct VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields
{
public:
	// Vuforia.VideoBackgroundManager Vuforia.VideoBackgroundManager::mInstance
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50 * ___mInstance_7;
	// System.Object Vuforia.VideoBackgroundManager::mPadlock
	RuntimeObject * ___mPadlock_8;

public:
	inline static int32_t get_offset_of_mInstance_7() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields, ___mInstance_7)); }
	inline VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50 * get_mInstance_7() const { return ___mInstance_7; }
	inline VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50 ** get_address_of_mInstance_7() { return &___mInstance_7; }
	inline void set_mInstance_7(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50 * value)
	{
		___mInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_7), value);
	}

	inline static int32_t get_offset_of_mPadlock_8() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields, ___mPadlock_8)); }
	inline RuntimeObject * get_mPadlock_8() const { return ___mPadlock_8; }
	inline RuntimeObject ** get_address_of_mPadlock_8() { return &___mPadlock_8; }
	inline void set_mPadlock_8(RuntimeObject * value)
	{
		___mPadlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDMANAGER_TDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_H
#ifndef VIRTUALBUTTON_T95A02A5A354F3D3274398C6CEE3CC0E96E38D654_H
#define VIRTUALBUTTON_T95A02A5A354F3D3274398C6CEE3CC0E96E38D654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton
struct  VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654  : public RuntimeObject
{
public:
	// System.String Vuforia.VirtualButton::mName
	String_t* ___mName_0;
	// System.Int32 Vuforia.VirtualButton::mID
	int32_t ___mID_1;
	// Vuforia.RectangleData Vuforia.VirtualButton::mArea
	RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9  ___mArea_2;
	// System.Boolean Vuforia.VirtualButton::mIsEnabled
	bool ___mIsEnabled_3;
	// Vuforia.ImageTarget Vuforia.VirtualButton::mParentImageTarget
	RuntimeObject* ___mParentImageTarget_4;
	// Vuforia.DataSet Vuforia.VirtualButton::mParentDataSet
	DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * ___mParentDataSet_5;

public:
	inline static int32_t get_offset_of_mName_0() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mName_0)); }
	inline String_t* get_mName_0() const { return ___mName_0; }
	inline String_t** get_address_of_mName_0() { return &___mName_0; }
	inline void set_mName_0(String_t* value)
	{
		___mName_0 = value;
		Il2CppCodeGenWriteBarrier((&___mName_0), value);
	}

	inline static int32_t get_offset_of_mID_1() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mID_1)); }
	inline int32_t get_mID_1() const { return ___mID_1; }
	inline int32_t* get_address_of_mID_1() { return &___mID_1; }
	inline void set_mID_1(int32_t value)
	{
		___mID_1 = value;
	}

	inline static int32_t get_offset_of_mArea_2() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mArea_2)); }
	inline RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9  get_mArea_2() const { return ___mArea_2; }
	inline RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9 * get_address_of_mArea_2() { return &___mArea_2; }
	inline void set_mArea_2(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9  value)
	{
		___mArea_2 = value;
	}

	inline static int32_t get_offset_of_mIsEnabled_3() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mIsEnabled_3)); }
	inline bool get_mIsEnabled_3() const { return ___mIsEnabled_3; }
	inline bool* get_address_of_mIsEnabled_3() { return &___mIsEnabled_3; }
	inline void set_mIsEnabled_3(bool value)
	{
		___mIsEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mParentImageTarget_4() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mParentImageTarget_4)); }
	inline RuntimeObject* get_mParentImageTarget_4() const { return ___mParentImageTarget_4; }
	inline RuntimeObject** get_address_of_mParentImageTarget_4() { return &___mParentImageTarget_4; }
	inline void set_mParentImageTarget_4(RuntimeObject* value)
	{
		___mParentImageTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___mParentImageTarget_4), value);
	}

	inline static int32_t get_offset_of_mParentDataSet_5() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mParentDataSet_5)); }
	inline DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * get_mParentDataSet_5() const { return ___mParentDataSet_5; }
	inline DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 ** get_address_of_mParentDataSet_5() { return &___mParentDataSet_5; }
	inline void set_mParentDataSet_5(DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * value)
	{
		___mParentDataSet_5 = value;
		Il2CppCodeGenWriteBarrier((&___mParentDataSet_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T95A02A5A354F3D3274398C6CEE3CC0E96E38D654_H
#ifndef VUFORIAARCONTROLLER_T7732FFB77105A2F5BBEA40E3CECC5C15DA624876_H
#define VUFORIAARCONTROLLER_T7732FFB77105A2F5BBEA40E3CECC5C15DA624876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController
struct  VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876  : public ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293
{
public:
	// Vuforia.CameraDevice_CameraDeviceMode Vuforia.VuforiaARController::mCameraDeviceModeSetting
	int32_t ___mCameraDeviceModeSetting_1;
	// System.Int32 Vuforia.VuforiaARController::mMaxSimultaneousImageTargets
	int32_t ___mMaxSimultaneousImageTargets_2;
	// System.Int32 Vuforia.VuforiaARController::mMaxSimultaneousObjectTargets
	int32_t ___mMaxSimultaneousObjectTargets_3;
	// System.Boolean Vuforia.VuforiaARController::mUseDelayedLoadingObjectTargets
	bool ___mUseDelayedLoadingObjectTargets_4;
	// System.Boolean Vuforia.VuforiaARController::mModelTargetRecoWhileExtendedTracked
	bool ___mModelTargetRecoWhileExtendedTracked_5;
	// Vuforia.VuforiaARController_WorldCenterMode Vuforia.VuforiaARController::mWorldCenterMode
	int32_t ___mWorldCenterMode_6;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaARController::mWorldCenter
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mWorldCenter_7;
	// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler> Vuforia.VuforiaARController::mVideoBgEventHandlers
	List_1_t1314A3DDAE2D8AD81F9403BD961C3CC0530B04A9 * ___mVideoBgEventHandlers_8;
	// System.Action Vuforia.VuforiaARController::mOnBeforeVuforiaTrackersInitialized
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnBeforeVuforiaTrackersInitialized_9;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaInitialized
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnVuforiaInitialized_10;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaStarted
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnVuforiaStarted_11;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaDeinitialized
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnVuforiaDeinitialized_12;
	// System.Action Vuforia.VuforiaARController::mOnTrackablesUpdated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnTrackablesUpdated_13;
	// System.Action Vuforia.VuforiaARController::mRenderOnUpdate
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mRenderOnUpdate_14;
	// System.Action`1<System.Boolean> Vuforia.VuforiaARController::mOnPause
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___mOnPause_15;
	// System.Boolean Vuforia.VuforiaARController::mPaused
	bool ___mPaused_16;
	// System.Action Vuforia.VuforiaARController::mOnBackgroundTextureChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnBackgroundTextureChanged_17;
	// System.Boolean Vuforia.VuforiaARController::mStartHasBeenInvoked
	bool ___mStartHasBeenInvoked_18;
	// System.Boolean Vuforia.VuforiaARController::mHasStarted
	bool ___mHasStarted_19;
	// Vuforia.ICameraConfiguration Vuforia.VuforiaARController::mCameraConfiguration
	RuntimeObject* ___mCameraConfiguration_20;
	// Vuforia.DigitalEyewearARController Vuforia.VuforiaARController::mEyewearBehaviour
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 * ___mEyewearBehaviour_21;
	// System.Boolean Vuforia.VuforiaARController::mCheckStopCamera
	bool ___mCheckStopCamera_22;
	// UnityEngine.Material Vuforia.VuforiaARController::mClearMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mClearMaterial_23;
	// System.Boolean Vuforia.VuforiaARController::mMetalRendering
	bool ___mMetalRendering_24;
	// System.Boolean Vuforia.VuforiaARController::mHasStartedOnce
	bool ___mHasStartedOnce_25;
	// System.Boolean Vuforia.VuforiaARController::mWasEnabledBeforePause
	bool ___mWasEnabledBeforePause_26;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforePause
	bool ___mObjectTrackerWasActiveBeforePause_27;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforeDisabling
	bool ___mObjectTrackerWasActiveBeforeDisabling_28;
	// System.Int32 Vuforia.VuforiaARController::mLastUpdatedFrame
	int32_t ___mLastUpdatedFrame_29;

public:
	inline static int32_t get_offset_of_mCameraDeviceModeSetting_1() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mCameraDeviceModeSetting_1)); }
	inline int32_t get_mCameraDeviceModeSetting_1() const { return ___mCameraDeviceModeSetting_1; }
	inline int32_t* get_address_of_mCameraDeviceModeSetting_1() { return &___mCameraDeviceModeSetting_1; }
	inline void set_mCameraDeviceModeSetting_1(int32_t value)
	{
		___mCameraDeviceModeSetting_1 = value;
	}

	inline static int32_t get_offset_of_mMaxSimultaneousImageTargets_2() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mMaxSimultaneousImageTargets_2)); }
	inline int32_t get_mMaxSimultaneousImageTargets_2() const { return ___mMaxSimultaneousImageTargets_2; }
	inline int32_t* get_address_of_mMaxSimultaneousImageTargets_2() { return &___mMaxSimultaneousImageTargets_2; }
	inline void set_mMaxSimultaneousImageTargets_2(int32_t value)
	{
		___mMaxSimultaneousImageTargets_2 = value;
	}

	inline static int32_t get_offset_of_mMaxSimultaneousObjectTargets_3() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mMaxSimultaneousObjectTargets_3)); }
	inline int32_t get_mMaxSimultaneousObjectTargets_3() const { return ___mMaxSimultaneousObjectTargets_3; }
	inline int32_t* get_address_of_mMaxSimultaneousObjectTargets_3() { return &___mMaxSimultaneousObjectTargets_3; }
	inline void set_mMaxSimultaneousObjectTargets_3(int32_t value)
	{
		___mMaxSimultaneousObjectTargets_3 = value;
	}

	inline static int32_t get_offset_of_mUseDelayedLoadingObjectTargets_4() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mUseDelayedLoadingObjectTargets_4)); }
	inline bool get_mUseDelayedLoadingObjectTargets_4() const { return ___mUseDelayedLoadingObjectTargets_4; }
	inline bool* get_address_of_mUseDelayedLoadingObjectTargets_4() { return &___mUseDelayedLoadingObjectTargets_4; }
	inline void set_mUseDelayedLoadingObjectTargets_4(bool value)
	{
		___mUseDelayedLoadingObjectTargets_4 = value;
	}

	inline static int32_t get_offset_of_mModelTargetRecoWhileExtendedTracked_5() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mModelTargetRecoWhileExtendedTracked_5)); }
	inline bool get_mModelTargetRecoWhileExtendedTracked_5() const { return ___mModelTargetRecoWhileExtendedTracked_5; }
	inline bool* get_address_of_mModelTargetRecoWhileExtendedTracked_5() { return &___mModelTargetRecoWhileExtendedTracked_5; }
	inline void set_mModelTargetRecoWhileExtendedTracked_5(bool value)
	{
		___mModelTargetRecoWhileExtendedTracked_5 = value;
	}

	inline static int32_t get_offset_of_mWorldCenterMode_6() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mWorldCenterMode_6)); }
	inline int32_t get_mWorldCenterMode_6() const { return ___mWorldCenterMode_6; }
	inline int32_t* get_address_of_mWorldCenterMode_6() { return &___mWorldCenterMode_6; }
	inline void set_mWorldCenterMode_6(int32_t value)
	{
		___mWorldCenterMode_6 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_7() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mWorldCenter_7)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mWorldCenter_7() const { return ___mWorldCenter_7; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mWorldCenter_7() { return &___mWorldCenter_7; }
	inline void set_mWorldCenter_7(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mWorldCenter_7 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_7), value);
	}

	inline static int32_t get_offset_of_mVideoBgEventHandlers_8() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mVideoBgEventHandlers_8)); }
	inline List_1_t1314A3DDAE2D8AD81F9403BD961C3CC0530B04A9 * get_mVideoBgEventHandlers_8() const { return ___mVideoBgEventHandlers_8; }
	inline List_1_t1314A3DDAE2D8AD81F9403BD961C3CC0530B04A9 ** get_address_of_mVideoBgEventHandlers_8() { return &___mVideoBgEventHandlers_8; }
	inline void set_mVideoBgEventHandlers_8(List_1_t1314A3DDAE2D8AD81F9403BD961C3CC0530B04A9 * value)
	{
		___mVideoBgEventHandlers_8 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBgEventHandlers_8), value);
	}

	inline static int32_t get_offset_of_mOnBeforeVuforiaTrackersInitialized_9() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnBeforeVuforiaTrackersInitialized_9)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnBeforeVuforiaTrackersInitialized_9() const { return ___mOnBeforeVuforiaTrackersInitialized_9; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnBeforeVuforiaTrackersInitialized_9() { return &___mOnBeforeVuforiaTrackersInitialized_9; }
	inline void set_mOnBeforeVuforiaTrackersInitialized_9(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnBeforeVuforiaTrackersInitialized_9 = value;
		Il2CppCodeGenWriteBarrier((&___mOnBeforeVuforiaTrackersInitialized_9), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaInitialized_10() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnVuforiaInitialized_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnVuforiaInitialized_10() const { return ___mOnVuforiaInitialized_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnVuforiaInitialized_10() { return &___mOnVuforiaInitialized_10; }
	inline void set_mOnVuforiaInitialized_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnVuforiaInitialized_10 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaInitialized_10), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaStarted_11() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnVuforiaStarted_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnVuforiaStarted_11() const { return ___mOnVuforiaStarted_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnVuforiaStarted_11() { return &___mOnVuforiaStarted_11; }
	inline void set_mOnVuforiaStarted_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnVuforiaStarted_11 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaStarted_11), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaDeinitialized_12() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnVuforiaDeinitialized_12)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnVuforiaDeinitialized_12() const { return ___mOnVuforiaDeinitialized_12; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnVuforiaDeinitialized_12() { return &___mOnVuforiaDeinitialized_12; }
	inline void set_mOnVuforiaDeinitialized_12(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnVuforiaDeinitialized_12 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaDeinitialized_12), value);
	}

	inline static int32_t get_offset_of_mOnTrackablesUpdated_13() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnTrackablesUpdated_13)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnTrackablesUpdated_13() const { return ___mOnTrackablesUpdated_13; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnTrackablesUpdated_13() { return &___mOnTrackablesUpdated_13; }
	inline void set_mOnTrackablesUpdated_13(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnTrackablesUpdated_13 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTrackablesUpdated_13), value);
	}

	inline static int32_t get_offset_of_mRenderOnUpdate_14() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mRenderOnUpdate_14)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mRenderOnUpdate_14() const { return ___mRenderOnUpdate_14; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mRenderOnUpdate_14() { return &___mRenderOnUpdate_14; }
	inline void set_mRenderOnUpdate_14(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mRenderOnUpdate_14 = value;
		Il2CppCodeGenWriteBarrier((&___mRenderOnUpdate_14), value);
	}

	inline static int32_t get_offset_of_mOnPause_15() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnPause_15)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_mOnPause_15() const { return ___mOnPause_15; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_mOnPause_15() { return &___mOnPause_15; }
	inline void set_mOnPause_15(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___mOnPause_15 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPause_15), value);
	}

	inline static int32_t get_offset_of_mPaused_16() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mPaused_16)); }
	inline bool get_mPaused_16() const { return ___mPaused_16; }
	inline bool* get_address_of_mPaused_16() { return &___mPaused_16; }
	inline void set_mPaused_16(bool value)
	{
		___mPaused_16 = value;
	}

	inline static int32_t get_offset_of_mOnBackgroundTextureChanged_17() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnBackgroundTextureChanged_17)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnBackgroundTextureChanged_17() const { return ___mOnBackgroundTextureChanged_17; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnBackgroundTextureChanged_17() { return &___mOnBackgroundTextureChanged_17; }
	inline void set_mOnBackgroundTextureChanged_17(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnBackgroundTextureChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&___mOnBackgroundTextureChanged_17), value);
	}

	inline static int32_t get_offset_of_mStartHasBeenInvoked_18() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mStartHasBeenInvoked_18)); }
	inline bool get_mStartHasBeenInvoked_18() const { return ___mStartHasBeenInvoked_18; }
	inline bool* get_address_of_mStartHasBeenInvoked_18() { return &___mStartHasBeenInvoked_18; }
	inline void set_mStartHasBeenInvoked_18(bool value)
	{
		___mStartHasBeenInvoked_18 = value;
	}

	inline static int32_t get_offset_of_mHasStarted_19() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mHasStarted_19)); }
	inline bool get_mHasStarted_19() const { return ___mHasStarted_19; }
	inline bool* get_address_of_mHasStarted_19() { return &___mHasStarted_19; }
	inline void set_mHasStarted_19(bool value)
	{
		___mHasStarted_19 = value;
	}

	inline static int32_t get_offset_of_mCameraConfiguration_20() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mCameraConfiguration_20)); }
	inline RuntimeObject* get_mCameraConfiguration_20() const { return ___mCameraConfiguration_20; }
	inline RuntimeObject** get_address_of_mCameraConfiguration_20() { return &___mCameraConfiguration_20; }
	inline void set_mCameraConfiguration_20(RuntimeObject* value)
	{
		___mCameraConfiguration_20 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraConfiguration_20), value);
	}

	inline static int32_t get_offset_of_mEyewearBehaviour_21() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mEyewearBehaviour_21)); }
	inline DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 * get_mEyewearBehaviour_21() const { return ___mEyewearBehaviour_21; }
	inline DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 ** get_address_of_mEyewearBehaviour_21() { return &___mEyewearBehaviour_21; }
	inline void set_mEyewearBehaviour_21(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 * value)
	{
		___mEyewearBehaviour_21 = value;
		Il2CppCodeGenWriteBarrier((&___mEyewearBehaviour_21), value);
	}

	inline static int32_t get_offset_of_mCheckStopCamera_22() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mCheckStopCamera_22)); }
	inline bool get_mCheckStopCamera_22() const { return ___mCheckStopCamera_22; }
	inline bool* get_address_of_mCheckStopCamera_22() { return &___mCheckStopCamera_22; }
	inline void set_mCheckStopCamera_22(bool value)
	{
		___mCheckStopCamera_22 = value;
	}

	inline static int32_t get_offset_of_mClearMaterial_23() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mClearMaterial_23)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_mClearMaterial_23() const { return ___mClearMaterial_23; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_mClearMaterial_23() { return &___mClearMaterial_23; }
	inline void set_mClearMaterial_23(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___mClearMaterial_23 = value;
		Il2CppCodeGenWriteBarrier((&___mClearMaterial_23), value);
	}

	inline static int32_t get_offset_of_mMetalRendering_24() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mMetalRendering_24)); }
	inline bool get_mMetalRendering_24() const { return ___mMetalRendering_24; }
	inline bool* get_address_of_mMetalRendering_24() { return &___mMetalRendering_24; }
	inline void set_mMetalRendering_24(bool value)
	{
		___mMetalRendering_24 = value;
	}

	inline static int32_t get_offset_of_mHasStartedOnce_25() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mHasStartedOnce_25)); }
	inline bool get_mHasStartedOnce_25() const { return ___mHasStartedOnce_25; }
	inline bool* get_address_of_mHasStartedOnce_25() { return &___mHasStartedOnce_25; }
	inline void set_mHasStartedOnce_25(bool value)
	{
		___mHasStartedOnce_25 = value;
	}

	inline static int32_t get_offset_of_mWasEnabledBeforePause_26() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mWasEnabledBeforePause_26)); }
	inline bool get_mWasEnabledBeforePause_26() const { return ___mWasEnabledBeforePause_26; }
	inline bool* get_address_of_mWasEnabledBeforePause_26() { return &___mWasEnabledBeforePause_26; }
	inline void set_mWasEnabledBeforePause_26(bool value)
	{
		___mWasEnabledBeforePause_26 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforePause_27() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mObjectTrackerWasActiveBeforePause_27)); }
	inline bool get_mObjectTrackerWasActiveBeforePause_27() const { return ___mObjectTrackerWasActiveBeforePause_27; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforePause_27() { return &___mObjectTrackerWasActiveBeforePause_27; }
	inline void set_mObjectTrackerWasActiveBeforePause_27(bool value)
	{
		___mObjectTrackerWasActiveBeforePause_27 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforeDisabling_28() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mObjectTrackerWasActiveBeforeDisabling_28)); }
	inline bool get_mObjectTrackerWasActiveBeforeDisabling_28() const { return ___mObjectTrackerWasActiveBeforeDisabling_28; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforeDisabling_28() { return &___mObjectTrackerWasActiveBeforeDisabling_28; }
	inline void set_mObjectTrackerWasActiveBeforeDisabling_28(bool value)
	{
		___mObjectTrackerWasActiveBeforeDisabling_28 = value;
	}

	inline static int32_t get_offset_of_mLastUpdatedFrame_29() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mLastUpdatedFrame_29)); }
	inline int32_t get_mLastUpdatedFrame_29() const { return ___mLastUpdatedFrame_29; }
	inline int32_t* get_address_of_mLastUpdatedFrame_29() { return &___mLastUpdatedFrame_29; }
	inline void set_mLastUpdatedFrame_29(int32_t value)
	{
		___mLastUpdatedFrame_29 = value;
	}
};

struct VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields
{
public:
	// Vuforia.VuforiaARController Vuforia.VuforiaARController::mInstance
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * ___mInstance_30;
	// System.Object Vuforia.VuforiaARController::mPadlock
	RuntimeObject * ___mPadlock_31;

public:
	inline static int32_t get_offset_of_mInstance_30() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields, ___mInstance_30)); }
	inline VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * get_mInstance_30() const { return ___mInstance_30; }
	inline VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 ** get_address_of_mInstance_30() { return &___mInstance_30; }
	inline void set_mInstance_30(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * value)
	{
		___mInstance_30 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_30), value);
	}

	inline static int32_t get_offset_of_mPadlock_31() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields, ___mPadlock_31)); }
	inline RuntimeObject * get_mPadlock_31() const { return ___mPadlock_31; }
	inline RuntimeObject ** get_address_of_mPadlock_31() { return &___mPadlock_31; }
	inline void set_mPadlock_31(RuntimeObject * value)
	{
		___mPadlock_31 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAARCONTROLLER_T7732FFB77105A2F5BBEA40E3CECC5C15DA624876_H
#ifndef VUFORIAMANAGER_T56F310BC8B59799D4A44824C42168D133AE095F7_H
#define VUFORIAMANAGER_T56F310BC8B59799D4A44824C42168D133AE095F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager
struct  VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7  : public RuntimeObject
{
public:
	// Vuforia.VuforiaARController_WorldCenterMode Vuforia.VuforiaManager::mWorldCenterMode
	int32_t ___mWorldCenterMode_1;
	// Vuforia.WorldCenterTrackableBehaviour Vuforia.VuforiaManager::mWorldCenter
	RuntimeObject* ___mWorldCenter_2;
	// Vuforia.VuMarkBehaviour Vuforia.VuforiaManager::mVuMarkWorldCenter
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4 * ___mVuMarkWorldCenter_3;
	// UnityEngine.Transform Vuforia.VuforiaManager::mARCameraTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mARCameraTransform_4;
	// UnityEngine.Transform Vuforia.VuforiaManager::mCentralAnchorPoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mCentralAnchorPoint_5;
	// Vuforia.TrackerData_TrackableResultData[] Vuforia.VuforiaManager::mTrackableResultDataArray
	TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D* ___mTrackableResultDataArray_6;
	// Vuforia.TrackerData_VuMarkTargetData[] Vuforia.VuforiaManager::mVuMarkDataArray
	VuMarkTargetDataU5BU5D_tDEF295778F1EBA00260FE5F7CF00E0E91F5E3605* ___mVuMarkDataArray_7;
	// Vuforia.TrackerData_VuMarkTargetResultData[] Vuforia.VuforiaManager::mVuMarkResultDataArray
	VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A* ___mVuMarkResultDataArray_8;
	// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager_TrackableIdPair> Vuforia.VuforiaManager::mWCTrackableFoundQueue
	LinkedList_1_tE86603FF5BEA21BD62849C673324AEE74A31E58A * ___mWCTrackableFoundQueue_9;
	// System.Collections.Generic.Dictionary`2<Vuforia.PIXEL_FORMAT,Vuforia.UnmanagedImage> Vuforia.VuforiaManager::mCameraImages
	Dictionary_2_tD36561317F15A4463B66EFAA17CF49A315A67587 * ___mCameraImages_10;
	// System.IntPtr Vuforia.VuforiaManager::mImageData
	intptr_t ___mImageData_11;
	// System.Int32 Vuforia.VuforiaManager::mInjectedFrameIdx
	int32_t ___mInjectedFrameIdx_12;
	// System.IntPtr Vuforia.VuforiaManager::mLastProcessedFrameStatePtr
	intptr_t ___mLastProcessedFrameStatePtr_13;
	// System.Boolean Vuforia.VuforiaManager::mInitialized
	bool ___mInitialized_14;
	// System.Boolean Vuforia.VuforiaManager::mPaused
	bool ___mPaused_15;
	// Vuforia.TrackerData_FrameState Vuforia.VuforiaManager::mFrameState
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  ___mFrameState_16;
	// System.Int32 Vuforia.VuforiaManager::mLastFrameIdx
	int32_t ___mLastFrameIdx_17;
	// Vuforia.LateLatchingManager Vuforia.VuforiaManager::mLateLatchingManager
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C * ___mLateLatchingManager_18;
	// Vuforia.CameraCalibrationComparer Vuforia.VuforiaManager::mCameraCalibrationComparer
	CameraCalibrationComparer_t7CF72E6611106D56BB040F32DB6D7BFF0898F7EA * ___mCameraCalibrationComparer_19;
	// UnityEngine.Vector3 Vuforia.VuforiaManager::mPosePositionalOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mPosePositionalOffset_20;
	// UnityEngine.Quaternion Vuforia.VuforiaManager::mPoseRotationalOffset
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___mPoseRotationalOffset_21;

public:
	inline static int32_t get_offset_of_mWorldCenterMode_1() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mWorldCenterMode_1)); }
	inline int32_t get_mWorldCenterMode_1() const { return ___mWorldCenterMode_1; }
	inline int32_t* get_address_of_mWorldCenterMode_1() { return &___mWorldCenterMode_1; }
	inline void set_mWorldCenterMode_1(int32_t value)
	{
		___mWorldCenterMode_1 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_2() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mWorldCenter_2)); }
	inline RuntimeObject* get_mWorldCenter_2() const { return ___mWorldCenter_2; }
	inline RuntimeObject** get_address_of_mWorldCenter_2() { return &___mWorldCenter_2; }
	inline void set_mWorldCenter_2(RuntimeObject* value)
	{
		___mWorldCenter_2 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_2), value);
	}

	inline static int32_t get_offset_of_mVuMarkWorldCenter_3() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mVuMarkWorldCenter_3)); }
	inline VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4 * get_mVuMarkWorldCenter_3() const { return ___mVuMarkWorldCenter_3; }
	inline VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4 ** get_address_of_mVuMarkWorldCenter_3() { return &___mVuMarkWorldCenter_3; }
	inline void set_mVuMarkWorldCenter_3(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4 * value)
	{
		___mVuMarkWorldCenter_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkWorldCenter_3), value);
	}

	inline static int32_t get_offset_of_mARCameraTransform_4() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mARCameraTransform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mARCameraTransform_4() const { return ___mARCameraTransform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mARCameraTransform_4() { return &___mARCameraTransform_4; }
	inline void set_mARCameraTransform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mARCameraTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___mARCameraTransform_4), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_5() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mCentralAnchorPoint_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mCentralAnchorPoint_5() const { return ___mCentralAnchorPoint_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mCentralAnchorPoint_5() { return &___mCentralAnchorPoint_5; }
	inline void set_mCentralAnchorPoint_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mCentralAnchorPoint_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_5), value);
	}

	inline static int32_t get_offset_of_mTrackableResultDataArray_6() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mTrackableResultDataArray_6)); }
	inline TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D* get_mTrackableResultDataArray_6() const { return ___mTrackableResultDataArray_6; }
	inline TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D** get_address_of_mTrackableResultDataArray_6() { return &___mTrackableResultDataArray_6; }
	inline void set_mTrackableResultDataArray_6(TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D* value)
	{
		___mTrackableResultDataArray_6 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableResultDataArray_6), value);
	}

	inline static int32_t get_offset_of_mVuMarkDataArray_7() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mVuMarkDataArray_7)); }
	inline VuMarkTargetDataU5BU5D_tDEF295778F1EBA00260FE5F7CF00E0E91F5E3605* get_mVuMarkDataArray_7() const { return ___mVuMarkDataArray_7; }
	inline VuMarkTargetDataU5BU5D_tDEF295778F1EBA00260FE5F7CF00E0E91F5E3605** get_address_of_mVuMarkDataArray_7() { return &___mVuMarkDataArray_7; }
	inline void set_mVuMarkDataArray_7(VuMarkTargetDataU5BU5D_tDEF295778F1EBA00260FE5F7CF00E0E91F5E3605* value)
	{
		___mVuMarkDataArray_7 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkDataArray_7), value);
	}

	inline static int32_t get_offset_of_mVuMarkResultDataArray_8() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mVuMarkResultDataArray_8)); }
	inline VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A* get_mVuMarkResultDataArray_8() const { return ___mVuMarkResultDataArray_8; }
	inline VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A** get_address_of_mVuMarkResultDataArray_8() { return &___mVuMarkResultDataArray_8; }
	inline void set_mVuMarkResultDataArray_8(VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A* value)
	{
		___mVuMarkResultDataArray_8 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkResultDataArray_8), value);
	}

	inline static int32_t get_offset_of_mWCTrackableFoundQueue_9() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mWCTrackableFoundQueue_9)); }
	inline LinkedList_1_tE86603FF5BEA21BD62849C673324AEE74A31E58A * get_mWCTrackableFoundQueue_9() const { return ___mWCTrackableFoundQueue_9; }
	inline LinkedList_1_tE86603FF5BEA21BD62849C673324AEE74A31E58A ** get_address_of_mWCTrackableFoundQueue_9() { return &___mWCTrackableFoundQueue_9; }
	inline void set_mWCTrackableFoundQueue_9(LinkedList_1_tE86603FF5BEA21BD62849C673324AEE74A31E58A * value)
	{
		___mWCTrackableFoundQueue_9 = value;
		Il2CppCodeGenWriteBarrier((&___mWCTrackableFoundQueue_9), value);
	}

	inline static int32_t get_offset_of_mCameraImages_10() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mCameraImages_10)); }
	inline Dictionary_2_tD36561317F15A4463B66EFAA17CF49A315A67587 * get_mCameraImages_10() const { return ___mCameraImages_10; }
	inline Dictionary_2_tD36561317F15A4463B66EFAA17CF49A315A67587 ** get_address_of_mCameraImages_10() { return &___mCameraImages_10; }
	inline void set_mCameraImages_10(Dictionary_2_tD36561317F15A4463B66EFAA17CF49A315A67587 * value)
	{
		___mCameraImages_10 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraImages_10), value);
	}

	inline static int32_t get_offset_of_mImageData_11() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mImageData_11)); }
	inline intptr_t get_mImageData_11() const { return ___mImageData_11; }
	inline intptr_t* get_address_of_mImageData_11() { return &___mImageData_11; }
	inline void set_mImageData_11(intptr_t value)
	{
		___mImageData_11 = value;
	}

	inline static int32_t get_offset_of_mInjectedFrameIdx_12() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mInjectedFrameIdx_12)); }
	inline int32_t get_mInjectedFrameIdx_12() const { return ___mInjectedFrameIdx_12; }
	inline int32_t* get_address_of_mInjectedFrameIdx_12() { return &___mInjectedFrameIdx_12; }
	inline void set_mInjectedFrameIdx_12(int32_t value)
	{
		___mInjectedFrameIdx_12 = value;
	}

	inline static int32_t get_offset_of_mLastProcessedFrameStatePtr_13() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mLastProcessedFrameStatePtr_13)); }
	inline intptr_t get_mLastProcessedFrameStatePtr_13() const { return ___mLastProcessedFrameStatePtr_13; }
	inline intptr_t* get_address_of_mLastProcessedFrameStatePtr_13() { return &___mLastProcessedFrameStatePtr_13; }
	inline void set_mLastProcessedFrameStatePtr_13(intptr_t value)
	{
		___mLastProcessedFrameStatePtr_13 = value;
	}

	inline static int32_t get_offset_of_mInitialized_14() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mInitialized_14)); }
	inline bool get_mInitialized_14() const { return ___mInitialized_14; }
	inline bool* get_address_of_mInitialized_14() { return &___mInitialized_14; }
	inline void set_mInitialized_14(bool value)
	{
		___mInitialized_14 = value;
	}

	inline static int32_t get_offset_of_mPaused_15() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mPaused_15)); }
	inline bool get_mPaused_15() const { return ___mPaused_15; }
	inline bool* get_address_of_mPaused_15() { return &___mPaused_15; }
	inline void set_mPaused_15(bool value)
	{
		___mPaused_15 = value;
	}

	inline static int32_t get_offset_of_mFrameState_16() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mFrameState_16)); }
	inline FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  get_mFrameState_16() const { return ___mFrameState_16; }
	inline FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0 * get_address_of_mFrameState_16() { return &___mFrameState_16; }
	inline void set_mFrameState_16(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  value)
	{
		___mFrameState_16 = value;
	}

	inline static int32_t get_offset_of_mLastFrameIdx_17() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mLastFrameIdx_17)); }
	inline int32_t get_mLastFrameIdx_17() const { return ___mLastFrameIdx_17; }
	inline int32_t* get_address_of_mLastFrameIdx_17() { return &___mLastFrameIdx_17; }
	inline void set_mLastFrameIdx_17(int32_t value)
	{
		___mLastFrameIdx_17 = value;
	}

	inline static int32_t get_offset_of_mLateLatchingManager_18() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mLateLatchingManager_18)); }
	inline LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C * get_mLateLatchingManager_18() const { return ___mLateLatchingManager_18; }
	inline LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C ** get_address_of_mLateLatchingManager_18() { return &___mLateLatchingManager_18; }
	inline void set_mLateLatchingManager_18(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C * value)
	{
		___mLateLatchingManager_18 = value;
		Il2CppCodeGenWriteBarrier((&___mLateLatchingManager_18), value);
	}

	inline static int32_t get_offset_of_mCameraCalibrationComparer_19() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mCameraCalibrationComparer_19)); }
	inline CameraCalibrationComparer_t7CF72E6611106D56BB040F32DB6D7BFF0898F7EA * get_mCameraCalibrationComparer_19() const { return ___mCameraCalibrationComparer_19; }
	inline CameraCalibrationComparer_t7CF72E6611106D56BB040F32DB6D7BFF0898F7EA ** get_address_of_mCameraCalibrationComparer_19() { return &___mCameraCalibrationComparer_19; }
	inline void set_mCameraCalibrationComparer_19(CameraCalibrationComparer_t7CF72E6611106D56BB040F32DB6D7BFF0898F7EA * value)
	{
		___mCameraCalibrationComparer_19 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraCalibrationComparer_19), value);
	}

	inline static int32_t get_offset_of_mPosePositionalOffset_20() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mPosePositionalOffset_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mPosePositionalOffset_20() const { return ___mPosePositionalOffset_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mPosePositionalOffset_20() { return &___mPosePositionalOffset_20; }
	inline void set_mPosePositionalOffset_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mPosePositionalOffset_20 = value;
	}

	inline static int32_t get_offset_of_mPoseRotationalOffset_21() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mPoseRotationalOffset_21)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_mPoseRotationalOffset_21() const { return ___mPoseRotationalOffset_21; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_mPoseRotationalOffset_21() { return &___mPoseRotationalOffset_21; }
	inline void set_mPoseRotationalOffset_21(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___mPoseRotationalOffset_21 = value;
	}
};

struct VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7_StaticFields
{
public:
	// Vuforia.VuforiaManager Vuforia.VuforiaManager::sInstance
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7_StaticFields, ___sInstance_0)); }
	inline VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMANAGER_T56F310BC8B59799D4A44824C42168D133AE095F7_H
#ifndef U3CU3EC__DISPLAYCLASS60_0_T9B16CEDAB1D664130D52803188ADF3EF78ED78A3_H
#define U3CU3EC__DISPLAYCLASS60_0_T9B16CEDAB1D664130D52803188ADF3EF78ED78A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager_<>c__DisplayClass60_0
struct  U3CU3Ec__DisplayClass60_0_t9B16CEDAB1D664130D52803188ADF3EF78ED78A3  : public RuntimeObject
{
public:
	// Vuforia.TrackerData_FrameState Vuforia.VuforiaManager_<>c__DisplayClass60_0::frameState
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  ___frameState_0;

public:
	inline static int32_t get_offset_of_frameState_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass60_0_t9B16CEDAB1D664130D52803188ADF3EF78ED78A3, ___frameState_0)); }
	inline FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  get_frameState_0() const { return ___frameState_0; }
	inline FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0 * get_address_of_frameState_0() { return &___frameState_0; }
	inline void set_frameState_0(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  value)
	{
		___frameState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS60_0_T9B16CEDAB1D664130D52803188ADF3EF78ED78A3_H
#ifndef VUFORIARENDERER_T76A570ACCA3476064DB729CCDAF59CF0BAB41081_H
#define VUFORIARENDERER_T76A570ACCA3476064DB729CCDAF59CF0BAB41081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer
struct  VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081  : public RuntimeObject
{
public:
	// Vuforia.VuforiaRenderer_VideoBGCfgData Vuforia.VuforiaRenderer::mVideoBGConfig
	VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A  ___mVideoBGConfig_1;
	// System.Boolean Vuforia.VuforiaRenderer::mVideoBGConfigSet
	bool ___mVideoBGConfigSet_2;
	// UnityEngine.Texture Vuforia.VuforiaRenderer::mVideoBackgroundTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___mVideoBackgroundTexture_3;
	// System.IntPtr Vuforia.VuforiaRenderer::mNativeRenderingCallback
	intptr_t ___mNativeRenderingCallback_4;
	// System.Boolean Vuforia.VuforiaRenderer::mBackgroundTextureHasChanged
	bool ___mBackgroundTextureHasChanged_5;
	// System.Func`1<System.Boolean> Vuforia.VuforiaRenderer::mLegacyRenderingCondition
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___mLegacyRenderingCondition_6;

public:
	inline static int32_t get_offset_of_mVideoBGConfig_1() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mVideoBGConfig_1)); }
	inline VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A  get_mVideoBGConfig_1() const { return ___mVideoBGConfig_1; }
	inline VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A * get_address_of_mVideoBGConfig_1() { return &___mVideoBGConfig_1; }
	inline void set_mVideoBGConfig_1(VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A  value)
	{
		___mVideoBGConfig_1 = value;
	}

	inline static int32_t get_offset_of_mVideoBGConfigSet_2() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mVideoBGConfigSet_2)); }
	inline bool get_mVideoBGConfigSet_2() const { return ___mVideoBGConfigSet_2; }
	inline bool* get_address_of_mVideoBGConfigSet_2() { return &___mVideoBGConfigSet_2; }
	inline void set_mVideoBGConfigSet_2(bool value)
	{
		___mVideoBGConfigSet_2 = value;
	}

	inline static int32_t get_offset_of_mVideoBackgroundTexture_3() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mVideoBackgroundTexture_3)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_mVideoBackgroundTexture_3() const { return ___mVideoBackgroundTexture_3; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_mVideoBackgroundTexture_3() { return &___mVideoBackgroundTexture_3; }
	inline void set_mVideoBackgroundTexture_3(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___mVideoBackgroundTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundTexture_3), value);
	}

	inline static int32_t get_offset_of_mNativeRenderingCallback_4() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mNativeRenderingCallback_4)); }
	inline intptr_t get_mNativeRenderingCallback_4() const { return ___mNativeRenderingCallback_4; }
	inline intptr_t* get_address_of_mNativeRenderingCallback_4() { return &___mNativeRenderingCallback_4; }
	inline void set_mNativeRenderingCallback_4(intptr_t value)
	{
		___mNativeRenderingCallback_4 = value;
	}

	inline static int32_t get_offset_of_mBackgroundTextureHasChanged_5() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mBackgroundTextureHasChanged_5)); }
	inline bool get_mBackgroundTextureHasChanged_5() const { return ___mBackgroundTextureHasChanged_5; }
	inline bool* get_address_of_mBackgroundTextureHasChanged_5() { return &___mBackgroundTextureHasChanged_5; }
	inline void set_mBackgroundTextureHasChanged_5(bool value)
	{
		___mBackgroundTextureHasChanged_5 = value;
	}

	inline static int32_t get_offset_of_mLegacyRenderingCondition_6() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mLegacyRenderingCondition_6)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_mLegacyRenderingCondition_6() const { return ___mLegacyRenderingCondition_6; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_mLegacyRenderingCondition_6() { return &___mLegacyRenderingCondition_6; }
	inline void set_mLegacyRenderingCondition_6(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___mLegacyRenderingCondition_6 = value;
		Il2CppCodeGenWriteBarrier((&___mLegacyRenderingCondition_6), value);
	}
};

struct VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081_StaticFields
{
public:
	// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::sInstance
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081_StaticFields, ___sInstance_0)); }
	inline VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARENDERER_T76A570ACCA3476064DB729CCDAF59CF0BAB41081_H
#ifndef VUFORIARUNTIMEUTILITIES_T2BBA4B702A9982A803DE4C93AEF9591B12744DA7_H
#define VUFORIARUNTIMEUTILITIES_T2BBA4B702A9982A803DE4C93AEF9591B12744DA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities
struct  VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7  : public RuntimeObject
{
public:

public:
};

struct VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields
{
public:
	// Vuforia.VuforiaRuntimeUtilities_InitializableBool Vuforia.VuforiaRuntimeUtilities::sWebCamUsed
	int32_t ___sWebCamUsed_0;
	// Vuforia.VuforiaRuntimeUtilities_InitializableBool Vuforia.VuforiaRuntimeUtilities::sNativePluginSupport
	int32_t ___sNativePluginSupport_1;

public:
	inline static int32_t get_offset_of_sWebCamUsed_0() { return static_cast<int32_t>(offsetof(VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields, ___sWebCamUsed_0)); }
	inline int32_t get_sWebCamUsed_0() const { return ___sWebCamUsed_0; }
	inline int32_t* get_address_of_sWebCamUsed_0() { return &___sWebCamUsed_0; }
	inline void set_sWebCamUsed_0(int32_t value)
	{
		___sWebCamUsed_0 = value;
	}

	inline static int32_t get_offset_of_sNativePluginSupport_1() { return static_cast<int32_t>(offsetof(VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields, ___sNativePluginSupport_1)); }
	inline int32_t get_sNativePluginSupport_1() const { return ___sNativePluginSupport_1; }
	inline int32_t* get_address_of_sNativePluginSupport_1() { return &___sNativePluginSupport_1; }
	inline void set_sNativePluginSupport_1(int32_t value)
	{
		___sNativePluginSupport_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARUNTIMEUTILITIES_T2BBA4B702A9982A803DE4C93AEF9591B12744DA7_H
#ifndef CAMERASTATE_T4A1994F5F7B9665FD6E136894A4586C0F1E405A6_H
#define CAMERASTATE_T4A1994F5F7B9665FD6E136894A4586C0F1E405A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities_CameraState
struct  CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6 
{
public:
	// System.Boolean Vuforia.VuforiaRuntimeUtilities_CameraState::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_0;
	// System.Boolean Vuforia.VuforiaRuntimeUtilities_CameraState::<Active>k__BackingField
	bool ___U3CActiveU3Ek__BackingField_1;
	// Vuforia.CameraDevice_CameraDeviceMode Vuforia.VuforiaRuntimeUtilities_CameraState::<DeviceMode>k__BackingField
	int32_t ___U3CDeviceModeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6, ___U3CInitializedU3Ek__BackingField_0)); }
	inline bool get_U3CInitializedU3Ek__BackingField_0() const { return ___U3CInitializedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_0() { return &___U3CInitializedU3Ek__BackingField_0; }
	inline void set_U3CInitializedU3Ek__BackingField_0(bool value)
	{
		___U3CInitializedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CActiveU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6, ___U3CActiveU3Ek__BackingField_1)); }
	inline bool get_U3CActiveU3Ek__BackingField_1() const { return ___U3CActiveU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CActiveU3Ek__BackingField_1() { return &___U3CActiveU3Ek__BackingField_1; }
	inline void set_U3CActiveU3Ek__BackingField_1(bool value)
	{
		___U3CActiveU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDeviceModeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6, ___U3CDeviceModeU3Ek__BackingField_2)); }
	inline int32_t get_U3CDeviceModeU3Ek__BackingField_2() const { return ___U3CDeviceModeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CDeviceModeU3Ek__BackingField_2() { return &___U3CDeviceModeU3Ek__BackingField_2; }
	inline void set_U3CDeviceModeU3Ek__BackingField_2(int32_t value)
	{
		___U3CDeviceModeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.VuforiaRuntimeUtilities/CameraState
struct CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6_marshaled_pinvoke
{
	int32_t ___U3CInitializedU3Ek__BackingField_0;
	int32_t ___U3CActiveU3Ek__BackingField_1;
	int32_t ___U3CDeviceModeU3Ek__BackingField_2;
};
// Native definition for COM marshalling of Vuforia.VuforiaRuntimeUtilities/CameraState
struct CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6_marshaled_com
{
	int32_t ___U3CInitializedU3Ek__BackingField_0;
	int32_t ___U3CActiveU3Ek__BackingField_1;
	int32_t ___U3CDeviceModeU3Ek__BackingField_2;
};
#endif // CAMERASTATE_T4A1994F5F7B9665FD6E136894A4586C0F1E405A6_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef AMBIENTOCCLUSIONMODEPARAMETER_T4C91527D21B57CFBD2DA5A8AAAB48B89555ED8B6_H
#define AMBIENTOCCLUSIONMODEPARAMETER_T4C91527D21B57CFBD2DA5A8AAAB48B89555ED8B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusionModeParameter
struct  AmbientOcclusionModeParameter_t4C91527D21B57CFBD2DA5A8AAAB48B89555ED8B6  : public ParameterOverride_1_tB4B18E969640B6C93068EF667D413C1DF741F342
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONMODEPARAMETER_T4C91527D21B57CFBD2DA5A8AAAB48B89555ED8B6_H
#ifndef AMBIENTOCCLUSIONQUALITYPARAMETER_T293F9DA77655C53F24A5AFC38014DBCEB25D8ECC_H
#define AMBIENTOCCLUSIONQUALITYPARAMETER_T293F9DA77655C53F24A5AFC38014DBCEB25D8ECC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusionQualityParameter
struct  AmbientOcclusionQualityParameter_t293F9DA77655C53F24A5AFC38014DBCEB25D8ECC  : public ParameterOverride_1_t6A5DF2D1C2BF88EA35C618A18492895C0F54A473
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONQUALITYPARAMETER_T293F9DA77655C53F24A5AFC38014DBCEB25D8ECC_H
#ifndef EYEADAPTATIONPARAMETER_TAECAA41037553C4EAAA628E6840DD6EEC8C84036_H
#define EYEADAPTATIONPARAMETER_TAECAA41037553C4EAAA628E6840DD6EEC8C84036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.EyeAdaptationParameter
struct  EyeAdaptationParameter_tAECAA41037553C4EAAA628E6840DD6EEC8C84036  : public ParameterOverride_1_t757461DE48A2FBE0DB6B12C864033CCD202AF941
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONPARAMETER_TAECAA41037553C4EAAA628E6840DD6EEC8C84036_H
#ifndef GRADINGMODEPARAMETER_TBC84E2E66719BACCD847B301C1F156E4A340362A_H
#define GRADINGMODEPARAMETER_TBC84E2E66719BACCD847B301C1F156E4A340362A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.GradingModeParameter
struct  GradingModeParameter_tBC84E2E66719BACCD847B301C1F156E4A340362A  : public ParameterOverride_1_t231434EB436B677A1218432A1AF3D6CF5D057EAE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADINGMODEPARAMETER_TBC84E2E66719BACCD847B301C1F156E4A340362A_H
#ifndef KERNELSIZEPARAMETER_T75F71C05A913F8456F68E98BD412A8B9990E2140_H
#define KERNELSIZEPARAMETER_T75F71C05A913F8456F68E98BD412A8B9990E2140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.KernelSizeParameter
struct  KernelSizeParameter_t75F71C05A913F8456F68E98BD412A8B9990E2140  : public ParameterOverride_1_tE8E6764045AEC904A3C2879ABCC08C9B801DAC48
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNELSIZEPARAMETER_T75F71C05A913F8456F68E98BD412A8B9990E2140_H
#ifndef POSTPROCESSEFFECTSETTINGS_TF399FB819B12B3398DDDEF1AAA1FB0685C524042_H
#define POSTPROCESSEFFECTSETTINGS_TF399FB819B12B3398DDDEF1AAA1FB0685C524042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings
struct  PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::active
	bool ___active_4;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::enabled
	BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * ___enabled_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.PostProcessing.ParameterOverride> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::parameters
	ReadOnlyCollection_1_t2D7596D677CC341719EF47DD23F94617FB662DED * ___parameters_6;

public:
	inline static int32_t get_offset_of_active_4() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042, ___active_4)); }
	inline bool get_active_4() const { return ___active_4; }
	inline bool* get_address_of_active_4() { return &___active_4; }
	inline void set_active_4(bool value)
	{
		___active_4 = value;
	}

	inline static int32_t get_offset_of_enabled_5() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042, ___enabled_5)); }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * get_enabled_5() const { return ___enabled_5; }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE ** get_address_of_enabled_5() { return &___enabled_5; }
	inline void set_enabled_5(BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * value)
	{
		___enabled_5 = value;
		Il2CppCodeGenWriteBarrier((&___enabled_5), value);
	}

	inline static int32_t get_offset_of_parameters_6() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042, ___parameters_6)); }
	inline ReadOnlyCollection_1_t2D7596D677CC341719EF47DD23F94617FB662DED * get_parameters_6() const { return ___parameters_6; }
	inline ReadOnlyCollection_1_t2D7596D677CC341719EF47DD23F94617FB662DED ** get_address_of_parameters_6() { return &___parameters_6; }
	inline void set_parameters_6(ReadOnlyCollection_1_t2D7596D677CC341719EF47DD23F94617FB662DED * value)
	{
		___parameters_6 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSEFFECTSETTINGS_TF399FB819B12B3398DDDEF1AAA1FB0685C524042_H
#ifndef TONEMAPPERPARAMETER_TD2E82FA78543CD6AAA7C34BCF1BFD5F3C1CE65EF_H
#define TONEMAPPERPARAMETER_TD2E82FA78543CD6AAA7C34BCF1BFD5F3C1CE65EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.TonemapperParameter
struct  TonemapperParameter_tD2E82FA78543CD6AAA7C34BCF1BFD5F3C1CE65EF  : public ParameterOverride_1_t9F3AFB32918ADB96160F6179BBD83B2105DCFFF0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPERPARAMETER_TD2E82FA78543CD6AAA7C34BCF1BFD5F3C1CE65EF_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef AMBIENTOCCLUSION_T087AD38AD8CB17B86F3810AFA8128118A321CE5B_H
#define AMBIENTOCCLUSION_T087AD38AD8CB17B86F3810AFA8128118A321CE5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AmbientOcclusion
struct  AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.AmbientOcclusionModeParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::mode
	AmbientOcclusionModeParameter_t4C91527D21B57CFBD2DA5A8AAAB48B89555ED8B6 * ___mode_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::intensity
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___intensity_8;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::color
	ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * ___color_9;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::ambientOnly
	BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * ___ambientOnly_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::noiseFilterTolerance
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___noiseFilterTolerance_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::blurTolerance
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___blurTolerance_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::upsampleTolerance
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___upsampleTolerance_13;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::thicknessModifier
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___thicknessModifier_14;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::directLightingStrength
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___directLightingStrength_15;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::radius
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___radius_16;
	// UnityEngine.Rendering.PostProcessing.AmbientOcclusionQualityParameter UnityEngine.Rendering.PostProcessing.AmbientOcclusion::quality
	AmbientOcclusionQualityParameter_t293F9DA77655C53F24A5AFC38014DBCEB25D8ECC * ___quality_17;

public:
	inline static int32_t get_offset_of_mode_7() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___mode_7)); }
	inline AmbientOcclusionModeParameter_t4C91527D21B57CFBD2DA5A8AAAB48B89555ED8B6 * get_mode_7() const { return ___mode_7; }
	inline AmbientOcclusionModeParameter_t4C91527D21B57CFBD2DA5A8AAAB48B89555ED8B6 ** get_address_of_mode_7() { return &___mode_7; }
	inline void set_mode_7(AmbientOcclusionModeParameter_t4C91527D21B57CFBD2DA5A8AAAB48B89555ED8B6 * value)
	{
		___mode_7 = value;
		Il2CppCodeGenWriteBarrier((&___mode_7), value);
	}

	inline static int32_t get_offset_of_intensity_8() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___intensity_8)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_intensity_8() const { return ___intensity_8; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_intensity_8() { return &___intensity_8; }
	inline void set_intensity_8(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___intensity_8 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_8), value);
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___color_9)); }
	inline ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * get_color_9() const { return ___color_9; }
	inline ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B ** get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * value)
	{
		___color_9 = value;
		Il2CppCodeGenWriteBarrier((&___color_9), value);
	}

	inline static int32_t get_offset_of_ambientOnly_10() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___ambientOnly_10)); }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * get_ambientOnly_10() const { return ___ambientOnly_10; }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE ** get_address_of_ambientOnly_10() { return &___ambientOnly_10; }
	inline void set_ambientOnly_10(BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * value)
	{
		___ambientOnly_10 = value;
		Il2CppCodeGenWriteBarrier((&___ambientOnly_10), value);
	}

	inline static int32_t get_offset_of_noiseFilterTolerance_11() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___noiseFilterTolerance_11)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_noiseFilterTolerance_11() const { return ___noiseFilterTolerance_11; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_noiseFilterTolerance_11() { return &___noiseFilterTolerance_11; }
	inline void set_noiseFilterTolerance_11(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___noiseFilterTolerance_11 = value;
		Il2CppCodeGenWriteBarrier((&___noiseFilterTolerance_11), value);
	}

	inline static int32_t get_offset_of_blurTolerance_12() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___blurTolerance_12)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_blurTolerance_12() const { return ___blurTolerance_12; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_blurTolerance_12() { return &___blurTolerance_12; }
	inline void set_blurTolerance_12(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___blurTolerance_12 = value;
		Il2CppCodeGenWriteBarrier((&___blurTolerance_12), value);
	}

	inline static int32_t get_offset_of_upsampleTolerance_13() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___upsampleTolerance_13)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_upsampleTolerance_13() const { return ___upsampleTolerance_13; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_upsampleTolerance_13() { return &___upsampleTolerance_13; }
	inline void set_upsampleTolerance_13(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___upsampleTolerance_13 = value;
		Il2CppCodeGenWriteBarrier((&___upsampleTolerance_13), value);
	}

	inline static int32_t get_offset_of_thicknessModifier_14() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___thicknessModifier_14)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_thicknessModifier_14() const { return ___thicknessModifier_14; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_thicknessModifier_14() { return &___thicknessModifier_14; }
	inline void set_thicknessModifier_14(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___thicknessModifier_14 = value;
		Il2CppCodeGenWriteBarrier((&___thicknessModifier_14), value);
	}

	inline static int32_t get_offset_of_directLightingStrength_15() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___directLightingStrength_15)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_directLightingStrength_15() const { return ___directLightingStrength_15; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_directLightingStrength_15() { return &___directLightingStrength_15; }
	inline void set_directLightingStrength_15(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___directLightingStrength_15 = value;
		Il2CppCodeGenWriteBarrier((&___directLightingStrength_15), value);
	}

	inline static int32_t get_offset_of_radius_16() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___radius_16)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_radius_16() const { return ___radius_16; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_radius_16() { return &___radius_16; }
	inline void set_radius_16(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___radius_16 = value;
		Il2CppCodeGenWriteBarrier((&___radius_16), value);
	}

	inline static int32_t get_offset_of_quality_17() { return static_cast<int32_t>(offsetof(AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B, ___quality_17)); }
	inline AmbientOcclusionQualityParameter_t293F9DA77655C53F24A5AFC38014DBCEB25D8ECC * get_quality_17() const { return ___quality_17; }
	inline AmbientOcclusionQualityParameter_t293F9DA77655C53F24A5AFC38014DBCEB25D8ECC ** get_address_of_quality_17() { return &___quality_17; }
	inline void set_quality_17(AmbientOcclusionQualityParameter_t293F9DA77655C53F24A5AFC38014DBCEB25D8ECC * value)
	{
		___quality_17 = value;
		Il2CppCodeGenWriteBarrier((&___quality_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSION_T087AD38AD8CB17B86F3810AFA8128118A321CE5B_H
#ifndef AUTOEXPOSURE_T5573D856D6AF109940FF24FD7051FCC6412FB39A_H
#define AUTOEXPOSURE_T5573D856D6AF109940FF24FD7051FCC6412FB39A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.AutoExposure
struct  AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.Vector2Parameter UnityEngine.Rendering.PostProcessing.AutoExposure::filtering
	Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2 * ___filtering_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AutoExposure::minLuminance
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___minLuminance_8;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AutoExposure::maxLuminance
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___maxLuminance_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AutoExposure::keyValue
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___keyValue_10;
	// UnityEngine.Rendering.PostProcessing.EyeAdaptationParameter UnityEngine.Rendering.PostProcessing.AutoExposure::eyeAdaptation
	EyeAdaptationParameter_tAECAA41037553C4EAAA628E6840DD6EEC8C84036 * ___eyeAdaptation_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AutoExposure::speedUp
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___speedUp_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.AutoExposure::speedDown
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___speedDown_13;

public:
	inline static int32_t get_offset_of_filtering_7() { return static_cast<int32_t>(offsetof(AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A, ___filtering_7)); }
	inline Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2 * get_filtering_7() const { return ___filtering_7; }
	inline Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2 ** get_address_of_filtering_7() { return &___filtering_7; }
	inline void set_filtering_7(Vector2Parameter_tBA2A4564AB00EBFD802D707178EEF0C9C9DAE1C2 * value)
	{
		___filtering_7 = value;
		Il2CppCodeGenWriteBarrier((&___filtering_7), value);
	}

	inline static int32_t get_offset_of_minLuminance_8() { return static_cast<int32_t>(offsetof(AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A, ___minLuminance_8)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_minLuminance_8() const { return ___minLuminance_8; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_minLuminance_8() { return &___minLuminance_8; }
	inline void set_minLuminance_8(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___minLuminance_8 = value;
		Il2CppCodeGenWriteBarrier((&___minLuminance_8), value);
	}

	inline static int32_t get_offset_of_maxLuminance_9() { return static_cast<int32_t>(offsetof(AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A, ___maxLuminance_9)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_maxLuminance_9() const { return ___maxLuminance_9; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_maxLuminance_9() { return &___maxLuminance_9; }
	inline void set_maxLuminance_9(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___maxLuminance_9 = value;
		Il2CppCodeGenWriteBarrier((&___maxLuminance_9), value);
	}

	inline static int32_t get_offset_of_keyValue_10() { return static_cast<int32_t>(offsetof(AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A, ___keyValue_10)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_keyValue_10() const { return ___keyValue_10; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_keyValue_10() { return &___keyValue_10; }
	inline void set_keyValue_10(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___keyValue_10 = value;
		Il2CppCodeGenWriteBarrier((&___keyValue_10), value);
	}

	inline static int32_t get_offset_of_eyeAdaptation_11() { return static_cast<int32_t>(offsetof(AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A, ___eyeAdaptation_11)); }
	inline EyeAdaptationParameter_tAECAA41037553C4EAAA628E6840DD6EEC8C84036 * get_eyeAdaptation_11() const { return ___eyeAdaptation_11; }
	inline EyeAdaptationParameter_tAECAA41037553C4EAAA628E6840DD6EEC8C84036 ** get_address_of_eyeAdaptation_11() { return &___eyeAdaptation_11; }
	inline void set_eyeAdaptation_11(EyeAdaptationParameter_tAECAA41037553C4EAAA628E6840DD6EEC8C84036 * value)
	{
		___eyeAdaptation_11 = value;
		Il2CppCodeGenWriteBarrier((&___eyeAdaptation_11), value);
	}

	inline static int32_t get_offset_of_speedUp_12() { return static_cast<int32_t>(offsetof(AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A, ___speedUp_12)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_speedUp_12() const { return ___speedUp_12; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_speedUp_12() { return &___speedUp_12; }
	inline void set_speedUp_12(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___speedUp_12 = value;
		Il2CppCodeGenWriteBarrier((&___speedUp_12), value);
	}

	inline static int32_t get_offset_of_speedDown_13() { return static_cast<int32_t>(offsetof(AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A, ___speedDown_13)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_speedDown_13() const { return ___speedDown_13; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_speedDown_13() { return &___speedDown_13; }
	inline void set_speedDown_13(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___speedDown_13 = value;
		Il2CppCodeGenWriteBarrier((&___speedDown_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOEXPOSURE_T5573D856D6AF109940FF24FD7051FCC6412FB39A_H
#ifndef BLOOM_TA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0_H
#define BLOOM_TA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Bloom
struct  Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::intensity
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___intensity_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::threshold
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___threshold_8;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::softKnee
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___softKnee_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::clamp
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___clamp_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::diffusion
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___diffusion_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::anamorphicRatio
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___anamorphicRatio_12;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.Bloom::color
	ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * ___color_13;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.Bloom::fastMode
	BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * ___fastMode_14;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.Bloom::dirtTexture
	TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * ___dirtTexture_15;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::dirtIntensity
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___dirtIntensity_16;

public:
	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0, ___intensity_7)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_intensity_7() const { return ___intensity_7; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___intensity_7 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_7), value);
	}

	inline static int32_t get_offset_of_threshold_8() { return static_cast<int32_t>(offsetof(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0, ___threshold_8)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_threshold_8() const { return ___threshold_8; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_threshold_8() { return &___threshold_8; }
	inline void set_threshold_8(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___threshold_8 = value;
		Il2CppCodeGenWriteBarrier((&___threshold_8), value);
	}

	inline static int32_t get_offset_of_softKnee_9() { return static_cast<int32_t>(offsetof(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0, ___softKnee_9)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_softKnee_9() const { return ___softKnee_9; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_softKnee_9() { return &___softKnee_9; }
	inline void set_softKnee_9(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___softKnee_9 = value;
		Il2CppCodeGenWriteBarrier((&___softKnee_9), value);
	}

	inline static int32_t get_offset_of_clamp_10() { return static_cast<int32_t>(offsetof(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0, ___clamp_10)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_clamp_10() const { return ___clamp_10; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_clamp_10() { return &___clamp_10; }
	inline void set_clamp_10(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___clamp_10 = value;
		Il2CppCodeGenWriteBarrier((&___clamp_10), value);
	}

	inline static int32_t get_offset_of_diffusion_11() { return static_cast<int32_t>(offsetof(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0, ___diffusion_11)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_diffusion_11() const { return ___diffusion_11; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_diffusion_11() { return &___diffusion_11; }
	inline void set_diffusion_11(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___diffusion_11 = value;
		Il2CppCodeGenWriteBarrier((&___diffusion_11), value);
	}

	inline static int32_t get_offset_of_anamorphicRatio_12() { return static_cast<int32_t>(offsetof(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0, ___anamorphicRatio_12)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_anamorphicRatio_12() const { return ___anamorphicRatio_12; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_anamorphicRatio_12() { return &___anamorphicRatio_12; }
	inline void set_anamorphicRatio_12(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___anamorphicRatio_12 = value;
		Il2CppCodeGenWriteBarrier((&___anamorphicRatio_12), value);
	}

	inline static int32_t get_offset_of_color_13() { return static_cast<int32_t>(offsetof(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0, ___color_13)); }
	inline ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * get_color_13() const { return ___color_13; }
	inline ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B ** get_address_of_color_13() { return &___color_13; }
	inline void set_color_13(ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * value)
	{
		___color_13 = value;
		Il2CppCodeGenWriteBarrier((&___color_13), value);
	}

	inline static int32_t get_offset_of_fastMode_14() { return static_cast<int32_t>(offsetof(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0, ___fastMode_14)); }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * get_fastMode_14() const { return ___fastMode_14; }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE ** get_address_of_fastMode_14() { return &___fastMode_14; }
	inline void set_fastMode_14(BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * value)
	{
		___fastMode_14 = value;
		Il2CppCodeGenWriteBarrier((&___fastMode_14), value);
	}

	inline static int32_t get_offset_of_dirtTexture_15() { return static_cast<int32_t>(offsetof(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0, ___dirtTexture_15)); }
	inline TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * get_dirtTexture_15() const { return ___dirtTexture_15; }
	inline TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 ** get_address_of_dirtTexture_15() { return &___dirtTexture_15; }
	inline void set_dirtTexture_15(TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * value)
	{
		___dirtTexture_15 = value;
		Il2CppCodeGenWriteBarrier((&___dirtTexture_15), value);
	}

	inline static int32_t get_offset_of_dirtIntensity_16() { return static_cast<int32_t>(offsetof(Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0, ___dirtIntensity_16)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_dirtIntensity_16() const { return ___dirtIntensity_16; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_dirtIntensity_16() { return &___dirtIntensity_16; }
	inline void set_dirtIntensity_16(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___dirtIntensity_16 = value;
		Il2CppCodeGenWriteBarrier((&___dirtIntensity_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOM_TA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0_H
#ifndef CHROMATICABERRATION_TA39377A5D30FD917158162E01C35B0EDB4BD3B1D_H
#define CHROMATICABERRATION_TA39377A5D30FD917158162E01C35B0EDB4BD3B1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ChromaticAberration
struct  ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.ChromaticAberration::spectralLut
	TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * ___spectralLut_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ChromaticAberration::intensity
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___intensity_8;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.ChromaticAberration::fastMode
	BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * ___fastMode_9;

public:
	inline static int32_t get_offset_of_spectralLut_7() { return static_cast<int32_t>(offsetof(ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D, ___spectralLut_7)); }
	inline TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * get_spectralLut_7() const { return ___spectralLut_7; }
	inline TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 ** get_address_of_spectralLut_7() { return &___spectralLut_7; }
	inline void set_spectralLut_7(TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * value)
	{
		___spectralLut_7 = value;
		Il2CppCodeGenWriteBarrier((&___spectralLut_7), value);
	}

	inline static int32_t get_offset_of_intensity_8() { return static_cast<int32_t>(offsetof(ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D, ___intensity_8)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_intensity_8() const { return ___intensity_8; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_intensity_8() { return &___intensity_8; }
	inline void set_intensity_8(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___intensity_8 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_8), value);
	}

	inline static int32_t get_offset_of_fastMode_9() { return static_cast<int32_t>(offsetof(ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D, ___fastMode_9)); }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * get_fastMode_9() const { return ___fastMode_9; }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE ** get_address_of_fastMode_9() { return &___fastMode_9; }
	inline void set_fastMode_9(BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * value)
	{
		___fastMode_9 = value;
		Il2CppCodeGenWriteBarrier((&___fastMode_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATION_TA39377A5D30FD917158162E01C35B0EDB4BD3B1D_H
#ifndef COLORGRADING_T12FC06E67BEB22D3368F661CEC790B9B6A81E035_H
#define COLORGRADING_T12FC06E67BEB22D3368F661CEC790B9B6A81E035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.ColorGrading
struct  ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.GradingModeParameter UnityEngine.Rendering.PostProcessing.ColorGrading::gradingMode
	GradingModeParameter_tBC84E2E66719BACCD847B301C1F156E4A340362A * ___gradingMode_7;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.ColorGrading::externalLut
	TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * ___externalLut_8;
	// UnityEngine.Rendering.PostProcessing.TonemapperParameter UnityEngine.Rendering.PostProcessing.ColorGrading::tonemapper
	TonemapperParameter_tD2E82FA78543CD6AAA7C34BCF1BFD5F3C1CE65EF * ___tonemapper_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveToeStrength
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___toneCurveToeStrength_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveToeLength
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___toneCurveToeLength_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveShoulderStrength
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___toneCurveShoulderStrength_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveShoulderLength
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___toneCurveShoulderLength_13;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveShoulderAngle
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___toneCurveShoulderAngle_14;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveGamma
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___toneCurveGamma_15;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.ColorGrading::ldrLut
	TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * ___ldrLut_16;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::ldrLutContribution
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___ldrLutContribution_17;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::temperature
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___temperature_18;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::tint
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___tint_19;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.ColorGrading::colorFilter
	ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * ___colorFilter_20;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::hueShift
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___hueShift_21;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::saturation
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___saturation_22;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::brightness
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___brightness_23;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::postExposure
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___postExposure_24;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::contrast
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___contrast_25;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerRedOutRedIn
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___mixerRedOutRedIn_26;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerRedOutGreenIn
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___mixerRedOutGreenIn_27;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerRedOutBlueIn
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___mixerRedOutBlueIn_28;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerGreenOutRedIn
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___mixerGreenOutRedIn_29;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerGreenOutGreenIn
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___mixerGreenOutGreenIn_30;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerGreenOutBlueIn
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___mixerGreenOutBlueIn_31;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerBlueOutRedIn
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___mixerBlueOutRedIn_32;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerBlueOutGreenIn
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___mixerBlueOutGreenIn_33;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerBlueOutBlueIn
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___mixerBlueOutBlueIn_34;
	// UnityEngine.Rendering.PostProcessing.Vector4Parameter UnityEngine.Rendering.PostProcessing.ColorGrading::lift
	Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 * ___lift_35;
	// UnityEngine.Rendering.PostProcessing.Vector4Parameter UnityEngine.Rendering.PostProcessing.ColorGrading::gamma
	Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 * ___gamma_36;
	// UnityEngine.Rendering.PostProcessing.Vector4Parameter UnityEngine.Rendering.PostProcessing.ColorGrading::gain
	Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 * ___gain_37;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::masterCurve
	SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * ___masterCurve_38;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::redCurve
	SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * ___redCurve_39;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::greenCurve
	SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * ___greenCurve_40;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::blueCurve
	SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * ___blueCurve_41;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::hueVsHueCurve
	SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * ___hueVsHueCurve_42;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::hueVsSatCurve
	SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * ___hueVsSatCurve_43;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::satVsSatCurve
	SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * ___satVsSatCurve_44;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::lumVsSatCurve
	SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * ___lumVsSatCurve_45;

public:
	inline static int32_t get_offset_of_gradingMode_7() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___gradingMode_7)); }
	inline GradingModeParameter_tBC84E2E66719BACCD847B301C1F156E4A340362A * get_gradingMode_7() const { return ___gradingMode_7; }
	inline GradingModeParameter_tBC84E2E66719BACCD847B301C1F156E4A340362A ** get_address_of_gradingMode_7() { return &___gradingMode_7; }
	inline void set_gradingMode_7(GradingModeParameter_tBC84E2E66719BACCD847B301C1F156E4A340362A * value)
	{
		___gradingMode_7 = value;
		Il2CppCodeGenWriteBarrier((&___gradingMode_7), value);
	}

	inline static int32_t get_offset_of_externalLut_8() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___externalLut_8)); }
	inline TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * get_externalLut_8() const { return ___externalLut_8; }
	inline TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 ** get_address_of_externalLut_8() { return &___externalLut_8; }
	inline void set_externalLut_8(TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * value)
	{
		___externalLut_8 = value;
		Il2CppCodeGenWriteBarrier((&___externalLut_8), value);
	}

	inline static int32_t get_offset_of_tonemapper_9() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___tonemapper_9)); }
	inline TonemapperParameter_tD2E82FA78543CD6AAA7C34BCF1BFD5F3C1CE65EF * get_tonemapper_9() const { return ___tonemapper_9; }
	inline TonemapperParameter_tD2E82FA78543CD6AAA7C34BCF1BFD5F3C1CE65EF ** get_address_of_tonemapper_9() { return &___tonemapper_9; }
	inline void set_tonemapper_9(TonemapperParameter_tD2E82FA78543CD6AAA7C34BCF1BFD5F3C1CE65EF * value)
	{
		___tonemapper_9 = value;
		Il2CppCodeGenWriteBarrier((&___tonemapper_9), value);
	}

	inline static int32_t get_offset_of_toneCurveToeStrength_10() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___toneCurveToeStrength_10)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_toneCurveToeStrength_10() const { return ___toneCurveToeStrength_10; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_toneCurveToeStrength_10() { return &___toneCurveToeStrength_10; }
	inline void set_toneCurveToeStrength_10(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___toneCurveToeStrength_10 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveToeStrength_10), value);
	}

	inline static int32_t get_offset_of_toneCurveToeLength_11() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___toneCurveToeLength_11)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_toneCurveToeLength_11() const { return ___toneCurveToeLength_11; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_toneCurveToeLength_11() { return &___toneCurveToeLength_11; }
	inline void set_toneCurveToeLength_11(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___toneCurveToeLength_11 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveToeLength_11), value);
	}

	inline static int32_t get_offset_of_toneCurveShoulderStrength_12() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___toneCurveShoulderStrength_12)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_toneCurveShoulderStrength_12() const { return ___toneCurveShoulderStrength_12; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_toneCurveShoulderStrength_12() { return &___toneCurveShoulderStrength_12; }
	inline void set_toneCurveShoulderStrength_12(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___toneCurveShoulderStrength_12 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveShoulderStrength_12), value);
	}

	inline static int32_t get_offset_of_toneCurveShoulderLength_13() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___toneCurveShoulderLength_13)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_toneCurveShoulderLength_13() const { return ___toneCurveShoulderLength_13; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_toneCurveShoulderLength_13() { return &___toneCurveShoulderLength_13; }
	inline void set_toneCurveShoulderLength_13(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___toneCurveShoulderLength_13 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveShoulderLength_13), value);
	}

	inline static int32_t get_offset_of_toneCurveShoulderAngle_14() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___toneCurveShoulderAngle_14)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_toneCurveShoulderAngle_14() const { return ___toneCurveShoulderAngle_14; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_toneCurveShoulderAngle_14() { return &___toneCurveShoulderAngle_14; }
	inline void set_toneCurveShoulderAngle_14(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___toneCurveShoulderAngle_14 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveShoulderAngle_14), value);
	}

	inline static int32_t get_offset_of_toneCurveGamma_15() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___toneCurveGamma_15)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_toneCurveGamma_15() const { return ___toneCurveGamma_15; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_toneCurveGamma_15() { return &___toneCurveGamma_15; }
	inline void set_toneCurveGamma_15(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___toneCurveGamma_15 = value;
		Il2CppCodeGenWriteBarrier((&___toneCurveGamma_15), value);
	}

	inline static int32_t get_offset_of_ldrLut_16() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___ldrLut_16)); }
	inline TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * get_ldrLut_16() const { return ___ldrLut_16; }
	inline TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 ** get_address_of_ldrLut_16() { return &___ldrLut_16; }
	inline void set_ldrLut_16(TextureParameter_t585968DEF0992D732C2826195CA4F7D721462A87 * value)
	{
		___ldrLut_16 = value;
		Il2CppCodeGenWriteBarrier((&___ldrLut_16), value);
	}

	inline static int32_t get_offset_of_ldrLutContribution_17() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___ldrLutContribution_17)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_ldrLutContribution_17() const { return ___ldrLutContribution_17; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_ldrLutContribution_17() { return &___ldrLutContribution_17; }
	inline void set_ldrLutContribution_17(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___ldrLutContribution_17 = value;
		Il2CppCodeGenWriteBarrier((&___ldrLutContribution_17), value);
	}

	inline static int32_t get_offset_of_temperature_18() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___temperature_18)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_temperature_18() const { return ___temperature_18; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_temperature_18() { return &___temperature_18; }
	inline void set_temperature_18(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___temperature_18 = value;
		Il2CppCodeGenWriteBarrier((&___temperature_18), value);
	}

	inline static int32_t get_offset_of_tint_19() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___tint_19)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_tint_19() const { return ___tint_19; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_tint_19() { return &___tint_19; }
	inline void set_tint_19(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___tint_19 = value;
		Il2CppCodeGenWriteBarrier((&___tint_19), value);
	}

	inline static int32_t get_offset_of_colorFilter_20() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___colorFilter_20)); }
	inline ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * get_colorFilter_20() const { return ___colorFilter_20; }
	inline ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B ** get_address_of_colorFilter_20() { return &___colorFilter_20; }
	inline void set_colorFilter_20(ColorParameter_tB218BF7292D70153A62E291FDF6C7534D5CA084B * value)
	{
		___colorFilter_20 = value;
		Il2CppCodeGenWriteBarrier((&___colorFilter_20), value);
	}

	inline static int32_t get_offset_of_hueShift_21() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___hueShift_21)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_hueShift_21() const { return ___hueShift_21; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_hueShift_21() { return &___hueShift_21; }
	inline void set_hueShift_21(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___hueShift_21 = value;
		Il2CppCodeGenWriteBarrier((&___hueShift_21), value);
	}

	inline static int32_t get_offset_of_saturation_22() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___saturation_22)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_saturation_22() const { return ___saturation_22; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_saturation_22() { return &___saturation_22; }
	inline void set_saturation_22(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___saturation_22 = value;
		Il2CppCodeGenWriteBarrier((&___saturation_22), value);
	}

	inline static int32_t get_offset_of_brightness_23() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___brightness_23)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_brightness_23() const { return ___brightness_23; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_brightness_23() { return &___brightness_23; }
	inline void set_brightness_23(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___brightness_23 = value;
		Il2CppCodeGenWriteBarrier((&___brightness_23), value);
	}

	inline static int32_t get_offset_of_postExposure_24() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___postExposure_24)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_postExposure_24() const { return ___postExposure_24; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_postExposure_24() { return &___postExposure_24; }
	inline void set_postExposure_24(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___postExposure_24 = value;
		Il2CppCodeGenWriteBarrier((&___postExposure_24), value);
	}

	inline static int32_t get_offset_of_contrast_25() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___contrast_25)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_contrast_25() const { return ___contrast_25; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_contrast_25() { return &___contrast_25; }
	inline void set_contrast_25(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___contrast_25 = value;
		Il2CppCodeGenWriteBarrier((&___contrast_25), value);
	}

	inline static int32_t get_offset_of_mixerRedOutRedIn_26() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___mixerRedOutRedIn_26)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_mixerRedOutRedIn_26() const { return ___mixerRedOutRedIn_26; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_mixerRedOutRedIn_26() { return &___mixerRedOutRedIn_26; }
	inline void set_mixerRedOutRedIn_26(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___mixerRedOutRedIn_26 = value;
		Il2CppCodeGenWriteBarrier((&___mixerRedOutRedIn_26), value);
	}

	inline static int32_t get_offset_of_mixerRedOutGreenIn_27() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___mixerRedOutGreenIn_27)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_mixerRedOutGreenIn_27() const { return ___mixerRedOutGreenIn_27; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_mixerRedOutGreenIn_27() { return &___mixerRedOutGreenIn_27; }
	inline void set_mixerRedOutGreenIn_27(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___mixerRedOutGreenIn_27 = value;
		Il2CppCodeGenWriteBarrier((&___mixerRedOutGreenIn_27), value);
	}

	inline static int32_t get_offset_of_mixerRedOutBlueIn_28() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___mixerRedOutBlueIn_28)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_mixerRedOutBlueIn_28() const { return ___mixerRedOutBlueIn_28; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_mixerRedOutBlueIn_28() { return &___mixerRedOutBlueIn_28; }
	inline void set_mixerRedOutBlueIn_28(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___mixerRedOutBlueIn_28 = value;
		Il2CppCodeGenWriteBarrier((&___mixerRedOutBlueIn_28), value);
	}

	inline static int32_t get_offset_of_mixerGreenOutRedIn_29() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___mixerGreenOutRedIn_29)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_mixerGreenOutRedIn_29() const { return ___mixerGreenOutRedIn_29; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_mixerGreenOutRedIn_29() { return &___mixerGreenOutRedIn_29; }
	inline void set_mixerGreenOutRedIn_29(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___mixerGreenOutRedIn_29 = value;
		Il2CppCodeGenWriteBarrier((&___mixerGreenOutRedIn_29), value);
	}

	inline static int32_t get_offset_of_mixerGreenOutGreenIn_30() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___mixerGreenOutGreenIn_30)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_mixerGreenOutGreenIn_30() const { return ___mixerGreenOutGreenIn_30; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_mixerGreenOutGreenIn_30() { return &___mixerGreenOutGreenIn_30; }
	inline void set_mixerGreenOutGreenIn_30(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___mixerGreenOutGreenIn_30 = value;
		Il2CppCodeGenWriteBarrier((&___mixerGreenOutGreenIn_30), value);
	}

	inline static int32_t get_offset_of_mixerGreenOutBlueIn_31() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___mixerGreenOutBlueIn_31)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_mixerGreenOutBlueIn_31() const { return ___mixerGreenOutBlueIn_31; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_mixerGreenOutBlueIn_31() { return &___mixerGreenOutBlueIn_31; }
	inline void set_mixerGreenOutBlueIn_31(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___mixerGreenOutBlueIn_31 = value;
		Il2CppCodeGenWriteBarrier((&___mixerGreenOutBlueIn_31), value);
	}

	inline static int32_t get_offset_of_mixerBlueOutRedIn_32() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___mixerBlueOutRedIn_32)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_mixerBlueOutRedIn_32() const { return ___mixerBlueOutRedIn_32; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_mixerBlueOutRedIn_32() { return &___mixerBlueOutRedIn_32; }
	inline void set_mixerBlueOutRedIn_32(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___mixerBlueOutRedIn_32 = value;
		Il2CppCodeGenWriteBarrier((&___mixerBlueOutRedIn_32), value);
	}

	inline static int32_t get_offset_of_mixerBlueOutGreenIn_33() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___mixerBlueOutGreenIn_33)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_mixerBlueOutGreenIn_33() const { return ___mixerBlueOutGreenIn_33; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_mixerBlueOutGreenIn_33() { return &___mixerBlueOutGreenIn_33; }
	inline void set_mixerBlueOutGreenIn_33(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___mixerBlueOutGreenIn_33 = value;
		Il2CppCodeGenWriteBarrier((&___mixerBlueOutGreenIn_33), value);
	}

	inline static int32_t get_offset_of_mixerBlueOutBlueIn_34() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___mixerBlueOutBlueIn_34)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_mixerBlueOutBlueIn_34() const { return ___mixerBlueOutBlueIn_34; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_mixerBlueOutBlueIn_34() { return &___mixerBlueOutBlueIn_34; }
	inline void set_mixerBlueOutBlueIn_34(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___mixerBlueOutBlueIn_34 = value;
		Il2CppCodeGenWriteBarrier((&___mixerBlueOutBlueIn_34), value);
	}

	inline static int32_t get_offset_of_lift_35() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___lift_35)); }
	inline Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 * get_lift_35() const { return ___lift_35; }
	inline Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 ** get_address_of_lift_35() { return &___lift_35; }
	inline void set_lift_35(Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 * value)
	{
		___lift_35 = value;
		Il2CppCodeGenWriteBarrier((&___lift_35), value);
	}

	inline static int32_t get_offset_of_gamma_36() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___gamma_36)); }
	inline Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 * get_gamma_36() const { return ___gamma_36; }
	inline Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 ** get_address_of_gamma_36() { return &___gamma_36; }
	inline void set_gamma_36(Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 * value)
	{
		___gamma_36 = value;
		Il2CppCodeGenWriteBarrier((&___gamma_36), value);
	}

	inline static int32_t get_offset_of_gain_37() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___gain_37)); }
	inline Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 * get_gain_37() const { return ___gain_37; }
	inline Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 ** get_address_of_gain_37() { return &___gain_37; }
	inline void set_gain_37(Vector4Parameter_t9EBADB1F7E10F976BC1D7CE0569287C5FA0BF888 * value)
	{
		___gain_37 = value;
		Il2CppCodeGenWriteBarrier((&___gain_37), value);
	}

	inline static int32_t get_offset_of_masterCurve_38() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___masterCurve_38)); }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * get_masterCurve_38() const { return ___masterCurve_38; }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 ** get_address_of_masterCurve_38() { return &___masterCurve_38; }
	inline void set_masterCurve_38(SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * value)
	{
		___masterCurve_38 = value;
		Il2CppCodeGenWriteBarrier((&___masterCurve_38), value);
	}

	inline static int32_t get_offset_of_redCurve_39() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___redCurve_39)); }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * get_redCurve_39() const { return ___redCurve_39; }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 ** get_address_of_redCurve_39() { return &___redCurve_39; }
	inline void set_redCurve_39(SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * value)
	{
		___redCurve_39 = value;
		Il2CppCodeGenWriteBarrier((&___redCurve_39), value);
	}

	inline static int32_t get_offset_of_greenCurve_40() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___greenCurve_40)); }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * get_greenCurve_40() const { return ___greenCurve_40; }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 ** get_address_of_greenCurve_40() { return &___greenCurve_40; }
	inline void set_greenCurve_40(SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * value)
	{
		___greenCurve_40 = value;
		Il2CppCodeGenWriteBarrier((&___greenCurve_40), value);
	}

	inline static int32_t get_offset_of_blueCurve_41() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___blueCurve_41)); }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * get_blueCurve_41() const { return ___blueCurve_41; }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 ** get_address_of_blueCurve_41() { return &___blueCurve_41; }
	inline void set_blueCurve_41(SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * value)
	{
		___blueCurve_41 = value;
		Il2CppCodeGenWriteBarrier((&___blueCurve_41), value);
	}

	inline static int32_t get_offset_of_hueVsHueCurve_42() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___hueVsHueCurve_42)); }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * get_hueVsHueCurve_42() const { return ___hueVsHueCurve_42; }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 ** get_address_of_hueVsHueCurve_42() { return &___hueVsHueCurve_42; }
	inline void set_hueVsHueCurve_42(SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * value)
	{
		___hueVsHueCurve_42 = value;
		Il2CppCodeGenWriteBarrier((&___hueVsHueCurve_42), value);
	}

	inline static int32_t get_offset_of_hueVsSatCurve_43() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___hueVsSatCurve_43)); }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * get_hueVsSatCurve_43() const { return ___hueVsSatCurve_43; }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 ** get_address_of_hueVsSatCurve_43() { return &___hueVsSatCurve_43; }
	inline void set_hueVsSatCurve_43(SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * value)
	{
		___hueVsSatCurve_43 = value;
		Il2CppCodeGenWriteBarrier((&___hueVsSatCurve_43), value);
	}

	inline static int32_t get_offset_of_satVsSatCurve_44() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___satVsSatCurve_44)); }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * get_satVsSatCurve_44() const { return ___satVsSatCurve_44; }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 ** get_address_of_satVsSatCurve_44() { return &___satVsSatCurve_44; }
	inline void set_satVsSatCurve_44(SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * value)
	{
		___satVsSatCurve_44 = value;
		Il2CppCodeGenWriteBarrier((&___satVsSatCurve_44), value);
	}

	inline static int32_t get_offset_of_lumVsSatCurve_45() { return static_cast<int32_t>(offsetof(ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035, ___lumVsSatCurve_45)); }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * get_lumVsSatCurve_45() const { return ___lumVsSatCurve_45; }
	inline SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 ** get_address_of_lumVsSatCurve_45() { return &___lumVsSatCurve_45; }
	inline void set_lumVsSatCurve_45(SplineParameter_t0D979CD5428457F64DEB05BA4F9AFB65A86DF2B0 * value)
	{
		___lumVsSatCurve_45 = value;
		Il2CppCodeGenWriteBarrier((&___lumVsSatCurve_45), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADING_T12FC06E67BEB22D3368F661CEC790B9B6A81E035_H
#ifndef DEPTHOFFIELD_T82BB37F78622CC143942BA19603D89B9BCCE37AC_H
#define DEPTHOFFIELD_T82BB37F78622CC143942BA19603D89B9BCCE37AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.DepthOfField
struct  DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.DepthOfField::focusDistance
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___focusDistance_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.DepthOfField::aperture
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___aperture_8;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.DepthOfField::focalLength
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___focalLength_9;
	// UnityEngine.Rendering.PostProcessing.KernelSizeParameter UnityEngine.Rendering.PostProcessing.DepthOfField::kernelSize
	KernelSizeParameter_t75F71C05A913F8456F68E98BD412A8B9990E2140 * ___kernelSize_10;

public:
	inline static int32_t get_offset_of_focusDistance_7() { return static_cast<int32_t>(offsetof(DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC, ___focusDistance_7)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_focusDistance_7() const { return ___focusDistance_7; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_focusDistance_7() { return &___focusDistance_7; }
	inline void set_focusDistance_7(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___focusDistance_7 = value;
		Il2CppCodeGenWriteBarrier((&___focusDistance_7), value);
	}

	inline static int32_t get_offset_of_aperture_8() { return static_cast<int32_t>(offsetof(DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC, ___aperture_8)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_aperture_8() const { return ___aperture_8; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_aperture_8() { return &___aperture_8; }
	inline void set_aperture_8(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___aperture_8 = value;
		Il2CppCodeGenWriteBarrier((&___aperture_8), value);
	}

	inline static int32_t get_offset_of_focalLength_9() { return static_cast<int32_t>(offsetof(DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC, ___focalLength_9)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_focalLength_9() const { return ___focalLength_9; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_focalLength_9() { return &___focalLength_9; }
	inline void set_focalLength_9(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___focalLength_9 = value;
		Il2CppCodeGenWriteBarrier((&___focalLength_9), value);
	}

	inline static int32_t get_offset_of_kernelSize_10() { return static_cast<int32_t>(offsetof(DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC, ___kernelSize_10)); }
	inline KernelSizeParameter_t75F71C05A913F8456F68E98BD412A8B9990E2140 * get_kernelSize_10() const { return ___kernelSize_10; }
	inline KernelSizeParameter_t75F71C05A913F8456F68E98BD412A8B9990E2140 ** get_address_of_kernelSize_10() { return &___kernelSize_10; }
	inline void set_kernelSize_10(KernelSizeParameter_t75F71C05A913F8456F68E98BD412A8B9990E2140 * value)
	{
		___kernelSize_10 = value;
		Il2CppCodeGenWriteBarrier((&___kernelSize_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELD_T82BB37F78622CC143942BA19603D89B9BCCE37AC_H
#ifndef GRAIN_T18BE9242394A40BCB11DB23E3FF58A5C37876FD6_H
#define GRAIN_T18BE9242394A40BCB11DB23E3FF58A5C37876FD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.PostProcessing.Grain
struct  Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6  : public PostProcessEffectSettings_tF399FB819B12B3398DDDEF1AAA1FB0685C524042
{
public:
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.Grain::colored
	BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * ___colored_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Grain::intensity
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___intensity_8;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Grain::size
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___size_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Grain::lumContrib
	FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * ___lumContrib_10;

public:
	inline static int32_t get_offset_of_colored_7() { return static_cast<int32_t>(offsetof(Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6, ___colored_7)); }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * get_colored_7() const { return ___colored_7; }
	inline BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE ** get_address_of_colored_7() { return &___colored_7; }
	inline void set_colored_7(BoolParameter_tEBE5D980C8D9609041E3D8D90F2CF2D92F62B0AE * value)
	{
		___colored_7 = value;
		Il2CppCodeGenWriteBarrier((&___colored_7), value);
	}

	inline static int32_t get_offset_of_intensity_8() { return static_cast<int32_t>(offsetof(Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6, ___intensity_8)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_intensity_8() const { return ___intensity_8; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_intensity_8() { return &___intensity_8; }
	inline void set_intensity_8(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___intensity_8 = value;
		Il2CppCodeGenWriteBarrier((&___intensity_8), value);
	}

	inline static int32_t get_offset_of_size_9() { return static_cast<int32_t>(offsetof(Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6, ___size_9)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_size_9() const { return ___size_9; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_size_9() { return &___size_9; }
	inline void set_size_9(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___size_9 = value;
		Il2CppCodeGenWriteBarrier((&___size_9), value);
	}

	inline static int32_t get_offset_of_lumContrib_10() { return static_cast<int32_t>(offsetof(Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6, ___lumContrib_10)); }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * get_lumContrib_10() const { return ___lumContrib_10; }
	inline FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F ** get_address_of_lumContrib_10() { return &___lumContrib_10; }
	inline void set_lumContrib_10(FloatParameter_t55E57BA8E6C776667673B2C973ADB64B15E2AB7F * value)
	{
		___lumContrib_10 = value;
		Il2CppCodeGenWriteBarrier((&___lumContrib_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAIN_T18BE9242394A40BCB11DB23E3FF58A5C37876FD6_H
#ifndef VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#define VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifndef TRACKABLEBEHAVIOUR_T579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_H
#define TRACKABLEBEHAVIOUR_T579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_4;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_5;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_6;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_7;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mPreviousScale_8;
	// Vuforia.TrackableBehaviour_Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_9;
	// Vuforia.TrackableBehaviour_StatusInfo Vuforia.TrackableBehaviour::mStatusInfo
	int32_t ___mStatusInfo_10;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_11;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B * ___mTrackableEventHandlers_12;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___U3CTimeStampU3Ek__BackingField_4)); }
	inline double get_U3CTimeStampU3Ek__BackingField_4() const { return ___U3CTimeStampU3Ek__BackingField_4; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_4() { return &___U3CTimeStampU3Ek__BackingField_4; }
	inline void set_U3CTimeStampU3Ek__BackingField_4(double value)
	{
		___U3CTimeStampU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackableName_5)); }
	inline String_t* get_mTrackableName_5() const { return ___mTrackableName_5; }
	inline String_t** get_address_of_mTrackableName_5() { return &___mTrackableName_5; }
	inline void set_mTrackableName_5(String_t* value)
	{
		___mTrackableName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_5), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mPreserveChildSize_6)); }
	inline bool get_mPreserveChildSize_6() const { return ___mPreserveChildSize_6; }
	inline bool* get_address_of_mPreserveChildSize_6() { return &___mPreserveChildSize_6; }
	inline void set_mPreserveChildSize_6(bool value)
	{
		___mPreserveChildSize_6 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mInitializedInEditor_7)); }
	inline bool get_mInitializedInEditor_7() const { return ___mInitializedInEditor_7; }
	inline bool* get_address_of_mInitializedInEditor_7() { return &___mInitializedInEditor_7; }
	inline void set_mInitializedInEditor_7(bool value)
	{
		___mInitializedInEditor_7 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mPreviousScale_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mPreviousScale_8() const { return ___mPreviousScale_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mPreviousScale_8() { return &___mPreviousScale_8; }
	inline void set_mPreviousScale_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mPreviousScale_8 = value;
	}

	inline static int32_t get_offset_of_mStatus_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mStatus_9)); }
	inline int32_t get_mStatus_9() const { return ___mStatus_9; }
	inline int32_t* get_address_of_mStatus_9() { return &___mStatus_9; }
	inline void set_mStatus_9(int32_t value)
	{
		___mStatus_9 = value;
	}

	inline static int32_t get_offset_of_mStatusInfo_10() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mStatusInfo_10)); }
	inline int32_t get_mStatusInfo_10() const { return ___mStatusInfo_10; }
	inline int32_t* get_address_of_mStatusInfo_10() { return &___mStatusInfo_10; }
	inline void set_mStatusInfo_10(int32_t value)
	{
		___mStatusInfo_10 = value;
	}

	inline static int32_t get_offset_of_mTrackable_11() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackable_11)); }
	inline RuntimeObject* get_mTrackable_11() const { return ___mTrackable_11; }
	inline RuntimeObject** get_address_of_mTrackable_11() { return &___mTrackable_11; }
	inline void set_mTrackable_11(RuntimeObject* value)
	{
		___mTrackable_11 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_11), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_12() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackableEventHandlers_12)); }
	inline List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B * get_mTrackableEventHandlers_12() const { return ___mTrackableEventHandlers_12; }
	inline List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B ** get_address_of_mTrackableEventHandlers_12() { return &___mTrackableEventHandlers_12; }
	inline void set_mTrackableEventHandlers_12(List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B * value)
	{
		___mTrackableEventHandlers_12 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_H
#ifndef USERDEFINEDTARGETBUILDINGBEHAVIOUR_T0712EA02EA85D448B84DBE79577CD2F2874F2CA7_H
#define USERDEFINEDTARGETBUILDINGBEHAVIOUR_T0712EA02EA85D448B84DBE79577CD2F2874F2CA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UserDefinedTargetBuildingBehaviour
struct  UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// Vuforia.ObjectTracker Vuforia.UserDefinedTargetBuildingBehaviour::mObjectTracker
	ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E * ___mObjectTracker_4;
	// Vuforia.ImageTargetBuilder_FrameQuality Vuforia.UserDefinedTargetBuildingBehaviour::mLastFrameQuality
	int32_t ___mLastFrameQuality_5;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mCurrentlyScanning
	bool ___mCurrentlyScanning_6;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mWasScanningBeforeDisable
	bool ___mWasScanningBeforeDisable_7;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mCurrentlyBuilding
	bool ___mCurrentlyBuilding_8;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mWasBuildingBeforeDisable
	bool ___mWasBuildingBeforeDisable_9;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_10;
	// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler> Vuforia.UserDefinedTargetBuildingBehaviour::mHandlers
	List_1_t0748E6C49D36B6B078F5DBEDEAEDDE881580AD0B * ___mHandlers_11;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StopTrackerWhileScanning
	bool ___StopTrackerWhileScanning_12;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StartScanningAutomatically
	bool ___StartScanningAutomatically_13;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StopScanningWhenFinshedBuilding
	bool ___StopScanningWhenFinshedBuilding_14;

public:
	inline static int32_t get_offset_of_mObjectTracker_4() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mObjectTracker_4)); }
	inline ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E * get_mObjectTracker_4() const { return ___mObjectTracker_4; }
	inline ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E ** get_address_of_mObjectTracker_4() { return &___mObjectTracker_4; }
	inline void set_mObjectTracker_4(ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E * value)
	{
		___mObjectTracker_4 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_4), value);
	}

	inline static int32_t get_offset_of_mLastFrameQuality_5() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mLastFrameQuality_5)); }
	inline int32_t get_mLastFrameQuality_5() const { return ___mLastFrameQuality_5; }
	inline int32_t* get_address_of_mLastFrameQuality_5() { return &___mLastFrameQuality_5; }
	inline void set_mLastFrameQuality_5(int32_t value)
	{
		___mLastFrameQuality_5 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyScanning_6() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mCurrentlyScanning_6)); }
	inline bool get_mCurrentlyScanning_6() const { return ___mCurrentlyScanning_6; }
	inline bool* get_address_of_mCurrentlyScanning_6() { return &___mCurrentlyScanning_6; }
	inline void set_mCurrentlyScanning_6(bool value)
	{
		___mCurrentlyScanning_6 = value;
	}

	inline static int32_t get_offset_of_mWasScanningBeforeDisable_7() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mWasScanningBeforeDisable_7)); }
	inline bool get_mWasScanningBeforeDisable_7() const { return ___mWasScanningBeforeDisable_7; }
	inline bool* get_address_of_mWasScanningBeforeDisable_7() { return &___mWasScanningBeforeDisable_7; }
	inline void set_mWasScanningBeforeDisable_7(bool value)
	{
		___mWasScanningBeforeDisable_7 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyBuilding_8() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mCurrentlyBuilding_8)); }
	inline bool get_mCurrentlyBuilding_8() const { return ___mCurrentlyBuilding_8; }
	inline bool* get_address_of_mCurrentlyBuilding_8() { return &___mCurrentlyBuilding_8; }
	inline void set_mCurrentlyBuilding_8(bool value)
	{
		___mCurrentlyBuilding_8 = value;
	}

	inline static int32_t get_offset_of_mWasBuildingBeforeDisable_9() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mWasBuildingBeforeDisable_9)); }
	inline bool get_mWasBuildingBeforeDisable_9() const { return ___mWasBuildingBeforeDisable_9; }
	inline bool* get_address_of_mWasBuildingBeforeDisable_9() { return &___mWasBuildingBeforeDisable_9; }
	inline void set_mWasBuildingBeforeDisable_9(bool value)
	{
		___mWasBuildingBeforeDisable_9 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_10() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mOnInitializedCalled_10)); }
	inline bool get_mOnInitializedCalled_10() const { return ___mOnInitializedCalled_10; }
	inline bool* get_address_of_mOnInitializedCalled_10() { return &___mOnInitializedCalled_10; }
	inline void set_mOnInitializedCalled_10(bool value)
	{
		___mOnInitializedCalled_10 = value;
	}

	inline static int32_t get_offset_of_mHandlers_11() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mHandlers_11)); }
	inline List_1_t0748E6C49D36B6B078F5DBEDEAEDDE881580AD0B * get_mHandlers_11() const { return ___mHandlers_11; }
	inline List_1_t0748E6C49D36B6B078F5DBEDEAEDDE881580AD0B ** get_address_of_mHandlers_11() { return &___mHandlers_11; }
	inline void set_mHandlers_11(List_1_t0748E6C49D36B6B078F5DBEDEAEDDE881580AD0B * value)
	{
		___mHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_11), value);
	}

	inline static int32_t get_offset_of_StopTrackerWhileScanning_12() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___StopTrackerWhileScanning_12)); }
	inline bool get_StopTrackerWhileScanning_12() const { return ___StopTrackerWhileScanning_12; }
	inline bool* get_address_of_StopTrackerWhileScanning_12() { return &___StopTrackerWhileScanning_12; }
	inline void set_StopTrackerWhileScanning_12(bool value)
	{
		___StopTrackerWhileScanning_12 = value;
	}

	inline static int32_t get_offset_of_StartScanningAutomatically_13() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___StartScanningAutomatically_13)); }
	inline bool get_StartScanningAutomatically_13() const { return ___StartScanningAutomatically_13; }
	inline bool* get_address_of_StartScanningAutomatically_13() { return &___StartScanningAutomatically_13; }
	inline void set_StartScanningAutomatically_13(bool value)
	{
		___StartScanningAutomatically_13 = value;
	}

	inline static int32_t get_offset_of_StopScanningWhenFinshedBuilding_14() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___StopScanningWhenFinshedBuilding_14)); }
	inline bool get_StopScanningWhenFinshedBuilding_14() const { return ___StopScanningWhenFinshedBuilding_14; }
	inline bool* get_address_of_StopScanningWhenFinshedBuilding_14() { return &___StopScanningWhenFinshedBuilding_14; }
	inline void set_StopScanningWhenFinshedBuilding_14(bool value)
	{
		___StopScanningWhenFinshedBuilding_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDEFINEDTARGETBUILDINGBEHAVIOUR_T0712EA02EA85D448B84DBE79577CD2F2874F2CA7_H
#ifndef VIDEOBACKGROUNDBEHAVIOUR_T5FE09B8C02AFCA301B6D915668BE3C050CE37C00_H
#define VIDEOBACKGROUNDBEHAVIOUR_T5FE09B8C02AFCA301B6D915668BE3C050CE37C00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundBehaviour
struct  VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mSkipStateUpdates
	int32_t ___mSkipStateUpdates_4;
	// UnityEngine.Camera Vuforia.VideoBackgroundBehaviour::mCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mCamera_5;
	// Vuforia.BackgroundPlaneBehaviour Vuforia.VideoBackgroundBehaviour::mBackgroundPlaneBehaviour
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 * ___mBackgroundPlaneBehaviour_6;
	// Vuforia.IVideoTextureUpdater Vuforia.VideoBackgroundBehaviour::mVideoTextureUpdater
	RuntimeObject* ___mVideoTextureUpdater_7;

public:
	inline static int32_t get_offset_of_mSkipStateUpdates_4() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00, ___mSkipStateUpdates_4)); }
	inline int32_t get_mSkipStateUpdates_4() const { return ___mSkipStateUpdates_4; }
	inline int32_t* get_address_of_mSkipStateUpdates_4() { return &___mSkipStateUpdates_4; }
	inline void set_mSkipStateUpdates_4(int32_t value)
	{
		___mSkipStateUpdates_4 = value;
	}

	inline static int32_t get_offset_of_mCamera_5() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00, ___mCamera_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mCamera_5() const { return ___mCamera_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mCamera_5() { return &___mCamera_5; }
	inline void set_mCamera_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_5), value);
	}

	inline static int32_t get_offset_of_mBackgroundPlaneBehaviour_6() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00, ___mBackgroundPlaneBehaviour_6)); }
	inline BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 * get_mBackgroundPlaneBehaviour_6() const { return ___mBackgroundPlaneBehaviour_6; }
	inline BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 ** get_address_of_mBackgroundPlaneBehaviour_6() { return &___mBackgroundPlaneBehaviour_6; }
	inline void set_mBackgroundPlaneBehaviour_6(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 * value)
	{
		___mBackgroundPlaneBehaviour_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundPlaneBehaviour_6), value);
	}

	inline static int32_t get_offset_of_mVideoTextureUpdater_7() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00, ___mVideoTextureUpdater_7)); }
	inline RuntimeObject* get_mVideoTextureUpdater_7() const { return ___mVideoTextureUpdater_7; }
	inline RuntimeObject** get_address_of_mVideoTextureUpdater_7() { return &___mVideoTextureUpdater_7; }
	inline void set_mVideoTextureUpdater_7(RuntimeObject* value)
	{
		___mVideoTextureUpdater_7 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoTextureUpdater_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDBEHAVIOUR_T5FE09B8C02AFCA301B6D915668BE3C050CE37C00_H
#ifndef VIRTUALBUTTONBEHAVIOUR_TD6CB39E767B87A0527253A412F4DB6F813BD6FEE_H
#define VIRTUALBUTTONBEHAVIOUR_TD6CB39E767B87A0527253A412F4DB6F813BD6FEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButtonBehaviour
struct  VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.String Vuforia.VirtualButtonBehaviour::mName
	String_t* ___mName_5;
	// Vuforia.VirtualButton_Sensitivity Vuforia.VirtualButtonBehaviour::mSensitivity
	int32_t ___mSensitivity_6;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mHasUpdatedPose
	bool ___mHasUpdatedPose_7;
	// UnityEngine.Matrix4x4 Vuforia.VirtualButtonBehaviour::mPrevTransform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___mPrevTransform_8;
	// UnityEngine.GameObject Vuforia.VirtualButtonBehaviour::mPrevParent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mPrevParent_9;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mSensitivityDirty
	bool ___mSensitivityDirty_10;
	// Vuforia.VirtualButton_Sensitivity Vuforia.VirtualButtonBehaviour::mPreviousSensitivity
	int32_t ___mPreviousSensitivity_11;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mPreviouslyEnabled
	bool ___mPreviouslyEnabled_12;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mPressed
	bool ___mPressed_13;
	// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler> Vuforia.VirtualButtonBehaviour::mHandlers
	List_1_tE1500C473509EDCE2ED838547DF41114B3785932 * ___mHandlers_14;
	// UnityEngine.Vector2 Vuforia.VirtualButtonBehaviour::mLeftTop
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mLeftTop_15;
	// UnityEngine.Vector2 Vuforia.VirtualButtonBehaviour::mRightBottom
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mRightBottom_16;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mUnregisterOnDestroy
	bool ___mUnregisterOnDestroy_17;
	// Vuforia.VirtualButton Vuforia.VirtualButtonBehaviour::mVirtualButton
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654 * ___mVirtualButton_18;

public:
	inline static int32_t get_offset_of_mName_5() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mName_5)); }
	inline String_t* get_mName_5() const { return ___mName_5; }
	inline String_t** get_address_of_mName_5() { return &___mName_5; }
	inline void set_mName_5(String_t* value)
	{
		___mName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mName_5), value);
	}

	inline static int32_t get_offset_of_mSensitivity_6() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mSensitivity_6)); }
	inline int32_t get_mSensitivity_6() const { return ___mSensitivity_6; }
	inline int32_t* get_address_of_mSensitivity_6() { return &___mSensitivity_6; }
	inline void set_mSensitivity_6(int32_t value)
	{
		___mSensitivity_6 = value;
	}

	inline static int32_t get_offset_of_mHasUpdatedPose_7() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mHasUpdatedPose_7)); }
	inline bool get_mHasUpdatedPose_7() const { return ___mHasUpdatedPose_7; }
	inline bool* get_address_of_mHasUpdatedPose_7() { return &___mHasUpdatedPose_7; }
	inline void set_mHasUpdatedPose_7(bool value)
	{
		___mHasUpdatedPose_7 = value;
	}

	inline static int32_t get_offset_of_mPrevTransform_8() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mPrevTransform_8)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_mPrevTransform_8() const { return ___mPrevTransform_8; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_mPrevTransform_8() { return &___mPrevTransform_8; }
	inline void set_mPrevTransform_8(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___mPrevTransform_8 = value;
	}

	inline static int32_t get_offset_of_mPrevParent_9() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mPrevParent_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mPrevParent_9() const { return ___mPrevParent_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mPrevParent_9() { return &___mPrevParent_9; }
	inline void set_mPrevParent_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mPrevParent_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPrevParent_9), value);
	}

	inline static int32_t get_offset_of_mSensitivityDirty_10() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mSensitivityDirty_10)); }
	inline bool get_mSensitivityDirty_10() const { return ___mSensitivityDirty_10; }
	inline bool* get_address_of_mSensitivityDirty_10() { return &___mSensitivityDirty_10; }
	inline void set_mSensitivityDirty_10(bool value)
	{
		___mSensitivityDirty_10 = value;
	}

	inline static int32_t get_offset_of_mPreviousSensitivity_11() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mPreviousSensitivity_11)); }
	inline int32_t get_mPreviousSensitivity_11() const { return ___mPreviousSensitivity_11; }
	inline int32_t* get_address_of_mPreviousSensitivity_11() { return &___mPreviousSensitivity_11; }
	inline void set_mPreviousSensitivity_11(int32_t value)
	{
		___mPreviousSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_mPreviouslyEnabled_12() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mPreviouslyEnabled_12)); }
	inline bool get_mPreviouslyEnabled_12() const { return ___mPreviouslyEnabled_12; }
	inline bool* get_address_of_mPreviouslyEnabled_12() { return &___mPreviouslyEnabled_12; }
	inline void set_mPreviouslyEnabled_12(bool value)
	{
		___mPreviouslyEnabled_12 = value;
	}

	inline static int32_t get_offset_of_mPressed_13() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mPressed_13)); }
	inline bool get_mPressed_13() const { return ___mPressed_13; }
	inline bool* get_address_of_mPressed_13() { return &___mPressed_13; }
	inline void set_mPressed_13(bool value)
	{
		___mPressed_13 = value;
	}

	inline static int32_t get_offset_of_mHandlers_14() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mHandlers_14)); }
	inline List_1_tE1500C473509EDCE2ED838547DF41114B3785932 * get_mHandlers_14() const { return ___mHandlers_14; }
	inline List_1_tE1500C473509EDCE2ED838547DF41114B3785932 ** get_address_of_mHandlers_14() { return &___mHandlers_14; }
	inline void set_mHandlers_14(List_1_tE1500C473509EDCE2ED838547DF41114B3785932 * value)
	{
		___mHandlers_14 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_14), value);
	}

	inline static int32_t get_offset_of_mLeftTop_15() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mLeftTop_15)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mLeftTop_15() const { return ___mLeftTop_15; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mLeftTop_15() { return &___mLeftTop_15; }
	inline void set_mLeftTop_15(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mLeftTop_15 = value;
	}

	inline static int32_t get_offset_of_mRightBottom_16() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mRightBottom_16)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mRightBottom_16() const { return ___mRightBottom_16; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mRightBottom_16() { return &___mRightBottom_16; }
	inline void set_mRightBottom_16(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mRightBottom_16 = value;
	}

	inline static int32_t get_offset_of_mUnregisterOnDestroy_17() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mUnregisterOnDestroy_17)); }
	inline bool get_mUnregisterOnDestroy_17() const { return ___mUnregisterOnDestroy_17; }
	inline bool* get_address_of_mUnregisterOnDestroy_17() { return &___mUnregisterOnDestroy_17; }
	inline void set_mUnregisterOnDestroy_17(bool value)
	{
		___mUnregisterOnDestroy_17 = value;
	}

	inline static int32_t get_offset_of_mVirtualButton_18() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mVirtualButton_18)); }
	inline VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654 * get_mVirtualButton_18() const { return ___mVirtualButton_18; }
	inline VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654 ** get_address_of_mVirtualButton_18() { return &___mVirtualButton_18; }
	inline void set_mVirtualButton_18(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654 * value)
	{
		___mVirtualButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButton_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONBEHAVIOUR_TD6CB39E767B87A0527253A412F4DB6F813BD6FEE_H
#ifndef WIREFRAMEBEHAVIOUR_T4D7E355A49179D0FA85D66D925AD5832274D939F_H
#define WIREFRAMEBEHAVIOUR_T4D7E355A49179D0FA85D66D925AD5832274D939F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeBehaviour
struct  WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// UnityEngine.Material Vuforia.WireframeBehaviour::lineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lineMaterial_4;
	// System.Boolean Vuforia.WireframeBehaviour::ShowLines
	bool ___ShowLines_5;
	// UnityEngine.Color Vuforia.WireframeBehaviour::LineColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___LineColor_6;
	// UnityEngine.Material Vuforia.WireframeBehaviour::mLineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mLineMaterial_7;

public:
	inline static int32_t get_offset_of_lineMaterial_4() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F, ___lineMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lineMaterial_4() const { return ___lineMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lineMaterial_4() { return &___lineMaterial_4; }
	inline void set_lineMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineMaterial_4), value);
	}

	inline static int32_t get_offset_of_ShowLines_5() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F, ___ShowLines_5)); }
	inline bool get_ShowLines_5() const { return ___ShowLines_5; }
	inline bool* get_address_of_ShowLines_5() { return &___ShowLines_5; }
	inline void set_ShowLines_5(bool value)
	{
		___ShowLines_5 = value;
	}

	inline static int32_t get_offset_of_LineColor_6() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F, ___LineColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_LineColor_6() const { return ___LineColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_LineColor_6() { return &___LineColor_6; }
	inline void set_LineColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___LineColor_6 = value;
	}

	inline static int32_t get_offset_of_mLineMaterial_7() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F, ___mLineMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_mLineMaterial_7() const { return ___mLineMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_mLineMaterial_7() { return &___mLineMaterial_7; }
	inline void set_mLineMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___mLineMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___mLineMaterial_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMEBEHAVIOUR_T4D7E355A49179D0FA85D66D925AD5832274D939F_H
#ifndef WIREFRAMETRACKABLEEVENTHANDLER_TB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13_H
#define WIREFRAMETRACKABLEEVENTHANDLER_TB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeTrackableEventHandler
struct  WireframeTrackableEventHandler_tB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// Vuforia.TrackableBehaviour Vuforia.WireframeTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_4;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(WireframeTrackableEventHandler_tB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMETRACKABLEEVENTHANDLER_TB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T84B7BB3C959046F38CC73E423800BD2F8859E706_H
#define DATASETTRACKABLEBEHAVIOUR_T84B7BB3C959046F38CC73E423800BD2F8859E706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706  : public TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_13;

public:
	inline static int32_t get_offset_of_mDataSetPath_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706, ___mDataSetPath_13)); }
	inline String_t* get_mDataSetPath_13() const { return ___mDataSetPath_13; }
	inline String_t** get_address_of_mDataSetPath_13() { return &___mDataSetPath_13; }
	inline void set_mDataSetPath_13(String_t* value)
	{
		___mDataSetPath_13 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T84B7BB3C959046F38CC73E423800BD2F8859E706_H
#ifndef IMAGETARGETBEHAVIOUR_T2014110FECB3CAB6142743A36CA3F50A91E97540_H
#define IMAGETARGETBEHAVIOUR_T2014110FECB3CAB6142743A36CA3F50A91E97540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBehaviour
struct  ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540  : public DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706
{
public:
	// System.Single Vuforia.ImageTargetBehaviour::mAspectRatio
	float ___mAspectRatio_14;
	// Vuforia.ImageTargetType Vuforia.ImageTargetBehaviour::mImageTargetType
	int32_t ___mImageTargetType_15;
	// System.Single Vuforia.ImageTargetBehaviour::mWidth
	float ___mWidth_16;
	// System.Single Vuforia.ImageTargetBehaviour::mHeight
	float ___mHeight_17;
	// Vuforia.ImageTarget Vuforia.ImageTargetBehaviour::mImageTarget
	RuntimeObject* ___mImageTarget_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonBehaviour> Vuforia.ImageTargetBehaviour::mVirtualButtonBehaviours
	Dictionary_2_t49D49E0D72539C160B6F736C64DBC729238B3481 * ___mVirtualButtonBehaviours_19;
	// System.Single Vuforia.ImageTargetBehaviour::mLastTransformScale
	float ___mLastTransformScale_20;
	// UnityEngine.Vector2 Vuforia.ImageTargetBehaviour::mLastSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mLastSize_21;

public:
	inline static int32_t get_offset_of_mAspectRatio_14() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mAspectRatio_14)); }
	inline float get_mAspectRatio_14() const { return ___mAspectRatio_14; }
	inline float* get_address_of_mAspectRatio_14() { return &___mAspectRatio_14; }
	inline void set_mAspectRatio_14(float value)
	{
		___mAspectRatio_14 = value;
	}

	inline static int32_t get_offset_of_mImageTargetType_15() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mImageTargetType_15)); }
	inline int32_t get_mImageTargetType_15() const { return ___mImageTargetType_15; }
	inline int32_t* get_address_of_mImageTargetType_15() { return &___mImageTargetType_15; }
	inline void set_mImageTargetType_15(int32_t value)
	{
		___mImageTargetType_15 = value;
	}

	inline static int32_t get_offset_of_mWidth_16() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mWidth_16)); }
	inline float get_mWidth_16() const { return ___mWidth_16; }
	inline float* get_address_of_mWidth_16() { return &___mWidth_16; }
	inline void set_mWidth_16(float value)
	{
		___mWidth_16 = value;
	}

	inline static int32_t get_offset_of_mHeight_17() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mHeight_17)); }
	inline float get_mHeight_17() const { return ___mHeight_17; }
	inline float* get_address_of_mHeight_17() { return &___mHeight_17; }
	inline void set_mHeight_17(float value)
	{
		___mHeight_17 = value;
	}

	inline static int32_t get_offset_of_mImageTarget_18() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mImageTarget_18)); }
	inline RuntimeObject* get_mImageTarget_18() const { return ___mImageTarget_18; }
	inline RuntimeObject** get_address_of_mImageTarget_18() { return &___mImageTarget_18; }
	inline void set_mImageTarget_18(RuntimeObject* value)
	{
		___mImageTarget_18 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTarget_18), value);
	}

	inline static int32_t get_offset_of_mVirtualButtonBehaviours_19() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mVirtualButtonBehaviours_19)); }
	inline Dictionary_2_t49D49E0D72539C160B6F736C64DBC729238B3481 * get_mVirtualButtonBehaviours_19() const { return ___mVirtualButtonBehaviours_19; }
	inline Dictionary_2_t49D49E0D72539C160B6F736C64DBC729238B3481 ** get_address_of_mVirtualButtonBehaviours_19() { return &___mVirtualButtonBehaviours_19; }
	inline void set_mVirtualButtonBehaviours_19(Dictionary_2_t49D49E0D72539C160B6F736C64DBC729238B3481 * value)
	{
		___mVirtualButtonBehaviours_19 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButtonBehaviours_19), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_20() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mLastTransformScale_20)); }
	inline float get_mLastTransformScale_20() const { return ___mLastTransformScale_20; }
	inline float* get_address_of_mLastTransformScale_20() { return &___mLastTransformScale_20; }
	inline void set_mLastTransformScale_20(float value)
	{
		___mLastTransformScale_20 = value;
	}

	inline static int32_t get_offset_of_mLastSize_21() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mLastSize_21)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mLastSize_21() const { return ___mLastSize_21; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mLastSize_21() { return &___mLastSize_21; }
	inline void set_mLastSize_21(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mLastSize_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETBEHAVIOUR_T2014110FECB3CAB6142743A36CA3F50A91E97540_H
#ifndef MULTITARGETBEHAVIOUR_TF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9_H
#define MULTITARGETBEHAVIOUR_TF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetBehaviour
struct  MultiTargetBehaviour_tF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9  : public DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706
{
public:
	// Vuforia.MultiTarget Vuforia.MultiTargetBehaviour::mMultiTarget
	RuntimeObject* ___mMultiTarget_14;

public:
	inline static int32_t get_offset_of_mMultiTarget_14() { return static_cast<int32_t>(offsetof(MultiTargetBehaviour_tF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9, ___mMultiTarget_14)); }
	inline RuntimeObject* get_mMultiTarget_14() const { return ___mMultiTarget_14; }
	inline RuntimeObject** get_address_of_mMultiTarget_14() { return &___mMultiTarget_14; }
	inline void set_mMultiTarget_14(RuntimeObject* value)
	{
		___mMultiTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___mMultiTarget_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETBEHAVIOUR_TF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { sizeof (ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3501[8] = 
{
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mAspectRatio_14(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mImageTargetType_15(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mWidth_16(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mHeight_17(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mImageTarget_18(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mVirtualButtonBehaviours_19(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mLastTransformScale_20(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mLastSize_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { sizeof (ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3502[4] = 
{
	ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E::get_offset_of_mActiveDataSets_1(),
	ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E::get_offset_of_mDataSets_2(),
	ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E::get_offset_of_mTargetFinders_3(),
	ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E::get_offset_of_mImageTargetBuilder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (MultiTargetBehaviour_tF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3504[1] = 
{
	MultiTargetBehaviour_tF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9::get_offset_of_mMultiTarget_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { sizeof (VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F), -1, sizeof(VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3505[3] = 
{
	VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields::get_offset_of_mHoloLensApiAbstraction_0(),
	VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields::get_offset_of_mRendererDirty_1(),
	VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields::get_offset_of_mWrapperType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { sizeof (InitError_t486F7D53F5B0B7943D4E22BE2D32F4913D4A0431)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3506[13] = 
{
	InitError_t486F7D53F5B0B7943D4E22BE2D32F4913D4A0431::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { sizeof (VuforiaHint_t5E4E8E78687FAEB9311A944CD4051169EF4A5A6D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3507[6] = 
{
	VuforiaHint_t5E4E8E78687FAEB9311A944CD4051169EF4A5A6D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3508 = { sizeof (StorageType_t6847D699566F42A8C42777B30537BA0C0521A4D3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3508[4] = 
{
	StorageType_t6847D699566F42A8C42777B30537BA0C0521A4D3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3509 = { sizeof (VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876), -1, sizeof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3509[31] = 
{
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mCameraDeviceModeSetting_1(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mMaxSimultaneousImageTargets_2(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mMaxSimultaneousObjectTargets_3(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mUseDelayedLoadingObjectTargets_4(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mModelTargetRecoWhileExtendedTracked_5(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mWorldCenterMode_6(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mWorldCenter_7(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mVideoBgEventHandlers_8(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnBeforeVuforiaTrackersInitialized_9(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnVuforiaInitialized_10(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnVuforiaStarted_11(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnVuforiaDeinitialized_12(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnTrackablesUpdated_13(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mRenderOnUpdate_14(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnPause_15(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mPaused_16(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnBackgroundTextureChanged_17(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mStartHasBeenInvoked_18(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mHasStarted_19(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mCameraConfiguration_20(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mEyewearBehaviour_21(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mCheckStopCamera_22(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mClearMaterial_23(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mMetalRendering_24(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mHasStartedOnce_25(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mWasEnabledBeforePause_26(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mObjectTrackerWasActiveBeforePause_27(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_28(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mLastUpdatedFrame_29(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields::get_offset_of_mInstance_30(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields::get_offset_of_mPadlock_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3510 = { sizeof (WorldCenterMode_t53E1430BD989A54F75332A2DA9D61C93545897E6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3510[4] = 
{
	WorldCenterMode_t53E1430BD989A54F75332A2DA9D61C93545897E6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3511 = { sizeof (VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7), -1, sizeof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3511[22] = 
{
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7_StaticFields::get_offset_of_sInstance_0(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mWorldCenterMode_1(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mWorldCenter_2(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mVuMarkWorldCenter_3(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mARCameraTransform_4(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mCentralAnchorPoint_5(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mTrackableResultDataArray_6(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mVuMarkDataArray_7(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mVuMarkResultDataArray_8(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mWCTrackableFoundQueue_9(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mCameraImages_10(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mImageData_11(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mInjectedFrameIdx_12(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mLastProcessedFrameStatePtr_13(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mInitialized_14(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mPaused_15(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mFrameState_16(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mLastFrameIdx_17(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mLateLatchingManager_18(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mCameraCalibrationComparer_19(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mPosePositionalOffset_20(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mPoseRotationalOffset_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3512 = { sizeof (TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F)+ sizeof (RuntimeObject), sizeof(TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F ), 0, 0 };
extern const int32_t g_FieldOffsetTable3512[2] = 
{
	TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F::get_offset_of_TrackableId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F::get_offset_of_ResultId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3513 = { sizeof (U3CU3Ec__DisplayClass60_0_t9B16CEDAB1D664130D52803188ADF3EF78ED78A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3513[1] = 
{
	U3CU3Ec__DisplayClass60_0_t9B16CEDAB1D664130D52803188ADF3EF78ED78A3::get_offset_of_frameState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3514 = { sizeof (U3CU3Ec__DisplayClass71_0_tF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3514[1] = 
{
	U3CU3Ec__DisplayClass71_0_tF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3515 = { sizeof (VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081), -1, sizeof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3515[7] = 
{
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081_StaticFields::get_offset_of_sInstance_0(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mVideoBGConfig_1(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mVideoBGConfigSet_2(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mVideoBackgroundTexture_3(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mNativeRenderingCallback_4(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mBackgroundTextureHasChanged_5(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mLegacyRenderingCondition_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3516 = { sizeof (VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A)+ sizeof (RuntimeObject), sizeof(VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A ), 0, 0 };
extern const int32_t g_FieldOffsetTable3516[2] = 
{
	VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A::get_offset_of_size_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3517 = { sizeof (Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5)+ sizeof (RuntimeObject), sizeof(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3517[2] = 
{
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3518 = { sizeof (VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D)+ sizeof (RuntimeObject), sizeof(VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D ), 0, 0 };
extern const int32_t g_FieldOffsetTable3518[2] = 
{
	VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3519 = { sizeof (RendererAPI_tC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3519[5] = 
{
	RendererAPI_tC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3520 = { sizeof (RenderEvent_t855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3520[8] = 
{
	RenderEvent_t855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3521 = { sizeof (U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609), -1, sizeof(U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3521[2] = 
{
	U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3522 = { sizeof (VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7), -1, sizeof(VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3522[2] = 
{
	VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3523 = { sizeof (InitializableBool_t6C711D5999687FC6E8CAF38208EC8E185BF1FC22)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3523[4] = 
{
	InitializableBool_t6C711D5999687FC6E8CAF38208EC8E185BF1FC22::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3524 = { sizeof (GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6), -1, sizeof(GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3524[1] = 
{
	GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6_StaticFields::get_offset_of_GLTF_ASSET_LOCATION_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3525 = { sizeof (CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6)+ sizeof (RuntimeObject), sizeof(CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3525[3] = 
{
	CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6::get_offset_of_U3CInitializedU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6::get_offset_of_U3CActiveU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t4A1994F5F7B9665FD6E136894A4586C0F1E405A6::get_offset_of_U3CDeviceModeU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3526 = { sizeof (U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427), -1, sizeof(U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3526[3] = 
{
	U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_StaticFields::get_offset_of_U3CU3E9__31_0_1(),
	U3CU3Ec_t6EBF0B9804BAEA455B4CBCE8CF2EAAFBCF990427_StaticFields::get_offset_of_U3CU3E9__31_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3527 = { sizeof (SurfaceUtilities_t904664FFE194582F491FD18913B0751AAC957680), -1, sizeof(SurfaceUtilities_t904664FFE194582F491FD18913B0751AAC957680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3527[1] = 
{
	SurfaceUtilities_t904664FFE194582F491FD18913B0751AAC957680_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3528 = { sizeof (TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3528[5] = 
{
	TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC::get_offset_of_mTargetFinderPtr_0(),
	TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC::get_offset_of_mTargets_1(),
	TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC::get_offset_of_mTargetFinderStatePtr_2(),
	TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC::get_offset_of_mTargetFinderState_3(),
	TargetFinder_t2AFA3E4A66C461FA522FE35048DB093003A7B1AC::get_offset_of_mNewResults_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3529 = { sizeof (InitState_tF8DCB9A690DFC7C81A7543FB4E5D6097B27A4925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3529[7] = 
{
	InitState_tF8DCB9A690DFC7C81A7543FB4E5D6097B27A4925::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3530 = { sizeof (UpdateState_t9780BF40F4F57951DF918E1522D9A863D587EF8A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3530[12] = 
{
	UpdateState_t9780BF40F4F57951DF918E1522D9A863D587EF8A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3531 = { sizeof (FilterMode_t3699B6BDA0391CF764A709D079B6AB7B81943779)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3531[3] = 
{
	FilterMode_t3699B6BDA0391CF764A709D079B6AB7B81943779::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3532 = { sizeof (TargetSearchResult_tC958489A5AB66221259FCDFD35F79CA426DAC6F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3532[3] = 
{
	TargetSearchResult_tC958489A5AB66221259FCDFD35F79CA426DAC6F5::get_offset_of_TargetName_0(),
	TargetSearchResult_tC958489A5AB66221259FCDFD35F79CA426DAC6F5::get_offset_of_UniqueTargetId_1(),
	TargetSearchResult_tC958489A5AB66221259FCDFD35F79CA426DAC6F5::get_offset_of_TargetSearchResultPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3533 = { sizeof (CloudRecoSearchResult_t2D45E5EE3E8F270F399A928879500AE5A8EFA02C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3533[3] = 
{
	CloudRecoSearchResult_t2D45E5EE3E8F270F399A928879500AE5A8EFA02C::get_offset_of_MetaData_3(),
	CloudRecoSearchResult_t2D45E5EE3E8F270F399A928879500AE5A8EFA02C::get_offset_of_TargetSize_4(),
	CloudRecoSearchResult_t2D45E5EE3E8F270F399A928879500AE5A8EFA02C::get_offset_of_TrackingRating_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3534 = { sizeof (TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D)+ sizeof (RuntimeObject), sizeof(TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D ), 0, 0 };
extern const int32_t g_FieldOffsetTable3534[2] = 
{
	TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderState_tFED6EAB7D0324D88E902EC9988067C11E8B8C11D::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3535 = { sizeof (SimpleTargetData_t1D59320853F5EB4E7773DE8AF4E146D4B384CF3D)+ sizeof (RuntimeObject), sizeof(SimpleTargetData_t1D59320853F5EB4E7773DE8AF4E146D4B384CF3D ), 0, 0 };
extern const int32_t g_FieldOffsetTable3535[2] = 
{
	SimpleTargetData_t1D59320853F5EB4E7773DE8AF4E146D4B384CF3D::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SimpleTargetData_t1D59320853F5EB4E7773DE8AF4E146D4B384CF3D::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3536 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3537 = { sizeof (TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3537[9] = 
{
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4::get_offset_of_U3CTimeStampU3Ek__BackingField_4(),
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4::get_offset_of_mTrackableName_5(),
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4::get_offset_of_mPreserveChildSize_6(),
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4::get_offset_of_mInitializedInEditor_7(),
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4::get_offset_of_mPreviousScale_8(),
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4::get_offset_of_mStatus_9(),
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4::get_offset_of_mStatusInfo_10(),
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4::get_offset_of_mTrackable_11(),
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4::get_offset_of_mTrackableEventHandlers_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3538 = { sizeof (Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3538[6] = 
{
	Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3539 = { sizeof (StatusInfo_t5507FB8CC09640E7771385EBE27221431A2FEB4E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3539[9] = 
{
	StatusInfo_t5507FB8CC09640E7771385EBE27221431A2FEB4E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3540 = { sizeof (TrackableSource_tD34E68F40275ADD2D4284E6CF1DA1800C8194E39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3540[1] = 
{
	TrackableSource_tD34E68F40275ADD2D4284E6CF1DA1800C8194E39::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3541 = { sizeof (Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3541[1] = 
{
	Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3542 = { sizeof (TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530), -1, sizeof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3542[5] = 
{
	TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530_StaticFields::get_offset_of_mInstance_0(),
	TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530::get_offset_of_mStateManager_1(),
	TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530::get_offset_of_mTrackers_2(),
	TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530::get_offset_of_mTrackerCreators_3(),
	TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530::get_offset_of_mTrackerNativeDeinitializers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3543 = { sizeof (U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370), -1, sizeof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3543[5] = 
{
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields::get_offset_of_U3CU3E9__8_1_2(),
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields::get_offset_of_U3CU3E9__8_2_3(),
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields::get_offset_of_U3CU3E9__8_3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3544 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3545 = { sizeof (UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3545[11] = 
{
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mObjectTracker_4(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mLastFrameQuality_5(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mCurrentlyScanning_6(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mWasScanningBeforeDisable_7(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mCurrentlyBuilding_8(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mWasBuildingBeforeDisable_9(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mOnInitializedCalled_10(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mHandlers_11(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_StopTrackerWhileScanning_12(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_StartScanningAutomatically_13(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_StopScanningWhenFinshedBuilding_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3546 = { sizeof (VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3546[4] = 
{
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00::get_offset_of_mSkipStateUpdates_4(),
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00::get_offset_of_mCamera_5(),
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00::get_offset_of_mBackgroundPlaneBehaviour_6(),
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00::get_offset_of_mVideoTextureUpdater_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3547 = { sizeof (VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50), -1, sizeof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3547[9] = 
{
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mVideoBackgroundNeedsRedrawing_4(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mDiscardStatesForRendering_5(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mIsSeeThroughDevice_6(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields::get_offset_of_mPadlock_8(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3548 = { sizeof (VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3548[7] = 
{
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mName_0(),
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mID_1(),
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mArea_2(),
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mIsEnabled_3(),
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mParentImageTarget_4(),
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mParentDataSet_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3549 = { sizeof (Sensitivity_t7654EFB20C36491C60EAA2D010FAA6A0D9D14B01)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3549[4] = 
{
	Sensitivity_t7654EFB20C36491C60EAA2D010FAA6A0D9D14B01::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3550 = { sizeof (VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3550[15] = 
{
	0,
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mName_5(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mSensitivity_6(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mHasUpdatedPose_7(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mPrevTransform_8(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mPrevParent_9(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mSensitivityDirty_10(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mPreviousSensitivity_11(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mPreviouslyEnabled_12(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mPressed_13(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mHandlers_14(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mLeftTop_15(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mRightBottom_16(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mUnregisterOnDestroy_17(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mVirtualButton_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3551 = { sizeof (WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A), -1, sizeof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3551[6] = 
{
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A::get_offset_of_mWebCamImpl_3(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A::get_offset_of_mWebCamTexAdaptorProvider_4(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields::get_offset_of_mInstance_5(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields::get_offset_of_mPadlock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3552 = { sizeof (U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB), -1, sizeof(U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3552[2] = 
{
	U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3553 = { sizeof (WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3553[4] = 
{
	WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F::get_offset_of_lineMaterial_4(),
	WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F::get_offset_of_ShowLines_5(),
	WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F::get_offset_of_LineColor_6(),
	WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F::get_offset_of_mLineMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3554 = { sizeof (WireframeTrackableEventHandler_tB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3554[1] = 
{
	WireframeTrackableEventHandler_tB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13::get_offset_of_mTrackableBehaviour_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3555 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3556 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3557 = { sizeof (UnityCompiledFacade_tF06053FC3EC0D6850A60CCF314ED1A2A820E7CBD), -1, sizeof(UnityCompiledFacade_tF06053FC3EC0D6850A60CCF314ED1A2A820E7CBD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3557[1] = 
{
	UnityCompiledFacade_tF06053FC3EC0D6850A60CCF314ED1A2A820E7CBD_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3558 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3559 = { sizeof (NullUnityCompiledFacade_t2C8910980052D89707A62705CBE0E40E17CB149A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3559[2] = 
{
	NullUnityCompiledFacade_t2C8910980052D89707A62705CBE0E40E17CB149A::get_offset_of_mUnityRenderPipeline_0(),
	NullUnityCompiledFacade_t2C8910980052D89707A62705CBE0E40E17CB149A::get_offset_of_mUnityAndroidPermissions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3560 = { sizeof (NullUnityRenderPipeline_tA8A5F0FD651D711616AABF06C73715DBB8E569D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3560[2] = 
{
	NullUnityRenderPipeline_tA8A5F0FD651D711616AABF06C73715DBB8E569D5::get_offset_of_BeginFrameRendering_0(),
	NullUnityRenderPipeline_tA8A5F0FD651D711616AABF06C73715DBB8E569D5::get_offset_of_BeginCameraRendering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3561 = { sizeof (NullUnityAndroidPermissions_t34F6B3D453B2B55CFF0BE5DD87DAA32229C7CBBB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3562 = { sizeof (U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47), -1, sizeof(U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3562[1] = 
{
	U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3563 = { sizeof (__StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3564 = { sizeof (U3CModuleU3E_t261AC3CE6934CC046947474E3B28E1E19C5641A6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3565 = { sizeof (PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3565[5] = 
{
	PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748::get_offset_of_renderer_0(),
	PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748::get_offset_of_eventType_1(),
	PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748::get_offset_of_menuItem_2(),
	PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748::get_offset_of_allowInSceneView_3(),
	PostProcessAttribute_t9E50FBFEC7C3457951445E73AD60F252DAB70748::get_offset_of_builtinEffect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3566 = { sizeof (AmbientOcclusionMode_tC03A1F9EA851520397D48B79A7853C2E3E15E0B1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3566[3] = 
{
	AmbientOcclusionMode_tC03A1F9EA851520397D48B79A7853C2E3E15E0B1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3567 = { sizeof (AmbientOcclusionQuality_t9A7185FB3F2CEA669147614CF1B96FAAD38A7443)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3567[6] = 
{
	AmbientOcclusionQuality_t9A7185FB3F2CEA669147614CF1B96FAAD38A7443::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3568 = { sizeof (AmbientOcclusionModeParameter_t4C91527D21B57CFBD2DA5A8AAAB48B89555ED8B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3569 = { sizeof (AmbientOcclusionQualityParameter_t293F9DA77655C53F24A5AFC38014DBCEB25D8ECC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3570 = { sizeof (AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3570[11] = 
{
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_mode_7(),
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_intensity_8(),
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_color_9(),
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_ambientOnly_10(),
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_noiseFilterTolerance_11(),
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_blurTolerance_12(),
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_upsampleTolerance_13(),
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_thicknessModifier_14(),
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_directLightingStrength_15(),
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_radius_16(),
	AmbientOcclusion_t087AD38AD8CB17B86F3810AFA8128118A321CE5B::get_offset_of_quality_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3571 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3572 = { sizeof (AmbientOcclusionRenderer_t0F9B973F0C092035B89AA216AE3F4539113C98E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3572[1] = 
{
	AmbientOcclusionRenderer_t0F9B973F0C092035B89AA216AE3F4539113C98E1::get_offset_of_m_Methods_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3573 = { sizeof (EyeAdaptation_t73FC6AA5F6081EDF0D54602E044501847E82AF9E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3573[3] = 
{
	EyeAdaptation_t73FC6AA5F6081EDF0D54602E044501847E82AF9E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3574 = { sizeof (EyeAdaptationParameter_tAECAA41037553C4EAAA628E6840DD6EEC8C84036), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3575 = { sizeof (AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3575[7] = 
{
	AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A::get_offset_of_filtering_7(),
	AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A::get_offset_of_minLuminance_8(),
	AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A::get_offset_of_maxLuminance_9(),
	AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A::get_offset_of_keyValue_10(),
	AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A::get_offset_of_eyeAdaptation_11(),
	AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A::get_offset_of_speedUp_12(),
	AutoExposure_t5573D856D6AF109940FF24FD7051FCC6412FB39A::get_offset_of_speedDown_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3576 = { sizeof (AutoExposureRenderer_tA09D22C0C405C77FE356AAC93097410A181CF5FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3576[3] = 
{
	AutoExposureRenderer_tA09D22C0C405C77FE356AAC93097410A181CF5FF::get_offset_of_m_AutoExposurePool_2(),
	AutoExposureRenderer_tA09D22C0C405C77FE356AAC93097410A181CF5FF::get_offset_of_m_AutoExposurePingPong_3(),
	AutoExposureRenderer_tA09D22C0C405C77FE356AAC93097410A181CF5FF::get_offset_of_m_CurrentAutoExposure_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3577 = { sizeof (Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3577[10] = 
{
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0::get_offset_of_intensity_7(),
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0::get_offset_of_threshold_8(),
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0::get_offset_of_softKnee_9(),
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0::get_offset_of_clamp_10(),
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0::get_offset_of_diffusion_11(),
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0::get_offset_of_anamorphicRatio_12(),
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0::get_offset_of_color_13(),
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0::get_offset_of_fastMode_14(),
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0::get_offset_of_dirtTexture_15(),
	Bloom_tA23BFB31C8FB9AD7C276157579B4CEA7AA09A5F0::get_offset_of_dirtIntensity_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3578 = { sizeof (BloomRenderer_tBB0EB5B9D5AF88501666D2F5B96C74AE4D6BD491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3578[1] = 
{
	BloomRenderer_tBB0EB5B9D5AF88501666D2F5B96C74AE4D6BD491::get_offset_of_m_Pyramid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3579 = { sizeof (Pass_t11A8ABEA4D0C022520585CD85A89C18846D0F9F8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3579[10] = 
{
	Pass_t11A8ABEA4D0C022520585CD85A89C18846D0F9F8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3580 = { sizeof (Level_t21B0259BF8468CA4457E5880ADBEF78D8CAFEC7C)+ sizeof (RuntimeObject), sizeof(Level_t21B0259BF8468CA4457E5880ADBEF78D8CAFEC7C ), 0, 0 };
extern const int32_t g_FieldOffsetTable3580[2] = 
{
	Level_t21B0259BF8468CA4457E5880ADBEF78D8CAFEC7C::get_offset_of_down_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Level_t21B0259BF8468CA4457E5880ADBEF78D8CAFEC7C::get_offset_of_up_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3581 = { sizeof (ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3581[3] = 
{
	ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D::get_offset_of_spectralLut_7(),
	ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D::get_offset_of_intensity_8(),
	ChromaticAberration_tA39377A5D30FD917158162E01C35B0EDB4BD3B1D::get_offset_of_fastMode_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3582 = { sizeof (ChromaticAberrationRenderer_tC87F0F11DA11CC60BEEDDA6637242EC319919C91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3582[1] = 
{
	ChromaticAberrationRenderer_tC87F0F11DA11CC60BEEDDA6637242EC319919C91::get_offset_of_m_InternalSpectralLut_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3583 = { sizeof (GradingMode_t3DC1002334E76F80BCBDA10A0D5A0D908062D489)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3583[4] = 
{
	GradingMode_t3DC1002334E76F80BCBDA10A0D5A0D908062D489::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3584 = { sizeof (Tonemapper_tB4DF48591BDC83824592387E9CA0332F0E0F4106)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3584[5] = 
{
	Tonemapper_tB4DF48591BDC83824592387E9CA0332F0E0F4106::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3585 = { sizeof (GradingModeParameter_tBC84E2E66719BACCD847B301C1F156E4A340362A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3586 = { sizeof (TonemapperParameter_tD2E82FA78543CD6AAA7C34BCF1BFD5F3C1CE65EF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3587 = { sizeof (ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3587[39] = 
{
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_gradingMode_7(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_externalLut_8(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_tonemapper_9(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_toneCurveToeStrength_10(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_toneCurveToeLength_11(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_toneCurveShoulderStrength_12(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_toneCurveShoulderLength_13(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_toneCurveShoulderAngle_14(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_toneCurveGamma_15(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_ldrLut_16(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_ldrLutContribution_17(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_temperature_18(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_tint_19(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_colorFilter_20(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_hueShift_21(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_saturation_22(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_brightness_23(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_postExposure_24(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_contrast_25(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_mixerRedOutRedIn_26(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_mixerRedOutGreenIn_27(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_mixerRedOutBlueIn_28(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_mixerGreenOutRedIn_29(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_mixerGreenOutGreenIn_30(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_mixerGreenOutBlueIn_31(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_mixerBlueOutRedIn_32(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_mixerBlueOutGreenIn_33(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_mixerBlueOutBlueIn_34(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_lift_35(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_gamma_36(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_gain_37(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_masterCurve_38(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_redCurve_39(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_greenCurve_40(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_blueCurve_41(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_hueVsHueCurve_42(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_hueVsSatCurve_43(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_satVsSatCurve_44(),
	ColorGrading_t12FC06E67BEB22D3368F661CEC790B9B6A81E035::get_offset_of_lumVsSatCurve_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3588 = { sizeof (ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3588[5] = 
{
	ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3::get_offset_of_m_GradingCurves_2(),
	ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3::get_offset_of_m_Pixels_3(),
	ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3::get_offset_of_m_InternalLdrLut_4(),
	ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3::get_offset_of_m_InternalLogLut_5(),
	ColorGradingRenderer_t5DF3057BEEB561EBFF2B032A0DD4C5D8401F15D3::get_offset_of_m_HableCurve_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3589 = { sizeof (Pass_t181A7D496ADD2D55DBFF34DCA58045535626646D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3589[4] = 
{
	Pass_t181A7D496ADD2D55DBFF34DCA58045535626646D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3590 = { sizeof (KernelSize_t9FB5AA957F2E8C257D6E05A4573CB05753B6BB4F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3590[5] = 
{
	KernelSize_t9FB5AA957F2E8C257D6E05A4573CB05753B6BB4F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3591 = { sizeof (KernelSizeParameter_t75F71C05A913F8456F68E98BD412A8B9990E2140), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3592 = { sizeof (DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3592[4] = 
{
	DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC::get_offset_of_focusDistance_7(),
	DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC::get_offset_of_aperture_8(),
	DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC::get_offset_of_focalLength_9(),
	DepthOfField_t82BB37F78622CC143942BA19603D89B9BCCE37AC::get_offset_of_kernelSize_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3593 = { sizeof (DepthOfFieldRenderer_t71088ABDEC239433D83BDD62EFE80B50DE928340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3593[2] = 
{
	DepthOfFieldRenderer_t71088ABDEC239433D83BDD62EFE80B50DE928340::get_offset_of_m_CoCHistoryTextures_2(),
	DepthOfFieldRenderer_t71088ABDEC239433D83BDD62EFE80B50DE928340::get_offset_of_m_HistoryPingPong_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3594 = { sizeof (Pass_tE33D40B930CCB914A8ADC1F369099F2C1995B954)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3594[11] = 
{
	Pass_tE33D40B930CCB914A8ADC1F369099F2C1995B954::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3595 = { sizeof (Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3595[2] = 
{
	Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0::get_offset_of_m_NoiseTextureIndex_0(),
	Dithering_t89B6809A4234E4E26A1DAAF702F743E3EEAE7FE0::get_offset_of_m_Random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3596 = { sizeof (FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3596[2] = 
{
	FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D::get_offset_of_fastMode_0(),
	FastApproximateAntialiasing_t8677D2B4EDDE97C4D6C747227D40395AD1DA134D::get_offset_of_keepAlpha_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3597 = { sizeof (Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3597[2] = 
{
	Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77::get_offset_of_enabled_0(),
	Fog_tCE0AAFBFFCC3EBE688BBFC6F0472694597D97E77::get_offset_of_excludeSkybox_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3598 = { sizeof (Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3598[4] = 
{
	Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6::get_offset_of_colored_7(),
	Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6::get_offset_of_intensity_8(),
	Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6::get_offset_of_size_9(),
	Grain_t18BE9242394A40BCB11DB23E3FF58A5C37876FD6::get_offset_of_lumContrib_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3599 = { sizeof (GrainRenderer_t8745359451C44DC431A99B61BD2F7584625F7F25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3599[2] = 
{
	GrainRenderer_t8745359451C44DC431A99B61BD2F7584625F7F25::get_offset_of_m_GrainLookupRT_2(),
	GrainRenderer_t8745359451C44DC431A99B61BD2F7584625F7F25::get_offset_of_m_SampleIndex_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
