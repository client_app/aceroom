# 개발정보 및 빌드 방법

## Tool/개발환경 정보
#### Unity
- Tool : Unity 2018.4.2f1
- Language : C#
- Extension: Vuforia(개발 시점 8.3)
#### Android
- Tool : Android Studio
- Language : java
- Minimum Target Level : Android 8.0 Oreo (API Level 26)
- Target Level : Android 9.0 Pie (API Level 28)
#### iOS
- Tool : Xcode
- Language : Objective-C, Swift5
- Target : iOS 11.3


## Unity Build 시 확인 및 주의점
#### Vuforia가 설치 및 활성화 되어 있는지 여부 확인
- Build info에서 Target 별 Vuforia 체크여부
#### Vufoira Information에서 라이센스 키 입력 여부 및 서비스용 라이센스 키 인지 여부
#### 어반베이스의 Crane 라이브러리(DLLCore.dll) 이 Android/iOS 타겟에 맞게 치환 및 빌드 했는지 여부
#### Native-App Plugin 수정 시 유니티와 각 플랫폼 소스가 같은지 체크

## Android Build 시 확인 및 주의점
#### Unity가 새로 빌드 된 경우
- Build.gradle 파일에서 aaptOptions – noCompress에 Vufoira DB 이름이 맞는지 확인(unity에서 export 시 갱신)
- AndroidManifest.xml 파일에서 unity.build-id 항목에 이번에 빌드 된 id인지 확인(unity에서 export 시 갱신)
## iOS Build 시 확인 및 주의점
#### Unity가 새로 빌드 된 경우
- ARViewer.framework를 Project-General에 다시 설정
- Splash-Screen 재설정


## Unity에서 각 플랫폼 소스코드 Export
#### Android
- Android 플랫폼으로 Build Target Switch
- 어반베이스의 Crane 라이브러리(DLLCore.dll)를 Android에 맞게 변경
- Export – “AceRoom” 폴더명으로 Export 되므로 해당 경로에 맞게 덮어 씌울 수 있도록 Export
#### iOS
- iOS 플랫폼으로 Build Target Switch
- 어반베이스의 Crane 라이브러리(DLLCore.dll)를 iOS에 맞게 변경
- Export – 선택한 폴더에 Export되며, Append 옵션으로 Export

