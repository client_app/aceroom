﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

using DLLCore;

#if UNITY_EDITOR
public class DebugUtil : EditorWindow
{
    Dictionary<string, Action> _testScenario = new Dictionary<string, Action>()
    {
        //{
        //    "Doran.Convert2D",
        //    delegate
        //    {
        //        JSONObject jsonPreset = new JSONObject();
        //        jsonPreset.AddField("direction", "top");
        //        jsonPreset.AddField("isOrtho", true);

        //        Main.Instance.graphicSetting.SetShadowType(0); // none

        //        Main.Instance.generalSetting.SetViewMode("orbit");
        //        Main.Instance.observerController.SetPresetView(jsonPreset.ToString());
        //        Main.Instance.observerController.ActivateRotation(0); // false
        //    }
        //},
        {
            "Editor / Unit A / Product 1",
            delegate
            {
                UISingleton.Instance.GetUIController().HomeDesignMode();

                //JSONObject unitJSONObj = new JSONObject();
                //unitJSONObj.AddField("url", "https://s3.ap-northeast-2.amazonaws.com/ub-3d-assets-dev/unit/test_unit.dae");
                //unitJSONObj.AddField("format", "dae");
                //Main.Instance.unitController.Load(unitJSONObj.ToString());

                //JSONObject productJSONObj = new JSONObject();
                //productJSONObj.AddField("url", "https://s3.ap-northeast-2.amazonaws.com/ub-3d-assets-dev/product/test_chair.dae");
                //productJSONObj.AddField("format", "dae");
                //productJSONObj.AddField("planeType", "floor");
                //Main.Instance.productController.Load(productJSONObj.ToString());

                //Main.Instance.networkController.LoadUnitFromID("5600c1ba71da53ab8ad5dbd8", "dae");

                //Main.Instance.networkController.LoadProductFromID("fe5f8c83567e1eb916b512a2");
            }
        },
        {
            "Editor / Unit B / Product 1",
            delegate
            {
                UISingleton.Instance.GetUIController().HomeDesignMode();

                //Main.Instance.networkController.LoadUnitFromID("5600c1ba71da53ab8ad5dbd8", "dae");
            }
        },
        {
            "HomeDesign Mode",
            delegate
            {
                UISingleton.Instance.GetUIController().HomeDesignMode();
            }
        },
        {
            "ProductViewer Mode",
            delegate
            {
                UISingleton.Instance.GetUIController().ProductViewerMode();
            }
        },
        {
            "Thumbnail Mode",
            delegate
            {
                UISingleton.Instance.GetUIController().ThumbnailMode();
            }
        },
        {
            "ProductViewer / ChangeProduct",
            delegate
            {
                JSONObject productJSONObj = new JSONObject();
                productJSONObj.AddField("frameName", "VENATO");
                productJSONObj.AddField("size", "K");
                productJSONObj.AddField("color", "OakNatural");
                productJSONObj.AddField("mattress", "ROYALACE380");
                //UIController.Instance.SetFrameAndMattressOption(productJSONObj.ToString());
                string jsonStr = "{\"frameName\":\"LUNATO3\",\"size\":\"K\",\"color\":\"Gray\",\"mattress\":\"HT3\"}";
                // load frame and mattress
                UISingleton.Instance.GetUIController().SetFrameAndMattressOption(jsonStr);
            }
        },
        {
            "ProductViewer / ChangeProduct2",
            delegate
            {
                JSONObject productJSONObj = new JSONObject();
                productJSONObj.AddField("frameName", "BRA1439");
                productJSONObj.AddField("size", "LQ");
                productJSONObj.AddField("color", "Walnut");
                productJSONObj.AddField("mattress", "HT3");

                // load frame and mattress
                UISingleton.Instance.GetUIController().SetFrameAndMattressOption(productJSONObj.ToString());
            }
        },
        {
            "SampleFloorPaper",
            delegate
            {
                LoadSampleFloorPaper();
            }
        },
        {
            "SampleWallPaper",
            delegate
            {
                LoadSampleWallPaper();
            }
        },
        {
            "Unit.Flip",
            delegate
            {
                string stringJSON = "{\"direction\":\"x\"}";
                Main.Instance.unitController.Flip(stringJSON);
            }
        },
        {
            "ProductContainer.PrintAllItem",
            delegate
            {
                //Main.Instance.productContainer.PrintAllItems();
            }
        },
        {
            "Product.Duplicate",
            delegate
            {
                string stringJSON = "{\"uuid\":\"fe5f8c83567e1eb916b512a2\"}";
                Main.Instance.productController.Duplicate(stringJSON);
            }
        },
        {
            "Product.Remove",
            delegate
            {
                string stringJSON = "{\"uuid\":\"fe5f8c83567e1eb916b512a2\"}";
                Main.Instance.productController.Remove(stringJSON);
            }
        }

    };

    static void LoadSampleFloorPaper()
    {
        //string jsonString = "{\"id\":\"56aad7dc4dd02a2f1cff7708\",\"data\":{\"wireframe\":false,\"mapAlphaAnisotropy\":2,\"mapAlphaWrap\":[\"repeat\",\"repeat\"],\"mapAlphaOffset\":[0,0],\"mapAlphaRepeat\":[1,1],\"mapAlpha\":\"\",\"mapMetalnessAnisotropy\":2,\"mapMetalnessWrap\":[\"repeat\",\"repeat\"],\"mapMetalnessOffset\":[0,0],\"mapMetalnessRepeat\":[1,1],\"mapMetalness\":\"\",\"mapRoughnessAnisotropy\":2,\"mapRoughnessWrap\":[\"repeat\",\"repeat\"],\"mapRoughnessOffset\":[0,0],\"mapRoughnessRepeat\":[1,1],\"mapRoughness\":\"\",\"mapSpecularAnisotropy\":2,\"mapSpecularWrap\":[\"repeat\",\"repeat\"],\"mapSpecularOffset\":[0,0],\"mapSpecularRepeat\":[1,1],\"mapSpecular\":\"\",\"mapNormalAnisotropy\":2,\"mapNormalWrap\":[\"repeat\",\"repeat\"],\"mapNormalOffset\":[0,0],\"mapNormalRepeat\":[1,1],\"mapNormalFactor\":1,\"mapNormal\":\"\",\"mapBumpAnisotropy\":2,\"mapBumpWrap\":[\"repeat\",\"repeat\"],\"mapBumpOffset\":[0,0],\"mapBumpRepeat\":[1,1],\"mapBumpScale\":1,\"mapBump\":\"\",\"mapAOAnisotropy\":2,\"mapAOWrap\":[\"repeat\",\"repeat\"],\"mapAOOffset\":[0,0],\"mapAORepeat\":[1,1],\"mapAO\":\"\",\"mapLightAnisotropy\":2,\"mapLightWrap\":[\"repeat\",\"repeat\"],\"mapLightOffset\":[0,0],\"mapLightRepeat\":[1,1],\"mapLight\":\"\",\"mapEmissiveAnisotropy\":2,\"mapEmissiveWrap\":[\"repeat\",\"repeat\"],\"mapEmissiveOffset\":[0,0],\"mapEmissiveRepeat\":[1,1],\"mapEmissive\":\"\",\"mapDiffuseAnisotropy\":2,\"mapDiffuseWrap\":[\"repeat\",\"repeat\"],\"mapDiffuseOffset\":[0,0],\"mapDiffuseRepeat\":[4,4],\"mapDiffuse\":\"https://d5ogl78ccxxow.cloudfront.net/ub/imgs/textures/floor/56aad7dc4dd02a2f1cff7708.jpg\",\"vertexColors\":false,\"visible\":true,\"reflectivity\":1,\"colorWrite\":true,\"depthWrite\":true,\"depthTest\":true,\"blending\":1,\"illumination\":\"\",\"opticalDensity\":0,\"DbgIndex\":0,\"DbgColor\":\"\",\"doubleSided\":false,\"flipSided\":false,\"flatShading\":false,\"opacity\":1,\"transparent\":false,\"specularCoef\":1,\"shading\":\"\",\"colorEmissive\":[0,0,0],\"colorSpecular\":[0,0,0],\"colorDiffuse\":[1,1,1]}}";
        //Main.Instance.unitController.ChangeStyleOfFloor(jsonString);
    }

    static void LoadSampleWallPaper()
    {
        //string jsonString = "{\"id\":\"56a9d0d5c3bad5333ca0a0e3\",\"data\":{\"wireframe\":false,\"mapAlphaAnisotropy\":2,\"mapAlphaWrap\":[\"repeat\",\"repeat\"],\"mapAlphaOffset\":[0,0],\"mapAlphaRepeat\":[1,1],\"mapAlpha\":\"\",\"mapMetalnessAnisotropy\":2,\"mapMetalnessWrap\":[\"repeat\",\"repeat\"],\"mapMetalnessOffset\":[0,0],\"mapMetalnessRepeat\":[1,1],\"mapMetalness\":\"\",\"mapRoughnessAnisotropy\":2,\"mapRoughnessWrap\":[\"repeat\",\"repeat\"],\"mapRoughnessOffset\":[0,0],\"mapRoughnessRepeat\":[1,1],\"mapRoughness\":\"\",\"mapSpecularAnisotropy\":2,\"mapSpecularWrap\":[\"repeat\",\"repeat\"],\"mapSpecularOffset\":[0,0],\"mapSpecularRepeat\":[1,1],\"mapSpecular\":\"\",\"mapNormalAnisotropy\":2,\"mapNormalWrap\":[\"repeat\",\"repeat\"],\"mapNormalOffset\":[0,0],\"mapNormalRepeat\":[1,1],\"mapNormalFactor\":1,\"mapNormal\":\"\",\"mapBumpAnisotropy\":2,\"mapBumpWrap\":[\"repeat\",\"repeat\"],\"mapBumpOffset\":[0,0],\"mapBumpRepeat\":[1,1],\"mapBumpScale\":1,\"mapBump\":\"\",\"mapAOAnisotropy\":2,\"mapAOWrap\":[\"repeat\",\"repeat\"],\"mapAOOffset\":[0,0],\"mapAORepeat\":[1,1],\"mapAO\":\"\",\"mapLightAnisotropy\":2,\"mapLightWrap\":[\"repeat\",\"repeat\"],\"mapLightOffset\":[0,0],\"mapLightRepeat\":[1,1],\"mapLight\":\"\",\"mapEmissiveAnisotropy\":2,\"mapEmissiveWrap\":[\"repeat\",\"repeat\"],\"mapEmissiveOffset\":[0,0],\"mapEmissiveRepeat\":[1,1],\"mapEmissive\":\"\",\"mapDiffuseAnisotropy\":2,\"mapDiffuseWrap\":[\"repeat\",\"repeat\"],\"mapDiffuseOffset\":[0,0],\"mapDiffuseRepeat\":[5.333333333333333,8],\"mapDiffuse\":\"https://d5ogl78ccxxow.cloudfront.net/ub/imgs/textures/wall/56a9d0d5c3bad5333ca0a0e3.jpg\",\"vertexColors\":false,\"visible\":true,\"reflectivity\":1,\"colorWrite\":true,\"depthWrite\":true,\"depthTest\":true,\"blending\":1,\"illumination\":\"\",\"opticalDensity\":0,\"DbgIndex\":0,\"DbgColor\":\"\",\"doubleSided\":false,\"flipSided\":false,\"flatShading\":false,\"opacity\":1,\"transparent\":false,\"specularCoef\":1,\"shading\":\"\",\"colorEmissive\":[0,0,0],\"colorSpecular\":[0,0,0],\"colorDiffuse\":[1,1,1]},\"updatedAt\":\"2018-04-11T03:11:53.384Z\"}";
        //Main.Instance.unitController.ChangeStyleOfWall(jsonString);
    }

    bool _bRun = false;

    Dictionary<string, List<object>> _methodValues;
    List<string> _ignoreMethodNames;

    Vector2 _scrollPosition = Vector2.zero;

    [MenuItem("Urbanbase/Debug Window")]
    static void Init()
    {
        GetWindow(typeof(DebugUtil));
    }

    void OnEnable()
    {
        _bRun = true;

        _methodValues = new Dictionary<string, List<object>>();

        // Save MonoBehaivour method names to ignoreMethodNames
        _ignoreMethodNames = new List<string>();
        Type t = typeof(MonoBehaviour);
        MethodInfo[] methodName = t.GetMethods();
        foreach (MethodInfo method in methodName)
        {
            _ignoreMethodNames.Add(method.Name);
        }
        _ignoreMethodNames.Add("Init");
        _ignoreMethodNames.Add("Destroy");
    }

    void OnDisable()
    {
        _bRun = false;
    }

    void OnGUI()
    {
        if (!EditorApplication.isPlaying) return;
        //if (!_bRun || Main.Instance == null) return;

        // current scene

        _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);

        ///////////////////////////////// test scenario

        GUILayout.Label("TEST SCENARIO", EditorStyles.boldLabel);
        foreach (var test in _testScenario)
        {
            bool cActive = GUILayout.Button(test.Key, GUILayout.MaxWidth(Screen.width / 2));

            if (cActive)
            {
                test.Value();
            }
        }

        GUILayout.Space(20);

        if (Main.Instance == null) return;

        GenerateDebugMenu(Main.Instance.generalSetting);
        GenerateDebugMenu(Main.Instance.graphicSetting);

        GenerateDebugMenu(Main.Instance.playerController);
        GenerateDebugMenu(Main.Instance.observerController);

        GenerateDebugMenu(Main.Instance.unitController);
        GenerateDebugMenu(Main.Instance.productController);
        GenerateDebugMenu(Main.Instance.daylightController);
        //GenerateDebugMenu(Main.Instance.liveSketchController);
        GenerateDebugMenu(Main.Instance.toolController);
        //GenerateDebugMenu(Main.Instance.postController);

        EditorGUILayout.EndScrollView();
    }

    void GenerateDebugMenu(object obj)
    {
        if (obj == null) return;
        Type t = obj.GetType();
        var name = t.ToString();
        GUILayout.Label(name, EditorStyles.boldLabel);

        MethodInfo[] methodName = t.GetMethods();

        foreach (MethodInfo method in methodName)
        {
            List<object> values = new List<object>();

            int tempInt = 0;
            float tempfloat = 0f;
            bool tempBool = false;
            string tempString = "";

            bool bExist = false;

            if (_ignoreMethodNames.Contains(method.Name)) continue;

            if (_methodValues.ContainsKey($"{name}.{method.Name}"))
            {
                values = _methodValues[$"{name}.{method.Name}"];
                bExist = true;
            }
            else
            {
                _methodValues.Add($"{name}.{method.Name}", values);
                bExist = false;
            }

            EditorGUILayout.BeginHorizontal();

            bool bActive = GUILayout.Button(method.Name.ToString(), GUILayout.MaxWidth(Screen.width / 2));

            ParameterInfo[] parameters = method.GetParameters();
            for (var i = 0; i < parameters.Length; i++)
            {
                if (method.ReflectedType.IsPublic)
                {
                    if (parameters[i].ParameterType == typeof(int))
                    {
                        EditorGUILayout.LabelField("Int", GUILayout.Width(40));
                        if (bExist) values[i] = EditorGUILayout.IntField((int)values[i], GUILayout.Width(30));
                        else values.Add(EditorGUILayout.IntField(tempInt, GUILayout.Width(30)));
                    }
                    else if (parameters[i].ParameterType == typeof(bool))
                    {
                        EditorGUILayout.LabelField("Bool", GUILayout.Width(40));
                        if (bExist) values[i] = EditorGUILayout.Toggle((bool)values[i], GUILayout.Width(30));
                        else values.Add(EditorGUILayout.Toggle(tempBool, GUILayout.Width(30)));
                    }
                    else if (parameters[i].ParameterType == typeof(float))
                    {
                        EditorGUILayout.LabelField("Float", GUILayout.Width(40));
                        if (bExist) values[i] = EditorGUILayout.FloatField((float)values[i], GUILayout.Width(30));
                        else values.Add(EditorGUILayout.FloatField(tempfloat, GUILayout.Width(30)));
                    }
                    else if (parameters[i].ParameterType == typeof(string))
                    {
                        EditorGUILayout.LabelField("String", GUILayout.Width(40));
                        if (bExist) values[i] = EditorGUILayout.TextField((string)values[i], GUILayout.Width(100));
                        else values.Add(EditorGUILayout.TextField(tempString, GUILayout.Width(100)));
                    }
                }
            }

            _methodValues[$"{name}.{method.Name}"] = values;

            EditorGUILayout.EndHorizontal();

            if (bActive)
            {
                method.Invoke(obj, values.ToArray());
            }
        }
        GUILayout.Space(20);

    }
}
#endif