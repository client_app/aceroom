﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fresnelScript : MonoBehaviour
{
    [SerializeField] private MeshRenderer   mesh;
     Material material;
    public float alpha = 0;


    // Start is called before the first frame update
    void Start()
    {
        material = mesh.GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        material.SetFloat("_Alpha", alpha);
    }
}
