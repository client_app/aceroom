﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectClip2 : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Renderer[] mesh;

    Material[] material = new Material[5];
    Color[] col = new Color[5];

    public float alpha = 0;

    void Start()
    {
        for(int i=0; i<mesh.Length; i++)
        {
            material[i] = mesh[i].material;
            col[i] = mesh[i].GetComponent<Renderer>().material.color;
        }
    }
    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < mesh.Length; i++)
        {
            //col[i].a = alpha;
            //material[i].SetColor("_Color", col[i]);
            material[i].SetFloat("_Alpha", alpha);
        }

    }
}
