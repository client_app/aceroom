﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manfresnelScript : MonoBehaviour
{
    [SerializeField] private MeshRenderer   mesh;
     Material material;
    public float alpha = 0;


    void Start()
    {
        material = mesh.GetComponent<Renderer>().material;
    }

    void Update()
    {
        material.SetFloat("_Alpha", alpha);
    }
}
