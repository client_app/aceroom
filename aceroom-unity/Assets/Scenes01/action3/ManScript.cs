﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManScript : MonoBehaviour
{
    [SerializeField] Renderer mesh;

    Material material;
    public float alpha = 1.0f;
    Color color;

    void Start()
    {
        material = mesh.GetComponent<Renderer>().material;
        color = material.color;
    }

    // Update is called once per frame
    void Update()
    {
        color.a = alpha;
        material.SetColor("_Color", color);
        //material.SetFloat("_Alpha", alpha);
    }
}
