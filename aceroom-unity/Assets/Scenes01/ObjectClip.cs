﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectClip : MonoBehaviour
{
    [SerializeField] Renderer   mesh;

    Material material;

    //private float accTime = -0.1f;
    public float anitime = 0;
    public float alpha = 1.0f;

    void Start()
    {
        material = mesh.GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        //accTime += Time.deltaTime * 0.5f;
        material.SetFloat("_Clip", anitime);
        material.SetFloat("_Alpha", alpha);
    }
}
