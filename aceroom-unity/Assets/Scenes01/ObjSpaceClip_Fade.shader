Shader "Custom/ObjSpaceClip_Fade"
{
    Properties
    {
        //_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
        _Smoothness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_Emission ("Emission Color", Color) = (1,1,1,1)
		_Clip  ("ClipValue", Range(-1,3)) = 0.0
        _Alpha ("AlphaValue", Range(0,1)) = 1.0
    }
	
    SubShader
    {
        Tags {"Queue" = "Transparent" "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Back
        LOD 200
		
		Pass 
		{
			ZWrite On
			ColorMask 0
		}
   
        CGPROGRAM
 
        #pragma surface surf Standard alpha:fade
		#pragma vertex vert
		
		struct Input 
		{
			float2 uv_MainTex;
            float3 objPos;
        };
 
		sampler2D _MainTex;
        half _Smoothness;
        half _Metallic;
        //fixed4 _Color;
		fixed4 _Emission;
		float _Clip;
        float _Alpha;

		void vert (inout appdata_full v, out Input o) 
		{
            UNITY_INITIALIZE_OUTPUT(Input,o);
			o.uv_MainTex = v.texcoord.xy;
			o.objPos = v.vertex;
        }
 
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			float z = _Clip - IN.objPos.z;

			//float a = clamp(z, 0, 1);
			float a = round(clamp(z, 0, 1));

            o.Albedo = tex2D(_MainTex, IN.uv_MainTex);
            o.Metallic = _Metallic;
			o.Emission = _Emission.rgb; 
            o.Smoothness = _Smoothness;
            o.Alpha = a * _Alpha;
        }
        ENDCG
    }
    FallBack "Standard"
}
 