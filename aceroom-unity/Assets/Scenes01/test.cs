﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Renderer  [] mesh;


    Material [] material = new Material[5];
    Color [] col = new Color[5];

    //private float accTime = -0.1f;
    //public float anitime = 0;
    public float alpha = 0;

    private void Awake()
    {
        for (int i = 0; i < mesh.Length; i++)
        {
            if (mesh[i] == null) continue;
            material[i] = mesh[i].material;
            col[i] = mesh[i].GetComponent<Renderer>().material.color;
        }
        Update();
    }
/*
    void Start()
    {
        for(int i=0; i<mesh.Length; i++)
        {
            material[i] = mesh[i].material;
            col[i] = mesh[i].GetComponent<Renderer>().material.color;
        }

        //material = GetComponent<Renderer>().material;
        
    }
*/
    // Update is called once per frame
    void Update()
    {
        //accTime += Time.deltaTime * 0.5f;
        //material.SetFloat("_CutValue", anitime);
        // alpha += Time.deltaTime * 0.5f;   
        for(int i=0; i< mesh.Length; i++)
        {
            if (col[i] == null) continue;
            col[i].a = alpha;
            material[i].SetColor("_Color", col[i]);
        }

    }
}
