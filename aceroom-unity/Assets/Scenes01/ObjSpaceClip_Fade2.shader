Shader "Custom/ObjSpaceClip_Fade2"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _Smoothness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_Emission ("Emission Color", Color) = (1,1,1,1)
		_Clip  ("ClipValue", Range(-1,3)) = 0.0
        _Alpha ("AlphaValue", Range(0,1)) = 1.0
    }
	
    SubShader
    {
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
		Cull Back
        LOD 200
   
		Pass 
		{
			ZWrite On
			ColorMask 0
		}

        CGPROGRAM
 
        #pragma surface surf Standard alpha:fade
		#pragma vertex vert
		
		struct Input 
		{
            float3 objPos;
        };
 
        half _Smoothness;
        half _Metallic;
        fixed4 _Color;
		fixed4 _Emission;
		float _Clip;
        float _Alpha;

		void vert (inout appdata_full v, out Input o) 
		{
            UNITY_INITIALIZE_OUTPUT(Input,o);
			o.objPos = v.vertex;
        }
 
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			float z = _Clip - IN.objPos.z;

			//float a = clamp(z, 0, 1);
			float a = round(clamp(z, 0, 1));

            o.Albedo = _Color.rgb;
            o.Metallic = _Metallic;
			o.Emission = _Emission.rgb; 
            o.Smoothness = _Smoothness;
            o.Alpha = 1;//a * _Alpha;
        }
        ENDCG
    }
   // FallBack "Standard"
}
 