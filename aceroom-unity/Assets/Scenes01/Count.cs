﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Count : MonoBehaviour
{
    [SerializeField] Text countText;
    private int boundcount =0;

    public void IncreaseCount()
    {
        boundcount++;
        countText.text = string.Format("{0:D2}", boundcount);
    }
}
