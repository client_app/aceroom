﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TestTime : MonoBehaviour
{
    [SerializeField] Text timetext;
    private float etime = 0;

    void Start()
    {
        etime = 0;
    }

    void Update()
    {
        etime += Time.deltaTime;
        TimeSpan t = TimeSpan.FromSeconds(etime);
        string timestr = string.Format("{0} : {1} : {2}", t.Minutes, t.Seconds, t.Milliseconds);
        timetext.text = timestr;
   }
}
