﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MattressUIController : AceSingleton<ARUIController>
{
    public enum GUIState : byte
    {
        NonDetected = 0,
        Detected,
        AnimationStart,
        AnimationEnd
    }

    public enum MattressAnimation
    {
        Animation1 = 0,
        Animation2 = 1,
        Animation3 = 2
    }

    public string[] animationNameList = { "Action-1", "Action-2", "Action-3" };

    public Button btnBack;
    public Button btnTutorial;
    public GameObject panelMenu;
    public Button btnAni1;
    public Button btnAni2;
    public Button btnAni3;
    public Button mattressButton;
    public GameObject midAirPosition;

    private int IntAniObjScale = 1;

    private string animationName;

    public void SetObjectScale(int scale)
    {
        IntAniObjScale = scale;
    }

    public int GetObjectScale()
    {
        return IntAniObjScale;
    }

    private void Start()
    {
        if (btnBack != null)
        {
            btnBack.onClick.AddListener(() => {
                Log("DefaultScene Load");
                RemoveAnimationObject();
                getMobileManager().CallChangeSceneDefault("");
            });
        }

        if (btnTutorial != null)
        {
            btnTutorial.onClick.AddListener(() => {
                Debug.Log(this.gameObject.name);
                RemoveAnimationObject();
                getMobileManager().CallTutorial();

            });
        }

        if (btnAni1 != null)
        {
            btnAni1.onClick.AddListener(() =>
            {
                SendAdbrixAction(MattressAnimation.Animation1);
                StartCoroutine(PlayAnimation(MattressAnimation.Animation1));
            });
        }

        if (btnAni2 != null)
        {
            btnAni2.onClick.AddListener(() =>
            {
                SendAdbrixAction(MattressAnimation.Animation1);
                StartCoroutine(PlayAnimation(MattressAnimation.Animation2));
            });
        }

        if (btnAni3 != null)
        {
            btnAni3.onClick.AddListener(() =>
            {
                SendAdbrixAction(MattressAnimation.Animation1);
                StartCoroutine(PlayAnimation(MattressAnimation.Animation3));
            });
        }

        if (mattressButton != null)
        {
            mattressButton.onClick.AddListener(() => {
                Debug.Log("MattressButton");
                panelMenu.SetActive(true);
            });
        }
    }

    public void SendAdbrixAction(MattressAnimation aniNo)
    {
        JSONObject actionJSONObj = new JSONObject();
        actionJSONObj.AddField("action", "AR3_AR_btn");

        if (aniNo == MattressAnimation.Animation1)
        {
            actionJSONObj.AddField("name", "spring");
        }
        else if (aniNo == MattressAnimation.Animation2)
        {
            actionJSONObj.AddField("name", "allinone");
        }
        else if (aniNo == MattressAnimation.Animation3)
        {
            actionJSONObj.AddField("name", "twomatt");
        }

        getMobileManager().CallAdbrixCustomAction(actionJSONObj.ToString());
    }
    

    private void Update()
    {
        
    }

    IEnumerator PlayAnimation(MattressAnimation aniNo)
    {
        RemoveAnimationObject();
        panelMenu.SetActive(false);

        if (aniNo == MattressAnimation.Animation1)
        {
            animationName = "Action-1";
        }
        else if (aniNo == MattressAnimation.Animation2)
        {
            animationName = "Action-2";
        }
        else if (aniNo == MattressAnimation.Animation3)
        {
            animationName = "Action-3";
        }


        Vector3 position = Vector3.zero;
        Log("Animation Instantiate : "+ animationName);

        ResourceRequest loadAsync = Resources.LoadAsync("Prefabs/MattressAnimation/" + animationName, typeof(GameObject));

        while (!loadAsync.isDone)
        {
            Debug.Log("Load Progress: " + loadAsync.progress);
            yield return null;
        }

        GameObject prefab = loadAsync.asset as GameObject;
        GameObject matress = Instantiate(prefab);
        matress.name = animationName;
        matress.transform.parent = midAirPosition.transform;
        matress.transform.localPosition = position;
        matress.transform.localRotation = Quaternion.identity;
        matress.transform.localScale = Vector3.one * IntAniObjScale;
    }

    private void RemoveAnimationObject()
    {
        foreach (Transform tf in midAirPosition.transform)
        {
            Destroy(tf.gameObject);
        }
    }

    public void OnAnimationStart(string animName)
    {
        Log("Animation Start - " + animName);
    }

    public void OnAnimationEnd(string animName)
    {
        Log("Animation End - " + animName);
    }

    public override void Log(string message)
    {
        if (DefineWord.__IS_SHOW_LOG)
        {
            Debug.Log("[Unity] "+ "[" + this.gameObject.name + "]" + message, this);
        }
    }
}
