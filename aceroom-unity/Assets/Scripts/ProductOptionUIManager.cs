﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using DLLCore;
using UnityEngine.EventSystems;

/// <summary>
/// [어반베이스] Crane 용 컴포넌트
/// 수정 시 어반베이스에 문의 필요.
/// </summary>
public class ProductOptionUIManager : MonoBehaviour
{
    public Button backButton;
    public Button placeButton;
    public Text productName;
    public Dropdown sizeDropdown;
    public Toggle[] colorButtons;
    public Dropdown mattressDropdown;

    public RawImage productImage;

    int sizeIndex = -1;
    int colorIndex = -1;
    int mattressIndex = -1;

    // Start is called before the first frame update
    void Start()
    {
        backButton.onClick.AddListener(() => { OnClickBack(); });
        placeButton.onClick.AddListener(() => { OnClickPlace(); });
        sizeDropdown.onValueChanged.AddListener((index) => { OnValueChangedSize(index); });
        mattressDropdown.onValueChanged.AddListener((index) => { OnValueChangedMattress(index); });
        colorButtons[0].onValueChanged.AddListener((value) => { OnValueChangedColorButton(0, value); });
        colorButtons[1].onValueChanged.AddListener((value) => { OnValueChangedColorButton(1, value); });
        colorButtons[2].onValueChanged.AddListener((value) => { OnValueChangedColorButton(2, value); });
        
    }

    public void SetProductData(string name, string[] sizeArray, string[] colorArray, string size, string color, string imagePath)
    {
        productName.text = name;
        // size option
        List<Dropdown.OptionData> sizeOption = new List<Dropdown.OptionData>();
        int _sizeIndex = -1;
        for(int i = 0; i < sizeArray.Length; ++i)
        {
            Dropdown.OptionData opt = new Dropdown.OptionData
            {
                text = sizeArray[i]
            };
            sizeOption.Add(opt);
            if (size == sizeArray[i])
                _sizeIndex = i;
        }
        sizeDropdown.options = sizeOption;

        int _colorIndex = -1;
        // hide first
        for (int i = 0; i < colorButtons.Length; ++i)
            colorButtons[i].gameObject.SetActive(false);

        for (int i = 0; i < colorArray.Length; ++i)
        {
            colorButtons[i].gameObject.SetActive(true);
            colorButtons[i].GetComponentInChildren<Text>().text = colorArray[i];
            if (color == colorArray[i])
            {
                _colorIndex = i;
                ExecuteEvents.Execute(colorButtons[i].gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
            }
        }

        sizeDropdown.value = sizeIndex = _sizeIndex;
        colorIndex = _colorIndex;

        // size filter mattress
        List<Dropdown.OptionData> mattressOption = new List<Dropdown.OptionData>();
        string[] mattressList = UISingleton.Instance.GetUIController().GetMattressListFromSize(sizeArray[_sizeIndex]);
        for (int i = 0; i < mattressList.Length; ++i)
        {
            Dropdown.OptionData opt = new Dropdown.OptionData
            {
                text = mattressList[i]
            };
            mattressOption.Add(opt);
        }
        mattressDropdown.options = mattressOption;
        mattressDropdown.value = mattressIndex = 0;

        StartCoroutine(LoadProductImage(imagePath));
    }

    IEnumerator LoadProductImage(string path)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(path);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            productImage.texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnClickBack()
    {
        gameObject.SetActive(false);
    }

    string GetFrameIDFromNameSizeColor()
    {
        string frameID = string.Empty;
        try
        {
            string size = sizeDropdown.options[sizeIndex].text;
            string color = colorButtons[colorIndex].GetComponentInChildren<Text>().text;
            frameID = UISingleton.Instance.GetUIController().GetFrameId(productName.text, size, color);
        }
        catch (System.ArgumentOutOfRangeException ex)
        {
            Debug.Log("GetFrameID argument error: " + ex.ToString());
        }

        return frameID;
    }

    void OnClickPlace()
    {
        // find frame id from name, size, color
        // dictionary (key: "FrameName_Size_Color", value: "frameId")
        string frameID = GetFrameIDFromNameSizeColor();
        string path = UISingleton.Instance.GetUIController().GetFramePathFromID(frameID);
        // load framd and load mattress from frame and size
        string frameName = productName.text;
        string size = sizeDropdown.options[sizeIndex].text;
        //string color = colorDropdown.options[colorIndex].text;
        string color = colorButtons[colorIndex].GetComponentInChildren<Text>().text;

        UISingleton.Instance.GetUIController().ChangeProductFrameAndMattress(frameName, size, color, mattressDropdown.options[mattressIndex].text);

        // hide UI
        this.gameObject.SetActive(false);
        UISingleton.Instance.GetUIController().OnClickShowItemList(false);
    }

    void OnValueChangedSize(int index)
    {
        sizeIndex = index;
        string frameID = GetFrameIDFromNameSizeColor();
        string imagePath = UISingleton.Instance.GetUIController().GetThumbnailPathFromID(frameID);
        StartCoroutine(LoadProductImage(imagePath));

        // mattress option size apply
        // size filter mattress
        mattressDropdown.options.Clear();

        List<Dropdown.OptionData> mattressOption = new List<Dropdown.OptionData>();
        string[] mattressList = UISingleton.Instance.GetUIController().GetMattressListFromSize(sizeDropdown.options[sizeIndex].text);
        for (int i = 0; i < mattressList.Length; ++i)
        {
            Dropdown.OptionData opt = new Dropdown.OptionData
            {
                text = mattressList[i]
            };
            mattressOption.Add(opt);
        }
        mattressDropdown.options = mattressOption;
        mattressDropdown.value = mattressIndex = 0;
    }

    void OnValueChangedColorButton(int index, bool value)
    {
        if (value)
        {
            colorIndex = index;
            colorButtons[index].Select();

            string frameID = GetFrameIDFromNameSizeColor();
            string imagePath = UISingleton.Instance.GetUIController().GetThumbnailPathFromID(frameID);
            StartCoroutine(LoadProductImage(imagePath));
        }
    }

    void OnValueChangedColor(int index)
    {
        colorIndex = index;
        string frameID = GetFrameIDFromNameSizeColor();
        string imagePath = UISingleton.Instance.GetUIController().GetThumbnailPathFromID(frameID);
        StartCoroutine(LoadProductImage(imagePath));
    }

    void OnValueChangedMattress(int index)
    {
        mattressIndex = index;
    }

    public void OnDisable()
    {
        // remove texture to avoid blink when show another thumbnail
        if (productImage != null && productImage.texture != null)
        {
            Destroy(productImage.texture);
            productImage.texture = null;
        }

        for(int i = 0; i < colorButtons.Length; ++i)
        {
            if (colorButtons[i] != null)
            {
                colorButtons[i].isOn = false;
            }
        }
    }
}
