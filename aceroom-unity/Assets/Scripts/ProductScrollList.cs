﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DLLCore;
using UnityEngine.UI;

/// <summary>
/// [어반베이스] Crane 용 컴포넌트
/// 수정 시 어반베이스에 문의 필요.
/// </summary>
public class ProductScrollList : MonoBehaviour
    , IPointerExitHandler, IPointerEnterHandler
{
    public static string FILTER_TOTAL = "Total";
    public static string FILTER_WEDDING = "Wedding";
    public static string FILTER_SUPERSINGLE = "SuperSingle";
    public static string FILTER_FAMILY = "Family";

    public Button scrollUpButton;
    public Dropdown filter;

    public void OnPointerExit(PointerEventData eventData)
    {
        if (Main.Instance.observerController != null)
            Main.Instance.observerController.ActivateRotation();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (Main.Instance.observerController != null)
            Main.Instance.observerController.DeactivateRotation();
    }

    // Start is called before the first frame update
    void Start()
    {
        scrollUpButton.onClick.AddListener(() => { OnClickResetScroll(); });

        InitDropdownFilter();
    }

    public void InitDropdownFilter()
    {
        List<Dropdown.OptionData> filterOption = new List<Dropdown.OptionData>();
        Dropdown.OptionData optionData0 = new Dropdown.OptionData()
        {
            text = "Total"
        };
        filterOption.Add(optionData0);

        Dropdown.OptionData optionData1 = new Dropdown.OptionData()
        {
            text = "NewWedding"
        };
        filterOption.Add(optionData1);

        Dropdown.OptionData optionData2 = new Dropdown.OptionData()
        {
            text = "SuperSingle"
        };
        filterOption.Add(optionData2);

        Dropdown.OptionData optionData3 = new Dropdown.OptionData()
        {
            text = "Family"
        };
        filterOption.Add(optionData3);

        filter.options = filterOption;

        filter.onValueChanged.AddListener((value) => { OnValueChangedFilter(value); });
    }

    void OnEnable()
    {
        
    }

    void OnDisable()
    {
        if (Main.Instance.observerController != null)
            Main.Instance.observerController.ActivateRotation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnClickResetScroll()
    {
        GetComponent<ScrollRect>().verticalNormalizedPosition = 1;
    }

    void OnValueChangedFilter(int filter)
    {
        Debug.Log("filter" + filter);
        UISingleton.Instance.GetUIController().ResetProductListWithFilter(filter);
    }
}
