﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DLLCore;
using CommandTerminal;
using System.Security.Cryptography;
using System.Reflection;
using System.Text;
using System;

/// <summary>
/// [어반베이스] Crane 용 컴포넌트
/// 수정 시 어반베이스에 문의 필요.
/// </summary>
public class ImportCoreDLL : Singleton<ImportCoreDLL>
{
    public TextAsset assembly;

    private string gainedKey = "8BD2EFF69C7D88C51CCBA92D741B4085";
    private string gainedIv = "1C31A4FDE89C4B9C";

    protected ImportCoreDLL() { } // guarantee this will be always a singleton only - can't use the constructor!

    public Main _main;

    [Header("Settings")]
    public GeneralSetting generalSetting;
    public GraphicSetting graphicSetting;

    [Header("Messaging")]
    public GameObject terminal;
    public EventManager eventManager;

    [Header("Modules")]
    public ReflectionControls reflectionControls;
    //public GizmoControls gizmoControls;

    [Header("Controllers")]
    //public GameObject goController;
    public DaylightController daylightController;

    [Header("Loaders")]
    public MaterialLoader materialLoader;
    public ModelLoader modelLoader;

    // Start is called before the first frame update
    void Start()
    {
        // Load encrypted data and decryption keys.  
        byte[] bytes = Convert.FromBase64String(assembly.text);
        byte[] key = StringToByteArray(gainedKey);
        byte[] iv = StringToByteArray(gainedIv);

        // Decrypt assembly.  
        RC2 rc2 = new RC2CryptoServiceProvider();
        rc2.Mode = CipherMode.CBC;
        ICryptoTransform xform = rc2.CreateDecryptor(key, iv);
        byte[] decrypted = xform.TransformFinalBlock(bytes, 0, bytes.Length);
        _main = Main.Instance;
        _main.CheckLicenseKey(decrypted);

        _main.generalSetting = generalSetting;
        _main.graphicSetting = graphicSetting;
        generalSetting.gameObject.SetActive(true);
        graphicSetting.gameObject.SetActive(true);

        _main.materialLoader = materialLoader;
        _main.modelLoader = modelLoader;
        materialLoader.gameObject.SetActive(true);
        modelLoader.gameObject.SetActive(true);

        _main.reflectionControls = reflectionControls;
        //_main.gizmoControls = gizmoControls;
        reflectionControls.gameObject.SetActive(true);
        //gizmoControls.gameObject.SetActive(true);

        _main.daylightController = daylightController;
        daylightController.gameObject.SetActive(true);

        daylightController.SetHour(9);
    }

    public Main GetCore()
    {
        return _main;
    }

    public void OnClickHomeDesignMode()
    {
        _main.generalSetting.SetSDKMode("homedesign");
    }

    public void OnClickLoadSampleUnitProduct()
    {
        _main.generalSetting.SetSDKMode("homedesign");

        JSONObject unitJSONObj = new JSONObject();
        unitJSONObj.AddField("url", UIController.BASE_PATH + "/unit/test_unit.dae");
        unitJSONObj.AddField("format", "dae");
        _main.unitController.Load(unitJSONObj.ToString());


        JSONObject productJSONObj = new JSONObject();
        productJSONObj.AddField("url", UIController.BASE_PATH + "/product/test_chair.dae");
        productJSONObj.AddField("format", "dae");
        productJSONObj.AddField("planeType", "floor");
        _main.productController.Load(productJSONObj.ToString());
    }

    public static byte[] StringToByteArray(string str)
    {
        int charLength = str.Length;
        byte[] bytes = new byte[charLength / 2];
        for (int i = 0; i < charLength; i += 2)
        {
            bytes[i / 2] = Convert.ToByte(str.Substring(i, 2), 16);
        }
        return bytes;
    }
}
