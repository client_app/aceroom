﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class MobileIOS : MobileBase
{
    #region Log
    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    public override void Log(string message)
    {
        message = "[IOS] " + message;
        base.Log(message);
    }
    #endregion

    #region iOS Method
#if UNITY_IPHONE
    [DllImport("__Internal")]
    private static extern void _iOSPluginShowToast(string strMessage);

    [DllImport("__Internal")]
    private static extern string _iOSPluginCallDeviceVersion();

    //app
    [DllImport("__Internal")]
    private static extern void _iOSPluginCallMainMenu(string strLastSceneName);

    [DllImport("__Internal")]
    private static extern void _iOSPluginCallNowScene(string strSceneName);
    [DllImport("__Internal")]
    private static extern void _iOSPluginCallTutorial(string strSceneName);

    [DllImport("__Internal")]
    private static extern void _iOSPluginCallScreenShot(string strImgPath);
    [DllImport("__Internal")]
    private static extern void _iOSPluginCallShare(string strImgPath);

    //Home Editor

    [DllImport("__Internal")]
    private static extern void _iOSPluginCallCatalog();
    [DllImport("__Internal")]
    private static extern void _iOSPluginLoadUnitComplete();
    [DllImport("__Internal")]
    private static extern void _iOSPluginLoadProductComplete();
    [DllImport("__Internal")]
    private static extern void _iOSPluginCallHomeOption(string optionJson);
    [DllImport("__Internal")]
    private static extern void _iOSPluginCallbackLoadProductView();
    [DllImport("__Internal")]
    private static extern void _iOSPluginCallbackLoadProductViewerScene();

    //[DllImport("__Internal")]
    //private static extern void _iOSPluginLoadFurnitureComplete();
    //[DllImport("__Internal")]
    //private static extern void _iOSPluginCallAdbrixCustomAction(string action);

    //TEST
    [DllImport("__Internal")]
    private static extern void _iOSPluginCallTEST(string strTestMsg);
#endif
    #endregion


    #region TestFunction
#if UNITY_IPHONE
    public override void CallShowToast(string msg)
    {
        Log("CallShowToast");
        //iOSPlugin.CallShowToast("Hello world!!");

        _iOSPluginShowToast(msg);
    }

    public override void CallDeviceVersionCheck()
    {
        Log("CallDeviceVersionCheck");
        _iOSPluginCallDeviceVersion();
    }

    public override void CallTest(string testMsg)
    {
        Log("CallTest");
        _iOSPluginCallTEST(testMsg);
    }
#endif
    #endregion

    #region Plugin
#if UNITY_IPHONE
    public override void CallPluginFunc(string strFuncName, string strTemp)
    {
        Log("CallPluginFunc - NOT USE");
    }

    public override void CallNowScene()
    {
        Log("CallNowScene");
        _iOSPluginCallNowScene(MobileManager.Instance.GetAceSceneModeName());
    }

    public override void CallBackButton()
    {
        Log("CallBackButton - NOT USE");
    }

    public override void CallTutorial()
    {
        Log("CallTutorial");
        _iOSPluginCallTutorial(MobileManager.Instance.GetAceSceneModeName());
    }

    public override void CallHomeEditorSave(string strImgPath)
    {
        Log("CallHomeEditorSave");
        _iOSPluginCallScreenShot(strImgPath);
    }

    public override void CallShare(string strImgPath)
    {
        Log("CallShare");
        _iOSPluginCallShare(strImgPath);
    }

    public override void CallMainMenu(string strOldScene)
    {
        Log("CallMainMenu");
        _iOSPluginCallMainMenu(strOldScene);
    }

    public override void CallCatalog()
    {
        Log("CallCatalog");
        _iOSPluginCallCatalog();
    }

    public override void CallbackLoadHomeUnit()
    {
        Log("CallbackLoadHomeUnit");
        _iOSPluginLoadUnitComplete();
    }

    public override void CallbackLoadHomeProduct()
    {
        Log("CallbackLoadHomeProduct");
        _iOSPluginLoadProductComplete();
    }

    [DllImport("__Internal")]
    private static extern void iOSPluginCallbackLoadFurnitureComplete();
    public override void CallbackLoadFurnitureComplete()
    {
        Log("CallbackLoadHomeProduct");
        iOSPluginCallbackLoadFurnitureComplete();
    }

    public override void CallHomeOption(string optionJson)
    {
        Log("CallHomeOption");
        _iOSPluginCallHomeOption(optionJson);
    }

    public override void CallbackLoadProductView(string msg)
    {
        Log("CallbackLoadProductView");
        _iOSPluginCallbackLoadProductView();
    }

    public override void CallbackLoadProductViewerScene(string msg)
    {
        Log("CallbackLoadProductViewerScene");
        _iOSPluginCallbackLoadProductViewerScene();
    }

    /// <summary>
    /// test 
    /// ARLens Viewer
    /// </summary>
    public override void requestHomeDesignMode()
    {
        Log("requestHomeDesignMode");
    }

    [DllImport("__Internal")]
    private static extern void iOSPluginCallStatic(string msg);
    /// <summary>
    /// Adbrix Custom Action 용 메소드
    /// </summary>
    public override void CallAdbrixCustomAction(string action)
    {
        Log("MobileBase - CallAdbrixCustomAction");
        iOSPluginCallStatic(action);
    }

    [DllImport("__Internal")]
    private static extern float FooPluginFunction();
#endif
    #endregion


}
