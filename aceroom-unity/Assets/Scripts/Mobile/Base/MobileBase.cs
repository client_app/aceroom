﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Android, iOS 네이티브 부분과 통신하기 위한 송신부 bridge
/// 통일성을 위해 return은 수신부에서 다른 메소드로 받는것으로 정의
/// </summary>
public class MobileBase : Object
{
    #region Log
    virtual public void Log(string message)
    {
        if (DefineWord.__IS_SHOW_LOG)
        {
            Debug.Log("[Unity] " + message, this);
        }
    }
    #endregion

    #region TestFunction
    /// <summary>
    /// Toast 메시지 팝업
    /// </summary>
    /// <param name="msg">표시될 메시지</param>
    virtual public void CallShowToast(string msg)
    {
        Log("MobileBase - CallShowToast");
    }

    /// <summary>
    /// 기기 앱 버전 체크 - 테스트용
    /// </summary>
    virtual public void CallDeviceVersionCheck()
    {
        Log("MobileBase - CallDeviceVersionCheck");
    }

    /// <summary>
    /// 기기 Plugin 테스트용
    /// </summary>
    /// <param name="testMsg">테스트 전달값</param>
    virtual public void CallTest(string testMsg)
    {
        Log("MobileBase - CallTest");
    }

    #endregion

    #region Plugin

    /// <summary>
    /// 플러그인 호출(네이티브로)
    /// </summary>
    /// <param name="strFuncName">네이티브 메소드 이름</param>
    /// <param name="strTemp">네이티브 메소드 파라미터</param>
    virtual public void CallPluginFunc(string strFuncName, string strTemp)
    {
        Log("MobileBase - CallPluginFunc");
    }

    /// <summary>
    /// 현제 Scene의 이름을 확인
    /// </summary>
    virtual public void CallNowScene()
    {
        Log("MobileBase - CallNowScene");
    }

    /// <summary>
    /// Unity에서 뒤로가기 버튼을 눌렀을 때
    /// </summary>
    virtual public void CallBackButton()
    {
        Log("MobileBase - CallBackButton");
    }

    /// <summary>
    /// 유니티에서 튜토리얼을 호출
    /// </summary>
    virtual public void CallTutorial()
    {
        Log("MobileBase - CallTutorial");
    }

    /// <summary>
    /// 홈에디터 - 저장 기능
    /// </summary>
    /// <param name="strImgPath">스크린샷 저장 경로</param>
    virtual public void CallHomeEditorSave(string strImgPath)
    {
        Log("MobileBase - CallHomeEditorSave");
    }

    /// <summary>
    /// 홈에디터 - 공유 기능
    /// </summary>
    /// <param name="strImgPath">스크린샷 저장 경로</param>
    virtual public void CallShare(string strImgPath)
    {
        Log("MobileBase - CallShare");
    }

    /// <summary>
    /// 네이티브 메뉴 호출
    /// </summary>
    virtual public void CallMainMenu(string strOldScene)
    {
        Log("MobileBase - CallMainMenu");
    }

    /// <summary>
    /// 홈에디터 - 제품선택 카탈로그 호출
    /// </summary>
    virtual public void CallCatalog()
    {
        Log("MobileBase - CallCatalog");
    }

    /// <summary>
    /// 홈에디터 - 도면 로딩 완료 시 호출
    /// </summary>
    virtual public void CallbackLoadHomeUnit()
    {
        Log("MobileBase - CallbackLoadHomeUnit");
    }

    /// <summary>
    /// 홈에디터 - 제품 로딩 완료 시 호출
    /// </summary>
    virtual public void CallbackLoadHomeProduct()
    {
        Log("MobileBase - CallbackLoadHomeProduct");
    }

    /// <summary>
    /// 홈에디터 - 제품(가구) 로딩 완료 시 호출
    /// </summary>
    virtual public void CallbackLoadFurnitureComplete()
    {
        Log("MobileBase - CallbackLoadFurnitureComplete");
    }

    /// <summary>
    /// 홈에디터 - 환경설정 호출
    /// </summary>
    /// <param name="optionJson">현제 적용중인 환경설정 데이터</param>
    virtual public void CallHomeOption(string optionJson)
    {
        Log("MobileBase - CallHomeOption");
    }

    /// <summary>
    /// 홈에디터 - 제품로딩 완료 콜백
    /// </summary>
    /// <param name="msg"></param>
    virtual public void CallbackLoadProductView(string msg)
    {
        Log("MobileBase - CallbackLoadProductView");
    }

    /// <summary>
    /// 홈에디터 - 홈에디터 씬 로드 완료 콜백
    /// </summary>
    /// <param name="msg"></param>
    virtual public void CallbackLoadProductViewerScene(string msg)
    {
        Log("MobileBase - CallbackLoadProductViewerScene");
    }
    /// <summary>
    /// test
    /// ARLens Viewer
    /// </summary>
    virtual public void requestHomeDesignMode()
    {
        Log("MobileBase - requestHomeDesignMode");
    }

    /// <summary>
    /// Adbrix Custom Action 용 메소드
    /// </summary>
    virtual public void CallAdbrixCustomAction(string action)
    {
        Log("MobileBase - CallAdbrixCustomAction");
    }

    #endregion

}
