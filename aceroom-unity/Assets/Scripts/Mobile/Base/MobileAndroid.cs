﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileAndroid : MobileBase
{

    private AndroidJavaObject UnityPlayerActivity = null;

    #region Log
    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    public override void Log(string message)
    {
        message = "[Android] " + message;
        base.Log(message);
    }
    #endregion

    #region Android Method
    public void CallActivityFunc(string strFuncName, string strTemp)
    {
        //if (UnityPlayerActivity != null)
        //{
        //    Log("MobileMaager - UnityPlayerActivity is null");
        //    UnityPlayerActivity.CallStatic(strFuncName, UnityPlayerActivity, strTemp);
        //}
        //else
        //{
            Log("MobileMaager - UnityPlayerActivity is not null");
            using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                UnityPlayerActivity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
                UnityPlayerActivity.CallStatic(strFuncName, UnityPlayerActivity, strTemp);

            }

            
        //}
    }

    #endregion

    //override


    #region TestFunction
    public override void CallShowToast(string msg)
    {
        Log("CallShowToast");

        if (UnityPlayerActivity != null)
        {
            CallActivityFunc("ShowToast", "TEST");
        }
    }

    public override void CallDeviceVersionCheck()
    {
        Log("CallDeviceVersionCheck");
        CallPluginFunc("CallDeviceVersionCheck", "");
    }

    public override void CallTest(string testMsg)
    {
        Log("CallTest - "+ testMsg);
        CallPluginFunc("CallTest", testMsg);
    }

    #endregion

    #region Plugin
    public override void CallPluginFunc(string strFuncName, string strTemp)
    {
        Log("CallPluginFunc");
        using (AndroidJavaClass cls = new AndroidJavaClass("com.acebed.aceroom.plugin.AcePlugin"))
        {
            using (AndroidJavaObject stars = cls.CallStatic<AndroidJavaObject>("getInstance"))
            {
                stars.Call(strFuncName, strTemp);
            }
        }
    }

    public override void CallNowScene()
    {
        Log("CallNowScene");
        
        CallActivityFunc("CallNowScene", MobileManager.Instance.GetAceSceneModeName());
    }

    public override void CallBackButton()
    {
        Log("CallBackButton");
        //CallActivityFunc("CallMainActivity", "call");
    }

    public override void CallTutorial()
    {
        Log("CallTutorial");
        CallActivityFunc("CallTutorial", MobileManager.Instance.GetAceSceneModeName());
    }

    public override void CallHomeEditorSave(string strImgPath)
    {
        Log("CallHomeEditorSave");
        CallActivityFunc("CallScreenShot", strImgPath);
    }

    public override void CallShare(string strImgPath)
    {
        Log("CallShare");
        CallActivityFunc("CallShare", strImgPath);
    }

    public override void CallMainMenu(string strOldScene)
    {
        Log("CallMainMenu");
        CallActivityFunc("CallMainActivity", strOldScene);
    }

    public override void CallCatalog()
    {
        Log("CallCatalog");
        CallActivityFunc("CallCatalog", "end");
    }

    public override void CallbackLoadHomeUnit()
    {
        Log("CallbackLoadHomeUnit");
        CallActivityFunc("CallbackLoadUnitComplete", "end");
    }

    public override void CallbackLoadHomeProduct()
    {
        Log("CallbackLoadHomeProduct");
        CallActivityFunc("CallbackLoadProductComplete", "end");
    }

    public override void CallbackLoadFurnitureComplete()
    {
        Log("CallbackLoadFurnitureComplete");
        CallActivityFunc("CallbackLoadFurnitureComplete", "end");
    }

    public override void CallHomeOption(string optionJson)
    {
        Log("CallHomeOption");
        CallActivityFunc("CallHomeOption", optionJson);
    }

    public override void CallbackLoadProductView(string msg)
    {
        Log("CallbackLoadProductView");
        CallActivityFunc("LoadProductCallback", msg);
    }

    public override void CallbackLoadProductViewerScene(string msg)
    {
        Log("CallbackLoadProductViewerScene");
        CallActivityFunc("LoadProductViewerSceneCallback", msg);
    }
    /// <summary>
    /// test
    /// ARLens Viewer
    /// </summary>
    public override void requestHomeDesignMode()
    {
        Log("requestHomeDesignMode");
        CallActivityFunc("HomeDesignMode", "end");
    }

    /// <summary>
    /// Adbrix Custom Action 용 메소드
    /// </summary>
    public override void CallAdbrixCustomAction(string action)
    {
        Log("MobileBase - CallBackStatic");
        CallActivityFunc("CallBackStatic", action);
    }



    #endregion
}
