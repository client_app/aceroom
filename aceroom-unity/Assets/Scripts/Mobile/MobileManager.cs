﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

/// <summary>
/// Android, iOS와 연동하여 유니티 부분을 컨트롤하는 싱글톤 오브젝트.
/// 모바일 네이티브 소스와 연동하기 위해 네이티브->유니티 방향의 수신부는 이 클래스에 정의
/// 송신부는 MobileBase 클래스 참고.
/// </summary>
public class MobileManager : AceSingleton<MobileManager>
{
    //static member
    public bool isNotch = false;
    public string strJsonHomeUnit;
    public string strModelTargetString;

    public Camera mainCam;    //기존 카메라
    public Camera loadingCam;    //로딩 카메라

	private MobileBase mobilePlugin;

    #region Lifecycle
    // Use this for initialization
    void Start()
    {
        mainCam = Camera.main;
    }

    void Awake()
    {
        Log("Awake");
    }

    /// <summary>
    /// Scene간 전환 시작
    /// </summary>
    public void SceneLoadStart()
    {
        if (mainCam == null)
        {
            mainCam = Camera.main;
        }

        mainCam.gameObject.SetActive(false); //기존 카메라 비활성화
        //loadingCam.gameObject.SetActive(true); //로딩화면에 있는 카메라
    }

    /// <summary>
    /// Scene간 전환이 완료된 경우 호출되는 메소드
    /// </summary>
    public void SceneLoadComplete()
    {
        Log("Scene 로딩 종료 - " + GetAceSceneName());

        if (DefineWord.__IS_TEST)
        {
            //loadingCam
            loadingCam.gameObject.SetActive(false); //로딩화면에 있는 카메라 활성화
        }
        else
        {
            if (!isDefaultScene())
            {
                //Camera loadingCamera = GameObject.FindWithTag("LoadingCam").GetComponent<Camera>() as Camera;
                loadingCam.gameObject.SetActive(false); //로딩화면에 있는 카메라 숨김
            }
        }

        Log("Scene 로딩 종료2 - " + GetAceSceneName());
        
        if (isHomeEditorScene()) // 전환된 Scene이 홈에디터인 경우, 어반베이스 Crane API 호출용 도면 Json을 전달
        {
            Log("LoadHomeUnit - " + strJsonHomeUnit);
            RoomSceneUIController.Instance.OnSceneLoadEnd(strJsonHomeUnit);
        }
        else if (isUrbanHomeEdit())
        {

        }
        else if (isUrbanProductViewerScene()) // 전환된 Scene이 어반베이스 제품 뷰어인 경우 로딩 완료 메세지 전달
        {
            CallbackLoadProductViewerScene("done");
        }
        Log("Scene 로딩 종료3 - " + GetAceSceneName());
    }
    #endregion

    #region 일반 Method

    public override void Log(string message)
    {
        message = "[MobileManager] " + message;
        base.Log(message);
    }

    /// <summary>
    /// Android/iOS 네이티브 플러그인 생성
    /// </summary>
    /// <returns>네이티브 송신 플러그인 MobileBase</returns>
    private MobileBase getMobilePlugin()
    {
        Log("mobilePlugin - Null is = "+ mobilePlugin == null ? "true" : "false");
        if (mobilePlugin == null)
        {
            Log("mobilePlugin - init");
            if (Application.platform == RuntimePlatform.Android)
            {
                Log("mobilePlugin - ANDROID");
                mobilePlugin = new MobileAndroid();
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                Log("mobilePlugin - IOS");
                mobilePlugin = new MobileIOS();
            }
            else
            {
                Log("mobilePlugin - PC/MAC");
                mobilePlugin = new MobileBase();
            }
        }
        
        return mobilePlugin;
    }

    #endregion

    #region Plugin 수신부
    public void NativeLog(string str)
    {
        Log(str);
    }

    /// <summary>
    /// 네이티브에서 유니티 화면을 가로로 강제 회전
    /// </summary>
    /// <param name="str">옵션</param>
    public void CallUnityLotationLandscape(string str)
    {
        Log("CallUnityLotationLandscape");
        Screen.orientation = ScreenOrientation.Landscape;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.autorotateToLandscapeLeft = true;

        reloadScene();
    }

    /// <summary>
    /// 네이티브에서 유니티 화면을 세로로 강제 회전
    /// </summary>
    /// <param name="str"></param>
    public void CallUnityLotationPortrait(string str)
    {
        Log("CallUnityLotationPortrait");
        Screen.orientation = ScreenOrientation.Portrait;
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;

        reloadScene();
    }

    /// <summary>
    /// 네이티브에서 유니티 Scene을 전환 요청
    /// </summary>
    /// <param name="sceneMode">Scene 이름</param>
    public void CallChangeScene(string sceneMode)
    {
        ChangeAceSceneMode(sceneMode);
    }

    /// <summary>
    /// 네이티브에서 UnityScene을 Default로 전환 요청
    /// </summary>
    /// <param name="strMsg">옵션</param>
    public void CallChangeSceneDefault(string strMsg)
    {
        //CallUnityLotationPortrait("");

        strJsonHomeUnit = null;
        ChangeAceSceneMode("Default");
    }

    /// <summary>
    /// 네이티브에서 UnityScene을 매트리스 분석 Scene으로 전환 요청
    /// </summary>
    /// <param name="strMsg">옵션</param>
    public void CallChangeSceneARMattress(string strMsg)
    {
        strJsonHomeUnit = null;
        ChangeAceSceneMode("ARMattress");
    }

    /// <summary>
    /// 네이티브에서 UnityScene을 매트리스 분석 Scene으로 전환 요청
    /// </summary>
    /// <param name="strMsg">옵션</param>
    public void CallChangeSceneMattress(string strMsg)
    {
        strJsonHomeUnit = null;
        Log("Mattress Scene mobile manager");
        ChangeAceSceneMode("Mattress");
    }

    /// <summary>
    /// 네이티브에서 UnityScene을 홈에디터 Scene으로 전환 요청
    /// </summary>
    /// <param name="strMsg">옵션</param>
    public void CallChangeSceneHomeEditor(string strMsg)
    {
        strJsonHomeUnit = null;
        if (strMsg != null)
        {
            Log("strMsg != null");
            strJsonHomeUnit = strMsg;
        }
        else
        {
            Log("strMsg == null");
        }

        ChangeAceSceneMode("HomeEditor");
    }

    /// <summary>
    /// [테스트용] 네이티브에서 UnityScene을 어반베이스의 홈에디터 Scene으로 전환 요청
    /// </summary>
    /// <param name="strMsg">옵션</param>
    public void CallChangeSceneUrbanHomeEditor(string strMsg)
    {
        strJsonHomeUnit = null;
        if (strMsg != null)
        {
            Log("strMsg != null");
            strJsonHomeUnit = strMsg;
        }
        else
        {
            Log("strMsg == null");
        }

        ChangeAceSceneMode("UrbanHomeEditor");
    }

    /// <summary>
    /// 네이티브에서 UnityScene을 어반베이스의 제품 미리보기 Scene으로 전환 요청
    /// </summary>
    /// <param name="strMsg">옵션</param>
    public void CallChangeSceneUrbanProductViewer(string strMsg)
    {
        ChangeAceSceneMode("UrbanProductViewer");
    }

    /// <summary>
    /// [테스트용] 사용하지 않음
    /// </summary>
    /// <param name="strMsg">옵션</param>
    public void CallChangeSceneModelTargetMenu()
    {
        ChangeAceSceneMode("ModelTargetMenu");
    }

    /// <summary>
    /// [테스트용] 사용하지 않음
    /// </summary>
    /// <param name="strMsg">옵션</param>
    public void CallChangeSceneModelTarget(string strMsg)
    {
        strModelTargetString = strMsg;
        ChangeAceSceneMode("ModelTarget");
    }

    /// <summary>
    /// [HomeEditorScene] 도면 불러오기 
    /// </summary>
    /// <param name="unitJson">Crane에 전달할 JsonString</param>
    public void LoadHomeUnit(string unitJson)
    {//
        Log("LoadHomeUnit ["+unitJson+"]");
        if (isHomeEditorScene())
        {
            RoomSceneUIController.Instance.LoadHomeUnit(unitJson);
        }

        CallShowToast("LoadHomeUnit success");
    }

    /// <summary>
    /// [HomeEditorScene] 제품 불러오기 
    /// </summary>
    /// <param name="unitJson">Crane에 전달할 JsonString</param>
    public void LoadHomeProduct(string productJson)
    {
        Log("LoadHomeProduct - " + productJson);
        if (isHomeEditorScene())
        {
            RoomSceneUIController.Instance.LoadHomeProduct(productJson);
        }

        CallShowToast("LoadHomeProduct success");
    }

    /// <summary>
    /// [HomeEditorScene] 벽지 불러오기 
    /// </summary>
    /// <param name="unitJson">Crane에 전달할 JsonString</param>
    public void ChangeWallPaperStyle(string productJson)
    {
        Log("ChangeWallPaperStyle - " + productJson);
        if (isHomeEditorScene())
        {
            RoomSceneUIController.Instance.ChangeUnitStyle(productJson);
        }

        CallShowToast("ChangeWallPaperStyle success");
    }

    /// <summary>
    /// [HomeEditorScene] 바닥재 불러오기 
    /// </summary>
    /// <param name="unitJson">Crane에 전달할 JsonString</param>
    public void ChangeFloorStyle(string productJson)
    {
        Log("ChangeFloorStyle - " + productJson);
        if (isHomeEditorScene())
        {
            RoomSceneUIController.Instance.ChangeFloorStyle(productJson);
        }

        CallShowToast("ChangeFloorStyle success");
    }

    /// <summary>
    /// [HomeEditorScene] Crane의 벽지/바닥제 편집모드 종료 
    /// </summary>
    public void StyleEditorEnd()
    {
        Log("StyleEditorEnd");
        if (isHomeEditorScene())
        {
            RoomSceneUIController.Instance.StopStyling();
        }

        CallShowToast("StyleEditorEnd success");
    }

    /// <summary>
    /// [HomeEditorScene] ㄱㅏㄱㅜ 불러오기 
    /// </summary>
    /// <param name="furnitureJson">Crane에 전달할 JsonString</param>
    public void LoadHomeFurniture(string furnitureJson)
    {
        Log("LoadHomeFurniture - " + furnitureJson);
        if (isHomeEditorScene())
        {
            RoomSceneUIController.Instance.LoadHomeFurniture(furnitureJson);
        }

        CallShowToast("LoadHomeFurniture success");
    }

    /// <summary>
    /// [HomeEditorScene] 도면/제품 모두 제거
    /// </summary>
    public void ClearHomeEditor()
    {
        Log("ClearHomeEditor");
        if (isHomeEditorScene())
        {
            RoomSceneUIController.Instance.ClearAll();
        }

        CallShowToast("ClearHomeEditor success");
    }

    #endregion

    #region Plugin 송신부
    /// <summary>
    /// [테스트용] 네이티브에 토스트 메세지 표시하도록 요청
    /// </summary>
    /// <param name="msg">표시할 메세지</param>
    public void CallShowToast(string msg)
    {
        Log("CallShowToast click");
        getMobilePlugin().CallShowToast(msg);
    }

    /// <summary>
    /// [테스트용] 실행 기기의 OS 버전 요청
    /// </summary>
    public void CallDeviceVersionCheck()
    {
        Log("CallDeviceVersionCheck click");
        getMobilePlugin().CallDeviceVersionCheck();
    }

    /// <summary>
    /// 현제 실행중인 Scene의 이름을 전달
    /// </summary>
    public void CallNowScene()
    {
        Log("CallNowScene");
        getMobilePlugin().CallNowScene();
    }

    /// <summary>
    /// 유니티에서 뒤로가기 버튼 이벤트를 네이티브로 전달
    /// </summary>
    public void CallBackButton()
    {
        Log("CallBackButton");
        getMobilePlugin().CallBackButton();
    }

    /// <summary>
    /// 튜토리얼 화면 호출
    /// </summary>
    public void CallTutorial()
    {
        Log("CallTutorial");
        getMobilePlugin().CallTutorial();
    }

    /// <summary>
    /// [홈에디터] 스크린샷을 촬영 후 이미지 저장 경로를 전달
    /// </summary>
    /// <param name="strImgPath">이미지 저장 경로</param>
    public void CallHomeEditorSave(string strImgPath)
    {
        Log("ScreenShot");
        getMobilePlugin().CallHomeEditorSave(strImgPath);
    }

    /// <summary>
    /// [홈에디터] 스크린샷을 촬영 후 다른 앱으로 공유하기 요청
    /// </summary>
    /// <param name="strImgPath">이미지 저장 경로</param>
    public void CallShare(string strImgPath)
    {
        Log("CallShare");
        getMobilePlugin().CallShare(strImgPath);
    }


    //Vuforia AR
    /// <summary>
    /// 메인 매뉴 호출
    /// </summary>
    /// <param name="oldSceneName"></param>
    public void CallMainMenu(string oldSceneName)
    {
        Log("CallMainMenu");
        if(getMobilePlugin() == null)
        {
            Log("mobilePlugin is Null!!");
        }
        else
        {
            Log("mobilePlugin is NOT Null!!");
        }
        getMobilePlugin().CallMainMenu(oldSceneName);
    }

    /* Home Editor 용 Plugin*/

    /// <summary>
    /// [홈에디터] 카달로그 화면 호출
    /// </summary>
    public void CallCatalog()
    {
        getMobilePlugin().CallCatalog();
    }

    /// <summary>
    /// [홈에디터] 도면 로딩 완료 후 콜백
    /// </summary>
    public void CallbackLoadHomeUnit()
    {
        getMobilePlugin().CallbackLoadHomeUnit();
    }

    /// <summary>
    /// [홈에디터] 제폼 로딩 완료 후 콜백
    /// </summary>
    public void CallbackLoadHomeProduct()
    {
        getMobilePlugin().CallbackLoadHomeProduct();
    }

    /// <summary>
    /// [홈에디터] 제폼(가구)로딩 완료 후 콜백
    /// </summary>
    public void CallbackLoadFurnitureComplete()
    {
        getMobilePlugin().CallbackLoadFurnitureComplete();
    }

    /// <summary>
    /// [홈에디터] 환결설정 화면 호출
    /// </summary>
    /// <param name="optionJson">환경설정에서 사용하는 옵션에 대한 JSON</param>
    public void CallHomeOption(string optionJson)
    {
        getMobilePlugin().CallHomeOption(optionJson);
    }

    /// <summary>
    /// [테스트용] 플러그인 동작 확인용 ecco plugin
    /// 네이티브와 유니티 양쪽에 로그가 남아야 함
    /// </summary>
    /// <param name="testMsg">로그로 남길 메세지</param>
    public void CallTest(string testMsg)
    {
        getMobilePlugin().CallTest(testMsg);
    }

    /// <summary>
    /// [홈에디터] 홈에디터 옵션 적용
    /// </summary>
    /// <param name="productJson">환경설정에서 사용하는 옵션에 대한 JSON</param>
    public void LoadHomeOption(string productJson)
    {
        Log("LoadHomeOption");
        if (isHomeEditorScene())
        {
            RoomSceneUIController.Instance.LoadHomeProduct(productJson);
        }

        CallShowToast("LoadHomeOption success");
    }

    /// <summary>
    /// [홈에디터] 옵샨 중 사양 옵션만 변경
    /// </summary>
    /// <param name="level">사양 코드(0 = 낮음, 2 = 높음)</param>
    public void LoadHomeOptionEffect(string level)
    {
        Log("LoadHomeOption");
        if (isHomeEditorScene())
        {
            RoomSceneUIController.Instance.OptionQuality(int.Parse(level));
        }

        CallShowToast("LoadHomeOption success");
    }

    /// <summary>
    /// [홈에디터] 옵샨 중 제품과 도면 벽면의 충돌 처리 여부
    /// </summary>
    /// <param name="stringBool">"true"인 경우 제품은 벽을 뚫을 수 없음.</param>
    public void LoadHomeOptionCollision(string stringBool)
    {
        Log("LoadHomeOption");
        if (isHomeEditorScene())
        {
            if ("true".Equals(stringBool))
            {
                RoomSceneUIController.Instance.OptionCollision(true);
            }
            else
            {
                RoomSceneUIController.Instance.OptionCollision(false);
            }

        }

        CallShowToast("LoadHomeOption success");
    }

    // Product Preview (어반베이스)에서 사용하는 메소드
    public void CallbackLoadProduct(string stringMsg)
    {
        Log("Callback ProductViewer Scene load is " + stringMsg);
        getMobilePlugin().CallbackLoadProductView(stringMsg);
    }

    // Product Preview (어반베이스)에서 사용하는 메소드
    public void CallbackLoadProductViewerScene(string stringMsg)
    {
        Log("Callback read productlist is " + stringMsg);
        getMobilePlugin().CallbackLoadProductViewerScene(stringMsg);
    }

    /// <summary>
    /// [iOS] 장비에서 노치 존재 유무
    /// </summary>
    /// <param name="strIsNotch"></param>
    public void setIsNotch(string strIsNotch)
    {
        if (strIsNotch.Equals("true"))
        {
            isNotch = true;
        }
        else
        {
            isNotch = false;
        }
    }

    //TEST
    public void CallEcco(string productJson)
    {
        Log("CallEcco - " + productJson);
    }


    /// <summary>
    /// [Adbrix] Custom Action용
    /// 네이티브와 유니티 양쪽에 로그가 남아야 함
    /// </summary>
    /// <param name="testMsg">로그로 남길 메세지</param>
    public void CallAdbrixCustomAction(string action)
    {
        getMobilePlugin().CallAdbrixCustomAction(action);
    }

    #endregion

}
