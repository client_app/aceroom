﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DLLCore;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;
using System.Runtime.InteropServices;

/// <summary>
/// [어반베이스] Crane 용 컴포넌트
/// 수정 시 어반베이스에 문의 필요.
/// </summary>
public class UIController : MonoBehaviour
{
    //protected UIController() { } // guarantee this will be always a singleton only - can't use the constructor!{

    static public string BASE_PATH = "https://s3.ap-northeast-2.amazonaws.com/acebed-3d-assets-dev";

    #region TestFunction
    public Button loadUnitA;
    public Button loadUnitB;
    public Button loadUnitC;

    public Button calcDistance;
    public Button endCalcDistance;
    public Button calcArea;
    public Button endCalcArea;

    public Button loadProduct;
    public Button loadProduct2;
    public Button loadProduct3;

    public Text lightDirectionText;
    public Slider lightDirection;

    public Button clearUnit;

    public Button requestMapAPIBtn;
    public InputField inputFieldSearchAddress;
    public Button pinButton;

    public GameObject joystickCotrol;
    public GameObject viewRotationControls;
    bool _fpsViewMode = false;
    #endregion

    public GameObject topPanel;
    public GameObject rightMenuPanel;

    public Button backToMain;

    // function
    public Button screenShot;
    public Button alignment;
    public Button undo;
    public Button redo;
    public Button cameraModePanning;
    public Button cameraModeOrbit;
    public Button settings;
    public Toggle fpsModeToggle;
    public Toggle orbitModeToggle;
    public Button stopStyling;

    public GameObject canvas;

    public ProductScrollList productScrollList;
    public ProductListButton productListButton;

    public Button showItemList;
    public GameObject itemListPanel;
    public Button hideItemList;

    public Toggle tabBed;
    public Toggle tabWall;
    public Toggle tabFloor;

    // wallpaper, floor
    public GameObject wallpaperListPanel;
    public GameObject floorListPanel;
    public Button[] wallpaperButtons;
    public Button[] floorButtons;


    bool _panningMode = false;

    class OptionData
    {
        public string name;
        public string[] sizeOption;
        public string[] colorOption;
        public string filter;
    }
    List<OptionData> _listOptionCombination;

    class FrameData
    {
        public string frameID;
        public string frameName;
        public string frameSize;
        public string frameColor;
        public string path;
        public string thumbnail;
    }
    List<FrameData> _frameProductList;

    //Dictionary<string, string> _frameMap;
    Dictionary<string, string> _mattressProductList;
    Dictionary<string, string> _mattressPathMap;
    Dictionary<string, string[]> _mattressListSize;

    public ProductOptionUIManager productOptionUIManager;

    public SettingPanelController settingPanelController;

    // wall list, floor list
    Dictionary<string, string> _wallpaperJsonMap;
    Dictionary<string, string> _floorJsonMap;
    List<string> _wallpaperIdList;
    List<string> _floorIdList;

    class ProductJsonInfo
    {
        public string frameId;
        public string name;
        public string size;
        public string color;
        public string framePath;
        public string thumbnailPath;
        public string catalogThumbnail;
    }
    List<ProductJsonInfo> _productJsonList = new List<ProductJsonInfo>();

    public GameObject[] _pointLightsForUnit;
    public GameObject pointLightPrefab;

#if UNITY_ANDROID
    //Android
    private AndroidJavaObject UnityPlayerActivity = null;
    private AndroidJavaClass AcePlugin = null;
    private AndroidJavaObject AcePluginInstance = null;
#elif UNITY_IPHONE
    [DllImport("__Internal")]
    private static extern string _iOSPluginCallMainMenu();
#endif

    public void Log(string message)
    {
        Debug.Log("[Unity] " + message, this);
    }

//    void Awake()
//    {
//        Log("DefautMainCamera Awake");

//#if UNITY_ANDROID
//            if (Application.platform == RuntimePlatform.Android)
//            {
//                Log("DefautMainCamera Android");
//                if (UnityPlayerActivity == null)
//                {
//                    //일단 아까 plugin의 context를 설정해주기 위해
//                    //유니티 자체의 UnityPlayerActivity를 가져옵시다.
//                    using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
//                    {
//                        UnityPlayerActivity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
//                        if (UnityPlayerActivity != null)
//                        {
//                            UnityPlayerActivity.CallStatic("CallMainActivity", UnityPlayerActivity, "UrbanHomeEditor");
//                        }

//                    }
//                }

//            }
//#elif UNITY_IPHONE
//                        if (Application.platform == RuntimePlatform.IPhonePlayer)
//                        {
//                            //nothing...
//                            //_iOSPluginCallMainMenu();
//                            getMobileManager().CallMainMenu();

//                        }
//            getMobileManager().CallMainMenu();
//#endif

//            //TODO: Android 확인 필요
//            //getMobileManager().CallMainMenu();
        
//    }

    // Start is called before the first frame update
    void Start()
    {
        //#region TestFunction
        // for test function
        loadUnitA.onClick.AddListener(() => { OnClickLoadUnitA(); });
        
        loadUnitB.onClick.AddListener(() => { OnClickLoadUnitB(); });
        loadUnitC.onClick.AddListener(() => { OnClickLoadUnitC(); });

        calcDistance.onClick.AddListener(() => { OnClickCalcDistance(); });
        endCalcDistance.onClick.AddListener(() => { OnClickEndCalcDistance(); });
        calcArea.onClick.AddListener(() => { OnClickCalcArea(); });
        endCalcArea.onClick.AddListener(() => { OnClickEndCalcArea(); });

        loadProduct.onClick.AddListener(() => { OnClickLoadProduct(); });
        /*
        loadProduct2.onClick.AddListener(() => { OnClickLoadProduct2(); });
        loadProduct3.onClick.AddListener(() => { OnClickLoadProduct3(); });
        lightDirection.onValueChanged.AddListener(delegate { OnValueChangedLightDirection(lightDirection.value); } );

        clearUnit.onClick.AddListener(() => { OnClickClearUnit(); });

        requestMapAPIBtn.onClick.AddListener(() => { OnClickRequestMapAPI(inputFieldSearchAddress.text); });
        inputFieldSearchAddress.onEndEdit.AddListener((address) => { OnClickRequestMapAPI(address); });
        #endregion

        backToMain.onClick.AddListener(() => { OnClickBackToMain(); });

        screenShot.onClick.AddListener(() => { OnClickScreenShot(); });
        alignment.onClick.AddListener(() => { OnClickAlignment(); });
        undo.onClick.AddListener(() => { OnClickUndo(); });
        redo.onClick.AddListener(() => { OnClickRedo(); });
        cameraModePanning.onClick.AddListener(() => { OnClickCameraModePanning(_panningMode); });
        cameraModeOrbit.onClick.AddListener(() => { OnClickCameraModePanning(_panningMode); });
        settings.onClick.AddListener(() => { settingPanelController.gameObject.SetActive(true);  });

        fpsModeToggle.onValueChanged.AddListener((value) => OnClickViewModeToggle(!value));
        orbitModeToggle.onValueChanged.AddListener((value) => OnClickViewModeToggle(value));
        stopStyling.onClick.AddListener(() => OnClickStopStyling());

        showItemList.onClick.AddListener(() => { OnClickShowItemList(true); });
        hideItemList.onClick.AddListener(() => { OnClickShowItemList(false); });

        tabBed.onValueChanged.AddListener((value) => { OnToggleBed(value); });
        tabFloor.onValueChanged.AddListener((value) => OnToggleFloor(value));
        tabWall.onValueChanged.AddListener((value) => OnToggleWallpaper(value));

        wallpaperButtons[0].onClick.AddListener(() => OnClickWallpaper(0));
        wallpaperButtons[1].onClick.AddListener(() => OnClickWallpaper(1));
        wallpaperButtons[2].onClick.AddListener(() => OnClickWallpaper(2));
        wallpaperButtons[3].onClick.AddListener(() => OnClickWallpaper(3));
        wallpaperButtons[4].onClick.AddListener(() => OnClickWallpaper(4));
        wallpaperButtons[5].onClick.AddListener(() => OnClickWallpaper(5));
        wallpaperButtons[6].onClick.AddListener(() => OnClickWallpaper(6));
        floorButtons[0].onClick.AddListener(() => OnClickFloor(0));
        floorButtons[1].onClick.AddListener(() => OnClickFloor(1));
        floorButtons[2].onClick.AddListener(() => OnClickFloor(2));
        floorButtons[3].onClick.AddListener(() => OnClickFloor(3));
        floorButtons[4].onClick.AddListener(() => OnClickFloor(4));
        floorButtons[5].onClick.AddListener(() => OnClickFloor(5));

        if (Main.Instance != null && Main.Instance.graphicSetting != null)
            Main.Instance.graphicSetting.SetMenuScaleFactor(10);

        _listOptionCombination = new List<OptionData>();
        _frameProductList = new List<FrameData>();
        //_frameMap = new Dictionary<string, string>();
        _mattressProductList = new Dictionary<string, string>();
        _mattressPathMap = new Dictionary<string, string>();
        _mattressListSize = new Dictionary<string, string[]>();

        _wallpaperJsonMap = new Dictionary<string, string>();
        _floorJsonMap = new Dictionary<string, string>();
        _wallpaperIdList = new List<string>();
        _floorIdList = new List<string>();


        joystickCotrol.SetActive(false);
        viewRotationControls.SetActive(false);
        */

    }

    private void OnDestroy()
    {
        _listOptionCombination.Clear();
        _frameProductList.Clear();
        _mattressProductList.Clear();
        _mattressPathMap.Clear();
        _mattressListSize.Clear();

        _wallpaperJsonMap.Clear();
        _floorJsonMap.Clear();
        _wallpaperIdList.Clear();
        _floorIdList.Clear();
    }

    // FPS, 3D View Mode toggle button
    void OnClickViewModeToggle(bool value)
    {
        if (value)
        {
            JSONObject jsonViewMode = new JSONObject();
            jsonViewMode.AddField("mode", "observer");
            jsonViewMode.AddField("animation_time", 10);
            Main.Instance.generalSetting.SetViewMode(jsonViewMode.ToString(), ()=> { Debug.Log("view mode toggle end"); });

            viewRotationControls.SetActive(false);
            joystickCotrol.SetActive(false);
            //joystickCotrol.GetComponentInChildren<Image>().enabled = false;
            _fpsViewMode = false;
            Main.Instance.playerController.SetDualTouchPad(false);

            // outline effect
            Main.Instance.productController.EnableOutlineEffect();

            // hide Right menu panel
            rightMenuPanel.SetActive(true);

            // test
            //Main.Instance.unitController.SetTextureBasePath("https://s3.ap-northeast-2.amazonaws.com/acebed-3d-assets-dev/");
        }
        else
        {
            JSONObject jsonViewMode = new JSONObject();
            jsonViewMode.AddField("mode", "player");
            jsonViewMode.AddField("animation_time", 10);
            Main.Instance.generalSetting.SetViewMode(jsonViewMode.ToString(), () => { Debug.Log("view mode toggle end"); });

            viewRotationControls.SetActive(true);
            joystickCotrol.SetActive(true);
            //joystickCotrol.GetComponentInChildren<Image>().enabled = true;

            _fpsViewMode = true;
            // disable camera rotation
            Main.Instance.playerController.SetDualTouchPad(true);

            // unselect product
            foreach (Product prd in Main.Instance.productController.store.selectedProducts)
                prd.Unselect();

            // disable outline effect
            Main.Instance.productController.DisableOutlineEffect();

            // hide Right menu panel
            rightMenuPanel.SetActive(false);

            // stop wall floor paint 
            Main.Instance.unitController.StopStyling();

            stopStyling.gameObject.SetActive(false);
        }
    }

    // show or hide frame list
    void OnToggleBed(bool value)
    {
        productScrollList.gameObject.SetActive(value);
    }

    // show or hide floor item list
    void OnToggleFloor(bool value)
    {
        floorListPanel.SetActive(value);

    }

    // show or hide wallpaper item list
    void OnToggleWallpaper(bool value)
    {
        wallpaperListPanel.SetActive(value);

    }
    
    void OnClickWallpaper(int index)
    {
        Debug.Log("select wallpaper " + index);

        // FPS mode, don't start styling
        if(!_fpsViewMode)
        {
            // start styling with this material
            // find button index to material id
            string materialID = _wallpaperIdList[index];
            JSONObject materialJson = new JSONObject(_wallpaperJsonMap[materialID]);
            JSONObject sendMaterialJson = new JSONObject();
            sendMaterialJson.AddField("material", materialJson);
            Main.Instance.unitController.StartStyling(sendMaterialJson.ToString());

            stopStyling.gameObject.SetActive(true);
        }

        // hide itemlist
        OnClickShowItemList(false);
    }

    void OnClickFloor(int index)
    {
        Debug.Log("select floor " + index);
        if (!_fpsViewMode)
        {
            string materialID = _floorIdList[index];
            JSONObject materialJson = new JSONObject(_floorJsonMap[materialID]);
            JSONObject sendMaterialJson = new JSONObject();
            sendMaterialJson.AddField("material", materialJson);
            Main.Instance.unitController.StartStyling(sendMaterialJson.ToString());

            stopStyling.gameObject.SetActive(true);
        }
        
        // hide itemlist
        OnClickShowItemList(false);
    }

    // stop painting
    public void OnClickStopStyling()
    {
        Main.Instance.unitController.StopStyling();
        stopStyling.gameObject.SetActive(false);
        Main.Instance.productController.EnableOutlineEffect();
    }

    public void RequestInitialProductData(bool createUIList = true)
    {
        // frame and mattress json 
        RequestProductList(createUIList);

        // wallpaper and floor json
        RequestWallFloorList();

    }

    public void ProductViewerMode()
    {
        Main.Instance.generalSetting.SetSDKMode("productview");

        SetCanvasMode(true);
        RequestInitialProductData(false);
    }

    public void HomeDesignMode()
    {
        Main.Instance.generalSetting.SetSDKMode("homedesign");

        SetCanvasMode(false);
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //    Application.Quit();

        if (_fpsViewMode)
        {
            float horizontalAxis = CrossPlatformInputManager.GetAxis("Horizontal");
            float verticalAxis = CrossPlatformInputManager.GetAxis("Vertical");

            float moveX = CrossPlatformInputManager.GetAxis("Mouse X");
            float moveY = CrossPlatformInputManager.GetAxis("Mouse Y");

            //Debug.Log($"virtual axis {horizontalAxis}, {verticalAxis}, {moveX}, {moveY}");
            Main.Instance.playerController.SetPlayerMoveAndRotate(horizontalAxis, verticalAxis, moveX, moveY);
        }
    }

    public void OnClickShowItemList(bool show)
    {
        itemListPanel.SetActive(show);
        if (show)
        {
            //tabBed.isOn = true;
            //tabBed.Select();
            //tabBed.onValueChanged.Invoke(true);
            //itemListPanel.GetComponent<ToggleGroup>().NotifyToggleOn(tabBed);
            ExecuteEvents.Execute(tabBed.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);

            RequestInitialProductData(true);
            
        }
        floorListPanel.SetActive(false);
        wallpaperListPanel.SetActive(false);

        // disable orbit control
        // enable orbit control
        if (Main.Instance.observerController)
            Main.Instance.observerController.GetComponent<OrbitControls>().enabled = !show;
    }

    void OnClickLoadUnitA()
    {
        //_LoadUnit(BASE_PATH + "/unit/5600c1c271da53ab8ad712e9.dae", "dae");
        
        //_LoadUnit("https://s3.ap-northeast-2.amazonaws.com/acebed-3d-assets-dev/unit/5600c1c271da53ab8ad712e9.dae", "dae");

        //_LoadUnit("http://dtribe.youyoung.net/sampleUnit/06721e4f-6f41-4c99-8b7d-881cc079fb86.dae", "dae");
        _LoadUnit("http://dtribe.youyoung.net/sampleUnit/06721e4f-6f41-4c99-8b7d-881cc079fb86.dae", "dae");

    }

    void OnClickLoadUnitB()
    {
        _LoadUnit(BASE_PATH + "/unit/5600c1c271da53ab8ad71300.dae", "dae");
    }

    void OnClickLoadUnitC()
    {
        _LoadUnit(BASE_PATH + "/unit/5600c1bc71da53ab8ad68263.dae", "dae");
    }

    void OnClickClearUnit()
    {
        GameObject[] units = GameObject.FindGameObjectsWithTag("Unit");
        foreach(GameObject unit in units)
        {
            GameObject.Destroy(unit);
        }
        // remove product
        Main.Instance.productController.store.DestroyAll();
    }

    void OnClickLoadProduct()
    {
        //"https://s3.ap-northeast-2.amazonaws.com/ub-3d-assets-dev/product/test_chair.dae"
        //lunato3_low_origin.zip
        //lamer2_low
        //1563440950241.zip
        //FrameAndMattress
        JSONObject productJSONObj = new JSONObject();
    
        productJSONObj.AddField("url", "http://dtribe.youyoung.net/product/ARNO_K_SaddleBrown.zip");
        productJSONObj.AddField("format", ".zip"); //dae
        productJSONObj.AddField("planeType", "floor");
        Main.Instance.productController.Load(productJSONObj.ToString(), (GameObject product) => { Debug.Log("load done"); });
    }

    void OnClickLoadProduct2()
    {
        JSONObject productJSONObj = new JSONObject();
        productJSONObj.AddField("url", "https://s3.ap-northeast-2.amazonaws.com/ub-3d-assets-dev/product/test_chair.dae");
        productJSONObj.AddField("format", "dae");
        productJSONObj.AddField("planeType", "floor");
        Main.Instance.productController.Load(productJSONObj.ToString(), (GameObject product) => { Debug.Log("load done"); });
    }

    void OnClickLoadProduct3()
    {

    }

    // change directional light 
    void OnValueChangedLightDirection(float value)
    {
        // value (0. ~ 1.) => hour (1 ~ 24)
        int hour = (int)(value * 23) + 1;
        Main.Instance.daylightController.SetHour(hour);
    }

    void _LoadUnit(string uri, string format = "dae")
    {
        HomeDesignMode();
        //Main.Instance.generalSetting.SetSDKMode("homedesign");

        JSONObject unitJSONObj = new JSONObject();
        unitJSONObj.AddField("url", uri);
        unitJSONObj.AddField("format", format);
        Debug.Log("[uicont] loading state :" + Main.Instance.unitController.GetLoadingState());
        if (Main.Instance.unitController.GetLoadingState() == 0)
            Main.Instance.unitController.Load(unitJSONObj.ToString(), delegate (string message) {
                Debug.Log(message);

                // add 4 points light
                //var unit = Main.Instance.unitController.store.unit;
                //var unitExtents = unit.gameObject.RecalculateBounds().extents;
                //_InstantiatePointLight(unitExtents, unit.gameObject);
            });
    }

    void _InstantiatePointLight(Vector3 factor, GameObject unit)
    {
        if (_pointLightsForUnit != null && _pointLightsForUnit.Length == 4)
        {
            for (int i = 0; i < 4; ++i)
            {
                _pointLightsForUnit[i] = Instantiate(pointLightPrefab, unit.transform);
                if (_pointLightsForUnit[i])
                {
                    float xSign = 1;
                    float zSign = 1;
                    if (i / 2 == 0)
                    {
                        if (i % 2 == 0)
                        {
                            xSign = -1f;
                            zSign = -1f;
                        }
                        else
                        {
                            xSign = -1f;
                            zSign = 1f;
                        }
                    }
                    else
                    {
                        if (i % 2 == 0)
                        {
                            xSign = 1f;
                            zSign = -1f;
                        }
                        else
                        {
                            xSign = 1f;
                            zSign = 1f;
                        }
                    }
                    _pointLightsForUnit[i].transform.position = new Vector3(factor.x * xSign, factor.y * 3f, factor.z * zSign);
                }
            }
        }
    }

    void OnClickCalcDistance()
    {
        if (Main.Instance.toolController == null)
            return;
        Main.Instance.toolController.StartMeasureDistance();
        endCalcDistance.gameObject.SetActive(true);
    }

    void OnClickEndCalcDistance()
    {
        Main.Instance.toolController.StopMeasureDistance();
        endCalcDistance.gameObject.SetActive(false);
    }

    void OnClickCalcArea()
    {
        if (Main.Instance.toolController == null)
            return;
        Main.Instance.toolController.StartMeasureArea();
        endCalcArea.gameObject.SetActive(true);
    }

    void OnClickEndCalcArea()
    {
        Main.Instance.toolController.StopMeasureArea();
        endCalcArea.gameObject.SetActive(false);
    }

    void OnClickScreenShot()
    {
        //int shadowType = Main.Instance.graphicSetting.GetShadowType();
        //int vsync = Main.Instance.graphicSetting.GetVSync();
        //bool wallCollider = Main.Instance.unitController.GetStatusWallCollider();
        //Debug.Log($"shadow {shadowType}, vsync {vsync}, wall {wallCollider}");

        StartCoroutine(ScreenShot());
    }

    IEnumerator ScreenShot()
    {
        // hide ui 
        canvas.SetActive(false);

        yield return new WaitForEndOfFrame();

        Main.Instance.toolController.ScreenShot((path)=>{ Debug.Log("screenshot path:" + path); });

        yield return new WaitForEndOfFrame();
        // show ui
        canvas.SetActive(true);
    }

    void OnClickAlignment()
    {
        JSONObject jsonObj = new JSONObject();
        jsonObj.AddField("direction", "init");
        jsonObj.AddField("isOrtho", false);

        if (Main.Instance.observerController)
            Main.Instance.observerController.SetPresetView(jsonObj.ToString());
    }

    void OnClickUndo()
    {
        History.Instance.Undo();
    }

    void OnClickRedo()
    {
        History.Instance.Redo();
    }

    void OnClickCameraModePanning(bool mode)
    {
        _panningMode = !mode;
        if (Main.Instance.observerController)
            Main.Instance.observerController.SetPanningMode(_panningMode);

        cameraModePanning.gameObject.SetActive(!_panningMode);
        cameraModeOrbit.gameObject.SetActive(_panningMode);
    }

    /*void OnClickCameraModeOrbit()
    {
        Debug.Log("set pannning mode off");
        if (Main.Instance.observerController)
            Main.Instance.observerController.SetPanningMode(false);

        cameraModeOrbit.gameObject.SetActive(false);
        cameraModePanning.gameObject.SetActive(true);
    }*/

    void OnClickBackToMain()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("DefaultScene", UnityEngine.SceneManagement.LoadSceneMode.Single);
        //UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu", UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    void OnClickRequestMapAPI(string address)
    {
        // buildings API
        // https://apis.urbanbase.com/dev2/buildings?bounds=[34.93398534446254,127.48514528373266,34.94151406025863,127.49927516082312]&include={"country": {}, "building_areas": {"include":{"units": {"include": {}}}}}
        StartCoroutine(GetParseFromJSON("https://apis.urbanbase.com/dev2/buildings?bounds=[34.93398534446254,127.48514528373266,34.94151406025863,127.49927516082312]&include={\"country\": {}, \"building_areas\": {\"include\":{\"units\": {\"include\": {}}}}}", ParseBuildings));

        // units API
        // https://apis.urbanbase.com/dev2/units/6823e20f-e391-4d45-a569-e89ce41310f8?include={"unit_attaches": {},"building_area": {"include":{"building":{} }}}
        StartCoroutine(GetParseFromJSON("https://apis.urbanbase.com/dev2/units/6823e20f-e391-4d45-a569-e89ce41310f8?include={\"unit_attaches\": {},\"building_area\": {\"include\":{\"building\":{} }}}", ParseUnits));

        
    }

    void RequestProductList(bool createUIList)
    {
        //StartCoroutine(GetParseFromJSONAndCallback(BASE_PATH + "/json/productList_basePath.json", ParseProductList, createUIList));
        StartCoroutine(GetParseFromJSONAndCallback(BASE_PATH + "/json/productList.json", ParseProductList, createUIList));
    }

    void RequestWallFloorList()
    {
        StartCoroutine(GetParseFromJSON(BASE_PATH + "/json/wallfloorList.json", ParseWallFloorList));
    }

    delegate void ParseFunction(string json);
    delegate void ParseFunctionWithPameter(string json, bool createUI);

    IEnumerator GetParseFromJSON(string url, ParseFunction callback)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Debug.Log(url);
        }
        else
        {
            string productListJson = ((DownloadHandler)www.downloadHandler).text;
            callback(productListJson);
        }
    }

    IEnumerator GetParseFromJSONAndCallback(string url, ParseFunctionWithPameter callback, bool createUI)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            string productListJson = ((DownloadHandler)www.downloadHandler).text;
            callback(productListJson, createUI);
        }
    }

    void ParseProductList(string productListJson, bool createUIList = true)
    {
        //Debug.Log("parse product list with create ui: " + createUIList);
        productListButton.ClearListButton();
        _frameProductList.Clear();
        _productJsonList.Clear();
        _listOptionCombination.Clear();

        JSONObject jsonProductList = new JSONObject(productListJson);
        if (!jsonProductList.HasField("products"))
        {
            Debug.LogError("Old JSON file");
            return;
        }

        string basePath = jsonProductList.GetField("basePath").str;

        // products
        JSONObject productList = jsonProductList.GetField("products");
        for(int i = 0; i < productList.Count; ++i)
        {
            JSONObject product = productList[i];
            // name
            string name = product.GetField("name").str;
            // catalog thumbnail
            string catalogThumbnail = basePath + product.GetField("catalogThumbnail").str;
            // descList
            // catalogName
            string catalogName = product.GetField("catalogName").str;
            // filter
            string filter = product.GetField("filter").str;
            // size color option
            OptionData optionData = new OptionData
            {
                name = name,
                filter = filter
            };
            // sizeList
            JSONObject sizeList = product.GetField("sizeList");
            optionData.sizeOption = new string[sizeList.Count];
            for (int j = 0; j < sizeList.Count; ++j)
            {
                optionData.sizeOption[j] = sizeList[j].str;
            }
            // colorList
            JSONObject colorList = product.GetField("colorList");
            optionData.colorOption = new string[colorList.Count];
            for (int j = 0; j < colorList.Count; ++j)
            {
                //string colorName = colorList[j].GetField("name").str;
                //string colorPath = colorList[j].GetField("path").str;
                //string colorRgb = colorList[j].GetField("rgb").str;
                optionData.colorOption[j] = colorList[j].GetField("name").str;
            }
            _listOptionCombination.Add(optionData);

            // baseOpt
            JSONObject baseOpt = product.GetField("baseOpt");
            string defaultSize = baseOpt.GetField("size").str;
            string defaultColor = baseOpt.GetField("color").str;
            string defaultMattress = baseOpt.GetField("mattress").str;
            
            // frameList
            JSONObject frameList = product.GetField("frameList");
            for(int j = 0; j < frameList.Count; ++j)
            {
                JSONObject frame = frameList[j];
                //frameid
                string frameId = frame.GetField("frameId").str;
                //size
                string frameSize = frame.GetField("size").str;
                //color
                string frameColor = frame.GetField("color").str;
                //thumbnail
                string frameThumbnailPath = basePath + frame.GetField("thumbnail").str;
                //path
                string framePath = basePath + frame.GetField("path").str;
                //sfbpath
                //daeipath
                //isdisplay
                bool isDisplay = frame.GetField("isDisplay").b;
                //mattressoffset
                //mattressscale

                if (createUIList && isDisplay)
                {
                    if (productScrollList.filter.value == 0)
                        productListButton.InstantiateProductButton(frameId, name, frameSize, frameColor, framePath, frameThumbnailPath, catalogThumbnail);
                    else if (productScrollList.filter.value == 1 && GetFilterOption(name) == ProductScrollList.FILTER_WEDDING)
                        productListButton.InstantiateProductButton(frameId, name, frameSize, frameColor, framePath, frameThumbnailPath, catalogThumbnail);
                    else if (productScrollList.filter.value == 2 && GetFilterOption(name) == ProductScrollList.FILTER_SUPERSINGLE)
                        productListButton.InstantiateProductButton(frameId, name, frameSize, frameColor, framePath, frameThumbnailPath, catalogThumbnail);
                    else if (productScrollList.filter.value == 3 && GetFilterOption(name) == ProductScrollList.FILTER_FAMILY)
                        productListButton.InstantiateProductButton(frameId, name, frameSize, frameColor, framePath, frameThumbnailPath, catalogThumbnail);
                }

                if (isDisplay)
                    _productJsonList.Add(new ProductJsonInfo() { frameId = frameId, name = name, size = frameSize, color = frameColor, framePath = framePath, thumbnailPath = frameThumbnailPath, catalogThumbnail = catalogThumbnail });

                FrameData fData = new FrameData()
                {
                    frameID = frameId,
                    frameName = name,
                    frameColor = frameColor,
                    frameSize = frameSize,
                    path = framePath,
                    thumbnail = frameThumbnailPath
                };
                _frameProductList.Add(fData);
            }
        }
        

        // mattress
        _mattressProductList.Clear();
        _mattressPathMap.Clear();
        _mattressListSize.Clear();
        List<string> mattressSS = new List<string>();
        List<string> mattressLQ = new List<string>();
        List<string> mattressK = new List<string>();
        List<string> mattressTWIN = new List<string>();
        List<string> mattressFAMILY = new List<string>();

        JSONObject mattressList = jsonProductList.GetField("mattressList");
        for (int i = 0; i < mattressList.Count; ++i)
        {
            JSONObject mattress = mattressList[i];
            string mattressid = mattress.GetField("mattressid").str;
            string path = basePath + mattress.GetField("path").str;
            string name = mattress.GetField("name").str;
            string size = mattress.GetField("size").str;

            _mattressProductList.Add(mattressid, path);

            _mattressPathMap.Add(name + "_" + size, path);
            if (size == "SS")
                mattressSS.Add(name);
            else if (size == "LQ")
                mattressLQ.Add(name);
            else if (size == "K")
                mattressK.Add(name);
            else if (size == "TWIN")
                mattressTWIN.Add(name);
            else if (size == "FAMILY")
                mattressFAMILY.Add(name);
        }

        _mattressListSize.Add("SS", mattressSS.ToArray());
        _mattressListSize.Add("LQ", mattressLQ.ToArray());
        _mattressListSize.Add("K", mattressK.ToArray());
        _mattressListSize.Add("TWIN", mattressTWIN.ToArray());
        _mattressListSize.Add("FAMILY", mattressFAMILY.ToArray());

    }

    // load Wallpaper and Floor material JSON 
    void ParseWallFloorList(string wallFloorListJson)
    {
        // read wallpaper list
        _wallpaperJsonMap.Clear();
        _wallpaperIdList.Clear();

        JSONObject jsonWallFloor = new JSONObject(wallFloorListJson);
        JSONObject wallList = jsonWallFloor.GetField("wallpaperList");
        for(int i = 0; i < wallList.Count; ++i)
        {
            // wall material json with meta
            JSONObject materialWithMeta = wallList[i];
            string id = materialWithMeta.GetField("id").str;
            _wallpaperJsonMap.Add(id, materialWithMeta.ToString());
            _wallpaperIdList.Add(id);
        }

        // lead floor list
        _floorJsonMap.Clear();
        _floorIdList.Clear();
        JSONObject floorList = jsonWallFloor.GetField("floorList");
        for(int i = 0; i < floorList.Count; ++i)
        {
            // floor material json with meta
            JSONObject materialWithMeta = floorList[i];
            string id = materialWithMeta.GetField("id").str;
            _floorJsonMap.Add(id, materialWithMeta.ToString());
            _floorIdList.Add(id);
        }

        // if select wallpaper or floor from UI list
        // then pass json to Core
    }

    void ParseBuildings(string buildingsJson)
    {
        JSONObject building = new JSONObject(buildingsJson);

        if (building.HasField("message"))
        {
            // message is success
            if (building.GetField("message").str == "success")
            {
                JSONObject data = building.GetField("data");
                if (data.HasField("buildings"))
                {
                    JSONObject buildings = data.GetField("buildings");
                    if (buildings.Count > 0)
                    {
                        string buildingName = buildings[0].GetField("building_name").str;
                        float latitude = buildings[0].GetField("latitude").f;
                        float longitude = buildings[0].GetField("longitude").f;
                        string roadAddress = buildings[0].GetField("road_address").str;
                        Debug.Log($"{buildingName}, lat {latitude}, lon {longitude}, address {roadAddress}");
                        // area
                        JSONObject buildingArea = buildings[0].GetField("building_areas");
                        for (int i = 0; i < buildingArea.Count; ++i)
                        {
                            float supplyArea = buildingArea[i].GetField("supply_area").f;
                            float exclusiveArea = buildingArea[i].GetField("exclusive_area").f;
                            Debug.Log($"supply area {supplyArea}, exclusive {exclusiveArea}");

                            JSONObject units = buildingArea[i].GetField("units");
                            // unit
                            for(int j = 0; j < units.Count; ++j)
                            {
                                int roomCount = (int)units[j].GetField("room_count").i;
                                int bathroomCount = (int)units[j].GetField("bathroom_count").i;

                                string topView = units[j].GetField("top_view_image_path").str;
                                string unit_uuid = units[j].GetField("unit_uuid").str;

                                Debug.Log($"units {j} unit uuid {unit_uuid}, top view {topView}");
                            }
                        }
                    }
                }
            }
        }
                
    }

    void ParseUnits(string unitsJson)
    {
        JSONObject units = new JSONObject(unitsJson);
        if (units.HasField("message"))
        {
            if (units.GetField("message").str == "success")
            {
                JSONObject data = units.GetField("data");
                JSONObject unitAttaches = data.GetField("unit_attaches");
                string unit_uuid = data.GetField("unit_uuid").str;

                if (unitAttaches.Count > 0)
                {
                    for(int i = 0; i < unitAttaches.Count; ++i)
                    {
                        string filePath = unitAttaches[i].GetField("file_path").str;
                        Debug.Log($"unitAttaches {i} uuid {unit_uuid}, file {filePath}");
                    }
                }
            }
        }

    }

    public void ShowProductOption(string name, string size, string color, string thumbnail)
    {
        if (!productOptionUIManager.isActiveAndEnabled)
            productOptionUIManager.gameObject.SetActive(true);

        OptionData optDataSrc = _listOptionCombination.Find(x => x.name == name);
        productOptionUIManager.SetProductData(name, optDataSrc.sizeOption, optDataSrc.colorOption, size, color, thumbnail);
    }

    string GetFilterOption(string name)
    {
        OptionData optDataSrc = _listOptionCombination.Find(x => x.name == name);
        return optDataSrc.filter;
    }

    public string GetFrameId(string name, string size, string color)
    {
        //for (int i = 0; i < _productJsonList.Count; ++i)
        //{
        //    ProductJsonInfo productInfo = _productJsonList[i];
        //    Debug.Log($"{i}, {productInfo.name}, {name},   {productInfo.size}, {size},     {productInfo.color}, {color},    frame {productInfo.frameId}");
        //    if (productInfo.name == name && productInfo.size == size && productInfo.color == color)
        //        return productInfo.frameId;
        //}

        if (_frameProductList == null) return string.Empty;

        for (int i = 0; i < _frameProductList.Count; ++i)
        {
            if (_frameProductList[i].frameName == name && _frameProductList[i].frameSize == size && _frameProductList[i].frameColor == color)
                return _frameProductList[i].frameID;
        }
        return string.Empty;
    }

    public string GetFramePathFromID(string frameid)
    {
        if (_frameProductList == null) return string.Empty;

        for(int i = 0; i < _frameProductList.Count; ++i)
        {
            if (_frameProductList[i].frameID == frameid)
                return _frameProductList[i].path;
        }
        return string.Empty;
    }

    public string GetThumbnailPathFromID(string frameid)
    {
        //return productListButton.GetThumbnailFromID(frameid);
        if (_frameProductList == null) return string.Empty;

        for (int i = 0; i < _frameProductList.Count; ++i)
        {
            if (_frameProductList[i].frameID == frameid)
                return _frameProductList[i].thumbnail;
        }
        return string.Empty;
    }

    public string GetMattressPath(string mattressid)
    {
        if (_mattressProductList.ContainsKey(mattressid))
            return _mattressProductList[mattressid];
        return string.Empty;
    }

    public string GetMattressPathFromNameSize(string name, string size)
    {
        string key = string.Format("{0}_{1}", name, size);
        if (_mattressPathMap.ContainsKey(key))
            return _mattressPathMap[key];
        return string.Empty;
    }

    public string[] GetMattressListFromSize(string size)
    {
        if (_mattressListSize.ContainsKey(size))
            return _mattressListSize[size];
        return null;
    }

    public void SetFrameAndMattressOption(string productJson)
    {
        Debug.Log("[UIController] SetFrameAndMattress");
        // clear exist product
        Main.Instance.productController.store.DestroyAll();

        // json parsing
        string frameName = string.Empty;
        string size = string.Empty;
        string color = string.Empty;
        string mattress = string.Empty;

        JSONObject product = new JSONObject(productJson);
        if (product != null)
        {
            if (product.HasField("frameName"))
                frameName = product.GetField("frameName").str;
            if (product.HasField("size"))
                size = product.GetField("size").str;
            if (product.HasField("color"))
                color = product.GetField("color").str;
            if (product.HasField("mattress"))
                mattress = product.GetField("mattress").str;

            ChangeProductFrameAndMattress(frameName, size, color, mattress);
        }
    }

    public void ChangeProductFrameAndMattress(string frameName, string size, string color, string mattress)
    {
        string frameID = GetFrameId(frameName, size, color);
        string path = GetFramePathFromID(frameID);
        //Debug.Log($"[UIController] Change Product frame and matt. {frameName}, {frameID}, {path}, {size}, {color}, {mattress}");
        JSONObject productJSONObj = new JSONObject();
        productJSONObj.AddField("url", path);
        productJSONObj.AddField("format", ".zip");
        productJSONObj.AddField("planeType", "floor");

        // load framd and load mattress from frame and size
        string mattressPath = GetMattressPathFromNameSize(mattress, size);
        if (mattressPath == string.Empty)
            mattressPath = BASE_PATH + "/product/mattressHT3.zip";

        productJSONObj.AddField("mattressUrl", mattressPath);
        productJSONObj.AddField("mattressFormat", ".zip");
        if (Main.Instance.productController != null)
            Main.Instance.productController.Load(productJSONObj.ToString(), (GameObject product) => {
                //Debug.Log("[UIController] product controller load done callback ");
#if UNITY_ANDROID && !UNITY_EDITOR
                //AndroidManager.Instance.CallJavaFunc("LoadProductCallback", "done");
#endif
            });

#if UNITY_ANDROID && !UNITY_EDITOR
        //AndroidManager.Instance.HideNavUI();
#endif

    }

    void SetCanvasMode(bool productViewMode = true)
    {
        // top panel
        topPanel.SetActive(!productViewMode);
        rightMenuPanel.SetActive(!productViewMode);

        // item list button
        showItemList.gameObject.SetActive(!productViewMode);

        loadUnitA.gameObject.SetActive(!productViewMode);
        loadUnitB.gameObject.SetActive(!productViewMode);
        loadUnitC.gameObject.SetActive(!productViewMode);

        //calcDistance.gameObject.SetActive(!productViewMode);
        //endCalcDistance.gameObject.SetActive(!productViewMode);
        //calcArea.gameObject.SetActive(!productViewMode);
        //endCalcArea.gameObject.SetActive(!productViewMode);

        //loadProduct.gameObject.SetActive(!productViewMode);
        //loadProduct2.gameObject.SetActive(!productViewMode);
        //loadProduct3.gameObject.SetActive(!productViewMode);

        //lightDirectionText.gameObject.SetActive(!productViewMode);
        //lightDirection.gameObject.SetActive(!productViewMode);

        //clearUnit.gameObject.SetActive(!productViewMode);

        screenShot.gameObject.SetActive(!productViewMode);
        alignment.gameObject.SetActive(!productViewMode);
        undo.gameObject.SetActive(!productViewMode);
        redo.gameObject.SetActive(!productViewMode);
        cameraModePanning.gameObject.SetActive(!productViewMode);
        cameraModeOrbit.gameObject.SetActive(!productViewMode);
        settings.gameObject.SetActive(!productViewMode);

        productScrollList.gameObject.SetActive(!productViewMode);

        //requestMapAPIBtn.gameObject.SetActive(!productViewMode);
        //inputFieldSearchAddress.gameObject.SetActive(!productViewMode);
        //pinButton.gameObject.SetActive(!productViewMode);

        //backToMain.gameObject.SetActive(productViewMode);
        if (productOptionUIManager.isActiveAndEnabled)
            productOptionUIManager.gameObject.SetActive(!productViewMode);

    }

    // for thumbnail
    public void ThumbnailMode()
    {
        Main.Instance.generalSetting.SetSDKMode("thumbnail");

        SetCanvasThumbnailMode(true);
        RequestInitialProductData(false);
    }

    void SetCanvasThumbnailMode(bool productViewMode = true)
    {
        loadUnitA.gameObject.SetActive(!productViewMode);
        loadUnitB.gameObject.SetActive(!productViewMode);
        loadUnitC.gameObject.SetActive(!productViewMode);

        calcDistance.gameObject.SetActive(!productViewMode);
        endCalcDistance.gameObject.SetActive(!productViewMode);
        calcArea.gameObject.SetActive(!productViewMode);
        endCalcArea.gameObject.SetActive(!productViewMode);

        loadProduct.gameObject.SetActive(!productViewMode);
        loadProduct2.gameObject.SetActive(!productViewMode);
        loadProduct3.gameObject.SetActive(!productViewMode);

        lightDirectionText.gameObject.SetActive(!productViewMode);
        lightDirection.gameObject.SetActive(!productViewMode);

        clearUnit.gameObject.SetActive(productViewMode);

        screenShot.gameObject.SetActive(!productViewMode);
        alignment.gameObject.SetActive(!productViewMode);
        undo.gameObject.SetActive(!productViewMode);
        redo.gameObject.SetActive(!productViewMode);
        cameraModePanning.gameObject.SetActive(!productViewMode);
        cameraModeOrbit.gameObject.SetActive(!productViewMode);

        productScrollList.gameObject.SetActive(productViewMode);
        requestMapAPIBtn.gameObject.SetActive(!productViewMode);
        inputFieldSearchAddress.gameObject.SetActive(!productViewMode);
        pinButton.gameObject.SetActive(!productViewMode);

        //backToMain.gameObject.SetActive(productViewMode);
        if (productOptionUIManager.isActiveAndEnabled)
            productOptionUIManager.gameObject.SetActive(!productViewMode);

    }

    public void ResetProductListWithFilter(int filter)
    {
        Debug.Log("reset filter " + filter);

        productListButton.ClearListButton();

        switch (filter)
        {
            case 0:
                foreach(ProductJsonInfo jsonInfo in _productJsonList)
                {
                    productListButton.InstantiateProductButton(jsonInfo.frameId, jsonInfo.name, jsonInfo.size, jsonInfo.color, jsonInfo.framePath, jsonInfo.thumbnailPath, jsonInfo.catalogThumbnail);
                }
                break;

            case 1:
                foreach (ProductJsonInfo jsonInfo in _productJsonList)
                {
                    if (GetFilterOption(jsonInfo.name) == "Wedding")
                        productListButton.InstantiateProductButton(jsonInfo.frameId, jsonInfo.name, jsonInfo.size, jsonInfo.color, jsonInfo.framePath, jsonInfo.thumbnailPath, jsonInfo.catalogThumbnail);
                }
                break;

            case 2:
                foreach (ProductJsonInfo jsonInfo in _productJsonList)
                {
                    if (GetFilterOption(jsonInfo.name) == "SuperSingle")
                        productListButton.InstantiateProductButton(jsonInfo.frameId, jsonInfo.name, jsonInfo.size, jsonInfo.color, jsonInfo.framePath, jsonInfo.thumbnailPath, jsonInfo.catalogThumbnail);
                }
                break;

            case 3:
                foreach (ProductJsonInfo jsonInfo in _productJsonList)
                {
                    if (GetFilterOption(jsonInfo.name) == "Family")
                        productListButton.InstantiateProductButton(jsonInfo.frameId, jsonInfo.name, jsonInfo.size, jsonInfo.color, jsonInfo.framePath, jsonInfo.thumbnailPath, jsonInfo.catalogThumbnail);
                }
                break;
        }

    }

    void OnApplicationFocus()
    {
        //AndroidManager.Instance.HideNavUI();
    }

    void OnApplicationPause(bool pause)
    {
        //AndroidManager.Instance.HideNavUI();
    }
}
