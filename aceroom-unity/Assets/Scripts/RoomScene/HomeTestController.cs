﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/// <summary>
/// [테스트용] 어반베이스 Crane 기능 테스트용 UI 컴포넌트.
/// </summary>
public class HomeTestController : AceSingleton<RoomSceneUIController>
{

    static public string BASE_PATH = "https://s3.ap-northeast-2.amazonaws.com/acebed-3d-assets-dev";

    public Button btnLoadRoom;
    public Button btnLoadProduct;

    public Button btnClearAll;

    public Button btnOptionHigh;
    public Button btnOptionMiddle;
    public Button btnOptionLow;
    public Button btnOptionCollision;

    public Button btnWallPaper;
    public Button btnUnitFloor;
    public Button btnStyleEditorEnd;


    // Start is called before the first frame update
    void Start()
    {
        btnLoadRoom.onClick.AddListener(() => {

            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                CallTestPlugin("LoadRoom");
            }else
            {

                RoomSceneUIController.Instance.LoadHomeUnit("{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1c271da53ab8ad712e9.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}");
                //RoomSceneUIController.Instance.LoadHomeUnit("{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1ba71da53ab8ad5d2e7/5600c1ba71da53ab8ad5d2e7.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}");
            }
        });
        btnLoadProduct.onClick.AddListener(() => {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                CallTestPlugin("LoadProduct");
            }
            else
            {
                RoomSceneUIController.Instance.LoadHomeProduct("{\"frame\":{\"frameid\": \"3d74a410-a1f3-11e9-a718-9ba302dbf24d\",\"path\": \"/product/LUNATO3_K_Gray.zip\",\"sfbPath\": \"/product/###.sfb\",\"3dModelPath\": \"\",\"thumbnail\": \"/product/LUNATO3_K_Gray_thumbnail.png\",\"catalogThumbnail\": \"/product/LUNATO3_K_Gray.png\",\"name\": \"LUNATO3\",\"size\": \"K\",\"color\": \"Gray\",\"isDisplay\": true,\"mattressOffset\": [3.817524,23.96977,331.5197],\"mattressScale\": 1},\"mattress\":{\"mattress\": \"9a113be0-c7cb-11e9-a8ed-bb36fb6c62a7\",\"path\": \"/product/HT3_K.zip\",\"sfbPath\": \"/product/###.sfb\",\"3dModelPath\": \"\",\"name\": \"HT3\",\"size\": \"K\"}}");
            }
        });
        btnClearAll.onClick.AddListener(() => {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                CallTestPlugin("ClearAll");
            }
            else
            {
                RoomSceneUIController.Instance.ClearAll();
            }
        });
        btnOptionHigh.onClick.AddListener(() => {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                CallTestPlugin("OptionHigh");
            }
            else
            {
                getMobileManager().LoadHomeOptionEffect("2");
            }
        });
        btnOptionMiddle.onClick.AddListener(() => {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                CallTestPlugin("OptionMiddle");
            }
            else
            {
                getMobileManager().LoadHomeOptionEffect("1");
            }
        });
        btnOptionLow.onClick.AddListener(() => {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                CallTestPlugin("OptionLow");
            }
            else
            {
            getMobileManager().LoadHomeOptionEffect("0");
            }
        });
        btnOptionCollision.onClick.AddListener(() => {
            CallTestPlugin("OptionCollision");
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                CallTestPlugin("LoadRoom");
            }
            else
            {
                if(UnityEngine.Random.Range(0, 2) == 1)
                {
                    getMobileManager().LoadHomeOptionCollision("true");
                } else {
                    getMobileManager().LoadHomeOptionCollision("false");
                }
            }
        });



        btnWallPaper.onClick.AddListener(() => {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                CallTestPlugin("WallPaper");
            }
            else
            {
                getMobileManager().ChangeFloorStyle("{\"material\":{\"id\":\"61100\",\"thumbnail_path\":\"thumbnail/material/61100/thumbnail.jpg\",\"title\":\"세라마블 라이트그레이 1 HQ\",\"description\":\"마루\",\"type\":\"wall\",\"data\":{\"colorDiffuse\":[1,1,1],\"colorSpecular\":[0,0,0],\"colorEmissive\":[0,0,0],\"shading\":\"phong\",\"specularCoef\":10,\"transparent\":false,\"opacity\":1,\"flatShading\":false,\"flipSided\":false,\"doubleSided\":false,\"DbgColor\":\"\",\"DbgIndex\":0,\"opticalDensity\":0,\"illumination\":\"\",\"blending\":1,\"depthTest\":true,\"depthWrite\":true,\"colorWrite\":true,\"reflectivity\":1,\"visible\":true,\"vertexColors\":false,\"mapDiffuse\":\"material/61100/0d3cdb251fffdbc2b30555fclightgray.jpg\",\"mapDiffuseRepeat\":[6,6],\"mapDiffuseOffset\":[0,0],\"mapDiffuseWrap\":[\"repeat\",\"repeat\"],\"mapDiffuseAnisotropy\":2,\"mapEmissive\":\"\",\"mapEmissiveRepeat\":[1,1],\"mapEmissiveOffset\":[0,0],\"mapEmissiveWrap\":[\"repeat\",\"repeat\"],\"mapEmissiveAnisotropy\":2,\"mapLight\":\"\",\"mapLightRepeat\":[1,1],\"mapLightOffset\":[0,0],\"mapLightWrap\":[\"repeat\",\"repeat\"],\"mapLightAnisotropy\":2,\"mapAO\":\"\",\"mapAORepeat\":[1,1],\"mapAOOffset\":[0,0],\"mapAOWrap\":[\"repeat\",\"repeat\"],\"mapAOAnisotropy\":2,\"mapBump\":\"material/61100/0d3cdb251fffdbc2b30555fclightgrayb.jpg\",\"mapBumpScale\":10,\"mapBumpRepeat\":[1,1],\"mapBumpOffset\":[0,0],\"mapBumpWrap\":[\"repeat\",\"repeat\"],\"mapBumpAnisotropy\":2,\"mapNormal\":\"material/61100/0d3cdb251fffdbc2b30555fclightgraybn.jpg\",\"mapNormalFactor\":1,\"mapNormalRepeat\":[1,1],\"mapNormalOffset\":[0,0],\"mapNormalWrap\":[\"repeat\",\"repeat\"],\"mapNormalAnisotropy\":2,\"mapSpecular\":\"\",\"mapSpecularRepeat\":[1,1],\"mapSpecularOffset\":[0,0],\"mapSpecularWrap\":[\"repeat\",\"repeat\"],\"mapSpecularAnisotropy\":2,\"mapRoughness\":\"\",\"mapRoughnessRepeat\":[1,1],\"mapRoughnessOffset\":[0,0],\"mapRoughnessWrap\":[\"repeat\",\"repeat\"],\"mapRoughnessAnisotropy\":2,\"mapMetalness\":\"\",\"mapMetalnessRepeat\":[1,1],\"mapMetalnessOffset\":[0,0],\"mapMetalnessWrap\":[\"repeat\",\"repeat\"],\"mapMetalnessAnisotropy\":2,\"mapAlpha\":\"\",\"mapAlphaRepeat\":[1,1],\"mapAlphaOffset\":[0,0],\"mapAlphaWrap\":[\"repeat\",\"repeat\"],\"mapAlphaAnisotropy\":2,\"wireframe\":false,\"metalness\":0,\"smoothness\":0.5}}}");
            }
        });
        btnUnitFloor.onClick.AddListener(() => {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                CallTestPlugin("UnitFloor");
            }
            else
            {
                getMobileManager().ChangeFloorStyle("{\"material\":{\"id\":\"65100\",\"thumbnail_path\":\"thumbnail/material/65100/thumbnail.jpg\",\"title\":\"세라마블 라이트그레이 1 HQ\",\"description\":\"마루\",\"type\":\"floor\",\"data\":{\"colorDiffuse\":[1,1,1],\"colorSpecular\":[0,0,0],\"colorEmissive\":[0,0,0],\"shading\":\"phong\",\"specularCoef\":10,\"transparent\":false,\"opacity\":1,\"flatShading\":false,\"flipSided\":false,\"doubleSided\":false,\"DbgColor\":\"\",\"DbgIndex\":0,\"opticalDensity\":0,\"illumination\":\"\",\"blending\":1,\"depthTest\":true,\"depthWrite\":true,\"colorWrite\":true,\"reflectivity\":1,\"visible\":true,\"vertexColors\":false,\"mapDiffuse\":\"material/65100/0d3cdb251fffdbc2b30555fclightgray.jpg\",\"mapDiffuseRepeat\":[6,6],\"mapDiffuseOffset\":[0,0],\"mapDiffuseWrap\":[\"repeat\",\"repeat\"],\"mapDiffuseAnisotropy\":2,\"mapEmissive\":\"\",\"mapEmissiveRepeat\":[1,1],\"mapEmissiveOffset\":[0,0],\"mapEmissiveWrap\":[\"repeat\",\"repeat\"],\"mapEmissiveAnisotropy\":2,\"mapLight\":\"\",\"mapLightRepeat\":[1,1],\"mapLightOffset\":[0,0],\"mapLightWrap\":[\"repeat\",\"repeat\"],\"mapLightAnisotropy\":2,\"mapAO\":\"\",\"mapAORepeat\":[1,1],\"mapAOOffset\":[0,0],\"mapAOWrap\":[\"repeat\",\"repeat\"],\"mapAOAnisotropy\":2,\"mapBump\":\"material/65100/0d3cdb251fffdbc2b30555fclightgrayb.jpg\",\"mapBumpScale\":10,\"mapBumpRepeat\":[1,1],\"mapBumpOffset\":[0,0],\"mapBumpWrap\":[\"repeat\",\"repeat\"],\"mapBumpAnisotropy\":2,\"mapNormal\":\"material/65100/0d3cdb251fffdbc2b30555fclightgraybn.jpg\",\"mapNormalFactor\":1,\"mapNormalRepeat\":[1,1],\"mapNormalOffset\":[0,0],\"mapNormalWrap\":[\"repeat\",\"repeat\"],\"mapNormalAnisotropy\":2,\"mapSpecular\":\"\",\"mapSpecularRepeat\":[1,1],\"mapSpecularOffset\":[0,0],\"mapSpecularWrap\":[\"repeat\",\"repeat\"],\"mapSpecularAnisotropy\":2,\"mapRoughness\":\"\",\"mapRoughnessRepeat\":[1,1],\"mapRoughnessOffset\":[0,0],\"mapRoughnessWrap\":[\"repeat\",\"repeat\"],\"mapRoughnessAnisotropy\":2,\"mapMetalness\":\"\",\"mapMetalnessRepeat\":[1,1],\"mapMetalnessOffset\":[0,0],\"mapMetalnessWrap\":[\"repeat\",\"repeat\"],\"mapMetalnessAnisotropy\":2,\"mapAlpha\":\"\",\"mapAlphaRepeat\":[1,1],\"mapAlphaOffset\":[0,0],\"mapAlphaWrap\":[\"repeat\",\"repeat\"],\"mapAlphaAnisotropy\":2,\"wireframe\":false,\"metalness\":0,\"smoothness\":0.5}}}");
            }
        });
        btnStyleEditorEnd.onClick.AddListener(() => {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                CallTestPlugin("StyleEditorEnd");
            }
            else
            {
                getMobileManager().StyleEditorEnd();
            }
        });

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CallTestPlugin(string testMsg)
    {
        getMobileManager().CallTest(testMsg);
    }
}
