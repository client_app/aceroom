﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using DLLCore;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;
using System.Text;

/// <summary>
/// 홈에디터 UI 컨트롤러
/// </summary>
public class RoomSceneUIController : AceSingleton<RoomSceneUIController>
{
    // static UI Size
    float btnSizeWidth = 107;
    float btnSizeNotchWidth = 107;

    //static value
    /// <summary> 패닝(회전이 아닌 이동) 모드 여부 </summary>
    bool isPanningMode = false;
    /// <summary> UI 버튼 기능 잠금 여부 </summary>
    bool isButtonLock = false;

    //UI
    public GameObject canvas;

    public Text productText;
    public Text startText;
    public Text endText;
    public Text timeText;


    //Top
    public Button btnBack;
    public Button btnShare;
    public Button btnSave;

    //Right
    public Button btnItemTap;
    public Button btnNorchItemTap;
    Button btnItem;

    //Left
    public Button btnPosition;
    public Button btnUnDo;
    public Button btnReDo;
    public Button btnHand;
    public Button btnOption;

    public Button btnStopStyling;
    /// <summary> 도면/벽치 편집 모드 여부 </summary>
    bool _isStyling = false;

    //1인칭모드 관련
    public GameObject joystickCotrol;
    public GameObject viewRotationControls;
    /// <summary> 1인칭 모드 여부 </summary>
    bool _fpsViewMode = false;

    public GameObject panelTop;
    public GameObject panelTest;
    //public GameObject panelLeft;
    public GameObject panelRight;

    public GameObject panelSightMode;
    public Button btnFPMode;
    public Button btnTPMode;
    public Image imgFPMode;
    public Image imgTPMode;


    //object
    Camera mainCam;
    

    // Use this for initialization
    void Start()
    {
        mainCam = Camera.main;

        //테스트용
        if (DefineWord.__IS_TEST)
        {
            //테스트용 버튼 UI 표시
            panelTest.SetActive(DefineWord.__IS_TEST);
        }

        Debug.Log("Start Call OptionQuality");
        // 기본설정 - 사양 상
        OptionQuality(2);

        //노치가 있는 경우 버튼 위치 변경(노치만큼 이동)
        if (getMobileManager().isNotch)
        {
            btnNorchItemTap.gameObject.SetActive(true);
            btnItemTap.gameObject.SetActive(false);

            btnItem = btnNorchItemTap;
        }
        else
        {
            btnNorchItemTap.gameObject.SetActive(false);
            btnItemTap.gameObject.SetActive(true);

            btnItem = btnItemTap;
        }

        Debug.Log(this.gameObject.name);
        if(btnBack != null)
        {
            btnBack.onClick.AddListener(() => {
                Debug.Log(this.gameObject.name);
                //native call -> 뒤로가기
                //1. clear all
                ClearAll();

				//2.native - backbutton 이벤트 호출
				//getMobileManager().CallBackButton();

                //3. DefaultScene로 이동
                Debug.Log("DefaultScene Load", gameObject);
                //LoadScene("DefaultScene");
                getMobileManager().CallChangeSceneDefault("");
            });
        }

        if(btnShare != null){
            btnShare.onClick.AddListener(() => {
                //get Screenshot Path
                StartCoroutine(ScreenShotShare());
            });
        }

        if (btnSave != null)
        {
            btnSave.onClick.AddListener(() =>
            {
                //get Screenshot Path
                StartCoroutine(ScreenShotSave());
            });
        }

        if (btnItem != null)
        {
            btnItem.onClick.AddListener(() =>
            {
                //native call
                Log("OnClick btnItem");
                getMobileManager().CallCatalog();
            });
        }

        if (btnPosition != null)
        {
            btnPosition.onClick.AddListener(() =>
            {   
                //어반베이스 Crane API - 카메라 위치 초기화
                JSONObject jsonObj = new JSONObject();
                jsonObj.AddField("direction", "init");
                jsonObj.AddField("isOrtho", false);

                Main.Instance.observerController.SetPresetView(jsonObj.ToString());
            });
        }
        if (btnUnDo != null)
        {
            btnUnDo.onClick.AddListener(() =>
            {
                History.Instance.Undo();
            });
        }
        if (btnReDo != null)
        {
            btnReDo.onClick.AddListener(() =>
            {
                History.Instance.Redo();
            });
        }
        if (btnHand != null)
        {
            btnHand.onClick.AddListener(() =>
            {
                TogglePanningMode();
            });
        }
        if (btnOption != null)
        {
            btnOption.onClick.AddListener(() =>
            {
                //어반베이스 Crane API - Crane에 적용중인 옵션을 조회 후 Native 쪽 옵션 화면 호출
                string strShadowType = Main.Instance.graphicSetting.GetShadowType();

                int shadowType = 2;

                if (strShadowType == "none")
                {
                    shadowType = 0;
                }
                else if (strShadowType == "hard")
                {
                    shadowType = 1;
                }
                else /*if (qualityLevel == 0)*/
                {
                    shadowType = 2;
                }

                int vsync = Main.Instance.graphicSetting.GetVSync();
                bool wallCollider = Main.Instance.unitController.GetStatusWallCollider();

                JSONObject optionJSONObj = new JSONObject();
                optionJSONObj.AddField("effect", shadowType);
                optionJSONObj.AddField("collision", wallCollider);
                Log("Option JSON - " + optionJSONObj.ToString());
                getMobileManager().CallHomeOption(optionJSONObj.ToString());

            });
        }


        if (btnFPMode != null)
        {
            btnFPMode.onClick.AddListener(() =>
            {
                OnClickViewModeToggle(false);
            });
        }

        if (btnTPMode != null)
        {
            btnTPMode.onClick.AddListener(() =>
            {
                OnClickViewModeToggle(true);
            });
        }

        if (btnStopStyling != null)
        {
            //stopStyling.gameObject.SetActive(true);
            btnStopStyling.onClick.AddListener(() => {
                Debug.Log(this.gameObject.name);
                OnClickStopStyling();
            });

            btnStopStyling.gameObject.SetActive(false);
        }

        setDefaultSetting();

    }

    // Update is called once per frame
    void Update()
    {
        //어반베이스 Crane API - 1인칭 모드에서 조이스틱으로 카메라 이동
        if (_fpsViewMode)
        {
            float horizontalAxis = CrossPlatformInputManager.GetAxis("Horizontal");
            float verticalAxis = CrossPlatformInputManager.GetAxis("Vertical");

            float moveX = CrossPlatformInputManager.GetAxis("Mouse X");
            float moveY = CrossPlatformInputManager.GetAxis("Mouse Y");

            //Debug.Log($"virtual axis {horizontalAxis}, {verticalAxis}, {moveX}, {moveY}");
            Main.Instance.playerController.SetPlayerMoveAndRotate(horizontalAxis, verticalAxis, moveX, moveY);
        }
    }

    /// <summary>
    /// 어반베이스 Crane 초기 설정
    /// </summary>
    private void setDefaultSetting()
    {

        if (Main.Instance != null && Main.Instance.graphicSetting != null && Main.Instance.observerController != null)
        {
            Debug.Log("(======================Main.Instance is not null");
            //제품 선택 시 나타나는 context 메뉴 사이즈
            Main.Instance.graphicSetting.SetMenuScaleFactor(7);

            Main.Instance.observerController.SetPinchZoomParameter(0.8f);

            //1인칭모드 시야각 조절(default: -60, 60)
            Main.Instance.playerController.SetAngleMinMax(-80, 80);
            //1인칭모드 이동속도(default:1.2)
            Main.Instance.playerController.SetMoveSpeed(1.4f);
            //1인칭모드 화면 회전 속도(default:1.0)
            Main.Instance.playerController.SetViewRotationSpeed(1.2f);

            //카메라 줌인(default:0.001)
            Main.Instance.observerController.SetCameraMinDistance(4f);
            //카메라 줌아웃(default:100)
            Main.Instance.observerController.SetCameraMaxDistance(35f);

            //벽지, 바닥재 서버 경로
            //Main.Instance.unitController.SetTextureBasePath(DefineWord.SERVER_URL + "/");


            SetUiFirstPersonMode(false);
        }
        else
        {
            Debug.Log("=======================Main.Instance is null");
        }
    }

    /// <summary>
    /// 스크린샷 저장
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator ScreenShotSave()
    {
        // hide ui
        canvas.SetActive(false);

        yield return new WaitForEndOfFrame();
        Debug.Log("screenshot Call Start");
        Main.Instance.toolController.ScreenShot((path) => {
            Debug.Log("screenshot path:" + path);
            //getMobileManager().CallHomeEditorSave(path);
            StartCoroutine(ScreenShotSaveConfirm(path));
        });
        Debug.Log("screenshot Call End");
        yield return new WaitForEndOfFrame();
        // show ui
        canvas.SetActive(true);
    }

    /// <summary>
    /// 스크린샷 저장 후 네이티브로 공유 요청
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator ScreenShotShare()
    {
        // hide ui 
        canvas.SetActive(false);

        yield return new WaitForEndOfFrame();

        Main.Instance.toolController.ScreenShot((path) => {
            Debug.Log("screenshot path:" + path);
			//getMobileManager().CallShare(path);
			StartCoroutine(ScreenShotShareConfirm(path));
		});

        yield return new WaitForEndOfFrame();
        // show ui
        canvas.SetActive(true);
    }

    /// <summary>
    /// 스크린샷 저장용 - 스크린샷이 저장됐는지 확인하는 루프. 무한루프 방지를 위해 100번 검사.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    IEnumerator ScreenShotSaveConfirm(string path)
    {
        int count = 0;
        while (true)
        {
            count++;
            System.IO.FileInfo fi = new System.IO.FileInfo(path);
            if (fi.Exists)
            {
                Log("이미지가 있다ㅏㅏㅏㅏㅏㅏ");
                getMobileManager().CallHomeEditorSave(path);
                break;
            }
            else
            {
				//방어용
				if (count == 100)
				{
					getMobileManager().CallShowToast("스크린샷 저장 중 오류가 발생하였습니다.");
					break;
				}

				Log("이미지 없음!!!!!!");
                yield return new WaitForEndOfFrame();
            }

            
        }   
    }

    /// <summary>
    /// 스크린샷 공유용 - 스크린샷이 저장됐는지 확인하는 루프. 무한루프 방지를 위해 100번 검사.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
	IEnumerator ScreenShotShareConfirm(string path)
	{
		int count = 0;
		while (true)
		{
			count++;
			System.IO.FileInfo fi = new System.IO.FileInfo(path);
			if (fi.Exists)
			{
				Log("이미지가 있다ㅏㅏㅏㅏㅏㅏ");
				getMobileManager().CallShare(path);
				break;
			}
			else
			{
				//방어용
				if (count == 100)
				{
					getMobileManager().CallShowToast("스크린샷 저장 중 오류가 발생하였습니다.");
					break;
				}
				Log("이미지 없음!!!!!!");
				yield return new WaitForEndOfFrame();
			}

			
		}
	}


    /// <summary>
    /// 현제 초(ms단위) 조회
    /// </summary>
    /// <returns>초(ms 단위)</returns>
	public int getNowMilisec()
    {
        DateTime dateTime = System.DateTime.Now;
        int milsec = (dateTime.Minute * 60 * 1000) + (dateTime.Second * 1000) + dateTime.Millisecond;

        return milsec;
    }

    /// <summary>
    /// 패닝 모드 활성/비활성 전환
    /// </summary>
    public void TogglePanningMode()
    {
        //cameraModePanning.gameObject.SetActive(false);
        //cameraModeOrbit.gameObject.SetActive(true);

        isPanningMode = !isPanningMode;
        Debug.Log("togglePanningMode : "+ isPanningMode);
        ToggleUIButtonPanningMode(isPanningMode);
        Main.Instance.observerController.SetPanningMode(isPanningMode);
    }

    /// <summary>
    /// Scene 로딩 완료 시 호출(Crane의 도면 로딩용)
    /// </summary>
    /// <param name="strUnitJson">Crane 에서 사용하는 도면 Json</param>
    public void OnSceneLoadEnd(string strUnitJson)
    {
        if (Main.Instance != null && Main.Instance.graphicSetting != null)
        {
            Log("Main.Instance != null");

            //기본설정(중옵)
            //OptionQuality(1);
            //기본설정(하옵)
            //OptionQuality(0);

            //충돌처리 여부 킴 -> Unit Load 후 킴.
            //OptionCollision(true);

            //기본 설정
            setDefaultSetting();
        }
        else
        {
            Log("Main.Instance == null");
        }
        

        LoadHomeUnit(strUnitJson);
    }

    /// <summary>
    /// 어반베이스 Buildings API(네이티브 지도)의 데이터를 Crane의 API에 맞게 사용하기 위한 메소드
    /// </summary>
    /// <param name="strUnitJson">Buildings API 중 하나의 도면에 대한 JSON String</param>
    public void LoadHomeUnit(string strUnitJson)
    {
        // Load/Unload 플래그 필요.
        Debug.Log("RoomSceneUIController - LoadHomeUnit(strUnitJson)");

        //1. Unload All
        //ClearAll();

        //2. parse json
        JSONObject unitJson = new JSONObject(strUnitJson);
        string filePath = unitJson.GetField("signed_url").str;

        //3. load unit
        string unitPath = filePath;
        Debug.Log("RoomSceneUIController - Load Unit Path = " + unitPath);
        LoadUnit(unitPath);
    }

    /// <summary>
    /// Urbanbase Crane - 도면 다운로드 및 화면 호출
    /// </summary>
    /// <param name="unitUri">도면 uri String</param>
    private void LoadUnit(string unitUri)
    {
        Main.Instance.generalSetting.SetSDKMode("homedesign");

        JSONObject unitJSONObj = new JSONObject();
        unitJSONObj.AddField("url", unitUri);
        unitJSONObj.AddField("format", "dae");
        Log("[uicont] loading state :" + Main.Instance.unitController.GetLoadingState());
        if (Main.Instance.unitController.GetLoadingState() == 0)
        {
            Main.Instance.unitController.Load(unitJSONObj.ToString(), (string message) => {
                Log("Load Unit Complete");
                getMobileManager().CallbackLoadHomeUnit();
                //충돌처리 여부 킴 -> Unit Load 후 킴.
                OptionCollision(true);
            });

            Main.Instance.graphicSetting.SetVSync(2);
            Main.Instance.graphicSetting.SetShadowType("soft");
        }
    }

    /// <summary>
    /// Urbanbase Crane - 제품 불러오기
    /// </summary>
    /// <param name="strProductJson">제품목록 API 중 프레임/매트리스 JSON String</param>
    public void LoadHomeProduct(string strProductJson)
    {
        // Load/Unload 플레그에 따라 분기 필요
        Log("RoomSceneUIController - LoadHomeUnit -> "+ strProductJson);
        //1. parse json
        JSONObject productJson = new JSONObject(strProductJson);

        JSONObject frameJson = productJson.GetField("frame");
        JSONObject mattressJson = productJson.GetField("mattress");

        string framePath = frameJson.GetField("path").str;
        string mattressPath = mattressJson.GetField("path").str;


        //2. load product
        placeProduct(framePath, mattressPath);
    }

    /// <summary>
    /// Urbanbase Crane - 도면 벽지/바닥재 편집모드 버튼 이벤트
    /// </summary>
    public void OnClickStopStyling()
    {
        Main.Instance.unitController.StopStyling();
        btnStopStyling.gameObject.SetActive(false);
        Main.Instance.productController.EnableOutlineEffect();
    }

    /// <summary>
    /// Urbanbase Crane - 도면 벽지를 변경한다(사용안함)
    /// </summary>
    /// <param name="strProductJson">벽지 JSON String</param>
    public void ChangeWallPaperStyle(string strProductJson)
    {
        startStyling(strProductJson);
    }

    /// <summary>
    /// Urbanbase Crane - 도면 바닥재를 변경한다
    /// </summary>
    /// <param name="strProductJson">바닥재 JSON String</param>
    public void ChangeFloorStyle(string strProductJson)
    {
        startStyling(strProductJson);
    }

    /// <summary>
    /// Urbanbase Crane - 가구 불러오기
    /// </summary>
    /// <param name="strFurnitureJson">제품목록 API 중 프레임/매트리스 JSON String</param>
    public void LoadHomeFurniture(string strFurnitureJson)
    {
        // Load/Unload 플레그에 따라 분기 필요
        Log("RoomSceneUIController - LoadHomeFurniture -> " + strFurnitureJson);

        JSONObject rootJson = new JSONObject(strFurnitureJson);

        JSONObject furnitureJson = rootJson.GetField("furniture");

        string furniturePath = furnitureJson.GetField("path").str;

        Log("=============개발 필요=========================");

        JSONObject productJSONObj = new JSONObject();
        productJSONObj.AddField("url", furniturePath);
        productJSONObj.AddField("format", ".zip"); //dae
        productJSONObj.AddField("planeType", "floor");
        productJSONObj.AddField("mattressUrl", "");
        productJSONObj.AddField("mattressFormat", ".zip");
        productJSONObj.AddField("productType", "roomset");

        Debug.Log("LoadHomeFurniture JSON [" + productJSONObj.ToString() + "]", gameObject);

        Main.Instance.productController.Load(productJSONObj.ToString(), (GameObject product) => {
            Log("Load Furniture Complete");
            getMobileManager().CallbackLoadFurnitureComplete();
        });
    }

    /// <summary>
    /// Urbanbase Crane - 벽지 + 바닥재를 변경한다(json 파라미터에 따라 다르다)
    /// </summary>
    /// <param name="strProductJson">벽지+바닥재 JSON String</param>
    public void ChangeUnitStyle(string strProductJson)
    {
        startStyling(strProductJson);
    }

    /// <summary>
    /// Urbanbase Crane - 벽지/바닥재 편집모드
    /// </summary>
    /// <param name="strProductJson">벽지+바닥재 JSON String</param>
    public void startStyling(string strProductJson)
    {
        //1인칭 모드인 경우 수정 불가
        if (_fpsViewMode)
        {
            getMobileManager().CallShowToast("1인칭 모드에서는 해당 기능을 사용할 수 없습니다.");
            return;
        }

        // Load/Unload 플레그에 따라 분기 필요
        Log("RoomSceneUIController - ChangeWallPaperStyle -> " + strProductJson);

        //load wall style
        //Main.Instance.unitController.ChangeStyleOfWall(strProductJson);
        _isStyling = true;
        Main.Instance.unitController.StartStyling(strProductJson);
        btnStopStyling.gameObject.SetActive(true);
    }

    /// <summary>
    /// Urbanbase Crane - 도면 벽지/바닥재 편집모드 해제
    /// </summary>
    public void StopStyling()
    {
        if(_isStyling == false)
        {
            return;
        }

        _isStyling = false;

        Main.Instance.unitController.StopStyling();
        btnStopStyling.gameObject.SetActive(false);
    }

    /// <summary>
    /// Urbanbase - 제품 다운로드 및 화면 호출
    /// </summary>
    /// <param name="framePath">제품(프레임) url String</param>
    /// <param name="mattressPath">제품(매트리스) url String</param>
    public void placeProduct(string framePath, string mattressPath)
    {

        JSONObject productJSONObj = new JSONObject();
        productJSONObj.AddField("url", framePath);
        productJSONObj.AddField("format", ".zip"); //dae
        productJSONObj.AddField("planeType", "floor");
        //productJSONObj.AddField("bedProductType", "frame");

        //JSONObject productJSONObj = new JSONObject();
        productJSONObj.AddField("mattressUrl", mattressPath);
        productJSONObj.AddField("mattressFormat", ".zip");

        Debug.Log("LoadHomeUnit JSON [" + productJSONObj.ToString() + "]", gameObject);

        Main.Instance.productController.Load(productJSONObj.ToString(), (GameObject product) => {
            Log("Load Product Complete");
            getMobileManager().CallbackLoadHomeProduct();
        });
    }


    /// <summary>
    /// 화면에 표시된 3D 오브젝트 모두 제거
    /// </summary>
    public void ClearAll()
    {
        GameObject[] units = GameObject.FindGameObjectsWithTag("Unit");
        foreach (GameObject unit in units)
        {
            GameObject.Destroy(unit);
        }
        // Urbanbase Crane - remove product
        Main.Instance.productController.store.DestroyAll();
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Urbanbase Crane - 사양 설정
    /// </summary>
    /// <param name="qualityLevel">사양 코드(0 = 낮음, 2 = 높음)</param>
    public void OptionQuality(int qualityLevel)
    {
        if (Main.Instance.graphicSetting != null)
        {
            if (qualityLevel == 2)
            {
                Main.Instance.graphicSetting.SetVSync(2);
                Main.Instance.graphicSetting.SetShadowType("soft");
            }
            else if (qualityLevel == 1)
            {
                Main.Instance.graphicSetting.SetVSync(1);
                Main.Instance.graphicSetting.SetShadowType("hard");
            }
            else /*if (qualityLevel == 0)*/
            {
                Main.Instance.graphicSetting.SetVSync(0);
                Main.Instance.graphicSetting.SetShadowType("none");
            }
        }
            
        
    }

    /// <summary>
    /// Urbanbase Crane - 충돌 체크 사용여부
    /// </summary>
    /// <param name="isCollision">true = 충돌 체크</param>
    public void OptionCollision(bool isCollision)
    {
        Main.Instance.unitController.ActivateWallCollider(isCollision ? 1 : 0);
    }

    /// <summary>
    /// Urbanbase Crane - 1/3인칭 모드 Toggle
    /// </summary>
    /// <param name="isObserverMode">true = 1인칭 모드</param>
    void OnClickViewModeToggle(bool isObserverMode)
    {
        OnClickStopStyling();

        if (isButtonLock)
        {
            return;
        }

        if (isObserverMode != _fpsViewMode) return;

        isButtonLock = true;

        Log("isObserverMode = " + isObserverMode);

        JSONObject jsonViewMode = new JSONObject();

        if(isObserverMode)
        {
            jsonViewMode.AddField("mode", "observer");
        }
        else
        {
            jsonViewMode.AddField("mode", "player");
        }
        jsonViewMode.AddField("animation_time", 10);

        Main.Instance.generalSetting.SetViewMode(jsonViewMode.ToString(), () => { isButtonLock = false; });

        if (isObserverMode)
        {
            //1인칭 모드인 경우 조이스틱 화면에 표시
            Main.Instance.playerController.SetDualTouchPad(false);
            Main.Instance.productController.EnableOutlineEffect();
        }
        else
        {
            Main.Instance.playerController.SetDualTouchPad(true);
            // unselect product
            foreach (Product prd in Main.Instance.productController.store.selectedProducts)
                prd.Unselect();

            Main.Instance.productController.DisableOutlineEffect();
        }

        SetUiFirstPersonMode(!isObserverMode);

    }

    /// <summary>
    /// Urbanbase Crane - 1인칭 모드 UI 설정
    /// </summary>
    /// <param name="isFPMode">true = 1인칭 모드</param>
    void SetUiFirstPersonMode(bool isFPMode)
    {
        //panelTop.SetActive(!isFPMode);
        panelRight.SetActive(!isFPMode);

        btnItem.gameObject.SetActive(!isFPMode);

        viewRotationControls.SetActive(isFPMode);
        joystickCotrol.SetActive(isFPMode);
        _fpsViewMode = isFPMode;
        Main.Instance.playerController.SetDualTouchPad(isFPMode);

        ToggleUIButtonSightMode(isFPMode);
    }

    /// <summary> UI 표시를 위한 앱 내 UI용 이미지 파일 경로 </summary>
    private string imagePathRoot = "Images/02_HomeEditor/";
    /// <summary>
    /// 1인칭/3인칭 모드 전환 시 화면에 모드 표시 버튼 이미지 변경용 메소드
    /// </summary>
    /// <param name="isFPMode">true = 1인칭 모드</param>
    void ToggleUIButtonSightMode(bool isFPMode)
    {

        if (isFPMode)
        {
            btnFPMode.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePathRoot + "02_sub_06_button_first_person_clk");
            imgFPMode.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePathRoot + "02_sub_06_text_first_person");

            btnTPMode.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePathRoot + "02_sub_06_button_third_person");
            imgTPMode.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePathRoot + "02_sub_06_text_third_person_clk");
        }
        else
        {
            btnFPMode.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePathRoot + "02_sub_06_button_first_person");
            imgFPMode.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePathRoot + "02_sub_06_text_first_person_clk");

            btnTPMode.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePathRoot + "02_sub_06_button_third_person_clk");
            imgTPMode.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePathRoot + "02_sub_06_text_third_person");
        }
    }

    /// <summary>
    /// 패닝 모드 전환 시 버튼 이미지 변경용 메소드
    /// </summary>
    /// <param name="isPanning">true = 패닝 모드</param>
    void ToggleUIButtonPanningMode(bool isPanning)
    {
        if (isPanning)
        {
            btnHand.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePathRoot + "sub_06_button_free_move");
        }
        else
        {
            btnHand.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePathRoot + "sub_06_button_move");
        }
    }
}
