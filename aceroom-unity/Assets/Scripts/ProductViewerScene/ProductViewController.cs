﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DLLCore;
using UnityEngine.Networking;

/// <summary>
/// [어반베이스] 제품 미리보기 화면 컨트롤러
/// 수정 시 어반베이스에 문의 필요.
/// </summary>
public class ProductViewController : AceSingleton<ProductViewController>
{
    public GeneralSetting generalSetting;
    public GraphicSetting graphicSetting;
    public MaterialLoader materialLoader;
    public ModelLoader modelLoader;
    public GameObject pointLightPrefab;
    GameObject[] _pointLightsForProduct;

    // Start is called before the first frame update
    void Start()
    {
        if (generalSetting)
        {
            Main.Instance.generalSetting = generalSetting;
            generalSetting.gameObject.SetActive(true);
            Debug.Log("set sdk mode product view");
            generalSetting.SetSDKMode("productview");
        }
        if (graphicSetting)
        {
            Main.Instance.graphicSetting = graphicSetting;
        }

        if (modelLoader)
        {
            Main.Instance.modelLoader = modelLoader;
            modelLoader.gameObject.SetActive(true);
        }

        if (materialLoader)
        {
            Main.Instance.materialLoader = materialLoader;
            materialLoader.gameObject.SetActive(true);
        }

        generalSetting.SetSDKMode("productview");

        // point light
        _pointLightsForProduct = new GameObject[4];

    }

    private void Destroy()
    {
        Debug.Log("[ProductViewController] destroy");

        foreach (GameObject light in _pointLightsForProduct)
            GameObject.Destroy(light);
        _pointLightsForProduct = null;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //    Application.Quit();

    }

    public void SetFrameAndMattress(string productJson)
    {
        //Log("Load Product View Object - "+productJson);
        //현재씬안에 배치된 포인트라이트 4개 제거하고 침대 제품도 제거한다
        
        /*JSONObject productJSONObj = new JSONObject(productJson);
        string framePath = productJSONObj.GetField("url").str;
        string frameFormat = productJSONObj.GetField("format").str;
        string planeType = productJSONObj.GetField("planeType").str;
        string mattressPath = productJSONObj.GetField("mattressUrl").str;
        string mattressFormat = productJSONObj.GetField("mattressFormat").str;*/

        if (Main.Instance.productController != null)
        {
            Product[] products = FindObjectsOfType<Product>();
            foreach(Product prd in products)
            {
                Light[] pointLights = prd.GetComponentsInChildren<Light>();
                foreach (Light light in pointLights)
                    Destroy(light.gameObject);
            }

            Main.Instance.productController.store.DestroyAll();

            Main.Instance.productController.Load(productJson, (GameObject product) =>
            {
                Debug.Log("[ProductViewController] Send to android load product is done.");

                // callback to native
                getMobileManager().CallbackLoadProduct("done");
                //#if UNITY_ANDROID && !UNITY_EDITOR
                //                AndroidManager.Instance.CallJavaFunc("LoadProductCallback", "done");
                //#endif
                // camera view position init
                Main.Instance.observerController.SetProductView();

                // light ON
                Debug.Log($"product transform " + product.transform.position);
                
                float positionFactor = product.transform.position.y * 2.5f;
                _InstantiatePointLight(positionFactor, product);

            });
        }
    }

    void _InstantiatePointLight(float positionFactor, GameObject product)
    {
        if (_pointLightsForProduct != null && _pointLightsForProduct.Length == 4)
        {
            for (int i = 0; i < 4; ++i)
            {
                _pointLightsForProduct[i] = Instantiate(pointLightPrefab, product.transform);
                if (_pointLightsForProduct[i])
                {
                    float xSign = 1;
                    float zSign = 1;
                    if (i / 2 == 0)
                    {
                        if (i % 2 == 0)
                        {
                            xSign = -1f;
                            zSign = -1f;
                        }
                        else
                        {
                            xSign = -1f;
                            zSign = 1f;
                        }
                    }
                    else
                    {
                        if (i % 2 == 0)
                        {
                            xSign = 1f;
                            zSign = -1f;
                        }
                        else
                        {
                            xSign = 1f;
                            zSign = 1f;
                        }
                    }
                    _pointLightsForProduct[i].transform.position = new Vector3(positionFactor * xSign, positionFactor, positionFactor * zSign);
                }
            }
        }
    }


}
