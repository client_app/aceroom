﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class DefaultMainCamera : AceSingleton<DefaultMainCamera>
{
   void Awake()
    {
        Log("DefautMainCamera Awake");
        Log("DefautMainCamera isReloadScene = " + isReloadScene);
        //DefaultScene이 실행되면 네이티브에서는 메인 메뉴 화면이 출력되어야 함.
        //Android의 경우 Controller가 Camera보다 Start 메소드 호출이 더 빠름.
        //iOS는 DefaultSceneController.cs 에서 호출
        if (isReloadScene == false)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                string oldSceneName = getOldAceSceneName();
                if (oldSceneName == null)
                {
                    oldSceneName = GetAceSceneName();
                }

                if (DefineWord.__IS_TEST == false)
                {
                    getMobileManager().CallMainMenu(oldSceneName);
                }
            }
        }
        else
        {
            isReloadScene = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
}
