﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/// <summary>
/// [테스트용] DefaultScene에서 각 기능 테스트용 UI 컨트롤러
/// </summary>
public class DefaultSceneUIController : BaseController
{

    public Button btnLoadARTargetScene;
    public Button btnLoadRoomScene;
    public Button btnLoadUrbanRoomScene;
    public Button btnLoadUrbanProductViewerScene;
    public Button btnLoadModeltTargetScene;


    public Button btnCallToast;
    public Button btnVersionCheck;


    public Button btnLoadVuTestScene1;
    public Button btnLoadVuTestScene2;

    public GameObject panelTest;

    void Awake()
    {
        Debug.Log("AndroidMainActivityController Awake", gameObject);
        
        
    }

        // Start is called before the first frame update
        void Start()
    {
        Log("DefaultSceneUIController Start()");

        //테스트용
        if (DefineWord.__IS_TEST)
        {
            panelTest.SetActive(DefineWord.__IS_TEST);

            btnLoadARTargetScene.onClick.AddListener(() => { onClickLoadARTargetScene(); });
            btnLoadRoomScene.onClick.AddListener(() => { onClickRoomScene(); });
            btnLoadUrbanRoomScene.onClick.AddListener(() => { onClickUrbanRoomScene(); });
            btnLoadUrbanProductViewerScene.onClick.AddListener(() => { onClickUrbanProductViewerScene(); });

            btnLoadModeltTargetScene.onClick.AddListener(() => { getMobileManager().CallChangeSceneModelTarget(null); });

            btnVersionCheck.onClick.AddListener(() => { OnClickVersionCheck(); });
            btnCallToast.onClick.AddListener(() => { OnClickToast(); });

            btnLoadVuTestScene1.onClick.AddListener(() => { OnClickVuTest(1); });
            btnLoadVuTestScene2.onClick.AddListener(() => { OnClickVuTest(2); });
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClickToast()
    {
        Log("OnClickToast");
        getMobileManager().CallShowToast("OnClickToast");
    }

    public void OnClickVersionCheck()
    {
        Log("OnClickVersionCheck");
        getMobileManager().CallDeviceVersionCheck();
    }

    public void onClickLoadARTargetScene()
    {
        Log("onClickLoadARTargetScene");
        getMobileManager().CallChangeSceneARMattress("");
    }

    public void onClickLoadMattressScene()
    {
        Log("onClickLoadARTargetScene");
        getMobileManager().CallChangeSceneMattress("");
    }

    public void onClickRoomScene()
    {
        Log("onClickLoadARTargetScene");
        //LoadScene("HomeEditorScene");

        getMobileManager().setIsNotch("true");

        //getMobileManager().CallChangeSceneHomeEditor("{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1ba71da53ab8ad5d2e7/5600c1ba71da53ab8ad5d2e7.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}");
        getMobileManager().CallChangeSceneHomeEditor("{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"http://dtribe.youyoung.net/sampleUnit/06721e4f-6f41-4c99-8b7d-881cc079fb86.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}");
    }

    public void onClickUrbanRoomScene()
    {
        Log("onClickUrbanRoomScene");
        //LoadScene("HomeEditorScene");

        getMobileManager().CallChangeSceneUrbanHomeEditor("{\"url\":\"http://dtribe.youyoung.net/product/LUNATO3_K_Gray.zip\",\"format\":\".zip\",\"planeType\":\"floor\",\"mattressUrl\":\"https://s3.ap-northeast-2.amazonaws.com/acebed-3d-assets-dev/product/HT3_K.zip\",\"mattressFormat\":\".zip\"}");
    }

    public void onClickUrbanProductViewerScene()
    {
        Log("onClickUrbanProductViewerScene");

        getMobileManager().CallChangeSceneUrbanProductViewer("");
    }

    public void onClickTest(){
        Log("DefaultSceneUIController Start()");
    }



    public void OnClickVuTest(int testNum)
    {
        ChangeAceSceneModeOnly(ACESceneMode.ARMattress);
        switch (testNum)
        {
            case 1:
                {
                    //ChangeSceneNonLoading("TEST1_ARMattressScene");
                    ChangeSceneNonLoading("UrbanHomeEditorScene");
                    
                }
                break;
            case 2:
                {
                    ChangeSceneNonLoading("TEST2_ARMattressScene");
                }
                break;
            default:
                {

                }
                break;
        }
    }








    public int getNowMilisec()
    {
        DateTime dateTime = System.DateTime.Now;
        int milsec = (dateTime.Minute * 60 * 1000) + (dateTime.Second * 1000) + dateTime.Millisecond;

        return milsec;
    }
}
