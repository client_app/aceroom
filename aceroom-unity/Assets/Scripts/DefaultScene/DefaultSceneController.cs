﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Runtime.InteropServices;

/// <summary>
/// 최초 실행 Scene인 DefaultScene 컨트롤러
/// </summary>
public class DefaultSceneController : AceSingleton<RoomSceneUIController>
{
    void Awake()
    {
    }


    // Start is called before the first frame update
    void Start()
    {
        //DefaultScene이 실행되면 네이티브에서는 메인 메뉴 화면이 출력되어야 함.
        //iOS의 경우 Camera보다 Controller의 Start 메소드 호출이 더 빠름.
        //Android는 DefaultMainCamera.cs 에서 호출
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            string oldSceneName = getOldAceSceneName();
            if (oldSceneName == null)
            {
                oldSceneName = GetAceSceneName();
            }

            if (DefineWord.__IS_TEST == false)
            {
                getMobileManager().CallMainMenu(oldSceneName);
            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    
}
