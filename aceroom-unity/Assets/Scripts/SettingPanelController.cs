﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DLLCore;

/// <summary>
/// [어반베이스] Crane 용 컴포넌트
/// 수정 시 어반베이스에 문의 필요.
/// </summary>
public class SettingPanelController : MonoBehaviour
{
    public Button closeButton;

    // quality level
    public Toggle highToggle;
    public Toggle middleToggle;
    public Toggle lowToggle;

    enum ToggleLevel
    {
        HIGHLEVEL,
        MIDLEVEL,
        LOWLEVEL
    }

    // toggle on is wall collide off, toggle off is wall collide on
    public Toggle wallColliderOffToggle;

    public Button linkButton;

    // Start is called before the first frame update
    void Start()
    {
        closeButton.onClick.AddListener(() => { OnClickClose(); });
        highToggle.onValueChanged.AddListener((value) => { OnValueChangedToggle(ToggleLevel.HIGHLEVEL, value); });
        middleToggle.onValueChanged.AddListener((value) => { OnValueChangedToggle(ToggleLevel.MIDLEVEL, value); });
        lowToggle.onValueChanged.AddListener((value) => { OnValueChangedToggle(ToggleLevel.LOWLEVEL, value); });
        wallColliderOffToggle.onValueChanged.AddListener((value) => { OnValueChangedDisableWallCollider(value); });
        linkButton.onClick.AddListener(() => { OnClickLink(); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnClickClose()
    {
        this.gameObject.SetActive(false);
    }

    void OnValueChangedToggle(ToggleLevel level, bool value)
    {
        switch(level)
        {
            case ToggleLevel.HIGHLEVEL:
                Main.Instance.graphicSetting.SetShadowType("soft");
                //Main.Instance.graphicSetting.SetVSync(2);
                break;

            case ToggleLevel.MIDLEVEL:
                Main.Instance.graphicSetting.SetShadowType("hard");
                //Main.Instance.graphicSetting.SetVSync(1);
                break;

            case ToggleLevel.LOWLEVEL:
                Main.Instance.graphicSetting.SetShadowType("none");
                Main.Instance.graphicSetting.SetVSync(0);
                break;
        }
    }

    void OnValueChangedDisableWallCollider(bool value)
    {
        // value is true, disable wall collide
        Main.Instance.unitController.ActivateWallCollider(value ? 0 : 1);
    }

    void OnClickLink()
    {

    }

}
