﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// [테스트용] 화면에 표시된 애니메이션 오브젝트의 사이즈를 조절
/// </summary>
public class TestScaleController : MonoBehaviour
{

    public Button btnSizeUp;
    public Button btnSizeDown;
    public Text textSize;

    // Start is called before the first frame update
    void Start()
    {
        if (!DefineWord.__IS_TEST) return;

        updateSize(0);

        if (btnSizeUp != null)
        {
            btnSizeUp.onClick.AddListener(() => {
                updateSize(1);
            });
        }
        if(btnSizeDown != null)
        {
            btnSizeDown.onClick.AddListener(() => {
                updateSize(-1);
            });
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void updateSize(int addSize)
    {
        Debug.Log("add size : "+ addSize);
        ARUIController.Instance.setObjectScale(ARUIController.Instance.getObjectScale() + addSize);
        //ARUIController.Instance.IntAniObjScale = ARUIController.Instance.getObjectScale + addSize;
        textSize.text = ARUIController.Instance.getObjectScale() + "";
    }
}
