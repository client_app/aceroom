﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using Vuforia;

/// <summary>
/// 매트리스 분석 Scene에서 UI기능 구현 컴포넌트
/// </summary>
public class ARUIController : AceSingleton<ARUIController>
{
    /// <summary> 애니메이션 동작 상태 코드값 enum </summary>
    public enum GUIState : byte { NonDetected = 0, Detected, AnimationStart, AnimationEnd }

    /// <summary> 애니메이션 객체 코드값 enum </summary>
    public enum MattressAnimation { Animation1 = 0, Animation2 = 1, Animation3 = 2 }

    /// <summary> 애니메이션 객체 prefab 이름 </summary>
    public string[] animationNameList = { "Action-1", "Action-2", "Action-3" };

    /// <summary> Vuforia에서 Target을 찾았는지 여부 </summary>
    bool _IsDetected;

    public Button btnBack;
    public Button btnTutorial;

    public GameObject guidePanel;
    public GameObject panelMenu;
    public Button btnAni1;
    public Button btnAni2;
    public Button btnAni3;

    public GameObject panelEnd;
    public Button btnMoveMenu;

    /// <summary> 찾은 객체의 Y축으로 공중에 띄워져 있는 객체(애니메이션 재생 시 기준 오브젝트) </summary>
    public GameObject midAirPosition;

    /// <summary> 애니메이션 객체의 스케일 </summary>
    int IntAniObjScale = 1;

    /// <summary> 지금 재생중인 애니메이션 객체의 이름 </summary>
    string animationName;
    /// <summary> 애니메이션 객체의 동작 상태 코드값 GUIState </summary>
    GUIState lastState;

    /// <summary>
    /// 애니메이션 오브젝트의 크기 배율을 변경한다
    /// </summary>
    /// <param name="scale">변경할 오브젝트의 크기 배율(기본 1)</param>
    public void setObjectScale(int scale)
    {
        IntAniObjScale = scale;
    }

    /// <summary>
    /// 애니메이션 오브젝트의 크기 배율을 반환한다
    /// </summary>
    /// <returns>오브젝트의 크기 배율(기본 1)</returns>
    public int getObjectScale()
    {
        return IntAniObjScale;
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log("ARUI-----------------------------");
        //ChangeAceSceneMode(ACESceneMode.ARMattress);
        if (btnBack != null)
        {
            btnBack.onClick.AddListener(() => {
                Log("DefaultScene Load");
                ///Scene을 변경하기 이전에 애니메이션 오브젝트와 기타 화면에 추가한 오브젝트를 제거
                removeAnimationObject();

                getMobileManager().CallChangeSceneDefault("");
            });
        }

        if (btnTutorial != null)
        {
            btnTutorial.onClick.AddListener(() => {
                Debug.Log(this.gameObject.name);
                //native call -> 튜토리얼

                //튜토리얼 화면 표시 전 재생중인 애니메이션을 제거 및 상태 초기화(일시정지 없음)
                removeAnimationObject();
                _IsDetected = false;
                GUIStateChange(GUIState.NonDetected);

                //각 Native 단 튜토리얼 화면 표시
                getMobileManager().CallTutorial();

            });
        }

        //Button Event
        if (btnAni1 != null)
        {
            btnAni1.onClick.AddListener(() =>
            {
                SendAdbrixAction(MattressAnimation.Animation1);
                StartCoroutine(PlayAnimation(MattressAnimation.Animation1));
            });
        }
        if (btnAni2 != null)
        {
            btnAni2.onClick.AddListener(() =>
            {
                SendAdbrixAction(MattressAnimation.Animation1);
                StartCoroutine(PlayAnimation(MattressAnimation.Animation2));
            });
        }
        if (btnAni3 != null)
        {
            btnAni3.onClick.AddListener(() =>
            {
                SendAdbrixAction(MattressAnimation.Animation1);
                StartCoroutine(PlayAnimation(MattressAnimation.Animation3));
            });
        }

        if (btnMoveMenu != null)
        {
            btnMoveMenu.onClick.AddListener(() =>
            {
                ///애니메이션 선택 버튼 화면으로 이동 전 화면에 표시된 애니메이션을 제거 및 상태 초기화(일시정지 없음)
                removeAnimationObject();
                removeAllObject();
                GUIStateChange(GUIState.Detected);
            });
        }

        GUIStateChange(GUIState.NonDetected);
    }

    public void SendAdbrixAction(MattressAnimation aniNo)
    {
        JSONObject actionJSONObj = new JSONObject();
        actionJSONObj.AddField("action", "AR3_AR_btn");

        if (aniNo == MattressAnimation.Animation1)
        {
            actionJSONObj.AddField("name", "spring");
        }
        else if (aniNo == MattressAnimation.Animation2)
        {
            actionJSONObj.AddField("name", "allinone");
        }
        else if (aniNo == MattressAnimation.Animation3)
        {
            actionJSONObj.AddField("name", "twomatt");
        }

        getMobileManager().CallAdbrixCustomAction(actionJSONObj.ToString());
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// AR마커의 탐색 상태 변경
    /// </summary>
    /// <param name="isFind">찾았는지 여부</param>
    public void FindedMarker(bool isFind)
    {
        if (guidePanel == null)
        {
            guidePanel = getGameObject("GuidePanel");
        }

        //UI배치상 한번만 실행
        if (_IsDetected == false)
        {
            GUIStateChange(isFind ? GUIState.Detected : GUIState.NonDetected);

            _IsDetected = true;
        }
        

    }

    /// <summary>
    /// 각 상태에 따라 화면 구성을 변경하는 매소드
    /// </summary>
    /// <param name="state"></param>
    public void GUIStateChange(GUIState state)
    {
        lastState = state;
        switch (state)
        {
            case GUIState.NonDetected:              //못 찾은 경우
                {
                    panelMenu.SetActive(false);
                    guidePanel.SetActive(true);
                    panelEnd.SetActive(false);
                }
                break;
            case GUIState.Detected:                 //찾은 경우
                {
                    panelMenu.SetActive(true);
                    guidePanel.SetActive(false);
                    panelEnd.SetActive(false);
                }
                break;
            case GUIState.AnimationStart:           //찾고 애니메이션이 시작된 경우
                {
                    panelMenu.SetActive(false);
                    guidePanel.SetActive(false);
                    panelEnd.SetActive(false);
                }
                break;
            case GUIState.AnimationEnd:             //찾고 애니메이션이 재생 끝난 경우
                {
                    //panelMenu.SetActive(false);
                    //guidePanel.SetActive(false);
                    //panelEnd.SetActive(true);
                    removeAnimationObject();
                    GUIStateChange(GUIState.Detected);
                }
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// 파라미터에 맞춰 애니메이션 오브젝트를 화면에 추가하고 재생
    /// </summary>
    /// <param name="aniNo">애니메이션 객체 코드값(MattressAnimation)</param>
    /// <returns>IEnumerator</returns>
    IEnumerator PlayAnimation(MattressAnimation aniNo)
    {
        removeAnimationObject();
        GUIStateChange(GUIState.AnimationStart);

        if (aniNo == MattressAnimation.Animation1)
        {
            animationName = "Action-1";
        }
        else if (aniNo == MattressAnimation.Animation2)
        {
            animationName = "Action-2";
        }
        else if (aniNo == MattressAnimation.Animation3)
        {
            animationName = "Action-3";
        }

        //    public enum MattressAnimation { Animation1 = 0, Animation2 = 1, Animation3 = 2 }
        //public string[] animationNameList = { "Action-1", "Action-2", "Action-3" };


        //animationName = animationNameList[(int)aniNo];
        Vector3 position = Vector3.zero;
        Log("Animation Instantiate : "+ animationName);
        //GameObject prefab = Resources.Load("Prefabs/MattressAnimation/" + animationName) as GameObject;

        ResourceRequest loadAsync = Resources.LoadAsync("Prefabs/MattressAnimation/" + animationName, typeof(GameObject));

        //Wait till we are done loading
        while (!loadAsync.isDone)
        {
            Debug.Log("Load Progress: " + loadAsync.progress);
            yield return null;
        }

        //Get the loaded data
        GameObject prefab = loadAsync.asset as GameObject;

        // Resources/Prefabs/Bullet.prefab 로드
        GameObject matress = Instantiate(prefab) as GameObject;
        // 실제 인스턴스 생성. GameObject name의 기본값은 Bullet (clone)
        matress.name = animationName; // name을 변경
        matress.transform.parent = midAirPosition.transform;
        // bullet을 player에 입양하는등 초기화작업 수행

        // Widen the object by x, y, and z value
        matress.transform.localPosition = position;
        matress.transform.localRotation = Quaternion.identity;
        matress.transform.localScale = Vector3.one * IntAniObjScale;
    }

    /// <summary>
    /// 애니메이션 오브젝트의 부모객체인 midAirPotision의 자식객체를 제거
    /// </summary>
    void removeAnimationObject()
    {
        foreach (Transform tf in midAirPosition.transform)
        {
            Destroy(tf.gameObject);
        }
    }

    /// <summary>
    /// Scene 내 모든 객체를 제거
    /// </summary>
    void removeAllObject()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void onAnimationStart(string animName)
    {
        Log("Animation Start - " + animName);
    }

    public void onAnimationEnd(string animName)
    {
        Log("Animation End - " + animName);
        GUIStateChange(GUIState.AnimationEnd);
    }

    
    GameObject getGameObject(string objectName)
    {
        return GameObject.Find(objectName);
    }


    public override void Log(string message)
    {
        if (DefineWord.__IS_SHOW_LOG)
        {
            Debug.Log("[Unity] "+ "[" + this.gameObject.name + "]" + message, this);
        }
    }
}
