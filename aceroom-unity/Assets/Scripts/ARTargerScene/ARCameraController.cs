﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

/// <summary>
/// Vuforia AR Camera 객체를 앱 사용성에 맞게 다루기 위한 컴포넌트
/// </summary>
public class ARCameraController : MonoBehaviour
{
    /// <summary> VuforiaRuntime 실행 여부 플래그 </summary>
    static bool isInitVuforia = false;
    // Start is called before the first frame update
    void Start()
    {
        //VuforiaRuntime은 Scene이 다시 로드가 되어도 1번만 initialize 되어야 한다.(중복적인 Initialization 시 앱 크래시)
        Debug.Log("Vuforia Initialize");
        if(isInitVuforia == false)
        {
            isInitVuforia = true;
            VuforiaRuntime.Instance.InitVuforia();
        }
        
        gameObject.GetComponent<VuforiaBehaviour>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        Debug.Log("Vuforia DeInitialize");
        //VuforiaRuntime.Instance.Deinit();
        //gameObject.GetComponent<VuforiaBehaviour>().enabled = false;
    }
}
