﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// [사용 안함] 애니메이션 시작 시 컨트롤
/// </summary>
public class AceAnimationHandler : MonoBehaviour
{
    Renderer m_ObjectRenderer;
    [SerializeField] private float fadePerSecond = 2.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        //var material = GetComponent<Renderer>().material;
        //var color = material.color;

        //material.color = new Color(color.r, color.g, color.b, color.a - (fadePerSecond * Time.deltaTime));
    }

    /// <summary>
    /// 애니메이션 시작 시 Fade In 효과
    /// </summary>
    /// <param name="second">Fade In 효과를 처리할 시간</param>
    public void FadeIn(float second)
    {
        StartCoroutine(FadeTo(1.0f, second));
    }
    
    /// <summary>
    /// 애니메이션 시작 시 Fade Out 효과
    /// </summary>
    /// <param name="second">Fade Out 효과를 처리할 시간</param>
    public void FadeOut(float second)
    {
        StartCoroutine(FadeTo(1.0f, second));
    }

    /// <summary>
    /// Fade In/Out 효과 처리를 위한 애니메이션 처리
    /// </summary>
    /// <param name="aValue">Fade 효과가 끝날 시 나타날 투명값(0.0 = 투명, 1.0 = 불투명)</param>
    /// <param name="aTime">Fade In/Out 효과를 처리할 시간</param>
    /// <returns></returns>
    IEnumerator FadeTo(float aValue, float aTime)
    {
        if(m_ObjectRenderer == null)
        {
            m_ObjectRenderer = GetComponent<Renderer>();
        }

        //float alpha = m_ObjectRenderer.material.color.a;
        //for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        //{
        //    Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
        //    m_ObjectRenderer.material.color = newColor;
        //    yield return null;
        //}
        //matress.transform.localScale += new Vector3(x, y, z);

        float defaultSize = 0.0f;
        
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            float size = Mathf.Lerp(defaultSize, aValue, t);
            Debug.Log("Size : " + size);
            gameObject.transform.localScale = new Vector3(size, size, size);
            yield return null;
        }

    }
}
