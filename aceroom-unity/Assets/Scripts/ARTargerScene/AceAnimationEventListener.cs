﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AceAnimationEventListener : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onAnimationStart(string AnimationName)
    {
        Debug.Log("OnAnimation Start - " + AnimationName);
        ARUIController.Instance.onAnimationStart(AnimationName);
    }

    public void onAnimationEnd(string AnimationName)
    {
        Debug.Log("OnAnimation End - "+ AnimationName);
        ARUIController.Instance.onAnimationEnd(AnimationName);
    }
}
