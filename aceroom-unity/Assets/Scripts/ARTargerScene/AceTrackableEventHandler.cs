﻿using UnityEngine;
using UnityEngine.UI;
using Vuforia;

/// <summary>
/// Vuforia의 Tracking 상태를 받아오는 Handler
/// 참고 : Vuforia.Script.DefaultTrackableEventHandler.cs 를 용도에 맞게 코드 추가함.
/// </summary>
public class AceTrackableEventHandler : BaseController, ITrackableEventHandler
{
	#region PROTECTED_MEMBER_VARIABLES

	protected TrackableBehaviour mTrackableBehaviour;
	protected TrackableBehaviour.Status m_PreviousStatus;
	protected TrackableBehaviour.Status m_NewStatus;

    #endregion // PROTECTED_MEMBER_VARIABLES


    #region UI_MEMBER_VARIABLES
    private GameObject guidePanel;
    private Text textTestInfo;
    #endregion // UI_MEMBER_VARIABLES

    #region UNITY_MONOBEHAVIOUR_METHODS


    void Awake()
    {
       
    }

    protected virtual void Start()
	{
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
			mTrackableBehaviour.RegisterTrackableEventHandler(this);

    }

	protected virtual void OnDestroy()
	{
		if (mTrackableBehaviour)
			mTrackableBehaviour.UnregisterTrackableEventHandler(this);
	}

	#endregion // UNITY_MONOBEHAVIOUR_METHODS

	#region PUBLIC_METHODS

	/// <summary>
	///     Implementation of the ITrackableEventHandler function called when the
	///     tracking state changes.
	/// </summary>
	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		m_PreviousStatus = previousStatus;
		m_NewStatus = newStatus;

		Debug.Log("Trackable " + mTrackableBehaviour.TrackableName +
				  " " + mTrackableBehaviour.CurrentStatus +
				  " -- " + mTrackableBehaviour.CurrentStatusInfo);


        if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			OnTrackingFound();
		}
		else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
				 newStatus == TrackableBehaviour.Status.NO_POSE)
		{
			OnTrackingLost();
		}
		else
		{
			// For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
			// Vuforia is starting, but tracking has not been lost or found yet
			// Call OnTrackingLost() to hide the augmentations
			OnTrackingLost();
		}
	}

    #endregion // PUBLIC_METHODS

    #region PROTECTED_METHODS

    int count = 0;

	protected virtual void OnTrackingFound()
	{
        if (mTrackableBehaviour)
        {
            //Debug.Log("[" + count + "]OnTrackingFound" + mTrackableBehaviour.TrackableName +
            //      " " + mTrackableBehaviour.CurrentStatus +
            //      " -- " + mTrackableBehaviour.CurrentStatusInfo);


            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = mTrackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = mTrackableBehaviour.GetComponentsInChildren<Canvas>(true);

            // Enable rendering:
            foreach (var component in rendererComponents)
                component.enabled = true;

            // Enable colliders:
            foreach (var component in colliderComponents)
                component.enabled = true;

            // Enable canvas':
            foreach (var component in canvasComponents)
                component.enabled = true;

            ARUIController.Instance.FindedMarker(true);
        }

        //count++;

        //      if(count == 10)
        //      {


        Debug.Log("OnTrackingStop" + mTrackableBehaviour.TrackableName +
              " " + mTrackableBehaviour.CurrentStatus +
              " -- " + mTrackableBehaviour.CurrentStatusInfo);

        //GameObject prefab = Resources.Load("Prefabs/MattressAnimation/01_zspring") as GameObject;
        //// Resources/Prefabs/Bullet.prefab 로드
        //GameObject matress = Instantiate(prefab) as GameObject;
        //// 실제 인스턴스 생성. GameObject name의 기본값은 Bullet (clone)
        //matress.name = "matress"; // name을 변경
        //matress.transform.parent = gameObject.transform;
        //// bullet을 player에 입양하는등 초기화작업 수행
        //float x = 1f;
        //float y = 1f;
        //float z = 1f;
        //// Widen the object by x, y, and z value
        //matress.transform.position = mTrackableBehaviour.transform.position;
        //matress.transform.localScale += new Vector3(x, y, z);
        //matress.transform.localScale = new Vector3(1f, 1f, 1f);


        //Tracker imageTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        //if (imageTracker != null)
        //{
        //    imageTracker.Stop();

        //}

        //}

    }


    protected virtual void OnTrackingLost()
	{


        /*
        if (mTrackableBehaviour && count <= 10)
        {
            Debug.Log("OnTrackingLost" + mTrackableBehaviour.TrackableName +
                  " " + mTrackableBehaviour.CurrentStatus +
                  " -- " + mTrackableBehaviour.CurrentStatusInfo);

            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = mTrackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = mTrackableBehaviour.GetComponentsInChildren<Canvas>(true);

            // Disable rendering:
            foreach (var component in rendererComponents)
                component.enabled = false;

            // Disable colliders:
            foreach (var component in colliderComponents)
                component.enabled = false;

            // Disable canvas':
            foreach (var component in canvasComponents)
                component.enabled = false;

            ARUIController.Instance.FindedMarker(false);
        }
        */
    }

	#endregion // PROTECTED_METHODS
}