﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// [테스트용] [사용안함] Vuforia Model Target 기능 테스트용 틀래스
/// </summary>
public class ModelTargetMenuController : AceSingleton<ModelTargetMenuController>
{
    public Button buttonLunato3;
    public Button buttonBra1439;
    public Button buttonLunato3Real;
    public Button buttonBra1439Real;
    public Button buttonTestBox;
    public Button buttonTestImage;

    public enum ModelTarget : byte { Lunato = 0, LunatoReal, Bra1439, Bra1439Real, TestBox, TestImage }
    
    // Start is called before the first frame update
    void Start()
    {

        if (buttonLunato3 != null)
        {
            buttonLunato3.onClick.AddListener(() => {
                Log("buttonLunato3 Click");
                modelChange(ModelTarget.Lunato);
            });
        }

        if (buttonLunato3Real != null)
        {
            buttonLunato3Real.onClick.AddListener(() => {
                Log("buttonBra1439 Click");
                modelChange(ModelTarget.LunatoReal);
            });
        }

        if (buttonBra1439 != null)
        {
            buttonBra1439.onClick.AddListener(() => {
                Log("buttonLunato3 Click");
                modelChange(ModelTarget.Bra1439);
            });
        }

        if (buttonBra1439Real != null)
        {
            buttonBra1439Real.onClick.AddListener(() => {
                Log("buttonBra1439 Click");
                modelChange(ModelTarget.Bra1439Real);
            });
        }

        if (buttonTestBox != null)
        {
            buttonTestBox.onClick.AddListener(() => {
                Log("buttonTestBox Click");
                modelChange(ModelTarget.TestBox);
            });
        }

        if (buttonTestImage != null)
        {
            buttonTestImage.onClick.AddListener(() => {
                Log("buttonTestImage Click");
                modelChange(ModelTarget.TestImage);
            });
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }



    void modelChange(ModelTarget target)
    {
        string infoString = "인식 대상 : ";
        //string modelPrefabNameString = "Prefabs/ModelTarget/";
        string modelPrefabNameString = "";
        switch (target)
        {
            case ModelTarget.Lunato:
                {
                    infoString += "Lunato3 + HT3";
                    modelPrefabNameString += "lunato3_ht3";
                }
                break;
            case ModelTarget.Bra1439:
                {
                    infoString += "BAR1439 + HT3";
                    modelPrefabNameString += "bra1439_ht3";
                }
                break;
            case ModelTarget.TestBox:
                {
                    infoString += "Test Box";
                    modelPrefabNameString += "Test_Box";
                }
                break;
            case ModelTarget.TestImage:
                {
                    infoString += "Test Image";
                    modelPrefabNameString += "ImageMarkerTest";
                }
                break;
            default:
                {
                    Debug.Log("modelChange Code Unknown = " + target);
                    infoString += "unknown";
                    modelPrefabNameString += "Test_Box";
                }
                break;
        }


        //Debug.Log("modelChange Code = " + modelPrefabNameString);
        //labelInfo.text = infoString;
        //StartCoroutine(addModelTarget(modelPrefabNameString));

        //dataSetName = modelPrefabNameString;
        //VuforiaARController.Instance.RegisterVuforiaStartedCallback(LoadDataSet);

    }


    IEnumerator LoadScene(string modelTargetName)
    {
        //로딩 시작
        getMobileManager().SceneLoadStart();

        Log("BaseController - modelTargetName(" + modelTargetName + ")");
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("ModelTargetMenuScene", LoadSceneMode.Single);
        while (!async.isDone)
        {
            //1프레임마다 검사
            Log("BaseController - LoadScene check - skip frame");
            yield return null;
            print(async.progress);
        }
        //로딩 종료
        Log("BaseController - LoadScene check - load complete");
        getMobileManager().SceneLoadComplete();
    }
}
