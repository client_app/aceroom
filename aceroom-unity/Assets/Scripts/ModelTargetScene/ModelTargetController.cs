﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

/// <summary>
/// [테스트용] [사용안함] Vuforia Model Target 기능 테스트용 틀래스
/// </summary>
public class ModelTargetController : AceSingleton<ModelTargetController>
{
    public Button buttonLunato3;
    public Button buttonBra1439;
    public Button buttonLunato3Real;
    public Button buttonBra1439Real;
    public Button buttonTestBox;
    public Button buttonTestImage;
    public Text labelInfo;
    public Text labelDetecting;

    public enum ModelTarget : byte { Lunato = 0, LunatoReal, Bra1439, Bra1439Real,  TestBox, TestImage }


    // Start is called before the first frame update
    void Start()
    {
        FindModel(false);

        VuforiaRuntime.Instance.InitVuforia();
        Camera.main.gameObject.GetComponent<VuforiaBehaviour>().enabled = true;
        VuforiaBehaviour.Instance.enabled = false;

        if (buttonLunato3 != null)
        {
            buttonLunato3.onClick.AddListener(() => {
                Log("buttonLunato3 Click");
                modelChange(ModelTarget.Lunato);
            });
        }

        if (buttonBra1439 != null)
        {
            buttonBra1439.onClick.AddListener(() => {
                Log("buttonBra1439 Click");
                modelChange(ModelTarget.Bra1439);
            });
        }

        if (buttonLunato3Real != null)
        {
            buttonLunato3Real.onClick.AddListener(() => {
                Log("buttonLunato3Real Click");
                modelChange(ModelTarget.LunatoReal);
            });
        }

        if (buttonBra1439Real != null)
        {
            buttonBra1439Real.onClick.AddListener(() => {
                Log("buttonBra1439Real Click");
                modelChange(ModelTarget.Bra1439Real);
            });
        }

        if (buttonTestBox != null)
        {
            buttonTestBox.onClick.AddListener(() => {
                Log("buttonTestBox Click");
                modelChange(ModelTarget.TestBox);
            });
        }

        if (buttonTestImage != null)
        {
            buttonTestImage.onClick.AddListener(() => {
                Log("buttonTestImage Click");
                modelChange(ModelTarget.TestImage);
            });
        }

        StartCoroutine(addModelTarget());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void modelChange(ModelTarget target)
    {
        FindModel(false);
        //string modelPrefabNameString = "Prefabs/ModelTarget/";
        string modelPrefabNameString = "";
        switch (target)
        {
            case ModelTarget.Lunato:
                {
                    modelPrefabNameString += "lunato3_head_nonreal";
                }
                break;
            case ModelTarget.LunatoReal:
                {
                    modelPrefabNameString += "lunato3_head_real";
                }
                break;
            case ModelTarget.Bra1439:
                {
                    modelPrefabNameString += "bra_hean_nonreal";
                }
                break;
            case ModelTarget.Bra1439Real:
                {
                    modelPrefabNameString += "bra_hean_real";
                }
                break;
            case ModelTarget.TestBox:
                {
                    modelPrefabNameString += "Test_Box";
                }
                break;
            case ModelTarget.TestImage:
                {
                    modelPrefabNameString += "ImageMarkerTest";
                }
                break;
            default:
                {
                    Debug.Log("modelChange Code Unknown = " + target);
                    modelPrefabNameString += "Test_Box";
                }
                break;
        }

        getMobileManager().strModelTargetString = modelPrefabNameString;
        getMobileManager().reloadScene();

    }

    IEnumerator addModelTarget()
    {
        string prefabName = getMobileManager().strModelTargetString;

        VuforiaBehaviour.Instance.enabled = false;

        if(prefabName == null || prefabName.Length == 0)
        {
            labelInfo.text = "인식 대상 : 없음";
            yield break;
        }
        labelInfo.text = "인식 대상 : " + prefabName;

        Log("Model Target Instantiate : " + prefabName);
        //GameObject prefab = Resources.Load("Prefabs/MattressAnimation/" + animationName) as GameObject;

        ResourceRequest loadAsync = Resources.LoadAsync("Prefabs/ModelTarget/" + prefabName, typeof(GameObject));

        //Wait till we are done loading
        while (!loadAsync.isDone)
        {
            Debug.Log("Load Progress: " + loadAsync.progress);
            yield return null;
        }

        //Get the loaded data
        GameObject prefab = loadAsync.asset as GameObject;

        // Resources/Prefabs/Bullet.prefab 로드
        GameObject modelTarget = Instantiate(prefab) as GameObject;
        // 실제 인스턴스 생성. GameObject name의 기본값은 Bullet (clone)
        modelTarget.name = prefabName; // name을 변경
        Vector3 position = Vector3.zero;
        position.z = -10;
        modelTarget.transform.localPosition = position;

        Debug.Log("Instantiate name :  [" + modelTarget.name + "] tag : [" + modelTarget.tag + "]");


        //ModelTargetBehaviour

        VuforiaBehaviour.Instance.enabled = true;

    }

    public void FindModel(bool isFind)
    {
        if (isFind)
        {
            labelDetecting.text = "인식여부 : 인식됨";
        } else
        {
            labelDetecting.text = "인식여부 : 인식안됨";
        }
    }

    /*
    // specify these in Unity Inspector
    public GameObject augmentationObject = null;  // you can use teapot or other object
    public string dataSetName = "";  //  Assets/StreamingAssets/QCAR/DataSetName

    void LoadDataSet()
    {

        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

        DataSet dataSet = objectTracker.CreateDataSet();

        if (dataSet.Load(dataSetName))
        {

            objectTracker.Stop();  // stop tracker so that we can add new dataset

            if (!objectTracker.ActivateDataSet(dataSet))
            {
                // Note: ImageTracker cannot have more than 100 total targets activated
                Debug.Log("<color=yellow>Failed to Activate DataSet: " + dataSetName + "</color>");
            }

            if (!objectTracker.Start())
            {
                Debug.Log("<color=yellow>Tracker Failed to Start.</color>");
            }

            int counter = 0;

            IEnumerable<TrackableBehaviour> tbs = TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();
            foreach (TrackableBehaviour tb in tbs)
            {
                if (tb.name == "New Game Object")
                {

                    // change generic name to include trackable name
                    tb.gameObject.name = ++counter + ":DynamicImageTarget-" + tb.TrackableName;

                    // add additional script components for trackable
                    tb.gameObject.AddComponent<DefaultTrackableEventHandler>();
                    tb.gameObject.AddComponent<TurnOffBehaviour>();

                    if (augmentationObject != null)
                    {
                        // instantiate augmentation object and parent to trackable
                        GameObject augmentation = (GameObject)GameObject.Instantiate(augmentationObject);
                        augmentation.transform.parent = tb.gameObject.transform;
                        augmentation.transform.localPosition = new Vector3(0f, 0f, 0f);
                        augmentation.transform.localRotation = Quaternion.identity;
                        augmentation.transform.localScale = new Vector3(0.005f, 0.005f, 0.005f);
                        augmentation.gameObject.SetActive(true);
                    }
                    else
                    {
                        Debug.Log("<color=yellow>Warning: No augmentation object specified for: " + tb.TrackableName + "</color>");
                    }
                }
            }
        }
        else
        {
            Debug.LogError("<color=yellow>Failed to load dataset: '" + dataSetName + "'</color>");
        }
    }
    */
}
