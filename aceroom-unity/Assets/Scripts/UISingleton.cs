﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DLLCore;

/// <summary>
/// [어반베이스] Crane 용 컴포넌트
/// 수정 시 어반베이스에 문의 필요.
/// </summary>
public class UISingleton : Singleton<UISingleton>
{
    UIController _uiController;
    // Start is called before the first frame update
    void Start()
    {
        _uiController = GameObject.FindObjectOfType<UIController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public UIController GetUIController()
    {
        if (_uiController == null)
        {
            _uiController = GameObject.FindObjectOfType<UIController>();
            Debug.Log("ui controller " + _uiController);
        }
        
        return _uiController;
    }
}
