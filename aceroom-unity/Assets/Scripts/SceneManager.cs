﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

/// <summary>
/// [어반베이스] Crane 용 컴포넌트
/// 수정 시 어반베이스에 문의 필요.
/// </summary>
namespace DLLCore
{
    public class SceneManager : MonoBehaviour
    {
        public Button toAR;
        public Button toHomeDesign;

//#if UNITY_IPHONE
//        [DllImport("__Internal")]
//        private static extern int _startLensAR();
//#endif

        // Start is called before the first frame update
        void Start()
        {
            toAR.onClick.AddListener(() => { OnClickToAR(); });
            toHomeDesign.onClick.AddListener(() => { OnClickToHomeDesign(); });
        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnClickToAR()
        {
//#if UNITY_IPHONE
//        if (Application.platform == RuntimePlatform.IPhonePlayer)
//        {
//            int result = _startLensAR();
//        }
//#endif
        }

        void OnClickToHomeDesign()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene", UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
    }
}