﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// [테스트용] 해당 컴포넌트를 지닌 객체는 DefineWord.__IS_TEST 값에 따라 화면에 숨김 처리
/// </summary>
public class TestVisibleObj : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (DefineWord.__IS_TEST)
        {
            gameObject.SetActive(true);
        }
		else
		{
			gameObject.SetActive(false);
		}
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
