﻿using UnityEngine;
using System.Collections;

/// <summary>
/// [사용안함] 유니티 내부 팝업 UI 구성 시 사용
/// </summary>
public class ModalController : BaseController
{
    public delegate void CallbackType(string type);
    public delegate void Callback(string type);
    public delegate void CallbackClose();
    public delegate void CallbackPop();


    protected CallbackType callbackType = null;
    protected Callback callback = null;
    protected CallbackClose callbackClose = null;
    protected CallbackPop callbackPop = null;
}
