﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using DLLCore;

/// <summary>
/// 각 Controller의 공통 상속부
/// </summary>
public class BaseController : MonoBehaviour
{
    /// <summary>
    /// 유니티의 Scene 코드화를 위한 객체
    /// </summary>
    public enum ACESceneMode : byte {Default=1, ARMattress, ARPlacement, HomeEdit, UrbanHomeEdit, UrbanProductViewer, ARModelTargetMenu, ARModelTarget, Mattress}

    /// <summary>
    /// 지금 실행중인 Scene
    /// </summary>
    private static ACESceneMode _sceneMode = ACESceneMode.Default;

    /// <summary>
    /// 이전에 실행됐던 Scene
    /// </summary>
    private static ACESceneMode _oldSceneMode = ACESceneMode.Default;

    /// <summary>
    /// Android/iOS 네이티브 코드와 통신을 위한 객체
    /// </summary>
    public static MobileManager mobileManager;

    /// <summary>
    /// 지금 실행중인 Scene을 다시 로드할 때 반복실행 방지용 플래그
    /// </summary>
    public static bool isReloadScene = false;


    // Use this for initialization
    void Start()
    {
        this.Init();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnDestroy()
    {
        this.Destory();
    }

    public void Init()
    {
        //MobileManager 객체가 없는 경우, 생성함.
        Debug.Log("BaseController - Init()", gameObject);
        if(mobileManager == null)
        {
            Debug.Log("BaseController - MobileManager is null", gameObject);
            GameObject managerObj = GameObject.FindWithTag("MobileManager");
            mobileManager = (MobileManager)managerObj.GetComponent(typeof(MobileManager));
            // 모든 Scene에서 사용하며, 로딩화면을 컨트롤 하기도 하므로 DontDestroy로 처리
            DontDestroyOnLoad(mobileManager);
        }
    }

    public void Destory()
    {
        Object.Destroy(this.gameObject);
    }

    /// <summary>
    /// Scene의 물리 파일명으로 Scene을 전환한다. 전환 시 로딩화면을 사용한다.
    /// </summary>
    /// <param name="sceneName">변경할 Scene의 파일명</param>
    /// <returns>IEnumerator</returns>
    IEnumerator LoadScene(string sceneName)
    {
        //로딩 시작
        getMobileManager().SceneLoadStart();

        Log("BaseController - LoadScene("+ sceneName+")");
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        while (!async.isDone)
        {
            //1프레임마다 검사
            Log("BaseController - LoadScene check - skip frame");
            yield return null;
            print(async.progress);
        }
        //로딩 종료
        Log("BaseController - LoadScene check - load complete");
        getMobileManager().SceneLoadComplete();
    }

    /// <summary>
    /// [테스트용] Scene의 물리 파일명으로 Scene을 전환한다. 전환 시 로딩화면을 사용하지 않는다.
    /// </summary>
    /// <param name="sceneName">변경할 Scene의 파일명</param>
    /// <returns>IEnumerator</returns>
    IEnumerator LoadSceneNonLoading(string sceneName)
    {
        //로딩 시작
        //getMobileManager().SceneLoadStart();

        Log("BaseController - LoadSceneNonLoading(" + sceneName + ")");
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName, UnityEngine.SceneManagement.LoadSceneMode.Single);
        while (!async.isDone)
        {
            //1프레임마다 검사
            Log("BaseController - LoadScene check - skip frame");
            yield return null;
            print(async.progress);
        }
        //로딩 종료
        Log("BaseController - LoadScene check - load complete");
        getMobileManager().SceneLoadComplete();
    }

    /// <summary>
    /// Scene을 변경한다.
    /// </summary>
    /// <param name="mode">Scene 이름(Scene이름이 TestScene인 경우 Test 만 넣는다.)</param>
    public void ChangeAceSceneMode(string mode)
    {
        ACESceneMode modeCode = AceSceneModeNameToCode(mode);

        _oldSceneMode = _sceneMode;
        _sceneMode = modeCode;

        StartCoroutine(LoadScene(AceSceneName(modeCode)));
    }

    /// <summary>
    ///  [테스트용] 실행중인 Scene 이름만 변경한다.
    /// </summary>
    /// <param name="mode"></param>
    public void ChangeAceSceneModeOnly(ACESceneMode mode)
    {
        _oldSceneMode = _sceneMode;
        _sceneMode = mode;
    }
    
    /// <summary>
    /// [테스트용] 로딩 화면 없이 Scene을 변경한다.
    /// </summary>
    /// <param name="sceneName"></param>
    public void ChangeSceneNonLoading(string sceneName)
    {
        StartCoroutine(LoadSceneNonLoading(sceneName));

        AceSceneModeNameToCode("");
    }

    /// <summary>
    /// Scene의 이름(String) 으로 Scene 코드값(ACESceneMode) 반환
    /// </summary>
    /// <param name="sceneName">Scene 이름(Scene이름이 TestScene인 경우 Test 만 넣는다.)</param>
    /// <returns>ACESceneMode enum 코드값</returns>
    public ACESceneMode AceSceneModeNameToCode(string sceneName)
    {
        if (sceneName.Equals("ARMattress"))
        {
            return ACESceneMode.ARMattress;
        }
        else if (sceneName.Equals("HomeEditor"))
        {
            return ACESceneMode.HomeEdit;
        }
        else if (sceneName.Equals("UrbanHomeEditor"))
        {
            return ACESceneMode.UrbanHomeEdit;
        }
        else if (sceneName.Equals("UrbanProductViewer"))
        {
            return ACESceneMode.UrbanProductViewer;
        }
        else if (sceneName.Equals("ModelTargetMenu"))
        {
            return ACESceneMode.ARModelTargetMenu;
        }
        else if (sceneName.Equals("ModelTarget"))
        {
            return ACESceneMode.ARModelTarget;
        }
        else if (sceneName.Equals("Mattress"))
        {
            return ACESceneMode.Mattress;
        }
        else
        {
            return ACESceneMode.Default;
        }
    }

    /// <summary>
    /// Scene의 코드값(ACESceneMode) 로 Scene 이름(String) 반환
    /// </summary>
    /// <param name="code">ACESceneMode enum 코드값</param>
    /// <returns>Scene 이름(Scene이름이 TestScene인 경우 Test 만 반환)</returns>
    public string AceSceneCodeToString(ACESceneMode code)
    {
        if(code == ACESceneMode.ARMattress)
        {
            return "ARMattress";
        }
        else if (code == ACESceneMode.HomeEdit)
        {
            return "HomeEditor";
        }
        else if (code == ACESceneMode.UrbanHomeEdit)
        {
            return "UrbanHomeEditor";
        }
        else if (code == ACESceneMode.UrbanProductViewer)
        {
            return "UrbanProductViewer";
        }
        else if (code == ACESceneMode.ARModelTargetMenu)
        {
            return "ModelTargetMenu";
        }
        else if (code == ACESceneMode.ARModelTarget)
        {
            return "ModelTarget";
        }
        else if (code == ACESceneMode.Mattress)
        {
            return "Mattress";
        }
        else
        {
            return "Default";
        }
    }

    /// <summary>
    /// Scene의 코드값(ACESceneMode)을 Scene 물리파일명(String) 으로 변환
    /// </summary>
    /// <param name="code">ACESceneMode enum 코드값</param>
    /// <returns>Scene 이름(Scene이름이 "Test"인 경우 "TestScene" 반환)</returns>
    public string AceSceneName(ACESceneMode code)
    {
        return AceSceneCodeToString(code) + "Scene";
    }

    /// <summary>
    /// 지금 실행중인 Scene의 이름을 반환
    /// </summary>
    /// <returns>Scene 이름(Scene이름이 TestScene인 경우 Test 만 반환)</returns>
    public string GetAceSceneModeName()
    {
        return AceSceneCodeToString(_sceneMode);
    }

    /// <summary>
    /// 지금 실행중인 Scene의 물리파일명(String) 변환
    /// </summary>
    /// <returns>Scene 파일명</returns>
    public string GetAceSceneName()
    {
        return AceSceneName(_sceneMode);
    }

    /// <summary>
    /// 직전에 사용했던 cene의 물리파일명(String) 변환
    /// </summary>
    /// <returns>Scene 파일명</returns>
    public string getOldAceSceneName()
    {
        return AceSceneName(_oldSceneMode);
    }

    /// <summary>
    /// 지금 실행중인 Scene이 기본 Scene(DefaultScene)인지 확인
    /// </summary>
    /// <returns>지금 실행중인 Scene이 "DefaultScene" 인지 여부</returns>
    public bool isDefaultScene()
    {
        if (_sceneMode == ACESceneMode.Default)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 지금 실행중인 Scene이 홈에디터 Scene(HomeEditorScene)인지 확인
    /// </summary>
    /// <returns>지금 실행중인 Scene이 "HomeEditorScene" 인지 여부</returns>
    public bool isHomeEditorScene()
    {
        if (_sceneMode == ACESceneMode.HomeEdit)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 지금 실행중인 Scene이 매트리스 분석 Scene(ARMattressScene)인지 확인
    /// </summary>
    /// <returns>지금 실행중인 Scene이 "ARMattressScene" 인지 여부</returns>
    public bool isARMattressScene()
    {
        if (_sceneMode == ACESceneMode.ARMattress)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// [테스트용] 지금 실행중인 Scene이 어반베이스의 홈에디터 Scene(UrbanHomeEditorScene)인지 확인
    /// </summary>
    /// <returns>지금 실행중인 Scene이 "UrbanHomeEditorScene" 인지 여부</returns>
    public bool isUrbanHomeEdit()
    {
        if (_sceneMode == ACESceneMode.UrbanHomeEdit)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 지금 실행중인 Scene이 어반베이스의 제품 미리보기 Scene(UrbanProductViewerScene)인지 확인
    /// 안드로이드에서만 사용한다.
    /// </summary>
    /// <returns>지금 실행중인 Scene이 "UrbanProductViewerScene" 인지 여부</returns>
    public bool isUrbanProductViewerScene()
    {
        if (_sceneMode == ACESceneMode.UrbanProductViewer)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// [테스트용] 유니티 로그 남기기용.
    /// </summary>
    /// <param name="message"></param>
    virtual public void Log(string message)
    {
        if (DefineWord.__IS_SHOW_LOG)
        {
            Debug.Log("[Unity] " + message, this);
        }
    }

    /// <summary>
    /// MobileManager를 가져온다.
    /// MobileManager는 싱글톤 객체이지만, DontDestoryOnLoad로 처리하므로
    /// Scene을 이동한 경우 BaseController를 상속받은 타 컨트롤러는 포인터가 null 상태이므로 찾아서 포인터를 연결한다.
    /// </summary>
    /// <returns>MobileManager.Instance</returns>
    protected MobileManager getMobileManager()
    {
        Log("BaseController - getMobileManager");
        if (mobileManager == null)
        {
            Log("BaseController - MobileManager is null - found");
            GameObject managerObj = GameObject.FindWithTag("MobileManager");
            DontDestroyOnLoad(managerObj);
            mobileManager = (MobileManager)managerObj.GetComponent(typeof(MobileManager));
        }
        else
        {
            Log("BaseController - MobileManager is NOT null");
        }

        //return mobileManager;
        return MobileManager.Instance;
    }

    /// <summary>
    /// 지금 실행중인 Scene을 다시 로드한다.
    /// </summary>
    public void reloadScene()
    {
        isReloadScene = true;
        Log("BaseController - Reload Scene");
        StartCoroutine(reloadSceneProcess());
    }

    /// <summary>
    /// 지금 실행중인 Scene을 다시 로드한다.
    /// </summary>
    /// <returnsIEnumerator></returnsIEnumerator>
    IEnumerator reloadSceneProcess()
    {
        yield return UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        isReloadScene = false;
    }
}
