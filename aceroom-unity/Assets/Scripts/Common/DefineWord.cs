﻿using UnityEngine;
using System.Collections;

public class DefineWord
{
    /// <summary> 실행 중 Log를 표시할지 여부 </summary>
    public static bool __IS_SHOW_LOG = true;

    /// <summary> 기능 테스트 모드(Default Scene 기준 테스트용 메뉴 노출 여부) </summary>
    public static bool __IS_TEST = false;

}
