﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using DLLCore;

/// <summary>
/// [어반베이스] Crane 용 컴포넌트
/// 수정 시 어반베이스에 문의 필요.
/// </summary>
public class ProductListButton : MonoBehaviour
{
    class ProductData
    {
        public Button _button;
        public string _id;
        public string _path;
        public string _size;
        public string _color;
    }
    public Button prefabButton;
    private List<ProductData> productListButtons; 
    // Start is called before the first frame update
    void Start()
    {
        productListButtons = new List<ProductData>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InstantiateProductButton(string id, string name, string size, string color, string path, string thumbnail, string catalogThumbnail)
    {
        Button button = Instantiate(prefabButton, this.transform);
        if (button.GetComponentInChildren<Text>() != null)
            button.GetComponentInChildren<Text>().text = name;

        button.onClick.AddListener(() => { OnClickButton(name, size, color, thumbnail); });

        StartCoroutine(GetTexture(catalogThumbnail, button));
        
        ProductData prdData = new ProductData()
        {
            _button = button,
            _id = id,
            _path = path,
            //_thumbnail = catalogThumbnail,
            _size = size,
            _color = color
        };

        productListButtons.Add(prdData);
    }

    IEnumerator GetTexture(string path, Button button)
    {
        if (path == string.Empty)
            yield break;

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(path);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            button.GetComponent<RawImage>().texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
        }
    }

    void OnClickButton(string name, string size, string color, string thumbnail)
    {
        // show option list
        UISingleton.Instance.GetUIController().ShowProductOption(name, size, color, thumbnail);

        //UIController.Instance.productOptionUIManager.gameObject.SetActive(true);
    }

    public void ClearListButton()
    {
        Button[] trs = GetComponentsInChildren<Button>();
        foreach(Button tr in trs)
        {
            Destroy(tr.gameObject);
        }

        if (productListButtons != null)
            productListButtons.Clear();
    }

    public string GetPathFromID(string frameID)
    {
        ProductData prdData = productListButtons.Find((x) => x._id == frameID);
        if (prdData != null)
            return prdData._path;
        return string.Empty;
    }

    //public string GetThumbnailFromID(string frameID)
    //{
    //    ProductData prdData = productListButtons.Find((x) => x._id == frameID);
    //    if (prdData != null)
    //        return prdData._thumbnail;
    //    return string.Empty;
    //}


}
