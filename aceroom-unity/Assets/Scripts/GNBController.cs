﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// [어반베이스] Crane 용 컴포넌트
/// 수정 시 어반베이스에 문의 필요.
/// </summary>
public class GNBController : MonoBehaviour
{
    public Button map;
    public Button style;
    public Button design;
    public Button help;

    public GameObject mapView;

    // Start is called before the first frame update
    void Start()
    {
        map.onClick.AddListener(() => { _OnClickMap(); });
        style.onClick.AddListener(() => { _OnClickStyle(); });
        design.onClick.AddListener(() => { _OnClickDesign(); });

        help.onClick.AddListener(() => { _OnClickHelp(); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void _OnClickMap()
    {
        //  orthographic camera, map enabled
        Camera.main.orthographic = true;
        mapView.SetActive(true);
    }

    void _OnClickStyle()
    {
        
    }

    void _OnClickDesign()
    {
        // map diable, perspective camera
        mapView.SetActive(false);
        Camera.main.orthographic = false;
    }

    void _OnClickHelp()
    {

    }
}
