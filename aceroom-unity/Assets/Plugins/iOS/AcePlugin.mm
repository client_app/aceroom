//
//  AcePlugin.m
//  Unity-iPhone
//
//  Created by 김태원 on 09/09/2019.
//

#import "AcePlugin.h"

#define ACE_EXTERNC extern "C"

static bool gTestOptionCollider = true;

@implementation AcePlugin


#pragma mark - Singleton
+ (instancetype)sharedInstance {
    static AcePlugin *shared = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[AcePlugin alloc] init];
    });
    
    return shared;
}

#pragma mark - Plugin Util
// When native code plugin is implemented in .mm / .cpp file, then functions
// should be surrounded with extern "C" block to conform C function naming rules

// Helper method to create C string copy

//char* MakeStringCopy (NSString *str){
//
//    //    return MakeStringCopy([[NSString stringWithString:str] UTF8String]);
//    //    const char* res =[[NSString stringWithString:str] UTF8String];
//    const char* res  =[str cStringUsingEncoding:NSUTF8StringEncoding];
//    return (char*)res;
//}

char *MakeStringCopy(const char* string) {
    
    if (string == NULL)
        
        return NULL;
    
    size_t len = strlen(string) + 1;
    char* res = (char*) malloc (len);
    
    strcpy (res, string);
    
    return res;
    
}

const char* NSStringToChar(NSString * str)
{
    NSString *forUnity = [[NSString alloc] initWithFormat:@"%@", str];
    
    return MakeStringCopy( [forUnity UTF8String] );
}

-(void)sendMassageForObj:(NSString *)objName andMethod:(NSString *)methodName andParam:(NSString *)message{
    const char* charObjName = NSStringToChar(objName);
    const char* charMethodName = NSStringToChar(methodName);
    const char* charMessage = NSStringToChar(message);
    UnitySendMessage(charObjName, charMethodName, charMessage);
}

void callUnityObject(const char* object, const char* method, const char *parameter)
{
    UnitySendMessage(object, method, parameter);
}

NSString* MakeNSString (const char* string) {
    if (string) {
        return [NSString stringWithUTF8String: string];
    } else {
        return [NSString stringWithUTF8String: ""];
    }
}

// Helper method to create C String copy from NSString
const char* MakeCString(NSString *str) {
    str = [str stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    
    const char* string = [str UTF8String];
    if (string == NULL) {
        return NULL;
    }
    
    char *buffer = (char*)malloc(strlen(string) + 1);
    strcpy(buffer, string);
    return buffer;
    
    
    //    NSString *forUnity = [[NSString alloc] initWithFormat:@"%@", str];
    //
    ////    return MakeStringCopy( [forUnity UTF8String] );
    //    return [forUnity UTF8String];
}

#pragma mark - Unity Plugin 공통부 호출
-(void)unitySetIsNotch:(BOOL)isNotch{
    if(isNotch){
        [self SendMessageForMethod:@"setIsNotch" andParam:@"true"];
    }else{
        [self SendMessageForMethod:@"setIsNotch" andParam:@"false"];
    }
    
}
-(void)changeSceneDefault{
    [self changeSceneDefault:@""];
}
-(void)changeSceneDefault:(NSString *)strMsg{
    [self SendMessageForMethod:@"CallChangeSceneDefault" andParam:strMsg];
    
}
-(void)changeSceneARMattress{
    [self changeSceneARMattress:@""];
}
-(void)changeSceneARMattress:(NSString *)strMsg{
    [self SendMessageForMethod:@"CallChangeSceneARMattress" andParam:strMsg];
}
-(void)changeSceneHomeEditor{
    [self changeSceneHomeEditor:@""];
}
-(void)changeSceneHomeEditor:(NSString *)strMsg{
    [self SendMessageForMethod:@"CallChangeSceneHomeEditor" andParam:strMsg];
}


-(void)callUnityLotationLandscape{
    [self SendMessageForMethod:@"CallUnityLotationLandscape" andParam:@""];
}

-(void)callUnityLotationPortrait{
    [self SendMessageForMethod:@"CallUnityLotationPortrait" andParam:@""];
}


#pragma mark - Unity Plugin Vuforia 호출
#pragma mark - Unity Plugin HomeEditor 호출


-(void)SendMessageForMethod:(NSString *)methodName andParam:(NSString *)param{
    ace_UnitySendMessageChar("MobileManager", MakeCString(methodName), MakeCString(param));
}

-(void)callEccoParam:(NSString *)param{
    ace_UnitySendMessageChar("MobileManager", "CallEcco", MakeCString(param));
}

ACE_EXTERNC void _iOSPluginShowToast (const char* strMessage);
void _iOSPluginShowToast (const char* strMessage) {
    NSLog(@"call _iOSPluginShowToast %s", strMessage);
    
//    [kUnityBridge showToast:MakeNSString(strMessage)];
    
}

ACE_EXTERNC const char* _iOSPluginCallDeviceVersion ();
const char*  _iOSPluginCallDeviceVersion () {
    NSLog(@"call _iOSPluginCallDeviceVersion");
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSLog(@"call _iOSPluginCallDeviceVersion %@", version);
    
    ace_UnitySendMessageChar("MobileManager", "CallEcco", MakeCString(version));
    return MakeCString(version);
}


ACE_EXTERNC void _iOSPluginCallMainMenu(const char* strOldSceneName);
void _iOSPluginCallMainMenu(const char* strOldSceneName) {
    NSLog(@"메인 메뉴 호출 [%s]", strOldSceneName);
    
    //메인화면 이동
    //    ace_UnitySendMessageChar("MobileManager", "CallChangeScene", "Default");
    //매트리스 분석 화면 이동
    //    ace_UnitySendMessageChar("MobileManager", "CallChangeScene", "ARMattress");
    //홈에디터 화면 이동
    //    ace_UnitySendMessageChar("MobileManager", "CallChangeScene", "HomeEditor");
    //    TestVC
    
    if(strcmp(strOldSceneName, "HomeEditorScene") == 0)
    {
        //HomeEditorScene에서 돌아옴.
        [kUnityBridge CallMapVC];
    }else
    {
        [kUnityBridge CallMainMenu];
    }
}

-(void)callNowScene{
    ace_UnitySendMessageChar("MobileManager", "CallNowScene", "");
}

ACE_EXTERNC void _iOSPluginCallNowScene(const char* strSceneName);
void _iOSPluginCallNowScene(const char* strSceneName) {
    NSLog(@"유니티의 현제 Scene 호출");
}

ACE_EXTERNC void _iOSPluginCallScreenShot(const char* strImgPath);
void _iOSPluginCallScreenShot(const char* strImgPath) {
    NSLog(@"스크린샷 호출");
    [kUnityBridge saveScreenShot:MakeNSString(strImgPath)];
}

ACE_EXTERNC void _iOSPluginCallShare(const char* strImgPath);
void _iOSPluginCallShare(const char* strImgPath) {
    NSLog(@"공유 호출");
    [kUnityBridge shareScreenShot:MakeNSString(strImgPath)];
}

ACE_EXTERNC void _iOSPluginCallTutorial(const char* strSceneName);
void _iOSPluginCallTutorial(const char* strSceneName) {
    NSLog(@"튜토리얼 호출");
    
    [kUnityBridge OpenTutorial:MakeNSString(strSceneName)];
    
//    if(strcmp(strSceneName, "ARMattress")){
//        NSLog(@"매트리스 분석 튜토리얼 호출");
//    }else if(strcmp(strSceneName, "HomeEditor")){
//        NSLog(@"홈에디터 튜토리얼 호출");
//    }
}

-(void)loadHomeUnitJson:(NSString *)jsonString{
    NSLog(@"도면 호출 요청");
    ace_UnitySendMessage("MobileManager", "LoadHomeUnit", jsonString);
}

ACE_EXTERNC void _iOSPluginLoadUnitComplete();
void _iOSPluginLoadUnitComplete() {
    NSLog(@"도면 호출 완료");
    [kUnityBridge showToast:@"도면 호출 완료"];
}

ACE_EXTERNC void _iOSPluginCallCatalog();
void _iOSPluginCallCatalog() {
    NSLog(@"홈에디터 제품목록 호출");
	[kUnityBridge OpenHomeEditorProduct];
}

-(void)loadHomeProduct:(NSString *)jsonString{
    NSLog(@"제품 호출 요청");
    ace_UnitySendMessage("MobileManager", "LoadHomeProduct", jsonString);
}

ACE_EXTERNC void _iOSPluginLoadProductComplete();
void _iOSPluginLoadProductComplete() {
    NSLog(@"제품 호출 완료");
    [kUnityBridge showToast:@"제품 호출 완료"];
}

-(void)LoadHomeFurniture:(NSString *)jsonString{
    NSLog(@"제품(가구) 호출 요청");
    ace_UnitySendMessage("MobileManager", "LoadHomeFurniture", jsonString);
}

ACE_EXTERNC void iOSPluginCallbackLoadFurnitureComplete();
void iOSPluginCallbackLoadFurnitureComplete() {
    NSLog(@"제품(가구) 호출 완료");
    [kUnityBridge showToast:@"제품(가구) 호출 완료"];
}

ACE_EXTERNC void _iOSPluginCallbackLoadProductViewerScene();
void _iOSPluginCallbackLoadProductViewerScene() {
    NSLog(@"제품 호출 완료");
}

ACE_EXTERNC void _iOSPluginCallbackLoadProductView();
void _iOSPluginCallbackLoadProductView() {
    NSLog(@"제품 호출 완료");
}

-(void)clearHomeEditor{
    NSLog(@"홈에디터 화면 초기화 요청");
    ace_UnitySendMessageChar("MobileManager", "ClearHomeEditor", "");
}

ACE_EXTERNC void _iOSPluginCallHomeOption(const char* jsonString);
void _iOSPluginCallHomeOption(const char* jsonString) {
    NSLog(@"홈에디터 설정화면 호출");
    
    [kUnityBridge OpenHomeEditorOption:MakeNSString(jsonString)];
}


-(void)changeWallPaperStyle:(NSString *)jsonString{
    NSLog(@"도면 벽지 변경 : %@", jsonString);
    ace_UnitySendMessage("MobileManager", "ChangeWallPaperStyle", jsonString);
}

-(void)changeFloorStyle:(NSString *)jsonString{
    NSLog(@"도면 바닥재 변경 : %@", jsonString);
    ace_UnitySendMessage("MobileManager", "ChangeFloorStyle", jsonString);
}

/*!
   Adbrix Custom Action 용 메소드
*/
ACE_EXTERNC void iOSPluginCallStatic(const char* msg);
void iOSPluginCallStatic(const char* msg) {
    NSLog(@"Adbrix Custom Action");
    [kUnityBridge adbrixCustomAction:MakeNSString(msg)];
}

extern "C" {
  float FooPluginFunction();
}

/**
 * 화면 설정 적용 - 사양
 */
-(void)LoadHomeOptionEffect:(HomeEditorQuality)quality{
    
    NSString *sendMsg;
    
    if(quality == kHomeEditorQualityHigh){
        sendMsg = @"2";
        NSLog(@"화면 설정 적용 - 사양 : 상");
    }else if(quality == kHomeEditorQualityMiddle){
        sendMsg = @"1";
        NSLog(@"화면 설정 적용 - 사양 : 중");
    }else if(quality == kHomeEditorQualityLow){
        sendMsg = @"0";
        NSLog(@"화면 설정 적용 - 사양 : 하");
    }else{
        sendMsg = @"1";
    }
    
    ace_UnitySendMessage("MobileManager", "LoadHomeOptionEffect", sendMsg);
}

/**
 * 화면 설정 적용 - 벽 충돌
 * @param isBool
 */
-(void)LoadHomeOptionCollision:(BOOL)isBool{
    NSLog(@"도면 바닥재 변경 : %@", isBool?@"YES":@"NO");
    
    NSString *strBool;
    if (isBool) {
        strBool = @"true";
    }else{
        strBool = @"false";
    }
    
    ace_UnitySendMessage("MobileManager", "LoadHomeOptionCollision", strBool);
}


////

ACE_EXTERNC void ace_UnitySendMessage(const char *name, const char *method, NSString *params);
void ace_UnitySendMessage(const char *name, const char *method, NSString *params){
    if(params == nil){
        params = @"";
    }
    const char* sendParam = MakeCString(params);
    ace_UnitySendMessageChar(name, method, sendParam);
}

ACE_EXTERNC void ace_UnitySendMessageChar(const char *name, const char *method, const char *params) {
    if ((params != NULL) && (params[0] == '\0')) {
        printf("c is empty\n");
    }
    
    UnitySendMessage(name, method, params);
}

#pragma mark - TEST

ACE_EXTERNC void _iOSPluginCallTEST (const char* strTestMsg);
void _iOSPluginCallTEST (const char* strTestMsg) {
    NSLog(@"call _iOSPluginCallTEST %s", strTestMsg);
    
    const char* returnMethod = "";
    const char* returnMsg = "";
    
    
    if(strcmp("LoadRoom",strTestMsg) == 0){//방 호출
        returnMethod = "LoadHomeUnit";
        returnMsg = "{\"id\": \"1\", \"unit_id\": \"6\", \"file_path\": \"unit/5600c1c271da53ab8ad712e9.dae\", \"is_default\": true, \"m_attach_type_id\": null, \"extension\": \"dae\", \"enabled\": true, \"created_date\": \"2019-07-30T09:32:09.072Z\", \"updated_date\": null, \"deleted_date\": null}";
    }
    else if(strcmp("LoadProduct",strTestMsg) == 0){//제품 호출
        returnMethod = "LoadHomeProduct";
        returnMsg = "{\"frame\":{\"frameid\": \"3d74a410-a1f3-11e9-a718-9ba302dbf24d\",\"path\": \"/product/LUNATO3_K_Gray.zip\",\"sfbPath\": \"/product/###.sfb\",\"3dModelPath\": \"\",\"thumbnail\": \"/product/LUNATO3_K_Gray_thumbnail.png\",\"catalogThumbnail\": \"/product/LUNATO3_K_Gray.png\",\"name\": \"LUNATO3\",\"size\": \"K\",\"color\": \"Gray\",\"isDisplay\": true,\"mattressOffset\": [3.817524,23.96977,331.5197],\"mattressScale\": 1},\"mattress\":{\"mattress\": \"9a113be0-c7cb-11e9-a8ed-bb36fb6c62a7\",\"path\": \"/product/HT3_K.zip\",\"sfbPath\": \"/product/###.sfb\",\"3dModelPath\": \"\",\"name\": \"HT3\",\"size\": \"K\"}}";
    }
    else if(strcmp("ClearAll",strTestMsg) == 0){//전체 삭제
        returnMethod = "ClearHomeEditor";
        returnMsg = "";//값 없음
    }
    else if(strcmp("OptionHigh",strTestMsg) == 0){//옵션
        returnMethod = "LoadHomeOptionEffect";
        returnMsg = "0";//하
    }
    else if(strcmp("OptionMiddle",strTestMsg) == 0){//옵션
        returnMethod = "LoadHomeOptionEffect";
        returnMsg = "1";//중
    }
    else if(strcmp("OptionLow",strTestMsg) == 0){//옵션
        returnMethod = "LoadHomeOptionEffect";
        returnMsg = "2";//상
    }
    else if(strcmp("OptionCollision",strTestMsg) == 0){//충돌
        returnMethod = "LoadHomeOptionCollision";
        
        if(gTestOptionCollider){
            returnMsg = "true";
        }else{
            returnMsg = "false";
        }
        gTestOptionCollider = !gTestOptionCollider;
    }
    
    
    NSLog(@"end _iOSPluginCallTEST (%s, %s)", returnMethod, returnMsg);
    
    ace_UnitySendMessageChar("MobileManager", returnMethod, returnMsg);
    
}

@end
