package com.acebed.aceroom.plugin;

public enum QualityOption {
    High,
    Middle,
    Low
}
